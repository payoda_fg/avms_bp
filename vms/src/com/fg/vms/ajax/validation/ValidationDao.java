package com.fg.vms.ajax.validation;

import com.fg.vms.admin.dto.UserDetailsDto;

public interface ValidationDao {
    
    /**
     * Validate login id.
     * 
     * @param loginId
     *            the login id
     * @param appDetails
     *            the app details
     * @return the string
     */
    public String validateLoginId(String loginId, UserDetailsDto appDetails);

    /**
     * Check duplicate email.
     * 
     * @param emailId
     *            the email id
     * @param appDetails
     *            the app details
     * @return the string
     */
    public String checkDuplicateEmail(String emailId, UserDetailsDto appDetails);

}
