/*
 * ValidationServlet.java
 */
package com.fg.vms.ajax.validation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fg.vms.admin.dao.CustomerDao;
import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.CustomerDaoImpl;
import com.fg.vms.admin.dao.impl.UserRolesDaoImpl;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.NaicsMasterDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.NaicsMasterDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorRolesDaoImpl;
import com.fg.vms.util.Constants;

/**
 * Ajax validation.
 */
public class ValidationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ValidationServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result = null;
		UsersDao usersDaoImpl;
		VendorDao vendorDao;
		ValidationDao validationDao;
		RolesDao userRolesDaoImpl;
		CustomerDao customerDao;
		NaicsMasterDao naicsMasterDao;
		CommonDao commonDao = new CommonDaoImpl();
		String object = request.getParameter("object");
		String type = request.getParameter("type");

		if (type != null) {
			if (type.equalsIgnoreCase("U")) {// User Login Id

				if (object != null && object.trim().length() != 0) {
					usersDaoImpl = new UsersDaoImpl();
					result = usersDaoImpl.checkUserLoginAndEmailIdExist(
							object.trim(), null, appDetails);
				}
			} else if (type.equalsIgnoreCase("VU")) {// Vendor User Login Id
				if (object != null && object.trim().length() != 0) {
				   
					result = commonDao.validateLoginId(object.trim(),
							appDetails);
				}

			} else if (type.equalsIgnoreCase("E")) {// User EmailId
				if (object != null && object.trim().length() != 0) {
					usersDaoImpl = new UsersDaoImpl();
					result = usersDaoImpl.checkUserLoginAndEmailIdExist(null,
							object.trim(), appDetails);
				}
			} else if (type.equalsIgnoreCase("VE") || type.equalsIgnoreCase("VT2E")) {// VE = Vendor User EmailId, & VT2E = Tier 2 Vendor Email Id
				if (object != null && object.trim().length() != 0) {
					result = commonDao.checkDuplicateEmail(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("C")) {// vendor code
				if (object != null && object.trim().length() != 0) {
					result = commonDao.validateVendorCode(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("R")) {// Role
				if (object != null && object.trim().length() != 0) {
					userRolesDaoImpl = new UserRolesDaoImpl();
					result = userRolesDaoImpl.checkDuplicateRole(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("VR")) {// Role
				if (object != null && object.trim().length() != 0) {
					userRolesDaoImpl = new VendorRolesDaoImpl();
					result = userRolesDaoImpl.checkDuplicateRole(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("CC")) {// Company Code
				if (object != null && object.trim().length() != 0) {
					customerDao = new CustomerDaoImpl();
					result = customerDao.checkDuplicateCustCode(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("D")) {// Duns Number
				if (object != null && object.trim().length() != 0) {
					customerDao = new CustomerDaoImpl();
					result = customerDao.checkDuplicateDunsNo(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("T")) {// Tax ID
				if (object != null && object.trim().length() != 0) {
					customerDao = new CustomerDaoImpl();
					result = customerDao.checkDuplicateTaxId(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("NC")) {// Naics Code
				if (object != null && object.trim().length() != 0) {
					naicsMasterDao = new NaicsMasterDaoImpl();
					result = naicsMasterDao.checkDuplicateNaicsCode(
							object.trim(), appDetails);
				}
			} else if (type.equalsIgnoreCase("P")) {// Count Prime Vendor
				if (object != null && object.trim().length() != 0) {
					vendorDao = new VendorDaoImpl();
					if ((vendorDao.countPrimeVendors(appDetails) < (long) appDetails
							.getCustomerSLA().getLicencedVendors())) {
						result = "";
					} else {
						result = "exceeded";
					}

				}
			} else if (type.equalsIgnoreCase("DB")) {// Database name
				if (object != null && object.trim().length() != 0) {
					customerDao = new CustomerDaoImpl();
					result = customerDao.checkDuplicateDatabaseName(
							object.trim(), appDetails);
				}
			} else if (type.equalsIgnoreCase("CU")) {// customer user
				if (object != null && object.trim().length() != 0) {
					validationDao = new CustomerDaoImpl();
					result = validationDao.validateLoginId(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("CE")) { // customer emailId
				if (object != null && object.trim().length() != 0) {
					validationDao = new CustomerDaoImpl();
					result = validationDao.checkDuplicateEmail(object.trim(),
							appDetails);
				}
			} else if (type.equalsIgnoreCase("delete")) { // customer emailId
				if (object != null && object.trim().length() != 0) {
					vendorDao = new VendorDaoImpl();
					result = vendorDao.deleteVendor(
							Integer.parseInt(object.trim()), appDetails);
				}
			} else if (type.equalsIgnoreCase("URL")) { // Application URL
				// validation
				if (object != null && object.trim().length() != 0) {
					customerDao = new CustomerDaoImpl();
					result = customerDao.checkDuplicateURL(object.trim() + "."
							+ Constants.getString(Constants.DOMAIN_NAME),
							appDetails);
				}
			}

		}

		if (result != null
				&& (result.equalsIgnoreCase("vendorCode")
						|| result.equalsIgnoreCase("userLogin")
						|| result.equalsIgnoreCase("userEmail")
						|| result.equalsIgnoreCase("notUnique")
						|| result.equalsIgnoreCase("exceeded")
						|| result.equalsIgnoreCase("duplicate") || result
							.equalsIgnoreCase("reference"))) {
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");

			response.getWriter().write("<valid>false</valid>");
		} else {
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");

			response.getWriter().write("<valid>true</valid>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

}
