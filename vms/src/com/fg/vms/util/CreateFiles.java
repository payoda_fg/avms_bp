package com.fg.vms.util;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class CreateFiles {
	private static final String CERTIFICATE_PATH="E:\\temp";
	private static final String OTHER_CERTIFICATE_PATH="E:\\temp\\other";
	private static final String DRIVWE_CLASS="com.mysql.jdbc.Driver";
	private static final String URL="jdbc:mysql://localhost:3306/avms_test";
	private static final String USERNAME="root";
	private static final String PASSWORD="root";
	
//	private static final String URL="jdbc:mysql://52.4.81.50:3306/bpavmsystem";
//	private static final String USERNAME="root";
//	private static final String PASSWORD="Matthew196#1";
	
	public static void main(String[] args) {
		
		try{  
			Class.forName(DRIVWE_CLASS);  
			Connection con=DriverManager.getConnection(URL,USERNAME,PASSWORD);  
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from customer_vendorcertificate");  
			
			while(rs.next()){  
				if(rs.getBlob(2) != null && rs.getString(9) !=null){
			Integer cid= rs.getInt(1);
			String fileName= URLEncoder.encode(rs.getString(9), "UTF-8");
			Integer vendoid = rs.getInt(13);
			
//			if(rs.getBlob(2) != null){	
			File file = new File(CERTIFICATE_PATH);
			String path=file.getPath()+"\\";
//			String value = " \""+path+"\" ";
			file.mkdirs();
			String newFileName=file.getPath()+"\\"+cid+"-"+vendoid+"-"+fileName;
			FileOutputStream fop = new FileOutputStream(newFileName);
			Blob contentInBytes = rs.getBlob(2); 
			byte[] blobAsBytes = contentInBytes.getBytes(1, (int)contentInBytes.length());
			fop.write(blobAsBytes);
			fop.flush();
			fop.close();

			PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO customer_vendor_certificatedocument (certificate_id,vendorid,filename,physicalpath,certificate_othercertificate) VALUES (?, ?, ?, ?, ?);");
			preparedStatement.setInt(1, cid);
			preparedStatement.setInt(2, vendoid);
			preparedStatement.setString(3, newFileName);
			preparedStatement.setString(4, path);
			preparedStatement.setString(5, "C");
			preparedStatement.executeUpdate(); 
			
//			}
			
				}
			
			}
			
			ResultSet rs1=stmt.executeQuery("select * from customer_vendorothercertificate");
			
			while(rs1.next()){  
				if(rs1.getBlob(4) != null && rs1.getString(9) !=null){
				Integer cid= rs1.getInt(1);
//				String fileName=rs1.getString(6);
				String fileName= URLEncoder.encode(rs1.getString(6), "UTF-8");
				Integer vendoid = rs1.getInt(7);
				
//				if(rs1.getBlob(4) != null){	
				File file = new File(OTHER_CERTIFICATE_PATH);
				String path=file.getPath()+"\\";
//				String value = " \""+path+"\" ";
				file.mkdirs();
				String newFileName=file.getPath()+"\\"+cid+"-"+vendoid+"-"+fileName;
				FileOutputStream fop = new FileOutputStream(newFileName);
				Blob contentInBytes = rs1.getBlob(4); 
				byte[] blobAsBytes = contentInBytes.getBytes(1, (int)contentInBytes.length());
				fop.write(blobAsBytes);
				fop.flush();
				fop.close();

				PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO customer_vendor_certificatedocument (certificate_id,vendorid,filename,physicalpath,certificate_othercertificate) VALUES (?, ?, ?, ?, ?);");
				preparedStatement.setInt(1, cid);
				preparedStatement.setInt(2, vendoid);
				preparedStatement.setString(3, newFileName);
				preparedStatement.setString(4, path);
				preparedStatement.setString(5, "O");
				preparedStatement.executeUpdate(); 
				
				}
				
				}
			
			con.close(); 
			System.out.println("Sucsussfully completed");
			}catch(Exception e){ System.out.println(e);} 
			}
	/*
	 * SessionFactory sessionFactory = new AnnotationConfiguration()
	 * .configure("hibernate.customer.cfg.xml") .setProperty(
	 * "hibernate.connection.url","jdbc:mysql://localhost:3306/avms_test")
	 * .setProperty("hibernate.connection.username","root")
	 * .setProperty("hibernate.connection.password", "root")
	 * .buildSessionFactory(); Session session=sessionFactory.openSession();
	 * Transaction t=session.beginTransaction(); List<VendorCertificate>
	 * vendorCertificate=session.c
	 * session.createCriteria(VendorCertificate.class).list(); t.commit();
	 * for(VendorCertificate vc:vendorCertificate){ if(vc.getContent() != null){
	 * try { File file = new File("E:\\temp"); file.mkdirs(); FileOutputStream
	 * fop = new FileOutputStream(file.getPath()+"\\"+vc.getFilename()); Blob
	 * contentInBytes = vc.getContent(); byte[] blobAsBytes =
	 * contentInBytes.getBytes(1, (int)contentInBytes.length());
	 * fop.write(blobAsBytes); fop.flush(); fop.close();
	 * CustomerVendorCertificateDocument cvcd=new
	 * CustomerVendorCertificateDocument(); cvcd.setCertificateId(vc.getId());
	 * cvcd.setFileName(vc.getFilename());
	 * cvcd.setPhysicalPath(file.getPath()+"\\");
	 * cvcd.setVendorId(vc.getVendorId().getId());
	 * cvcd.setCertificateOtherCertificate("C"); t=session.beginTransaction();
	 * session.save(cvcd); t.commit(); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * } } System.out.println("All files are created"); session.close(); }
	 */
}
