package com.fg.vms.util;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

public class CompileJasper {
	
	public static void main(String[] args)
	{
		System.out.println("Compiling jrxml");
		try {
//	JasperCompileManager.compileReportToFile("WebContent/jasper/certificate_expiration_notification_email_report.jrxml","WebContent/jasper/certificate_expiration_notification_email_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/vendor_status_pendingreview_search.jrxml","WebContent/jasper/vendor_status_pendingreview_search.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/prime_vendor_search.jrxml","WebContent/jasper/prime_vendor_search.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/vendor_commodities_not_saved_report.jrxml","WebContent/jasper/vendor_commodities_not_saved_report.jasper");			
//	JasperCompileManager.compileReportToFile("WebContent/jasper/spend_data_dashboard_report.jrxml","WebContent/jasper/spend_data_dashboard_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/tier2_reporting_summary.jrxml","WebContent/jasper/tier2_reporting_summary.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity_report_direct.jrxml","WebContent/jasper/supplier_diversity_report_direct.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity_report_both.jrxml","WebContent/jasper/supplier_diversity_report_both.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity_report_indirect.jrxml","WebContent/jasper/supplier_diversity_report_indirect.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/indirect_spend_report.jrxml","WebContent/jasper/indirect_spend_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/direct_spend_report.jrxml","WebContent/jasper/direct_spend_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/total_sales_report.jrxml","WebContent/jasper/total_sales_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/spend_by_agency_report.jrxml","WebContent/jasper/spend_by_agency_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/indirect_procurement_commodities_report.jrxml","WebContent/jasper/indirect_procurement_commodities_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity_report_both_eth.jrxml","WebContent/jasper/supplier_diversity_report_both_eth.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity_report_direct_eth.jrxml","WebContent/jasper/supplier_diversity_report_direct_eth.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity_report_indirect_eth.jrxml","WebContent/jasper/supplier_diversity_report_indirect_eth.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/bpvendor_count_by_state_report.jrxml","WebContent/jasper/bpvendor_count_by_state_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_count_by_state_report.jrxml","WebContent/jasper/supplier_count_by_state_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/vendor_status_breakdown_report.jrxml","WebContent/jasper/vendor_status_breakdown_report.jasper");
	JasperCompileManager.compileReportToFile("WebContent/jasper/supplier_diversity.jrxml","WebContent/jasper/supplier_diversity.jasper");	
//	JasperCompileManager.compileReportToFile("WebContent/jasper/diversity_analysis_report.jrxml","WebContent/jasper/diversity_analysis_report.jasper");		
//	JasperCompileManager.compileReportToFile("WebContent/jasper/vendor_master.jrxml","WebContent/jasper/vendor_master.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/address_template.jrxml","WebContent/jaspers/address_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/bussinessbio_safety_template.jrxml","WebContent/jaspers/bussinessbio_safety_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/contact_info_template.jrxml","WebContent/jaspers/contact_info_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/custom_supplier_search_report.jrxml","WebContent/jasper/custom_supplier_search_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_bussinessarea_template.jrxml","WebContent/jaspers/vendor_bussinessarea_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_bussinessbio_template.jrxml","WebContent/jaspers/vendor_bussinessbio_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_divcert_template.jrxml","WebContent/jaspers/vendor_divcert_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_meetinginfo_template.jrxml","WebContent/jaspers/vendor_meetinginfo_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_othercert_template.jrxml","WebContent/jaspers/vendor_othercert_template.jasper");	
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_owner_template.jrxml","WebContent/jaspers/vendor_owner_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_reference_template.jrxml","WebContent/jaspers/vendor_reference_template.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_search_report.jrxml","WebContent/jaspers/vendor_search_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jaspers/vendor_status_report.jrxml","WebContent/jaspers/vendor_status_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/tier2_spend_report.jrxml","WebContent/jasper/tier2_spend_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/tier2_indirect_spend_report.jrxml","WebContent/jasper/tier2_indirect_spend_report.jasper");
//	JasperCompileManager.compileReportToFile("WebContent/jasper/tier2_direct_spend_report.jrxml","WebContent/jasper/tier2_direct_spend_report.jasper");
		
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Compiled jrxml");
	}

}
