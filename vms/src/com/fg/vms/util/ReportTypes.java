/**
 * 
 */
package com.fg.vms.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gpirabu
 * 
 */
public enum ReportTypes {

	AGENCYDASHBOARD("Agency Dashboard", 1), DIVERSITYDASHBOARD(
			"Diversity Dashboard", 2), SUPPLIERDASHBOARD("Supplier Dashboard",
			3), ETHNICITYDASHBOARD("Ethnicity Dashboard", 4), TOTALSPEND(
			"Total Spend", 5), DIRECTSSPEND("Directs Spend", 6), INDIRECTSPEND(
			"Indirect Spend", 7), DIVERSITYSTATUS("Diversity Status", 8), ETHNICITYANALYSIS(
			"Ethnicity Analysis", 9), SPENDDASHBOARD("Spend Dashboard", 10), STATUSDASHBOARD(
			"Status Dashboard", 11), BPVENDORSCOUNT("BP Vendors Count",12);

	private ReportTypes(String name, Integer index) {
		this.name = name;
		this.index = index;
	}

	/**
	 * A mapping between the integer code and its corresponding Status to
	 * facilitate lookup by code.
	 */
	private static Map<String, ReportTypes> codeToReportMapping;

	public static ReportTypes getReportTypes(String type) {
		if (codeToReportMapping == null) {
			initMapping();
		}
		return codeToReportMapping.get(type);
	}

	private static void initMapping() {
		codeToReportMapping = new HashMap<String, ReportTypes>();
		for (ReportTypes r : values()) {
			codeToReportMapping.put(r.getName(), r);
		}
	}

	private Integer index;
	private String name;

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
