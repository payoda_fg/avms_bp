/**
 * SelfRegistrationValidation.java
 */
package com.fg.vms.util;

import java.util.List;

import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorOwner;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;

/**
 * Validate self registration on submit at end of the page.
 * 
 * @author vinoth
 * 
 */
public class SelfRegistrationValidation {

	/**
	 * Validate vendor contact information.
	 * 
	 * @param contact
	 * @param vendorContactErrors
	 */
	public static List<String> validateContactInfo(VendorContact contact,
			VendorContact businessContact, List<String> vendorContactErrors) {

		/*
		 * Check any mandatory field empty in preparer contact, if so add that
		 * into error list.
		 */
		if (null == contact.getFirstName()) {
			vendorContactErrors.add("FirstName in Contact Information");
		}

		if (null == contact.getLastName()) {
			vendorContactErrors.add("LastName in Contact Information");
		}

		if (null == contact.getDesignation()) {
			vendorContactErrors.add("Title in Contact Information");
		}

		if (null == contact.getPhoneNumber()) {
			vendorContactErrors.add("PhoneNumber in Contact Information");
		}

		/*
		 * Check any mandatory field empty in business contact, if so add that
		 * into error list.
		 */
		if (null == businessContact.getFirstName()) {
			vendorContactErrors
					.add("Preparer FirstName in Contact Information");
		}

		if (null == businessContact.getLastName()) {
			vendorContactErrors.add("Preparer LastName in Contact Information");
		}

		if (null == businessContact.getDesignation()) {
			vendorContactErrors.add("Preparer Title in Contact Information");
		}

		if (null == businessContact.getPhoneNumber()) {
			vendorContactErrors
					.add("Preparer PhoneNumber in Contact Information");
		}

		return vendorContactErrors;
	}

	/**
	 * Validate vendor company information.
	 * 
	 * @param vendorMaster
	 * @param vendorErrors
	 * @return
	 */
	public static List<String> validateCompanyInfo(VendorMaster vendorMaster,
			List<String> vendorCompanyErrors) {

		if (vendorMaster.getVendorName().isEmpty()) {
			vendorCompanyErrors.add("VendorName in Company Information");
		}

		/*if (null == vendorMaster.getDunsNumber()
				|| vendorMaster.getDunsNumber().isEmpty()) {
			vendorCompanyErrors.add("Duns Number in Company Information");
		}*/

		if (vendorMaster.getTaxId().isEmpty()) {
			vendorCompanyErrors.add("TaxId in Company Information");
		}

		if (vendorMaster.getCompanyType().isEmpty()) {
			vendorCompanyErrors.add("CompanyType in Company Information");
		}

		if (null == vendorMaster.getNumberOfEmployees()) {
			vendorCompanyErrors
					.add("Number Of Employees in Company Information");
		}

		if (vendorMaster.getBusinesstype().isEmpty()) {
			vendorCompanyErrors.add("BusinessType in Company Information");
		}

		if (null == vendorMaster.getYearOfEstablishment()) {
			vendorCompanyErrors
					.add("Year of establishment in Company Information");
		}

		if (null == vendorMaster.getEmailId() || vendorMaster.getEmailId().isEmpty()) {
			vendorCompanyErrors.add("Email ID in Company Information");
		}
		
		if (null == vendorMaster.getWebsite() || vendorMaster.getWebsite().isEmpty()) {
			vendorCompanyErrors.add("Website in Company Information");
		}
		return vendorCompanyErrors;
	}

	/**
	 * Validate vendor address (Physical) information.
	 * 
	 * @param addressMaster1
	 * @param addressMaster2
	 * @param vendorErrors
	 * @return
	 */
	public static List<String> validateAddressInfo(
			CustomerVendorAddressMaster addressMaster1,
			List<String> vendorAddressErrors) {

		if (null != addressMaster1) {

			if (null == addressMaster1.getAddress()) {
				vendorAddressErrors
						.add("Address in Company Address (Physical) Information");
			}

			if (addressMaster1.getCity().isEmpty()) {
				vendorAddressErrors
						.add("City in Company Address (Physical) Information");
			}

//			if (addressMaster1.getCountry().isEmpty()) {
//				vendorAddressErrors
//						.add("Country in Company Address (Physical) Information");
//			} else if (addressMaster1.getCountry().equals("1")
//					&& addressMaster1.getState().isEmpty()) {
//				vendorAddressErrors
//						.add("State in Company Address (Physical) Information");
//			} else if (!addressMaster1.getCountry().equals("1")
//					&& addressMaster1.getProvince().isEmpty()) {
//				vendorAddressErrors
//						.add("Province in Company Address (Physical) Information");
//			}

			if (addressMaster1.getZipCode().isEmpty()) {
				vendorAddressErrors
						.add("ZipCode in Company Address (Physical) Information");
			}

			if (addressMaster1.getPhone().isEmpty()) {
				vendorAddressErrors
						.add("Phone in Company Address (Physical) Information");
			}
		}
		return vendorAddressErrors;
	}

	/**
	 * Validate vendor reference information.
	 * 
	 * @param vendorreference
	 * @param vendorErrors
	 * @return
	 */
	public static List<String> validateReferenceInfo(
			CustomerVendorreference vendorreference,
			List<String> vendorReferenceErrors) {

		if (null == vendorreference.getReferenceAddress()) {
			vendorReferenceErrors
					.add("Address in Vendor Reference (Reference1) Information");
		}

		if (null == vendorreference.getReferencePhone()) {
			vendorReferenceErrors
					.add("Phone in Vendor Reference (Reference1) Information");
		}

		if (null == vendorreference.getReferenceEmailId()) {
			vendorReferenceErrors
					.add("EmailID in Vendor Reference (Reference1) Information");
		}

		if (null == vendorreference.getReferenceCity()) {
			vendorReferenceErrors
					.add("City in Vendor Reference (Reference1) Information");
		}

		if (null == vendorreference.getReferenceZip()) {
			vendorReferenceErrors
					.add("Zip in Vendor Reference (Reference1) Information");
		}

		/*if (null == vendorreference.getReferenceCountry()) {
			vendorReferenceErrors
					.add("Country in Vendor Reference (Reference1) Information");
		} else if (vendorreference.getReferenceCountry().equals("1")
				&& null == vendorreference.getReferenceState()) {
			vendorReferenceErrors
					.add("State in Vendor Reference (Reference1) Information");
		} else if (!vendorreference.getReferenceCountry().equals("1")
				&& null == vendorreference.getReferenceProvince()) {
			vendorReferenceErrors
					.add("Province in Vendor Reference (Reference1) Information");
		}*/

		return vendorReferenceErrors;
	}
	
	/**
	 * Validate Vendor Company Ownership Information.
	 * 
	 * @param vendorMaster
	 * @param vendorErrors
	 * @return
	 */
	public static List<String> validateCompanyOwnershipInfo(List<CustomerVendorOwner> customerVendorOwner, List<String> vendorCompanyOwnershipErrors) {		
		if(customerVendorOwner.get(0).getOwnername().isEmpty()) {
			vendorCompanyOwnershipErrors.add("Owner Name in Company Ownership Information");
		}
		if(customerVendorOwner.get(0).getTitle().isEmpty()) {
			vendorCompanyOwnershipErrors.add("Title in Company Ownership Information");
		}
		if(customerVendorOwner.get(0).getEmail().isEmpty()) {
			vendorCompanyOwnershipErrors.add("Email Id in Company Ownership Information");
		}
		if(customerVendorOwner.get(0).getPhone().isEmpty()) {
			vendorCompanyOwnershipErrors.add("Phone No in Company Ownership Information");
		}
		/*if(customerVendorOwner.get(0).getMobile().isEmpty()) {
			vendorCompanyOwnershipErrors.add("Mobile No in Company Ownership Information");
		}*/
		if(customerVendorOwner.get(0).getGender().isEmpty()) {
			vendorCompanyOwnershipErrors.add("Gender in Company Ownership Information");
		}
		if(customerVendorOwner.get(0).getOwnersethinicity() == 0) {
			vendorCompanyOwnershipErrors.add("Ethnicity in Company Ownership Information");
		}
		if(customerVendorOwner.get(0).getOwnershippercentage() == null) {
			vendorCompanyOwnershipErrors.add("Ownership % in Company Ownership Information");
		}
		return vendorCompanyOwnershipErrors;
	}
}