/*
 * LogInitialization.java
 */
package com.fg.vms.util;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * The Class LogInitialization.
 */
@SuppressWarnings("serial")
public class LogInitialization extends HttpServlet {

    /** The logger. */
    private Logger logger = Constants.logger;

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) {
	try {
	    System.setProperty("vmslogfile",
		    Constants.getString(Constants.VMS_LOGFILE_PATH));
	    String log4jpropertyFilePath = config.getServletContext()
		    .getRealPath("/");
	    String rootPath = log4jpropertyFilePath
		    + Constants.getString(Constants.VMS_LOGFILE_NAME);
	    PropertyConfigurator.configure(rootPath);
	    logger.info("Message : " + "Log Initialized");
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
