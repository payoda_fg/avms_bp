/**
 * FileUploadProcess.java
 */
package com.fg.vms.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * The Class FileUploadProcess.
 * 
 * @author anand
 */
public class FileUploadProcess {

	private static Logger logger = Constants.logger;

	/**
	 * Check folder available.
	 * 
	 * @param realpath
	 *            the realpath
	 * @param foldername
	 *            the foldername
	 * @return true, if successful
	 */
	public static boolean checkFolderAvailable(String realpath,
			String foldername) {
		boolean flag = false;
		ArrayList<String> directoryList = new ArrayList<String>();
		try {
			File checkfolder = new File(realpath);
			File[] directorylist = checkfolder.listFiles();

			if (directorylist != null && directorylist.length != 0) {

				for (int i = 0; i < directorylist.length; i++) {
					if (directorylist[i].isDirectory()) {
						directoryList.add(directorylist[i].getName());
					}
				}
				for (int i = 0; i < directoryList.size(); i++) {
					if (directoryList.get(i).contains(foldername)) {
						flag = true;
					}
				}
			}

		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return flag;
	}

	/**
	 * Checking whether the folder is available or not.
	 * 
	 * @param realpath
	 *            the realpath
	 * @param foldername
	 *            the foldername
	 */
	public static boolean createFolder(String realpath, String foldername) {
		boolean createFolderReturnValue = false;
		try {
			createFolderReturnValue = new File(realpath + File.separator
					+ foldername).mkdirs();
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return createFolderReturnValue;
	}

	/**
	 * Concat the userid and image file name to save in DB + to assign filename
	 * for the uploaded file.
	 * 
	 * @param path
	 *            the path
	 * @param compCode
	 *            the comp code
	 * @return the string
	 */
	public static String createUploadImageFilePath(String path, String compCode) {
		String uploadedImagePath = "";
		try {
			uploadedImagePath = path + File.separator + compCode;
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return uploadedImagePath;
	}

	/**
	 * Checking the file(/logo) present in the contextpath for display in
	 * img src tag.
	 * 
	 * @param imagePath
	 *            the image path
	 * @param filename
	 *            the filename
	 * @return true, if successful
	 */
	public static boolean checkFileExistence(String imagePath, String filename) {
		boolean availablestatus = false;
		try {
			ArrayList<String> fileArrayList = new ArrayList<String>();
			File checkfile = new File(imagePath);
			File[] fileList = checkfile.listFiles();
			if (fileList != null && fileList.length != 0) {
				for (int i = 0; i < fileList.length; i++) {
					if (fileList[i].isFile()) {
						fileArrayList.add(fileList[i].getName());
					}
				}
				for (int i = 0; i < fileArrayList.size(); i++) {
					if (fileArrayList.get(i).contains(filename)) {
						availablestatus = true;
					}
				}
			}

		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return availablestatus;

	}

	/**
	 * Check image available.
	 * 
	 * @param imagePath
	 *            the image path
	 * @param userid
	 *            the userid
	 */
	public static void checkImageAvailable(String imagePath, int userid) {
		try {
			ArrayList<String> fileArrayList = new ArrayList<String>();
			File checkfile = new File(imagePath);
			File[] fileList = checkfile.listFiles();
			for (int i = 0; i < fileList.length; i++) {
				if (fileList[i].isFile()) {
					fileArrayList.add(fileList[i].getName());
				}
			}
			for (int i = 0; i < fileArrayList.size(); i++) {
				// String [] splitArr=fileArrayList.get(i).split("_");
				String[] splitArr = StringUtils
						.split(fileArrayList.get(i), "_");
				if (splitArr[0].equals(String.valueOf(userid))) {
					File deleteFile = new File(imagePath + fileArrayList.get(i));
					deleteFile.delete();
				}
			}
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
	}

	public static void removeFolder(String realpath, String folderName) {

		File directory = new File(realpath + File.separator + folderName);

		// make sure directory exists
		if (!directory.exists()) {
			logger.info("Directory does not exist.");

		} else {
			try {
				delete(directory);
			} catch (IOException e) {
				PrintExceptionInLogFile.printException(e);

			}
		}
		logger.info("Done");
	}

	/**
	 * delete the file
	 * 
	 * @param file
	 * @throws IOException
	 */
	public static void delete(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();
				logger.info("Directory is deleted : " + file.getAbsolutePath());

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					delete(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
					logger.info("Directory is deleted : "
							+ file.getAbsolutePath());
				}
			}

		} else {
			// if file, then delete it
			file.delete();
			logger.info("File is deleted : " + file.getAbsolutePath());
		}
	}
}
