/**
 * 
 */
package com.fg.vms.util;

/**
 * @author gpirabu
 * 
 */
public enum FrequencyPeriod {

    M("Monthly"), Q("Quarterly"), A("Annually");
    private String period;

    private FrequencyPeriod(String period) {
        this.period = period;
    }

    /**
     * @return the period
     */
    public String getPeriod() {
        return period;
    }

    /**
     * @param period
     *            the period to set
     */
    public void setPeriod(String period) {
        this.period = period;
    }

}
