/**
 * PrintExceptionInLogFile.java
 */
package com.fg.vms.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;

/**
 * The Class PrintExceptionInLogFile.
 * 
 * @author anand
 */
public class PrintExceptionInLogFile {

    /**
     * Prints the exception.
     * 
     * @param exception
     *            the e
     */
    public static void printException(Exception exception) {
	Logger logger = Constants.logger;
	StringWriter sw = new StringWriter();
	PrintWriter pw = new PrintWriter(sw, true);
	exception.printStackTrace(pw);
	String exceptionDetails = sw.toString();
	pw.flush();
	sw.flush();
	logger.debug(exceptionDetails);
    }
}
