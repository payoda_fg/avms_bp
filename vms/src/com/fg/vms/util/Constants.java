/**
 * Constants.java
 */
package com.fg.vms.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * Constants value used for the application.
 * 
 * @author pirabu
 * 
 */
public final class Constants {

	/**
	 * Logger Initialization. @see com.utils.LogInitialization @see
	 * log4j.properties
	 */
	public static final Logger logger = Logger.getLogger("vmslogfile");

	/**
	 * log4j Property File Name. @see com.utils.LogInitialization @see
	 * log4j.properties
	 */
	public static final String VMS_LOGFILE_NAME = "VMS_LOGFILE_NAME";

	/** VMS Apps Log File Path. @see com.utils.LogInitialization */
	public static final String VMS_LOGFILE_PATH = "VMS_LOGFILE_PATH";

	/** The Constant MAIL_SMPT_HOST. */
	public static final String MAIL_SMPT_HOST = "MAIL_SMPT_HOST";

	/** The Constant MAIL_SMTP_SOCKETFACTORY_PORT. */
	public static final String MAIL_SMTP_SOCKETFACTORY_PORT = "MAIL_SMTP_SOCKETFACTORY_PORT";

	/** The Constant MAIL_SMTP_SOCKETFACTORY_CLASS. */
	public static final String MAIL_SMTP_SOCKETFACTORY_CLASS = "javax.net.ssl.SSLSocketFactory";

	/** The Constant MAIL_SMTP_AUTH. */
	public static final String MAIL_SMTP_AUTH = "MAIL_SMTP_AUTH";

	/** The Constant MAIL_SMTP_PORT. */
	public static final String MAIL_SMTP_PORT = "MAIL_SMTP_PORT";

	/** The ALPHANUMERIC. */
	public static final String ALPHANUMERIC = new String(
			"QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");

	/** The Constant FG_VMS_FROM_USERNAME. */
	public static final String FG_VMS_FROM_USERNAME = "FG_VMS_FROM_USERNAME";

	/** The Constant FG_VMS_FROM_EMIL_ID. */
	public static final String FG_VMS_FROM_EMIL_ID = "FG_VMS_FROM_EMIL_ID";

	/** The Constant FG_VMS_FROM_PASSWORD. */
	public static final String FG_VMS_FROM_PASSWORD = "FG_VMS_FROM_PASSWORD";

	/** The Constant DB_CONNECTION_URL. */
	public static final String DB_CONNECTION_URL = "DB_CONNECTION_URL";

	/** The project name. */
	public static final String PROJECT_NAME = "vms";

	/** The Upload logo path. */
	public static final String UPLOAD_LOGO_PATH = "UPLOAD_LOGO_PATH";

	/** The Upload document path. */
	public static final String UPLOAD_DOCUMENT_PATH = "UPLOAD_DOCUMENT_PATH";

	/** The Upload insurance path. */
	public static final String UPLOAD_INSURANCE_PATH = "UPLOAD_INSURANCE_PATH";

	public static final String UPLOAD_CUSTOMERDOCUMENT_PATH = "UPLOAD_CUSTOMERDOCUMENT_PATH";
	public static final String EMAIL_ATTACHMENT_PATH = "EMAIL_ATTACHMENT_PATH";

	/** The Report File path. */
	public static final String REPORT_FILE_PATH = "REPORT_FILE_PATH";

	public static final String MASTER_DB_NAME = "MASTER_DB_NAME";
	/** The Database username. */
	public static final String DB_USERNAME = "DB_USERNAME";

	/** The Database password. */
	public static final String DB_PASSWORD = "DB_PASSWORD";

	/** The APPLICATION URL. */
	public static final String APPLICATION_URL = "APPLICATION_URL";

	public static final String DOMAIN_NAME = "DOMAIN_NAME";

	/** Property file - Resource file for the vms app. */
	private static final String BUNDLE_NAME = "vmsconstants";

	/** The Constant RESOURCE_BUNDLE. */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);
	
	public static final String UPLOAD_EXCELFORMATE_PATH = "UPLOAD_EXCELFORMATE_PATH";
	
	
	public static final String SECTOR_SUBSECTOR_DATE = "SECTOR_SUBSECTOR_DATE";
	/**
	 * To get the value for the corresponding key for the vmscontants.properties
	 * file
	 * 
	 * @param key
	 *            the key
	 * @return the string
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

}
