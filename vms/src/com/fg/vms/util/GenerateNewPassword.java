/*
 * GenerateNewPassword.java
 */
package com.fg.vms.util;

import java.util.Random;

/**
 * Randomly Generate a new password.
 * 
 * @author pirabu
 * 
 */
public class GenerateNewPassword {

    /**
     * Generate a new password randomly.
     * 
     * @return new password.
     */
    public static String generateRandomPassword() {
	StringBuffer sb = new StringBuffer();
	Random random = new Random();
	int temp = 0;
	for (int i = 1; i <= 7; i++) {
	    temp = random.nextInt(62);
	    sb.append(Constants.ALPHANUMERIC.charAt(temp));
	}
	return sb.toString();
    }
}
