/*
 * KeyGen.java
 */
package com.fg.vms.util;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;

/**
 * KeyGen used to generate key for password to store in db.
 */
public class KeyGen {

    /**
     * calling empty constructor.
     */
    KeyGen() {
	initialize();
    }

    /**
     * Initialize.
     */
    private void initialize() {
	Provider ibmjce = new com.ibm.crypto.provider.IBMJCE();
	Security.addProvider(ibmjce);
    }

    /**
     * Derive key method generates an array of bytes ,the key value for a
     * String. <br>
     * The key generated can be used for encryption and decryption of data. <br>
     * SHA algorithm is used in creating the fixed length key with the custom
     * algorithm.
     * 
     * @param clearPassword
     *            The String containing the value used in key creation.
     * @return byte[] Returns an array of bytes of key.
     * @throws Exception
     *             the exception
     */
    protected byte[] deriveKey(String clearPassword) throws Exception {
	String pwd = clearPassword;
	MessageDigest sha = MessageDigest.getInstance("SHA");
	byte[] first = new byte[64];
	byte[] second = new byte[64];
	byte[] h = sha.digest(pwd.getBytes());
	for (int i = 0; i < h.length; i++) {
	    first[i] = h[i];
	    second[i] = h[i];
	}

	for (int i = 0; i < 64; i++) {
	    first[i] ^= 0x36;
	    second[i] ^= 0x5C;
	}

	byte[] firsthash = sha.digest(first);
	byte[] secondhash = sha.digest(second);

	for (int i = 0; i < firsthash.length; i++) {
	    byte count1 = 0;
	    byte count2 = 0;

	    // Conta il numero di bit settati nel byte
	    for (int j = 0; j < 8; j++) {
		if ((firsthash[i] & (1 << j)) != 0)
		    count1++;
		if ((secondhash[i] & (1 << j)) != 0)
		    count2++;
	    }

	    if ((count1 % 2) == 0)
		firsthash[i] ^= 0x1;
	    if ((count2 % 2) == 0)
		secondhash[i] ^= 0x1;
	}

	ByteArrayOutputStream txt = new ByteArrayOutputStream();
	txt.write(firsthash, 0, firsthash.length);
	txt.write(secondhash, 0, 4);
	return txt.toByteArray();
    }

    /**
     * Method to convert a byte array into an equivalent HEX format.
     * 
     * @param source
     *            An array of crypted bytes.
     * @return String representing the equivalent of the byte array.
     * @throws Exception
     *             the exception
     */

    protected String byteArray2HexString(byte[] source) throws Exception {
	StringBuffer key = new StringBuffer();
	int x;
	for (int i = 0; i < source.length; i++) {
	    x = source[i];
	    int hi = (x & 0xF0) >> 4;
	    int lo = x & 0x0F;
	    key.append(Integer.toHexString(hi)).append(Integer.toHexString(lo));
	}
	return key.toString();
    }

    /**
     * Method to convert a String in HEX format into an equivalent byte array.
     * 
     * @param inStr
     *            String in HEX format.
     * @return byte[] byte array representing the equivalent of the HEX String.
     * @throws Exception
     *             the exception
     */
    protected byte[] hextobyte(String inStr) throws Exception {
	int byteArrLen = inStr.length() / 2;
	byte[] result = new byte[byteArrLen];
	for (int i = 0; i < result.length; i++) {
	    result[i] = (byte) Integer.parseInt(
		    inStr.substring(i * 2, (i + 1) * 2), 16);
	}
	return result;
    }
}
