/**
 * SendEmail.java
 */
package com.fg.vms.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.pojo.UsersForm;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.CertificateExpiration;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.CertificateExpirationNotification;
import com.fg.vms.customer.model.EmailHistory;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.ReportDue;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.VendorMasterForm;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * Send new Password to Email.
 * 
 * @author pirabu
 */

public class SendEmail {
	
	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/** The properties. */
	static Properties properties = new Properties();
	static {
		properties.put("mail.smtp.host",
				Constants.getString(Constants.MAIL_SMPT_HOST));
		properties.put("mail.smtp.socketFactory.port",
				Constants.getString(Constants.MAIL_SMTP_SOCKETFACTORY_PORT));
		properties.put("mail.smtp.starttls.enable", "true");
		// properties.put("mail.smtp.socketFactory.class",
		// Constants.MAIL_SMTP_SOCKETFACTORY_CLASS);
		properties.put("mail.smtp.auth",
				Constants.getString(Constants.MAIL_SMTP_AUTH));
		properties.put("mail.smtp.port",
				Constants.getString(Constants.MAIL_SMTP_PORT));
	}

	static Session session = Session.getInstance(properties,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(Constants
							.getString(Constants.FG_VMS_FROM_USERNAME),
							Constants.getString(Constants.FG_VMS_FROM_PASSWORD));
				}
			});

	/**
	 * Send new password to email id(to).
	 * 
	 * @param to
	 *            the to
	 * @param subject
	 *            the subject
	 * @param userName
	 *            the user name
	 * @param password
	 *            the password
	 * @param userDetails
	 * @param body
	 *            the body
	 * @return the string
	 */
	public String sendNewPassword(String to, String subject, String userName,
			String password, String name, String appRoot,
			UserDetailsDto userDetails) {
		String result = "success";
//		String msg = "You or somebody else claiming to be you have initiated the process to reset your password. "
//				+ "If you would like to proceed with the password reset request, please enter the temporary password.<br><br> Temporary Password: ["
//				+ password + "]";
		
		EmailTemplate emailTemplate=getTemplates(7, userDetails);
		String msg=emailTemplate.getEmailTemplateMessage();
		msg=msg.replace("#password", password);
		
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject(emailTemplate.getEmailTemplateSubject());
			// freemarker stuff.
			Configuration cfg = new Configuration();
			// Set Directory for templates
			cfg.setDirectoryForTemplateLoading(new File(appRoot
					+ "email_templates"));
			Template template = cfg.getTemplate("html-mail-template.ftl");
			Map<String, String> rootMap = new HashMap<String, String>();
			if (null != name && name.isEmpty()) {
				rootMap.put("to", name);
			} else {
				rootMap.put("to", "Requestor");
			}

			rootMap.put("body", msg);
			rootMap.put("from", "AVMS Security Admin.");
			Writer out = new StringWriter();
			template.process(rootMap, out);

			message.setContent(out.toString(), "text/html");

			Transport.send(message);
			saveEmailHistory(userDetails, msg, subject, to, "TO",
					EmailType.NEWPASSWORD);

		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * Send mail.
	 * 
	 * @param to
	 *            the to
	 * @param subject
	 *            the subject
	 * @param msg
	 *            the msg
	 * @param emailType
	 * @param userDetails
	 * @return the string
	 */
	public String sendMail(String to, String cc, String bcc, String subject,
			String msg, EmailType emailType, UserDetailsDto userDetails,
			String filePath, String fileName) {
		String result = "success";
		String splitresult = splitaddress(to, cc, bcc);
		try {
			if (splitresult.equals("success")) {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				if (address != null && address.length != 0) {
					javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[address.length];
					for (int i = 0; i < address.length; i++) {
						addressTo[i] = new javax.mail.internet.InternetAddress(
								address[i]);
						saveEmailHistory(userDetails, msg, subject, address[i],
								"TO", emailType);
					}
					message.setRecipients(javax.mail.Message.RecipientType.TO,
							addressTo);
				}
				if (ccAddress != null && ccAddress.length != 0) {
					javax.mail.internet.InternetAddress[] addressCc = new javax.mail.internet.InternetAddress[ccAddress.length];
					for (int i = 0; i < ccAddress.length; i++) {
						addressCc[i] = new javax.mail.internet.InternetAddress(
								ccAddress[i]);
						saveEmailHistory(userDetails, msg, subject,
								ccAddress[i], "CC", emailType);
					}
					message.setRecipients(javax.mail.Message.RecipientType.CC,
							addressCc);
				}
				if (bccAddress != null && bccAddress.length != 0) {
					javax.mail.internet.InternetAddress[] addressBcc = new javax.mail.internet.InternetAddress[bccAddress.length];
					for (int i = 0; i < bccAddress.length; i++) {
						addressBcc[i] = new javax.mail.internet.InternetAddress(
								bccAddress[i]);
						saveEmailHistory(userDetails, msg, subject,
								bccAddress[i], "BCC", emailType);
					}
					message.setRecipients(javax.mail.Message.RecipientType.BCC,
							addressBcc);
				}

				if (emailType != null
						&& emailType.equals(EmailType.CERTIFICATEEXPIRATION)) {
					// message.setRecipients(Message.RecipientType.CC,
					// InternetAddress
					// .parse(certificateExpiration.getCustomerAdminEmailId()));
				}

				if (null != fileName) {

					// create MimeBodyPart object and set your message text
					BodyPart messageBodyPart1 = new MimeBodyPart();
//					messageBodyPart1.setText(msg);
					messageBodyPart1.setContent( msg, "text/html; charset=utf-8");
					message.setSubject(subject);

					// create new MimeBodyPart object and set DataHandler object
					// to this object
					MimeBodyPart messageBodyPart2 = new MimeBodyPart();
					DataSource source = new FileDataSource(filePath);
					messageBodyPart2.setDataHandler(new DataHandler(source));
					messageBodyPart2.setFileName(fileName);

					// create Multipart object and add MimeBodyPart objects to
					// this object
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart1);
					multipart.addBodyPart(messageBodyPart2);

					// set the multiplart object to the message object
					message.setContent(multipart);
				} else {
//					message.setText(msg);
					message.setContent( msg, "text/html; charset=utf-8");
					message.setSubject(subject);
				}
				// send message
				Transport.send(message);
				result = "success";
			} else {
				result = "error";
			}

		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/** The address. */
	String address[];

	/** The cc address. */
	String ccAddress[];

	/** The bcc address. */
	String bccAddress[];

	/**
	 * Splitaddress.
	 * 
	 * @param to
	 *            the to
	 * @return the string
	 */
	public String splitaddress(String to, String cc, String bcc) {
		String result = "";

		try {
			if (to.contains(";")) {
				address = to.split(";");
				result = "success";
			} else if (to.contains(",")) {
				address = to.split(",");
				result = "success";
			} else if (!to.isEmpty()) {
				address = to.split(",");
				result = "success";
			}

			if (cc != null && !cc.isEmpty() && cc.contains(";")) {
				ccAddress = cc.split(";");
				result = "success";
			} else if (cc != null && !cc.isEmpty() && cc.contains(",")) {
				ccAddress = cc.split(",");
				result = "success";
			} else if (!cc.isEmpty()) {
				ccAddress = cc.split(",");
				result = "success";
			}

			if (bcc != null && !bcc.isEmpty() && bcc.contains(";")) {
				bccAddress = bcc.split(";");
				result = "success";
			} else if (bcc != null && !bcc.isEmpty() && bcc.contains(",")) {
				bccAddress = bcc.split(",");
				result = "success";
			} else if (!bcc.isEmpty()) {
				bccAddress = bcc.split(",");
				result = "success";
			}
		} catch (NullPointerException e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;

	}

	/**
	 * Send mail to distribution list while registering vendor.
	 * 
	 * @param to
	 * @param subject
	 * @param msg
	 * @param userDetails
	 * @return
	 */
	public String sendMailToDistribution(List<String> to, String subject,
			String msg, UserDetailsDto userDetails) {
		String result = "success";

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[to
					.size()];

			for (int i = 0; i < to.size(); i++) {
				addressTo[i] = new javax.mail.internet.InternetAddress(
						to.get(i));
				saveEmailHistory(userDetails, msg, subject, to.get(i), "TO",
						EmailType.MAILTODISTRIBUTION);
			}

			message.setRecipients(javax.mail.Message.RecipientType.BCC,
					addressTo);
			message.setSubject(subject);
			message.setContent(msg, "text/html; charset=utf-8");
//			message.setText(msg);
			Transport.send(message);

			result = "success";

		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;

	}

	/**
	 * send login credentials to new vendor.
	 * 
	 * @param selectedId
	 *            the selected id
	 * @param appDetails
	 *            the app details
	 * @return the string
	 */
	public String sendloginCredentials(Integer selectedId,
			UserDetailsDto appDetails, String url, String approvalStatus) {
		org.hibernate.Session hibernateSession = HibernateUtilCustomer
				.buildSessionFactory().openSession(
						DataBaseConnection.getConnection(appDetails));
		VendorContact vendorContact = null;
		try {
			hibernateSession.getTransaction().begin();
			VendorMaster vendorMaster = null;
			vendorMaster = (VendorMaster) hibernateSession.get(
					VendorMaster.class, selectedId);
			vendorContact = (VendorContact) hibernateSession
					.createCriteria(VendorContact.class)
					.add(Restrictions.and(
							Restrictions.eq("vendorId", vendorMaster),
							Restrictions.eq("isPrimaryContact", (byte) 1)))
					.uniqueResult();
			WorkflowConfigurationDao conf = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration wfConf = conf.listWorkflowConfig(appDetails);
			// Send email to distribution when approved the vendor.
			if (wfConf != null && null != wfConf.getApprovalEmail()
					&& wfConf.getApprovalEmail() == 1) {
				conf.updateCustomerWfLog(vendorMaster, "approved", appDetails);
			}
			hibernateSession.getTransaction().commit();
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (hibernateSession.isConnected()) {
				hibernateSession.close();
			}
		}
		String emailId;
		//String userName;
		String password = null;
		if (vendorContact != null) {
			//userName = vendorContact.getEmailId();
			emailId = vendorContact.getEmailId();
			Decrypt decrypt = new Decrypt();
			Integer keyvalue = vendorContact.getKeyValue();
			try {
				if (vendorContact.getPassword() != null) {
					password = decrypt.decryptText(
							String.valueOf(keyvalue.toString()),
							vendorContact.getPassword());
				}
			} catch (Exception e) {
				PrintExceptionInLogFile.printException(e);
			}
		} else {
			return "failure";
		}
		/*//getting divisionid from appdeatails for getting templates
		Integer customerDivisionId = null;
		if(null != appDetails.getCustomerDivisionID() && 0 != appDetails.getCustomerDivisionID() ) {
			customerDivisionId = appDetails.getCustomerDivisionID();
		}*/
		//calling method for  template message body and subject
		EmailTemplate emailTemplate=getTemplates(2, appDetails);
		EmailTemplate emailTemplate1=getTemplates(3, appDetails);
		String subject = emailTemplate.getEmailTemplateSubject();

		String result = "success";
		
		String content=null;

		try {

			Message message = new MimeMessage(session);

			if (vendorContact.getEmailId() != null
					&& vendorContact.getPassword() != null
					&& approvalStatus != null) {
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailId));
				message.setSubject(subject);
				String companyName = appDetails.getCurrentCustomer()
						.getCustomerId().getCustName();
					
                content=emailTemplate.getEmailTemplateMessage();
                content= content.replace("#userName",vendorContact.getEmailId());
                content=content.replace("#password",password);
                
                
//				content = "Congratulations! You have been approved as a BP Supplier. We look forward to doing business with you. "
//						+ " Please find your credentials within the body of this email.\n User name : "
//						+ userName
//						+ "\n Password : "
//						+ password
//						+ "\n\n Thank you.";
				//message.setText(content);
				message.setContent(content, "text/html; charset=utf-8");
			} else {
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailId));
				message.setSubject(subject);
//				message.setText("Dear Supplier,  \n\n"
//						+ "You have successfully registered as VMS vendor \n\n"
//						+ "You do not have login credentials. \n\n"
//						+ "Warm regards,\n" + "VMS Admin.");
				//message.setText(getTemplates(3, appDetails));
				
				message.setContent(emailTemplate1.getEmailTemplateMessage(), "text/html; charset=utf-8");

			}
			Transport.send(message);
			saveEmailHistory(appDetails, content.toString(), subject, emailId,
					"TO", EmailType.SENDCREDENTIALSTOVENDOR);
			result = "success";
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}
	
	
	/**
	 * send Status Change for vendor.
	 * 
	 * @param selectedId
	 *            the selected id
	 * @param appDetails
	 *            the app details
	 * @return the string
	 */
	public String sendVendorStatusChange(Integer selectedId,
			UserDetailsDto appDetails, String url,String newstatus,String oldstatus,String approvalStatus) {
		org.hibernate.Session hibernateSession = HibernateUtilCustomer
				.buildSessionFactory().openSession(
						DataBaseConnection.getConnection(appDetails));
		VendorContact vendorContact = null;
		try {
			hibernateSession.getTransaction().begin();
			VendorMaster vendorMaster = null;
			vendorMaster = (VendorMaster) hibernateSession.get(
					VendorMaster.class, selectedId);
			vendorContact = (VendorContact) hibernateSession
					.createCriteria(VendorContact.class)
					.add(Restrictions.and(
							Restrictions.eq("vendorId", vendorMaster),
							Restrictions.eq("isPrimaryContact", (byte) 1)))
					.uniqueResult();
			WorkflowConfigurationDao conf = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration wfConf = conf.listWorkflowConfig(appDetails);
			// Send email to distribution when approved the vendor.
			if (wfConf != null && null != wfConf.getApprovalEmail()
					&& wfConf.getApprovalEmail() == 1) {
				conf.updateCustomerWfLog(vendorMaster, "approved", appDetails);
			}
			hibernateSession.getTransaction().commit();
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (hibernateSession.isConnected()) {
				hibernateSession.close();
			}
		}
		String emailId;
		//String userName;
		String password = null;
		if (vendorContact != null) {
			//userName = vendorContact.getEmailId();
			emailId = vendorContact.getEmailId();
//			emailId = "bhaskar@cgvakindia.com";
			Decrypt decrypt = new Decrypt();
			Integer keyvalue = vendorContact.getKeyValue();
			try {
				if (vendorContact.getPassword() != null) {
					password = decrypt.decryptText(
							String.valueOf(keyvalue.toString()),
							vendorContact.getPassword());
				}
			} catch (Exception e) {
				PrintExceptionInLogFile.printException(e);
			}
		} else {
			return "failure";
		}
		/*//getting divisionid from appdeatails for getting templates
		Integer customerDivisionId = null;
		if(null != appDetails.getCustomerDivisionID() && 0 != appDetails.getCustomerDivisionID() ) {
			customerDivisionId = appDetails.getCustomerDivisionID();
		}*/
		//calling method for  template message body and subject
		EmailTemplate emailTemplate=getTemplates(28, appDetails);
		String subject = emailTemplate.getEmailTemplateSubject();

		String result = "success";
		
		String content=null;

		try {

			Message message = new MimeMessage(session);

			if (vendorContact.getEmailId() != null
					&& vendorContact.getPassword() != null
					&& approvalStatus != null) {
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailId));
				message.setSubject(subject);
				String companyName = appDetails.getCurrentCustomer()
						.getCustomerId().getCustName();
					
                content=emailTemplate.getEmailTemplateMessage();
                content= content.replace("#previous_status",oldstatus);
                content=content.replace("#new_status",newstatus);
                
                
//				content = "Congratulations! You have been approved as a BP Supplier. We look forward to doing business with you. "
//						+ " Please find your credentials within the body of this email.\n User name : "
//						+ userName
//						+ "\n Password : "
//						+ password
//						+ "\n\n Thank you.";
				//message.setText(content);
				message.setContent(content, "text/html; charset=utf-8");
			} 
			Transport.send(message);
			saveEmailHistory(appDetails, content.toString(), subject, emailId,
					"TO", EmailType.SENDCREDENTIALSTOVENDOR);
			result = "success";
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * Send alert mail to admin - anonymous Vendor.
	 * 
	 * @param firstName
	 *            the first name
	 * @param userDetails
	 *            the user details
	 * @param vendorId
	 *            the vendor id
	 * @return the string
	 */
	public String sendAlertMailToAdmin(String firstName,
			UserDetailsDto userDetails, Integer vendorId, String serverName) {
       
		//caling method for gettemplate from db.
		Integer customerDivisionId = null;
		if(null != userDetails.getCustomerDivisionID() && 0 != userDetails.getCustomerDivisionID()) {
			customerDivisionId = userDetails.getCustomerDivisionID();
		}
		EmailTemplate emailTemplate=getTemplates(5, userDetails, customerDivisionId);
		String emailId = userDetails.getCurrentCustomer().getEmailId();
//		String subject = "Vendor Registration Alert";
		String subject= emailTemplate.getEmailTemplateSubject();
		String result = "success";
		
		//converting vendorId to String
		String svendorId=vendorId.toString();
		String modifiedServerName=serverName.concat("/approval.do?method=loginforapproval&id=vendorId'");
		modifiedServerName=modifiedServerName.replace("vendorId", svendorId);
		try {
//			String body = "Dear Customer,  \n\n"
//					+ "A new vendor has been registered in your site. \n\n"
//					+ "They are, \n\n" + firstName + "\n\n"
//					+ "You can approve them by clicking the link below \n\n "
//					+ serverName + "/approval.do?method=loginforapproval&id="
//					+ vendorId + "\n\n" + "Warm regards,\n"
//					+ "AVMS Admin Team Admin Team.";
			
			
			String body=emailTemplate.getEmailTemplateMessage();
			body=body.replace("#firstName",firstName);
			body=body.replace("#serverName", modifiedServerName);
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailId));
			message.setSubject(subject);
//			message.setText(body);
			message.setContent(body, "text/html; charset=utf-8");
			Transport.send(message);

			saveEmailHistory(userDetails, body, subject, emailId, "TO",
					EmailType.ANONYMOUSVENDORREGISTERED);
			result = "success";
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}

		return result;
	}

	/**
	 * Send alert mail to new customers registered by AVMS Admin Team.
	 * 
	 * @param customerName
	 *            the customer name
	 * @param url
	 *            the url
	 * @param emailId
	 *            the email id
	 * @return the result string
	 */
	public String sendCredentialsToNewCustomer(String customerName, String url,
			String emailId, String username, String password,UserDetailsDto userDetails) {
		//Template is added to this method
		String subject = "Registered succussfully under AVMS...";
		String result = "success";
		EmailTemplate emailTemplate=getTemplates(28, userDetails);
		String msg=emailTemplate.getEmailTemplateMessage();
		msg=msg.replace("#customerName", customerName);
		msg=msg.replace("#url", url);
		msg=msg.replace("#username", username);
		msg=msg.replace("#password", password);

		try {
        
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailId));
			message.setSubject(subject);
//			message.setText(getTemplates(4, userDetails));
			/*message.setText("Dear "
					+ customerName
					+ ",  \n\n"
					+ "You are registered as a new customer in Advanced Vendor Management System. \n\n"
					+ "Now onwards you can login through the link given below \n\n Your credentials are given below.\n"

					+ url + "\n\n" + "User Name: " + username + "\n"
					+ "Password:" + password + "\n\n" + "Warm regards,\n"
					+ "AVMS Admin Team.");*/
			message.setContent(msg, "text/html; charset=utf-8");
			Transport.send(message);
			result = "success";
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * 
	 * @param emailId
	 * @param subject
	 * @param content
	 * @return
	 */
	public String sendAssessmentToVendor(String emailId, String subject,
			String content) {

		String result = "failure";

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailId));
			message.setSubject(subject);
			message.setText(content);
			Transport.send(message);
			result = "success";

		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * Send Certificate Expiration Alert
	 * 
	 * @param certificateExpirations
	 * @param appRoot
	 * @param logoPath
	 * @return
	 */
	public String sendCertificateExpirationAlert(
			List<CertificateExpiration> certificateExpirations, String appRoot,
			String logoPath, UserDetailsDto userDetails) {
// 			For this template is created
		String result = "success";
		try {

			CURDDao<CertificateExpirationNotification> curdTemplate = new CURDTemplateImpl<CertificateExpirationNotification>();
			for (CertificateExpiration certificateExpiration : certificateExpirations)
			{
				EmailTemplate emailTemplate=getTemplates(27, userDetails);
				String templageMsg=emailTemplate.getEmailTemplateMessage();
				String linkToSite = certificateExpiration.getLinkToSite()+certificateExpiration.getApplicationUrl();
				
				templageMsg = templageMsg.replace("#contactName", certificateExpiration.getContactName());
				templageMsg = templageMsg.replace("#vendorName", certificateExpiration.getSupplierCompanyName());
				templageMsg = templageMsg.replace("#certificateNumber", certificateExpiration.getCertificationNo());
				templageMsg = templageMsg.replace("#agencyName", certificateExpiration.getAgencyName());
				templageMsg = templageMsg.replace("#certificateName", certificateExpiration.getCategoryName());
				templageMsg = templageMsg.replace("#expiryDate", CommonUtils.convertDateToString(certificateExpiration.getExpirationDate()));
				templageMsg = templageMsg.replaceAll("#customerName", certificateExpiration.getCustomerName());
				templageMsg = templageMsg.replace("#supportEmailId", certificateExpiration.getSupportEmailId());
				templageMsg = templageMsg.replace("#linkToSite", linkToSite);
				String msg = templageMsg;
				String codeGroup = null;
				Pattern p = Pattern.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>");
			    Matcher m = p.matcher(templageMsg);
			    if (m.find()) {

				      // get the matching group
				      codeGroup = m.group(1);
				      msg = msg.replace(codeGroup, "cid:icon");
				    }
				
			/*	StringBuilder html = new StringBuilder();
				html.append("<html>"
						+ "<head>"
						+ "<style>#p1{text-align: justify; text-justify: inter-word;}</style>"
						+ "</head>"
						+ "<body>"
						+ "<table border='0' cellspacing='0' cellpadding='10' width='600' style=' border:25px solid #003366;' align='center'>"
						+ " <tr>  <td>        <table border='0' cellspacing='0' cellpadding='0' width='100%'> <tr><td width='100%'> "
						+ "   <table border='0' cellspacing='0' cellpadding='0' width='100%'>  <tr><td> <p align='right'><img width='145' height='144' src='cid:icon' alt='Company Logo'  /></p>"
						+ " </td></tr>  </table>  </td> </tr><tr><td width='100%'><table border='0' cellspacing='0' cellpadding='0' width='100%' id='content_LETTER.BLOCK2'>"
						+ "<tr> <td> To: <p id='p1'>"
						+ certificateExpiration.getContactName()
						+ " </p> <p id='p1'>"
						+ certificateExpiration.getSupplierCompanyName()
						+ "</p><p id='p1'>Regarding: Certificate Expiration</p>"
						+ certificateExpiration.getCertificationNo()
						+ " <p id='p1'> Your certification with Certification Number: <strong>"
						+ "</strong> for Certifying Council: <strong> "
						+ certificateExpiration.getAgencyName()
						+ "</strong> and  Diversity Category name: "
						+ "<strong>"
						+ certificateExpiration.getCategoryName()
						+ "</strong> will expire on <strong>"
						+ CommonUtils.convertDateToString(certificateExpiration.getExpirationDate())
						+ "</strong>.</p>"
						+ "<p id='p1'> Please update your company profile in "+certificateExpiration.getCustomerName()+"'s diverse supplier database (AVMS) with the latest certification details at " + linkToSite + ".");
				
				html.append(" If you cannot recall your login credentials, please use the \"Forgotten your password?\" function on the main login page "
						+ "or contact our technical support team at 800/251-4610 for assistance. "
						+ "Please remember to regularly update your company profile with the latest certification details "
						+ "and any other pertinent information or changes regarding your company.</p>");
				
				html.append("<p id='p1'>If your diversity owned status has changed (due to acquisition, internal ownership changes, etc.), "
						+ "please do notify our team immediately via email at "+certificateExpiration.getSupportEmailId()+".</p>");
				
				html.append("<p id='p1'>If your profile is not updated after 1 more expiration communication attempt, "
						+ "we will assume your diversity status no longer applies and will delete your registration with us. "
						+ "You would then need to re-build your profile again.</p>");
				
				html.append("<p id='p1'>Thank you for your attention.</p> <p id='p1'><strong>"
						+ certificateExpiration.getCustomerName()
						+ "</strong> Supplier Diversity</p>"
						+ "</td></tr></table></td></tr> <tr> <td width='100%'></td> </tr> </table> </td></tr></table></body></html>");
*/
				Multipart multipart = new MimeMultipart("related");

				// Create the HTML message part and add it to the email content.
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(msg, "text/html");
				multipart.addBodyPart(messageBodyPart);

				// Read the image from a file and add it to the email content.
				// Note the "<>" added around the content ID used in the HTML
				// above.
				// Setting Content-Type does not seem to be required.
				// I guess it is determined from the file type
				MimeBodyPart iconBodyPart = new MimeBodyPart();
				DataSource iconDataSource = new FileDataSource(new File(
						Constants.getString(Constants.UPLOAD_LOGO_PATH)
								+ File.separator + logoPath));
				iconBodyPart.setDataHandler(new DataHandler(iconDataSource));
				iconBodyPart.setDisposition(Part.INLINE);
				iconBodyPart.setContentID("<icon>");
				iconBodyPart.addHeader("Content-Type", "image/jpeg");
				multipart.addBodyPart(iconBodyPart);

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO, InternetAddress
						.parse(certificateExpiration.getEmailId()));
				//message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(certificateExpiration.getCustomerAdminEmailId()));
				if (certificateExpiration.getCc() != null && !certificateExpiration.getCc().isEmpty()) 
				{
					InternetAddress[] addressCc = new InternetAddress[certificateExpiration.getCc().size()];
					
					for (int index = 0; index < certificateExpiration.getCc().size(); index++) 
					{
						addressCc[index] = new InternetAddress(certificateExpiration.getCc().get(index));
//						saveEmailHistory(userDetails, html.toString(), "Certificate Expiration", certificateExpiration.getCc().get(index), "CC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
						saveEmailHistory(userDetails, msg, "Certificate Expiration", certificateExpiration.getCc().get(index), "CC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
					}
					message.setRecipients(Message.RecipientType.CC, addressCc);
				}
				
				message.setSubject("Regarding: Certificate Expiration");

				message.setContent(multipart);
				Transport.send(message);

				// Save the Certificate Expiration Notification
				CertificateExpirationNotification notification = new CertificateExpirationNotification();
				notification.setAgencyname(certificateExpiration.getAgencyName());
				notification.setCertificatenumber(certificateExpiration.getCertificationNo());
				notification.setEmaildate(new Date());
				notification.setExpirationdate(certificateExpiration.getExpirationDate());
				notification.setStatus("NO");
				notification.setVendorid(certificateExpiration.getVendorId());
				notification.setEmailId(certificateExpiration.getEmailId());
				notification.setCertificateName(certificateExpiration.getCertificateName());
				notification.setVendorCertificateId(certificateExpiration.getVendorCertificateId());
				curdTemplate.save(notification, userDetails);

				// Email history
//				saveEmailHistory(userDetails, html.toString(), "Certificate Expiration", certificateExpiration.getEmailId(), "TO", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				saveEmailHistory(userDetails, msg, "Certificate Expiration", certificateExpiration.getEmailId(), "TO", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				
				log.info("Sended Certificate Expiration Email Alert to Vendor.");
			}

		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * Send mail to Anonymous vendor for credentials on save details.
	 * 
	 * @param firstName
	 *            the first name
	 * @param userDetails
	 *            the user details
	 * @param vendorId
	 *            the vendor id
	 * @return the string
	 */
	public String sendCredentialsToUnknownVendorOnSave(
			VendorMasterForm vendorForm, UserDetailsDto userDetails,
			String password, String serverName, String appRoot) {

		String emailId = vendorForm.getPreparerEmail();
		Integer customerDivisionId = null;
		if(null != vendorForm.getCustomerDivision() && 0 != vendorForm.getCustomerDivision())
			customerDivisionId = vendorForm.getCustomerDivision();
		
		//calling the methods for get emailtemplate message and body to prevent hard coding and for saving db only
		EmailTemplate emailTemplate=null;
		String msg;
		emailTemplate=getTemplates(1,userDetails, customerDivisionId);
		
		String result = "success";
		if(emailTemplate.getId() != null) {
			msg=emailTemplate.getEmailTemplateMessage();
			
			String subject = emailTemplate.getEmailTemplateSubject();
			msg=msg.replace("&lt;", "<");
			msg=msg.replace("&gt;", ">");
			     
			//replacing param names, in the message body for db saving
			String modifiedServerName=serverName.concat("/viewanonymousvendors.do?parameter=emailLogin'");
			msg=msg.replace("#emailid", vendorForm.getPreparerEmail());
			msg=msg.replace("#password", password);
			msg=msg.replace("#serverName", modifiedServerName);
			//System.out.println("Final msg-->"+msg);
					
			StringBuilder body = new StringBuilder(msg);
			/*"Dear Supplier,  \n\n"
			+ "Please find your AVMS credentials below to include the following: \n\n"
			+ "1. Email ID confirmation \n"
			+ "2. Temporary Password \n\n" + " EmailId : "
			+ emailId
			+ "\n\n"
			+ " Password : "
			+ password
			+ "\n\n"
			+ "Your temporary password will expire 7 days from today's date.  \n\n"
			+ "You may use the below URL to log into your AVMS account.  \n\n"
			+ "<a href='http://"
			+ serverName
			+ "/viewanonymousvendors.do?parameter=emailLogin'>Click Here</a> \n\n"

			// "http://registration.bp.avmsystem.com/viewanonymousvendors.do?parameter=emailLogin \n\n"
			+ "To log in and register, use the following steps:  \n\n"
			+ "1. Go to the AVMS registration portal using the above link . \n"
			+ "2. Enter your Email ID and temporary password. \n"
			+ "3. System will take you to the Reset Password section. \n"
			+ "4. After Resetting Your Password, Login with New Password. \n"
			+ "5. System will take you to the Contact Information section. \n"
			+ "6. Finalize the registration process by completing all required fields. \n"
			+ "7. Confirm that your information is complete and accurate. \n"
			+ "8. Submit your form to be reviewed by BP. \n\n"
			+ "If you have any questions, you may email support@avmsystem.com. \n\n"
			+ "Thank you. \n\n"
			+ "Warmest Regards, \n"
			+ "AVMS Support Team.");*/
			
			try {
				// Message message = new MimeMessage(session);
				// message.setFrom(new InternetAddress(Constants
				// .getString(Constants.FG_VMS_FROM_EMIL_ID)));
				// message.setRecipients(Message.RecipientType.TO,
				// InternetAddress.parse(emailId));
				// message.setSubject(subject);
				// message.setText(body);
				// Transport.send(message);
				
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));
				message.setSubject(subject);
				Configuration cfg = new Configuration();
				// Set Directory for templates
				cfg.setDirectoryForTemplateLoading(new File(appRoot + "email_templates"));
				Template template = cfg.getTemplate("supplier-mail-template.ftl");
				Map<String, String> rootMap = new HashMap<String, String>();
				rootMap.put("serverName", serverName);
				rootMap.put("password", password);
				rootMap.put("emailID", emailId);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				/*message.setContent(out.toString(), "text/html");
				message.setContent(body.toString(),"text/html");*/
				message.setContent(body.toString(), "text/html; charset=utf-8");
				Transport.send(message);	
				saveEmailHistory(userDetails, body.toString(), subject, emailId, "TO", EmailType.CREDENTIALSTOUNKNOWNVENDORONSAVE);
				result = "vendorsavesuccess";
			} catch (Exception e) {
				result = "error";
				PrintExceptionInLogFile.printException(e);
			}
		}
		return result;
	}

	public String sendReportDueMail(String to, String subject, String msg,
			EmailType emailType, UserDetailsDto userDetails) {
		String result = "success";
		String splitresult = splitaddress1(to);
		try {
			if (splitresult.equals("success")) {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[address.length];
				for (int i = 0; i < address.length; i++) {
					addressTo[i] = new javax.mail.internet.InternetAddress(
							address[i]);
					saveEmailHistory(userDetails, msg, subject, address[i],
							"TO", emailType);
				}

				message.setRecipients(javax.mail.Message.RecipientType.BCC,
						addressTo);
				message.setSubject(subject);
				message.setText(msg);
				Transport.send(message);
				result = "success";
			} else {
				result = "error";
			}
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	public String sendCertificateExpirationNextDay(
			List<CertificateExpirationNotification> notifications,
			UserDetailsDto userDetails) {
		//Template is added to this method to send mail
		String result = "success";
		EmailTemplate emailTemplate=getTemplates(29, userDetails);
		String msg=emailTemplate.getEmailTemplateMessage();
		WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration workflowConfiguration = configuration.listWorkflowConfig(userDetails);

		try {
			if (notifications != null && !notifications.isEmpty()) {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));

				for (CertificateExpirationNotification due : notifications) {
					msg=msg.replace("#certificateName", due.getCertificateName());
					msg=msg.replace("#expirationdate", CommonUtils.convertDateToString(due.getExpirationdate()));
					msg=msg.replace("#supportEmailId", workflowConfiguration.getSupportMailId());

					/*String body = "Dear Supplier,\n This is just a friendly reminder that your "
							+ due.getCertificateName()
							+ " Certificate expires on "
							+ due.getExpirationdate()
							+ ". Please update the Certification Expiration"
							+ " Date with the new date and upload the new certificate."
							+ " Please complete this task prior to the Certification expiration date. Thank you very much.";*/
					message.setRecipients(Message.RecipientType.TO,
							InternetAddress.parse(due.getEmailId()));
					message.setSubject("Certificate Expires(next day).");
					message.setContent(msg.toString(), "text/html; charset=utf-8");
//					message.setText(body);
					Transport.send(message);
					/*saveEmailHistory(userDetails, body,
							"Certificate Expires(next day).", due.getEmailId(),
							"TO", EmailType.CERTIFICATEEXPIRATION);*/
					saveEmailHistory(userDetails, msg,
							"Certificate Expires(next day).", due.getEmailId(),
							"TO", EmailType.CERTIFICATEEXPIRATION);
					log.info("Sended Certificate Expiration Next Day Email.");
				}
			}
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	public String sendReportDueNextDay(List<ReportDue> reportDues, UserDetailsDto userDetails)
	{
		String result = "success";
		VendorDao vendordao = new VendorDaoImpl();

		try
		{
			if (reportDues != null && !reportDues.isEmpty())
			{
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				
				//get reportNotSubmittedReminderDay from workflow config.
				WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration workflowConfiguration = configuration.listWorkflowConfig(userDetails);
				
				Integer reportNotSubmittedReminderDay = workflowConfiguration.getReportNotSubmittedReminderDate();
				
				Calendar cal = Calendar.getInstance();
				//set hours,minutes,seconds as Zero for current date.
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				Date currentDate = cal.getTime();

				for (ReportDue due : reportDues)
				{
					//get dueDate from DB.
					Calendar dueDate = Calendar.getInstance();
					dueDate.setTime(due.getDuedate()); 
					// Added reportNotSubmittedReminderDay to due date for sending report not submitted remainder email
					dueDate.add(Calendar.DATE, reportNotSubmittedReminderDay);
					
					//check current date and dueDate (after adding report not submitted date) is same or not
					if(currentDate.compareTo(dueDate.getTime()) == 0)
					{
						/*String body = "Dear Supplier, please note that your report has not been submitted. The due date is "
								+ due.getDuedate() + ". Please ensure that you meet the required  deliverable date.";*/
						
						//Calling Method to get template Msg from DB.
						EmailTemplate emailTemplate=getTemplates(8, userDetails,userDetails.getCustomerDivisionID());
						StringBuilder emailSubject = new StringBuilder(emailTemplate.getEmailTemplateSubject());
						
						//Getting Corresponding Vendor Name to append in Email Subject.					
						if(due.getVendorid() != null)
						{
							VendorMaster vendorMaster = vendordao.retrieveVendorById(userDetails, due.getVendorid());
							if(vendorMaster != null && vendorMaster.getVendorName() != null && vendorMaster.getVendorName() != "")
							{
								emailSubject.append(" - " + vendorMaster.getVendorName() + ".");
							}
						}
						
						String body=emailTemplate.getEmailTemplateMessage();
						body=body.replace("#dueDate", CommonUtils.convertDateToString(due.getDuedate()));
						body=body.replace("#startDate", CommonUtils.convertDateToString(due.getReportstartdate()));
						body=body.replace("#endDate", CommonUtils.convertDateToString(due.getReportenddate()));
						
						message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(due.getEmailid()));
						message.setSubject(emailSubject.toString());
						//message.setText(body);
						message.setContent(body, "text/html; charset=utf-8");
						Transport.send(message);
						saveEmailHistory(userDetails, body, emailSubject.toString(), due.getEmailid(), "TO", EmailType.REPORTNOTSUBMITTED);
						log.info("Sended REPORT NOT SUBMITTED Email Alert to Vendor.");
					}
				}
			}
		}
		catch (Exception e)
		{
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	public String sendMail(List<String> to, List<String> cc, List<String> bcc,
			String subject, String msg, EmailType emailType,
			UserDetailsDto userDetails) {
		String result = "success";
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			if (null != to && !to.isEmpty()) {
				javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[to
						.size()];
				for (int index = 0; index < to.size(); index++) {
					addressTo[index] = new javax.mail.internet.InternetAddress(
							to.get(index));
					saveEmailHistory(userDetails, msg, subject, to.get(index),
							"TO", emailType);
				}
				message.setRecipients(javax.mail.Message.RecipientType.TO,
						addressTo);
			}
			if (null != cc && !cc.isEmpty()) {
				javax.mail.internet.InternetAddress[] addressCc = new javax.mail.internet.InternetAddress[cc
						.size()];
				for (int index = 0; index < cc.size(); index++) {
					addressCc[index] = new javax.mail.internet.InternetAddress(
							cc.get(index));
					saveEmailHistory(userDetails, msg, subject, cc.get(index),
							"CC", emailType);
				}
				message.setRecipients(javax.mail.Message.RecipientType.CC,
						addressCc);
			}
			if (null != bcc && !bcc.isEmpty()) {
				javax.mail.internet.InternetAddress[] addressBcc = new javax.mail.internet.InternetAddress[bcc
						.size()];
				for (int i = 0; i < bcc.size(); i++) {
					addressBcc[i] = new javax.mail.internet.InternetAddress(
							bcc.get(i));
					saveEmailHistory(userDetails, msg, subject, bcc.get(i),
							"BCC", emailType);
				}
				message.setRecipients(javax.mail.Message.RecipientType.BCC,
						addressBcc);
			}
			// send message
			message.setSubject(subject);
//			message.setText(msg);
			message.setContent(msg, "text/html; charset=utf-8");
			Transport.send(message);
			result = "success";
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	public String splitaddress1(String to) {
		String result = "";
		try {
			if (to.contains(";")) {
				address = to.split(";");
				result = "success";
			} else if (to.contains(",")) {
				address = to.split(",");
				result = "success";
			} else {
				address = to.split(",");
				result = "success";
			}
		} catch (NullPointerException e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * send login credential to user
	 * 
	 * @param to
	 * @param subject
	 * @param userName
	 * @param password
	 * @param name
	 * @param appRoot
	 * @param userDetails
	 * @return
	 */
	public String sendLoginCredentialToUser(String to, String subject,
			String password, String appRoot, UserDetailsDto userDetails) {
		String result = "success";
		String msg = "Email address : " + to + "<br/>Temporary Password : ["
				+ password + "]";
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants
					.getString(Constants.FG_VMS_FROM_EMIL_ID)));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject(subject);
			// freemarker stuff.
			Configuration cfg = new Configuration();
			// Set Directory for templates
			cfg.setDirectoryForTemplateLoading(new File(appRoot
					+ "email_templates"));
			Template template = cfg.getTemplate("html-mail-template.ftl");
			Map<String, String> rootMap = new HashMap<String, String>();

			rootMap.put("to", "Requestor");

			rootMap.put("body", msg);
			rootMap.put("from", "AVMS Security Admin.");
			Writer out = new StringWriter();
			template.process(rootMap, out);

			message.setContent(out.toString(), "text/html");

			Transport.send(message);
			saveEmailHistory(userDetails, msg, subject, to, "TO",
					EmailType.NEWUSERREGISTERED);
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * 
	 * @param userDetails
	 * @param msg
	 * @param subject
	 * @param emailId
	 * @param emailType
	 */
	private static void saveEmailHistory(UserDetailsDto userDetails,
			String msg, String subject, String emailId,
			String emailAddressType, EmailType emailType) {

		CURDDao<EmailHistory> curdTemplateHistory = new CURDTemplateImpl<EmailHistory>();
		EmailHistory emailHistory = new EmailHistory();
		emailHistory.setEmailbody(msg);
		emailHistory.setEmailsubject(subject);
		emailHistory.setEmailto(emailId);
		emailHistory.setEmaildate(new Date());
		if (emailType != null) {
			emailHistory.setEmailtype(emailType.getIndex());
		}
		emailHistory.setEmailAddressType(emailAddressType);
		curdTemplateHistory.save(emailHistory, userDetails);
	}

	public String sendCredentialsToPrimeVendor(VendorMasterForm vendorForm,
			UserDetailsDto userDetails, String password, String serverName,
			String appRoot) {
        
		//calling the methods for get emailtemplate message and body to prevent hard coding and for saving db only
		Integer customerDivisionId = null;
		if(null != vendorForm.getCustomerDivision() && 0 != vendorForm.getCustomerDivision()) {
			customerDivisionId = vendorForm.getCustomerDivision();
		}
		EmailTemplate emailTemplate=getTemplates(6, userDetails, customerDivisionId);
		
		String result = "success";
		if(emailTemplate.getId() != null) {
			String emailId = vendorForm.getPreparerEmail();
			String subject = emailTemplate.getEmailTemplateSubject();
			
			
			String templateMsg=emailTemplate.getEmailTemplateMessage();
			templateMsg=templateMsg.replace("&lt;", "<");
			templateMsg=templateMsg.replace("&gt;", ">");
			     
			//replacing param names, in the message body for db saving
			String modifiedServerName=serverName.concat("/viewanonymousvendors.do?parameter=emailLogin&prime=yes'");
			templateMsg=templateMsg.replace("#emailId", vendorForm.getPreparerEmail());
			templateMsg=templateMsg.replace("#password", password);
			templateMsg=templateMsg.replace("#serverName", modifiedServerName);
			//System.out.println("final msg--->"+templateMsg);
			
			StringBuilder body = new StringBuilder(templateMsg);
			/*"Dear Supplier,  \n\n"
			+ "Please find your AVMS credentials below to include the following: \n\n"
			+ "1. Email ID confirmation \n"
			+ "2. Temporary Password \n\n" + " EmailId : "
			+ emailId
			+ "\n\n"
			+ " Password : "
			+ password
			+ "\n\n"
			+ "Your temporary password will expire 7 days from today's date.  \n\n"
			+ "You may use the below URL to log into your AVMS account.  \n\n"
			+ "<a href='http://"
			+ serverName
			+ "/viewanonymousvendors.do?parameter=emailLogin&prime=yes'>Click Here</a> \n\n"

			+ "To log in and register, use the following steps:  \n\n"
			+ "1. Go to the AVMS registration portal using the above link. \n"
			+ "2. Enter your Email ID and temporary password. \n"
			+ "3. System will take you to the Reset Password section. \n"
			+ "4. After Resetting Your Password, Login with New Password. \n"
			+ "5. Enter your company information. \n"
			+ "6. Finalize the registration process by completing all required fields. \n"
			+ "7. Confirm that your information is complete and accurate. \n"
			+ "8. Submit your form to be reviewed by BP. \n\n"
			+ "If you have any questions, you may email support@avmsystem.com. \n\n"
			+ "Thank you. \n\n"
			+ "Warmest Regards, \n"
			+ "AVMS Support Team.");*/
			
			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));
				message.setSubject(subject);
				Configuration cfg = new Configuration();
				// Set Directory for templates
				cfg.setDirectoryForTemplateLoading(new File(appRoot + "email_templates"));
				Template template = cfg.getTemplate("prime-supplier-mail-template.ftl");
				Map<String, String> rootMap = new HashMap<String, String>();
				rootMap.put("serverName", serverName);
				rootMap.put("password", password);
				rootMap.put("emailID", emailId);
				Writer out = new StringWriter();
				template.process(rootMap, out);
				//message.setContent(out.toString(), "text/html");
				message.setContent(templateMsg.toString(), "text/html; charset=utf-8");
				Transport.send(message);
				saveEmailHistory(userDetails, templateMsg.toString(), subject, emailId, "TO", EmailType.CREDENTIALSTOUNKNOWNVENDORONSAVE);
				result = "vendorsavesuccess";
			} catch (Exception e) {
				result = "error";
				PrintExceptionInLogFile.printException(e);
			}
		}		
		return result;
	}

	/**
	 * This is the method to get emailtemplate message based templateCode and divisionid (from db) to prevent hard coding
	 * Here templateCode hard coded by the calling method
	 * params:templatecode and userDetails
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EmailTemplate getTemplates(int templateCode, UserDetailsDto userDetails, Integer customerDivisionId) {
		org.hibernate.Session hibernateSession = HibernateUtilCustomer
				.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		
		EmailTemplate template = new EmailTemplate();
		StringBuilder query=new StringBuilder();
		
		query.append("Select emailtemplate.ID,emailtemplate.EMAILTEMPLATENAME,emailtemplate.EMAILTEMPLATEMESSAGE,emailtemplate.EMAILTEMPLATESUBJECT"
				     +" from customer_emailtemplate emailtemplate ");
		
		if(null != customerDivisionId && 0 != customerDivisionId) {
			query.append("INNER JOIN customer_emailtemplate_division  division ON emailtemplate.ID=division.EMAILTEMPLATEID ");
		}
		
		query.append("where emailtemplate.TEMPLATECODE = " + templateCode);
		
		if(null != customerDivisionId && 0 != customerDivisionId) {
			query.append(" and division.CUSTOMERDIVISIONID="+customerDivisionId);
		}
		
		System.out.println("SendEmail @1305: "+query);
		SQLQuery query1=hibernateSession.createSQLQuery(query.toString());

		List<EmailTemplate> templates=(List<EmailTemplate>)query1.list();
		Iterator itr=templates.iterator();
		while(itr.hasNext())
		{
			Object obj[]=(Object[])itr.next();
			template.setId(Integer.parseInt(obj[0].toString()));
			template.setEmailTemplateName(obj[1].toString());
			template.setEmailTemplateMessage(obj[2].toString());
			template.setEmailTemplateSubject(obj[3].toString());
		}			
		return template;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EmailTemplate getTemplates(int templateCode, UserDetailsDto userDetails)
	{
		org.hibernate.Session hibernateSession = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		
		EmailTemplate template = new EmailTemplate();
		StringBuilder query = new StringBuilder();
		
		query.append("Select emailtemplate.ID,emailtemplate.EMAILTEMPLATENAME,emailtemplate.EMAILTEMPLATEMESSAGE,emailtemplate.EMAILTEMPLATESUBJECT"
				     +" from customer_emailtemplate emailtemplate ");
				
		if (userDetails.getSettings() != null)
		{
			if (userDetails.getSettings().getIsDivision() != 0 && userDetails.getSettings().getIsDivision() != null) 
			{
				if(userDetails.getCustomerDivisionIds() != null && userDetails.getCustomerDivisionIds().length != 0 && userDetails.getIsGlobalDivision() == 0)
				{
					query.append("INNER JOIN customer_emailtemplate_division  division ON emailtemplate.ID=division.EMAILTEMPLATEID ");
				}
			}
		}		
		
		query.append("where emailtemplate.TEMPLATECODE = " + templateCode);
		
		if (userDetails.getSettings() != null)
		{
			if (userDetails.getSettings().getIsDivision() != 0 && userDetails.getSettings().getIsDivision() != null) 
			{
				if(userDetails.getCustomerDivisionIds() != null && userDetails.getCustomerDivisionIds().length != 0 && userDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i = 0; i < userDetails.getCustomerDivisionIds().length; i++)
					{
						divisionIds.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					query.append(" and division.CUSTOMERDIVISIONID in(" + divisionIds.toString() + ") ");
				}
			}
		}		
		
		query.append(" LIMIT 1");
		
		System.out.println("SendEmail.java @1365: " + query);
		SQLQuery query1=hibernateSession.createSQLQuery(query.toString());

		List<EmailTemplate> templates=(List<EmailTemplate>)query1.list();
		Iterator itr=templates.iterator();
		while(itr.hasNext())
		{
			Object obj[]=(Object[])itr.next();
			template.setId(Integer.parseInt(obj[0].toString()));
			template.setEmailTemplateName(obj[1].toString());
			template.setEmailTemplateMessage(obj[2].toString());
			template.setEmailTemplateSubject(obj[3].toString());
		}			
		return template;
	}
	
	/**
	 * for sendAlert Mail To Admin On User Creation 
	 * @param firstName
	 * @param userDetails
	 * @param vendorId
	 * @param serverName
	 * @return
	 */
	public String sendAlertMailOnUserCreation(UserDetailsDto userDetails,
			UsersForm userForm, List<String> to, List<String> cc, List<String> bcc) {
		
		Integer customerDivisionId = userForm.getCustomerDivision();
		
		String result = null;
		
		try {
			
			EmailTemplate emailTemplate = getTemplates(23, userDetails, customerDivisionId);
			if(emailTemplate.getId() != null)
			{
				String subject= emailTemplate.getEmailTemplateSubject();
				String body = emailTemplate.getEmailTemplateMessage();
			
				body = body.replace("#firstName", userForm.getStackHolderFirstName());
				body = body.replace("#managerName", userForm.getStackHolderManagerName());
				body = body.replace("#reason", userForm.getStackHolderReason());
				body = body.replace("#emailAddress", userForm.getStackHolderEmailId());
			
				
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				if (null != to && !to.isEmpty()) {
					javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[to
							.size()];
					for (int index = 0; index < to.size(); index++) {
						addressTo[index] = new javax.mail.internet.InternetAddress(
								to.get(index));
						saveEmailHistory(userDetails, body, subject, to.get(index),
								"TO", EmailType.USERACCOUNTREGISTRATION);
					}
					message.setRecipients(javax.mail.Message.RecipientType.TO,
							addressTo);
				}
				if (null != cc && !cc.isEmpty()) {
					javax.mail.internet.InternetAddress[] addressCc = new javax.mail.internet.InternetAddress[cc
							.size()];
					for (int index = 0; index < cc.size(); index++) {
						addressCc[index] = new javax.mail.internet.InternetAddress(
								cc.get(index));
						saveEmailHistory(userDetails, body, subject, cc.get(index),
								"CC", EmailType.USERACCOUNTREGISTRATION);
					}
					message.setRecipients(javax.mail.Message.RecipientType.CC,
							addressCc);
				}
				if (null != bcc && !bcc.isEmpty()) {
					javax.mail.internet.InternetAddress[] addressBcc = new javax.mail.internet.InternetAddress[bcc
							.size()];
					for (int i = 0; i < bcc.size(); i++) {
						addressBcc[i] = new javax.mail.internet.InternetAddress(
								bcc.get(i));
						saveEmailHistory(userDetails, body, subject, bcc.get(i),
								"BCC", EmailType.USERACCOUNTREGISTRATION);
					}
					message.setRecipients(javax.mail.Message.RecipientType.BCC,
							addressBcc);
				}
				// send message
				message.setSubject(subject);
				message.setContent(body, "text/html; charset=utf-8");
				Transport.send(message);
				
				result = "success";
			}
			else
				return "templateNotFound";
			
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * for send send registration unapproved  alert to user
	 * @param userDetails
	 * @param emailId
	 * @param customerDivisionId
	 * @return
	 */
	public String sendRegistrationUnApprovedAlert(UserDetailsDto userDetails,
			String emailId, Integer customerDivisionId) {
		
		String result = null;
		try{
			EmailTemplate emailTemplate = getTemplates(25, userDetails, customerDivisionId);
			if(emailTemplate.getId() != null)
			{
				
				String subject= emailTemplate.getEmailTemplateSubject();
				String body = emailTemplate.getEmailTemplateMessage();
				
				saveEmailHistory(userDetails, body, subject, emailId,
						"TO", EmailType.USERREGISTRATIONAPPROVED);
				
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO,	InternetAddress.parse(emailId));
				message.setSubject(subject);
				message.setContent(body, "text/html; charset=utf-8");
				Transport.send(message);
				result = "success";
			}
			else
				return "templateNotFound";
			
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
	return result;
	}

	/**
	 * for send registration approved alert to user
	 * @param userDetails
	 * @param emailId
	 * @param password
	 * @param customerDivisionId
	 * @return
	 */
	public String sendRegistrationApprovedAlert(UserDetailsDto userDetails,
			String emailId, String password, Integer customerDivisionId) {

		String result = null;
		try {
			EmailTemplate emailTemplate = getTemplates(24, userDetails,
					customerDivisionId);
			if (emailTemplate.getId() != null) {
				
				String subject = emailTemplate.getEmailTemplateSubject();
				String body = emailTemplate.getEmailTemplateMessage();

				body = body.replace("#emailId", emailId);
				body = body.replace("#password", password);

				saveEmailHistory(userDetails, body, subject, emailId, "TO",
						EmailType.USERREGISTRATIONUNAPPROVED);

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emailId));
				message.setSubject(subject);
				message.setContent(body, "text/html; charset=utf-8");
				Transport.send(message);
				result = "success";
			} else
				return "templateNotFound";

		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}

	/**
	 * for send certificate expiry monthly notification report
	 * 
	 * @param userDetails
	 * @param to
	 * @param cc
	 * @param bcc
	 * @param certificateExpirationNotificationEmailReport
	 * @param subject
	 * @param attachedFileName
	 * @param bos
	 */
	public void sendCertificateExpirationNotificationMonthlyEmailReport(UserDetailsDto userDetails, List<String> to,
			List<String> cc, List<String> bcc, List<Tier2ReportDto> certificateExpirationNotificationEmailReport,
			String attachedFileName, String subject, ByteArrayOutputStream bos) 
	{		
		try 
		{
			log.info("Sending Email sendCertificateExpirationNotificationMonthlyEmailReport Email with Data & Attachment.");
			
			StringBuilder body = new StringBuilder();
			body.append("<html>"
					+ "<body>"
					+"<span align=justify> Hi,<br/><br/> "
					+ " 	Certification Expiration Notifications Report for last Month <br><br><br> "
					+"<table border='1' cellspacing='1' cellpadding='1' width='100%'><tr> "
					+ "<th> Vendor Name</th> <th>Email Id</th> <th>Email Date</th> <th>"
					+ " Certificate No</th><th>Certificate Name</th><th>Agency Name</th>"
					+ "<th>Expiry Date</th><th>Is Renewed</th></tr>");

			for (Tier2ReportDto certificateExpiration : certificateExpirationNotificationEmailReport) 
			{
				body.append("<td> "+ certificateExpiration.getVendorName() +"</td>"
						+"<td> "+ certificateExpiration.getEmailId() +"</td>"
						+"<td> "+ certificateExpiration.getEmailDate() +"</td>"
						+"<td> "+ certificateExpiration.getCertificateNumber() +"</td>"
						+"<td> "+ certificateExpiration.getCertificateName() +"</td>"
						+"<td> "+ certificateExpiration.getCertAgencyName() +"</td>"
						+"<td> "+ certificateExpiration.getExpDate() +"</td>"
						+"<td> "+ certificateExpiration.getCertificateExpiryStatus() +"</td> </tr>" );
			}
						
			body.append("</table><br><br>");

			body.append("Warmest Regards, <br> AVMS Support Team. </body></html>");
		
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants.getString(Constants.FG_VMS_FROM_EMIL_ID)));	
			
			
			if (to != null && !to.isEmpty()) 
			{
				javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[to.size()];
				
				for (int index = 0; index < to.size(); index++) 
				{
					addressTo[index] = new javax.mail.internet.InternetAddress(to.get(index));
					saveEmailHistory(userDetails, body.toString(), subject, to.get(index),"TO", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				}
				message.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);
			}
			
			if (cc != null && !cc.isEmpty()) 
			{
				javax.mail.internet.InternetAddress[] addressCc = new javax.mail.internet.InternetAddress[cc.size()];

				for (int index = 0; index < cc.size(); index++) 
				{
					addressCc[index] = new javax.mail.internet.InternetAddress(cc.get(index));
					saveEmailHistory(userDetails, body.toString(), subject, cc.get(index),"CC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				}
				message.setRecipients(javax.mail.Message.RecipientType.CC, addressCc);
			}
			
			if (bcc != null && !bcc.isEmpty()) 
			{
				javax.mail.internet.InternetAddress[] addressBcc = new javax.mail.internet.InternetAddress[bcc.size()];
				
				for (int i = 0; i < bcc.size(); i++) 
				{
					addressBcc[i] = new javax.mail.internet.InternetAddress(bcc.get(i));
					saveEmailHistory(userDetails, body.toString(), subject, bcc.get(i),	"BCC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				}
				message.setRecipients(javax.mail.Message.RecipientType.BCC, addressBcc);
			}
			
			message.setSubject(subject);
		
			Multipart mp = new MimeMultipart();
			
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(body.toString(), "text/html; charset=utf-8");
			mp.addBodyPart(htmlPart);
 
			MimeBodyPart attachment = new MimeBodyPart();
			attachment.setFileName(attachedFileName+".xls");
			DataSource fds = new ByteArrayDataSource(bos.toByteArray(), "application/vnd.ms-excel");
 
			attachment.setDataHandler(new DataHandler(fds));
			mp.addBodyPart(attachment);
         
			message.setContent(mp);
			Transport.send(message);
			
			log.info("Email Sending Successfull.");
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}		
	}
	
	/**
	 * for send certificate expiry monthly notification report(Zero record)
	 * 
	 * @param userDetails
	 * @param to
	 * @param cc
	 * @param bcc
	 * @param subject
	 */
	public void sendZeroCertificateExpirationNotificationMonthlyEmailReport(UserDetailsDto userDetails, List<String> to,
			List<String> cc, List<String> bcc, String subject) 
	{		
		try 
		{		
			log.info("Sending Email sendCertificateExpirationNotificationMonthlyEmailReport Email without Data & Attachment.");
			EmailTemplate emailTemplate=getTemplates(30, userDetails);
			String msg=emailTemplate.getEmailTemplateMessage();
			
			/*StringBuilder body = new StringBuilder();
			body.append("<html>"
					+ "<body>"
					+"<span align=justify> Hi,<br/><br/> "
					+ "		There was no Certificate Expiration Notification Emails for Last Month. <br/><br/>");			

			body.append("Warmest Regards, <br> AVMS Support Team. </body></html>");*/
		
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constants.getString(Constants.FG_VMS_FROM_EMIL_ID)));			
			
			if (to != null && !to.isEmpty()) 
			{
				javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[to.size()];
				
				for (int index = 0; index < to.size(); index++) 
				{
					addressTo[index] = new javax.mail.internet.InternetAddress(to.get(index));
//					saveEmailHistory(userDetails, body.toString(), subject, to.get(index),"TO", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
					saveEmailHistory(userDetails, msg, subject, to.get(index),"TO", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				}
				message.setRecipients(javax.mail.Message.RecipientType.TO, addressTo);
			}
			
			if (cc != null && !cc.isEmpty()) 
			{
				javax.mail.internet.InternetAddress[] addressCc = new javax.mail.internet.InternetAddress[cc.size()];
				
				for (int index = 0; index < cc.size(); index++) 
				{
					addressCc[index] = new javax.mail.internet.InternetAddress(cc.get(index));
//					saveEmailHistory(userDetails, body.toString(), subject, cc.get(index),"CC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
					saveEmailHistory(userDetails, msg, subject, cc.get(index),"CC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				}
				message.setRecipients(javax.mail.Message.RecipientType.CC, addressCc);
			}
			
			if (bcc != null && !bcc.isEmpty()) 
			{
				javax.mail.internet.InternetAddress[] addressBcc = new javax.mail.internet.InternetAddress[bcc.size()];
				
				for (int i = 0; i < bcc.size(); i++) 
				{
					addressBcc[i] = new javax.mail.internet.InternetAddress(bcc.get(i));
//					saveEmailHistory(userDetails, body.toString(), subject, bcc.get(i),	"BCC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
					saveEmailHistory(userDetails, msg, subject, bcc.get(i),	"BCC", EmailType.CERTIFICATEEXPIRATIONNOTIFICATION);
				}
				message.setRecipients(javax.mail.Message.RecipientType.BCC, addressBcc);
			}
			
			message.setSubject(subject);			
//			message.setContent(body.toString() , "text/html; charset=utf-8");
			message.setContent(msg , "text/html; charset=utf-8");
			Transport.send(message);
			log.info("Email Sending Successfull.");
		} 
		catch (Exception e) 
		{
			PrintExceptionInLogFile.printException(e);
		}		
	}
	
	/**
	 * for send Customer User Registration Success Alert
	 * @param userDetails
	 * @param userForm
	 * @param to
	 * @param cc
	 * @param bcc
	 * @return
	 */
	public String sendCustomerUserRegistrationAlert(UserDetailsDto userDetails,
			UsersForm userForm, List<String> to, List<String> cc, List<String> bcc) {
		
		Integer customerDivisionId = userForm.getCustomerDivision();
		
		String result = null;
		
		try {
			
			EmailTemplate emailTemplate = getTemplates(26, userDetails, customerDivisionId);
			if(emailTemplate.getId() != null)
			{
				String subject= emailTemplate.getEmailTemplateSubject();
				String body = emailTemplate.getEmailTemplateMessage();
			
				body = body.replace("#firstName", userForm.getFirstName());
				body = body.replace("#lastName", userForm.getLastName());
				body = body.replace("#emailAddress", userForm.getUserEmailId());
				
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(Constants
						.getString(Constants.FG_VMS_FROM_EMIL_ID)));
				if (null != to && !to.isEmpty()) {
					javax.mail.internet.InternetAddress[] addressTo = new javax.mail.internet.InternetAddress[to
							.size()];
					for (int index = 0; index < to.size(); index++) {
						addressTo[index] = new javax.mail.internet.InternetAddress(
								to.get(index));
						saveEmailHistory(userDetails, body, subject, to.get(index),
								"TO", EmailType.APPROVEDCUSTOMERUSERACCOUNTCREATIONALERT);
					}
					message.setRecipients(javax.mail.Message.RecipientType.TO,
							addressTo);
				}
				if (null != cc && !cc.isEmpty()) {
					javax.mail.internet.InternetAddress[] addressCc = new javax.mail.internet.InternetAddress[cc
							.size()];
					for (int index = 0; index < cc.size(); index++) {
						addressCc[index] = new javax.mail.internet.InternetAddress(
								cc.get(index));
						saveEmailHistory(userDetails, body, subject, cc.get(index),
								"CC", EmailType.APPROVEDCUSTOMERUSERACCOUNTCREATIONALERT);
					}
					message.setRecipients(javax.mail.Message.RecipientType.CC,
							addressCc);
				}
				if (null != bcc && !bcc.isEmpty()) {
					javax.mail.internet.InternetAddress[] addressBcc = new javax.mail.internet.InternetAddress[bcc
							.size()];
					for (int i = 0; i < bcc.size(); i++) {
						addressBcc[i] = new javax.mail.internet.InternetAddress(
								bcc.get(i));
						saveEmailHistory(userDetails, body, subject, bcc.get(i),
								"BCC", EmailType.APPROVEDCUSTOMERUSERACCOUNTCREATIONALERT);
					}
					message.setRecipients(javax.mail.Message.RecipientType.BCC,
							addressBcc);
				}
				// send message
				message.setSubject(subject);
				message.setContent(body, "text/html; charset=utf-8");
				Transport.send(message);
				
				result = "success";
			}
			else
				return "templateNotFound";
			
		} catch (Exception e) {
			result = "error";
			PrintExceptionInLogFile.printException(e);
		}
		return result;
	}
}