/**
 * ExportGridUtil.java
 */
package com.fg.vms.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Utility class used for dynamic column names and dynamic column model for
 * exporting the supplier report.
 * 
 * @author vinoth
 * 
 */
public final class ExportGridUtil {

	/**
	 * Get the column name array dynamically.
	 * 
	 * @param colNamesArray
	 * @return
	 */
	public static JSONArray getColumnNamesArray(JSONArray colNamesArray) {
		/*
		 * push col model names.
		 */
		colNamesArray.add("Vendor Name");
		colNamesArray.add("Owner Name");
		colNamesArray.add("Vendor Address");
		colNamesArray.add("City");
		colNamesArray.add("State");
		colNamesArray.add("Zipcode");
		colNamesArray.add("Phone");
		colNamesArray.add("Fax");
		colNamesArray.add("EmailID");
		
		/*colNamesArray.add("Agency Name");
		colNamesArray.add("Classification");
		colNamesArray.add("Effective Date");
		colNamesArray.add("Expiry Date");
		colNamesArray.add("Capablities");
		colNamesArray.add("NAICS Desc");*/
		
		colNamesArray.add("NAICS Capabilities");
		colNamesArray.add("Diverse Classification");
		colNamesArray.add("Certificate Agency,Effective & Expire Date");
	
		return colNamesArray;
	}

	public static JSONArray getColumnModelArray(JSONArray colModelArray) {

		JSONObject colModelObjVendorname = new JSONObject();
		JSONObject colModelObjOwnerName = new JSONObject();
		//JSONObject colModelObjFirstname = new JSONObject();
		JSONObject colModelObjVendoraddress = new JSONObject();
		JSONObject colModelObjCity = new JSONObject();
		JSONObject colModelObjState = new JSONObject();
		JSONObject colModelObjZipcode = new JSONObject();
		JSONObject colModelObjPhone = new JSONObject();
		JSONObject colModelObjFax = new JSONObject();
		JSONObject colModelObjEmailID = new JSONObject();
		
		/*JSONObject colModelObjAgencyName = new JSONObject();
		JSONObject colModelObjCert = new JSONObject();
		JSONObject colModelObjEffDate = new JSONObject();
		JSONObject colModelObjExpDate = new JSONObject();
		JSONObject colModelObjCapab = new JSONObject();
		JSONObject colModelObjNaicsDesc = new JSONObject();
*/
		JSONObject colModelObjNaicsCapabilities = new JSONObject();
		JSONObject colModelObjDivesseClassification = new JSONObject();
		JSONObject colModelObjcertificateDetails = new JSONObject();
		

		/*
		 * push colModel into json objects.
		 */
		colModelObjVendorname.put("index", "vendorName");
		colModelObjVendorname.put("name", "vendorName");
		colModelObjOwnerName.put("index", "ownerName");
		colModelObjOwnerName.put("name", "ownerName");
/*		colModelObjFirstname.put("index", "firstName");
		colModelObjFirstname.put("name", "firstName");*/
		colModelObjVendoraddress.put("index", "address");
		colModelObjVendoraddress.put("name", "address");
		colModelObjCity.put("index", "city");
		colModelObjCity.put("name", "city");
		colModelObjState.put("index", "state");
		colModelObjState.put("name", "state");
		colModelObjZipcode.put("index", "zipcode");
		colModelObjZipcode.put("name", "zipcode");
		colModelObjPhone.put("index", "phone");
		colModelObjPhone.put("name", "phone");
		colModelObjFax.put("index", "fax");
		colModelObjFax.put("name", "fax");
		colModelObjEmailID.put("index", "emailId");
		colModelObjEmailID.put("name", "emailId");
	
	/*	colModelObjAgencyName.put("index", "agencyName");
		colModelObjAgencyName.put("name", "agencyName");
		colModelObjCert.put("index", "certName");
		colModelObjCert.put("name", "certName");
		colModelObjEffDate.put("index", "effDate");
		colModelObjEffDate.put("name", "effDate");
		colModelObjExpDate.put("index", "expDate");
		colModelObjExpDate.put("name", "expDate");
		colModelObjCapab.put("index", "capablities");
		colModelObjCapab.put("name", "capablities");
		colModelObjNaicsDesc.put("index", "naicsDesc");
		colModelObjNaicsDesc.put("name", "naicsDesc");*/
		
		colModelObjNaicsCapabilities.put("index", "naicsCapabilities");
		colModelObjNaicsCapabilities.put("name", "naicsCapabilities");
		colModelObjDivesseClassification.put("index", "diverseClassification");
		colModelObjDivesseClassification.put("name", "diverseClassification");
		colModelObjcertificateDetails.put("index", "certificateAgencyDetails");
		colModelObjcertificateDetails.put("name", "certificateAgencyDetails");
		
		
		/*
		 * push colModel into json array.
		 */
		colModelArray.add(colModelObjVendorname);
		colModelArray.add(colModelObjOwnerName);
		colModelArray.add(colModelObjVendoraddress);
		colModelArray.add(colModelObjCity);
		colModelArray.add(colModelObjState);
		colModelArray.add(colModelObjZipcode);
		colModelArray.add(colModelObjPhone);
		colModelArray.add(colModelObjFax);
		colModelArray.add(colModelObjEmailID);

		/*colModelArray.add(colModelObjAgencyName);
		colModelArray.add(colModelObjCert);
		colModelArray.add(colModelObjEffDate);
		colModelArray.add(colModelObjExpDate);
		colModelArray.add(colModelObjCapab);
		colModelArray.add(colModelObjNaicsDesc);*/
		
		colModelArray.add(colModelObjNaicsCapabilities);
		colModelArray.add(colModelObjDivesseClassification);
		colModelArray.add(colModelObjcertificateDetails);
		
		return colModelArray;
	}

}
