/**
 * 
 */
package com.fg.vms.util;

/**
 * @author gpirabu
 * 
 */
public enum EmailType {

	CERTIFICATEEXPIRATIONNOTIFICATION((short) 1), CERTIFICATEEXPIRATION(
			(short) 2), REPORTDUE((short) 3), REPORTNOTSUBMITTED((short) 4), ANONYMOUSVENDORREGISTERED(
			(short) 5), ASSESSMENTTOVENDOR((short) 6), NEWPASSWORD((short) 7), MAILTODISTRIBUTION(
			(short) 8), CREDENTIALSTOUNKNOWNVENDORONSAVE((short) 9), REPORTSUBMITTED(
			(short) 10), NEWUSERREGISTERED((short) 11), SENDCREDENTIALSTOVENDOR(
			(short)9), USERACCOUNTREGISTRATION((short)23), USERREGISTRATIONAPPROVED(
			(short)24), USERREGISTRATIONUNAPPROVED((short)25), APPROVEDCUSTOMERUSERACCOUNTCREATIONALERT(
			(short)26) ;

	private EmailType(Short index) {
		this.index = index;
	}

	private short index;

	/**
	 * @return the index
	 */
	public short getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(short index) {
		this.index = index;
	}

}
