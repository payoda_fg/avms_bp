/*
 * HibernateUtilCustomer.java
 */
package com.fg.vms.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * HibernateUtilCustomer - Initialization for the SessionFactory for particular
 * customer.
 * 
 * @author pirabu
 * 
 */
@SuppressWarnings("deprecation")
public class HibernateUtilCustomer {

    /** The session factory. */
    private static SessionFactory sessionFactory;

    /**
     * Instantiates a new hibernate util customer.
     */
    private HibernateUtilCustomer() {

    }

    /**
     * Builds the session factory.
     * 
     * @param dbName
     *            the db name
     * @param dbConnectonUrl
     *            the db connecton url
     * @return the session factory
     */
    public static SessionFactory buildSessionFactory() {

		if (sessionFactory == null) {
			try {
				sessionFactory = new AnnotationConfiguration()
						.configure("hibernate.customer.cfg.xml")
						.setProperty(
								"hibernate.connection.url",
								Constants
										.getString(Constants.DB_CONNECTION_URL))
						.setProperty("hibernate.connection.username",
								Constants.getString(Constants.DB_USERNAME))
						.setProperty("hibernate.connection.password",
								Constants.getString(Constants.DB_PASSWORD))
						.buildSessionFactory();
				/*
				 * sessionFactory = new AnnotationConfiguration()
				 * .configure("hibernate.customer.cfg.xml")
				 * .buildSessionFactory();
				 */
			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed."
						+ ex);
				throw new ExceptionInInitializerError(ex);
			}
		}

	return sessionFactory;
    }

    /**
     * Gets the session factory.
     * 
     * @return the session factory
     */
    public static void removeSessionFactory() {

	sessionFactory = null;
    }

}
