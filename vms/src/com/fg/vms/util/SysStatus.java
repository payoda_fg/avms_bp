/**
 * SysStatus.java
 */
package com.fg.vms.util;

/**
 * Enumeration of the allowed "isActive" state of any persistent record (i.e.,
 * this is meta data for records we store in the database). ACTIVE indicates the
 * record is a current/valid record. DELETED indicates the record is no longer
 * valid and should _not_ be accessed by normal program code
 * 
 * @author pirabu
 * 
 */
public enum SysStatus {

    /** The ACTIVE. */
    ACTIVE,
    /** The DELETED. */
    DELETED
}