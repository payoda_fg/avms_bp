/*
 * Encrypt.java
 */
package com.fg.vms.util;

import java.io.ByteArrayOutputStream;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * EncryptText used to encrypt the the password to store in db.
 */
public class Encrypt {

    /**
     * Initialize.
     */
    private void initialize() {
	Provider ibmjce = new com.ibm.crypto.provider.IBMJCE();
	Security.addProvider(ibmjce);
    }

    /**
     * encryptText used to encrypt the the password to store in db.
     * 
     * @param key
     *            the key
     * @param value
     *            the value
     * @return the string
     * @throws Exception
     *             the exception
     */
    public String encryptText(String key, String value) throws Exception {
	KeyGen kgn = new KeyGen();
	byte[] keyVal = kgn.deriveKey(key);
	byte[] encrVal = encrypt(keyVal, value.getBytes());
	return kgn.byteArray2HexString(encrVal);
    }

    /**
     * This method encrypts the data . For encryption , a key is needed.<br>
     * The key and the textt to be crypted are sent as array of type byte. The
     * resulting crypted text is also an array of bytes. The encryption is done
     * using the DES algorithm
     * 
     * @param cryptedKey
     *            Array of type byte containing the key.
     * @param clearText
     *            Array of text containing the text to be crypted.
     * @return byte[] Returns the crypted value as an array of bytes.
     * @throws Exception
     *             the exception
     */
    public byte[] encrypt(byte[] cryptedKey, byte[] clearText) throws Exception {
	byte[] ciphertext = null;
	SecretKeySpec desKeySpec = new SecretKeySpec(cryptedKey, "DESede");
	javax.crypto.Cipher desCipher = Cipher
		.getInstance("DESede/CBC/PKCS5Padding");
	desCipher.init(Cipher.ENCRYPT_MODE, desKeySpec);
	byte[] iv = desCipher.getIV();
	ByteArrayOutputStream txt = new ByteArrayOutputStream();
	txt.write(iv, 0, iv.length);
	txt.write(clearText, 0, clearText.length);
	ciphertext = desCipher.doFinal(txt.toByteArray());
	return ciphertext;
    }
}
