/**
 * 
 */
package com.fg.vms.util;

/**
 * @author gpirabu
 * 
 */
public enum VendorStatus {

	NEWREGISTRATION("N"), PENDINGREVIEW("P"), ACTIVE("A"), INACTIVE("I"), BPSUPPLIER(
            "B");

    private VendorStatus(String status) {
        this.status = status;
    }

    private String status;

    /**
     * @return the index
     */
    public String getIndex() {
        return status;
    }

    /**
     * @param index
     *            the index to set
     */
    public void setIndex(String index) {
        this.status = index;
    }

}
