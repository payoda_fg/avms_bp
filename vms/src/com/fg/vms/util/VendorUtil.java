/**
 * 
 */
package com.fg.vms.util;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorOwner;
import com.fg.vms.customer.model.CustomerVendorSales;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorbusinessbiographysafety;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.customer.pojo.VendorMasterForm;

/**
 * This class contains common functionality of vendor such as bind the value to
 * action form from db.
 * 
 * @author gpirabu
 * 
 */
public class VendorUtil {

	public static VendorMasterForm packVendorInfomation(
			VendorMasterForm vendorMasterForm, RetriveVendorInfoDto vendorInfo,
			Integer vendorId, VendorMaster vendorMaster,
			List<VendorCertificate> vendorCetrificate, UserDetailsDto appDetails) {

		// naicsMasters = vendorInfo.getNaicsMaster();

		vendorMasterForm.setVendorName(vendorMaster.getVendorName());
		vendorMasterForm.setVendorCode(vendorMaster.getVendorCode());
		vendorMasterForm.setHiddenVendorCode(vendorMaster.getVendorCode());
		vendorMasterForm.setDunsNumber(vendorMaster.getDunsNumber());
		vendorMasterForm.setCompanytype(vendorMaster.getCompanyType());
		vendorMasterForm.setTaxId(vendorMaster.getTaxId());
		vendorMasterForm.setWebsite(vendorMaster.getWebsite());
		vendorMasterForm.setEmailId(vendorMaster.getEmailId());
		vendorMasterForm.setIsApproved(vendorMaster.getIsApproved());
		// Convert double to String
		java.text.NumberFormat formatter = java.text.NumberFormat
				.getCurrencyInstance(java.util.Locale.US);

		vendorMasterForm.setNumberOfEmployees(String.valueOf(vendorMaster
				.getNumberOfEmployees() != null ? vendorMaster
				.getNumberOfEmployees() : ""));
		vendorMasterForm.setYearOfEstablishment(String.valueOf(vendorMaster
				.getYearOfEstablishment() != null ? vendorMaster
				.getYearOfEstablishment() : ""));
		if (vendorMaster.getPrimeNonPrimeVendor() != null) {
			vendorMasterForm.setPrimeNonPrimeVendor(vendorMaster
					.getPrimeNonPrimeVendor().toString());
		}
//		vendorMasterForm.setVendorNotes(vendorMaster.getVendorNotes());
		if (vendorMaster.getPrimeNonPrimeVendor() != null) {
			if (vendorMaster.getPrimeNonPrimeVendor() == 1) {
				vendorMasterForm.setPrime("1");
			} else {
				vendorMasterForm.setNonprime("0");
			}
		}
		boolean diverse = (vendorMaster.getDeverseSupplier() != 0);

		if (vendorMaster.getVendorStatus() != null
				&& vendorMaster.getVendorStatus().equalsIgnoreCase("p")) {
			vendorMasterForm.setManualFlag("on");
		}

		vendorMasterForm.setVendorDescription(vendorMaster
				.getVendorDescription());
		vendorMasterForm.setCompanyInformation(vendorMaster
				.getCompanyInformation());
		vendorMasterForm.setStateIncorporation(vendorMaster
				.getStateIncorporation());
		vendorMasterForm.setStateSalesTaxId(vendorMaster.getStateSalesTaxId());

		vendorMasterForm.setClassificationNotes(vendorMaster.getDiverseNotes());
		if (null != vendorCetrificate) {
			vendorMasterForm
					.setDiverseCertificats(packDiverseCertificationTypes(vendorCetrificate));
		}
//		vendorMasterForm.setVendorNotes(vendorMaster.getVendorNotes());
		vendorMasterForm.setBusinessType(vendorMaster.getBusinesstype());
		vendorMasterForm.setBpSegmentId(vendorMaster.getBpSegment());
		
		if (null != vendorMaster.getCompanyownership()
				&& !(vendorMaster.getCompanyownership().equals((short) 0))) {
			if (vendorMaster.getCompanyownership().equals((short) 1)) {
				vendorMasterForm.setOwnerPublic("1");
				vendorMasterForm.setOwnerPrivate("0");
				vendorMasterForm.setCompanyOwnership("1");
			} else if (vendorMaster.getCompanyownership().equals((short) 2)) {
				vendorMasterForm.setOwnerPublic("0");
				vendorMasterForm.setOwnerPrivate("2");
				vendorMasterForm.setCompanyOwnership("2");
			}

		} else {
			vendorMasterForm.setOwnerPublic("1");
			vendorMasterForm.setOwnerPrivate("2");
			vendorMasterForm.setCompanyOwnership(null);
		}

		if (null != vendorCetrificate && !vendorCetrificate.isEmpty()) {
			for (int i = 0; i < vendorCetrificate.size(); i++) {

				if (i == 0) {
					vendorMasterForm.setVendorCertID1(vendorCetrificate.get(i)
							.getId());
					vendorMasterForm.setDivCertAgen1(vendorCetrificate.get(i)
							.getCertAgencyId().getId().toString());
					vendorMasterForm.setDivCertType1(vendorCetrificate.get(i)
							.getCertMasterId().getId().toString());
					vendorMasterForm.setCertType1(vendorCetrificate.get(i)
							.getCertificateType());
					vendorMasterForm.setCertAgencies1(agenciesByClassification(
							vendorCetrificate.get(i).getCertMasterId().getId(),
							appDetails));

					vendorMasterForm
							.setCertificateTypes1(certificateTypesByClassification(
									vendorCetrificate.get(i).getCertMasterId()
											.getId(), appDetails));
					vendorMasterForm.setExpDate1(CommonUtils
							.convertDateToString(vendorCetrificate.get(i)
									.getExpiryDate()));
					vendorMasterForm.setCertificationNo1(vendorCetrificate.get(
							i).getCertificateNumber());
					vendorMasterForm.setEffDate1(CommonUtils
							.convertDateToString(vendorCetrificate.get(i)
									.getEffectiveDate()));
					vendorMasterForm
							.setEthnicity1(String.valueOf(vendorCetrificate
									.get(i).getEthnicityID()));
					try {
						if (vendorCetrificate.get(i).getContent() != null
								&& vendorCetrificate.get(i).getContent()
										.length() != 0) {
							vendorMasterForm.setCertFile1Id(vendorCetrificate
									.get(i).getId());
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
				if (i == 1) {
					vendorMasterForm.setVendorCertID2(vendorCetrificate.get(i)
							.getId());
					vendorMasterForm.setDivCertAgen2(vendorCetrificate.get(i)
							.getCertAgencyId().getId().toString());
					vendorMasterForm.setDivCertType2(vendorCetrificate.get(i)
							.getCertMasterId().getId().toString());
					vendorMasterForm.setCertType2(vendorCetrificate.get(i)
							.getCertificateType());
					vendorMasterForm.setExpDate2(CommonUtils
							.convertDateToString(vendorCetrificate.get(i)
									.getExpiryDate()));
					vendorMasterForm.setCertificationNo2(vendorCetrificate.get(
							i).getCertificateNumber());
					vendorMasterForm.setEffDate2(CommonUtils
							.convertDateToString(vendorCetrificate.get(i)
									.getEffectiveDate()));
					vendorMasterForm.setCertAgencies2(agenciesByClassification(
							vendorCetrificate.get(i).getCertMasterId().getId(),
							appDetails));

					vendorMasterForm
							.setCertificateTypes2(certificateTypesByClassification(
									vendorCetrificate.get(i).getCertMasterId()
											.getId(), appDetails));
					vendorMasterForm
							.setEthnicity2(String.valueOf(vendorCetrificate
									.get(i).getEthnicityID()));
					try {
						if (vendorCetrificate.get(i).getContent() != null
								&& vendorCetrificate.get(i).getContent()
										.length() != 0) {
							vendorMasterForm.setCertFile2Id(vendorCetrificate
									.get(i).getId());
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (i == 2) {
					vendorMasterForm.setVendorCertID3(vendorCetrificate.get(i)
							.getId());
					vendorMasterForm.setDivCertAgen3(vendorCetrificate.get(i)
							.getCertAgencyId().getId().toString());
					vendorMasterForm.setDivCertType3(vendorCetrificate.get(i)
							.getCertMasterId().getId().toString());
					vendorMasterForm.setCertType3(vendorCetrificate.get(i)
							.getCertificateType());
					vendorMasterForm.setExpDate3(CommonUtils
							.convertDateToString(vendorCetrificate.get(i)
									.getExpiryDate()));
					vendorMasterForm.setCertificationNo3(vendorCetrificate.get(
							i).getCertificateNumber());
					vendorMasterForm.setEffDate3(CommonUtils
							.convertDateToString(vendorCetrificate.get(i)
									.getEffectiveDate()));
					vendorMasterForm.setCertAgencies3(agenciesByClassification(
							vendorCetrificate.get(i).getCertMasterId().getId(),
							appDetails));
					vendorMasterForm
							.setCertificateTypes3(certificateTypesByClassification(
									vendorCetrificate.get(i).getCertMasterId()
											.getId(), appDetails));
					vendorMasterForm
							.setEthnicity3(String.valueOf(vendorCetrificate
									.get(i).getEthnicityID()));
					try {
						if (vendorCetrificate.get(i).getContent() != null
								&& vendorCetrificate.get(i).getContent()
										.length() != 0) {
							vendorMasterForm.setCertFile3Id(vendorCetrificate
									.get(i).getId());
						}
					} catch (Exception e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		}
		if (diverse) {// get the vendor master certificates
			vendorMasterForm.setDeverseSupplier("on");
		} else {
			vendorMasterForm.setDeverseSupplier("");
		}

		// Get the Naics Master Codes
		// List<NaicsCode> naicsMasters = vendorInfo.getNaicsMaster();
		List<VendorNAICS> vendorNaics = vendorInfo.getVendorNaics();
		Collections.sort(vendorNaics, VendorUtil.NAICSPrimaryComparator);
		if (vendorNaics != null && !vendorNaics.isEmpty()) {
			// vendorMasterForm.setNaicsPrimary1("0");
			// vendorMasterForm.setNaicsPrimary2("0");
			// vendorMasterForm.setNaicsPrimary3("0");
			// vendorMasterForm.setLastrowcount(vendorNaics.size());
			for (int index = 0; index < vendorNaics.size(); index++) {
				if (index == 0) {
					vendorMasterForm
							.setNaicsID1(vendorNaics.get(index).getId());
					vendorMasterForm.setNaicsCode_0(vendorNaics.get(index)
							.getNaicsId().getNaicsCode());
					vendorMasterForm.setNaicsDesc1(vendorNaics.get(index)
							.getNaicsId().getNaicsDescription());
					vendorMasterForm.setCapabilitie1(vendorNaics.get(index)
							.getCapabilities());
					// if (!(vendorNaics.get(index).getPrimary().equals((byte)
					// 0))) {
					// vendorMasterForm.setPrimary("1");
					// // vendorMasterForm.setNaicsPrimary1("1");
					//
					// }
				} else if (index == 1) {
					vendorMasterForm
							.setNaicsID2(vendorNaics.get(index).getId());
					vendorMasterForm.setNaicsCode_1(vendorNaics.get(index)
							.getNaicsId().getNaicsCode());
					vendorMasterForm.setNaicsDesc2(vendorNaics.get(index)
							.getNaicsId().getNaicsDescription());
					vendorMasterForm.setCapabilitie2(vendorNaics.get(index)
							.getCapabilities());
					// if (!(vendorNaics.get(index).getPrimary().equals((byte)
					// 0))) {
					// vendorMasterForm.setPrimary("2");
					// // vendorMasterForm.setNaicsPrimary2("2");
					//
					// }
				} else if (index == 2) {
					vendorMasterForm
							.setNaicsID3(vendorNaics.get(index).getId());
					vendorMasterForm.setNaicsCode_2(vendorNaics.get(index)
							.getNaicsId().getNaicsCode());
					vendorMasterForm.setNaicsDesc3(vendorNaics.get(index)
							.getNaicsId().getNaicsDescription());
					vendorMasterForm.setCapabilitie3(vendorNaics.get(index)
							.getCapabilities());
					// if (!(vendorNaics.get(index).getPrimary().equals((byte)
					// 0))) {
					// vendorMasterForm.setPrimary("3");
					// // vendorMasterForm.setNaicsPrimary3("3");
					// }
				}
			}
		}
		// else {
		// vendorMasterForm.setPrimary("1");
		// vendorMasterForm.setNaicsPrimary1("1");
		// vendorMasterForm.setNaicsPrimary2("2");
		// vendorMasterForm.setNaicsPrimary3("3");
		// }
		vendorMasterForm.setVendorNaics(vendorNaics);
		Integer salesYear1 = null;
		if (vendorInfo.getVendorMaster().getCustomerVendorSalesList() != null
				&& !vendorInfo.getVendorMaster().getCustomerVendorSalesList()
						.isEmpty()) {

			if (vendorInfo.getVendorMaster().getCustomerVendorSalesList()
					.size() != 0) {
				CustomerVendorSales sales1 = vendorInfo.getVendorMaster()
						.getCustomerVendorSalesList().get(0);
				if (null != sales1.getYearsales()) {
					vendorMasterForm.setAnnualTurnover(formatter.format(sales1
							.getYearsales()));
				}
				vendorMasterForm.setAnnualYear1(sales1.getYearnumber());
				vendorMasterForm.setAnnualTurnoverId1(sales1.getId());
				salesYear1 = sales1.getYearnumber();
			}

			if (vendorInfo.getVendorMaster().getCustomerVendorSalesList()
					.size() > 1) {
				CustomerVendorSales sales2 = vendorInfo.getVendorMaster()
						.getCustomerVendorSalesList().get(1);
				if (null != sales2.getYearsales()) {
					vendorMasterForm.setSales2(formatter.format(sales2
							.getYearsales()));
				}
				vendorMasterForm.setAnnualYear2(sales2.getYearnumber());
				vendorMasterForm.setAnnualTurnoverId2(sales2.getId());

			}
			else
			{
				vendorMasterForm.setSales2(null);
				vendorMasterForm.setAnnualYear2(null);
				vendorMasterForm.setAnnualTurnoverId2(null);
			}
			/*
			 * else { if (salesYear1 != null)
			 * vendorMasterForm.setAnnualYear2(salesYear1 - 1); }
			 */
			if (vendorInfo.getVendorMaster().getCustomerVendorSalesList()
					.size() > 2) {
				CustomerVendorSales sales3 = vendorInfo.getVendorMaster()
						.getCustomerVendorSalesList().get(2);
				if (null != sales3.getYearsales()) {
					vendorMasterForm.setSales3(formatter.format(sales3
							.getYearsales()));
				}
				vendorMasterForm.setAnnualYear3(sales3.getYearnumber());
				vendorMasterForm.setAnnualTurnoverId3(sales3.getId());

			}
			else
			{
				vendorMasterForm.setAnnualYear3(null);
				vendorMasterForm.setAnnualTurnoverId3(null);
				vendorMasterForm.setSales3(null);
			}
			/*
			 * else { if (salesYear1 != null)
			 * vendorMasterForm.setAnnualYear3(salesYear1 - 2); }
			 */

		}
		else
		{
			vendorMasterForm.setAnnualTurnover(null);
			vendorMasterForm.setAnnualYear1(null);
			vendorMasterForm.setAnnualTurnoverId1(null);
			
			vendorMasterForm.setSales2(null);
			vendorMasterForm.setAnnualYear2(null);
			vendorMasterForm.setAnnualTurnoverId2(null);
			
			vendorMasterForm.setAnnualYear3(null);
			vendorMasterForm.setAnnualTurnoverId3(null);
			vendorMasterForm.setSales3(null);
		}
		/*
		 * else { vendorMasterForm.setAnnualYear1(calendar.get(Calendar.YEAR) -
		 * 1); vendorMasterForm.setAnnualYear2(calendar.get(Calendar.YEAR) - 2);
		 * vendorMasterForm.setAnnualYear3(calendar.get(Calendar.YEAR) - 3); }
		 */

		if (vendorInfo.getVendorMaster().getCustomerVendorownerList() != null
				&& !vendorInfo.getVendorMaster().getCustomerVendorownerList()
						.isEmpty()) {
			if (vendorInfo.getVendorMaster().getCustomerVendorownerList()
					.size() != 0) {
				CustomerVendorOwner owner1 = vendorInfo.getVendorMaster()
						.getCustomerVendorownerList().get(0);
				vendorMasterForm.setOwnerEmail1(owner1.getEmail());
				vendorMasterForm.setOwnerEthnicity1(owner1
						.getOwnersethinicity());
				vendorMasterForm.setOwnerExt1(owner1.getExtension());
				vendorMasterForm
						.setOwnerGender1(owner1.getGender() != null ? owner1
								.getGender().toString() : "");
				vendorMasterForm.setOwnerMobile1(owner1.getMobile());
				vendorMasterForm.setOwnerName1(owner1.getOwnername());
				vendorMasterForm.setOwnerOwnership1(owner1
						.getOwnershippercentage() != null ? String
						.valueOf(owner1.getOwnershippercentage().intValue())
						: "");
				vendorMasterForm.setOwnerPhone1(owner1.getPhone());
				vendorMasterForm.setOwnerTitle1(owner1.getTitle());

				vendorMasterForm.setOwnerId1(owner1.getId());
			}
			if (vendorInfo.getVendorMaster().getCustomerVendorownerList()
					.size() > 1) {
				CustomerVendorOwner owner2 = vendorInfo.getVendorMaster()
						.getCustomerVendorownerList().get(1);
				vendorMasterForm.setOwnerEmail2(owner2.getEmail());
				vendorMasterForm.setOwnerEthnicity2(owner2
						.getOwnersethinicity());
				vendorMasterForm.setOwnerExt2(owner2.getExtension());
				vendorMasterForm
						.setOwnerGender2(owner2.getGender() != null ? owner2
								.getGender().toString() : "");
				vendorMasterForm.setOwnerMobile2(owner2.getMobile());
				vendorMasterForm.setOwnerName2(owner2.getOwnername());
				vendorMasterForm.setOwnerOwnership2(owner2
						.getOwnershippercentage() != null ? String
						.valueOf(owner2.getOwnershippercentage().intValue())
						: "");
				vendorMasterForm.setOwnerPhone2(owner2.getPhone());
				vendorMasterForm.setOwnerTitle2(owner2.getTitle());

				vendorMasterForm.setOwnerId2(owner2.getId());
			}
			if (vendorInfo.getVendorMaster().getCustomerVendorownerList()
					.size() > 2) {
				CustomerVendorOwner owner3 = vendorInfo.getVendorMaster()
						.getCustomerVendorownerList().get(2);
				vendorMasterForm.setOwnerEmail3(owner3.getEmail());
				vendorMasterForm.setOwnerEthnicity3(owner3
						.getOwnersethinicity());
				vendorMasterForm.setOwnerExt3(owner3.getExtension());
				vendorMasterForm
						.setOwnerGender3(owner3.getGender() != null ? owner3
								.getGender().toString() : "");
				vendorMasterForm.setOwnerMobile3(owner3.getMobile());
				vendorMasterForm.setOwnerName3(owner3.getOwnername());
				vendorMasterForm.setOwnerOwnership3(owner3
						.getOwnershippercentage() != null ? String
						.valueOf(owner3.getOwnershippercentage().intValue())
						: "");
				vendorMasterForm.setOwnerPhone3(owner3.getPhone());
				vendorMasterForm.setOwnerTitle3(owner3.getTitle());

				vendorMasterForm.setOwnerId3(owner3.getId());
			}

		}
		return vendorMasterForm;
	}

	/**
	 * 
	 * @param id
	 * @param userDetails
	 * @return
	 */
	public static List<CertifyingAgency> agenciesByClassification(Integer id,
			UserDetailsDto userDetails) {
		CURDDao<CertifyingAgency> curdDao = new CURDTemplateImpl<CertifyingAgency>();
		List<CertifyingAgency> certificateAgencies = curdDao
				.list(userDetails,
						" select new com.fg.vms.customer.model.CertifyingAgency(ca.id,ca.agencyName) "
								+ " From CertifyingAgency ca, Certificate cm,  CustomerClassificationCertifyingAgencies cca"
								+ " where cm.id = cca.certificateId and ca.id = cca.agencyId And cm.isActive=1 and ca.isActive = 1 "
								+ " And cca.isActive = 1 And cm.id = " + id);
		return certificateAgencies;
	}

	/**
	 * 
	 * @param id
	 * @param userDetails
	 * @return
	 */
	public static List<CustomerCertificateType> certificateTypesByClassification(
			Integer id, UserDetailsDto userDetails) {
		CURDDao<CustomerCertificateType> curdDao = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypesList = curdDao
				.list(userDetails,
						" select new com.fg.vms.customer.model.CustomerCertificateType(ct.id,ct.certificateTypeDesc,ct.shortDescription,"
								+ "ct.isActive) From CustomerCertificateType ct, Certificate cm,  CustomerClassificationCertificateTypes cct"
								+ " where cm.id = cct.certificateId and ct.id = cct.certificateTypeId And cm.isActive=1 and ct.isActive = 1 "
								+ " And cct.isActive = 1 And cm.id = " + id);
		return certificateTypesList;
	}

	/**
	 * 
	 * @param vendorCetrificate
	 * @return
	 */
	private static List<DiverseCertificationTypesDto> packDiverseCertificationTypes(
			List<VendorCertificate> vendorCetrificate) {

		List<DiverseCertificationTypesDto> certificationTypes = new ArrayList<DiverseCertificationTypesDto>();
		for (VendorCertificate certificate : vendorCetrificate) {
			DiverseCertificationTypesDto diverseCertification = new DiverseCertificationTypesDto();
			diverseCertification.setExpDate(CommonUtils
					.convertDateToString(certificate.getExpiryDate()));
			diverseCertification.setCertificateNumber(certificate
					.getCertificateNumber());
			diverseCertification.setEffectiveDate(CommonUtils
					.convertDateToString(certificate.getEffectiveDate()));
			diverseCertification.setDivCertAgent(certificate.getCertAgencyId()
					.getId());
			diverseCertification.setDivCertType(certificate.getCertMasterId()
					.getId());
			// diverseCertification.setCertificate(certificate
			// .getCertificate());
			diverseCertification.setId(certificate.getId());
			certificationTypes.add(diverseCertification);

		}
		return certificationTypes;
	}

	public static VendorMaster packVendorMasterEntity(
			VendorMasterForm vendorForm, Integer userId,
			VendorMaster vendorMaster) {

		// Save VendorMaster Company Information
		vendorMaster.setVendorName(vendorForm.getVendorName());
		// vendorMaster.setVendorCode(vendorForm.getVendorCode());

		// Modification needed fields for tier2 vendor creation
		// dated (05-08-2013)
		vendorMaster.setDunsNumber(vendorForm.getDunsNumber());
		vendorMaster.setTaxId(vendorForm.getTaxId());
		if (vendorForm.getNumberOfEmployees() != null
				&& !vendorForm.getNumberOfEmployees().isEmpty()) {

			vendorMaster.setNumberOfEmployees(Integer.parseInt(vendorForm
					.getNumberOfEmployees()));
		}

		vendorMaster.setAddress1(vendorForm.getAddress1());
		vendorMaster.setAddress2(vendorForm.getAddress2());
		vendorMaster.setAddress3(vendorForm.getAddress3());
		vendorMaster.setCompanyType(vendorForm.getCompanytype());
		vendorMaster.setCity(vendorForm.getCity());

		vendorMaster.setCountry(vendorForm.getCountry());

		vendorMaster.setState(vendorForm.getState());
		vendorMaster.setProvince(vendorForm.getProvince());
		vendorMaster.setPhone(vendorForm.getPhone());
		vendorMaster.setRegion(vendorForm.getRegion());
		vendorMaster.setZipCode(vendorForm.getZipcode());
		vendorMaster.setMobile(vendorForm.getMobile());
		vendorMaster.setFax(vendorForm.getFax());
		vendorMaster.setWebsite(vendorForm.getWebsite());
		vendorMaster.setEmailId(vendorForm.getEmailId());
		if (vendorForm.getYearOfEstablishment() != null
				&& !vendorForm.getYearOfEstablishment().isEmpty()) {
			vendorMaster.setYearOfEstablishment(Integer.parseInt(vendorForm
					.getYearOfEstablishment()));
		}
		vendorMaster.setDeverseSupplier(CommonUtils.getCheckboxValue(vendorForm
				.getDeverseSupplier()));
		// vendorMaster.setParentVendorId(userId);
		// vendorMaster.setPrimeNonPrimeVendor(CommonUtils.getByteValue(false));

		// vendorMaster.setApprovedBy(userId);// no need for
		// approval process
		// if (vendorForm.getMinorityPercent() != null
		// && !vendorForm.getMinorityPercent().isEmpty())
		// vendorMaster.setMinorityPercent(Double.valueOf(vendorForm
		// .getMinorityPercent()));
		// if (vendorForm.getWomenPercent() != null
		// && !vendorForm.getWomenPercent().isEmpty())
		// vendorMaster.setWomenPercent(Double.valueOf(vendorForm
		// .getWomenPercent()));

		// vendorMaster.setDiverseNotes(vendorForm.getClassificationNotes());

		vendorMaster.setVendorDescription(vendorForm.getVendorDescription());
		vendorMaster.setCompanyInformation(vendorForm.getCompanyInformation());
		vendorMaster.setStateIncorporation(vendorForm.getStateIncorporation());
		vendorMaster.setStateSalesTaxId(vendorForm.getStateSalesTaxId());
		if (vendorForm.getCompanyOwnership() != null
				&& !vendorForm.getCompanyOwnership().isEmpty()) {
			vendorMaster.setCompanyownership(Short.valueOf(vendorForm
					.getCompanyOwnership()));
		}
		vendorMaster.setBusinesstype(vendorForm.getBusinessType());

		return vendorMaster;

	}

	public static CustomerVendorAddressMaster packVendorAddress1(
			VendorMasterForm vendorForm, Integer userId,
			CustomerVendorAddressMaster addressMaster, VendorMaster vendorMaster) {

		addressMaster.setVendorId(vendorMaster);
		addressMaster.setAddressType("p");
		addressMaster.setAddress(vendorForm.getAddress1());
		addressMaster.setCity(vendorForm.getCity());
		addressMaster.setCountry(vendorForm.getCountry());
		addressMaster.setState(vendorForm.getState());
		if (null != vendorForm.getProvince()
				&& !vendorForm.getProvince().equalsIgnoreCase("Optional")) {
			addressMaster.setProvince(vendorForm.getProvince());
		}
		addressMaster.setPhone(vendorForm.getPhone());
		if (null != vendorForm.getRegion()
				&& !vendorForm.getRegion().equalsIgnoreCase("Optional")) {
			addressMaster.setRegion(vendorForm.getRegion());
		}
		addressMaster.setZipCode(vendorForm.getZipcode());
		addressMaster.setMobile(vendorForm.getMobile());
		addressMaster.setFax(vendorForm.getFax());
		addressMaster.setIsActive((byte) 1);

		return addressMaster;

	}

	public static CustomerVendorAddressMaster packVendorAddress2(
			VendorMasterForm vendorForm, Integer userId,
			CustomerVendorAddressMaster addressMaster, VendorMaster vendorMaster) {

		addressMaster.setVendorId(vendorMaster);
		addressMaster.setAddressType("m");
		addressMaster.setAddress(vendorForm.getAddress2());
		addressMaster.setCity(vendorForm.getCity2());
		addressMaster.setCountry(vendorForm.getCountry2());
		addressMaster.setState(vendorForm.getState2());
		if (null != vendorForm.getProvince2()
				&& !vendorForm.getProvince2().equalsIgnoreCase("Optional")) {
			addressMaster.setProvince(vendorForm.getProvince2());
		}
		addressMaster.setPhone(vendorForm.getPhone2());
		if (null != vendorForm.getRegion2()
				&& !vendorForm.getRegion2().equalsIgnoreCase("Optional")) {
			addressMaster.setRegion(vendorForm.getRegion2());
		}
		if (null != vendorForm.getZipcode2()
				&& !vendorForm.getZipcode2().equalsIgnoreCase("Optional")) {
			addressMaster.setZipCode(vendorForm.getZipcode2());
		}
		addressMaster.setMobile(vendorForm.getMobile2());
		addressMaster.setFax(vendorForm.getFax2());
		addressMaster.setIsActive((byte) 1);
		return addressMaster;

	}

	public static VendorContact packVendorContact(VendorMasterForm vendorForm,
			Integer userId, VendorContact vendorContact,
			VendorMaster vendorMaster) {

		vendorContact.setVendorId(vendorMaster);

		Random random = new java.util.Random();
		Integer randVal = random.nextInt();
		Encrypt encrypt = new Encrypt();
		// convert password to encrypted password
		String encyppassword = null;
		try {
			// if (vendorForm.getFirstName() != null
			// && !vendorForm.getFirstName().isEmpty()) {
			if (vendorForm.getLoginpassword() != null
					&& !vendorForm.getLoginpassword().isEmpty()) {
				encyppassword = encrypt.encryptText(Integer.toString(randVal)
						+ "", vendorForm.getLoginpassword());
			}
			vendorContact.setFirstName(vendorForm.getFirstName());
			vendorContact.setLastName(vendorForm.getLastName());
			vendorContact.setPhoneNumber(vendorForm.getContactPhone());
			vendorContact.setDesignation(vendorForm.getDesignation());
			vendorContact.setMobile(vendorForm.getContactMobile());
			vendorContact.setFax(vendorForm.getContactFax());
			vendorContact.setEmailId(vendorForm.getContanctEmail());
			vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(true));
			vendorContact.setIsBusinessContact(CommonUtils.getByteValue(false));
			vendorContact.setIsPreparer(CommonUtils.getByteValue(true));
			vendorContact.setIsPrimaryContact((byte) 1);
			// vendorContact.setLoginDisplayName(vendorForm
			// .getLoginDisplayName());
			// vendorContact.setLoginId(vendorForm.getLoginId());
			vendorContact.setPassword(encyppassword);
			vendorContact.setKeyValue(randVal);
			vendorContact.setIsActive((byte) 1);
			vendorContact.setCreatedBy(userId);
			vendorContact.setCreatedOn(new Date());
			vendorContact.setModifiedBy(userId);
			vendorContact.setModifiedOn(new Date());
			// }
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return vendorContact;
	}

	public static VendorCertificate packVendorCertificate(
			VendorMasterForm vendorForm, Integer userId,
			VendorCertificate vendorCertificate, VendorMaster vendorMaster,
			Certificate certMasterId, CertifyingAgency certAgencyId,
			DiverseCertificationTypesDto certificate) {

		if (certificate != null && certificate.getId() != null) {
			vendorCertificate.setId(certificate.getId());
		}
		vendorCertificate.setCertMasterId(certMasterId);
		vendorCertificate.setCertAgencyId(certAgencyId);
		vendorCertificate.setVendorId(vendorMaster);
		vendorCertificate.setCertificateNumber(certificate
				.getCertificateNumber());
		vendorCertificate.setCertificateType(certificate.getCertType());
		if (null != certificate.getEffectiveDate()
				&& !certificate.getEffectiveDate().isEmpty()
				&& !certificate.getEffectiveDate().equalsIgnoreCase(
						"Please click to select date")) {
			vendorCertificate.setEffectiveDate(CommonUtils
					.dateConvertValue(certificate.getEffectiveDate()));
		}
		if (null != certificate.getExpDate()
				&& !certificate.getExpDate().isEmpty()
				&& !certificate.getExpDate().equalsIgnoreCase(
						"Please click to select date")) {
			vendorCertificate.setExpiryDate(CommonUtils
					.dateConvertValue(certificate.getExpDate()));
		}
		vendorCertificate.setDiverseQuality(CommonUtils
				.getCheckboxValue(vendorForm.getDeverseSupplier()));
		if (certificate.getEthnicity() != null
				&& !certificate.getEthnicity().isEmpty()) {
			vendorCertificate.setEthnicityID(Integer.parseInt(certificate
					.getEthnicity()));
		}

		if (certificate.getCertificate() != null
				&& certificate.getCertificate().getFileSize() != 0) {

			vendorCertificate.setContent(CommonUtils
					.convertFormFileToBlob(certificate.getCertificate()));
			vendorCertificate.setContentType(certificate.getCertificate()
					.getContentType());
			vendorCertificate.setFilename(certificate.getCertificate()
					.getFileName());
		}
		return vendorCertificate;

	}

	// public static CustomerVendorInsurance packVendorInsurance(
	// VendorMasterForm vendorForm, Integer userId,
	// CustomerVendorInsurance insurance, VendorMaster vendorMaster) {
	// insurance.setVendorId(vendorMaster);
	// if (vendorForm.getInsurance() != null
	// && !vendorForm.getInsurance().isEmpty()) {
	// insurance.setInsurance(vendorForm.getInsurance());
	// }
	// if (vendorForm.getLimit() != null && !vendorForm.getLimit().isEmpty()) {
	// insurance.setLimit(vendorForm.getLimit());
	// }
	// if (vendorForm.getProvider() != null
	// && !vendorForm.getProvider().isEmpty()) {
	// insurance.setProvider(vendorForm.getProvider());
	// }
	// if (vendorForm.getCarrierName() != null
	// && !vendorForm.getCarrierName().isEmpty()) {
	// insurance.setCarrierName(vendorForm.getCarrierName());
	// }
	// if (vendorForm.getExpirationDate() != null
	// && !vendorForm.getExpirationDate().isEmpty()) {
	// insurance.setExpirationDate(CommonUtils.dateConvertValue(vendorForm
	// .getExpirationDate()));
	// }
	// if (vendorForm.getPolicyNumber() != null
	// && !vendorForm.getPolicyNumber().isEmpty()) {
	// insurance.setPolicyNumber(vendorForm.getPolicyNumber());
	// }
	// if (vendorForm.getAdditionalInsured() != null
	// && !vendorForm.getAdditionalInsured().isEmpty()) {
	// insurance.setAdditionalInsured(vendorForm.getAdditionalInsured());
	// }
	// if (vendorForm.getAgent() != null && !vendorForm.getAgent().isEmpty()) {
	// insurance.setAgent(vendorForm.getAgent());
	// }
	// if (vendorForm.getFaxInsurance() != null
	// && !vendorForm.getFaxInsurance().isEmpty()) {
	// insurance.setFaxInsurance(vendorForm.getFaxInsurance());
	// }
	// if (vendorForm.getPhoneInsurance() != null
	// && !vendorForm.getPhoneInsurance().isEmpty()) {
	// insurance.setPhoneInsurance(vendorForm.getPhoneInsurance());
	// }
	// if (vendorForm.getExtInsurance() != null
	// && !vendorForm.getExtInsurance().isEmpty()) {
	// insurance.setExtension(vendorForm.getExtInsurance());
	// }
	// if (vendorForm.getStatus() != null && !vendorForm.getStatus().isEmpty())
	// {
	// insurance.setStatus(vendorForm.getStatus());
	// }
	// insurance.setCreatedBy(userId);
	// insurance.setCreatedOn(new Date());
	//
	// if (vendorForm.getInsuranceFile() != null
	// && vendorForm.getInsuranceFile().getFileSize() != 0) {
	// String filePath = Constants
	// .getString(Constants.UPLOAD_INSURANCE_PATH)
	// + "/"
	// + vendorMaster.getId();
	//
	// File theDir = new File(filePath);
	// // if the directory does not exist, create it
	// if (!theDir.exists()) {
	// theDir.mkdir();
	// }
	// // Saving Image to appropriate path
	// CommonUtils.saveImage(vendorForm.getInsuranceFile(), filePath);
	// insurance.setFileName(vendorForm.getInsuranceFile().getFileName());
	// insurance.setPathName(filePath);
	// }
	// return insurance;
	// }

	public static CustomerVendorOwner packVendorOwner1(
			VendorMasterForm vendorForm, Integer userId,
			CustomerVendorOwner owner, VendorMaster vendorMaster) {

		owner.setOwnername(vendorForm.getOwnerName1());
		owner.setOwnersethinicity(vendorForm.getOwnerEthnicity1());
		if (vendorForm.getOwnerOwnership1() != null
				&& !vendorForm.getOwnerOwnership1().isEmpty()) {
			if (vendorForm.getCompanyOwnership().equals("1")) {
				owner.setOwnershippercentage(null);
			} else {
				owner.setOwnershippercentage(new BigDecimal(vendorForm
						.getOwnerOwnership1()));
			}
		}

		owner.setEmail(vendorForm.getOwnerEmail1());
		if (!vendorForm.getOwnerExt1().equalsIgnoreCase("Optional")) {
			owner.setExtension(vendorForm.getOwnerExt1());
		}
		owner.setGender(vendorForm.getOwnerGender1());
		owner.setMobile(vendorForm.getOwnerMobile1());
		owner.setPhone(vendorForm.getOwnerPhone1());
		owner.setTitle(vendorForm.getOwnerTitle1());
		owner.setVendorid(vendorMaster);
		owner.setIsactive((short) 1);
		return owner;

	}

	public static CustomerVendorOwner packVendorOwner2(
			VendorMasterForm vendorForm, Integer userId,
			CustomerVendorOwner owner, VendorMaster vendorMaster) {

		owner.setOwnersethinicity(vendorForm.getOwnerEthnicity2());
		if (vendorForm.getOwnerOwnership2() != null
				&& !vendorForm.getOwnerOwnership2().isEmpty()
				&& !vendorForm.getOwnerOwnership2()
						.equalsIgnoreCase("Optional")) {
			if (vendorForm.getCompanyOwnership().equals("1")) {
				owner.setOwnershippercentage(null);
			} else {
				owner.setOwnershippercentage(new BigDecimal(vendorForm
						.getOwnerOwnership2()));
			}
		}
		if (!vendorForm.getOwnerName2().equalsIgnoreCase("Optional")) {
			owner.setOwnername(vendorForm.getOwnerName2());
		}
		if (!vendorForm.getOwnerEmail2().equalsIgnoreCase("Optional")) {
			owner.setEmail(vendorForm.getOwnerEmail2());
		}
		if (!vendorForm.getOwnerExt2().equalsIgnoreCase("Optional")) {
			owner.setExtension(vendorForm.getOwnerExt2());
		}
		if (!vendorForm.getOwnerTitle2().equalsIgnoreCase("Optional")) {
			owner.setTitle(vendorForm.getOwnerTitle2());
		}
		owner.setGender(vendorForm.getOwnerGender2());
		owner.setMobile(vendorForm.getOwnerMobile2());
		owner.setPhone(vendorForm.getOwnerPhone2());
		owner.setVendorid(vendorMaster);
		owner.setIsactive((short) 1);
		return owner;

	}

	public static CustomerVendorOwner packVendorOwner3(
			VendorMasterForm vendorForm, Integer userId,
			CustomerVendorOwner owner, VendorMaster vendorMaster) {

		owner.setOwnersethinicity(vendorForm.getOwnerEthnicity3());
		if (vendorForm.getOwnerOwnership3() != null
				&& !vendorForm.getOwnerOwnership3().isEmpty()
				&& !vendorForm.getOwnerOwnership3()
						.equalsIgnoreCase("Optional")) {
			if (vendorForm.getCompanyOwnership().equals("1")) {
				owner.setOwnershippercentage(null);
			} else {
				owner.setOwnershippercentage(new BigDecimal(vendorForm
						.getOwnerOwnership3()));
			}
		}
		if (!vendorForm.getOwnerName3().equalsIgnoreCase("Optional")) {
			owner.setOwnername(vendorForm.getOwnerName3());
		}
		if (!vendorForm.getOwnerEmail3().equalsIgnoreCase("Optional")) {
			owner.setEmail(vendorForm.getOwnerEmail3());
		}
		if (!vendorForm.getOwnerExt3().equalsIgnoreCase("Optional")) {
			owner.setExtension(vendorForm.getOwnerExt3());
		}
		if (!vendorForm.getOwnerTitle3().equalsIgnoreCase("Optional")) {
			owner.setTitle(vendorForm.getOwnerTitle3());
		}
		owner.setGender(vendorForm.getOwnerGender3());
		owner.setMobile(vendorForm.getOwnerMobile3());
		owner.setPhone(vendorForm.getOwnerPhone3());
		owner.setVendorid(vendorMaster);
		owner.setIsactive((short) 1);
		return owner;

	}

	/**
	 * This method will set the values in business biography and safety.
	 * 
	 * @param vendorMasterForm
	 * @param vendorMaster
	 * @return
	 */
	public static VendorMasterForm packBioGraphySafety(
			VendorMasterForm vendorMasterForm, Integer vendorId,
			UserDetailsDto appDetails) {

		VendorDao vendorDao = new VendorDaoImpl();
		String type = "b";
		List<CustomerVendorbusinessbiographysafety> business = vendorDao
				.getBiography(vendorId, appDetails, type);

		String[] businessValue = new String[100];
		if (business != null && business.size() != 0) {
			for (CustomerVendorbusinessbiographysafety b : business) {

				switch (b.getQuestionnumber()) {
				case 1:
					if (b.getSequence() == 1) {
						if (b.getAnswer() != null) {
							vendorMasterForm.setExpOilGas(Byte.parseByte(b
									.getAnswer()));
							if (b.getAnswer().equalsIgnoreCase("1")) {
								businessValue[0] = "1";
							} else {
								businessValue[1] = "0";
							}
						}
					}
					
					if (b.getSequence() == 2) {
						vendorMasterForm.setWorkPlace(b.getAnswer());
					}
					if (b.getSequence() == 3) {
						vendorMasterForm.setListClients(b.getAnswer());
					}
					break;
				case 2:
					if (b.getSequence() == 1) {
						if (b.getAnswer() != null) {
							vendorMasterForm.setIsBpSupplier(Byte.parseByte(b
									.getAnswer()));
							if (b.getAnswer().equalsIgnoreCase("1")) {
								businessValue[2] = "1";
							} else {
								businessValue[3] = "0";
							}
						}
					} else if (b.getSequence() == 2) {
						vendorMasterForm.setBpContact(b.getAnswer());
					} else if (b.getSequence() == 3) {
						vendorMasterForm.setBpDivision(b.getAnswer());
					} else if (b.getSequence() == 4) {
						vendorMasterForm.setBpContactPhone(b.getAnswer());
					} else if (b.getSequence() == 5) {
						vendorMasterForm.setBpContactPhoneExt(b.getAnswer());
					}else if(b.getSequence() == 6){
						vendorMasterForm.setTypeBpSupplier(b.getAnswer());
					}else if(b.getSequence() == 7){
						vendorMasterForm.setSpecificwork(b.getAnswer());
					}else if(b.getSequence() == 8){
						vendorMasterForm.setWorkstartdate(b.getAnswer());
					}else if(b.getSequence() == 9){
						vendorMasterForm.setContractValue(b.getAnswer());
					}else if(b.getSequence() == 10){
						vendorMasterForm.setSiteLocation(b.getAnswer());
					}
					break;

				case 3:
					if (b.getSequence() == 1) {
						if (b.getAnswer() != null) {
							vendorMasterForm.setIsCurrentBpSupplier(Byte
									.parseByte(b.getAnswer()));
							if (b.getAnswer().equalsIgnoreCase("1")) {
								businessValue[4] = "1";
							} else {
								businessValue[5] = "0";
							}
						}
					} else if (b.getSequence() == 2) {
						vendorMasterForm
								.setCurrentBpSupplierDesc(b.getAnswer());
					}
					break;
				case 4:
					if (b.getSequence() == 1) {
						if (b.getAnswer() != null) {
							vendorMasterForm.setIsFortune(Byte.parseByte(b
									.getAnswer()));
							if (b.getAnswer().equalsIgnoreCase("1")) {
								businessValue[6] = "1";
							} else {
								businessValue[7] = "0";
							}
						}
					} else if (b.getSequence() == 2) {
						vendorMasterForm.setFortuneList(b.getAnswer());
					}
					break;
				case 5:
					if (b.getSequence() == 1) {
						if (b.getAnswer() != null) {
							vendorMasterForm.setIsUnionWorkforce(Byte
									.parseByte(b.getAnswer()));
							if (b.getAnswer().equalsIgnoreCase("1")) {
								businessValue[8] = "1";
							} else {
								businessValue[9] = "0";
							}
						}
					}

				case 6:
					if (b.getSequence() == 1) {
						if (b.getAnswer() != null) {
							vendorMasterForm.setIsNonUnionWorkforce(Byte
									.parseByte(b.getAnswer()));
							if (b.getAnswer().equalsIgnoreCase("1")) {
								businessValue[10] = "1";
							} else {
								businessValue[11] = "0";
							}
						}
					}
					break;
				default:
					break;
				}
			}
		}

		String type1 = "s";
		List<CustomerVendorbusinessbiographysafety> safety = vendorDao
				.getBiography(vendorId, appDetails, type1);

		if (safety != null && safety.size() != 0) {
			for (CustomerVendorbusinessbiographysafety safe : safety) {

				switch (safe.getQuestionnumber()) {

				case 7:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsPicsCertified(Byte
									.parseByte(safe.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[12] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[13] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[14] = "2";
							}
						}
					}else if(safe.getSequence() == 2){
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsNetworldCertified(Byte
									.parseByte(safe.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[36] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[37] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[38] = "2";
							}
						}
						
					}
					break;
					
//				case 8:
//					if (safe.getSequence() == 1) {
//						if (safe.getAnswer() != null) {
//							vendorMasterForm.setIsNetworldCertified(Byte
//									.parseByte(safe.getAnswer()));
//							if (safe.getAnswer().equalsIgnoreCase("1")) {
//								businessValue[36] = "1";
//							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
//								businessValue[37] = "0";
//							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
//								businessValue[38] = "2";
//							}
//						}
//					}
//					break;

				case 8:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsIso9000Certified(Byte
									.parseByte(safe.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[15] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[16] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[17] = "2";
							}
						}
					}
					break;
				case 9:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsIso14000Certified(Byte
									.parseByte(safe.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[18] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[19] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[20] = "2";
							}
						}
					}
					break;
				case 10:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsOshaRecd(Byte.parseByte(safe
									.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[21] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[22] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[23] = "2";
							}
						}
					}
					break;
				case 11:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsOshaAvg(Byte.parseByte(safe
									.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[24] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[25] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[26] = "2";
							}
						}
						vendorMasterForm.setIsOshaAvgTxt(safe.getDescription());
					}
					break;
				case 12:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsEmr(Byte.parseByte(safe
									.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[27] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[28] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[29] = "2";
							}
						}
						vendorMasterForm.setIsEmrTxt(safe.getDescription());
					}
					break;
				case 13:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							vendorMasterForm.setIsOshaAgree(Byte.parseByte(safe
									.getAnswer()));
							if (safe.getAnswer().equalsIgnoreCase("1")) {
								businessValue[30] = "1";
							} else if (safe.getAnswer().equalsIgnoreCase("0")) {
								businessValue[31] = "0";
							} else if (safe.getAnswer().equalsIgnoreCase("2")) {
								businessValue[32] = "2";
							}
						}
					}
					break;
				case 14:
					if (safe.getSequence() == 1) {
						if (safe.getAnswer() != null) {
							
							if (safe.getAnswer().toString().equals("N/A")) {
								businessValue[35] = "2";
							} else {
								vendorMasterForm.setIsNib(Byte.parseByte(safe
										.getAnswer()));
								if (safe.getAnswer().equalsIgnoreCase("1")) {
									businessValue[33] = "1";
								} else if (safe.getAnswer().equalsIgnoreCase(
										"0")) {
									businessValue[34] = "0";
								} else if (safe.getAnswer().equalsIgnoreCase(
										"2")) {
									businessValue[35] = "2";
								}
							}
						}
					}
					break;
				case 15:
					if (safe.getSequence() == 1) {

						vendorMasterForm.setJobsDesc(safe.getAnswer());
					}
					break;
				case 16:
					if (safe.getSequence() == 1) {
						vendorMasterForm.setCustLocation(safe.getAnswer());
					}
					break;
				case 17:
					if (safe.getSequence() == 1) {
						vendorMasterForm.setCustContact(safe.getAnswer());
					}
					break;
				case 18:
					if (safe.getSequence() == 1) {

						vendorMasterForm.setTelephone(safe.getAnswer());
					}
					break;
				case 19:
					if (safe.getSequence() == 1) {
						vendorMasterForm.setWorkType(safe.getAnswer());
					}
					break;
				case 20:
					if (safe.getSequence() == 1) {
						java.text.NumberFormat formatter = java.text.NumberFormat
								.getCurrencyInstance(java.util.Locale.US);
						if (null != safe.getAnswer().toString()
								&& !safe.getAnswer().isEmpty()) {
							if (CommonUtils.isNumber(safe.getAnswer())) {
								vendorMasterForm.setSize(safe.getAnswer());
							} else {
								vendorMasterForm.setSize(formatter
										.format(Double.parseDouble(safe
												.getAnswer())));
							}
						}
					}
					break;
				default:
					break;
				}
			}
		}
		vendorMasterForm.setBussines(businessValue);
		return vendorMasterForm;
	}

	/**
	 * Method used for setting reference details to vendor
	 * 
	 * @param vendorMasterForm
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public static VendorMasterForm packVendorReference(
			VendorMasterForm vendorMasterForm,
			List<CustomerVendorreference> vendorreferences,
			UserDetailsDto userDetails, Integer countryId) {

		if (vendorreferences != null && vendorreferences.size() != 0) {
			CURDDao<State> dao = new CURDTemplateImpl<State>();
			for (int i = 0; i < vendorreferences.size(); i++) {
				if (vendorreferences.get(i).getReferenceId() == 1) {
					CustomerVendorreference vendorreference = vendorreferences
							.get(i);
					vendorMasterForm.setReferenceCompanyName1(vendorreference
							.getReferenceCompanyName());
					vendorMasterForm.setReferenceName1(vendorreference
							.getReferenceName());
					vendorMasterForm.setReferenceAddress1(vendorreference
							.getReferenceAddress());
					vendorMasterForm.setReferenceMailId1(vendorreference
							.getReferenceEmailId());
					vendorMasterForm.setReferenceMobile1(vendorreference
							.getReferenceMobile());
					vendorMasterForm.setReferencePhone1(vendorreference
							.getReferencePhone());
					vendorMasterForm.setReferenceCity1(vendorreference
							.getReferenceCity());
					vendorMasterForm.setReferenceZip1(vendorreference
							.getReferenceZip());
					vendorMasterForm.setReferenceCountry1(vendorreference
							.getReferenceCountry());
					vendorMasterForm.setReferenceState1(vendorreference
							.getReferenceState());
					vendorMasterForm.setReferenceProvince1(vendorreference
							.getReferenceProvince());
					vendorMasterForm.setExtension1(vendorreference
							.getReferenceExtension());
					if(null != vendorreference.getReferenceCountry()) {
						List<State> refStates1 = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								if(countryId != null)
									refStates1 = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								refStates1 = dao.list(userDetails, " From State where countryid=" + vendorreference.getReferenceCountry() + " and isactive=1 order by statename");
							}
						}
						vendorMasterForm.setRefStates1(refStates1);
					}					
				} else if (vendorreferences.get(i).getReferenceId() == 2) {
					CustomerVendorreference vendorreference = vendorreferences
							.get(i);
					vendorMasterForm.setReferenceCompanyName2(vendorreference
							.getReferenceCompanyName());
					vendorMasterForm.setReferenceName2(vendorreference
							.getReferenceName());
					vendorMasterForm.setReferenceAddress2(vendorreference
							.getReferenceAddress());
					vendorMasterForm.setReferenceMailId2(vendorreference
							.getReferenceEmailId());
					vendorMasterForm.setReferenceMobile2(vendorreference
							.getReferenceMobile());
					vendorMasterForm.setReferencePhone2(vendorreference
							.getReferencePhone());
					vendorMasterForm.setReferenceCity2(vendorreference
							.getReferenceCity());
					vendorMasterForm.setReferenceZip2(vendorreference
							.getReferenceZip());
					vendorMasterForm.setReferenceCountry2(vendorreference
							.getReferenceCountry());
					vendorMasterForm.setReferenceState2(vendorreference
							.getReferenceState());
					vendorMasterForm.setReferenceProvince2(vendorreference
							.getReferenceProvince());
					vendorMasterForm.setExtension2(vendorreference
							.getReferenceExtension());
					if(null != vendorreference.getReferenceCountry()) {
						List<State> refStates2 = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								if(countryId != null)
									refStates2 = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								refStates2 = dao.list(userDetails, " From State where countryid=" + vendorreference.getReferenceCountry() + " and isactive=1 order by statename");
							}
						}
						vendorMasterForm.setRefStates2(refStates2);
					}					
				} else {
					CustomerVendorreference vendorreference = vendorreferences
							.get(i);
					vendorMasterForm.setReferenceCompanyName3(vendorreference
							.getReferenceCompanyName());
					vendorMasterForm.setReferenceName3(vendorreference
							.getReferenceName());
					vendorMasterForm.setReferenceAddress3(vendorreference
							.getReferenceAddress());
					vendorMasterForm.setReferenceMailId3(vendorreference
							.getReferenceEmailId());
					vendorMasterForm.setReferenceMobile3(vendorreference
							.getReferenceMobile());
					vendorMasterForm.setReferencePhone3(vendorreference
							.getReferencePhone());
					vendorMasterForm.setReferenceCity3(vendorreference
							.getReferenceCity());
					vendorMasterForm.setReferenceZip3(vendorreference
							.getReferenceZip());
					vendorMasterForm.setReferenceCountry3(vendorreference
							.getReferenceCountry());
					vendorMasterForm.setReferenceState3(vendorreference
							.getReferenceState());
					vendorMasterForm.setReferenceProvince3(vendorreference
							.getReferenceProvince());
					vendorMasterForm.setExtension3(vendorreference
							.getReferenceExtension());
					if(null != vendorreference.getReferenceCountry()) {
						List<State> refStates3 = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								if(countryId != null)
									refStates3 = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								refStates3 = dao.list(userDetails, " From State where countryid=" + vendorreference.getReferenceCountry() + " and isactive=1 order by statename");
							}
						}
						vendorMasterForm.setRefStates3(refStates3);
					}					
				}
			}
		}

		return vendorMasterForm;
	}

	/**
	 * Method to do save business biography.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param vendorMaster
	 * @param session
	 */
	public static void saveBusinessBiography(VendorMasterForm vendorForm,
			Integer userId, VendorMaster vendorMaster, Session session) {
		if (vendorForm.getExpOilGas() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(1);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getExpOilGas()
					.toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		
		if (vendorForm.getWorkPlace() != null
				&& !vendorForm.getWorkPlace().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(1);
			vendorbusinessbiographysafety.setSequence(2);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety
					.setAnswer(vendorForm.getWorkPlace());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
			
		if (vendorForm.getListClients() != null
				&& !vendorForm.getListClients().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(1);
			vendorbusinessbiographysafety.setSequence(3);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety
					.setAnswer(vendorForm.getListClients());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		

		if (vendorForm.getIsBpSupplier() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsBpSupplier().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getBpContact() != null
				&& !vendorForm.getBpContact().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(2);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getBpContact());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getBpDivision() != null
				&& !vendorForm.getBpDivision().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(3);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getBpDivision());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getBpContactPhone() != null
				&& !vendorForm.getBpContactPhone().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(4);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getBpContactPhone());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getBpContactPhoneExt() != null
				&& !vendorForm.getBpContactPhoneExt().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(5);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getBpContactPhoneExt());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getTypeBpSupplier() != null
				&& !vendorForm.getTypeBpSupplier().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(6);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getTypeBpSupplier());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		
	if (vendorForm.getSpecificwork() != null
				&& !vendorForm.getSpecificwork().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(7);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getSpecificwork());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		
		if (vendorForm.getWorkstartdate() != null
				&& !vendorForm.getWorkstartdate().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(8);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getWorkstartdate());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		
		if (vendorForm.getContractValue() != null
				&& !vendorForm.getContractValue().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(9);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getContractValue());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		
		if (vendorForm.getSiteLocation() != null
				&& !vendorForm.getSiteLocation().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(2);
			vendorbusinessbiographysafety.setSequence(10);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getSiteLocation());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		/*			if (vendorForm.getIsCurrentBpSupplier() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(3);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsCurrentBpSupplier().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getCurrentBpSupplierDesc() != null
				&& !vendorForm.getCurrentBpSupplierDesc().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(3);
			vendorbusinessbiographysafety.setSequence(2);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getCurrentBpSupplierDesc());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}*/

		if (vendorForm.getIsFortune() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(4);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getIsFortune()
					.toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getFortuneList() != null
				&& !vendorForm.getFortuneList().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(4);
			vendorbusinessbiographysafety.setSequence(2);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety
					.setAnswer(vendorForm.getFortuneList());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsUnionWorkforce() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(5);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsUnionWorkforce().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsNonUnionWorkforce() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("b");
			vendorbusinessbiographysafety.setQuestionnumber(6);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsNonUnionWorkforce().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsPicsCertified() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(7);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsPicsCertified().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
		
		if (vendorForm.getIsNetworldCertified() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(7);
			vendorbusinessbiographysafety.setSequence(2);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsNetworldCertified().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsIso9000Certified() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(8);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsIso9000Certified().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsIso14000Certified() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(9);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getIsIso14000Certified().toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsOshaRecd() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(10);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getIsOshaRecd()
					.toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsOshaAvg() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(11);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getIsOshaAvg()
					.toString());
			vendorbusinessbiographysafety.setSetdescription("y");
			vendorbusinessbiographysafety.setDescription(vendorForm
					.getIsOshaAvgTxt());
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsEmr() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(12);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getIsEmr()
					.toString());
			vendorbusinessbiographysafety.setSetdescription("");
			vendorbusinessbiographysafety.setDescription(vendorForm
					.getIsEmrTxt());
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsOshaAgree() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(13);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getIsOshaAgree()
					.toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getIsNib() != null) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(14);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("radio");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getIsNib()
					.toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getJobsDesc() != null
				&& !vendorForm.getJobsDesc().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(15);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getJobsDesc());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getCustLocation() != null
				&& !vendorForm.getCustLocation().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(16);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm
					.getCustLocation());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getCustContact() != null
				&& !vendorForm.getCustContact().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(17);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety
					.setAnswer(vendorForm.getCustContact());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getTelephone() != null
				&& !vendorForm.getTelephone().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(18);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getTelephone());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getWorkType() != null
				&& !vendorForm.getWorkType().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(19);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(vendorForm.getWorkType());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}

		if (vendorForm.getSize() != null && !vendorForm.getSize().isEmpty()) {
			CustomerVendorbusinessbiographysafety vendorbusinessbiographysafety = new CustomerVendorbusinessbiographysafety();
			vendorbusinessbiographysafety.setVendorid(vendorMaster);
			vendorbusinessbiographysafety.setBusinesssafety("s");
			vendorbusinessbiographysafety.setQuestionnumber(20);
			vendorbusinessbiographysafety.setSequence(1);
			vendorbusinessbiographysafety.setAnswertype("text");
			vendorbusinessbiographysafety.setAnswer(CommonUtils.deformatMoney(
					vendorForm.getSize()).toString());
			vendorbusinessbiographysafety.setSetdescription(null);
			vendorbusinessbiographysafety.setDescription(null);
			vendorbusinessbiographysafety.setIsactive(1);
			vendorbusinessbiographysafety.setCreatedby(userId);
			vendorbusinessbiographysafety.setCreatedon(new Date());
			session.save(vendorbusinessbiographysafety);
		}
	}

	/**
	 * Method to do save references.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param vendorMaster
	 * @param session
	 */
	public static void saveReferences(VendorMasterForm vendorForm,
			Integer userId, VendorMaster vendorMaster, Session session) {

		// For set the values in reference 1

		if (vendorForm.getReferenceName1() != null
				&& !vendorForm.getReferenceName1().isEmpty()) {
			CustomerVendorreference vendorreference = new CustomerVendorreference();

			if (vendorForm.getReferenceName1() != null
					&& !vendorForm.getReferenceName1().isEmpty()) {
				vendorreference
						.setReferenceName(vendorForm.getReferenceName1());
				vendorreference.setReferenceId(1);
				vendorreference.setVendorId(vendorMaster);
			}

			if (vendorForm.getReferenceCompanyName1() != null
					&& !vendorForm.getReferenceName1().isEmpty()) {
				vendorreference.setReferenceCompanyName(vendorForm
						.getReferenceCompanyName1());

			}
			if (vendorForm.getReferenceAddress1() != null
					&& !vendorForm.getReferenceAddress1().isEmpty()) {
				vendorreference.setReferenceAddress(vendorForm
						.getReferenceAddress1());
			}
			if (vendorForm.getReferenceMailId1() != null
					&& !vendorForm.getReferenceMailId1().isEmpty()) {
				vendorreference.setReferenceEmailId(vendorForm
						.getReferenceMailId1());
			}
			if (vendorForm.getReferenceMobile1() != null
					&& !vendorForm.getReferenceMobile1().isEmpty()) {
				vendorreference.setReferenceMobile(vendorForm
						.getReferenceMobile1());
			}
			if (vendorForm.getReferencePhone1() != null
					&& !vendorForm.getReferencePhone1().isEmpty()) {
				vendorreference.setReferencePhone(vendorForm
						.getReferencePhone1());
			}
			if (vendorForm.getExtension1() != null
					&& !vendorForm.getExtension1().isEmpty()
					&& !vendorForm.getExtension1().equalsIgnoreCase("Optional")) {
				vendorreference.setReferenceExtension(vendorForm
						.getExtension1());
			}
			if (vendorForm.getReferenceCity1() != null
					&& !vendorForm.getReferenceCity1().isEmpty()) {
				vendorreference
						.setReferenceCity(vendorForm.getReferenceCity1());
			}
			if (vendorForm.getReferenceZip1() != null
					&& !vendorForm.getReferenceZip1().isEmpty()) {
				vendorreference.setReferenceZip(vendorForm.getReferenceZip1());
			}
			if (vendorForm.getReferenceState1() != null
					&& !vendorForm.getReferenceState1().isEmpty()) {
				vendorreference.setReferenceState(vendorForm
						.getReferenceState1());
			}
			if (vendorForm.getReferenceCountry1() != null
					&& !vendorForm.getReferenceCountry1().isEmpty()) {
				vendorreference.setReferenceCountry(vendorForm
						.getReferenceCountry1());
			}
			if (vendorForm.getReferenceProvince1() != null
					&& !vendorForm.getReferenceProvince1().isEmpty()) {
				vendorreference.setReferenceProvince(vendorForm
						.getReferenceProvince1());
			}
			vendorreference.setCreatedBy(userId);
			vendorreference.setCreatedOn(new Date());
			session.save(vendorreference);
		}

		// For set the values in reference 2
		if (vendorForm.getReferenceName2() != null
				&& !vendorForm.getReferenceName2().isEmpty()
				&& !vendorForm.getReferenceName2().equalsIgnoreCase("Optional")) {
			CustomerVendorreference vendorreference = new CustomerVendorreference();

			if (vendorForm.getReferenceName2() != null
					&& !vendorForm.getReferenceName2().isEmpty()) {
				vendorreference
						.setReferenceName(vendorForm.getReferenceName2());
				vendorreference.setReferenceId(2);
				vendorreference.setVendorId(vendorMaster);
			}
			if (vendorForm.getReferenceCompanyName2() != null
					&& !vendorForm.getReferenceCompanyName2().isEmpty()) {
				vendorreference.setReferenceCompanyName(vendorForm
						.getReferenceCompanyName2());
			}

			if (vendorForm.getReferenceAddress2() != null
					&& !vendorForm.getReferenceAddress2().isEmpty()) {
				vendorreference.setReferenceAddress(vendorForm
						.getReferenceAddress2());
			}
			if (vendorForm.getReferenceMailId2() != null
					&& !vendorForm.getReferenceMailId2().isEmpty()
					&& !vendorForm.getReferenceMailId2().equalsIgnoreCase(
							"Optional")) {
				vendorreference.setReferenceEmailId(vendorForm
						.getReferenceMailId2());
			}
			if (vendorForm.getReferenceMobile2() != null
					&& !vendorForm.getReferenceMobile2().isEmpty()) {
				vendorreference.setReferenceMobile(vendorForm
						.getReferenceMobile2());
			}
			if (vendorForm.getReferencePhone2() != null
					&& !vendorForm.getReferencePhone2().isEmpty()) {
				vendorreference.setReferencePhone(vendorForm
						.getReferencePhone2());
			}
			if (vendorForm.getExtension2() != null
					&& !vendorForm.getExtension2().isEmpty()
					&& !vendorForm.getExtension2().equalsIgnoreCase("Optional")) {
				vendorreference.setReferenceExtension(vendorForm
						.getExtension2());
			}
			if (vendorForm.getReferenceCity2() != null
					&& !vendorForm.getReferenceCity2().isEmpty()
					&& !vendorForm.getReferenceCity2().equalsIgnoreCase(
							"Optional")) {
				vendorreference
						.setReferenceCity(vendorForm.getReferenceCity2());
			}
			if (vendorForm.getReferenceZip2() != null
					&& !vendorForm.getReferenceZip2().isEmpty()
					&& !vendorForm.getReferenceZip2().equalsIgnoreCase(
							"Optional")) {
				vendorreference.setReferenceZip(vendorForm.getReferenceZip2());
			}
			if (vendorForm.getReferenceState2() != null
					&& !vendorForm.getReferenceState2().isEmpty()) {
				vendorreference.setReferenceState(vendorForm
						.getReferenceState2());
			}
			if (vendorForm.getReferenceCountry2() != null
					&& !vendorForm.getReferenceCountry2().isEmpty()) {
				vendorreference.setReferenceCountry(vendorForm
						.getReferenceCountry2());
			}
			if (vendorForm.getReferenceProvince2() != null
					&& !vendorForm.getReferenceProvince2().isEmpty()
					&& !vendorForm.getReferenceProvince2().equalsIgnoreCase(
							"Optional")) {
				vendorreference.setReferenceProvince(vendorForm
						.getReferenceProvince2());
			}
			vendorreference.setCreatedBy(userId);
			vendorreference.setCreatedOn(new Date());
			session.save(vendorreference);
		}

		// For set the values in reference 3
		if (vendorForm.getReferenceName3() != null
				&& !vendorForm.getReferenceName3().isEmpty()
				&& !vendorForm.getReferenceName3().equalsIgnoreCase("Optional")) {
			CustomerVendorreference vendorreference = new CustomerVendorreference();

			if (vendorForm.getReferenceName3() != null
					&& !vendorForm.getReferenceName3().isEmpty()) {
				vendorreference
						.setReferenceName(vendorForm.getReferenceName3());
				vendorreference.setReferenceId(3);
				vendorreference.setVendorId(vendorMaster);
			}

			if (vendorForm.getReferenceCompanyName3() != null
					&& !vendorForm.getReferenceCompanyName3().isEmpty()) {
				vendorreference.setReferenceCompanyName(vendorForm
						.getReferenceCompanyName3());
			}
			if (vendorForm.getReferenceAddress3() != null
					&& !vendorForm.getReferenceAddress3().isEmpty()) {
				vendorreference.setReferenceAddress(vendorForm
						.getReferenceAddress3());
			}
			if (vendorForm.getReferenceMailId3() != null
					&& !vendorForm.getReferenceMailId3().isEmpty()
					&& !vendorForm.getReferenceMailId3().equalsIgnoreCase(
							"Optional")) {
				vendorreference.setReferenceEmailId(vendorForm
						.getReferenceMailId3());
			}
			if (vendorForm.getReferenceMobile3() != null
					&& !vendorForm.getReferenceMobile3().isEmpty()) {
				vendorreference.setReferenceMobile(vendorForm
						.getReferenceMobile3());
			}
			if (vendorForm.getReferencePhone3() != null
					&& !vendorForm.getReferencePhone3().isEmpty()) {
				vendorreference.setReferencePhone(vendorForm
						.getReferencePhone3());
			}
			if (vendorForm.getExtension3() != null
					&& !vendorForm.getExtension3().isEmpty()
					&& !vendorForm.getExtension3().equalsIgnoreCase("Optional")) {
				vendorreference.setReferenceExtension(vendorForm
						.getExtension3());
			}
			if (vendorForm.getReferenceCity3() != null
					&& !vendorForm.getReferenceCity3().isEmpty()
					&& !vendorForm.getReferenceCity3().equalsIgnoreCase(
							"Optional")) {
				vendorreference
						.setReferenceCity(vendorForm.getReferenceCity3());
			}
			if (vendorForm.getReferenceZip3() != null
					&& !vendorForm.getReferenceZip3().isEmpty()
					&& !vendorForm.getReferenceZip3().equalsIgnoreCase(
							"Optional")) {
				vendorreference.setReferenceZip(vendorForm.getReferenceZip3());
			}
			if (vendorForm.getReferenceState3() != null
					&& !vendorForm.getReferenceState3().isEmpty()) {
				vendorreference.setReferenceState(vendorForm
						.getReferenceState3());
			}
			if (vendorForm.getReferenceCountry3() != null
					&& !vendorForm.getReferenceCountry3().isEmpty()) {
				vendorreference.setReferenceCountry(vendorForm
						.getReferenceCountry3());
			}
			if (vendorForm.getReferenceProvince3() != null
					&& !vendorForm.getReferenceProvince3().isEmpty()
					&& !vendorForm.getReferenceProvince3().equalsIgnoreCase(
							"Optional")) {
				vendorreference.setReferenceProvince(vendorForm
						.getReferenceProvince3());
			}
			vendorreference.setCreatedBy(userId);
			vendorreference.setCreatedOn(new Date());
			session.save(vendorreference);
		}
	}

	public static VendorMasterForm packVendorContactInfomation(
			VendorMasterForm vendorMasterForm, VendorContact vendorContact) {

		vendorMasterForm.setFirstName(vendorContact.getFirstName());
		vendorMasterForm.setLastName(vendorContact.getLastName());
		vendorMasterForm.setDesignation(vendorContact.getDesignation());
		vendorMasterForm.setContactPhone(vendorContact.getPhoneNumber());
		vendorMasterForm.setContactMobile(vendorContact.getMobile());
		vendorMasterForm.setContactFax(vendorContact.getFax());
		vendorMasterForm.setContanctEmail(vendorContact.getEmailId());

		Integer keyvalue = vendorContact.getKeyValue();
		Decrypt decrypt = new Decrypt();
		String decryptedPassword = null;
		if (keyvalue != null) {

			decryptedPassword = decrypt.decryptText(
					String.valueOf(keyvalue.toString()),
					vendorContact.getPassword());
		}
		vendorMasterForm.setLoginpassword(decryptedPassword);
		vendorMasterForm.setConfirmPassword(decryptedPassword);
		// vendorMasterForm.setLoginDisplayName(vendorContact
		// .getLoginDisplayName());
		// vendorMasterForm.setLoginId(vendorContact.getLoginId());
		if (vendorContact.getSecretQuestionId() != null) {
			vendorMasterForm.setUserSecQn(vendorContact.getSecretQuestionId()
					.getId());
		} else {
			vendorMasterForm.setUserSecQn(0);
		}
		vendorMasterForm.setUserSecQnAns(vendorContact.getSecQueAns());

		return vendorMasterForm;

	}

	/**
	 * Set business area details.
	 * 
	 * @param vendorMasterForm
	 * @param businessAreas
	 * @param vendorId
	 * @return
	 */
	public static VendorMasterForm packVendorBusinessAreas(
			VendorMasterForm vendorMasterForm,
			List<VendorBusinessArea> businessAreas, Integer vendorId) {
		String[] value = new String[businessAreas.size()];
		int i = 0;
		for (VendorBusinessArea businessArea : businessAreas) {
			value[i++] = businessArea.getBusinessAreaId().getId().toString();
		}
		vendorMasterForm.setBusinessArea(value);
		return vendorMasterForm;
	}

	public static VendorMasterForm packVendorServiceAreas(
			VendorMasterForm vendorMasterForm,
			List<CustomerVendorServiceArea> serviceAreas, Integer vendorId) {
		Integer[] value = new Integer[serviceAreas.size()];
		int i = 0;
		for (CustomerVendorServiceArea serviceArea : serviceAreas) {
			value[i++] = serviceArea.getServiceAreaId().getId();
		}
		vendorMasterForm.setServiceArea(value);
		return vendorMasterForm;
	}

	/**
	 * Business vendor contact save method.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorContact
	 * @param vendorMaster
	 * @return
	 */
	public static VendorContact packVendorPreparerContact(
			VendorMasterForm vendorForm, Integer userId,
			VendorContact vendorContact, VendorMaster vendorMaster) {
		vendorContact.setVendorId(vendorMaster);

		try {
			// if (vendorForm.getPreparerFirstName() != null
			// && !vendorForm.getPreparerFirstName().isEmpty()) {
			vendorContact.setFirstName(vendorForm.getPreparerFirstName());
			vendorContact.setLastName(vendorForm.getPreparerLastName());
			vendorContact.setPhoneNumber(vendorForm.getPreparerPhone());
			vendorContact.setDesignation(vendorForm.getPreparerTitle());
			vendorContact.setMobile(vendorForm.getPreparerMobile());
			vendorContact.setFax(vendorForm.getPreparerFax());
			vendorContact.setEmailId(vendorForm.getPreparerEmail());
			vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(false));
			vendorContact.setIsBusinessContact(CommonUtils.getByteValue(true));
			vendorContact.setIsPreparer(CommonUtils.getByteValue(false));
			// vendorContact.setIsPrimaryContact((byte) 0);
			vendorContact.setIsActive((byte) 1);
			vendorContact.setCreatedBy(userId);
			vendorContact.setCreatedOn(new Date());
			vendorContact.setModifiedBy(userId);
			vendorContact.setModifiedOn(new Date());
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vendorContact;

	}

	public static VendorContact packVendorPreparerContact1(
			VendorMasterForm vendorForm, Integer userId,
			VendorContact vendorContact, VendorMaster vendorMaster) {
		vendorContact.setVendorId(vendorMaster);
		Random random = new java.util.Random();
		Integer randVal = random.nextInt();
		Encrypt encrypt = new Encrypt();
		// convert password to encrypted password
		String encyppassword = null;

		try {
			if (vendorForm.getLoginpassword() != null
					&& !vendorForm.getLoginpassword().isEmpty()) {
				encyppassword = encrypt.encryptText(Integer.toString(randVal)
						+ "", vendorForm.getLoginpassword());
			}
			vendorContact.setFirstName(vendorForm.getFirstName());
			vendorContact.setLastName(vendorForm.getLastName());
			vendorContact.setPhoneNumber(vendorForm.getContactPhone());
			vendorContact.setDesignation(vendorForm.getDesignation());
			vendorContact.setMobile(vendorForm.getContactMobile());
			vendorContact.setFax(vendorForm.getContactFax());
			vendorContact.setEmailId(vendorForm.getContanctEmail());
			vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(false));
			vendorContact.setIsBusinessContact(CommonUtils.getByteValue(true));
			vendorContact.setIsPreparer(CommonUtils.getByteValue(false));
			// vendorContact.setIsPrimaryContact((byte) 0);
			vendorContact.setIsActive((byte) 1);
			vendorContact.setCreatedBy(userId);
			vendorContact.setCreatedOn(new Date());
			vendorContact.setModifiedBy(userId);
			vendorContact.setModifiedOn(new Date());
			vendorContact.setPassword(encyppassword);
			vendorContact.setKeyValue(randVal);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vendorContact;

	}

	/**
	 * Vendor package for business contact information.
	 * 
	 * @param vendorForm
	 * @param contact
	 * @return
	 */
	public static VendorMasterForm packVendorBusinessContactInfomation(
			VendorMasterForm vendorMasterForm, VendorContact vendorContact) {

		vendorMasterForm.setPreparerFirstName(vendorContact.getFirstName());
		vendorMasterForm.setPreparerLastName(vendorContact.getLastName());
		vendorMasterForm.setPreparerTitle(vendorContact.getDesignation());
		vendorMasterForm.setPreparerPhone(vendorContact.getPhoneNumber());
		vendorMasterForm.setPreparerMobile(vendorContact.getMobile());
		vendorMasterForm.setPreparerFax(vendorContact.getFax());
		vendorMasterForm.setPreparerEmail(vendorContact.getEmailId());
		return vendorMasterForm;
	}

	/**
	 * Pack vendor meeting details.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorMeetingInfo
	 * @param vendorMaster
	 */
	public static CustomerVendorMeetingInfo packMeetingInformation(
			VendorMasterForm vendorForm, Integer userId,
			CustomerVendorMeetingInfo vendorMeetingInfo,
			VendorMaster vendorMaster) {

		if (vendorForm.getContactFirstName() != null
				&& !vendorForm.getContactFirstName().isEmpty()) {
			vendorMeetingInfo.setVendorId(vendorMaster);
			vendorMeetingInfo.setContactFirstName(vendorForm
					.getContactFirstName());
			vendorMeetingInfo.setContactLastName(vendorForm
					.getContactLastName());
			vendorMeetingInfo.setContactEvent(vendorForm.getContactEvent());
			vendorMeetingInfo.setContactDate(CommonUtils
					.dateConvertValue(vendorForm.getContactDate()));
			vendorMeetingInfo.setContactState(vendorForm.getContactState());
			vendorMeetingInfo.setCreatedBy(userId);
			vendorMeetingInfo.setCreatedOn(new Date());
		}
		return vendorMeetingInfo;
	}

	/**
	 * Update Preparer business contact.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param contact
	 * @param vendorMaster
	 * @return
	 */
	public static VendorContact packPreparerBusinessContact(
			VendorMasterForm vendorForm, Integer userId,
			VendorContact vendorContact, VendorMaster vendorMaster) {

		vendorContact.setVendorId(vendorMaster);

		Random random = new java.util.Random();
		Integer randVal = random.nextInt();
		Encrypt encrypt = new Encrypt();
		// convert password to encrypted password
		String encyppassword = null;
		try {
			if (vendorForm.getLoginpassword() != null
					&& !vendorForm.getLoginpassword().isEmpty()) {
				encyppassword = encrypt.encryptText(Integer.toString(randVal)
						+ "", vendorForm.getLoginpassword());
			}

			if (vendorForm.getFirstName() != null
					&& !vendorForm.getFirstName().isEmpty()) {
				vendorContact.setFirstName(vendorForm.getFirstName());
			} else if (vendorForm.getPreparerFirstName() != null
					&& !vendorForm.getPreparerFirstName().isEmpty()) {
				vendorContact.setFirstName(vendorForm.getPreparerFirstName());
			}

			if (vendorForm.getLastName() != null
					&& !vendorForm.getLastName().isEmpty()) {
				vendorContact.setLastName(vendorForm.getLastName());
			} else if (vendorForm.getPreparerLastName() != null
					&& !vendorForm.getPreparerLastName().isEmpty()) {
				vendorContact.setLastName(vendorForm.getPreparerLastName());
			}

			if (vendorForm.getDesignation() != null
					&& !vendorForm.getDesignation().isEmpty()) {
				vendorContact.setDesignation(vendorForm.getDesignation());
			} else if (vendorForm.getPreparerTitle() != null
					&& !vendorForm.getPreparerTitle().isEmpty()) {
				vendorContact.setDesignation(vendorForm.getPreparerTitle());
			}

			if (vendorForm.getContactPhone() != null
					&& !vendorForm.getContactPhone().isEmpty()) {
				vendorContact.setPhoneNumber(vendorForm.getContactPhone());
			} else if (vendorForm.getPreparerPhone() != null
					&& !vendorForm.getPreparerPhone().isEmpty()) {
				vendorContact.setPhoneNumber(vendorForm.getPreparerPhone());
			}

			if (vendorForm.getContactMobile() != null
					&& !vendorForm.getContactMobile().isEmpty()) {
				vendorContact.setMobile(vendorForm.getContactMobile());
			} else if (vendorForm.getPreparerMobile() != null
					&& !vendorForm.getPreparerMobile().isEmpty()) {
				vendorContact.setMobile(vendorForm.getPreparerMobile());
			}

			if (vendorForm.getContactFax() != null
					&& !vendorForm.getContactFax().isEmpty()) {
				vendorContact.setFax(vendorForm.getContactFax());
			} else if (vendorForm.getPreparerFax() != null
					&& !vendorForm.getPreparerFax().isEmpty()) {
				vendorContact.setFax(vendorForm.getPreparerFax());
			}

			vendorContact.setEmailId(vendorForm.getContanctEmail());
			vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(true));
			vendorContact.setIsBusinessContact(CommonUtils.getByteValue(true));
			vendorContact.setIsPreparer(CommonUtils.getByteValue(true));
			// vendorContact.setIsPrimaryContact((byte) 1);
			vendorContact.setPassword(encyppassword);
			vendorContact.setKeyValue(randVal);
			vendorContact.setIsActive((byte) 1);
			vendorContact.setCreatedBy(userId);
			vendorContact.setCreatedOn(new Date());
			vendorContact.setModifiedBy(userId);
			vendorContact.setModifiedOn(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vendorContact;
	}

	/**
	 * Wrap the vendor meeting information into vendor form.
	 * 
	 * @param vendorForm
	 * @param meetingInfo
	 * @return
	 */
	public static VendorMasterForm packVendorContactMeetingInfomation(
			VendorMasterForm vendorForm, CustomerVendorMeetingInfo meetingInfo) {
		vendorForm.setContactFirstName(meetingInfo.getContactFirstName());
		vendorForm.setContactLastName(meetingInfo.getContactLastName());
		vendorForm.setContactDate(CommonUtils.convertDateToString(meetingInfo
				.getContactDate()));
		vendorForm.setContactEvent(meetingInfo.getContactEvent());
		vendorForm.setContactState(meetingInfo.getContactState());
		return vendorForm;
	}

	public static Comparator<VendorNAICS> NAICSPrimaryComparator = new Comparator<VendorNAICS>() {
		public int compare(VendorNAICS naics1, VendorNAICS naics2) {
			// descending order
			return naics2.getPrimary().compareTo(naics1.getPrimary());
		}
	};
}
