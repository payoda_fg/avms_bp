/*
 * HibernateUtil.java
 */
package com.fg.vms.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * HibernateUtil - Initialization for the SessionFactory.
 * 
 * @author pirabu
 */
@SuppressWarnings("deprecation")
public class HibernateUtil {

    /** The session factory. */
    private static SessionFactory sessionFactory;

    /**
     * Instantiates a new hibernate util.
     */
    private HibernateUtil() {

    }

    /**
     * Builds the session factory.
     * 
     * @return the session factory
     */
    public static SessionFactory buildSessionFactory() {
        if (sessionFactory == null) {
            try {
                sessionFactory = new AnnotationConfiguration().configure()
                        .buildSessionFactory();
            } catch (Throwable ex) {
                System.err.println("Initial SessionFactory creation failed."
                        + ex);
                throw new ExceptionInInitializerError(ex);
            }
        }

        return sessionFactory;
    }

    /**
     * Gets the session factory.
     * 
     * @return the session factory
     */
    public static SessionFactory getSessionFactory() {
        return null;
    }

}
