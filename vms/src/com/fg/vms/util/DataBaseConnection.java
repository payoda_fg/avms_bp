/*
 * DataBaseConnection.java
 */
package com.fg.vms.util;

import java.sql.Connection;
import java.sql.DriverManager;

import com.fg.vms.admin.dto.UserDetailsDto;

/**
 * Represents get Database Connection for Grid.
 * 
 * @author pirabu
 * 
 */
public class DataBaseConnection {

	/**
	 * Gets the connection.
	 * 
	 * @param userDetails
	 *            the user details
	 * @return the connection
	 */
	public static Connection getConnection(UserDetailsDto userDetails) {
		Connection conn = null;
		// We are giving the port number in DB Tables itself Table
		// Name:vms_master.customer
		String dbConnectionUrl = "jdbc:mysql://" + userDetails.getDatabaseIp()
				+ "/";
		if (userDetails.getDbName() != null
				&& userDetails.getDbName().length() != 0) {
			dbConnectionUrl += userDetails.getDbName();
		}
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(dbConnectionUrl + "?user="
					+ userDetails.getDbUsername() + "&password="
					+ userDetails.getDbPassword());
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return conn;
	}

}
