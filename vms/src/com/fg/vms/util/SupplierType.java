package com.fg.vms.util;

/**
 * Enumeration of the allowed "prime/non prime" state of any persistent record
 * (i.e., this is meta data for records we store in the database). PRIME
 * indicates the record is a prime vendor record. NONPRIME indicates the record
 * is non prime vendor
 * 
 * @author pirabu
 * 
 */
public enum SupplierType {

    /** The non prime vendor. */
    NONPRIME(0),
    /** The prime vendor. */
    PRIME(1),
    BOTH(2);
    private SupplierType(Integer index) {
        this.index = index;
    }

    private int index;

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index
     *            the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

}
