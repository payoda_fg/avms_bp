/*
 * Decrypt.java
 */
package com.fg.vms.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * The Class Decrypt.
 */
public class Decrypt {

	/**
	 * Instantiates a new decrypt.
	 */
	public Decrypt() {
		initialize();
	}

	/**
	 * Initialize.
	 */
	private void initialize() {
		Provider ibmjce = new com.ibm.crypto.provider.IBMJCE();
		Security.addProvider(ibmjce);
	}

	/**
	 * Decrypt text.
	 * 
	 * @param key
	 *            the key
	 * @param cryptedValue
	 *            the crypted value
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public String decryptText(String key, String cryptedValue) {
		KeyGen kgn = new KeyGen();
		byte[] cryptedKey;
		int tLen = -1;
		String decVal = "";
		byte[] decrVal = null;
		try {
			cryptedKey = kgn.deriveKey(key);
			byte[] encrypted = kgn.hextobyte(cryptedValue);
			do {
				decrVal = decrypt(cryptedKey, encrypted);
				decVal = new String(decrVal);
				tLen = decVal.length();
			} while (tLen > 48);
			decrVal = decrypt(cryptedKey, encrypted);
		} catch (Exception e) {

			PrintExceptionInLogFile.printException(e);
		}

		return new String(decrVal);
	}

	/**
	 * This method decrypts an encrypted text.<br>
	 * The decryption is done with a key value. The key used should be the key
	 * which <br>
	 * is used in encrypting the text. If the encrypting key and the deceypring
	 * key differs <br>
	 * data will not be returned properly. The decryption is done using the DES
	 * algorithm
	 * 
	 * @param cryptedKey
	 *            Array of type byte containing the key.
	 * @param encrypted
	 *            the encrypted
	 * @return byte[] Returns the decrypted value as an array of bytes.
	 * @throws Exception
	 *             the exception
	 */
	public byte[] decrypt(byte[] cryptedKey, byte[] encrypted) {
		SecretKeySpec desKeySpec = new SecretKeySpec(cryptedKey, "DESede");
		Cipher desCipher;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			desCipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			ByteArrayInputStream in = new ByteArrayInputStream(encrypted);

			out.flush();
			byte[] iv = new byte[8];
			in.read(iv);
			IvParameterSpec ivp = new IvParameterSpec(iv);
			desCipher.init(Cipher.DECRYPT_MODE, desKeySpec, ivp);
			byte[] buffer = new byte[2048];
			int bytesRead;
			while ((bytesRead = in.read(buffer)) != -1) {
				out.write(desCipher.update(buffer, 0, bytesRead));
			}
			out.write(desCipher.doFinal());
		} catch (NoSuchAlgorithmException e) {
			PrintExceptionInLogFile.printException(e);
		} catch (NoSuchPaddingException e) {
			PrintExceptionInLogFile.printException(e);
		} catch (IOException e) {
			PrintExceptionInLogFile.printException(e);
		} catch (InvalidKeyException e) {
			PrintExceptionInLogFile.printException(e);
		} catch (InvalidAlgorithmParameterException e) {
			PrintExceptionInLogFile.printException(e);
		} catch (IllegalBlockSizeException e) {
			PrintExceptionInLogFile.printException(e);
		} catch (BadPaddingException e) {
			PrintExceptionInLogFile.printException(e);
		}
		return out.toByteArray();
	}
}
