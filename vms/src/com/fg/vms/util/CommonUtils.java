/**
 * CommonUtils.java
 */
package com.fg.vms.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.io.FileUtils;
import org.apache.struts.upload.FormFile;
import org.hibernate.Hibernate;
import org.joda.time.LocalDate;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.customer.dto.Compliance;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.dto.RFIDocument;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.ProactiveMonitoringCfg;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.pojo.RFIInformationForm;
import com.fg.vms.customer.pojo.VendorMasterForm;

/**
 * The Class CommonUtils.
 * 
 * @author pirabu
 */
public final class CommonUtils {

	final static String UI_FORMAT = "MM/dd/yyyy";
	final static String DB_FORMAT = "yyyy/MM/dd";

	/**
	 * Convert date to string.
	 * 
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String convertDateToString(Date date) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				UI_FORMAT);
		if (date != null) {
			return sdf.format(date);
		} else
			return "";
	}

	/**
	 * Save the Image to filePath.
	 * 
	 * @param logo
	 *            the logo
	 * @param filePath
	 *            the file path
	 */
	public static void saveImage(FormFile logo, String filePath) {

		// Get the file name
		if (logo != null) {
			String fileName = logo.getFileName();
			// Save file to c:/vms/data/logos
			if (!fileName.equals("")) {
				// Create file
				File fileToCreate = new File(filePath, fileName);
				// If file does not exists create file
				if (!fileToCreate.exists()) {
					FileOutputStream fileOutStream;
					try {
						// fileToCreate.createNewFile();
						fileOutStream = new FileOutputStream(fileToCreate);
						fileOutStream.write(logo.getFileData());
						fileOutStream.flush();
						fileOutStream.close();
					} catch (FileNotFoundException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (IOException e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param file
	 * @param filePath
	 * @param prefix
	 */
	public static void saveAttachment(FormFile file, String filePath,
			String prefix) {
		// Get the file name
		if (file != null) {
			String fileName = file.getFileName();
			// Save file to c:/vms/data/logos
			if (!fileName.equals("")) {
				// Create file
				File fileToCreate = new File(filePath, prefix + "_" + fileName);
				// If file does not exists create file
				if (!fileToCreate.exists()) {
					FileOutputStream fileOutStream;
					try {
						// fileToCreate.createNewFile();
						fileOutStream = new FileOutputStream(fileToCreate);
						fileOutStream.write(file.getFileData());
						fileOutStream.flush();
						fileOutStream.close();
					} catch (FileNotFoundException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (IOException e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		}
	}

	/**
	 * Read image.
	 * 
	 * @param filePath
	 *            the file path
	 * @param fileName
	 *            the file name
	 */
	public static void readImage(String filePath, String fileName) {

	}

	/**
	 * Pass byte value to get booleanS.
	 * 
	 * @param value
	 *            the value
	 * @return the boolean value
	 */
	public static boolean getBooleanValue(Byte value) {
		if (value == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * To pack the list of RFIDocument from array of title and documents from
	 * information form.
	 * 
	 * @param rfiInformation
	 *            the rfi information
	 * @param object
	 *            the object
	 * @return the list
	 */
	public static List<?> packRFIDocuments(RFIInformationForm rfiInformation,
			Object object) {
		if (object.equals(RFIDocument.class)) {
			List<RFIDocument> documents = new ArrayList<RFIDocument>();
			if (rfiInformation.getTitles() != null
					&& rfiInformation.getTitles().length != 0) {
				for (int i = 0; i < rfiInformation.getTitles().length; i++) {
					RFIDocument document = new RFIDocument();
					document.setTitle(rfiInformation.getTitles()[i]);
					document.setDocPath(rfiInformation.getDocs().get(i)
							.getFileName());
					documents.add(document);
				}
			}
			return documents;
		} else {
			List<Compliance> compliances = new ArrayList<Compliance>();
			List<String> attributes = packAttributes(rfiInformation
					.getAttribute());
			List<String> weightages = null;
			if (rfiInformation.getWeightages() != null
					&& rfiInformation.getWeightages().length != 0) {
				weightages = packAttributes(rfiInformation.getWeightages());
			}

			if (rfiInformation.getComplianceItems() != null
					&& rfiInformation.getComplianceItems().length != 0) {

				for (int i = 0; i < rfiInformation.getComplianceItems().length; i++) {
					Compliance compliance = new Compliance();

					compliance.setComplianceItem(rfiInformation
							.getComplianceItems()[i]);
					if (rfiInformation.getTarget() != null
							&& rfiInformation.getTarget().size() != 0) {

						compliance.setTarget(rfiInformation.getTarget().get(i)
								.charAt(0));
					}
					compliance.setNote(rfiInformation.getNotes()[i]);
					if (rfiInformation.getMandatory() != null
							&& rfiInformation.getMandatory().size() != 0) {
						compliance.setMandatory(rfiInformation.getMandatory()
								.get(i).charAt(0));
					}
					if (i == 0) {
						compliance.setAttributes(attributes.subList(0,
								rfiInformation.getAttributeCount()[i])
								.toArray());
						if (weightages != null && weightages.size() != 0) {
							compliance.setWeightages(weightages.subList(0,
									rfiInformation.getAttributeCount()[i])
									.toArray());
						}
					} else {
						compliance
								.setAttributes(attributes
										.subList(
												rfiInformation
														.getAttributeCount()[i - 1],
												rfiInformation
														.getAttributeCount()[i]
														+ rfiInformation
																.getAttributeCount()[i - 1])
										.toArray());
						if (weightages != null && weightages.size() != 0) {
							compliance
									.setWeightages(weightages
											.subList(
													rfiInformation
															.getAttributeCount()[i - 1],
													rfiInformation
															.getAttributeCount()[i]
															+ rfiInformation
																	.getAttributeCount()[i - 1])
											.toArray());
						}

					}
					compliances.add(compliance);
				}
			}
			return compliances;
		}
	}

	/**
	 * Pack attributes.
	 * 
	 * @param attribute
	 *            the attribute
	 * @return the list
	 */
	public static List<String> packAttributes(String attribute[]) {

		List<String> attributes = new ArrayList<String>();
		for (String attr : attribute) {
			attributes.add(attr);

		}
		return attributes;
	}

	/**
	 * Pack weightages.
	 * 
	 * @param weightages
	 *            the weightages
	 * @return the list
	 */
	public static List<String> packWeightages(String weightages[]) {

		List<String> weightage = new ArrayList<String>();
		for (String attr : weightages) {
			weightage.add(attr);
		}
		return weightage;

	}

	/**
	 * Getting diverse certification from the form bean class and return it to
	 * action class.
	 * 
	 * @param vendorForm
	 *            the vendor form
	 * @return the list
	 */
	public static List<DiverseCertificationTypesDto> packageDiverseTypes(
			VendorMasterForm vendorForm) {
		List<DiverseCertificationTypesDto> certifications = new ArrayList<DiverseCertificationTypesDto>();
		DiverseCertificationTypesDto certification1 = null;
		// if (vendorForm.getDivCertAgen1() != null
		// && vendorForm.getDivCertAgen1().length() != 0
		// && (vendorForm.getDivCertType1() != null && vendorForm
		// .getDivCertType1().length() != 0)
		// && (vendorForm.getCertificationNo1() != null && vendorForm
		// .getCertificationNo1().length() != 0)
		//
		// && (vendorForm.getExpDate1() != null && vendorForm
		// .getExpDate1().length() != 0)) {
		if (vendorForm.getDivCertType1() != null
				&& vendorForm.getDivCertType1().length() != 0) {
			certification1 = new DiverseCertificationTypesDto();

			if (vendorForm.getVendorCertID1() != null
					&& vendorForm.getVendorCertID1() != 0) {
				certification1.setId(vendorForm.getVendorCertID1());
			}
			if (vendorForm.getDivCertAgen1() != null
					&& vendorForm.getDivCertAgen1().length() != 0) {
				certification1.setDivCertAgent(Integer.parseInt(vendorForm
						.getDivCertAgen1()));
			}
			if (vendorForm.getDivCertType1() != null
					&& vendorForm.getDivCertType1().length() != 0) {
				certification1.setDivCertType(Integer.parseInt(vendorForm
						.getDivCertType1()));
			}
			if (vendorForm.getCertType1() != null
					&& !vendorForm.getCertType1().isEmpty()) {
				certification1.setCertType(vendorForm.getCertType1());
			}
			if (vendorForm.getCertificationNo1() != null
					&& vendorForm.getCertificationNo1().length() != 0) {
				certification1.setCertificateNumber(vendorForm
						.getCertificationNo1());
			}
			if (vendorForm.getEffDate1() != null
					&& vendorForm.getEffDate1().length() != 0) {
				certification1.setEffectiveDate(vendorForm.getEffDate1());
			}
			if (vendorForm.getExpDate1() != null
					&& vendorForm.getExpDate1().length() != 0) {
				certification1.setExpDate(vendorForm.getExpDate1());
			}
			if (vendorForm.getCertFile1() != null
					&& vendorForm.getCertFile1().getFileSize() != 0) {
				certification1.setCertificate(vendorForm.getCertFile1());
				// saveFile(vendorForm.getCertFile1(),
				// vendorForm.getVendorCode());
			}
			if (vendorForm.getEthnicity1() != null
					&& !vendorForm.getEthnicity1().isEmpty()) {
				certification1.setEthnicity(vendorForm.getEthnicity1());
			}
		}

		DiverseCertificationTypesDto certification2 = null;
		// if (vendorForm.getDivCertAgen2() != null
		// && vendorForm.getDivCertAgen2().length() != 0
		// && (vendorForm.getDivCertType2() != null && vendorForm
		// .getDivCertType2().length() != 0)
		// && (vendorForm.getExpDate2() != null && vendorForm
		// .getExpDate2().length() != 0)) {
		if (vendorForm.getDivCertType2() != null
				&& vendorForm.getDivCertType2().length() != 0) {
			certification2 = new DiverseCertificationTypesDto();
			if (vendorForm.getVendorCertID2() != null
					&& vendorForm.getVendorCertID2() != 0) {
				certification2.setId(vendorForm.getVendorCertID2());
			}
			if (vendorForm.getDivCertAgen2() != null
					&& vendorForm.getDivCertAgen2().length() != 0) {

				certification2.setDivCertAgent(Integer.parseInt(vendorForm
						.getDivCertAgen2()));
			}
			if (vendorForm.getDivCertType2() != null
					&& vendorForm.getDivCertType2().length() != 0) {
				certification2.setDivCertType(Integer.parseInt(vendorForm
						.getDivCertType2()));
			}
			if (vendorForm.getCertType2() != null
					&& !vendorForm.getCertType2().isEmpty()) {
				certification2.setCertType(vendorForm.getCertType2());
			}
			if (vendorForm.getCertificationNo2() != null
					&& vendorForm.getCertificationNo2().length() != 0) {
				certification2.setCertificateNumber(vendorForm
						.getCertificationNo2());
			}
			if (vendorForm.getEffDate2() != null
					&& vendorForm.getEffDate2().length() != 0) {
				certification2.setEffectiveDate(vendorForm.getEffDate2());
			}
			if (vendorForm.getExpDate2() != null
					&& vendorForm.getExpDate2().length() != 0) {
				certification2.setExpDate(vendorForm.getExpDate2());
			}
			if (vendorForm.getCertFile2() != null
					&& vendorForm.getCertFile2().getFileSize() != 0) {
				certification2.setCertificate(vendorForm.getCertFile2());
				// saveFile(vendorForm.getCertFile2(),
				// vendorForm.getVendorCode());
			}
			if (vendorForm.getEthnicity2() != null
					&& !vendorForm.getEthnicity2().isEmpty()) {
				certification2.setEthnicity(vendorForm.getEthnicity2());
			}
		}

		DiverseCertificationTypesDto certification3 = null;
		// if (vendorForm.getDivCertAgen3() != null
		// && vendorForm.getDivCertAgen3().length() != 0
		// && (vendorForm.getDivCertType3() != null && vendorForm
		// .getDivCertType3().length() != 0)
		// && (vendorForm.getExpDate3() != null && vendorForm
		// .getExpDate3().length() != 0)) {
		if (vendorForm.getDivCertType3() != null
				&& vendorForm.getDivCertType3().length() != 0) {
			certification3 = new DiverseCertificationTypesDto();
			if (vendorForm.getVendorCertID3() != null
					&& vendorForm.getVendorCertID3() != 0) {
				certification3.setId(vendorForm.getVendorCertID3());
			}
			if (vendorForm.getDivCertAgen3() != null
					&& vendorForm.getDivCertAgen3().length() != 0) {
				certification3.setDivCertAgent(Integer.parseInt(vendorForm
						.getDivCertAgen3()));
			}
			if (vendorForm.getDivCertType3() != null
					&& vendorForm.getDivCertType3().length() != 0) {
				certification3.setDivCertType(Integer.parseInt(vendorForm
						.getDivCertType3()));
			}
			if (vendorForm.getCertType3() != null
					&& !vendorForm.getCertType3().isEmpty()) {
				certification3.setCertType(vendorForm.getCertType3());
			}
			if (vendorForm.getCertificationNo3() != null
					&& vendorForm.getCertificationNo3().length() != 0) {
				certification3.setCertificateNumber(vendorForm
						.getCertificationNo3());
			}
			if (vendorForm.getEffDate3() != null
					&& vendorForm.getEffDate3().length() != 0) {
				certification3.setEffectiveDate(vendorForm.getEffDate3());
			}
			if (vendorForm.getExpDate3() != null
					&& vendorForm.getExpDate3().length() != 0) {
				certification3.setExpDate(vendorForm.getExpDate3());
			}
			if (vendorForm.getCertFile3() != null
					&& vendorForm.getCertFile3().getFileSize() != 0) {
				certification3.setCertificate(vendorForm.getCertFile3());
				// saveFile(vendorForm.getCertFile3(),
				// vendorForm.getVendorCode());
			}
			if (vendorForm.getEthnicity3() != null
					&& !vendorForm.getEthnicity3().isEmpty()) {
				certification3.setEthnicity(vendorForm.getEthnicity3());
			}
		}

		if (certification1 != null) {
			certifications.add(certification1);
		}
		if (certification2 != null) {
			certifications.add(certification2);
		}
		if (certification3 != null) {
			certifications.add(certification3);
		}

		return certifications;
	}

	/**
	 * Getting NaicsCodes from the form bean class and return it to action
	 * class.
	 * 
	 * @param vendorForm
	 *            the vendor form
	 * @return the list
	 */
	public static List<NaicsCodesDto> packageNiacsCodes(
			VendorMasterForm vendorForm) {
		List<NaicsCodesDto> nacisCodes = new ArrayList<NaicsCodesDto>();

		if (vendorForm.getNaicsCode_0() != null
				&& !vendorForm.getNaicsCode_0().isEmpty()) {
			NaicsCodesDto naicsCodeDto = new NaicsCodesDto();

			if (vendorForm.getNaicsID1() != null
					&& vendorForm.getNaicsID1() != 0) {
				naicsCodeDto.setId(vendorForm.getNaicsID1());
			}
			naicsCodeDto.setNaicsCode(vendorForm.getNaicsCode_0());
			naicsCodeDto.setCapabilitie(vendorForm.getCapabilitie1());
			naicsCodeDto.setPrimaryKey((byte) 1);
			// if (vendorForm.getPrimary() != null
			// && vendorForm.getPrimary().equalsIgnoreCase("1")) {
			// naicsCodeDto.setPrimaryKey((byte) 1);
			// } else {
			// naicsCodeDto.setPrimaryKey((byte) 0);
			// }

			nacisCodes.add(naicsCodeDto);
		}
		if (vendorForm.getNaicsCode_1() != null
				&& !vendorForm.getNaicsCode_1().isEmpty()) {
			NaicsCodesDto naicsCodeDto2 = new NaicsCodesDto();
			if (vendorForm.getNaicsID2() != null
					&& vendorForm.getNaicsID2() != 0) {
				naicsCodeDto2.setId(vendorForm.getNaicsID2());
			}
			naicsCodeDto2.setNaicsCode(vendorForm.getNaicsCode_1());
			naicsCodeDto2.setCapabilitie(vendorForm.getCapabilitie2());
			naicsCodeDto2.setPrimaryKey((byte) 0);
			// if (vendorForm.getPrimary() != null
			// && vendorForm.getPrimary().equalsIgnoreCase("2")) {
			// naicsCodeDto2.setPrimaryKey((byte) 2);
			// } else {
			// naicsCodeDto2.setPrimaryKey((byte) 0);
			// }
			nacisCodes.add(naicsCodeDto2);
		}

		if (vendorForm.getNaicsCode_2() != null
				&& !vendorForm.getNaicsCode_2().isEmpty()) {
			NaicsCodesDto naicsCodeDto3 = new NaicsCodesDto();
			if (vendorForm.getNaicsID3() != null
					&& vendorForm.getNaicsID3() != 0) {
				naicsCodeDto3.setId(vendorForm.getNaicsID3());
			}
			naicsCodeDto3.setNaicsCode(vendorForm.getNaicsCode_2());
			naicsCodeDto3.setCapabilitie(vendorForm.getCapabilitie3());
			naicsCodeDto3.setPrimaryKey((byte) 0);
			// if (vendorForm.getPrimary() != null
			// && vendorForm.getPrimary().equalsIgnoreCase("3")) {
			// naicsCodeDto3.setPrimaryKey((byte) 3);
			// } else {
			// naicsCodeDto3.setPrimaryKey((byte) 0);
			// }

			nacisCodes.add(naicsCodeDto3);
		}

		return nacisCodes;
	}

	/**
	 * Package other certificates.
	 * 
	 * @param vendorForm
	 *            the vendor form
	 * @return the list
	 */
	public static List<VendorOtherCertificate> packageOtherCertificates(
			VendorMasterForm vendorForm, Integer userId) {

		List<VendorOtherCertificate> otherCertificates = new ArrayList<VendorOtherCertificate>();

		// int noOfCertificate=vendorForm.getOtherCerts().size()-1;
		for (int index = 0; index < vendorForm.getOtherCerts().size(); index++) {

			if (null != vendorForm.getOtherCertType1()
					&& vendorForm.getOtherCertType1().length > index) {

				if (vendorForm.getOtherCertType1()[index] != null
						&& vendorForm.getOtherCertType1()[index] != -1
						&& vendorForm.getOtherExpiryDate()[index] != null
						&& vendorForm.getOtherExpiryDate()[index].length() != 0) {
					VendorOtherCertificate certificate = new VendorOtherCertificate();
					certificate.setCertificationType(vendorForm
							.getOtherCertType1()[index]);

					if (vendorForm.getOtherCerts().containsKey(index)
							&& vendorForm.getOtherCerts().get(index)
									.getFileSize() != 0) {
						certificate.setContent(CommonUtils
								.convertFormFileToBlob(vendorForm
										.getOtherCerts().get(index)));
						certificate.setContentType(vendorForm.getOtherCerts()
								.get(index).getContentType());
						certificate.setFilename(vendorForm.getOtherCerts()
								.get(index).getFileName());
					}
					certificate
							.setExpiryDate(CommonUtils
									.dateConvertValue(vendorForm
											.getOtherExpiryDate()[index]));
					certificate.setCreatedBy(userId);
					certificate.setCreatedOn(new Date());
					certificate.setModifiedBy(userId);
					certificate.setModifiedOn(new Date());
					otherCertificates.add(certificate);
				}
			}
		}
		return otherCertificates;
	}
	
	/**
	 * Package Vendor Keywords.
	 */
	public static List<CustomerVendorKeywords> packageVendorKeywords(VendorMasterForm vendorForm, Integer userId) {
		List<CustomerVendorKeywords> keywords = new ArrayList<CustomerVendorKeywords>();
		
		if(vendorForm.getVendorKeyword() != null && vendorForm.getVendorKeyword().length > 0) {
			for (int index = 0; index < vendorForm.getVendorKeyword().length; index++) {
				if (vendorForm.getVendorKeyword()[index] != null && !vendorForm.getVendorKeyword()[index].isEmpty()) {
					CustomerVendorKeywords vendorKeywords = new CustomerVendorKeywords();
					vendorKeywords.setKeyword(vendorForm.getVendorKeyword()[index]);
					vendorKeywords.setCreatedBy(userId);
					vendorKeywords.setCreatedOn(new Date());
					keywords.add(vendorKeywords);
				}
			}
		}		
		return keywords;
	}

	/**
	 * Package vendor documents.
	 * 
	 * @param vendorForm
	 *            the vendor form
	 * @return the list
	 */
	public static List<VendorDocuments> packageVendorDocuments(
			VendorMasterForm vendorForm, VendorMaster master, Integer userId,
			String companyCode) {

		List<VendorDocuments> documents = new ArrayList<VendorDocuments>();

		if (vendorForm.getVendorDocs() != null
				&& vendorForm.getVendorDocs().size() > 0
				&& vendorForm.getVendorDocName() != null
				&& vendorForm.getVendorDocDesc() != null) {
			for (int index = 0; index < vendorForm.getVendorDocs().size(); index++) {

				if (vendorForm.getVendorDocName()[index] != null
						&& !vendorForm.getVendorDocName()[index].isEmpty()
						&& vendorForm.getVendorDocDesc()[index] != null
						&& !vendorForm.getVendorDocDesc()[index].isEmpty()) {
					VendorDocuments vendorDocuments = new VendorDocuments();
					vendorDocuments.setDocumentName(vendorForm
							.getVendorDocName()[index]);
					vendorDocuments.setDocumentDescription(vendorForm
							.getVendorDocDesc()[index]);
					vendorDocuments.setCreatedBy(userId);
					vendorDocuments.setCreatedOn(new Date());
					if (vendorForm.getVendorDocs().containsKey(index)
							&& vendorForm.getVendorDocs().get(index)
									.getFileSize() != 0) {
						vendorDocuments.setDocumentFilename(vendorForm
								.getVendorDocs().get(index).getFileName());
						vendorDocuments.setDocumentType(vendorForm
								.getVendorDocs().get(index).getContentType());
						vendorDocuments.setDocumentFilesize((double) vendorForm
								.getVendorDocs().get(index).getFileSize());
					}

					String filePath = Constants
							.getString(Constants.UPLOAD_DOCUMENT_PATH)
							+ "/"
							+ companyCode + "/" + master.getId();
					File theDir = new File(filePath);

					// if the directory does not exist, create it
					if (!theDir.exists()) {
						theDir.mkdirs();
					}
					vendorDocuments.setDocumentPhysicalpath(filePath);
					// Saving Image to appropriate path
					saveImage(vendorForm.getVendorDocs().get(index), filePath);
					documents.add(vendorDocuments);
				}
			}
		}

		return documents;

	}

	/**
	 * Conversion function for boolean value to Byte value.
	 * 
	 * @param value
	 *            the boolean value
	 * @return the byte value
	 */
	public static Byte getByteValue(boolean value) {
		if (value) {
			return (byte) 1;
		} else {
			return (byte) 0;
		}
	}

	/**
	 * Convert string to date.
	 * 
	 * @param date
	 *            the date
	 * @return the date
	 */
	@SuppressWarnings("deprecation")
	public static Date dateConvertValue(String date) {
		Date newDate = null;
		if (date != null) {
			String slaDate = date.replace("-", "/");
			if (slaDate != null && slaDate.length() != 0) {
				newDate = new Date(slaDate);
			}
		}
		return newDate;
	}

	/**
	 * Save file.
	 * 
	 * @param file
	 *            the file
	 * @param vendorCode
	 *            the vendor code
	 */
	public static void saveFile(FormFile file, String vendorCode) {
		String filePath = FileUploadProcess.createUploadImageFilePath(
				Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/",
				vendorCode);
		if (FileUploadProcess.checkFolderAvailable(
				Constants.getString(Constants.UPLOAD_LOGO_PATH), vendorCode)) {
			CommonUtils.saveImage(file, filePath);
		} else {
			FileUploadProcess
					.createFolder(
							Constants.getString(Constants.UPLOAD_LOGO_PATH),
							vendorCode);
			CommonUtils.saveImage(file, filePath);
		}
	}

	// Convert Money Value to Double value
	/**
	 * Deformat money.
	 * 
	 * @param money
	 *            the money
	 * @return the double
	 */
	public static Double deformatMoney(String money) {
		Pattern pattern = Pattern.compile(".*[^0-9.,$].*");
		if (money != null && !money.isEmpty()
				&& !pattern.matcher(money).matches()) {
			String at = "";
			for (int i = 0; i < money.length(); i++) {
				if (money.charAt(i) != ',') {
					at += money.charAt(i);
				}
			}
			String temp = at.replace('$', ' ');
			try{
			double moneyvalue = Double.parseDouble(temp.toString().trim());
			return moneyvalue;
			}
			catch(Exception e)
			{
				return 0.0;
			}
		} else {
			return 0.0;
		}
	}

	/**
	 * Convert form file to blob.
	 * 
	 * @param file
	 *            the file
	 * @return blob object
	 */
	@SuppressWarnings("deprecation")
	public static Blob convertFormFileToBlob(FormFile file) {
		Blob blob = null;
		try {
			if (file != null && file.getFileSize() != 0) {
				blob = Hibernate.createBlob(file.getInputStream());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return blob;
	}

	/**
	 * Gets the checkbox value.
	 * 
	 * @param string
	 *            the string
	 * @return the checkbox value
	 */
	public static Byte getCheckboxValue(String string) {
		if (string != null && string.length() != 0) {
			if (string.equalsIgnoreCase("on")) {
				return (byte) 1;
			} else {
				return (byte) 0;
			}
		} else {
			return (byte) 0;
		}
	}

	/**
	 * Convert DateString (MM-dd-yyyy) to MysqlDB date format (yyyy/MM/dd).
	 * 
	 * @param string
	 * @return
	 */
	public static String convertDateToDBFormat(String string) {
		String newDateString = null;
		SimpleDateFormat sdf = new SimpleDateFormat(UI_FORMAT);
		Date d = null;
		try {
			if (!string.isEmpty() && !string.equals(" ")) {
				d = sdf.parse(string);
				sdf.applyPattern(DB_FORMAT);
				newDateString = sdf.format(d);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return newDateString;
	}

	/**
	 * get the nth day of month
	 * 
	 * @param day_of_week
	 * @param month
	 * @param year
	 * @param week
	 * @return
	 */
	public static LocalDate getNthOfMonth(int day_of_week, int month, int year,
			int week) {
		if (week > 1) {
			week -= 1;
		}
		LocalDate date = new LocalDate(year, month, 1).dayOfMonth()
				.withMinimumValue().plusWeeks(week - 1).dayOfWeek()
				.setCopy(day_of_week);
		if (date.getMonthOfYear() != month) {
			return date.dayOfWeek().addToCopy(-7);
		}
		return date;
	}

	/**
	 * 
	 * @param cfg
	 * @return
	 */
	public static Integer getWeekNumber(ProactiveMonitoringCfg cfg,
			String option) {
		int week = 0;
		if (option.equalsIgnoreCase("month")) {
			if (cfg.getMonthlyWeekNumber().equalsIgnoreCase("first")) {
				week = 1;
			} else if (cfg.getMonthlyWeekNumber().equalsIgnoreCase("second")) {
				week = 2;
			} else if (cfg.getMonthlyWeekNumber().equalsIgnoreCase("third")) {
				week = 3;
			} else if (cfg.getMonthlyWeekNumber().equalsIgnoreCase("fourth")) {
				week = 4;
			} else if (cfg.getMonthlyWeekNumber().equalsIgnoreCase("last")) {
				week = 5;
			}
		} else {
			if (cfg.getYearlyWeekNumber().equalsIgnoreCase("first")) {
				week = 1;
			} else if (cfg.getYearlyWeekNumber().equalsIgnoreCase("second")) {
				week = 2;
			} else if (cfg.getYearlyWeekNumber().equalsIgnoreCase("third")) {
				week = 3;
			} else if (cfg.getYearlyWeekNumber().equalsIgnoreCase("fourth")) {
				week = 4;
			} else if (cfg.getYearlyWeekNumber().equalsIgnoreCase("last")) {
				week = 5;
			}
		}
		return week;
	}

	/**
	 * check the privilege for add
	 * 
	 * @param privileges
	 * @param objectName
	 * @return
	 */
	public static boolean isAdd(List<RolePrivileges> privileges,
			String objectName) {
		for (RolePrivileges privilege : privileges) {
			if (privilege.getObjectId().getObjectName()
					.equalsIgnoreCase(objectName)) {
				if (privilege.getAdd().equals(new Byte((byte) 1))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * check the privilege for view
	 * 
	 * @param privileges
	 * @param objectName
	 * @return
	 */
	public static boolean isView(List<RolePrivileges> privileges,
			String objectName) {

		for (RolePrivileges privilege : privileges) {
			if (privilege.getObjectId().getObjectName()
					.equalsIgnoreCase(objectName)) {
				if (privilege.getView().equals(new Byte((byte) 1))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * check the privilege for modify
	 * 
	 * @param privileges
	 * @param objectName
	 * @return
	 */
	public static boolean isModify(List<RolePrivileges> privileges,
			String objectName) {

		for (RolePrivileges privilege : privileges) {
			if (privilege.getObjectId().getObjectName()
					.equalsIgnoreCase(objectName)) {
				if (privilege.getModify().equals(new Byte((byte) 1))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * check the privilege for delete
	 * 
	 * @param privileges
	 * @param objectName
	 * @return
	 */
	public static boolean isDelete(List<RolePrivileges> privileges,
			String objectName) {
		for (RolePrivileges privilege : privileges) {
			if (privilege.getObjectId().getObjectName()
					.equalsIgnoreCase(objectName)) {
				if (privilege.getDelete().equals(new Byte((byte) 1))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Delete the uploaded document from physical path.
	 * 
	 * @param docId
	 * @param vendorId
	 */
	public static void deleteDocument(String fileName, Integer vendorId) {

		String filePath = Constants.getString(Constants.UPLOAD_DOCUMENT_PATH)
				+ "/" + vendorId + "/" + fileName;
		File theDir = new File(filePath);

		try {
			// if the directory exist, delete it
			if (theDir.exists()) {
				FileUtils.forceDelete(theDir);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Calculate the reporting period based on the Tier2 Reporting Period
	 * configuration.
	 * 
	 * @param reportingPeriod
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static List<String> calculateReportingPeriods(
			Tier2ReportingPeriod reportingPeriod, Integer year1) {
		int year = year1;
		int reportStartMonth = 0;
		int reportEndMonth = 3 - 1;
		List<String> reportPeriod = new ArrayList<String>();
		if (reportingPeriod != null) {
			// DateTime startDateTime = new DateTime(
			// reportingPeriod.getStartDate());
			// DateTime endDateTime = new
			// DateTime(reportingPeriod.getStartDate());
			Calendar c = Calendar.getInstance();
			switch (FrequencyPeriod
					.valueOf(reportingPeriod.getDataUploadFreq())) {

			case M:
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();

				for (int i = 0; i < 12; i++) {
					c.set(year, reportStartMonth, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH,
							c.getActualMinimum(Calendar.DAY_OF_MONTH));
					SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
					// System.out.println(sdf.format(c.getTime()));
					StringBuilder period = new StringBuilder();

					period.append(sdf.format(c.getTime()));
					c.set(year, reportEndMonth, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH,
							c.getActualMaximum(Calendar.DAY_OF_MONTH));
					period.append(" - " + sdf.format(c.getTime()));

					reportStartMonth++;
					reportEndMonth++;
					reportPeriod.add(period.toString());

				}
				break;
			case Q:
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();

				for (int i = 0; i < 4; i++) {
					c.set(year, reportStartMonth, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH,
							c.getActualMinimum(Calendar.DAY_OF_MONTH));
					SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
					// System.out.println(sdf.format(c.getTime()));
					StringBuilder period = new StringBuilder();

					period.append(sdf.format(c.getTime()));
					c.set(year, reportEndMonth, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH,
							c.getActualMaximum(Calendar.DAY_OF_MONTH));
					period.append(" - " + sdf.format(c.getTime()));

					reportStartMonth += 3;
					reportEndMonth += 3;
					reportPeriod.add(period.toString());

				}
				break;
			case A:
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();

				c.set(year, reportStartMonth, 1); // ------>
				c.set(Calendar.DAY_OF_MONTH,
						c.getActualMinimum(Calendar.DAY_OF_MONTH));
				SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
				// System.out.println(sdf.format(c.getTime()));
				StringBuilder period = new StringBuilder();

				period.append(sdf.format(c.getTime()));
				c.set(year, reportEndMonth, 1); // ------>
				c.set(Calendar.DAY_OF_MONTH,
						c.getActualMaximum(Calendar.DAY_OF_MONTH));
				period.append(" - " + sdf.format(c.getTime()));

				reportPeriod.add(period.toString());
				break;
			default:
				break;
			}

		}
		return reportPeriod;
	}

	public static String checkPasswordLimitExist(
			List<PasswordHistory> histories, String password) {
		if (histories != null && !histories.isEmpty()) {
			Decrypt decrypt = new Decrypt();
			for (PasswordHistory history : histories) {
				String decryptedPassword = decrypt.decryptText(
						String.valueOf(history.getKeyvalue().toString()),
						history.getPassword());
				if (decryptedPassword.equals(password)) {
					return "exist";
				}
			}
		}
		return "success";

	}

	@SuppressWarnings("deprecation")
	public static int findCurrentQuarter(Tier2ReportingPeriod reportingPeriod) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int reportStartMonth = 0;
		int reportEndMonth = 3 - 1;
		List<String> reportPeriod = new ArrayList<String>();
		reportStartMonth = reportingPeriod.getStartDate().getMonth();
		reportEndMonth = reportingPeriod.getEndDate().getMonth();
		Calendar c = Calendar.getInstance();

		for (int i = 0; i < 4; i++) {

			c.set(year, reportStartMonth, 1); // ------>
			c.set(Calendar.DAY_OF_MONTH,
					c.getActualMinimum(Calendar.DAY_OF_MONTH));
			StringBuilder period = new StringBuilder();

			period.append(c.get(Calendar.MONTH) + "," + c.get(Calendar.MONTH)
					+ 1 + ",");
			c.set(year, reportEndMonth, 1); // ------>
			c.set(Calendar.DAY_OF_MONTH,
					c.getActualMaximum(Calendar.DAY_OF_MONTH));
			period.append(c.get(Calendar.MONTH));
			reportStartMonth += 3;
			reportEndMonth += 3;
			reportPeriod.add(period.toString());

		}
		Calendar calendar = Calendar.getInstance();
		int currentMonth = calendar.get(Calendar.MONTH);
		if(reportingPeriod.getDataUploadFreq().equalsIgnoreCase("Q")){
			for (int index = 0; index < 4; index++) {
				String period[] = reportPeriod.get(index).split(",");
				if (Arrays.asList(period).contains(String.valueOf(currentMonth))) {
					return index;
				}
			}
		}
		return 0;
	}

	/**
	 * Find the Frequency Period
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static FrequencyPeriod findFrequencyPeriod(Date startDate,
			Date endDate) {
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(startDate);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(endDate);

		int diffYear = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);
		if (diffMonth == 1) {
			return FrequencyPeriod.M;
		} else if (diffMonth == 3) {
			return FrequencyPeriod.Q;
		} else {
			return FrequencyPeriod.A;
		}
	}

	public static List<String> packToEmailIds(
			List<EmailDistributionListDetailModel> detailModels) {
		List<String> toEmailIds = null;
		if (null != detailModels) {
			if (null != detailModels && !detailModels.isEmpty()) {
				toEmailIds = new ArrayList<String>();
				for (EmailDistributionListDetailModel model : detailModels) {
					if (model.getIsTo() != null && model.getIsTo() != 0) {
						if (null != model.getUserId()) {
							toEmailIds.add(model.getUserId().getUserEmailId());
						}
						if (null != model.getVendorId()) {
							toEmailIds.add(model.getVendorId().getEmailId());
						}
					}
				}
			}
		}
		return toEmailIds;
	}

	public static List<String> packCCEmailIds(
			List<EmailDistributionListDetailModel> detailModels) {
		List<String> ccEmailIds = null;
		if (null != detailModels) {
			if (null != detailModels && !detailModels.isEmpty()) {
				ccEmailIds = new ArrayList<String>();
				for (EmailDistributionListDetailModel model : detailModels) {
					if (null != model.getIsCC() && model.getIsCC() != 0) {
						if (null != model.getUserId()) {
							ccEmailIds.add(model.getUserId().getUserEmailId());
						}
						if (null != model.getVendorId()) {
							ccEmailIds.add(model.getVendorId().getEmailId());
						}
					}
				}
			}
		}
		return ccEmailIds;
	}

	public static List<String> packBCCEmailIds(
			List<EmailDistributionListDetailModel> detailModels) {
		List<String> bccEmailIds = null;
		if (null != detailModels) {
			if (null != detailModels && !detailModels.isEmpty()) {
				bccEmailIds = new ArrayList<String>();
				for (EmailDistributionListDetailModel model : detailModels) {
					if (null != model.getIsBCC() && model.getIsBCC() != 0) {
						if (null != model.getUserId()) {
							bccEmailIds.add(model.getUserId().getUserEmailId());
						}
						if (null != model.getVendorId()) {
							bccEmailIds.add(model.getVendorId().getEmailId());
						}
					}
				}
			}
		}
		return bccEmailIds;
	}
	
	public static List<String> packToCCEmailIds(List<EmailDistributionListDetailModel> detailModels)
	{
		List<String> toCCEmailIds = null;
		if (null != detailModels)
		{
			if (null != detailModels && !detailModels.isEmpty())
			{
				toCCEmailIds = new ArrayList<String>();
				for (EmailDistributionListDetailModel model : detailModels)
				{
					if ((model.getIsTo() != null && model.getIsTo() != 0) || (model.getIsCC() != null && model.getIsCC() != 0))
					{
						if (null != model.getUserId())
						{
							toCCEmailIds.add(model.getUserId().getUserEmailId());
						}
						if (null != model.getVendorId())
						{
							toCCEmailIds.add(model.getVendorId().getEmailId());
						}
					}
				}
			}
		}
		return toCCEmailIds;
	}

	public static String[] splitEmailIds(String address) {
		String results[] = null;
		try {
			if (null != address && !address.isEmpty()) {
				if (address.contains(";")) {
					results = address.split(";");
				} else {
					results = address.split(";");
				}
			}
		} catch (NullPointerException e) {
			PrintExceptionInLogFile.printException(e);
		}
		return results;
	}

	/**
	 * Check if a String is a number or not
	 * 
	 * @param number
	 * @return
	 */
	public static boolean isNumber(String number) {
		Pattern pattern = Pattern.compile(".*[^0-9.E].*");
		return pattern.matcher(number).matches();

	}

	/**
	 * replace the single single apostrophe (' -- \') for performing the query
	 * 
	 * @param value
	 * @return
	 */
	public static String escape(String value) {
		if (value.contains("'")) {
			return value.replace("'", "\\'");
		}
		return value;
	}

	/**
	 * Convert date to string.
	 * 
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String convertDateTimeToString(Date date) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				"dd-MM-yyyy HH-mm-ss");
		if (date != null) {
			return sdf.format(date);
		} else
			return "";
	}
	
	public static String convertDateTimeToStringFormated(Date date) {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				"dd-MM-yyyy");
		if (date != null) {
			return sdf.format(date);
		} else
			return "";
	}
	
	public static void generateJasperCSVFile(UserDetailsDto appDetails, HttpServletResponse response, String reportName, String jasperName, List<SearchVendorDto> getVendorSearchReportList)
	{
		try
		{
			OutputStream servletOutputStream = null;
			
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/csv");			
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".csv");

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getVendorSearchReportList);
			
			InputStream is = JRLoader.getResourceInputStream(jasperName);
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, null, beanCollectionDataSource);
			
			servletOutputStream = httpServletResponse.getOutputStream();
			
			//Coding For CSV
			JRCsvExporter csvExporter = new JRCsvExporter();
			csvExporter.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);
			csvExporter.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, servletOutputStream);
		    csvExporter.setParameter(JRCsvExporterParameter.IGNORE_PAGE_MARGINS, true);
		    csvExporter.setParameter(JRCsvExporterParameter.OUTPUT_FILE_NAME, reportName);
		    csvExporter.exportReport();
			
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
	}
	
	public static String stringArrayToString(String[] value){
		if (value.length > 0) {
		    StringBuilder nameBuilder = new StringBuilder();

		    for (String n : value) {
		    	nameBuilder.append(n.trim()).append(",");
		        //nameBuilder.append("'").append(n.replace("'", "\\'")).append("',");
		        // can also do the following
		        // nameBuilder.append("'").append(n.replace("'", "''")).append("',");
		    }

		    nameBuilder.deleteCharAt(nameBuilder.length() - 1);

		    return nameBuilder.toString();
		} else {
		    return "";
		}
	}
	
	public static String integerArrayToString(Integer[] value){
		if (value.length > 0) {
		    StringBuilder nameBuilder = new StringBuilder();

		    for (Integer n : value) {
		    	nameBuilder.append(n).append(",");
		        //nameBuilder.append("'").append(n.replace("'", "\\'")).append("',");
		        // can also do the following
		        // nameBuilder.append("'").append(n.replace("'", "''")).append("',");
		    }

		    nameBuilder.deleteCharAt(nameBuilder.length() - 1);

		    return nameBuilder.toString();
		} else {
		    return "";
		}
	}
}
