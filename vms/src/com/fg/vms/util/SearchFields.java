/**
 * 
 */
package com.fg.vms.util;

/**
 * Represents list of fields for vendor search.
 * 
 * @author pirabu
 * 
 */
public enum SearchFields {

	LEGALCOMPANYNAME(1, "Legal Company Name", "VENDORNAME","",""),
	COMPANYCODE(2,"Company Code", "VENDORCODE","",""),
	DUNSNUMBER(3, "Duns Number","DUNSNUMBER","",""), 
	TAXID(4, "Tax ID", "TAXID","",""),
	ANNUALREVENUE(5,"Annual Revenue", "ANNUALTURNOVER","",""), 
	NUMBEROFEMPLOYEES(6,"Number of Employees", "NUMBEROFEMPLOYEES","",""),
	COMPANYTYPE(7,"Company Type", "COMPANY_TYPE","",""),
	YEARESTABLISHED(8,"Year Established", "YEAROFESTABLISHMENT","",""), 
	
		ADDRESS(9, "Address","ADDRESS1","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		CITY(10, "City", "CITY","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		STATE(11, "State", "STATE","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		PROVINCE(12, "Province", "PROVINCE","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		REGION(13, "Region", "REGION","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		COUNTRY(14, "Country", "COUNTRY","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		ZIPCODE(15, "Zip Code", "ZIPCODE","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		MOBILE(16, "Mobile", "MOBILE","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		PHONE(17, "Phone", "PHONE","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		FAX(18,"Fax", "FAX","Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID",""),
		
	WEBSITEURL(19, "Website URL", "WEBSITE","",""),
	EMAIL(20,"Email", "EMAILID","",""),
	NAICSCODE(21, "NAICS Code", "NAICSCODE","Left Join customer_vendornaics On customer_vendornaics.VENDORID = customer_vendormaster.ID","Inner Join customer_naicscode On customer_vendornaics.NAICSID = customer_naicscode.NAICSID"),
	NAICSDESC(22, "NAICS Desc", "NAICSDESCRIPTION","Left Join customer_vendornaics On customer_vendornaics.VENDORID = customer_vendormaster.ID","Inner Join customer_naicscode On customer_vendornaics.NAICSID = customer_naicscode.NAICSID"), 
	CAPABILITIES(23,"Capabilities", "CAPABILITIES","Left Join customer_vendornaics On customer_vendornaics.VENDORID = customer_vendormaster.ID",""),
	
	DIVERSECLASSIFICATION(24,"Diverse Classification", "","Left Join customer_vendordiverseclassifcation on customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID",
			"inner Join customer_certificatemaster On customer_vendordiverseclassifcation.CERTMASTERID = customer_certificatemaster.ID"),
			
	CERTIFYINGAGENCY(25,"Certifying Agency", "","Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID",
			"Inner Join customer_certificateagencymaster On customer_vendorcertificate.CERTAGENCYID =  customer_certificateagencymaster.ID"),
	
	
	
	CERTIFICATION(26, "  Certification #","CERTIFICATENUMBER","Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID",""),
	EFFECTIVEDATE(27, "Effective Date", "","Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID",""),
	EXPIREDATE(28, "Expire Date", "","Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID",""),
	
	DIVERSESUPPLIER(29, "Diverse Supplier", "","",""),
			MINORITYOWNERSHIP(30, "% of Minority Ownership", "","",""),
			WOMENOWNERSHIP(31,"% of Women Ownership", "","",""), 
			DIVERSITYCLASSIFICATIONNOTES(32,"Diversity Classification Notes","", "",""),
			FIRSTNAME(33, "First Name","",	"Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""),
			LASTNAME(34, "Last Name", "","Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""),
			DESIGNATION(35, "Designation","","Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""),
			CONTACTPHONE(36, "Contact Phone", "","Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""),
			CONTACTMOBILE(37,"Contact Mobile", "","Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""),
			CONTACTFAX(38, "Contact Fax", "","Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""),
			CONTACTEMAIL(39, "Contact Email", "","Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID",""), 
			DISPLAYNAME(40, "Display Name", "","",""),
	VENDORCLASSIFICATION(41, "VENDOR CLASSIFICATION","","",""),
	BUSINESSAREA(42,"Business Area","","Left Join vendor_businessarea On vendor_businessarea.VENDORID = customer_vendormaster.ID",""),
	COMMODITYDESCRIPTION(43,"Commodity Description","","Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID",""),
	
	CERTIFICATIONTYPE(44,"Certification Type","","Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID",
			"Left Join customer_vendordiverseclassifcation on customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID  AND customer_vendordiverseclassifcation.ACTIVE=1"
			+" INNER JOIN customer_vendorcertificate certificate ON certificate.VENDORID=customer_vendormaster.id AND customer_vendordiverseclassifcation.CERTMASTERID=certificate.CERTMASTERID AND certificate.VENDORID=customer_vendordiverseclassifcation.VENDORID"
			+" INNER JOIN customer_classification_certificatetypes ON certificate.CERTMASTERID=customer_classification_certificatetypes.CERTIFICATEID AND customer_vendordiverseclassifcation.CERTMASTERID=customer_classification_certificatetypes.CERTIFICATEID "), 
	KEYWORD(45,"Keyword","","inner join customer_vendorkeywords  on customer_vendorkeywords.VENDORID = customer_vendormaster.id",""),
	VENDORCOMMODITY(46,"BP Commodity","","Left Join customer_vendorcommodity On customer_vendorcommodity.VENDORID = customer_vendormaster.ID",""),
	CONTACTFIRSTNAME(47, "Contact First Name", "", "INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id", ""),
	CONTACTLASTNAME(48, "Contact Last Name", "", "INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id", ""),
	CONTACTDATE(49, "Contact Date", "", "INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id", ""),
	CONTACTSTATE(50, "Contact State", "", "INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id", ""),
	CONTACTEVENT(51, "Contact Event", "", "INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id", ""),
	BPSEGMENT(52, "BP Segment", "", "", ""),
	OILANDGASINDUSTRYEXPERIENCE(53, "Oil and Gas Industry Experience", "", "INNER JOIN customer_vendorbusinessbiographysafety ON customer_vendorbusinessbiographysafety.VENDORID = customer_vendormaster.ID AND customer_vendorbusinessbiographysafety.QUESTIONNUMBER = 1 AND customer_vendorbusinessbiographysafety.SEQUENCE = 1", ""),
	BPMARKETSECTOR(54, "BP Market Sector", "", "INNER JOIN customer_vendorcommodity cvcinfo ON cvcinfo.VENDORID = customer_vendormaster.ID INNER JOIN customer_commodity ccinfo ON ccinfo.ID = cvcinfo.COMMODITYID INNER JOIN customer_commoditycategory cccinfo ON cccinfo.ID = ccinfo.COMMODITYCATEGORYID INNER JOIN customer_marketsector cmsinfo ON cmsinfo.ID = cccinfo.MARKETSECTORID", ""),
	YEAR(55, "Spend Year", "Tier2Report","",""),
	REPORTINGPERIOD(56, "Reporting Period", "Tier2Report","",""),
	VENDORNAME(57, "Vendor Name", "Tier2Report","",""), RFIRFPNOTE(58, "RFI RFP Note", "Tier2Report","","");
	
	private int index;
	private String desc;
	private String dbFieldName;
	private String fromTable1;
	private String fromTable2;
	

	private SearchFields(int index, String desc, String dbFieldName ,String fromTable1,String fromTable2) {
		this.index = index;
		this.desc = desc;
		this.dbFieldName = dbFieldName;
		this.fromTable1=fromTable1;
		this.fromTable2=fromTable2;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the dbFieldName
	 */
	public String getDbFieldName() {
		return dbFieldName;
	}

	/**
	 * @param dbFieldName
	 *            the dbFieldName to set
	 */
	public void setDbFieldName(String dbFieldName) {
		this.dbFieldName = dbFieldName;
	}

	public String getFromTable1() {
		return fromTable1;
	}

	public void setFromTable1(String fromTable1) {
		this.fromTable1 = fromTable1;
	}

	public String getFromTable2() {
		return fromTable2;
	}

	public void setFromTable2(String fromTable2) {
		this.fromTable2 = fromTable2;
	}
 
}
