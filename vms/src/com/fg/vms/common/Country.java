/**
 * Country.java
 */
package com.fg.vms.common;

import java.io.Serializable;

/**
 * The Class Country.
 * 
 * @author pirabu
 */

public class Country implements Comparable<Country>, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The iso. */
    private String iso;

    /** The name. */
    private String name;

    /** The code. */
    private String code;

    /**
     * Instantiates a new country.
     * 
     * @param iso
     *            the iso
     * @param code
     *            the code
     * @param name
     *            the name
     */
    public Country(String iso, String code, String name) {
	super();
	this.iso = iso;
	this.code = code;
	this.name = name;
    }

    /**
     * Gets the iso.
     * 
     * @return the iso
     */
    public String getIso() {
	return iso;
    }

    /**
     * Sets the iso.
     * 
     * @param iso
     *            the iso to set
     */
    public void setIso(String iso) {
	this.iso = iso;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * Gets the code.
     * 
     * @return the code
     */
    public String getCode() {
	return code;
    }

    /**
     * Sets the code.
     * 
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
	this.code = code;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((code == null) ? 0 : code.hashCode());
	result = prime * result + ((iso == null) ? 0 : iso.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (!(obj instanceof Country)) {
	    return false;
	}
	Country other = (Country) obj;
	if (code == null) {
	    if (other.code != null) {
		return false;
	    }
	} else if (!code.equals(other.code)) {
	    return false;
	}
	if (iso == null) {
	    if (other.iso != null) {
		return false;
	    }
	} else if (!iso.equals(other.iso)) {
	    return false;
	}
	if (name == null) {
	    if (other.name != null) {
		return false;
	    }
	} else if (!name.equals(other.name)) {
	    return false;
	}
	return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Country country) {
	return this.getName().compareTo(country.name);
    }

}
