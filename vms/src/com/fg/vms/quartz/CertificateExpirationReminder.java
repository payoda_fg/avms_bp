/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.quartz;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ProactiveMonitoringDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.ProactiveMonitoringDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.util.Constants;

/**
 * 
 * @author wjirawong
 */
@DisallowConcurrentExecution
public class CertificateExpirationReminder implements Job
{
	public void execute(JobExecutionContext jec) throws JobExecutionException 
    {
		final Logger log = Constants.logger;
		log.info("Inside CertificateExpirationReminder Job Class.");
		System.out.println("Inside CertificateExpirationReminder Job Class. Thread Name:" + Thread.currentThread().getName());

        JobDataMap jdMap = jec.getJobDetail().getJobDataMap();
        String appRoot = (String) jdMap.get("appRoot");
        UserDetailsDto userDetail = (UserDetailsDto) jdMap.get("userDetails");
 
        WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
        WorkflowConfiguration workflowConfiguration = configuration.listWorkflowConfig(userDetail);
        
        if (workflowConfiguration.getCertificateEmail() == 1) 
        {
        	log.info("Calling notifyCertificateExpiration Method.");
            ProactiveMonitoringDao proactiveMonitoringDao = new ProactiveMonitoringDaoImpl();
            proactiveMonitoringDao.notifyCertificateExpiration(userDetail, workflowConfiguration, appRoot);
        }
    }
}