package com.fg.vms.quartz;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
//import org.joda.time.LocalDate;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.fg.vms.admin.dao.CustomerDao;
import com.fg.vms.admin.dao.impl.CustomerDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.Tier2ReportingPeriodDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.util.Constants;
import com.fg.vms.util.FrequencyPeriod;

/**
 * This is class used to trigger the job to send the email alert. such as report
 * due, certificate expiration date. Cron Expression: second minute hour
 * dayofmonth monthdayof week year
 * 
 * @author pirabu
 * 
 */
@DisallowConcurrentExecution
public class AvmsSchedulerJob implements Job
{
	/** Logger of the system. */
	private Logger logger = Constants.logger;
	
	@Override
	public void execute(JobExecutionContext jec) throws JobExecutionException 
	{
		System.out.println("Thread Active Count:" + Thread.activeCount());		
		System.out.println("Thread Name:" + Thread.currentThread().getName());		
		System.out.println("doing simple job." + jec.getJobDetail());

		logger.info("------- Initializing -------------------");
		JobDataMap jdMap = jec.getJobDetail().getJobDataMap();
		CustomerDao customerDao = new CustomerDaoImpl();
		String appRoot = (String) jdMap.get("appRoot");

		// MASTER DATABASE DETAILS -- FG
		UserDetailsDto userDetail = new UserDetailsDto();
		userDetail.setDbUsername(Constants.getString(Constants.DB_USERNAME));
		userDetail.setDbPassword(Constants.getString(Constants.DB_PASSWORD));
		userDetail.setDbName(Constants.getString(Constants.MASTER_DB_NAME));
		userDetail.setHibernateCfgFileName("hibernate.cfg.xml");
		userDetail.setUserType("FG");

		// It holds the list of active customer details
		List<Customer> customers = customerDao.displaySingleCustomer(userDetail);
		for (Customer customer : customers) 
		{
			// Customer database details
			UserDetailsDto appDetail = new UserDetailsDto();
			appDetail.setCustomer(customer);
			appDetail.setDbName(customer.getDatabaseName());
			appDetail.setDatabaseIp(customer.getDatabaseIp());
			appDetail.setDbUsername(customer.getUserName());
			appDetail.setDbPassword(customer.getDbpassword());
			appDetail.setHibernateCfgFileName("hibernate.customer.cfg.xml");
			appDetail.setUserType("customerOrVendor");
			
			CustomerContacts contact = customerDao.findCustomerContact(appDetail);
			appDetail.setCurrentCustomer(contact);
			
			// Job for Notify Report Due based on reporting period.
			JobDetail job = newJob(ProactiveAlertJob.class).withIdentity(customer.getCustCode() + "notifyReportDue", 
							customer.getId().toString()).build();
			job.getJobDataMap().put("customer", customer);
			job.getJobDataMap().put("userDetails", appDetail);

			// delete unused self reg data..
			/*JobDetail deleteUnusedDataJob = newJob(DeleteUnusedDataJob.class).withIdentity(customer.getCustCode() + "deleteUnusedDataJob",
							customer.getId().toString()).build();
			deleteUnusedDataJob.getJobDataMap().put("customer", customer);
			deleteUnusedDataJob.getJobDataMap().put("userDetails", appDetail);*/

			// Job for Notify Certificate Expiration, before the no of days based on work flow configuration.
			JobDetail certificateJob = newJob(CertificateExpirationReminder.class).withIdentity(customer.getCustCode() + "certificateExpirationAlert", 
							customer.getId().toString()).build();
			certificateJob.getJobDataMap().put("customer", customer);
			certificateJob.getJobDataMap().put("appRoot", appRoot);
			certificateJob.getJobDataMap().put("userDetails", appDetail);

			// Job for Notify Report not submitted, after report due the no of days based on work flow configuration.
			JobDetail reportNotSubmittedJob = newJob(ReportNotSubmittedJob.class).withIdentity(customer.getCustCode() + "reportNotSubmitted",
							customer.getId().toString()).build();
			reportNotSubmittedJob.getJobDataMap().put("customer", customer);
			reportNotSubmittedJob.getJobDataMap().put("userDetails", appDetail);
			
			// ----- Added Certificate Expiry Notification Monthly Report Starts Here -----			
			JobDetail certificateExpiryReportJob = newJob(CertificateExpiryNotificationMonthlyReport.class).withIdentity(customer.getCustCode() + "certificateExpiryNotificationMonthlyReport",
							customer.getId().toString()).build();
			certificateExpiryReportJob.getJobDataMap().put("customer", customer);
			certificateExpiryReportJob.getJobDataMap().put("userDetails", appDetail);
			// ----- Added Certificate Expiry Notification Monthly Report Ends Here -----

			/*ProactiveMonitoringDao proactiveMonitoringDao = new ProactiveMonitoringDaoImpl();
			ProactiveMonitoringCfg cfg = proactiveMonitoringDao.retrive(appDetail);*/
			
			WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration workflowConfiguration = configuration.listWorkflowConfig(appDetail);

			Tier2ReportingPeriodDao reportingPeriod = new Tier2ReportingPeriodDaoImpl();
			Tier2ReportingPeriod period = reportingPeriod.currentReportingPeriod(appDetail);
			
			// List<String> reportPeriods = CommonUtils
			// .calculateReportingPeriods(period);
			String reportDueTiggerTime = null;
			int priorDays = workflowConfiguration.getDaysUpload();
			if (period != null) 
			{
				switch (FrequencyPeriod.valueOf(period.getDataUploadFreq())) 
				{
					case M:
						Calendar cal = new GregorianCalendar();
						Calendar temp = new GregorianCalendar();
						temp.setTime(period.getEndDate());
						cal.set(Calendar.DATE, temp.get(Calendar.DATE) - priorDays);
						String day = String.valueOf(cal.get(Calendar.DATE));
						//reportDueTiggerTime = "0 22 20 " + day + " * ?";
						reportDueTiggerTime = "0 0 3 " + day + " * ?";
						break;
						
					case Q:
						Calendar calq = new GregorianCalendar();
						Calendar tempq = new GregorianCalendar();
						tempq.setTime(period.getEndDate());
						calq.set(Calendar.DATE, tempq.get(Calendar.DATE) - priorDays);
						String day1 = String.valueOf(calq.get(Calendar.DATE));
						//reportDueTiggerTime = "0 22 20 " + day1 + " 3/3 ?";
						reportDueTiggerTime = "0 0 3 " + day1 + " 3/3 ?";
						break;
						
					case A:
						Calendar cala = new GregorianCalendar();
						Calendar tempa = new GregorianCalendar();
						tempa.setTime(period.getEndDate());
						cala.set(Calendar.DATE, tempa.get(Calendar.DATE) - priorDays);
						String day2 = String.valueOf(cala.get(Calendar.DATE));
						//reportDueTiggerTime = "0 22 20 " + day2 + " +" + cala.get(Calendar.MONTH) + " ?";
						reportDueTiggerTime = "0 0 3 " + day2 + " +" + cala.get(Calendar.MONTH) + " ?";
						break;
				}
			}
			
			try
			{
				Properties prop = new Properties();
				prop.setProperty("org.quartz.threadPool.threadCount", "1");

				Scheduler scheduler = new StdSchedulerFactory(prop).getScheduler();
				//Scheduler scheduler = new StdSchedulerFactory().getScheduler();
				
				/*if (cfg != null)
				{
					String startDate = CommonUtils.convertDateToString(cfg != null ? cfg.getRangeStartDate() != null ? cfg.getRangeStartDate() : new Date() : new Date());
					String tiggerTime = "";
					
					// if recurrence pattern is monthly, then trigger the job based on month config.
					if (cfg.getReccurrencePattern() == 2)
					{
						if (cfg.getMonthlyOption() == 0)
						{
							Date date = new Date();
							date.setDate(cfg.getMonthlyDayNumber());
							String day = String.valueOf(date.getDate());
							//tiggerTime = "0 30 18 " + day + " * ?";
							tiggerTime = "0 0 4 " + day + " * ?";
						}
						else
						{
							Date date = new Date();
							int week = CommonUtils.getWeekNumber(cfg, "month");
							LocalDate day = CommonUtils.getNthOfMonth(cfg.getMonthlyWeekDay(), 1, date.getYear(), week);
							//tiggerTime = "0 30 18 " + day.getDayOfMonth() + " * ?";
							tiggerTime = "0 0 4 " + day.getDayOfMonth() + " * ?";
						}
					}
					// if recurrence pattern is yearly, then trigger the job based on yearly config.
					else if (cfg.getReccurrencePattern() == 3)
					{
						if (cfg.getYearlyOption() == 0)
						{
							Date date = new Date();
							date.setDate(cfg.getYearlyDayNumber());
							String day = String.valueOf(date.getDate());
							//tiggerTime = "0 30 18 " + day + " " + cfg.getYearlyMonthNumber() + " ?";
							tiggerTime = "0 0 4 " + day + " " + cfg.getYearlyMonthNumber() + " ?";
						}
						else
						{
							int week = CommonUtils.getWeekNumber(cfg, "year");
							Date date = new Date();
							date.setDate(cfg.getMonthlyDayNumber());
							LocalDate day = CommonUtils.getNthOfMonth(cfg.getYearlyWeekDayNumber(), 1, date.getYear(), week);
							//tiggerTime = "0 30 18 " + day.getDayOfMonth() + " " + cfg.getYearlyMonthNumber() + " * ?";
							tiggerTime = "0 0 4 " + day.getDayOfMonth() + " " + cfg.getYearlyMonthNumber() + " * ?";
						}
					}

					// if the no end date, then we not specify the trigger end date.
					Trigger reportDueTrigger = null;					
					if (cfg.getRangeOccurenceOption() == 0)
					{
						reportDueTrigger = newTrigger().withIdentity(customer.getCustCode(), customer.getId().toString())
								.startAt(new Date(startDate)).withSchedule(CronScheduleBuilder.cronSchedule(tiggerTime)).build();
					}
					// if the End after no of Occurrence, then create the trigger with end after no of Occurrence.
					else if (cfg.getRangeOccurenceOption() == 1)
					{
						reportDueTrigger = newTrigger().withIdentity(customer.getCustCode(), customer.getId().toString())
								.startAt(new Date(startDate)).withSchedule(CronScheduleBuilder.cronSchedule(tiggerTime)).build();
					}
					// if the End date, then create the trigger with end date.
					else
					{
						reportDueTrigger = newTrigger().withIdentity(customer.getCustCode(), customer.getId().toString())
								.startAt(new Date(startDate)).withSchedule(CronScheduleBuilder.cronSchedule(tiggerTime)).endAt(cfg.getOccurenceEndate()).build();
					}

					Date ft = scheduler.scheduleJob(reportNotSubmittedJob, reportDueTrigger);

					logger.info(reportNotSubmittedJob.getKey() + " has been scheduled to run at: " + ft
							+ " and repeat based on expression: " + ((CronTrigger) reportDueTrigger).getCronExpression());
				}*/

				// Create trigger based on specified expression and Range of recurrence.
				Trigger trigger = null;
				trigger = newTrigger().withIdentity(customer.getCustName() + "before").withSchedule(CronScheduleBuilder.cronSchedule(reportDueTiggerTime)).build();

				Date ft = scheduler.scheduleJob(job, trigger);

				logger.info(job.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: " + ((CronTrigger) trigger).getCronExpression());
				
				// Trigger for Vendors Who has Not Submitted their Report Even after Due Date is Completed (ReportNotSubmittedJob).
				Integer reportNotSubmittedReminderDay = workflowConfiguration.getReportNotSubmittedReminderDate();
				if(reportNotSubmittedReminderDay != null && reportNotSubmittedReminderDay != 0)
				{
				/*	Trigger reportNotSubmittedTrigger = newTrigger().withIdentity(customer.getCustName() + "reportNotSubmittedJob1")
							.withSchedule(CronScheduleBuilder.cronSchedule("0 0 4 " + reportNotSubmittedReminderDay + " * ?")).build();*/
					
					Trigger reportNotSubmittedTrigger = newTrigger().withIdentity(customer.getCustName() + "reportNotSubmittedJob1")
							.withSchedule(CronScheduleBuilder.cronSchedule("0 0 4 * * ?")).build();
					
					ft = scheduler.scheduleJob(reportNotSubmittedJob, reportNotSubmittedTrigger);
					
					logger.info(reportNotSubmittedJob.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: " + ((CronTrigger) reportNotSubmittedTrigger).getCronExpression());
				}

				// Trigger for Notify Certificate Expiration before the no of days based on work flow configuration.
				Trigger certTrigger = newTrigger().withIdentity(customer.getCustName() + "certificateJob1")
						.withSchedule(CronScheduleBuilder.cronSchedule("0 0 5 ? * *")).build();
						//.withSchedule(CronScheduleBuilder.cronSchedule("0 30 18 ? * *")).build();

				ft = scheduler.scheduleJob(certificateJob, certTrigger);

				logger.info(certificateJob.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: " + ((CronTrigger) certTrigger).getCronExpression());
			 
				// ----- Added Certificate Expiry Notification Monthly Report Starts Here -----
				Trigger certExpiryReportTiger = newTrigger().withIdentity(customer.getCustName() + "certificateExpiryReportJob1")
						.withSchedule(CronScheduleBuilder.cronSchedule("0 0 6 1 * ?")).build();

				ft = scheduler.scheduleJob(certificateExpiryReportJob, certExpiryReportTiger);
				
				logger.info(certificateExpiryReportJob.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: " + ((CronTrigger) certExpiryReportTiger).getCronExpression());
				// ----- Added Certificate Expiry Notification Monthly Report Ends Here -----				
				
				//Trigger for Delete Unused Data
				/*Trigger deleteUnusedDataTrigger = newTrigger().withIdentity(customer.getCustName() + "deleteUnusedDataTrigger")
						.withSchedule(CronScheduleBuilder.cronSchedule("0 30 18 ? * *")).build();

				ft = scheduler.scheduleJob(deleteUnusedDataJob,	deleteUnusedDataTrigger);

				logger.info(deleteUnusedDataJob.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: " + ((CronTrigger) deleteUnusedDataTrigger).getCronExpression());*/

				logger.info("------- Starting Scheduler ----------------");
				scheduler.start();
				logger.info("------- Started Scheduler -----------------");
			}
			catch (SchedulerException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.out.println(e1);
			}
		}
	}
}