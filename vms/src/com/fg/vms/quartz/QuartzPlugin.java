/**
 * 
 */
package com.fg.vms.quartz;

import java.util.Properties;

import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author gpirabu
 * 
 */
public class QuartzPlugin implements PlugIn
{
	Scheduler scheduler;
	
	@Override
	public void destroy()
	{		
		try 
		{
			if(scheduler.isStarted())
			{
				scheduler.shutdown(true);
				System.out.println("Quartz Scheduler Destroyed.");
			}
			else
			{
				System.out.println("Scheduler isn't Started.");
			}			
		}
		catch (SchedulerException e)
		{
			e.printStackTrace();
			System.out.println("Expection Occured: " + e);
		}
	}

	@Override
	public void init(ActionServlet servlet, ModuleConfig config) throws ServletException
	{
		JobDetail job = JobBuilder.newJob(AvmsSchedulerJob.class).withIdentity("anyJobName", "group1").build();

		try
		{
		    String appRoot = servlet.getServletContext().getRealPath("") + "/";
		    job.getJobDataMap().put("appRoot", appRoot);
			Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity("SimpleJob")
					/*.withSchedule(CronScheduleBuilder.cronSchedule("0 0/3 * * * ?"))*/
					.build();

			Properties prop = new Properties();
			prop.setProperty("org.quartz.threadPool.threadCount", "1");
			
			scheduler = new StdSchedulerFactory(prop).getScheduler();			
			//Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}