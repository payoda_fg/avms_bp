/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.quartz;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Customer;
import com.fg.vms.customer.dao.ProactiveMonitoringDao;
import com.fg.vms.customer.dao.impl.ProactiveMonitoringDaoImpl;
import com.fg.vms.util.Constants;

/**
 * 
 * @author pirabu
 */
@DisallowConcurrentExecution
public class ReportNotSubmittedJob implements Job
{
    public void execute(JobExecutionContext jec) throws JobExecutionException 
    {
    	final Logger log = Constants.logger;
    	log.info("Inside ReportNotSubmittedJob Job Class.");
    	System.out.println("Inside ReportNotSubmittedJob Job Class. Thread Name:" + Thread.currentThread().getName());
    	
        JobDataMap jdMap = jec.getJobDetail().getJobDataMap();
        Customer customer = (Customer) jdMap.get("customer");
        UserDetailsDto userDetail = (UserDetailsDto) jdMap.get("userDetails");

        ProactiveMonitoringDao proactiveMonitoringDao = new ProactiveMonitoringDaoImpl();

        System.out.println("doing simple job.: " + customer.getCustName());
        log.info("Calling notifyReportDueNotSubmitted Method.");
        proactiveMonitoringDao.notifyReportDueNotSubmitted(userDetail);
    }
}