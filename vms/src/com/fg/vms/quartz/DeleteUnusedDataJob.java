package com.fg.vms.quartz;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fg.vms.admin.model.Customer;
import com.fg.vms.util.Constants;

@DisallowConcurrentExecution
public class DeleteUnusedDataJob implements Job
{
    public void execute(JobExecutionContext jec) throws JobExecutionException
    {
    	final Logger log = Constants.logger;
		log.info("Inside DeleteUnusedDataJob Job Class.");
		System.out.println("Inside DeleteUnusedDataJob Job Class. Thread Name:" + Thread.currentThread().getName());
    	
        JobDataMap jdMap = jec.getJobDetail().getJobDataMap();
        Customer customer = (Customer) jdMap.get("customer");
        //UserDetailsDto userDetail = (UserDetailsDto) jdMap.get("userDetails");

        //ProactiveMonitoringDao proactiveMonitoringDao = new ProactiveMonitoringDaoImpl();

        System.out.println("doing simple job.: " + customer.getCustName());
        //proactiveMonitoringDao.notifyReportDue(userDetail);
    }
}