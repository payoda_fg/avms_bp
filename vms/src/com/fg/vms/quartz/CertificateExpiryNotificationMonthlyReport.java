package com.fg.vms.quartz;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ProactiveMonitoringDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.ProactiveMonitoringDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.util.Constants;

@DisallowConcurrentExecution
public class CertificateExpiryNotificationMonthlyReport implements Job
{
	@Override
	public void execute(JobExecutionContext jec) throws JobExecutionException 
	{
		final Logger log = Constants.logger;
		log.info("Inside CertificateExpiryNotificationMonthlyReport Job Class.");
		System.out.println("Inside CertificateExpiryNotificationMonthlyReport Job Class. Thread Name:" + Thread.currentThread().getName());
		
		JobDataMap jdMap = jec.getJobDetail().getJobDataMap();
        UserDetailsDto userDetails = (UserDetailsDto) jdMap.get("userDetails");
        
        WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
        WorkflowConfiguration workflowConfiguration = configuration.listWorkflowConfig(userDetails);
        if (workflowConfiguration.getCertificateEmailSummaryAlert() != null && workflowConfiguration.getCertificateEmailSummaryAlert() == 1) 
        {
        	log.info("Calling certificateExpiryNotificationMonthlyReport Method.");
            ProactiveMonitoringDao proactiveMonitoringDao = new ProactiveMonitoringDaoImpl();
		    proactiveMonitoringDao.certificateExpiryNotificationMonthlyReport(userDetails);
        }
	}
}