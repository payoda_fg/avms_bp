/*
 * SessionFilter.java
 */
package com.fg.vms.filter;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.model.VendorContact;

/**
 * Validate the current user is alive or not when request the action .
 * 
 * @author pirabu
 */
public class SecurityFilter implements Filter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public SecurityFilter() {
	}

	/**
	 * Destroy.
	 * 
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * Do filter.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * @param chain
	 *            the chain
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ServletException
	 *             the servlet exception
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		LoginDao loginDao = new LoginDaoImpl();

		System.out.println("Request Url: "+request.getRequestURL());
		System.out.println("Request Uri: "+request.getRequestURI());
		
		HttpSession session = request.getSession(false);
		// check whether session is time out or not
		if (null == session
				&& !(request.getRequestURI().contains("/home.do") || request
						.getRequestURI().contains(
								"viewcustomeruserregistration.do"))) {
			response.sendRedirect("error_page.jsp");
			return;
		}
		/*
		 * Check the user is alive in current session or not.If the current user
		 * is deleted by admin , redirect to some login/invalid page. the
		 * current user object represent the customer user or fg user and
		 * vendorUser represent the the vendor user.
		 */
		if (request.getRequestURI().contains("/home.do")
				|| request.getRequestURI().contains("login.do")
				|| request.getRequestURI().contains("forgot.do")
				|| request.getRequestURI().contains("reset.do")
				|| request.getRequestURI().contains(
						"viewcustomeruserregistration.do")) {
			/* common request for all the user */

			chain.doFilter(request, response);

		} else if (null != session
				&& null != session.getAttribute("currentUser")) {
			Users currentUser = (Users) session.getAttribute("currentUser");
			Users user = loginDao.authenticate(currentUser.getUserEmailId(),
					(UserDetailsDto) session.getAttribute("userDetails"));
			if (user == null) {
				session.invalidate();
				response.sendRedirect("index.jsp");
				return;
			}
			chain.doFilter(request, response);
		} else if (null != session
				&& null != session.getAttribute("vendorUser")) {
			VendorContact currentUser = (VendorContact) session
					.getAttribute("vendorUser");
			VendorContact vendorUser = loginDao.authenticateVendor(
					currentUser.getEmailId(),
					(UserDetailsDto) session.getAttribute("userDetails"));
			if (vendorUser == null) {
				session.invalidate();
				response.sendRedirect("index.jsp");
				return;
			}
			chain.doFilter(request, response);

		} else if (null != session && session.getAttribute("userType") != null
				&& session.getAttribute("userType").equals("customerOrVendor")) {
			/* anonymous vendors registration comes only customer user */
			if (request.getRequestURI().contains("viewanonymousvendors.do")
					|| request.getRequestURI().contains("logout.do")
					|| request.getRequestURI().contains(
							"createanonymousvendor.do")
					|| request.getRequestURI().contains("selfregistration.do")
					|| request.getRequestURI().contains("retrieveselfreg.do")
					|| request.getRequestURI().contains("selfvendornavigation.do")
					|| request.getRequestURI().contains("resetselfpassword.do")
					|| request.getRequestURI().contains("selfregcontactinfo.do")
					|| request.getRequestURI().contains("selfregcompanyinfo.do")
					|| request.getRequestURI().contains("selfregaddressinfo.do")
					|| request.getRequestURI().contains("selfregownership.do")
					|| request.getRequestURI().contains("selfregservicearea.do")
					|| request.getRequestURI().contains("selfregbusinessarea.do")
					|| request.getRequestURI().contains("selfregdiversecertificate.do")
					|| request.getRequestURI().contains("selfregbiography.do")
					|| request.getRequestURI().contains("selfregreferences.do")
					|| request.getRequestURI().contains("selfothercertificate.do")
					|| request.getRequestURI().contains("selfregdocuments.do")
					|| request.getRequestURI().contains("selfregmeetinginfo.do")
					|| request.getRequestURI().contains("selfregsubmit.do")
					|| request.getRequestURI().contains("selfprimevendor.do")
					|| request.getRequestURI().contains("retrieveprimereg.do")
					|| request.getRequestURI().contains(
							"geographicalsettings.do")
					|| request.getRequestURI().contains("commoditycategory.do")
					|| request.getRequestURI().contains(
							"customerloginapproval.do")
					|| request.getRequestURI().contains("NaicsCode.do")
					|| request.getRequestURI()
							.contains("ajaxclassification.do")
					|| request.getRequestURI().contains(
							"viewsubvendorcontact.do")
					|| request.getRequestURI().contains("approval.do")
					|| request.getRequestURI().contains("naicscode.do")
					|| request.getRequestURI().contains("state.do")
					|| request.getRequestURI().contains("deletenaics.do")
					|| request.getRequestURI().contains("deletevendorcert.do")
					|| request.getRequestURI().contains("deleteothercert.do")
					|| request.getRequestURI().contains("meetinginfo.do")
					|| request.getRequestURI().contains("deletecommodity.do")
					|| request.getRequestURI().contains("deletedoc.do")
					|| request.getRequestURI().contains("deleteKeyword.do")
					|| request.getRequestURI().contains(
							"viewcustomeruserregistration.do")
					|| request.getRequestURI().contains(
							"customeruserregistration.do")
					|| request.getRequestURI().contains("savecustomeruser.do")
					|| request.getRequestURI().contains("validationServlet")) {
				chain.doFilter(request, response);
			}else {
				response.sendRedirect("index.jsp");
				return;
			}
		} else if (request.getRequestURI().contains("approval.do")) {
			chain.doFilter(request, response);
		} else if (request.getRequestURI().contains("viewanonymousvendors.do")) {
			chain.doFilter(request, response);
		} else if (request.getRequestURI().contains("support")) {
			response.sendRedirect(request.getContextPath() + "/index.jsp");
			return;
		} else {
			response.sendRedirect("index.jsp");
			return;
		}
	}

	/**
	 * Inits the.
	 * 
	 * @param fConfig
	 *            the f config
	 * @throws ServletException
	 *             the servlet exception
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
