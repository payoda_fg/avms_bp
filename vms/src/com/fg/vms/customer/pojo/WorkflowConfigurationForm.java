/*
 * WorkflowConfigurationForm.java 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * The Workflow configuration POJO class.
 * 
 * @author vinoth
 * 
 */
public class WorkflowConfigurationForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The WorkflowConfigId. */
	private Integer workflowConfigId;

	/** The naicsCodeAM. */
	private Integer naicsCodeAM;

	/** The followWF. */
	private Integer followWF;

	/** The distributionEmail. */
	private Integer distributionEmail;

	/** The assessmentRequired. */
	private Integer assessmentRequired;

	/** The assessmentCompleted. */
	private Integer assessmentCompleted;

	/** The approvalEmail. */
	private Integer approvalEmail;

	/** The certificateEmail. */
	private Integer certificateEmail;

	/** The distributionEmailPercent. */
	private Double distributionEmailPercent;

	/** The assessmentRequiredPercent. */
	private Double assessmentRequiredPercent;

	/** The assessmentCompletedPercent. */
	private Double assessmentCompletedPercent;

	/** The approvalEmailPercent. */
	private Double approvalEmailPercent;

	/** The emailDistributionMasterId. */
	private Integer emailDistributionMasterId;

	/** The daysUpload. */
	private Integer daysUpload;

	/** The daysExpiry. */
	private Integer daysExpiry;
	private Integer reportDueDays;

	private Integer documentUpload;

	/** The daysInactive. */
	private Integer daysInactive;
	private Integer tier2ReportEmailDist;

	private Integer adminGroup;

	private Integer documentSize;

	private Integer internationalMode;

	private Integer defaultCountry;

	private Integer bpSegment;

	private String status;

	private Integer certificateEmailSummaryAlert;

	private Integer reportNotSubmittedReminderDate;

	private String ccMailId;

	private String supportMailId;
	
	private Integer isDisplaySDF;

	public Integer getIsDisplaySDF() {
		return isDisplaySDF;
	}

	public void setIsDisplaySDF(Integer isDisplaySDF) {
		this.isDisplaySDF = isDisplaySDF;
	}

	/**
	 * @return the workflowConfigId
	 */
	public Integer getWorkflowConfigId() {
		return workflowConfigId;
	}

	/**
	 * @param workflowConfigId
	 *            the workflowConfigId to set
	 */
	public void setWorkflowConfigId(Integer workflowConfigId) {
		this.workflowConfigId = workflowConfigId;
	}

	/**
	 * @return the naicsCodeAM
	 */
	public Integer getNaicsCodeAM() {
		return naicsCodeAM;
	}

	/**
	 * @param naicsCodeAM
	 *            the naicsCodeAM to set
	 */
	public void setNaicsCodeAM(Integer naicsCodeAM) {
		this.naicsCodeAM = naicsCodeAM;
	}

	/**
	 * @return the followWF
	 */
	public Integer getFollowWF() {
		return followWF;
	}

	/**
	 * @param followWF
	 *            the followWF to set
	 */
	public void setFollowWF(Integer followWF) {
		this.followWF = followWF;
	}

	/**
	 * @return the distributionEmail
	 */
	public Integer getDistributionEmail() {
		return distributionEmail;
	}

	/**
	 * @param distributionEmail
	 *            the distributionEmail to set
	 */
	public void setDistributionEmail(Integer distributionEmail) {
		this.distributionEmail = distributionEmail;
	}

	/**
	 * @return the assessmentRequired
	 */
	public Integer getAssessmentRequired() {
		return assessmentRequired;
	}

	/**
	 * @param assessmentRequired
	 *            the assessmentRequired to set
	 */
	public void setAssessmentRequired(Integer assessmentRequired) {
		this.assessmentRequired = assessmentRequired;
	}

	/**
	 * @return the assessmentCompleted
	 */
	public Integer getAssessmentCompleted() {
		return assessmentCompleted;
	}

	/**
	 * @param assessmentCompleted
	 *            the assessmentCompleted to set
	 */
	public void setAssessmentCompleted(Integer assessmentCompleted) {
		this.assessmentCompleted = assessmentCompleted;
	}

	/**
	 * @return the approvalEmail
	 */
	public Integer getApprovalEmail() {
		return approvalEmail;
	}

	/**
	 * @param approvalEmail
	 *            the approvalEmail to set
	 */
	public void setApprovalEmail(Integer approvalEmail) {
		this.approvalEmail = approvalEmail;
	}

	/**
	 * @return the certificateEmail
	 */
	public Integer getCertificateEmail() {
		return certificateEmail;
	}

	/**
	 * @param certificateEmail
	 *            the certificateEmail to set
	 */
	public void setCertificateEmail(Integer certificateEmail) {
		this.certificateEmail = certificateEmail;
	}

	/**
	 * @return the distributionEmailPercent
	 */
	public Double getDistributionEmailPercent() {
		return distributionEmailPercent;
	}

	/**
	 * @param distributionEmailPercent
	 *            the distributionEmailPercent to set
	 */
	public void setDistributionEmailPercent(Double distributionEmailPercent) {
		this.distributionEmailPercent = distributionEmailPercent;
	}

	/**
	 * @return the assessmentRequiredPercent
	 */
	public Double getAssessmentRequiredPercent() {
		return assessmentRequiredPercent;
	}

	/**
	 * @param assessmentRequiredPercent
	 *            the assessmentRequiredPercent to set
	 */
	public void setAssessmentRequiredPercent(Double assessmentRequiredPercent) {
		this.assessmentRequiredPercent = assessmentRequiredPercent;
	}

	/**
	 * @return the assessmentCompletedPercent
	 */
	public Double getAssessmentCompletedPercent() {
		return assessmentCompletedPercent;
	}

	/**
	 * @param assessmentCompletedPercent
	 *            the assessmentCompletedPercent to set
	 */
	public void setAssessmentCompletedPercent(Double assessmentCompletedPercent) {
		this.assessmentCompletedPercent = assessmentCompletedPercent;
	}

	/**
	 * @return the approvalEmailPercent
	 */
	public Double getApprovalEmailPercent() {
		return approvalEmailPercent;
	}

	/**
	 * @param approvalEmailPercent
	 *            the approvalEmailPercent to set
	 */
	public void setApprovalEmailPercent(Double approvalEmailPercent) {
		this.approvalEmailPercent = approvalEmailPercent;
	}

	/**
	 * @return the emailDistributionMasterId
	 */
	public Integer getEmailDistributionMasterId() {
		return emailDistributionMasterId;
	}

	/**
	 * @param emailDistributionMasterId
	 *            the emailDistributionMasterId to set
	 */
	public void setEmailDistributionMasterId(Integer emailDistributionMasterId) {
		this.emailDistributionMasterId = emailDistributionMasterId;
	}

	/**
	 * @return the daysUpload
	 */
	public Integer getDaysUpload() {
		return daysUpload;
	}

	/**
	 * @param daysUpload
	 *            the daysUpload to set
	 */
	public void setDaysUpload(Integer daysUpload) {
		this.daysUpload = daysUpload;
	}

	/**
	 * @return the daysExpiry
	 */
	public Integer getDaysExpiry() {
		return daysExpiry;
	}

	/**
	 * @param daysExpiry
	 *            the daysExpiry to set
	 */
	public void setDaysExpiry(Integer daysExpiry) {
		this.daysExpiry = daysExpiry;
	}

	/**
	 * @return the daysInactive
	 */
	public Integer getDaysInactive() {
		return daysInactive;
	}

	/**
	 * @param daysInactive
	 *            the daysInactive to set
	 */
	public void setDaysInactive(Integer daysInactive) {
		this.daysInactive = daysInactive;
	}

	/**
	 * @return the reportDueDays
	 */
	public Integer getReportDueDays() {
		return reportDueDays;
	}

	/**
	 * @param reportDueDays
	 *            the reportDueDays to set
	 */
	public void setReportDueDays(Integer reportDueDays) {
		this.reportDueDays = reportDueDays;
	}

	/**
	 * @return the documentUpload
	 */
	public Integer getDocumentUpload() {
		return documentUpload;
	}

	/**
	 * @param documentUpload
	 *            the documentUpload to set
	 */
	public void setDocumentUpload(Integer documentUpload) {
		this.documentUpload = documentUpload;
	}

	public Integer getTier2ReportEmailDist() {
		return tier2ReportEmailDist;
	}

	public void setTier2ReportEmailDist(Integer tier2ReportEmailDist) {
		this.tier2ReportEmailDist = tier2ReportEmailDist;
	}

	public Integer getAdminGroup() {
		return adminGroup;
	}

	public void setAdminGroup(Integer adminGroup) {
		this.adminGroup = adminGroup;
	}

	/**
	 * @return the documentSize
	 */
	public Integer getDocumentSize() {
		return documentSize;
	}

	/**
	 * @param documentSize
	 *            the documentSize to set
	 */
	public void setDocumentSize(Integer documentSize) {
		this.documentSize = documentSize;
	}

	/**
	 * @return the internationalMode
	 */
	public Integer getInternationalMode() {
		return internationalMode;
	}

	/**
	 * @param internationalMode
	 *            the internationalMode to set
	 */
	public void setInternationalMode(Integer internationalMode) {
		this.internationalMode = internationalMode;
	}

	/**
	 * @return the defaultCountry
	 */
	public Integer getDefaultCountry() {
		return defaultCountry;
	}

	/**
	 * @param defaultCountry
	 *            the defaultCountry to set
	 */
	public void setDefaultCountry(Integer defaultCountry) {
		this.defaultCountry = defaultCountry;
	}

	/**
	 * @return the bpSegment
	 */
	public Integer getBpSegment() {
		return bpSegment;
	}

	/**
	 * @param bpSegment
	 *            the bpSegment to set
	 */
	public void setBpSegment(Integer bpSegment) {
		this.bpSegment = bpSegment;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the certificateEmailSummaryAlert
	 */
	public Integer getCertificateEmailSummaryAlert() {
		return certificateEmailSummaryAlert;
	}

	/**
	 * @param certificateEmailSummaryAlert
	 *            the certificateEmailSummaryAlert to set
	 */
	public void setCertificateEmailSummaryAlert(
			Integer certificateEmailSummaryAlert) {
		this.certificateEmailSummaryAlert = certificateEmailSummaryAlert;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the reportNotSubmittedReminderDate
	 */
	public Integer getReportNotSubmittedReminderDate() {
		return reportNotSubmittedReminderDate;
	}

	/**
	 * @param reportNotSubmittedReminderDate
	 *            the reportNotSubmittedReminderDate to set
	 */
	public void setReportNotSubmittedReminderDate(
			Integer reportNotSubmittedReminderDate) {
		this.reportNotSubmittedReminderDate = reportNotSubmittedReminderDate;
	}
	
	public String getCcMailId() {
		return ccMailId;
	}

	public void setCcMailId(String ccMailId) {
		this.ccMailId = ccMailId;
	}

	public String getSupportMailId() {
		return supportMailId;
	}

	public void setSupportMailId(String supportMailId) {
		this.supportMailId = supportMailId;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.naicsCodeAM = null;
		this.followWF = null;
		this.distributionEmail = null;
		this.assessmentRequired = null;
		this.assessmentCompleted = null;
		this.approvalEmail = null;
		this.certificateEmail = null;
		this.distributionEmailPercent = null;
		this.assessmentRequiredPercent = null;
		this.assessmentCompletedPercent = null;
		this.approvalEmailPercent = null;
		this.emailDistributionMasterId = null;
		this.reportDueDays = null;
		this.documentUpload = null;
		this.tier2ReportEmailDist = null;
		this.documentSize = null;
		this.internationalMode = null;
		this.defaultCountry = null;
		this.bpSegment = null;
		this.status = null;
		this.certificateEmailSummaryAlert = null;
		this.reportNotSubmittedReminderDate = null;
		this.ccMailId = null;
		this.supportMailId = null;
	}
}