/*
 * ItemForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * 
 * Represents the Item Details (eg. itemId,itemDescription,itemCode) for
 * interacting with UI
 * 
 * @author hemavaishnavi
 * 
 */
public class ItemForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private String itemId;

    private String itemDescription;

    private String itemCode;

    private String itemCategory;

    private String itemManufacturer;

    private String itemUom;

    private Byte isActive;

    private Date createdOn;

    private Integer createdBy;

    private Date modifiedOn;

    private Integer modifiedBy;

    /**
     * @return the itemId
     */
    public String getItemId() {
	return itemId;
    }

    /**
     * @param itemId
     *            the itemId to set
     */
    public void setItemId(String itemId) {
	this.itemId = itemId;
    }

    /**
     * @return the itemDescription
     */
    public String getItemDescription() {
	return itemDescription;
    }

    /**
     * @param itemDescription
     *            the itemDescription to set
     */
    public void setItemDescription(String itemDescription) {
	this.itemDescription = itemDescription;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
	return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(String itemCode) {
	this.itemCode = itemCode;
    }

    /**
     * @return the itemCategory
     */
    public String getItemCategory() {
	return itemCategory;
    }

    /**
     * @param itemCategory
     *            the itemCategory to set
     */
    public void setItemCategory(String itemCategory) {
	this.itemCategory = itemCategory;
    }

    /**
     * @return the itemManufacturer
     */
    public String getItemManufacturer() {
	return itemManufacturer;
    }

    /**
     * @param itemManufacturer
     *            the itemManufacturer to set
     */
    public void setItemManufacturer(String itemManufacturer) {
	this.itemManufacturer = itemManufacturer;
    }

    /**
     * @return the itemUom
     */
    public String getItemUom() {
	return itemUom;
    }

    /**
     * @param itemUom
     *            the itemUom to set
     */
    public void setItemUom(String itemUom) {
	this.itemUom = itemUom;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.itemDescription = "";
	this.itemCode = "";
	this.itemCategory = "";
	this.itemManufacturer = "";
	this.itemUom = "";
    }
}
