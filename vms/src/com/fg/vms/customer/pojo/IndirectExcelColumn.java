package com.fg.vms.customer.pojo;

import java.io.Serializable;


/**
 * @author Kaleswararao
 *
 */
public class IndirectExcelColumn implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String indirectLegalCompanyName;
	private String indirectAddress;
	private String indirectCity;
	private String indirectState;
	private Integer indirectZipcode;
	private String indirectFirstName;
	private String indirectLastName;
	private String indirectEmail;
	private Long indirectPhone;
	private String[] indirectDiversity;
	private String indirectEthanicity;
	private Double indirectAmount;
	private String country;
	private String diverseSupplier;
	private String diverstiyClassification;
	/**
	 * @return the indirectLegalCompanyName
	 */
	public String getIndirectLegalCompanyName() {
		return indirectLegalCompanyName;
	}
	/**
	 * @param indirectLegalCompanyName the indirectLegalCompanyName to set
	 */
	public void setIndirectLegalCompanyName(String indirectLegalCompanyName) {
		this.indirectLegalCompanyName = indirectLegalCompanyName;
	}
	/**
	 * @return the indirectAddress
	 */
	public String getIndirectAddress() {
		return indirectAddress;
	}
	/**
	 * @param indirectAddress the indirectAddress to set
	 */
	public void setIndirectAddress(String indirectAddress) {
		this.indirectAddress = indirectAddress;
	}
	/**
	 * @return the indirectCity
	 */
	public String getIndirectCity() {
		return indirectCity;
	}
	/**
	 * @param indirectCity the indirectCity to set
	 */
	public void setIndirectCity(String indirectCity) {
		this.indirectCity = indirectCity;
	}
	/**
	 * @return the indirectState
	 */
	public String getIndirectState() {
		return indirectState;
	}
	/**
	 * @param indirectState the indirectState to set
	 */
	public void setIndirectState(String indirectState) {
		this.indirectState = indirectState;
	}
	/**
	 * @return the indirectZipcode
	 */
	public Integer getIndirectZipcode() {
		return indirectZipcode;
	}
	/**
	 * @param indirectZipcode the indirectZipcode to set
	 */
	public void setIndirectZipcode(Integer indirectZipcode) {
		this.indirectZipcode = indirectZipcode;
	}
	/**
	 * @return the indirectFirstName
	 */
	public String getIndirectFirstName() {
		return indirectFirstName;
	}
	/**
	 * @param indirectFirstName the indirectFirstName to set
	 */
	public void setIndirectFirstName(String indirectFirstName) {
		this.indirectFirstName = indirectFirstName;
	}
	/**
	 * @return the indirectLastName
	 */
	public String getIndirectLastName() {
		return indirectLastName;
	}
	/**
	 * @param indirectLastName the indirectLastName to set
	 */
	public void setIndirectLastName(String indirectLastName) {
		this.indirectLastName = indirectLastName;
	}
	/**
	 * @return the indirectEmail
	 */
	public String getIndirectEmail() {
		return indirectEmail;
	}
	/**
	 * @param indirectEmail the indirectEmail to set
	 */
	public void setIndirectEmail(String indirectEmail) {
		this.indirectEmail = indirectEmail;
	}
	/**
	 * @return the indirectPhone
	 */
	public Long getIndirectPhone() {
		return indirectPhone;
	}
	/**
	 * @param indirectPhone the indirectPhone to set
	 */
	public void setIndirectPhone(Long indirectPhone) {
		this.indirectPhone = indirectPhone;
	}
	/**
	 * @return the indirectDiversity
	 */
	public String[] getIndirectDiversity() {
		return indirectDiversity;
	}
	/**
	 * @param indirectDiversity the indirectDiversity to set
	 */
	public void setIndirectDiversity(String[] indirectDiversity) {
		this.indirectDiversity = indirectDiversity;
	}
	/**
	 * @return the indirectEthanicity
	 */
	public String getIndirectEthanicity() {
		return indirectEthanicity;
	}
	/**
	 * @param indirectEthanicity the indirectEthanicity to set
	 */
	public void setIndirectEthanicity(String indirectEthanicity) {
		this.indirectEthanicity = indirectEthanicity;
	}
	/**
	 * @return the indirectAmount
	 */
	public Double getIndirectAmount() {
		return indirectAmount;
	}
	/**
	 * @param indirectAmount the indirectAmount to set
	 */
	public void setIndirectAmount(Double indirectAmount) {
		this.indirectAmount = indirectAmount;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the diverseSupplier
	 */
	public String getDiverseSupplier() {
		return diverseSupplier;
	}
	/**
	 * @param diverseSupplier the diverseSupplier to set
	 */
	public void setDiverseSupplier(String diverseSupplier) {
		this.diverseSupplier = diverseSupplier;
	}
	/**
	 * @return the diverstiyClassification
	 */
	public String getDiverstiyClassification() {
		return diverstiyClassification;
	}
	/**
	 * @param diverstiyClassification the diverstiyClassification to set
	 */
	public void setDiverstiyClassification(String diverstiyClassification) {
		this.diverstiyClassification = diverstiyClassification;
	}
}
