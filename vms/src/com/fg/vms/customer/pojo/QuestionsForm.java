/*
 * QuestionsForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.Template;

/**
 * Class to hold the form fields which associated to the question.jsp page.
 * 
 * @author srinivasarao
 * 
 */

public class QuestionsForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String templateName;

    private String template;

    private List<Template> templateNames;

    private String[] questions;

    private String checkQuestions;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the templateName
     */
    public String getTemplateName() {
	return templateName;
    }

    /**
     * @param templateName
     *            the templateName to set
     */
    public void setTemplateName(String templateName) {
	this.templateName = templateName;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
	return template;
    }

    /**
     * @param template
     *            the template to set
     */
    public void setTemplate(String template) {
	this.template = template;
    }

    /**
     * @return the templateNames
     */
    public List<Template> getTemplateNames() {
	return templateNames;
    }

    /**
     * @param templateNames
     *            the templateNames to set
     */
    public void setTemplateNames(List<Template> templateNames) {
	this.templateNames = templateNames;
    }

    /**
     * @return the questions
     */
    public String[] getQuestions() {
	return questions;
    }

    /**
     * @param questions
     *            the questions to set
     */
    public void setQuestions(String[] questions) {
	this.questions = questions;
    }

    /**
     * @return the checkQuestions
     */
    public String getCheckQuestions() {
	return checkQuestions;
    }

    /**
     * @param checkQuestions
     *            the checkQuestions to set
     */
    public void setCheckQuestions(String checkQuestions) {
	this.checkQuestions = checkQuestions;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.templateName = "";
    }

}
