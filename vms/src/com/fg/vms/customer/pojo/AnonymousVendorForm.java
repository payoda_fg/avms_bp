/* 
 * AnonymousVendorForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.common.Country;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.model.NAICSubCategory;

/**
 * Represents the Anonymous vendor Details (eg.vendorName, vendorAddress,
 * reference details) for interact with UI
 * 
 * @author vinoth
 * 
 */
public class AnonymousVendorForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The current vendor id. */
	private Integer currentVendorId;

	/** The vendor name. */
	private String vendorName;

	/** The vendor code. */
	private String vendorCode;

	/** The duns number. */
	private String dunsNumber;

	/** The tax id. */
	private String taxId;

	/** The address1. */
	private String address1;

	/** The address2. */
	private String address2;

	/** The address3. */
	private String address3;

	/** The city. */
	private String city;

	/** The state. */
	private String state;

	/** The province. */
	private String province;

	/** The zipcode. */
	private String zipcode;

	/** The region. */
	private String region;

	/** The country. */
	private String country;

	/** The phone. */
	private String phone;

	/** The mobile. */
	private String mobile;

	/** The fax. */
	private String fax;

	/** The email id. */
	private String emailId;

	/** The website. */
	private String website;

	/** The companytype. */
	private String companytype;

	/** The number of employees. */
	private int numberOfEmployees;

	/** The annual turnover. */
	private String annualTurnover;

	/** The annual turnover format. */
	private String annualTurnoverFormat;

	/** The year of establishment. */
	private Integer yearOfEstablishment;

	/** The naics code_1. */
	private String naicsCode_1;

	/** The certificates. */
	private Integer[] certificates;

	/** The div cert type. */
	private Integer divCertType;

	/** The div cert agen. */
	private Integer divCertAgen;

	/** The exp date. */
	private String expDate;

	/** The cert file. */
	private FormFile certFile;

	/** The cert id. */
	private Integer certId;

	/** The diverse supplier. */
	private String diverseSupplier;

	/** The naics desc. */
	private String naicsDesc;

	/** The isic code. */
	private String isicCode;

	/** The isic desc. */
	private String isicDesc;

	/** The prime non prime vendor. */
	private Byte primeNonPrimeVendor;

	/** The parent vendor id. */
	private int parentVendorId;

	/** The is approved. */
	private Byte isApproved;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The designation. */
	private String designation;

	/** The contact phone. */
	private String contactPhone;

	/** The contact mobile. */
	private String contactMobile;

	/** The contact fax. */
	private String contactFax;

	/** The contanct email. */
	private String contanctEmail;

	/** The reference name1. */
	private String referenceName1;

	/** The reference address1. */
	private String referenceAddress1;

	/** The reference phone1. */
	private String referencePhone1;

	/** The reference mail id1. */
	private String referenceMailId1;

	/** The reference mobile1. */
	private String referenceMobile1;

	/** The reference city1. */
	private String referenceCity1;

	/** The reference zip1. */
	private String referenceZip1;

	/** The reference state1. */
	private String referenceState1;

	/** The reference country1. */
	private String referenceCountry1;

	/** The reference name2. */
	private String referenceName2;

	/** The reference address2. */
	private String referenceAddress2;

	/** The reference phone2. */
	private String referencePhone2;

	/** The reference mail id2. */
	private String referenceMailId2;

	/** The reference mobile2. */
	private String referenceMobile2;

	/** The reference city2. */
	private String referenceCity2;

	/** The reference zip2. */
	private String referenceZip2;

	/** The reference state2. */
	private String referenceState2;

	/** The reference country2. */
	private String referenceCountry2;

	/** The reference name3. */
	private String referenceName3;

	/** The reference address3. */
	private String referenceAddress3;

	/** The reference phone3. */
	private String referencePhone3;

	/** The reference mail id3. */
	private String referenceMailId3;

	/** The reference mobile3. */
	private String referenceMobile3;

	/** The reference city3. */
	private String referenceCity3;

	/** The reference zip3. */
	private String referenceZip3;

	/** The reference state3. */
	private String referenceState3;

	/** The reference country3. */
	private String referenceCountry3;

	/** The category. */
	private String category;

	/** The sub category. */
	private String subCategory;

	/** The categories. */
	private List<NaicsCategoryDto> categories;

	/** The sub categories. */
	private List<NAICSubCategory> subCategories;

	/** The diverse quality. */
	private String diverseQuality;

	/** The selected ids. */
	private String selectedIds[];

	/** The naicslastrowcount. */
	private Integer naicslastrowcount;

	/** The lastrowcount. */
	private int lastrowcount;

	/** The countries. */
	private List<Country> countries;

	/** The vendors list. */
	List<SearchVendorDto> vendorsList;

	/** The capabilities. */
	private String capabilities;

	/** The hidden email id. */
	private String hiddenEmailId;

	/** The ethnicity. */
	private String ethnicity;

	private String username;

	private String password;

	private String confirmPassword;

	private String minorityPercent;

	private String womenPercent;

	private String classificationNotes;

	/** The certificationNo1. */
	private String certificationNo1;

	/** The eff date1. */
	private String effDate1;

	/** The diverseCertificateName. */
	private Integer diverseCertificateName;

	/** The userName. */
	private String userName;

	/** The passwordTemp. */
	private String passwordTemp;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the passwordTemp
	 */
	public String getPasswordTemp() {
		return passwordTemp;
	}

	/**
	 * @param passwordTemp
	 *            the passwordTemp to set
	 */
	public void setPasswordTemp(String passwordTemp) {
		this.passwordTemp = passwordTemp;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *            the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the hidden email id.
	 * 
	 * @return the hidden email id
	 */
	public String getHiddenEmailId() {
		return hiddenEmailId;
	}

	/**
	 * Sets the hidden email id.
	 * 
	 * @param hiddenEmailId
	 *            the new hidden email id
	 */
	public void setHiddenEmailId(String hiddenEmailId) {
		this.hiddenEmailId = hiddenEmailId;
	}

	/**
	 * Gets the reference city1.
	 * 
	 * @return the referenceCity1
	 */
	public String getReferenceCity1() {
		return referenceCity1;
	}

	/**
	 * Sets the reference city1.
	 * 
	 * @param referenceCity1
	 *            the referenceCity1 to set
	 */
	public void setReferenceCity1(String referenceCity1) {
		this.referenceCity1 = referenceCity1;
	}

	/**
	 * Gets the reference state1.
	 * 
	 * @return the referenceState1
	 */
	public String getReferenceState1() {
		return referenceState1;
	}

	/**
	 * Sets the reference state1.
	 * 
	 * @param referenceState1
	 *            the referenceState1 to set
	 */
	public void setReferenceState1(String referenceState1) {
		this.referenceState1 = referenceState1;
	}

	/**
	 * Gets the reference country1.
	 * 
	 * @return the referenceCountry1
	 */
	public String getReferenceCountry1() {
		return referenceCountry1;
	}

	/**
	 * Sets the reference country1.
	 * 
	 * @param referenceCountry1
	 *            the referenceCountry1 to set
	 */
	public void setReferenceCountry1(String referenceCountry1) {
		this.referenceCountry1 = referenceCountry1;
	}

	/**
	 * Gets the reference city2.
	 * 
	 * @return the referenceCity2
	 */
	public String getReferenceCity2() {
		return referenceCity2;
	}

	/**
	 * Sets the reference city2.
	 * 
	 * @param referenceCity2
	 *            the referenceCity2 to set
	 */
	public void setReferenceCity2(String referenceCity2) {
		this.referenceCity2 = referenceCity2;
	}

	/**
	 * Gets the reference state2.
	 * 
	 * @return the referenceState2
	 */
	public String getReferenceState2() {
		return referenceState2;
	}

	/**
	 * Sets the reference state2.
	 * 
	 * @param referenceState2
	 *            the referenceState2 to set
	 */
	public void setReferenceState2(String referenceState2) {
		this.referenceState2 = referenceState2;
	}

	/**
	 * Gets the reference country2.
	 * 
	 * @return the referenceCountry2
	 */
	public String getReferenceCountry2() {
		return referenceCountry2;
	}

	/**
	 * Sets the reference country2.
	 * 
	 * @param referenceCountry2
	 *            the referenceCountry2 to set
	 */
	public void setReferenceCountry2(String referenceCountry2) {
		this.referenceCountry2 = referenceCountry2;
	}

	/**
	 * Gets the reference city3.
	 * 
	 * @return the referenceCity3
	 */
	public String getReferenceCity3() {
		return referenceCity3;
	}

	/**
	 * Sets the reference city3.
	 * 
	 * @param referenceCity3
	 *            the referenceCity3 to set
	 */
	public void setReferenceCity3(String referenceCity3) {
		this.referenceCity3 = referenceCity3;
	}

	/**
	 * Gets the reference zip1.
	 * 
	 * @return the referenceZip1
	 */
	public String getReferenceZip1() {
		return referenceZip1;
	}

	/**
	 * Sets the reference zip1.
	 * 
	 * @param referenceZip1
	 *            the referenceZip1 to set
	 */
	public void setReferenceZip1(String referenceZip1) {
		this.referenceZip1 = referenceZip1;
	}

	/**
	 * Gets the reference zip2.
	 * 
	 * @return the referenceZip2
	 */
	public String getReferenceZip2() {
		return referenceZip2;
	}

	/**
	 * Sets the reference zip2.
	 * 
	 * @param referenceZip2
	 *            the referenceZip2 to set
	 */
	public void setReferenceZip2(String referenceZip2) {
		this.referenceZip2 = referenceZip2;
	}

	/**
	 * Gets the reference zip3.
	 * 
	 * @return the referenceZip3
	 */
	public String getReferenceZip3() {
		return referenceZip3;
	}

	/**
	 * Sets the reference zip3.
	 * 
	 * @param referenceZip3
	 *            the referenceZip3 to set
	 */
	public void setReferenceZip3(String referenceZip3) {
		this.referenceZip3 = referenceZip3;
	}

	/**
	 * Gets the reference state3.
	 * 
	 * @return the referenceState3
	 */
	public String getReferenceState3() {
		return referenceState3;
	}

	/**
	 * Sets the reference state3.
	 * 
	 * @param referenceState3
	 *            the referenceState3 to set
	 */
	public void setReferenceState3(String referenceState3) {
		this.referenceState3 = referenceState3;
	}

	/**
	 * Gets the reference country3.
	 * 
	 * @return the referenceCountry3
	 */
	public String getReferenceCountry3() {
		return referenceCountry3;
	}

	/**
	 * Sets the reference country3.
	 * 
	 * @param referenceCountry3
	 *            the referenceCountry3 to set
	 */
	public void setReferenceCountry3(String referenceCountry3) {
		this.referenceCountry3 = referenceCountry3;
	}

	/**
	 * Gets the selected ids.
	 * 
	 * @return the selectedIds
	 */
	public String[] getSelectedIds() {
		return selectedIds;
	}

	/**
	 * Sets the selected ids.
	 * 
	 * @param selectedIds
	 *            the selectedIds to set
	 */
	public void setSelectedIds(String[] selectedIds) {
		this.selectedIds = selectedIds;
	}

	/**
	 * Gets the diverse quality.
	 * 
	 * @return the diverseQuality
	 */
	public String getDiverseQuality() {
		return diverseQuality;
	}

	/**
	 * Sets the diverse quality.
	 * 
	 * @param diverseQuality
	 *            the diverseQuality to set
	 */
	public void setDiverseQuality(String diverseQuality) {
		this.diverseQuality = diverseQuality;
	}

	/**
	 * Gets the prime non prime vendor.
	 * 
	 * @return the primeNonPrimeVendor
	 */
	public Byte getPrimeNonPrimeVendor() {
		return primeNonPrimeVendor;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the current vendor id.
	 * 
	 * @return the currentVendorId
	 */
	public Integer getCurrentVendorId() {
		return currentVendorId;
	}

	/**
	 * Sets the current vendor id.
	 * 
	 * @param currentVendorId
	 *            the currentVendorId to set
	 */
	public void setCurrentVendorId(Integer currentVendorId) {
		this.currentVendorId = currentVendorId;
	}

	/**
	 * Gets the vendor name.
	 * 
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * Sets the vendor name.
	 * 
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * Gets the vendor code.
	 * 
	 * @return the vendorCode
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * Sets the vendor code.
	 * 
	 * @param vendorCode
	 *            the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	/**
	 * Gets the duns number.
	 * 
	 * @return the dunsNumber
	 */
	public String getDunsNumber() {
		return dunsNumber;
	}

	/**
	 * Sets the duns number.
	 * 
	 * @param dunsNumber
	 *            the dunsNumber to set
	 */
	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	/**
	 * Gets the tax id.
	 * 
	 * @return the taxId
	 */
	public String getTaxId() {
		return taxId;
	}

	/**
	 * Sets the tax id.
	 * 
	 * @param taxId
	 *            the taxId to set
	 */
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	/**
	 * Gets the address1.
	 * 
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * Sets the address1.
	 * 
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * Gets the address2.
	 * 
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * Sets the address2.
	 * 
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * Gets the address3.
	 * 
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * Sets the address3.
	 * 
	 * @param address3
	 *            the address3 to set
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the state.
	 * 
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 * 
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the province.
	 * 
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 * 
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * Gets the zipcode.
	 * 
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Sets the zipcode.
	 * 
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * Gets the region.
	 * 
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Sets the region.
	 * 
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the phone.
	 * 
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 * 
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the mobile.
	 * 
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Sets the mobile.
	 * 
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Gets the fax.
	 * 
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax.
	 * 
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the email id.
	 * 
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * Sets the email id.
	 * 
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Gets the website.
	 * 
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Sets the website.
	 * 
	 * @param website
	 *            the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * Gets the companytype.
	 * 
	 * @return the companytype
	 */
	public String getCompanytype() {
		return companytype;
	}

	/**
	 * Sets the companytype.
	 * 
	 * @param companytype
	 *            the companytype to set
	 */
	public void setCompanytype(String companytype) {
		this.companytype = companytype;
	}

	/**
	 * Gets the number of employees.
	 * 
	 * @return the numberOfEmployees
	 */
	public int getNumberOfEmployees() {
		return numberOfEmployees;
	}

	/**
	 * Sets the number of employees.
	 * 
	 * @param numberOfEmployees
	 *            the numberOfEmployees to set
	 */
	public void setNumberOfEmployees(int numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	/**
	 * Gets the annual turnover.
	 * 
	 * @return the annualTurnover
	 */
	public String getAnnualTurnover() {
		return annualTurnover;
	}

	/**
	 * Sets the annual turnover.
	 * 
	 * @param annualTurnover
	 *            the annualTurnover to set
	 */
	public void setAnnualTurnover(String annualTurnover) {
		this.annualTurnover = annualTurnover;
	}

	/**
	 * Gets the annual turnover format.
	 * 
	 * @return the annualTurnoverFormat
	 */
	public String getAnnualTurnoverFormat() {
		return annualTurnoverFormat;
	}

	/**
	 * Sets the annual turnover format.
	 * 
	 * @param annualTurnoverFormat
	 *            the annualTurnoverFormat to set
	 */
	public void setAnnualTurnoverFormat(String annualTurnoverFormat) {
		this.annualTurnoverFormat = annualTurnoverFormat;
	}

	/**
	 * Gets the year of establishment.
	 * 
	 * @return the yearOfEstablishment
	 */
	public Integer getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	/**
	 * Sets the year of establishment.
	 * 
	 * @param yearOfEstablishment
	 *            the yearOfEstablishment to set
	 */
	public void setYearOfEstablishment(Integer yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	/**
	 * Gets the naics code_1.
	 * 
	 * @return the naicsCode_1
	 */
	public String getNaicsCode_1() {
		return naicsCode_1;
	}

	/**
	 * Sets the naics code_1.
	 * 
	 * @param naicsCode_1
	 *            the naicsCode_1 to set
	 */
	public void setNaicsCode_1(String naicsCode_1) {
		this.naicsCode_1 = naicsCode_1;
	}

	/**
	 * @return the certificates
	 */
	public Integer[] getCertificates() {
		return certificates;
	}

	/**
	 * @param certificates
	 *            the certificates to set
	 */
	public void setCertificates(Integer[] certificates) {
		this.certificates = certificates;
	}

	/**
	 * Gets the div cert type.
	 * 
	 * @return the divCertType
	 */
	public Integer getDivCertType() {
		return divCertType;
	}

	/**
	 * Sets the div cert type.
	 * 
	 * @param divCertType
	 *            the divCertType to set
	 */
	public void setDivCertType(Integer divCertType) {
		this.divCertType = divCertType;
	}

	/**
	 * Gets the div cert agen.
	 * 
	 * @return the divCertAgen
	 */
	public Integer getDivCertAgen() {
		return divCertAgen;
	}

	/**
	 * Sets the div cert agen.
	 * 
	 * @param divCertAgen
	 *            the divCertAgen to set
	 */
	public void setDivCertAgen(Integer divCertAgen) {
		this.divCertAgen = divCertAgen;
	}

	/**
	 * Gets the exp date.
	 * 
	 * @return the expDate
	 */
	public String getExpDate() {
		return expDate;
	}

	/**
	 * Sets the exp date.
	 * 
	 * @param expDate
	 *            the expDate to set
	 */
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	/**
	 * Gets the cert file.
	 * 
	 * @return the certFile
	 */
	public FormFile getCertFile() {
		return certFile;
	}

	/**
	 * Sets the cert file.
	 * 
	 * @param certFile
	 *            the certFile to set
	 */
	public void setCertFile(FormFile certFile) {
		this.certFile = certFile;
	}

	/**
	 * Gets the diverse supplier.
	 * 
	 * @return the deverseSupplier
	 */
	public String getDiverseSupplier() {
		return diverseSupplier;
	}

	/**
	 * Sets the diverse supplier.
	 * 
	 * @param diverseSupplier
	 *            the new diverse supplier
	 */
	public void setDiverseSupplier(String diverseSupplier) {
		this.diverseSupplier = diverseSupplier;
	}

	/**
	 * Gets the naics desc.
	 * 
	 * @return the naicsDesc
	 */
	public String getNaicsDesc() {
		return naicsDesc;
	}

	/**
	 * Sets the naics desc.
	 * 
	 * @param naicsDesc
	 *            the naicsDesc to set
	 */
	public void setNaicsDesc(String naicsDesc) {
		this.naicsDesc = naicsDesc;
	}

	/**
	 * Gets the isic code.
	 * 
	 * @return the isicCode
	 */
	public String getIsicCode() {
		return isicCode;
	}

	/**
	 * Sets the isic code.
	 * 
	 * @param isicCode
	 *            the isicCode to set
	 */
	public void setIsicCode(String isicCode) {
		this.isicCode = isicCode;
	}

	/**
	 * Gets the isic desc.
	 * 
	 * @return the isicDesc
	 */
	public String getIsicDesc() {
		return isicDesc;
	}

	/**
	 * Sets the isic desc.
	 * 
	 * @param isicDesc
	 *            the isicDesc to set
	 */
	public void setIsicDesc(String isicDesc) {
		this.isicDesc = isicDesc;
	}

	/**
	 * Checks if is prime non prime vendor.
	 * 
	 * @return the primeNonPrimeVendor
	 */
	public Byte isPrimeNonPrimeVendor() {
		return primeNonPrimeVendor;
	}

	/**
	 * Sets the prime non prime vendor.
	 * 
	 * @param primeNonPrimeVendor
	 *            the primeNonPrimeVendor to set
	 */
	public void setPrimeNonPrimeVendor(Byte primeNonPrimeVendor) {
		this.primeNonPrimeVendor = primeNonPrimeVendor;
	}

	/**
	 * Gets the parent vendor id.
	 * 
	 * @return the parentVendorId
	 */
	public int getParentVendorId() {
		return parentVendorId;
	}

	/**
	 * Sets the parent vendor id.
	 * 
	 * @param parentVendorId
	 *            the parentVendorId to set
	 */
	public void setParentVendorId(int parentVendorId) {
		this.parentVendorId = parentVendorId;
	}

	/**
	 * Gets the checks if is approved.
	 * 
	 * @return the isApproved
	 */
	public Byte getIsApproved() {
		return isApproved;
	}

	/**
	 * Sets the checks if is approved.
	 * 
	 * @param isApproved
	 *            the isApproved to set
	 */
	public void setIsApproved(Byte isApproved) {
		this.isApproved = isApproved;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 * 
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 * 
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the designation.
	 * 
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * Sets the designation.
	 * 
	 * @param designation
	 *            the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * Gets the contact phone.
	 * 
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * Sets the contact phone.
	 * 
	 * @param contactPhone
	 *            the contactPhone to set
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * Gets the contact mobile.
	 * 
	 * @return the contactMobile
	 */
	public String getContactMobile() {
		return contactMobile;
	}

	/**
	 * Sets the contact mobile.
	 * 
	 * @param contactMobile
	 *            the contactMobile to set
	 */
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	/**
	 * Gets the contact fax.
	 * 
	 * @return the contactFax
	 */
	public String getContactFax() {
		return contactFax;
	}

	/**
	 * Sets the contact fax.
	 * 
	 * @param contactFax
	 *            the contactFax to set
	 */
	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	/**
	 * Gets the contanct email.
	 * 
	 * @return the contanctEmail
	 */
	public String getContanctEmail() {
		return contanctEmail;
	}

	/**
	 * Sets the contanct email.
	 * 
	 * @param contanctEmail
	 *            the contanctEmail to set
	 */
	public void setContanctEmail(String contanctEmail) {
		this.contanctEmail = contanctEmail;
	}

	// /**
	// * @return the naicCategory
	// */
	// public String getNaicCategory() {
	// return naicCategory;
	// }
	//
	// /**
	// * @param naicCategory
	// * the naicCategory to set
	// */
	// public void setNaicCategory(String naicCategory) {
	// this.naicCategory = naicCategory;
	// }
	//
	// /**
	// * @return the naicSubCategory
	// */
	// public String getNaicSubCategory() {
	// return naicSubCategory;
	// }

	// /**
	// * @param naicSubCategory
	// * the naicSubCategory to set
	// */
	// public void setNaicSubCategory(String naicSubCategory) {
	// this.naicSubCategory = naicSubCategory;
	// }

	/**
	 * Gets the reference name1.
	 * 
	 * @return the referenceName1
	 */
	public String getReferenceName1() {
		return referenceName1;
	}

	/**
	 * Sets the reference name1.
	 * 
	 * @param referenceName1
	 *            the referenceName1 to set
	 */
	public void setReferenceName1(String referenceName1) {
		this.referenceName1 = referenceName1;
	}

	/**
	 * Gets the reference address1.
	 * 
	 * @return the referenceAddress1
	 */
	public String getReferenceAddress1() {
		return referenceAddress1;
	}

	/**
	 * Sets the reference address1.
	 * 
	 * @param referenceAddress1
	 *            the referenceAddress1 to set
	 */
	public void setReferenceAddress1(String referenceAddress1) {
		this.referenceAddress1 = referenceAddress1;
	}

	/**
	 * Gets the reference phone1.
	 * 
	 * @return the referencePhone1
	 */
	public String getReferencePhone1() {
		return referencePhone1;
	}

	/**
	 * Sets the reference phone1.
	 * 
	 * @param referencePhone1
	 *            the referencePhone1 to set
	 */
	public void setReferencePhone1(String referencePhone1) {
		this.referencePhone1 = referencePhone1;
	}

	/**
	 * Gets the reference mail id1.
	 * 
	 * @return the referenceMailId1
	 */
	public String getReferenceMailId1() {
		return referenceMailId1;
	}

	/**
	 * Sets the reference mail id1.
	 * 
	 * @param referenceMailId1
	 *            the referenceMailId1 to set
	 */
	public void setReferenceMailId1(String referenceMailId1) {
		this.referenceMailId1 = referenceMailId1;
	}

	/**
	 * Gets the reference mobile1.
	 * 
	 * @return the referenceMobile1
	 */
	public String getReferenceMobile1() {
		return referenceMobile1;
	}

	/**
	 * Sets the reference mobile1.
	 * 
	 * @param referenceMobile1
	 *            the referenceMobile1 to set
	 */
	public void setReferenceMobile1(String referenceMobile1) {
		this.referenceMobile1 = referenceMobile1;
	}

	/**
	 * Gets the reference name2.
	 * 
	 * @return the referenceName2
	 */
	public String getReferenceName2() {
		return referenceName2;
	}

	/**
	 * Sets the reference name2.
	 * 
	 * @param referenceName2
	 *            the referenceName2 to set
	 */
	public void setReferenceName2(String referenceName2) {
		this.referenceName2 = referenceName2;
	}

	/**
	 * Gets the reference address2.
	 * 
	 * @return the referenceAddress2
	 */
	public String getReferenceAddress2() {
		return referenceAddress2;
	}

	/**
	 * Sets the reference address2.
	 * 
	 * @param referenceAddress2
	 *            the referenceAddress2 to set
	 */
	public void setReferenceAddress2(String referenceAddress2) {
		this.referenceAddress2 = referenceAddress2;
	}

	/**
	 * Gets the reference phone2.
	 * 
	 * @return the referencePhone2
	 */
	public String getReferencePhone2() {
		return referencePhone2;
	}

	/**
	 * Sets the reference phone2.
	 * 
	 * @param referencePhone2
	 *            the referencePhone2 to set
	 */
	public void setReferencePhone2(String referencePhone2) {
		this.referencePhone2 = referencePhone2;
	}

	/**
	 * Gets the reference mail id2.
	 * 
	 * @return the referenceMailId2
	 */
	public String getReferenceMailId2() {
		return referenceMailId2;
	}

	/**
	 * Sets the reference mail id2.
	 * 
	 * @param referenceMailId2
	 *            the referenceMailId2 to set
	 */
	public void setReferenceMailId2(String referenceMailId2) {
		this.referenceMailId2 = referenceMailId2;
	}

	/**
	 * Gets the reference mobile2.
	 * 
	 * @return the referenceMobile2
	 */
	public String getReferenceMobile2() {
		return referenceMobile2;
	}

	/**
	 * Sets the reference mobile2.
	 * 
	 * @param referenceMobile2
	 *            the referenceMobile2 to set
	 */
	public void setReferenceMobile2(String referenceMobile2) {
		this.referenceMobile2 = referenceMobile2;
	}

	/**
	 * Gets the reference name3.
	 * 
	 * @return the referenceName3
	 */
	public String getReferenceName3() {
		return referenceName3;
	}

	/**
	 * Sets the reference name3.
	 * 
	 * @param referenceName3
	 *            the referenceName3 to set
	 */
	public void setReferenceName3(String referenceName3) {
		this.referenceName3 = referenceName3;
	}

	/**
	 * Gets the reference address3.
	 * 
	 * @return the referenceAddress3
	 */
	public String getReferenceAddress3() {
		return referenceAddress3;
	}

	/**
	 * Sets the reference address3.
	 * 
	 * @param referenceAddress3
	 *            the referenceAddress3 to set
	 */
	public void setReferenceAddress3(String referenceAddress3) {
		this.referenceAddress3 = referenceAddress3;
	}

	/**
	 * Gets the reference phone3.
	 * 
	 * @return the referencePhone3
	 */
	public String getReferencePhone3() {
		return referencePhone3;
	}

	/**
	 * Sets the reference phone3.
	 * 
	 * @param referencePhone3
	 *            the referencePhone3 to set
	 */
	public void setReferencePhone3(String referencePhone3) {
		this.referencePhone3 = referencePhone3;
	}

	/**
	 * Gets the reference mail id3.
	 * 
	 * @return the referenceMailId3
	 */
	public String getReferenceMailId3() {
		return referenceMailId3;
	}

	/**
	 * Sets the reference mail id3.
	 * 
	 * @param referenceMailId3
	 *            the referenceMailId3 to set
	 */
	public void setReferenceMailId3(String referenceMailId3) {
		this.referenceMailId3 = referenceMailId3;
	}

	/**
	 * Gets the reference mobile3.
	 * 
	 * @return the referenceMobile3
	 */
	public String getReferenceMobile3() {
		return referenceMobile3;
	}

	/**
	 * Sets the reference mobile3.
	 * 
	 * @param referenceMobile3
	 *            the referenceMobile3 to set
	 */
	public void setReferenceMobile3(String referenceMobile3) {
		this.referenceMobile3 = referenceMobile3;
	}

	/**
	 * Gets the category.
	 * 
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 * 
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * Gets the sub category.
	 * 
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}

	/**
	 * Sets the sub category.
	 * 
	 * @param subCategory
	 *            the subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * Gets the categories.
	 * 
	 * @return the categories
	 */
	public List<NaicsCategoryDto> getCategories() {
		return categories;
	}

	/**
	 * Sets the categories.
	 * 
	 * @param categories
	 *            the categories to set
	 */
	public void setCategories(List<NaicsCategoryDto> categories) {
		this.categories = categories;
	}

	/**
	 * Gets the sub categories.
	 * 
	 * @return the subCategories
	 */
	public List<NAICSubCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * Sets the sub categories.
	 * 
	 * @param subCategories
	 *            the subCategories to set
	 */
	public void setSubCategories(List<NAICSubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	/**
	 * Gets the naicslastrowcount.
	 * 
	 * @return the naicslastrowcount
	 */
	public Integer getNaicslastrowcount() {
		return naicslastrowcount;
	}

	/**
	 * Sets the naicslastrowcount.
	 * 
	 * @param naicslastrowcount
	 *            the naicslastrowcount to set
	 */
	public void setNaicslastrowcount(Integer naicslastrowcount) {
		this.naicslastrowcount = naicslastrowcount;
	}

	/**
	 * Gets the lastrowcount.
	 * 
	 * @return the lastrowcount
	 */
	public int getLastrowcount() {
		return lastrowcount;
	}

	/**
	 * Sets the lastrowcount.
	 * 
	 * @param lastrowcount
	 *            the lastrowcount to set
	 */
	public void setLastrowcount(int lastrowcount) {
		this.lastrowcount = lastrowcount;
	}

	/**
	 * Gets the countries.
	 * 
	 * @return the countries
	 */
	public List<Country> getCountries() {
		return countries;
	}

	/**
	 * Sets the countries.
	 * 
	 * @param countries
	 *            the countries to set
	 */
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	/**
	 * Gets the vendors list.
	 * 
	 * @return the vendorsList
	 */
	public List<SearchVendorDto> getVendorsList() {
		return vendorsList;
	}

	/**
	 * Sets the vendors list.
	 * 
	 * @param vendorsList
	 *            the vendorsList to set
	 */
	public void setVendorsList(List<SearchVendorDto> vendorsList) {
		this.vendorsList = vendorsList;
	}

	/**
	 * Gets the capabilities.
	 * 
	 * @return the capabilities
	 */
	public String getCapabilities() {
		return capabilities;
	}

	/**
	 * Sets the capabilities.
	 * 
	 * @param capabilities
	 *            the capabilities to set
	 */
	public void setCapabilities(String capabilities) {
		this.capabilities = capabilities;
	}

	/**
	 * Gets the cert id.
	 * 
	 * @return the certId
	 */
	public Integer getCertId() {
		return certId;
	}

	/**
	 * Sets the cert id.
	 * 
	 * @param certId
	 *            the certId to set
	 */
	public void setCertId(Integer certId) {
		this.certId = certId;
	}

	/**
	 * @return the ethnicity
	 */
	public String getEthnicity() {
		return ethnicity;
	}

	/**
	 * @param ethnicity
	 *            the ethnicity to set
	 */
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	/**
	 * @return the minorityPercent
	 */
	public String getMinorityPercent() {
		return minorityPercent;
	}

	/**
	 * @param minorityPercent
	 *            the minorityPercent to set
	 */
	public void setMinorityPercent(String minorityPercent) {
		this.minorityPercent = minorityPercent;
	}

	/**
	 * @return the womenPercent
	 */
	public String getWomenPercent() {
		return womenPercent;
	}

	/**
	 * @param womenPercent
	 *            the womenPercent to set
	 */
	public void setWomenPercent(String womenPercent) {
		this.womenPercent = womenPercent;
	}

	/**
	 * @return the classificationNotes
	 */
	public String getClassificationNotes() {
		return classificationNotes;
	}

	/**
	 * @param classificationNotes
	 *            the classificationNotes to set
	 */
	public void setClassificationNotes(String classificationNotes) {
		this.classificationNotes = classificationNotes;
	}

	/**
	 * @return the certificationNo1
	 */
	public String getCertificationNo1() {
		return certificationNo1;
	}

	/**
	 * @param certificationNo1
	 *            the certificationNo1 to set
	 */
	public void setCertificationNo1(String certificationNo1) {
		this.certificationNo1 = certificationNo1;
	}

	/**
	 * @return the effDate1
	 */
	public String getEffDate1() {
		return effDate1;
	}

	/**
	 * @param effDate1
	 *            the effDate1 to set
	 */
	public void setEffDate1(String effDate1) {
		this.effDate1 = effDate1;
	}

	/**
	 * @return the diverseCertificateName
	 */
	public Integer getDiverseCertificateName() {
		return diverseCertificateName;
	}

	/**
	 * @param diverseCertificateName
	 *            the diverseCertificateName to set
	 */
	public void setDiverseCertificateName(Integer diverseCertificateName) {
		this.diverseCertificateName = diverseCertificateName;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.vendorName = "";
		this.vendorCode = "";
		this.dunsNumber = "";
		this.taxId = "";
		this.address1 = "";
		this.address2 = "";
		this.address3 = "";
		this.city = "";
		this.state = "";
		this.province = "";
		this.zipcode = "";
		this.region = "";
		this.country = "";
		this.phone = "";
		this.mobile = "";
		this.fax = "";
		this.emailId = "";
		this.website = "";
		this.companytype = "";
		this.numberOfEmployees = 0;
		this.annualTurnover = "";
		this.annualTurnoverFormat = "";
		this.yearOfEstablishment = null;
		this.category = "";
		this.subCategory = "";
		this.naicsCode_1 = "";
		this.naicsDesc = "";
		this.isicCode = "";
		this.isicDesc = "";
		this.firstName = "";
		this.lastName = "";
		this.designation = "";
		this.contactPhone = "";
		this.contactMobile = "";
		this.contactFax = "";
		this.contanctEmail = "";
		this.diverseQuality = null;
		this.divCertAgen = 0;
		this.divCertType = 0;
		this.primeNonPrimeVendor = null;
		this.diverseSupplier = "";
		this.expDate = "";
		this.referenceName1 = "";
		this.referenceAddress1 = "";
		this.referenceMailId1 = "";
		this.referenceMobile1 = "";
		this.referencePhone1 = "";
		this.referenceName2 = "";
		this.referenceAddress2 = "";
		this.referenceMailId2 = "";
		this.referenceMobile2 = "";
		this.referencePhone2 = "";
		this.referenceName3 = "";
		this.referenceAddress3 = "";
		this.referenceMailId3 = "";
		this.referenceMobile3 = "";
		this.referencePhone3 = "";
		this.capabilities = "";
		this.certId = 0;
		this.referenceCity1 = "";
		this.referenceCity2 = "";
		this.referenceCity3 = "";
		this.referenceState1 = "";
		this.referenceState2 = "";
		this.referenceState3 = "";
		this.referenceZip1 = "";
		this.referenceZip2 = "";
		this.referenceZip3 = "";
		this.referenceCountry1 = "";
		this.referenceCountry2 = "";
		this.referenceCountry3 = "";
		this.certificates = null;
		this.ethnicity = "";
		this.minorityPercent = null;
		this.womenPercent = null;
		this.classificationNotes = "";
		this.certificationNo1 = "";
		this.effDate1 = "";
		this.diverseCertificateName = null;
	}

}
