package com.fg.vms.customer.pojo;

import java.io.Serializable;


/**
 * @author Kaleswararao
 *
 */
public class DirectExcelColumn implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String LegalCompanyName;
	private String Address;
	private String City;
	private String State;
	private Integer Zipcode;
	private String FirstName;
	private String LastName;
	private String Email;
	private Long Phone;
	private String[] Diversity;
	private Double Amount;
	private String country;
	private String diverseSupplier;
	private String diverstiyClassification;
	private String directEthanicity;
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the legalCompanyName
	 */
	public String getLegalCompanyName() {
		return LegalCompanyName;
	}
	/**
	 * @param legalCompanyName the legalCompanyName to set
	 */
	public void setLegalCompanyName(String legalCompanyName) {
		LegalCompanyName = legalCompanyName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return Address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		Address = address;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return City;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		City = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return State;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		State = state;
	}
	/**
	 * @return the zipcode
	 */
	public Integer getZipcode() {
		return Zipcode;
	}
	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(Integer zipcode) {
		Zipcode = zipcode;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return FirstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return LastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		Email = email;
	}
	/**
	 * @return the phone
	 */
	public Long getPhone() {
		return Phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(Long phone) {
		Phone = phone;
	}
	/**
	 * @return the diversity
	 */
	public String[] getDiversity() {
		return Diversity;
	}
	/**
	 * @param diversity the diversity to set
	 */
	public void setDiversity(String[] diversity) {
		Diversity = diversity;
	}
	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return Amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		Amount = amount;
	}
	/**
	 * @return the diverseSupplier
	 */
	public String getDiverseSupplier() {
		return diverseSupplier;
	}
	/**
	 * @param diverseSupplier the diverseSupplier to set
	 */
	public void setDiverseSupplier(String diverseSupplier) {
		this.diverseSupplier = diverseSupplier;
	}
	/**
	 * @return the diverstiyClassification
	 */
	public String getDiverstiyClassification() {
		return diverstiyClassification;
	}
	/**
	 * @param diverstiyClassification the diverstiyClassification to set
	 */
	public void setDiverstiyClassification(String diverstiyClassification) {
		this.diverstiyClassification = diverstiyClassification;
	}
	public String getDirectEthanicity() {
		return directEthanicity;
	}
	public void setDirectEthanicity(String directEthanicity) {
		this.directEthanicity = directEthanicity;
	}

}
