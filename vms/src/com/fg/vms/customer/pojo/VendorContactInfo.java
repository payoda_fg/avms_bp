/*
 * VendorContactInfo.java
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * The Class VendorContactInfo.
 * 
 * @author srinivasarao
 */
public class VendorContactInfo extends ValidatorForm {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private String id;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The designation. */
	private String designation;

	/** The contact phone. */
	private String contactPhone;

	/** The contact mobile. */
	private String contactMobile;

	/** The contact fax. */
	private String contactFax;

	/** The contanct email. */
	private String contanctEmail;

	/** The hidden email id. */
	private String hiddenEmailId;

	/** The login allowed. */
	private String loginAllowed;

	/** The login display name. */
	private String loginDisplayName;

	/** The login id. */
	// private String loginId;

	/** The hidden login id. */
	private String hiddenLoginId;

	/** The loginpassword. */
	private String loginpassword;

	/** The confirm password. */
	private String confirmPassword;

	/** The primary contact. */
	private String primaryContact;

	/** The user sec qn. */
	private int userSecQn;

	/** The user sec qn ans. */
	private String userSecQnAns;

	private String isBusinessContact;

	private String isPreparer;

	private Integer vendorId;
	
	private String gender;
	
	private Integer roleId;
	
	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the isBusinessContact
	 */
	public String getIsBusinessContact() {
		return isBusinessContact;
	}

	/**
	 * @param isBusinessContact
	 *            the isBusinessContact to set
	 */
	public void setIsBusinessContact(String isBusinessContact) {
		this.isBusinessContact = isBusinessContact;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 * 
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the hiddenEmailId
	 */
	public String getHiddenEmailId() {
		return hiddenEmailId;
	}

	/**
	 * @param hiddenEmailId
	 *            the hiddenEmailId to set
	 */
	public void setHiddenEmailId(String hiddenEmailId) {
		this.hiddenEmailId = hiddenEmailId;
	}

	/**
	 * @return the hiddenLoginId
	 */
	public String getHiddenLoginId() {
		return hiddenLoginId;
	}

	/**
	 * @param hiddenLoginId
	 *            the hiddenLoginId to set
	 */
	public void setHiddenLoginId(String hiddenLoginId) {
		this.hiddenLoginId = hiddenLoginId;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 * 
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the designation.
	 * 
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * Sets the designation.
	 * 
	 * @param designation
	 *            the new designation
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * Gets the contact phone.
	 * 
	 * @return the contact phone
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * Sets the contact phone.
	 * 
	 * @param contactPhone
	 *            the new contact phone
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * Gets the contact mobile.
	 * 
	 * @return the contact mobile
	 */
	public String getContactMobile() {
		return contactMobile;
	}

	/**
	 * Sets the contact mobile.
	 * 
	 * @param contactMobile
	 *            the new contact mobile
	 */
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	/**
	 * Gets the contact fax.
	 * 
	 * @return the contact fax
	 */
	public String getContactFax() {
		return contactFax;
	}

	/**
	 * Sets the contact fax.
	 * 
	 * @param contactFax
	 *            the new contact fax
	 */
	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	/**
	 * Gets the contanct email.
	 * 
	 * @return the contanct email
	 */
	public String getContanctEmail() {
		return contanctEmail;
	}

	/**
	 * Sets the contanct email.
	 * 
	 * @param contanctEmail
	 *            the new contanct email
	 */
	public void setContanctEmail(String contanctEmail) {
		this.contanctEmail = contanctEmail;
	}

	/**
	 * @return the loginAllowed
	 */
	public String getLoginAllowed() {
		return loginAllowed;
	}

	/**
	 * @param loginAllowed
	 *            the loginAllowed to set
	 */
	public void setLoginAllowed(String loginAllowed) {
		this.loginAllowed = loginAllowed;
	}

	/**
	 * Gets the login display name.
	 * 
	 * @return the login display name
	 */
	public String getLoginDisplayName() {
		return loginDisplayName;
	}

	/**
	 * Sets the login display name.
	 * 
	 * @param loginDisplayName
	 *            the new login display name
	 */
	public void setLoginDisplayName(String loginDisplayName) {
		this.loginDisplayName = loginDisplayName;
	}

	/**
	 * Gets the login id.
	 * 
	 * @return the login id
	 */
	// public String getLoginId() {
	// return loginId;
	// }

	/**
	 * Sets the login id.
	 * 
	 * @param loginId
	 *            the new login id
	 */
	// public void setLoginId(String loginId) {
	// this.loginId = loginId;
	// }

	/**
	 * Gets the loginpassword.
	 * 
	 * @return the loginpassword
	 */

	public String getLoginpassword() {
		return loginpassword;
	}

	/**
	 * Sets the loginpassword.
	 * 
	 * @param loginpassword
	 *            the new loginpassword
	 */
	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}

	/**
	 * Gets the confirm password.
	 * 
	 * @return the confirm password
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Sets the confirm password.
	 * 
	 * @param confirmPassword
	 *            the new confirm password
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * Gets the primary contact.
	 * 
	 * @return the primaryContact
	 */
	public String getPrimaryContact() {
		return primaryContact;
	}

	/**
	 * Sets the primary contact.
	 * 
	 * @param primaryContact
	 *            the primaryContact to set
	 */
	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	/**
	 * Gets the user sec qn.
	 * 
	 * @return the user sec qn
	 */
	public int getUserSecQn() {
		return userSecQn;
	}

	/**
	 * Sets the user sec qn.
	 * 
	 * @param userSecQn
	 *            the new user sec qn
	 */
	public void setUserSecQn(int userSecQn) {
		this.userSecQn = userSecQn;
	}

	/**
	 * Gets the user sec qn ans.
	 * 
	 * @return the user sec qn ans
	 */
	public String getUserSecQnAns() {
		return userSecQnAns;
	}

	/**
	 * Sets the user sec qn ans.
	 * 
	 * @param userSecQnAns
	 *            the new user sec qn ans
	 */
	public void setUserSecQnAns(String userSecQnAns) {
		this.userSecQnAns = userSecQnAns;
	}

	/**
	 * @return the isPreparer
	 */
	public String getIsPreparer() {
		return isPreparer;
	}

	/**
	 * @param isPreparer
	 *            the isPreparer to set
	 */
	public void setIsPreparer(String isPreparer) {
		this.isPreparer = isPreparer;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.firstName = "";
		this.lastName = "";
		this.designation = "";
		this.contactPhone = "";
		this.contactMobile = "";
		this.contactFax = "";
		this.contanctEmail = "";
		this.loginDisplayName = "";
		// this.loginId = "";
		this.loginpassword = "";
		this.confirmPassword = "";
		this.userSecQnAns = "";
		this.loginAllowed = "";
		this.primaryContact = "";
		this.isBusinessContact = "";
		this.isPreparer = "";
		this.gender="";
		this.roleId = null;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
}