/*
 * EmailDistributionForm.java 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * The Email Distribution POJO class.
 * 
 * @author vinoth
 * 
 */
public class EmailDistributionForm extends ValidatorForm {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The emailDistributionMasterId. */
	private Integer emailDistributionMasterId;

	/** The email distribution name. */
	private String emailDistributionListName;

	/** The isActive. */
	private String isActive;

	/** the checkbox. */
	private Integer[] checkboxIsTo;

	/** the checkbox. */
	private Integer[] checkboxIsCC;

	/** the checkbox. */
	private Integer[] checkboxIsBCC;

	/** the checkbox. */
	private Integer[] vendorCheckboxIsTo;

	/** the checkbox. */
	private Integer[] vendorCheckboxIsCC;

	/** the checkbox. */
	private Integer[] vendorCheckboxIsBCC;

	/** the checkbox. */
	private String[] checkbox;

	/** the checkbox. */
	private String[] checkbox1;

	/** the checkbox. */
	private String[] checkbox2;

	/** the checkbox. */
	private String[] checkbox3;

	/** the isDivision Status. */
	private Byte isDivision;

	/** the customer division id. */
	private Integer customerDivisionId;

	/**
	 * @return the emailDistributionMasterId
	 */
	public Integer getEmailDistributionMasterId() {
		return emailDistributionMasterId;
	}

	/**
	 * @param emailDistributionMasterId
	 *            the emailDistributionMasterId to set
	 */
	public void setEmailDistributionMasterId(Integer emailDistributionMasterId) {
		this.emailDistributionMasterId = emailDistributionMasterId;
	}

	/**
	 * @return the emailDistributionListName
	 */
	public String getEmailDistributionListName() {
		return emailDistributionListName;
	}

	/**
	 * @param emailDistributionListName
	 *            the emailDistributionListName to set
	 */
	public void setEmailDistributionListName(String emailDistributionListName) {
		this.emailDistributionListName = emailDistributionListName;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Integer[] getCheckboxIsTo() {
		return checkboxIsTo;
	}

	public void setCheckboxIsTo(Integer[] checkboxIsTo) {
		this.checkboxIsTo = checkboxIsTo;
	}

	public Integer[] getCheckboxIsCC() {
		return checkboxIsCC;
	}

	public void setCheckboxIsCC(Integer[] checkboxIsCC) {
		this.checkboxIsCC = checkboxIsCC;
	}

	public Integer[] getCheckboxIsBCC() {
		return checkboxIsBCC;
	}

	public void setCheckboxIsBCC(Integer[] checkboxIsBCC) {
		this.checkboxIsBCC = checkboxIsBCC;
	}

	public Integer[] getVendorCheckboxIsTo() {
		return vendorCheckboxIsTo;
	}

	public void setVendorCheckboxIsTo(Integer[] vendorCheckboxIsTo) {
		this.vendorCheckboxIsTo = vendorCheckboxIsTo;
	}

	public Integer[] getVendorCheckboxIsCC() {
		return vendorCheckboxIsCC;
	}

	public void setVendorCheckboxIsCC(Integer[] vendorCheckboxIsCC) {
		this.vendorCheckboxIsCC = vendorCheckboxIsCC;
	}

	public Integer[] getVendorCheckboxIsBCC() {
		return vendorCheckboxIsBCC;
	}

	public void setVendorCheckboxIsBCC(Integer[] vendorCheckboxIsBCC) {
		this.vendorCheckboxIsBCC = vendorCheckboxIsBCC;
	}

	public String[] getCheckbox() {
		return checkbox;
	}

	public void setCheckbox(String[] checkbox) {
		this.checkbox = checkbox;
	}

	public String[] getCheckbox1() {
		return checkbox1;
	}

	public void setCheckbox1(String[] checkbox1) {
		this.checkbox1 = checkbox1;
	}

	public String[] getCheckbox2() {
		return checkbox2;
	}

	public void setCheckbox2(String[] checkbox2) {
		this.checkbox2 = checkbox2;
	}

	public String[] getCheckbox3() {
		return checkbox3;
	}

	public void setCheckbox3(String[] checkbox3) {
		this.checkbox3 = checkbox3;
	}

	public Byte getIsDivision() {
		return isDivision;
	}

	public void setIsDivision(Byte isDivision) {
		this.isDivision = isDivision;
	}

	public Integer getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(Integer customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.emailDistributionListName = "";
		this.checkboxIsTo = null;
		this.checkboxIsCC = null;
		this.checkboxIsBCC = null;
		this.vendorCheckboxIsTo = null;
		this.vendorCheckboxIsCC = null;
		this.vendorCheckboxIsBCC = null;
		this.checkbox = null;
		this.checkbox1 = null;
		this.checkbox2 = null;
		this.checkbox3 = null;

	}

}
