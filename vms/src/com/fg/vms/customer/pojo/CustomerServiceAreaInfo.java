/**
 * 
 */
package com.fg.vms.customer.pojo;

import java.io.Serializable;

/**
 * @author Asarudeen
 *
 */
public class CustomerServiceAreaInfo implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private Integer serialNum;
	private String serviceArea;
	
	/**
	 * @return the serialNum
	 */
	public Integer getSerialNum() {
		return serialNum;
	}
	/**
	 * @param serialNum the serialNum to set
	 */
	public void setSerialNum(Integer serialNum) {
		this.serialNum = serialNum;
	}
	/**
	 * @return the serviceArea
	 */
	public String getServiceArea() {
		return serviceArea;
	}
	/**
	 * @param serviceArea the serviceArea to set
	 */
	public void setServiceArea(String serviceArea) {
		this.serviceArea = serviceArea;
	}	
}