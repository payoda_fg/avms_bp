/*
 * VendorObjectsForm.java
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * Represents vendor object details (eg. objectName, objectDescription,
 * objectType) to interact with UI.
 * 
 * @author hemavaishnavi
 * 
 */

public class VendorObjectsForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private int id;

    private String objectName = null;

    private String objectType = null;

    private String objectDescription = null;

    private Byte isActive = 1;

    /**
     * @return the id
     */
    public int getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
	this.id = id;
    }

    /**
     * @return the objectName
     */
    public String getObjectName() {
	return objectName;
    }

    /**
     * @param objectName
     *            the objectName to set
     */
    public void setObjectName(String objectName) {
	this.objectName = objectName;
    }

    /**
     * @return the objectType
     */
    public String getObjectType() {
	return objectType;
    }

    /**
     * @param objectType
     *            the objectType to set
     */
    public void setObjectType(String objectType) {
	this.objectType = objectType;
    }

    /**
     * @return the objectDescription
     */
    public String getObjectDescription() {
	return objectDescription;
    }

    /**
     * @param objectDescription
     *            the objectDescription to set
     */
    public void setObjectDescription(String objectDescription) {
	this.objectDescription = objectDescription;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {

	this.objectName = "";
	this.objectType = "";
	this.objectDescription = "";

    }
}
