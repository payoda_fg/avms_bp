/*
 * ReportForm.java 
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.VendorMaster;

/**
 * Represents the report details (eg. startDate, endDate, year, supplier,
 * vendor) to interact with UI.
 * 
 * @author pirabu
 * 
 */
public class ReportForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private String option;

    private String[] supplier;

    private String year[];

    private List<Integer> years;

    private String startDate;

    private String endDate;

    private List<VendorMaster> vendors;

    private String status;

    /**
     * @return the option
     */
    public String getOption() {
	return option;
    }

    /**
     * @param option
     *            the option to set
     */
    public void setOption(String option) {
	this.option = option;
    }

    /**
     * @return the supplier
     */
    public String[] getSupplier() {
	return supplier;
    }

    /**
     * @param supplier
     *            the supplier to set
     */
    public void setSupplier(String[] supplier) {
	this.supplier = supplier;
    }

    /**
     * @return the year
     */
    public String[] getYear() {
	return year;
    }

    /**
     * @param year
     *            the year to set
     */
    public void setYear(String[] year) {
	this.year = year;
    }

    /**
     * @return the years
     */
    public List<Integer> getYears() {
	return years;
    }

    /**
     * @param years
     *            the years to set
     */
    public void setYears(List<Integer> years) {
	this.years = years;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
	return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(String startDate) {
	this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
	return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(String endDate) {
	this.endDate = endDate;
    }

    /**
     * @return the vendors
     */
    public List<VendorMaster> getVendors() {
	return vendors;
    }

    /**
     * @param vendors
     *            the vendors to set
     */
    public void setVendors(List<VendorMaster> vendors) {
	this.vendors = vendors;
    }

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }

}
