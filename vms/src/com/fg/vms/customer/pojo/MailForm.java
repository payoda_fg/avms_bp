/*
 * MailForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.VendorMailNotificationDetail;

/**
 * 
 * Represents the Mail Details (eg. To address, Subject, Message, final date for
 * reply and list of template names) for interacting with UI
 * 
 * @author hemavaishnavi
 * 
 */
public class MailForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	// private Integer id;

	private String to;

	private String cc;

	private String bcc;

	private String subject;

	private String message;

	// private Date emailDate;

	private String templateId;

	private List<VendorMailNotificationDetail> mailNotificationDetails;

	private List<Template> templateNames;

	private String finalDateForReply;

	private String[] listDistribution;
	
	private FormFile attachment;

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to
	 *            the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the cc
	 */
	public String getCc() {
		return cc;
	}

	/**
	 * @param cc
	 *            the cc to set
	 */
	public void setCc(String cc) {
		this.cc = cc;
	}

	/**
	 * @return the bcc
	 */
	public String getBcc() {
		return bcc;
	}

	/**
	 * @param bcc
	 *            the bcc to set
	 */
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the templateNames
	 */
	public List<Template> getTemplateNames() {
		return templateNames;
	}

	/**
	 * @param templateNames
	 *            the templateNames to set
	 */
	public void setTemplateNames(List<Template> templateNames) {
		this.templateNames = templateNames;
	}

	/**
	 * @return the finalDateForReply
	 */
	public String getFinalDateForReply() {
		return finalDateForReply;
	}

	/**
	 * @param finalDateForReply
	 *            the finalDateForReply to set
	 */
	public void setFinalDateForReply(String finalDateForReply) {
		this.finalDateForReply = finalDateForReply;
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the mailNotificationDetails
	 */
	public List<VendorMailNotificationDetail> getMailNotificationDetails() {
		return mailNotificationDetails;
	}

	/**
	 * @param mailNotificationDetails
	 *            the mailNotificationDetails to set
	 */
	public void setMailNotificationDetails(
			List<VendorMailNotificationDetail> mailNotificationDetails) {
		this.mailNotificationDetails = mailNotificationDetails;
	}

	/**
	 * @return the listDistribution
	 */
	public String[] getListDistribution() {
		return listDistribution;
	}

	/**
	 * @param listDistribution
	 *            the listDistribution to set
	 */
	public void setListDistribution(String[] listDistribution) {
		this.listDistribution = listDistribution;
	}

	public FormFile getAttachment() {
		return attachment;
	}

	public void setAttachment(FormFile attachment) {
		this.attachment = attachment;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.to = "";
		this.cc = "";
		this.bcc = "";
		this.subject = "";
		this.message = "";
		this.finalDateForReply = "";
		this.templateId = "";
		this.listDistribution = null;
	}

}
