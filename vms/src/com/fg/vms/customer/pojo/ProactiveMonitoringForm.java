/**
 * 
 */
package com.fg.vms.customer.pojo;

import org.apache.struts.validator.ValidatorForm;

/**
 * @author gpirabu
 * 
 */
public class ProactiveMonitoringForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String reccurrencePattern;

	private String dailyOption;

	private String dailyEveryDayNumber;

	private String weeklyWeekNumber;

	private String weeklyWeekDays[];

	private String monthlyOption;

	private String monthlyDayNumber;

	private String monthlyWeekNumber;

	private String monthlyWeekDay;

	private String monthlyMonthNumber;

	private String yearlyOption;

	private String yearlyDayNumber;

	private String yearlyWeekNumber;

	private String yearlyWeekDayNumber;

	private String yearlyMonthNumber;

	private Short rangeOccurrence;

	private String rangeStartDate;

	private String rangeOccurenceOption;

	private String numberOfOccurrence;

	private String occurenceEndate;

	/**
	 * @return the reccurrencePattern
	 */
	public String getReccurrencePattern() {
		return reccurrencePattern;
	}

	/**
	 * @param reccurrencePattern
	 *            the reccurrencePattern to set
	 */
	public void setReccurrencePattern(String reccurrencePattern) {
		this.reccurrencePattern = reccurrencePattern;
	}

	/**
	 * @return the dailyOption
	 */
	public String getDailyOption() {
		return dailyOption;
	}

	/**
	 * @param dailyOption
	 *            the dailyOption to set
	 */
	public void setDailyOption(String dailyOption) {
		this.dailyOption = dailyOption;
	}

	/**
	 * @return the dailyEveryDayNumber
	 */
	public String getDailyEveryDayNumber() {
		return dailyEveryDayNumber;
	}

	/**
	 * @param dailyEveryDayNumber
	 *            the dailyEveryDayNumber to set
	 */
	public void setDailyEveryDayNumber(String dailyEveryDayNumber) {
		this.dailyEveryDayNumber = dailyEveryDayNumber;
	}

	/**
	 * @return the weeklyWeekNumber
	 */
	public String getWeeklyWeekNumber() {
		return weeklyWeekNumber;
	}

	/**
	 * @param weeklyWeekNumber
	 *            the weeklyWeekNumber to set
	 */
	public void setWeeklyWeekNumber(String weeklyWeekNumber) {
		this.weeklyWeekNumber = weeklyWeekNumber;
	}

	/**
	 * @return the weeklyWeekDays
	 */
	public String[] getWeeklyWeekDays() {
		return weeklyWeekDays;
	}

	/**
	 * @param weeklyWeekDays
	 *            the weeklyWeekDays to set
	 */
	public void setWeeklyWeekDays(String[] weeklyWeekDays) {
		this.weeklyWeekDays = weeklyWeekDays;
	}

	/**
	 * @return the monthlyOption
	 */
	public String getMonthlyOption() {
		return monthlyOption;
	}

	/**
	 * @param monthlyOption
	 *            the monthlyOption to set
	 */
	public void setMonthlyOption(String monthlyOption) {
		this.monthlyOption = monthlyOption;
	}

	/**
	 * @return the monthlyDayNumber
	 */
	public String getMonthlyDayNumber() {
		return monthlyDayNumber;
	}

	/**
	 * @param monthlyDayNumber
	 *            the monthlyDayNumber to set
	 */
	public void setMonthlyDayNumber(String monthlyDayNumber) {
		this.monthlyDayNumber = monthlyDayNumber;
	}

	/**
	 * @return the monthlyWeekNumber
	 */
	public String getMonthlyWeekNumber() {
		return monthlyWeekNumber;
	}

	/**
	 * @param monthlyWeekNumber
	 *            the monthlyWeekNumber to set
	 */
	public void setMonthlyWeekNumber(String monthlyWeekNumber) {
		this.monthlyWeekNumber = monthlyWeekNumber;
	}

	/**
	 * @return the monthlyWeekDay
	 */
	public String getMonthlyWeekDay() {
		return monthlyWeekDay;
	}

	/**
	 * @param monthlyWeekDay
	 *            the monthlyWeekDay to set
	 */
	public void setMonthlyWeekDay(String monthlyWeekDay) {
		this.monthlyWeekDay = monthlyWeekDay;
	}

	/**
	 * @return the monthlyMonthNumber
	 */
	public String getMonthlyMonthNumber() {
		return monthlyMonthNumber;
	}

	/**
	 * @param monthlyMonthNumber
	 *            the monthlyMonthNumber to set
	 */
	public void setMonthlyMonthNumber(String monthlyMonthNumber) {
		this.monthlyMonthNumber = monthlyMonthNumber;
	}

	/**
	 * @return the yearlyOption
	 */
	public String getYearlyOption() {
		return yearlyOption;
	}

	/**
	 * @param yearlyOption
	 *            the yearlyOption to set
	 */
	public void setYearlyOption(String yearlyOption) {
		this.yearlyOption = yearlyOption;
	}

	/**
	 * @return the yearlyDayNumber
	 */
	public String getYearlyDayNumber() {
		return yearlyDayNumber;
	}

	/**
	 * @param yearlyDayNumber
	 *            the yearlyDayNumber to set
	 */
	public void setYearlyDayNumber(String yearlyDayNumber) {
		this.yearlyDayNumber = yearlyDayNumber;
	}

	/**
	 * @return the yearlyWeekNumber
	 */
	public String getYearlyWeekNumber() {
		return yearlyWeekNumber;
	}

	/**
	 * @param yearlyWeekNumber
	 *            the yearlyWeekNumber to set
	 */
	public void setYearlyWeekNumber(String yearlyWeekNumber) {
		this.yearlyWeekNumber = yearlyWeekNumber;
	}

	/**
	 * @return the yearlyWeekDayNumber
	 */
	public String getYearlyWeekDayNumber() {
		return yearlyWeekDayNumber;
	}

	/**
	 * @param yearlyWeekDayNumber
	 *            the yearlyWeekDayNumber to set
	 */
	public void setYearlyWeekDayNumber(String yearlyWeekDayNumber) {
		this.yearlyWeekDayNumber = yearlyWeekDayNumber;
	}

	/**
	 * @return the yearlyMonthNumber
	 */
	public String getYearlyMonthNumber() {
		return yearlyMonthNumber;
	}

	/**
	 * @param yearlyMonthNumber
	 *            the yearlyMonthNumber to set
	 */
	public void setYearlyMonthNumber(String yearlyMonthNumber) {
		this.yearlyMonthNumber = yearlyMonthNumber;
	}

	/**
	 * @return the rangeOccurrence
	 */
	public Short getRangeOccurrence() {
		return rangeOccurrence;
	}

	/**
	 * @param rangeOccurrence
	 *            the rangeOccurrence to set
	 */
	public void setRangeOccurrence(Short rangeOccurrence) {
		this.rangeOccurrence = rangeOccurrence;
	}

	/**
	 * @return the rangeStartDate
	 */
	public String getRangeStartDate() {
		return rangeStartDate;
	}

	/**
	 * @param rangeStartDate
	 *            the rangeStartDate to set
	 */
	public void setRangeStartDate(String rangeStartDate) {
		this.rangeStartDate = rangeStartDate;
	}

	/**
	 * @return the rangeOccurenceOption
	 */
	public String getRangeOccurenceOption() {
		return rangeOccurenceOption;
	}

	/**
	 * @param rangeOccurenceOption
	 *            the rangeOccurenceOption to set
	 */
	public void setRangeOccurenceOption(String rangeOccurenceOption) {
		this.rangeOccurenceOption = rangeOccurenceOption;
	}

	/**
	 * @return the numberOfOccurrence
	 */
	public String getNumberOfOccurrence() {
		return numberOfOccurrence;
	}

	/**
	 * @param numberOfOccurrence
	 *            the numberOfOccurrence to set
	 */
	public void setNumberOfOccurrence(String numberOfOccurrence) {
		this.numberOfOccurrence = numberOfOccurrence;
	}

	/**
	 * @return the occurenceEndate
	 */
	public String getOccurenceEndate() {
		return occurenceEndate;
	}

	/**
	 * @param occurenceEndate
	 *            the occurenceEndate to set
	 */
	public void setOccurenceEndate(String occurenceEndate) {
		this.occurenceEndate = occurenceEndate;
	}

}
