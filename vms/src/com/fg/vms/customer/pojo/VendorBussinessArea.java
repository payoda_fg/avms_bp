package com.fg.vms.customer.pojo;

import java.io.Serializable;

public class VendorBussinessArea implements Serializable{
	
	private Integer serialNum;
	private String bussinessGroupName;
	private String areaName;
	
	public String getBussinessGroupName() {
		return bussinessGroupName;
	}
	public void setBussinessGroupName(String bussinessGroupName) {
		this.bussinessGroupName = bussinessGroupName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public Integer getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(Integer serialNum) {
		this.serialNum = serialNum;
	}

}
