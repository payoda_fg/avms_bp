/*
 * CertifyingAgencyForm.java 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import fr.improve.struts.taglib.layout.datagrid.Datagrid;

/**
 * Represents the Certificate Agency Details (eg.certifyingAgency, agencyName)
 * for interact with UI
 * 
 * @author vinoth
 * 
 */
public class CertifyingAgencyForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Datagrid certifyingAcency;

	private String agencyName;

	private String agencyShortName;

	private String phone;

	private String fax;

	private String address;

	private Integer id;

	/**
	 * @return the certifyingAcency
	 */
	public Datagrid getCertifyingAcency() {
		return certifyingAcency;
	}

	/**
	 * @param certifyingAcency
	 *            the certifyingAcency to set
	 */
	public void setCertifyingAcency(Datagrid certifyingAcency) {
		this.certifyingAcency = certifyingAcency;
	}

	/**
	 * @return the agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * @param agencyName
	 *            the agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * @return the agencyShortName
	 */
	public String getAgencyShortName() {
		return agencyShortName;
	}

	/**
	 * @param agencyShortName
	 *            the agencyShortName to set
	 */
	public void setAgencyShortName(String agencyShortName) {
		this.agencyShortName = agencyShortName;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.agencyName = "";
		this.agencyShortName = "";
		this.phone = "";
		this.fax = "";
		this.address = "";
		this.id= null;
	}

}
