/*
 * SpendUploadForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.admin.dto.SpendDataDTO;
import com.fg.vms.customer.model.SpendDataMaster;
import com.fg.vms.customer.model.VendorMaster;

/**
 * The Class SpendUploadForm.
 * 
 * @author pirabu
 */
public class SpendUploadForm extends ValidatorForm {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Integer id;

    /** The vendor. */
    private String vendor;

    /** The vendor id. */
    private String vendorId;

    /** The year of data. */
    private String yearOfData;

    /** The file. */
    private FormFile file;

    /** The vendors. */
    private List<VendorMaster> vendors;

    /** The spend data dto list. */
    private List<SpendDataDTO> spendDataDTOList;

    /** The spend start date. */
    private String spendStartDate;

    /** The spend end date. */
    private String spendEndDate;

    /** The indirect value. */
    private Integer indirectValue;

    /** The indirect value percentage. */
    private Integer indirectValuePercentage;

    /** The total prime supplier sales. */
    private String totalPrimeSupplierSales;

    /** The total direct sales. */
    private String totalDirectSales;

    /** The percentage of direct sales. */
    private Double percentageOfDirectSales;

    /** The spend datas. */
    private List<SpendDataMaster> spendDatas;

    /*
     * private String tpss;
     * 
     * private String tds;
     */

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the vendor.
     * 
     * @return the vendor
     */
    public String getVendor() {
	return vendor;
    }

    /**
     * Sets the vendor.
     * 
     * @param vendor
     *            the vendor to set
     */
    public void setVendor(String vendor) {
	this.vendor = vendor;
    }

    /**
     * Gets the vendor id.
     * 
     * @return the vendorId
     */
    public String getVendorId() {
	return vendorId;
    }

    /**
     * Sets the vendor id.
     * 
     * @param vendorId
     *            the vendorId to set
     */
    public void setVendorId(String vendorId) {
	this.vendorId = vendorId;
    }

    /**
     * Gets the year of data.
     * 
     * @return the yearOfData
     */
    public String getYearOfData() {
	return yearOfData;
    }

    /**
     * Sets the year of data.
     * 
     * @param yearOfData
     *            the yearOfData to set
     */
    public void setYearOfData(String yearOfData) {
	this.yearOfData = yearOfData;
    }

    /**
     * Gets the file.
     * 
     * @return the file
     */
    public FormFile getFile() {
	return file;
    }

    /**
     * Sets the file.
     * 
     * @param file
     *            the file to set
     */
    public void setFile(FormFile file) {
	this.file = file;
    }

    /**
     * Gets the vendors.
     * 
     * @return the vendors
     */
    public List<VendorMaster> getVendors() {
	return vendors;
    }

    /**
     * Sets the vendors.
     * 
     * @param vendors
     *            the vendors to set
     */
    public void setVendors(List<VendorMaster> vendors) {
	this.vendors = vendors;
    }

    /**
     * Gets the spend data dto list.
     * 
     * @return the spend data dto list
     */
    public List<SpendDataDTO> getSpendDataDTOList() {
	return spendDataDTOList;
    }

    /**
     * Sets the spend data dto list.
     * 
     * @param spendDataDTOList
     *            the new spend data dto list
     */
    public void setSpendDataDTOList(List<SpendDataDTO> spendDataDTOList) {
	this.spendDataDTOList = spendDataDTOList;
    }

    /**
     * Gets the spend start date.
     * 
     * @return the spendStartDate
     */
    public String getSpendStartDate() {
	return spendStartDate;
    }

    /**
     * Sets the spend start date.
     * 
     * @param spendStartDate
     *            the spendStartDate to set
     */
    public void setSpendStartDate(String spendStartDate) {
	this.spendStartDate = spendStartDate;
    }

    /**
     * Gets the spend end date.
     * 
     * @return the spendEndDate
     */
    public String getSpendEndDate() {
	return spendEndDate;
    }

    /**
     * Sets the spend end date.
     * 
     * @param spendEndDate
     *            the spendEndDate to set
     */
    public void setSpendEndDate(String spendEndDate) {
	this.spendEndDate = spendEndDate;
    }

    /**
     * Gets the indirect value.
     * 
     * @return the indirectValue
     */
    public Integer getIndirectValue() {
	return indirectValue;
    }

    /**
     * Sets the indirect value.
     * 
     * @param indirectValue
     *            the indirectValue to set
     */
    public void setIndirectValue(Integer indirectValue) {
	this.indirectValue = indirectValue;
    }

    /**
     * Gets the indirect value percentage.
     * 
     * @return the indirectValuePercentage
     */
    public Integer getIndirectValuePercentage() {
	return indirectValuePercentage;
    }

    /**
     * Gets the total prime supplier sales.
     * 
     * @return the totalPrimeSupplierSales
     */
    public String getTotalPrimeSupplierSales() {
	return totalPrimeSupplierSales;
    }

    /**
     * Sets the total prime supplier sales.
     * 
     * @param totalPrimeSupplierSales
     *            the totalPrimeSupplierSales to set
     */
    public void setTotalPrimeSupplierSales(String totalPrimeSupplierSales) {
	this.totalPrimeSupplierSales = totalPrimeSupplierSales;
    }

    /**
     * Gets the total direct sales.
     * 
     * @return the totalDirectSales
     */
    public String getTotalDirectSales() {
	return totalDirectSales;
    }

    /**
     * Sets the total direct sales.
     * 
     * @param totalDirectSales
     *            the totalDirectSales to set
     */
    public void setTotalDirectSales(String totalDirectSales) {
	this.totalDirectSales = totalDirectSales;
    }

    /**
     * Gets the percentage of direct sales.
     * 
     * @return the percentageOfDirectSales
     */
    public Double getPercentageOfDirectSales() {
	return percentageOfDirectSales;
    }

    /**
     * Sets the percentage of direct sales.
     * 
     * @param percentageOfDirectSales
     *            the percentageOfDirectSales to set
     */
    public void setPercentageOfDirectSales(Double percentageOfDirectSales) {
	this.percentageOfDirectSales = percentageOfDirectSales;
    }

    /**
     * Sets the indirect value percentage.
     * 
     * @param indirectValuePercentage
     *            the indirectValuePercentage to set
     */
    public void setIndirectValuePercentage(Integer indirectValuePercentage) {
	this.indirectValuePercentage = indirectValuePercentage;
    }

    /**
     * Gets the spend datas.
     * 
     * @return the spendDatas
     */
    public List<SpendDataMaster> getSpendDatas() {
	return spendDatas;
    }

    /**
     * Sets the spend datas.
     * 
     * @param spendDatas
     *            the spendDatas to set
     */
    public void setSpendDatas(List<SpendDataMaster> spendDatas) {
	this.spendDatas = spendDatas;
    }

    /**
     * Reset.
     * 
     * @param mapping
     *            the mapping
     * @param request
     *            the request
     * @return the tpss
     */
    /*
     * public String getTpss() { return tpss; }
     *//**
     * @param tpss
     *            the tpss to set
     */
    /*
     * public void setTpss(String tpss) { this.tpss = tpss; }
     *//**
     * @return the tds
     */
    /*
     * public String getTds() { return tds; }
     *//**
     * @param tds
     *            the tds to set
     */
    /*
     * public void setTds(String tds) { this.tds = tds; }
     */

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	// this.vendor = "";
	// this.yearOfData = "";
	// this.spendEndDate = null;
	// this.spendStartDate = null;
	// this.spendDataDTOList=null;

    }

}
