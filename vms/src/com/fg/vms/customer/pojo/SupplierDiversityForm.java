package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;


public class SupplierDiversityForm extends ValidatorForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String firstName;
	
	private String lastName;
	
	private Integer requestorId;
	
	private String createdOn;
	
	private String requestedOn;
	
	private String oppurtunityEventName;
	
	private String[] sourceEvent;
	
	private String sourceEventStartDate;
	
	private Double estimateSpend;
	
	private String estimateWorkStartDate;
	
	private String lastResponseDate;
	
	private String[] commodityCode;
	
	private String scopOfWork;
	
	private String geography;
	
	private String miniSupplierRequirement;
	
	private String contractInsight;
	
	private Integer[] incumbentVendorId;
	
	private String business;
	
	//private List<String> commodityDataList;
	
	private Map<String,String> commodityDataList;
	
	private Map<String,String> incumbentDataList;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(String requestedOn) {
		this.requestedOn = requestedOn;
	}

	public String getOppurtunityEventName() {
		return oppurtunityEventName;
	}

	public void setOppurtunityEventName(String oppurtunityEventName) {
		this.oppurtunityEventName = oppurtunityEventName;
	}

	public String getSourceEventStartDate() {
		return sourceEventStartDate;
	}

	public void setSourceEventStartDate(String sourceEventStartDate) {
		this.sourceEventStartDate = sourceEventStartDate;
	}

	public Double getEstimateSpend() {
		return estimateSpend;
	}

	public void setEstimateSpend(Double estimateSpend) {
		this.estimateSpend = estimateSpend;
	}

	public String getEstimateWorkStartDate() {
		return estimateWorkStartDate;
	}

	public void setEstimateWorkStartDate(String estimateWorkStartDate) {
		this.estimateWorkStartDate = estimateWorkStartDate;
	}

	public String getLastResponseDate() {
		return lastResponseDate;
	}

	public void setLastResponseDate(String lastResponseDate) {
		this.lastResponseDate = lastResponseDate;
	}

	public String getScopOfWork() {
		return scopOfWork;
	}

	public void setScopOfWork(String scopOfWork) {
		this.scopOfWork = scopOfWork;
	}

	public String getGeography() {
		return geography;
	}

	public void setGeography(String geography) {
		this.geography = geography;
	}

	public String getMiniSupplierRequirement() {
		return miniSupplierRequirement;
	}

	public void setMiniSupplierRequirement(String miniSupplierRequirement) {
		this.miniSupplierRequirement = miniSupplierRequirement;
	}

	public String getContractInsight() {
		return contractInsight;
	}

	public void setContractInsight(String contractInsight) {
		this.contractInsight = contractInsight;
	}

	public String[] getSourceEvent() {
		return sourceEvent;
	}

	public void setSourceEvent(String[] sourceEvent) {
		this.sourceEvent = sourceEvent;
	}

	public String[] getCommodityCode() {
		return commodityCode;
	}

	public void setCommodityCode(String[] commodityCode) {
		this.commodityCode = commodityCode;
	}

	public Integer[] getIncumbentVendorId() {
		return incumbentVendorId;
	}

	public void setIncumbentVendorId(Integer[] incumbentVendorId) {
		this.incumbentVendorId = incumbentVendorId;
	}
	
	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		this.firstName=null;
		this.lastName=null;
		this.requestorId=null;
		this.createdOn=null;
		this.requestedOn=null;
		this.oppurtunityEventName=null;
		this.sourceEvent=null;
		this.sourceEventStartDate=null;
		this.estimateSpend=null;
		this.estimateWorkStartDate=null;
		this.lastResponseDate=null;
		this.commodityCode=null;
		this.scopOfWork=null;
		this.geography=null;
		this.miniSupplierRequirement=null;
		this.contractInsight=null;
		this.incumbentVendorId=null;
		this.business=null;
		this.id=null;
	}

	public Map<String,String> getCommodityDataList() {
		return commodityDataList;
	}

	public void setCommodityDataList(Map<String,String> commodityDataList) {
		this.commodityDataList = commodityDataList;
	}

	public Map<String,String> getIncumbentDataList() {
		return incumbentDataList;
	}

	public void setIncumbentDataList(Map<String,String> incumbentDataList) {
		this.incumbentDataList = incumbentDataList;
	}

//	public List<String> getCommodityDataList() {
//		return commodityDataList;
//	}
//
//	public void setCommodityDataList(List<String> commodityDataList) {
//		this.commodityDataList = commodityDataList;
//	}
}
