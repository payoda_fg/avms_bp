package com.fg.vms.customer.pojo;

import java.io.Serializable;

public class VendorDiverseCertificate implements Serializable{
	
	private String certiicateName;
	private String certificateType;
	private String certificateNumber;
	private String effectiveDate;
	private String expiryDate;
	private String certAgencyName;
	private String ethinicity;
	
	public String getCertiicateName() {
		return certiicateName;
	}
	public void setCertiicateName(String certiicateName) {
		this.certiicateName = certiicateName;
	}
	public String getCertificateType() {
		return certificateType;
	}
	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getCertAgencyName() {
		return certAgencyName;
	}
	public void setCertAgencyName(String certAgencyName) {
		this.certAgencyName = certAgencyName;
	}
	public String getEthinicity() {
		return ethinicity;
	}
	public void setEthinicity(String ethinicity) {
		this.ethinicity = ethinicity;
	}
	
	

}
