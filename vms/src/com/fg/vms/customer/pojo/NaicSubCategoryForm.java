/*
 * NaicSubCategoryForm.java 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * Represents the Naics sub-category details (eg. sub-category description,
 * naics category detail) to interact with UI.
 * 
 * @author vinoth
 * 
 */
public class NaicSubCategoryForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private int id;

    private Integer naicCategory = 0;

    private String naicSubCategoryDesc;

    private String isActive = "1";

    /**
     * @return the id
     */
    public int getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
	this.id = id;
    }

    /**
     * @return the naicSubCategoryDesc
     */
    public String getNaicSubCategoryDesc() {
	return naicSubCategoryDesc;
    }

    /**
     * @param naicSubCategoryDesc
     *            the naicSubCategoryDesc to set
     */
    public void setNaicSubCategoryDesc(String naicSubCategoryDesc) {
	this.naicSubCategoryDesc = naicSubCategoryDesc;
    }

    /**
     * @return the naicCategory
     */
    public Integer getNaicCategory() {
	return naicCategory;
    }

    /**
     * @param naicCategory
     *            the naicCategory to set
     */
    public void setNaicCategory(Integer naicCategory) {
	this.naicCategory = naicCategory;
    }

    /**
     * @return the isActive
     */
    public String getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(String isActive) {
	this.isActive = isActive;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.naicSubCategoryDesc = "";
	this.naicCategory = 0;
    }

}
