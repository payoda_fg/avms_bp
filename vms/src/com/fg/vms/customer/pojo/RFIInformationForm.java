/*
 * RFIInformationForm.java 
 */
package com.fg.vms.customer.pojo;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.dto.RFIPDto;
import com.fg.vms.customer.model.Currency;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.VendorMaster;

/**
 * Represents the RFI details such as RF information, Compliance, Documents and
 * Attributes.
 * 
 * @author pirabu
 * 
 */
public class RFIInformationForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String rfType;

	private String rfiNumber;

	private String contactPerson;

	private String rfiStartDate;

	private String rfiEndDate;

	private String phoneNumber;

	private String emailId;

	private String[] vendors;

	private String[] allVendors;

	private List<VendorMaster> listOfVendors;

	private String rfiDescription;

	private String currency;

	private List<Currency> currencies;

	private ArrayList<FormFile> docs;

	private String titles[];

	private String complianceItems[];

	private List<String> targets = new ArrayList<String>();

	private String attribute[];

	private List<String> mandatories = new ArrayList<String>();

	private String weightages[];

	private String notes[];

	private List<NaicsCategoryDto> categories;

	private List<NAICSubCategory> subCategories;

	private String category;

	private String subCategory;

	private Integer lastrowcount;

	private Integer filelastrowcount;

	private Integer attributeCount[];

	private List<RFIPDto> rfInformations;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the rfType
	 */
	public String getRfType() {
		return rfType;
	}

	/**
	 * @param rfType
	 *            the rfType to set
	 */
	public void setRfType(String rfType) {
		this.rfType = rfType;
	}

	/**
	 * @return the rfiNumber
	 */
	public String getRfiNumber() {
		return rfiNumber;
	}

	/**
	 * @param rfiNumber
	 *            the rfiNumber to set
	 */
	public void setRfiNumber(String rfiNumber) {
		this.rfiNumber = rfiNumber;
	}

	/**
	 * @return the contactPerson
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson
	 *            the contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return the rfiStartDate
	 */
	public String getRfiStartDate() {
		return rfiStartDate;
	}

	/**
	 * @param rfiStartDate
	 *            the rfiStartDate to set
	 */
	public void setRfiStartDate(String rfiStartDate) {
		this.rfiStartDate = rfiStartDate;
	}

	/**
	 * @return the rfiEndDate
	 */
	public String getRfiEndDate() {
		return rfiEndDate;
	}

	/**
	 * @param rfiEndDate
	 *            the rfiEndDate to set
	 */
	public void setRfiEndDate(String rfiEndDate) {
		this.rfiEndDate = rfiEndDate;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the vendors
	 */
	public String[] getVendors() {
		return vendors;
	}

	/**
	 * @param vendors
	 *            the vendors to set
	 */
	public void setVendors(String[] vendors) {
		this.vendors = vendors;
	}

	/**
	 * @return the allVendors
	 */
	public String[] getAllVendors() {
		return allVendors;
	}

	/**
	 * @param allVendors
	 *            the allVendors to set
	 */
	public void setAllVendors(String[] allVendors) {
		this.allVendors = allVendors;
	}

	/**
	 * @return the rfiDescription
	 */
	public String getRfiDescription() {
		return rfiDescription;
	}

	/**
	 * @param rfiDescription
	 *            the rfiDescription to set
	 */
	public void setRfiDescription(String rfiDescription) {
		this.rfiDescription = rfiDescription;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the docs
	 */
	public ArrayList<FormFile> getDocs() {
		return docs;
	}

	/**
	 * @param docs
	 *            the docs to set
	 */
	public void setDocs(ArrayList<FormFile> docs) {
		this.docs = docs;
	}

	/**
	 * @return the titles
	 */
	public String[] getTitles() {
		return titles;
	}

	/**
	 * @param titles
	 *            the titles to set
	 */
	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	/**
	 * @return the complianceItems
	 */
	public String[] getComplianceItems() {
		return complianceItems;
	}

	/**
	 * @param complianceItems
	 *            the complianceItems to set
	 */
	public void setComplianceItems(String[] complianceItems) {
		this.complianceItems = complianceItems;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getTarget() {
		return this.targets;
	}

	/**
	 * 
	 * @param iIndex
	 * @param target
	 */
	public void setTarget(int iIndex, String target) {
		if (target != null && !target.equals("")) {
			if (this.targets.size() <= iIndex) {
				// Fill the list to the specified size
				for (int i = this.targets.size(); i < iIndex; i++) {
					this.targets.add(null);
				}
				this.targets.add(target);
			} else {
				this.targets.set(iIndex, target);
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getMandatory() {
		return this.mandatories;
	}

	/**
	 * 
	 * @param iIndex
	 * @param mandatory
	 */
	public void setMandatory(int iIndex, String mandatory) {
		if (mandatory != null && !mandatory.equals("")) {
			if (this.mandatories.size() <= iIndex) {
				// Fill the list to the specified size
				for (int i = this.mandatories.size(); i < iIndex; i++) {
					this.mandatories.add(null);
				}
				this.mandatories.add(mandatory);
			} else {
				this.mandatories.set(iIndex, mandatory);
			}
		}
	}

	/**
	 * @return the attribute
	 */
	public String[] getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute
	 *            the attribute to set
	 */
	public void setAttribute(String[] attribute) {
		this.attribute = attribute;
	}

	/**
	 * @return the weightages
	 */
	public String[] getWeightages() {
		return weightages;
	}

	/**
	 * @param weightages
	 *            the weightages to set
	 */
	public void setWeightages(String[] weightages) {
		this.weightages = weightages;
	}

	/**
	 * @return the notes
	 */
	public String[] getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String[] notes) {
		this.notes = notes;
	}

	/**
	 * @return the categories
	 */
	public List<NaicsCategoryDto> getCategories() {
		return categories;
	}

	/**
	 * @param categories
	 *            the categories to set
	 */
	public void setCategories(List<NaicsCategoryDto> categories) {
		this.categories = categories;
	}

	/**
	 * @return the subCategories
	 */
	public List<NAICSubCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * @param subCategories
	 *            the subCategories to set
	 */
	public void setSubCategories(List<NAICSubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}

	/**
	 * @param subCategory
	 *            the subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * @return the lastrowcount
	 */
	public Integer getLastrowcount() {
		return lastrowcount;
	}

	/**
	 * @param lastrowcount
	 *            the lastrowcount to set
	 */
	public void setLastrowcount(Integer lastrowcount) {
		this.lastrowcount = lastrowcount;
	}

	/**
	 * @return the filelastrowcount
	 */
	public Integer getFilelastrowcount() {
		return filelastrowcount;
	}

	/**
	 * @param filelastrowcount
	 *            the filelastrowcount to set
	 */
	public void setFilelastrowcount(Integer filelastrowcount) {
		this.filelastrowcount = filelastrowcount;
	}

	/**
	 * @return the attributeCount
	 */
	public Integer[] getAttributeCount() {
		return attributeCount;
	}

	/**
	 * @param attributeCount
	 *            the attributeCount to set
	 */
	public void setAttributeCount(Integer[] attributeCount) {
		this.attributeCount = attributeCount;
	}

	/**
	 * @return the currencies
	 */
	public List<Currency> getCurrencies() {
		return currencies;
	}

	/**
	 * @param currencies
	 *            the currencies to set
	 */
	public void setCurrencies(List<Currency> currencies) {
		this.currencies = currencies;
	}

	/**
	 * @return the listOfVendors
	 */
	public List<VendorMaster> getListOfVendors() {
		return listOfVendors;
	}

	/**
	 * @return the rfInformations
	 */
	public List<RFIPDto> getRfInformations() {
		return rfInformations;
	}

	/**
	 * @param rfInformations
	 *            the rfInformations to set
	 */
	public void setRfInformations(List<RFIPDto> rfInformations) {
		this.rfInformations = rfInformations;
	}

	/**
	 * @param listOfVendors
	 *            the listOfVendors to set
	 */
	public void setListOfVendors(List<VendorMaster> listOfVendors) {
		this.listOfVendors = listOfVendors;
	}

}
