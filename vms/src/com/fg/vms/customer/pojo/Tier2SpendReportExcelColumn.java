package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Kaleswararao
 *
 */
public class Tier2SpendReportExcelColumn implements Serializable {
	
	private Integer year;
	private Date periodStartDate;
	private Date periodEndDate;
	private Double totalSales;
	private Double totalSales_TO_Company;
	private Double indirect_Allocate_Per;
	private String comments;
	private String startDate_EndDate;
	
	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}
	/**
	 * @return the periodStartDate
	 */
	public Date getPeriodStartDate() {
		return periodStartDate;
	}
	/**
	 * @param periodStartDate the periodStartDate to set
	 */
	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}
	/**
	 * @return the periodEndDate
	 */
	public Date getPeriodEndDate() {
		return periodEndDate;
	}
	/**
	 * @param periodEndDate the periodEndDate to set
	 */
	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}
	/**
	 * @return the totalSales
	 */
	public Double getTotalSales() {
		return totalSales;
	}
	/**
	 * @param totalSales the totalSales to set
	 */
	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}
	/**
	 * @return the totalSales_TO_Company
	 */
	public Double getTotalSales_TO_Company() {
		return totalSales_TO_Company;
	}
	/**
	 * @param totalSales_TO_Company the totalSales_TO_Company to set
	 */
	public void setTotalSales_TO_Company(Double totalSales_TO_Company) {
		this.totalSales_TO_Company = totalSales_TO_Company;
	}
	/**
	 * @return the indirect_Allocate_Per
	 */
	public Double getIndirect_Allocate_Per() {
		return indirect_Allocate_Per;
	}
	/**
	 * @param indirect_Allocate_Per the indirect_Allocate_Per to set
	 */
	public void setIndirect_Allocate_Per(Double indirect_Allocate_Per) {
		this.indirect_Allocate_Per = indirect_Allocate_Per;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the startDate_EndDate
	 */
	public String getStartDate_EndDate() {
		return startDate_EndDate;
	}
	/**
	 * @param startDate_EndDate the startDate_EndDate to set
	 */
	public void setStartDate_EndDate(String startDate_EndDate) {
		this.startDate_EndDate = startDate_EndDate;
	}
}
