package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.List;

public class VendorMasterBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private Integer vendorId;
	private String vendorName;
	
	private String dunsNumber;
	private String taxId;

	private String emailId;
	private String website;
	private String companytype;
	private String numberOfEmployees;
	private String annualTurnover;

	private String yearOfEstablishment;
	private String deverseSupplier;
	
	
	private String prime;
	private String companyInformation;
	private String businessType;
	private String companyOwnership;
	private String stateSalesTaxId;
	private String stateIncorporation;
	private String vendorDescription;
	private List<VendorAddressInfo> vendorAddressList;
	private List<VendorContactInfo> vendorContactInfoList;
	private List<VendorOwnerDetails> vendorOwnerList;
	private String vendorDiverseClassification;
	private List<VendorDiverseCertificate> vendorDivCertificateList;
	private List<CustomerServiceAreaInfo> vendorServiceArea;
	private List<VendorBussinessArea> vendorBussinessAreaList;
	private List<VendorDetailsBean> vendorsBussinessBiographyList;
	private List<VendorDetailsBean> vendorsSafetyBiographyList;
	private List<VendorDetailsBean> vendorsReferenceList;
	private List<VendorDetailsBean> vendorsOtherCertificatesList;
	private List<VendorDetailsBean> vendorsMeetingInfoList;
	private List<VendorDetailsBean> businessAreaCommodityInfoList;
	private List<VendorDetailsBean> serviceAreaNaicsInfoList;
	private List<VendorDetailsBean> serviceAreaKeywordsInfoList;	
	
	public String getVendorDiverseClassification() {
		return vendorDiverseClassification;
	}
	public void setVendorDiverseClassification(String vendorDiverseClassification) {
		this.vendorDiverseClassification = vendorDiverseClassification;
	}
	public List<VendorContactInfo> getVendorContactInfoList() {
		return vendorContactInfoList;
	}
	public void setVendorContactInfoList(
			List<VendorContactInfo> vendorContactInfoList) {
		this.vendorContactInfoList = vendorContactInfoList;
	}

	public List<VendorAddressInfo> getVendorAddressList() {
		return vendorAddressList;
	}
	public void setVendorAddressList(List<VendorAddressInfo> vendorAddressList) {
		this.vendorAddressList = vendorAddressList;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getDunsNumber() {
		return dunsNumber;
	}
	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getCompanytype() {
		return companytype;
	}
	public void setCompanytype(String companytype) {
		this.companytype = companytype;
	}
	public String getNumberOfEmployees() {
		return numberOfEmployees;
	}
	public void setNumberOfEmployees(String numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}
	public String getAnnualTurnover() {
		return annualTurnover;
	}
	public void setAnnualTurnover(String annualTurnover) {
		this.annualTurnover = annualTurnover;
	}
	public String getYearOfEstablishment() {
		return yearOfEstablishment;
	}
	public void setYearOfEstablishment(String yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}
	public String getDeverseSupplier() {
		return deverseSupplier;
	}
	public void setDeverseSupplier(String deverseSupplier) {
		this.deverseSupplier = deverseSupplier;
	}
	public String getPrime() {
		return prime;
	}
	public void setPrime(String prime) {
		this.prime = prime;
	}
	public String getCompanyInformation() {
		return companyInformation;
	}
	public void setCompanyInformation(String companyInformation) {
		this.companyInformation = companyInformation;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getCompanyOwnership() {
		return companyOwnership;
	}
	public void setCompanyOwnership(String companyOwnership) {
		this.companyOwnership = companyOwnership;
	}
	public String getStateSalesTaxId() {
		return stateSalesTaxId;
	}
	public void setStateSalesTaxId(String stateSalesTaxId) {
		this.stateSalesTaxId = stateSalesTaxId;
	}
	public String getStateIncorporation() {
		return stateIncorporation;
	}
	public void setStateIncorporation(String stateIncorporation) {
		this.stateIncorporation = stateIncorporation;
	}
	public String getVendorDescription() {
		return vendorDescription;
	}
	public void setVendorDescription(String vendorDescription) {
		this.vendorDescription = vendorDescription;
	}
	public List<VendorOwnerDetails> getVendorOwnerList() {
		return vendorOwnerList;
	}
	public void setVendorOwnerList(List<VendorOwnerDetails> vendorOwnerList) {
		this.vendorOwnerList = vendorOwnerList;
	}
	public List<VendorDiverseCertificate> getVendorDivCertificateList() {
		return vendorDivCertificateList;
	}
	public void setVendorDivCertificateList(
			List<VendorDiverseCertificate> vendorDivCertificateList) {
		this.vendorDivCertificateList = vendorDivCertificateList;
	}
	public List<CustomerServiceAreaInfo> getVendorServiceArea() {
		return vendorServiceArea;
	}
	public void setVendorServiceArea(List<CustomerServiceAreaInfo> vendorServiceArea) {
		this.vendorServiceArea = vendorServiceArea;
	}
	public List<VendorBussinessArea> getVendorBussinessAreaList() {
		return vendorBussinessAreaList;
	}
	public void setVendorBussinessAreaList(
			List<VendorBussinessArea> vendorBussinessAreaList) {
		this.vendorBussinessAreaList = vendorBussinessAreaList;
	}
	public List<VendorDetailsBean> getVendorsBussinessBiographyList() {
		return vendorsBussinessBiographyList;
	}
	public void setVendorsBussinessBiographyList(
			List<VendorDetailsBean> vendorsBussinessBiographyList) {
		this.vendorsBussinessBiographyList = vendorsBussinessBiographyList;
	}
	public List<VendorDetailsBean> getVendorsReferenceList() {
		return vendorsReferenceList;
	}
	public void setVendorsReferenceList(List<VendorDetailsBean> vendorsReferenceList) {
		this.vendorsReferenceList = vendorsReferenceList;
	}
	public List<VendorDetailsBean> getVendorsOtherCertificatesList() {
		return vendorsOtherCertificatesList;
	}
	public void setVendorsOtherCertificatesList(
			List<VendorDetailsBean> vendorsOtherCertificatesList) {
		this.vendorsOtherCertificatesList = vendorsOtherCertificatesList;
	}
	public List<VendorDetailsBean> getVendorsMeetingInfoList() {
		return vendorsMeetingInfoList;
	}
	public void setVendorsMeetingInfoList(
			List<VendorDetailsBean> vendorsMeetingInfoList) {
		this.vendorsMeetingInfoList = vendorsMeetingInfoList;
	}
	public List<VendorDetailsBean> getVendorsSafetyBiographyList() {
		return vendorsSafetyBiographyList;
	}
	public void setVendorsSafetyBiographyList(
			List<VendorDetailsBean> vendorsSafetyBiographyList) {
		this.vendorsSafetyBiographyList = vendorsSafetyBiographyList;
	}
	public List<VendorDetailsBean> getBusinessAreaCommodityInfoList() {
		return businessAreaCommodityInfoList;
	}
	public void setBusinessAreaCommodityInfoList(
			List<VendorDetailsBean> businessAreaCommodityInfoList) {
		this.businessAreaCommodityInfoList = businessAreaCommodityInfoList;
	}
	public List<VendorDetailsBean> getServiceAreaNaicsInfoList() {
		return serviceAreaNaicsInfoList;
	}
	public void setServiceAreaNaicsInfoList(
			List<VendorDetailsBean> serviceAreaNaicsInfoList) {
		this.serviceAreaNaicsInfoList = serviceAreaNaicsInfoList;
	}
	
	/**
	 * @return the serviceAreaKeywordsInfoList
	 */
	public List<VendorDetailsBean> getServiceAreaKeywordsInfoList() {
		return serviceAreaKeywordsInfoList;
	}
	
	/**
	 * @param serviceAreaKeywordsInfoList the serviceAreaKeywordsInfoList to set
	 */
	public void setServiceAreaKeywordsInfoList(
			List<VendorDetailsBean> serviceAreaKeywordsInfoList) {
		this.serviceAreaKeywordsInfoList = serviceAreaKeywordsInfoList;
	}
}