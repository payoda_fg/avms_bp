package com.fg.vms.customer.pojo;

import java.io.Serializable;

public class VendorDetailsBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String addresstype;
	private String address;
	private String city;
	private String state;
	private String country;
	private String province;
	private String region;
	private String zipcode;
	private String fax;
	private String mobile;
	private String phone;
	
	private String firstName;
    private String lastName;
    private String designation;
    private String contactPhone;
    private String contactMobile;
    private String contactFax;
    private String contanctEmail;
    private String primaryContact;
    private String isBusinessContact;
    private String isPreparer;
    
    private String ownername;
    private String title;
    private String email;
    private String extension;
    private String gender;
    private String ethinicity;
    private String ownershippercentage;
    private String isactive;
    private String ownerCount;
    
    private String certiicateName;
	private String certificateType;
	private String certificateNumber;
	private String effectiveDate;
	private String expiryDate;
	private String certAgencyName;

	private Integer serialNum;
	private String bussinessGroupName;
	private String areaName;
	
	private String questions;
	private String bussinessSafety;
	private String description;
	private String answers;
	
	private String certificateShortName;
	
	private String refAddress;
	private String refCity;
	private String stateName;
	private String countryName;
	private String refEmailId;
	private String refMobile;
	private String refName;
	private String refPhone;
	private String refExtn;
	private String refZip;
	private String refCompanyName;
	private String refCountyProvince;
	private String referenceCount;
	
	private String contactDate;
	private String contactEvent;
	private String commodityCode;
	private String commoditydesc;
	private String cateogoryCode;
	private String categoryDesc;
	private String sectorDesc;
	private String naicsCode;
	private String naicsdesc;
	private String capabilities;
	private String naicsType;
	private String keyword;
	
	
	public String getAddresstype() {
		return addresstype;
	}
	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactMobile() {
		return contactMobile;
	}
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}
	public String getContactFax() {
		return contactFax;
	}
	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}
	public String getContanctEmail() {
		return contanctEmail;
	}
	public void setContanctEmail(String contanctEmail) {
		this.contanctEmail = contanctEmail;
	}
	public String getPrimaryContact() {
		return primaryContact;
	}
	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}
	public String getIsBusinessContact() {
		return isBusinessContact;
	}
	public void setIsBusinessContact(String isBusinessContact) {
		this.isBusinessContact = isBusinessContact;
	}
	public String getIsPreparer() {
		return isPreparer;
	}
	public void setIsPreparer(String isPreparer) {
		this.isPreparer = isPreparer;
	}
	public String getOwnername() {
		return ownername;
	}
	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEthinicity() {
		return ethinicity;
	}
	public void setEthinicity(String ethinicity) {
		this.ethinicity = ethinicity;
	}
	public String getOwnershippercentage() {
		return ownershippercentage;
	}
	public void setOwnershippercentage(String ownershippercentage) {
		this.ownershippercentage = ownershippercentage;
	}
	public String getIsactive() {
		return isactive;
	}
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	public String getOwnerCount() {
		return ownerCount;
	}
	public void setOwnerCount(String ownerCount) {
		this.ownerCount = ownerCount;
	}
	public String getCertiicateName() {
		return certiicateName;
	}
	public void setCertiicateName(String certiicateName) {
		this.certiicateName = certiicateName;
	}
	public String getCertificateType() {
		return certificateType;
	}
	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getCertAgencyName() {
		return certAgencyName;
	}
	public void setCertAgencyName(String certAgencyName) {
		this.certAgencyName = certAgencyName;
	}
	public Integer getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(Integer serialNum) {
		this.serialNum = serialNum;
	}
	public String getBussinessGroupName() {
		return bussinessGroupName;
	}
	public void setBussinessGroupName(String bussinessGroupName) {
		this.bussinessGroupName = bussinessGroupName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getQuestions() {
		return questions;
	}
	public void setQuestions(String questions) {
		this.questions = questions;
	}
	public String getBussinessSafety() {
		return bussinessSafety;
	}
	public void setBussinessSafety(String bussinessSafety) {
		this.bussinessSafety = bussinessSafety;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {
		this.answers = answers;
	}
	public String getCertificateShortName() {
		return certificateShortName;
	}
	public void setCertificateShortName(String certificateShortName) {
		this.certificateShortName = certificateShortName;
	}
	public String getRefAddress() {
		return refAddress;
	}
	public void setRefAddress(String refAddress) {
		this.refAddress = refAddress;
	}
	public String getRefCity() {
		return refCity;
	}
	public void setRefCity(String refCity) {
		this.refCity = refCity;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getRefEmailId() {
		return refEmailId;
	}
	public void setRefEmailId(String refEmailId) {
		this.refEmailId = refEmailId;
	}
	public String getRefMobile() {
		return refMobile;
	}
	public void setRefMobile(String refMobile) {
		this.refMobile = refMobile;
	}
	public String getRefName() {
		return refName;
	}
	public void setRefName(String refName) {
		this.refName = refName;
	}
	public String getRefPhone() {
		return refPhone;
	}
	public void setRefPhone(String refPhone) {
		this.refPhone = refPhone;
	}
	public String getRefExtn() {
		return refExtn;
	}
	public void setRefExtn(String refExtn) {
		this.refExtn = refExtn;
	}
	public String getRefZip() {
		return refZip;
	}
	public void setRefZip(String refZip) {
		this.refZip = refZip;
	}
	public String getRefCompanyName() {
		return refCompanyName;
	}
	public void setRefCompanyName(String companyName) {
		this.refCompanyName = companyName;
	}
	public String getRefCountyProvince() {
		return refCountyProvince;
	}
	public void setRefCountyProvince(String countyProvince) {
		this.refCountyProvince = countyProvince;
	}
	public String getReferenceCount() {
		return referenceCount;
	}
	public void setReferenceCount(String referenceCount) {
		this.referenceCount = referenceCount;
	}
	public String getContactDate() {
		return contactDate;
	}
	public void setContactDate(String contactDate) {
		this.contactDate = contactDate;
	}
	public String getContactEvent() {
		return contactEvent;
	}
	public void setContactEvent(String contactEvent) {
		this.contactEvent = contactEvent;
	}
	public String getCommodityCode() {
		return commodityCode;
	}
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}
	public String getCommoditydesc() {
		return commoditydesc;
	}
	public void setCommoditydesc(String commoditydesc) {
		this.commoditydesc = commoditydesc;
	}
	public String getNaicsCode() {
		return naicsCode;
	}
	public void setNaicsCode(String naicsCode) {
		this.naicsCode = naicsCode;
	}
	public String getNaicsdesc() {
		return naicsdesc;
	}
	public void setNaicsdesc(String naicsdesc) {
		this.naicsdesc = naicsdesc;
	}
	public String getCapabilities() {
		return capabilities;
	}
	public void setCapabilities(String capabilities) {
		this.capabilities = capabilities;
	}
	
	public String getCateogoryCode() {
		return cateogoryCode;
	}
	public void setCateogoryCode(String cateogoryCode) {
		this.cateogoryCode = cateogoryCode;
	}
	public String getCategoryDesc() {
		return categoryDesc;
	}
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	public String getNaicsType() {
		return naicsType;
	}
	public void setNaicsType(String naicsType) {
		this.naicsType = naicsType;
	}
	
	/**
	 * @return the sectorDesc
	 */
	public String getSectorDesc() {
		return sectorDesc;
	}
	
	/**
	 * @param sectorDesc the sectorDesc to set
	 */
	public void setSectorDesc(String sectorDesc) {
		this.sectorDesc = sectorDesc;
	}
	
	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}
	
	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}	
}