package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.common.Country;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.EmailHistoryDto;
import com.fg.vms.customer.dto.MeetingNotesDto;
import com.fg.vms.customer.dto.RFIRPFNotesDto;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerBusinessType;
import com.fg.vms.customer.model.CustomerCertificateBusinesAreaConfig;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerLegalStructure;
import com.fg.vms.customer.model.CustomerWflog;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.model.Region;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Bean class for vendor form which includes vendor registration, vendor
 * contacts, NAICS codes properties.
 * 
 * @author srinivasarao
 */
public class VendorMasterForm extends ValidatorForm implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String vendorName;
	private String vendorCode;
	private String hiddenVendorCode;
	private String dunsNumber;
	// private String ethnicity;
	private String taxId;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String state;
	private String province;
	private String zipcode;
	private String region;
	private String country;
	private String phone;
	private String mobile;
	private String fax;
	private String emailId;
	private String website;
	private String companytype;
	private String numberOfEmployees;
	private String annualTurnover;
	private String annualTurnoverFormat;
	private String sales2;
	private String salesFormat2;
	private String sales3;
	private String salesFormat3;
	private Integer annualYear1;
	private Integer annualYear2;
	private Integer annualYear3;
	private Integer annualTurnoverId1;
	private Integer annualTurnoverId2;
	private Integer annualTurnoverId3;
	private String yearOfEstablishment;
	private String deverseSupplier;
	private String naicsCode_0;
	private String naicsCode_1;
	private String naicsCode_2;
	private String capabilitie1;
	private String capabilitie2;
	private String capabilitie3;
	private String naicsDesc1;
	private String naicsDesc2;
	private String naicsDesc3;
	private String[] naicsCode;

	private String ownerPublic;
	private String ownerPrivate;
	private Integer[] certificates;
	private String divCertType1;
	private String divCertType2;
	private String divCertType3;
	private String divCertAgen1;
	private String divCertAgen2;
	private String divCertAgen3;
	private String certificationNo1;
	private String certificationNo2;
	private String certificationNo3;
	private String effDate1;
	private String effDate2;
	private String effDate3;
	private String expDate1;
	private String expDate2;
	private String expDate3;
	private FormFile certFile1;
	private FormFile certFile2;
	private FormFile certFile3;
	private Integer certFile1Id;
	private Integer certFile2Id;
	private Integer certFile3Id;
	private String ethnicity1;
	private String ethnicity2;
	private String ethnicity3;
	private Integer otherCertFileId;
	private List<String> primaries = new ArrayList<String>();
	private String primary;
	private Integer[] divCertType;
	private Integer[] divCertAgen;
	private String[] expDate;
	// private ArrayList<FormFile> certFile;
	private String[] certPath;
	private String[] naicsDesc;
	private String[] isicCode;
	private String[] isicDesc;
	private String[] capabilities;
	private Integer filelastrowcount;
	private Integer naicslastrowcount;
	private String primeNonPrimeVendor;
	private int parentVendorId;
	private Byte isApproved;
	private String firstName;
	private String middleName;
	private String lastName;
	private String designation;
	private String contactPhone;
	private String contactMobile;
	private String contactFax;
	private String contanctEmail;
	private boolean loginAllowed;
	private String loginDisplayName;
	// private String loginId;
	private String loginpassword;
	private String confirmPassword;
	private String primaryContact;
	private int userSecQn;
	private String userSecQnAns;
	private Integer[] otherCertType1;
	private String otherCertPath;
	private String[] otherExpiryDate;
	private int naicCategory;
	private int naicSubCategory;
	private int lastrowcount;
	private List<NaicsMaster> naicsCodes;
	private List<VendorNAICS> vendorNaics;
	private List<VendorCertificate> vendorCetrificate;
	private List<DiverseCertificationTypesDto> diverseCertificats;
	private List<Ethnicity> ethnicities;
	private Byte select;
	private List<Country> countries;
	private CustomerWflog wflog;
	private WorkflowConfiguration configuration;
	private String minorityPercent;
	private String womenPercent;
	private String classificationNotes;
	private String vendorNotes;
	private String vendorrfiNotes;
	private FormFile vendorDoc;
	private Map<Integer, FormFile> vendorDocs;
	int index1;
	
	private String[] vendorKeyword;
	private String[] vendorDocName;
	private String[] vendorDocDesc;
	private Integer vendorCertID1;
	private Integer vendorCertID2;
	private Integer vendorCertID3;
	private Integer naicsID1;
	private Integer naicsID2;
	private Integer naicsID3;
	private String manualFlag;
	private String[] geographicState;
	private String geographicRegion;
	private String[] newGeographicState;
	private List<State> states;
	private String[] geographicalServiceArea;
	private List<Region> regions;
	private String[] commodityCode;
	private String[] geographicAreaId;
	private String[] commodityId;
	private String[] newCommodityCode;
	private String vendorDescription;
	private String stateIncorporation;
	private String stateSalesTaxId;
	private String insurance;
	private String limit;
	private String provider;
	private String carrierName;
	private String expirationDate;
	private String policyNumber;
	private String additionalInsured;
	private String agent;
	private String phoneInsurance;
	private String extInsurance;
	private String faxInsurance;
	private String status;
	private FormFile insuranceFile;
	private String city2;
	private String state2;
	private String province2;
	private String zipcode2;
	private String region2;
	private String country2;
	private String phone2;
	private String mobile2;
	private String fax2;
	private String businessType;
	private String companyOwnership;
	private String ownerName1;
	private String ownerTitle1;
	private String ownerEmail1;
	private String ownerGender1;
	private Integer ownerEthnicity1;
	private String ownerOwnership1;
	private String ownerPhone1;
	private String ownerExt1;
	private String ownerMobile1;

	private String ownerName2;
	private String ownerTitle2;
	private String ownerEmail2;
	private String ownerGender2;
	private Integer ownerEthnicity2;
	private String ownerOwnership2;
	private String ownerPhone2;
	private String ownerExt2;
	private String ownerMobile2;

	private String ownerName3;
	private String ownerTitle3;
	private String ownerEmail3;
	private String ownerGender3;
	private Integer ownerEthnicity3;
	private String ownerOwnership3;
	private String ownerPhone3;
	private String ownerExt3;
	private String ownerMobile3;

	private Integer ownerId1;
	private Integer ownerId2;
	private Integer ownerId3;

	private String certType1;
	private String certType2;
	private String certType3;
	private List<CertifyingAgency> certAgencies1;
	private List<CertifyingAgency> certAgencies2;
	private List<CertifyingAgency> certAgencies3;
	private List<CustomerCertificateType> certificateTypes1;
	private List<CustomerCertificateType> certificateTypes2;
	private List<CustomerCertificateType> certificateTypes3;
	private Byte expOilGas;
	private String workPlace;
	private String listClients;

	private Byte isBpSupplier;
	private String bpContact;
	private String bpDivision;
	private String bpContactPhone;
	private String bpContactPhoneExt;
	private Byte isCurrentBpSupplier;
	private String typeBpSupplier;
	private String currentBpSupplierDesc;
	private Byte isFortune;
	private String fortuneList;
	private Byte isUnionWorkforce;
	private Byte isNonUnionWorkforce;

	private Byte isPicsCertified;
	private Byte isNetworldCertified;
	private Byte isIso9000Certified;
	private Byte isIso14000Certified;
	private Byte isOshaRecd;
	private Byte isOshaAvg;
	private String isOshaAvgTxt;
	private Byte isEmr;
	private Byte isOshaAgree;
	private Byte isNib;
	private String isEmrTxt;
	private String jobsDesc;
	private String custLocation;
	private String custContact;
	private String telephone;
	private String workType;
	private String size;

	private String referenceCompanyName1;
	private String referenceName1;
	private String referenceAddress1;
	private String referencePhone1;
	private String referenceMailId1;
	private String referenceMobile1;
	private String referenceCity1;
	private String referenceZip1;
	private String referenceState1;
	private String referenceCountry1;
	private String referenceCompanyName2;
	private String referenceName2;
	private String referenceAddress2;
	private String referencePhone2;
	private String referenceMailId2;
	private String referenceMobile2;
	private String referenceCity2;
	private String referenceZip2;
	private String referenceState2;
	private String referenceCountry2;
	private String referenceCompanyName3;
	private String referenceName3;
	private String referenceAddress3;
	private String referencePhone3;
	private String referenceMailId3;
	private String referenceMobile3;
	private String referenceCity3;
	private String referenceZip3;
	private String referenceState3;
	private String referenceCountry3;
	private String extension1;
	private String extension2;
	private String extension3;
	private String referenceProvince1;
	private String referenceProvince2;
	private String referenceProvince3;

	private String isPartiallySubmitted;
	private String prime;
	private String nonprime;
	private String bussines[];
	private Integer[] serviceArea;
	private String[] businessArea;
	private String companyInformation;

	private String preparerTitle;
	private String preparerPhone;
	private String preparerMobile;
	private String preparerFax;
	private String preparerEmail;
	private String preparerFirstName;
	private String preparerLastName;
	private String moreInfo;
	private Integer customerDivision;
	private String checkList;
	private FormFile otherCert = null;
	private FormFile networldCert = null;
	private Boolean hasNetWorldCert;

	private HashMap<Integer, FormFile> otherCerts = null;
	private int index;

	private String contactFirstName;
	private String contactLastName;
	private String contactDate;
	private Integer contactState;
	private String contactEvent;

	private List<State> refStates1;
	private List<State> refStates2;
	private List<State> refStates3;
	private List<State> phyStates1;
	private List<State> mailingStates2;
	private List<State> contactStates;

	private List<CustomerBusinessType> businessTypes;
	private List<CustomerLegalStructure> legalStructures;
	private String vendorStatus;
	private List<StatusMaster> statusMasters;
	private String submitType;
	private List<CustomerCertificateBusinesAreaConfig> businessAreaconfig;
	private List<String> vendorErrors;
	private List<VendorStatusPojo> vendorStatusList;
	private String gender;
	
	private String termsCondition;
	
	private FormFile picsCertificate = null;
	private String picsExpireDate;
	private Integer isPics;
	private Integer bpSegmentId;
	private List<MeetingNotesDto> meetingNotesList;
	private List<RFIRPFNotesDto> rfiNotesList;
	private Integer vendorNotesId;
	private Integer vendorrfiNotesId;
	private String vendorRfiRfpNotes;
	
	private List<EmailHistoryDto> emailHistoryList;
	
	private String specificwork;
	private String workstartdate;
	private String contractValue;
	private String siteLocation;
	
	public List<String> getVendorErrors() {
		return vendorErrors;
	}

	public void setVendorErrors(List<String> vendorErrors) {
		this.vendorErrors = vendorErrors;
	}

	public List<CustomerCertificateBusinesAreaConfig> getBusinessAreaconfig() {
		return businessAreaconfig;
	}

	public void setBusinessAreaconfig(
			List<CustomerCertificateBusinesAreaConfig> businessAreaconfig) {
		this.businessAreaconfig = businessAreaconfig;
	}

	public String getSubmitType() {
		return submitType;
	}

	public void setSubmitType(String submitType) {
		this.submitType = submitType;
	}

	public VendorMasterForm() {
		otherCerts = new HashMap<Integer, FormFile>();
		index = 0;
		index1 = 0;
		vendorDocs = new HashMap<Integer, FormFile>();
	}

	public FormFile getOtherCert(int in) {
		return otherCert;
	}

	public void setOtherCert(int in, FormFile t) {
		try {
			this.otherCert = t;
			setOtherCerts(t, in);
			index++;
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
	}

	public Map<Integer, FormFile> getOtherCerts() {
		return otherCerts;
	}

	public void setOtherCerts(FormFile otherCerts, Integer in) {
		this.otherCerts.put(in, otherCerts);
	}

	public FormFile getVendorDoc(int in) {
		return vendorDoc;
	}

	public void setVendorDoc(int in, FormFile t) {
		try {
			// System.out.println("---------Inside setVendorDoc  ::  index1:  "
			// + index1 + "  ::  " + in);

			this.vendorDoc = t;
			setVendorDocs(t, in);
			index1++;
		} catch (Exception e) {
			// System.out.println("Exception in setVendorDoc!" + e);
		}
	}

	public Map<Integer, FormFile> getVendorDocs() {
		return vendorDocs;
	}

	public void setVendorDocs(FormFile vendorDoc, Integer in) {
		this.vendorDocs.put(in, vendorDoc);
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.id = null;
		this.index = 0;
		this.submitType = "";
		this.vendorErrors = null;
		this.contactStates = null;
		this.vendorStatus = "";
		this.statusMasters = null;
		this.index1 = 0;
		this.otherCerts = new HashMap<Integer, FormFile>();
		this.vendorDocs = new HashMap<Integer, FormFile>();
		this.contactEvent = "";
		this.contactFirstName = "";
		this.contactLastName = "";
		this.contactDate = "";
		this.contactState = null;
		this.preparerLastName = "";
		this.preparerFirstName = "";
		this.vendorName = "";
		this.vendorCode = "";
		this.dunsNumber = "";
		this.taxId = "";
		this.address1 = "";
		this.address2 = "";
		this.address3 = "";
		this.moreInfo = "";
		this.city = "";
		this.state = "";
		this.province = "";
		this.zipcode = "";
		this.region = "";
		this.country = "";
		this.phone = "";
		this.mobile = "";
		this.fax = "";
		this.emailId = "";
		this.website = "";
		this.companytype = "";
		this.numberOfEmployees = "";
		this.annualTurnover = "";
		this.sales2 = "";
		this.sales3 = "";
		// this.annualTurnoverFormat = "";
		this.yearOfEstablishment = "";
		this.firstName = "";
		this.lastName = "";
		this.designation = "";
		this.contactPhone = "";
		this.contactMobile = "";
		this.contactFax = "";
		this.contanctEmail = "";
		this.loginDisplayName = "";
		// this.loginId = "";
		this.loginpassword = "";
		this.confirmPassword = "";
		this.userSecQnAns = "";
		this.otherCertPath = "";
		this.userSecQn = 0;
		this.otherCertType1 = null;
		this.otherExpiryDate = null;
		this.otherCert = null;
		this.naicCategory = 0;
		this.naicSubCategory = 0;
		this.capabilitie1 = "";
		this.capabilitie2 = "";
		this.capabilitie3 = "";
		this.naicsCode_0 = "";
		this.naicsCode_1 = "";
		this.naicsCode_2 = "";
		this.naicsDesc1 = "";
		this.naicsDesc2 = "";
		this.naicsDesc3 = "";
		this.divCertAgen1 = "";
		this.divCertAgen2 = "";
		this.divCertAgen3 = "";
		this.divCertType1 = "";
		this.divCertType2 = "";
		this.divCertType3 = "";
		this.certificationNo1 = null;
		this.certificationNo2 = null;
		this.certificationNo3 = null;
		this.effDate1 = "";
		this.effDate2 = "";
		this.effDate3 = "";
		this.expDate1 = "";
		this.expDate2 = "";
		this.expDate3 = "";
		this.otherCertFileId = 0;
		this.certFile1Id = 0;
		this.certFile2Id = 0;
		this.certFile3Id = 0;
		this.deverseSupplier = "";
		this.minorityPercent = null;
		this.womenPercent = null;
		this.classificationNotes = "";
		this.certificates = null;
		this.vendorNotes = "";
		this.vendorrfiNotes= "";
		this.vendorCertID1 = null;
		this.vendorCertID2 = null;
		this.vendorCertID3 = null;
		this.naicsID1 = null;
		this.naicsID2 = null;
		this.naicsID3 = null;
		this.primary = "";
		this.vendorDescription = "";
		this.stateIncorporation = "";
		this.stateSalesTaxId = "";
		this.insurance = "";
		this.limit = "";
		this.provider = "";
		this.carrierName = "";
		this.expirationDate = "";
		this.policyNumber = "";
		this.additionalInsured = "";
		this.agent = "";
		this.phoneInsurance = "";
		this.faxInsurance = "";
		this.status = "";
		this.insuranceFile = null;
		this.city2 = "";
		this.state2 = "";
		this.province2 = "";
		this.zipcode2 = "";
		this.region2 = "";
		this.country2 = "";
		this.phone2 = "";
		this.mobile2 = "";
		this.fax2 = "";
		this.businessType = "";
		this.companyOwnership = "";
		this.ownerName1 = "";
		this.ownerTitle1 = "";
		this.ownerEmail1 = "";
		this.ownerGender1 = "";
		this.ownerEthnicity1 = 0;
		this.ownerOwnership1 = null;
		this.ownerName2 = "";
		this.ownerTitle2 = "";
		this.ownerEmail2 = "";
		this.ownerGender2 = "";
		this.ownerEthnicity2 = 0;
		this.ownerOwnership2 = null;
		this.ownerName3 = "";
		this.ownerTitle3 = "";
		this.ownerEmail3 = "";
		this.ownerGender3 = "";
		this.ownerEthnicity3 = 0;
		this.ownerOwnership3 = null;
		this.ownerExt1 = "";
		this.ownerExt2 = "";
		this.ownerExt3 = "";
		this.ownerPhone1 = "";
		this.ownerPhone2 = "";
		this.ownerPhone3 = "";
		this.ownerMobile1 = "";
		this.ownerMobile2 = "";
		this.ownerMobile3 = "";
		this.annualYear1 = null;
		this.annualYear2 = null;
		this.annualYear3 = null;
		this.annualTurnoverId1 = null;
		this.annualTurnoverId2 = null;
		this.annualTurnoverId3 = null;
		this.ownerId1 = null;
		this.ownerId2 = null;
		this.ownerId3 = null;
		// Fields added for business biography and safety.
		this.expOilGas = -1;
		this.listClients = "";
		this.isBpSupplier = -1;
		this.bpContact = "";
		this.bpDivision = "";
		this.bpContactPhone = "";
		this.bpContactPhoneExt = "";
		this.isCurrentBpSupplier = -1;
		this.typeBpSupplier = "";
		this.workPlace="";
		this.hasNetWorldCert=false;
		this.currentBpSupplierDesc = "";
		this.isFortune = -1;
		this.fortuneList = "";
		this.isUnionWorkforce = -1;
		this.isNonUnionWorkforce = -1;
		this.isPicsCertified = -1;
		this.isNetworldCertified =-1;
		this.isIso9000Certified = -1;
		this.isIso14000Certified = -1;
		this.isOshaRecd = -1;
		this.isOshaAvg = -1;
		this.isOshaAvgTxt = "";
		this.isEmr = -1;
		this.isOshaAgree = -1;
		this.isNib = -1;
		this.isEmrTxt = "";
		this.jobsDesc = "";
		this.custLocation = "";
		this.custContact = "";
		this.telephone = "";
		this.workType = "";
		this.size = "";

		// For reference
		this.referenceCompanyName1 = "";
		this.referenceName1 = "";
		this.referenceAddress1 = "";
		this.referenceMailId1 = "";
		this.referenceMobile1 = "";
		this.referencePhone1 = "";
		this.referenceCompanyName2 = "";
		this.referenceName2 = "";
		this.referenceAddress2 = "";
		this.referenceMailId2 = "";
		this.referenceMobile2 = "";
		this.referencePhone2 = "";
		this.referenceCompanyName3 = "";
		this.referenceName3 = "";
		this.referenceAddress3 = "";
		this.referenceMailId3 = "";
		this.referenceMobile3 = "";
		this.referencePhone3 = "";
		this.referenceCity1 = "";
		this.referenceCity2 = "";
		this.referenceCity3 = "";
		this.referenceState1 = "";
		this.referenceState2 = "";
		this.referenceState3 = "";
		this.referenceZip1 = "";
		this.referenceZip2 = "";
		this.referenceZip3 = "";
		this.referenceCountry1 = "";
		this.referenceCountry2 = "";
		this.referenceCountry3 = "";
		this.extension1 = "";
		this.extension2 = "";
		this.extension3 = "";
		this.certType1 = "";
		this.certType2 = "";
		this.certType3 = "";
		this.referenceProvince1 = "";
		this.referenceProvince2 = "";
		this.referenceProvince3 = "";
		this.isPartiallySubmitted = "No";
		this.ownerPublic = null;
		this.ownerPrivate = null;
		this.nonprime = "";
		this.prime = "";
		this.bussines = null;
		this.businessArea = null;
		this.serviceArea = null;
		this.companyInformation = "";
		this.preparerTitle = "";
		this.preparerPhone = "";
		this.preparerMobile = "";
		this.preparerFax = "";
		this.preparerEmail = "";
		this.refStates1 = null;
		this.refStates2 = null;
		this.refStates3 = null;
		this.phyStates1 = null;
		this.mailingStates2 = null;
		this.businessTypes = null;
		this.legalStructures = null;
		this.newCommodityCode = null;
		this.ethnicity1 = "0";
		this.ethnicity2 = "0";
		this.ethnicity3 = "0";
		this.customerDivision=null;
		this.gender=null;
		this.vendorKeyword = null;
		
		this.picsCertificate = null;
		this.picsExpireDate = null;
		this.isPics = null;
		this.bpSegmentId = null;
		this.meetingNotesList = null;
		this.rfiNotesList = null;
		this.vendorNotesId = null;
		this.vendorrfiNotesId =null;
		
		this.emailHistoryList = null;
		this.vendorRfiRfpNotes = null;
		
		this.specificwork ="";
		this.workstartdate ="";
		this.contractValue ="";
		this.siteLocation ="";
	}

	public String getEthnicity1() {
		return ethnicity1;
	}

	public void setEthnicity1(String ethnicity1) {
		this.ethnicity1 = ethnicity1;
	}

	public String getEthnicity2() {
		return ethnicity2;
	}

	public void setEthnicity2(String ethnicity2) {
		this.ethnicity2 = ethnicity2;
	}

	public String getEthnicity3() {
		return ethnicity3;
	}

	public void setEthnicity3(String ethnicity3) {
		this.ethnicity3 = ethnicity3;
	}

	public List<CustomerBusinessType> getBusinessTypes() {
		return businessTypes;
	}

	public void setBusinessTypes(List<CustomerBusinessType> businessTypes) {
		this.businessTypes = businessTypes;
	}

	public List<CustomerLegalStructure> getLegalStructures() {
		return legalStructures;
	}

	public void setLegalStructures(List<CustomerLegalStructure> legalStructures) {
		this.legalStructures = legalStructures;
	}

	public List<State> getRefStates1() {
		return refStates1;
	}

	public void setRefStates1(List<State> refStates1) {
		this.refStates1 = refStates1;
	}

	public List<State> getRefStates2() {
		return refStates2;
	}

	public void setRefStates2(List<State> refStates2) {
		this.refStates2 = refStates2;
	}

	public List<State> getRefStates3() {
		return refStates3;
	}

	public void setRefStates3(List<State> refStates3) {
		this.refStates3 = refStates3;
	}

	public List<State> getPhyStates1() {
		return phyStates1;
	}

	public void setPhyStates1(List<State> phyStates1) {
		this.phyStates1 = phyStates1;
	}

	public List<State> getMailingStates2() {
		return mailingStates2;
	}

	public void setMailingStates2(List<State> mailingStates2) {
		this.mailingStates2 = mailingStates2;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getHiddenVendorCode() {
		return hiddenVendorCode;
	}

	public void setHiddenVendorCode(String hiddenVendorCode) {
		this.hiddenVendorCode = hiddenVendorCode;
	}

	public String getDunsNumber() {
		return dunsNumber;
	}

	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	// public String getEthnicity() {
	// return ethnicity;
	// }
	//
	// public void setEthnicity(String ethnicity) {
	// this.ethnicity = ethnicity;
	// }

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCompanytype() {
		return companytype;
	}

	public void setCompanytype(String companytype) {
		this.companytype = companytype;
	}

	public String getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public void setNumberOfEmployees(String numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	public String getAnnualTurnover() {
		return annualTurnover;
	}

	public void setAnnualTurnover(String annualTurnover) {
		this.annualTurnover = annualTurnover;
	}

	public String getAnnualTurnoverFormat() {
		return annualTurnoverFormat;
	}

	public void setAnnualTurnoverFormat(String annualTurnoverFormat) {
		this.annualTurnoverFormat = annualTurnoverFormat;
	}

	public String getSales2() {
		return sales2;
	}

	public void setSales2(String sales2) {
		this.sales2 = sales2;
	}

	public String getMoreInfo() {
		return moreInfo;
	}

	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}

	public String getSalesFormat2() {
		return salesFormat2;
	}

	public void setSalesFormat2(String salesFormat2) {
		this.salesFormat2 = salesFormat2;
	}

	public String getSales3() {
		return sales3;
	}

	public void setSales3(String sales3) {
		this.sales3 = sales3;
	}

	public String getSalesFormat3() {
		return salesFormat3;
	}

	public void setSalesFormat3(String salesFormat3) {
		this.salesFormat3 = salesFormat3;
	}

	public Integer getAnnualYear1() {
		return annualYear1;
	}

	public void setAnnualYear1(Integer annualYear1) {
		this.annualYear1 = annualYear1;
	}

	public Integer getAnnualYear2() {
		return annualYear2;
	}

	public void setAnnualYear2(Integer annualYear2) {
		this.annualYear2 = annualYear2;
	}

	public Integer getAnnualYear3() {
		return annualYear3;
	}

	public void setAnnualYear3(Integer annualYear3) {
		this.annualYear3 = annualYear3;
	}

	public Integer getAnnualTurnoverId1() {
		return annualTurnoverId1;
	}

	public void setAnnualTurnoverId1(Integer annualTurnoverId1) {
		this.annualTurnoverId1 = annualTurnoverId1;
	}

	public Integer getAnnualTurnoverId2() {
		return annualTurnoverId2;
	}

	public void setAnnualTurnoverId2(Integer annualTurnoverId2) {
		this.annualTurnoverId2 = annualTurnoverId2;
	}

	public Integer getAnnualTurnoverId3() {
		return annualTurnoverId3;
	}

	public void setAnnualTurnoverId3(Integer annualTurnoverId3) {
		this.annualTurnoverId3 = annualTurnoverId3;
	}

	public String getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	public void setYearOfEstablishment(String yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	public String getDeverseSupplier() {
		return deverseSupplier;
	}

	public void setDeverseSupplier(String deverseSupplier) {
		this.deverseSupplier = deverseSupplier;
	}

	public String getNaicsCode_0() {
		return naicsCode_0;
	}

	public void setNaicsCode_0(String naicsCode_0) {
		this.naicsCode_0 = naicsCode_0;
	}

	public String getNaicsCode_1() {
		return naicsCode_1;
	}

	public void setNaicsCode_1(String naicsCode_1) {
		this.naicsCode_1 = naicsCode_1;
	}

	public String getNaicsCode_2() {
		return naicsCode_2;
	}

	public void setNaicsCode_2(String naicsCode_2) {
		this.naicsCode_2 = naicsCode_2;
	}

	public String getCapabilitie1() {
		return capabilitie1;
	}

	public void setCapabilitie1(String capabilitie1) {
		this.capabilitie1 = capabilitie1;
	}

	public String getCapabilitie2() {
		return capabilitie2;
	}

	public void setCapabilitie2(String capabilitie2) {
		this.capabilitie2 = capabilitie2;
	}

	public String getCapabilitie3() {
		return capabilitie3;
	}

	public void setCapabilitie3(String capabilitie3) {
		this.capabilitie3 = capabilitie3;
	}

	public String getNaicsDesc1() {
		return naicsDesc1;
	}

	public void setNaicsDesc1(String naicsDesc1) {
		this.naicsDesc1 = naicsDesc1;
	}

	public String getNaicsDesc2() {
		return naicsDesc2;
	}

	public void setNaicsDesc2(String naicsDesc2) {
		this.naicsDesc2 = naicsDesc2;
	}

	public String getNaicsDesc3() {
		return naicsDesc3;
	}

	public void setNaicsDesc3(String naicsDesc3) {
		this.naicsDesc3 = naicsDesc3;
	}

	public String[] getNaicsCode() {
		return naicsCode;
	}

	public void setNaicsCode(String[] naicsCode) {
		this.naicsCode = naicsCode;
	}

	public String getOwnerPublic() {
		return ownerPublic;
	}

	public void setOwnerPublic(String ownerPublic) {
		this.ownerPublic = ownerPublic;
	}

	public String getOwnerPrivate() {
		return ownerPrivate;
	}

	public void setOwnerPrivate(String ownerPrivate) {
		this.ownerPrivate = ownerPrivate;
	}

	public Integer[] getCertificates() {
		return certificates;
	}

	public void setCertificates(Integer[] certificates) {
		this.certificates = certificates;
	}

	public String getDivCertType1() {
		return divCertType1;
	}

	public void setDivCertType1(String divCertType1) {
		this.divCertType1 = divCertType1;
	}

	public String getDivCertType2() {
		return divCertType2;
	}

	public void setDivCertType2(String divCertType2) {
		this.divCertType2 = divCertType2;
	}

	public String getDivCertType3() {
		return divCertType3;
	}

	public void setDivCertType3(String divCertType3) {
		this.divCertType3 = divCertType3;
	}

	public String getDivCertAgen1() {
		return divCertAgen1;
	}

	public void setDivCertAgen1(String divCertAgen1) {
		this.divCertAgen1 = divCertAgen1;
	}

	public String getDivCertAgen2() {
		return divCertAgen2;
	}

	public void setDivCertAgen2(String divCertAgen2) {
		this.divCertAgen2 = divCertAgen2;
	}

	public String getDivCertAgen3() {
		return divCertAgen3;
	}

	public void setDivCertAgen3(String divCertAgen3) {
		this.divCertAgen3 = divCertAgen3;
	}

	public String getCertificationNo1() {
		return certificationNo1;
	}

	public void setCertificationNo1(String certificationNo1) {
		this.certificationNo1 = certificationNo1;
	}

	public String getCertificationNo2() {
		return certificationNo2;
	}

	public void setCertificationNo2(String certificationNo2) {
		this.certificationNo2 = certificationNo2;
	}

	public String getCertificationNo3() {
		return certificationNo3;
	}

	public void setCertificationNo3(String certificationNo3) {
		this.certificationNo3 = certificationNo3;
	}

	public String getEffDate1() {
		return effDate1;
	}

	public void setEffDate1(String effDate1) {
		this.effDate1 = effDate1;
	}

	public String getEffDate2() {
		return effDate2;
	}

	public void setEffDate2(String effDate2) {
		this.effDate2 = effDate2;
	}

	public String getEffDate3() {
		return effDate3;
	}

	public void setEffDate3(String effDate3) {
		this.effDate3 = effDate3;
	}

	public String getExpDate1() {
		return expDate1;
	}

	public void setExpDate1(String expDate1) {
		this.expDate1 = expDate1;
	}

	public String getExpDate2() {
		return expDate2;
	}

	public void setExpDate2(String expDate2) {
		this.expDate2 = expDate2;
	}

	public String getExpDate3() {
		return expDate3;
	}

	public void setExpDate3(String expDate3) {
		this.expDate3 = expDate3;
	}

	public FormFile getCertFile1() {
		return certFile1;
	}

	public void setCertFile1(FormFile certFile1) {
		this.certFile1 = certFile1;
	}

	public FormFile getCertFile2() {
		return certFile2;
	}

	public void setCertFile2(FormFile certFile2) {
		this.certFile2 = certFile2;
	}

	public FormFile getCertFile3() {
		return certFile3;
	}

	public void setCertFile3(FormFile certFile3) {
		this.certFile3 = certFile3;
	}

	public Integer getCertFile1Id() {
		return certFile1Id;
	}

	public void setCertFile1Id(Integer certFile1Id) {
		this.certFile1Id = certFile1Id;
	}

	public Integer getCertFile2Id() {
		return certFile2Id;
	}

	public void setCertFile2Id(Integer certFile2Id) {
		this.certFile2Id = certFile2Id;
	}

	public Integer getCertFile3Id() {
		return certFile3Id;
	}

	public void setCertFile3Id(Integer certFile3Id) {
		this.certFile3Id = certFile3Id;
	}

	public Integer getOtherCertFileId() {
		return otherCertFileId;
	}

	public void setOtherCertFileId(Integer otherCertFileId) {
		this.otherCertFileId = otherCertFileId;
	}

	public List<String> getPrimaries() {
		return primaries;
	}

	public void setPrimaries(List<String> primaries) {
		this.primaries = primaries;
	}

	public String getPrimary() {
		return primary;
	}

	public void setPrimary(String primary) {
		this.primary = primary;
	}

	public Integer[] getDivCertType() {
		return divCertType;
	}

	public void setDivCertType(Integer[] divCertType) {
		this.divCertType = divCertType;
	}

	public Integer[] getDivCertAgen() {
		return divCertAgen;
	}

	public void setDivCertAgen(Integer[] divCertAgen) {
		this.divCertAgen = divCertAgen;
	}

	public String[] getExpDate() {
		return expDate;
	}

	public void setExpDate(String[] expDate) {
		this.expDate = expDate;
	}

	// public ArrayList<FormFile> getCertFile() {
	// return certFile;
	// }
	//
	// public void setCertFile(ArrayList<FormFile> certFile) {
	// this.certFile = certFile;
	// }

	public String[] getCertPath() {
		return certPath;
	}

	public void setCertPath(String[] certPath) {
		this.certPath = certPath;
	}

	public String[] getNaicsDesc() {
		return naicsDesc;
	}

	public void setNaicsDesc(String[] naicsDesc) {
		this.naicsDesc = naicsDesc;
	}

	public String[] getIsicCode() {
		return isicCode;
	}

	public void setIsicCode(String[] isicCode) {
		this.isicCode = isicCode;
	}

	public String[] getIsicDesc() {
		return isicDesc;
	}

	public void setIsicDesc(String[] isicDesc) {
		this.isicDesc = isicDesc;
	}

	public String[] getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(String[] capabilities) {
		this.capabilities = capabilities;
	}

	public Integer getFilelastrowcount() {
		return filelastrowcount;
	}

	public void setFilelastrowcount(Integer filelastrowcount) {
		this.filelastrowcount = filelastrowcount;
	}

	public Integer getNaicslastrowcount() {
		return naicslastrowcount;
	}

	public void setNaicslastrowcount(Integer naicslastrowcount) {
		this.naicslastrowcount = naicslastrowcount;
	}

	public String getPrimeNonPrimeVendor() {
		return primeNonPrimeVendor;
	}

	public void setPrimeNonPrimeVendor(String primeNonPrimeVendor) {
		this.primeNonPrimeVendor = primeNonPrimeVendor;
	}

	public int getParentVendorId() {
		return parentVendorId;
	}

	public void setParentVendorId(int parentVendorId) {
		this.parentVendorId = parentVendorId;
	}

	public Byte getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(Byte isApproved) {
		this.isApproved = isApproved;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public String getContactFax() {
		return contactFax;
	}

	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	public String getContanctEmail() {
		return contanctEmail;
	}

	public void setContanctEmail(String contanctEmail) {
		this.contanctEmail = contanctEmail;
	}

	public boolean isLoginAllowed() {
		return loginAllowed;
	}

	public void setLoginAllowed(boolean loginAllowed) {
		this.loginAllowed = loginAllowed;
	}

	public String getLoginDisplayName() {
		return loginDisplayName;
	}

	public void setLoginDisplayName(String loginDisplayName) {
		this.loginDisplayName = loginDisplayName;
	}

	public String getLoginpassword() {
		return loginpassword;
	}

	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public int getUserSecQn() {
		return userSecQn;
	}

	public void setUserSecQn(int userSecQn) {
		this.userSecQn = userSecQn;
	}

	public String getUserSecQnAns() {
		return userSecQnAns;
	}

	public void setUserSecQnAns(String userSecQnAns) {
		this.userSecQnAns = userSecQnAns;
	}

	public Integer[] getOtherCertType1() {
		return otherCertType1;
	}

	public void setOtherCertType1(Integer[] otherCertType1) {
		this.otherCertType1 = otherCertType1;
	}

	public String getOtherCertPath() {
		return otherCertPath;
	}

	public void setOtherCertPath(String otherCertPath) {
		this.otherCertPath = otherCertPath;
	}

	public String[] getOtherExpiryDate() {
		return otherExpiryDate;
	}

	public void setOtherExpiryDate(String[] otherExpiryDate) {
		this.otherExpiryDate = otherExpiryDate;
	}

	public int getNaicCategory() {
		return naicCategory;
	}

	public void setNaicCategory(int naicCategory) {
		this.naicCategory = naicCategory;
	}

	public int getNaicSubCategory() {
		return naicSubCategory;
	}

	public void setNaicSubCategory(int naicSubCategory) {
		this.naicSubCategory = naicSubCategory;
	}

	public int getLastrowcount() {
		return lastrowcount;
	}

	public void setLastrowcount(int lastrowcount) {
		this.lastrowcount = lastrowcount;
	}

	public List<NaicsMaster> getNaicsCodes() {
		return naicsCodes;
	}

	public void setNaicsCodes(List<NaicsMaster> naicsCodes) {
		this.naicsCodes = naicsCodes;
	}

	public List<VendorNAICS> getVendorNaics() {
		return vendorNaics;
	}

	public void setVendorNaics(List<VendorNAICS> vendorNaics) {
		this.vendorNaics = vendorNaics;
	}

	public List<VendorCertificate> getVendorCetrificate() {
		return vendorCetrificate;
	}

	public void setVendorCetrificate(List<VendorCertificate> vendorCetrificate) {
		this.vendorCetrificate = vendorCetrificate;
	}

	public List<DiverseCertificationTypesDto> getDiverseCertificats() {
		return diverseCertificats;
	}

	public void setDiverseCertificats(
			List<DiverseCertificationTypesDto> diverseCertificats) {
		this.diverseCertificats = diverseCertificats;
	}

	public List<Ethnicity> getEthnicities() {
		return ethnicities;
	}

	public void setEthnicities(List<Ethnicity> ethnicities) {
		this.ethnicities = ethnicities;
	}

	public Byte getSelect() {
		return select;
	}

	public void setSelect(Byte select) {
		this.select = select;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public CustomerWflog getWflog() {
		return wflog;
	}

	public void setWflog(CustomerWflog wflog) {
		this.wflog = wflog;
	}

	public WorkflowConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(WorkflowConfiguration configuration) {
		this.configuration = configuration;
	}

	public String getMinorityPercent() {
		return minorityPercent;
	}

	public void setMinorityPercent(String minorityPercent) {
		this.minorityPercent = minorityPercent;
	}

	public String getWomenPercent() {
		return womenPercent;
	}

	public void setWomenPercent(String womenPercent) {
		this.womenPercent = womenPercent;
	}

	public String getClassificationNotes() {
		return classificationNotes;
	}

	public void setClassificationNotes(String classificationNotes) {
		this.classificationNotes = classificationNotes;
	}

	public String getVendorNotes() {
		return vendorNotes;
	}

	public void setVendorNotes(String vendorNotes) {
		this.vendorNotes = vendorNotes;
	}

	public String[] getVendorDocName() {
		return vendorDocName;
	}

	public void setVendorDocName(String[] vendorDocName) {
		this.vendorDocName = vendorDocName;
	}

	public String[] getVendorDocDesc() {
		return vendorDocDesc;
	}

	public void setVendorDocDesc(String[] vendorDocDesc) {
		this.vendorDocDesc = vendorDocDesc;
	}

	public Integer getVendorCertID1() {
		return vendorCertID1;
	}

	public void setVendorCertID1(Integer vendorCertID1) {
		this.vendorCertID1 = vendorCertID1;
	}

	public Integer getVendorCertID2() {
		return vendorCertID2;
	}

	public void setVendorCertID2(Integer vendorCertID2) {
		this.vendorCertID2 = vendorCertID2;
	}

	public Integer getVendorCertID3() {
		return vendorCertID3;
	}

	public void setVendorCertID3(Integer vendorCertID3) {
		this.vendorCertID3 = vendorCertID3;
	}

	public Integer getNaicsID1() {
		return naicsID1;
	}

	public void setNaicsID1(Integer naicsID1) {
		this.naicsID1 = naicsID1;
	}

	public Integer getNaicsID2() {
		return naicsID2;
	}

	public void setNaicsID2(Integer naicsID2) {
		this.naicsID2 = naicsID2;
	}

	public Integer getNaicsID3() {
		return naicsID3;
	}

	public void setNaicsID3(Integer naicsID3) {
		this.naicsID3 = naicsID3;
	}

	public String getManualFlag() {
		return manualFlag;
	}

	public void setManualFlag(String manualFlag) {
		this.manualFlag = manualFlag;
	}

	public String[] getGeographicState() {
		return geographicState;
	}

	public void setGeographicState(String[] geographicState) {
		this.geographicState = geographicState;
	}

	public String getGeographicRegion() {
		return geographicRegion;
	}

	public void setGeographicRegion(String geographicRegion) {
		this.geographicRegion = geographicRegion;
	}

	public String[] getNewGeographicState() {
		return newGeographicState;
	}

	public void setNewGeographicState(String[] newGeographicState) {
		this.newGeographicState = newGeographicState;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public String[] getGeographicalServiceArea() {
		return geographicalServiceArea;
	}

	public void setGeographicalServiceArea(String[] geographicalServiceArea) {
		this.geographicalServiceArea = geographicalServiceArea;
	}

	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	public String[] getCommodityCode() {
		return commodityCode;
	}

	public void setCommodityCode(String[] commodityCode) {
		this.commodityCode = commodityCode;
	}

	public String[] getGeographicAreaId() {
		return geographicAreaId;
	}

	public void setGeographicAreaId(String[] geographicAreaId) {
		this.geographicAreaId = geographicAreaId;
	}

	public String[] getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(String[] commodityId) {
		this.commodityId = commodityId;
	}

	public String[] getNewCommodityCode() {
		return newCommodityCode;
	}

	public void setNewCommodityCode(String[] newCommodityCode) {
		this.newCommodityCode = newCommodityCode;
	}

	public String getVendorDescription() {
		return vendorDescription;
	}

	public void setVendorDescription(String vendorDescription) {
		this.vendorDescription = vendorDescription;
	}

	public String getStateIncorporation() {
		return stateIncorporation;
	}

	public void setStateIncorporation(String stateIncorporation) {
		this.stateIncorporation = stateIncorporation;
	}

	public String getStateSalesTaxId() {
		return stateSalesTaxId;
	}

	public void setStateSalesTaxId(String stateSalesTaxId) {
		this.stateSalesTaxId = stateSalesTaxId;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getAdditionalInsured() {
		return additionalInsured;
	}

	public void setAdditionalInsured(String additionalInsured) {
		this.additionalInsured = additionalInsured;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getPhoneInsurance() {
		return phoneInsurance;
	}

	public void setPhoneInsurance(String phoneInsurance) {
		this.phoneInsurance = phoneInsurance;
	}

	public String getExtInsurance() {
		return extInsurance;
	}

	public void setExtInsurance(String extInsurance) {
		this.extInsurance = extInsurance;
	}

	public String getFaxInsurance() {
		return faxInsurance;
	}

	public void setFaxInsurance(String faxInsurance) {
		this.faxInsurance = faxInsurance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public FormFile getInsuranceFile() {
		return insuranceFile;
	}

	public void setInsuranceFile(FormFile insuranceFile) {
		this.insuranceFile = insuranceFile;
	}

	public String getCity2() {
		return city2;
	}

	public void setCity2(String city2) {
		this.city2 = city2;
	}

	public String getState2() {
		return state2;
	}

	public void setState2(String state2) {
		this.state2 = state2;
	}

	public String getProvince2() {
		return province2;
	}

	public void setProvince2(String province2) {
		this.province2 = province2;
	}

	public String getZipcode2() {
		return zipcode2;
	}

	public void setZipcode2(String zipcode2) {
		this.zipcode2 = zipcode2;
	}

	public String getRegion2() {
		return region2;
	}

	public void setRegion2(String region2) {
		this.region2 = region2;
	}

	public String getCountry2() {
		return country2;
	}

	public void setCountry2(String country2) {
		this.country2 = country2;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getMobile2() {
		return mobile2;
	}

	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}

	public String getFax2() {
		return fax2;
	}

	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getCompanyOwnership() {
		return companyOwnership;
	}

	public void setCompanyOwnership(String companyOwnership) {
		this.companyOwnership = companyOwnership;
	}

	public String getOwnerName1() {
		return ownerName1;
	}

	public void setOwnerName1(String ownerName1) {
		this.ownerName1 = ownerName1;
	}

	public String getOwnerTitle1() {
		return ownerTitle1;
	}

	public void setOwnerTitle1(String ownerTitle1) {
		this.ownerTitle1 = ownerTitle1;
	}

	public String getOwnerEmail1() {
		return ownerEmail1;
	}

	public void setOwnerEmail1(String ownerEmail1) {
		this.ownerEmail1 = ownerEmail1;
	}

	public String getOwnerGender1() {
		return ownerGender1;
	}

	public void setOwnerGender1(String ownerGender1) {
		this.ownerGender1 = ownerGender1;
	}

	public Integer getOwnerEthnicity1() {
		return ownerEthnicity1;
	}

	public void setOwnerEthnicity1(Integer ownerEthnicity1) {
		this.ownerEthnicity1 = ownerEthnicity1;
	}

	public String getOwnerOwnership1() {
		return ownerOwnership1;
	}

	public void setOwnerOwnership1(String ownerOwnership1) {
		this.ownerOwnership1 = ownerOwnership1;
	}

	public String getOwnerPhone1() {
		return ownerPhone1;
	}

	public void setOwnerPhone1(String ownerPhone1) {
		this.ownerPhone1 = ownerPhone1;
	}

	public String getOwnerExt1() {
		return ownerExt1;
	}

	public void setOwnerExt1(String ownerExt1) {
		this.ownerExt1 = ownerExt1;
	}

	public String getOwnerMobile1() {
		return ownerMobile1;
	}

	public void setOwnerMobile1(String ownerMobile1) {
		this.ownerMobile1 = ownerMobile1;
	}

	public String getOwnerName2() {
		return ownerName2;
	}

	public void setOwnerName2(String ownerName2) {
		this.ownerName2 = ownerName2;
	}

	public String getOwnerTitle2() {
		return ownerTitle2;
	}

	public void setOwnerTitle2(String ownerTitle2) {
		this.ownerTitle2 = ownerTitle2;
	}

	public String getOwnerEmail2() {
		return ownerEmail2;
	}

	public void setOwnerEmail2(String ownerEmail2) {
		this.ownerEmail2 = ownerEmail2;
	}

	public String getOwnerGender2() {
		return ownerGender2;
	}

	public void setOwnerGender2(String ownerGender2) {
		this.ownerGender2 = ownerGender2;
	}

	public Integer getOwnerEthnicity2() {
		return ownerEthnicity2;
	}

	public void setOwnerEthnicity2(Integer ownerEthnicity2) {
		this.ownerEthnicity2 = ownerEthnicity2;
	}

	public String getOwnerOwnership2() {
		return ownerOwnership2;
	}

	public void setOwnerOwnership2(String ownerOwnership2) {
		this.ownerOwnership2 = ownerOwnership2;
	}

	public String getOwnerPhone2() {
		return ownerPhone2;
	}

	public void setOwnerPhone2(String ownerPhone2) {
		this.ownerPhone2 = ownerPhone2;
	}

	public String getOwnerExt2() {
		return ownerExt2;
	}

	public void setOwnerExt2(String ownerExt2) {
		this.ownerExt2 = ownerExt2;
	}

	public String getOwnerMobile2() {
		return ownerMobile2;
	}

	public void setOwnerMobile2(String ownerMobile2) {
		this.ownerMobile2 = ownerMobile2;
	}

	public String getOwnerName3() {
		return ownerName3;
	}

	public void setOwnerName3(String ownerName3) {
		this.ownerName3 = ownerName3;
	}

	public String getOwnerTitle3() {
		return ownerTitle3;
	}

	public void setOwnerTitle3(String ownerTitle3) {
		this.ownerTitle3 = ownerTitle3;
	}

	public String getOwnerEmail3() {
		return ownerEmail3;
	}

	public void setOwnerEmail3(String ownerEmail3) {
		this.ownerEmail3 = ownerEmail3;
	}

	public String getOwnerGender3() {
		return ownerGender3;
	}

	public void setOwnerGender3(String ownerGender3) {
		this.ownerGender3 = ownerGender3;
	}

	public Integer getOwnerEthnicity3() {
		return ownerEthnicity3;
	}

	public void setOwnerEthnicity3(Integer ownerEthnicity3) {
		this.ownerEthnicity3 = ownerEthnicity3;
	}

	public String getOwnerOwnership3() {
		return ownerOwnership3;
	}

	public void setOwnerOwnership3(String ownerOwnership3) {
		this.ownerOwnership3 = ownerOwnership3;
	}

	public String getOwnerPhone3() {
		return ownerPhone3;
	}

	public void setOwnerPhone3(String ownerPhone3) {
		this.ownerPhone3 = ownerPhone3;
	}

	public String getOwnerExt3() {
		return ownerExt3;
	}

	public void setOwnerExt3(String ownerExt3) {
		this.ownerExt3 = ownerExt3;
	}

	public String getOwnerMobile3() {
		return ownerMobile3;
	}

	public void setOwnerMobile3(String ownerMobile3) {
		this.ownerMobile3 = ownerMobile3;
	}

	public Integer getOwnerId1() {
		return ownerId1;
	}

	public void setOwnerId1(Integer ownerId1) {
		this.ownerId1 = ownerId1;
	}

	public Integer getOwnerId2() {
		return ownerId2;
	}

	public void setOwnerId2(Integer ownerId2) {
		this.ownerId2 = ownerId2;
	}

	public Integer getOwnerId3() {
		return ownerId3;
	}

	public void setOwnerId3(Integer ownerId3) {
		this.ownerId3 = ownerId3;
	}

	public String getCertType1() {
		return certType1;
	}

	public void setCertType1(String certType1) {
		this.certType1 = certType1;
	}

	public String getCertType2() {
		return certType2;
	}

	public void setCertType2(String certType2) {
		this.certType2 = certType2;
	}

	public String getCertType3() {
		return certType3;
	}

	public void setCertType3(String certType3) {
		this.certType3 = certType3;
	}

	public Byte getExpOilGas() {
		return expOilGas;
	}

	public void setExpOilGas(Byte expOilGas) {
		this.expOilGas = expOilGas;
	}

	public String getListClients() {
		return listClients;
	}

	public void setListClients(String listClients) {
		this.listClients = listClients;
	}

	public Byte getIsBpSupplier() {
		return isBpSupplier;
	}

	public void setIsBpSupplier(Byte isBpSupplier) {
		this.isBpSupplier = isBpSupplier;
	}

	public String getBpContact() {
		return bpContact;
	}

	public void setBpContact(String bpContact) {
		this.bpContact = bpContact;
	}

	public String getBpDivision() {
		return bpDivision;
	}

	public void setBpDivision(String bpDivision) {
		this.bpDivision = bpDivision;
	}

	public String getBpContactPhone() {
		return bpContactPhone;
	}

	public void setBpContactPhone(String bpContactPhone) {
		this.bpContactPhone = bpContactPhone;
	}

	public String getBpContactPhoneExt() {
		return bpContactPhoneExt;
	}

	public void setBpContactPhoneExt(String bpContactPhoneExt) {
		this.bpContactPhoneExt = bpContactPhoneExt;
	}

	public Byte getIsCurrentBpSupplier() {
		return isCurrentBpSupplier;
	}

	public void setIsCurrentBpSupplier(Byte isCurrentBpSupplier) {
		this.isCurrentBpSupplier = isCurrentBpSupplier;
	}

	public String getCurrentBpSupplierDesc() {
		return currentBpSupplierDesc;
	}

	public void setCurrentBpSupplierDesc(String currentBpSupplierDesc) {
		this.currentBpSupplierDesc = currentBpSupplierDesc;
	}

	public Byte getIsFortune() {
		return isFortune;
	}

	public void setIsFortune(Byte isFortune) {
		this.isFortune = isFortune;
	}

	public String getFortuneList() {
		return fortuneList;
	}

	public void setFortuneList(String fortuneList) {
		this.fortuneList = fortuneList;
	}

	public Byte getIsUnionWorkforce() {
		return isUnionWorkforce;
	}

	public void setIsUnionWorkforce(Byte isUnionWorkforce) {
		this.isUnionWorkforce = isUnionWorkforce;
	}

	public Byte getIsNonUnionWorkforce() {
		return isNonUnionWorkforce;
	}

	public void setIsNonUnionWorkforce(Byte isNonUnionWorkforce) {
		this.isNonUnionWorkforce = isNonUnionWorkforce;
	}

	public Byte getIsPicsCertified() {
		return isPicsCertified;
	}

	public void setIsPicsCertified(Byte isPicsCertified) {
		this.isPicsCertified = isPicsCertified;
	}

	public Byte getIsIso9000Certified() {
		return isIso9000Certified;
	}

	public void setIsIso9000Certified(Byte isIso9000Certified) {
		this.isIso9000Certified = isIso9000Certified;
	}

	public Byte getIsIso14000Certified() {
		return isIso14000Certified;
	}

	public void setIsIso14000Certified(Byte isIso14000Certified) {
		this.isIso14000Certified = isIso14000Certified;
	}

	public Byte getIsOshaRecd() {
		return isOshaRecd;
	}

	public void setIsOshaRecd(Byte isOshaRecd) {
		this.isOshaRecd = isOshaRecd;
	}

	public Byte getIsOshaAvg() {
		return isOshaAvg;
	}

	public void setIsOshaAvg(Byte isOshaAvg) {
		this.isOshaAvg = isOshaAvg;
	}

	public String getIsOshaAvgTxt() {
		return isOshaAvgTxt;
	}

	public void setIsOshaAvgTxt(String isOshaAvgTxt) {
		this.isOshaAvgTxt = isOshaAvgTxt;
	}

	public Byte getIsEmr() {
		return isEmr;
	}

	public void setIsEmr(Byte isEmr) {
		this.isEmr = isEmr;
	}

	public Byte getIsOshaAgree() {
		return isOshaAgree;
	}

	public void setIsOshaAgree(Byte isOshaAgree) {
		this.isOshaAgree = isOshaAgree;
	}

	public Byte getIsNib() {
		return isNib;
	}

	public void setIsNib(Byte isNib) {
		this.isNib = isNib;
	}

	public String getIsEmrTxt() {
		return isEmrTxt;
	}

	public void setIsEmrTxt(String isEmrTxt) {
		this.isEmrTxt = isEmrTxt;
	}

	public String getJobsDesc() {
		return jobsDesc;
	}

	public void setJobsDesc(String jobsDesc) {
		this.jobsDesc = jobsDesc;
	}

	public String getCustLocation() {
		return custLocation;
	}

	public void setCustLocation(String custLocation) {
		this.custLocation = custLocation;
	}

	public String getCustContact() {
		return custContact;
	}

	public void setCustContact(String custContact) {
		this.custContact = custContact;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getWorkType() {
		return workType;
	}

	public void setWorkType(String workType) {
		this.workType = workType;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public List<State> getContactStates() {
		return contactStates;
	}

	public String getReferenceCompanyName1() {
		return referenceCompanyName1;
	}

	public void setReferenceCompanyName1(String referenceCompanyName1) {
		this.referenceCompanyName1 = referenceCompanyName1;
	}

	public void setContactStates(List<State> contactStates) {
		this.contactStates = contactStates;
	}

	public String getReferenceName1() {
		return referenceName1;
	}

	public void setReferenceName1(String referenceName1) {
		this.referenceName1 = referenceName1;
	}

	public String getReferenceAddress1() {
		return referenceAddress1;
	}

	public void setReferenceAddress1(String referenceAddress1) {
		this.referenceAddress1 = referenceAddress1;
	}

	public String getReferencePhone1() {
		return referencePhone1;
	}

	public void setReferencePhone1(String referencePhone1) {
		this.referencePhone1 = referencePhone1;
	}

	public String getReferenceMailId1() {
		return referenceMailId1;
	}

	public void setReferenceMailId1(String referenceMailId1) {
		this.referenceMailId1 = referenceMailId1;
	}

	public String getReferenceMobile1() {
		return referenceMobile1;
	}

	public void setReferenceMobile1(String referenceMobile1) {
		this.referenceMobile1 = referenceMobile1;
	}

	public String getReferenceCity1() {
		return referenceCity1;
	}

	public void setReferenceCity1(String referenceCity1) {
		this.referenceCity1 = referenceCity1;
	}

	public String getReferenceZip1() {
		return referenceZip1;
	}

	public void setReferenceZip1(String referenceZip1) {
		this.referenceZip1 = referenceZip1;
	}

	public String getReferenceState1() {
		return referenceState1;
	}

	public void setReferenceState1(String referenceState1) {
		this.referenceState1 = referenceState1;
	}

	public String getReferenceCountry1() {
		return referenceCountry1;
	}

	public void setReferenceCountry1(String referenceCountry1) {
		this.referenceCountry1 = referenceCountry1;
	}

	public String getReferenceCompanyName2() {
		return referenceCompanyName2;
	}

	public void setReferenceCompanyName2(String referenceCompanyName2) {
		this.referenceCompanyName2 = referenceCompanyName2;
	}

	public String getReferenceName2() {
		return referenceName2;
	}

	public void setReferenceName2(String referenceName2) {
		this.referenceName2 = referenceName2;
	}

	public String getReferenceAddress2() {
		return referenceAddress2;
	}

	public void setReferenceAddress2(String referenceAddress2) {
		this.referenceAddress2 = referenceAddress2;
	}

	public String getReferencePhone2() {
		return referencePhone2;
	}

	public void setReferencePhone2(String referencePhone2) {
		this.referencePhone2 = referencePhone2;
	}

	public String getReferenceMailId2() {
		return referenceMailId2;
	}

	public void setReferenceMailId2(String referenceMailId2) {
		this.referenceMailId2 = referenceMailId2;
	}

	public String getReferenceMobile2() {
		return referenceMobile2;
	}

	public void setReferenceMobile2(String referenceMobile2) {
		this.referenceMobile2 = referenceMobile2;
	}

	public String getReferenceCity2() {
		return referenceCity2;
	}

	public void setReferenceCity2(String referenceCity2) {
		this.referenceCity2 = referenceCity2;
	}

	public String getReferenceZip2() {
		return referenceZip2;
	}

	public void setReferenceZip2(String referenceZip2) {
		this.referenceZip2 = referenceZip2;
	}

	public String getReferenceState2() {
		return referenceState2;
	}

	public void setReferenceState2(String referenceState2) {
		this.referenceState2 = referenceState2;
	}

	public String getReferenceCountry2() {
		return referenceCountry2;
	}

	public void setReferenceCountry2(String referenceCountry2) {
		this.referenceCountry2 = referenceCountry2;
	}

	public String getReferenceCompanyName3() {
		return referenceCompanyName3;
	}

	public void setReferenceCompanyName3(String referenceCompanyName3) {
		this.referenceCompanyName3 = referenceCompanyName3;
	}

	public String getReferenceName3() {
		return referenceName3;
	}

	public void setReferenceName3(String referenceName3) {
		this.referenceName3 = referenceName3;
	}

	public String getReferenceAddress3() {
		return referenceAddress3;
	}

	public void setReferenceAddress3(String referenceAddress3) {
		this.referenceAddress3 = referenceAddress3;
	}

	public String getReferencePhone3() {
		return referencePhone3;
	}

	public void setReferencePhone3(String referencePhone3) {
		this.referencePhone3 = referencePhone3;
	}

	public String getReferenceMailId3() {
		return referenceMailId3;
	}

	public void setReferenceMailId3(String referenceMailId3) {
		this.referenceMailId3 = referenceMailId3;
	}

	public String getReferenceMobile3() {
		return referenceMobile3;
	}

	public void setReferenceMobile3(String referenceMobile3) {
		this.referenceMobile3 = referenceMobile3;
	}

	public String getReferenceCity3() {
		return referenceCity3;
	}

	public void setReferenceCity3(String referenceCity3) {
		this.referenceCity3 = referenceCity3;
	}

	public String getReferenceZip3() {
		return referenceZip3;
	}

	public void setReferenceZip3(String referenceZip3) {
		this.referenceZip3 = referenceZip3;
	}

	public String getReferenceState3() {
		return referenceState3;
	}

	public void setReferenceState3(String referenceState3) {
		this.referenceState3 = referenceState3;
	}

	public String getReferenceCountry3() {
		return referenceCountry3;
	}

	public void setReferenceCountry3(String referenceCountry3) {
		this.referenceCountry3 = referenceCountry3;
	}

	public String getExtension1() {
		return extension1;
	}

	public void setExtension1(String extension1) {
		this.extension1 = extension1;
	}

	public String getExtension2() {
		return extension2;
	}

	public void setExtension2(String extension2) {
		this.extension2 = extension2;
	}

	public String getExtension3() {
		return extension3;
	}

	public void setExtension3(String extension3) {
		this.extension3 = extension3;
	}

	public String getIsPartiallySubmitted() {
		return isPartiallySubmitted;
	}

	public void setIsPartiallySubmitted(String isPartiallySubmitted) {
		this.isPartiallySubmitted = isPartiallySubmitted;
	}

	public String getPrime() {
		return prime;
	}

	public void setPrime(String prime) {
		this.prime = prime;
	}

	public String getNonprime() {
		return nonprime;
	}

	public void setNonprime(String nonprime) {
		this.nonprime = nonprime;
	}

	public String[] getBussines() {
		return bussines;
	}

	public void setBussines(String[] bussines) {
		this.bussines = bussines;
	}

	public String[] getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String[] businessArea) {
		this.businessArea = businessArea;
	}

	public Integer[] getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(Integer[] serviceArea) {
		this.serviceArea = serviceArea;
	}

	public String getCompanyInformation() {
		return companyInformation;
	}

	public void setCompanyInformation(String companyInformation) {
		this.companyInformation = companyInformation;
	}

	public String getPreparerTitle() {
		return preparerTitle;
	}

	public void setPreparerTitle(String preparerTitle) {
		this.preparerTitle = preparerTitle;
	}

	public String getPreparerPhone() {
		return preparerPhone;
	}

	public void setPreparerPhone(String preparerPhone) {
		this.preparerPhone = preparerPhone;
	}

	public String getPreparerMobile() {
		return preparerMobile;
	}

	public void setPreparerMobile(String preparerMobile) {
		this.preparerMobile = preparerMobile;
	}

	public String getPreparerFax() {
		return preparerFax;
	}

	public void setPreparerFax(String preparerFax) {
		this.preparerFax = preparerFax;
	}

	public String getPreparerEmail() {
		return preparerEmail;
	}

	public void setPreparerEmail(String preparerEmail) {
		this.preparerEmail = preparerEmail;
	}

	public String getPreparerFirstName() {
		return preparerFirstName;
	}

	public void setPreparerFirstName(String preparerFirstName) {
		this.preparerFirstName = preparerFirstName;
	}

	public String getPreparerLastName() {
		return preparerLastName;
	}

	public void setPreparerLastName(String preparerLastName) {
		this.preparerLastName = preparerLastName;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactDate() {
		return contactDate;
	}

	public void setContactDate(String contactDate) {
		this.contactDate = contactDate;
	}

	public Integer getContactState() {
		return contactState;
	}

	public void setContactState(Integer contactState) {
		this.contactState = contactState;
	}

	public String getContactEvent() {
		return contactEvent;
	}

	public void setContactEvent(String contactEvent) {
		this.contactEvent = contactEvent;
	}

	public String getVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

	public List<StatusMaster> getStatusMasters() {
		return statusMasters;
	}

	public void setStatusMasters(List<StatusMaster> statusMasters) {
		this.statusMasters = statusMasters;
	}

	public List<CertifyingAgency> getCertAgencies1() {
		return certAgencies1;
	}

	public void setCertAgencies1(List<CertifyingAgency> certAgencies1) {
		this.certAgencies1 = certAgencies1;
	}

	public List<CertifyingAgency> getCertAgencies2() {
		return certAgencies2;
	}

	public void setCertAgencies2(List<CertifyingAgency> certAgencies2) {
		this.certAgencies2 = certAgencies2;
	}

	public List<CertifyingAgency> getCertAgencies3() {
		return certAgencies3;
	}

	public void setCertAgencies3(List<CertifyingAgency> certAgencies3) {
		this.certAgencies3 = certAgencies3;
	}

	public List<CustomerCertificateType> getCertificateTypes1() {
		return certificateTypes1;
	}

	public void setCertificateTypes1(
			List<CustomerCertificateType> certificateTypes1) {
		this.certificateTypes1 = certificateTypes1;
	}

	public List<CustomerCertificateType> getCertificateTypes2() {
		return certificateTypes2;
	}

	public void setCertificateTypes2(
			List<CustomerCertificateType> certificateTypes2) {
		this.certificateTypes2 = certificateTypes2;
	}

	public List<CustomerCertificateType> getCertificateTypes3() {
		return certificateTypes3;
	}

	public void setCertificateTypes3(
			List<CustomerCertificateType> certificateTypes3) {
		this.certificateTypes3 = certificateTypes3;
	}

	public String getReferenceProvince1() {
		return referenceProvince1;
	}

	public void setReferenceProvince1(String referenceProvince1) {
		this.referenceProvince1 = referenceProvince1;
	}

	public String getReferenceProvince2() {
		return referenceProvince2;
	}

	public void setReferenceProvince2(String referenceProvince2) {
		this.referenceProvince2 = referenceProvince2;
	}

	public String getReferenceProvince3() {
		return referenceProvince3;
	}

	public void setReferenceProvince3(String referenceProvince3) {
		this.referenceProvince3 = referenceProvince3;
	}

	public Integer getCustomerDivision() {
		return customerDivision;
	}

	public void setCustomerDivision(Integer customerDivision) {
		this.customerDivision = customerDivision;
	}

	public String getCheckList() {
		return checkList;
	}

	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}

	public List<VendorStatusPojo> getVendorStatusList() {
		return vendorStatusList;
	}

	public void setVendorStatusList(List<VendorStatusPojo> vendorStatusList) {
		this.vendorStatusList = vendorStatusList;
	}

	/**
	 * @return the termsCondition
	 */
	public String getTermsCondition() {
		return termsCondition;
	}

	/**
	 * @param termsCondition the termsCondition to set
	 */
	public void setTermsCondition(String termsCondition) {
		this.termsCondition = termsCondition;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the vendorKeyword
	 */
	public String[] getVendorKeyword() {
		return vendorKeyword;
	}

	/**
	 * @param vendorKeyword the vendorKeyword to set
	 */
	public void setVendorKeyword(String[] vendorKeyword) {
		this.vendorKeyword = vendorKeyword;
	}

	/**
	 * @return the picsCertificate
	 */
	public FormFile getPicsCertificate() {
		return picsCertificate;
	}

	/**
	 * @param picsCertificate the picsCertificate to set
	 */
	public void setPicsCertificate(FormFile picsCertificate) {
		this.picsCertificate = picsCertificate;
	}

	/**
	 * @return the picsExpireDate
	 */
	public String getPicsExpireDate() {
		return picsExpireDate;
	}

	/**
	 * @param picsExpireDate the picsExpireDate to set
	 */
	public void setPicsExpireDate(String picsExpireDate) {
		this.picsExpireDate = picsExpireDate;
	}

	/**
	 * @return the isPics
	 */
	public Integer getIsPics() {
		return isPics;
	}

	/**
	 * @param isPics the isPics to set
	 */
	public void setIsPics(Integer isPics) {
		this.isPics = isPics;
	}

	/**
	 * @return the bpSegmentId
	 */
	public Integer getBpSegmentId() {
		return bpSegmentId;
	}

	/**
	 * @param bpSegmentId the bpSegmentId to set
	 */
	public void setBpSegmentId(Integer bpSegmentId) {
		this.bpSegmentId = bpSegmentId;
	}

	/**
	 * @return the meetingNotesList
	 */
	public List<MeetingNotesDto> getMeetingNotesList() {
		return meetingNotesList;
	}

	/**
	 * @param meetingNotesList the meetingNotesList to set
	 */
	public void setMeetingNotesList(List<MeetingNotesDto> meetingNotesList) {
		this.meetingNotesList = meetingNotesList;
	}

	/**
	 * @return the vendorNotesId
	 */
	public Integer getVendorNotesId() {
		return vendorNotesId;
	}

	/**
	 * @param vendorNotesId the vendorNotesId to set
	 */
	public void setVendorNotesId(Integer vendorNotesId) {
		this.vendorNotesId = vendorNotesId;
	}

	/**
	 * @return the emailHistoryList
	 */
	public List<EmailHistoryDto> getEmailHistoryList() {
		return emailHistoryList;
	}

	/**
	 * @param emailHistoryList the emailHistoryList to set
	 */
	public void setEmailHistoryList(List<EmailHistoryDto> emailHistoryList) {
		this.emailHistoryList = emailHistoryList;
	}

	public String getVendorRfiRfpNotes() {
		return vendorRfiRfpNotes;
	}

	public void setVendorRfiRfpNotes(String vendorRfiRfpNotes) {
		this.vendorRfiRfpNotes = vendorRfiRfpNotes;
	}

	public FormFile getNetworldCert() {
		return networldCert;
	}

	public void setNetworldCert(FormFile networldCert) {
		this.networldCert = networldCert;
	}

	public Boolean getHasNetWorldCert() {
		return hasNetWorldCert;
	}

	public void setHasNetWorldCert(Boolean hasNetWorldCert) {
		this.hasNetWorldCert = hasNetWorldCert;
	}

	public String getWorkPlace() {
		return workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	public String getTypeBpSupplier() {
		return typeBpSupplier;
	}

	public void setTypeBpSupplier(String typeBpSupplier) {
		this.typeBpSupplier = typeBpSupplier;
	}

	public String getSpecificwork() {
		return specificwork;
	}

	public void setSpecificwork(String specificwork) {
		this.specificwork = specificwork;
	}

	public String getWorkstartdate() {
		return workstartdate;
	}

	public void setWorkstartdate(String workstartdate) {
		this.workstartdate = workstartdate;
	}

	public String getContractValue() {
		return contractValue;
	}

	public void setContractValue(String contractValue) {
		this.contractValue = contractValue;
	}

	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	public Byte getIsNetworldCertified() {
		return isNetworldCertified;
	}

	public void setIsNetworldCertified(Byte isNetworldCertified) {
		this.isNetworldCertified = isNetworldCertified;
	}

	public List<RFIRPFNotesDto> getRfiNotesList() {
		return rfiNotesList;
	}

	public void setRfiNotesList(List<RFIRPFNotesDto> rfiNotesList) {
		this.rfiNotesList = rfiNotesList;
	}

	public Integer getVendorrfiNotesId() {
		return vendorrfiNotesId;
	}

	public void setVendorrfiNotesId(Integer vendorrfiNotesId) {
		this.vendorrfiNotesId = vendorrfiNotesId;
	}

	public String getVendorrfiNotes() {
		return vendorrfiNotes;
	}

	public void setVendorrfiNotes(String vendorrfiNotes) {
		this.vendorrfiNotes = vendorrfiNotes;
	}
}