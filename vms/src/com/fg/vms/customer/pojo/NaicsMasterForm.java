/*
 * NaicsMasterForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.dto.NaicsSubCategoryDto;
import com.fg.vms.customer.model.NaicsMaster;

/**
 * Represents the naics master details (eg. unique naics code, naics
 * description, isic code and isic description, list of categories, list of
 * sub-categories) for interact with UI.
 * 
 * @author vinoth
 * 
 */
public class NaicsMasterForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    private int naicsCategoryId;

    private int subCategory;

    private String naicsCode;

    private String naicsDescription;

    private String isicCode;

    private String isicDescription;

    private Byte isActive = 1;

    private String categoryName;

    private String subCategoryName;

    private String naicsSubCategoryID;

    private String naicsCategorySearchId;

    private List<NaicsCategoryDto> categories;

    private List<NaicsSubCategoryDto> subCategories;

    private List<NaicsMaster> masters;
    
    private String hiddenNaicsCode;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the naicsCategoryId
     */
    public int getNaicsCategoryId() {
	return naicsCategoryId;
    }

    /**
     * @param naicsCategoryId
     *            the naicsCategoryId to set
     */
    public void setNaicsCategoryId(int naicsCategoryId) {
	this.naicsCategoryId = naicsCategoryId;
    }

    /**
     * @return the subCategory
     */
    public int getSubCategory() {
	return subCategory;
    }

    /**
     * @param subCategory
     *            the subCategory to set
     */
    public void setSubCategory(int subCategory) {
	this.subCategory = subCategory;
    }

    /**
     * @return the naicsCode
     */
    public String getNaicsCode() {
	return naicsCode;
    }

    /**
     * @param naicsCode
     *            the naicsCode to set
     */
    public void setNaicsCode(String naicsCode) {
	this.naicsCode = naicsCode;
    }

    /**
     * @return the naicsDescription
     */
    public String getNaicsDescription() {
	return naicsDescription;
    }

    /**
     * @param naicsDescription
     *            the naicsDescription to set
     */
    public void setNaicsDescription(String naicsDescription) {
	this.naicsDescription = naicsDescription;
    }

    /**
     * @return the isicCode
     */
    public String getIsicCode() {
	return isicCode;
    }

    /**
     * @param isicCode
     *            the isicCode to set
     */
    public void setIsicCode(String isicCode) {
	this.isicCode = isicCode;
    }

    /**
     * @return the isicDescription
     */
    public String getIsicDescription() {
	return isicDescription;
    }

    /**
     * @param isicDescription
     *            the isicDescription to set
     */
    public void setIsicDescription(String isicDescription) {
	this.isicDescription = isicDescription;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
	return categoryName;
    }

    /**
     * @param categoryName
     *            the categoryName to set
     */
    public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
    }

    /**
     * @return the subCategoryName
     */
    public String getSubCategoryName() {
	return subCategoryName;
    }

    /**
     * @param subCategoryName
     *            the subCategoryName to set
     */
    public void setSubCategoryName(String subCategoryName) {
	this.subCategoryName = subCategoryName;
    }

    /**
     * @return the naicsSubCategoryID
     */
    public String getNaicsSubCategoryID() {
	return naicsSubCategoryID;
    }

    /**
     * @param naicsSubCategoryID
     *            the naicsSubCategoryID to set
     */
    public void setNaicsSubCategoryID(String naicsSubCategoryID) {
	this.naicsSubCategoryID = naicsSubCategoryID;
    }

    /**
     * @return the naicsCategorySearchId
     */
    public String getNaicsCategorySearchId() {
	return naicsCategorySearchId;
    }

    /**
     * @param naicsCategorySearchId
     *            the naicsCategorySearchId to set
     */
    public void setNaicsCategorySearchId(String naicsCategorySearchId) {
	this.naicsCategorySearchId = naicsCategorySearchId;
    }

    /**
     * @return the categories
     */
    public List<NaicsCategoryDto> getCategories() {
	return categories;
    }

    /**
     * @param categories
     *            the categories to set
     */
    public void setCategories(List<NaicsCategoryDto> categories) {
	this.categories = categories;
    }

    /**
     * @return the subCategories
     */
    public List<NaicsSubCategoryDto> getSubCategories() {
	return subCategories;
    }

    /**
     * @param subCategories
     *            the subCategories to set
     */
    public void setSubCategories(List<NaicsSubCategoryDto> subCategories) {
	this.subCategories = subCategories;
    }

    /**
     * @return the hiddenNaicsCode
     */
    public String getHiddenNaicsCode() {
        return hiddenNaicsCode;
    }

    /**
     * @param hiddenNaicsCode the hiddenNaicsCode to set
     */
    public void setHiddenNaicsCode(String hiddenNaicsCode) {
        this.hiddenNaicsCode = hiddenNaicsCode;
    }

    /**
     * @return the masters
     */
    public List<NaicsMaster> getMasters() {
	return masters;
    }

    /**
     * @param masters
     *            the masters to set
     */
    public void setMasters(List<NaicsMaster> masters) {
	this.masters = masters;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.naicsCode = "";
	this.naicsDescription = "";
	this.isicCode = "";
	this.isicDescription = "";
	this.naicsCategoryId = 0;
	this.subCategory = 0;
	this.naicsSubCategoryID = "";
	this.categoryName = "";
	this.categories = null;
    }

}
