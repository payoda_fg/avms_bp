/* 
 * CertificateForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.CustomerCertificateType;

/**
 * Represents the Certificate Details (eg.certificateName, certificateShortName,
 * Diverse Quality) for interact with UI
 * 
 * @author vinoth
 * 
 */
public class CertificateForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String certificateName;

	private String certificateShortName;

	private String diverseQuality = null;

	private String isActive;

	private String[] certificateAgency;

	private String[] certificateType;

	private List<CustomerCertificateType> certificateTypes;

	private String isEthinicity;

	private String certificateUpload;

	private String isRefinerySelectable;

	private String[] customerDivision;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the certificateName
	 */
	public String getCertificateName() {
		return certificateName;
	}

	/**
	 * @param certificateName
	 *            the certificateName to set
	 */
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	/**
	 * @return the certificateShortName
	 */
	public String getCertificateShortName() {
		return certificateShortName;
	}

	/**
	 * @param certificateShortName
	 *            the certificateShortName to set
	 */
	public void setCertificateShortName(String certificateShortName) {
		this.certificateShortName = certificateShortName;
	}

	/**
	 * @return the diverseQuality
	 */
	public String getDiverseQuality() {
		return diverseQuality;
	}

	/**
	 * @param diverseQuality
	 *            the diverseQuality to set
	 */
	public void setDiverseQuality(String diverseQuality) {
		this.diverseQuality = diverseQuality;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the certificateTypes
	 */
	public List<CustomerCertificateType> getCertificateTypes() {
		return certificateTypes;
	}

	/**
	 * @param certificateTypes
	 *            the certificateTypes to set
	 */
	public void setCertificateTypes(
			List<CustomerCertificateType> certificateTypes) {
		this.certificateTypes = certificateTypes;
	}

	/**
	 * @return the certificateAgency
	 */
	public String[] getCertificateAgency() {
		return certificateAgency;
	}

	/**
	 * @param certificateAgency
	 *            the certificateAgency to set
	 */
	public void setCertificateAgency(String[] certificateAgency) {
		this.certificateAgency = certificateAgency;
	}

	/**
	 * @return the certificateType
	 */
	public String[] getCertificateType() {
		return certificateType;
	}

	/**
	 * @param certificateType
	 *            the certificateType to set
	 */
	public void setCertificateType(String[] certificateType) {
		this.certificateType = certificateType;
	}

	public String getIsEthinicity() {
		return isEthinicity;
	}

	public void setIsEthinicity(String isEthinicity) {
		this.isEthinicity = isEthinicity;
	}

	public String getCertificateUpload() {
		return certificateUpload;
	}

	public void setCertificateUpload(String certificateUpload) {
		this.certificateUpload = certificateUpload;
	}

	/**
	 * @return the isRefinerySelectable
	 */
	public String getIsRefinerySelectable() {
		return isRefinerySelectable;
	}

	/**
	 * @param isRefinerySelectable
	 *            the isRefinerySelectable to set
	 */
	public void setIsRefinerySelectable(String isRefinerySelectable) {
		this.isRefinerySelectable = isRefinerySelectable;
	}

	/**
	 * @return the customerDivision
	 */
	public String[] getCustomerDivision() {
		return customerDivision;
	}

	/**
	 * @param customerDivision
	 *            the customerDivision to set
	 */
	public void setCustomerDivision(String[] customerDivision) {
		this.customerDivision = customerDivision;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.certificateName = "";
		this.certificateShortName = "";
		this.diverseQuality = null;
		this.isActive = "1";
		this.certificateTypes = null;
		this.certificateAgency = null;
		this.certificateType = null;
		this.isEthinicity = null;
		this.isRefinerySelectable = null;
		this.customerDivision = null;
	}

}
