/**
 * CountryStateForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.State;

/**
 * @author asarudeen
 * 
 */

public class CountryStateForm extends ValidatorForm
{
	private static final long serialVersionUID = 1L;

	private String countryName;
	private Integer countryId;
	private String stateName;
	private Integer stateId;
	private List<Country> countries;
	private List<State> states;
	private String[] countriesGroup;
	private String provinceState;
	
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the countryId
	 */
	public Integer getCountryId() {
		return countryId;
	}
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}
	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	/**
	 * @return the countries
	 */
	public List<Country> getCountries() {
		return countries;
	}
	/**
	 * @param countries the countries to set
	 */
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}
	/**
	 * @return the states
	 */
	public List<State> getStates() {
		return states;
	}
	/**
	 * @param states the states to set
	 */
	public void setStates(List<State> states) {
		this.states = states;
	}
	/**
	 * @return the countriesGroup
	 */
	public String[] getCountriesGroup() {
		return countriesGroup;
	}
	/**
	 * @param countriesGroup the countriesGroup to set
	 */
	public void setCountriesGroup(String[] countriesGroup) {
		this.countriesGroup = countriesGroup;
	}
	/**
	 * @return the provinceState
	 */
	public String getProvinceState() {
		return provinceState;
	}
	/**
	 * @param provinceState the provinceState to set
	 */
	public void setProvinceState(String provinceState) {
		this.provinceState = provinceState;
	}	
}
