package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.List;

public class VendorBussinessBioDetails implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<VendorDetailsBean> bussinessSafetylist;
	private List<VendorDetailsBean> bussinessList;
	public List<VendorDetailsBean> getBussinessSafetylist() {
		return bussinessSafetylist;
	}
	public void setBussinessSafetylist(List<VendorDetailsBean> bussinessSafetylist) {
		this.bussinessSafetylist = bussinessSafetylist;
	}
	public List<VendorDetailsBean> getBussinessList() {
		return bussinessList;
	}
	public void setBussinessList(List<VendorDetailsBean> bussinessList) {
		this.bussinessList = bussinessList;
	}
	
	

}
