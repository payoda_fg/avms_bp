/**
 * GeographicForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerBusinessGroup;
import com.fg.vms.customer.model.State;

/**
 * @author vinoth
 * 
 */
public class GeographicForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String region;

	private Integer regionId;

	private String state;

	private Integer stateId;

	private String[] states;

	private String[] regions;

	private List<State> stateList;

	private List<CustomerBusinessGroup> groups;

	private List<CustomerBusinessArea> areas;
	
	private String isActive;

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the regionId
	 */
	public Integer getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId
	 *            the regionId to set
	 */
	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId
	 *            the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the states
	 */
	public String[] getStates() {
		return states;
	}

	/**
	 * @param states
	 *            the states to set
	 */
	public void setStates(String[] states) {
		this.states = states;
	}

	/**
	 * @return the regions
	 */
	public String[] getRegions() {
		return regions;
	}

	/**
	 * @param regions
	 *            the regions to set
	 */
	public void setRegions(String[] regions) {
		this.regions = regions;
	}

	/**
	 * @return the stateList
	 */
	public List<State> getStateList() {
		return stateList;
	}

	/**
	 * @param stateList
	 *            the stateList to set
	 */
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}

	/**
	 * @return the groups
	 */
	public List<CustomerBusinessGroup> getGroups() {
		return groups;
	}

	/**
	 * @param groups
	 *            the groups to set
	 */
	public void setGroups(List<CustomerBusinessGroup> groups) {
		this.groups = groups;
	}

	/**
	 * @return the areas
	 */
	public List<CustomerBusinessArea> getAreas() {
		return areas;
	}

	/**
	 * @param areas
	 *            the areas to set
	 */
	public void setAreas(List<CustomerBusinessArea> areas) {
		this.areas = areas;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.region = "";
		this.regionId = null;
		this.regions = null;
		this.state = null;
		this.stateId = null;
		this.states = null;
		this.groups = null;
		this.areas = null;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

}
