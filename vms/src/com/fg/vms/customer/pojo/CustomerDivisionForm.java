package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * 
 * @author SHEFEEK.A
 * 
 */
public class CustomerDivisionForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String divisionName;
	private String divisionShortName;
	private String isActive;
	private String checkList;
	private String certificationAgencies;
	private Byte isDivision;
	private String registrationRequest;
	private String registrationQuestion;
	private String isGlobal;


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the divisionName
	 */
	public String getDivisionName() {
		return divisionName;
	}
	/**
	 * @param divisionName the divisionName to set
	 */
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	/**
	 * @return the divisionShortName
	 */
	public String getDivisionShortName() {
		return divisionShortName;
	}
	/**
	 * @param divisionShortName the divisionShortName to set
	 */
	public void setDivisionShortName(String divisionShortName) {
		this.divisionShortName = divisionShortName;
	}
	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the checkList
	 */
	public String getCheckList() {
		return checkList;
	}
	/**
	 * @param checkList the checkList to set
	 */
	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}
	/**
	 * @return the certificationAgencies
	 */
	public String getCertificationAgencies() {
		return certificationAgencies;
	}
	/**
	 * @param certificationAgencies the certificationAgencies to set
	 */
	public void setCertificationAgencies(String certificationAgencies) {
		this.certificationAgencies = certificationAgencies;
	}



	/**
	 * @return the isDivision
	 */
	public Byte getIsDivision() {
		return isDivision;
	}



	/**
	 * @param isDivision the isDivision to set
	 */
	public void setIsDivision(Byte isDivision) {
		this.isDivision = isDivision;
	}
	/**
	 * @return the registrationRequest
	 */
	public String getRegistrationRequest() {
		return registrationRequest;
	}
	/**
	 * @param registrationRequest the registrationRequest to set
	 */
	public void setRegistrationRequest(String registrationRequest) {
		this.registrationRequest = registrationRequest;
	}



	/**
	 * @return the registrationQuestion
	 */
	public String getRegistrationQuestion() {
		return registrationQuestion;
	}



	/**
	 * @param registrationQuestion the registrationQuestion to set
	 */
	public void setRegistrationQuestion(String registrationQuestion) {
		this.registrationQuestion = registrationQuestion;
	}
	/**
	 * @return the isGlobal
	 */
	public String getIsGlobal() {
		return isGlobal;
	}
	/**
	 * @param isGlobal the isGlobal to set
	 */
	public void setIsGlobal(String isGlobal) {
		this.isGlobal = isGlobal;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.divisionName = "";
		this.divisionShortName = "";
		this.isActive = "";
		this.id = null;
		this.checkList = "";
		this.certificationAgencies = "";
		this.registrationRequest="";
		this.registrationQuestion="";
		this.isGlobal = "";
		super.reset(mapping, request);
	}
}
