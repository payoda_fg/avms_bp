/*
 * NaicsCategoryForm.java 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import fr.improve.struts.taglib.layout.datagrid.Datagrid;

/**
 * Represents the Naics category details (eg. categoryDescription, web service
 * URL, web service parameters)
 * 
 * @author vinoth
 * 
 */
public class NaicsCategoryForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private Datagrid naicsCategoryMaster;

    private String naicsCategoryDesc;

    private Integer id;

    private Byte isActive;

    private Byte isWebService;

    private String webServiceURL;

    private String webServiceParameters;

    /**
     * @return the naicsCategoryMaster
     */
    public Datagrid getNaicsCategoryMaster() {
	return naicsCategoryMaster;
    }

    /**
     * @param naicsCategoryMaster
     *            the naicsCategoryMaster to set
     */
    public void setNaicsCategoryMaster(Datagrid naicsCategoryMaster) {
	this.naicsCategoryMaster = naicsCategoryMaster;
    }

    /**
     * @return the naicsCategoryDesc
     */
    public String getNaicsCategoryDesc() {
	return naicsCategoryDesc;
    }

    /**
     * @param naicsCategoryDesc
     *            the naicsCategoryDesc to set
     */
    public void setNaicsCategoryDesc(String naicsCategoryDesc) {
	this.naicsCategoryDesc = naicsCategoryDesc;
    }

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the isWebService
     */
    public Byte getIsWebService() {
	return isWebService;
    }

    /**
     * @param isWebService
     *            the isWebService to set
     */
    public void setIsWebService(Byte isWebService) {
	this.isWebService = isWebService;
    }

    /**
     * @return the webServiceURL
     */
    public String getWebServiceURL() {
	return webServiceURL;
    }

    /**
     * @param webServiceURL
     *            the webServiceURL to set
     */
    public void setWebServiceURL(String webServiceURL) {
	this.webServiceURL = webServiceURL;
    }

    /**
     * @return the webServiceParameters
     */
    public String getWebServiceParameters() {
	return webServiceParameters;
    }

    /**
     * @param webServiceParameters
     *            the webServiceParameters to set
     */
    public void setWebServiceParameters(String webServiceParameters) {
	this.webServiceParameters = webServiceParameters;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.naicsCategoryDesc = "";
	this.webServiceParameters = "";
	this.webServiceURL = "";
	this.isActive = (byte) 1;
	this.isWebService = (byte) 1;
    }
}