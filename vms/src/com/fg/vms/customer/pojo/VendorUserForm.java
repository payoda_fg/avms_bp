/*
 * VendorUserForm.java
 */

package com.fg.vms.customer.pojo;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorRoles;

/**
 * Represents vendor user details (eg. vendorName, phoneNumber, designation etc)
 * to interact with UI.
 * 
 * @author Vinoth
 */
public class VendorUserForm extends ValidatorForm {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The vid. */
    private Integer vid;

    /** The id. */
    private Integer id;

    /** The role id. */
    private Integer roleId;

    /** The sec id. */
    private Integer secId;

    /** The vendor id. */
    private VendorMaster vendorId;

    /** The vendor user role id. */
    private VendorRoles vendorUserRoleId;

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The designation. */
    private String designation;

    /** The phonenumber. */
    private String phonenumber;

    /** The mobile. */
    private String mobile;

    /** The fax. */
    private String fax;

    /** The email id. */
    private String emailId;

    /** The hidden email id. */
    private String hiddenEmailId;

    /** The is allowed login. */
    private Byte isAllowedLogin;

    /** The login display name. */
    private String loginDisplayName;

    /** The login id. */
   // private String loginId;

    /** The hidden login id. */
    private String hiddenLoginId;

    /** The password. */
    private String password;

    /** The confirm password. */
    private String confirmPassword;

    /** The secret question id. */
    private SecretQuestion secretQuestionId;

    /** The secret question answer. */
    private String secretQuestionAnswer;

    /** The is primary contact. */
    private Byte isPrimaryContact;

    /** The is active. */
    private Byte isActive;

    /** The created by. */
    private Integer createdBy;

    /** The created on. */
    private Date createdOn;

    /** The modified by. */
    private Integer modifiedBy;

    /** The modified on. */
    private Date modifiedOn;
    
    private String gender;

    /**
     * Gets the vid.
     * 
     * @return the vid
     */
    public Integer getVid() {
	return vid;
    }

    /**
     * Sets the vid.
     * 
     * @param vid
     *            the vid to set
     */
    public void setVid(Integer vid) {
	this.vid = vid;
    }

    /**
     * Gets the role id.
     * 
     * @return the roleId
     */
    public Integer getRoleId() {
	return roleId;
    }

    /**
     * Sets the role id.
     * 
     * @param roleId
     *            the new role id
     */
    public void setRoleId(Integer roleId) {
	this.roleId = roleId;
    }

    /**
     * Gets the sec id.
     * 
     * @return the secId
     */
    public Integer getSecId() {
	return secId;
    }

    /**
     * Sets the sec id.
     * 
     * @param secId
     *            the new sec id
     */
    public void setSecId(Integer secId) {
	this.secId = secId;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the vendor id.
     * 
     * @return the vendorId
     */
    public VendorMaster getVendorId() {
	return vendorId;
    }

    /**
     * Sets the vendor id.
     * 
     * @param vendorId
     *            the vendorId to set
     */
    public void setVendorId(VendorMaster vendorId) {
	this.vendorId = vendorId;
    }

    /**
     * Gets the vendor user role id.
     * 
     * @return the vendorUserRoleId
     */
    public VendorRoles getVendorUserRoleId() {
	return vendorUserRoleId;
    }

    /**
     * Sets the vendor user role id.
     * 
     * @param vendorUserRoleId
     *            the new vendor user role id
     */
    public void setVendorUserRoleId(VendorRoles vendorUserRoleId) {
	this.vendorUserRoleId = vendorUserRoleId;
    }

    /**
     * Gets the first name.
     * 
     * @return the firstName
     */
    public String getFirstName() {
	return firstName;
    }

    /**
     * Sets the first name.
     * 
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    /**
     * Gets the last name.
     * 
     * @return the lastname
     */
    public String getLastName() {
	return lastName;
    }

    /**
     * Sets the last name.
     * 
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    /**
     * Gets the designation.
     * 
     * @return the Designation
     */
    public String getDesignation() {
	return designation;
    }

    /**
     * Sets the designation.
     * 
     * @param designation
     *            the designation to set
     */
    public void setDesignation(String designation) {
	this.designation = designation;
    }

    /**
     * Gets the phonenumber.
     * 
     * @return the phonenumber
     */
    public String getPhonenumber() {
	return phonenumber;
    }

    /**
     * Sets the phonenumber.
     * 
     * @param phonenumber
     *            the new phonenumber
     */
    public void setPhonenumber(String phonenumber) {
	this.phonenumber = phonenumber;
    }

    /**
     * Gets the mobile.
     * 
     * @return the mobile
     */
    public String getMobile() {
	return mobile;
    }

    /**
     * Sets the mobile.
     * 
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    /**
     * Gets the fax.
     * 
     * @return the fax
     */
    public String getFax() {
	return fax;
    }

    /**
     * Sets the fax.
     * 
     * @param fax
     *            the fax to set
     */
    public void setFax(String fax) {
	this.fax = fax;
    }

    /**
     * Gets the email id.
     * 
     * @return the emailId
     */
    public String getEmailId() {
	return emailId;
    }

    /**
     * Sets the email id.
     * 
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(String emailId) {
	this.emailId = emailId;
    }

    /**
     * Gets the checks if is allowed login.
     * 
     * @return the isAllowedLogin
     */
    public Byte getIsAllowedLogin() {
	return isAllowedLogin;
    }

    /**
     * Sets the checks if is allowed login.
     * 
     * @param isAllowedLogin
     *            the isAllowedLogin to set
     */
    public void setIsAllowedLogin(Byte isAllowedLogin) {
	this.isAllowedLogin = isAllowedLogin;
    }

    /**
     * Gets the login display name.
     * 
     * @return the loginDisplayName
     */
    public String getLoginDisplayName() {
	return loginDisplayName;
    }

    /**
     * Sets the login display name.
     * 
     * @param loginDisplayName
     *            the loginDisplayName to set
     */
    public void setLoginDisplayName(String loginDisplayName) {
	this.loginDisplayName = loginDisplayName;
    }


    /**
     * Gets the password.
     * 
     * @return the password
     */
    public String getPassword() {
	return password;
    }

    /**
     * Sets the password.
     * 
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * Gets the confirm password.
     * 
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
	return confirmPassword;
    }

    /**
     * Sets the confirm password.
     * 
     * @param confirmPassword
     *            the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
	this.confirmPassword = confirmPassword;
    }

    /**
     * Gets the secret question id.
     * 
     * @return the secretQuestionId
     */
    public SecretQuestion getSecretQuestionId() {
	return secretQuestionId;
    }

    /**
     * Sets the secret question id.
     * 
     * @param secretQuestionId
     *            the secretQuestionId to set
     */
    public void setSecretQuestionId(SecretQuestion secretQuestionId) {
	this.secretQuestionId = secretQuestionId;
    }

    /**
     * Gets the secret question answer.
     * 
     * @return the secretQuestionAnswer
     */
    public String getSecretQuestionAnswer() {
	return secretQuestionAnswer;
    }

    /**
     * Sets the secret question answer.
     * 
     * @param secretQuestionAnswer
     *            the secretQuestionAnswer to set
     */
    public void setSecretQuestionAnswer(String secretQuestionAnswer) {
	this.secretQuestionAnswer = secretQuestionAnswer;
    }

    /**
     * Gets the checks if is primary contact.
     * 
     * @return the isPrimaryContact
     */
    public Byte getIsPrimaryContact() {
	return isPrimaryContact;
    }

    /**
     * Sets the checks if is primary contact.
     * 
     * @param isPrimaryContact
     *            the isPrimaryContact to set
     */
    public void setIsPrimaryContact(Byte isPrimaryContact) {
	this.isPrimaryContact = isPrimaryContact;
    }

    /**
     * Gets the checks if is active.
     * 
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * Sets the checks if is active.
     * 
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * Gets the created by.
     * 
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * Sets the created by.
     * 
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * Gets the created on.
     * 
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * Sets the created on.
     * 
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * Gets the modified by.
     * 
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * Sets the modified by.
     * 
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the modified on.
     * 
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * Sets the modified on.
     * 
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

    /**
     * @return the hiddenEmailId
     */
    public String getHiddenEmailId() {
	return hiddenEmailId;
    }

    /**
     * @param hiddenEmailId
     *            the hiddenEmailId to set
     */
    public void setHiddenEmailId(String hiddenEmailId) {
	this.hiddenEmailId = hiddenEmailId;
    }

    /**
     * @return the hiddenLoginId
     */
    public String getHiddenLoginId() {
	return hiddenLoginId;
    }

    /**
     * @param hiddenLoginId
     *            the hiddenLoginId to set
     */
    public void setHiddenLoginId(String hiddenLoginId) {
	this.hiddenLoginId = hiddenLoginId;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.firstName = "";
	this.lastName = "";
	this.password = "";
	this.designation = "";
	this.phonenumber = "";
	this.mobile = "";
	this.fax = "";
	this.emailId = "";
	this.confirmPassword = "";
	this.loginDisplayName = "";
	this.roleId = null;
	this.secId = null;
	this.secretQuestionAnswer = "";
	this.gender="";
    }

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
}
