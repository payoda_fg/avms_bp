/**
 * CommodityForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.MarketSector;

/**
 * Represents the Commodity Details (eg.commodityCode, commodityDesc, Is active)
 * for interact with UI
 * 
 * @author vinoth
 * 
 */
public class CommodityForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer commCategoryId;

	private String categoryCode;

	private String categoryDesc;

	private String isActive;
	
	private Integer commodityId;
	
	private Integer sectorId;
	
	private String commodityCode;
	
	private String sectorCode;
	
	private String commodityDescription;
	
	private String sectorDescription;
	
	private List<MarketSector> marketSectors;
	
	private List<CustomerCommodityCategory> marketSubSectors;
	/**
	 * @return the commCategoryId
	 */
	public Integer getCommCategoryId() {
		return commCategoryId;
	}

	/**
	 * @param commCategoryId
	 *            the commCategoryId to set
	 */
	public void setCommCategoryId(Integer commCategoryId) {
		this.commCategoryId = commCategoryId;
	}

	/**
	 * @return the categoryCode
	 */
	public String getCategoryCode() {
		return categoryCode;
	}

	/**
	 * @param categoryCode
	 *            the categoryCode to set
	 */
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * @return the categoryDesc
	 */
	public String getCategoryDesc() {
		return categoryDesc;
	}

	/**
	 * @param categoryDesc
	 *            the categoryDesc to set
	 */
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the commodityId
	 */
	public Integer getCommodityId() {
		return commodityId;
	}

	/**
	 * @param commodityId the commodityId to set
	 */
	public void setCommodityId(Integer commodityId) {
		this.commodityId = commodityId;
	}

	/**
	 * @return the sectorId
	 */
	public Integer getSectorId() {
		return sectorId;
	}

	/**
	 * @param sectorId the sectorId to set
	 */
	public void setSectorId(Integer sectorId) {
		this.sectorId = sectorId;
	}

	/**
	 * @return the sectorCode
	 */
	public String getSectorCode() {
		return sectorCode;
	}

	/**
	 * @param sectorCode the sectorCode to set
	 */
	public void setSectorCode(String sectorCode) {
		this.sectorCode = sectorCode;
	}

	/**
	 * @return the sectorDescription
	 */
	public String getSectorDescription() {
		return sectorDescription;
	}

	/**
	 * @param sectorDescription the sectorDescription to set
	 */
	public void setSectorDescription(String sectorDescription) {
		this.sectorDescription = sectorDescription;
	}

	/**
	 * @return the marketSectors
	 */
	public List<MarketSector> getMarketSectors() {
		return marketSectors;
	}

	/**
	 * @param marketSectors the marketSectors to set
	 */
	public void setMarketSectors(List<MarketSector> marketSectors) {
		this.marketSectors = marketSectors;
	}

	/**
	 * @return the marketSubSectors
	 */
	public List<CustomerCommodityCategory> getMarketSubSectors() {
		return marketSubSectors;
	}

	/**
	 * @param marketSubSectors the marketSubSectors to set
	 */
	public void setMarketSubSectors(List<CustomerCommodityCategory> marketSubSectors) {
		this.marketSubSectors = marketSubSectors;
	}
	
	/**
	 * @return the commodityCode
	 */
	public String getCommodityCode() {
		return commodityCode;
	}

	/**
	 * @param commodityCode the commodityCode to set
	 */
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}

	/**
	 * @return the commodityDescription
	 */
	public String getCommodityDescription() {
		return commodityDescription;
	}

	/**
	 * @param commodityDescription the commodityDescription to set
	 */
	public void setCommodityDescription(String commodityDescription) {
		this.commodityDescription = commodityDescription;
	}

	public void reset(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		this.categoryCode="";
		this.categoryDesc="";
		this.commCategoryId=null;
		this.commodityDescription="";
		this.commodityId=null;
		this.commodityCode="";
		this.isActive="";
		this.marketSectors=null;
		this.marketSubSectors=null;
		this.sectorCode="";
		this.sectorDescription="";
		this.sectorId=null;
	}
}
