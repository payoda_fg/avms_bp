/**
 * 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author vinoth
 * 
 */
public class Tier2ReportingPeriodForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The reportingPeriodId. */
	private Integer reportingPeriodId;

	/** The frequencyPeriod. */
	private String frequencyPeriod;

	/** The fiscalStartDate. */
	private String fiscalStartDate;

	/** The fiscalEndDate. */
	private String fiscalEndDate;

	/**
	 * @return the frequencyPeriod
	 */
	public String getFrequencyPeriod() {
		return frequencyPeriod;
	}

	/**
	 * @return the reportingPeriodId
	 */
	public Integer getReportingPeriodId() {
		return reportingPeriodId;
	}

	/**
	 * @param reportingPeriodId
	 *            the reportingPeriodId to set
	 */
	public void setReportingPeriodId(Integer reportingPeriodId) {
		this.reportingPeriodId = reportingPeriodId;
	}

	/**
	 * @param frequencyPeriod
	 *            the frequencyPeriod to set
	 */
	public void setFrequencyPeriod(String frequencyPeriod) {
		this.frequencyPeriod = frequencyPeriod;
	}

	/**
	 * @return the fiscalStartDate
	 */
	public String getFiscalStartDate() {
		return fiscalStartDate;
	}

	/**
	 * @param fiscalStartDate
	 *            the fiscalStartDate to set
	 */
	public void setFiscalStartDate(String fiscalStartDate) {
		this.fiscalStartDate = fiscalStartDate;
	}

	/**
	 * @return the fiscalEndDate
	 */
	public String getFiscalEndDate() {
		return fiscalEndDate;
	}

	/**
	 * @param fiscalEndDate
	 *            the fiscalEndDate to set
	 */
	public void setFiscalEndDate(String fiscalEndDate) {
		this.fiscalEndDate = fiscalEndDate;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.frequencyPeriod = "";
		this.fiscalStartDate = null;
		this.fiscalEndDate = null;

	}

}
