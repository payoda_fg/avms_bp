/**
 * EmailTemplateForm.java
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

/**
 * The Email Template POJO class.
 * 
 * @author vinoth
 * 
 */
public class EmailTemplateForm extends ValidatorForm {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private Integer templateId;
	private Integer emailTemplateName;
	private String emailTemplateMessage;
	private String isActive;
	private String[] emailtemplateDivision;
	private String emailTemplateParameters;
	private String subject;
	private int templateCode;
	private String emailNewTemplateName;
	private FormFile importImageFile;
	private String imageBaseUrl;

	/**
	 * @return the templateId
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the emailTemplateName
	 */
	public Integer getEmailTemplateName() {
		return emailTemplateName;
	}

	/**
	 * @param emailTemplateName
	 *            the emailTemplateName to set
	 */
	public void setEmailTemplateName(Integer emailTemplateName) {
		this.emailTemplateName = emailTemplateName;
	}

	/**
	 * @return the emailTemplateMessage
	 */
	public String getEmailTemplateMessage() {
		return emailTemplateMessage;
	}

	/**
	 * @param emailTemplateMessage
	 *            the emailTemplateMessage to set
	 */
	public void setEmailTemplateMessage(String emailTemplateMessage) {
		this.emailTemplateMessage = emailTemplateMessage;
	}

	/**
	 * @return the emailtemplateDivision
	 */
	public String[] getEmailtemplateDivision() {
		return emailtemplateDivision;
	}

	/**
	 * @param emailtemplateDivision the emailtemplateDivision to set
	 */
	public void setEmailtemplateDivision(String[] emailtemplateDivision) {
		this.emailtemplateDivision = emailtemplateDivision;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the emailTemplateParameters
	 */
	public String getEmailTemplateParameters() {
		return emailTemplateParameters;
	}

	/**
	 * @param emailTemplateParameters the emailTemplateParameters to set
	 */
	public void setEmailTemplateParameters(String emailTemplateParameters) {
		this.emailTemplateParameters = emailTemplateParameters;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the templateCode
	 */
	public int getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode the templateCode to set
	 */
	public void setTemplateCode(int templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.templateId = null;
		this.emailTemplateName = null;
		this.emailTemplateMessage = "";
		this.emailtemplateDivision = null;
		this.emailTemplateParameters= "";
		this.isActive= "";
		this.subject= "";
		this.emailNewTemplateName="";
	}

	/**
	 * @return the emailNewTemplateName
	 */
	public String getEmailNewTemplateName() {
		return emailNewTemplateName;
	}

	/**
	 * @param emailNewTemplateName the emailNewTemplateName to set
	 */
	public void setEmailNewTemplateName(String emailNewTemplateName) {
		this.emailNewTemplateName = emailNewTemplateName;
	}

	/**
	 * @return the importImageFile
	 */
	public FormFile getImportImageFile() {
		return importImageFile;
	}

	/**
	 * @param importImageFile the importImageFile to set
	 */
	public void setImportImageFile(FormFile importImageFile) {
		this.importImageFile = importImageFile;
	}

	/**
	 * @return the imageBaseUrl
	 */
	public String getImageBaseUrl() {
		return imageBaseUrl;
	}

	/**
	 * @param imageBaseUrl the imageBaseUrl to set
	 */
	public void setImageBaseUrl(String imageBaseUrl) {
		this.imageBaseUrl = imageBaseUrl;
	}
}
