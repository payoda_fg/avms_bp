package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.List;

public class SupplierDiversityBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String requestorName;
	private String business;
	private String requestedOn;
	private String oppurtunityEventName;
	private String sourceEvent;
	private String sourceEventStartDate;
	private Long estimateSpend;
	private String estimateWorkStartDate;
	private String lastResponseDate;
	private StringBuilder commodityCode;
	private String scopOfWork;
	private String geography;
	private String miniSupplierRequirement;
	private String contractInsight;
	private StringBuilder incumbentVendor;
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public String getRequestedOn() {
		return requestedOn;
	}
	public void setRequestedOn(String requestedOn) {
		this.requestedOn = requestedOn;
	}
	public String getOppurtunityEventName() {
		return oppurtunityEventName;
	}
	public void setOppurtunityEventName(String oppurtunityEventName) {
		this.oppurtunityEventName = oppurtunityEventName;
	}
	public String getSourceEvent() {
		return sourceEvent;
	}
	public void setSourceEvent(String sourceEvent) {
		this.sourceEvent = sourceEvent;
	}
	public String getSourceEventStartDate() {
		return sourceEventStartDate;
	}
	public void setSourceEventStartDate(String sourceEventStartDate) {
		this.sourceEventStartDate = sourceEventStartDate;
	}
	public Long getEstimateSpend() {
		return estimateSpend;
	}
	public void setEstimateSpend(Long estimateSpend) {
		this.estimateSpend = estimateSpend;
	}
	public String getEstimateWorkStartDate() {
		return estimateWorkStartDate;
	}
	public void setEstimateWorkStartDate(String estimateWorkStartDate) {
		this.estimateWorkStartDate = estimateWorkStartDate;
	}
	public String getLastResponseDate() {
		return lastResponseDate;
	}
	public void setLastResponseDate(String lastResponseDate) {
		this.lastResponseDate = lastResponseDate;
	}
	public StringBuilder getCommodityCode() {
		return commodityCode;
	}
	public void setCommodityCode(StringBuilder commodityCode) {
		this.commodityCode = commodityCode;
	}
	public String getScopOfWork() {
		return scopOfWork;
	}
	public void setScopOfWork(String scopOfWork) {
		this.scopOfWork = scopOfWork;
	}
	public String getGeography() {
		return geography;
	}
	public void setGeography(String geography) {
		this.geography = geography;
	}
	public String getMiniSupplierRequirement() {
		return miniSupplierRequirement;
	}
	public void setMiniSupplierRequirement(String miniSupplierRequirement) {
		this.miniSupplierRequirement = miniSupplierRequirement;
	}
	public String getContractInsight() {
		return contractInsight;
	}
	public void setContractInsight(String contractInsight) {
		this.contractInsight = contractInsight;
	}
	public StringBuilder getIncumbentVendor() {
		return incumbentVendor;
	}
	public void setIncumbentVendor(StringBuilder incumbentVendor) {
		this.incumbentVendor = incumbentVendor;
	}
	
}
