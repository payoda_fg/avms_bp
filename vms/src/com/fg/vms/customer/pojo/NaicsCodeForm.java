/**
 * 
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.NaicsCode;

/**
 * Represents the naics master details (eg. unique naics code, naics
 * description) for interact with UI.
 * 
 * @author vinoth
 * 
 */
public class NaicsCodeForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer naicsId;

	private String naicsCode;

	private String naicsDescription;

	private Integer naicsParent;

	private Byte isActive = 1;

	private List<NaicsCode> naicsCodes;

	/**
	 * @return the naicsCodes
	 */
	public List<NaicsCode> getNaicsCodes() {
		return naicsCodes;
	}

	/**
	 * @param naicsCodes
	 *            the naicsCodes to set
	 */
	public void setNaicsCodes(List<NaicsCode> naicsCodes) {
		this.naicsCodes = naicsCodes;
	}

	/**
	 * @return the naicsId
	 */
	public Integer getNaicsId() {
		return naicsId;
	}

	/**
	 * @param naicsId
	 *            the naicsId to set
	 */
	public void setNaicsId(Integer naicsId) {
		this.naicsId = naicsId;
	}

	/**
	 * @return the naicsCode
	 */
	public String getNaicsCode() {
		return naicsCode;
	}

	/**
	 * @param naicsCode
	 *            the naicsCode to set
	 */
	public void setNaicsCode(String naicsCode) {
		this.naicsCode = naicsCode;
	}

	/**
	 * @return the naicsDescription
	 */
	public String getNaicsDescription() {
		return naicsDescription;
	}

	/**
	 * @param naicsDescription
	 *            the naicsDescription to set
	 */
	public void setNaicsDescription(String naicsDescription) {
		this.naicsDescription = naicsDescription;
	}

	/**
	 * @return the parentID
	 */
	public Integer getNaicsParent() {
		return naicsParent;
	}

	/**
	 * @param parentID
	 *            the parentID to set
	 */
	public void setNaicsParent(Integer naicsParent) {
		this.naicsParent = naicsParent;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.naicsId = null;
		this.naicsCode = "";
		this.naicsDescription = "";
		this.naicsParent = null;
		/*this.isActive = null;*/
	}

}
