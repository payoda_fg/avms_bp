package com.fg.vms.customer.pojo;

import java.io.Serializable;

public class Tier2ReportIndirectExpensesBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String indirectTier2VendorName;
	private String indirectCertificate;
	private String indirectEthnicity;
	private String indirectSector;
	private Integer indirectExpence;
	public String getIndirectTier2VendorName() {
		return indirectTier2VendorName;
	}
	public void setIndirectTier2VendorName(String indirectTier2VendorName) {
		this.indirectTier2VendorName = indirectTier2VendorName;
	}
	public String getIndirectCertificate() {
		return indirectCertificate;
	}
	public void setIndirectCertificate(String indirectCertificate) {
		this.indirectCertificate = indirectCertificate;
	}
	public String getIndirectEthnicity() {
		return indirectEthnicity;
	}
	public void setIndirectEthnicity(String indirectEthnicity) {
		this.indirectEthnicity = indirectEthnicity;
	}
	public String getIndirectSector() {
		return indirectSector;
	}
	public void setIndirectSector(String indirectSector) {
		this.indirectSector = indirectSector;
	}
	public Integer getIndirectExpence() {
		return indirectExpence;
	}
	public void setIndirectExpence(Integer double1) {
		this.indirectExpence = double1;
	}
	
}
