package com.fg.vms.customer.pojo;

import java.io.Serializable;

public class VendorOwnerDetails implements Serializable  {
	
	    private String ownername;
	    private String title;
	    private String email;
	    private String phone;
	    private String extension;
	    private String mobile;
	    private String gender;
	    private String ethinicity;
	    private String ownershippercentage;
	    private String isactive;
	    private String ownerCount;
	    
		public String getOwnername() {
			return ownername;
		}
		public void setOwnername(String ownername) {
			this.ownername = ownername;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getExtension() {
			return extension;
		}
		public void setExtension(String extension) {
			this.extension = extension;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getEthinicity() {
			return ethinicity;
		}
		public void setEthinicity(String ethinicity) {
			this.ethinicity = ethinicity;
		}
		public String getOwnershippercentage() {
			return ownershippercentage;
		}
		public void setOwnershippercentage(String ownershippercentage) {
			this.ownershippercentage = ownershippercentage;
		}
		public String getIsactive() {
			return isactive;
		}
		public void setIsactive(String isactive) {
			this.isactive = isactive;
		}
		public String getOwnerCount() {
			return ownerCount;
		}
		public void setOwnerCount(String ownerCount) {
			this.ownerCount = ownerCount;
		}

}
