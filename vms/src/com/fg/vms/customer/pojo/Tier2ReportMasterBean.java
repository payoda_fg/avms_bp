package com.fg.vms.customer.pojo;

import java.io.Serializable;
import java.util.List;

public class Tier2ReportMasterBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String vendorName;
	private String reportingName;
	private String totalSales;
	private String totalSalesToCompany;
	private String indirectAllocationPercentage;
	private String allocatedAmmount;
	
	private List<Tier2ReportIndirectExpensesBean> tier2ReportIndirectExpensesBeanList;
	private List<Tier2ReportDirectExpensesBean> tier2ReportDirectExpensesBeanList;
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getReportingName() {
		return reportingName;
	}
	public void setReportingName(String reportingName) {
		this.reportingName = reportingName;
	}
	public String getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}
	public String getTotalSalesToCompany() {
		return totalSalesToCompany;
	}
	public void setTotalSalesToCompany(String totalSalesToCompany) {
		this.totalSalesToCompany = totalSalesToCompany;
	}
	public String getIndirectAllocationPercentage() {
		return indirectAllocationPercentage;
	}
	public void setIndirectAllocationPercentage(String indirectAllocationPercentage) {
		this.indirectAllocationPercentage = indirectAllocationPercentage;
	}
	public String getAllocatedAmmount() {
		return allocatedAmmount;
	}
	public void setAllocatedAmmount(String allocatedAmmount) {
		this.allocatedAmmount = allocatedAmmount;
	}
	public List<Tier2ReportIndirectExpensesBean> getTier2ReportIndirectExpensesBeanList() {
		return tier2ReportIndirectExpensesBeanList;
	}
	public void setTier2ReportIndirectExpensesBeanList(
			List<Tier2ReportIndirectExpensesBean> tier2ReportIndirectExpensesBeanList) {
		this.tier2ReportIndirectExpensesBeanList = tier2ReportIndirectExpensesBeanList;
	}
	public List<Tier2ReportDirectExpensesBean> getTier2ReportDirectExpensesBeanList() {
		return tier2ReportDirectExpensesBeanList;
	}
	public void setTier2ReportDirectExpensesBeanList(
			List<Tier2ReportDirectExpensesBean> tier2ReportDirectExpensesBeanList) {
		this.tier2ReportDirectExpensesBeanList = tier2ReportDirectExpensesBeanList;
	}
}
