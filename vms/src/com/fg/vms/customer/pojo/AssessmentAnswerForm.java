/*
 * AssessmentAnswerForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.dto.AssessmentQuestionsDto;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.VendorMaster;

/**
 * 
 * @author vivek
 * 
 */
public class AssessmentAnswerForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private String[] answerBoolean;

    private String[] answerVarChar;

    private String[] answerChar;

    private String[] answerNumber;

    private String[] answerDate;

    private String templateId;

    private String[] templateQuestionId;

    private String[] answer;

    private Integer vendorId;

    private List<VendorMaster> templatevendors;

    private Byte isreviewed;

    private LabelValueBean[] lvbeans;

    /**
     * @return the answerBoolean
     */
    public String[] getAnswerBoolean() {
	return answerBoolean;
    }

    /**
     * @param answerBoolean
     *            the answerBoolean to set
     */
    public void setAnswerBoolean(String[] answerBoolean) {
	this.answerBoolean = answerBoolean;
    }

    /**
     * @return the answerVarChar
     */
    public String[] getAnswerVarChar() {
	return answerVarChar;
    }

    /**
     * @param answerVarChar
     *            the answerVarChar to set
     */
    public void setAnswerVarChar(String[] answerVarChar) {
	this.answerVarChar = answerVarChar;
    }

    /**
     * @return the answerChar
     */
    public String[] getAnswerChar() {
	return answerChar;
    }

    /**
     * @param answerChar
     *            the answerChar to set
     */
    public void setAnswerChar(String[] answerChar) {
	this.answerChar = answerChar;
    }

    /**
     * @return the answerNumber
     */
    public String[] getAnswerNumber() {
	return answerNumber;
    }

    /**
     * @param answerNumber
     *            the answerNumber to set
     */
    public void setAnswerNumber(String[] answerNumber) {
	this.answerNumber = answerNumber;
    }

    /**
     * @return the answerDate
     */
    public String[] getAnswerDate() {
	return answerDate;
    }

    /**
     * @param answerDate
     *            the answerDate to set
     */
    public void setAnswerDate(String[] answerDate) {
	this.answerDate = answerDate;
    }

    /**
     * @return the templateId
     */
    public String getTemplateId() {
	return templateId;
    }

    /**
     * @param templateId
     *            the templateId to set
     */
    public void setTemplateId(String templateId) {
	this.templateId = templateId;
    }

    /**
     * @return the templateQuestionId
     */
    public String[] getTemplateQuestionId() {
	return templateQuestionId;
    }

    /**
     * @param templateQuestionId
     *            the templateQuestionId to set
     */
    public void setTemplateQuestionId(String[] templateQuestionId) {
	this.templateQuestionId = templateQuestionId;
    }

    /**
     * @return the answer
     */
    public String[] getAnswer() {
	return answer;
    }

    /**
     * @param answer
     *            the answer to set
     */
    public void setAnswer(String[] answer) {
	this.answer = answer;
    }

    /**
     * @return the vendorId
     */
    public Integer getVendorId() {
	return vendorId;
    }

    /**
     * @param vendorId
     *            the vendorId to set
     */
    public void setVendorId(Integer vendorId) {
	this.vendorId = vendorId;
    }

    /**
     * @return the templatevendors
     */
    public List<VendorMaster> getTemplatevendors() {
	return templatevendors;
    }

    /**
     * @param templatevendors
     *            the templatevendors to set
     */
    public void setTemplatevendors(List<VendorMaster> templatevendors) {
	this.templatevendors = templatevendors;
    }

    /**
     * @return the isreviewed
     */
    public Byte getIsreviewed() {
	return isreviewed;
    }

    /**
     * @param isreviewed
     *            the isreviewed to set
     */
    public void setIsreviewed(Byte isreviewed) {
	this.isreviewed = isreviewed;
    }

    /**
     * @return the lvbeans
     */
    public LabelValueBean[] getLvbeans() {
	return lvbeans;
    }

    public void setAssessmentQuestions(
	    Map<Template, List<AssessmentQuestionsDto>> map) {
	List<AssessmentQuestionsDto> dtos = new ArrayList<AssessmentQuestionsDto>();

	Set set = map.entrySet();

	Iterator iterator = set.iterator();
	while (iterator.hasNext()) {
	    Map.Entry me = (Map.Entry) iterator.next();
	    dtos.addAll((Collection<? extends AssessmentQuestionsDto>) me
		    .getValue());

	}

	lvbeans = new LabelValueBean[dtos.size()];
	for (int index = 0; index < dtos.size(); index++) {

	    lvbeans[index] = new LabelValueBean(dtos.get(index)
		    .getTemplateQuestionId().toString(), "");
	}

    }

    public LabelValueBean getLabelValue(int index) {
	return lvbeans[index];
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {

	this.answerBoolean = null;
	this.answerVarChar = null;
	this.answerChar = null;
	this.answerNumber = null;
	this.answerDate = null;
	// this.templateId = null;
	// this.templateQuestionId = null;
	this.answer = null;
	this.vendorId = 0;
	this.templatevendors = null;
	this.isreviewed = 2;
    }
}
