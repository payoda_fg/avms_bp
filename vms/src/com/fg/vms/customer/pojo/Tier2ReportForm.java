/**
 * 
 */
package com.fg.vms.customer.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.Tier2ReportDirectExpenses;
import com.fg.vms.customer.model.Tier2ReportIndirectExpenses;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorMaster;

/**
 * @author pirabu
 * 
 */
public class Tier2ReportForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer tier2ReportMasterId;

	private String tier2vendorName[];

	private String tier2indirectvendorName[];
	
	private String reportingPeriod;

	private String totalUsSalesSelectedQtr;

	private String totalUsSalestoSelectedQtr;

	private String indirectPer;

	private String directAmt[];

	private String certificate[];

	private String ethnicity[];
	
	private String ethnicityDrt[];
	
	private String marketSector[];
	
	private String marketSectorDrt[];

	private String indirectAmt[];

	private String directPurchases[];

	private String startDate;

	private String endDate;

	private String groupName;
	
	private String reportPeriod[];
	
	private Integer reportSpendYears[];

	private ArrayList<String> reportPeriods;

	private List<VendorMaster> tier2Vendors;

	private List<VendorMaster> vendors;

	private List<Tier2ReportMaster> reportMasters;

	private List<Certificate> certificates;

	private List<Tier2ReportDirectExpenses> directExpenses;

	private List<Tier2ReportIndirectExpenses> indirectExpenses;

	private String[] tier2VendorNames;

	private List<Tier2ReportDto> reportDtos;
	
	private List<Tier2ReportDto> indirectReportDtos;

	private List<Ethnicity> ethnicities;
	
	private List<MarketSector> marketSectorList;
	
	private List<CustomerCommodityCategory> marketSubSectorList;

	private Integer spendYear;

	private String reportStartDate;

	private String reportEndDate;

	private String certificateDrt[];

	private Integer reportType;

	private List<String> spendYears;

	/** The vendor name. */
	private String vendorName;

	/** The naic code. */
	private String naicCode;

	/** The naic desc. */
	private String naicsDesc;

	/** The region. */
	private String region;

	/** The city. */
	private String city;

	/** The province. */
	private String province;

	/** The diverse. */
	private String diverse;

	/** The country. */
	private String country;

	/** The state. */
	private String state;

	/** The countries. */
	private List<Country> countries;

	private Integer dashboardId;

	private String diversityDashboard;

	private String ethnicityDashboard;

	private String supplierDashboard;

	private String agencyDashboard;

	private String totalSpend;

	private String directSpend;

	private String indirectSpend;

	private String diversityStatus;

	private String ethnicityAnalysis;

	private String spendDashboard;

	private String comments;

	private String vendorStatus;

	private Integer customerDivisionId;
	
	private String bpVendorsCountDashboard;
	
	private Integer tier2ReportPreviousQuarterId;
	
	private String CurrentReportingPeriod;
	
	private FormFile importExcelFile;
	
	/** The previous quarter details status. */
	private Integer previousQuarterStatus;
	
	private List<StatusMaster> vendorStatusMaster;
	
	private String[] vendorStatusList;
	
	private Integer isRenewed;
	
	private String[] indirectCommoditiesList;
	
	private String[] certificateTypeList;

	public String getVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the dashboardId
	 */
	public Integer getDashboardId() {
		return dashboardId;
	}

	/**
	 * @param dashboardId
	 *            the dashboardId to set
	 */
	public void setDashboardId(Integer dashboardId) {
		this.dashboardId = dashboardId;
	}

	/**
	 * @return the diversityDashboard
	 */
	public String getDiversityDashboard() {
		return diversityDashboard;
	}

	/**
	 * @param diversityDashboard
	 *            the diversityDashboard to set
	 */
	public void setDiversityDashboard(String diversityDashboard) {
		this.diversityDashboard = diversityDashboard;
	}

	/**
	 * @return the ethnicityDashboard
	 */
	public String getEthnicityDashboard() {
		return ethnicityDashboard;
	}

	/**
	 * @param ethnicityDashboard
	 *            the ethnicityDashboard to set
	 */
	public void setEthnicityDashboard(String ethnicityDashboard) {
		this.ethnicityDashboard = ethnicityDashboard;
	}

	/**
	 * @return the supplierDashboard
	 */
	public String getSupplierDashboard() {
		return supplierDashboard;
	}

	/**
	 * @param supplierDashboard
	 *            the supplierDashboard to set
	 */
	public void setSupplierDashboard(String supplierDashboard) {
		this.supplierDashboard = supplierDashboard;
	}

	/**
	 * @return the agencyDashboard
	 */
	public String getAgencyDashboard() {
		return agencyDashboard;
	}

	/**
	 * @param agencyDashboard
	 *            the agencyDashboard to set
	 */
	public void setAgencyDashboard(String agencyDashboard) {
		this.agencyDashboard = agencyDashboard;
	}

	/**
	 * @return the spendYear
	 */
	public Integer getSpendYear() {
		return spendYear;
	}

	/**
	 * @param spendYear
	 *            the spendYear to set
	 */
	public void setSpendYear(Integer spendYear) {
		this.spendYear = spendYear;
	}

	/**
	 * @return the reportDtos
	 */
	public List<Tier2ReportDto> getReportDtos() {
		return reportDtos;
	}

	/**
	 * @param reportDtos
	 *            the reportDtos to set
	 */
	public void setReportDtos(List<Tier2ReportDto> reportDtos) {
		this.reportDtos = reportDtos;
	}

	/**
	 * @return the tier2VendorName
	 */
	public String[] getTier2VendorNames() {
		return tier2VendorNames;
	}

	/**
	 * @param tier2VendorName
	 *            the tier2VendorName to set
	 */
	public void setTier2VendorNames(String[] tier2VendorNames) {
		this.tier2VendorNames = tier2VendorNames;
	}

	/**
	 * @return the reportMasters
	 */
	public List<Tier2ReportMaster> getReportMasters() {
		return reportMasters;
	}

	/**
	 * @param reportMasters
	 *            the reportMasters to set
	 */
	public void setReportMasters(List<Tier2ReportMaster> reportMasters) {
		this.reportMasters = reportMasters;
	}

	/**
	 * @return the certificates
	 */
	public List<Certificate> getCertificates() {
		return certificates;
	}

	/**
	 * @param certificates
	 *            the certificates to set
	 */
	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}

	/**
	 * @return the vendors
	 */
	public List<VendorMaster> getVendors() {
		return vendors;
	}

	/**
	 * @param vendors
	 *            the vendors to set
	 */
	public void setVendors(List<VendorMaster> vendors) {
		this.vendors = vendors;
	}

	/**
	 * @return the directPurchases
	 */
	public String[] getDirectPurchases() {
		return directPurchases;
	}

	/**
	 * @param directPurchases
	 *            the directPurchases to set
	 */
	public void setDirectPurchases(String[] directPurchases) {
		this.directPurchases = directPurchases;
	}

	/**
	 * @return the tier2Vendors
	 */
	public List<VendorMaster> getTier2Vendors() {
		return tier2Vendors;
	}

	/**
	 * @param tier2Vendors
	 *            the tier2Vendors to set
	 */
	public void setTier2Vendors(List<VendorMaster> tier2Vendors) {
		this.tier2Vendors = tier2Vendors;
	}

	/**
	 * @return the tier2ReportMasterId
	 */
	public Integer getTier2ReportMasterId() {
		return tier2ReportMasterId;
	}

	/**
	 * @param tier2ReportMasterId
	 *            the tier2ReportMasterId to set
	 */
	public void setTier2ReportMasterId(Integer tier2ReportMasterId) {
		this.tier2ReportMasterId = tier2ReportMasterId;
	}

	/**
	 * @return the tier2vendorName
	 */
	public String[] getTier2vendorName() {
		return tier2vendorName;
	}

	/**
	 * @param tier2vendorName
	 *            the tier2vendorName to set
	 */
	public void setTier2vendorName(String[] tier2vendorName) {
		this.tier2vendorName = tier2vendorName;
	}

	/**
	 * @return the reportingPeriod
	 */
	public String getReportingPeriod() {
		return reportingPeriod;
	}

	/**
	 * @param reportingPeriod
	 *            the reportingPeriod to set
	 */
	public void setReportingPeriod(String reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	/**
	 * @return the totalUsSalesSelectedQtr
	 */
	public String getTotalUsSalesSelectedQtr() {
		return totalUsSalesSelectedQtr;
	}

	/**
	 * @param totalUsSalesSelectedQtr
	 *            the totalUsSalesSelectedQtr to set
	 */
	public void setTotalUsSalesSelectedQtr(String totalUsSalesSelectedQtr) {
		this.totalUsSalesSelectedQtr = totalUsSalesSelectedQtr;
	}

	/**
	 * @return the totalUsSalestoSelectedQtr
	 */
	public String getTotalUsSalestoSelectedQtr() {
		return totalUsSalestoSelectedQtr;
	}

	/**
	 * @param totalUsSalestoSelectedQtr
	 *            the totalUsSalestoSelectedQtr to set
	 */
	public void setTotalUsSalestoSelectedQtr(String totalUsSalestoSelectedQtr) {
		this.totalUsSalestoSelectedQtr = totalUsSalestoSelectedQtr;
	}

	/**
	 * @return the indirectPer
	 */
	public String getIndirectPer() {
		return indirectPer;
	}

	/**
	 * @param indirectPer
	 *            the indirectPer to set
	 */
	public void setIndirectPer(String indirectPer) {
		this.indirectPer = indirectPer;
	}

	/**
	 * @return the directAmt
	 */
	public String[] getDirectAmt() {
		return directAmt;
	}

	/**
	 * @param directAmt
	 *            the directAmt to set
	 */
	public void setDirectAmt(String[] directAmt) {
		this.directAmt = directAmt;
	}

	/**
	 * @return the certificate
	 */
	public String[] getCertificate() {
		return certificate;
	}

	/**
	 * @param certificate
	 *            the certificate to set
	 */
	public void setCertificate(String[] certificate) {
		this.certificate = certificate;
	}

	/**
	 * @return the indirectAmt
	 */
	public String[] getIndirectAmt() {
		return indirectAmt;
	}

	/**
	 * @param indirectAmt
	 *            the indirectAmt to set
	 */
	public void setIndirectAmt(String[] indirectAmt) {
		this.indirectAmt = indirectAmt;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the reportPeriods
	 */
	public ArrayList<String> getReportPeriods() {
		return reportPeriods;
	}

	/**
	 * @param reportPeriods
	 *            the reportPeriods to set
	 */
	public void setReportPeriods(ArrayList<String> reportPeriods) {
		this.reportPeriods = reportPeriods;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName
	 *            the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the reportStartDate
	 */
	public String getReportStartDate() {
		return reportStartDate;
	}

	/**
	 * @param reportStartDate
	 *            the reportStartDate to set
	 */
	public void setReportStartDate(String reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	/**
	 * @return the reportEndDate
	 */
	public String getReportEndDate() {
		return reportEndDate;
	}

	/**
	 * @param reportEndDate
	 *            the reportEndDate to set
	 */
	public void setReportEndDate(String reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	/**
	 * @return the certificateDrt
	 */
	public String[] getCertificateDrt() {
		return certificateDrt;
	}

	/**
	 * @param certificateDrt
	 *            the certificateDrt to set
	 */
	public void setCertificateDrt(String[] certificateDrt) {
		this.certificateDrt = certificateDrt;
	}

	/**
	 * @return the ethnicity
	 */
	public String[] getEthnicity() {
		return ethnicity;
	}

	/**
	 * @param ethnicity
	 *            the ethnicity to set
	 */
	public void setEthnicity(String[] ethnicity) {
		this.ethnicity = ethnicity;
	}

	/**
	 * @return the ethnicities
	 */
	public List<Ethnicity> getEthnicities() {
		return ethnicities;
	}

	/**
	 * @param ethnicities
	 *            the ethnicities to set
	 */
	public void setEthnicities(List<Ethnicity> ethnicities) {
		this.ethnicities = ethnicities;
	}

	/**
	 * @return the reportType
	 */
	public Integer getReportType() {
		return reportType;
	}

	/**
	 * @param reportType
	 *            the reportType to set
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the naicCode
	 */
	public String getNaicCode() {
		return naicCode;
	}

	/**
	 * @return the naicsDesc
	 */
	public String getNaicsDesc() {
		return naicsDesc;
	}

	/**
	 * @param naicsDesc
	 *            the naicsDesc to set
	 */
	public void setNaicsDesc(String naicsDesc) {
		this.naicsDesc = naicsDesc;
	}

	/**
	 * @param naicCode
	 *            the naicCode to set
	 */
	public void setNaicCode(String naicCode) {
		this.naicCode = naicCode;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the diverse
	 */
	public String getDiverse() {
		return diverse;
	}

	/**
	 * @param diverse
	 *            the diverse to set
	 */
	public void setDiverse(String diverse) {
		this.diverse = diverse;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the countries
	 */
	public List<Country> getCountries() {
		return countries;
	}

	/**
	 * @param countries
	 *            the countries to set
	 */
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	/**
	 * @return the directExpenses
	 */
	public List<Tier2ReportDirectExpenses> getDirectExpenses() {
		return directExpenses;
	}

	/**
	 * @param directExpenses
	 *            the directExpenses to set
	 */
	public void setDirectExpenses(List<Tier2ReportDirectExpenses> directExpenses) {
		this.directExpenses = directExpenses;
	}

	/**
	 * @return the indirectExpenses
	 */
	public List<Tier2ReportIndirectExpenses> getIndirectExpenses() {
		return indirectExpenses;
	}

	/**
	 * @param indirectExpenses
	 *            the indirectExpenses to set
	 */
	public void setIndirectExpenses(
			List<Tier2ReportIndirectExpenses> indirectExpenses) {
		this.indirectExpenses = indirectExpenses;
	}

	/**
	 * @return the totalSpend
	 */
	public String getTotalSpend() {
		return totalSpend;
	}

	/**
	 * @param totalSpend
	 *            the totalSpend to set
	 */
	public void setTotalSpend(String totalSpend) {
		this.totalSpend = totalSpend;
	}

	/**
	 * @return the directSpend
	 */
	public String getDirectSpend() {
		return directSpend;
	}

	/**
	 * @param directSpend
	 *            the directSpend to set
	 */
	public void setDirectSpend(String directSpend) {
		this.directSpend = directSpend;
	}

	/**
	 * @return the indirectSpend
	 */
	public String getIndirectSpend() {
		return indirectSpend;
	}

	/**
	 * @param indirectSpend
	 *            the indirectSpend to set
	 */
	public void setIndirectSpend(String indirectSpend) {
		this.indirectSpend = indirectSpend;
	}

	/**
	 * @return the diversityStatus
	 */
	public String getDiversityStatus() {
		return diversityStatus;
	}

	/**
	 * @param diversityStatus
	 *            the diversityStatus to set
	 */
	public void setDiversityStatus(String diversityStatus) {
		this.diversityStatus = diversityStatus;
	}

	/**
	 * @return the ethnicityAnalysis
	 */
	public String getEthnicityAnalysis() {
		return ethnicityAnalysis;
	}

	/**
	 * @param ethnicityAnalysis
	 *            the ethnicityAnalysis to set
	 */
	public void setEthnicityAnalysis(String ethnicityAnalysis) {
		this.ethnicityAnalysis = ethnicityAnalysis;
	}

	/**
	 * @return the spendDashboard
	 */
	public String getSpendDashboard() {
		return spendDashboard;
	}

	/**
	 * @param spendDashboard
	 *            the spendDashboard to set
	 */
	public void setSpendDashboard(String spendDashboard) {
		this.spendDashboard = spendDashboard;
	}

	public List<String> getSpendYears() {
		return spendYears;
	}

	public void setSpendYears(List<String> spendYears) {
		this.spendYears = spendYears;
	}

	public Integer getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(Integer customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}	

	/**
	 * @return the bpVendorsCountDashboard
	 */
	public String getBpVendorsCountDashboard() {
		return bpVendorsCountDashboard;
	}

	/**
	 * @param bpVendorsCountDashboard the bpVendorsCountDashboard to set
	 */
	public void setBpVendorsCountDashboard(String bpVendorsCountDashboard) {
		this.bpVendorsCountDashboard = bpVendorsCountDashboard;
	}

	/**
	 * @return the tier2indirectvendorName
	 */
	public String[] getTier2indirectvendorName() {
		return tier2indirectvendorName;
	}

	/**
	 * @param tier2indirectvendorName the tier2indirectvendorName to set
	 */
	public void setTier2indirectvendorName(String[] tier2indirectvendorName) {
		this.tier2indirectvendorName = tier2indirectvendorName;
	}

	/**
	 * @return the indirectReportDtos
	 */
	public List<Tier2ReportDto> getIndirectReportDtos() {
		return indirectReportDtos;
	}

	/**
	 * @param indirectReportDtos the indirectReportDtos to set
	 */
	public void setIndirectReportDtos(List<Tier2ReportDto> indirectReportDtos) {
		this.indirectReportDtos = indirectReportDtos;
	}

	/**
	 * @return  previousQuarterStatus
	 */
	public Integer getPreviousQuarterStatus() {
		return previousQuarterStatus;
	}

	/**
	 * @param previousQuarterStatus the previousQuarterStatus to set
	 */
	public void setPreviousQuarterStatus(Integer previousQuarterStatus) {
		this.previousQuarterStatus = previousQuarterStatus;
	}

	public Integer getTier2ReportPreviousQuarterId() {
		return tier2ReportPreviousQuarterId;
	}

	public void setTier2ReportPreviousQuarterId(Integer tier2ReportPreviousQuarterId) {
		this.tier2ReportPreviousQuarterId = tier2ReportPreviousQuarterId;
	}

	public String getCurrentReportingPeriod() {
		return CurrentReportingPeriod;
	}

	public void setCurrentReportingPeriod(String currentReportingPeriod) {
		CurrentReportingPeriod = currentReportingPeriod;
	}

	/**
	 * @return the importExcelFile
	 */
	public FormFile getImportExcelFile() {
		return importExcelFile;
	}

	/**
	 * @param importExcelFile the importExcelFile to set
	 */
	public void setImportExcelFile(FormFile importExcelFile) {
		this.importExcelFile = importExcelFile;
	}

	/**
	 * @return the vendorStatusMaster
	 */
	public List<StatusMaster> getVendorStatusMaster() {
		return vendorStatusMaster;
	}

	/**
	 * @param vendorStatusMaster the vendorStatusMaster to set
	 */
	public void setVendorStatusMaster(List<StatusMaster> vendorStatusMaster) {
		this.vendorStatusMaster = vendorStatusMaster;
	}

	/**
	 * @return the vendorStatusList
	 */
	public String[] getVendorStatusList() {
		return vendorStatusList;
	}

	/**
	 * @param vendorStatusList the vendorStatusList to set
	 */
	public void setVendorStatusList(String[] vendorStatusList) {
		this.vendorStatusList = vendorStatusList;
	}
	
	/**
	 * @return the isRenewed
	 */
	public Integer getIsRenewed() {
		return isRenewed;
	}

	/**
	 * @param isRenewed the isRenewed to set
	 */
	public void setIsRenewed(Integer isRenewed) {
		this.isRenewed = isRenewed;
	}
	
	/**
	 * @return the indirectCommoditiesList
	 */
	public String[] getIndirectCommoditiesList() {
		return indirectCommoditiesList;
	}

	/**
	 * @param indirectCommoditiesList the indirectCommoditiesList to set
	 */
	public void setIndirectCommoditiesList(String[] indirectCommoditiesList) {
		this.indirectCommoditiesList = indirectCommoditiesList;
	}

	/**
	 * @return the certificateType
	 */
	public String[] getCertificateTypeList() {
		return certificateTypeList;
	}

	/**
	 * @param certificateType the certificateType to set
	 */
	public void setCertificateTypeList(String[] certificateType) {
		this.certificateTypeList = certificateType;
	}

	/**
	 * @return
	 */
	public String[] getEthnicityDrt() {
		return ethnicityDrt;
	}

	/**
	 * @param ethnicityDrt
	 */
	public void setEthnicityDrt(String[] ethnicityDrt) {
		this.ethnicityDrt = ethnicityDrt;
	}

	public String[] getMarketSector() {
		return marketSector;
	}

	public void setMarketSector(String[] marketSector) {
		this.marketSector = marketSector;
	}

	public String[] getMarketSectorDrt() {
		return marketSectorDrt;
	}

	public void setMarketSectorDrt(String[] marketSectorDrt) {
		this.marketSectorDrt = marketSectorDrt;
	}

	public List<MarketSector> getMarketSectorList() {
		return marketSectorList;
	}

	public void setMarketSectorList(List<MarketSector> marketSectorList) {
		this.marketSectorList = marketSectorList;
	}
	
	public String[] getReportPeriod() {
		return reportPeriod;
	}

	public void setReportPeriod(String[] reportPeriod) {
		this.reportPeriod = reportPeriod;
	}
	
	public Integer[] getReportSpendYears() {
		return reportSpendYears;
	}

	public void setReportSpendYears(Integer[] reportSpendYears) {
		this.reportSpendYears = reportSpendYears;
	}
	
	public List<CustomerCommodityCategory> getMarketSubSectorList() {
		return marketSubSectorList;
	}

	public void setMarketSubSectorList(
			List<CustomerCommodityCategory> marketSubSectorList) {
		this.marketSubSectorList = marketSubSectorList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Reset the Report Form
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.certificate = null;
		this.directAmt = null;
		this.indirectAmt = null;
		this.indirectPer = null;
		this.spendYear = null;
		this.reportSpendYears = null;
		this.reportingPeriod = "";
		this.totalUsSalesSelectedQtr = "";
		this.totalUsSalestoSelectedQtr = "";
		this.tier2vendorName = null;
		this.reportStartDate = "";
		this.reportEndDate = "";
		this.certificateDrt = null;
		this.tier2VendorNames = null;
		this.ethnicity = null;
		this.ethnicityDrt = null;
		this.reportType = null;
		this.vendorName = "";
		this.naicCode = "";
		this.naicsDesc = "";
		this.city = "";
		this.state = "";
		this.region = "";
		this.country = "";
		this.diverse = "";
		this.province = "";
		this.directExpenses = null;
		this.indirectExpenses = null;
		this.tier2ReportMasterId = null;
		this.agencyDashboard = "";
		this.diversityDashboard = "";
		this.supplierDashboard = "";
		this.ethnicityDashboard = "";
		this.totalSpend = "";
		this.directSpend = "";
		this.indirectSpend = "";
		this.diversityStatus = "";
		this.ethnicityAnalysis = "";
		this.spendDashboard = "";
		this.comments = "";
		this.vendorStatus = "";
		this.bpVendorsCountDashboard = "";
		this.importExcelFile=null;
		this.startDate = "";
		this.endDate = "";
		this.vendorStatusList = null;
		this.vendorStatusMaster = null;
		this.isRenewed = null;
		this.indirectCommoditiesList = null;
		this.certificateTypeList = null;
		this.marketSector = null;
		this.marketSectorDrt = null;
		this.reportPeriod = null;
		super.reset(mapping, request);
	}
}