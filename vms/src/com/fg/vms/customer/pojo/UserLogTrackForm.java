/**
 * 
 */
package com.fg.vms.customer.pojo;

import org.apache.struts.validator.ValidatorForm;

/**
 * @author venkatesh
 * 
 */
public class UserLogTrackForm extends ValidatorForm {

	private static final long serialVersionUID = 1L;
	
	/** The user loginId. */
	private Integer id;

	/** The user login. */
	private String userLogon;

	/** The user firstname. */
	private String firstName;
	
	/** The user lastname. */
	private String lastName;

	/** The user email id. */
	private String userEmailId;

	/** The user role name. */
	private String roleName;
	
	private String username;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the userLogon
	 */
	public String getUserLogon() {
		return userLogon;
	}

	/**
	 * @param userLogon the userLogon to set
	 */
	public void setUserLogon(String userLogon) {
		this.userLogon = userLogon;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the userEmailId
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * @param userEmailId the userEmailId to set
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}	
}