/*
 * CopyTemplateForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.List;

import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.TemplateQuestions;

/**
 * Represents the details of copying new template from existing template
 * (eg.templateName, templateQuestions) for interact with UI
 * 
 * @author vivek
 * 
 */
public class CopyTemplateForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private String template;

    private List<Template> templateNames;

    private List<TemplateQuestions> Questions;

    private String checkQuestions;

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return the templateNames
     */
    public List<Template> getTemplateNames() {
        return templateNames;
    }

    /**
     * @param templateNames the templateNames to set
     */
    public void setTemplateNames(List<Template> templateNames) {
        this.templateNames = templateNames;
    }

    /**
     * @return the questions
     */
    public List<TemplateQuestions> getQuestions() {
        return Questions;
    }

    /**
     * @param questions the questions to set
     */
    public void setQuestions(List<TemplateQuestions> questions) {
        Questions = questions;
    }

    /**
     * @return the checkQuestions
     */
    public String getCheckQuestions() {
        return checkQuestions;
    }

    /**
     * @param checkQuestions the checkQuestions to set
     */
    public void setCheckQuestions(String checkQuestions) {
        this.checkQuestions = checkQuestions;
    }

}
