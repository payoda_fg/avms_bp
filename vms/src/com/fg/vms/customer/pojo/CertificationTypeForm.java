/**
 * 
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * @author venkatesh
 * 
 */
public class CertificationTypeForm extends ValidatorForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer certificateId;
	private String description;
	private String shortDescription;
	private String isActive;
	private String[] certifiacateDivision;
	private Byte isCertificateExpiryAlertRequired;

	/**
	 * @return the certificateId
	 */
	public Integer getCertificateId() {
		return certificateId;
	}

	/**
	 * @param certificateId
	 *            the certificateId to set
	 */
	public void setCertificateId(Integer certificateId) {
		this.certificateId = certificateId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param certificateId
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * @param certificateId
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the certifiacateDivision
	 */
	public String[] getCertifiacateDivision() {
		return certifiacateDivision;
	}

	/**
	 * @param certifiacateDivision the certifiacateDivision to set
	 */
	public void setCertifiacateDivision(String[] certifiacateDivision) {
		this.certifiacateDivision = certifiacateDivision;
	}

	/**
	 * @return the isCertificateExpiryAlertRequired
	 */
	public Byte getIsCertificateExpiryAlertRequired() {
		return isCertificateExpiryAlertRequired;
	}

	/**
	 * @param isCertificateExpiryAlertRequired the isCertificateExpiryAlertRequired to set
	 */
	public void setIsCertificateExpiryAlertRequired(
			Byte isCertificateExpiryAlertRequired) {
		this.isCertificateExpiryAlertRequired = isCertificateExpiryAlertRequired;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.certificateId = null;
		this.description = "";
		this.shortDescription = "";
		this.isActive = "";
		this.certifiacateDivision = null;
		this.isCertificateExpiryAlertRequired = null;
	}

}
