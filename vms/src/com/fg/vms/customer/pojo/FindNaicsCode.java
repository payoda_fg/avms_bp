/*
 * FindNaicsCode.java
 */

package com.fg.vms.customer.pojo;

import org.apache.struts.action.ActionForm;

/**
 * Represents the NAICS Details (eg.naicsCategory, naicsSubCategory, and
 * naicsMasterCode details) for interact with UI
 * 
 * @author srinivasarao
 * 
 */
public class FindNaicsCode extends ActionForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    private String naicsCategoryId;

    private String naicsSubCategoryId;

    private String naicMasterCode;

    public static long getSerialversionuid() {
	return serialVersionUID;
    }

    /**
     * @return the naicsCategoryId
     */
    public String getNaicsCategoryId() {
	return naicsCategoryId;
    }

    /**
     * @param naicsCategoryId
     *            the naicsCategoryId to set
     */
    public void setNaicsCategoryId(String naicsCategoryId) {
	this.naicsCategoryId = naicsCategoryId;
    }

    /**
     * @return the naicsSubCategoryId
     */
    public String getNaicsSubCategoryId() {
	return naicsSubCategoryId;
    }

    /**
     * @param naicsSubCategoryId
     *            the naicsSubCategoryId to set
     */
    public void setNaicsSubCategoryId(String naicsSubCategoryId) {
	this.naicsSubCategoryId = naicsSubCategoryId;
    }

    /**
     * @return the naicMasterCode
     */
    public String getNaicMasterCode() {
	return naicMasterCode;
    }

    /**
     * @param naicMasterCode
     *            the naicMasterCode to set
     */
    public void setNaicMasterCode(String naicMasterCode) {
	this.naicMasterCode = naicMasterCode;
    }
}