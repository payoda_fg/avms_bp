/*
 * SearchVendorForm.java
 */
package com.fg.vms.customer.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.VendorMaster;

/**
 * The Class SearchVendorForm.
 */
public class SearchVendorForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The vendor name. */
	private String vendorName;

	/** The naic code. */
	private String naicCode;

	/** The naic desc. */
	private String naicsDesc;

	/** The region. */
	private String region;

	/** The city. */
	private String city;

	/** The status. */
	private String status;

	/** The category. */
	private String category;

	/** The province. */
	private String province;

	/** The diverse. */
	private String diverse;

	/** The country. */
	private String country;

	/** The sub category. */
	private String subCategory;

	/** The state. */
	private String state;

	/** The prime. */
	private Byte prime;

	/** The select all. */
	private Byte selectAll;

	/** The Select. */
	private Byte Select;

	/** The status list. */
	private ArrayList<String> statusList;

	/** The category name. */
	private String categoryName;

	/** The sub category name. */
	private String subCategoryName;

	/** The categories. */
	private List<NaicsCategoryDto> categories;

	/** The sub categories. */
	private List<NAICSubCategory> subCategories;

	/** The countries. */
	private List<Country> countries;

	/** The vendors list. */
	List<SearchVendorDto> vendorsList;

	private List<String> fields;

	/** The selected email address. */
	private String selectedEmailAddress[];

	/** The year. */
	private Integer year;

	/** The vendor id. */
	private Integer vendorId;

	/** The vendors. */
	private List<VendorMaster> vendors;

	/** The primeornonprimevendor. */
	private String primeornonprimevendor;

	/** The template. */
	private String template;

	/** The template names. */
	private List<Template> templateNames;

	/** The final date for reply. */
	private Date finalDateForReply;

	/** The approvalStatus. */
	private Integer approvalStatus;

	/** The approvalStatus. */
	private String isApprovedDesc;

	/** The tier2VendorNames. */
	private String[] diverseCertificateNames;

	private String[] certifyAgencies;

	private String[] searchFields;

	private String[] searchFieldsValue;

	private List<CertifyingAgency> certifyingAgencies;
	private String[] selectCountries;

	private String[] searchVendorStatus;
	
	private String[] allStatusList;
	
	private String primeVendorStatus;

	private String vendorType;

	private String to;

	private String cc;

	private String bcc;

	private String searchFullText;

	private Integer customerDivisionId;

	private Byte isDivision;

	private String[] businessAreaName;

	private String companyName;

	private String email;

	private String firstName;
	
	private String searchQuery;
	
	private String searchConditions;
	
	private List<String> searchCriterias;

	/** The status list. */
	private List<StatusMaster> vendorStatusList;

	private List<StatusMaster> vendorStatus;
	private List<State> states;

	private List<GeographicalStateRegionDto> exportMasters;

	private String[] exportFields;

	private String[] commodityDescription;

	private String[] naicsCode;

	private String[] naicsDescription;
	
	private String[] certificationType;
	
	private List<String> fromtableForConditions;
	
	private String[] vendorCommodity;
	
	private String nonNMSDCWBENCQuery;
	
	private String[] certificateAgency;
	
	private String[] certificateAgencyIds;
	
	private String[] certificateName;
	
	private String[] bpSegmentIds;
	
	private String[] bpMarketSectorId;
	
	private Integer[] bpMarketSectorIds;
	
	private String[] vendorNamesList;
	
	private Double minRevenueRange;
	
	private Double maxRevenueRange;
	
	private String yearRelationId;
	
	private Integer establishedYear;
	
	private String criteriaJsonString;
	
	private String keyWord;
	
	private String lastName;
	
	private String capabilities;
	
	private String oilAndGas;
	
	private String diversClassificationNotes;
	
	private String dunsNumber;
	
	private String taxId;
	
	private String noOfEmployee;
	
	private String companyType;
	
	private String address;
	
	private String zipCode;
	
	private String mobile;
	
	private String phone;
	
	private String fax;
	
	private String webSiteUrl;
	
	private String title;
	
	private String contactPhone;
	
	private String contactMobile;
	
	private String contactFax;
	
	private String contactEmail;
	
	private String rfiRfpNote;
	
	private String certificationNumber;
	
	private String effectiveDate;
	
	private String expireDate;
	
	private String contactDate;
	
	private String contactFirstName;
	
	private String contactLastName;
	
	private String contactState;
	
	private String contactEvent;
	
	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public List<GeographicalStateRegionDto> getExportMasters() {
		return exportMasters;
	}

	public void setExportMasters(List<GeographicalStateRegionDto> exportMasters) {
		this.exportMasters = exportMasters;
	}

	public String[] getExportFields() {
		return exportFields;
	}

	public void setExportFields(String[] exportFields) {
		this.exportFields = exportFields;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public List<StatusMaster> getVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(List<StatusMaster> vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

	/**
	 * @return the selectCountries
	 */
	public String[] getSelectCountries() {
		return selectCountries;
	}

	/**
	 * @param selectCountries
	 *            the selectCountries to set
	 */
	public void setSelectCountries(String[] selectCountries) {
		this.selectCountries = selectCountries;
	}

	/**
	 * @return the certifyAgencies
	 */
	public String[] getCertifyAgencies() {
		return certifyAgencies;
	}

	/**
	 * @param certifyAgencies
	 *            the certifyAgencies to set
	 */
	public void setCertifyAgencies(String[] certifyAgencies) {
		this.certifyAgencies = certifyAgencies;
	}

	/**
	 * @return the certifyingAgencies
	 */
	public List<CertifyingAgency> getCertifyingAgencies() {
		return certifyingAgencies;
	}

	/**
	 * @param certifyingAgencies
	 *            the certifyingAgencies to set
	 */
	public void setCertifyingAgencies(List<CertifyingAgency> certifyingAgencies) {
		this.certifyingAgencies = certifyingAgencies;
	}

	/**
	 * @return the searchFields
	 */
	public String[] getSearchFields() {
		return searchFields;
	}

	/**
	 * @param searchFields
	 *            the searchFields to set
	 */
	public void setSearchFields(String[] searchFields) {
		this.searchFields = searchFields;
	}

	/**
	 * @return the searchFieldsValue
	 */
	public String[] getSearchFieldsValue() {
		return searchFieldsValue;
	}

	/**
	 * @param searchFieldsValue
	 *            the searchFieldsValue to set
	 */
	public void setSearchFieldsValue(String[] searchFieldsValue) {
		this.searchFieldsValue = searchFieldsValue;
	}

	/**
	 * Gets the vendors.
	 * 
	 * @return the vendors
	 */
	public List<VendorMaster> getVendors() {
		return vendors;
	}

	/**
	 * Sets the vendors.
	 * 
	 * @param vendors
	 *            the vendors to set
	 */
	public void setVendors(List<VendorMaster> vendors) {
		this.vendors = vendors;
	}

	/**
	 * Gets the vendor id.
	 * 
	 * @return the vendorId
	 */
	public Integer getVendorId() {
		return vendorId;
	}

	/**
	 * Sets the vendor id.
	 * 
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * Gets the year.
	 * 
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * Sets the year.
	 * 
	 * @param year
	 *            the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * Gets the vendors list.
	 * 
	 * @return the vendorsList
	 */
	public List<SearchVendorDto> getVendorsList() {
		return vendorsList;
	}

	/**
	 * Sets the vendors list.
	 * 
	 * @param vendorsList
	 *            the vendorsList to set
	 */
	public void setVendorsList(List<SearchVendorDto> vendorsList) {
		this.vendorsList = vendorsList;
	}

	/**
	 * Gets the countries.
	 * 
	 * @return the countries
	 */
	public List<Country> getCountries() {
		return countries;
	}

	/**
	 * Sets the countries.
	 * 
	 * @param countries
	 *            the countries to set
	 */
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	/**
	 * Gets the select.
	 * 
	 * @return the select
	 */
	public Byte getSelect() {
		return Select;
	}

	/**
	 * Sets the select.
	 * 
	 * @param select
	 *            the select to set
	 */
	public void setSelect(Byte select) {
		Select = select;
	}

	/**
	 * Gets the select all.
	 * 
	 * @return the selectAll
	 */
	public Byte getSelectAll() {
		return selectAll;
	}

	/**
	 * Sets the select all.
	 * 
	 * @param selectAll
	 *            the selectAll to set
	 */
	public void setSelectAll(Byte selectAll) {
		this.selectAll = selectAll;
	}

	/**
	 * Gets the status list.
	 * 
	 * @return the statusList
	 */
	public ArrayList<String> getStatusList() {
		return statusList;
	}

	/**
	 * Sets the status list.
	 * 
	 * @param statusList
	 *            the statusList to set
	 */
	public void setStatusList(ArrayList<String> statusList) {
		this.statusList = statusList;
	}

	/**
	 * Gets the category name.
	 * 
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * Sets the category name.
	 * 
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * Gets the sub category name.
	 * 
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * Sets the sub category name.
	 * 
	 * @param subCategoryName
	 *            the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * Gets the categories.
	 * 
	 * @return the categories
	 */
	public List<NaicsCategoryDto> getCategories() {
		return categories;
	}

	/**
	 * Sets the categories.
	 * 
	 * @param categories
	 *            the categories to set
	 */
	public void setCategories(List<NaicsCategoryDto> categories) {
		this.categories = categories;
	}

	/**
	 * Gets the sub categories.
	 * 
	 * @return the subCategories
	 */
	public List<NAICSubCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * Sets the sub categories.
	 * 
	 * @param subCategories
	 *            the subCategories to set
	 */
	public void setSubCategories(List<NAICSubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	/**
	 * Gets the naic code.
	 * 
	 * @return the naicCode
	 */
	public String getNaicCode() {
		return naicCode;
	}

	/**
	 * Sets the naic code.
	 * 
	 * @param naicCode
	 *            the naicCode to set
	 */
	public void setNaicCode(String naicCode) {
		this.naicCode = naicCode;
	}

	/**
	 * @return the naicsDesc
	 */
	public String getNaicsDesc() {
		return naicsDesc;
	}

	/**
	 * @param naicsDesc
	 *            the naicsDesc to set
	 */
	public void setNaicsDesc(String naicsDesc) {
		this.naicsDesc = naicsDesc;
	}

	/**
	 * Gets the region.
	 * 
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Sets the region.
	 * 
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the category.
	 * 
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 * 
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * Gets the province.
	 * 
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 * 
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * Gets the diverse.
	 * 
	 * @return the diverse
	 */
	public String getDiverse() {
		return diverse;
	}

	/**
	 * Sets the diverse.
	 * 
	 * @param diverse
	 *            the diverse to set
	 */
	public void setDiverse(String diverse) {
		this.diverse = diverse;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the sub category.
	 * 
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}

	/**
	 * Sets the sub category.
	 * 
	 * @param subCategory
	 *            the subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * Gets the state.
	 * 
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 * 
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the prime.
	 * 
	 * @return the prime
	 */
	public Byte getPrime() {
		return prime;
	}

	/**
	 * Sets the prime.
	 * 
	 * @param prime
	 *            the prime to set
	 */
	public void setPrime(Byte prime) {
		this.prime = prime;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the vendor name.
	 * 
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * Sets the vendor name.
	 * 
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * Gets the selected email address.
	 * 
	 * @return the selectedEmailAddress
	 */
	public String[] getSelectedEmailAddress() {
		return selectedEmailAddress;
	}

	/**
	 * Sets the selected email address.
	 * 
	 * @param selectedEmailAddress
	 *            the selectedEmailAddress to set
	 */
	public void setSelectedEmailAddress(String[] selectedEmailAddress) {
		this.selectedEmailAddress = selectedEmailAddress;
	}

	/**
	 * Gets the primeornonprimevendor.
	 * 
	 * @return the primeornonprimevendor
	 */
	public String getPrimeornonprimevendor() {
		return primeornonprimevendor;
	}

	/**
	 * Sets the primeornonprimevendor.
	 * 
	 * @param primeornonprimevendor
	 *            the new primeornonprimevendor
	 */
	public void setPrimeornonprimevendor(String primeornonprimevendor) {
		this.primeornonprimevendor = primeornonprimevendor;
	}

	/**
	 * Gets the template.
	 * 
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Sets the template.
	 * 
	 * @param template
	 *            the new template
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * Gets the template names.
	 * 
	 * @return the template names
	 */
	public List<Template> getTemplateNames() {
		return templateNames;
	}

	/**
	 * Sets the template names.
	 * 
	 * @param templateNames
	 *            the new template names
	 */
	public void setTemplateNames(List<Template> templateNames) {
		this.templateNames = templateNames;
	}

	/**
	 * Gets the final date for reply.
	 * 
	 * @return the final date for reply
	 */
	public Date getFinalDateForReply() {
		return finalDateForReply;
	}

	/**
	 * Sets the final date for reply.
	 * 
	 * @param finalDateForReply
	 *            the new final date for reply
	 */
	public void setFinalDateForReply(Date finalDateForReply) {
		this.finalDateForReply = finalDateForReply;
	}

	/**
	 * @return the approvalStatus
	 */
	public Integer getApprovalStatus() {
		return approvalStatus;
	}

	/**
	 * @param approvalStatus
	 *            the approvalStatus to set
	 */
	public void setApprovalStatus(Integer approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * @return the isApprovedDesc
	 */
	public String getIsApprovedDesc() {
		return isApprovedDesc;
	}

	/**
	 * @param isApprovedDesc
	 *            the isApprovedDesc to set
	 */
	public void setIsApprovedDesc(String isApprovedDesc) {
		this.isApprovedDesc = isApprovedDesc;
	}

	
	public String getDiverseCertificateNamesAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getDiverseCertificateNames());
	}
	/**
	 * @return the diverseCertificateNames
	 */
	public String[] getDiverseCertificateNames() {
		return diverseCertificateNames;
	}

	/**
	 * @param diverseCertificateNames
	 *            the diverseCertificateNames to set
	 */
	public void setDiverseCertificateNames(String[] diverseCertificateNames) {
		this.diverseCertificateNames = diverseCertificateNames;
	}

	/**
	 * @return the fields
	 */
	public List<String> getFields() {
		return fields;
	}

	/**
	 * @param fields
	 *            the fields to set
	 */
	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSearchFullText() {
		return searchFullText;
	}

	public void setSearchFullText(String searchFullText) {
		this.searchFullText = searchFullText;
	}

	public Integer getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(Integer customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}

	public Byte getIsDivision() {
		return isDivision;
	}

	public void setIsDivision(Byte isDivision) {
		this.isDivision = isDivision;
	}
	
	public String getBusinessAreaNameAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getBusinessAreaName());
	}
	
	public String[] getBusinessAreaName() {
		return businessAreaName;
	}

	public void setBusinessAreaName(String[] businessAreaName) {
		this.businessAreaName = businessAreaName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.exportFields = null;
		this.searchFullText = "";
		this.vendorName = "";
		this.naicCode = null;
		this.naicsDesc = "";
		this.category = "";
		this.categoryName = "";
		this.subCategory = "";
		this.province = "";
		this.city = "";
		this.state = "";
		this.region = "";
		this.country = "";
		this.diverse = "";
		this.primeornonprimevendor = "";
		this.status = "";
		this.diverseCertificateNames = null;
		this.searchFieldsValue = null;
		this.selectCountries = null;
		this.certifyAgencies = null;
		this.searchVendorStatus = null;
		this.primeVendorStatus="";
		this.vendorType = "";
		this.to = "";
		this.cc = "";
		this.bcc = "";
		this.vendorStatus = null;
		this.exportMasters = null;
		this.businessAreaName = null;
		this.commodityDescription = null;
		this.naicCode = null;
		this.naicsDescription = null;
		this.certificationType=null;
		this.searchCriterias=null;
		this.searchQuery="";
		this.vendorCommodity = null;
		this.nonNMSDCWBENCQuery = null;
		this.certificateAgency = null;
		this.certificateName = null;
		this.searchFields = null;
		this.allStatusList = null;
		this.bpMarketSectorId = null;
		this.vendorNamesList = null;
		this.minRevenueRange = null;
		this.maxRevenueRange = null;
		this.establishedYear = null;
		this.yearRelationId = null;
		this.companyName="";
		this.firstName="";
		this.lastName="";
		this.capabilities="";
		this.bpSegmentIds=null;
		this.oilAndGas="";
		this.dunsNumber="";
		this.taxId="";
		this.noOfEmployee="";
		this.companyType="";
		this.address="";
		this.zipCode="";
		this.mobile="";
		this.phone="";
		this.fax="";
		this.webSiteUrl="";
		this.rfiRfpNote="";
		this.certificateAgencyIds=null;
		this.certificationNumber="";
		this.effectiveDate="";
		this.expireDate="";
		this.title="";
		this.contactPhone="";
		this.contactMobile="";
		this.contactFax="";
		this.contactEmail="";
		this.contactDate="";
		this.contactFirstName="";
		this.contactLastName="";
		this.contactState="";
		this.contactEvent="";
//		this.criteriaJsonString = null;
		super.reset(mapping, request);
	}

	public List<StatusMaster> getVendorStatusList() {
		return vendorStatusList;
	}

	public void setVendorStatusList(List<StatusMaster> vendorStatusList) {
		this.vendorStatusList = vendorStatusList;
	}

	public String[] getCommodityDescription() {
		return commodityDescription;
	}

	public void setCommodityDescription(String[] commodityDescription) {
		this.commodityDescription = commodityDescription;
	}

	public String[] getNaicsCode() {
		return naicsCode;
	}

	public void setNaicsCode(String[] naicsCode) {
		this.naicsCode = naicsCode;
	}

	public String[] getNaicsDescription() {
		return naicsDescription;
	}

	public void setNaicsDescription(String[] naicsDescription) {
		this.naicsDescription = naicsDescription;
	}
	
	public String getCertificationTypeAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getCertificationType());
	}

	public String[] getCertificationType() {
		return certificationType;
	}

	public void setCertificationType(String[] certificationType) {
		this.certificationType = certificationType;
	}

	public List<String> getSearchCriterias() {
		return searchCriterias;
	}

	public void setSearchCriterias(List<String> searchCriterias) {
		this.searchCriterias = searchCriterias;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public String getSearchConditions() {
		return searchConditions;
	}

	public void setSearchConditions(String searchConditions) {
		this.searchConditions = searchConditions;
	}

	public List<String> getFromtableForConditions() {
		return fromtableForConditions;
	}

	public void setFromtableForConditions(List<String> fromtableForConditions) {
		this.fromtableForConditions = fromtableForConditions;
	}

	public String getVendorCommodityAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getVendorCommodity());
	}
	/**
	 * @return the vendorCommodity
	 */
	public String[] getVendorCommodity() {
		return vendorCommodity;
	}

	/**
	 * @param vendorCommodity the vendorCommodity to set
	 */
	public void setVendorCommodity(String[] vendorCommodity) {
		this.vendorCommodity = vendorCommodity;
	}

	public String[] getSearchVendorStatus() {
		return searchVendorStatus;
	}

	public void setSearchVendorStatus(String[] searchVendorStatus) {
		this.searchVendorStatus = searchVendorStatus;
	}

	public String getPrimeVendorStatus() {
		return primeVendorStatus;
	}

	public void setPrimeVendorStatus(String primeVendorStatus) {
		this.primeVendorStatus = primeVendorStatus;
	}

	/**
	 * @return the nonNMSDCWBENCQuery
	 */
	public String getNonNMSDCWBENCQuery() {
		return nonNMSDCWBENCQuery;
	}

	/**
	 * @param nonNMSDCWBENCQuery the nonNMSDCWBENCQuery to set
	 */
	public void setNonNMSDCWBENCQuery(String nonNMSDCWBENCQuery) {
		this.nonNMSDCWBENCQuery = nonNMSDCWBENCQuery;
	}

	/**
	 * @return the certificateAgency
	 */
	public String[] getCertificateAgency() {
		return certificateAgency;
	}

	/**
	 * @param certificateAgency the certificateAgency to set
	 */
	public void setCertificateAgency(String[] certificateAgency) {
		this.certificateAgency = certificateAgency;
	}

	/**
	 * @return the certificateName
	 */
	public String[] getCertificateName() {
		return certificateName;
	}

	/**
	 * @param certificateName the certificateName to set
	 */
	public void setCertificateName(String[] certificateName) {
		this.certificateName = certificateName;
	}
	
	public String getBpSegmentIdsAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getBpSegmentIds());
	}

	/**
	 * @return the bpSegmentIds
	 */
	public String[] getBpSegmentIds() {
		return bpSegmentIds;
	}

	/**
	 * @param bpSegmentIds the bpSegmentIds to set
	 */
	public void setBpSegmentIds(String[] bpSegmentIds) {
		this.bpSegmentIds = bpSegmentIds;
	}

	/**
	 * @return the allStatusList
	 */
	public String[] getAllStatusList() {
		return allStatusList;
	}

	/**
	 * @param allStatusList the allStatusList to set
	 */
	public void setAllStatusList(String[] allStatusList) {
		this.allStatusList = allStatusList;
	}

	public String getBpMarketSectorIdAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getBpMarketSectorId());
	}
	
	/**
	 * @return the bpMarketSectorId
	 */
	public String[] getBpMarketSectorId() {
		return bpMarketSectorId;
	}

	/**
	 * @param bpMarketSectorId the bpMarketSectorId to set
	 */
	public void setBpMarketSectorId(String[] bpMarketSectorId) {
		this.bpMarketSectorId = bpMarketSectorId;
	}

	/**
	 * @return the vendorNamesList
	 */
	public String[] getVendorNamesList() {
		return vendorNamesList;
	}

	/**
	 * @param vendorNamesList the vendorNamesList to set
	 */
	public void setVendorNamesList(String[] vendorNamesList) {
		this.vendorNamesList = vendorNamesList;
	}

	public Double getMinRevenueRange() {
		return minRevenueRange;
	}

	public void setMinRevenueRange(Double minRevenueRange) {
		this.minRevenueRange = minRevenueRange;
	}

	public Double getMaxRevenueRange() {
		return maxRevenueRange;
	}

	public void setMaxRevenueRange(Double maxRevenueRange) {
		this.maxRevenueRange = maxRevenueRange;
	}

	public String getYearRelationId() {
		return yearRelationId;
	}

	public void setYearRelationId(String yearRelationId) {
		this.yearRelationId = yearRelationId;
	}

	public Integer getEstablishedYear() {
		return establishedYear;
	}

	public void setEstablishedYear(Integer establishedYear) {
		this.establishedYear = establishedYear;
	}

	public String getCriteriaJsonString() {
		return criteriaJsonString;
	}

	public void setCriteriaJsonString(String criteriaJsonString) {
		this.criteriaJsonString = criteriaJsonString;
	}

	public Integer[] getBpMarketSectorIds() {
		return bpMarketSectorIds;
	}

	public void setBpMarketSectorIds(Integer[] bpMarketSectorIds) {
		this.bpMarketSectorIds = bpMarketSectorIds;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(String capabilities) {
		this.capabilities = capabilities;
	}

	public String getOilAndGas() {
		return oilAndGas;
	}

	public void setOilAndGas(String oilAndGas) {
		this.oilAndGas = oilAndGas;
	}

	public String getDiversClassificationNotes() {
		return diversClassificationNotes;
	}

	public void setDiversClassificationNotes(String diversClassificationNotes) {
		this.diversClassificationNotes = diversClassificationNotes;
	}

	public String getDunsNumber() {
		return dunsNumber;
	}

	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getNoOfEmployee() {
		return noOfEmployee;
	}

	public void setNoOfEmployee(String noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getWebSiteUrl() {
		return webSiteUrl;
	}

	public void setWebSiteUrl(String webSiteUrl) {
		this.webSiteUrl = webSiteUrl;
	}

	public String getRfiRfpNote() {
		return rfiRfpNote;
	}

	public void setRfiRfpNote(String rfiRfpNote) {
		this.rfiRfpNote = rfiRfpNote;
	}

	public String[] getCertificateAgencyIds() {
		return certificateAgencyIds;
	}

	public void setCertificateAgencyIds(String[] certificateAgencyIds) {
		this.certificateAgencyIds = certificateAgencyIds;
	}
	
	public String getCertificateAgencyIdsAsJsonString() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(getCertificateAgencyIds());
	}

	public String getCertificationNumber() {
		return certificationNumber;
	}

	public void setCertificationNumber(String certificationNumber) {
		this.certificationNumber = certificationNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public String getContactFax() {
		return contactFax;
	}

	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactDate() {
		return contactDate;
	}

	public void setContactDate(String contactDate) {
		this.contactDate = contactDate;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactState() {
		return contactState;
	}

	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	public String getContactEvent() {
		return contactEvent;
	}

	public void setContactEvent(String contactEvent) {
		this.contactEvent = contactEvent;
	}
}
