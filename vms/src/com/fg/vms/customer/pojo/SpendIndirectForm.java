/*
 * SpendIndirectForm.java
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * The Class SpendIndirectForm.
 */
public class SpendIndirectForm extends ValidatorForm {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Integer id;

    /** The vendor id. */
    private Integer vendorId;

    /** The vendor. */
    private String vendor;

    /** The vendor number. */
    private Integer vendorNumber;

    /** The vendor contact name. */
    private String vendorContactName;

    /** The vendor email address. */
    private String vendorEmailAddress;

    /** The reporting year. */
    private String reportingYear;

    /** The reporting quater from date. */
    private String reportingQuaterFromDate;

    /** The reporting quater to date. */
    private String reportingQuaterToDate;

    /** The total prime supplier sales. */
    private Double totalPrimeSupplierSales;

    /** The total direct sales. */
    private Double totalDirectSales;

    /** The percentage of direct sales. */
    private Double percentageOfDirectSales;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the vendor id.
     * 
     * @return the vendor id
     */
    public Integer getVendorId() {
	return vendorId;
    }

    /**
     * Sets the vendor id.
     * 
     * @param vendorId
     *            the new vendor id
     */
    public void setVendorId(Integer vendorId) {
	this.vendorId = vendorId;
    }

    /**
     * Gets the vendor.
     * 
     * @return the vendor
     */
    public String getVendor() {
	return vendor;
    }

    /**
     * Sets the vendor.
     * 
     * @param vendor
     *            the vendor to set
     */
    public void setVendor(String vendor) {
	this.vendor = vendor;
    }

    /**
     * Gets the vendor number.
     * 
     * @return the vendorNumber
     */
    public Integer getVendorNumber() {
	return vendorNumber;
    }

    /**
     * Sets the vendor number.
     * 
     * @param vendorNumber
     *            the vendorNumber to set
     */
    public void setVendorNumber(Integer vendorNumber) {
	this.vendorNumber = vendorNumber;
    }

    /**
     * Gets the vendor contact name.
     * 
     * @return the vendorContactName
     */
    public String getVendorContactName() {
	return vendorContactName;
    }

    /**
     * Sets the vendor contact name.
     * 
     * @param vendorContactName
     *            the vendorContactName to set
     */
    public void setVendorContactName(String vendorContactName) {
	this.vendorContactName = vendorContactName;
    }

    /**
     * Gets the vendor email address.
     * 
     * @return the vendorEmailAddress
     */
    public String getVendorEmailAddress() {
	return vendorEmailAddress;
    }

    /**
     * Sets the vendor email address.
     * 
     * @param vendorEmailAddress
     *            the vendorEmailAddress to set
     */
    public void setVendorEmailAddress(String vendorEmailAddress) {
	this.vendorEmailAddress = vendorEmailAddress;
    }

    /**
     * Gets the reporting year.
     * 
     * @return the reportingYear
     */
    public String getReportingYear() {
	return reportingYear;
    }

    /**
     * Sets the reporting year.
     * 
     * @param reportingYear
     *            the reportingYear to set
     */
    public void setReportingYear(String reportingYear) {
	this.reportingYear = reportingYear;
    }

    /**
     * Gets the reporting quater from date.
     * 
     * @return the reportingQuaterFromDate
     */
    public String getReportingQuaterFromDate() {
	return reportingQuaterFromDate;
    }

    /**
     * Sets the reporting quater from date.
     * 
     * @param reportingQuaterFromDate
     *            the reportingQuaterFromDate to set
     */
    public void setReportingQuaterFromDate(String reportingQuaterFromDate) {
	this.reportingQuaterFromDate = reportingQuaterFromDate;
    }

    /**
     * Gets the reporting quater to date.
     * 
     * @return the reportingQuaterToDate
     */
    public String getReportingQuaterToDate() {
	return reportingQuaterToDate;
    }

    /**
     * Sets the reporting quater to date.
     * 
     * @param reportingQuaterToDate
     *            the reportingQuaterToDate to set
     */
    public void setReportingQuaterToDate(String reportingQuaterToDate) {
	this.reportingQuaterToDate = reportingQuaterToDate;
    }

    /**
     * Gets the total prime supplier sales.
     * 
     * @return the totalPrimeSupplierSales
     */
    public Double getTotalPrimeSupplierSales() {
	return totalPrimeSupplierSales;
    }

    /**
     * Sets the total prime supplier sales.
     * 
     * @param totalPrimeSupplierSales
     *            the totalPrimeSupplierSales to set
     */
    public void setTotalPrimeSupplierSales(Double totalPrimeSupplierSales) {
	this.totalPrimeSupplierSales = totalPrimeSupplierSales;
    }

    /**
     * Gets the total direct sales.
     * 
     * @return the totalDirectSales
     */
    public Double getTotalDirectSales() {
	return totalDirectSales;
    }

    /**
     * Sets the total direct sales.
     * 
     * @param totalDirectSales
     *            the totalDirectSales to set
     */
    public void setTotalDirectSales(Double totalDirectSales) {
	this.totalDirectSales = totalDirectSales;
    }

    /**
     * Gets the percentage of direct sales.
     * 
     * @return the percentageOfDirectSales
     */
    public Double getPercentageOfDirectSales() {
	return percentageOfDirectSales;
    }

    /**
     * Sets the percentage of direct sales.
     * 
     * @param percentageOfDirectSales
     *            the percentageOfDirectSales to set
     */
    public void setPercentageOfDirectSales(Double percentageOfDirectSales) {
	this.percentageOfDirectSales = percentageOfDirectSales;
    }

    /**
     * Reset all properties to their default values.
     * 
     * @param mapping
     *            The mapping used to select this instance
     * @param request
     *            The servlet request which is processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.vendor = "";
	this.vendorNumber = null;
	this.vendorContactName = "";
	this.vendorEmailAddress = "";
	this.reportingYear = null;
	this.reportingQuaterFromDate = null;
	this.reportingQuaterToDate = null;
	this.totalPrimeSupplierSales = null;
	this.totalDirectSales = null;
	this.percentageOfDirectSales = null;

    }

}
