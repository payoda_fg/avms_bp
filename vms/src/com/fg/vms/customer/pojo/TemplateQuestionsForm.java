/*
 * TemplateQuestionsForm.java
 */
package com.fg.vms.customer.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * Class to hold the form fields which associated to the templateQuestion.jsp
 * page.
 * 
 * @author srinivasarao
 * 
 */
public class TemplateQuestionsForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Holds the Questions id
     */
    private Integer id;

    /**
     * Holds the question name
     */

    private String questionDescription;
    /**
     * Holds the answer data type for example Boolean,Varchar etc.
     */

    private String answerDatatype;
    /**
     * Holds the weightage of the answer for example 22 %.
     */
    private Float answerWeightage;
    /**
     * Holds the question Order
     */
    private Integer questionOrder;
    /**
     * Holds the Template id to save the questions based on template id
     */
    private Integer templateId;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getQuestionDescription() {
	return questionDescription;
    }

    public void setQuestionDescription(String questionDescription) {
	this.questionDescription = questionDescription;
    }

    public String getAnswerDatatype() {
	return answerDatatype;
    }

    public void setAnswerDatatype(String answerDatatype) {
	this.answerDatatype = answerDatatype;
    }

    public Float getAnswerWeightage() {
	return answerWeightage;
    }

    public void setAnswerWeightage(Float answerWeightage) {
	this.answerWeightage = answerWeightage;
    }

    public Integer getQuestionOrder() {
	return questionOrder;
    }

    public void setQuestionOrder(Integer questionOrder) {
	this.questionOrder = questionOrder;
    }

    public Integer getTemplateId() {
	return templateId;
    }

    public void setTemplateId(Integer templateId) {
	this.templateId = templateId;
    }

    /**
     * Resets the form fields based after the create or update method executes.
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	this.questionDescription = "";
	this.answerWeightage = null;
	this.questionOrder = null;
    }

}
