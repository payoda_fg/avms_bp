package com.fg.vms.customer.pojo;

import java.io.Serializable;

public class Tier2ReportDirectExpensesBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String directTier2VendorName;
	private String directCertificate;
	private String directEthnicity;
	private String directSector;
	private Integer directExpence;
	public String getDirectTier2VendorName() {
		return directTier2VendorName;
	}
	public void setDirectTier2VendorName(String directTier2VendorName) {
		this.directTier2VendorName = directTier2VendorName;
	}
	public String getDirectCertificate() {
		return directCertificate;
	}
	public void setDirectCertificate(String directCertificate) {
		this.directCertificate = directCertificate;
	}
	public String getDirectEthnicity() {
		return directEthnicity;
	}
	public void setDirectEthnicity(String directEthnicity) {
		this.directEthnicity = directEthnicity;
	}
	public String getDirectSector() {
		return directSector;
	}
	public void setDirectSector(String directSector) {
		this.directSector = directSector;
	}
	public Integer getDirectExpence() {
		return directExpence;
	}
	public void setDirectExpence(Integer double1) {
		this.directExpence = double1;
	}
	
}
