/**
 * SupplierAction.java
 */
package com.fg.vms.customer.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.customer.dao.AnonymousVendorDao;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.SupplierDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.AnonymousVendorDaoImpl;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.SupplierDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.EmailHistoryDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.MeetingNotesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.dto.RFIRPFNotesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerBusinessType;
import com.fg.vms.customer.model.CustomerCertificateBusinesAreaConfig;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerLegalStructure;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.SegmentMaster;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.VendorUtil;

/**
 * Defined Supplier Action class for Supplier details like Update contact,
 * services, diverse classification, other classifications, vendor
 * classifications, vendor notes, references, documents, business biography.
 * 
 * @author vinoth
 * 
 */
public class SupplierAction extends DispatchAction {

	private Logger logger = Constants.logger;

	/**
	 * New view supplier page company details wizard type.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward showSupplierContactDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside new supplier update wizard..");

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer userId = (Integer) session.getAttribute("userId");
		Integer supplierId = Integer.parseInt(request.getParameter("id")
				.toString());
		session.setAttribute("supplierId", supplierId);
		session.setAttribute("cancelString", null);
		if (null != request.getParameter("requestString") && !request.getParameter("requestString").isEmpty()) {
			if (request.getParameter("requestString").equalsIgnoreCase("progress")) {
				session.setAttribute("cancelString", "viewVendorsStatus.do?method=showVendorSearch");
			} else if (request.getParameter("requestString").equalsIgnoreCase("criteria")) {
				session.setAttribute("cancelString", "viewVendors.do?method=showVendorSearch");
			} else if (request.getParameter("requestString").equalsIgnoreCase("prime")) {
				session.setAttribute("cancelString", "tire2home.do?method=tire2HomePage");
			} else if (request.getParameter("requestString").equalsIgnoreCase("nonprime")) {
				session.setAttribute("cancelString", "tire2home.do?method=tire2HomePage");
			} else if (request.getParameter("requestString").equalsIgnoreCase("status")) {
				session.setAttribute("cancelString", "viewVendors.do?method=showVendorStatusBySearch");
			}
		} else {
			session.setAttribute("cancelString", "viewVendors.do?method=showVendorFullSearch");
		}
		/*
		 * Get vendor details.
		 */
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		session.setAttribute("vendorInfoDto", vendorInfo);
		SupplierDao supplierDao = new SupplierDaoImpl();
		CURDDao<?> cdao1 = new CURDTemplateImpl();
		/*
		 * Get the list of menu status based on vendor id.
		 */
		List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
				.list(userDetails,
						" From CustomerVendorMenuStatus where vendorId="
								+ supplierId);
		if (null != menuStatus && menuStatus.size() > 0) {
			session.setAttribute("menuStatus", menuStatus);
		} else {
			String result = supplierDao.createMenuStatusForVendor(vendorMaster,
					userId, userDetails);
			if (null != result && !result.isEmpty()
					&& result.equalsIgnoreCase("success")) {
				/*
				 * Get the list of menu status based on vendor id.
				 */
				List<CustomerVendorMenuStatus> menuStatus1 = (List<CustomerVendorMenuStatus>) cdao1
						.list(userDetails,
								" From CustomerVendorMenuStatus where vendorId="
										+ supplierId);
				session.setAttribute("menuStatus", menuStatus1);
			}
		}
		if (vendorMaster.getVendorStatus() != null
				&& !vendorMaster.getVendorStatus().isEmpty()) {
			CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
			List<StatusMaster> statusMasters = curdDao
					.list(userDetails,
							" From StatusMaster s where s.disporder >= ( select disporder from StatusMaster where id='"
									+ vendorMaster.getVendorStatus()
									+ "' ) order by s.disporder");
			vendorForm.setStatusMasters(statusMasters);
		}
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				supplierId, vendorMaster, vendorCertificate, userDetails);
		session.setAttribute("vendorName", vendorMaster.getVendorName());
		session.setAttribute("vendorStatus", vendorMaster.getVendorStatus());
		/*
		 * To make secret questions list available in edit vendor wizard.
		 */
		LoginDao login = new LoginDaoImpl();
		List<SecretQuestion> secretQuestionList = login
				.getSecurityQuestions(userDetails);
		session.setAttribute("secretQnsList", secretQuestionList);
		
		int workflowBpInfo = userDetails.getWorkflowConfiguration().getBpSegment();
		StatusMaster statusMaster = userDetails.getWorkflowConfiguration().getStatus();
		if(statusMaster != null && workflowBpInfo != 0)
		{
			CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
			List<StatusMaster> statusMasters =curdDao.list(userDetails," From StatusMaster s where s.disporder>= "+statusMaster.getDisporder()+" order by s.statusname");
			session.setAttribute("statusMastersList",statusMasters);
			CURDDao<SegmentMaster> curdDao1 = new CURDTemplateImpl<SegmentMaster>();
			List<SegmentMaster> segmentMasters = curdDao1.list(userDetails," From SegmentMaster order by segmentName");
			session.setAttribute("bpSegmentList", segmentMasters);
		}
		else
		{
			session.setAttribute("statusMastersList",null);
		}
		return mapping.findForward("showcontactinfo");
	}

	/**
	 * Save company information and move to address navigation page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveCompanyInformation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String result = null;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		String submitType = request.getParameter("submitType");
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		Integer userId = (Integer) session.getAttribute("userId");
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeVendorInformation(vendorForm,
					userId, supplierId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {			
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			}  
			else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.company.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			CURDDao<?> cdao = new CURDTemplateImpl();
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorCertificate> vendorCertificate = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(supplierId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				vendorCertificate = vendorInfo.getVendorCertificate();
				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, supplierId, vendorMaster,
						vendorCertificate, userDetails);
				vendorForm.setCompanytype(vendorMaster.getCompanyType());
				List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
						.list(userDetails,
								"From CustomerBusinessType where isactive=1 order by typeName");
				List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
						.list(userDetails,
								"From CustomerLegalStructure where isactive=1 order by name");
				vendorForm.setLegalStructures(legalStructures);
				vendorForm.setBusinessTypes(businessTypes);
				if (vendorMaster.getVendorStatus() != null
						&& !vendorMaster.getVendorStatus().isEmpty()) {
					CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
					List<StatusMaster> statusMasters = curdDao
							.list(userDetails,
									" From StatusMaster s where s.disporder >= ( select disporder from StatusMaster where id='"
											+ vendorMaster.getVendorStatus()
											+ "' ) order by s.disporder");
					vendorForm.setStatusMasters(statusMasters);
				}
				return mapping.findForward("savevendorinfo");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
				
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}		
				List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<Country> country = (List<Country>) dao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}		
				vendorForm.setPhyStates1(statesList);
				vendorForm.setMailingStates2(statesList);
				
				CustomerVendorAddressMaster phyAddress = documentsDao
						.getMailingAddress(supplierId, "p", userDetails);
				if (phyAddress != null) {
					vendorForm.setAddress1(phyAddress.getAddress());
					vendorForm.setCity(phyAddress.getCity());
					vendorForm.setState(phyAddress.getState());
					vendorForm.setProvince(phyAddress.getProvince());
					vendorForm.setRegion(phyAddress.getRegion());
					vendorForm.setCountry(phyAddress.getCountry());
					vendorForm.setZipcode(phyAddress.getZipCode());
					vendorForm.setMobile(phyAddress.getMobile());
					vendorForm.setPhone(phyAddress.getPhone());
					vendorForm.setFax(phyAddress.getFax());
					if (null != phyAddress.getCountry() && !phyAddress.getCountry().isEmpty()) {
						List<State> phyStates = null;				
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								phyStates = dao.list(userDetails, " From State where countryid=" + phyAddress.getCountry() + " and isactive=1 order by statename");
							}
						}
						vendorForm.setPhyStates1(phyStates);
					}
				}
				CustomerVendorAddressMaster addressMaster = documentsDao
						.getMailingAddress(supplierId, "m", userDetails);
				if (addressMaster != null) {
					vendorForm.setAddress2(addressMaster.getAddress());
					vendorForm.setCity2(addressMaster.getCity());
					vendorForm.setState2(addressMaster.getState());
					vendorForm.setProvince2(addressMaster.getProvince());
					vendorForm.setRegion2(addressMaster.getRegion());
					vendorForm.setCountry2(addressMaster.getCountry());
					vendorForm.setZipcode2(addressMaster.getZipCode());
					vendorForm.setMobile2(addressMaster.getMobile());
					vendorForm.setPhone2(addressMaster.getPhone());
					vendorForm.setFax2(addressMaster.getFax());
					if (null != addressMaster.getCountry() && !addressMaster.getCountry().isEmpty()) {
						List<State> mStates = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								mStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								mStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
							}
						}
						vendorForm.setMailingStates2(mStates);
					}
				}				
				vendorForm = SupplierNavigationAction.getContactInformation(
						vendorForm, userDetails, supplierId);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		return mapping.findForward("savecompanyinfo");
	}

	/**
	 * Save supplier address information and move to company owner page
	 * information.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveAddressInformation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CURDDao<?> cdao1 = new CURDTemplateImpl();
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer userId = (Integer) session.getAttribute("userId");
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			Integer countryId = null;
			List<Country> country = (List<Country>) cdao1.list(userDetails,"From Country where isDefault = 1");
			countryId = country.get(0).getId();
			vendorForm.setCountry2(countryId.toString());
			result = anonymousVendorDao.mergeVendorAddress(vendorForm, userId,
					supplierId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.address.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);			
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
			CURDDao<State> dao = new CURDTemplateImpl<State>();
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}				
				List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}				
				vendorForm.setPhyStates1(statesList);
				vendorForm.setMailingStates2(statesList);
				
				CustomerVendorAddressMaster phyAddress = documentsDao
						.getMailingAddress(supplierId, "p", userDetails);
				if (phyAddress != null) {
					vendorForm.setAddress1(phyAddress.getAddress());
					vendorForm.setCity(phyAddress.getCity());
					vendorForm.setState(phyAddress.getState());
					vendorForm.setProvince(phyAddress.getProvince());
					vendorForm.setRegion(phyAddress.getRegion());
					vendorForm.setCountry(phyAddress.getCountry());
					vendorForm.setZipcode(phyAddress.getZipCode());
					vendorForm.setMobile(phyAddress.getMobile());
					vendorForm.setPhone(phyAddress.getPhone());
					vendorForm.setFax(phyAddress.getFax());
					if (null != phyAddress.getCountry() && !phyAddress.getCountry().isEmpty()) {
						List<State> phyStates = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								phyStates = dao.list(userDetails, " From State where countryid=" + phyAddress.getCountry() + " and isactive=1 order by statename");
							}
						}
						vendorForm.setPhyStates1(phyStates);
					}
				}
				CustomerVendorAddressMaster addressMaster = documentsDao
						.getMailingAddress(supplierId, "m", userDetails);
				if (addressMaster != null) {
					vendorForm.setAddress2(addressMaster.getAddress());
					vendorForm.setCity2(addressMaster.getCity());
					vendorForm.setState2(addressMaster.getState());
					vendorForm.setProvince2(addressMaster.getProvince());
					vendorForm.setRegion2(addressMaster.getRegion());
					vendorForm.setCountry2(addressMaster.getCountry());
					vendorForm.setZipcode2(addressMaster.getZipCode());
					vendorForm.setMobile2(addressMaster.getMobile());
					vendorForm.setPhone2(addressMaster.getPhone());
					vendorForm.setFax2(addressMaster.getFax());
					if (null != addressMaster.getCountry() && !addressMaster.getCountry().isEmpty()) {
						List<State> mStates = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								mStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								mStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
							}
						}
						vendorForm.setMailingStates2(mStates);
					}
				}
				return mapping.findForward("saveaddress");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
				if (null != vendorForm.getCompanyOwnership()
						&& !vendorForm.getCompanyOwnership().isEmpty()) {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(vendorForm
							.getCompanyOwnership());
				} else {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(null);
				}
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("saveaddressinfo");
	}

	/**
	 * Save supplier owner information and move to company diverse
	 * classification page information.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveOwnerInformation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();

		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeVendorOwnerInfo(vendorForm,
					userId, supplierId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			}else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			}else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.owner.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			/*
			 * Load vendor information
			 */
			VendorDao vendorDao = new VendorDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					supplierId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
				if (null != vendorForm.getCompanyOwnership()
						&& !vendorForm.getCompanyOwnership().isEmpty()) {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(vendorForm
							.getCompanyOwnership());
				} else {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(null);
				}
				return mapping.findForward("saveowner");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				List<Certificate> certificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				certificates = certificate.listOfCertificatesByType((byte) 1,
						userDetails);
				session.setAttribute("certificateTypes", certificates);
				// Get vendor diverse classification list
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				// Certificate Type
				CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
				List<CustomerCertificateType> certificateTypesList = curdDao2
						.list(userDetails, " from CustomerCertificateType ");
				session.setAttribute("certificateTypesList",
						certificateTypesList);
				// certificate agencies list.
				SearchDao searchDao = new SearchDaoImpl();
				List<CertifyingAgency> certificateAgencies;
				certificateAgencies = searchDao.listAgencies(userDetails);
				session.setAttribute("certAgencyList", certificateAgencies);
				// ethnicities list
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("saveownerinfo");
	}

	/**
	 * Save partial diverse classification data from vendor edit page wizard
	 * type.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveDiverseClassification(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		List<DiverseCertificationTypesDto> diverseCertifications = new ArrayList<DiverseCertificationTypesDto>();
		if (getDeverseSupplier(vendorForm.getDeverseSupplier()) == (byte) 1) {
			diverseCertifications = CommonUtils.packageDiverseTypes(vendorForm);
		}
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao
					.mergeDiverseClassification(vendorForm, userId, supplierId,
							userDetails, url, diverseCertifications);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			}else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.diverse.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.reset(mapping, request);
			/**
			 * Load vendor data
			 */
			VendorDao vendorDao = new VendorDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					supplierId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			vendorForm.setVendorStatus(vendorMaster.getVendorStatus());
			vendorForm.setId(supplierId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			
			/*
			 * List of Vendor Keywords.
			 */
			VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
			List<CustomerVendorKeywords> keywords = null;
			keywords = documentsDao.getVendorKeywords(supplierId, userDetails);
			session.setAttribute("vendorKeywords", keywords);
			
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				session.setAttribute("diverseClassification", null);
				// get the list of certificates by diverse.
				List<Certificate> certificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				certificates = certificate.listOfCertificatesByType((byte) 1,
						userDetails);
				session.setAttribute("certificateTypes", certificates);
				// Get vendor diverse classification list
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				// Certificate Type
				CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
				List<CustomerCertificateType> certificateTypesList = curdDao2
						.list(userDetails, " from CustomerCertificateType ");
				session.setAttribute("certificateTypesList",
						certificateTypesList);
				// certificate agencies list.
				SearchDao searchDao = new SearchDaoImpl();
				List<CertifyingAgency> certificateAgencies;
				certificateAgencies = searchDao.listAgencies(userDetails);
				session.setAttribute("certAgencyList", certificateAgencies);
				// ethnicities list
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));			
				
				return mapping.findForward("savediverseclassification");
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitdiverseclassification");
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private Byte getDeverseSupplier(String string) {

		if (string != null && string.length() != 0) {
			if (string.equalsIgnoreCase("on")) {
				return (byte) 1;
			} else {
				return (byte) 0;
			}
		} else {
			return (byte) 0;
		}

	}

	/**
	 * Save partial service area data from vendor edit page wizard type.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveServiceArea(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		List<NaicsCodesDto> nacisCodes = new ArrayList<NaicsCodesDto>();
		nacisCodes = CommonUtils.packageNiacsCodes(vendorForm);

		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeServiceArea(vendorForm, userId,
					supplierId, userDetails, url, nacisCodes);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.service.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			/**
			 * Load vendor data
			 */
			vendorForm.reset(mapping, request);
			VendorDao vendorDao = new VendorDaoImpl();
			CertificateDaoImpl certificate = new CertificateDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					supplierId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			vendorForm.setVendorStatus(vendorMaster.getVendorStatus());

			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				/*
				 * List of Vendor Keywords.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				List<CustomerVendorKeywords> keywords = null;
				keywords = documentsDao.getVendorKeywords(supplierId, userDetails);
				session.setAttribute("vendorKeywords", keywords);
				return mapping.findForward("saveservicearea");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
				List<CustomerVendorCommodity> vendorCommodities = null;
				// Show the list of Vendor Commodities.
				vendorCommodities = documentsDao.getVendorCommodities(
						supplierId, userDetails);
				List<CommodityDto> commodities=documentsDao.listVendorCommodities(supplierId, userDetails);
				session.setAttribute("vendorCommodities", commodities);
//				session.setAttribute("vendorCommodities", vendorCommodities);

				List<GeographicalStateRegionDto> serviceAreas1 = null;
				serviceAreas1 = regionDao.getServiceAreasList(userDetails);
				session.setAttribute("serviceAreas1", serviceAreas1);

				CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
				List<CustomerServiceArea> serviceAreaList = curdDao.list(
						userDetails, " from CustomerServiceArea ");
				session.setAttribute("serviceAreaList", serviceAreaList);
				/*
				 * Get the selected vendor service and business area.
				 */
				CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
				List<VendorBusinessArea> businessAreas = curdDao1.list(
						userDetails,
						"from VendorBusinessArea where isActive = 1 and vendorId="
								+ supplierId);
				CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
				List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
						userDetails,
						"from CustomerVendorServiceArea where isActive = 1 and vendorId="
								+ supplierId);
				// Get vendor service area details
				vendorForm = VendorUtil.packVendorServiceAreas(vendorForm,
						serviceAreas, supplierId);
				// Get vendor business area details
				vendorForm = VendorUtil.packVendorBusinessAreas(vendorForm,
						businessAreas, supplierId);

				/*
				 * Get the list of certificate business area configuration for
				 * apply business rules in business area selection.
				 */
				CURDDao<CustomerCertificateBusinesAreaConfig> dao = new CURDTemplateImpl<CustomerCertificateBusinesAreaConfig>();
				List<CustomerCertificateBusinesAreaConfig> businessAreaconfig = dao
						.list(userDetails,
								"from CustomerCertificateBusinesAreaConfig where isActive = 1");
				session.setAttribute("businessAreaconfig", businessAreaconfig);

				/*
				 * Diverse certificate list of vendor
				 */
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				
				//To Get BP Market Sector List
				CommodityDaoImpl commodityDao=new CommodityDaoImpl();
				String query = "From MarketSector where isActive=1 order by sectorDescription";
				List<MarketSector> marketSectors = commodityDao.marketSectorAndCommodities(userDetails, query);
				if(marketSectors != null) {
					session.setAttribute("marketsectors", marketSectors);
				}
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitservicearea");
	}

	/**
	 * Save partial business area and commodity data from new self registration
	 * page wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveBusinessArea(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();

		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeBusinessArea(vendorForm, userId,
					supplierId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.business.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			/**
			 * Load vendor data
			 */
			VendorDao vendorDao = new VendorDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					supplierId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();			
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			vendorForm.setId(supplierId);
			vendorForm.setVendorStatus(vendorMaster.getVendorStatus());

			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
				List<CustomerVendorCommodity> vendorCommodities = null;
				// Show the list of Vendor Commodities.
				vendorCommodities = documentsDao.getVendorCommodities(
						supplierId, userDetails);
				List<CommodityDto> commodities=documentsDao.listVendorCommodities(supplierId, userDetails);
				session.setAttribute("vendorCommodities", commodities);
//				session.setAttribute("vendorCommodities", vendorCommodities);
				/*
				 * Show list of service areas.
				 */
				List<GeographicalStateRegionDto> serviceAreas1 = null;
				serviceAreas1 = regionDao.getServiceAreasList(userDetails);
				session.setAttribute("serviceAreas1", serviceAreas1);
				/*
				 * Show list of business areas.
				 */
				CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
				List<CustomerServiceArea> serviceAreaList = curdDao.list(
						userDetails, " from CustomerServiceArea ");
				session.setAttribute("serviceAreaList", serviceAreaList);

				/*
				 * Get the selected vendor service and business area.
				 */
				CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
				List<VendorBusinessArea> businessAreas = curdDao1.list(
						userDetails,
						"from VendorBusinessArea where isActive = 1 and vendorId="
								+ supplierId);
				CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
				List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
						userDetails,
						"from CustomerVendorServiceArea where isActive = 1 and vendorId="
								+ supplierId);
				// Get vendor service area details
				vendorForm = VendorUtil.packVendorServiceAreas(vendorForm,
						serviceAreas, supplierId);
				// Get vendor business area details
				vendorForm = VendorUtil.packVendorBusinessAreas(vendorForm,
						businessAreas, supplierId);

				/*
				 * Get the list of certificate business area configuration for
				 * apply business rules in business area selection.
				 */
				CURDDao<CustomerCertificateBusinesAreaConfig> dao = new CURDTemplateImpl<CustomerCertificateBusinesAreaConfig>();
				List<CustomerCertificateBusinesAreaConfig> businessAreaconfig = dao
						.list(userDetails,
								"from CustomerCertificateBusinesAreaConfig where isActive = 1");
				session.setAttribute("businessAreaconfig", businessAreaconfig);

				/*
				 * Diverse certificate list of vendor
				 */
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				
				//To Get BP Market Sector List
				CommodityDaoImpl commodityDao=new CommodityDaoImpl();
				String query = "From MarketSector where isActive=1 order by sectorDescription";
				List<MarketSector> marketSectors = commodityDao.marketSectorAndCommodities(userDetails, query);
				if(marketSectors != null) {
					session.setAttribute("marketsectors", marketSectors);
				}

				return mapping.findForward("savebusinessarea");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				vendorForm = VendorUtil.packBioGraphySafety(vendorForm,
						supplierId, userDetails);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitbusinessarea");
	}

	/**
	 * Save business biography and safety.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveBusinessBiography(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeBusinessBiography(vendorForm,
					userId, supplierId, userDetails);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.biography.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.reset(mapping, request);
			vendorForm.setId(supplierId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				vendorForm = VendorUtil.packBioGraphySafety(vendorForm,
						supplierId, userDetails);
				return mapping.findForward("savebusinessbiography");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
				
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}
				List<com.fg.vms.customer.model.Country> countries = dao.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<Country> country = (List<Country>) dao.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) cdao1.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}				
				vendorForm.setRefStates1(statesList);
				vendorForm.setRefStates2(statesList);
				vendorForm.setRefStates3(statesList);
				
				VendorDao vendorDao = new VendorDaoImpl();
				List<CustomerVendorreference> vendorreferences = vendorDao
						.getListReferences(supplierId, userDetails);
				vendorForm = VendorUtil.packVendorReference(vendorForm,	vendorreferences, userDetails, countryId);				
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitbusinessbiography");
	}

	/**
	 * Save References data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveReferences(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeReferences(vendorForm, userId,
					supplierId, userDetails);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.reference.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.setId(supplierId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}				
				List<com.fg.vms.customer.model.Country> countries = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) cdao1.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}
				vendorForm.setRefStates1(statesList);
				vendorForm.setRefStates2(statesList);
				vendorForm.setRefStates3(statesList);
				
				VendorDao vendorDao = new VendorDaoImpl();
				List<CustomerVendorreference> vendorreferences = vendorDao
						.getListReferences(supplierId, userDetails);
				vendorForm = VendorUtil.packVendorReference(vendorForm,
						vendorreferences, userDetails, countryId);
				
				return mapping.findForward("savereferences");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorOtherCertificate> otherCertificate = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(supplierId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				otherCertificate = vendorInfo.getVendorOtherCert();
				session.setAttribute("vendorInfoDto", vendorInfo);
				session.setAttribute("otherCertificates", otherCertificate);
				List<Certificate> qualityCertificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				qualityCertificates = certificate.listOfCertificatesByType(
						(byte) 0, userDetails);
				session.setAttribute("qualityCertificates", qualityCertificates);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitreferences");
	}

	/**
	 * Save other certificates data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveOtherCertificates(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		String netWorldResult =null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeOtherCertificate(vendorForm,
					userId, supplierId, userDetails);
			netWorldResult =anonymousVendorDao.addVendorNetWorldCertificate(vendorForm, 
					userId, supplierId, userDetails);
				
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)&&( "success".equalsIgnoreCase(netWorldResult) || "saved".equalsIgnoreCase(netWorldResult))) {
			ActionMessages messages = new ActionMessages();
			ActionMessage netmsg=null;
			ActionMessage msg = new ActionMessage("vendor.quality.save.ok");
			if("success".equalsIgnoreCase(netWorldResult)){
			netmsg =new ActionMessage("netwotld.save.ok");
			}else{
			netmsg =new ActionMessage("netwotld.saved.ok");
			}
			messages.add("vendor", msg);
			messages.add("networld",netmsg);
			saveMessages(request, messages);
			vendorForm.setId(supplierId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorOtherCertificate> otherCertificate = null;
				String networldcertificate =null;
				Boolean hasnetworldcsrtificate=false;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(supplierId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				otherCertificate = vendorInfo.getVendorOtherCert();
				networldcertificate=vendorInfo.getIsNetWorldCertificate();
				hasnetworldcsrtificate=vendorInfo.getHasNetWoldcert();
				List<VendorCertificate> vendorCetrificate = null;

				vendorMaster = vendorInfo.getVendorMaster();
				vendorCetrificate = vendorInfo.getVendorCertificate();

				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, supplierId, vendorMaster,
						vendorCetrificate, userDetails);
				session.setAttribute("vendorInfoDto", vendorInfo);
				session.setAttribute("otherCertificates", otherCertificate);
				if(hasnetworldcsrtificate){
					session.setAttribute("networldcertificate", networldcertificate);
					session.setAttribute("hasnetworldcsrtificate", hasnetworldcsrtificate);
					}
				List<Certificate> qualityCertificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				qualityCertificates = certificate.listOfCertificatesByType(
						(byte) 0, userDetails);
				session.setAttribute("qualityCertificates", qualityCertificates);
				return mapping.findForward("saveothercertificate");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				vendorForm.setId(supplierId);
				/*
				 * List of vendor documents.
				 */
//				List<VendorDocuments> documents = null;
//				documents = documentsDao.getVendorDocuments(supplierId,
//						userDetails);
				List<VendorDocuments> documents = new ArrayList<VendorDocuments>();
				List<VendorDocuments> vendorDocuments = null;
				vendorDocuments = documentsDao.getVendorDocuments(supplierId, userDetails);
				for(VendorDocuments vendorDocument:vendorDocuments){
					String docSize=humanReadableByteCount(vendorDocument.getDocumentFilesize(),true);
					vendorDocument.setDocumentSize(docSize);
					documents.add(vendorDocument);
				}
				session.setAttribute("vendorDocuments", documents);
				WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration configuration = configurationDao
						.listWorkflowConfig(userDetails);
				vendorForm.setConfiguration(configuration);
				int uploadRestriction = configuration.getDocumentUpload();				
				session.setAttribute("uploadRestriction", uploadRestriction);
				int documentMaxSize = configuration.getDocumentSize();
				session.setAttribute("documentMaxSize", documentMaxSize);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}else if(netWorldResult.equals("failure")){
			ActionMessages messages = new ActionMessages();
			ActionMessage netmsg = new ActionMessage("transaction.failure");
			messages.add("networldfailure", netmsg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitothercertificate");
	}

	/**
	 * Save vendor documents.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveDocuments(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		if (supplierId != null && supplierId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeDocuments(vendorForm, userId,
					supplierId, userDetails);
		}
		
		VendorDao vendorDao1 = new VendorDaoImpl();
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + supplierId + " ORDER BY v.CREATEDON DESC";
		List<MeetingNotesDto> meetingList = vendorDao1.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		
		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + supplierId + " ORDER BY v.CREATEDON DESC";
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao1.setRfiMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setRfiNotesList(rfiMeetingList);
		
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.document.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.setId(supplierId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				vendorForm.setId(supplierId);
				/*
				 * List of vendor documents.
				 */
//				List<VendorDocuments> documents = null;
//				documents = documentsDao.getVendorDocuments(supplierId,
//						userDetails);
				List<VendorDocuments> documents = new ArrayList<VendorDocuments>();
				List<VendorDocuments> vendorDocuments = null;
				vendorDocuments = documentsDao.getVendorDocuments(supplierId, userDetails);
				for(VendorDocuments vendorDocument:vendorDocuments){
					String docSize=humanReadableByteCount(vendorDocument.getDocumentFilesize(),true);
					vendorDocument.setDocumentSize(docSize);
					documents.add(vendorDocument);
				}
				session.setAttribute("vendorDocuments", documents);
				WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration configuration = configurationDao
						.listWorkflowConfig(userDetails);
				vendorForm.setConfiguration(configuration);
				int uploadRestriction = configuration.getDocumentUpload();
				session.setAttribute("uploadRestriction", uploadRestriction);
				int documentMaxSize = configuration.getDocumentSize();
				session.setAttribute("documentMaxSize", documentMaxSize);
				return mapping.findForward("savedocuments");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(supplierId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
//				vendorForm.setVendorNotes(vendorMaster.getVendorNotes());
				CURDDao<CustomerVendorMeetingInfo> curdDao = new CURDTemplateImpl<CustomerVendorMeetingInfo>();
				List<CustomerVendorMeetingInfo> vendorMeetingInfo = curdDao
						.list(userDetails,
								"from CustomerVendorMeetingInfo where vendorId="
										+ supplierId);
				if (null != vendorMeetingInfo && vendorMeetingInfo.size() > 0) {
					CustomerVendorMeetingInfo meetingInfo = vendorMeetingInfo
							.get(0);
					VendorUtil.packVendorContactMeetingInfomation(vendorForm,
							meetingInfo);
				}
				CURDDao<State> dao = new CURDTemplateImpl<State>();
				List<State> contactStates = dao.list(userDetails,
						" From State order by statename");
				session.setAttribute("contactStates", contactStates);
				
				//Starting Code To Retrieve Email History.
				StringBuilder vendorEmailIdList = new StringBuilder();		
				List<EmailHistoryDto> emailHistoryList = null;
				
				for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
				{
					if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
					{
						if(i == 0)
							vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
						else
							vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
					}
				}		
				
				if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
					emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
				
				vendorForm.setEmailHistoryList(emailHistoryList);
				//Ending Code To Retrieve Email History.
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("hugeFileSize")) {
			// if File Size Higher than Specified Limit, return to input page with message
			int documentMaxSize=0;
			if(session.getAttribute("documentMaxSize") != null)
			{
				documentMaxSize=(Integer) session.getAttribute("documentMaxSize");
			}				
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.hugeFileSize", documentMaxSize);
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitdocuments");
	}
	
	public static String humanReadableByteCount(double bytes, boolean si) {
	    int unit = si ? 1024 : 1000;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
	    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	/**
	 * Save vendor contact meeting information.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveMeetingInfo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result = null;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		vendorForm.setVendorNotesId(null);
		VendorDao vendorDao = new VendorDaoImpl();
		String submitType = request.getParameter("submitType");
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}		
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + supplierId + " ORDER BY v.CREATEDON DESC";
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		
		if (supplierId != null && supplierId != 0) {
			result = vendorDao.saveMeetingDetails(userDetails, vendorForm,
					supplierId);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorSearch")) {
				return mapping.findForward("logout");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendorsStatus.do?method=showVendorSearch")) {
				return mapping.findForward("logouttoprogress");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorFullSearch")) {
				return mapping.findForward("logouttofulltextsearch");
			} else if (null != session.getAttribute("cancelString")
					&& session.getAttribute("cancelString").equals(
							"viewVendors.do?method=showVendorStatusBySearch")) {
				return mapping.findForward("logoutAllStatusSearch");
			} else {
				return mapping.findForward("homepage");
			}
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.setId(supplierId);
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					supplierId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
//			vendorForm.setVendorNotes(vendorMaster.getVendorNotes());
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ supplierId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, supplierId);
			vendorForm.setVendorNotes(null);
			
			//Starting Code To Retrieve Email History.
			StringBuilder vendorEmailIdList = new StringBuilder();		
			List<EmailHistoryDto> emailHistoryList = null;
			
			for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
			{
				if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
				{
					if(i == 0)
						vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
					else
						vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				}
			}		
			
			if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
				emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
			
			vendorForm.setEmailHistoryList(emailHistoryList);
			//Ending Code To Retrieve Email History.
			
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				return mapping.findForward("meetinginfo");
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitmeetinginfo");
	}

	public ActionForward editPrimeVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Inside new supplier update wizard..");
		Integer supplierId = null;
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;

		if (request.getParameter("id") != null) {
			supplierId = Integer
					.parseInt(request.getParameter("id").toString());
		} else {
			supplierId = Integer.parseInt(session.getAttribute("supplierId")
					.toString());
		}

		session.setAttribute("supplierId", supplierId);
		session.setAttribute("cancelString", null);
		/*
		 * To make secret questions list available in edit vendor wizard.
		 */
		SupplierDao supplierDao = new SupplierDaoImpl();

		VendorMaster vendorMaster = null;
		VendorContact vendorContact = null;
		CustomerVendorMeetingInfo meetingInfo = null;
		CustomerVendorAddressMaster addressMaster = null;		
		CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		
		String query = "From Country where isactive=1 order by countryname";
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}		
		List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
		session.setAttribute("countryList", countries);
		
		Integer countryId = null;
		List<Country> country = (List<Country>) dao1.list(userDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();		

		List<State> statesList = null;
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
			}
		}		
		vendorForm.setPhyStates1(statesList);
		
		vendorMaster = supplierDao.retreveVendorMaster(supplierId, userDetails);

		vendorContact = supplierDao.retreveVendorContact(supplierId,
				userDetails);

		meetingInfo = supplierDao.retreveVendorMeetingInfo(supplierId,
				userDetails);

		addressMaster = supplierDao.retreveAddressMaster(supplierId,
				userDetails);

		if (vendorMaster != null) {
			vendorForm.setId(vendorMaster.getId());
			vendorForm.setVendorName(vendorMaster.getVendorName());
			vendorForm.setEmailId(vendorMaster.getEmailId());
			if (vendorMaster.getCustomerDivisionId() != null) {
				vendorForm.setCustomerDivision(vendorMaster
						.getCustomerDivisionId().getId());
			}
			vendorForm.setVendorStatus(vendorMaster.getVendorStatus());
		}

		if (addressMaster != null) {

			if ((addressMaster.getAddress() != null)) {
				vendorForm.setAddress1(addressMaster.getAddress());
			}
			vendorForm.setCity(addressMaster.getCity());
			vendorForm.setCountry(addressMaster.getCountry());
			vendorForm.setState(addressMaster.getState());
			vendorForm.setState2(addressMaster.getState());
			vendorForm.setZipcode(addressMaster.getZipCode());
			vendorForm.setPhone(addressMaster.getPhone());
			vendorForm.setFax(addressMaster.getFax());
						
			if(null != addressMaster.getCountry()) {
				List<State> phyStates = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
					} else {
						phyStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
					}
				}
				vendorForm.setPhyStates1(phyStates);
			}			
		}

		if (vendorContact != null) {
			vendorForm.setFirstName(vendorContact.getFirstName());
			vendorForm.setDesignation(vendorContact.getDesignation());
			vendorForm.setContactPhone(vendorContact.getPhoneNumber());
			vendorForm.setContanctEmail(vendorContact.getEmailId());
			vendorForm.setGender(vendorContact.getGender());
		}

		if (meetingInfo != null) {
			vendorForm.setContactFirstName(meetingInfo.getContactFirstName());
			vendorForm.setContactLastName(meetingInfo.getContactLastName());
			if (meetingInfo.getContactDate() != null) {
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				vendorForm.setContactDate(format.format(meetingInfo
						.getContactDate()));
			}
			vendorForm.setContactState(meetingInfo.getContactState());
			vendorForm.setContactEvent(meetingInfo.getContactEvent());
		}		
		return mapping.findForward("primeVendor");
	}
	
	/**
	 * To Save / Update PICS Certificate.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward savePicsCertificate(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.info("In savePicsCertificate");
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		
		String result = null;
		Integer userId = (Integer) session.getAttribute("userId");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer supplierId = null;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		if (supplierId != null && supplierId != 0) {			
			result = anonymousVendorDao.savePicsCertificate(vendorForm, userId, supplierId, userDetails);
		}
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result",result);
		response.getWriter().println(jsonObject);
		return null;
	}
}
