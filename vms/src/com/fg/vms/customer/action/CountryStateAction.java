/**
 * CountryStateAction.java
 */
package com.fg.vms.customer.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.customer.dao.CountryStateDao;
import com.fg.vms.customer.dao.impl.CountryStateDaoImpl;
import com.fg.vms.customer.dto.CountryStateDto;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.pojo.CountryStateForm;
import com.fg.vms.util.Constants;

/**
 * Defined CountryStateAction class for Country and State details like region,
 * state
 * 
 * @author asarudeen
 * 
 */
/**
 * @author Kaleswararao
 *
 */
public class CountryStateAction extends DispatchAction 
{
	/** Logger of the system. */
	private Logger logger = Constants.logger;
	
	/**
	 * View CountryState settings page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showCountryStateSettingsPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to view the CountryState settings page..");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		CountryStateForm countryStateForm = (CountryStateForm) form;

		CountryStateDao countryDao = new CountryStateDaoImpl();
		
		List<State> states = countryDao.listStates(appDetails);
		countryStateForm.setStates(states);
		session.setAttribute("stateList1", states);		

		List<Country> countries = countryDao.listCountries(appDetails);
		countryStateForm.setCountries(countries);
		session.setAttribute("country1", countries);
		
		return mapping.findForward("viewsuccess");
	}
	
	/**
	 * Save Country.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveCountry(ActionMapping mapping,	ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to save Country...");
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");		
		CountryStateForm countryStateForm=(CountryStateForm) form;		
		CountryStateDao countryDao = new CountryStateDaoImpl();
		// Save Country.
		countryDao.saveCountry(userId, countryStateForm, appDetails);
		return mapping.findForward("save");
	}
	
	/**
	 * Delete Country.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteCountry(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Delete Country...");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");		
		Integer userId = (Integer) session.getAttribute("userId");		
		Integer countryId = Integer.parseInt(request.getParameter("id"));
		CountryStateDao countryDao = new CountryStateDaoImpl();
		countryDao.deleteCountry(countryId, userId, appDetails);
		return mapping.findForward("delete");
	}
	
	/**
	 * Delete Country Checking.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteCountryCheck(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Delete Country Check...");
		List<Country> country = null;
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");		
		Integer userId = (Integer) session.getAttribute("userId");		
		Integer countryId = Integer.parseInt(request.getParameter("id"));
		CountryStateDao countryDao = new CountryStateDaoImpl();
		country=countryDao.deleteCountryCheck(countryId, userId, appDetails);
		if(country != null  && country.size() != 0)
		{
			return mapping.findForward("checkDeleteNotSuccess");			
		}
		else
		{
			return mapping.findForward("checkDeleteSuccess");
		}		
	}
	
	/**
	 * Delete State Checking.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteStateCheck(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Delete State Check...");
		List<State> state = null;
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");		
		Integer userId = (Integer) session.getAttribute("userId");		
		Integer stateId = Integer.parseInt(request.getParameter("id"));
		CountryStateDao countryDao = new CountryStateDaoImpl();
		state=countryDao.deleteStateCheck(stateId, userId, appDetails);
		if(state != null  && state.size() != 0)
		{
			return mapping.findForward("checkDeleteNotSuccess");			
		}
		else
		{
			return mapping.findForward("checkDeleteSuccess");
		}		
	}

	/**
	 * Retrieve Countries.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrieveCountry(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Retrieve Countries...");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");		
		Integer countryId = Integer.parseInt(request.getParameter("id"));
		CountryStateDao countryDao = new CountryStateDaoImpl();		
		List<Country> countries = countryDao.retrieveCountries(countryId, appDetails);
		session.setAttribute("countries", countries);		
		return mapping.findForward("retrieve");
	}

	/**
	 * Save State.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveState(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Save State...");		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		CountryStateForm countryStateForm=(CountryStateForm) form;
		CountryStateDao countryDao = new CountryStateDaoImpl();
		// Save State.		
		countryDao.saveState(userId, countryStateForm, appDetails);
		return mapping.findForward("save");
	}

	/**
	 * Delete State.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteState(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Delete State...");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer stateId = Integer.parseInt(request.getParameter("id"));
		Integer userId = (Integer) session.getAttribute("userId");
		CountryStateDao countryDao = new CountryStateDaoImpl();
		countryDao.deleteState(stateId, userId, appDetails);
		return mapping.findForward("deleteState");
	}

	/**
	 * Retrieve State.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrieveState(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("Method to Retrieve State...");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer stateId = Integer.parseInt(request.getParameter("id"));
		CountryStateDao countryDao = new CountryStateDaoImpl();
		List<State> states = countryDao.retrieveState(stateId, appDetails);
		if (states != null && states.size() != 0) 
		{
			session.setAttribute("bizStates", states);
		} /*else {
			State state = regionDao.getStateName(areaId, appDetails);
			session.setAttribute("stateSesObj", state);
		}*/
		return mapping.findForward("retrieveState");
	}

	/**
	 * List the State by Country.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward listStateByCountry(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		CountryStateDao countryDao = new CountryStateDaoImpl();
		List<CountryStateDto> countryStateDto = null;
		countryStateDto = countryDao.getStateByCountryList(appDetails);
		session.setAttribute("countryStateDto", countryStateDto);
		
		return mapping.findForward("listStates");
	}
	
	/*
	 * Retrieve State By Selected Country.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 * 
	 */
	public ActionForward showCountryStateSettingByCountry(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		CountryStateDao countryDao = new CountryStateDaoImpl();
		CountryStateForm countryStateForm = (CountryStateForm) form;
		List<State> states=countryDao.
				retrieveStateByCountry(Integer.parseInt(request.getParameter("selectedcountry")), appDetails);
		countryStateForm.setStates(states);
		String hasEdit="no";
		String hasDelete="no";
		@SuppressWarnings("unchecked")
		List<RolePrivileges> roleprivileges=(List<RolePrivileges>)session.getAttribute("privileges");
		Iterator<RolePrivileges> itr=roleprivileges.iterator();
		while(itr.hasNext())
		{
			RolePrivileges privilege=(RolePrivileges)itr.next();
			VMSObjects vms=privilege.getObjectId();
			if(vms.getObjectName().equalsIgnoreCase("Country"))
			{
				if(privilege.getModify()==1)
				   hasEdit="yes";
				if(privilege.getDelete()==1)
					hasDelete="yes";
			}
		}
		JSONObject jsonObject = new JSONObject();
		JSONArray jStateList = new JSONArray();
		JSONObject jStateObject = null;
		for(State state : states)
		{
			jStateObject = new JSONObject();
			jStateObject.put("id", state.getId());
			jStateObject.put("name", state.getStatename());
			jStateList.add(jStateObject);
		}
		jsonObject.put("stateList", jStateList);
		jsonObject.put("editprivilege", hasEdit);
		jsonObject.put("deleteprivilege",hasDelete);
		response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
		return null;
	}
}