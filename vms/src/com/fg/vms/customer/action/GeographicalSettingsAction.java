/**
 * GeographicalSettingsAction.java
 */
package com.fg.vms.customer.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerBusinessGroup;
import com.fg.vms.customer.pojo.GeographicForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;

/**
 * Defined GeographicalSettingsAction class for geographic details like region,
 * state
 * 
 * @author vinoth
 * 
 */
public class GeographicalSettingsAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * View Geographical settings page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showGeographicalSettingsPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to view the geographical settings page..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		GeographicForm geographicForm = (GeographicForm) form;

		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<CustomerBusinessArea> areas = regionDao
				.listBusinessAreas(appDetails);
		geographicForm.setAreas(areas);
		session.setAttribute("areasList1", areas);

		List<CustomerBusinessGroup> groups = regionDao
				.listBusinessGroups(appDetails);
		geographicForm.setGroups(groups);
		session.setAttribute("group1", groups);

		return mapping.findForward("viewsuccess");
	}

	/**
	 * List the state by region.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward listAreaByGroup(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<GeographicalStateRegionDto> stateRegionDtos = null;
		stateRegionDtos = regionDao.getStateByRegionList(appDetails);
		session.setAttribute("stateRegionDtos", stateRegionDtos);

		return mapping.findForward("listStates");
	}

	/**
	 * List the geographical region.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward listGeographicalRegion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorId = null;
		if (session.getAttribute("vendorId") != null) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}

		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<GeographicalStateRegionDto> stateRegionDtos = null;
		stateRegionDtos = regionDao.getGeographicalRegionList(vendorId,
				appDetails);
		session.setAttribute("stateRegionDtos", stateRegionDtos);

		return mapping.findForward("listStates");
	}

	/**
	 * Retrieve region.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrieveBgroup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to retrieve business group..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer groupId = Integer.parseInt(request.getParameter("id"));
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<CustomerBusinessGroup> bgroups = regionDao.retrieveBusinessGroup(
				groupId, appDetails);
		session.setAttribute("bgroups", bgroups);
		return mapping.findForward("retrieve");
	}

	/**
	 * Delete region.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteGroup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to delete business group..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer groupId = Integer.parseInt(request.getParameter("id"));
		Integer userId = (Integer) session.getAttribute("userId");
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		String isDeletable=regionDao.checkDeleteGroup(groupId, userId, appDetails);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("isDeletable", isDeletable);
		response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
		return null;// mapping.findForward("viewsuccess");
	}

	/**
	 * Retrieve region.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrieveArea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to retrieve business area..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer areaId = Integer.parseInt(request.getParameter("id"));
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<CustomerBusinessArea> areas = regionDao.retrieveBusinessArea(
				areaId, appDetails);
		if (areas != null && areas.size() != 0) {
			session.setAttribute("bizAreas", areas);
		} /*else {
			State state = regionDao.getStateName(areaId, appDetails);
			session.setAttribute("stateSesObj", state);
		}*/
		return mapping.findForward("retrieveState");
	}

	/**
	 * Delete region.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteArea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to delete business area..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer areaId = Integer.parseInt(request.getParameter("id"));
		Integer userId = (Integer) session.getAttribute("userId");
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		regionDao.deleteArea(areaId, userId, appDetails);
		return mapping.findForward("deleteState");
	}

	/**
	 * Save business group.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveBusinessGroup(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to save business group..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		GeographicForm geographicForm = (GeographicForm) form;

		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();

		// Save business group.
		regionDao.saveBusinessGroup(userId, geographicForm, appDetails);

		return mapping.findForward("save");
	}

	/**
	 * Save service area.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveServiceArea(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to save service area..");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		GeographicForm geographicForm = (GeographicForm) form;

		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();

		// Save service area.
		regionDao.saveServiceArea(userId, geographicForm, appDetails);

		return mapping.findForward("save");
	}
	
	/**
	 * Check of the Unity service area.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward checkServerAreaUnity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to check for uniqueness of service area.");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Session hibersession = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		
		Integer serviceAreaHidId = null;
		Integer regionId = Integer.parseInt(request.getParameter("id"));
		String  stateName=request.getParameter("name");
		
		List<CustomerBusinessArea> list = null;
		
		CustomerBusinessGroup cg=new CustomerBusinessGroup();
		cg=(CustomerBusinessGroup)hibersession.createQuery("from CustomerBusinessGroup cg where cg.id="+regionId).uniqueResult();
		CustomerBusinessArea cb=new CustomerBusinessArea();
		String isDuplicate="no";
		String isActiveAtDuplicate="yes";
	
		if(null != request.getParameter("serviceAreaHidId") && ! request.getParameter("serviceAreaHidId").isEmpty()) {
			serviceAreaHidId = Integer.parseInt(request.getParameter("serviceAreaHidId"));
			list=(List<CustomerBusinessArea>)hibersession.createQuery("from CustomerBusinessArea cb where cb.groupId="+cg.getId()+ "and cb.id!="+serviceAreaHidId).list();
		} else {
			list=(List<CustomerBusinessArea>)hibersession.createQuery("from CustomerBusinessArea cb where cb.groupId="+cg.getId()).list();			
		}	
		Iterator itr=list.iterator();
		while(itr.hasNext())
		{
			cb=(CustomerBusinessArea)itr.next();
			if(cb.getAreaName().equalsIgnoreCase(stateName))
			{		
				if(cb.getIsActive()==(byte)1){
					 isDuplicate="yes";
					 break;
				}
				else
					isActiveAtDuplicate="no";
			}			
		}
		
		JSONObject jsonObject = new JSONObject();
       	jsonObject.put("isDuplicate", isDuplicate);
       	jsonObject.put("isActiveAtDuplicate", isActiveAtDuplicate);
       	response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
		return null;
	}
	
	/**
	 * Check BusinessGroupUnity.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward checkBusinessGroupUnity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to check for uniqueness of businessgroup.");

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Session hibersession = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String regionName=request.getParameter("name");
		String regionId=request.getParameter("regionId");
		
		CustomerBusinessGroup cg=new CustomerBusinessGroup();
		List<CustomerBusinessGroup> list= null;
		
		if(regionId != null && regionId != "")
		{
			//Modify or Edit			
			list=(List<CustomerBusinessGroup>)hibersession.createQuery("from CustomerBusinessGroup a where a.id != "+regionId).list();			
		}
		else
		{
			//New
			list=(List<CustomerBusinessGroup>)hibersession.createQuery("from CustomerBusinessGroup ORDER BY businessGroupName ASC").list();
		}		
		
		String isBgDuplicate="no";
		String isActiveAtDuplicate="yes";

		Iterator it=list.iterator();
		
		while(it.hasNext())
		{
			cg=(CustomerBusinessGroup)it.next();
			if(cg.getBusinessGroupName().equalsIgnoreCase(regionName))
			{
				if(cg.getIsActive()==(byte)1)
				{
					isBgDuplicate="yes";
					break;
				}
				else
					isActiveAtDuplicate="no";
			}
		}
		JSONObject jsonObject = new JSONObject();
       	jsonObject.put("isBgDuplicate", isBgDuplicate);
       	jsonObject.put("isActiveAtDuplicate", isActiveAtDuplicate);
       	response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
		return null;
	}	
}