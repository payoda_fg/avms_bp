/**
 * 
 */
package com.fg.vms.customer.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ProactiveMonitoringDao;
import com.fg.vms.customer.dao.impl.ProactiveMonitoringDaoImpl;
import com.fg.vms.customer.model.ProactiveMonitoringCfg;
import com.fg.vms.customer.pojo.ProactiveMonitoringForm;
import com.fg.vms.util.CommonUtils;

/**
 * @author pirabu
 * 
 */
public class ProactiveMonitoring extends DispatchAction {

	public ActionForward proactiveMonitoring(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProactiveMonitoringForm monitoringForm = (ProactiveMonitoringForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/* Currently login user id */

		ProactiveMonitoringDao monitoringDao = new ProactiveMonitoringDaoImpl();

		ProactiveMonitoringCfg cfg = monitoringDao.retrive(appDetails);

		if (cfg != null) {
			monitoringForm.setReccurrencePattern(convertIntToString(cfg
					.getReccurrencePattern()));
			monitoringForm.setDailyOption(convertIntToString(cfg
					.getDailyOption()));
			monitoringForm.setMonthlyOption(convertIntToString(cfg
					.getMonthlyOption()));
			monitoringForm.setYearlyOption(convertIntToString(cfg
					.getYearlyOption()));
			monitoringForm.setRangeOccurenceOption(convertIntToString(cfg
					.getRangeOccurenceOption()));
			monitoringForm.setDailyEveryDayNumber(convertIntToString(cfg
					.getDailyEveryDayNumber()));

			monitoringForm.setNumberOfOccurrence(convertIntToString(cfg
					.getNumberOfOccurrence()));
			monitoringForm.setWeeklyWeekNumber(convertIntToString(cfg
					.getWeeklyWeekNumber()));
			monitoringForm.setMonthlyDayNumber(convertIntToString(cfg
					.getMonthlyDayNumber()));

			monitoringForm.setMonthlyWeekNumber(cfg.getMonthlyWeekNumber());

			monitoringForm.setMonthlyWeekDay(convertIntToString(cfg
					.getMonthlyWeekDay()));
			monitoringForm.setMonthlyMonthNumber(convertIntToString(cfg
					.getMonthlyMonthNumber()));
			if (cfg.getWeeklyWeekDays() != null
					&& cfg.getWeeklyWeekDays().length() > 0) {
				monitoringForm
				.setWeeklyWeekDays(cfg.getWeeklyWeekDays().split(","));
			}

			monitoringForm.setYearlyMonthNumber(convertIntToString(cfg
					.getYearlyMonthNumber()));

			monitoringForm.setYearlyDayNumber(convertIntToString(cfg
					.getYearlyDayNumber()));
			monitoringForm.setYearlyWeekNumber(cfg.getYearlyWeekNumber());
			monitoringForm.setYearlyWeekDayNumber(convertIntToString(cfg
					.getYearlyWeekDayNumber()));
			monitoringForm.setYearlyMonthNumber(convertIntToString(cfg
					.getYearlyMonthNumber()));

			monitoringForm.setRangeStartDate(CommonUtils
					.convertDateToString(cfg.getRangeStartDate()));
			monitoringForm.setOccurenceEndate(CommonUtils
					.convertDateToString(cfg.getOccurenceEndate()));

		}

		return mapping.findForward("showPage");
	}

	/**
	 * Save the Proactive Monitoring and alerting
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */

	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		ProactiveMonitoringForm monitoringForm = (ProactiveMonitoringForm) form;
		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/* Currently login user id */
		Integer userId = (Integer) session.getAttribute("userId");

		ProactiveMonitoringDao monitoringDao = new ProactiveMonitoringDaoImpl();

		String result = monitoringDao.save(appDetails, monitoringForm, userId);
		// For view the list of certificates
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("monitoring.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);

		} else {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("monitoring.fail");
			messages.add("failureMsg", msg);
			saveMessages(request, messages);
			mapping.getInputForward();
		}
		return mapping.findForward(result);
	}

	private String convertIntToString(Integer value) {

		if (value != null && value != -1) {
			return value.toString();
		} else {
			return "";
		}

	}
}
