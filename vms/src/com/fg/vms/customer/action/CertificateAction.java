/*
 * CertificateAction.java
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CertificateDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertificateDivision;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerClassificationCertificateTypes;
import com.fg.vms.customer.model.CustomerClassificationCertifyingAgencies;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.pojo.CertificateForm;

/**
 * Represents certificate details (eg.. create, view, modify and delete details
 * of certificate and list of certificates.)
 * 
 * @author vinoth
 * 
 */
public class CertificateAction extends DispatchAction {

	/**
	 * View the Form contains list of certificates
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward viewCertificate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		List<Certificate> certificates = null;
		List<CustomerDivision> customerDivisions = null;
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CertificateDaoImpl certificateDaoImpl = new CertificateDaoImpl();
		certificates = certificateDaoImpl.listOfCertificates(appDetails);
		session.setAttribute("certificates", certificates);

		/*CURDDao<CustomerCertificateType> curdDao = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypes = curdDao.list(
				appDetails, " from CustomerCertificateType where isActive=1");
		session.setAttribute("certificateTypes", certificateTypes);*/
		

		List<GeographicalStateRegionDto> certificateTypes = null;
		
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		certificateTypes = regionDao.getCertificationType(appDetails);
		session.setAttribute("certificateTypes", certificateTypes);

		SearchDao searchDao = new SearchDaoImpl();
		List<CertifyingAgency> certificateAgencies;
		certificateAgencies = searchDao.listAgencies(appDetails);
		session.setAttribute("certAgencyList", certificateAgencies);

		// to get the customer divisions
		UsersDao usersDao = new UsersDaoImpl();
		//if Both Division(Division name="BOTH") is not needed in UI then pass true otherwise  pass false;
		customerDivisions = usersDao.listCustomerDivisons(appDetails, true);
		session.setAttribute("customerDivisions", customerDivisions);

		return mapping.findForward("viewsuccess");
	}

	/**
	 * Save new certificates in DB
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveCertificate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId");

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CertificateForm certificateForm = (CertificateForm) form;
		CertificateDaoImpl certificateDaoImpl = new CertificateDaoImpl();
		String result = certificateDaoImpl.createCertificate(certificateForm,
				userId, appDetails);
		// For check unique certificate name

		if (result.equals("unique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("certificateName", new ActionMessage(
					"certificate.unique"));
			saveErrors(request, errors);
			mapping.findForward(mapping.getInput());
		}
		// For view the list of certificates
		if (result.equals("success")) {
			List<Certificate> certificates = null;
			certificates = certificateDaoImpl.listOfCertificates(appDetails);
			session.setAttribute("certificates", certificates);

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("certificate.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			certificateForm.reset(mapping, request);
		}
		return mapping.findForward("savesuccess");
	}

	/**
	 * Method to update the certificate into inactive (Delete action in UI)
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deletecertificate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		CertificateDaoImpl certificateDaoImpl = new CertificateDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result = certificateDaoImpl.deleteCertificate(
				Integer.parseInt(request.getParameter("id")), appDetails);

		if (result.equals("reference")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage(
					"certificate.reference.delete");
			messages.add("certificateReferenceDelete", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		// For view the list of certificates
		if (result.equals("success")) {
			List<Certificate> certificates = null;
			certificates = certificateDaoImpl.listOfCertificates(appDetails);
			session.setAttribute("certificates", certificates);

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("certificate.delete");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
		}

		return mapping.findForward("deletesuccess");
	}

	/**
	 * Method to retrieve the selected certificate for update
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrivecertificate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		CertificateForm certificateForm = (CertificateForm) form;
		CertificateDaoImpl certificateDaoImpl = new CertificateDaoImpl();
		HttpSession session = request.getSession();
		Integer id = Integer.parseInt(request.getParameter("id").toString());

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Certificate certificate = certificateDaoImpl.retriveCertificate(id,
				appDetails);

		if (certificate != null) {

			CURDDao<CustomerClassificationCertifyingAgencies> curdDao = new CURDTemplateImpl<CustomerClassificationCertifyingAgencies>();
			List<CustomerClassificationCertifyingAgencies> agencies = curdDao
					.list(appDetails,
							"from CustomerClassificationCertifyingAgencies where isActive = 1 and certificateId="
									+ id);
			certificateForm = packCertifyingAgencyData(certificateForm,
					agencies);

			CURDDao<CustomerClassificationCertificateTypes> curdDao1 = new CURDTemplateImpl<CustomerClassificationCertificateTypes>();
			List<CustomerClassificationCertificateTypes> certificateTypes = curdDao1
					.list(appDetails,
							"from CustomerClassificationCertificateTypes where isActive = 1 and certificateId="
									+ id);
			certificateForm = packCertificateTypes(certificateForm,
					certificateTypes);

			CURDDao<CertificateDivision> curdDao3 = new CURDTemplateImpl<CertificateDivision>();
			List<CertificateDivision> customerDivisions = curdDao3.list(
					appDetails,
					"from CertificateDivision where isActive = 1 and certMasterId="
							+ id);
			certificateForm = packCustomerDivisions(certificateForm,
					customerDivisions);

			certificateForm.setId(certificate.getId());
			certificateForm
					.setCertificateName(certificate.getCertificateName());
			certificateForm.setCertificateShortName(certificate
					.getCertificateShortName());
			certificateForm.setDiverseQuality(certificate.getDiverseQuality()
					.toString());
			certificateForm.setIsEthinicity(String.valueOf(certificate
					.getIsEthinicity()));
			certificateForm.setCertificateUpload(String.valueOf(certificate
					.getCertificateUpload()));
			certificateForm.setIsActive(certificate.getIsActive().toString());
		}

		// For view the list of certificates
		List<Certificate> certificates = null;
		certificates = certificateDaoImpl.listOfCertificates(appDetails);
		session.setAttribute("certificates", certificates);
		return mapping.findForward("retrievesuccess");
	}

	/**
	 * Method to update the selected object
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward updatecertificate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer currentUserId = (Integer) session.getAttribute("userId");

		CertificateDao certificateDaoImpl = new CertificateDaoImpl();
		CertificateForm certificateForm = (CertificateForm) form;

		String result = certificateDaoImpl.updateCertificate(certificateForm,
				certificateForm.getId(), currentUserId, appDetails);
		
		/* To check unique certificate. */
		if (result.equals("unique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("certificateName", new ActionMessage("certificate.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}		

		/*
		 * Don't allow to change diverse or quality after selecting the
		 * certificate in the vendor creation
		 */
		if (result.equals("reference")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("certificate.reference");
			messages.add("certificateReference", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		// For view the list of certificates
		if (result.equals("success")) {
			List<Certificate> certificates = null;
			certificates = certificateDaoImpl.listOfCertificates(appDetails);
			session.setAttribute("certificates", certificates);

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("certificate.update");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			certificateForm.reset(mapping, request);
		}

		return mapping.findForward("updatesuccess");
	}

	/**
	 * Delete VendorCertificate
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteVendorCert(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorCertificateID = null;
		CertificateDao certificateDao = new CertificateDaoImpl();
		if (null != request.getParameter("id")
				&& !request.getParameter("id").isEmpty()) {
			vendorCertificateID = Integer.parseInt(request.getParameter("id"));
			String result = certificateDao.deleteVendorCertificate(
					vendorCertificateID, appDetails);
		}
		return mapping.findForward("success");
	}

	/**
	 * Delete Vendor Other Certificate
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteVendorOtherCert(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorCertificateID = null;
		CertificateDao certificateDao = new CertificateDaoImpl();
		if (null != request.getParameter("id")
				&& !request.getParameter("id").isEmpty()) {
			vendorCertificateID = Integer.parseInt(request.getParameter("id"));
			certificateDao.deleteVendorOtherCertificate(vendorCertificateID,
					appDetails);
		}
		return mapping.findForward("success");
	}

	/**
	 * Setting certifying agencies in edit classification.
	 * 
	 * @param certificateForm
	 * @param agencies
	 * @return
	 */
	private CertificateForm packCertifyingAgencyData(
			CertificateForm certificateForm,
			List<CustomerClassificationCertifyingAgencies> agencies) {

		String[] value = new String[agencies.size()];
		int i = 0;
		for (CustomerClassificationCertifyingAgencies agencies2 : agencies) {
			value[i++] = agencies2.getAgencyId().getId().toString();
		}
		certificateForm.setCertificateAgency(value);
		return certificateForm;
	}

	/**
	 * Setting certificate types in edit classification.
	 * 
	 * @param certificateForm
	 * @param agencies
	 * @return
	 */
	private CertificateForm packCertificateTypes(
			CertificateForm certificateForm,
			List<CustomerClassificationCertificateTypes> certificateTypes) {

		String[] value = new String[certificateTypes.size()];
		int i = 0;
		for (CustomerClassificationCertificateTypes types : certificateTypes) {
			value[i++] = types.getCertificateTypeId().getId().toString();
		}
		certificateForm.setCertificateType(value);
		return certificateForm;
	}

	/**
	 * Setting Customer Division in edit classification.
	 * 
	 * @param certificateForm
	 * @param divisions
	 * @return
	 */
	private CertificateForm packCustomerDivisions(
			CertificateForm certificateForm, List<CertificateDivision> divisions) {

		String[] value = new String[divisions.size()];
		int i = 0;
		for (CertificateDivision division : divisions) {
			value[i++] = division.getCustomerDivisionId().getId().toString();
		}
		certificateForm.setCustomerDivision(value);
		return certificateForm;
	}
}
