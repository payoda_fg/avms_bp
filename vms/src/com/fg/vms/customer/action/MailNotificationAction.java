package com.fg.vms.customer.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.EmailTemplateDao;
import com.fg.vms.customer.dao.MailNotificationDao;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.EmailTemplateDaoImpl;
import com.fg.vms.customer.dao.impl.MailNotificationDaoImpl;
import com.fg.vms.customer.dao.impl.NAICSubCategoryImpl;
import com.fg.vms.customer.dao.impl.NaicsCategoryDaoImpl;
import com.fg.vms.customer.dao.impl.TemplateDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorEmailNotificationMaster;
import com.fg.vms.customer.model.VendorMailNotificationDetail;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.MailForm;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.util.Constants;

public class MailNotificationAction extends DispatchAction {

	private Logger logger = Constants.logger;
	
	private UserDetailsDto userDetails;

	/**
	 * Get mailId for vendor.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward searchVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		session.setAttribute("emailAddress", "");
		session.setAttribute("cc", "");
		session.setAttribute("bcc", "");
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
		List<Template> templateNameList = templateDaoImpl
				.listSelectTemplateQuestions(userDetails);
		session.setAttribute("templateNameList", templateNameList);

		/*
		 * Get the active distribution list
		 */
		WorkflowConfigurationDaoImpl workflowConfigurationDaoImpl = new WorkflowConfigurationDaoImpl();
		List<EmailDistributionModel> distributionModels = workflowConfigurationDaoImpl
				.view(userDetails);

		session.setAttribute("distributionModels", distributionModels);
		/*
		 * For displaying email template list
		 */
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		List<EmailTemplate> emailTemplates = null;
		emailTemplates = dao.listTemplates(userDetails);
		session.setAttribute("emailTemplates", emailTemplates);

		return mapping.findForward("searchSuccess");
	}

	/**
	 * Get mailId for assessment vendor.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward searchAssessmentVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		session.setAttribute("emailAddress", "");
		session.setAttribute("cc", "");
		session.setAttribute("bcc", "");

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
		List<Template> templateNameList = templateDaoImpl
				.listSelectTemplateQuestions(userDetails);
		session.setAttribute("templateNameList", templateNameList);
		session.setAttribute("templateassessment", "");
		session.setAttribute("templateAssessmentName", "");
		session.setAttribute("finalDate", "");

		return mapping.findForward("searchAssessmentSuccess");
	}

	/**
	 * Searching vendor by capability.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward capabalitySearchVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		session.setAttribute("emailAddress", "");

		return mapping.findForward("searchSuccess");
	}

	/**
	 * searching vendor by name.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward vendorNameSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		Integer customerDivisionId = null;
		MailNotificationDaoImpl mailNotificationDaoImpl = new MailNotificationDaoImpl();

		HttpSession session = request.getSession();
		
		if(session.getAttribute("customerDivisionId") != null)
		{
			customerDivisionId =(Integer) session.getAttribute("customerDivisionId");
			searchVendorForm.setCustomerDivisionId(customerDivisionId);
		}		
		
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<SearchVendorDto> vendorsList = null;
		vendorsList = mailNotificationDaoImpl.searchVendorName(
				searchVendorForm, 1, appDetails, 1, null);
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		List<State> states = (List<State>) dao.list(appDetails,
				" From State where countryid=" + searchVendorForm.getCountry());
		searchVendorForm.setStates(states);
		searchVendorForm.setVendorsList(vendorsList);

		return mapping.findForward("Success");
	}

	/**
	 * searching assessment vendor by name.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward vendorAssessmentNameSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		MailNotificationDaoImpl mailNotificationDaoImpl = new MailNotificationDaoImpl();

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<SearchVendorDto> vendorsList = null;
		vendorsList = mailNotificationDaoImpl.searchVendorName(
				searchVendorForm, 1, appDetails, 0, "Assessment");
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		List<State> states = (List<State>) dao.list(appDetails,
				" From State where countryid=" + searchVendorForm.getCountry());
		searchVendorForm.setStates(states);
		searchVendorForm.setVendorsList(vendorsList);
		// searchVendorForm.reset(mapping, request);
		return mapping.findForward("assessmentPage");
	}

	/**
	 * Searching vendor for approval.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward vendorApprovalSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		MailNotificationDaoImpl mailNotificationDaoImpl = new MailNotificationDaoImpl();

		List<SearchVendorDto> vendorsList = null;
		vendorsList = mailNotificationDaoImpl.searchVendorName(
				searchVendorForm, 0, appDetails, null, null);

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		searchVendorForm.setCountries(countries);

		searchVendorForm.setVendorsList(vendorsList);
		session.setAttribute("searchVendorsList", vendorsList);

		return mapping.findForward("searchresult");
	}

	/**
	 * Show Mail Notification page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showMailNotificationPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		if (request.getParameter("id") != null) {
			String id = request.getParameter("id");
			session.setAttribute("fieldId", id);
		}
		if (request.getParameter("assessmentTemplate") != null
				&& request.getParameter("finalDate") != null) {
			Integer templateid = Integer.parseInt(request.getParameter(
					"assessmentTemplate").toString());
			String finalDate = request.getParameter("finalDate");
			String templateAssessmentName = request
					.getParameter("templateName").toString();

			session.setAttribute("templateassessment", templateid);
			session.setAttribute("finalDate", finalDate);
			session.setAttribute("templateAssessmentName",
					templateAssessmentName);
		}

		SearchVendorForm searchVendorForm = (SearchVendorForm) form;

		if (searchVendorForm.getTo() != null
				&& !searchVendorForm.getTo().isEmpty()) {
			session.setAttribute("manualEmailAddress", searchVendorForm.getTo());
		}
		if (searchVendorForm.getCc() != null
				&& !searchVendorForm.getCc().isEmpty()) {
			session.setAttribute("cc", searchVendorForm.getCc());
		}
		if (searchVendorForm.getBcc() != null
				&& !searchVendorForm.getBcc().isEmpty()) {
			session.setAttribute("bcc", searchVendorForm.getBcc());
		}

		CertificateDaoImpl certificate = new CertificateDaoImpl();

		List<Certificate> certificates = null;
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails); // List of certificate types.
		session.setAttribute("certificateTypes", certificates);
		
		String query = "From Country where isactive=1 order by countryname";
		if(appDetails.getWorkflowConfiguration() != null) {
			if(appDetails.getWorkflowConfiguration().getInternationalMode() != null && appDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}
		
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(appDetails, query);
		searchVendorForm.setCountries(countries);
		
		Integer countryId = null;
		List<Country> country = (List<Country>) dao.list(appDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();		

		List<State> states = null;
		if(appDetails.getWorkflowConfiguration() != null) {
			if(appDetails.getWorkflowConfiguration().getInternationalMode() != null && appDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				CURDDao<State> dao1 = new CURDTemplateImpl<State>();
				states = (List<State>) dao1.list(appDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");				
			}
		}
		searchVendorForm.setStates(states);
		
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		searchVendorForm.setVendorsList(vendorsList);		
		return mapping.findForward("Success");
	}

	/**
	 * Show capability assessment mail notification page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward showCapabalityAssessmentEmailNotificationPage(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
				.view(appDetails);
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");

		searchVendorForm.setCategories(naicsCategories);
		searchVendorForm.setCountries(countries);
		return mapping.findForward("Success");
	}

	/**
	 * Show spend indirect search page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward showSpendIndirectSearchPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
				.view(appDetails);
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");

		searchVendorForm.setCategories(naicsCategories);
		searchVendorForm.setCountries(countries);

		return mapping.findForward("Success");

	}

	/**
	 * Retrieve the list of Sub-categories by Categories.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewSubCategoriesById(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		// get the selected category id
		Integer categoryId = Integer.parseInt(request
				.getParameter("categoryId").toString());
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
		List<NAICSubCategory> subCategories;

		// Get category list.
		if (categoryId != 0) {
			subCategories = naicSubCategoryImpl.retrieveSubCategoryById(
					categoryId, appDetails);
		} else {
			ActionErrors errors = new ActionErrors();

			errors.add("category", new ActionMessage("error.category.invalid"));
			saveErrors(request, errors);
			// Return back to the previous state
			session.setAttribute("categoryId", categoryId);
			session.removeAttribute("naicssubcategories");
			return mapping.findForward("Success");
		}

		NaicsCategory category = naicSubCategoryImpl.retriveNaicsCategory(
				categoryId, appDetails);

		session.setAttribute("categoryId", category.getId());
		session.setAttribute("naicssubcategories", subCategories);

		// forward to the view Naics Masters page.
		return mapping.findForward("Success");
	}

	/**
	 * View assessment emailId.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward viewAssessmentMailId(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		SearchVendorForm vendorForm = (SearchVendorForm) form;

		String manualEmailAddress = "";
		if (session.getAttribute("manualEmailAddress") != null) {
			manualEmailAddress = (String) session
					.getAttribute("manualEmailAddress");
		}

		String manualCC = "";
		if (session.getAttribute("cc") != null) {
			manualCC = (String) session.getAttribute("cc");
		}

		String manualBCC = "";
		if (session.getAttribute("bcc") != null) {
			manualBCC = (String) session.getAttribute("bcc");
		}

		String fieldId = null;
		if (session.getAttribute("fieldId") != null) {
			fieldId = (String) session.getAttribute("fieldId");
		}

		StringBuilder emailAddress = new StringBuilder();
		if (vendorForm.getSelectedEmailAddress() != null
				&& vendorForm.getSelectedEmailAddress().length != 0) {
			for (String email : vendorForm.getSelectedEmailAddress()) {
				emailAddress.append(email + ";");
			}
		}

		if (fieldId.equalsIgnoreCase("TO")) {
			emailAddress.append(manualEmailAddress);
			session.removeAttribute("emailAddress");
			session.setAttribute("emailAddress", emailAddress);
		} else if (fieldId.equalsIgnoreCase("CC")) {
			emailAddress.append(manualCC);
			session.removeAttribute("cc");
			session.setAttribute("cc", emailAddress);
		} else if (fieldId.equalsIgnoreCase("BCC")) {
			emailAddress.append(manualBCC);
			session.removeAttribute("bcc");
			session.setAttribute("bcc", emailAddress);
		}

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
		List<Template> templateNameList = templateDaoImpl
				.listSelectTemplateQuestions(userDetails);
		session.setAttribute("templateNameList", templateNameList);		
		return mapping.findForward("searchAssessmentSuccess");
	}

	/**
	 * View emailId.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward viewMailId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		session.setAttribute("emailAddress", "");		
		session.setAttribute("cc", "");
		session.setAttribute("bcc", "");
		SearchVendorForm vendorForm = (SearchVendorForm) form;

		String manualEmailAddress = "";
		if (session.getAttribute("manualEmailAddress") != null) {
			manualEmailAddress = (String) session
					.getAttribute("manualEmailAddress");
		}

		StringBuilder emailAddress = new StringBuilder();
		if (vendorForm.getSelectedEmailAddress() != null
				&& vendorForm.getSelectedEmailAddress().length != 0) {
			for (String email : vendorForm.getSelectedEmailAddress()) {
				emailAddress.append(email + ";");
			}			
		}
		emailAddress.append(manualEmailAddress);	
		session.setAttribute("manualEmailAddress", "");
		session.setAttribute("emailAddress", emailAddress);		
		return mapping.findForward("searchSuccess");
	}

	/**
	 * View vendor name.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward viewVendorName(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchVendorForm vendorForm = (SearchVendorForm) form;

		MailNotificationDao notificationDao = new MailNotificationDaoImpl();
		Integer vendorId = vendorForm.getVendorId();
		VendorMaster vendorMaster = notificationDao.getVendor(vendorId,
				appDetails);
		session.setAttribute("vendorId", vendorMaster.getId());
		session.setAttribute("vendorName", vendorMaster.getVendorName());
		session.setAttribute("vendorNumber", vendorMaster.getVendorCode());
		session.setAttribute("vendorEmail", vendorMaster.getEmailId());

		return mapping.findForward("spendIndirectSearchSuccess");
	}

	/**
	 * This method for notifying the email.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward mailNotify(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer currentUserId = (Integer) session.getAttribute("userId");
		MailForm mailForm = (MailForm) form;
		// Added Full Path for email Image(s)...
		StringBuffer url=new StringBuffer(request.getRequestURL());
		url.replace(url.lastIndexOf("/"), url.length(), "");
		url.append("/");
		if(mailForm.getMessage()!=null)
		{
			String orginalMessage=mailForm.getMessage();
			String splitedMessage[]=orginalMessage.split("<img src=\"");
			String changedMessage="";
			if(splitedMessage.length>=2)
				changedMessage=changedMessage+splitedMessage[0];
			for(int i=1;i<splitedMessage.length;i++){
					changedMessage=changedMessage+"<img src=\""+url.toString()+""+splitedMessage[i];
			}
			if(splitedMessage.length>=2)
			  mailForm.setMessage(changedMessage);
			
		}
		
		MailNotificationDao mailDaoImpl = new MailNotificationDaoImpl();

		userDetails = (UserDetailsDto) session.getAttribute("userDetails");

		VendorEmailNotificationMaster vendorEmailNotificationMaster = mailDaoImpl
				.saveMailNotificationInformation(mailForm, userDetails,
						currentUserId, appDetails);
		List<VendorMailNotificationDetail> listOfVendorMailids = (List<VendorMailNotificationDetail>) mailDaoImpl
				.retriveMailnotificationDetails(userDetails, currentUserId);

		String result = mailDaoImpl.sendMail(mailForm, appDetails,
				vendorEmailNotificationMaster.getId().toString());
		mailForm.setMailNotificationDetails(listOfVendorMailids);

		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("mail.ok");
			messages.add("mail", msg);
			saveMessages(request, messages);
			mailForm.reset(mapping, request);
			session.setAttribute("emailAddress", "");
			session.setAttribute("cc", "");
			session.setAttribute("bcc", "");
			return mapping.findForward("success");
		} else {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("err.mail.notsent");
			messages.add("mailfailed", msg);
			saveMessages(request, messages);
			return mapping.findForward("error");
		}
	}

	/**
	 * This method is for notifying assessment email.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward assessmentMailNotify(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		MailForm mailForm = (MailForm) form;
		HttpSession session = request.getSession();
		// Added Full Path for email Image(s)...
		StringBuffer url=new StringBuffer(request.getRequestURL());
		url.replace(url.lastIndexOf("/"), url.length(), "");
		url.append("/");
		if(mailForm.getMessage()!=null)
		{
			String orginalMessage=mailForm.getMessage();
			String splitedMessage[]=orginalMessage.split("<img src=\"");
			String changedMessage="";
			if(splitedMessage.length>=2)
				changedMessage=changedMessage+splitedMessage[0];
			for(int i=1;i<splitedMessage.length;i++){
					changedMessage=changedMessage+"<img src=\""+url.toString()+""+splitedMessage[i];
			}
			if(splitedMessage.length>=2)
			 mailForm.setMessage(changedMessage);
			
		}
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		MailNotificationDao mailDaoImpl = new MailNotificationDaoImpl();
		VendorEmailNotificationMaster vendorEmailNotificationMaster = mailDaoImpl
				.saveAssessmentMailInfo(mailForm, userId, appDetails);
		String result = mailDaoImpl.sendMail(mailForm, appDetails,
				vendorEmailNotificationMaster.getId().toString());

		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("mail.ok");
			messages.add("mail", msg);
			saveMessages(request, messages);
			mailForm.reset(mapping, request);
			session.setAttribute("emailAddress", "");
			session.setAttribute("cc", "");
			session.setAttribute("bcc", "");
			session.removeAttribute("templateassessment");
			session.removeAttribute("finalDate");
			session.setAttribute("templateAssessmentName", "");
			return mapping.findForward("mailSuccess");
		} else {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("err.mail.notsent");
			messages.add("mailfailed", msg);
			saveMessages(request, messages);
			return mapping.findForward("error");
		}

	}

	/**
	 * Vendor approval for customer created vendors
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward approveVendorCustomer(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		MailNotificationDao mailDaoImpl = new MailNotificationDaoImpl();
		// SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		Integer selectedId = Integer.parseInt(request
				.getParameter("selectedId").toString());
		Integer prime = null;
		String primeVal = request.getParameter("prime");
		if (primeVal != null && !primeVal.isEmpty()
				&& !primeVal.equalsIgnoreCase("undefined")) {
			prime = Integer.parseInt(request.getParameter("prime").toString());
		}

		String approvalStatus = null;
		approvalStatus = request.getParameter("approvalStatus");
		String isApprovedDesc = request.getParameter("isApprovedDesc");
		String bpSegmentId = request.getParameter("bpSegmentId");
		Users user = (Users) session.getAttribute("currentUser");
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		String result = mailDaoImpl.changeVendorStatus(selectedId, user,
				approvalStatus,bpSegmentId, isApprovedDesc, appDetails, prime, url);
		session.setAttribute("statusResult", result);
		return mapping.findForward("approvesuccess");
	}

	/**
	 * Get mailIds based on email distribution list.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward getEmailIds(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		MailForm mailForm = (MailForm) form;

		StringBuilder builder = new StringBuilder();
		for (int index = 0; index < mailForm.getListDistribution().length; index++) {
			builder.append(mailForm.getListDistribution()[index]);
			if (index != mailForm.getListDistribution().length - 1) {
				builder.append(",");
			}
		}
		// Get email distribution detail list
		CURDDao<EmailDistributionListDetailModel> curdDao = new CURDTemplateImpl<EmailDistributionListDetailModel>();
		List<EmailDistributionListDetailModel> detailModels = curdDao.list(
				appDetails,
				" from EmailDistributionListDetailModel where emailDistributionMasterId in ("
						+ builder.toString() + ")");
		session.setAttribute("detailModels", detailModels);
		return mapping.findForward("searchSuccess");
	}
	
	public ActionForward showEmailThisVendorPage(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		logger.info("Show Email to This Vendor Notification Page");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		MailForm mailForm = (MailForm) form;
		
		Integer vendorId = Integer.parseInt(request.getParameter("vendorId").toString());
		String requestFrom = request.getParameter("requestFrom").toString();
		
		CURDDao<VendorContact> curdDao = new CURDTemplateImpl<VendorContact>();
		List<VendorContact> primaryVendorUserList = curdDao.list(appDetails, "from VendorContact where isPrimaryContact = 1 and vendorId = " + vendorId);
		
		if(!primaryVendorUserList.isEmpty() && primaryVendorUserList.size() > 0)
		{
			mailForm.setTo(primaryVendorUserList.get(0).getEmailId() + ";");
		}
		
		List<VendorContact> nonPrimaryVendorUserList = curdDao.list(appDetails, "from VendorContact where isPrimaryContact = 0 and vendorId = " + vendorId);
		request.setAttribute("nonPrimaryVendorUserList", nonPrimaryVendorUserList);
		
		if(requestFrom.equalsIgnoreCase("vendorProfilePage")) //For Vendor Profile Page
		{
			return mapping.findForward("showEmailThisVendorNotificationPage");
		}			
		else //if(requestFrom.equalsIgnoreCase("searchPage")) For Search Result Page (Search Vendor All Statuses).
		{
			return mapping.findForward("showEmailThisVendorForSearchPage");
		}
	}
	
	public ActionForward sendEmailThisVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer currentUserId = (Integer) session.getAttribute("userId");
		MailForm mailForm = (MailForm) form;
		MailNotificationDao mailDaoImpl = new MailNotificationDaoImpl();
		String requestFrom = request.getParameter("requestFrom").toString();
		
		// Added Full Path for email Image(s)...
		StringBuffer url=new StringBuffer(request.getRequestURL());
		url.replace(url.lastIndexOf("/"), url.length(), "");
		url.append("/");
		
		if(mailForm.getMessage() != null)
		{
			String orginalMessage = mailForm.getMessage();
			String splitedMessage[] = orginalMessage.split("<img src=\"");
			String changedMessage = "";
			
			if(splitedMessage.length >= 2)
				changedMessage = changedMessage + splitedMessage[0];
			
			for(int i = 1; i < splitedMessage.length; i++)
			{
				changedMessage=changedMessage + "<img src=\"" + url.toString() + "" + splitedMessage[i];
			}
			
			if(splitedMessage.length >= 2)
				mailForm.setMessage(changedMessage);			
		}

		VendorEmailNotificationMaster vendorEmailNotificationMaster = mailDaoImpl.saveMailNotificationInformation(mailForm, appDetails, currentUserId, appDetails);

		String result = mailDaoImpl.sendMail(mailForm, appDetails, vendorEmailNotificationMaster.getId().toString());

		if (result.equals("success"))
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("mail.ok");
			messages.add("mail", msg);
			
			saveMessages(request, messages);
			mailForm.reset(mapping, request);
			
			if(requestFrom.equalsIgnoreCase("vendorProfilePage"))//For Vendor Profile Page
			{
				return mapping.findForward("successSendEmailThisVendor");
			}
			else //For Search Result Page (Search Vendor All Statuses).
			{
				StringBuffer path = new StringBuffer((mapping.findForward("successSendEmailThisVendorForSearchPage")).getPath());
		        String valueFrom = "wizard";
		        path.append("&valueFrom=" + valueFrom);
		        return new ActionForward(path.toString());
			}			
		}
		else
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("err.mail.notsent");
			messages.add("mailfailed", msg);
			saveMessages(request, messages);
			
			if(requestFrom.equalsIgnoreCase("vendorProfilePage"))//For Vendor Profile Page
			{
				return mapping.findForward("errorSendEmailThisVendor");
			}
			else //For Search Result Page (Search Vendor All Statuses).
			{
				return mapping.findForward("errorSendEmailThisVendorForSearchPage");
			}			
		}
	}
}