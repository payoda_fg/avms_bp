/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dao.impl.NaicsCategoryDaoImpl;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.pojo.NaicsCategoryForm;

/**
 * @author vinoth
 * 
 */
public class NaicsCategoryAction extends DispatchAction {

    /**
     * View the category details.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward view(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();

	// For show the list of objects

	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	NaicsCategoryDaoImpl categoryDaoImpl = new NaicsCategoryDaoImpl();

	// List the categories
	List<NaicsCategory> categorymaster = null;
	categorymaster = categoryDaoImpl.listCategory(userDetails);
	session.setAttribute("categorymaster", categorymaster);

	return mapping.findForward("naicsMaster");
    }

    /**
     * To add NAICS Category
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward saveCategory(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer userId = (Integer) session.getAttribute("userId");

	NaicsCategoryForm naicsCategoryForm = (NaicsCategoryForm) form;
	NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();

	String result = naicsCategoryDao.addCategory(naicsCategoryForm, userId,
		appDetails);

	// List the categories
	List<NaicsCategory> categorymaster = null;
	NaicsCategoryDaoImpl categoryDaoImpl = new NaicsCategoryDaoImpl();
	categorymaster = categoryDaoImpl.listCategory(appDetails);
	session.setAttribute("categorymaster", categorymaster);

	if (result.equals("unique")) {
	    ActionErrors errors = new ActionErrors();
	    errors.add("naicsCategoryDesc", new ActionMessage(
		    "naicsCategoryDesc.unique"));
	    saveErrors(request, errors);
	    return mapping.getInputForward();
	}

	// For show successful category insertion message

	if (result.equals("success")) {

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("naicsCategoryDesc.ok");
	    messages.add("category", msg);
	    saveMessages(request, messages);
	    naicsCategoryForm.reset(mapping, request);
	}

	// forward to the category page
	return mapping.findForward("naicsMaster");
    }

    /**
     * Method to delete the category.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward deletecategory(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	NaicsCategoryDaoImpl categoryDaoImpl = new NaicsCategoryDaoImpl();

	String result = categoryDaoImpl.deleteCategory(
		Integer.parseInt(request.getParameter("id")), userDetails);

	// For show the List of categories
	if (result.equals("success")) {
	    List<NaicsCategory> categorymaster = null;
	    categorymaster = categoryDaoImpl.listCategory(userDetails);
	    session.setAttribute("categorymaster", categorymaster);

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("deletenaicscategory.ok");
	    messages.add("category", msg);
	    saveMessages(request, messages);
	}

	if (result.equals("failure")) {
	    List<NaicsCategory> categorymaster = null;
	    categorymaster = categoryDaoImpl.listCategory(userDetails);
	    session.setAttribute("categorymaster", categorymaster);

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage(
		    "deletenaicscategory.references");
	    messages.add("categoryreference", msg);
	    saveMessages(request, messages);
	}

	return mapping.findForward("deletecategory");
    }

    /**
     * Method to retrieve the selected category for update.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retrive(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();

	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer id = Integer.parseInt(request.getParameter("id").toString());
	NaicsCategoryDaoImpl categoryDaoImpl = new NaicsCategoryDaoImpl();
	NaicsCategoryForm categoryForm = (NaicsCategoryForm) form;
	NaicsCategory category = categoryDaoImpl.retriveCategory(id,
		userDetails);
	if (category != null) {
	    categoryForm.setId(category.getId());
	    categoryForm.setNaicsCategoryDesc(category.getNaicsCategoryDesc());
	    categoryForm.setWebServiceURL(category.getWebserivceURL());
	    categoryForm.setWebServiceParameters(category
		    .getWebserviceParameters());
	    categoryForm.setIsWebService(category.getIsWebService());
	    categoryForm.setIsActive(category.getIsActive());
	}

	// For show the List of categories
	List<NaicsCategory> categorymaster = null;
	categorymaster = categoryDaoImpl.listCategory(userDetails);
	session.setAttribute("categorymaster", categorymaster);
	return mapping.findForward("retrivecategory");
    }

    /**
     * Method to update the selected category.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updateCategory(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();

	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer currentUserId = (Integer) session.getAttribute("userId");

	NaicsCategoryDaoImpl categoryDaoImpl = new NaicsCategoryDaoImpl();
	NaicsCategoryForm categoryForm = (NaicsCategoryForm) form;

	String result = categoryDaoImpl.updateSelectedCategory(categoryForm,
		categoryForm.getId(), currentUserId, userDetails);

	// For show the List of categories
	List<NaicsCategory> categorymaster = null;
	if (result.equals("success")) {
	    categorymaster = categoryDaoImpl.listCategory(userDetails);
	    session.setAttribute("categorymaster", categorymaster);

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("updatenaicscategory.ok");
	    messages.add("category", msg);
	    saveMessages(request, messages);
	    categoryForm.reset(mapping, request);
	}
	return mapping.findForward("updatecategory");
    }
}