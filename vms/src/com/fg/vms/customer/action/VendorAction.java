package com.fg.vms.customer.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.json.simple.JSONArray;
import org.jsoup.Jsoup;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CustomerDivisionDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.SupplierDao;
import com.fg.vms.customer.dao.Tier2VendorDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.CustomerDivisionDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.SupplierDaoImpl;
import com.fg.vms.customer.dao.impl.Tier2VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.ClassificationDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.dto.SupplierDiversityDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerBusinessType;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerLegalStructure;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.CustomerVendorSelfRegInfo;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.SupplierDiversity;
import com.fg.vms.customer.model.Tier2ReportDirectExpenses;
import com.fg.vms.customer.model.Tier2ReportIndirectExpenses;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.CustomerServiceAreaInfo;
import com.fg.vms.customer.pojo.SupplierDiversityBean;
import com.fg.vms.customer.pojo.SupplierDiversityForm;
import com.fg.vms.customer.pojo.Tier2ReportDirectExpensesBean;
import com.fg.vms.customer.pojo.Tier2ReportIndirectExpensesBean;
import com.fg.vms.customer.pojo.Tier2ReportMasterBean;
import com.fg.vms.customer.pojo.VendorAddressInfo;
import com.fg.vms.customer.pojo.VendorBussinessArea;
import com.fg.vms.customer.pojo.VendorBussinessBioDetails;
import com.fg.vms.customer.pojo.VendorContactInfo;
import com.fg.vms.customer.pojo.VendorDetailsBean;
import com.fg.vms.customer.pojo.VendorDiverseCertificate;
import com.fg.vms.customer.pojo.VendorMasterBean;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.customer.pojo.VendorOwnerDetails;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.PasswordUtil;
import com.fg.vms.util.SendEmail;
import com.fg.vms.util.VendorUtil;

/**
 * Defined VendorAction class for Vendor details like save, edit, naics category
 * and sub category.
 * 
 * @author srinivasarao
 * 
 */
public class VendorAction extends DispatchAction {

	private Logger logger = Constants.logger;

	/**
	 * View vendor details.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		SearchDao searchDao = new SearchDaoImpl();
		VendorMasterForm vendorMasterForm = (VendorMasterForm) form;
		vendorMasterForm.reset(mapping, request);
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<CertifyingAgency> certificateAgencies;
		List<Certificate> certificates = null;
		List<Certificate> qualityCertificates = null;
		List<GeographicalStateRegionDto> serviceAreas = null;

		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificateAgencies = searchDao.listAgencies(appDetails);
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails);
		qualityCertificates = certificate.listOfCertificatesByType((byte) 0,
				appDetails);
		serviceAreas = regionDao.getServiceAreasList(appDetails);
		List<CustomerServiceArea> serviceAreaList = curdDao.list(appDetails,
				" from CustomerServiceArea ");

		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);
		session.setAttribute("qualityCertificates", qualityCertificates);
		session.setAttribute("serviceAreas", serviceAreas);
		session.setAttribute("serviceAreaList", serviceAreaList);

		WorkflowConfigurationDao workflowConfigurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration workflowConfiguration = workflowConfigurationDao
				.listWorkflowConfig(appDetails);
		int uploadRestriction = workflowConfiguration.getDocumentUpload();
		session.setAttribute("uploadRestriction", uploadRestriction);

		// populate countries list
		CURDDao<?> dao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countryList = (List<com.fg.vms.customer.model.Country>) dao
				.list(appDetails,
						" From Country where isactive=1 order by countryname");

		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) dao
				.list(appDetails,
						"From CustomerBusinessType where isactive=1 order by typeName");

		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) dao
				.list(appDetails,
						"From CustomerLegalStructure where isactive=1 order by name");

		vendorMasterForm.setLegalStructures(legalStructures);
		vendorMasterForm.setBusinessTypes(businessTypes);
		session.setAttribute("countryList", countryList);

		session.setAttribute("insuranceId", null);

		// populate states for configuring geographic regional data
		List<GeographicalStateRegionDto> stateRegionDtos = null;
		stateRegionDtos = regionDao.getStateByRegionList(appDetails);
		session.setAttribute("stateRegionDtos", stateRegionDtos);

		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));

		List<SecretQuestion> securityQnsList;
		securityQnsList = searchDao.listSecQns(appDetails);
		session.setAttribute("secretQnsList", securityQnsList);

		// Process for tire 2 vendor
		if (request.getParameter("tire2vendor") != null
				&& request.getParameter("tire2vendor").equalsIgnoreCase("yes")) {
			logger.info("Show Tire 2 vendor page");
			return mapping.findForward("tire2vendors");
		}

		return mapping.findForward("viewPage");
	}

	/**
	 * Create new tire2 vendor.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward createTire2Vendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;

		List<DiverseCertificationTypesDto> diverseCertifications = new ArrayList<DiverseCertificationTypesDto>();
		List<NaicsCodesDto> nacisCodes = new ArrayList<NaicsCodesDto>();

		if (getDeverseSupplier(vendorForm.getDeverseSupplier()) == (byte) 1) {

			diverseCertifications = CommonUtils.packageDiverseTypes(vendorForm);
		}

		nacisCodes = CommonUtils.packageNiacsCodes(vendorForm);

		Integer vendorId = vendorForm.getId();
		VendorContact currentVendor = (VendorContact) session
				.getAttribute("vendorUser");
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result;
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (vendorId != null && vendorId != 0) {
			result = vendorDao.mergeVendor(vendorForm, userId, vendorId,
					diverseCertifications, nacisCodes, userDetails,
					currentVendor, url);
		} else {
			result = vendorDao.saveVendor(vendorForm, userId,
					diverseCertifications, nacisCodes, userDetails, url,
					currentVendor);
		}
		if (null != result
				&& (result.equals("userEmail") || result.equals("failure"))) {
			// populate countries list
			CURDDao<?> dao = new CURDTemplateImpl();

			List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) dao
					.list(userDetails,
							"From CustomerBusinessType where isactive=1 order by typeName");

			List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) dao
					.list(userDetails,
							"From CustomerLegalStructure where isactive=1 order by name");
			if (null != vendorForm.getCountry()
					&& !vendorForm.getCountry().isEmpty()) {
				List<State> phyStates = (List<State>) dao.list(
						userDetails,
						" From State where countryid="
								+ vendorForm.getCountry());
				vendorForm.setPhyStates1(phyStates);
			}
			if (null != vendorForm.getCountry2()
					&& !vendorForm.getCountry2().isEmpty()) {
				List<State> mStates = (List<State>) dao.list(
						userDetails,
						" From State where countryid="
								+ vendorForm.getCountry2());
				vendorForm.setMailingStates2(mStates);
			}
			vendorForm.setLegalStructures(legalStructures);
			vendorForm.setBusinessTypes(businessTypes);
			vendorForm.setOtherExpiryDate(null);
			// vendorForm.setNaicsPrimary3("3");
			// vendorForm.setNaicsPrimary2("2");
			// vendorForm.setNaicsPrimary1("1");

			if (null != vendorForm.getCompanyOwnership()
					&& !vendorForm.getCompanyOwnership().isEmpty()) {
				vendorForm.setOwnerPublic("1");
				vendorForm.setOwnerPrivate("2");
				vendorForm
						.setCompanyOwnership(vendorForm.getCompanyOwnership());
			} else {
				vendorForm.setOwnerPublic("1");
				vendorForm.setOwnerPrivate("2");
				vendorForm.setCompanyOwnership(null);
			}
		}
		if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("tier2EmailId.unique"));
			vendorForm.setOtherExpiryDate(null);
			saveErrors(request, errors);
			if (request.getParameter("nonprime") != null) {
				return mapping.getInputForward();
			}
			return mapping.getInputForward();
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			vendorForm.setOtherExpiryDate(null);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);

			if (request.getParameter("nonprime") != null) {
				return mapping.getInputForward();
			}
			return mapping.getInputForward();
		} else if (result.equals("success")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.save.ok");
			messages.add("vendor", msg);
			vendorForm.reset(mapping, request);
			saveMessages(request, messages);

		} else if (result.equals("update")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.update.ok");
			messages.add("updateVendor", msg);
			// vendorForm.reset(mapping, request);
			saveMessages(request, messages);
			if (null != vendorForm.getMoreInfo()
					&& !vendorForm.getMoreInfo().isEmpty()
					&& vendorForm.getMoreInfo().equalsIgnoreCase("more")) {
				retriveMoreInfo(vendorForm, session, userDetails, vendorId);
				result = "moreinfo";
			} else if (request.getParameter("nonprime") != null) {
				vendorForm.setOtherExpiryDate(null);
				return mapping.findForward("nonprimevendor");
			}

		}
		return mapping.findForward(result);
	}

	/**
	 * Create new tire2 short vendor from tier2report.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveShortTier2Vendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		Tier2VendorDao tier2VendorDao = new Tier2VendorDaoImpl();

		VendorMasterForm vendorForm = (VendorMasterForm) form;
		String[] divCertificates = null;
		String address = "";
		String city = "";
		String contactPhone = "";
		String contanctEmail = "";
		String deverseSupplier = "";
		String firstName = "";
		String lastName = "";
		String middleName = "";
		String state = "";
		String vendorName = "";
		String zipcode = "";
		String country = "";

		if (request.getParameter("address1") != null
				&& !request.getParameter("address1").isEmpty()) {
			address = request.getParameter("address1");
		}
		if (request.getParameter("country") != null
				&& !request.getParameter("country").isEmpty()) {
			country = request.getParameter("country");
		}
		if (request.getParameter("divCertificates") != null
				&& request.getParameter("divCertificates").length() != 0) {
			divCertificates = request.getParameter("divCertificates")
					.split(",");
		}
		if (request.getParameter("city") != null
				&& !request.getParameter("city").isEmpty()) {
			city = request.getParameter("city");
		}
		if (request.getParameter("contactPhone") != null
				&& !request.getParameter("contactPhone").isEmpty()) {
			contactPhone = request.getParameter("contactPhone");
		}
		if (request.getParameter("contanctEmail") != null
				&& !request.getParameter("contanctEmail").isEmpty()) {
			contanctEmail = request.getParameter("contanctEmail");
		}
		if (request.getParameter("deverseSupplier") != null
				&& !request.getParameter("deverseSupplier").isEmpty()) {
			deverseSupplier = request.getParameter("deverseSupplier");
		}
		if (request.getParameter("firstName") != null
				&& !request.getParameter("firstName").isEmpty()) {
			firstName = request.getParameter("firstName");
		}
		if (request.getParameter("lastName") != null
				&& !request.getParameter("lastName").isEmpty()) {
			lastName = request.getParameter("lastName");
		}
		if (request.getParameter("middleName") != null
				&& !request.getParameter("middleName").isEmpty()) {
			middleName = request.getParameter("middleName");
		}
		if (request.getParameter("state") != null
				&& !request.getParameter("state").isEmpty()) {
			state = request.getParameter("state");
		}
		if (request.getParameter("vendorName") != null
				&& !request.getParameter("vendorName").isEmpty()) {
			vendorName = request.getParameter("vendorName");
		}
		if (request.getParameter("zipcode") != null
				&& !request.getParameter("zipcode").isEmpty()) {
			zipcode = request.getParameter("zipcode");
		}

		List<DiverseCertificationTypesDto> diverseCertifications = new ArrayList<DiverseCertificationTypesDto>();

		Integer vendorId = 1;
		VendorContact currentVendor = (VendorContact) session
				.getAttribute("vendorUser");
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String result;

		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		result = tier2VendorDao.saveShortTire2Vendor(vendorForm, userId,
				vendorId, diverseCertifications, null, currentVendor,
				userDetails, url, divCertificates, address, city, contactPhone,
				contanctEmail, deverseSupplier, firstName, lastName,
				middleName, state, vendorName, zipcode, country);
		if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("tier2EmailId.unique"));

			saveErrors(request, errors);
			if (request.getParameter("nonprime") != null) {
				return mapping.getInputForward();
			}
			return mapping.getInputForward();
		} else if (result.equals("failure")) { // if Transaction fails, return
												// to input page with message

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);

			if (request.getParameter("nonprime") != null) {
				return mapping.getInputForward();
			}
			return mapping.getInputForward();
		} else if (result.equals("success")) {
			session.setAttribute("lastAddedVendor", vendorName);
		}

		return mapping.findForward("createShortVendor");
	}

	/**
	 * Implementing the code for save and update the vendor information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward create(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String result;
		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		List<DiverseCertificationTypesDto> diverseCertifications = new ArrayList<DiverseCertificationTypesDto>();
		List<NaicsCodesDto> nacisCodes = new ArrayList<NaicsCodesDto>();

		if (getDeverseSupplier(vendorForm.getDeverseSupplier()) == (byte) 1) {

			diverseCertifications = CommonUtils.packageDiverseTypes(vendorForm);
		}

		nacisCodes = CommonUtils.packageNiacsCodes(vendorForm);

		VendorDao vendorDao = new VendorDaoImpl();
		Integer vendorId = vendorForm.getId();
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (vendorId != null && vendorId > 0) {
			result = vendorDao.mergeVendor(vendorForm, userId, vendorId,
					diverseCertifications, nacisCodes, userDetails, null, url);

		} else {
			result = vendorDao.saveVendor(vendorForm, userId,
					diverseCertifications, nacisCodes, userDetails, url, null);
		}

		if (null != result
				&& (result.equals("userEmail") || result.equals("vendorCode") || result
						.equals("failure"))) {
			// populate countries list
			CURDDao<?> dao = new CURDTemplateImpl();

			List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) dao
					.list(userDetails,
							"From CustomerBusinessType where isactive=1 order by typeName");

			List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) dao
					.list(userDetails,
							"From CustomerLegalStructure where isactive=1 order by name");

			if (null != vendorForm.getCountry()
					&& !vendorForm.getCountry().isEmpty()) {
				List<State> phyStates = (List<State>) dao.list(
						userDetails,
						" From State where countryid="
								+ vendorForm.getCountry());
				vendorForm.setPhyStates1(phyStates);
			}
			if (null != vendorForm.getCountry2()
					&& !vendorForm.getCountry2().isEmpty()) {
				List<State> mStates = (List<State>) dao.list(
						userDetails,
						" From State where countryid="
								+ vendorForm.getCountry2());
				vendorForm.setMailingStates2(mStates);
			}
			vendorForm.setLegalStructures(legalStructures);
			vendorForm.setBusinessTypes(businessTypes);
			vendorForm.setOtherExpiryDate(null);
			if (null != vendorForm.getVendorStatus()
					&& !vendorForm.getVendorStatus().isEmpty()) {
				CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
				List<StatusMaster> statusMasters = curdDao
						.list(userDetails,
								" From StatusMaster s where s.disporder >= ( select disporder from StatusMaster where id='"
										+ vendorForm.getVendorStatus()
										+ "' ) order by s.disporder");
				vendorForm.setStatusMasters(statusMasters);
			}
			// vendorForm.setNaicsPrimary3("3");
			// vendorForm.setNaicsPrimary2("2");
			// vendorForm.setNaicsPrimary1("1");

			if (null != vendorForm.getCompanyOwnership()
					&& !vendorForm.getCompanyOwnership().isEmpty()) {
				vendorForm.setOwnerPublic("1");
				vendorForm.setOwnerPrivate("2");
				vendorForm
						.setCompanyOwnership(vendorForm.getCompanyOwnership());
			} else {
				vendorForm.setOwnerPublic("1");
				vendorForm.setOwnerPrivate("2");
				vendorForm.setCompanyOwnership(null);
			}
		}
		if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("tier2EmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("vendorCode")) {
			ActionErrors errors = new ActionErrors();
			errors.add("vendorCode", new ActionMessage("err.vendor.code"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.save.ok");
			messages.add("vendor", msg);
			vendorForm.reset(mapping, request);
			saveMessages(request, messages);
		} else if (result.equals("update")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.update.ok");
			messages.add("updateVendor", msg);
			// vendorForm.reset(mapping, request);
			saveMessages(request, messages);
			if (null != vendorForm.getMoreInfo()
					&& !vendorForm.getMoreInfo().isEmpty()
					&& vendorForm.getMoreInfo().equalsIgnoreCase("more")) {
				retriveMoreInfo(vendorForm, session, userDetails, vendorId);
				result = "moreinfo";
			}
		}
		return mapping.findForward(result);
	}

	/**
	 * Display the tire2 vendor details.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward displayTier2Vendors(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		List<VendorMaster> vendorMaster = null;
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Tier2VendorDao tier2VendorDao = new Tier2VendorDaoImpl();
		SearchDao searchDao = new SearchDaoImpl();
		vendorMaster = tier2VendorDao.tire2Vendors(appDetails);

		List<CertifyingAgency> certificateAgencies;
		List<Certificate> certificates = null;

		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificateAgencies = searchDao.listAgencies(appDetails);
		certificates = certificate.listOfCertificates(appDetails);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);
		session.setAttribute("vendorsList", vendorMaster);

		return mapping.findForward("viewvendors");
	}

	/**
	 * Retrive the vendor Information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retriveVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		VendorMasterForm vendorMasterForm = (VendorMasterForm) form;

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorId = Integer.parseInt(request.getParameter("id")
				.toString());
		showVendorById(vendorMasterForm, appDetails, session, vendorId);
		return mapping.findForward("editvendor");
	}

	public void showVendorById(VendorMasterForm vendorMasterForm,
			UserDetailsDto appDetails, HttpSession session, Integer vendorId) {
		VendorDao vendorDao = new VendorDaoImpl();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		SearchDao searchDao = new SearchDaoImpl();
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		// load company type and password security questions list
		List<CertifyingAgency> certificateAgencies;
		certificateAgencies = searchDao.listAgencies(appDetails);

		List<SecretQuestion> securityQnsList;
		securityQnsList = searchDao.listSecQns(appDetails);

		// populate countries list
		CURDDao<?> commonDao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countries = (List<Country>) commonDao
				.list(appDetails,
						" From Country where isactive=1 order by countryname");
		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) commonDao
				.list(appDetails,
						"From CustomerBusinessType where isactive=1 order by typeName");
		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) commonDao
				.list(appDetails,
						"From CustomerLegalStructure where isactive=1 order by name");

		vendorMasterForm.setLegalStructures(legalStructures);
		vendorMasterForm.setBusinessTypes(businessTypes);

		RetriveVendorInfoDto vendorInfo = null;

		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCetrificate = null;
		// List<NaicsMaster> naicsMasters = null;
		List<VendorOtherCertificate> otherCertificate = null;
		// List<GeographicalStateRegionDto> serviceAreas = null;
		List<CustomerVendorCommodity> vendorCommodities = null;

		session.setAttribute("vendorId", vendorId);

		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, appDetails);

		vendorMaster = vendorInfo.getVendorMaster();
		vendorCetrificate = vendorInfo.getVendorCertificate();
		otherCertificate = vendorInfo.getVendorOtherCert();

		if (vendorMaster.getVendorStatus() != null
				&& !vendorMaster.getVendorStatus().isEmpty()) {
			CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
			List<StatusMaster> statusMasters = curdDao
					.list(appDetails,
							" From StatusMaster s where s.disporder >= ( select disporder from StatusMaster where id='"
									+ vendorMaster.getVendorStatus()
									+ "' ) order by s.disporder");
			vendorMasterForm.setStatusMasters(statusMasters);
			// VendorUtil.packVendorStatus(vendorMasterForm,
			// vendorMaster.getId(), );
		}

		// Get document upload restriction details.
		List<VendorDocuments> documents = null;
		documents = documentsDao.getVendorDocuments(vendorId, appDetails);
		session.setAttribute("vendorDocuments", documents);

		// Show the list of Vendor Commodities.
		vendorCommodities = documentsDao.getVendorCommodities(vendorId,
				appDetails);

		CURDDao<State> dao = new CURDTemplateImpl<State>();

		vendorMasterForm = VendorUtil.packVendorInfomation(vendorMasterForm,
				vendorInfo, vendorId, vendorMaster, vendorCetrificate,
				appDetails);
		// session.setAttribute("vendorId", vendorId);
		session.setAttribute("vendorInfoDto", vendorInfo);
		session.setAttribute("secretQnsList", securityQnsList);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("otherCertificates", otherCertificate);
		// session.setAttribute("serviceAreas", serviceAreas);
		session.setAttribute("vendorCommodities", vendorCommodities);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));

		List<Certificate> qualityCertificates = null;

		List<Certificate> certificates = null;
		// List<CustomerVendorbusinessbiographysafety> safeties = null;
		// safeties = vendorDao.getBiography(vendorId, appDetails);

		CertificateDaoImpl certificate = new CertificateDaoImpl();
		// certificateAgencies = searchDao.listAgencies(appDetails);
		// certificates = certificate.listOfCertificates(appDetails);
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails);
		session.setAttribute("countryList", countries);
		session.setAttribute("certificateTypes", certificates);
		qualityCertificates = certificate.listOfCertificatesByType((byte) 0,
				appDetails);
		session.setAttribute("qualityCertificates", qualityCertificates);

		List<GeographicalStateRegionDto> serviceAreas1 = null;
		serviceAreas1 = regionDao.getServiceAreasList(appDetails);
		session.setAttribute("serviceAreas1", serviceAreas1);

		CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
		CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
		List<CustomerServiceArea> serviceAreaList = curdDao.list(appDetails,
				" from CustomerServiceArea ");
		session.setAttribute("serviceAreaList", serviceAreaList);

		WorkflowConfigurationDao workflowConfigurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration workflowConfiguration = workflowConfigurationDao
				.listWorkflowConfig(appDetails);
		int uploadRestriction = workflowConfiguration.getDocumentUpload();
		session.setAttribute("uploadRestriction", uploadRestriction);

		WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration configuration = configurationDao
				.listWorkflowConfig(appDetails);
		vendorMasterForm.setConfiguration(configuration);
		vendorMasterForm.setId(vendorId);
		vendorMasterForm.setWflog(vendorInfo.getCustomerWflog());

		CustomerVendorAddressMaster phyAddress = documentsDao
				.getMailingAddress(vendorId, "p", appDetails);
		if (phyAddress != null) {
			vendorMasterForm.setAddress1(phyAddress.getAddress());
			vendorMasterForm.setCity(phyAddress.getCity());
			vendorMasterForm.setState(phyAddress.getState());
			vendorMasterForm.setProvince(phyAddress.getProvince());
			vendorMasterForm.setRegion(phyAddress.getRegion());
			vendorMasterForm.setCountry(phyAddress.getCountry());
			vendorMasterForm.setZipcode(phyAddress.getZipCode());
			vendorMasterForm.setMobile(phyAddress.getMobile());
			vendorMasterForm.setPhone(phyAddress.getPhone());
			vendorMasterForm.setFax(phyAddress.getFax());
			List<State> phyStates = dao.list(appDetails,
					" From State where countryid=" + phyAddress.getCountry());
			vendorMasterForm.setPhyStates1(phyStates);
		}

		CustomerVendorAddressMaster addressMaster = documentsDao
				.getMailingAddress(vendorId, "m", appDetails);
		if (addressMaster != null) {
			vendorMasterForm.setAddress2(addressMaster.getAddress());
			vendorMasterForm.setCity2(addressMaster.getCity());
			vendorMasterForm.setState2(addressMaster.getState());
			vendorMasterForm.setProvince2(addressMaster.getProvince());
			vendorMasterForm.setRegion2(addressMaster.getRegion());
			vendorMasterForm.setCountry2(addressMaster.getCountry());
			vendorMasterForm.setZipcode2(addressMaster.getZipCode());
			vendorMasterForm.setMobile2(addressMaster.getMobile());
			vendorMasterForm.setPhone2(addressMaster.getPhone());
			vendorMasterForm.setFax2(addressMaster.getFax());
			List<State> mStates = dao
					.list(appDetails, " From State where countryid="
							+ addressMaster.getCountry());
			vendorMasterForm.setMailingStates2(mStates);
		}

		// vendorMasterForm = VendorUtil.packBioGraphySafety(vendorMasterForm,
		// vendorId, appDetails);

		// Get vendor diverse classification list
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(appDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);
		List<VendorBusinessArea> businessAreas = curdDao1.list(appDetails,
				"from VendorBusinessArea where isActive = 1 and vendorId="
						+ vendorId);
		CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
		List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
				appDetails,
				"from CustomerVendorServiceArea where isActive = 1 and vendorId="
						+ vendorId);
		vendorMasterForm = VendorUtil.packVendorServiceAreas(vendorMasterForm,
				serviceAreas, vendorId);
		// Get vendor business area details
		vendorMasterForm = VendorUtil.packVendorBusinessAreas(vendorMasterForm,
				businessAreas, vendorId);

		// Certificate Type
		CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypesList = curdDao2.list(
				appDetails, " from CustomerCertificateType ");
		session.setAttribute("certificateTypesList", certificateTypesList);

		CURDDao<State> dao1 = new CURDTemplateImpl<State>();
		List<State> contactStates = dao1.list(appDetails,
				" From State order by statename");
		session.setAttribute("contactStates", contactStates);

		// Customer Meeting Info
		CURDDao<CustomerVendorMeetingInfo> curdDao3 = new CURDTemplateImpl<CustomerVendorMeetingInfo>();
		List<CustomerVendorMeetingInfo> customerMeetinginfoList = curdDao3
				.list(appDetails,
						"from CustomerVendorMeetingInfo where vendorId="
								+ vendorId);

		if (customerMeetinginfoList != null
				&& customerMeetinginfoList.size() > 0) {
			CustomerVendorMeetingInfo meetingInfo = customerMeetinginfoList
					.get(0);
			vendorMasterForm.setContactFirstName(meetingInfo
					.getContactFirstName());
			vendorMasterForm.setContactLastName(meetingInfo
					.getContactLastName());
			vendorMasterForm.setContactDate(CommonUtils
					.convertDateToString(meetingInfo.getContactDate()));
			vendorMasterForm.setContactState(meetingInfo.getContactState());
			vendorMasterForm.setContactEvent(meetingInfo.getContactEvent());
		}

	}

	public void retriveMoreInfo(VendorMasterForm vendorMasterForm,
			HttpSession session, UserDetailsDto appDetails, Integer vendorId)
			throws Exception {
		VendorDao vendorDao = new VendorDaoImpl();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		List<VendorDocuments> documents = null;
		session.setAttribute("vendorId", vendorId);

		CURDDao<VendorMaster> curdDao = new CURDTemplateImpl<VendorMaster>();

		VendorMaster master = curdDao.find(appDetails,
				" from VendorMaster vm where id=" + vendorId);
		if (master != null && master.getVendorName() != null) {
			vendorMasterForm.setVendorName(master.getVendorName());
		}
		if (master.getPrimeNonPrimeVendor() != null) {
			vendorMasterForm.setPrimeNonPrimeVendor(master
					.getPrimeNonPrimeVendor().toString());
		}
		vendorMasterForm.setIsApproved(master.getIsApproved());
		vendorMasterForm.setVendorStatus(master.getVendorStatus());
		documents = documentsDao.getVendorDocuments(vendorId, appDetails);

		session.setAttribute("vendorDocuments", documents);
		session.setAttribute("countryList", countries);

		WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration configuration = configurationDao
				.listWorkflowConfig(appDetails);
		int uploadRestriction = configuration.getDocumentUpload();
		session.setAttribute("uploadRestriction", uploadRestriction);
		vendorMasterForm.setConfiguration(configuration);
		vendorMasterForm.setId(vendorId);

		vendorMasterForm = VendorUtil.packBioGraphySafety(vendorMasterForm,
				vendorId, appDetails);

		List<CustomerVendorreference> vendorreferences = vendorDao
				.getListReferences(vendorId, appDetails);
		vendorMasterForm = VendorUtil.packVendorReference(vendorMasterForm,
				vendorreferences, appDetails, null);

	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward retriveNonPrimeVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		VendorMasterForm vendorMasterForm = (VendorMasterForm) form;
		vendorMasterForm.reset(mapping, request);
		VendorDao vendorDao = new VendorDaoImpl();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		SearchDao searchDao = new SearchDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// load company type and password security questions list
		List<CertifyingAgency> certificateAgencies;
		certificateAgencies = searchDao.listAgencies(appDetails);

		List<SecretQuestion> securityQnsList;
		securityQnsList = searchDao.listSecQns(appDetails);

		// populate countries list
		CURDDao<?> cdao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countries = (List<Country>) cdao
				.list(appDetails,
						" From Country where isactive=1 order by countryname");

		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
				.list(appDetails,
						"From CustomerBusinessType where isactive=1 order by typeName");
		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
				.list(appDetails,
						"From CustomerLegalStructure where isactive=1 order by name");

		vendorMasterForm.setLegalStructures(legalStructures);
		vendorMasterForm.setBusinessTypes(businessTypes);
		RetriveVendorInfoDto vendorInfo = null;

		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCetrificate = null;
		// List<NaicsMaster> naicsMasters = null;
		List<VendorOtherCertificate> otherCertificate = null;
		List<VendorDocuments> documents = null;
		// List<GeographicalStateRegionDto> serviceAreas = null;
		List<CustomerVendorCommodity> vendorCommodities = null;

		VendorContact contact = (VendorContact) session
				.getAttribute("vendorUser");

		Integer vendorId = contact.getVendorId().getId();

		session.setAttribute("vendorId", vendorId);

		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, appDetails);

		vendorMaster = vendorInfo.getVendorMaster();
		vendorCetrificate = vendorInfo.getVendorCertificate();
		otherCertificate = vendorInfo.getVendorOtherCert();
		documents = documentsDao.getVendorDocuments(vendorId, appDetails);

		CURDDao<State> dao = new CURDTemplateImpl<State>();
		// Show the list of Vendor Commodities.
		vendorCommodities = documentsDao.getVendorCommodities(vendorId,
				appDetails);

		vendorMasterForm = VendorUtil.packVendorInfomation(vendorMasterForm,
				vendorInfo, vendorId, vendorMaster, vendorCetrificate,
				appDetails);
		// session.setAttribute("vendorId", vendorId);
		session.setAttribute("vendorInfoDto", vendorInfo);
		session.setAttribute("secretQnsList", securityQnsList);
		session.setAttribute("certAgencyList", certificateAgencies);
		if (null != otherCertificate && !otherCertificate.isEmpty()) {
			session.setAttribute("otherCertificates", otherCertificate);
		} else {
			session.setAttribute("otherCertificates", null);
		}
		vendorMasterForm.setOtherExpiryDate(null);
		session.setAttribute("vendorDocuments", documents);
		// session.setAttribute("serviceAreas", serviceAreas);
		session.setAttribute("vendorCommodities", vendorCommodities);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));

		List<Certificate> qualityCertificates = null;

		List<Certificate> certificates = null;

		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificateAgencies = searchDao.listAgencies(appDetails);
		// certificates = certificate.listOfCertificates(appDetails);
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails);
		session.setAttribute("countryList", countries);
		session.setAttribute("certificateTypes", certificates);
		qualityCertificates = certificate.listOfCertificatesByType((byte) 0,
				appDetails);
		session.setAttribute("qualityCertificates", qualityCertificates);

		WorkflowConfigurationDao workflowConfigurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration workflowConfiguration = workflowConfigurationDao
				.listWorkflowConfig(appDetails);
		int uploadRestriction = workflowConfiguration.getDocumentUpload();
		session.setAttribute("uploadRestriction", uploadRestriction);

		WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration configuration = configurationDao
				.listWorkflowConfig(appDetails);
		vendorMasterForm.setConfiguration(configuration);
		vendorMasterForm.setId(vendorId);
		vendorMasterForm.setWflog(vendorInfo.getCustomerWflog());
		CustomerVendorAddressMaster phyAddress = documentsDao
				.getMailingAddress(vendorId, "p", appDetails);
		if (phyAddress != null) {
			vendorMasterForm.setAddress1(phyAddress.getAddress());
			vendorMasterForm.setCity(phyAddress.getCity());
			vendorMasterForm.setState(phyAddress.getState());
			vendorMasterForm.setProvince(phyAddress.getProvince());
			vendorMasterForm.setRegion(phyAddress.getRegion());
			vendorMasterForm.setCountry(phyAddress.getCountry());
			vendorMasterForm.setZipcode(phyAddress.getZipCode());
			vendorMasterForm.setMobile(phyAddress.getMobile());
			vendorMasterForm.setPhone(phyAddress.getPhone());
			vendorMasterForm.setFax(phyAddress.getFax());
			List<State> phyStates = dao.list(appDetails,
					" From State where countryid=" + phyAddress.getCountry());
			vendorMasterForm.setPhyStates1(phyStates);
		}

		CustomerVendorAddressMaster addressMaster = documentsDao
				.getMailingAddress(vendorId, "m", appDetails);
		if (addressMaster != null) {
			vendorMasterForm.setAddress2(addressMaster.getAddress());
			vendorMasterForm.setCity2(addressMaster.getCity());
			vendorMasterForm.setState2(addressMaster.getState());
			vendorMasterForm.setProvince2(addressMaster.getProvince());
			vendorMasterForm.setRegion2(addressMaster.getRegion());
			vendorMasterForm.setCountry2(addressMaster.getCountry());
			vendorMasterForm.setZipcode2(addressMaster.getZipCode());
			vendorMasterForm.setMobile2(addressMaster.getMobile());
			vendorMasterForm.setPhone2(addressMaster.getPhone());
			vendorMasterForm.setFax2(addressMaster.getFax());
			List<State> mStates = dao
					.list(appDetails, " From State where countryid="
							+ addressMaster.getCountry());
			vendorMasterForm.setMailingStates2(mStates);
		}

		// Get vendor diverse classification list
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(appDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);

		CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
		CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
		List<CustomerServiceArea> serviceAreaList = curdDao.list(appDetails,
				" from CustomerServiceArea ");
		session.setAttribute("serviceAreaList", serviceAreaList);
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<GeographicalStateRegionDto> serviceAreas1 = null;
		serviceAreas1 = regionDao.getServiceAreasList(appDetails);
		session.setAttribute("serviceAreas1", serviceAreas1);
		List<VendorBusinessArea> businessAreas = curdDao1.list(appDetails,
				"from VendorBusinessArea where isActive = 1 and vendorId="
						+ vendorId);
		// Get vendor business area details
		vendorMasterForm = VendorUtil.packVendorBusinessAreas(vendorMasterForm,
				businessAreas, vendorId);
		CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
		List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
				appDetails,
				"from CustomerVendorServiceArea where isActive = 1 and vendorId="
						+ vendorId);
		vendorMasterForm = VendorUtil.packVendorServiceAreas(vendorMasterForm,
				serviceAreas, vendorId);
		// Certificate Type
		CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypesList = curdDao2.list(
				appDetails, " from CustomerCertificateType ");
		session.setAttribute("certificateTypesList", certificateTypesList);
		return mapping.findForward("editvendor");
	}

	public ActionForward saveMoreInfo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String result = null;
		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId");
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;

		VendorDao vendorDao = new VendorDaoImpl();
		Integer vendorId = vendorForm.getId();
		if (vendorId != null && vendorId > 0) {
			result = vendorDao.mergeMoreInfo(vendorForm, userId, vendorId,
					userDetails, null);
		}

		if (result.equals("failure")) {
			mapping.getInputForward();
		} else {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.save.ok");
			messages.add("vendor", msg);
			// vendorForm.reset(mapping, request);
			saveMessages(request, messages);
			result = "success";
			if (null != vendorForm.getMoreInfo()
					&& !vendorForm.getMoreInfo().isEmpty()
					&& vendorForm.getMoreInfo().equalsIgnoreCase("back")) {
				showVendorById(vendorForm, userDetails, session, vendorId);
				result = "editvendor";
			} else if (null != vendorForm.getMoreInfo()
					&& !vendorForm.getMoreInfo().isEmpty()
					&& vendorForm.getMoreInfo().equalsIgnoreCase(
							"editsubvendor")) {
				showVendorById(vendorForm, userDetails, session, vendorId);
				result = "editsubvendor";
			} else if (null != vendorForm.getMoreInfo()
					&& !vendorForm.getMoreInfo().isEmpty()
					&& vendorForm.getMoreInfo().equalsIgnoreCase(
							"nonprimevendor")) {
				showVendorById(vendorForm, userDetails, session, vendorId);
				result = "nonprimevendor";
			} else if (null != vendorForm.getMoreInfo()
					&& !vendorForm.getMoreInfo().isEmpty()
					&& vendorForm.getMoreInfo().equalsIgnoreCase("primevendor")) {
				showVendorById(vendorForm, userDetails, session, vendorId);
				result = "editvendor";
			}
		}
		return mapping.findForward(result);
	}

	/**
	 * Retrive the Tier2 vendor Information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retriveT2Vendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		VendorMasterForm vendorMasterForm = (VendorMasterForm) form;
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorId = Integer.parseInt(request.getParameter("id")
				.toString());
		showT2Vendor(vendorMasterForm, appDetails, session, vendorId);
		return mapping.findForward("editsubvendor");
	}

	public void showT2Vendor(VendorMasterForm vendorMasterForm,
			UserDetailsDto appDetails, HttpSession session, Integer vendorId) {
		VendorDao vendorDao = new VendorDaoImpl();
		SearchDao searchDao = new SearchDaoImpl();
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();

		// load company type and password security questions list
		List<CertifyingAgency> certificateAgencies;
		certificateAgencies = searchDao.listAgencies(appDetails);

		List<SecretQuestion> securityQnsList;
		securityQnsList = searchDao.listSecQns(appDetails);

		// populate countries list
		CURDDao<?> cdao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countries = (List<Country>) cdao
				.list(appDetails,
						" From Country where isactive=1 order by countryname");
		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
				.list(appDetails,
						"From CustomerBusinessType where isactive=1 order by typeName");
		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
				.list(appDetails,
						"From CustomerLegalStructure where isactive=1 order by name");

		vendorMasterForm.setLegalStructures(legalStructures);
		vendorMasterForm.setBusinessTypes(businessTypes);
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCetrificate = null;
		// List<NaicsMaster> naicsMasters = null;
		List<VendorOtherCertificate> otherCertificate = null;
		// List<GeographicalStateRegionDto> serviceAreas = null;
		List<CustomerVendorCommodity> vendorCommodities = null;

		session.setAttribute("vendorId", vendorId);

		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, appDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCetrificate = vendorInfo.getVendorCertificate();

		otherCertificate = vendorInfo.getVendorOtherCert();
		vendorMasterForm = VendorUtil.packVendorInfomation(vendorMasterForm,
				vendorInfo, vendorId, vendorMaster, vendorCetrificate,
				appDetails);
		vendorMasterForm.setId(vendorId);
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		// Show the list of Vendor service Areas.
		// serviceAreas = documentsDao.getVendorServiceAreas(vendorId,
		// appDetails);

		// Show the list of Vendor Commodities.
		vendorCommodities = documentsDao.getVendorCommodities(vendorId,
				appDetails);

		// session.setAttribute("vendorId", vendorId);
		session.setAttribute("vendorInfoDto", vendorInfo);
		session.setAttribute("secretQnsList", securityQnsList);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("otherCertificates", otherCertificate);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));
		List<Certificate> certificates = null;
		List<Certificate> qualityCertificates = null;
		// List<VendorDocuments> documents = null;
		// documents = documentsDao.getVendorDocuments(vendorId, appDetails);
		// session.setAttribute("vendorDocuments", documents);
		// session.setAttribute("serviceAreas", serviceAreas);
		session.setAttribute("vendorCommodities", vendorCommodities);

		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificateAgencies = searchDao.listAgencies(appDetails);
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails);
		session.setAttribute("countryList", countries);
		session.setAttribute("certificateTypes", certificates);
		qualityCertificates = certificate.listOfCertificatesByType((byte) 0,
				appDetails);
		session.setAttribute("qualityCertificates", qualityCertificates);

		CustomerVendorAddressMaster phyAddress = documentsDao
				.getMailingAddress(vendorId, "p", appDetails);
		if (phyAddress != null) {
			vendorMasterForm.setAddress1(phyAddress.getAddress());
			vendorMasterForm.setCity(phyAddress.getCity());
			vendorMasterForm.setState(phyAddress.getState());
			vendorMasterForm.setProvince(phyAddress.getProvince());
			vendorMasterForm.setRegion(phyAddress.getRegion());
			vendorMasterForm.setCountry(phyAddress.getCountry());
			vendorMasterForm.setZipcode(phyAddress.getZipCode());
			vendorMasterForm.setMobile(phyAddress.getMobile());
			vendorMasterForm.setPhone(phyAddress.getPhone());
			vendorMasterForm.setFax(phyAddress.getFax());
			List<State> phyStates = dao.list(appDetails,
					" From State where countryid=" + phyAddress.getCountry());
			vendorMasterForm.setPhyStates1(phyStates);
		}

		CustomerVendorAddressMaster addressMaster = documentsDao
				.getMailingAddress(vendorId, "m", appDetails);
		if (addressMaster != null) {
			vendorMasterForm.setAddress2(addressMaster.getAddress());
			vendorMasterForm.setCity2(addressMaster.getCity());
			vendorMasterForm.setState2(addressMaster.getState());
			vendorMasterForm.setProvince2(addressMaster.getProvince());
			vendorMasterForm.setRegion2(addressMaster.getRegion());
			vendorMasterForm.setCountry2(addressMaster.getCountry());
			vendorMasterForm.setZipcode2(addressMaster.getZipCode());
			vendorMasterForm.setMobile2(addressMaster.getMobile());
			vendorMasterForm.setPhone2(addressMaster.getPhone());
			vendorMasterForm.setFax2(addressMaster.getFax());
			List<State> mStates = dao
					.list(appDetails, " From State where countryid="
							+ addressMaster.getCountry());
			vendorMasterForm.setMailingStates2(mStates);
		}

		// Get vendor diverse classification list
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(appDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);

		CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
		CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
		List<CustomerServiceArea> serviceAreaList = curdDao.list(appDetails,
				" from CustomerServiceArea ");
		session.setAttribute("serviceAreaList", serviceAreaList);

		List<GeographicalStateRegionDto> serviceAreas1 = null;
		serviceAreas1 = regionDao.getServiceAreasList(appDetails);
		session.setAttribute("serviceAreas1", serviceAreas1);

		List<VendorBusinessArea> businessAreas = curdDao1.list(appDetails,
				"from VendorBusinessArea where isActive = 1 and vendorId="
						+ vendorId);
		// Get vendor business area details
		vendorMasterForm = VendorUtil.packVendorBusinessAreas(vendorMasterForm,
				businessAreas, vendorId);
		CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
		List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
				appDetails,
				"from CustomerVendorServiceArea where isActive = 1 and vendorId="
						+ vendorId);
		vendorMasterForm = VendorUtil.packVendorServiceAreas(vendorMasterForm,
				serviceAreas, vendorId);
		// Certificate Type
		CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypesList = curdDao2.list(
				appDetails, " from CustomerCertificateType ");
		session.setAttribute("certificateTypesList", certificateTypesList);

	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private Byte getDeverseSupplier(String string) {

		if (string != null && string.length() != 0) {
			if (string.equalsIgnoreCase("on")) {
				return (byte) 1;
			} else {
				return (byte) 0;
			}
		} else {
			return (byte) 0;
		}

	}

	/**
	 * Delete Vendor document based on id.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteVendorDoc(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		Integer vendorId = null;
		if (null != session.getAttribute("supplierId")) {
			vendorId = Integer.parseInt(session.getAttribute("supplierId")
					.toString());
		} else if (null != session.getAttribute("vendorId")) {
			vendorId = Integer.parseInt(session.getAttribute("vendorId")
					.toString());
		}
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		int docId = Integer.parseInt(request.getParameter("id").toString());
		String deletedFile = documentsDao.deleteDocument(docId, appDetails);

		CommonUtils.deleteDocument(deletedFile, vendorId);

		return mapping.findForward("success");
	}

	/**
	 * Delete Vendor service area based on id.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteArea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		// Integer vendorId = Integer.parseInt(session.getAttribute("vendorId")
		// .toString());
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer serviceAreaId = Integer.parseInt(request.getParameter("id")
				.toString());
		documentsDao.deleteServiceArea(serviceAreaId, appDetails);

		return mapping.findForward("success");
	}

	/**
	 * Delete Vendor commodity based on id.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteCommodity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer commodityId = Integer.parseInt(request.getParameter("id")
				.toString());
		documentsDao.deleteCommodity(commodityId, appDetails);

		return mapping.findForward("success");
	}

	/**
	 * Retrieve self registration.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward retrieveSavedData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorDao vendorDao = new VendorDaoImpl();
		Decrypt decrypt = new Decrypt();
		String decryptedPassword = null;
		SearchDao searchDao = new SearchDaoImpl();

		// To Display Privacy and Terms Dialog Box on the first Page.
		request.setAttribute("showPrivacyTerms", "showPrivacyTerms");

		// load company type and password security questions list
		List<SecretQuestion> securityQnsList;
		VendorContact contact = null;
		List<VendorContact> businessContacts = null;
		CustomerVendorSelfRegInfo selfRegInfo = null;
		if (null != vendorForm.getContanctEmail()
				&& vendorForm.getContanctEmail().isEmpty()
				&& session.getAttribute("tempLoginEmail") != null) {
			contact = vendorDao.getSavedVendorData(
					session.getAttribute("tempLoginEmail").toString(),
					appDetails);
		} else {

			CURDDao<CustomerVendorSelfRegInfo> curdDaos = new CURDTemplateImpl<CustomerVendorSelfRegInfo>();
			List<CustomerVendorSelfRegInfo> selfRegInfos = curdDaos.list(
					appDetails,
					" from CustomerVendorSelfRegInfo where emailId = '"
							+ vendorForm.getContanctEmail()
							+ "' and vendorId is null");
			if (selfRegInfos != null && !selfRegInfos.isEmpty()) {
				selfRegInfo = selfRegInfos.get(0);
			}
			if (null == selfRegInfo) {
				contact = vendorDao.getSavedVendorData(
						vendorForm.getContanctEmail(), appDetails);
			}
		}
		if (contact != null && contact.getEmailId() != null
				&& !contact.getEmailId().isEmpty()
				&& contact.getPassword() != null
				&& !contact.getPassword().isEmpty()) {

			// Setting Workflow Configuration in the Session to Access in the
			// Whole Application
			WorkflowConfigurationDaoImpl configurationDaoImpl = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration workflowConfigurations = configurationDaoImpl
					.listWorkflowConfig(appDetails);
			appDetails.setWorkflowConfiguration(workflowConfigurations);

			VendorMaster master = vendorDao.getSavedVendorMasterData(contact
					.getVendorId().getId(), appDetails);

			if (master != null && master.getIsPartiallySubmitted() != null
					&& master.getIsPartiallySubmitted().equalsIgnoreCase("Yes")) {

				businessContacts = vendorDao.getBusinessContactDetails(
						master.getId(), appDetails);
				Integer keyvalue = contact.getKeyValue();
				decryptedPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						contact.getPassword());

				com.fg.vms.customer.model.CustomerApplicationSettings settings = (com.fg.vms.customer.model.CustomerApplicationSettings) session
						.getAttribute("isDivisionStatus");
				if (settings.getIsDivision() != null
						&& settings.getIsDivision() != 0) {
					if (master.getCustomerDivisionId() != null
							&& master.getCustomerDivisionId().getId() != null) {
						session.setAttribute("divisionId", master
								.getCustomerDivisionId().getId());
						appDetails.setCustomerDivisionID((Integer) session
								.getAttribute("divisionId"));

						// To set customer division isGlobal or not in
						// userDetails
						if (master.getCustomerDivisionId().getIsGlobal() != null
								&& master.getCustomerDivisionId().getIsGlobal() != 0)
							appDetails.setIsGlobalDivision(Integer
									.valueOf(master.getCustomerDivisionId()
											.getIsGlobal()));
						else
							appDetails.setIsGlobalDivision(0);
					}
				}

				boolean formFlag = false;
				if (session.getAttribute("tempLoginEmail") == null) {
					if (vendorForm.getLoginpassword() != null
							&& vendorForm.getLoginpassword().equals(
									decryptedPassword)) {
						session.setAttribute("tempLoginEmail",
								contact.getEmailId());
						formFlag = true;
					}
				} else {
					formFlag = true;
				}
				if (formFlag) {
					securityQnsList = searchDao.listSecQns(appDetails);
					vendorForm.reset(mapping, request);
					// populate countries list
					CURDDao<?> cdao = new CURDTemplateImpl();
					String query = "From Country where isactive=1 order by countryname";
					if (appDetails.getWorkflowConfiguration() != null) {
						if (appDetails.getWorkflowConfiguration()
								.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							query = "From Country where isactive=1 and isDefault=1 order by countryname";
						}
					}
					List<com.fg.vms.customer.model.Country> countries = (List<Country>) cdao
							.list(appDetails, query);
					session.setAttribute("countryList", countries);

					Integer countryId = null;
					List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) cdao
							.list(appDetails, "From Country where isDefault=1");
					countryId = country.get(0).getId();

					List<State> statesList = null;
					if (appDetails.getWorkflowConfiguration() != null) {
						if (appDetails.getWorkflowConfiguration()
								.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							statesList = (List<State>) cdao
									.list(appDetails,
											"From State where countryid="
													+ countryId
													+ " and isactive=1 order by statename");
						}
					}
					session.setAttribute("statesList", statesList);

					Integer vendorId = contact.getVendorId().getId();
					session.setAttribute("vendorId", vendorId);
					vendorForm.setOtherExpiryDate(null);

					// Always Display US States as Default Contact States.
					country = (List<Country>) cdao.list(appDetails,
							"From Country where countryname = 'United States'");
					countryId = country.get(0).getId();
					CURDDao<State> dao = new CURDTemplateImpl<State>();
					List<State> contactStates = dao.list(appDetails,
							"From State where countryid=" + countryId
									+ " and isactive=1 order by statename");
					vendorForm.setContactStates(contactStates);

					vendorForm.setId(vendorId);
					vendorForm = VendorUtil.packVendorContactInfomation(
							vendorForm, contact);
					vendorForm.setCompanytype(master.getCompanyType());
					if (businessContacts != null) {
						VendorContact contact1 = businessContacts.get(0);
						vendorForm = VendorUtil
								.packVendorBusinessContactInfomation(
										vendorForm, contact1);
					} else {
						vendorForm.setPreparerFirstName(contact.getFirstName());
						vendorForm.setPreparerLastName(contact.getLastName());
						vendorForm.setPreparerTitle(contact.getDesignation());
						vendorForm.setPreparerPhone(contact.getPhoneNumber());
						vendorForm.setPreparerMobile(contact.getMobile());
						vendorForm.setPreparerFax(contact.getFax());
						vendorForm.setPreparerEmail(contact.getEmailId());
					}
					session.setAttribute("secretQnsList", securityQnsList);
					CURDDao<?> cdao1 = new CURDTemplateImpl();
					List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
							.list(appDetails,
									" From CustomerVendorMenuStatus where vendorId="
											+ vendorId);
					session.setAttribute("menuStatus", menuStatus);
					/*
					 * prime and diverse vendors login check.
					 */
					if (null != request.getParameter("prime")
							&& !request.getParameter("prime").isEmpty()) {
						/*
						 * session.setAttribute("divisionId", selfRegInfo
						 * .getCustomerDivisionId().getId());
						 */
						if (request.getParameter("prime").equalsIgnoreCase(
								"yes")) {

							if (null != master.getPrimeNonPrimeVendor()
									&& master.getPrimeNonPrimeVendor() == 1) {
								session.setAttribute("contactStates",
										contactStates);
								session.setAttribute("primeEmailId",
										contact.getEmailId());
								vendorForm.setId(vendorId);
								return mapping.findForward("retrievePrime");
							} else {
								vendorForm.setPreparerEmail(null);
								ActionMessages messages = new ActionMessages();
								ActionMessage msg = new ActionMessage(
										"err.user.username.invalid");
								messages.add("emailId", msg);
								saveMessages(request, messages);
								return mapping.getInputForward();
							}

						} else if (request.getParameter("prime")
								.equalsIgnoreCase("no")) {

							if (null != master.getPrimeNonPrimeVendor()
									&& master.getPrimeNonPrimeVendor() == 1) {
								vendorForm.setPreparerEmail(null);
								ActionMessages messages = new ActionMessages();
								ActionMessage msg = new ActionMessage(
										"err.user.username.invalid");
								messages.add("emailId", msg);
								saveMessages(request, messages);
								return mapping.getInputForward();
							}
						}
					}

				} else {
					ActionMessages messages = new ActionMessages();
					ActionMessage msg = new ActionMessage(
							"error.password.invalid");
					messages.add("loginfail", msg);
					saveMessages(request, messages);
					return mapping.getInputForward();
				}
			} else {
				ActionMessages messages = new ActionMessages();
				ActionMessage msg = new ActionMessage("err.emailid.submitted");
				messages.add("emailId", msg);
				saveMessages(request, messages);
				return mapping.getInputForward();
			}
		} else if (selfRegInfo != null) {

			Integer keyvalue = selfRegInfo.getKeyValue();
			decryptedPassword = decrypt.decryptText(
					String.valueOf(keyvalue.toString()),
					selfRegInfo.getPassword());
			if (vendorForm.getLoginpassword() != null
					&& vendorForm.getLoginpassword().equals(decryptedPassword)) {
				// To Check Whether Password is Valid or Not (Valid Upto 7 Days
				// from Password Generated Date)
				Date validUptoDate = selfRegInfo.getValidUpto();
				Date today = new Date();

				if (today.compareTo(validUptoDate) < 0) {
					// Password Valid
					String resultPage = resetPage(session, appDetails,
							selfRegInfo);
					if (resultPage.equalsIgnoreCase("resetprime")
							&& request.getParameter("prime").equalsIgnoreCase(
									"yes")) {
						return mapping.findForward(resultPage);
					} else if (resultPage.equalsIgnoreCase("reset")
							&& request.getParameter("prime").equalsIgnoreCase(
									"no")) {
						return mapping.findForward(resultPage);
					} else {
						ActionMessages messages = new ActionMessages();
						ActionMessage msg = new ActionMessage(
								"error.usernamepassword.invalid");
						messages.add("emailId", msg);
						saveMessages(request, messages);
						return mapping.getInputForward();
					}
				} else if (selfRegInfo.getCount() == 0) {
					// Allow User to login First Time (Password Reset Page) When
					// Password is not Valid (After 7 Days) for Only One Time
					String result = "success";
					// Need To Update Count Value to 1
					VendorDao vendor = new VendorDaoImpl();
					result = vendor.updateContact(appDetails, selfRegInfo);// 1
																			// to
																			// Update
																			// Count=1

					if (result.equalsIgnoreCase("success")) {
						String resultPage = resetPage(session, appDetails,
								selfRegInfo);
						if (resultPage.equalsIgnoreCase("resetprime")
								&& request.getParameter("prime")
										.equalsIgnoreCase("yes")) {
							return mapping.findForward(resultPage);
						} else if (resultPage.equalsIgnoreCase("reset")
								&& request.getParameter("prime")
										.equalsIgnoreCase("no")) {
							return mapping.findForward(resultPage);
						} else {
							ActionMessages messages = new ActionMessages();
							ActionMessage msg = new ActionMessage(
									"error.usernamepassword.invalid");
							messages.add("emailId", msg);
							saveMessages(request, messages);
							return mapping.getInputForward();
						}
					} else {
						// Password Not Valid (Password Greater than 7 Days from
						// Generated Date)
						ActionMessages messages = new ActionMessages();
						ActionMessage msg = new ActionMessage(
								"error.password.outofdate");
						messages.add("emailId", msg);
						saveMessages(request, messages);
						return mapping.getInputForward();
					}
				} else {
					// Password Not Valid (Password Greater than 7 Days from
					// Generated Date)
					ActionMessages messages = new ActionMessages();
					ActionMessage msg = new ActionMessage(
							"error.password.outofdate");
					messages.add("emailId", msg);
					saveMessages(request, messages);
					return mapping.getInputForward();
				}
			} else {
				ActionMessages messages = new ActionMessages();
				ActionMessage msg = new ActionMessage("error.password.invalid");
				messages.add("emailId", msg);
				saveMessages(request, messages);
				return mapping.getInputForward();
			}
		} else {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("err.user.username.invalid");
			messages.add("emailId", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("retrieve");
	}

	/**
	 * Method to Return Reset Page.
	 * 
	 * @param session
	 * 
	 * @param appDetails
	 * 
	 * @param selfRegInfo
	 * 
	 */
	public String resetPage(HttpSession session, UserDetailsDto appDetails,
			CustomerVendorSelfRegInfo selfRegInfo) {
		LoginDao login = new LoginDaoImpl();
		List<SecretQuestion> secretQuestionList = login
				.getSecurityQuestions(appDetails);
		session.setAttribute("secretQuestionsList", secretQuestionList);
		session.setAttribute("resetEmail", selfRegInfo.getEmailId());

		com.fg.vms.customer.model.CustomerApplicationSettings settings = (com.fg.vms.customer.model.CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");
		if (settings.getIsDivision() != null && settings.getIsDivision() != 0) {
			if (selfRegInfo.getCustomerDivisionId() != null
					&& selfRegInfo.getCustomerDivisionId().getId() != null) {
				// if(selfRegInfo.getCustomerDivisionId().getId() == 3)
				// {
				// session.setAttribute("divisionId",0);
				// appDetails.setCustomerDivisionID((Integer)
				// session.getAttribute("divisionId"));
				// }
				// else{
				session.setAttribute("divisionId", selfRegInfo
						.getCustomerDivisionId().getId());
				appDetails.setCustomerDivisionID((Integer) session
						.getAttribute("divisionId"));
				// }
				// To set customer division isGlobal or not in userDetails
				if (selfRegInfo.getCustomerDivisionId().getIsGlobal() != null
						&& selfRegInfo.getCustomerDivisionId().getIsGlobal() != 0)
					appDetails.setIsGlobalDivision(Integer.valueOf(selfRegInfo
							.getCustomerDivisionId().getIsGlobal()));
				else
					appDetails.setIsGlobalDivision(0);
			}
		}
		if (null != selfRegInfo.getVendorType()
				&& !selfRegInfo.getVendorType().isEmpty()
				&& selfRegInfo.getVendorType().equalsIgnoreCase("P")) {
			return "resetprime";
		} else {
			return "reset";
		}
	}

	/**
	 * Method to update the certificate agencies and certificate types.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward getClassificationData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		VendorDao vendorDao = new VendorDaoImpl();
		List<ClassificationDto> classifications = vendorDao
				.getClassifcationDetails(vendorForm, appDetails);
		session.setAttribute("classifications", classifications);

		return mapping.findForward("ajaxsuccess");
	}

	/**
	 * Method to get email id and send login credentials to new vendor (Self
	 * Registration).
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward sendLoginCredentials(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result = "";
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		VendorDao vendorDao = new VendorDaoImpl();
		String prime = null;
		if (null != session.getAttribute("primeSelfRegistration")) {
			prime = session.getAttribute("primeSelfRegistration").toString();
		}
		String psw = String.valueOf(PasswordUtil.generatePswd(8, 8, 1, 1, 0));
		result = vendorDao.newRegistration(appDetails, vendorForm, psw, prime);

		if (result.equals("userEmail")) {
			session.setAttribute("registration", "This Email ID already exist.");
		} else if (result.equals("failure")) {
			session.setAttribute("registration", "Email sent failed.");
		} else if (result.equals("success")) {
			SendEmail sendEmail = new SendEmail();
			String url = request.getRequestURL().toString()
					.replaceAll(request.getServletPath(), "").trim();
			String appRoot = getServlet().getServletContext().getRealPath("")
					+ "/";
			if (null != request.getParameter("prime")
					&& !request.getParameter("prime").isEmpty()) {
				sendEmail.sendCredentialsToPrimeVendor(vendorForm, appDetails,
						psw, url, appRoot);
			} else {
				sendEmail.sendCredentialsToUnknownVendorOnSave(vendorForm,
						appDetails, psw, url, appRoot);
			}
			session.setAttribute(
					"registration",
					"Email sent successfully. Use Credentials from Email to enter additional particulars.");
		}
		return mapping.findForward("registration");
	}

	/**
	 * Method to get save meeting information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveMeetingInfo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Integer vendorId = null;
		String result = "";
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		VendorDao vendorDao = new VendorDaoImpl();
		if (session.getAttribute("meetingVendor") != null) {
			vendorId = Integer.parseInt(session.getAttribute("meetingVendor")
					.toString());
		}
		result = vendorDao.saveMeetingDetails(appDetails, vendorForm, vendorId);
		if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward(result);
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward primeVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CURDDao<?> dao = new CURDTemplateImpl();
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		CustomerApplicationSettings settings = (CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");

		if (settings.getIsDivision() != null && settings.getIsDivision() != 0) {
			List<CustomerDivision> customerDivisionList = (List<CustomerDivision>) dao
					.list(appDetails,
							"From CustomerDivision where isactive=1 and isGlobal=0");
			session.setAttribute("customerDivisionList", customerDivisionList);
		}

		String query = "From Country where isactive=1 order by countryname";
		if (appDetails.getWorkflowConfiguration() != null) {
			if (appDetails.getWorkflowConfiguration().getInternationalMode() != null
					&& appDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}

		List<com.fg.vms.customer.model.Country> countryList = (List<com.fg.vms.customer.model.Country>) dao
				.list(appDetails, query);
		session.setAttribute("countryList", countryList);

		Integer countryId = null;
		List<Country> country = (List<Country>) dao.list(appDetails,
				"From Country where isDefault=1");
		countryId = country.get(0).getId();

		if (appDetails.getWorkflowConfiguration() != null) {
			if (appDetails.getWorkflowConfiguration().getInternationalMode() != null
					&& appDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
				List<State> statesList = (List<State>) dao.list(appDetails,
						"From State where countryid=" + countryId
								+ " and isactive=1 order by statename");
				session.setAttribute("statesList", statesList);
			}
		}

		// Always Display US States as Default Contact States.
		country = (List<Country>) dao.list(appDetails,
				"From Country where countryname = 'United States'");
		countryId = country.get(0).getId();
		List<State> contactStates = (List<State>) dao.list(appDetails,
				"From State where countryid=" + countryId
						+ " and isactive=1 order by statename");
		session.setAttribute("contactStates", contactStates);

		return mapping.findForward("primeVendor");
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward supplierDivercityForm(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session=request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SupplierDiversityForm supplierDiversityForm=(SupplierDiversityForm)form;
		UsersDao udao=new UsersDaoImpl<Users>();
		String date = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		Integer userId = (Integer) session.getAttribute("userId");
		Users users=udao.retriveUser(userId, appDetails);
		supplierDiversityForm.setFirstName(users.getFirstName());
		supplierDiversityForm.setLastName(users.getLastName());
		supplierDiversityForm.setRequestedOn(date);
		session.setAttribute("supplierDiversityForm", supplierDiversityForm);
		VendorDao dao = new VendorDaoImpl();
		session.setAttribute("form", false);
		List<SupplierDiversityDto> supplierDiversityDtoList=dao.retrieveSupplierDiversityList(appDetails,userId);
		session.setAttribute("supplierDiversityDtoList", supplierDiversityDtoList);
		
		String create = "Yes";
		request.setAttribute("create", create);
		return mapping.findForward("supplierdivercityform");
	}
	
	
	public ActionForward createSupplierDiversity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SupplierDiversityForm supplierDiversityForm=(SupplierDiversityForm)form;
		UsersDao udao=new UsersDaoImpl<Users>();
		Integer userId = (Integer) session.getAttribute("userId");
		Users users=udao.retriveUser(userId, appDetails);
		supplierDiversityForm.setFirstName(users.getFirstName());
		supplierDiversityForm.setLastName(users.getLastName());
		session.setAttribute("supplierDiversityForm", supplierDiversityForm);
		VendorDao dao = new VendorDaoImpl();
		String result = dao.saveSupplierDiversity(appDetails, supplierDiversityForm,users);
		List<SupplierDiversityDto> supplierDiversityDtoList=dao.retrieveSupplierDiversityList(appDetails,userId);
		session.setAttribute("supplierDiversityDtoList", supplierDiversityDtoList);
		String date = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		if (result!=null && result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("supplier.save");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			form.reset(mapping, request);
			supplierDiversityForm.setFirstName(users.getFirstName());
			supplierDiversityForm.setLastName(users.getLastName());
			session.setAttribute("supplierDiversityForm", supplierDiversityForm);
			supplierDiversityForm.setRequestedOn(date);
			session.setAttribute("form", false);
			return mapping.getInputForward();
		}else if (result!=null && result.equals("updated")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("supplier.update");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			form.reset(mapping, request);
			supplierDiversityForm.setFirstName(users.getFirstName());
			supplierDiversityForm.setLastName(users.getLastName());
			supplierDiversityForm.setRequestedOn(date);
			session.setAttribute("supplierDiversityForm", supplierDiversityForm);
			session.setAttribute("form", false);
			return mapping.getInputForward();
		}else if(result!=null && result.equals("failure")){
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("supplier.failure");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
			form.reset(mapping, request);
			supplierDiversityForm.setFirstName(users.getFirstName());
			supplierDiversityForm.setLastName(users.getLastName());
			supplierDiversityForm.setRequestedOn(date);
			session.setAttribute("supplierDiversityForm", supplierDiversityForm);
			session.setAttribute("form", false);
			return mapping.getInputForward();
		}
		System.out.println(supplierDiversityForm.getFirstName());
		return mapping.findForward("supplierdivercityform");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward updateSupplierDivercity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session=request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SupplierDiversityForm supplierDiversityForm=(SupplierDiversityForm)form;
		VendorDao dao = new VendorDaoImpl();
		Integer supplierDiversityId=Integer.parseInt(request.getParameter("id"));
		SupplierDiversity supplierDiversity=(SupplierDiversity)dao.retrieveSupplierDiversity(appDetails, supplierDiversityId);
		
		if(supplierDiversity != null){
		supplierDiversityForm.setId(supplierDiversity.getId());
		supplierDiversityForm.setFirstName(supplierDiversity.getRequestorId().getFirstName());
		supplierDiversityForm.setLastName(supplierDiversity.getRequestorId().getLastName());
		supplierDiversityForm.setCreatedOn(CommonUtils.convertDateTimeToStringFormated(supplierDiversity.getCreatedOn()));
		supplierDiversityForm.setRequestedOn(CommonUtils.convertDateTimeToStringFormated(supplierDiversity.getRequestedOn()));
		supplierDiversityForm.setOppurtunityEventName(supplierDiversity.getOppurtunityEventName());
		supplierDiversityForm.setSourceEvent(supplierDiversity.getSourceEvent().split(","));
		supplierDiversityForm.setSourceEventStartDate(CommonUtils.convertDateTimeToStringFormated(supplierDiversity.getSourceEventStartDate()));
		supplierDiversityForm.setEstimateSpend(supplierDiversity.getEstimateSpend());
		supplierDiversityForm.setEstimateWorkStartDate(CommonUtils.convertDateTimeToStringFormated(supplierDiversity.getEstimateWorkStartDate()));
		supplierDiversityForm.setLastResponseDate(CommonUtils.convertDateTimeToStringFormated(supplierDiversity.getLastResponseDate()));
		
		//String[] commodityData=supplierDiversity.getCommodityCode().split(",");
		//supplierDiversityForm.setCommodityDataList(Arrays.asList(commodityData));
		
		String[] commodityDatas=supplierDiversity.getCommodityCode().split(",");
		supplierDiversityForm.setCommodityDataList(dao.commodityUpdateSearchList(appDetails, commodityDatas));
		
		//List commodityUpdateSearchList=dao.commodityUpdateSearchList(appDetails, commodityDatas);
		
		supplierDiversityForm.setCommodityCode(supplierDiversity.getCommodityCode().split(","));
		supplierDiversityForm.setScopOfWork(supplierDiversity.getScopOfWork());
		supplierDiversityForm.setGeography(supplierDiversity.getGeography());
		supplierDiversityForm.setMiniSupplierRequirement(supplierDiversity.getMiniSupplierRequirement());
		supplierDiversityForm.setContractInsight(supplierDiversity.getContractInsight());
		if( supplierDiversity.getIncumbentVendorId()!=null){
		String[] vendorData = supplierDiversity.getIncumbentVendorId().split(",");
		supplierDiversityForm.setIncumbentDataList(dao.getVendorList(appDetails, vendorData));
		}
		
		
//		String [] ar=supplierDiversity.getIncumbentVendorId().split(",");
//		Integer [] intarray=new Integer[ar.length];
//		for(String a:ar){
//			int i=0;
//			intarray[i]=new Integer(a.trim());
//			i++;
//		}
//		supplierDiversityForm.setIncumbentVendorId(intarray);
		
		//dao.retrieveVendorList(appDetails, commodityCode);
		//supplierDiversityForm.setCommodityDataList(dao.commodityUpdateSearchList(appDetails, commodityDatas));

		//supplierDiversityForm.setIncumbentVendorId();
		supplierDiversityForm.setBusiness(supplierDiversity.getBusiness());
		session.setAttribute("form", true);
		}else{
		session.setAttribute("form", false);
		}
		
		JSONArray jarray=new JSONArray();
		JSONObject jobject=new JSONObject();
		jobject.put("commodityCode",supplierDiversity.getCommodityCode());
		jarray.add(jobject);
		
		//session.setAttribute("commodityArray", jarray.toString().getBytes());
		//response.getOutputStream().write(jarray.toString().getBytes());
		
		session.setAttribute("supplierDiversityForm", supplierDiversityForm);
		return mapping.findForward("supplierdivercityform");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward deleteSupplierDivercityForm(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session=request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SupplierDiversityForm supplierDiversityForm=(SupplierDiversityForm)form;
		VendorDao dao = new VendorDaoImpl();
		Integer supplierDiversityId=Integer.parseInt(request.getParameter("id"));
		SupplierDiversity supplierDiversity=(SupplierDiversity)dao.retrieveSupplierDiversity(appDetails, supplierDiversityId);
		String result=dao.deleteSupplierDiversity(appDetails, supplierDiversity);
		
		UsersDao udao=new UsersDaoImpl<Users>();
		Integer userId = (Integer) session.getAttribute("userId");
		Users users=udao.retriveUser(userId, appDetails);
		supplierDiversityForm.setFirstName(users.getFirstName());
		supplierDiversityForm.setLastName(users.getLastName());
		session.setAttribute("supplierDiversityForm", supplierDiversityForm);
		session.setAttribute("form", false);
		List<SupplierDiversityDto> supplierDiversityDtoList=dao.retrieveSupplierDiversityList(appDetails,userId);
		session.setAttribute("supplierDiversityDtoList", supplierDiversityDtoList);
		String date = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			form.reset(mapping, request);
			session.setAttribute("form", false);
			supplierDiversityForm.setFirstName(users.getFirstName());
			supplierDiversityForm.setLastName(users.getLastName());
			supplierDiversityForm.setRequestedOn(date);
			session.setAttribute("supplierDiversityForm", supplierDiversityForm);
			return mapping.getInputForward();
		}
		return mapping.findForward("supplierdivercityform");
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward createPrimeVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		VendorDao dao = new VendorDaoImpl();
		Integer userId = (Integer) session.getAttribute("userId");
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		String result = dao
				.savePrimeVendor(vendorForm, userId, appDetails, url);
		if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("tier2EmailId.unique"));

			saveErrors(request, errors);
			if (request.getParameter("nonprime") != null) {
				return mapping.getInputForward();
			}
			return mapping.getInputForward();
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);

			return mapping.getInputForward();
		} else if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.save.ok");
			messages.add("vendor", msg);
			vendorForm.reset(mapping, request);
			saveMessages(request, messages);
		}
		return mapping.findForward(result);
	}

	@SuppressWarnings("unused")
	private VendorMasterForm packVendor() {
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward generateVendorReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorId = Integer.parseInt(request.getParameter("id")
				.toString());
		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;
		VendorDao dao = new VendorDaoImpl();
		try {
			String vendorName = dao.getVendorName(appDetails, vendorId) + " "
					+ dateTime;
			vendorName = vendorName.replaceAll("[^\\dA-Za-z]", "");
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + vendorName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			List<VendorMasterBean> reportData = getReportData(appDetails,
					vendorId);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					reportData);

			InputStream is = JRLoader
					.getResourceInputStream("vendor_master.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
		// return mapping.findForward("vendorReport");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward generateVendorReportXLS(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorId = Integer.parseInt(request.getParameter("id")
				.toString());
		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;
		VendorDao dao = new VendorDaoImpl();
		try {
			String vendorName = dao.getVendorName(appDetails, vendorId) + " "
					+ dateTime;
			vendorName = vendorName.replaceAll("[^\\dA-Za-z]", "");
			List<VendorMasterBean> reportData = getReportData(appDetails,
					vendorId);

			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/xls");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + vendorName + ".xls");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					reportData);

			InputStream is = JRLoader
					.getResourceInputStream("vendor_master.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();

			// Coding For Excel
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
					jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
					servletOutputStream);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
					Boolean.FALSE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
					Boolean.TRUE);
			exporterXLS.exportReport();

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
		// return mapping.findForward("vendorReport");
	}

	public List<VendorMasterBean> getReportData(UserDetailsDto appDetails,
			Integer vendorId) {
		VendorDao dao = new VendorDaoImpl();
		List<VendorMasterBean> vendorMasterList = dao.listVendorsDetails(
				appDetails, vendorId);
		List<VendorAddressInfo> vendorAddressList = dao
				.listVendorsAddressDetails(appDetails, vendorId);
		List<VendorContactInfo> vendorContactInfoList = dao
				.listVendorsContactInfo(appDetails, vendorId);
		List<VendorOwnerDetails> vendorOwnerList = dao.listVendorsOwnerDetails(
				appDetails, vendorId);
		String vendorDiverseClassification = dao.vendorsDiverseClassification(
				appDetails, vendorId);
		List<VendorDiverseCertificate> vendorDivCertificateList = dao
				.listVendorsDivCertificate(appDetails, vendorId);
		List<CustomerServiceAreaInfo> vendorServiceAreaList = dao
				.listVendorServiceArea(appDetails, vendorId);
		List<VendorBussinessArea> vendorBussinessAreaList = dao
				.listVendorsBussinessArea(appDetails, vendorId);
		VendorBussinessBioDetails vendorsBussinessBiographyDetails = dao
				.listVendorsBussinessBiography(appDetails, vendorId);
		List<VendorDetailsBean> vendorsReferenceList = dao
				.listVendorsReference(appDetails, vendorId);
		List<VendorDetailsBean> vendorsOtherCertificatesList = dao
				.listVendorsOtherCertificates(appDetails, vendorId);
		List<VendorDetailsBean> vendorsMeetingInfoList = dao
				.listVendorsMeetingInfo(appDetails, vendorId);
		List<VendorDetailsBean> businessAreaCommodityList = dao
				.listBusinessAreaCommodityInfo(appDetails, vendorId);
		List<VendorDetailsBean> serviceAreaNaicsList = dao
				.listServiceAreaNaicsInfo(appDetails, vendorId);
		List<VendorDetailsBean> serviceAreaKeywordsList = dao
				.listServiceAreaKeywordsInfo(appDetails, vendorId);

		for (VendorMasterBean bean : vendorMasterList) {
			bean.setVendorAddressList(vendorAddressList);
			bean.setVendorContactInfoList(vendorContactInfoList);
			bean.setVendorOwnerList(vendorOwnerList);
			bean.setVendorDiverseClassification(vendorDiverseClassification);
			bean.setVendorDivCertificateList(vendorDivCertificateList);
			bean.setVendorServiceArea(vendorServiceAreaList);
			bean.setVendorBussinessAreaList(vendorBussinessAreaList);
			bean.setVendorsBussinessBiographyList(vendorsBussinessBiographyDetails
					.getBussinessList());
			bean.setVendorsSafetyBiographyList(vendorsBussinessBiographyDetails
					.getBussinessSafetylist());
			bean.setVendorsReferenceList(vendorsReferenceList);
			bean.setVendorsOtherCertificatesList(vendorsOtherCertificatesList);
			bean.setVendorsMeetingInfoList(vendorsMeetingInfoList);
			bean.setBusinessAreaCommodityInfoList(businessAreaCommodityList);
			bean.setServiceAreaNaicsInfoList(serviceAreaNaicsList);
			bean.setServiceAreaKeywordsInfoList(serviceAreaKeywordsList);
		}
		return vendorMasterList;
	}

	public ActionForward populateCheckList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		VendorDaoImpl vendorDaoImpl = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String checkList = "";
		String registrationRequest = "";
		String registrationQuestion = "";
		Integer divisionId = vendorForm.getCustomerDivision();

		checkList = vendorDaoImpl.populateCheckList(appDetails, divisionId);
		registrationRequest = vendorDaoImpl.populateRegistrationRequest(
				appDetails, divisionId);
		registrationQuestion = vendorDaoImpl.populateRegistrationQuestions(
				appDetails, divisionId);
		String[] array = checkList.split("\\n");
		String finalCheckList = "";
		for (String s : array) {
			finalCheckList = finalCheckList + "<li>" + s + "</li>";
		}
		session.setAttribute("checkList", finalCheckList);
		session.setAttribute("registrationRequest", registrationRequest);
		session.setAttribute("registrationQuestion", registrationQuestion);
		return mapping.findForward("checkList");
	}

	public ActionForward getIsGlobalDivision(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		CustomerDivisionDao customerDivisionDaoImpl = new CustomerDivisionDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer isDivisionGlobal = 0;

		CustomerDivision customerDivision = customerDivisionDaoImpl
				.editDivision(vendorForm.getCustomerDivision(), appDetails);

		if (customerDivision != null) {
			if (customerDivision.getIsGlobal() != null
					&& customerDivision.getIsGlobal() != 0) {
				isDivisionGlobal = 1;
			} else {
				isDivisionGlobal = 0;
			}
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("isDivisionGlobal", isDivisionGlobal);
		response.addHeader("Content-Type", "application/json");
		response.getOutputStream().write(
				jsonObject.toString().getBytes("UTF-8"));
		response.getOutputStream().flush();
		return null;
	}

	public ActionForward populateCertificationAgencies(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		VendorDaoImpl vendorDaoImpl = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String cetIficationAgenies = "";
		Integer divisionId = vendorForm.getCustomerDivision();

		cetIficationAgenies = vendorDaoImpl.populateCertificationAgencies(
				appDetails, divisionId);

		String[] array = cetIficationAgenies.split("\\n");
		String finalCetIficationAgenies = "";
		for (String s : array) {
			finalCetIficationAgenies = finalCetIficationAgenies + "<li>" + s
					+ "</li>";
		}
		session.setAttribute("checkList", null);
		session.setAttribute("certificationAgencies", finalCetIficationAgenies);
		return mapping.findForward("certification");
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward updatePrimeVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		VendorDao vendorDaoImpl = new VendorDaoImpl();
		Integer userId = (Integer) session.getAttribute("userId");

		String result = vendorDaoImpl.updatePrimeVendor(vendorForm, userId,
				appDetails);

		if (result.equals("updatefailure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.update.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.findForward("update");
		} else if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.update.ok");
			messages.add("vendorupdate", msg);
			vendorForm.reset(mapping, request);
			saveMessages(request, messages);
		}
		CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao1.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		session.setAttribute("countryList", countries);

		if ("success".equalsIgnoreCase(result)) {

			SupplierDao supplierDao = new SupplierDaoImpl();

			VendorMaster vendorMaster = null;
			VendorContact vendorContact = null;
			CustomerVendorMeetingInfo meetingInfo = null;
			CustomerVendorAddressMaster addressMaster = null;
			Integer supplierId = Integer.parseInt(session.getAttribute(
					"supplierId").toString());

			vendorMaster = supplierDao.retreveVendorMaster(supplierId,
					appDetails);
			vendorContact = supplierDao.retreveVendorContact(supplierId,
					appDetails);
			meetingInfo = supplierDao.retreveVendorMeetingInfo(supplierId,
					appDetails);
			addressMaster = supplierDao.retreveAddressMaster(supplierId,
					appDetails);

			if (vendorMaster != null) {
				vendorForm.setId(vendorMaster.getId());
				vendorForm.setVendorName(vendorMaster.getVendorName());
				vendorForm.setEmailId(vendorMaster.getEmailId());
				if (vendorMaster.getCustomerDivisionId() != null) {
					vendorForm.setCustomerDivision(vendorMaster
							.getCustomerDivisionId().getId());
				}
				vendorForm.setVendorStatus(vendorMaster.getVendorStatus());

			}

			if (addressMaster != null) {

				if ((addressMaster.getAddress() != null)) {
					vendorForm.setAddress1(addressMaster.getAddress());
				}
				vendorForm.setCity(addressMaster.getCity());
				vendorForm.setCountry(addressMaster.getCountry());
				vendorForm.setState(addressMaster.getState());
				vendorForm.setZipcode(addressMaster.getZipCode());
				vendorForm.setPhone(addressMaster.getPhone());
				vendorForm.setFax(addressMaster.getFax());

				CURDDao<State> dao = new CURDTemplateImpl<State>();
				List<State> phyStates = dao.list(
						appDetails,
						" From State where countryid="
								+ addressMaster.getCountry());
				vendorForm.setPhyStates1(phyStates);

			}

			if (vendorContact != null) {
				vendorForm.setFirstName(vendorContact.getFirstName());
				vendorForm.setDesignation(vendorContact.getDesignation());
				vendorForm.setContactPhone(vendorContact.getPhoneNumber());
				vendorForm.setContanctEmail(vendorContact.getEmailId());
				vendorForm.setGender(vendorContact.getGender());
			}

			if (meetingInfo != null) {
				vendorForm.setContactFirstName(meetingInfo
						.getContactFirstName());
				vendorForm.setContactLastName(meetingInfo.getContactLastName());
				if (meetingInfo.getContactDate() != null) {
					SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
					vendorForm.setContactDate(format.format(meetingInfo
							.getContactDate()));
				}
				vendorForm.setContactState(meetingInfo.getContactState());
				vendorForm.setContactEvent(meetingInfo.getContactEvent());
			}
		}
		return mapping.findForward("update");
	}

	/**
	 * Delete Vendor Keyword based on Id.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteVendorKeyword(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		System.out.println("Sample Delete");
		int keywordId = Integer.parseInt(request.getParameter("id").toString());
		documentsDao.deleteKeywords(keywordId, appDetails);

		return mapping.findForward("success");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward generateTier2VendorsSpendReportXLS(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer tier2ReportMasterId = Integer.parseInt(request.getParameter("id")
				.toString());
		ReportDao reportDao = new ReportDaoImpl();
		Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(
				tier2ReportMasterId, appDetails);
		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;
		VendorDao dao = new VendorDaoImpl();
		try {
			String fileName = reportMaster.getTier2ReportMasterid()+"_"+reportMaster.getVendorId().getVendorName() + " "
					+ dateTime;
			fileName = fileName.replaceAll("[^\\dA-Za-z]", "");
			List<Tier2ReportMasterBean> reportData = getTier2ReportData(appDetails,
					tier2ReportMasterId);

			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/xls");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + fileName + ".xls");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(reportData);

			InputStream is = JRLoader
					.getResourceInputStream("tier2_spend_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();

			// Coding For Excel
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
					jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
					servletOutputStream);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
					Boolean.FALSE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
					Boolean.TRUE);
			exporterXLS.exportReport();

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
		// return mapping.findForward("vendorReport");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward generateSupplierDiversityXLS(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer supplierdiversityID = Integer.parseInt(request.getParameter("id")
				.toString());
		ReportDao reportDao = new ReportDaoImpl();
//		Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(
//				supplierdiversityID, appDetails);
		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;
		VendorDao dao = new VendorDaoImpl();
		try {
		
		//	SupplierDiversity reportData=(SupplierDiversity)dao.retrieveSupplierDiversity(appDetails, supplierdiversityID);
			//List<Tier2ReportMasterBean> reportData = getTier2ReportData(appDetails,
//					tier2ReportMasterId);
			List<SupplierDiversityBean> reportData = getSupplierDiversityData(appDetails, supplierdiversityID);
			String fileName = "SupplierdiversityRequestForm"+"_"+reportData.get(0).getRequestorName()+"_"
					+ dateTime;
			
			fileName = fileName.replaceAll("[^\\dA-Za-z]", "");
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/xls");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + fileName + ".xls");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(reportData);

			InputStream is = JRLoader
					.getResourceInputStream("supplier_diversity.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();

			// Coding For Excel
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
					jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
					servletOutputStream);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
					Boolean.FALSE);
			exporterXLS.setParameter(
					JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
					Boolean.TRUE);
			exporterXLS.exportReport();

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
		// return mapping.findForward("vendorReport");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward generateSupplierDiversity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer supplierdiversityID = Integer.parseInt(request.getParameter("id")
				.toString());

//		ReportDao reportDao = new ReportDaoImpl();
	
		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;
//		VendorDao dao = new VendorDaoImpl();
		try {
//			String fileName = reportMaster.getTier2ReportMasterid()+"_"+reportMaster.getVendorId().getVendorName() + " "
//					+ dateTime;
			List<SupplierDiversityBean> reportData = getSupplierDiversityData(appDetails, supplierdiversityID);
			String fileName = "SupplierdiversityRequestForm"+"_"+reportData.get(0).getRequestorName()+"_ "
					+ dateTime;
			
			fileName = fileName.replaceAll("[^\\dA-Za-z]", "");
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + fileName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(reportData);

			InputStream is = JRLoader
					.getResourceInputStream("supplier_diversity.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
		
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward generateTier2VendorsSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer tier2ReportMasterId = Integer.parseInt(request.getParameter("id")
				.toString());
		ReportDao reportDao = new ReportDaoImpl();
		Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(
				tier2ReportMasterId, appDetails);
		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;
//		VendorDao dao = new VendorDaoImpl();
		try {
			String fileName = reportMaster.getTier2ReportMasterid()+"_"+reportMaster.getVendorId().getVendorName() + " "
					+ dateTime;
			fileName = fileName.replaceAll("[^\\dA-Za-z]", "");
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + fileName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			List<Tier2ReportMasterBean> reportData = getTier2ReportData(appDetails,
					tier2ReportMasterId);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(reportData);

			InputStream is = JRLoader
					.getResourceInputStream("tier2_spend_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
		// return mapping.findForward("vendorReport");
	}

	public List<Tier2ReportMasterBean> getTier2ReportData(UserDetailsDto appDetails,
			Integer vendorId) throws ParseException {
		List<Tier2ReportMasterBean> tier2ReportMasterBeanList=new ArrayList<Tier2ReportMasterBean>();
		Tier2ReportMasterBean tier2ReportMasterBean=new Tier2ReportMasterBean();
		List<Tier2ReportIndirectExpensesBean> tier2ReportIndirectExpensesBeanList=new ArrayList<Tier2ReportIndirectExpensesBean>();
		List<Tier2ReportDirectExpensesBean> tier2ReportDirectExpensesBeanList=new ArrayList<Tier2ReportDirectExpensesBean>();
		ReportDao reportDao = new ReportDaoImpl();
		Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(
				vendorId, appDetails);
		
		// Initiate direct expenses details
		List<Tier2ReportDirectExpenses> directExpenses = reportMaster
				.getTier2ReportDirectExpensesList();

		// Initiate indirect expenses details
		List<Tier2ReportIndirectExpenses> indirectExpenses = reportMaster
				.getTier2ReportIndirectExpensesList();

		CommodityDaoImpl commodityDaoImpl = new CommodityDaoImpl();
		String query = "From CustomerCommodityCategory where isActive=1 order by categoryDescription";
		List<CustomerCommodityCategory> marketSubSectors = commodityDaoImpl
				.marketSectorAndCommodities(appDetails, query);

		String ssDate = Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(ssDate);

		if (reportMaster != null) {
				double totalInAmt=0;
				if(directExpenses != null){
				for (Tier2ReportDirectExpenses tier2ReportDirectExpenses : directExpenses) {
					Tier2ReportDirectExpensesBean tier2ReportDirectExpensesBean=new Tier2ReportDirectExpensesBean();
					if(tier2ReportDirectExpenses.getTier2Vendorid() != null)
					tier2ReportDirectExpensesBean.setDirectTier2VendorName(tier2ReportDirectExpenses.getTier2Vendorid().getVendorName());
					if(tier2ReportDirectExpenses.getCertificateId() != null)
					tier2ReportDirectExpensesBean.setDirectCertificate(tier2ReportDirectExpenses.getCertificateId().getCertificateName());
					if(tier2ReportDirectExpenses.getEthnicityId() != null)
					tier2ReportDirectExpensesBean.setDirectEthnicity(tier2ReportDirectExpenses.getEthnicityId().getEthnicity());
					if(tier2ReportDirectExpenses.getDirExpenseAmt() != null)
					tier2ReportDirectExpensesBean.setDirectExpence(Integer.valueOf((int) Math.round(tier2ReportDirectExpenses.getDirExpenseAmt())));
					if (reportMaster.getCreatedOn().after(date)) {
						if(tier2ReportDirectExpenses.getMarketSector() != null){
						String query1 = "From CustomerCommodityCategory where isActive=1 and id="+tier2ReportDirectExpenses.getMarketSector().getId();
						CustomerCommodityCategory marketSubSector = (CustomerCommodityCategory) commodityDaoImpl.find(appDetails, query1);
						tier2ReportDirectExpensesBean.setDirectSector(marketSubSector.getCategoryDescription());
						}
					} else {
						if(tier2ReportDirectExpenses.getMarketSector() != null){
						tier2ReportDirectExpensesBean
								.setDirectSector(tier2ReportDirectExpenses
										.getMarketSector()
										.getSectorDescription());
						}
					}
					tier2ReportDirectExpensesBeanList.add(tier2ReportDirectExpensesBean);
				}
				}
				
				if(indirectExpenses != null){
				for (Tier2ReportIndirectExpenses tier2ReportIndirectExpenses : indirectExpenses) {
					Tier2ReportIndirectExpensesBean tier2ReportIndirectExpensesBean=new Tier2ReportIndirectExpensesBean();
					if(tier2ReportIndirectExpenses.getTier2IndirectVendorId() != null)
					tier2ReportIndirectExpensesBean.setIndirectTier2VendorName(tier2ReportIndirectExpenses.getTier2IndirectVendorId().getVendorName());
					if(tier2ReportIndirectExpenses.getCertificateId() != null)
					tier2ReportIndirectExpensesBean.setIndirectCertificate(tier2ReportIndirectExpenses.getCertificateId().getCertificateName());
					if(tier2ReportIndirectExpenses.getEthnicityId() != null)
					tier2ReportIndirectExpensesBean.setIndirectEthnicity(tier2ReportIndirectExpenses.getEthnicityId().getEthnicity());
					if(tier2ReportIndirectExpenses.getIndirectExpenseAmt() != null)
					tier2ReportIndirectExpensesBean.setIndirectExpence(Integer.valueOf((int)Math.round(tier2ReportIndirectExpenses.getIndirectExpenseAmt())));
					totalInAmt+=tier2ReportIndirectExpenses.getIndirectExpenseAmt();
					if (reportMaster.getCreatedOn().after(date)) {
						if(tier2ReportIndirectExpenses.getMarketSector() != null){
						String query2 = "From CustomerCommodityCategory where isActive=1 and id="+tier2ReportIndirectExpenses.getMarketSector().getId();
						CustomerCommodityCategory marketSubSector = (CustomerCommodityCategory) commodityDaoImpl.find(appDetails, query2);
						tier2ReportIndirectExpensesBean.setIndirectSector(marketSubSector.getCategoryDescription());
						}
					}else{
						if(tier2ReportIndirectExpenses.getMarketSector() != null){
						tier2ReportIndirectExpensesBean.setIndirectSector(tier2ReportIndirectExpenses.getMarketSector().getSectorDescription());
						}
					}
					tier2ReportIndirectExpensesBeanList.add(tier2ReportIndirectExpensesBean);
				}
				}
				// session.setAttribute("createdOnDate", true);
				tier2ReportMasterBean.setVendorName(reportMaster.getVendorId().getVendorName());
				tier2ReportMasterBean.setReportingName(reportMaster.getReportingPeriod());
				tier2ReportMasterBean.setTotalSales(String.valueOf(Math.round(reportMaster.getTotalSales())));
				tier2ReportMasterBean.setTotalSalesToCompany(String.valueOf(Math.round(reportMaster.getTotalSalesToCompany())));
				tier2ReportMasterBean.setIndirectAllocationPercentage(calculate(reportMaster.getTotalSales(),reportMaster.getTotalSalesToCompany()).toString());
				tier2ReportMasterBean.setAllocatedAmmount(String.valueOf(Math.round(setAllocatedAmount(calculate(reportMaster.getTotalSales(),reportMaster.getTotalSalesToCompany()),totalInAmt))));
				tier2ReportMasterBean.setTier2ReportDirectExpensesBeanList(tier2ReportDirectExpensesBeanList);
				tier2ReportMasterBean.setTier2ReportIndirectExpensesBeanList(tier2ReportIndirectExpensesBeanList);
				tier2ReportMasterBeanList.add(tier2ReportMasterBean);
		}
		
		return tier2ReportMasterBeanList;
	}
	
	public static long setAllocatedAmount(Float indirectPercentage, Double totslInAmt){
		long totalIndirectAmt = 0;
		if(indirectPercentage != null && totslInAmt != null){
			totalIndirectAmt = Math.round(((indirectPercentage/100)*totslInAmt));
		}
		return totalIndirectAmt;
	}
	
	public static Float calculate(Double TotalSales, Double TotalSalesToCompany){
		Float per=0f;
		if(TotalSales != null && TotalSalesToCompany !=null){
			per=(float) ((TotalSalesToCompany/TotalSales)*100);
		}
		return per;
	}
	
	public List<SupplierDiversityBean> getSupplierDiversityData(UserDetailsDto appDetails,
			Integer SupplierDiversityId) throws ParseException {
		List<SupplierDiversityBean> supplierDiversityBeanlist=new ArrayList<SupplierDiversityBean>();
		VendorDao dao = new VendorDaoImpl();
		SupplierDiversity supplierDiversity=dao.retrieveSupplierDiversity(appDetails,SupplierDiversityId);
		SupplierDiversityBean supplierDiversityBean=new SupplierDiversityBean();
		if(supplierDiversity != null){
			if(supplierDiversity.getRequestorId().getFirstName() != null)
				supplierDiversityBean.setRequestorName(supplierDiversity.getRequestorId().getFirstName()+" "+supplierDiversity.getRequestorId().getLastName());
			if(supplierDiversity.getBusiness() != null)
				supplierDiversityBean.setBusiness(supplierDiversity.getBusiness());
			if(supplierDiversity.getRequestedOn() != null)
				supplierDiversityBean.setRequestedOn(CommonUtils.convertDateTimeToString(supplierDiversity.getRequestedOn()));
			if(supplierDiversity.getOppurtunityEventName() != null)
				supplierDiversityBean.setOppurtunityEventName(supplierDiversity.getOppurtunityEventName());
			if(supplierDiversity.getSourceEvent() != null)
				supplierDiversityBean.setSourceEvent(supplierDiversity.getSourceEvent());
			if(supplierDiversity.getSourceEventStartDate() != null)
				supplierDiversityBean.setSourceEventStartDate(CommonUtils.convertDateTimeToString(supplierDiversity.getSourceEventStartDate()));
			if(supplierDiversity.getEstimateSpend() != null)
				supplierDiversityBean.setEstimateSpend(Long.valueOf((int)Math.round(supplierDiversity.getEstimateSpend())));
			if(supplierDiversity.getEstimateWorkStartDate() != null)
				supplierDiversityBean.setEstimateWorkStartDate(CommonUtils.convertDateTimeToString(supplierDiversity.getEstimateWorkStartDate()));
			if(supplierDiversity.getLastResponseDate() != null)
				supplierDiversityBean.setLastResponseDate(CommonUtils.convertDateTimeToString(supplierDiversity.getLastResponseDate()));
			if(supplierDiversity.getScopOfWork() != null)
			{
				

				supplierDiversityBean.setScopOfWork(supplierDiversity.getScopOfWork());
			}
			if(supplierDiversity.getGeography() != null)
				supplierDiversityBean.setGeography(supplierDiversity.getGeography());
			if(supplierDiversity.getMiniSupplierRequirement() != null)
				supplierDiversityBean.setMiniSupplierRequirement(supplierDiversity.getMiniSupplierRequirement());
			if(supplierDiversity.getContractInsight() != null)
				supplierDiversityBean.setContractInsight(supplierDiversity.getContractInsight());
			if( supplierDiversity.getCommodityCode()!=null)	{
				String[] commodityDatas=supplierDiversity.getCommodityCode().split(",");
				Map<String,String> commodityList = new HashMap<String,String>();
				commodityList.putAll(dao.commodityUpdateSearchList(appDetails, commodityDatas));
//				List<String> commodityValue = new ArrayList<String>();
				StringBuilder commodityValue =new StringBuilder();
				if (commodityList !=null){
//				int commoditySize = commodityList.size();
				
					int count =1;
					for(String key:commodityList.keySet()){
						//commodityValue.add(commodityList.get(key));
						commodityValue.append(commodityList.get(key)+" ");
						if(count < commodityList.size()){
							commodityValue.append(", ");
						}
						count++;
					}
					
					
				}
				supplierDiversityBean.setCommodityCode(commodityValue);		
			}
			
			if(supplierDiversity.getIncumbentVendorId()!=null){
				String[] vendorData = supplierDiversity.getIncumbentVendorId().split(",");
				Map<String,String> vendorList = new HashMap<String,String>();
				vendorList.putAll(dao.getVendorList(appDetails, vendorData));
//				List<String> vendorValue = new ArrayList<String>();	
				StringBuilder vendorValue =new StringBuilder();
				if (vendorList !=null){
					
					int count=1;
					for(String key:vendorList.keySet()){
//						vendorValue.add(vendorList.get(key));
//						vendorValue[count]	= vendorList.get(key);
						vendorValue.append(vendorList.get(key)+" ");
						if(count < vendorList.size()){
							vendorValue.append(", ");
						}
						count++;
				
					}
				
					}
				supplierDiversityBean.setIncumbentVendor(vendorValue);
			}
		}
		supplierDiversityBeanlist.add(supplierDiversityBean);
				return supplierDiversityBeanlist;
	}
	
	public ActionForward commoditySearchList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,HttpServletResponse response){
		
		HttpSession session=request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
	
		VendorDao dao = new VendorDaoImpl();
		
		String commoditySearchValue = request.getParameter("commoditySearchValue");
		String commodityCode = request.getParameter("commodityCode");
		
		// commodity category & Sector start
		
		if(commoditySearchValue != null){
			
			try {
				JSONArray commoditySearchList=dao.retrieveCommoditySearchList(appDetails, commoditySearchValue);
				response.getOutputStream().write(commoditySearchList.toString().getBytes());
				
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Exception occured - " + e.getMessage());
			}
			
		}
		// commodity category & Sector end
		
		
		// Incumbent diverse supplier names start
		if(commodityCode != null){
			
			try {
				JSONArray vendorList=dao.retrieveVendorList(appDetails, commodityCode);
				response.getOutputStream().write(vendorList.toString().getBytes());
				
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Exception occured - " + e.getMessage());
			}
			
		}
		// Incumbent diverse supplier names end
		
		return null;
	}
	
	public ActionForward commodityCodeData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,HttpServletResponse response){
		
		HttpSession session=request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorDao dao = new VendorDaoImpl();
		
		Integer id = Integer.parseInt(request.getParameter("id"));
		System.out.println("Id="+id);
		
		try {
			JSONArray commodityCodeData=dao.retrieveCommodityCodeData(appDetails, id);
			response.getOutputStream().write(commodityCodeData.toString().getBytes());
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		
		
		return null;
	}
	
	public static String html2text(String html) {
		String normalString = StringEscapeUtils.escapeHtml(html.toString());
		return normalString;
		
//	    return Jsoup.parse(html).text();
	}
}
	
	
