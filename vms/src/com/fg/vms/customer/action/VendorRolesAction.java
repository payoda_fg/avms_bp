package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.customer.dao.impl.VendorRolesDaoImpl;
import com.fg.vms.customer.model.VendorRoles;

/**
 * The Class VendorRolesAction.
 */
public class VendorRolesAction extends DispatchAction {

    /** The Constant SUCCESS. */
    private static final String SUCCESS = "success";

    /**
     * View vendor roles.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewVendorRoles(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();

        UserRolesForm roleForm = (UserRolesForm) form;
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
        List<UserRolesForm> vendorRoles = vendorRolesDaoImpl
                .viewRoles(appDetails);
        roleForm.reset(mapping, request);
        session.setAttribute("vendorRoles", vendorRoles);

        return mapping.findForward(SUCCESS);
    }

    /**
     * Delete vendor roles.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        VendorRolesDaoImpl vendorRolesDaoImpl = new VendorRolesDaoImpl();
        String result = vendorRolesDaoImpl.deleteRole(
                Integer.parseInt(request.getParameter("id")), appDetails);

        List<UserRolesForm> vendorRolesForms = null;

        if (result != null) {
            if (result.equals("deletesuccess")) {
                vendorRolesForms = vendorRolesDaoImpl.viewRoles(appDetails);
                request.setAttribute("vendorRoles", vendorRolesForms);
                ActionMessages messages = new ActionMessages();
                ActionMessage msg = new ActionMessage("delete.userrole");
                messages.add("userrole", msg);
                saveMessages(request, messages);
            }
            if (result.equalsIgnoreCase("reference")) {
                ActionErrors errors = new ActionErrors();
                errors.add("referencerole", new ActionMessage("reference"));
                saveErrors(request, errors);
                return mapping.getInputForward();
            }

        } else {
            return mapping.getInputForward();
        }

        return mapping.findForward(result);
    }

    /**
     * Add the vendor role.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward addVendorRole(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        Integer userId = (Integer) session.getAttribute("userId");
        VendorRolesDaoImpl vendorRolesDaoImpl = new VendorRolesDaoImpl();
        UserRolesForm rolesForm = (UserRolesForm) form;
        String result = vendorRolesDaoImpl.addRole(rolesForm, userId,
                appDetails);
        List<UserRolesForm> vendorRolesForms = null;
        if (result.equals("success")) {
            vendorRolesForms = vendorRolesDaoImpl.viewRoles(appDetails);
            request.setAttribute("vendorRoles", vendorRolesForms);
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("add.userrole");
            messages.add("userrole", msg);
            saveMessages(request, messages);
            rolesForm.reset(mapping, request);
        }
        if (result.equalsIgnoreCase("notUnique")) {
            ActionErrors errors = new ActionErrors();
            errors.add("roleName", new ActionMessage("roleName.unique"));
            saveErrors(request, errors);
            vendorRolesForms = vendorRolesDaoImpl.viewRoles(appDetails);
            request.setAttribute("vendorRoles", vendorRolesForms);
            return mapping.getInputForward();
        }

        return mapping.findForward(result);
    }

    /**
     * Update vendor role.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward update(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        VendorRolesDaoImpl vendorRolesDaoImpl = new VendorRolesDaoImpl();
        HttpSession session = request.getSession();
        List<UserRolesForm> vendorRolesForms = null;
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        UserRolesForm rolesForm = (UserRolesForm) form;

        if (!rolesForm.getHiddenRoleName().equalsIgnoreCase(
                rolesForm.getRoleName())) {
            if (vendorRolesDaoImpl.checkDuplicateRole(rolesForm.getRoleName(),
                    appDetails).equalsIgnoreCase("notUnique")) {

                ActionErrors errors = new ActionErrors();
                errors.add("roleName", new ActionMessage("roleName.unique"));
                saveErrors(request, errors);
                vendorRolesForms = vendorRolesDaoImpl.viewRoles(appDetails);
                request.setAttribute("userRoles", vendorRolesForms);
                return mapping.getInputForward();

            }
        }
        String result = vendorRolesDaoImpl.updateRole(rolesForm,
                rolesForm.getId(), appDetails);
        if (result.equals("success")) {
            vendorRolesForms = vendorRolesDaoImpl.viewRoles(appDetails);
            session.setAttribute("vendorRoles", vendorRolesForms);
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("update.userrole");
            messages.add("userrole", msg);
            saveMessages(request, messages);
            rolesForm.reset(mapping, request);
        }
        return mapping.findForward("success");
    }

    /**
     * Retrive vendor role.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retrive(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        Integer id = Integer.parseInt(request.getParameter("id"));
        VendorRoles vendorRole = (VendorRoles) vendorRolesDaoImpl.retriveRole(
                id, appDetails);
        UserRolesForm rolesForm = (UserRolesForm) form;

        List<UserRolesForm> vendorRolesForms = null;
        vendorRolesForms = vendorRolesDaoImpl.viewRoles(appDetails);
        session.setAttribute("vendorroles", vendorRolesForms);
        if (vendorRole != null) {
            rolesForm.setId(vendorRole.getId());
            rolesForm.setHiddenRoleName(vendorRole.getRoleName());
            rolesForm.setRoleName(vendorRole.getRoleName());
        }
        // forward to edit page
        return mapping.findForward("editvendorrole");
    }
}
