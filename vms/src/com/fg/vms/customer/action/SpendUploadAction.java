/**
 * 
 */
package com.fg.vms.customer.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.fg.vms.admin.dto.SpendDataDTO;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.SpendUploadDao;
import com.fg.vms.customer.dao.Tier2VendorDao;
import com.fg.vms.customer.dao.impl.SpendUploadDaoImpl;
import com.fg.vms.customer.dao.impl.Tier2VendorDaoImpl;
import com.fg.vms.customer.dto.SpendIndirectDto;
import com.fg.vms.customer.model.SpendDataMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.SpendUploadForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;

/**
 * This class represents the spend data upload actions.
 * 
 * @author pirabu
 * 
 */
public class SpendUploadAction extends DispatchAction {

    /** Logger of the system. */
    private Logger logger = Constants.logger;

    /**
     * Show upload page.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward showUploadPage(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        SpendUploadForm spendUpload = (SpendUploadForm) form;
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        List<VendorMaster> vendors = null;
        Tier2VendorDao tier2VendorDao=new Tier2VendorDaoImpl();

        if (session.getAttribute("vendorUser") != null) {
            vendors = tier2VendorDao.tire2Vendors(appDetails);
        }

        spendUpload.setVendors(vendors);
        return mapping.findForward("spendupload");
    }

    /**
     * Upload file.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward uploadFile(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SpendUploadForm spendUpload = (SpendUploadForm) form;

        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        SpendUploadDao spendUploadDao = new SpendUploadDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        VendorContact currentVendor = (VendorContact) session
                .getAttribute("vendorUser");
        FormFile formFile = null;
        FileOutputStream outputStream = null;
        try {
            formFile = spendUpload.getFile();

            String path = getServlet().getServletContext().getRealPath("")
                    + "/" + formFile.getFileName();
            outputStream = new FileOutputStream(new File(path));
            outputStream.write(formFile.getFileData());
            SpendDataMaster spendDataMaster = spendUploadDao.uploadSpendData(
                    spendUpload, userDetails, userId, path, currentVendor);

            BufferedReader br = new BufferedReader(new FileReader(path));
            String strLine = "";
            StringTokenizer st = null;
            ArrayList<SpendDataDTO> al_spendDataList = new ArrayList<SpendDataDTO>();

            int i = 0;
            while ((strLine = br.readLine()) != null) {
                if (i != 0) {
                    SpendDataDTO spendDataDTO = new SpendDataDTO();
                    st = new StringTokenizer(strLine, ",");
                    while (st.hasMoreTokens()) {

                        spendDataDTO.setCategory(st.nextToken());
                        String spendDate = st.nextToken();
                        if (!spendUpload.getYearOfData().equalsIgnoreCase(
                                String.valueOf(CommonUtils.dateConvertValue(
                                        spendDate).getYear() + 1900))) {

                            spendDataDTO.setIsValid((byte) 0);
                        } else {
                            spendDataDTO.setIsValid((byte) 1);
                        }

                        spendDataDTO.setSpenDate(spendDate);
                        spendDataDTO.setNaicsCode(st.nextToken());
                        spendDataDTO.setSpentValue(st.nextToken());
                        spendDataDTO.setCurrency(st.nextToken());
                        spendDataDTO.setDiverseCerificateCode(st.nextToken());
                    }
                    al_spendDataList.add(spendDataDTO);
                }
                i++;
            }
            if (spendDataMaster != null && spendDataMaster.getId() != null) {
                // session.setAttribute("spenddatalist", al_spendDataList);
                spendUpload.setSpendDataDTOList(al_spendDataList);
                session.setAttribute("spendDataMaster", spendDataMaster);
            }

        } catch (Exception e) {
            logger.info("Exception while reading csv file: " + e);
            return mapping.getInputForward();
        }

        return mapping.findForward("success");
    }

    /**
     * Save uploaded data.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward saveUploadedData(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ArrayList<SpendDataDTO> al_spendDataList;
        SpendUploadDao spendUploadDao = new SpendUploadDaoImpl();
        SpendUploadForm spendUploadForm = (SpendUploadForm) form;
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        String result = "failure";
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        if (session.getAttribute("spenddatalist") != null
                && session.getAttribute("spendDataMaster") != null) {
            al_spendDataList = (ArrayList<SpendDataDTO>) session
                    .getAttribute("spenddatalist");
            SpendDataMaster spendDataMaster = (SpendDataMaster) session
                    .getAttribute("spendDataMaster");
            result = spendUploadDao.saveUploadedSpendData(userDetails, userId,
                    al_spendDataList, spendDataMaster);
            if (result.equalsIgnoreCase("spenduploadsuccess")) {
                // session.removeAttribute("spenddatalist");

            }

        } else {
            return mapping.getInputForward();
        }

        return mapping.findForward(result);
    }

    /**
     * Calculate direct sales.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward calculateDirectSales(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        SpendUploadForm spendUploadForm = (SpendUploadForm) form;

        List<SpendIndirectDto> resultList = calculate(spendUploadForm);

        session.setAttribute("resultList", resultList);
        return mapping.findForward("spendIndirect");

    }

    /**
     * Save pro rate supply.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    @SuppressWarnings("unchecked")
    public ActionForward saveProRateSupply(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        SpendUploadDao spendUploadDao = new SpendUploadDaoImpl();
        SpendUploadForm spendUploadForm = (SpendUploadForm) form;
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        String result = "failure";
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        ArrayList<SpendDataDTO> al_spendDataList;
        if (spendUploadForm.getSpendDataDTOList() != null
                && spendUploadForm.getSpendDataDTOList().size() != 0
                && session.getAttribute("spendDataMaster") != null) {
            al_spendDataList = (ArrayList<SpendDataDTO>) spendUploadForm
                    .getSpendDataDTOList();
            SpendDataMaster spendDataMaster = (SpendDataMaster) session
                    .getAttribute("spendDataMaster");
            // Update totalPrimeSupplierSales and totalCustomerSales
            spendUploadDao.updateSpendDataMaster(spendUploadForm, appDetails,
                    spendDataMaster);
            result = spendUploadDao.saveUploadedSpendData(appDetails, userId,
                    al_spendDataList, spendDataMaster);
            if (result.equalsIgnoreCase("spenduploadsuccess")) {
                // session.removeAttribute("spenddatalist");
                spendUploadForm.setSpendDataDTOList(null);
                spendUploadForm.setSpendEndDate("");
                spendUploadForm.setSpendStartDate("");
                spendUploadForm.setYearOfData("");
                spendUploadForm.setVendor("0");

            }

        } else {
            return mapping.getInputForward();
        }

        ArrayList<SpendIndirectDto> al_spendIndirectDataList;
        if (session.getAttribute("resultList") != null
                && session.getAttribute("spendDataMaster") != null) {
            al_spendIndirectDataList = (ArrayList<SpendIndirectDto>) session
                    .getAttribute("resultList");
            SpendDataMaster spendDataMaster = (SpendDataMaster) session
                    .getAttribute("spendDataMaster");

            result = spendUploadDao.saveProRateSupply(al_spendIndirectDataList,
                    spendDataMaster, appDetails);
            if (result.equalsIgnoreCase("spenduploadsuccess")) {
                session.removeAttribute("resultList");
                spendUploadForm.setPercentageOfDirectSales(null);
                spendUploadForm.setTotalDirectSales(null);
                spendUploadForm.setTotalPrimeSupplierSales(null);

            }
        }

        return mapping.findForward(result);
    }

    /**
     * Calculate spend indirect values.
     * 
     * @param spendUploadForm
     *            the spend upload form
     * @return the list
     */
    private List<SpendIndirectDto> calculate(SpendUploadForm spendUploadForm) {

        // Store the unique Diverse certificate code into map for calculate the
        // indirect spend.
        Map<String, String> diverseCertificateCode = new HashMap<String, String>();

        // Store the valid spend uploaded data in list
        List<SpendDataDTO> listOfValidSpendValues = new ArrayList<SpendDataDTO>();
        for (SpendDataDTO spendData : spendUploadForm.getSpendDataDTOList()) {
            SpendDataDTO spendUploadData = new SpendDataDTO();
            if (spendData.getIsValid() == (byte) 1) {
                diverseCertificateCode.put(
                        spendData.getDiverseCerificateCode(),
                        spendData.getDiverseCerificateCode());
                spendUploadData.setDiverseCerificateCode(spendData
                        .getDiverseCerificateCode());
                spendUploadData.setSpentValue(spendData.getSpentValue());

                listOfValidSpendValues.add(spendUploadData);

            }

        }

        // Get a set of the Diverse Certificate code
        Set<Entry<String, String>> set = diverseCertificateCode.entrySet();
        // Get an iterator
        Iterator<Entry<String, String>> i = set.iterator();
        // Calculate the Indirect Spend value
        List<SpendIndirectDto> listOfIndirectSpendValues = new ArrayList<SpendIndirectDto>();

        Double totalDirectSales = CommonUtils.deformatMoney(spendUploadForm
                .getTotalDirectSales());
        while (i.hasNext()) {
            SpendIndirectDto spendIndirect = new SpendIndirectDto();
            Map.Entry me = (Map.Entry) i.next();

            // Sum of Direct spend value
            Double sumOfSpendValue = (double) 0;
            for (SpendDataDTO spendDataDTO : listOfValidSpendValues) {

                if (spendDataDTO.getDiverseCerificateCode().equalsIgnoreCase(
                        me.getKey().toString())) {
                    spendIndirect.setDiverseCertificate(spendDataDTO
                            .getDiverseCerificateCode());
                    sumOfSpendValue += Double.parseDouble(spendDataDTO
                            .getSpentValue());
                    spendIndirect.setDirectSpendValue(sumOfSpendValue);
                    spendIndirect.setDirect_Spendpercent((double) (Math
                            .round((sumOfSpendValue * spendUploadForm
                                    .getPercentageOfDirectSales()) / 100)));
                    spendIndirect
                            .setProrated_amount((double) (Math
                                    .round((sumOfSpendValue / totalDirectSales) * 100)));
                }
            }
            listOfIndirectSpendValues.add(spendIndirect);
        }

        return listOfIndirectSpendValues;

    }

    /**
     * Retrieve uploaded spend data.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retrieveUploadedSpendData(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        SpendUploadForm spendForm = (SpendUploadForm) form;
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        VendorContact currentUser = (VendorContact) session
                .getAttribute("vendorUser");
        SpendUploadDao uploadDao = new SpendUploadDaoImpl();
        List<SpendDataMaster> spendDataMasters = (List<SpendDataMaster>) uploadDao
                .retrieveUploadedSpendData(userDetails, currentUser);
        spendForm.setSpendDatas(spendDataMasters);
        return mapping.findForward("viewspenduploadeddata");
    }

    /**
     * Retrive spend details.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retriveSpendDetails(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        Integer selectedSpendMasterId = Integer.parseInt(request.getParameter(
                "id").toString());

        session.setAttribute("selectedSpendMasterId", selectedSpendMasterId);

        return mapping.findForward("viewspenduploadeddata");
    }
}
