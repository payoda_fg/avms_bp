package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ItemsDao;
import com.fg.vms.customer.dao.impl.ItemsDaoImpl;
import com.fg.vms.customer.model.Items;
import com.fg.vms.customer.pojo.ItemForm;

/**
 * Action class which implements Dispatch Action
 * 
 * @author hemavaishnavi
 * 
 */
public class ItemAction extends DispatchAction {
    /**
     * Method to add new Items with server validation
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward addItems(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	ItemForm itemsForm = (ItemForm) form;
	ItemsDaoImpl itemsDaoImpl = new ItemsDaoImpl();
	Integer userId = (Integer) session.getAttribute("userId");
	String result = itemsDaoImpl.addItem(itemsForm, userId, appDetails);
	List<Items> item = null;
	if (result.equals("itemcode")) {
	    ActionErrors errors = new ActionErrors();
	    errors.add("itemCode", new ActionMessage("itemCode.unique"));
	    saveErrors(request, errors);
	    mapping.findForward(mapping.getInput());
	}
	if (result.equals("failure")) {
	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("items.failure");
	    messages.add("item", msg);
	    saveMessages(request, messages);
	}
	if (result.equals("success")) {
	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("items.ok");
	    messages.add("item", msg);
	    saveMessages(request, messages);
	    itemsForm.reset(mapping, request);
	}
	item = itemsDaoImpl.listItems(appDetails);
	session.setAttribute("items", item);
	return mapping.findForward("success");
    }

    /**
     * Method to view the list of Items
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewItems(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	List<Items> item = null;
	ItemsDao itemsDao = new ItemsDaoImpl();
	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	item = itemsDao.listItems(appDetails);
	session.setAttribute("items", item);
	return mapping.findForward("success");
    }

    /**
     * Method to retrieve the selected item for update
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retriveitem(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	ItemsDaoImpl itemsDaoImpl = new ItemsDaoImpl();
	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer code = Integer.parseInt(request.getParameter("itemid"));
	Items items = itemsDaoImpl.retriveItem(code, appDetails);
	if (items != null) {
	    session.setAttribute("currentitem", items);
	}
	List<Items> items1 = null;
	items1 = itemsDaoImpl.listItems(appDetails);
	session.setAttribute("items", items1);
	return mapping.findForward("modify");
    }

    /**
     * Method to update the selected item
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updateItems(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {
	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	ItemsDaoImpl itemsDaoImpl = new ItemsDaoImpl();
	ItemForm itemForm = (ItemForm) form;
	Integer itemid = Integer.parseInt(itemForm.getItemId());
	Integer userId = (Integer) session.getAttribute("userId");
	String result = itemsDaoImpl.updateItem(itemForm, itemid, userId,
		appDetails);
	List<Items> items = null;
	if (result.equals("success")) {
	    items = itemsDaoImpl.listItems(appDetails);
	    request.setAttribute("items", items);
	    session.removeAttribute("items");
	}
	if (result.equals("failure")) {
	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("items.failure");
	    messages.add("item", msg);
	    saveMessages(request, messages);
	}
	return mapping.findForward(result);
    }

    /**
     * Method to Delete Items in UI
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward deleteItem(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	ItemsDaoImpl itemsDaoImpl = new ItemsDaoImpl();
	String result = itemsDaoImpl.deleteItem(
		Integer.parseInt(request.getParameter("itemid")), appDetails);
	List<Items> items = null;
	if (result.equals("failure")) {
	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("items.failure");
	    messages.add("item", msg);
	    saveMessages(request, messages);
	}
	if (result.equals("success")) {
	    items = itemsDaoImpl.listItems(appDetails);
	    session.setAttribute("items", items);
	}

	return mapping.findForward("success");
    }
}
