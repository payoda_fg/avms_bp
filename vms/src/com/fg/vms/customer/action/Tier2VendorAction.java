/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.CustomerVendorTier2Vendor;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;

/**
 * @author gpirabu
 * 
 */
public class Tier2VendorAction extends DispatchAction {

	public ActionForward findAllTier2Vendors(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorContact contact = (VendorContact) session
				.getAttribute("vendorUser");

		Integer vendorId = contact.getVendorId().getId();
		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
		String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
				+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
				+ vendorId;

		List<Tier2ReportDto> tier2VendorList = (List<Tier2ReportDto>) curdDao
				.findAllByQuery(appDetails, query);

		session.setAttribute("tier2VendorList", tier2VendorList);

		return mapping.findForward("success");
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward assignTier2VendorToVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorContact contact = (VendorContact) session
				.getAttribute("vendorUser");

		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();

		String vendorId = request.getParameter("id");
		Integer parent = contact.getVendorId().getId();

		VendorMaster vendorMaster = (VendorMaster) curdDao.find(appDetails,
				"from VendorMaster vm where vm.id=" + vendorId);

		CURDDao<CustomerVendorTier2Vendor> curdDao1 = new CURDTemplateImpl<CustomerVendorTier2Vendor>();
		CustomerVendorTier2Vendor customerVendorTier2Vendor = new CustomerVendorTier2Vendor();
		customerVendorTier2Vendor.setVendorid(vendorMaster);
		customerVendorTier2Vendor.setParentvendorid(parent);
		customerVendorTier2Vendor.setCreatedby(contact.getId());
		customerVendorTier2Vendor.setCreatedon(new Date());
		customerVendorTier2Vendor.setIsactive((short) 1);
		curdDao1.save(customerVendorTier2Vendor, appDetails);

		return mapping.findForward("assigned");
	}

}
