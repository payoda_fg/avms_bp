/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.model.Ethnicity;

/**
 * @author gpirabu
 * 
 */
public class EthnicityAction<T> extends DispatchAction {
	int isUnique;

    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward("success");
    }

    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward listEthnicity(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        /*
         * Application details such as database settings, application
         * personalize settings of customer.
         */
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        CURDDao<T> ethnicityDao = new EthnicityDaoImpl<T>();
        session.setAttribute("ethnicityList", ethnicityDao.list(appDetails, null));

        return mapping.findForward("ethnicity");
    }

    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward updateEthnicity(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        // Ethnicity tutut
        // id
        // oper add
        HttpSession session = request.getSession();
        /*
         * Application details such as database settings, application
         * personalize settings of customer.
         */
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer currentUser = (Integer) session.getAttribute("userId");
        if (request.getParameter("oper") != null
                && "add".equalsIgnoreCase(request.getParameter("oper"))) 
        {
        	CURDDao<T> ethnicityDao = new EthnicityDaoImpl<T>();
        	List<Ethnicity> listEthnicity=(List<Ethnicity>)ethnicityDao.listEthnicities(appDetails);
        	
        	Iterator<Ethnicity> itr=listEthnicity.iterator();
        	String ethName=request.getParameter("Ethnicity");
        	Ethnicity ety=new Ethnicity();
        	int isExisted=0;
            isUnique=0;//for identifying unique record or not
        	
        	while(itr.hasNext())
        	{
        		ety=(Ethnicity)itr.next();
        		String name=ety.getEthnicity();
        		Short isActive=ety.getIsactive();
        		
        		if(name.equalsIgnoreCase(ethName))
        		{
        			if(isActive==1)
        				isUnique=1;
        			if(isActive==0)
        			{
        				isUnique=0;
        				Ethnicity ethnicity = new Ethnicity();
                        ethnicity.setId(Integer.valueOf(ety.getId()));
                        ethnicity.setEthnicity(name);
                        ethnicity.setModifiedBy(currentUser);
                        ethnicity.setModifiedOn(new Date());
                        ethnicity.setIsactive((byte) 1);

                        CURDDao<T> enthnicityDao = new EthnicityDaoImpl<T>();
                        enthnicityDao.update((T) ethnicity, appDetails);
        	        }
    				isExisted=1;
        			break;
        		}
        	}
            if (request.getParameter("Ethnicity") != null && isExisted==0 && isUnique==0) 
            {
                Ethnicity ethnicity = new Ethnicity();
                ethnicity.setEthnicity(request.getParameter("Ethnicity"));
                ethnicity.setCreatedBy(currentUser);
                ethnicity.setCreatedOn(new Date());
                ethnicity.setIsactive((byte)1);

                CURDDao<T> enthnicityDao = new EthnicityDaoImpl<T>();
                enthnicityDao.save((T) ethnicity, appDetails);
            }
        } else if (request.getParameter("oper") != null
                && "del".equalsIgnoreCase(request.getParameter("oper"))) {
            if (null != request.getParameter("id")) {
                Ethnicity ethnicity = new Ethnicity();
                ethnicity.setId(Integer.valueOf(request.getParameter("id")));
                CURDDao<T> enthnicityDao = new EthnicityDaoImpl<T>();
                enthnicityDao.delete((T) ethnicity, appDetails);
            }
        } else 
		{
            if (request.getParameter("id") != null) 
			{          	
            	Integer currentId=Integer.parseInt(request.getParameter("id"));
            	String ethName=request.getParameter("Ethnicity");
            	Integer i=checkAvailability(currentId,ethName,appDetails);

                Ethnicity ethnicity = new Ethnicity();
                ethnicity.setId(Integer.valueOf(request.getParameter("id")));
                ethnicity.setEthnicity(request.getParameter("Ethnicity"));
                ethnicity.setModifiedBy(currentUser);
                ethnicity.setModifiedOn(new Date());
                ethnicity.setIsactive((byte) 1);
                if(isUnique!=1)
                {
	                CURDDao<T> enthnicityDao = new EthnicityDaoImpl<T>();
    	            enthnicityDao.update((T) ethnicity, appDetails);
                }
            }
        }
        return mapping.findForward("success");
    }
    
    public ActionForward getJSONData(ActionMapping mapping, ActionForm form,
       		   HttpServletRequest request, HttpServletResponse response)throws Exception {

       	JSONObject jsonObject = new JSONObject();
       	jsonObject.put("isUnique", isUnique);
       	response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();

   	    return null;
      }
    
    public int checkAvailability(Integer id,String ethName,UserDetailsDto appDetails)
    {
    	Integer currentId=id;
    	CURDDao<Ethnicity> curdDao = new CURDTemplateImpl<Ethnicity>();
		List<Ethnicity> currentEthnicities = curdDao.list(appDetails,
				" from Ethnicity e where e.id !="+currentId);
		Iterator<Ethnicity> itr=currentEthnicities.iterator();
		Ethnicity ety=new Ethnicity();
   	    String name=null;
   	    
   	    while(itr.hasNext())
     	{
   	    	ety=(Ethnicity)itr.next();
     	    name=ety.getEthnicity();
     		if(name.equalsIgnoreCase(ethName))
   		    {
     			isUnique=1;     			
     			break;
   		    }
     		else
     		{
     			isUnique=0;
     		}
     	}
   	    return isUnique;
    }
}