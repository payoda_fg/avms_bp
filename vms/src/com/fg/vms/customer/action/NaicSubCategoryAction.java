/*
 * NaicSubCategoryAction.java 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dao.impl.NAICSubCategoryImpl;
import com.fg.vms.customer.dao.impl.NaicsCategoryDaoImpl;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.pojo.NaicSubCategoryForm;

/**
 * @author vinoth
 * 
 */
public class NaicSubCategoryAction extends DispatchAction {

    /**
     * Retrieve the list of sub categories.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewNAICSubCategory(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	List<NAICSubCategory> naicsub1 = null;
	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	// session.removeAttribute("naicsub1");
	// session.removeAttribute("categoryName1");
	NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();

	naicsub1 = naicSubCategoryImpl.retrieveNAICSubCategories(appDetails,
		null);
	NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
	List naicsCategories = naicsCategoryDao.view(appDetails);

	List<NAICSubCategory> categories = naicSubCategoryImpl
		.retrieveSubCategoryById(0, appDetails);
	session.setAttribute("naicsub1", categories);
	session.setAttribute("categoryName1", naicsCategories);
	return mapping.findForward("viewsuccess");

    }

    /**
     * Insert new NAIC Sub-Category
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward insertNAICSubCategory(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer userId = (Integer) session.getAttribute("userId");
	Integer category = (Integer) session.getAttribute("categoryValue");

	NaicSubCategoryForm naicSubCategoryForm = (NaicSubCategoryForm) form;
	NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();

	int categoryId = naicSubCategoryForm.getNaicCategory();
	String result = naicSubCategoryImpl.createNAICSubCategory(
		naicSubCategoryForm, userId, categoryId, appDetails);

	if (result.equals("success")) {
	    List<NAICSubCategory> categories = naicSubCategoryImpl
		    .retrieveSubCategoryById(category, appDetails);
	    session.setAttribute("naicsub1", categories);

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("naicsMaster.ok");
	    messages.add("master", msg);
	    saveMessages(request, messages);
	    naicSubCategoryForm.reset(mapping, request);
	}

	return mapping.findForward("savesuccess");
    }

    /**
     * Method to update the NAIC sub-category into inactive (Delete action in
     * UI)
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward deleteNAICSubCategory(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	String result = naicSubCategoryImpl.deleteNAICSubCategory(
		Integer.parseInt(request.getParameter("id")), appDetails);
	Integer category = (Integer) session.getAttribute("categoryValue");
	
	if (result.equals("reference")) {

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("naicsMaster.delete.reference");
	    messages.add("referenceSubCategory", msg);
	    saveMessages(request, messages);
	    return mapping.getInputForward();
	}

	if (result.equals("success")) {
	    List<NAICSubCategory> categories = naicSubCategoryImpl
		    .retrieveSubCategoryById(category, appDetails);
	    session.setAttribute("naicsub1", categories);
	    
	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("naicsMaster.delete.ok");
	    messages.add("master", msg);
	    saveMessages(request, messages);
	}

	return mapping.findForward("deletesuccess");
    }

    /**
     * Method to retrieve the selected NAIC Sub-Category for update
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retriveNAICSubCategory(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
	NaicSubCategoryForm naicSubCategoryForm = (NaicSubCategoryForm) form;

	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer id = Integer.parseInt(request.getParameter("id").toString());
	Integer category = (Integer) session.getAttribute("categoryValue");

	NAICSubCategory subCategory = naicSubCategoryImpl.retrieveSubCategory(
		id, appDetails);

	if (subCategory != null) {
	    naicSubCategoryForm.setId(subCategory.getId());
	    naicSubCategoryForm.setNaicSubCategoryDesc(subCategory
		    .getNaicSubCategoryDesc());
	    naicSubCategoryForm.setIsActive(subCategory.getIsActive()
		    .toString());
	}
	List<NAICSubCategory> categories = naicSubCategoryImpl
		.retrieveSubCategoryById(category, appDetails);
	session.setAttribute("naicsub1", categories);
	return mapping.findForward("retrievesuccess");
    }

    /**
     * Method to update the selected NAICS Sub-Category
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updateNAICSubCategory(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	HttpSession session = request.getSession();
	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer currentUserId = (Integer) session.getAttribute("userId");
	Integer category = (Integer) session.getAttribute("categoryValue");

	NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
	NaicSubCategoryForm naicSubCategoryForm = (NaicSubCategoryForm) form;

	String result = naicSubCategoryImpl.updateNAICSubCategory(
		naicSubCategoryForm, naicSubCategoryForm.getId(),
		currentUserId, appDetails);
	if (result.equals("success")) {
	    List<NAICSubCategory> categories = naicSubCategoryImpl
		    .retrieveSubCategoryById(category, appDetails);
	    session.setAttribute("naicsub1", categories);
	    naicSubCategoryForm.reset(mapping, request);
	}

	return mapping.findForward("updatesuccess");
    }

    /**
     * Retrieve the list of Sub-categories by Categories.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewSubCategoriesById(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	// get the selected category id
	Integer categoryId = Integer.parseInt(request
		.getParameter("categoryId").toString());
	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	session.setAttribute("categoryValue", categoryId);

	NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();

	// Get category list.
	List<NAICSubCategory> categories = naicSubCategoryImpl
		.retrieveSubCategoryById(categoryId, appDetails);
	NaicsCategory category = naicSubCategoryImpl.retriveNaicsCategory(
		categoryId, appDetails);
	NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
	List naicsCategories = naicsCategoryDao.view(appDetails);

	session.setAttribute("categoryId2", category);
	session.setAttribute("naicsub1", categories);
	session.setAttribute("categoryName2", naicsCategories);

	// NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
	// List naicsCategories = naicsCategoryDao.view(appDetails);
	session.setAttribute("categoryName1", naicsCategories);

	// forward to the privileges page.
	return mapping.findForward("viewsuccess");
    }

}
