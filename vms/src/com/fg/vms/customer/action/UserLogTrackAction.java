/**
 * 
 */
package com.fg.vms.customer.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.UserLogTrackDao;
import com.fg.vms.customer.dao.impl.UserLogTrackDaoImpl;
import com.fg.vms.customer.pojo.UserLogTrackForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;

/**
 * @author venkatesh
 * 
 */
public class UserLogTrackAction extends DispatchAction {
	/** Logger of the system. */
	private Logger logger = Constants.logger;
	
	private List<UserLogTrackForm> userLogTrackReportListPdf;

	/**
	 * @return the userLogTrackReportListPdf
	 */
	public List<UserLogTrackForm> getUserLogTrackReportListPdf() {
		return userLogTrackReportListPdf;
	}

	/**
	 * @param userLogTrackReportListPdf the userLogTrackReportListPdf to set
	 */
	public void setUserLogTrackReportListPdf(
			List<UserLogTrackForm> userLogTrackReportListPdf) {
		this.userLogTrackReportListPdf = userLogTrackReportListPdf;
	}

	/**
	 * Retrieve Users Login Details.	 
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewUserLoginDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception,
			NullPointerException {
		logger.info("Method to view the Login Details of All Users ..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		UserLogTrackDao userLogTrackdao = new UserLogTrackDaoImpl();

		List<UserLogTrackForm> logTracks = null;

		logTracks = userLogTrackdao.setUserLoginDetails(userDetails);

		if (logTracks != null) {
			session.setAttribute("logTracks", logTracks);
			setUserLogTrackReportListPdf(logTracks);
		}
		return mapping.findForward("showUserLogTrackPage");
	}

	/**
	 * Retrieves User Login Time Informations.
	 *	 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewUserLoginTimes(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception,
			NullPointerException {
		logger.info("Method to view the Login Times of All Users.");		

		HttpSession session = request.getSession();
		
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		List<UserLogTrackForm> loginTimes = null;

		UserLogTrackDao userLogTrackdao = new UserLogTrackDaoImpl();

		loginTimes = userLogTrackdao.getUserLoginTimes(userDetails,
				request.getParameter("userEmailId"));

		if (loginTimes != null) {
			session.setAttribute("loginTimes", loginTimes);
		}
		return mapping.findForward("showUserLogTimesPage");
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward generateViewUserLogReportInPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("Method to Generate View User log in PDF...");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try 
		{
			String reportName = "UserLogReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getUserLogTrackReportListPdf());

			InputStream is = JRLoader.getResourceInputStream("user_log_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
			
			logger.info("PDF Generated for View User log...");
		} 
		catch (IOException e) 
		{
			System.out.println("Exception occured - " + e.getMessage());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
}