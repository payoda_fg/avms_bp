/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CommodityDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CommodityCategoryDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityHelperDaoImpl;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.model.CustomerCommodity;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.pojo.CommodityForm;
import com.fg.vms.util.Constants;

/**
 * @author vinoth
 * 
 */
public class CommodityAction<T> extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;
	int isUnique;	

	/**
	 * View Commodity category page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewCommodityCategory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to view the commodities page..");
		
		return mapping.findForward("viewsuccess");
	}

	/**
	 * List the commodity categories available.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward listCommodityCategory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to view the list of active commodity category..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CURDDao<T> commodityDao = new CommodityCategoryDaoImpl<T>();
		session.setAttribute("customerCommodityCategories",
				commodityDao.list(appDetails, null));

		return mapping.findForward("viewcommodities");
	}

	/**
	 * Add or update new commodity category from jqgrid.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward updateCommodityCategory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Add/Update commodity category..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer currentUser = (Integer) session.getAttribute("userId");
		if (request.getParameter("oper") != null
				&& "add".equalsIgnoreCase(request.getParameter("oper"))) {				
			//Making inactive record as active starts from here.
			CommodityCategoryDaoImpl commodityDaoImpl = new CommodityCategoryDaoImpl();
	        List<CustomerCommodityCategory> commodityCategories=(List<CustomerCommodityCategory>)commodityDaoImpl.listCommodityCategories(appDetails);
	        Iterator<CustomerCommodityCategory> itr=commodityCategories.iterator();
	           
	        String categoryCode=request.getParameter("categoryCode");
        	String categoryDesc=request.getParameter("categoryDesc");
        	CustomerCommodityCategory ccategory = new CustomerCommodityCategory();
            int isExisted=0;
            isUnique=0;//for identifying unique record or not
	        	
            while(itr.hasNext())
	        {
            	ccategory=(CustomerCommodityCategory)itr.next();
	        	String testCode=ccategory.getCategoryCode();
	        	Byte isActive=ccategory.getIsActive();
	        		
	        	if(categoryCode.equals(testCode))
	        	{
	        		if(isActive==1)
	        			isUnique=1;
	        			
	        		if(isActive==0)
	        		{
	        			CustomerCommodityCategory category = new CustomerCommodityCategory();
	        			category.setId(Integer.valueOf(ccategory.getId()));
	        			category.setCategoryCode(categoryCode);
	        			category.setCategoryDescription(categoryDesc);
	        			category.setModifiedBy(currentUser);
	        			category.setModifiedOn(new Date());
	        			category.setIsActive((byte)1);
	        			CURDDao<T> commodityDao = new CommodityCategoryDaoImpl<T>();
	    				commodityDao.update((T) category, appDetails);
	        		}
    				isExisted=1;
        			break;
        		}
        	}
        	//Making inactive record as active end.
            if (request.getParameter("categoryCode") != null
					&& request.getParameter("categoryDesc") != null && isExisted==0 && isUnique==0) 
            {
	          	CustomerCommodityCategory category = new CustomerCommodityCategory();
				category.setCategoryCode(request.getParameter("categoryCode"));
				category.setCategoryDescription(request
						.getParameter("categoryDesc"));
				category.setIsActive((byte) 1);
				category.setCreatedBy(currentUser);
				category.setCreatedOn(new Date());
				CURDDao<T> commodityDao = new CommodityCategoryDaoImpl<T>();
				commodityDao.save((T) category, appDetails);
			}
		} 
		else 
		{
			if (request.getParameter("id") != null) 
			{
				if (request.getParameter("oper") != null
						&& "edit"
								.equalsIgnoreCase(request.getParameter("oper"))) 
				{
					CustomerCommodityCategory category = new CustomerCommodityCategory();
					category.setId(Integer.valueOf(request.getParameter("id")));
					String categoryCode=request.getParameter("categoryCode");
					
					Integer currentId=Integer.parseInt(request.getParameter("id"));
					isUnique=checkAvailability(currentId,categoryCode,appDetails);
					category.setCategoryCode(request
							.getParameter("categoryCode"));
					category.setCategoryDescription(request
							.getParameter("categoryDesc"));
					category.setIsActive((byte) 1);
					category.setModifiedBy(currentUser);
					category.setModifiedOn(new Date());
					if(isUnique != 1)
					{
						CURDDao<T> commodityDao = new CommodityCategoryDaoImpl<T>();
						commodityDao.update((T) category, appDetails);
					}
					
				} 
				else if (request.getParameter("oper") != null
						&& "del".equalsIgnoreCase(request.getParameter("oper"))) 
				{
					CustomerCommodityCategory category = new CustomerCommodityCategory();
					category.setId(Integer.valueOf(request.getParameter("id")));
					CURDDao<T> commodityDao = new CommodityCategoryDaoImpl<T>();
					commodityDao.delete((T) category, appDetails);
				}
			}
		}
		return mapping.findForward("success");
	}

	/**
	 * View Commodity page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewCommodity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to view the commodity page..");
		
		
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityForm commodityForm=(CommodityForm)form;
		CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
//		 Market Sector List
		String query="From MarketSector where isActive=1 order by sectorDescription";
		List<MarketSector> marketSectors=commodityDaoImpl.marketSectorAndCommodities(appDetails, query);
		if(marketSectors!=null)
			commodityForm.setMarketSectors(marketSectors);
		session.setAttribute("marketsectors", marketSectors);
		
//		Market Sub Sector List
		 query="From CustomerCommodityCategory where isActive=1 order by categoryDescription";
		List<CustomerCommodityCategory> marketSubSectors=commodityDaoImpl.marketSectorAndCommodities(appDetails, query);
		if(marketSubSectors!=null)
			commodityForm.setMarketSubSectors(marketSubSectors);
		session.setAttribute("marketsubsectors", marketSubSectors);
		
//		Commodity Group List
		/*query="From CustomerCommodity where isActive=1 order by commodityDescription";
		List<CustomerCommodity> commoditygroups=commodityDaoImpl.marketSectorAndCommodities(appDetails, query);
		session.setAttribute("commoditygroups", commoditygroups);*/

		return mapping.findForward("viewcommodity");
	}

	/**
	 * List the commodity available.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward listCommodity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to view the list of active commodities..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CURDDao<T> commodityDao = new CommodityDaoImpl<T>();
		session.setAttribute("customerCommodities",
				commodityDao.list(appDetails, null));

		return mapping.findForward("viewcommodityList");
	}
	
	/**
	 * To List the Commodities based on Market SubSector.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward listCommodityBasedOnSubSector(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("Method to View the List of Active Commodities based on Market SubSector...");
		HttpSession session = request.getSession();
		
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		CURDDao<T> commodityDao = new CommodityDaoImpl<T>();
		
		List<CustomerCommodity> commodities = null;
		Integer marketSubSectorId = null;
		
		if(request.getParameter("marketSubSectorId") != null) {
			marketSubSectorId = Integer.parseInt(request.getParameter("marketSubSectorId"));
			commodities = (List<CustomerCommodity>) commodityDao.listCommoditiesBasedOnSubSector(appDetails, marketSubSectorId);
		}
		
		session.setAttribute("customerCommodities",	commodities);

		return mapping.findForward("viewcommodityList");
	}

	/**
	 * Add or update new commodity from jqgrid.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward updateCommodity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Add/Update commodity..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityDao commodityDao1 = new CommodityHelperDaoImpl();
		Integer currentUser = (Integer) session.getAttribute("userId");
		if (request.getParameter("oper") != null
				&& "add".equalsIgnoreCase(request.getParameter("oper"))) {			
			//Making Inactive  commodity record as active starts from here.
			CommodityDaoImpl commodityDaoImpl = new CommodityDaoImpl();
	        List<CustomerCommodity> commodities=(List<CustomerCommodity>)commodityDaoImpl.listCommodites(appDetails);
	        Iterator<CustomerCommodity> itr=commodities.iterator();
	        String commodityCode=request.getParameter("commodityCode");
        	String commodityDesc=request.getParameter("commodityDesc");
        	CustomerCommodityCategory commodityCategoryId = commodityDao1
					.getCommodity(Integer.parseInt(request
							.getParameter("commodityCategoryId")),
							appDetails);
        	CustomerCommodity ccommodity = new CustomerCommodity();
            int isExisted=0;
            isUnique=0;
            while(itr.hasNext())
	        {
            	ccommodity=(CustomerCommodity)itr.next();
	        	String testCode=ccommodity.getCommodityCode();
	        	Byte isActive=ccommodity.getIsActive();
	        		
	        	if(commodityCode.equals(testCode))
	        	{
	        		if(isActive==1)
	        			isUnique=1;
	        			
	        		if(isActive==0)
	        		{
	        			CustomerCommodity commodity = new CustomerCommodity();
	        			commodity.setId(Integer.valueOf(ccommodity.getId()));
	        			commodity.setCommodityCode(commodityCode);
	        			commodity.setCommodityDescription(commodityDesc);
	        			commodity.setCommCategoryId(commodityCategoryId);
	        			commodity.setModifiedBy(currentUser);
	        			commodity.setModifiedOn(new Date());
	        			commodity.setIsActive((byte)1);
	        			CURDDao<T> commodityDao = new CommodityDaoImpl<T>();
						commodityDao.update((T) commodity, appDetails);
	        		}
    				isExisted=1;
        			break;
        		}
        	}
        	//Making Inactive  commodity record as active ends with here 
            
			if (request.getParameter("commodityCode") != null
					&& request.getParameter("commodityDesc") != null
					&& request.getParameter("commodityCategoryId") != null && isExisted==0 && isUnique==0) {

				CustomerCommodityCategory category = commodityDao1
						.getCommodity(Integer.parseInt(request
								.getParameter("commodityCategoryId")),
								appDetails);

				CustomerCommodity commodity = new CustomerCommodity();
				commodity.setCommodityCode(request
						.getParameter("commodityCode"));
				commodity.setCommodityDescription(request
						.getParameter("commodityDesc"));
				commodity.setCommCategoryId(category);
				commodity.setIsActive((byte) 1);
				commodity.setCreatedBy(currentUser);
				commodity.setCreatedOn(new Date());
				CURDDao<T> commodityDao = new CommodityDaoImpl<T>();
				commodityDao.save((T) commodity, appDetails);
			}
		} else {
			if (request.getParameter("id") != null) {
				if (request.getParameter("oper") != null
						&& "edit"
								.equalsIgnoreCase(request.getParameter("oper"))) {
					
					Integer currentId=Integer.parseInt(request.getParameter("id"));
					String commodityCode=request.getParameter("commodityCode");
					isUnique=checkCommodityAvailability(currentId,commodityCode,appDetails);
	     			CustomerCommodityCategory category = commodityDao1
							.getCommodity(Integer.parseInt(request
									.getParameter("commodityCategoryId")),
									appDetails);
					CustomerCommodity commodity = new CustomerCommodity();
					commodity
							.setId(Integer.valueOf(request.getParameter("id")));
					commodity.setCommodityCode(request
							.getParameter("commodityCode"));
					commodity.setCommodityDescription(request
							.getParameter("commodityDesc"));
					commodity.setCommCategoryId(category);
					commodity.setIsActive((byte) 1);
					commodity.setModifiedBy(currentUser);
					commodity.setModifiedOn(new Date());
					if(isUnique !=1){
					CURDDao<T> commodityDao = new CommodityDaoImpl<T>();
					commodityDao.update((T) commodity, appDetails);
					}
				} else if (request.getParameter("oper") != null
						&& "del".equalsIgnoreCase(request.getParameter("oper"))) {
					CustomerCommodity commodity = new CustomerCommodity();
					commodity
							.setId(Integer.valueOf(request.getParameter("id")));
					CURDDao<T> commodityDao = new CommodityDaoImpl<T>();
					commodityDao.delete((T) commodity, appDetails);
				}
			}
		}
		return mapping.findForward("success");
	}

	@SuppressWarnings("unchecked")
	public ActionForward parentCategory(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		CURDDao<T> commodityDao = new CommodityCategoryDaoImpl<T>();
		List<CustomerCommodityCategory> categories = (List<CustomerCommodityCategory>) commodityDao
				.list(appDetails, null);
		StringBuilder builder = new StringBuilder();
		
		builder.append("<select>");
		for (CustomerCommodityCategory category : categories) 
		{
			builder.append("<option value=\"" + category.getId() + "\">"
					+ category.getCategoryDescription() + "</option>");
		}
		builder.append("</select>");
		
		session.setAttribute("categoryList", builder);
		return mapping.findForward("listParentCategory");
	}
	
	public ActionForward getJSONData(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		JSONObject jsonObject = new JSONObject();
     	jsonObject.put("isUnique", isUnique);
     	response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();

 	    return null;
    }
	
	// For check the uniqueness while updating Commodity Category 
	public int checkAvailability(Integer id,String testCode,UserDetailsDto appDetails)
	{
		Integer currentId=id;
		CURDDao<CustomerCommodityCategory> curdDao = new CURDTemplateImpl<CustomerCommodityCategory>();
		List<CustomerCommodityCategory> currentCommodities = curdDao.list(appDetails,
					" from CustomerCommodityCategory c where c.id !="+currentId);
		Iterator<CustomerCommodityCategory> itr=currentCommodities.iterator();
		CustomerCommodityCategory cmdt=new CustomerCommodityCategory();
	   	String code=null;
	   	while(itr.hasNext())
	    {
	   		cmdt=(CustomerCommodityCategory)itr.next();
	     	code=cmdt.getCategoryCode();
	     	if(code.equals(testCode))
	   		{
	     		isUnique=1;
	     		break;
   		    }
	     	else
	     	{
	     		isUnique=0;
	     	}
     	}			
	   	return isUnique;
    }
	
	// For check the uniqueness while updating Commodity
	public int checkCommodityAvailability(Integer id,String testCode,UserDetailsDto appDetails)
    {
	  Integer currentId=id;
	  CURDDao<CustomerCommodity> curdDao = new CURDTemplateImpl<CustomerCommodity>();
	  List<CustomerCommodity> currentCommodities = curdDao.list(appDetails,
				   " from CustomerCommodity cmdty where cmdty.id !="+currentId);
	  Iterator<CustomerCommodity> itr=currentCommodities.iterator();
	  CustomerCommodity commodity=new CustomerCommodity();
	  String code=null;
	  while(itr.hasNext())
	  {
		   commodity=(CustomerCommodity)itr.next();
		   code=commodity.getCommodityCode();
		   if(code.equals(testCode))
		   {
		       isUnique=1;
		       break;
	   	   }
		   else
		   {
		       isUnique=0;
		   }
	  }			
		   	return isUnique;
	}
	
	/**
	 * Getting commodities with Sectors set to jqgrid.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward listCommodityWithSectors(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		logger.info("Method to view the list of active commodities with Market Sectors..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityDaoImpl commoditydaoimpl=new CommodityDaoImpl();
		List<CommodityDto> listCommoditySectors=commoditydaoimpl.listCommoditySectors(appDetails);
		session.setAttribute("commoditysectors", listCommoditySectors);
		return mapping.findForward("commodityList");
	}
	
	/**
	 * Getting SubSectors by selected Sector.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public void showMarketSubSectorsBySector(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityDaoImpl commoditydaoimpl=new CommodityDaoImpl();
		Integer sectorId=Integer.parseInt(request.getParameter("selectedSector"));
		String sectorChangedIn=request.getParameter("sectorChangedIn");
		String hasEdit="no";
		String hasDelete="no";
		
		if(sectorChangedIn.equalsIgnoreCase("subsector")){
			@SuppressWarnings("unchecked")
			List<RolePrivileges> roleprivileges=(List<RolePrivileges>)session.getAttribute("privileges");
			Iterator<RolePrivileges> itr=roleprivileges.iterator();
			while(itr.hasNext())
			{
				RolePrivileges privilege=(RolePrivileges)itr.next();
				VMSObjects vms=privilege.getObjectId();
				if(vms.getObjectName().equalsIgnoreCase("Commodity Group"))
				{
					if(privilege.getModify()==1)
					   hasEdit="yes";
					if(privilege.getDelete()==1)
						hasDelete="yes";
					break;
				}
			}
		}
		String query="From CustomerCommodityCategory where marketSector="+sectorId+" and isActive=1 order by categoryDescription asc";
		List<CustomerCommodityCategory> subSectors=commoditydaoimpl.marketSectorAndCommodities(appDetails, query);
		
		JSONObject jsonObject = new JSONObject();
		JSONArray jSubSectorList = new JSONArray();
		JSONObject jSubSectorObject = null;
		for(CustomerCommodityCategory subsector : subSectors)
		{
			jSubSectorObject = new JSONObject();
			jSubSectorObject.put("id", subsector.getId());
			jSubSectorObject.put("subSectorName", subsector.getCategoryDescription());
			jSubSectorList.add(jSubSectorObject);
		}
		jsonObject.put("subSectorList", jSubSectorList);
		if(sectorChangedIn.equalsIgnoreCase("subsector")){
			jsonObject.put("editprivilege", hasEdit);
			jsonObject.put("deleteprivilege",hasDelete);
		}
		response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
	}
	
	/**
	 * Getting Commodities by selected SubSector.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public void showCommodityBySubSector(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityDaoImpl commoditydaoimpl=new CommodityDaoImpl();
		Integer subsectorId=Integer.parseInt(request.getParameter("selectedSubSector"));
		String hasEdit="no";
		String hasDelete="no";
		@SuppressWarnings("unchecked")
		List<RolePrivileges> roleprivileges=(List<RolePrivileges>)session.getAttribute("privileges");
		Iterator<RolePrivileges> itr=roleprivileges.iterator();
		while(itr.hasNext())
		{
			RolePrivileges privilege=(RolePrivileges)itr.next();
			VMSObjects vms=privilege.getObjectId();
			if(vms.getObjectName().equalsIgnoreCase("Commodity Group"))
			{
				if(privilege.getModify()==1)
				   hasEdit="yes";
				if(privilege.getDelete()==1)
					hasDelete="yes";
				break;
			}
		}
		String query="From CustomerCommodity where commCategoryId="+subsectorId+" and isActive=1 order by commodityDescription asc";
		List<CustomerCommodity> commodities=commoditydaoimpl.marketSectorAndCommodities(appDetails, query);
		
		JSONObject jsonObject = new JSONObject();
		JSONArray jCommodityList = new JSONArray();
		JSONObject jCommodityObject = null;
		for(CustomerCommodity commodity : commodities)
		{
			jCommodityObject = new JSONObject();
			jCommodityObject.put("id", commodity.getId());
			jCommodityObject.put("commodityName", commodity.getCommodityDescription());
			jCommodityList.add(jCommodityObject);
		}
		jsonObject.put("commodityList", jCommodityList);
		jsonObject.put("editprivilege", hasEdit);
		jsonObject.put("deleteprivilege",hasDelete);
		response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
	}
	
	/**
	 * Getting SubSectors by selected Sector.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveMarketSector(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		CommodityForm commodityform=(CommodityForm)form;
		CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
		String result=commodityDaoImpl.saveMarketSector(appDetails,commodityform,userId);
		if(result.equalsIgnoreCase("Success") || result.equalsIgnoreCase("Updated") || result.equalsIgnoreCase("Failure"))
		  commodityform.reset(mapping,form,request,response);
		session.setAttribute("CommoditySectorStatus", result);
		return mapping.findForward("sectorSaveOrUpdated");
	}

	public ActionForward editMarketSector(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityForm commodityForm=(CommodityForm)form;
		Integer sectorId=Integer.parseInt(request.getParameter("sectorId"));
		CommodityDaoImpl commoditydao=new CommodityDaoImpl();
		MarketSector marketSector=commoditydao.retriveMarketSector(appDetails, commodityForm, sectorId);
		if(marketSector!=null)
			session.setAttribute("marketSector", marketSector);
		return mapping.findForward("retriveSectorById");
	}

	public ActionForward deleteMarketSector(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// Get the current user id from session
				Integer userId = (Integer) session.getAttribute("userId");
		Integer sectorId=Integer.parseInt(request.getParameter("sectorId"));
		CommodityDaoImpl commoditydao=new CommodityDaoImpl();
		String result=commoditydao.deleteMarketSector(appDetails, sectorId, userId);
		CommodityForm commodityform=(CommodityForm)form;
		commodityform.reset(mapping,form,request,response);
		session.setAttribute("CommoditySectorStatus", result);
		return mapping.findForward("sectorSaveOrUpdated");
	}
	
	public ActionForward saveSubSector(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		CommodityForm commodityform=(CommodityForm)form;
		CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
		String result=commodityDaoImpl.saveSubSector(appDetails,commodityform,userId);
		if(result.equalsIgnoreCase("Success") || result.equalsIgnoreCase("Updated") || result.equalsIgnoreCase("Failure"))
		 commodityform.reset(mapping,form,request,response);
		session.setAttribute("CommoditySectorStatus", result);
		return mapping.findForward("sectorSaveOrUpdated");
	}
	
	public ActionForward editSubSector(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityForm commodityForm=(CommodityForm)form;
		Integer subsectorId=Integer.parseInt(request.getParameter("commCategoryId"));
		CommodityDaoImpl commoditydao=new CommodityDaoImpl();
		CustomerCommodityCategory subSector=commoditydao.retriveSubSector(appDetails, commodityForm, subsectorId);
		if(subSector!=null)
			session.setAttribute("subSector", subSector);
		return mapping.findForward("retriveSubSectorById");
	}
	
	public ActionForward deleteSubSector(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// Get the current user id from session
				Integer userId = (Integer) session.getAttribute("userId");
		Integer subsectorId=Integer.parseInt(request.getParameter("commCategoryId"));
		CommodityDaoImpl commoditydao=new CommodityDaoImpl();
		String result=commoditydao.deleteSubSector(appDetails, subsectorId, userId);
		CommodityForm commodityform=(CommodityForm)form;
		commodityform.reset(mapping,form,request,response);
		session.setAttribute("CommoditySectorStatus", result);
		return mapping.findForward("sectorSaveOrUpdated");
	}
	
	public ActionForward saveCommodity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		CommodityForm commodityform=(CommodityForm)form;
		CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
		String result=commodityDaoImpl.saveCommodity(appDetails,commodityform,userId);
		if(result.equalsIgnoreCase("Success") || result.equalsIgnoreCase("Updated") || result.equalsIgnoreCase("Failure"))
		 commodityform.reset(mapping,form,request,response);
		session.setAttribute("CommoditySectorStatus", result);
		return mapping.findForward("sectorSaveOrUpdated");
	}
	
	public ActionForward editCommodity(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommodityForm commodityForm=(CommodityForm)form;
		Integer commodityId=Integer.parseInt(request.getParameter("commodityId"));
		CommodityDaoImpl commoditydao=new CommodityDaoImpl();
		CustomerCommodity commmodity=commoditydao.retriveCommodity(appDetails, commodityForm, commodityId);
		if(commmodity!=null)
			session.setAttribute("commodity", commmodity);
		return mapping.findForward("retriveCommodityById");
	}
	
	public ActionForward deleteCommodity(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// Get the current user id from session
				Integer userId = (Integer) session.getAttribute("userId");
		Integer commodityId=Integer.parseInt(request.getParameter("commodityId"));
		CommodityDaoImpl commoditydao=new CommodityDaoImpl();
		String result=commoditydao.deleteCommodity(appDetails, commodityId, userId);
		CommodityForm commodityform=(CommodityForm)form;
		commodityform.reset(mapping,form,request,response);
		session.setAttribute("CommoditySectorStatus", result);
		return mapping.findForward("sectorSaveOrUpdated");
	}
	
	public ActionForward listOfCommoditiesBasedOnSearchCommodityText(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String fullTextSearch = request.getParameter("searchCommodityText");
		CommodityDaoImpl commodityDao = new CommodityDaoImpl();
		List<CommodityDto> commodities = (List<CommodityDto>) commodityDao.listOfCommoditiesBasedOnSearchCommodityText(appDetails, fullTextSearch);
		session.setAttribute("customerCommodities",	commodities);
		return mapping.findForward("viewcommodityList");
	}
}
