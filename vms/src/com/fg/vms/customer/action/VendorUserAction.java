package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.customer.dao.CustomerDivisionDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorUserDao;
import com.fg.vms.customer.dao.impl.CustomerDivisionDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorRolesDaoImpl;
import com.fg.vms.customer.dao.impl.VendorUserDaoImpl;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.VendorUserForm;
import com.fg.vms.util.Decrypt;

/**
 * The Class VendorUserAction.
 */
public class VendorUserAction extends DispatchAction {

	/**
	 * Adds the vendors.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward addVendors(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		VendorUserForm vendorsForm = (VendorUserForm) form;
		VendorUserDaoImpl vendorDaoImpl = new VendorUserDaoImpl();
		Integer userId = (Integer) session.getAttribute("userId");

		VendorContact currentUser = (VendorContact) session
				.getAttribute("vendorUser");
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result = vendorDaoImpl.addVendor(vendorsForm, userId,
				appDetails, currentUser);
		List<VendorContact> vendor = null;

		if (result.equals("both")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			errors.add("emailId", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("userLogin")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("emailId", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendors.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorsForm.reset(mapping, request);
		}

		vendor = vendorDaoImpl.listVendors(currentUser.getVendorId().getId(), appDetails);
		session.setAttribute("vendorusers", vendor);
		return mapping.findForward("success");
	}

	/**
	 * Method to view the list of Vendors.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewVendors(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session1 = request.getSession();
		List<VendorContact> vendor = null;
		VendorUserDao vendorDao = new VendorUserDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session1	
				.getAttribute("userDetails");
		SearchDao search = new SearchDaoImpl();
		HttpSession session = request.getSession(true);
		CustomerDivisionDao dao = new CustomerDivisionDaoImpl();
		List<CustomerDivision> customerDivisions = null;
		customerDivisions = dao.retrieveDivisionAll(userDetails);
		session.setAttribute("customerDivisionAll", customerDivisions);
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorContact currentUser = (VendorContact) session
				.getAttribute("vendorUser");
		vendor = vendorDao.listVendors(currentUser.getVendorId().getId(), appDetails);
		session.setAttribute("vendorusers", vendor);
		RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
		List<UserRolesForm> vendorRolesForms = vendorRolesDaoImpl
				.viewRoles(appDetails);
		session.setAttribute("vendorRoles", vendorRolesForms);
		VendorDao vendorDaoImpl = new VendorDaoImpl();
		List<VendorMaster> vendorids = vendorDaoImpl.listVendors(appDetails);
		session.setAttribute("vendorIds", vendorids);
		List<SecretQuestion> securityQnsList = search.listSecQns(appDetails);
		session.setAttribute("secretIds", securityQnsList);
		String isAdminMenuAvailable = vendorDao.checkAdimMenuApplicable(appDetails,currentUser.getVendorId().getId());
		if(isAdminMenuAvailable.equalsIgnoreCase("yes"))
		{
			return mapping.findForward("success");
		}
		else
		{
			return mapping.findForward("successWithoutAdminMenu");
		}
	}

	/**
	 * Method to retrieve the selected vendor for update.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retriveVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		VendorUserDaoImpl vendorsDaoImpl = new VendorUserDaoImpl();
		VendorUserForm vendorUserForm = (VendorUserForm) form;
		SearchDao search = new SearchDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer code = Integer.parseInt(request.getParameter("id"));
		session.setAttribute("vid", code);
		VendorContact vendorUser = vendorsDaoImpl.retriveVendor(code,
				appDetails);
		if (vendorUser != null) {
			vendorUserForm.setId(vendorUser.getId());
			vendorUserForm.setFirstName(vendorUser.getFirstName());
			vendorUserForm.setLastName(vendorUser.getLastName());
			vendorUserForm.setDesignation(vendorUser.getDesignation());

			/* Decrypt password using key value. */
			if (vendorUser.getPassword() != null
					&& vendorUser.getPassword().length() != 0) {
				Integer keyvalue = vendorUser.getKeyValue();
				Decrypt decrypt = new Decrypt();
				String decryptedPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						vendorUser.getPassword());
				vendorUserForm.setPassword(decryptedPassword);
				vendorUserForm.setConfirmPassword(decryptedPassword);
				if (vendorUser.getSecretQuestionId() != null) {
					vendorUserForm.setSecId(vendorUser.getSecretQuestionId()
							.getId());
				}
			}
			vendorUserForm.setEmailId(vendorUser.getEmailId());
			vendorUserForm.setHiddenEmailId(vendorUser.getEmailId());
			vendorUserForm.setPhonenumber(vendorUser.getPhoneNumber());
			vendorUserForm.setMobile(vendorUser.getMobile());
			vendorUserForm.setFax(vendorUser.getFax());
//			vendorUserForm
//					.setLoginDisplayName(vendorUser.getLoginDisplayName());

		//	vendorUserForm.setLoginId(vendorUser.getLoginId());
			//vendorUserForm.setHiddenLoginId(vendorUser.getLoginId());
			vendorUserForm.setRoleId(vendorUser.getVendorUserRoleId().getId());
			vendorUserForm.setSecretQuestionAnswer(vendorUser.getSecQueAns());
			vendorUserForm.setGender(vendorUser.getGender());
		}
		RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
		List<UserRolesForm> vendorRolesForms = vendorRolesDaoImpl
				.viewRoles(appDetails);
		session.setAttribute("vendorRoles", vendorRolesForms);
		List<VendorContact> vendors1 = null;
		VendorContact currentUser = (VendorContact) session
				.getAttribute("vendorUser");
		vendors1 = vendorsDaoImpl.listVendors(currentUser.getVendorId().getId(), appDetails);
		session.setAttribute("vendorusers", vendors1);
		List<SecretQuestion> securityQnsList = search.listSecQns(appDetails);
		session.setAttribute("secretIds", securityQnsList);

		String isAdminMenuAvailable = vendorsDaoImpl.checkAdimMenuApplicable(appDetails,currentUser.getVendorId().getId());
		if(isAdminMenuAvailable.equalsIgnoreCase("yes"))
		{
			return mapping.findForward("modify");
		}
		else
		{
			return mapping.findForward("modifyWithoutAdminMenu");
		}
	}

	/**
	 * Method to update the selected Vendor.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward updateVendors(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorContact currentUser = (VendorContact) session.getAttribute("vendorUser");
		VendorUserDaoImpl vendorDaoImpl = new VendorUserDaoImpl();
		VendorUserForm vendorForm = (VendorUserForm) form;
		Integer vendorid1 = (Integer) session.getAttribute("vid");
		Integer userId = (Integer) session.getAttribute("userId");
		String result = vendorDaoImpl.updateVendor(vendorForm, userId,
				vendorid1, appDetails);
		List<VendorContact> vendors = null;
		if (result.equals("success")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendors.updated");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			
			vendors = vendorDaoImpl.listVendors(currentUser.getVendorId().getId(), appDetails);
			request.setAttribute("vendorusers", vendors);
			session.removeAttribute("vendors");
		} else if (result.equals("failure")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendors.failure");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}else if (result.equals("exist")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("error.exist");
			messages.add("password", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		
		String isAdminMenuAvailable = vendorDaoImpl.checkAdimMenuApplicable(appDetails,currentUser.getVendorId().getId());
		if(isAdminMenuAvailable.equalsIgnoreCase("yes"))
		{
			return mapping.findForward(result);
		}
		else
		{
			return mapping.findForward("successWithoutAdminMenu");
		}
	}

	/**
	 * Method to Delete Vendors in UI.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorUserDaoImpl vendorDaoImpl = new VendorUserDaoImpl();
		VendorContact currentUser = (VendorContact) session
				.getAttribute("vendorUser");
		String result = vendorDaoImpl.deleteVendor(
				Integer.parseInt(request.getParameter("id")), appDetails);
		List<VendorContact> vendors = null;
		if (result.equals("failure")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendors.failure");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		}
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendors.deleted");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendors = vendorDaoImpl
					.listVendors(currentUser.getVendorId().getId(), appDetails);
			session.setAttribute("vendorusers", vendors);
		}

		return mapping.findForward("success");
	}
}
