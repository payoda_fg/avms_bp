/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.util.Constants;

/**
 * Its used to login customer user to complete their profile.
 * 
 * @author pirabu
 * 
 */
public class CustomerUserRegistrationAction extends Action {
	/** Logger of the system. */
	private Logger logger = Constants.logger;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		logger.info("Server Name:" + request.getServerName());

		LoginDao login = new LoginDaoImpl();
//		For local & Cloud
//		String requestUrl =  request.getServerName(); 

//		For staging / New Live
		String requestUrl =  request.getRequestURI().toString()
				.replaceAll(request.getServletPath(), "").trim();

		logger.info("Request URL:" + request.getRequestURL());
		/**
		 * If Requested URL is FG admin URI(/vms), then set the default
		 * application settings.
		 */
		if (null != requestUrl && !requestUrl.isEmpty()) {

			/**
			 * If Requested URL is customer, then validate the url.
			 */
			UserDetailsDto userDetails = login
					.authenticateServerURL(requestUrl);		
			UsersDao usersDao = new UsersDaoImpl();
			/** validate the SLA period */
			if (login.validateSLAPeriod(userDetails.getCustomerSLA())
					.equalsIgnoreCase("ended")) {
				return mapping.findForward("slaperiodended");

			}
			/** Forward to customer login page. */
			if (userDetails.getDbName() != null) {
				/*
				 * hibernate.customer.cfg.xml represent the application act as
				 * customer.
				 */				
				userDetails
						.setHibernateCfgFileName("hibernate.customer.cfg.xml");
				userDetails.setUserType("customerOrVendor");
				userDetails.setSettings(login
						.getCustomerApplicationSettings(userDetails));
				session.setAttribute("userDetails", userDetails);
				session.setAttribute("userType", "customerOrVendor");
				List<SecretQuestion> secretQuestionList = login
						.getSecurityQuestions(userDetails);

				session.setAttribute("secretQuestionsList", secretQuestionList);
				
				//For Getting IsDivision Id
				com.fg.vms.customer.model.CustomerApplicationSettings applicationSettings = new com.fg.vms.customer.model.CustomerApplicationSettings();
				applicationSettings = login.getCustomerApplicationSettings(userDetails);
				if (applicationSettings != null && applicationSettings.getIsDivision() != null) 
				{
					applicationSettings.setIsDivision(applicationSettings.getIsDivision());
				}				
				// to get the customer divisions
				//if Both Division(Division name="BOTH") is not needed in UI then pass true otherwise  pass false;
				List<CustomerDivision> customerDivisions = usersDao.listCustomerDivisons(userDetails, true);
				session.setAttribute("customerDivisions", customerDivisions);
				session.setAttribute("isDivisionStatus", applicationSettings);
				
				return mapping.findForward("success");
			}
		}
		return mapping.findForward("incorrectURL");
	}
}
