/**
 * EmailTemplateAction.java
 */
package com.fg.vms.customer.action;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.EmailTemplateDao;
import com.fg.vms.customer.dao.impl.EmailTemplateDaoImpl;
import com.fg.vms.customer.dto.EmailTemplateDto;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.EmailTemplateParameters;
import com.fg.vms.customer.model.EmailType;
import com.fg.vms.customer.model.EmailtemplateDivsion;
import com.fg.vms.customer.pojo.EmailTemplateForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.FileUploadProcess;

/**
 * Represent the Email Template data (eg.. Create template and using templates
 * in email)
 * 
 * @author vinoth
 * 
 */
public class EmailTemplateAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Show the email template page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward showEmailTemplatePage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to view the list of active email templates..");

		List<EmailTemplate> emailTemplates = null;
		HttpSession session = request.getSession();
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		emailTemplates = dao.listTemplates(userDetails);
		session.setAttribute("emailTemplates", emailTemplates);
		
		// For getting the Available Templates List
		List<EmailTemplateDto> emailTemplatesWithDivisions=dao.listTemplatesWithDivision(userDetails);
		session.setAttribute("emailTemplatesDivisions", emailTemplatesWithDivisions);
		
		// For Getting the Customer Divisions
		List<CustomerDivision> customerDivisions = null;
		UsersDao usersDao = new UsersDaoImpl();
		//if Both Division(Division name="BOTH") is not needed in UI then pass true otherwise  pass false;
		customerDivisions = usersDao.listCustomerDivisons(userDetails, true);
		session.setAttribute("customerDivisions", customerDivisions);
		
		// For Getting the Email Template Names
		List<EmailType> emailTemplateNames = null;
		emailTemplateNames = dao.listTemplateNames(userDetails);
		session.setAttribute("emailTemplateNames", emailTemplateNames);	
		
		logger.info("Forward to show the list of templates page");
		return mapping.findForward("showpage");
	}

	/**
	 * Save the email templates.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveTemplates(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the create email templates method");

		HttpSession session = request.getSession();

		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		EmailTemplateForm emailTemplateForm = (EmailTemplateForm) form;
		String result = null;
		
		// Added Full Path for email Image(s)...
		StringBuffer url=new StringBuffer(request.getRequestURL());
		url.replace(url.lastIndexOf("/"), url.length(), "");
		url.append("/");
		emailTemplateForm.setImageBaseUrl(url.toString());
		
		if (null != emailTemplateForm.getTemplateId()
				&& 0 != emailTemplateForm.getTemplateId()) {
			getParameters(emailTemplateForm,userDetails);
			/* Id available, update the email template. */
			result = dao.updateEmailTemplates(emailTemplateForm, userId,
					userDetails);
		} else {
			/* New email template creation */
			result = dao.createEmailTemplates(emailTemplateForm, userId,
					userDetails);
		}
		
		// For getting the Available Templates List
		List<EmailTemplateDto> emailTemplatesWithDivisions=dao.listTemplatesWithDivision(userDetails);
		session.setAttribute("emailTemplatesDivisions", emailTemplatesWithDivisions);
		
		/* For show the successful email template creation message. */
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.ok");
			messages.add("template", msg);
			saveMessages(request, messages);
			emailTemplateForm.reset(mapping, request);

			List<EmailTemplate> emailTemplates = null;
			emailTemplates = dao.listTemplates(userDetails);
			session.setAttribute("emailTemplates", emailTemplates);
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("parameterExist")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.parameter");
			messages.add("parameterExist", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("templateExists")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.templateExists");
			messages.add("templateExists", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		logger.info("Forward to show the list of email templates page");

		return mapping.findForward("showpage");
	}

	/**
	 * Delete Email Template.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteTemplate(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the delete email templates method");

		HttpSession session = request.getSession();

		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		EmailTemplateForm emailTemplateForm = (EmailTemplateForm) form;

		/* Delete existing email template. */
		String result = dao.deleteEmailTemplate(
				Integer.parseInt(request.getParameter("id")), userDetails);

		/* For show the successful email template creation message. */
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.delete.ok");
			messages.add("template", msg);
			saveMessages(request, messages);
			emailTemplateForm.reset(mapping, request);

			List<EmailTemplate> emailTemplates = null;
			emailTemplates = dao.listTemplates(userDetails);
			session.setAttribute("emailTemplates", emailTemplates);
			
			// For getting the Available Templates List
			List<EmailTemplateDto> emailTemplatesWithDivisions=dao.listTemplatesWithDivision(userDetails);
			session.setAttribute("emailTemplatesDivisions", emailTemplatesWithDivisions);
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("reference")){
			// if EmailTemplate Have divisions
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.reference");
			messages.add("divisionExist", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		logger.info("Forward to show the list of email templates page");

		return mapping.findForward("showpage");
	}

	/**
	 * Edit Email Template.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editTemplate(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the edit email templates method");

		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		EmailTemplateForm emailTemplateForm = (EmailTemplateForm) form;

		/* Edit existing email template. */
		EmailTemplate template = dao.editEmailTemplate(
				Integer.parseInt(request.getParameter("id")), userDetails);

		if (null != template) {
			
			List<EmailtemplateDivsion> emailTemplateDivision=dao.
					listEmailTemplateDivisions(template.getId(), userDetails);
			
			String templateName = null;
			Integer templateCode = null;
			if(null != template.getTemplateCode() && null != template.getTemplateCode().getId())
			{
				templateCode = template.getTemplateCode().getId();
			}			
			EmailType emailType=dao.retriveTemplateName(templateCode, userDetails);
			
			if(emailType!=null)
		
		    templateName=emailType.getDescription();
	
			emailTemplateForm=packEmailTemplateDivisions(emailTemplateForm,emailTemplateDivision);
			
			emailTemplateForm.setTemplateId(template.getId());
			emailTemplateForm.setEmailTemplateName(emailType.getId());
			emailTemplateForm.setEmailTemplateMessage(template
					.getEmailTemplateMessage());
			emailTemplateForm.setSubject(template.getEmailTemplateSubject());
			emailTemplateForm.setEmailTemplateParameters(getParameters(emailTemplateForm,userDetails));			
			
			getParameters(emailTemplateForm,userDetails);			
		}
		logger.info("Forward to show the list of email templates page");

		return mapping.findForward("showpage");
	}

	/**
	 * Get email template message based on email template id.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getEmailTemplateMessage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside the show email template message method");

		HttpSession session = request.getSession();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		String result = null;
		/* Edit existing email template. */
		if (null != request.getParameter("id")
				&& !request.getParameter("id").isEmpty()) {
			result = dao.showEmailTemplateMessage(
					Integer.parseInt(request.getParameter("id")), userDetails);
		}
		session.setAttribute("emailMessage", result);
		logger.info("Forward to show email template message");
       
		return mapping.findForward("getmessage");
	}
	
	/**
	 * Setting Division in edit template.
	 * 
	 * @param emailTemplateForm
	 * @param divisions
	 * @return
	 */
	private EmailTemplateForm packEmailTemplateDivisions(
			EmailTemplateForm emailTemplateForm, List<EmailtemplateDivsion> divisions) {

		String[] value = new String[divisions.size()];
		int i = 0;
		for (EmailtemplateDivsion templateDivision : divisions) {
			value[i++] = templateDivision.getCustomerDivisionId().getId().toString();
		}
		emailTemplateForm.setEmailtemplateDivision(value);
		return emailTemplateForm;
	}

	/**
	 * Retrive Emailtemplate parameter.
	 * 
	 * @param emailTemplateForm
	 * @param divisions
	 * @return
	 */
	public String getParameters(EmailTemplateForm emailTemplateForm,
			UserDetailsDto userDetails) throws Exception {		
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		List<EmailTemplateParameters> emailTemplatesParams = dao
				.listEmailTemplateParameters(emailTemplateForm.getTemplateId(), userDetails);
		StringBuilder textArray = new StringBuilder();
		if((emailTemplatesParams!=null) && (emailTemplatesParams.size()!=0)){
			for(EmailTemplateParameters param : emailTemplatesParams){
				textArray.append(param.getParameterName() + ",");
			}
			textArray.deleteCharAt(textArray.length()-1);
		}		
        return textArray.toString();	
	}
	
	/**
	 * Get New Email Template Page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward newTemplatePage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("newtemplatepage");
	}
	
	/**
	 * Save New Email Templates.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveNewTemplates(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Inside the create email templates method");

		HttpSession session = request.getSession();

		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		EmailTemplateForm emailTemplateForm = (EmailTemplateForm) form;
		String result = "failure";
		
		// Added Full Path for email Image(s)...
		StringBuffer url=new StringBuffer(request.getRequestURL());
		url.replace(url.lastIndexOf("/"), url.length(), "");
		url.append("/");
		emailTemplateForm.setImageBaseUrl(url.toString());
		if (null != emailTemplateForm.getTemplateId()
				&& 0 != emailTemplateForm.getTemplateId()) {
			getParameters(emailTemplateForm,userDetails);
			/* Id available, update the email template. */
			result = dao.updateNewEmailTemplates(emailTemplateForm, userId,
					userDetails);
		}else{
			/* New email template creation */
			result=dao.createNewEmailTemplates(emailTemplateForm, userId, userDetails);
		}
		// For getting the Available Templates List
				List<EmailTemplateDto> emailTemplatesWithDivisions=dao.listTemplatesWithDivision(userDetails);
				session.setAttribute("emailTemplatesDivisions", emailTemplatesWithDivisions);
		/* For show the successful email template creation message. */
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.ok");
			messages.add("template", msg);
			saveMessages(request, messages);
			emailTemplateForm.reset(mapping, request);

			List<EmailTemplate> emailTemplates = null;
			emailTemplates = dao.listTemplates(userDetails);
			session.setAttribute("emailTemplates", emailTemplates);
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
		}
		else if(result.equals("update"))
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.update.ok");
			messages.add("template", msg);
			saveMessages(request, messages);
			emailTemplateForm.reset(mapping, request);
		}
		else if(result.equals("duplicate"))
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emailTemplate.duplicate");
			messages.add("template", msg);
			saveMessages(request, messages);
			emailTemplateForm.reset(mapping, request);
		}
		return mapping.findForward("savenewtemplate");
	}
	
	/**
	 * Edit New Email Templates.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editNewTemplate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Inside the edit email templates method");

		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		EmailTemplateDao dao = new EmailTemplateDaoImpl();
		EmailTemplateForm emailTemplateForm = (EmailTemplateForm) form;

		/* Edit existing email template. */
		EmailTemplate template = dao.editEmailTemplate(
				Integer.parseInt(request.getParameter("id")), userDetails);

		if (null != template) {
			
			List<EmailtemplateDivsion> emailTemplateDivision=dao.
					listEmailTemplateDivisions(template.getId(), userDetails);
			
			String templateName = null;
			Integer templateCode = null;
			if(null != template.getTemplateCode() && null != template.getTemplateCode().getId())
			{
				templateCode = template.getTemplateCode().getId();
			}			
			EmailType emailType=dao.retriveTemplateName(templateCode, userDetails);
			
			if(emailType!=null) {
				templateName=emailType.getDescription();
			}
			emailTemplateForm=packEmailTemplateDivisions(emailTemplateForm,emailTemplateDivision);
			emailTemplateForm.setTemplateCode(emailType.getId());
			emailTemplateForm.setTemplateId(template.getId());
			emailTemplateForm.setEmailNewTemplateName(emailType.getDescription());
			emailTemplateForm.setEmailTemplateMessage(template
					.getEmailTemplateMessage());
			emailTemplateForm.setSubject(template.getEmailTemplateSubject());				
		}
		logger.info("Forward to show the list of email templates page");
		return mapping.findForward("editnewtemplate");
	}
	
	public ActionForward showImageUploadPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		session.setAttribute("type", request.getParameter("type"));
		session.setAttribute("field_name", request.getParameter("field_name"));
		
		HashMap<Object, Object> list=new HashMap<Object, Object>();
		File folder = new File(FileUploadProcess.createUploadImageFilePath(Constants.getString(Constants.UPLOAD_LOGO_PATH),"emailtemplateimages/BP"));
		File[] listOfFiles = folder.listFiles();
		
		if(listOfFiles!=null && listOfFiles.length>0)
		{
			for (int i = 0; i < listOfFiles.length; i++) 
		    {
				if (listOfFiles[i].isFile()) 
				{
					String onlyName=listOfFiles[i].getName().substring(0,listOfFiles[i].getName().lastIndexOf("."));
					if(onlyName.length()>15)
						onlyName=onlyName.substring(0,15);
					list.put(onlyName, "images/dynamic/?file=emailtemplateimages/BP/"+listOfFiles[i].getName());
				}
		    }
		}
		session.setAttribute("listUploadedImagesNames", list);
		return mapping.findForward("imageUploader");
	}
	
	/**
	 * Uploading the New Email Template Image.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward uploadEmailTemplateImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		EmailTemplateForm emailForm=(EmailTemplateForm)form;
		FormFile uploadImage = null;
		String filePath;
	
		if(emailForm!=null)
		{
			uploadImage = emailForm.getImportImageFile();
			if (uploadImage != null && uploadImage.getFileSize() != 0) 
			{
				filePath = FileUploadProcess.createUploadImageFilePath(Constants.getString(Constants.UPLOAD_LOGO_PATH),"emailtemplateimages/BP");
				// Chech the folder is available
				if (FileUploadProcess.checkFolderAvailable(Constants.getString(Constants.UPLOAD_LOGO_PATH),"emailtemplateimages/BP")) 
				{
					CommonUtils.saveImage(uploadImage, filePath);

				} 
				else 
				{
					FileUploadProcess.createFolder(Constants.getString(Constants.UPLOAD_LOGO_PATH),"emailtemplateimages/BP");
					CommonUtils.saveImage(uploadImage, filePath);
				}				
			}
		}
		
		// Taking particular from URL ( Eg :- Taking 'http://bp.avms.com:8080/vms' from 'http://bp.avms.com:8080/vms/emailtemplate.do')
		StringBuffer url=new StringBuffer(request.getRequestURL());
		url.replace(url.lastIndexOf("/"), url.length(), "");
		url.append("/");
		
		String helping = "images/dynamic/?file=emailtemplateimages/BP/"+uploadImage.getFileName();
		session.setAttribute("emailImageUpload",helping);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result","success");
		jsonObject.put("newImagePath", helping);
		jsonObject.put("globalimageurl", url.toString());
		response.getWriter().println(jsonObject);
		return null;
	}
	
	/**
	 * Uploading the New Email Template Image.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward deleteUploadedEmailTemplateImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{		
		String deletedImages=request.getParameter("deletedImages");
		String array[]=deletedImages.split("--");
		List feedBack=new ArrayList();
		for(int i=0;i<array.length;i++)
		{
			String url=new String(array[i]);
			String fileName=url.substring(url.lastIndexOf("/"));
			String filePath = FileUploadProcess.createUploadImageFilePath(Constants.getString(Constants.UPLOAD_LOGO_PATH),"emailtemplateimages/BP");
			File deletableFile=new File(filePath+fileName);
			feedBack.add(deletableFile.delete());
		}
		JSONObject jsonObject = new JSONObject();
		if(feedBack.contains(false))
			jsonObject.put("feedBack", "fail");
		else
		    jsonObject.put("feedBack","success");
		response.getWriter().println(jsonObject);
		return null;
	}
}
