/*
 * NaicsCodeAction.java 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsCodeDao;
import com.fg.vms.customer.dao.impl.NaicsCodeDaoImpl;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.pojo.NaicsCodeForm;

/**
 * This class is used to insert and display NAICS Master
 * 
 * @author vinoth
 * 
 */
public class NaicsCodeAction extends DispatchAction {

	/**
	 * View the Form contains list of NAICS Records
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward viewNaicsCode(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		NaicsCodeDao naicsCodeDao = new NaicsCodeDaoImpl();
		NaicsCodeForm codeForm = (NaicsCodeForm) form;
		List<NaicsCode> naicsCodes = null;
		naicsCodes = naicsCodeDao.listNaicsParents(appDetails);
		codeForm.setNaicsCodes(naicsCodes);

		return mapping.findForward("viewsuccess");
	}

	/**
	 * Save new NAICS Record in DB
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	// public ActionForward saveNaicsCode(ActionMapping mapping, ActionForm
	// form,
	// HttpServletRequest request, HttpServletResponse response)
	// throws Exception {
	//
	// HttpSession session = request.getSession();
	// /*
	// * Application details such as database settings, application
	// * personalize settings of customer.
	// */
	// UserDetailsDto appDetails = (UserDetailsDto) session
	// .getAttribute("userDetails");
	//
	// Integer userId = (Integer) session.getAttribute("userId");
	//
	// NaicsCodeForm codeForm = (NaicsCodeForm) form;
	//
	// NaicsCodeDao naicsCodeDao = new NaicsCodeDaoImpl();
	//
	// if (codeForm != null) {
	//
	// String result = naicsCodeDao.saveNaicsCode(codeForm, userId,
	// appDetails);
	//
	// // For view the list of email distributions
	// if (result.equals("success")) {
	//
	// ActionMessages messages = new ActionMessages();
	// ActionMessage msg = new ActionMessage("naicscode.ok");
	// messages.add("successMsg", msg);
	// List<NaicsCode> naicsCodes = null;
	// naicsCodes = naicsCodeDao.listNaicsParents(appDetails);
	// codeForm.setNaicsCodes(naicsCodes);
	// saveMessages(request, messages);
	// codeForm.reset(mapping, request);
	// }
	// if (result.equals("failure")) {
	// ActionMessages messages = new ActionMessages();
	// ActionMessage msg = new ActionMessage("naicscode.failed");
	// messages.add("errorMsg", msg);
	// saveMessages(request, messages);
	//
	// }
	// }
	//
	// return mapping.findForward("savesuccess");
	// }

	public ActionForward listNaicsCode(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		StringBuilder builder = new StringBuilder();
		builder.append("<rows>");
		String query = null;
		Integer node = 0;

		Integer n_lvl = 0;
		if (request.getParameter("nodeid") != null
				&& request.getParameter("nodeid").trim().length() != 0
				&& Integer.parseInt(request.getParameter("nodeid")) > 0) {

			n_lvl = Integer.parseInt(request.getParameter("n_level"));
			n_lvl = n_lvl + 1;
			node = Integer.parseInt(request.getParameter("nodeid"));
			//System.out.println("=================" + node);
			query = "from NaicsCode n where n.isActive=1 and n.parentId="
					+ node + " ORDER BY n.naicsDescription ASC";
		} else if (request.getParameter("searchString") != null
				&& !request.getParameter("searchString").isEmpty()
				&& request.getParameter("searchString").trim().length() != 0) {

			if (null != request.getParameter("searchField")
					&& !request.getParameter("searchField").isEmpty()
					&& request.getParameter("searchField").equalsIgnoreCase(
							"name")) {
				String naicsDesc = request.getParameter("searchString");
				query = "from NaicsCode n where n.isActive=1 and n.naicsDescription like '%"
						+ naicsDesc + "%'";
			} else if (null != request.getParameter("searchField")
					&& !request.getParameter("searchField").isEmpty()
					&& request.getParameter("searchField").equalsIgnoreCase(
							"code")) {
				String naicsCode = request.getParameter("searchString");
				query = "from NaicsCode n where n.isActive=1 and n.naicsCode = "
						+ naicsCode;
				//System.out.println(query);
			}

		} else {
			query = "from NaicsCode n where n.isActive=1 and n.parentId=0 ORDER BY n.naicsDescription ASC";
		}

		NaicsCodeDao naicsCodeDao = new NaicsCodeDaoImpl();
		List<NaicsCode> naicsCodes = null;
		naicsCodes = naicsCodeDao.listNaics(appDetails, query);
		/* List<NaicsCode> naicsCodes1 = naicsCodeDao.leafNode(appDetails); */
		for (NaicsCode naicsCode : naicsCodes) {

			builder.append("<row><cell>" + naicsCode.getId() + "</cell>");
			builder.append("<cell>" + naicsCode.getNaicsDescription()
					+ "</cell>");
			builder.append("<cell>" + naicsCode.getNaicsCode() + "</cell>");
			builder.append("<cell>" + naicsCode.getParentId() + "</cell>");
			builder.append("<cell>" + n_lvl + "</cell>");

			if (naicsCode.getParentId() != 0) {
				builder.append("<cell>" + naicsCode.getParentId() + "</cell>");
			} else {
				builder.append("<cell>" + null + "</cell>");
			}

			// if($row[account_id] == $leafnodes[$row[account_id]])
			// $leaf='true'; else $leaf = 'false';
			boolean flag = true;
			// for (NaicsCode naicsCode1 : naicsCodes1) {
			// if (naicsCode1.getId() == naicsCode.getId()) {
			// flag = true;
			// }
			// }
			List<NaicsCode> naicsCodes1 = naicsCodeDao.sizeByCode(appDetails,
					naicsCode.getId());
			if (naicsCodes1 != null && naicsCodes1.size() != 0) {
				flag = false;
			}
			builder.append("<cell>" + flag + "</cell>");

			builder.append("<cell>" + false + "</cell>");
			builder.append("</row>");

		}
		builder.append("</rows>");
		session.setAttribute("naicsdataxml", builder);

		return mapping.findForward("listNaics");
	}

	public ActionForward editNaicsCode(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String query = null;
		Integer id;
		String description;
		String result;
		NaicsCodeDao naicsCodeDao = new NaicsCodeDaoImpl();
		if (request.getParameter("id") != null
				&& request.getParameter("id").trim().length() != 0) {
			id = Integer.parseInt(request.getParameter("id"));
			if (request.getParameter("oper") != null
					&& request.getParameter("oper").trim().length() != 0
					&& request.getParameter("oper").trim()
							.equalsIgnoreCase("del")) {

				naicsCodeDao.deleteNaics(appDetails, id);
			} else {
				description = request.getParameter("NAICSDESCRIPTION");
				Integer userId = (Integer) session.getAttribute("userId");

				Integer parentId = Integer.valueOf(request
						.getParameter("parentID"));

				String naicscode = request.getParameter("NAICSCODE");

				result = naicsCodeDao.editNaicsCode(description, id, parentId,
						naicscode, userId, appDetails);
			}
		} else {
			description = request.getParameter("NAICSDESCRIPTION");
			Integer userId = (Integer) session.getAttribute("userId");

			Integer parentId = 0;
			if (request.getParameter("parentID") != null
					&& !request.getParameter("parentID").equalsIgnoreCase(
							"null")) {
				parentId = Integer.valueOf(request.getParameter("parentID"));
			}
			String naicscode = request.getParameter("NAICSCODE");

			result = naicsCodeDao.editNaicsCode(description, null, parentId,
					naicscode, userId, appDetails);
		}

		return mapping.findForward("listNaics");
	}

	public ActionForward parentNaicsCode(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		NaicsCodeDao naicsCodeDao = new NaicsCodeDaoImpl();
		List<NaicsCode> naicsCodes = null;
		naicsCodes = naicsCodeDao.listNaicsParents(appDetails);
		StringBuilder builder = new StringBuilder();
		// <select><option value="FE">FedEx</option><option
		// value="TN">TNT</option><option value="IN">Intime</option></select>
		builder.append("<select>");
		builder.append("<option value=\"" + 0 + "\">" + "Parent" + "</option>");
		for (NaicsCode naicsCode : naicsCodes) {
			builder.append("<option value=\"" + naicsCode.getId() + "\">"
					+ naicsCode.getNaicsCode() + " - "
					+ naicsCode.getNaicsDescription() + "</option>");
		}
		builder.append("</select>");
		session.setAttribute("naicsParentCode", builder);
		return mapping.findForward("listNaicsParentCode");
	}

	public ActionForward validateNaicsCode(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String naicscode = request.getParameter("NAICSCODE");
		NaicsCodeDao naicsCodeDao = new NaicsCodeDaoImpl();
		String result = naicsCodeDao.uniqueNaicsCode(appDetails, naicscode);
		//System.out.println("==================" + result);
		session.setAttribute("uniqueNaicsresult", result);

		return mapping.findForward("uniqueNaicsresult");
	}
}
