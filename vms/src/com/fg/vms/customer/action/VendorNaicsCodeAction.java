/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NAICSubCategoryDao;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dao.NaicsMasterDao;
import com.fg.vms.customer.dao.impl.NAICSubCategoryImpl;
import com.fg.vms.customer.dao.impl.NaicsCategoryDaoImpl;
import com.fg.vms.customer.dao.impl.NaicsCodeDaoImpl;
import com.fg.vms.customer.dao.impl.NaicsMasterDaoImpl;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.pojo.FindNaicsCode;

/**
 * Represent the controller of supplier naics code functionality.
 * 
 * @author pirabu
 * 
 */
public class VendorNaicsCodeAction extends DispatchAction {

	/**
	 * Implements the code for get the naicsCodes.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward naicsCodes(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		session.setAttribute("rowcount", request.getParameter("rowcount"));
		NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
				.view(appDetails);
		session.setAttribute("categoryName", naicsCategories);
		List<NaicsMaster> masters = null;
		NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();
		masters = naicsMasterDaoImpl.listOfNAICS(appDetails);
		session.setAttribute("masters", masters);
		session.setAttribute("categoryId", 0);
		session.setAttribute("subCategoryId", 0);
		session.setAttribute("naicsMasters", new ArrayList<NaicsMaster>());
		return mapping.findForward("naicsCode");

	}

	/**
	 * Implements the code for get the naicsCodes.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward anonymousNaicsCodes(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		session.setAttribute("rowcount", request.getParameter("rowcount"));
		NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
				.view(appDetails);
		session.setAttribute("categoryName", naicsCategories);
		List<NaicsMaster> masters = null;
		NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();
		masters = naicsMasterDaoImpl.listOfNAICS(appDetails);
		session.setAttribute("masters", masters);
		session.setAttribute("categoryId", 0);
		session.setAttribute("subCategoryId", 0);
		session.setAttribute("naicsMasters", new ArrayList<NaicsMaster>());
		return mapping.findForward("anonymousNaicsCode");

	}

	/**
	 * Search subcategories by id.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward naicsSubCategoriesById(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		Integer categoryId = Integer.parseInt(request
				.getParameter("categoryId").toString());
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
		List<NAICSubCategory> subCategories = null;

		if (categoryId > 0) {
			subCategories = naicSubCategoryImpl.retrieveSubCategoryById(
					categoryId, appDetails);
			NaicsCategory category = naicSubCategoryImpl.retriveNaicsCategory(
					categoryId, appDetails);
			session.setAttribute("categoryId", category.getId());
		} else {
			session.setAttribute("categoryId", 0);
		}
		session.setAttribute("subcategories", subCategories);
		session.setAttribute("naicsMasters", null);
		return mapping.findForward("naicsCode");

	}

	/**
	 * Search anonymous naics subcategories by id.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward anonymousNaicsSubCategoriesById(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		Integer categoryId = Integer.parseInt(request
				.getParameter("categoryId").toString());
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
		List<NAICSubCategory> subCategories = null;

		if (categoryId > 0) {
			subCategories = naicSubCategoryImpl.retrieveSubCategoryById(
					categoryId, appDetails);
			NaicsCategory category = naicSubCategoryImpl.retriveNaicsCategory(
					categoryId, appDetails);
			session.setAttribute("categoryId", category.getId());
		} else {
			session.setAttribute("categoryId", 0);
		}
		session.setAttribute("subcategories", subCategories);
		session.setAttribute("naicsMasters", null);
		return mapping.findForward("anonymousNaicsCode");

	}

	/**
	 * Implements the code for get the NaicsMasters list
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward naicsMastersById(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Integer categoryId = (Integer) session.getAttribute("categoryId");
		List<NaicsMaster> naicsMastersList = null;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// Integer categoryId =
		// Integer.parseInt(request.getParameter("categoryId").toString());
		Integer subCategoryId = Integer.parseInt(request.getParameter(
				"subCategoryId").toString());

		NaicsMasterDao naicsMasterDao = new NaicsMasterDaoImpl();

		if (subCategoryId > 0) {
			naicsMastersList = naicsMasterDao.naicsMastersByIds(categoryId,
					subCategoryId, appDetails);

			NAICSubCategoryDao naicsSubCategoryId = new NAICSubCategoryImpl();
			NAICSubCategory naicsSubCategory = naicsSubCategoryId
					.retrieveSubCategory(subCategoryId, appDetails);
			session.setAttribute("subCategoryId", naicsSubCategory.getId());
		} else {
			session.setAttribute("subCategoryId", 0);
		}
		session.setAttribute("naicsMasters", naicsMastersList);
		return mapping.findForward("naicsMasters");
	}

	/**
	 * Implements the code for get the NaicsMasters list
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward anonymousNaicsMastersById(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		Integer categoryId = (Integer) session.getAttribute("categoryId");
		List<NaicsMaster> naicsMastersList = null;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// Integer categoryId =
		// Integer.parseInt(request.getParameter("categoryId").toString());
		Integer subCategoryId = Integer.parseInt(request.getParameter(
				"subCategoryId").toString());

		NaicsMasterDao naicsMasterDao = new NaicsMasterDaoImpl();

		if (subCategoryId > 0) {
			naicsMastersList = naicsMasterDao.naicsMastersByIds(categoryId,
					subCategoryId, appDetails);

			NAICSubCategoryDao naicsSubCategoryId = new NAICSubCategoryImpl();
			NAICSubCategory naicsSubCategory = naicsSubCategoryId
					.retrieveSubCategory(subCategoryId, appDetails);
			session.setAttribute("subCategoryId", naicsSubCategory.getId());
		} else {
			session.setAttribute("subCategoryId", 0);
		}
		session.setAttribute("naicsMasters", naicsMastersList);
		return mapping.findForward("anonymousNaicsMasters");

	}

	/**
	 * View naics code.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewNaicsCodes(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		FindNaicsCode findNaicsForm = (FindNaicsCode) form;
		NaicsMaster naicsMaster = null;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String naicsMasterCode = findNaicsForm.getNaicMasterCode();
		String codes[];
		codes = naicsMasterCode.split("-");
		Integer nacisId = Integer.parseInt(codes[0].toString());
		NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();
		naicsMaster = naicsMasterDaoImpl.naicsMaster(nacisId, appDetails);
		session.setAttribute("naicsCodes", naicsMaster);
		return mapping.findForward("success");
	}

	/**
	 * Delete Selected NAICS record based on vendor.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteNaics(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer vendorNaicsID = null;
		NaicsCodeDaoImpl codeDaoImpl = new NaicsCodeDaoImpl();
		if (null != request.getParameter("id")
				&& !request.getParameter("id").isEmpty()) {
			vendorNaicsID = Integer.parseInt(request.getParameter("id"));
			codeDaoImpl.deleteVendorNaics(vendorNaicsID, appDetails);
		}
		return mapping.findForward("success");
	}

}
