/**
 * 
 */
package com.fg.vms.customer.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.ReportForm;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.FileUploadProcess;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * 
 * @author vinoth
 * 
 */
public class T2SupplierSpendReportAction extends DispatchAction {

    private Logger logger = Constants.logger;

    /**
     * Show spend report data
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward showT2SpendReport(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	BufferedWriter bufferedWriter = null;
	HttpSession session = request.getSession();
	StringBuffer buffer = new StringBuffer();
	ReportDao reportDao = new ReportDaoImpl();
	SearchVendorForm searchVendorForm = (SearchVendorForm) form;
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	buffer = reportDao.spendDataReport(userDetails);

	if (!buffer.toString().equals("empty")) {
	    searchVendorForm.setStatus("success");
	    String appRoot = getServlet().getServletContext().getRealPath("")
		    + "/";
	    String reportPath = appRoot + "xml";
	    try {
		if (!FileUploadProcess.checkFolderAvailable(reportPath, session
			.getId().toString())) {
		    FileUploadProcess.createFolder(reportPath, session.getId()
			    .toString());
		}
		File f = new File(reportPath + File.separator
			+ session.getId().toString() + "/spendData.xml");
		f.createNewFile();
		bufferedWriter = new BufferedWriter(new FileWriter(f));

		// Start writing to the output stream
		bufferedWriter.write(buffer.toString());
	    } catch (Exception ex) {
		PrintExceptionInLogFile.printException(ex);
	    } finally {
		bufferedWriter.close();
	    }
	} else {
	    searchVendorForm.setStatus("noreport");
	}
	return mapping.findForward("viewSpendDataReport");
    }

    /**
     * Get diversity spend report.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward diversitySpendReport(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	SearchVendorForm searchVendorForm = (SearchVendorForm) form;
	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	List<VendorMaster> vendors = null;
	VendorDao vendorDao = new VendorDaoImpl();
	vendors = vendorDao.listVendors(appDetails);
	searchVendorForm.setVendors(vendors);
	searchVendorForm.setStatus("noreport");
	return mapping.findForward("success");

    }

    /**
     * show Diversity Report
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward showDiversityReport(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {
	BufferedWriter bufferedWriter = null;
	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	StringBuffer buffer = new StringBuffer();
	VendorDao vendorDao = new VendorDaoImpl();
	ReportDao reportDao = new ReportDaoImpl();
	SearchVendorForm searchVendorForm = (SearchVendorForm) form;
	List<VendorMaster> vendors = null;
	vendors = vendorDao.listVendors(appDetails);
	searchVendorForm.setVendors(vendors);
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");

	buffer = reportDao.diversityReport(userDetails, searchVendorForm);

	if (!buffer.toString().equals("empty")) {
	    searchVendorForm.setStatus("success");
	    String appRoot = getServlet().getServletContext().getRealPath("")
		    + "/";
	    String reportPath = appRoot + "xml";

	    try {
		if (!FileUploadProcess.checkFolderAvailable(reportPath, session
			.getId().toString())) {
		    FileUploadProcess.createFolder(reportPath, session.getId()
			    .toString());
		}
		File f = new File(reportPath + File.separator + session.getId()
			+ "/diverseReport.xml");
		f.createNewFile();
		bufferedWriter = new BufferedWriter(new FileWriter(f));
		// Start writing to the output stream
		bufferedWriter.write(buffer.toString());
	    } catch (Exception ex) {
		PrintExceptionInLogFile.printException(ex);
	    } finally {
		bufferedWriter.close();
	    }
	} else {
	    searchVendorForm.setStatus("noreport");
	}
	return mapping.findForward("success");
    }

    /**
     * Get indirect spend report.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward indirectSpendReportPage(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	CommonDao commonDao = new CommonDaoImpl();
	ReportForm reportForm = (ReportForm) form;
	List<VendorMaster> vendors = null;
	VendorDao vendorDao = new VendorDaoImpl();
	vendors = vendorDao.listVendors(appDetails);
	reportForm.setVendors(vendors);
	reportForm.setStatus("noreport");
	reportForm.setYears(commonDao.getSpendDataUploadedYears(appDetails));

	return mapping.findForward("indirectSpendReport");

    }

    /**
     * Show indirect spend report.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward indirectSpendReport(ActionMapping mapping,
	    ActionForm form, HttpServletRequest request,
	    HttpServletResponse response) throws Exception {

	ReportForm reportForm = (ReportForm) form;
	ReportDao reportDao = new ReportDaoImpl();
	BufferedWriter bufferedWriter = null;
	HttpSession session = request.getSession();
	StringBuffer buffer = new StringBuffer();

	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	buffer = reportDao.indirectSpendReport(userDetails, reportForm);
	String op = reportForm.getOption();
	String path = "";
	if (!buffer.toString().equals("empty")) {

	    reportForm.setStatus("success");
	    // Reset the form values.
	    if (op.equalsIgnoreCase("Supplier")) {
		reportForm.setEndDate("");
		reportForm.setStartDate("");
		reportForm.setYear(null);
		path = "/indirectSpendDataBySupplier.xml";
	    }
	    if (op.equalsIgnoreCase("Quarter")) {
		reportForm.setSupplier(null);
		reportForm.setYear(null);
		path = "/indirectSpendDataByQuarter.xml";
	    }
	    if (op.equalsIgnoreCase("Year")) {
		reportForm.setEndDate("");
		reportForm.setStartDate("");
		reportForm.setSupplier(null);
		path = "/indirectSpendDataByYear.xml";
	    }
	    if (op.equalsIgnoreCase("YearToDate")) {
		reportForm.setEndDate("");
		reportForm.setStartDate("");
		reportForm.setSupplier(null);
		reportForm.setYear(null);
		path = "/indirectSpendDataByYearToDate.xml";

	    }

	    logger.info("Report Option:" + reportForm.getOption());
	    reportForm.setOption(op);
	    String appRoot = getServlet().getServletContext().getRealPath("")
		    + "/";
	    String reportPath =  appRoot +  "xml";
	    try {
		if (!FileUploadProcess.checkFolderAvailable(reportPath, session
			.getId().toString())) {
		    FileUploadProcess.createFolder(reportPath, session.getId()
			    .toString());
		}
		File f = new File(reportPath + File.separator
			+ session.getId().toString() + path);
		f.createNewFile();
		bufferedWriter = new BufferedWriter(new FileWriter(f));
		session.setAttribute("path", "xml/"
			+ session.getId().toString() + path);
		// Start writing to the output stream
		bufferedWriter.write(buffer.toString());
	    } catch (Exception ex) {
		PrintExceptionInLogFile.printException(ex);
	    } finally {
		bufferedWriter.close();
	    }
	} else {
	    reportForm.setStatus("noreport");
	}
	return mapping.findForward("viewIndirectSpendReport");

    }

}
