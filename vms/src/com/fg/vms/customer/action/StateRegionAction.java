/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.impl.StateRegionDaoImpl;
import com.fg.vms.customer.model.Region;
import com.fg.vms.customer.model.State;
import com.fg.vms.util.Constants;

/**
 * @author vinoth
 * 
 */
public class StateRegionAction<T> extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * List region for update.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward regionCode(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CURDDao<T> stateRegionDao = new StateRegionDaoImpl<T>();
		List<Region> regions = (List<Region>) stateRegionDao.list(appDetails, null);
		StringBuilder builder = new StringBuilder();
		builder.append("<select>");
		builder.append("<option value=\"" + 0 + "\">" + "Parent" + "</option>");
		for (Region region : regions) {
			builder.append("<option value=\"" + region.getId() + "\">"
					+ region.getRegionName() + "</option>");
		}
		builder.append("</select>");
		session.setAttribute("regionList", builder);
		return mapping.findForward("listRegions");
	}

	/**
	 * Update state/region from jqgrid.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward editStateByRegion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Update State/Region..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer currentUser = (Integer) session.getAttribute("userId");

		if (request.getParameter("oper") != null
				&& "edit".equalsIgnoreCase(request.getParameter("oper"))) {

			Integer parentId = Integer.parseInt(request
					.getParameter("ParentId"));

			if (parentId != null && parentId == 0) {

				String regionName = request.getParameter("RegionId");
				Integer regionId = Integer.parseInt(request.getParameter("id"));
				Region region = new Region();
				region.setId(regionId);
				region.setRegionName(regionName);
				region.setIsActive((byte) 1);
				region.setCreatedBy(0);
				region.setModifiedBy(currentUser);
				region.setModifiedOn(new Date());
				CURDDao<T> regionDao = new StateRegionDaoImpl<T>();
				regionDao.update1((T) region, "region", appDetails);

			} else if (parentId != null && parentId != 0) {

				String stateName = request.getParameter("RegionId");
				Integer stateId = Integer.parseInt(request.getParameter("id"));
				State state = new State();
				state.setId(stateId);
//				state.setStateName(stateName);
//				state.setIsActive((byte) 1);
//				state.setCreatedBy(1);
//				state.setModifiedBy(currentUser);
//				state.setModifiedOn(new Date());
				CURDDao<T> regionDao = new StateRegionDaoImpl<T>();
				regionDao.update1((T) state, "state", appDetails);
			}

		} else if (request.getParameter("oper") != null
				&& "del".equalsIgnoreCase(request.getParameter("oper"))) {

			Integer regionId = Integer.parseInt(request.getParameter("id"));

			if (regionId != null && regionId == 0) {

				Region region = new Region();
				region.setId(regionId);
				region.setIsActive((byte) 0);
				CURDDao<T> regionDao = new StateRegionDaoImpl<T>();
				regionDao.delete1((T) region, "region", appDetails);

			} else if (regionId != null && regionId != 0) {

				State state = new State();
				state.setId(regionId);
				//state.setIsActive((byte) 0);
				CURDDao<T> regionDao = new StateRegionDaoImpl<T>();
				regionDao.delete1((T) state, "state", appDetails);
			}

		}
		return mapping.findForward("success");
	}

}
