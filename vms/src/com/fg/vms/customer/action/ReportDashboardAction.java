/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ReportDashboardDao;
import com.fg.vms.customer.dao.impl.ReportDashboardDaoImpl;
import com.fg.vms.customer.dto.Tier2ReportDto;

/**
 * @author vinoth
 * 
 */
public class ReportDashboardAction extends DispatchAction {

	/**
	 * Total spend dashboard for tier2 vendor.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward totalSpendDashboardReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String spendYear = request.getParameter("spendYear");
		String reportingPeriod = request.getParameter("reportingPeriod");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> totalSales = null;
		totalSales = dashboardDao.getTotalSpendReport(appDetails, spendYear,
				reportingPeriod);
		session.setAttribute("totalSalesDBG", totalSales);

		return mapping.findForward("totalSpendDBReport");
	}

	/**
	 * Direct spend dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward directSpendDashboardReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String spendYear = request.getParameter("spendYear");
		String reportingPeriod = request.getParameter("reportingPeriod");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> directSpendDB = null;
		directSpendDB = dashboardDao.getDirectSpendReport(appDetails,
				spendYear, reportingPeriod);
		session.setAttribute("directSpendDBG", directSpendDB);

		return mapping.findForward("directSpendDBReport");
	}

	/**
	 * Indirect spend dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward indirectSpendDashboardReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String spendYear = request.getParameter("spendYear");
		String reportingPeriod = request.getParameter("reportingPeriod");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> indirectSpendDB = null;
		indirectSpendDB = dashboardDao.getIndirectSpendReport(appDetails,
				spendYear, reportingPeriod);
		session.setAttribute("indirectSpendDBG", indirectSpendDB);

		return mapping.findForward("indirectSpendDBReport");
	}
	
	/**
	 * Diversity spend dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward diversityDashboardReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String spendYear = request.getParameter("spendYear");
		String reportingPeriod = request.getParameter("reportingPeriod");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> diversityDBG = null;
		diversityDBG = dashboardDao.getDiversityReport(appDetails,
				spendYear, reportingPeriod);
		session.setAttribute("diversityDBG", diversityDBG);

		return mapping.findForward("diversityDBReport");
	}

	/**
	 * Ethnicity analysis dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward ethnicityDashboardReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String spendYear = request.getParameter("spendYear");
		String reportingPeriod = request.getParameter("reportingPeriod");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> ethnicityDBG = null;
		ethnicityDBG = dashboardDao.getEthnicityReport(appDetails,
				spendYear, reportingPeriod);
		session.setAttribute("ethnicityDBG", ethnicityDBG);

		return mapping.findForward("ethnicityDBReport");
	}
	
	/**
	 * To View New Registrations By Month Chart Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getNewRegistrationsByMonthReportChartData(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> newRegistrationsByMonth = null;
		newRegistrationsByMonth = dashboardDao.getNewRegistrationsByMonthDashboardReport(appDetails);
		session.setAttribute("newRegistrationsByMonth", newRegistrationsByMonth);

		return mapping.findForward("newRegistrationsByMonthDashboard");
	}
	
	/**
	 * To View the list of Vendors By Business Type in Dashboard Report.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorsByIndustryDashboardReportChartData(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> vendorsByIndustry = null;
		vendorsByIndustry = dashboardDao.getVendorsByIndustryDashboardReport(appDetails);
		session.setAttribute("vendorsByIndustry", vendorsByIndustry);

		return mapping.findForward("vendorsByIndustryDashboard");
	}
	
	/**
	 * To View the list of BP Vendors By Business Type in Dashboard Report.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getBPVendorsByIndustryDashboardReportChartData(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> bpVendorsByIndustry = null;
		bpVendorsByIndustry = dashboardDao.getBPVendorsByIndustryDashboardReport(appDetails);
		session.setAttribute("bpVendorsByIndustry", bpVendorsByIndustry);

		return mapping.findForward("bpVendorsByIndustryDashboard");
	}
	
	/**
	 * To View the list of Vendors By NAICS Description in Dashboard Report.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorsByNaicsDescriptionDashboardReportChartData(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> vendorsByNaicsDescription = null;
		vendorsByNaicsDescription = dashboardDao.getVendorsByNAICSDecriptionDashboardReport(appDetails);
		session.setAttribute("vendorsByNaicsDescription", vendorsByNaicsDescription);

		return mapping.findForward("vendorsByNaicsDescriptionDashboard");
	}
	
	/**
	 * To View the list of BP Vendors By BP Market Sector in Dashboard Report.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorsByBPMarketSectorDashboardReportChartData(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		List<Tier2ReportDto> vendorsByBPMarketSector = null;
		vendorsByBPMarketSector = dashboardDao.getVendorsByBPMarketSectorDashboardReport(appDetails);
		session.setAttribute("vendorsByBPMarketSector", vendorsByBPMarketSector);

		return mapping.findForward("vendorsByBPMarketSectorDashboard");
	}
}