/*
 * WorkflowConfigurationAction.java 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CountryStateDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CountryStateDaoImpl;
import com.fg.vms.customer.dao.impl.EmailDistributionDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.WorkflowConfigurationForm;
import com.fg.vms.util.Constants;

/**
 * Represents workflow configuration details (eg.. email details, etc.)
 * 
 * @author vinoth
 * 
 */
public class WorkflowConfigurationAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * View Workflow Configuration page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward viewWorkflowConfig(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to view workflow configuration..");

		HttpSession session = request.getSession();
		List<Users> users = null;
		UsersDao usersDao = new UsersDaoImpl();
		List<EmailDistributionModel> distributionModels = null;

		WorkflowConfigurationForm configurationForm = (WorkflowConfigurationForm) form;

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// if (session.getAttribute("users") == null
		// && session.getAttribute("vendorContacts") == null) {
		/*
		 * Get the List Email IDs for Customers Admin
		 */
		users = usersDao.listUsers(appDetails, false);
		session.setAttribute("users", users);

		/*
		 * Get the List vendor Email IDs.
		 */
		List<EmailDistribution> vendorContacts = null;
		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();
		vendorContacts = emailDistributionDaoImpl
				.viewVendorEmailList(appDetails);
		session.setAttribute("vendorContacts", vendorContacts);
		// }
		WorkflowConfigurationDaoImpl configurationDaoImpl = new WorkflowConfigurationDaoImpl();
		distributionModels = configurationDaoImpl.view(appDetails);
		session.setAttribute("distributionModels", distributionModels);
		
		CURDDao<?> dao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countryList = (List<com.fg.vms.customer.model.Country>) dao.list(appDetails, "From Country where isactive=1 order by countryname");
		session.setAttribute("countryList", countryList);
		
		CountryStateDao countryStateDao = new CountryStateDaoImpl();
		com.fg.vms.customer.model.Country defaultCountry = countryStateDao.listDefaultCountry(appDetails);

		/*
		 * If workflow available load the workflow in form.
		 */
		WorkflowConfiguration workflowConfigurations = configurationDaoImpl.listWorkflowConfig(appDetails);
		
		if (workflowConfigurations != null && configurationForm != null) {
			configurationForm.setWorkflowConfigId(workflowConfigurations
					.getWorkflowConfigId());
			configurationForm.setFollowWF(workflowConfigurations.getFollowWF());
			configurationForm.setDistributionEmail(workflowConfigurations
					.getDistributionEmail());
			configurationForm.setAssessmentRequired(workflowConfigurations
					.getAssessmentRequired());
			configurationForm.setAssessmentCompleted(workflowConfigurations
					.getAssessmentCompleted());
			configurationForm.setApprovalEmail(workflowConfigurations
					.getApprovalEmail());
			configurationForm.setCertificateEmail(workflowConfigurations
					.getCertificateEmail());
			configurationForm.setCertificateEmailSummaryAlert(workflowConfigurations.getCertificateEmailSummaryAlert());

			if (null != workflowConfigurations.getEmailDistributionMasterId()) {
				configurationForm
						.setEmailDistributionMasterId(workflowConfigurations
								.getEmailDistributionMasterId()
								.getEmailDistributionMasterId());
			}
			if (null != workflowConfigurations.getTier2EmailDist()) {
				configurationForm
						.setTier2ReportEmailDist(workflowConfigurations
								.getTier2EmailDist()
								.getEmailDistributionMasterId());
			}
			if (null != workflowConfigurations.getAdminGroup()) {
				configurationForm
						.setAdminGroup(workflowConfigurations
								.getAdminGroup().getEmailDistributionMasterId());
			}
			configurationForm.setDaysUpload(workflowConfigurations
					.getDaysUpload());
			configurationForm.setDaysExpiry(workflowConfigurations
					.getDaysExpiry());
			configurationForm.setDaysInactive(workflowConfigurations
					.getDaysInactive());
			configurationForm.setReportDueDays(workflowConfigurations
					.getReportDue());
			configurationForm.setDocumentUpload(workflowConfigurations
					.getDocumentUpload());
			configurationForm.setDocumentSize(workflowConfigurations
					.getDocumentSize());
			configurationForm.setReportNotSubmittedReminderDate(workflowConfigurations.getReportNotSubmittedReminderDate());
			configurationForm.setInternationalMode(workflowConfigurations.getInternationalMode());
			
			if(defaultCountry != null) {
				configurationForm.setDefaultCountry(defaultCountry.getId());
			}
			
			configurationForm.setBpSegment(workflowConfigurations.getBpSegment());
			
			CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
			List<StatusMaster> statusMasters = curdDao
					.list(appDetails,
							" From StatusMaster s where s.id NOT IN('I','S') order by s.disporder");
			session.setAttribute("workFlowStatus", statusMasters);
			if(workflowConfigurations.getStatus() != null)
				configurationForm.setStatus(String.valueOf(workflowConfigurations.getStatus().getId()));
		}	
		configurationForm.setCcMailId(workflowConfigurations.getCcEmailId());
		configurationForm.setSupportMailId(workflowConfigurations.getSupportMailId());
		configurationForm.setIsDisplaySDF(workflowConfigurations.getIsDisplaySdf());
		logger.info("Forward to show the workflow configuration page");

		return mapping.findForward("successconfig");
	}

	/**
	 * Save Workflow Configuration list.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveWFconfigList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to save workflow configuration settings..");

		HttpSession session = request.getSession();

		Integer userId = (Integer) session.getAttribute("userId"); // Get the
																	// current
																	// user id
																	// from
																	// session

		WorkflowConfigurationForm workflowConfigurationForm = (WorkflowConfigurationForm) form;

		WorkflowConfigurationDaoImpl configurationDaoImpl = new WorkflowConfigurationDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String result = "";
		if (workflowConfigurationForm.getWorkflowConfigId() != null
				&& workflowConfigurationForm.getWorkflowConfigId() != 0) {
			/* Workflow Id available hence update the workflow */
			result = configurationDaoImpl.updateWorkflowConfigurationSettings(
					workflowConfigurationForm, userId, userDetails);
		} else {
			/* New workflow configuration settings creation */
			result = configurationDaoImpl.createWorkflowConfigurationSettings(
					workflowConfigurationForm, userId, userDetails);
		}

		// For view the list of certificates
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("workflowconfiguration.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			
			//Updating Workflow Configuration in the Session to Access in the Whole Application
			WorkflowConfiguration workflowConfigurations = configurationDaoImpl.listWorkflowConfig(userDetails);
			userDetails.setWorkflowConfiguration(workflowConfigurations);
			// workflowConfigurationForm.reset(mapping, request);
		}

		logger.info("Forward to show the workflow configuration setting page");

		return mapping.findForward("savesuccess");
	}

}
