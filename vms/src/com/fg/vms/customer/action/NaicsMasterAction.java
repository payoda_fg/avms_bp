/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dao.impl.NAICSubCategoryImpl;
import com.fg.vms.customer.dao.impl.NaicsCategoryDaoImpl;
import com.fg.vms.customer.dao.impl.NaicsMasterDaoImpl;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.pojo.NaicsMasterForm;

/**
 * This class is used to insert and display NAICS Master
 * 
 * @author vinoth
 * 
 */
public class NaicsMasterAction extends DispatchAction {

    /**
     * View the Form contains list of NAICS Records
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward viewNAICSMasterPage(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        // Get the drop down values from category and sub category
        HttpSession session = request.getSession();

        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
        List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
                .view(appDetails);
        session.setAttribute("categoryName", naicsCategories);

        // List<NaicsMaster> masters = null;
        // NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();
        // masters = naicsMasterDaoImpl.listOfNAICS(appDetails);

        // session.setAttribute("masters", masters);
        session.setAttribute("categoryId", 0);
        session.setAttribute("searchCategoryId", 0);
        /*
         * Initialize the Naics Sub categories values.
         */
        session.setAttribute("searchSubcategories", null);
        session.setAttribute("subcategories", null);
        session.setAttribute("masters", null);

        return mapping.findForward("viewsuccess");
    }

    /**
     * Save new NAICS Record in DB
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward saveNaicsMaster(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer userId = (Integer) session.getAttribute("userId");
        NaicsMasterForm naicsMasterForm = (NaicsMasterForm) form;
        NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();

        Integer categoryId = naicsMasterForm.getNaicsCategoryId();
        Integer subCategoryId = naicsMasterForm.getSubCategory();
        String result = naicsMasterDaoImpl.saveNaicsMaster(naicsMasterForm,
                userId, categoryId, subCategoryId, appDetails);

        // For check unique NAICS code
        if (result.equals("unique")) {
            ActionErrors errors = new ActionErrors();
            errors.add("naicsCode", new ActionMessage("naicsCode.unique"));
            saveErrors(request, errors);
            mapping.findForward(mapping.getInput());
        }

        if (result.equals("success")) {

            /*
             * Initialize the Naics Sub categories values.
             */
            session.setAttribute("searchSubcategories", null);
            session.setAttribute("subcategories", null);
            session.setAttribute("searchCategoryId", 0);
            session.setAttribute("categoryId", 0);

            // session.setAttribute("masters", naicsMasterDaoImpl.search(
            // categoryId, subCategoryId, appDetails));

            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("naicsCode.ok");
            messages.add("master", msg);
            saveMessages(request, messages);
            naicsMasterForm.reset(mapping, request);
        }

        return mapping.findForward("savesuccess");
    }

    /**
     * Method to update the NAICS record into inactive Delete action in UI
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward deletenaicsmaster(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        NaicsMaster naicsMaster = naicsMasterDaoImpl.retriveNaicsMaster(
                Integer.parseInt(request.getParameter("id")), appDetails);

        String result = naicsMasterDaoImpl.deleteRecord(
                Integer.parseInt(request.getParameter("id")), appDetails);

        // For show the list of NAICS master records
        if (result.equals("success")) {

            /*
             * Initialize the Naics Sub categories values.
             */
            session.setAttribute("searchSubcategories", null);
            session.setAttribute("subcategories", null);
            session.setAttribute("searchCategoryId", 0);
            session.setAttribute("categoryId", 0);

            session.setAttribute("masters", naicsMasterDaoImpl.search(
                    naicsMaster.getNaicsCategoryId().getId(), naicsMaster
                            .getNaicsSubCategoryId().getId(), appDetails));
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("naicsCode.delete.ok");
            messages.add("master", msg);
            saveMessages(request, messages);
        }

        return mapping.findForward("deletesuccess");
    }

    /**
     * Method to search Naics Master record
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward search(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();

        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        NaicsMasterForm naicsMasterForm = (NaicsMasterForm) form;
        NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();

        Integer categoryId = naicsMasterForm.getNaicsCategoryId();
        if (!naicsMasterForm.getNaicsSubCategoryID().isEmpty()) {
            Integer subCategoryId = Integer.parseInt(naicsMasterForm
                    .getNaicsSubCategoryID());
            session.setAttribute("masters", naicsMasterDaoImpl.search(
                    categoryId, subCategoryId, appDetails));
        }

        return mapping.findForward("searchsuccess");
    }

    /**
     * Method to retrieve the selected Naics Master for update
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retrivenaicsmaster(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        NaicsMasterForm naicsMasterForm = (NaicsMasterForm) form;

        NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();
        HttpSession session = request.getSession();

        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer id = Integer.parseInt(request.getParameter("id").toString());

        NaicsMaster naicsMaster = naicsMasterDaoImpl.retriveNaicsMaster(id,
                appDetails);
        if (naicsMaster != null) {

            naicsMasterForm.setId(naicsMaster.getId());
            naicsMasterForm.setNaicsCategoryId(naicsMaster.getNaicsCategoryId()
                    .getId());
            naicsMasterForm.setSubCategory(naicsMaster.getNaicsSubCategoryId()
                    .getId());
            naicsMasterForm.setIsicCode(naicsMaster.getIsicCode());
            naicsMasterForm
                    .setIsicDescription(naicsMaster.getIsicDescription());
            naicsMasterForm.setIsActive(naicsMaster.getIsActive());
            naicsMasterForm.setNaicsCode(naicsMaster.getNaicsCode());
            naicsMasterForm.setHiddenNaicsCode(naicsMaster.getNaicsCode());
            naicsMasterForm.setNaicsDescription(naicsMaster
                    .getNaicsDescription());

            NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();

            List<NAICSubCategory> naicsub = null;
            naicsub = naicSubCategoryImpl.retrieveNAICSubCategories(appDetails,
                    naicsMaster.getNaicsCategoryId());
            session.setAttribute("naicsub", naicsub);
        }

        // // For show the list of NAICS master records
        // List<NaicsMaster> masters = null;
        // masters = naicsMasterDaoImpl.listOfNAICS(appDetails);
        // session.setAttribute("masters", masters);

        return mapping.findForward("retrievesuccess");
    }

    /**
     * Method to update the selected Naics Master Record
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updatenaicsmaster(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();

        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer currentUserId = (Integer) session.getAttribute("userId");

        NaicsMasterForm naicsMasterForm = (NaicsMasterForm) form;
        NaicsMasterDaoImpl naicsMasterDaoImpl = new NaicsMasterDaoImpl();

        Integer Id = naicsMasterForm.getId();
        Integer categoryId = naicsMasterForm.getNaicsCategoryId();
        Integer subCategoryId = naicsMasterForm.getSubCategory();

        String result = naicsMasterDaoImpl.updateNAICSMaster(naicsMasterForm,
                Id, categoryId, subCategoryId, currentUserId, appDetails);

        // For check unique NAICS code
        /*
         * if (result.equals("unique")) { ActionErrors errors = new
         * ActionErrors(); errors.add("naicsCode", new
         * ActionMessage("naicsCode.unique")); saveErrors(request, errors);
         * mapping.findForward(mapping.getInput()); }
         */

        // For show the list of NAICS master records
        if (result.equals("success")) {

            /*
             * Initialize the Naics Sub categories values.
             */
            session.setAttribute("searchSubcategories", null);
            session.setAttribute("subcategories", null);
            session.setAttribute("searchCategoryId", 0);
            session.setAttribute("categoryId", 0);

            session.setAttribute("masters", naicsMasterDaoImpl.search(
                    categoryId, subCategoryId, appDetails));

            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("naicsCode.update.ok");
            messages.add("master", msg);
            saveMessages(request, messages);
            naicsMasterForm.reset(mapping, request);

        }

        return mapping.findForward("updatesuccess");
    }

    /**
     * Retrieve the list of Sub-categories by Categories.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewSubCategoriesById(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        // get the selected category id
        Integer categoryId = Integer.parseInt(request
                .getParameter("categoryId").toString());
        HttpSession session = request.getSession();

        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
        List<NAICSubCategory> subCategories;

        // Get category list.
        if (categoryId != 0) {
            subCategories = naicSubCategoryImpl.retrieveSubCategoryById(
                    categoryId, appDetails);
        } else {
            ActionErrors errors = new ActionErrors();

            errors.add("category", new ActionMessage("error.category.invalid"));
            saveErrors(request, errors);
            // Return back to the previous state
            session.setAttribute("categoryId", categoryId);
            session.removeAttribute("subcategories");
            return mapping.findForward("viewsuccess1");
        }

        NaicsCategory category = naicSubCategoryImpl.retriveNaicsCategory(
                categoryId, appDetails);

        session.setAttribute("categoryId", category.getId());
        session.setAttribute("subcategories", subCategories);

        // forward to the view Naics Masters page.
        return mapping.findForward("viewsuccess1");
    }

    /**
     * Retrieve the Search list of Sub-categories by Categories.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewSubCategoriesSearchById(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        // get the selected category id
        Integer searchId = Integer.parseInt(request.getParameter("searchId")
                .toString());

        HttpSession session = request.getSession();

        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
        List<NAICSubCategory> subCategories;

        // Get category list.
        if (searchId != 0) {
            subCategories = naicSubCategoryImpl.retrieveSubCategoryById(
                    searchId, appDetails);
        } else {
            ActionErrors errors = new ActionErrors();

            errors.add("category", new ActionMessage("error.category.invalid"));
            saveErrors(request, errors);
            // Return back to the previous state
            session.setAttribute("searchCategoryId", searchId);
            session.removeAttribute("subcategories");
            return mapping.findForward("viewsuccess1");
        }

        session.setAttribute("searchCategoryId", searchId);
        session.setAttribute("categoryId", 0);
        session.setAttribute("searchSubcategories", subCategories);

        // forward to the view Naics Masters page.
        return mapping.findForward("viewsuccess1");
    }

}
