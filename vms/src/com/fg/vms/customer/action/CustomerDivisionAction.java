package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CustomerDivisionDao;
import com.fg.vms.customer.dao.impl.CustomerDivisionDaoImpl;
import com.fg.vms.customer.dto.CustomerUsersDivisionDto;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.pojo.CustomerDivisionForm;
import com.fg.vms.util.Constants;

/**
 * Defined ManageDivisionAction class for manage division details like
 * 
 * 
 * @author SHEFEEK.A
 * 
 */
public class CustomerDivisionAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;
	public static String divisionName;

	/**
	 * Show the customer division page
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showMangeDivisionPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view Manage Division page");

		List<CustomerDivision> customerDivisions = null;
		List<CustomerDivision> customerDivisionsAll = null;
		HttpSession session = request.getSession();

		CustomerDivisionDao dao = new CustomerDivisionDaoImpl();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/* View the division details. */
		customerDivisions = dao.retrieveDivision(userDetails);
		customerDivisionsAll = dao.retrieveDivisionAll(userDetails);
		session.setAttribute("customerDivisionsAll", customerDivisionsAll);
		session.setAttribute("customerDivisions", customerDivisions);
		session.setAttribute("divisionReadyOnlyStatus", "yes");
		return mapping.findForward("viewsuccess");
	}

	/**
	 * Add the Customer Division Details
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward addDivision(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		Integer userId = (Integer) session.getAttribute("userId");
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerDivisionForm customerDivisionForm = (CustomerDivisionForm) form;
		CustomerDivisionDao customerDivisionDaoImpl = new CustomerDivisionDaoImpl();
		String result = null;
		if (customerDivisionForm.getId() != null
				&& customerDivisionForm.getId() != 0) {
			result = customerDivisionDaoImpl.updateDivision(
					customerDivisionForm, userId, userDetails);
		} else {
			result = customerDivisionDaoImpl.addDivision(customerDivisionForm,
					userId, userDetails);
		}

		// For check unique division name
		if (result.equals("unique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("divisionName", new ActionMessage("division.unique"));
			saveErrors(request, errors);
			mapping.findForward(mapping.getInput());
		}
		// For view the list of divisions
		else if (result.equals("success")) {
			List<CustomerDivision> customerDivisions = null;
			customerDivisions = customerDivisionDaoImpl
					.retrieveDivision(userDetails);
			session.setAttribute("customerDivisions", customerDivisions);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.add.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			customerDivisionForm.reset(mapping, request);
		} else if (result.equals("update")) {
			List<CustomerDivision> customerDivisions = null;
			customerDivisions = customerDivisionDaoImpl
					.retrieveDivision(userDetails);
			session.setAttribute("customerDivisions", customerDivisions);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.update.ok");
			messages.add("updateMsg", msg);
			saveMessages(request, messages);
			customerDivisionForm.reset(mapping, request);
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.add.fail");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);

			return mapping.getInputForward();
		} else if (result.equals("reference")) {
			// if Division refer any user, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.update.reference");
			messages.add("updateReference", msg);
			saveMessages(request, messages);
			customerDivisionForm.reset(mapping, request);
		}
		// customerDivisionForm.reset(mapping, request);
		session.setAttribute("divisionReadyOnlyStatus","yes");
		return mapping.findForward("viewsuccess");
	}

	/**
	 * Edit the Customer Division
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editDivision(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the edit email templates method");

		HttpSession session = request.getSession();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerDivisionDao dao = new CustomerDivisionDaoImpl();
		CustomerDivisionForm customerDivisionForm = (CustomerDivisionForm) form;

		/* Edit existing Division. */
		CustomerDivision division = dao.editDivision(
				Integer.parseInt(request.getParameter("id")), userDetails);

		if (null != division) {
			divisionName = division.getDivisionname();
			customerDivisionForm.setId(division.getId());
			customerDivisionForm.setDivisionName(division.getDivisionname());
			customerDivisionForm.setDivisionShortName(division
					.getDivisionshortname());
			customerDivisionForm.setIsActive(division.getIsactive().toString());
			customerDivisionForm.setCheckList(division.getCheckList());
			customerDivisionForm.setCertificationAgencies(division
					.getCertificationAgencies());
			customerDivisionForm.setRegistrationRequest(division
					.getRegistrationRequest());
			customerDivisionForm.setRegistrationQuestion(division
					.getRegistrationQuestion());
			if(division.getIsGlobal() != null && division.getIsGlobal() != 0)
				customerDivisionForm.setIsGlobal(division.getIsGlobal().toString());
			else
				customerDivisionForm.setIsGlobal("0");

			logger.info("Forward to show the list of division page");
		}
		
		session.setAttribute("divisionReadyOnlyStatus", "no");
		
		return mapping.findForward("viewsuccess");
	}

	/**
	 * Delete the customer Division
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteDivision(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the delete division method");

		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		CustomerDivisionDao dao = new CustomerDivisionDaoImpl();
		CustomerDivisionForm customerDivisionForm = (CustomerDivisionForm) form;
		List<CustomerDivision> customerDivisions = null;

		/* Delete existing division details. */
		String result = dao.deleteDivision(
				Integer.parseInt(request.getParameter("id")), userId,
				userDetails);

		/* For show the successful customer division deleted message */
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.delete.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			customerDivisionForm.reset(mapping, request);
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.delete.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("reference")) {
			// if Division refer any user, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("division.delete.reference");
			messages.add("divisionReference", msg);
			saveMessages(request, messages);
			customerDivisionForm.reset(mapping, request);
		}
		customerDivisions = dao.retrieveDivision(userDetails);
		session.setAttribute("customerDivisions", customerDivisions);
		logger.info("Forward to show the list of division page");
		return mapping.findForward("viewsuccess");
	}

	public ActionForward getCustomerContacts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view the list of active customers..");
		
		CustomerDivisionDao customerDivisionDao = new CustomerDivisionDaoImpl();
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/*
		 * Get the List Email IDs for Customers Admin
		 */
		List<CustomerUsersDivisionDto> divisionUsersList = null;
		divisionUsersList  = customerDivisionDao.listUsersByDivision(userDetails);
		session.setAttribute("divisionUsers", divisionUsersList);
		logger.info("Forward to show the list of customer contact");
		return mapping.findForward("getcustomerdata");
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getUsersDivision(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view the list of active customers in particular Custoner Division..");
		List<CustomerUsersDivisionDto> divisionUsers = null;
		Integer divisionId = null;
		CustomerDivisionDao customerDivisionDaoImpl = new CustomerDivisionDaoImpl();
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		divisionId = Integer.parseInt(request.getParameter("divisionId"));
		divisionUsers = customerDivisionDaoImpl.listUsers(userDetails,
				divisionId);
		session.setAttribute("divisionUsers", divisionUsers);

		logger.info("Forward to show the list of customer contact");
		return mapping.findForward("getdivisionuserdata");
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward assignToUsers(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		Integer userId = null;
		Integer divisionId = null;
		String userIds = null;

		CustomerDivisionDao customerDivisionDaoImpl = new CustomerDivisionDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		userId = (Integer) session.getAttribute("userId");
		userIds = request.getParameter("userIds");
		divisionId = Integer.parseInt(request.getParameter("divisionId"));
		String result = customerDivisionDaoImpl.assignDivisionIdToUsers(
				userDetails, userId, userIds, divisionId);

		String[] array = userIds.split(",");
		String user = "";
		for (String s : array) {
			user = s;
			if (Integer.parseInt(user) == userId) {
				/*if(divisionId == 3)
				{
					userDetails.setCustomerDivisionID(0);
					session.setAttribute("customerDivisionId", 0);
				}
				else
				{*/
					userDetails.setCustomerDivisionID(divisionId);
					session.setAttribute("customerDivisionId", divisionId);
//				}
			}
		}
		return mapping.findForward("viewsuccess");

	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward removeDivisionFromUsers(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		Integer userId = null;
		Integer divisionId = null;
		String userIds = null;
		String result = null;
		CustomerDivisionDao customerDivisionDaoImpl = new CustomerDivisionDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		userId = (Integer) session.getAttribute("userId");
		userIds = request.getParameter("userIds");
		divisionId = Integer.parseInt(request.getParameter("divisionId"));

		String[] array = userIds.split(",");
		String user = "";
		Integer status = null;
		for (String s : array) {
			user = s;
			if (Integer.parseInt(user) == userId) {
				if (userDetails.getSettings().getIsDivision() != null
						&& userDetails.getSettings().getIsDivision() != 0) {
					status = 0;
					session.setAttribute("isDivisionActive", status);
					return mapping.findForward("validatedivision");

				}
				if (userDetails.getSettings().getIsDivision() == 0) {

					result = customerDivisionDaoImpl.removeDivisionIdFromUsers(
							userDetails, userId, userIds, divisionId);
					status = 1;
				}
			} else {
				result = customerDivisionDaoImpl.removeDivisionIdFromUsers(
						userDetails, userId, userIds, divisionId);
				status = 1;

			}
		}
		session.setAttribute("isDivisionActive", status);
		return mapping.findForward("validatedivision");
	}
	
	
	/**
	 * for checkIsGlobalDivisionAvailable method
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward checkIsGlobalDivisionAvailable(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		UsersDao usersDao = new UsersDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Integer divisionId = Integer.parseInt(request.getParameter("divisionId"));
		Integer globalDivision = 0;
		CustomerDivision customerDivision = usersDao.getGlobalDivisionId(userDetails);
		if(customerDivision != null && customerDivision.getId() != divisionId)
			globalDivision = 1;
		else
			globalDivision = 0;
			
			JSONObject jsonObject = new JSONObject();		
			jsonObject.put("globalDivision", globalDivision);		
			response.addHeader("Content-Type", "application/json");
	        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
	        response.getOutputStream().flush();
		
		return null;
	}
}
