/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl;
import com.fg.vms.customer.dao.impl.VendorRolesDaoImpl;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.dto.VendorHistory;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.VendorContactInfo;
import com.fg.vms.util.Decrypt;

/**
 * Represent the controller of supplier contact information CURD functionality.
 * 
 * @author pirabu
 * 
 */
public class VendorContactAction extends DispatchAction {

	/**
	 * Implements the code for editing the contact details for update in
	 * viewpage
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorContactInfo vendorContactInfo = (VendorContactInfo) form;
		Integer contactId = Integer.parseInt(request.getParameter("vendorId").toString());
		VendorContact vendorContact = vendorDao.retriveVendorContact(contactId, appDetails);
		vendorContactInfo.setId(vendorContact.getId().toString());
		vendorContactInfo.setFirstName(vendorContact.getFirstName());
		vendorContactInfo.setLastName(vendorContact.getLastName());
		vendorContactInfo.setDesignation(vendorContact.getDesignation());
		vendorContactInfo.setContactPhone(vendorContact.getPhoneNumber());
		vendorContactInfo.setContactMobile(vendorContact.getMobile());
		vendorContactInfo.setContactFax(vendorContact.getFax());
		vendorContactInfo.setContanctEmail(vendorContact.getEmailId());
		vendorContactInfo.setGender(vendorContact.getGender());
		vendorContactInfo.setHiddenEmailId(vendorContact.getEmailId());
		if(null != session.getAttribute("supplierId")){
			vendorContactInfo.setVendorId((Integer) session.getAttribute("supplierId"));
		}
		Integer keyvalue = vendorContact.getKeyValue();
		Decrypt decrypt = new Decrypt();
		String decryptedPassword = null;
		if (keyvalue != null) {

			if (null != vendorContact.getPassword()
					&& !vendorContact.getPassword().isEmpty()) {
				decryptedPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						vendorContact.getPassword());
			}
		}
		vendorContactInfo.setLoginpassword(decryptedPassword);
		vendorContactInfo.setConfirmPassword(decryptedPassword);
		// vendorContactInfo.setLoginDisplayName(vendorContact
		// .getLoginDisplayName());
		// vendorContactInfo.setLoginId(vendorContact.getLoginId());
		// vendorContactInfo.setHiddenLoginId(vendorContact.getLoginId());

		if (vendorContact.getIsAllowedLogin() != null) {
			if (vendorContact.getIsAllowedLogin() == 1) {
				vendorContactInfo.setLoginAllowed("on");
			} else {
				vendorContactInfo.setLoginAllowed("");
			}
		}

		if (vendorContact.getIsBusinessContact() != null) {
			if (vendorContact.getIsBusinessContact() == 1) {
				vendorContactInfo.setIsBusinessContact("on");
			} else {
				vendorContactInfo.setIsBusinessContact("");
			}
		}

		if (vendorContact.getIsPreparer() != null) {
			if (vendorContact.getIsPreparer() == 1) {
				vendorContactInfo.setIsPreparer("on");
			} else {
				vendorContactInfo.setIsPreparer("");
			}
		}

		if (vendorContact.getSecretQuestionId() != null) {
			vendorContactInfo.setUserSecQn(vendorContact.getSecretQuestionId().getId());
		} else {
			vendorContactInfo.setUserSecQn(0);
		}
		
		if (vendorContact.getVendorUserRoleId() != null) {
			vendorContactInfo.setRoleId(vendorContact.getVendorUserRoleId().getId());
		} else {
			vendorContactInfo.setRoleId(0);
		}
		
		if (vendorContact.getIsPrimaryContact() == 1) {
			vendorContactInfo.setPrimaryContact("on");
		} else {
			vendorContactInfo.setPrimaryContact("");
		}

		vendorContactInfo.setUserSecQnAns(vendorContact.getSecQueAns());
		session.setAttribute("vendorContact", vendorContactInfo);
		session.setAttribute("contactId", contactId);
		
		//Getting Vendor Roles
		RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
		List<UserRolesForm> vendorRoles = vendorRolesDaoImpl.viewRoles(appDetails);
		session.setAttribute("vendorRoles", vendorRoles);
				
		return mapping.findForward("saveVendor");
	}

	/**
	 * Implements the code for editing the sub vendor contact details for update
	 * in viewpage
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewSubVendorContact(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorContactInfo vendorContactInfo = (VendorContactInfo) form;
		Integer vendorId = Integer.parseInt(request.getParameter("vendorId")
				.toString());
		VendorContact vendorContact = vendorDao.retriveVendorContact(vendorId,
				appDetails);
		vendorContactInfo.setId(vendorContact.getId().toString());
		vendorContactInfo.setFirstName(vendorContact.getFirstName());
		vendorContactInfo.setLastName(vendorContact.getLastName());
		vendorContactInfo.setDesignation(vendorContact.getDesignation());
		vendorContactInfo.setContactPhone(vendorContact.getPhoneNumber());
		vendorContactInfo.setContactMobile(vendorContact.getMobile());
		vendorContactInfo.setContactFax(vendorContact.getFax());
		vendorContactInfo.setContanctEmail(vendorContact.getEmailId());
		vendorContactInfo.setHiddenEmailId(vendorContact.getEmailId());
		Integer keyvalue = vendorContact.getKeyValue();
		Decrypt decrypt = new Decrypt();
		String decryptedPassword = null;
		if (keyvalue != null) {

			decryptedPassword = decrypt.decryptText(
					String.valueOf(keyvalue.toString()),
					vendorContact.getPassword());
		}
		vendorContactInfo.setLoginpassword(decryptedPassword);
		vendorContactInfo.setConfirmPassword(decryptedPassword);
		vendorContactInfo.setLoginDisplayName(vendorContact
				.getLoginDisplayName());
		// vendorContactInfo.setLoginId(vendorContact.getLoginId());
		// vendorContactInfo.setHiddenLoginId(vendorContact.getLoginId());

		if (vendorContact.getIsAllowedLogin() != null) {
			if (vendorContact.getIsAllowedLogin().equals((byte) 1)) {
				vendorContactInfo.setLoginAllowed("on");
			} else {
				vendorContactInfo.setLoginAllowed("");
			}
		}

		if (vendorContact.getIsBusinessContact() != null) {
			if (vendorContact.getIsBusinessContact() == 1) {
				vendorContactInfo.setIsBusinessContact("on");
			} else {
				vendorContactInfo.setIsBusinessContact("");
			}
		}

		if (vendorContact.getIsPreparer() != null) {
			if (vendorContact.getIsPreparer() == 1) {
				vendorContactInfo.setIsPreparer("on");
			} else {
				vendorContactInfo.setIsPreparer("");
			}
		}

		if (vendorContact.getSecretQuestionId() != null) {
			vendorContactInfo.setUserSecQn(vendorContact.getSecretQuestionId()
					.getId());
		} else {
			vendorContactInfo.setUserSecQn(0);
		}
		if (vendorContact.getIsPrimaryContact() == 1) {
			vendorContactInfo.setPrimaryContact("on");
		} else {
			vendorContactInfo.setPrimaryContact("");
		}
		vendorContactInfo.setUserSecQnAns(vendorContact.getSecQueAns());
		session.setAttribute("vendorContact", vendorContactInfo);
		session.setAttribute("contactId", vendorId);
		return mapping.findForward("saveSubVendor");
	}

	/**
	 * Add vendor contact.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 */
	public ActionForward vendorcontact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchDao search = new SearchDaoImpl();
		List<SecretQuestion> securityQnsList;
		securityQnsList = search.listSecQns(appDetails);
		session.setAttribute("secretQnsList", securityQnsList);
		
		//Getting Vendor Roles
		RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
		List<UserRolesForm> vendorRoles = vendorRolesDaoImpl.viewRoles(appDetails);
		session.setAttribute("vendorRoles", vendorRoles);
				
		return mapping.findForward("addVendorContact");

	}

	public ActionForward subVendorContact(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		SearchDao search = new SearchDaoImpl();
		List<SecretQuestion> securityQnsList;
		securityQnsList = search.listSecQns(appDetails);
		session.setAttribute("secretQnsList", securityQnsList);
		
		//Getting Vendor Roles
		RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
		List<UserRolesForm> vendorRoles = vendorRolesDaoImpl.viewRoles(appDetails);
		session.setAttribute("vendorRoles", vendorRoles);
				
		return mapping.findForward("addSubVendorContact");

	}

	/**
	 * Code implemented for save the contact into database for the vendor
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		VendorDao vendorDao = new VendorDaoImpl();
		VendorContactInfo vendorContactInfo = (VendorContactInfo) form;
		VendorMaster vendorMaster = null;

		Integer userId = (Integer) session.getAttribute("userId");
		vendorMaster = (VendorMaster) session.getAttribute("vendorMaster");
		String result = vendorDao.saveContact(vendorMaster, vendorContactInfo, userId, appDetails);
		vendorContactInfo.reset(mapping, request);
		RetriveVendorInfoDto vendorInfoDto = vendorDao.retriveVendorInfo(vendorMaster.getId(), appDetails);
		session.setAttribute("vendorInfoDto", vendorInfoDto);

		return mapping.findForward(result);
	}

	/**
	 * Implements the code for update the contact info for the vendor into
	 * database
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward updateContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		VendorDao vendorDao = new VendorDaoImpl();
		VendorContactInfo vendorContactInfo = (VendorContactInfo) form;
		VendorMaster vendorMaster = null;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		vendorMaster = (VendorMaster) session.getAttribute("vendorMaster1");
		String result = vendorDao.updateContact(vendorMaster,
				vendorContactInfo, userId, null, appDetails);
		RetriveVendorInfoDto vendorInfoDto = vendorDao.retriveVendorInfo(
				vendorMaster.getId(), appDetails);
		session.setAttribute("vendorInfoDto", vendorInfoDto);

//		RetriveVendorInfoDto vendorInfo = null;
//		List<VendorCertificate> vendorCertificate = null;
//		CURDDao<?> cdao = new CURDTemplateImpl();
//		VendorMasterForm vendorForm = new VendorMasterForm();
//		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
//				vendorMaster.getId(), appDetails);
//		vendorMaster = vendorInfo.getVendorMaster();
//		vendorCertificate = vendorInfo.getVendorCertificate();
//		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
//				vendorMaster.getId(), vendorMaster, vendorCertificate,
//				appDetails);
//		vendorForm.setCompanytype(vendorMaster.getCompanyType());
//		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
//				.list(appDetails,
//						"From CustomerBusinessType where isactive=1 order by typeName");
//		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
//				.list(appDetails,
//						"From CustomerLegalStructure where isactive=1 order by name");
//		vendorForm.setLegalStructures(legalStructures);
//		vendorForm.setBusinessTypes(businessTypes);

		if (result.equalsIgnoreCase("updateContact")) {
			if (request.getParameter("nonprime") != null) {
				return mapping.findForward("nonprimevendorcontactupdated");
			}
		}

		if (result.equals("both")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			errors.add("contanctEmail", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("userLogin")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("contanctEmail", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("exist")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginpassword", new ActionMessage("error.exist"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}
		return mapping.findForward(result);
	}

	/**
	 * Implements the code for update the sub vendor contact info for the vendor
	 * into database
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 * */
	public ActionForward updateSubVendorContact(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		VendorDao vendorDao = new VendorDaoImpl();
		VendorContactInfo vendorContactInfo = (VendorContactInfo) form;
		VendorMaster vendorMaster = null;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		vendorMaster = (VendorMaster) session.getAttribute("vendorMaster1");

		String result = vendorDao.updateContact(vendorMaster,
				vendorContactInfo, userId, null, appDetails);
		RetriveVendorInfoDto vendorInfoDto = vendorDao.retriveVendorInfo(
				vendorMaster.getId(), appDetails);
		session.setAttribute("vendorInfoDto", vendorInfoDto);
		if (result.equalsIgnoreCase("updateContact")) {
			if (request.getParameter("nonprime") != null) {
				return mapping.findForward("nonprimevendorcontactupdated");
			}
		}

		if (result.equals("both")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			errors.add("contanctEmail", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("userLogin")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("contanctEmail", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		} else if (result.equals("exist")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginpassword", new ActionMessage("error.exist"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}
		return mapping.findForward(result);
	}

	/**
	 * Implements code for delete the vendor contact means set the isActive is
	 * 0.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		VendorDao vendorDao = new VendorDaoImpl();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		int vendorId = Integer.parseInt(request.getParameter("vendorId")
				.toString());
		String result = vendorDao.deleteContact(vendorId, appDetails);
		VendorMaster vendorMaster = (VendorMaster) session
				.getAttribute("vendorMaster");
		RetriveVendorInfoDto vendorInfoDto = vendorDao.retriveVendorInfo(
				vendorMaster.getId(), appDetails);
		session.setAttribute("vendorInfoDto", vendorInfoDto);

		return mapping.findForward(result);
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewModifiedInfo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		int vendorId = Integer.parseInt(request.getParameter("id").toString());
		VendorDocumentsDao dao = new VendorDocumentsDaoImpl();
		List<VendorHistory> vendorHistories = dao.modifiedDate(appDetails,
				vendorId);
		session.setAttribute("vendorHistories", vendorHistories);
		return mapping.findForward("success");
	}
}
