package com.fg.vms.customer.action;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.ImageBanner;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.StyleBuilder;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CertificateDao;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.ReportDashboardDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.dao.Tier2VendorDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDashboardDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.Tier2ReportingPeriodDaoImpl;
import com.fg.vms.customer.dao.impl.Tier2VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.ReportSearchFieldsDto;
import com.fg.vms.customer.dto.SearchReportDto;
import com.fg.vms.customer.dto.SearchVendorDetailsDto;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomReportSearchFields;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.CustomerSearchFilter;
import com.fg.vms.customer.model.DashboardSettings;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.SegmentMaster;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.Tier2ReportDirectExpenses;
import com.fg.vms.customer.model.Tier2ReportIndirectExpenses;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.DirectExcelColumn;
import com.fg.vms.customer.pojo.IndirectExcelColumn;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.customer.pojo.Tier2ReportForm;
import com.fg.vms.customer.pojo.Tier2ReportingPeriodForm;
import com.fg.vms.customer.pojo.Tier2SpendReportExcelColumn;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.EmailType;
import com.fg.vms.util.FrequencyPeriod;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.ReportTypes;
import com.fg.vms.util.SendEmail;


/**
 * @author pirabu
 * 
 */
public class Tier2Report extends DispatchAction {
	
	private List<Tier2ReportDto> printReportPdf;	
	private SearchReportDto supplierCustomColSearchReport;

	private Logger logger = Constants.logger;
		
	/**
	 * @return the printReportPdf
	 */
	public List<Tier2ReportDto> getPrintReportPdf() {
		return printReportPdf;
	}

	/**
	 * @param printReportPdf the printReportPdf to set
	 */
	public void setPrintReportPdf(List<Tier2ReportDto> printReportPdf) {
		this.printReportPdf = printReportPdf;
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	public ActionForward tire2Report(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		VendorContact vendorUser = (VendorContact) session
				.getAttribute("vendorUser");

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
		String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
				+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
				+ vendorUser.getVendorId().getId();

		List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao
				.findAllByQuery(appDetails, query);
		// List<VendorMaster> tier2Vendors = vendorDao.tire2Vendors(appDetails);
		reportForm.setReportDtos(tier2Vendors);
		reportForm.setIndirectReportDtos(tier2Vendors);

		SearchDao searchDao = new SearchDaoImpl();
		ReportDao reportDao = new ReportDaoImpl();
		CertificateDao certificateDao = new CertificateDaoImpl();
		List<Certificate> certificates = certificateDao
				.listOfCertificatesByType((byte) 1, appDetails);
		reportForm.setCertificates(certificates);
		List<CertifyingAgency> certificateAgencies;
		List<Tier2ReportMaster> reportMasters;
		certificateAgencies = searchDao.listAgencies(appDetails);

		reportMasters = reportDao.getReportMasterList(appDetails, vendorUser);
		session.setAttribute("reportMasters", reportMasters);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);

		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		reportForm.setEthnicities(ethnicityDao.list(appDetails, null));

		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorList = marketSectorDao.list(appDetails,"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorList", marketSectorList);
		reportForm.setMarketSectorList(marketSectorList);
		
//		Market Sub Sector List
		CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
		 query="From CustomerCommodityCategory where isActive=1 order by categoryDescription";
		List<CustomerCommodityCategory> marketSubSectors=commodityDaoImpl.marketSectorAndCommodities(appDetails, query);
		session.setAttribute("marketSubSectorList", marketSubSectors);
		reportForm.setMarketSubSectorList(marketSubSectors);
		
		CURDDao<?> dao = new CURDTemplateImpl();
		String query1 = "From Country where isactive=1 order by countryname";
		if(appDetails.getWorkflowConfiguration() != null) {
			if(appDetails.getWorkflowConfiguration().getInternationalMode() != null && appDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query1 = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}		
		List<com.fg.vms.customer.model.Country> countryList = (List<com.fg.vms.customer.model.Country>) dao.list(appDetails, query1);
		session.setAttribute("countryList", countryList);
		
		Integer countryId = null;
		List<Country> country = (List<Country>) dao.list(appDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();		

		List<State> statesList = null;
		if(appDetails.getWorkflowConfiguration() != null) {
			if(appDetails.getWorkflowConfiguration().getInternationalMode() != null && appDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				statesList = (List<State>) dao.list(appDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
			}
		}
		session.setAttribute("statesList", statesList);
		
		WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();

		WorkflowConfiguration workflowConfiguration = configuration
				.listWorkflowConfig(appDetails);

		if (workflowConfiguration != null
				&& null != workflowConfiguration.getTier2EmailDist()) {
			reportForm.setGroupName(workflowConfiguration.getTier2EmailDist()
					.getEmailDistributionListName());
		} else {
			reportForm.setGroupName("");
		}

		Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
		Tier2ReportingPeriod reportingPeriod = periodDao
				.currentReportingPeriod(appDetails);

		int reportStartMonth = 0;
		int reportEndMonth = 3 - 1;
		if (reportingPeriod != null) {
			reportStartMonth = reportingPeriod.getStartDate().getMonth();
			reportEndMonth = reportingPeriod.getEndDate().getMonth();
		}

		reportForm.setStartDate(String.valueOf(reportStartMonth));
		reportForm.setEndDate(String.valueOf(reportEndMonth));

		List<String> reportPeriod = CommonUtils.calculateReportingPeriods(
				reportingPeriod, Calendar.getInstance().get(Calendar.YEAR));

		reportForm.setReportPeriods((ArrayList<String>) reportPeriod);

		// populate countries list
		// CommonDao commonDao = new CommonDaoImpl();
		// List<Country> countries = commonDao.getCountries(appDetails);
		// session.setAttribute("countryList", countries);

		session.setAttribute("directExpensesSession", null);
		session.setAttribute("indirectExpensesSession", null);
		session.setAttribute("createdOnDate", true);
		/* session.setAttribute("certificates", certificates); */

		/* Get Previous Quarter Report Details Details */
		Integer vendorId = vendorUser.getVendorId().getId();
		Tier2ReportMaster previousQuarterDetails;
		previousQuarterDetails = reportDao.getPreviousQuarterDetails(
				appDetails, vendorId);
		if (previousQuarterDetails != null) {
			reportForm.setTier2ReportPreviousQuarterId(previousQuarterDetails
					.getTier2ReportMasterid());
		}

		reportForm.setPreviousQuarterStatus(0);
		return mapping.findForward("tire2Report");
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public ActionForward tire2ReportCreate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		VendorContact vendorUser = (VendorContact) session.getAttribute("vendorUser");
		ReportDao reportDao = new ReportDaoImpl();
		WorkflowConfigurationDao dao = new WorkflowConfigurationDaoImpl();
		String ssDate=Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
	    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(ssDate); 
	    
		Tier2ReportMaster tier2ReportMaster = null;
		List<String> toEmailIds = null;

		Boolean result = false;
		String submitType = request.getParameter("submitType");
		if (reportForm.getTier2ReportMasterId() != null && reportForm.getTier2ReportMasterId() != 0)
		{
			if (reportForm.getPreviousQuarterStatus() == 1 && !((reportForm.getCurrentReportingPeriod()).equals(reportForm.getReportingPeriod())))
			{
				tier2ReportMaster = reportDao.saveTier2Report(reportForm, appDetails, vendorUser, submitType);
			}
			else
			{
				tier2ReportMaster = reportDao.updateTier2Report(reportForm, appDetails, vendorUser, submitType);
			}
		}
		else
		{
			tier2ReportMaster = reportDao.saveTier2Report(reportForm, appDetails, vendorUser, submitType);
		}
		
		//To Check Whether Transaction Succeed or Failed
		if(tier2ReportMaster != null && tier2ReportMaster.getTier2ReportMasterid() != null)
		{
			result = true;
		}
		
		// send mail to distribution list
		if (null != submitType && !submitType.isEmpty() && "submit".equals(submitType) && result)
		{
			WorkflowConfiguration configuration = dao.listWorkflowConfig(appDetails);
			
			if (null != configuration.getTier2EmailDist())
			{
				CURDDao<EmailDistributionListDetailModel> curdDao = new CURDTemplateImpl<EmailDistributionListDetailModel>();
				List<EmailDistributionListDetailModel> detailModels = curdDao.list(appDetails, "from EmailDistributionListDetailModel where emailDistributionMasterId ="
										+ configuration.getTier2EmailDist().getEmailDistributionMasterId());
				String vendorName = vendorUser.getVendorId().getVendorName();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				
				SendEmail sendEmail = new SendEmail();
				// Calling a method to get template msg and subject
				//This is Vendor Division ID not Customer Division ID - Correctly Coded.
				Integer customerDivisionId = null;
				if (null != appDetails.getSettings())
				{
					if (null != appDetails.getSettings().getIsDivision() && 0 != appDetails.getSettings().getIsDivision())
					{
						if (null != appDetails.getCustomerDivisionID() && 0 != appDetails.getCustomerDivisionID())
						{
							customerDivisionId = appDetails.getCustomerDivisionID();
						}
					}
				}
				
				EmailTemplate emailTemplate = sendEmail.getTemplates(11, appDetails, customerDivisionId);
				String templateMsg = emailTemplate.getEmailTemplateMessage();
				String subject = emailTemplate.getEmailTemplateSubject();
				// replaceing params
				templateMsg = templateMsg.replace("#vendorName", vendorName);
				templateMsg = templateMsg.replace("#date", dateFormat.format(new Date()).toString());
				templateMsg = templateMsg.replace("#reportingPeriod", tier2ReportMaster.getReportingPeriod());

				//Vendors Primary Email Id.
				if(vendorUser.getEmailId() != null)
				{
					toEmailIds = new ArrayList<String>();
					toEmailIds.add(vendorUser.getEmailId());
				}					
				
				sendEmail.sendMail(toEmailIds, CommonUtils.packToCCEmailIds(detailModels), CommonUtils.packBCCEmailIds(detailModels),
						// "Report Submited", "Test Mail for Report Submited",
						subject, templateMsg, EmailType.REPORTSUBMITTED, appDetails);
			}
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("report.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			
			session.setAttribute("directExpensesSession", null);
			session.setAttribute("indirectExpensesSession", null);
			reportForm.reset(mapping, request);
		}
		else if (null != submitType && !submitType.isEmpty() && "save".equals(submitType) && result)
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("report.save.ok");
			messages.add("saveSuccessMsg", msg);
			saveMessages(request, messages);
			
			session.setAttribute("directExpensesSession", null);
			session.setAttribute("indirectExpensesSession", null);
			reportForm.reset(mapping, request);
			
			//Information saved, stay in same page.
			if(tier2ReportMaster != null && tier2ReportMaster.getTier2ReportMasterid() != null)
			{
				CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
				String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
						+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
						+ vendorUser.getVendorId().getId();
				List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao.findAllByQuery(appDetails, query);
				reportForm.setReportDtos(tier2Vendors);
				reportForm.setIndirectReportDtos(tier2Vendors);
				
				CertificateDao certificateDao = new CertificateDaoImpl();
				List<Certificate> certificates = certificateDao.listOfCertificatesByType((byte) 1, appDetails);
				reportForm.setCertificates(certificates);
				session.setAttribute("certificateTypes", certificates);				
										
				SearchDao searchDao = new SearchDaoImpl();
				List<CertifyingAgency> certificateAgencies = searchDao.listAgencies(appDetails);
				session.setAttribute("certAgencyList", certificateAgencies);
				
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));
				reportForm.setEthnicities(ethnicityDao.list(appDetails, null));
				
				CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
				List<MarketSector> marketSectorList = marketSectorDao.list(appDetails,"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
				session.setAttribute("marketSectorList", marketSectorList);
				reportForm.setMarketSectorList(marketSectorList);
				

				// Market Sub Sector List
				CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
				 query="From CustomerCommodityCategory where isActive=1 order by categoryDescription";
				List<CustomerCommodityCategory> marketSubSectors=commodityDaoImpl.marketSectorAndCommodities(appDetails, query);
				session.setAttribute("marketSubSectorList", marketSubSectors);
				reportForm.setMarketSubSectorList(marketSubSectors);
				
				WorkflowConfiguration workflowConfiguration = dao.listWorkflowConfig(appDetails);
				
				if (workflowConfiguration != null && null != workflowConfiguration.getTier2EmailDist())
				{
					reportForm.setGroupName(workflowConfiguration.getTier2EmailDist().getEmailDistributionListName());
				}
				else
				{
					reportForm.setGroupName("");
				}
				
				Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
				Tier2ReportingPeriod reportingPeriod = periodDao.currentReportingPeriod(appDetails);
				
				ArrayList<String> reportPeriod = null;
				Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(tier2ReportMaster.getTier2ReportMasterid(), appDetails);
					
				int year = reportMaster.getReportingStartDate().getYear() + 1900;
				int reportStartMonth = 0;
				int reportEndMonth = 3 - 1;
				if (reportingPeriod != null)
				{
					reportStartMonth = reportingPeriod.getStartDate().getMonth();
					reportEndMonth = reportingPeriod.getEndDate().getMonth();
				}
				reportForm.setSpendYear(year);
				reportForm.setStartDate(String.valueOf(reportStartMonth));
				reportForm.setEndDate(String.valueOf(reportEndMonth));

				reportPeriod = (ArrayList<String>) CommonUtils.calculateReportingPeriods(reportingPeriod, year);
				//Initiate direct expenses details
				List<Tier2ReportDirectExpenses> directExpenses = reportMaster.getTier2ReportDirectExpensesList();
				if (directExpenses != null && directExpenses.size() != 0)
				{
					session.setAttribute("directExpensesSession", directExpenses);
				}
				
				//Initiate indirect expenses details
				List<Tier2ReportIndirectExpenses> indirectExpenses = reportMaster.getTier2ReportIndirectExpensesList();
				if (indirectExpenses != null && indirectExpenses.size() != 0)
				{
					session.setAttribute("indirectExpensesSession", indirectExpenses);
				}

				if (null != reportMaster)
				{
					reportForm.setTier2ReportMasterId(reportMaster.getTier2ReportMasterid());
					reportForm.setReportingPeriod(reportMaster.getReportingPeriod());
					NumberFormat formatter = NumberFormat.getInstance();
					formatter.setGroupingUsed(false);
					reportForm.setTotalUsSalesSelectedQtr(String.valueOf(formatter.format(reportMaster.getTotalSales())));
					reportForm.setTotalUsSalestoSelectedQtr(String.valueOf(formatter.format(reportMaster.getTotalSalesToCompany())));
					reportForm.setIndirectPer(reportMaster.getIndirectAllocationPercentage().toString());
					reportForm.setComments(reportMaster.getComments());		
					
					reportMaster.getReportingStartDate().getYear();
					reportPeriod.add(reportMaster.getReportingPeriod());
				
						if(reportMaster.getCreatedOn().after(date)){
							session.setAttribute("createdOnDate", true);
						}else{
							session.setAttribute("createdOnDate", false);
						}
					Set<String> repPeriod = new LinkedHashSet<String>(reportPeriod);
					reportPeriod = null;
					reportPeriod = new ArrayList<String>(repPeriod);
					
				}				
				reportForm.setReportPeriods(reportPeriod);
			}
		}
		else if (null != submitType && !submitType.isEmpty() && "saveexit".equals(submitType) && result)
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("report.save.ok");
			messages.add("saveSuccessMsg", msg);
			saveMessages(request, messages);
			
			session.setAttribute("directExpensesSession", null);
			session.setAttribute("indirectExpensesSession", null);
			reportForm.reset(mapping, request);
		}
		else
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("report.fail");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
		}

		List<Tier2ReportMaster> reportMasters = reportDao.getReportMasterList(appDetails, vendorUser);
		session.setAttribute("reportMasters", reportMasters);
		
		/* Get Previous Quarter Report Details Details */
		Integer vendorId = vendorUser.getVendorId().getId();
		Tier2ReportMaster previousQuarterDetails = reportDao.getPreviousQuarterDetails(appDetails, vendorId);
		if (previousQuarterDetails != null)
		{
			reportForm.setTier2ReportPreviousQuarterId(previousQuarterDetails.getTier2ReportMasterid());
		}
		
		return mapping.findForward("tire2Report");
	}

	/**
	 * View Tier2 Report Master Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	public ActionForward editTier2ReportMaster(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorContact vendorUser = (VendorContact) session
				.getAttribute("vendorUser");
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		ReportDao reportDao = new ReportDaoImpl();
		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
		
		if(request.getParameter("type") != null && request.getParameter("type").equalsIgnoreCase("history")){
			reportForm.setPreviousQuarterStatus(0);
		}
		
		String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode,vm.createdOn) from  VendorMaster vm "
				+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
				+ vendorUser.getVendorId().getId();
		List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao
				.findAllByQuery(appDetails, query);
		// List<VendorMaster> tier2Vendors = vendorDao.tire2Vendors(appDetails);
		reportForm.setReportDtos(tier2Vendors);

		reportForm.setIndirectReportDtos(tier2Vendors);

		// reportForm.setTier2Vendors(tier2Vendors);

		SearchDao searchDao = new SearchDaoImpl();
		CertificateDao certificateDao = new CertificateDaoImpl();
		List<Certificate> certificates = certificateDao
				.listOfCertificatesByType((byte) 1, appDetails);
		reportForm.setCertificates(certificates);
		List<CertifyingAgency> certificateAgencies;
		// List<Certificate> qualityCertificates = null;
		List<Tier2ReportMaster> reportMasters;
		certificateAgencies = searchDao.listAgencies(appDetails);
		// qualityCertificates = certificateDao.listOfCertificatesByType((byte)
		// 0,
		// appDetails);
		reportMasters = reportDao.getReportMasterList(appDetails, vendorUser);
		session.setAttribute("reportMasters", reportMasters);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);
		// session.setAttribute("qualityCertificates", qualityCertificates);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));
		
		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorList = marketSectorDao.list(appDetails,"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorList", marketSectorList);
		reportForm.setMarketSectorList(marketSectorList);
		
		// Market Sub Sector List
		CommodityDaoImpl commodityDaoImpl=new CommodityDaoImpl();
		 query="From CustomerCommodityCategory where isActive=1 order by categoryDescription";
		List<CustomerCommodityCategory> marketSubSectors=commodityDaoImpl.marketSectorAndCommodities(appDetails, query);
		session.setAttribute("marketSubSectorList", marketSubSectors);
		reportForm.setMarketSubSectorList(marketSubSectors);

		// List<SecretQuestion> securityQnsList;
		// securityQnsList = searchDao.listSecQns(appDetails);
		// session.setAttribute("secretQnsList", securityQnsList);
		WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();

		WorkflowConfiguration workflowConfiguration = configuration
				.listWorkflowConfig(appDetails);
		
		if (workflowConfiguration != null && null != workflowConfiguration.getTier2EmailDist()) {
			reportForm.setGroupName(workflowConfiguration.getTier2EmailDist()
					.getEmailDistributionListName());
		} else {
			reportForm.setGroupName("");
		}

		/*if (workflowConfiguration != null) {
			reportForm.setGroupName(workflowConfiguration
					.getEmailDistributionMasterId()
					.getEmailDistributionListName());
		}*/

		Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
		Tier2ReportingPeriod reportingPeriod = periodDao
				.currentReportingPeriod(appDetails);

		CURDDao<Ethnicity> dao = new EthnicityDaoImpl<Ethnicity>();
		reportForm.setEthnicities(dao.list(appDetails, null));
		
		// populate countries list
		// CommonDao commonDao = new CommonDaoImpl();
		// List<Country> countries = commonDao.getCountries(appDetails);
		// /session.setAttribute("countryList", countries);
		ArrayList<String> reportPeriod = null;
		if (request.getParameter("id") != null
				&& !request.getParameter("id").isEmpty()) {
			Integer reportmasterId = Integer.parseInt(request
					.getParameter("id"));
			Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(
					reportmasterId, appDetails);
			
//			String sDate="2017-06-16";  
			String ssDate=Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
		    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(ssDate); 
			
			if(reportMaster != null){
				if(reportMaster.getCreatedOn().after(date)){
					session.setAttribute("createdOnDate", true);
				}else{
					session.setAttribute("createdOnDate", false);
				}
			}
			// int year = Calendar.getInstance().get(Calendar.YEAR);
			int year = reportMaster.getReportingStartDate().getYear() + 1900;
			int reportStartMonth = 0;
			int reportEndMonth = 3 - 1;
			if (reportingPeriod != null) {
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();
			}

			reportForm.setSpendYear(year);
			reportForm.setStartDate(String.valueOf(reportStartMonth));
			reportForm.setEndDate(String.valueOf(reportEndMonth));

			reportPeriod = (ArrayList<String>) CommonUtils
					.calculateReportingPeriods(reportingPeriod, year);
			// Initiate direct expenses details
			List<Tier2ReportDirectExpenses> directExpenses = reportMaster
					.getTier2ReportDirectExpensesList();

			// Initiate indirect expenses details
			List<Tier2ReportIndirectExpenses> indirectExpenses = reportMaster
					.getTier2ReportIndirectExpensesList();

			if (null != reportMaster) {
				reportForm.setTier2ReportMasterId(reportMaster
						.getTier2ReportMasterid());
				reportForm
						.setReportingPeriod(reportMaster.getReportingPeriod());
				NumberFormat formatter = NumberFormat.getInstance();
				formatter.setGroupingUsed(false);
				reportForm.setTotalUsSalesSelectedQtr(String.valueOf(formatter
						.format(reportMaster.getTotalSales())));
				reportForm.setTotalUsSalestoSelectedQtr(String
						.valueOf(formatter.format(reportMaster
								.getTotalSalesToCompany())));
				reportForm.setIndirectPer(reportMaster
						.getIndirectAllocationPercentage().toString());
				reportForm.setComments(reportMaster.getComments());
				// if (reportMaster.getReportingStartDate().getYear() == year) {
				//
				// } else {
				//
				// }
				reportMaster.getReportingStartDate().getYear();
				reportPeriod.add(reportMaster.getReportingPeriod());

				Set<String> repPeriod = new LinkedHashSet<String>(reportPeriod);
				reportPeriod = null;
				reportPeriod = new ArrayList<String>(repPeriod);

			}
			
			if (directExpenses != null && directExpenses.size() != 0)
			{
				// reportForm.setDirectExpenses(directExpenses);
				session.setAttribute("directExpensesSession", directExpenses);
			}
			else
			{				
				session.removeAttribute("directExpensesSession");
			}
			
			if (indirectExpenses != null && indirectExpenses.size() != 0)
			{
				// reportForm.setIndirectExpenses(indirectExpenses);
				session.setAttribute("indirectExpensesSession", indirectExpenses);
			}
			else
			{
				session.removeAttribute("indirectExpensesSession");
			}
		}
		reportForm.setReportPeriods(reportPeriod);

		return mapping.findForward("retriveTier2Report");
	}

	public ActionForward viewTier2Report(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		VendorDao vendorDao = new VendorDaoImpl();
		List<VendorMaster> vendors = vendorDao.listVendors(appDetails);
		reportForm.setVendors(vendors);

		session.setAttribute("reportMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("viewTier2Report");
	}

	/**
	 * View Direct Spend Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewDirectSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();

		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append("from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')"
				+ "");
		// check the isDIvision status and add the costomerDivision based query
		
		if(appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0)
		{
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				sqlQuery.append(" and customerDivisionId in("+ divisionIds.toString()+") ");
			}
		}
		sqlQuery.append("ORDER BY vendorName ASC");
		List<VendorMaster> vendors = dao.list(appDetails, sqlQuery.toString());
		// "from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);
		ReportDao reportDao = new ReportDaoImpl();
		List<String> spendYears = reportDao.spendYear(appDetails);
		Collections.sort(spendYears, Collections.reverseOrder());
		reportForm.setSpendYears(spendYears);
		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("viewDirectSpendReport");
	}

	/**
	 * View total sales Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewTotalSalesReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')"
				+ "");
		// check the isDIvision status and add the costomerDivision based query
		if(appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0)
		{
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				sqlQuery.append(" and customerDivisionId in("+ divisionIds.toString()+") ");
			}
		}
		sqlQuery.append(" ORDER BY vendorName ASC");
		List<VendorMaster> vendors = dao.list(appDetails, sqlQuery.toString());
		// "from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC"
		// );

		ReportDao reportDao = new ReportDaoImpl();
		List<String> spendYears = reportDao.spendYear(appDetails);
		Collections.sort(spendYears, Collections.reverseOrder());
		reportForm.setSpendYears(spendYears);
		reportForm.setVendors(vendors);

		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("viewTotalSalesReport");
	}

	/**
	 * Search Tier2 Spend Report for vendor (Report-1).
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> totalSales = null;
		totalSales = reportDao.getTotalSalesReport(reportForm, appDetails);
		session.setAttribute("totalSales", totalSales);
		setPrintReportPdf(totalSales);

		return mapping.findForward("searchSpendReport");
	}

	/**
	 * To Produce PDF Using Jasper for Search Tier2 Spend Report for Vendor
	 * (Report-1). [PS Report by T2 Total Spend]
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getSearchSpendReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "TotalSalesReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("total_sales_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}	

	public ActionForward loadReportPeriod(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String year = request.getParameter("year");
		Integer year1 = null;
		if (year != null && !year.isEmpty()) {
			year1 = Integer.parseInt(year);
		}
		if (year1 == null) {
			year1 = Calendar.getInstance().get(Calendar.YEAR);
		}

		// Get the details of reporting period.
		ReportDao reportDao = new ReportDaoImpl();
		// Tier2ReportingPeriodDao periodDao = new
		// Tier2ReportingPeriodDaoImpl();
		// Tier2ReportingPeriod reportingPeriod = periodDao
		// .currentReportingPeriod(appDetails);

		List<String> reportPeriod = reportDao.findReportingPeriod(appDetails,
				year1);
		session.setAttribute("reportPeriod", reportPeriod);
		return mapping.findForward("loadPeriod");
	}
	
	
	public ActionForward loadReportPeriodForMultipleYears(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String year = request.getParameter("year");
		String year1 = null;
		if (year != null && !year.isEmpty()) {
			year1 = year;
		}
		if (year1 == null) {
			year1 = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		}

		// Get the details of reporting period.
		ReportDao reportDao = new ReportDaoImpl();
		// Tier2ReportingPeriodDao periodDao = new
		// Tier2ReportingPeriodDaoImpl();
		// Tier2ReportingPeriod reportingPeriod = periodDao
		// .currentReportingPeriod(appDetails);

		List<String> reportPeriod = reportDao.findReportingPeriodForMultipleYears(appDetails,
				year1);
		session.setAttribute("reportPeriod", reportPeriod);
		return mapping.findForward("loadPeriod");
	}

	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadReportPeriodByYear(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String year = request.getParameter("year");
		Integer year1 = null;
		if (year != null && !year.isEmpty()) {
			year1 = Integer.parseInt(year);
		}
		if (year1 == null) {
			year1 = Calendar.getInstance().get(Calendar.YEAR);
		}

		// Get the details of reporting period.
		Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
		Tier2ReportingPeriod reportingPeriod = periodDao
				.currentReportingPeriod(appDetails);
		List<String> reportPeriod = CommonUtils.calculateReportingPeriods(
				reportingPeriod, year1);

		session.setAttribute("reportPeriod", reportPeriod);
		return mapping.findForward("loadPeriod");
	}

	/**
	 * To show the tier2 reporting period page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showPage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		Tier2ReportingPeriod reportingPeriod = null;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/*
		 * Create Reporting period form object.
		 */
		Tier2ReportingPeriodForm periodForm = (Tier2ReportingPeriodForm) form;

		/*
		 * Create Reporting period dao implementation form object.
		 */
		Tier2ReportingPeriodDaoImpl periodDaoImpl = new Tier2ReportingPeriodDaoImpl();

		/*
		 * Get the current reporting period if available.
		 */
		reportingPeriod = periodDaoImpl.currentReportingPeriod(appDetails);

		/*
		 * Set the form values if data available.
		 */
		if (reportingPeriod != null && periodForm != null) {
			periodForm.setReportingPeriodId(reportingPeriod
					.getReportingPeriodId());
			periodForm.setFrequencyPeriod(reportingPeriod.getDataUploadFreq());
			periodForm.setFiscalStartDate(CommonUtils
					.convertDateToString(reportingPeriod.getStartDate()));
			periodForm.setFiscalEndDate(CommonUtils
					.convertDateToString(reportingPeriod.getEndDate()));
		}

		return mapping.findForward("showreportpage");
	}

	/**
	 * Save the tier2 reporting page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveReportingPeriod(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Integer userId = (Integer) session.getAttribute("userId"); // Get the
																	// current
																	// user id
																	// from
																	// session

		Tier2ReportingPeriodForm periodForm = (Tier2ReportingPeriodForm) form;

		Tier2ReportingPeriodDaoImpl reportingPeriodDaoImpl = new Tier2ReportingPeriodDaoImpl();

		String result = reportingPeriodDaoImpl.saveTier2Period(userDetails,
				userId, periodForm);

		// For view the success save message.
		if (result.equals("success")) {
			session.getAttribute("user");

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("tier2report.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
		}
		if (result.equals("update")) {
			session.getAttribute("user");

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("updatetier2report.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
		}

		return mapping.findForward("showreportpage");
	}

	/**
	 * To get direct spend report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDirectSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> spendMasters = null;
		spendMasters = reportDao.directSpendReport(reportForm, appDetails);
		session.setAttribute("directSpend", spendMasters);
		setPrintReportPdf(spendMasters);

		return mapping.findForward("getdirectspenddata");
	}

	/**
	 * To Produce PDF Using Jasper for Direct Spend Report for Vendor
	 * (Report-1). [PS Report by T2 Direct Spend]
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getDirectSpendReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "DirectSpendReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("direct_spend_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To show the indirect spend report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewIndirectSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')"
				+ "");
		// check the isDIvision status and add the costomerDivision based query
		if(appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0)
		{
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				sqlQuery.append(" and customerDivisionId in("+ divisionIds.toString()+") ");
			}
		}
		sqlQuery.append("ORDER BY vendorName ASC");
		List<VendorMaster> vendors = dao.list(appDetails, sqlQuery.toString());
		// "from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);
		ReportDao reportDao = new ReportDaoImpl();
		List<String> spendYears = reportDao.spendYear(appDetails);
		Collections.sort(spendYears, Collections.reverseOrder());
		reportForm.setSpendYears(spendYears);
		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("showindirectreportpage");
	}

	/**
	 * To get Indirect spend report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getIndirectSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> spendMasters = null;
		spendMasters = reportDao.indirectSpendReport(reportForm, appDetails);
		session.setAttribute("indirectSpend", spendMasters);
		setPrintReportPdf(spendMasters);

		return mapping.findForward("getindirectspenddata");
	}

	/**
	 * To Produce PDF Using Jasper for Indirect Spend Report for Vendor
	 * (Report-1). [PS Report by T2 Indirect Spend]
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getIndirectSpendReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "IndirectSpendReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("indirect_spend_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * View Diversity Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewDiversityReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		String type=request.getParameter("type");
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')");
		// check the isDIvision status and add the costomerDivision based query
		if(appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0)
		{
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				sqlQuery.append(" and customerDivisionId in("+ divisionIds.toString()+") ");
			}
		}
		sqlQuery.append(" ORDER BY vendorName ASC");
		List<VendorMaster> vendors = dao.list(appDetails, sqlQuery.toString());
		// "from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);
		
		ReportDao reportDao = new ReportDaoImpl();
		List<String> spendYears = reportDao.spendYear(appDetails);
		Collections.sort(spendYears, Collections.reverseOrder());
		reportForm.setSpendYears(spendYears);
		session.setAttribute("spendMasters", null);
		
		//Setting Values for Certificate Type Field in the Form.
		CURDTemplateImpl<Certificate> dao2 = new CURDTemplateImpl<Certificate>();		
		List<Certificate> certificatesList = dao2.list(appDetails, "From Certificate where isActive = 1 order by certificateName");
		session.setAttribute("certificatesList", certificatesList);
		if(type != null){
			if(type.equals("ethnicity")){
				request.setAttribute("type", type);
			}
		}else{
			request.setAttribute("type", "category");
		}
		reportForm.reset(mapping, request);
		if(type == null){
			return mapping.findForward("viewDiversityStatusBySectorAndEthnicity");
		}
		return mapping.findForward("viewDiversityStatus");
	}

	/**
	 * To get Indirect spend report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDiversityReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		String type=request.getParameter("type");
		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> diversityReport = null;
		if(type.equals("ethnicity")){
			diversityReport = reportDao.getDiversityReport(reportForm, appDetails, type);
		}else{
			diversityReport = reportDao.getDiversityReportBySectorAndEthnicity(reportForm, appDetails, type);
		}
		session.setAttribute("diversityReport", diversityReport);
		setPrintReportPdf(diversityReport);

		return mapping.findForward("getDiversityData");
	}

	/**
	 * To Produce PDF Using Jasper for Diversity Category Report for Vendor
	 * (Report-1). [PS Report by T2 Diversity Category]
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getDiversityReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String jasperFileName = null;
		int reportType = Integer.parseInt(request.getParameter("reportType"));
		String type = request.getParameter("type");
		if (type.equals("ethnicity")) {
			if (reportType == 2) {
				jasperFileName = "supplier_diversity_report_both_eth.jasper";
			}

			if (reportType == 1) {
				jasperFileName = "supplier_diversity_report_direct_eth.jasper";
			}

			if (reportType == 0) {
				jasperFileName = "supplier_diversity_report_indirect_eth.jasper";
			}
		} else {
			if (reportType == 2) {
				jasperFileName = "supplier_diversity_report_both.jasper";
			}

			if (reportType == 1) {
				jasperFileName = "supplier_diversity_report_direct.jasper";
			}

			if (reportType == 0) {
				jasperFileName = "supplier_diversity_report_indirect.jasper";
			}
		}

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "PrimeSupplierbyTier2DiversityCategory_"
					+ dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream(jasperFileName);
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * View Agency Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewAgencyReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("viewCertifyingAgency");
	}

	/**
	 * To get agency report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getAgencyReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> agencyReport = null;
		agencyReport = reportDao.getAgencyReport(reportForm, appDetails);
		session.setAttribute("agencyReport", agencyReport);

		return mapping.findForward("getCertifyingAgencyData");
	}

	/**
	 * View Ethnicity Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewEthnicityReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("showEthnicityPage");
	}

	/**
	 * To get ethnicity spend report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getEthnicitySpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> ethnicityReport = null;
		ethnicityReport = reportDao.getAgencyReport(reportForm, appDetails);
		session.setAttribute("ethnicityReport", ethnicityReport);

		return mapping.findForward("getethnicityspenddata");
	}

	/**
	 * View Diversity Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewEthnicityBreakdownReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')"
				+ "");
		// check the isDIvision status and add the costomerDivision based query
		if(appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0)
		{
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				sqlQuery.append(" and customerDivisionId in("+ divisionIds.toString()+") ");
			}
		}
		sqlQuery.append(" ORDER BY vendorName ASC");
		List<VendorMaster> vendors = dao.list(appDetails, sqlQuery.toString());
		// "from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);
		ReportDao reportDao = new ReportDaoImpl();
		List<String> spendYears = reportDao.spendYear(appDetails);
		Collections.sort(spendYears, Collections.reverseOrder());
		reportForm.setSpendYears(spendYears);
		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("showethnicitybreakdownstatus");
	}

	/**
	 * To get ethnicity breakdown report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getEthnicityBreakdownReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> ethnicityBreakdownReport = null;
		ethnicityBreakdownReport = reportDao.getEthnicityBreakdownReport(
				reportForm, appDetails);
		session.setAttribute("ethnicityBreakdownReport",
				ethnicityBreakdownReport);
		setPrintReportPdf(ethnicityBreakdownReport);

		return mapping.findForward("getbreakdowndata");
	}

	/**
	 * To Produce PDF Using Jasper for Tier 2 Reporting Summary for Vendor
	 * (Report-1). [PS Tier 2 Reporting Summary]
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getEthnicityBreakdownReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "Tier2ReportingSummary_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("tier2_reporting_summary.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * View Spend Report Dashboard Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewSpendDashboard(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();

		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append("from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B') "
				+ "");
		// check the isDIvision status and add the costomerDivision based query
		if(appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0)
		{
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				sqlQuery.append(" and customerDivisionId in("+ divisionIds.toString()+") ");
			}
		}
		sqlQuery.append("ORDER BY vendorName ASC");
		List<VendorMaster> vendors = dao.list(appDetails, sqlQuery.toString());
		// "from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);
		ReportDao reportDao = new ReportDaoImpl();
		List<String> spendYears = reportDao.spendYear(appDetails);
		Collections.sort(spendYears, Collections.reverseOrder());
		reportForm.setSpendYears(spendYears);
		session.setAttribute("spendMasters", null);
		reportForm.reset(mapping, request);

		return mapping.findForward("showspenddashboard");
	}

	/**
	 * To get spend dashboard report data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getSpendReportDashboard(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		CURDTemplateImpl<VendorMaster> dao = new CURDTemplateImpl<VendorMaster>();
		List<VendorMaster> vendors = dao
				.list(appDetails,
						"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus='A' OR vendorStatus='B')  ORDER BY vendorName ASC");
		reportForm.setVendors(vendors);

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> spendDashboard = null;
		spendDashboard = reportDao.getSpendDashboardReport(reportForm,
				appDetails);
		session.setAttribute("spendDashboard", spendDashboard);
		setPrintReportPdf(spendDashboard);

		return mapping.findForward("getdashboarddata");
	}

	/**
	 * To Produce PDF Using Jasper for Spend Data Dashboard Report for Vendor
	 * (Report-1).
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getSpendDataDashboardReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "SpendDataDashboard_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("spend_data_dashboard_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To view Reports Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "deprecation", "rawtypes", "unused" })
	public ActionForward viewDashboard(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		ReportDao reportDao = new ReportDaoImpl();
		ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
		Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
		Tier2ReportingPeriod reportingPeriod = periodDao
				.currentReportingPeriod(appDetails);

		int reportStartMonth = 0;
		int reportEndMonth = 3 - 1;
		Integer year = Calendar.getInstance().get(Calendar.YEAR);
		String currentQuarter = "";
		if (reportingPeriod != null) {
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
			switch (FrequencyPeriod
					.valueOf(reportingPeriod.getDataUploadFreq())) {

			case M:
				c.set(year, c.get(Calendar.MONTH) - 1, 1); // ------>
				c.set(Calendar.DAY_OF_MONTH,
						c.getActualMinimum(Calendar.DAY_OF_MONTH));

				StringBuilder period = new StringBuilder();

				period.append(sdf.format(c.getTime()));
				c.set(Calendar.DAY_OF_MONTH,
						c.getActualMaximum(Calendar.DAY_OF_MONTH));
				period.append(" - " + sdf.format(c.getTime()));
				currentQuarter = period.toString();
				break;
			case Q:
				List<String> reportPeriod = CommonUtils
						.calculateReportingPeriods(reportingPeriod, year);
				int index = CommonUtils.findCurrentQuarter(reportingPeriod);
				// For Getting Previous Quarter of Reporting Period
				if (0 == index) {
					// For Previous Year Last Quarter
					reportPeriod = CommonUtils.calculateReportingPeriods(
							reportingPeriod, year - 1);
					currentQuarter = reportPeriod.get(3);
				} else {
					currentQuarter = reportPeriod.get(index - 1);
				}
				break;
			case A:
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();

				c.set(year - 1, reportStartMonth, 1); // ------>
				c.set(Calendar.DAY_OF_MONTH,
						c.getActualMinimum(Calendar.DAY_OF_MONTH));
				StringBuilder period1 = new StringBuilder();

				period1.append(sdf.format(c.getTime()));
				c.set(year - 1, reportEndMonth, 1); // ------>
				c.set(Calendar.DAY_OF_MONTH,
						c.getActualMaximum(Calendar.DAY_OF_MONTH));
				period1.append(" - " + sdf.format(c.getTime()));
				currentQuarter = period1.toString();
				break;

			}
		}

		session.setAttribute("agencydashboardGV", null);
		session.setAttribute("diversityview", null);
		session.setAttribute("supplierdashboard", null);
		session.setAttribute("ethnicitydashboard", null);
		session.setAttribute("totalSpendDashboard", null);
		session.setAttribute("directDashboard", null);
		session.setAttribute("indirectDashboard", null);
		session.setAttribute("diversitydashboard", null);
		session.setAttribute("ethnicityAnalysis", null);
		session.setAttribute("ststusdashboardvalue", null);
		session.setAttribute("bpVendorsCountDashboard", null);
		session.setAttribute("newRegistrationsByMonthDashboard", null);
		session.setAttribute("vendorsByIndustryDashboard", null);
		session.setAttribute("bpVendorsByIndustryDashboard", null);
		session.setAttribute("vendorsByNaicsDescriptionDashboard", null);
		session.setAttribute("vendorsByBPMarketSectorDashboard", null);
		
		if (null != currentQuarter && !currentQuarter.isEmpty()) {
			session.setAttribute("currentQuarter", currentQuarter);
		}

		List<DashboardSettings> dashboardSettings = reportDao
				.listDashboardSettings(appDetails);

		CustomerApplicationSettings appSettings = (CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");
		if (appSettings.getIsDivision() != null
				&& appSettings.getIsDivision() != 0) {
			appDetails.setCustomerDivisionID((Integer) session
					.getAttribute("customerDivisionId"));
		}
		
		// User Role Based Dash Board Displaying
		Users user = (Users)session.getAttribute("currentUser");
		int userRoleId=0;
		if(null != user.getUserRoleId() && null != user.getUserRoleId().getId())
		{
			userRoleId=user
					.getUserRoleId().getId();
		}
		
		
		CURDDao<RolePrivileges> curdDao = new CURDTemplateImpl<RolePrivileges>();
		List<RolePrivileges> rolePrivilege = curdDao.list(appDetails,
				" from RolePrivileges r where r.view=1 and r.roleId="+userRoleId+" and r.objectId.applicationCategory='D'");
		
		if(rolePrivilege.size()!=0 && rolePrivilege!=null)
		{
			Iterator itr=rolePrivilege.iterator();
			RolePrivileges privilege=new RolePrivileges();
			while(itr.hasNext())
			{
				privilege = (RolePrivileges)itr.next();
				if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Diversity Analysis Dashboard"))
				{
					session.setAttribute("diversityview", "1");
					List<Tier2ReportDto> diversityDashboard = null;
					diversityDashboard = reportDao
							.getDiversityDashboardReport(appDetails);
					session.setAttribute("diversityDashboard",
							diversityDashboard);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendor Status Breakdown Dashboard"))
				{
					session.setAttribute(
							"ststusdashboardvalue", "1");
					List<Tier2ReportDto> statusDashboard = null;
					statusDashboard = reportDao
							.getStatusDashboardReport(appDetails);
					session.setAttribute("statusDashboard",
							statusDashboard);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Supplier Count By State Dashboard"))
				{
					session.setAttribute("supplierdashboard",
							"1");
					List<Tier2ReportDto> supplierStateDashboard = null;
					supplierStateDashboard = reportDao
							.getSupplierStateDashboardReport(appDetails);
					session.setAttribute(
							"supplierStateDashboard",
							supplierStateDashboard);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendor Count By State Dashboard"))
				{
					session.setAttribute(
							"bpVendorsCountDashboard", "1");
					List<Tier2ReportDto> bpVendorsCount = null;
					bpVendorsCount = reportDao
							.getBpVendorCountDashboardReport(appDetails);
					session.setAttribute("bpVendorsCount",
							bpVendorsCount);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Spend By Agency Dashboard"))
				{
					session.setAttribute("agencydashboardGV",
							"1");
					List<Tier2ReportDto> agencyDashboard = null;
					agencyDashboard = reportDao
							.getAgencyDashboardReport(appDetails);
					session.setAttribute("agencyDashboard",
							agencyDashboard);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Total Spend Dashboard"))
				{
					session.setAttribute("totalSpendDashboard",
							"1");
					List<Tier2ReportDto> totalSalesDB = null;
					totalSalesDB = dashboardDao
							.getTotalSpendReport(appDetails,
									year.toString(),
									currentQuarter);
					session.setAttribute("totalSalesDB",
							totalSalesDB);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Direct Spend Dashboard"))
				{
					session.setAttribute("directDashboard", "1");
					List<Tier2ReportDto> directSpendDB = null;
					directSpendDB = dashboardDao
							.getDirectSpendReport(appDetails,
									year.toString(),
									currentQuarter);
					session.setAttribute("directSpendDBG",
							directSpendDB);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Indirect Spend Dashboard"))
				{
					session.setAttribute("indirectDashboard",
							"1");
					List<Tier2ReportDto> indirectSpendDB = null;
					indirectSpendDB = dashboardDao
							.getIndirectSpendReport(
									appDetails,
									year.toString(),
									currentQuarter);
					session.setAttribute("indirectSpendDBG",
							indirectSpendDB);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Tier 2 Vendor Name Report Dashboard"))
				{
					session.setAttribute("diversitydashboard",
							"1");
					List<Tier2ReportDto> diversityDBG = null;
					diversityDBG = dashboardDao
							.getDiversityReport(appDetails,
									year.toString(),
									currentQuarter);
					session.setAttribute("diversityDBG",
							diversityDBG);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Tier 2 Reporting Summary Dashboard"))
				{
					session.setAttribute("ethnicityAnalysis",
							"1");
					List<Tier2ReportDto> ethnicityDBG = null;
					ethnicityDBG = dashboardDao
							.getEthnicityReport(appDetails,
									year.toString(),
									currentQuarter);
					session.setAttribute("ethnicityDBG",
							ethnicityDBG);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("New Registrations By Month Dashboard"))
				{
					session.setAttribute("newRegistrationsByMonthDashboard", "1");
					List<Tier2ReportDto> newRegistrationsByMonth = null;
					newRegistrationsByMonth = dashboardDao.getNewRegistrationsByMonthDashboardReport(appDetails);
					session.setAttribute("newRegistrationsByMonth", newRegistrationsByMonth);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendors By Business Type"))
				{
					session.setAttribute("vendorsByIndustryDashboard", "1");
					List<Tier2ReportDto> vendorsByIndustry = null;
					vendorsByIndustry = dashboardDao.getVendorsByIndustryDashboardReport(appDetails);
					session.setAttribute("vendorsByIndustry", vendorsByIndustry);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendors By Business Type"))
				{
					session.setAttribute("bpVendorsByIndustryDashboard", "1");
					List<Tier2ReportDto> bpVendorsByIndustry = null;
					bpVendorsByIndustry = dashboardDao.getBPVendorsByIndustryDashboardReport(appDetails);
					session.setAttribute("bpVendorsByIndustry", bpVendorsByIndustry);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendors By NAICS Description"))
				{
					session.setAttribute("vendorsByNaicsDescriptionDashboard", "1");
					List<Tier2ReportDto> vendorsByNaicsDescription = null;
					vendorsByNaicsDescription = dashboardDao.getVendorsByNAICSDecriptionDashboardReport(appDetails);
					session.setAttribute("vendorsByNaicsDescription", vendorsByNaicsDescription);
				}
				else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendors By BP Market Sector"))
				{
					session.setAttribute("vendorsByBPMarketSectorDashboard", "1");
					List<Tier2ReportDto> vendorsByBPMarketSector = null;
					vendorsByBPMarketSector = dashboardDao.getVendorsByBPMarketSectorDashboardReport(appDetails);
					session.setAttribute("vendorsByBPMarketSector", vendorsByBPMarketSector);
				}
			}
		}
		return mapping.findForward("dashboard");
	}

	/**
	 * To view Reports Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDashboardData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> ethnicityDashboard = null;
		ethnicityDashboard = reportDao.getEthnicityDashboardReport(appDetails);
		session.setAttribute("ethnicityDashboard", ethnicityDashboard);

		return mapping.findForward("dashboarddata");
	}

	/**
	 * To view Diversity Chart Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDiversityChartData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> diversityDashboard = null;
		diversityDashboard = reportDao.getDiversityDashboardReport(appDetails);
		session.setAttribute("diversityDashboard", diversityDashboard);

		return mapping.findForward("diversitydashboard");
	}

	/**
	 * To view Supplier count by state Chart Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getSupplierStateChartData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> supplierStateDashboard = null;
		supplierStateDashboard = reportDao
				.getSupplierStateDashboardReport(appDetails);
		session.setAttribute("supplierStateDashboard", supplierStateDashboard);

		return mapping.findForward("statedashboard");
	}

	/**
	 * To view Supplier count by state Chart Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getAgencyChartData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> agencyDashboard = null;
		agencyDashboard = reportDao.getAgencyDashboardReport(appDetails);
		session.setAttribute("agencyDashboard", agencyDashboard);

		return mapping.findForward("agencydashboard");
	}

	/**
	 * To view Search vendor report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchVendorReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();
		SearchVendorForm reportForm = (SearchVendorForm) form;

//			char searchType=request.getParameter("searchType").charAt(0);
				Integer userId = (Integer) session.getAttribute("userId");
				List<CustomerSearchFilter> previousSearchDetailsList=searchDao.listPreviousSearchDetails(appDetails,'R',userId);
				session.setAttribute("previousSearchList", previousSearchDetailsList);
		
		

		CertificateDao certificate = new CertificateDaoImpl();
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		CURDDao<com.fg.vms.customer.model.StatusMaster> statusDao = new CURDTemplateImpl<com.fg.vms.customer.model.StatusMaster>();
		List<com.fg.vms.customer.model.StatusMaster> vendorStatusList = statusDao
				.list(appDetails,
						" From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");
		reportForm.setVendorStatusList(vendorStatusList);
		reportForm.setCountries(countries);
		List<com.fg.vms.customer.model.StatusMaster> vendorStatus = new ArrayList<StatusMaster>();
		if (vendorStatusList != null && !vendorStatusList.isEmpty()) {
			for (StatusMaster master : vendorStatusList) {
				if (master.getId().equals('A') || master.getId().equals('B')) {
					vendorStatus.add(master);
				}
			}
		}
		reportForm.setVendorStatus(vendorStatus);
		List<Certificate> certificates = null;
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails); // List of certificate types.
		List<CertifyingAgency> certificateAgencies = searchDao
				.listAgencies(appDetails);
		session.setAttribute("certificateTypes", certificates);
		reportForm.setCertifyingAgencies(certificateAgencies);

		/*
		 * Show list of Business areas.
		 */
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<GeographicalStateRegionDto> businessAreas = null;

		businessAreas = regionDao.getServiceAreasList(appDetails);
		session.setAttribute("businessAreas", businessAreas);

		/*
		 * Show list of commodity description.
		 */
		List<GeographicalStateRegionDto> commodityDescription = null;

		commodityDescription = regionDao.getCommodityDescription(appDetails);
		session.setAttribute("commodityDescription", commodityDescription);

		/*
		 * Show list of naics Code.
		 */
		/*
		 * List<NaicsCode> naicsCodeList=null;
		 * 
		 * naicsCodeList=regionDao.getNaicsCode(appDetails);
		 * session.setAttribute("naicsCodeList", naicsCodeList);
		 */

		/*
		 * Get the custom display/export fields.
		 */
		List<GeographicalStateRegionDto> exportMasters = searchDao
				.listOfExportFields(appDetails);

		/*
		 * Show list of certification type.
		 */
		List<GeographicalStateRegionDto> certificationTypes = null;

		certificationTypes = regionDao.getCertificationType(appDetails);
		session.setAttribute("certificationTypes", certificationTypes);
		
		//Show List Commodity Group with Market Sector, Market SubSector, & Commodity Description Hierarchy
		List<GeographicalStateRegionDto> vendorCommodity = null;
		
		vendorCommodity = regionDao.getCommodityGroup(appDetails);
		session.setAttribute("vendorCommodity", vendorCommodity);
		
		CURDDao<com.fg.vms.customer.model.State> daoContactStates = new CURDTemplateImpl<com.fg.vms.customer.model.State>();
		List<com.fg.vms.customer.model.State> contactStates = daoContactStates.list(appDetails, "From State where isactive=1 order by statename");
		session.setAttribute("contactStates", contactStates);

		reportForm.setExportMasters(exportMasters);
		session.setAttribute("vendorStatusList", vendorStatusList);
		
		CURDDao<SegmentMaster> curdDao1 = new CURDTemplateImpl<SegmentMaster>();
		List<SegmentMaster> segmentMasters = curdDao1.list(appDetails," From SegmentMaster order by segmentName");
		session.setAttribute("bpSegmentSearchList", segmentMasters);
		
		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorsList = marketSectorDao.list(appDetails,"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorsList", marketSectorsList);
		
		return mapping.findForward("showsearchvendor");
	}

	/**
	 * Get the data based on search criteria.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward getSearchReportData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String searchType=request.getParameter("type");
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();

		CustomerApplicationSettings settings = (CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");
		searchVendorForm.setIsDivision(settings.getIsDivision());
		// check is divsion status && and set customerDivsion id
		if (settings.getIsDivision() != 0 && settings.getIsDivision() != null) {
			searchVendorForm.setCustomerDivisionId((Integer) session
					.getAttribute("customerDivisionId"));
		}
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		searchVendorForm.setCountries(countries);

		ReportDao reportDao = new ReportDaoImpl();
		List<?> searchList = null;
		searchList = reportDao.getSearchReport(searchVendorForm, appDetails);
		setPrintReportPdf((List<Tier2ReportDto>) searchList);
		session.setAttribute("searchList", searchList);

		if(searchType != null && searchType.equalsIgnoreCase("customize")){
			session.setAttribute("searchConditions", searchVendorForm.getSearchConditions());
			session.setAttribute("whereConditionTableList", searchVendorForm.getFromtableForConditions());
		}
		
		return mapping.findForward("getsearcheddata");
	}
 
	/**
	 * To export report data into excel format.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward excelExport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		return mapping.findForward("getexportedexceldata");
	}

	/**
	 * To export report data into csv format.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward csvExport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		return mapping.findForward("getexportedcsvdata");
	}

	/**
	 * To export report data into PDF format.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward pdfExport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		return mapping.findForward("getexportedpdfdata");
	}

	/**
	 * Dashboard configuration settings.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewDashboardSettings(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		ReportDao reportDao = new ReportDaoImpl();

		List<DashboardSettings> dashboardSettings = reportDao
				.listDashboardSettings(appDetails);

		if (dashboardSettings != null) {

			for (DashboardSettings settings : dashboardSettings) {

				switch (ReportTypes.getReportTypes(settings.getReportName())) {

				case AGENCYDASHBOARD:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setAgencyDashboard("1");
					}
					break;
				case DIVERSITYDASHBOARD:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setDiversityDashboard("1");
					}
					break;
				case SUPPLIERDASHBOARD:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setSupplierDashboard("1");
					}
					break;

				case ETHNICITYDASHBOARD:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setEthnicityDashboard("1");
					}
					break;

				case TOTALSPEND:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setTotalSpend("1");
					}
					break;
				case DIRECTSSPEND:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setDirectSpend("1");
					}
					break;
				case INDIRECTSPEND:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setIndirectSpend("1");
					}
					break;
				case DIVERSITYSTATUS:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setDiversityStatus("1");
					}
					break;

				case ETHNICITYANALYSIS:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setEthnicityAnalysis("1");
					}
					break;

				case SPENDDASHBOARD:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setSpendDashboard("1");
					}
					break;

				case STATUSDASHBOARD:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setVendorStatus("1");
					}
					break;
				case BPVENDORSCOUNT:
					if (settings.getIsDashboard().equals("1")) {
						reportForm.setBpVendorsCountDashboard("1");
					}
					break;

				default:
					break;
				}
			}
		}
		return mapping.findForward("successconfig");
	}

	/**
	 * Dashboard configuration settings.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveDashboardSettings(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;

		ReportDao reportDao = new ReportDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String result = "failure";

		result = reportDao.updateDashboardSettings(reportForm, userId,
				userDetails);

		// For view the list of certificates
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("dashboardsettings.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			// workflowConfigurationForm.reset(mapping, request);
		} else if (result.equals("failure")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("dashboardsettings.fail");
			messages.add("failureMsg", msg);
			saveMessages(request, messages);
		}

		return mapping.findForward("saveconfig");
	}

	/**
	 * To view vendor status Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorStatusChartData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> statusDashboard = null;
		statusDashboard = reportDao.getStatusDashboardReport(appDetails);
		session.setAttribute("statusDashboard", statusDashboard);

		return mapping.findForward("statusdashboard");
	}

	/**
	 * To view BP Vendor count by state Chart Dashboard.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getBPVendorStateChartData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> bpVendorsCount = null;
		bpVendorsCount = reportDao.getBpVendorCountDashboardReport(appDetails);
		session.setAttribute("bpVendorsCount", bpVendorsCount);

		return mapping.findForward("bpvendorstatedashboard");
	}

	/**
	 * To Get the Search Vendor Report for Jasper Export.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */	
	public ActionForward getSearchVendorReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String colHeader = request.getParameter("colHeader");
		System.out.println("colHeader-->:"+colHeader);
		// For Setting Selected Header Names to True
		SearchVendorDetailsDto headerParam = null;
		headerParam = setSelectedHeader(colHeader);

		String columnHeader[] = colHeader.split(",");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		// OutputStream servletOutputStream = null;
		//
		// try{
		// String reportName = "SupplierReport_" + dateTime;
		// HttpServletResponse httpServletResponse = response;
		// httpServletResponse.setContentType("application/pdf	");
		// httpServletResponse.setHeader("Content-Disposition",
		// "attachment; filename=" + reportName + ".pdf");
		//
		// Map params = new HashMap();
		// params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
		// + "/" + appDetails.getSettings().getLogoPath());
		// params.put("colHeader", headerParam);
		//
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(
		// getSupplierCustomSearchReportList());
		//
		// InputStream is = JRLoader
		// .getResourceInputStream("custom_supplier_search_report.jasper");
		// JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
		// beanCollectionDataSource);
		//
		// servletOutputStream = httpServletResponse.getOutputStream();
		// JasperExportManager.exportReportToPdfStream(jasperPrint,
		// servletOutputStream);
		// servletOutputStream.flush();
		// servletOutputStream.close();
		// }
		// catch (IOException e) {
		// System.out.println("Exception occured - " + e.getMessage());
		// }
		// catch (Exception e) {
		// e.printStackTrace();
		// System.out.println("Exception occured - " + e.getMessage());
		// }
		OutputStream servletOutputStream = null;
		try {
			String reportName = "SupplierReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();
			Page page = new Page(1000, 1500, true);
			dynamicReportBuilder.setPageSizeAndOrientation(page);
			String imagePath = Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath();
			dynamicReportBuilder.setUseFullPageWidth(true)
					.addFirstPageImageBanner(imagePath, 60, 50,
							ImageBanner.ALIGN_LEFT);

			// dynamicReportBuilder.setPageSizeAndOrientation(Page.Page_Letter_Landscape());

			// Style for title
			Style TitleStyle = new StyleBuilder(true).setFont(
					new Font(12, Font._FONT_VERDANA, true, false, false))
					.build();
			TitleStyle.setHorizontalAlign(HorizontalAlign.CENTER);
			// dynamicReportBuilder.addAutoText("Supplier Custom Search Report",
			// AutoText.POSITION_HEADER,
			// AutoText.ALIGNMENT_CENTER,200,atStyle2);
			dynamicReportBuilder.setTitle("Supplier Custom Search Report");
			dynamicReportBuilder.setTitleStyle(TitleStyle);

			// dynamicReportBuilder.setUseFullPageWidth(true);

			// Style for column header
			Style headerStyle = new Style();
			headerStyle.setFont(Font.VERDANA_MEDIUM_BOLD);
			headerStyle.setHorizontalAlign(HorizontalAlign.LEFT);

			// Style for column header
			Style ColumnBodyStyle = new Style();
			ColumnBodyStyle.setFont(Font.VERDANA_MEDIUM);
			ColumnBodyStyle.setHorizontalAlign(HorizontalAlign.LEFT);
			ColumnBodyStyle.setVerticalAlign(VerticalAlign.MIDDLE);
			int coulumnWidth = Math.round(1425 / (columnHeader.length));

			if (headerParam.isVendorName()) {
				ColumnBuilder columnVendorName = ColumnBuilder.getNew();

				columnVendorName.setTitle("Vendor Name");
				columnVendorName.setHeaderStyle(headerStyle);
				columnVendorName.setWidth(coulumnWidth);
				columnVendorName.setFixedWidth(true);
				columnVendorName.setColumnProperty("vendorName",
						String.class.getName(), "vendorName");
				columnVendorName.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnVendorName.build());
			}
			if (headerParam.isOwnerName()) {
				ColumnBuilder columnOwnerName = ColumnBuilder.getNew();

				columnOwnerName.setTitle("Owner Name");
				columnOwnerName.setHeaderStyle(headerStyle);
				columnOwnerName.setWidth(coulumnWidth);
				columnOwnerName.setFixedWidth(true);
				columnOwnerName.setColumnProperty("ownerName",
						String.class.getName(), "ownerName");
				columnOwnerName.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnOwnerName.build());
			}

			if (headerParam.isAddress()) {
				ColumnBuilder columnVendorAddress = ColumnBuilder.getNew();

				columnVendorAddress.setTitle("Address");
				columnVendorAddress.setHeaderStyle(headerStyle);
				columnVendorAddress.setWidth(coulumnWidth);
				columnVendorAddress.setFixedWidth(true);
				columnVendorAddress.setColumnProperty("address1",
						String.class.getName(), "address1");
				columnVendorAddress.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnVendorAddress.build());
			}

			if (headerParam.isCity()) {
				ColumnBuilder columnCity = ColumnBuilder.getNew();

				columnCity.setTitle("City");
				columnCity.setHeaderStyle(headerStyle);
				columnCity.setWidth(coulumnWidth);
				columnCity.setFixedWidth(true);
				columnCity.setColumnProperty("city", String.class.getName(),
						"city");
				columnCity.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnCity.build());
			}

			if (headerParam.isState()) {
				ColumnBuilder columnState = ColumnBuilder.getNew();

				columnState.setTitle("State");
				columnState.setHeaderStyle(headerStyle);
				columnState.setWidth(coulumnWidth);
				columnState.setFixedWidth(true);
				columnState.setColumnProperty("state", String.class.getName(),
						"state");
				columnState.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnState.build());
			}

			if (headerParam.isZipcode()) {
				ColumnBuilder columnZip = ColumnBuilder.getNew();

				columnZip.setTitle("Zip");
				columnZip.setHeaderStyle(headerStyle);
				columnZip.setWidth(coulumnWidth);
				columnZip.setFixedWidth(true);
				columnZip.setColumnProperty("zip", String.class.getName(),
						"zip");
				columnZip.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnZip.build());
			}

			if (headerParam.isPhone()) {
				ColumnBuilder columnPhone = ColumnBuilder.getNew();

				columnPhone.setTitle("Phone");
				columnPhone.setHeaderStyle(headerStyle);
				columnPhone.setWidth(coulumnWidth);
				columnPhone.setFixedWidth(true);
				columnPhone.setColumnProperty("phone", String.class.getName(),
						"phone");
				columnPhone.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnPhone.build());
			}

			if (headerParam.isFax()) {
				ColumnBuilder columnFax = ColumnBuilder.getNew();

				columnFax.setTitle("Fax");
				columnFax.setHeaderStyle(headerStyle);
				columnFax.setWidth(coulumnWidth);
				columnFax.setFixedWidth(true);
				columnFax.setColumnProperty("fax", String.class.getName(),
						"fax");
				columnFax.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnFax.build());
			}

			if (headerParam.isEmailId()) {
				ColumnBuilder columnEmailId = ColumnBuilder.getNew();

				columnEmailId.setTitle("EmailID");
				columnEmailId.setHeaderStyle(headerStyle);
				columnEmailId.setWidth(coulumnWidth);
				columnEmailId.setFixedWidth(true);
				columnEmailId.setColumnProperty("emailId",
						String.class.getName(), "emailId");
				columnEmailId.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnEmailId.build());
			}

			if (headerParam.isAgencyName()) {
				ColumnBuilder columnAgencyName = ColumnBuilder.getNew();

				columnAgencyName.setTitle("Agency Name");
				columnAgencyName.setHeaderStyle(headerStyle);
				columnAgencyName.setWidth(coulumnWidth);
				columnAgencyName.setFixedWidth(true);
				columnAgencyName.setColumnProperty("certAgencyName",
						String.class.getName(), "certAgencyName");
				columnAgencyName.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnAgencyName.build());
			}

			if (headerParam.isCertName()) {
				ColumnBuilder columnCertName = ColumnBuilder.getNew();

				columnCertName.setTitle("Certificate Name");
				columnCertName.setHeaderStyle(headerStyle);
				columnCertName.setWidth(coulumnWidth);
				columnCertName.setFixedWidth(true);
				columnCertName.setColumnProperty("certificateName",
						String.class.getName(), "certificateName");
				columnCertName.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnCertName.build());
			}

			if (headerParam.isEffDate()) {
				ColumnBuilder columnEffDate = ColumnBuilder.getNew();

				columnEffDate.setTitle("Effective Date");
				columnEffDate.setHeaderStyle(headerStyle);
				columnEffDate.setWidth(coulumnWidth);
				columnEffDate.setFixedWidth(true);
				columnEffDate.setColumnProperty("effDate",
						String.class.getName(), "effDate");
				columnEffDate.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnEffDate.build());
			}

			if (headerParam.isExpDate()) {
				ColumnBuilder columnExpDate = ColumnBuilder.getNew();

				columnExpDate.setTitle("Expiry date");
				columnExpDate.setHeaderStyle(headerStyle);
				columnExpDate.setWidth(coulumnWidth);
				columnExpDate.setFixedWidth(true);
				columnExpDate.setColumnProperty("expDate",
						String.class.getName(), "expDate");
				columnExpDate.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnExpDate.build());
			}

			if (headerParam.isCapablities()) {
				ColumnBuilder columnCapabilities = ColumnBuilder.getNew();

				columnCapabilities.setTitle("Capabilities");
				columnCapabilities.setHeaderStyle(headerStyle);
				columnCapabilities.setWidth(coulumnWidth);
				columnCapabilities.setFixedWidth(true);
				columnCapabilities.setColumnProperty("capablities",
						String.class.getName(), "capablities");
				columnCapabilities.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnCapabilities.build());
			}

			if (headerParam.isNaicsDesc()) {
				ColumnBuilder columnNaicsDesc = ColumnBuilder.getNew();

				columnNaicsDesc.setTitle("NAICS Description");
				columnNaicsDesc.setHeaderStyle(headerStyle);
				columnNaicsDesc.setWidth(coulumnWidth);
				columnNaicsDesc.setFixedWidth(true);
				columnNaicsDesc.setColumnProperty("naicsDesc",
						String.class.getName(), "naicsDesc");
				columnNaicsDesc.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnNaicsDesc.build());
			}

			if (headerParam.isNaicsCapabilities()) {
				ColumnBuilder columnNaicsCapabilities = ColumnBuilder.getNew();

				columnNaicsCapabilities.setTitle("NAICS Capabilities");
				columnNaicsCapabilities.setHeaderStyle(headerStyle);
				columnNaicsCapabilities.setWidth(coulumnWidth);
				columnNaicsCapabilities.setFixedWidth(true);
				columnNaicsCapabilities.setColumnProperty("naicsCapabilities",
						String.class.getName(), "naicsCapabilities");
				columnNaicsCapabilities.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnNaicsCapabilities.build());
			}

			if (headerParam.isDiverseClassification()) {
				ColumnBuilder columnDiverseClassification = ColumnBuilder
						.getNew();

				columnDiverseClassification.setTitle("Diverse Classification");
				columnDiverseClassification.setHeaderStyle(headerStyle);
				columnDiverseClassification.setWidth(coulumnWidth);
				columnDiverseClassification.setFixedWidth(true);
				columnDiverseClassification.setColumnProperty(
						"diverseClassification", String.class.getName(),
						"diverseClassification");
				columnDiverseClassification.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnDiverseClassification
						.build());
			}

			if (headerParam.isCertificateAgencyDetails()) {
				ColumnBuilder columncertificateAgencyDetails = ColumnBuilder
						.getNew();

				columncertificateAgencyDetails
						.setTitle("Certificate Agency Details");
				columncertificateAgencyDetails.setHeaderStyle(headerStyle);
				columncertificateAgencyDetails.setWidth(coulumnWidth);
				columncertificateAgencyDetails.setFixedWidth(true);
				columncertificateAgencyDetails.setColumnProperty(
						"certificateAgencyDetails", String.class.getName(),
						"certificateAgencyDetails");
				columncertificateAgencyDetails.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columncertificateAgencyDetails
						.build());
			}

			DynamicReport dynamicReport = dynamicReportBuilder.build();

			// build a datasource representing the XML file
			// JRDataSource dataSource = new JRXmlDataSource(new
			// File("data.xml"), "//employee");

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			// build JasperPrint instance, filling the report with data from
			// datasource created above
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
					dynamicReport, new ClassicLayoutManager(),
					beanCollectionDataSource, new HashMap<String, Object>());

			// export to pdf
			// String pdfFile = Math.round(Math.random() * 100000) + ".pdf";

			JRExporter exporter = new JRPdfExporter();

			servletOutputStream = httpServletResponse.getOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					servletOutputStream);

			exporter.exportReport();
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (JRException e) {
			e.printStackTrace();
		}

		return mapping.findForward("showsearchvendor");
	}

	/**
	 * To Set Selected Header Names to True.
	 * 
	 * @param colHeader
	 * 
	 */
	public SearchVendorDetailsDto setSelectedHeader(String colHeader) {
		SearchVendorDetailsDto headerParam = new SearchVendorDetailsDto();

		if (colHeader != null) {
			String[] splitHeader = colHeader.split(",");

			for (String item : splitHeader) {
				if (item.equalsIgnoreCase("vendorName")) {
					headerParam.setVendorName(true);
				}
				if (item.equalsIgnoreCase("ownerName")) {
					headerParam.setOwnerName(true);
				}
				if (item.equalsIgnoreCase("address")) {
					headerParam.setAddress(true);
				}
				if (item.equalsIgnoreCase("city")) {
					headerParam.setCity(true);
				}
				if (item.equalsIgnoreCase("state")) {
					headerParam.setState(true);
				}
				if (item.equalsIgnoreCase("zipcode")) {
					headerParam.setZipcode(true);
				}
				if (item.equalsIgnoreCase("phone")) {
					headerParam.setPhone(true);
				}
				if (item.equalsIgnoreCase("fax")) {
					headerParam.setFax(true);
				}
				if (item.equalsIgnoreCase("emailId")) {
					headerParam.setEmailId(true);
				}
				if (item.equalsIgnoreCase("agencyName")) {
					headerParam.setAgencyName(true);
				}
				if (item.equalsIgnoreCase("certName")) {
					headerParam.setCertName(true);
				}
				if (item.equalsIgnoreCase("effDate")) {
					headerParam.setEffDate(true);
				}
				if (item.equalsIgnoreCase("expDate")) {
					headerParam.setExpDate(true);
				}
				if (item.equalsIgnoreCase("capablities")) {
					headerParam.setCapablities(true);
				}
				if (item.equalsIgnoreCase("naicsDesc")) {
					headerParam.setNaicsDesc(true);
				}
				if (item.equalsIgnoreCase("naicsCapabilities")) {
					headerParam.setNaicsCapabilities(true);
				}
				if (item.equalsIgnoreCase("diverseClassification")) {
					headerParam.setDiverseClassification(true);
				}
				if (item.equalsIgnoreCase("certificateAgencyDetails")) {
					headerParam.setCertificateAgencyDetails(true);
				}
			}
		}
		return headerParam;
	}

	/**
	 * To view Diversity Analysis Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewDiversityAnalysisReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("showDiversityAnalysisReport");
	}

	/**
	 * To Retrieve Diversity Analysis Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDiversityAnalysisReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> diversityAnalysisReport = null;
		diversityAnalysisReport = reportDao
				.getDiversityDashboardReport(appDetails);
		session.setAttribute("diversityAnalysisReport", diversityAnalysisReport);
		setPrintReportPdf(diversityAnalysisReport);

		return mapping.findForward("getDiversityAnalysisData");
	}

	/**
	 * To Produce PDF Using Jasper for Diversity Analysis Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getDiversityAnalysisReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "DiversityAnalysisReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("diversity_analysis_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Vendor Status Breakdown Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewVendorStatusBreakdownReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("showVendorStatusBreakdownReport");
	}

	/**
	 * To Retrieve Vendor Status Breakdown Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorStatusBreakdownReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorStatusBreakdownReport = null;
		vendorStatusBreakdownReport = reportDao
				.getStatusDashboardReport(appDetails);
		session.setAttribute("vendorStatusBreakdownReport",
				vendorStatusBreakdownReport);
		setPrintReportPdf(vendorStatusBreakdownReport);

		return mapping.findForward("getVendorStatusBreakdownData");
	}

	/**
	 * To Produce PDF Using Jasper for Vendor Status Breakdown Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorStatusBreakdownReportPdf(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorStatusBreakdownReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("vendor_status_breakdown_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Supplier Count by State Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewSupplierCountByStateReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("showSupplierCountByStateReport");
	}

	/**
	 * To Retrieve Supplier Count by State Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getSupplierCountByStateReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> supplierCountByStateReport = null;
		supplierCountByStateReport = reportDao
				.getSupplierStateDashboardReport(appDetails);
		session.setAttribute("supplierCountByStateReport",
				supplierCountByStateReport);
		setPrintReportPdf(supplierCountByStateReport);

		return mapping.findForward("getSupplierCountByStateData");
	}

	/**
	 * To Produce PDF Using Jasper for Supplier Count by State Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getSupplierCountByStateReportPdf(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "SupplierCountByStateReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("supplier_count_by_state_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View BP Vendor Count by State Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewBPVendorCountByStateReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("showBPVendorCountByStateReport");
	}

	/**
	 * To Retrieve BP Vendor Count by State Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getBPVendorCountByStateReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> bpVendorCountByStateReport = null;
		bpVendorCountByStateReport = reportDao
				.getBpVendorCountDashboardReport(appDetails);
		session.setAttribute("bpVendorCountByStateReport",
				bpVendorCountByStateReport);
		setPrintReportPdf(bpVendorCountByStateReport);

		return mapping.findForward("getBPVendorCountByStateData");
	}

	/**
	 * To Produce PDF Using Jasper for BP Vendor Count by State Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getBPVendorCountByStateReportPdf(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "BPVendorCountByStateReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("bpvendor_count_by_state_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Spend By Agency Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewSpendByAgencyReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("showSpendByAgencyReport");
	}

	/**
	 * To Retrieve Spend By Agency Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getSpendByAgencyReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> spendByAgencyReport = null;
		spendByAgencyReport = reportDao.getAgencyDashboardReport(appDetails);
		session.setAttribute("spendByAgencyReport", spendByAgencyReport);
		setPrintReportPdf(spendByAgencyReport);

		return mapping.findForward("getSpendByAgencyData");
	}

	/**
	 * To Produce PDF Using Jasper for Spend By Agency Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getSpendByAgencyReportPdf(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "SpendByAgencyReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("spend_by_agency_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Vendor Commodities Not Saved Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewVendorCommoditiesNotSavedReport(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("showVendorCommoditiesNotSavedReport");
	}

	/**
	 * To Retrieve Vendor Commodities Not Saved Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorCommoditiesNotSavedReport(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorCommoditiesNotSavedReport = null;
		vendorCommoditiesNotSavedReport = reportDao
				.getVendorCommoditiesNotSavedReport(appDetails);
		session.setAttribute("vendorCommoditiesNotSavedReport",
				vendorCommoditiesNotSavedReport);
		setPrintReportPdf(vendorCommoditiesNotSavedReport);

		return mapping.findForward("getVendorCommoditiesNotSavedData");
	}

	/**
	 * To Produce PDF Using Jasper for Vendor Commodities Not Saved Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorCommoditiesNotSavedReportPdf(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorCommoditiesNotSavedReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getPrintReportPdf());

			InputStream is = JRLoader
					.getResourceInputStream("vendor_commodities_not_saved_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Certificate Expiration Notification Email Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewCertificateExpirationNotificationEmailReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		logger.info("To View Certificate Expiration Notification Email Report Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
		List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
		session.setAttribute("VendorStatusMasterList", vendorStatusMaster);
		
		if(vendorStatusMaster != null && vendorStatusMaster.size() != 0)
		{
			String statusIds[] = new String[vendorStatusMaster.size()];
			for(int i = 0; i < vendorStatusMaster.size(); i++)
			{
				if(vendorStatusMaster.get(i).getIscertificateexpiryalertreq() == 1)
					statusIds[i] = vendorStatusMaster.get(i).getId().toString();
			}
			reportForm.setVendorStatusList(statusIds);
		}
		
		return mapping.findForward("showCertificateExpirationNotificationEmailReport");
	}
	
	/**
	 * To Retrieve Certificate Expiration Notification Email Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getCertificateExpirationNotificationEmailReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		
		if(reportForm.getStartDate() != null && !reportForm.getStartDate().isEmpty() 
				&& reportForm.getEndDate() != null && !reportForm.getEndDate().isEmpty())
		{
			ReportDao reportDao = new ReportDaoImpl();
			List<Tier2ReportDto> certificateExpirationNotificationEmailReport = null;
			certificateExpirationNotificationEmailReport = reportDao.getCertificateExpirationNotificationEmailReport(reportForm, appDetails);
			session.setAttribute("certificateExpirationNotificationEmailReport",	certificateExpirationNotificationEmailReport);
			setPrintReportPdf(certificateExpirationNotificationEmailReport);		

			return mapping.findForward("getCertificateExpirationNotificationEmailData");
		}
		return mapping.findForward("showCertificateExpirationNotificationEmailReport");
	}
	
	/**
	 * To Produce PDF Using Jasper for Certificate Expiration Notification Email Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getCertificateExpirationNotificationEmailReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "CertificateExpirationNotificationEmailReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("certificate_expiration_notification_email_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Registered Vendors Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewRegisteredVendorsReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward("showRegisteredVendorsReport");
	}
	
	/**
	 * To Retrieve Registered Vendors Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getRegisteredVendorsReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		
		if(reportForm.getStartDate() != null && !reportForm.getStartDate().isEmpty() 
				&& reportForm.getEndDate() != null && !reportForm.getEndDate().isEmpty()) 
		{
			ReportDao reportDao = new ReportDaoImpl();
			List<Tier2ReportDto> registeredVendorsReport = null;
			registeredVendorsReport = reportDao.getRegisteredVendorsReport(reportForm, appDetails);
			session.setAttribute("registeredVendorsReport",	registeredVendorsReport);
			setPrintReportPdf(registeredVendorsReport);		

			return mapping.findForward("getRegisteredVendorsReportData");
		}
		return mapping.findForward("showRegisteredVendorsReport");
	}
	
	/**
	 * To Produce PDF Using Jasper for Registered Vendors Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getRegisteredVendorsReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "RegisteredVendorsReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("registered_vendors_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To View Vendors By Business Type.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewVendorsByIndustryReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To View Vendors By Business Type Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
		List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
		reportForm.setVendorStatusMaster(vendorStatusMaster);		
		
		return mapping.findForward("showVendorsByIndustryReport");
	}
	
	/**
	 * To Retrieve Vendors By Business Type Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorsByIndustryReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Retrieve Data for Vendors By Business Type in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		
		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorsByIndustryReport = null;
		vendorsByIndustryReport = reportDao.getVendorsByIndustryReport(reportForm, appDetails);
		session.setAttribute("vendorsByIndustryReport",	vendorsByIndustryReport);
		setPrintReportPdf(vendorsByIndustryReport);		

		return mapping.findForward("getVendorsByIndustryReportData");
	}

	/**
	 * To Produce PDF Using Jasper for Vendors By Business Type.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorsByIndustryReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Produce PDF Using Jasper for Vendors By Business Type in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorsByBusinessTypeReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("vendors_by_industry_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
	
	/**
	 * To View Vendors By NAICS Description Report Page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewVendorsByNaicsReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To View Vendors By NAICS Description Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
		List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
		reportForm.setVendorStatusMaster(vendorStatusMaster);		
		
		return mapping.findForward("showVendorsByNaicsReport");
	}
	
	/**
	 * To Retrieve Vendors By NAICS Description Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorsByNaicsReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Retrieve Data for Vendors By NAICS Description Report in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		
		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorsByNaicsReport = null;
		vendorsByNaicsReport = reportDao.getVendorsByNaicsReport(reportForm, appDetails);
		session.setAttribute("vendorsByNaicsReport", vendorsByNaicsReport);
		setPrintReportPdf(vendorsByNaicsReport);

		return mapping.findForward("getVendorsByNaicsReportData");
	}
	
	/**
	 * To Produce PDF Using Jasper for Vendors By NAICS Description Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorsByNaicsReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Produce PDF Using Jasper for Vendors By NAICS Description in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorsByNaicsDescriptionReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("vendors_by_naics_description_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
	
	/**
	 * To View Vendors By BP Market Sector Report Page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewVendorsByBPMarketSectorReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To View Vendors By BP Market Sector Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
		List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
		reportForm.setVendorStatusMaster(vendorStatusMaster);		
		
		return mapping.findForward("showVendorsByBPMarketSectorReport");
	}
	
	public ActionForward viewVendorsByBPMarketSubsectorReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To View Vendors By BP Market Subsector Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
		List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
		reportForm.setVendorStatusMaster(vendorStatusMaster);		
		
		return mapping.findForward("showVendorsByBPMarketSubsectorReport");
	}
	
	public ActionForward viewVendorsByBPCommodityGroupReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To View Vendors By BP Commodity Group Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
		List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
		reportForm.setVendorStatusMaster(vendorStatusMaster);		
		
		return mapping.findForward("showVendorsByBPCommodityGroupReport");
	}
	
	/**
	 * To Retrieve Vendors By BP Market Sector Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorsByBPMarketSectorReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Retrieve Data for Vendors By BP Market Sector Report in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		
		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorsByBPMarketSector = null;
		vendorsByBPMarketSector = reportDao.getVendorsByBPMarketSectorReport(reportForm, appDetails);
		session.setAttribute("vendorsByBPMarketSector", vendorsByBPMarketSector);
		setPrintReportPdf(vendorsByBPMarketSector);

		return mapping.findForward("getVendorsByBPMarketSectorReportData");
	}
	
	public ActionForward getVendorsByBPMarketSubsectorReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Retrieve Data for Vendors By BP Market Sector Report in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		
		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorsByBPMarketSector = null;
		vendorsByBPMarketSector = reportDao.getVendorsByBPMarketSubsectorReport(reportForm, appDetails);
		session.setAttribute("vendorsByBPMarketSector", vendorsByBPMarketSector);
		setPrintReportPdf(vendorsByBPMarketSector);

		return mapping.findForward("getVendorsByBPMarketSubsectorReportData");
	}
	
	public ActionForward getVendorsByBPCommodityGroupReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Retrieve Data for Vendors By BP Market Sector Report in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		
		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> vendorsByBPCommodityGroup = null;
		vendorsByBPCommodityGroup = reportDao.getVendorsByBPCommodityGroupReport(reportForm, appDetails);
		session.setAttribute("vendorsByBPCommodityGroup", vendorsByBPCommodityGroup);
		setPrintReportPdf(vendorsByBPCommodityGroup);

		return mapping.findForward("getVendorsByBPCommodityGroupReportData");
	}

	/**
	 * To Produce PDF Using Jasper for Vendors By BP Market Sector Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorsByBPMarketSectorReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Produce PDF Using Jasper for Vendors By BP Market Sector in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorsByBPMarketSectorReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("vendors_by_bp_market_sector_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorsByBPMarketSubsectorReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Produce PDF Using Jasper for Vendors By BP Market Sector in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorsByBPMarketSubsectorReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("vendors_by_bp_market_subsector_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getVendorsByBPCommodityGroupReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Produce PDF Using Jasper for Vendors By BP Market Sector in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorsByBPCommodityGroupReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());

			InputStream is = JRLoader.getResourceInputStream("vendors_by_bp_commodity_group_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
	
	/**
	 * To recall previous Quarter Details.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public ActionForward recallPrviousQuarter(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorContact vendorUser = (VendorContact) session
				.getAttribute("vendorUser");
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		ReportDao reportDao = new ReportDaoImpl();
		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
		String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
				+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
				+ vendorUser.getVendorId().getId();
		@SuppressWarnings("unchecked")
		List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao
				.findAllByQuery(appDetails, query);
		reportForm.setReportDtos(tier2Vendors);
		reportForm.setIndirectReportDtos(tier2Vendors);
		SearchDao searchDao = new SearchDaoImpl();
		CertificateDao certificateDao = new CertificateDaoImpl();
		List<Certificate> certificates = certificateDao
				.listOfCertificatesByType((byte) 1, appDetails);
		reportForm.setCertificates(certificates);
		List<CertifyingAgency> certificateAgencies;
		List<Tier2ReportMaster> reportMasters;
		certificateAgencies = searchDao.listAgencies(appDetails);
		reportMasters = reportDao.getReportMasterList(appDetails, vendorUser);
		session.setAttribute("reportMasters", reportMasters);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));
		
		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorList = marketSectorDao.list(appDetails,"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorList", marketSectorList);

		WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();

		WorkflowConfiguration workflowConfiguration = configuration
				.listWorkflowConfig(appDetails);

		if (workflowConfiguration != null && workflowConfiguration.getEmailDistributionMasterId() != null) {
			if(workflowConfiguration.getEmailDistributionMasterId().getEmailDistributionListName() != null) {
				reportForm.setGroupName(workflowConfiguration.getEmailDistributionMasterId().getEmailDistributionListName());
			}			
		}

		Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
		Tier2ReportingPeriod reportingPeriod = periodDao
				.currentReportingPeriod(appDetails);

		CURDDao<Ethnicity> dao = new EthnicityDaoImpl<Ethnicity>();
		reportForm.setEthnicities(dao.list(appDetails, null));
		reportForm.setMarketSectorList(marketSectorList);
		Integer vendorId = vendorUser.getVendorId().getId();
		Tier2ReportMaster previousQuarter = reportDao
				.getPreviousQuarterDetails(appDetails, vendorId);
		ArrayList<String> reportPeriod = null;
		if (previousQuarter != null) {
			Integer reportmasterId = previousQuarter.getTier2ReportMasterid();
			Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(
					reportmasterId, appDetails);
			int year = reportMaster.getReportingStartDate().getYear() + 1900;
			int reportStartMonth = 0;
			int reportEndMonth = 3 - 1;
			if (reportingPeriod != null) {
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();
			}

			reportForm.setSpendYear(year);
			reportForm.setStartDate(String.valueOf(reportStartMonth));
			reportForm.setEndDate(String.valueOf(reportEndMonth));

			reportPeriod = (ArrayList<String>) CommonUtils
					.calculateReportingPeriods(reportingPeriod, year);
			List<Tier2ReportDirectExpenses> directExpenses = reportMaster
					.getTier2ReportDirectExpensesList();

			List<Tier2ReportIndirectExpenses> indirectExpenses = reportMaster
					.getTier2ReportIndirectExpensesList();

			if (null != reportMaster) {
				reportForm.setTier2ReportMasterId(reportMaster
						.getTier2ReportMasterid());
				reportForm
						.setReportingPeriod(reportMaster.getReportingPeriod());
				reportForm.setCurrentReportingPeriod(reportMaster
						.getReportingPeriod());
				NumberFormat formatter = NumberFormat.getInstance();
				formatter.setGroupingUsed(false);
				reportForm.setTotalUsSalesSelectedQtr(String.valueOf(formatter
						.format(reportMaster.getTotalSales())));
				reportForm.setTotalUsSalestoSelectedQtr(String
						.valueOf(formatter.format(reportMaster
								.getTotalSalesToCompany())));
				reportForm.setIndirectPer(reportMaster
						.getIndirectAllocationPercentage().toString());
				reportForm.setComments(reportMaster.getComments());
				reportMaster.getReportingStartDate().getYear();
				reportPeriod.add(reportMaster.getReportingPeriod());

				Set<String> repPeriod = new LinkedHashSet<String>(reportPeriod);
				reportPeriod = null;
				reportPeriod = new ArrayList<String>(repPeriod);

			}
			if (directExpenses != null && directExpenses.size() != 0) {
				session.setAttribute("directExpensesSession", directExpenses);
			}
			if (indirectExpenses != null && indirectExpenses.size() != 0) {
				session.setAttribute("indirectExpensesSession",
						indirectExpenses);
			}
		}
		reportForm.setReportPeriods(reportPeriod);
		reportForm.setPreviousQuarterStatus(1);
		return mapping.findForward("retriveTier2Report");
	}

	/**
	 * showDynamicColumnSet
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showDynamicColumnSet(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		ReportDao reportDao = new ReportDaoImpl();

		List<ReportSearchFieldsDto> searchFeildsList = reportDao
				.getReportSearchFields(userDetails);
		session.setAttribute("searchFeildsList", searchFeildsList);

		return mapping.findForward("showDynamicColumnSet");
	}

	/**
	 * saveCustomColumnsForSearch
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward saveCustomColumnsForSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		String columnIds[] = request.getParameter("columnIds").split(",");
		
		CustomReportSearchFields searchFields = null;
		ReportDao reportDao = new ReportDaoImpl();
		
		StringBuilder selectQuery = new StringBuilder();
		StringBuilder fromQuery = new StringBuilder();
		StringBuilder searchedFields = new StringBuilder();
		
		List<String> tablesForCondition=(List<String>) session.getAttribute("whereConditionTableList");
		List<String> whereConditionList=new ArrayList<String>();
		SearchReportDto serachReportDto = new SearchReportDto();
		List <String> columnHeader=serachReportDto.getColumnHeader();
		
		fromQuery.append(" From customer_vendormaster ");
		
		//Its needed only when International Mode = 0, with default country selected.
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null) {
				if(userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
					String addressMasterQuery = "Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID";
					if(!(tablesForCondition.contains(addressMasterQuery)))
						tablesForCondition.add(addressMasterQuery);					
				}					
			}
		}		
		
		if(tablesForCondition != null && tablesForCondition.size() > 0){
			for(String condition:tablesForCondition){
				if(!(fromQuery.toString().contains(condition.toString())))
					fromQuery.append(condition.toString()+" ");
			}
		}
		
		for (int i = 0; i < columnIds.length; i++) {
			
			searchFields = reportDao.getSearchFields(userDetails,Integer.parseInt(columnIds[i]));
			
			columnHeader.add(searchFields.getCategory()+"_"+searchFields.getInputColumnName());
			
			searchedFields.append(searchFields.getCategory()+"_"+searchFields.getInputColumnName());
			selectQuery.append(searchFields.getOutputSql());
			
			if(searchFields.getWhereCondition() != null )
			{
				if(!(tablesForCondition.contains(searchFields.getWhereCondition().trim()))){
					if(!(whereConditionList.contains(searchFields.getWhereCondition().trim()))){
						if(!(fromQuery.toString().contains(searchFields.getWhereCondition())))
						{
							fromQuery.append(searchFields.getWhereCondition() +" ");
							whereConditionList.add(searchFields.getWhereCondition().trim());
						}
					}
				}
			}
			
			
			
			if(searchFields.getWhereCondition2() != null){
				if(!(tablesForCondition.contains(searchFields.getWhereCondition2().trim()))){
					if(!(fromQuery.toString().contains(searchFields.getWhereCondition2())))
					{
						if(!(whereConditionList.contains(searchFields.getWhereCondition2().trim()))){
							fromQuery.append(searchFields.getWhereCondition2() +" ");
						}
					}
				}
			}
			
			if(searchFields.getWhereCondition3() != null){
				if(!(tablesForCondition.contains(searchFields.getWhereCondition3().trim()))){
					if(!(fromQuery.toString().contains(searchFields.getWhereCondition3())))
					{
						if(!(whereConditionList.contains(searchFields.getWhereCondition3().trim()))){
							fromQuery.append(searchFields.getWhereCondition3() +" ");
						}
					}
				}
			}
			
			if(columnIds.length > 1 && i <= columnIds.length-2){
				selectQuery.append(", ");
				searchedFields.append(", ");
			}
			
		}
		
		if(session.getAttribute("searchConditions") != null){
			fromQuery.append(" WHERE 1=1 ");
			fromQuery.append(session.getAttribute("searchConditions").toString());
		}
			
		StringBuilder searchQuery = new StringBuilder();
		searchQuery.append("SELECT DISTINCT ");
		searchQuery.append(selectQuery.toString());
		searchQuery.append( fromQuery.toString());
	    reportDao.getSearchReportByDynamicColumns(userDetails,serachReportDto,searchQuery.toString());
	    String jsonData=getJsonData(serachReportDto);
        response.getWriter().write(jsonData);
        setSupplierCustomColSearchReport(serachReportDto);
        
        logger.info("searchQuery--->"+searchQuery);
        logger.info("jsonData--->"+jsonData);
        session.setAttribute("searchedFields",searchedFields.toString());
		session.setAttribute("dynamicSearchQuery",searchQuery.toString());
		session.setAttribute("dynamicColumnHeder", serachReportDto.getColumnHeader());
		
	    return null;
	}
	
	
	private String getJsonData(SearchReportDto searchReportDto) {
		JSONArray colNamesArray = getColNamesArray(searchReportDto.getColumnHeader());
		JSONArray colModelArray = getcolModelArray(searchReportDto.getColumnHeader());
		JSONArray colDataArray = getColDataArray(searchReportDto.getColumnDetails());
		String json = colNamesArray.toString()+"|"+colModelArray+"|"+colDataArray;
		return json;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JSONArray getColDataArray(LinkedHashMap<String, List<Object>> columnDetails) {
		JSONArray colDataArray = new JSONArray();
		JSONObject jsonObject = null;
		int totalRec = 0;
		
		Iterator it = columnDetails.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        totalRec = ((List<Object>)pairs.getValue()).size();
	    }
		
	    for(int rowNumber=0; rowNumber<totalRec; rowNumber++){
	    	jsonObject = new JSONObject();
			for(String columnHeading : columnDetails.keySet()){
				jsonObject.put(columnHeading, 
						((List<Object>)columnDetails.get(columnHeading)).get(rowNumber) !=null ? ((List<Object>)columnDetails.get(columnHeading)).get(rowNumber).toString().replace("|", " "):null);
			}
			colDataArray.add(jsonObject);
	    }
		return colDataArray;
	}
	
	private JSONArray getcolModelArray(List<String> columnHeaderList) {
		JSONArray colModelArray = new JSONArray();
		JSONObject colModelObj = null;
		for(String columnHeader : columnHeaderList){
			colModelObj = new JSONObject();
			colModelObj.put("index", columnHeader);
			colModelObj.put("name", columnHeader);
			colModelArray.add(colModelObj);
		}
		return colModelArray;
	}

	private JSONArray getColNamesArray(List<String> columnHeader) {
		JSONArray colNamesArray = new JSONArray();
		for(String header : columnHeader ){
			colNamesArray.add(header);
		}
		return colNamesArray;
	}
	
	/**
	 * saveSearchedData
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveSearchedData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		
		VendorDao vendorDao = new VendorDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto userDetails= (UserDetailsDto) session.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		
		String searchName=request.getParameter("searchName").toString();
		String searchType=request.getParameter("searchType").toString();
		String result;
		
		CustomerSearchFilter searchFilter=vendorDao.findByFilterName(userDetails,searchName,searchType,userId);
		if(searchFilter != null){
			result="unique";
		}
		else{
			String searchedFields=session.getAttribute("searchedFields").toString();
			String sqlQuery=session.getAttribute("dynamicSearchQuery").toString();
			searchFilter=vendorDao.saveSearchFilters(userDetails,searchName,searchType,searchedFields, sqlQuery, userId);
			
			if(searchFilter != null)
				result="success";
			else 
				result="fail";
		}
		JSONObject jsonObject = new JSONObject();
     	jsonObject.put("result", result);
     	response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
        
		 return null;
	}
	
	
	/**
	 * searchByPreviousSavedColumns
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchByPreviousSavedColumns(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails= (UserDetailsDto) session.getAttribute("userDetails");
		String searchId= request.getParameter("searchId");
		
		SearchDao searchDao = new SearchDaoImpl();
		SearchReportDto serachReportDto = new SearchReportDto();
		ReportDao reportDao = new ReportDaoImpl();
		
			CustomerSearchFilter customerSearchFilter=searchDao.getCustomerSearchFilter(userDetails,Integer.parseInt(searchId));
			List <String> columnHeader=serachReportDto.getColumnHeader();
			columnHeader.addAll((Arrays.asList(customerSearchFilter.getCriteria().split(","))));
		
		 reportDao.getSearchReportByDynamicColumns(userDetails,serachReportDto,customerSearchFilter.getSearchQuery());
		 setSupplierCustomColSearchReport(serachReportDto);
		 String jsonData=getJsonData(serachReportDto);
		 response.getWriter().write(jsonData);
		 session.setAttribute("dynamicColumnHeder", columnHeader);

	return null;
		  
	}
	/**
	 * getDynamicColumnHeader
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward getDynamicColumnHeader(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		String jsonData="";
		List<String> dynamicColumnHeder=(List<String>) session.getAttribute("dynamicColumnHeder");
		for(String header : dynamicColumnHeder){
			jsonData=jsonData+header+",";
		}
		 response.getWriter().write(jsonData);
		 return null;
	}
	
	/**
	 * To Get the Search Vendor Report for Jasper Export.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unused", "unchecked", "rawtypes" })
	public ActionForward getCustomSearchVendorReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String colHeader = request.getParameter("colHeader");
		// For Setting Selected Header Names to True
		SearchVendorDetailsDto headerParam = null;
		headerParam = setSelectedHeader(colHeader);

		String columnHeader[] = colHeader.split(",");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		
		OutputStream servletOutputStream = null;
		try {
			String reportName = "SupplierReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();
			Page page = new Page(1000, 1500, true);
			dynamicReportBuilder.setPageSizeAndOrientation(page);
			String imagePath = Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath();
			dynamicReportBuilder.setUseFullPageWidth(true)
					.addFirstPageImageBanner(imagePath, 60, 50,
							ImageBanner.ALIGN_LEFT);

			// dynamicReportBuilder.setPageSizeAndOrientation(Page.Page_Letter_Landscape());

			// Style for title
			Style TitleStyle = new StyleBuilder(true).setFont(
					new Font(12, Font._FONT_VERDANA, true, false, false))
					.build();
			TitleStyle.setHorizontalAlign(HorizontalAlign.CENTER);
			// dynamicReportBuilder.addAutoText("Supplier Custom Search Report",
			// AutoText.POSITION_HEADER,
			// AutoText.ALIGNMENT_CENTER,200,atStyle2);
			dynamicReportBuilder.setTitle("Supplier Custom Search Report");
			dynamicReportBuilder.setTitleStyle(TitleStyle);

			// dynamicReportBuilder.setUseFullPageWidth(true);

			// Style for column header
			Style headerStyle = new Style();
			headerStyle.setFont(Font.VERDANA_MEDIUM_BOLD);
			headerStyle.setHorizontalAlign(HorizontalAlign.LEFT);

			// Style for column header
			Style ColumnBodyStyle = new Style();
			ColumnBodyStyle.setFont(Font.VERDANA_MEDIUM);
			ColumnBodyStyle.setHorizontalAlign(HorizontalAlign.LEFT);
			ColumnBodyStyle.setVerticalAlign(VerticalAlign.MIDDLE);
			
			
			LinkedHashMap<String, List<Object>> columnDetails=getSupplierCustomColSearchReport().getColumnDetails();
			List<String> columnTitle=getSupplierCustomColSearchReport().getColumnHeader();
			
			int totalRec = 0;
			
			Iterator it = columnDetails.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        totalRec = ((List<Object>)pairs.getValue()).size();
		    }
			
//			System.out.println("totalRec: "+totalRec);
		    List data = new ArrayList();

		    for(int rowNumber=0; rowNumber<totalRec; rowNumber++){
		    	
		    	 Map<String, String> map1 = new HashMap<String, String>();
				for(String columnHeading : columnDetails.keySet()){
					String colBeanName=columnHeading.replaceAll("\\s", "");
					map1.put(colBeanName, 
							columnDetails.get(columnHeading).get(rowNumber) != null ? columnDetails.get(columnHeading).get(rowNumber).toString(): " " );
					
				}
				data.add(map1);
		    }
			
		  int coulumnWidth = Math.round(1425 / (columnHeader.length));

		  //Create Datasource and put it in Dynamic Jasper Format
		  
		  for(int i = 0; i < columnHeader.length; i++){
		
		   for(String colHead:columnTitle)
		   {
			   if(columnHeader[i].equalsIgnoreCase(colHead)){
				   
			   String colBeanName=colHead.replaceAll("\\s", "");
			   ColumnBuilder columnVendorName = ColumnBuilder.getNew();

				columnVendorName.setTitle(colHead);
				columnVendorName.setHeaderStyle(headerStyle);
				columnVendorName.setWidth(coulumnWidth);
				columnVendorName.setFixedWidth(true);
				columnVendorName.setColumnProperty(colBeanName,
						String.class.getName(), colBeanName);
				columnVendorName.setStyle(ColumnBodyStyle);

				dynamicReportBuilder.addColumn(columnVendorName.build());
			   }
		   }
		    
		  }
			DynamicReport dynamicReport = dynamicReportBuilder.build();

			// build a datasource representing the XML file

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(data);

			// build JasperPrint instance, filling the report with data from
			// datasource created above
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
										dynamicReport, new ClassicLayoutManager(),
											beanCollectionDataSource);

			// export to pdf
			// String pdfFile = Math.round(Math.random() * 100000) + ".pdf";

			JRExporter exporter = new JRPdfExporter();

			servletOutputStream = httpServletResponse.getOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					servletOutputStream);

			exporter.exportReport();
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (JRException e) {
			e.printStackTrace();
		}

		return mapping.findForward("showsearchvendor");
	}

	public SearchReportDto getSupplierCustomColSearchReport() {
		return supplierCustomColSearchReport;
	}

	public void setSupplierCustomColSearchReport(
			SearchReportDto supplierCustomColSearchReport) {
		this.supplierCustomColSearchReport = supplierCustomColSearchReport;
	}
	
	/**
	 * To Saving the Excel File data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation", "unused" })
	public ActionForward tier2ReportExcelFileUpload(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Tier2ReportForm reportForm=(Tier2ReportForm)form;
		String result="success";
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorContact currentVendorUser = (VendorContact) session.getAttribute("vendorUser");
		
		Integer userId = (Integer) session.getAttribute("userId");
		Iterator iterator;
		String errorMessage=null;
		String warringMessage=null;
		try
		{
			FormFile formFile=reportForm.getImportExcelFile();

			org.apache.poi.ss.usermodel.Workbook workbook = WorkbookFactory.create(formFile.getInputStream());
		
			//Get the number of sheets in the xlsx file
			/*int numberOfSheets = workbook.getNumberOfSheets();*/
			List tier2SpendList=new ArrayList();
			List indirectList=Collections.synchronizedList( new ArrayList());
			List directList=Collections.synchronizedList( new ArrayList());
			        
			Map<Object,Object> indirectMap=new LinkedHashMap<Object, Object>();
			Map<Object,Object> directMap=new LinkedHashMap<Object, Object>();

			String fetchDataFrom=null;
			int fetchData=0;
			int directColumnsReading=0;
			int indirectColumnsReading=0;
			boolean sectionOne=true;
			String sectionOneStart="no";
			int indirectRowNumber=0;
			int directRowNumber=0;			
			IndirectExcelColumn indirectExcelObj=null;
			DirectExcelColumn directExcelObj=null;
			Tier2SpendReportExcelColumn tier2SpendReportObj=null;
			String[] divCertificates=null;
			Tier2VendorDao tier2VendorDao = new Tier2VendorDaoImpl();
			CommonDao commonDao = new CommonDaoImpl();
        
			/*---------------------------------- Start Getting the Applicable Certificates & Ethncity  for this Vendor--------------------------------------*/
			List certificates = new CopyOnWriteArrayList();
			List certificatesList =new ArrayList();
			CertificateDaoImpl certificate = new CertificateDaoImpl();
			Certificate cert=null;
        
			certificates = certificate.listOfCertificatesByType((byte) 1, appDetails);
			iterator=certificates.iterator();
			while(iterator.hasNext())
			{
				cert=(Certificate)iterator.next();
				certificatesList.add(cert.getCertificateName().toUpperCase());
			}
        
			CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
			List ethnicities=ethnicityDao.list(appDetails, null);
			Ethnicity ethn=new Ethnicity();
			iterator=ethnicities.iterator();
			while(iterator.hasNext())
			{
				ethn=(Ethnicity)iterator.next();
				certificatesList.add(ethn.getEthnicity().toUpperCase());
			}		
			/*---------------------------------- End Getting the Applicable Certificates & Ethnicity for this Vendor--------------------------------------*/
        
			//loop through each of the sheets
			for(int i=0; i < 1; i++) 
			{
				//Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);
            
				//every sheet has rows, iterate over them
				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) 
				{
					//Get the row object
					Row row = rowIterator.next();
                
					indirectExcelObj=new IndirectExcelColumn();
					directExcelObj=new DirectExcelColumn(); 
					tier2SpendReportObj=new Tier2SpendReportExcelColumn();
                
					//Every row has columns, get the column iterator and iterate over them
					Iterator<Cell> cellIterator = row.cellIterator();
					while (cellIterator.hasNext()) 
					{
						//Get the Cell object
						Cell cell = cellIterator.next();
                    
						if((fetchDataFrom==null && fetchData==0) || (fetchDataFrom.equalsIgnoreCase("indirect") && fetchData==1))
						{
		                    switch(cell.getCellType())
		                    {
		                    	case Cell.CELL_TYPE_STRING:
		                    		if(cell.getStringCellValue().trim().equalsIgnoreCase("Tier2 Legal Company Name") && fetchDataFrom==null)
		                    		{
		                    			fetchDataFrom="indirect";
		                    			fetchData=fetchData+1;
		                    		}
		                    		else if(cell.getStringCellValue().trim().equalsIgnoreCase("Tier2 Legal Company Name") && fetchDataFrom!=null && fetchDataFrom.equalsIgnoreCase("indirect"))
		                    		{
		                    			fetchDataFrom="direct";
		                    			fetchData=0;
		                    		}
		                    		else if(cell.getStringCellValue().trim().equalsIgnoreCase("Period"))
		                    		{
		                    			sectionOneStart="yes";
		                    			if(fetchDataFrom==null && sectionOne) 
		                    			{
		                    				sectionOne=false;
		                    			}
		                    		}else if(fetchDataFrom !=null && cell.getStringCellValue().trim().equalsIgnoreCase("Direct Expenses")){
		                    			sectionOneStart = "no";
		                    			fetchDataFrom = "Direct Expenses";
		                    		}
		                    		break;
	                    	}
						}
                    
						if(fetchDataFrom!=null && fetchDataFrom.equalsIgnoreCase("indirect"))
						{
							switch(cell.getCellType())
							{
								case Cell.CELL_TYPE_STRING:
									//random data, leave it
									if(cell.getColumnIndex()<=11 && indirectColumnsReading<=11)
									{
										indirectColumnsReading=indirectColumnsReading+1;
									}
									else
										indirectList.add(cell.getStringCellValue());
									break;
								
								case Cell.CELL_TYPE_NUMERIC:
									if(cell.getColumnIndex()==4)
									{
										Integer integer=new Integer((int) cell.getNumericCellValue());
										indirectList.add(integer);
									}
									else if(cell.getColumnIndex()==8 )
									{
										Long l=new Long((long)cell.getNumericCellValue());
										indirectList.add(l);
									}
									else
										indirectList.add(cell.getNumericCellValue());
									break;
							}
						}
						else if(fetchDataFrom!=null && fetchDataFrom.equalsIgnoreCase("direct"))
						{
							switch(cell.getCellType())
							{
								case Cell.CELL_TYPE_STRING:
									//random data, leave it;
									if(cell.getColumnIndex()<=11 && directColumnsReading<=11)
									{
										directColumnsReading=directColumnsReading+1;
									}
									else
										directList.add(cell.getStringCellValue());
									break;
								
								case Cell.CELL_TYPE_NUMERIC:
									if(cell.getColumnIndex()==4 )
									{
										Integer integer=new Integer((int) cell.getNumericCellValue());
										directList.add(integer);
									}
									else if(cell.getColumnIndex()==8)
									{
										Long l=new Long((long)cell.getNumericCellValue());
										directList.add(l);
									}
									else
										directList.add(cell.getNumericCellValue());
									break;
							}
						}
						else
						{
							if(sectionOneStart.equalsIgnoreCase("no"))
							{
								switch(cell.getCellType())
								{
									case Cell.CELL_TYPE_STRING:
										if(cell.getColumnIndex()!=0)
											tier2SpendList.add(cell.getStringCellValue());
										break;
		                    
									case Cell.CELL_TYPE_NUMERIC:
										if(row.getRowNum()==0){
											Integer integer=new Integer((int)cell.getNumericCellValue());
											tier2SpendList.add(integer);
										}
										else
											tier2SpendList.add(cell.getNumericCellValue());
										break;
								}
							}
							else
							{
								// For Period Start & End Dates Getting from Excel sheet
								switch(cell.getCellType())
								{
									case Cell.CELL_TYPE_STRING:
										 if(!cell.getStringCellValue().trim().equalsIgnoreCase("Period"))
											 errorMessage="Check the dates given in period (row:"+(row.getRowNum()+1)+")";
										break;
    	                    
									case Cell.CELL_TYPE_NUMERIC:
										tier2SpendList.add(cell.getDateCellValue());
										if(cell.getColumnIndex()>1)
											sectionOneStart="no";
										break;
								}
							}
						}
						if(errorMessage!=null)
							break;
					} //end of cell iterator
					
					if(errorMessage!=null)
						break;
					String query=null;
					Object id=null;
					
					if(indirectList!=null && indirectList.size()<12 && indirectList.size()>0)
					{
						errorMessage="Some required fields are empty (row:"+(row.getRowNum()+1)+")";
					}
					
					if(indirectList!=null && indirectList.size()>0 && indirectColumnsReading>11 && errorMessage==null)
					{
	                	indirectExcelObj.setIndirectLegalCompanyName(indirectList.get(0).toString().trim());
	                	indirectExcelObj.setIndirectAddress(indirectList.get(1).toString().trim());
	                	indirectExcelObj.setIndirectCity(indirectList.get(2).toString().trim());
	                	query="From State where statename='"+indirectList.get(3).toString().trim()+"'";
	                	id=tier2VendorDao.idFromName(appDetails, query);
	                	if(id!=null)
	                		indirectExcelObj.setIndirectState(indirectList.get(3).toString().trim());
	                	else
	                		errorMessage="Given state ("+indirectList.get(3).toString()+") is not belongs to United States (row:"+(row.getRowNum()+1)+")";
	                	warringMessage="Zipcode should be in numeric (row:"+(row.getRowNum()+1)+")";
	                	indirectExcelObj.setIndirectZipcode((Integer)indirectList.get(4));
	                	indirectExcelObj.setIndirectFirstName(indirectList.get(5).toString().trim());
	                	indirectExcelObj.setIndirectLastName(indirectList.get(6).toString().trim());
	                	
	                	try
	                	{
	                		InternetAddress emailAddr = new InternetAddress(indirectList.get(7).toString());
	                		emailAddr.validate();
	                		String emailExist=commonDao.checkDuplicateEmail(indirectList.get(7).toString(), appDetails);
	                		if (emailExist.equalsIgnoreCase("submit")) 
		                	   indirectExcelObj.setIndirectEmail(indirectList.get(7).toString());
	                		else
	                			errorMessage="Given email "+indirectList.get(7).toString()+" is already exists (row:"+(row.getRowNum()+1)+")";
	                	}
	                	catch (AddressException ex) 
	                	{
	                		errorMessage="Given email "+indirectList.get(7).toString()+" is invalid format (row:"+(row.getRowNum()+1)+")";
						}
	                	
	                	if(indirectList.get(8).toString().trim().matches("[0-9]+")){
	                	if(indirectList.get(8).toString().trim().length()==10){
	                		warringMessage="Phone number should be in numeric (row:"+(row.getRowNum()+1)+")";
	                		indirectExcelObj.setIndirectPhone((Long)indirectList.get(8));
	                	}
	                	else
	                		errorMessage="Phone number must be 10 digits (row:"+(row.getRowNum()+1)+")";
	                	}
	                // Checking the Given Diversity Classification is Applicable for this Vendor.
	                	if(indirectList.get(9)!=null)
	                	{
	                		if(certificatesList.contains(indirectList.get(9).toString().toUpperCase()))
	                		{
	                			indirectExcelObj.setDiverstiyClassification(indirectList.get(9).toString());
	                			indirectExcelObj.setIndirectDiversity(indirectList.get(9).toString().split(","));
	                		}
	                		else
	                			errorMessage="Diversity name "+indirectList.get(9).toString()+" is invalid for this Vendor (row:"+(row.getRowNum()+1)+")";
	                		indirectExcelObj.setDiverseSupplier("on");
	                	}
	                	else
	                	{
	                		indirectExcelObj.setDiverstiyClassification("");
	                		indirectExcelObj.setIndirectDiversity(divCertificates);
	                		indirectExcelObj.setDiverseSupplier("on");
	                	}
	                	// Checking the Given Ethnicity  is Applicable for this Vendor.
	                	if(indirectList.get(10)!=null)
	                	{
	                		if(certificatesList.contains(indirectList.get(10).toString().toUpperCase()))
	                			indirectExcelObj.setIndirectEthanicity(indirectList.get(10).toString());
	                		else
	                			errorMessage="Ethnicity name "+indirectList.get(10).toString()+" is invalid for this vendor (row:"+(row.getRowNum()+1)+")";
	                	}
	                	else
	                		indirectExcelObj.setIndirectEthanicity("");
	                	
	                	warringMessage="Indirect amount should be in numeric (row:"+(row.getRowNum()+1)+")";
	                	indirectExcelObj.setIndirectAmount(Double.parseDouble(indirectList.get(11).toString()));
	                	warringMessage=null;
	                	indirectExcelObj.setCountry("United States"); 
                	
	                	indirectMap.put(indirectRowNumber, indirectExcelObj);
	                	indirectRowNumber=indirectRowNumber+1;
					}
					
					if(directList!=null && directList.size()<11 && directList.size()>0)
					{
						errorMessage="Some required fields are empty (row:"+(row.getRowNum()+1)+")";
					}
					
					if(directList!=null && directList.size()>0 && directColumnsReading>10 && errorMessage==null)
					{
	                	directExcelObj.setLegalCompanyName(directList.get(0).toString());
	                	directExcelObj.setAddress(directList.get(1).toString());
	                	directExcelObj.setCity(directList.get(2).toString());
	                	query="From State where statename='"+directList.get(3).toString()+"'";
	                	id=tier2VendorDao.idFromName(appDetails, query);
	                	if(id!=null)
	                		directExcelObj.setState(directList.get(3).toString());
	                	else
	                		errorMessage="Given state ("+directList.get(3).toString()+") is not belongs to United States (row:"+(row.getRowNum()+1)+")";
	                	
	                	warringMessage="Zipcode should be in numeric (row:"+(row.getRowNum()+1)+")";
	                	directExcelObj.setZipcode((Integer) directList.get(4));
	                	directExcelObj.setFirstName(directList.get(5).toString());
	                	directExcelObj.setLastName(directList.get(6).toString());
	                	
	                	try{
	                		InternetAddress emailAddr = new InternetAddress(directList.get(7).toString());
	                		emailAddr.validate();
	                		String emailExist=commonDao.checkDuplicateEmail(directList.get(7).toString(), appDetails);
	                		if (emailExist.equalsIgnoreCase("submit"))
	                			directExcelObj.setEmail(directList.get(7).toString().trim());
	                		else
	                			errorMessage="Given email "+directList.get(7).toString()+" is already exists (row:"+(row.getRowNum()+1)+")";
	                	}
	                	catch (AddressException ex) 
	                	{
	                		errorMessage="Given email "+directList.get(7).toString()+" is invalid format (row:"+(row.getRowNum()+1)+")";
						}
	                	
	                	if(directList.get(8).toString().trim().length()==10){
	                		warringMessage="Phone number should be in numeric (row:"+(row.getRowNum()+1)+")";
	                	  directExcelObj.setPhone((Long)directList.get(8));
	                	}
	                	else
	                		errorMessage="Phone number must have 10 digits (row:"+(row.getRowNum()+1)+")";
	                	
	                	// Checking the Given Diversity Classification is Applicable for this Vendor.
	                	if(directList.get(9)!=null)
	                	{
	                		if(certificatesList.contains(directList.get(9).toString()))
	                		{
	                	       directExcelObj.setDiverstiyClassification(directList.get(9).toString());
	                	       directExcelObj.setDiversity(directList.get(9).toString().split(","));
	                		}
	                		else
	                			errorMessage="Diversity name "+directList.get(9).toString()+" is invalid for this vendor (row:"+(row.getRowNum()+1)+")";
	                		directExcelObj.setDiverseSupplier("on");
	                    }
	                	else
	                	{
	                		directExcelObj.setDiverstiyClassification("");
	                		directExcelObj.setDiversity(divCertificates);
	                		directExcelObj.setDiverseSupplier("on");
	                	}
	                	
	                	if(directList.get(10)!=null)
	                	{
	                		if(certificatesList.contains(directList.get(10).toString().toUpperCase()))
	                			directExcelObj.setDirectEthanicity(directList.get(10).toString());
	                		else
	                			errorMessage="Ethnicity name "+directList.get(10).toString()+" is invalid for this vendor (row:"+(row.getRowNum()+1)+")";
	                	}
	                	else
	                		directExcelObj.setDirectEthanicity("");
	                	
	                	warringMessage="Direct amount should be in numeric (row:"+(row.getRowNum()+1)+")";
	                	directExcelObj.setAmount(Double.parseDouble(directList.get(11).toString()));
	                	warringMessage=null;
	                	directExcelObj.setCountry("United States");
	                	directMap.put(directRowNumber, directExcelObj);
	                	directRowNumber=directRowNumber+1;
					}
					
					if(fetchDataFrom!=null && fetchDataFrom.equalsIgnoreCase("indirect") && indirectColumnsReading>11)
					{
						indirectList.clear();
					}
					else if(fetchDataFrom!=null && fetchDataFrom.equalsIgnoreCase("direct") && directColumnsReading>10)
					{
						directList.clear();
					}else if(fetchDataFrom!=null && fetchDataFrom.equalsIgnoreCase("Direct Expenses")){
						fetchDataFrom="indirect";
					}
				} //end of rows iterator
				if(errorMessage!=null)
					break;
			} //end of sheets for loop
        
			/*---------------------------Start Tier2 Vendor Spend Year Record Saving----------------------*/
			SimpleDateFormat sdf1=new SimpleDateFormat("MMM dd, yyyy");
			ReportDao reportDao = new ReportDaoImpl();
			Tier2ReportMaster master = null;
			VendorMaster vendorMaster=null;
			if(tier2SpendList!=null && tier2SpendList.size()<5 && tier2SpendList.size()>11)
				errorMessage="Some fields are empty or added extra info (row:1 to 4)";
	    
			if(tier2SpendList!=null && tier2SpendList.size()>0 && errorMessage==null)
			{
		    	tier2SpendReportObj.setYear((Integer)tier2SpendList.get(0));
		        tier2SpendReportObj.setPeriodStartDate((java.util.Date)tier2SpendList.get(1));
	        	tier2SpendReportObj.setPeriodEndDate((java.util.Date)tier2SpendList.get(2));
	        	tier2SpendReportObj.setTotalSales((Double)tier2SpendList.get(3));
	        	tier2SpendReportObj.setTotalSales_TO_Company((Double)tier2SpendList.get(4));
	        	
	        	if(tier2SpendReportObj.getTotalSales() > tier2SpendReportObj.getTotalSales_TO_Company())
	        	{
	        		tier2SpendReportObj.setIndirect_Allocate_Per((tier2SpendReportObj.getTotalSales_TO_Company()/tier2SpendReportObj.getTotalSales())*100);
	        	}
	        	else
	        	{
	        		errorMessage="Total sales should be greater than total sales to company (row: 3 & 4)";
	        	    tier2SpendReportObj.setIndirect_Allocate_Per(0.0);
	        	}
	        	
	        	if(tier2SpendList.get(5)!=null )
	        	{
	        		String isInteger="no";
	        		try 
	        		{
	        		    Double.parseDouble(tier2SpendList.get(5).toString());
	        		    isInteger="yes";
	        		} 
	        		catch(NumberFormatException e) 
	        		{
	        			isInteger="no";
	        		}
	        		if(isInteger.equalsIgnoreCase("yes"))
	        		{
			        	if(tier2SpendList.get(6)!=null)
			        		tier2SpendReportObj.setComments(tier2SpendList.get(6).toString());
			        	else
			        		tier2SpendReportObj.setComments("");
	        		}
	        		else
	        			tier2SpendReportObj.setComments(tier2SpendList.get(5).toString());
	        	}
	        	else
	        		tier2SpendReportObj.setComments("");
		    }
			
			if(errorMessage==null){
		    if(tier2SpendReportObj.getYear().intValue()==tier2SpendReportObj.getPeriodStartDate().getYear()+1900 && tier2SpendReportObj.getYear().intValue()==tier2SpendReportObj.getPeriodEndDate().getYear()+1900)
		    {
		    	SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
	 			String inputString1 = myFormat.format(tier2SpendReportObj.getPeriodStartDate());
	 			String inputString2 = myFormat.format(tier2SpendReportObj.getPeriodEndDate());
	
	 			try 
	 			{
	 			    Date date1 = myFormat.parse(inputString1);
	 			    Date date2 = myFormat.parse(inputString2);
	 			    long diff = date2.getTime() - date1.getTime();
	 			    diff=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	 			    if(diff>=89 && diff<=91)
	 		    	{
	 			    	tier2SpendReportObj.setStartDate_EndDate(sdf1.format(tier2SpendReportObj.getPeriodStartDate())
	 			    			+ " - "
	 							+ sdf1.format(tier2SpendReportObj.getPeriodEndDate()));
	 		    	}
	 		    	else
	 		    		errorMessage="Reporting period should be in quarterly or end date > start date (always) (row: 2)";
	 			} 
	 			catch (ParseException e) 
	 			{
	 			    e.printStackTrace();
	 			}
		    }
		    else
		    	errorMessage="Reporting period should be in given year (check row:1 & 2)";
			}
	    
		    if(errorMessage==null && warringMessage==null)
		    {
		    	reportForm.setSpendYear(tier2SpendReportObj.getYear());
				reportForm.setReportingPeriod(tier2SpendReportObj.getStartDate_EndDate());
				reportForm.setTotalUsSalesSelectedQtr(tier2SpendReportObj.getTotalSales().toString());
				reportForm.setTotalUsSalestoSelectedQtr(tier2SpendReportObj.getTotalSales_TO_Company().toString());
				reportForm.setIndirectPer(tier2SpendReportObj.getIndirect_Allocate_Per().toString());
				reportForm.setComments(tier2SpendReportObj.getComments());
		
				String submitType = "save";
		       
				/*---------------------------End Tier2 Vendor Spend Year Record Storing----------------------*/
		        result="tier2mastersuccess";
				master = reportDao.saveTier2ReportMasterOnly(reportForm, appDetails, currentVendorUser, submitType);
				/*Store file pathe for perticular tier2reportmaster*/
				master = reportDao.updateTier2ReportMasterOnly(master, appDetails, formFile);
				/*---------------------------start Indirect Expenditures Iteration of each Record and Saving----------------------*/
		    	if(master!=null)
		    	{
		    		for(int i=0;i<indirectMap.size();i++)
				    {
		    			indirectExcelObj=(IndirectExcelColumn)indirectMap.get(i);
			
		    			if(indirectExcelObj!=null)
						{
		    				vendorMaster = tier2VendorDao.saveExcelTire2Vendor(null, userId, 1, null, null, currentVendorUser, appDetails, null, 
		    								indirectExcelObj.getDiverstiyClassification(), indirectExcelObj.getIndirectAddress(), indirectExcelObj.getIndirectCity(),
		    								indirectExcelObj.getIndirectPhone().toString(), indirectExcelObj.getIndirectEmail(), indirectExcelObj.getDiverseSupplier(),
		    								indirectExcelObj.getIndirectFirstName(), indirectExcelObj.getIndirectLastName(), null, indirectExcelObj.getIndirectState(), indirectExcelObj.getIndirectLegalCompanyName(), 
		    								indirectExcelObj.getIndirectZipcode().toString(), indirectExcelObj.getCountry());
						}
		    			
		    			if(vendorMaster!=null)
		    			{
		    				result="Indirect New Tier2 Vendor Registration Success";
						    String help=null;
						    help= reportDao.saveIndirectExpenditure(indirectExcelObj.getIndirectAmount(), indirectExcelObj.getIndirectEthanicity(), vendorMaster,
						    			indirectExcelObj.getDiverstiyClassification(), master, appDetails, currentVendorUser);
					    	 
						    if(help.equalsIgnoreCase("fail"))
						    	errorMessage=indirectExcelObj.getDiverstiyClassification()+" or "+indirectExcelObj.getIndirectEthanicity()+" is not existed(in DB), ";
					     }
						 else
						 {
							 warringMessage="Indirect expenditures row no:"+(i+1)+" '"+indirectExcelObj.getIndirectEmail().toString()+"' is already existed, or state is not exists ";
					     }			    	 
				    }
			
		    		/*---------------------------start Direct Expenditures Iteration of each Record and Saving----------------------*/
				    for(int i=0 ; i<directMap.size();i++)
				    {
				    	directExcelObj=(DirectExcelColumn)directMap.get(i);
				    	if(directExcelObj!=null )
				    	{
				    		vendorMaster = tier2VendorDao.saveExcelTire2Vendor(null, userId, 1, null, null, currentVendorUser, appDetails, null, 
				    							directExcelObj.getDiverstiyClassification(), directExcelObj.getAddress(), directExcelObj.getCity(), directExcelObj.getPhone().toString(),
				    							directExcelObj.getEmail(), directExcelObj.getDiverseSupplier(), directExcelObj.getFirstName(),directExcelObj.getLastName(),null, 
				    							directExcelObj.getState(), directExcelObj.getLegalCompanyName(), directExcelObj.getZipcode().toString(),directExcelObj.getCountry());
				    	}
				    	
				    	if(vendorMaster!=null)
				    	{
				    		result="Direct New Tier2 Vendor Registration Success";
							String help = reportDao.saveDirectExpenditure(directExcelObj.getAmount(), directExcelObj.getDirectEthanicity(), vendorMaster, directExcelObj.getDiverstiyClassification(), master, appDetails, currentVendorUser);
							if(help.equalsIgnoreCase("fail"))
								errorMessage=directExcelObj.getDiverstiyClassification()+" is existed(in DB), ";
					    }
					    else
					    {
					    	warringMessage="Direct expenditures row no:"+(i+1)+" '"+directExcelObj.getEmail().toString()+"' is already exists, or state is not exists ";
					    }
				    }
				    
				    session.setAttribute("previousExcelRecordTier2Master", master);
		    	}
			    else
			    {
			    	reportForm.reset(mapping, request);
			        errorMessage="Reporting period already exists for this vendor";
		        }
		    }
		    else
		    	result="fail";
		    /*---------------------------End Direct Expenditures Iteration of each Record and Saving----------------------*/
		    if(tier2SpendList==null)
		    {
		    	errorMessage = "Excel file is empty";
		    }		    	
	    
		    if(errorMessage==null && master!=null)
		    {
		    	request.setAttribute("tier2masterid", master.getTier2ReportMasterid());
		    	gettingSessions(mapping,form,request,response);
		    	result="success";
		    }
		}
		catch(Exception exception)
		{
			PrintExceptionInLogFile.printException(exception);
			result="fail";
			if(warringMessage!=null)
				errorMessage=warringMessage;
			else
				errorMessage="Some required fields are empty or Excel template format error";
		}
		
		if(result.equalsIgnoreCase("success"))
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("excel.save.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			if(warringMessage!=null)
			{
				String js="<script>(function(){alert('"+warringMessage+"');})();</script>";
				request.setAttribute("errorAlert", js);
			}
		}
		else
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("excel.fail");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
			String js="<script>(function(){alert('"+errorMessage+"');})();</script>";
			request.setAttribute("errorAlert", js);
		}
		return mapping.findForward("tire2Report");
	}
	
	@SuppressWarnings("unused")
	public ActionForward deleteUnsubmittedRecords(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Tier2ReportMaster tier2ReportMaster=(Tier2ReportMaster)session.getAttribute("previousExcelRecordTier2Master");
		if(tier2ReportMaster!=null) {
			ReportDao reportDao=new ReportDaoImpl();
			String result=reportDao.deleteUnsubmittedRecords(appDetails, tier2ReportMaster.getTier2ReportMasterid());
		}
		return null;
	}	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String gettingSessions(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorContact vendorUser = (VendorContact) session.getAttribute("vendorUser");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		ReportDao reportDao = new ReportDaoImpl();
		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
		String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
				+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
				+ vendorUser.getVendorId().getId();
		List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao.findAllByQuery(appDetails, query);
		reportForm.setReportDtos(tier2Vendors);
		reportForm.setIndirectReportDtos(tier2Vendors);

		SearchDao searchDao = new SearchDaoImpl();
		CertificateDao certificateDao = new CertificateDaoImpl();
		List<Certificate> certificates = certificateDao.listOfCertificatesByType((byte) 1, appDetails);
		reportForm.setCertificates(certificates);
		List<CertifyingAgency> certificateAgencies;		
		List<Tier2ReportMaster> reportMasters;
		certificateAgencies = searchDao.listAgencies(appDetails);
		
		reportMasters = reportDao.getReportMasterList(appDetails, vendorUser);
		session.setAttribute("reportMasters", reportMasters);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);
		
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));
		
		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorList = marketSectorDao.list(appDetails,"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorList", marketSectorList);

		WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();

		WorkflowConfiguration workflowConfiguration = configuration.listWorkflowConfig(appDetails);

		if (workflowConfiguration != null) 
		{
			reportForm.setGroupName(workflowConfiguration.getEmailDistributionMasterId().getEmailDistributionListName());
		}

		Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
		Tier2ReportingPeriod reportingPeriod = periodDao.currentReportingPeriod(appDetails);

		CURDDao<Ethnicity> dao = new EthnicityDaoImpl<Ethnicity>();
		reportForm.setEthnicities(dao.list(appDetails, null));
		reportForm.setMarketSectorList(marketSectorList);

		ArrayList<String> reportPeriod = null;
		if (request.getAttribute("tier2masterid") != null && !request.getAttribute("tier2masterid").toString().isEmpty()) 
		{
			Integer reportmasterId = Integer.parseInt(request.getAttribute("tier2masterid").toString());
			Tier2ReportMaster reportMaster = reportDao.retriveReportmaster(reportmasterId, appDetails);
			
			int year = reportMaster.getReportingStartDate().getYear() + 1900;
			int reportStartMonth = 0;
			int reportEndMonth = 3 - 1;
			if (reportingPeriod != null) 
			{
				reportStartMonth = reportingPeriod.getStartDate().getMonth();
				reportEndMonth = reportingPeriod.getEndDate().getMonth();
			}

			reportForm.setSpendYear(year);
			reportForm.setStartDate(String.valueOf(reportStartMonth));
			reportForm.setEndDate(String.valueOf(reportEndMonth));

			reportPeriod = (ArrayList<String>) CommonUtils.calculateReportingPeriods(reportingPeriod, year);
			
			// Initiate direct expenses details
			List<Tier2ReportDirectExpenses> directExpenses = reportMaster.getTier2ReportDirectExpensesList();

			// Initiate indirect expenses details
			List<Tier2ReportIndirectExpenses> indirectExpenses = reportMaster.getTier2ReportIndirectExpensesList();

			if (null != reportMaster) 
			{
				reportForm.setTier2ReportMasterId(reportMaster.getTier2ReportMasterid());
				reportForm.setReportingPeriod(reportMaster.getReportingPeriod());
				NumberFormat formatter = NumberFormat.getInstance();
				formatter.setGroupingUsed(false);
				
				reportForm.setTotalUsSalesSelectedQtr(String.valueOf(formatter.format(reportMaster.getTotalSales())));
				reportForm.setTotalUsSalestoSelectedQtr(String.valueOf(formatter.format(reportMaster.getTotalSalesToCompany())));
				reportForm.setIndirectPer(reportMaster.getIndirectAllocationPercentage().toString());
				reportForm.setComments(reportMaster.getComments());

				reportMaster.getReportingStartDate().getYear();
				reportPeriod.add(reportMaster.getReportingPeriod());

				Set<String> repPeriod = new LinkedHashSet<String>(reportPeriod);
				reportPeriod = null;
				reportPeriod = new ArrayList<String>(repPeriod);
			}
			
			if (directExpenses != null && directExpenses.size() != 0) 
			{
				session.setAttribute("directExpensesSession", directExpenses);
			}
			if (indirectExpenses != null && indirectExpenses.size() != 0) 
			{
				session.setAttribute("indirectExpensesSession",	indirectExpenses);
			}
		}
		reportForm.setReportPeriods(reportPeriod);
		return null;
	}
	
	public String downloadExcelFormate(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		String documentName="Tier2ReportUploadTemplate.xls";
		// File Location 		C:\vms\data\documents\ExcelTemplate
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=Tier2ReportUploadTemplate.xls");
		response.setHeader("Pragma","no-cache");
		String filePath=Constants.getString(Constants.UPLOAD_EXCELFORMATE_PATH)+"\\Temp";
		// Set content size
		File file = new File(filePath,documentName);
		//--------------------------------------------------------------
		CertificateDao certificateDao = new CertificateDaoImpl();
		List<Certificate> certificates = certificateDao.listOfCertificatesByType((byte) 1, appDetails);
		ArrayList<String> certificatesList=new ArrayList<String>();
		if(certificates != null){
		for(Certificate certificate:certificates){
			String escaped = escapeHtml4(certificate.getCertificateName().toString());
			certificatesList.add(escaped);
		}
		}
		String[] stockArr = new String[certificatesList.size()];
		stockArr = certificatesList.toArray(stockArr);
		
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		List<Ethnicity> ethnicitys=ethnicityDao.list(appDetails, null);
		ArrayList<String> ethnicitysList=new ArrayList<String>();
		if(ethnicitys != null){
		for(Ethnicity ethnicity:ethnicitys){
			String escaped = escapeHtml4(ethnicity.getEthnicity().toString());
			ethnicitysList.add(escaped);
		}
		}
		String[] stockArr1 = new String[ethnicitysList.size()];
		stockArr1 = ethnicitysList.toArray(stockArr1);
		
		FileInputStream fileIn = new FileInputStream(file);
		Workbook workbook = new HSSFWorkbook(fileIn);
		Sheet sheet = workbook.getSheetAt(0);
		
		HSSFSheet hidden = (HSSFSheet) workbook.createSheet("hidden");
		for (int i = 0, length= stockArr.length; i < length; i++) {
			   String name = stockArr[i];
			   HSSFRow row = hidden.createRow(i);
			   HSSFCell cell = row.createCell(0);
			   cell.setCellValue(name);
			 }
		
		Name namedCell = workbook.createName();
		 namedCell.setNameName("hidden");
		 namedCell.setRefersToFormula("hidden!$A$1:$A$" + stockArr.length);
		 DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("hidden");
		CellRangeAddressList addressList = new CellRangeAddressList(11, 19,9,9);
		DataValidation dataValidation = new HSSFDataValidation(addressList,	dvConstraint);
		dataValidation.setSuppressDropDownArrow(false);
		sheet.addValidationData(dataValidation);
		CellRangeAddressList addressList11 = new CellRangeAddressList(22, 35,9,9);
		DataValidation dataValidation11 = new HSSFDataValidation(addressList11,	dvConstraint);
		dataValidation.setSuppressDropDownArrow(false);
		sheet.addValidationData(dataValidation11);
		
		HSSFSheet hidden1 = (HSSFSheet) workbook.createSheet("hidden1");
		for (int i = 0, length= stockArr1.length; i < length; i++) {
			   String name = stockArr1[i];
			   HSSFRow row = hidden1.createRow(i);
			   HSSFCell cell = row.createCell(0);
			   cell.setCellValue(name);
			 }
		 Name namedCell1 = workbook.createName();
		 namedCell1.setNameName("hidden1");
		 namedCell1.setRefersToFormula("hidden1!$A$1:$A$" + stockArr1.length);
		 DVConstraint dvConstraint1 = DVConstraint.createFormulaListConstraint("hidden1");
		CellRangeAddressList addressList1 = new CellRangeAddressList(11, 19,10,10);
		DataValidation dataValidation1 = new HSSFDataValidation(addressList1,	dvConstraint1);
		dataValidation.setSuppressDropDownArrow(false);
		sheet.addValidationData(dataValidation1);
		CellRangeAddressList addressList12 = new CellRangeAddressList(22, 35,10,10);
		DataValidation dataValidation12 = new HSSFDataValidation(addressList12,	dvConstraint1);
		dataValidation.setSuppressDropDownArrow(false);
		sheet.addValidationData(dataValidation12);
		
		DVConstraint dvConstraint2 = DVConstraint.createFormulaListConstraint("hidden");
		CellRangeAddressList addressList2 = new CellRangeAddressList(18, 18,9,9);
		DataValidation dataValidation2 = new HSSFDataValidation(addressList2,	dvConstraint2);
		dataValidation.setSuppressDropDownArrow(false);
		sheet.addValidationData(dataValidation2);
		workbook.setSheetHidden(2, true);
		workbook.setSheetHidden(3, true);
		
		FileOutputStream fileOut = new FileOutputStream(Constants.getString(Constants.UPLOAD_EXCELFORMATE_PATH)+"\\"+documentName);
		System.out.println("Excel is generated");
		try {
		workbook.write(fileOut);
		fileOut.close();
		} catch (IOException e) {
		e.printStackTrace();
		}
		//-------------------------------------------------------------
		
		// response.setContentLength((int) file.length());
		// Open the file and output streams
		File fileNew = new File(Constants.getString(Constants.UPLOAD_EXCELFORMATE_PATH),documentName);
		FileInputStream in = new FileInputStream(fileNew);
		OutputStream out = response.getOutputStream();

		// Copy the contents of the file to the output stream
		int data = 0; 
		while((data = in.read()) != -1) 
        { 
			out.write(data); 
        }
		in.close();
		out.flush();
		out.close();
		return null;
	}
	
	/**
	 * showSpendReportHistory
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward showSpendReportHistory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorContact vendorUser = (VendorContact) session.getAttribute("vendorUser");
		
		ReportDao reportDao = new ReportDaoImpl();
		
		List<Tier2ReportDto> reportHistory = reportDao.getSpendReportHistory(userDetails, vendorUser);
		
		session.setAttribute("tier2ReportHistory", reportHistory);
		
		CURDDao<?> dao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countryList = (List<com.fg.vms.customer.model.Country>) dao
				.list(userDetails, "From Country where isactive=1 and isDefault=1 order by countryname");
		session.setAttribute("countryList", countryList);
		
		Integer countryId = null;		
		countryId = countryList.get(0).getId();		

		List<State> statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
		session.setAttribute("statesList", statesList);

		
		return mapping.findForward("showHistoryPage");
	}
	
	/**
	 * listReportHistory
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward listReportHistory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorContact vendorUser = (VendorContact) session.getAttribute("vendorUser");
		
		ReportDao reportDao = new ReportDaoImpl();
		
		List<Tier2ReportDto> reportHistory = reportDao.getSpendReportHistory(userDetails, vendorUser);
		
		session.setAttribute("tier2ReportHistory", reportHistory);
		
		return mapping.findForward("historyData");
	}
	
	/**
	 * getCustomSearchExcelReport
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	public ActionForward getCustomSearchExcelReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String colHeader = request.getParameter("colHeader");
		// For Setting Selected Header Names to True
		SearchVendorDetailsDto headerParam = null;
		headerParam = setSelectedHeader(colHeader);

		String columnHeader[] = colHeader.split(",");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		
		OutputStream servletOutputStream = null;
		try {
			String reportName = "SupplierReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/vnd.ms-excel");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".xls");
			DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();

			// Style for title
			
			dynamicReportBuilder.setPrintColumnNames(true);
		    dynamicReportBuilder.setIgnorePagination(true); //for Excel, we may dont want pagination, just a plain list
		    dynamicReportBuilder.setMargins(0, 0, 0, 0);
		    dynamicReportBuilder .setUseFullPageWidth(true);
			
			Style TitleStyle = new StyleBuilder(true).setFont(
					new Font(12, Font._FONT_VERDANA, true, false, false))
					.build();
			TitleStyle.setHorizontalAlign(HorizontalAlign.CENTER);

			// Style for column header
			Style headerStyle = new Style();
			headerStyle.setFont(Font.VERDANA_MEDIUM_BOLD);
			headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);

			// Style for column header
			Style ColumnBodyStyle = new Style();
			ColumnBodyStyle.setFont(Font.VERDANA_MEDIUM);
			ColumnBodyStyle.setHorizontalAlign(HorizontalAlign.LEFT);
			ColumnBodyStyle.setVerticalAlign(VerticalAlign.MIDDLE);
			
			
			LinkedHashMap<String, List<Object>> columnDetails=getSupplierCustomColSearchReport().getColumnDetails();
			List<String> columnTitle=getSupplierCustomColSearchReport().getColumnHeader();
			
			int totalRec = 0;
			
			Iterator it = columnDetails.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        totalRec = ((List<Object>)pairs.getValue()).size();
		    }
			
//			System.out.println("totalRec: "+totalRec);
		    List data = new ArrayList();

		    for(int rowNumber=0; rowNumber<totalRec; rowNumber++){
		    	
		    	 Map<String, String> map1 = new HashMap<String, String>();
				for(String columnHeading : columnDetails.keySet()){
					String colBeanName=columnHeading.replaceAll("\\s", "");
					map1.put(colBeanName, 
							columnDetails.get(columnHeading).get(rowNumber) != null ? columnDetails.get(columnHeading).get(rowNumber).toString(): " " );
					
				}
				data.add(map1);
		    }
			
		  //Create Datasource and put it in Dynamic Jasper Format
		  
		  for(int i = 0; i < columnHeader.length; i++){
		
		   for(String colHead:columnTitle)
		   {
			   if(columnHeader[i].equalsIgnoreCase(colHead)){
				   
			   String colBeanName=colHead.replaceAll("\\s", "");
			   ColumnBuilder columnContent = ColumnBuilder.getNew();

			   columnContent.setTitle(colHead);
			   columnContent.setHeaderStyle(headerStyle);
			   columnContent.setWidth((colHead.length())*8);
			   columnContent.setFixedWidth(true);
			   columnContent.setColumnProperty(colBeanName,
						String.class.getName(), colBeanName);

				dynamicReportBuilder.addColumn(columnContent.build());
			   }
		   }
		    
		  }
			DynamicReport dynamicReport = dynamicReportBuilder.build();

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(data);

			// build JasperPrint instance, filling the report with data from
			// datasource created above
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
										dynamicReport, new ClassicLayoutManager(),
											beanCollectionDataSource);

			// export to pdf
			// String pdfFile = Math.round(Math.random() * 100000) + ".pdf";

			JRXlsExporter exporter = new JRXlsExporter();

			servletOutputStream = httpServletResponse.getOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					servletOutputStream);
			
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
	        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
	        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

			exporter.exportReport();
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (JRException e) {
			e.printStackTrace();
		}

		return mapping.findForward("showsearchvendor");
	}
	
	/**
	 * getCustomSearchCSVReport
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public ActionForward getCustomSearchCSVReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String colHeader = request.getParameter("colHeader");
		// For Setting Selected Header Names to True
		SearchVendorDetailsDto headerParam = null;
		headerParam = setSelectedHeader(colHeader);

		String columnHeader[] = colHeader.split(",");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		
		OutputStream servletOutputStream = null;
		try {
			String reportName = "SupplierReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/csv");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".csv");

			DynamicReportBuilder dynamicReportBuilder = new DynamicReportBuilder();

			// Style for title
			
			dynamicReportBuilder.setPrintColumnNames(true);
		    dynamicReportBuilder.setIgnorePagination(true); //for Excel, we may dont want pagination, just a plain list
		    dynamicReportBuilder.setMargins(0, 0, 0, 0);
		    dynamicReportBuilder .setUseFullPageWidth(true);
			
			Style TitleStyle = new StyleBuilder(true).setFont(
					new Font(12, Font._FONT_VERDANA, true, false, false))
					.build();
			TitleStyle.setHorizontalAlign(HorizontalAlign.CENTER);

			// Style for column header
			Style headerStyle = new Style();
			headerStyle.setFont(Font.VERDANA_MEDIUM_BOLD);
			headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);

			// Style for column header
			Style ColumnBodyStyle = new Style();
			ColumnBodyStyle.setFont(Font.VERDANA_MEDIUM);
			ColumnBodyStyle.setHorizontalAlign(HorizontalAlign.LEFT);
			ColumnBodyStyle.setVerticalAlign(VerticalAlign.MIDDLE);
			
			
			LinkedHashMap<String, List<Object>> columnDetails=getSupplierCustomColSearchReport().getColumnDetails();
			List<String> columnTitle=getSupplierCustomColSearchReport().getColumnHeader();
			
			int totalRec = 0;
			
			Iterator it = columnDetails.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        totalRec = ((List<Object>)pairs.getValue()).size();
		    }
			
//			System.out.println("totalRec: "+totalRec);
		    List data = new ArrayList();

		    for(int rowNumber=0; rowNumber<totalRec; rowNumber++){
		    	
		    	 Map<String, String> map1 = new HashMap<String, String>();
				for(String columnHeading : columnDetails.keySet()){
					String colBeanName=columnHeading.replaceAll("\\s", "");
					map1.put(colBeanName, 
							columnDetails.get(columnHeading).get(rowNumber) != null ? columnDetails.get(columnHeading).get(rowNumber).toString(): " " );
					
				}
				data.add(map1);
		    }
			
		  //Create Datasource and put it in Dynamic Jasper Format
		  
		  for(int i = 0; i < columnHeader.length; i++){
		
		   for(String colHead:columnTitle)
		   {
			   if(columnHeader[i].equalsIgnoreCase(colHead)){
				   
			   String colBeanName=colHead.replaceAll("\\s", "");
			   ColumnBuilder columnContent = ColumnBuilder.getNew();

			   columnContent.setTitle(colHead);
			   columnContent.setHeaderStyle(headerStyle);
			   columnContent.setWidth((colHead.length())*8);
			   columnContent.setFixedWidth(true);
			   columnContent.setColumnProperty(colBeanName,
						String.class.getName(), colBeanName);

				dynamicReportBuilder.addColumn(columnContent.build());
			   }
		   }
		    
		  }
			DynamicReport dynamicReport = dynamicReportBuilder.build();

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(data);

			// build JasperPrint instance, filling the report with data from
			// datasource created above
			JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(
										dynamicReport, new ClassicLayoutManager(),
											beanCollectionDataSource);

			// export to pdf
			// String pdfFile = Math.round(Math.random() * 100000) + ".pdf";

			JRXlsExporter exporter = new JRXlsExporter();

			servletOutputStream = httpServletResponse.getOutputStream();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
					servletOutputStream);
			
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
	        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
	        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

			exporter.exportReport();
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (JRException e) {
			e.printStackTrace();
		}

		return mapping.findForward("showsearchvendor");
	}
	
	/**
	 * To View BP Market Sector Search Report Page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewIndirectProcurementCommoditiesReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To View BP Market Sector Search Report Page in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;
		reportForm.reset(mapping, request);

		try
		{
			//Setting Values for Vendor Status Field in the Form.
			CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();		
			List<StatusMaster> vendorStatusMaster = dao.list(appDetails, "From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");		
			session.setAttribute("VendorStatusMasterList", vendorStatusMaster);
			
			if(vendorStatusMaster != null && vendorStatusMaster.size() != 0)
			{
				String statusIds[] = new String[vendorStatusMaster.size()];
				for(int i = 0; i < vendorStatusMaster.size(); i++)
				{
					statusIds[i] = vendorStatusMaster.get(i).getId().toString();
				}
				reportForm.setVendorStatusList(statusIds);
			}
			
			//Setting Values for Market Sector Field in the Form.
			CURDTemplateImpl<MarketSector> dao1 = new CURDTemplateImpl<MarketSector>();		
			List<MarketSector> marketSectorsList = dao1.list(appDetails, "From MarketSector where isActive=1 order by sectorDescription");
			session.setAttribute("marketSectorsList", marketSectorsList);
			
			ReportDao reportDao = new ReportDaoImpl();
			List<Tier2ReportDto> indirectProcurementCommodities = reportDao.listIndirectProcurementCommodities(appDetails);		
			
			if(indirectProcurementCommodities != null && indirectProcurementCommodities.size() != 0 )
			{
				String commodities[] = new String[indirectProcurementCommodities.size()];
				for(int i = 0; i < indirectProcurementCommodities.size(); i++)
				{
					commodities[i] = indirectProcurementCommodities.get(i).getMarketSectorId().toString();
				}
				reportForm.setIndirectCommoditiesList(commodities);
			}
			
			//Setting Values for Certificate Type Field in the Form.
			CURDTemplateImpl<CustomerCertificateType> dao2 = new CURDTemplateImpl<CustomerCertificateType>();		
			List<CustomerCertificateType> certificateTypesList = dao2.list(appDetails, "From CustomerCertificateType where isActive = 1 order by certificateTypeDesc");
			session.setAttribute("certificateTypesList", certificateTypesList);
			
			List<Tier2ReportDto> indirectProcurementCertificateTypes = reportDao.listIndirectProcurementCertificateTypes(appDetails);		
			
			if(indirectProcurementCertificateTypes != null && indirectProcurementCertificateTypes.size() != 0 )
			{
				String certificateTypes[] = new String[indirectProcurementCertificateTypes.size()];
				for(int i = 0; i < indirectProcurementCertificateTypes.size(); i++)
				{
					certificateTypes[i] = indirectProcurementCertificateTypes.get(i).getCertificateTypeId().toString();
				}
				reportForm.setCertificateTypeList(certificateTypes);
			}			
		}
		catch(Exception ex)
		{
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
		}
		
		return mapping.findForward("showIndirectProcurementCommoditiesReport");
	}
	
	/**
	 * To Retrieve BP Market Sector Search Report Data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getIndirectProcurementCommoditiesReport(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{		
		logger.info("To Retrieve BP Market Sector Search Report Data in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		
		ReportDao reportDao = new ReportDaoImpl();
		List<Tier2ReportDto> indirectProcurementCommodities = null;
		indirectProcurementCommodities = reportDao.getIndirectProcurementCommoditiesReport(reportForm, appDetails);
		session.setAttribute("indirectProcurementCommodities", indirectProcurementCommodities);
		setPrintReportPdf(indirectProcurementCommodities);

		return mapping.findForward("getIndirectProcurementCommoditiesReportData");
	}
	
	
	/**
	 * To Produce PDF Using Jasper for BP Market Sector Search Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward getIndirectProcurementCommoditiesReportPdf(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Produce PDF Using Jasper for BP Market Sector Search in Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try
		{
			String reportName = "BPMarketSectorSearchReport_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH) + "/" + appDetails.getSettings().getLogoPath());

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(getPrintReportPdf());
			InputStream is = JRLoader.getResourceInputStream("indirect_procurement_commodities_report.jasper");
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params, beanCollectionDataSource);

			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}
	
	/**
	 * To Save Indirect Procurement Commodity List from Report.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveIndirectProcurementCommodityList(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		logger.info("To Save Indirect Procurement Commodity List from Report Menu");
		
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		String result = "No Data Found";
		
		Tier2ReportForm reportForm = (Tier2ReportForm) form;		
		if(reportForm.getIndirectCommoditiesList() != null && reportForm.getIndirectCommoditiesList().length > 0)
		{
			ReportDao reportDao = new ReportDaoImpl();
			result = reportDao.saveIndirectProcurementCommodityList(appDetails, userId, reportForm, "BP Market Sector Search");
		}		
		
		JSONObject jsonObject = new JSONObject();		
		jsonObject.put("result", result);		
		response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
		return null;
	}
	
	
	/*public ActionForward listOfCommoditiesBasedOnSearchCommodityText(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String fullTextSearch = request.getParameter("searchCommodityText");
		CommodityDaoImpl commodityDao = new CommodityDaoImpl();
		List<CommodityDto> commodities = (List<CommodityDto>) commodityDao.listOfCommoditiesBasedOnSearchCommodityText(appDetails, fullTextSearch);
		session.setAttribute("customerCommodities",	commodities);
		return mapping.findForward("viewcommodityList");
	}*/
	
	public ActionForward listOfTier2Vendors(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		String fullTextSearch = request.getParameter("searchVendorsText");
		UserDetailsDto appDetails = (UserDetailsDto) session.getAttribute("userDetails");
		CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
		String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
				+ " where  vm.primeNonPrimeVendor=0 and vm.deverseSupplier=1 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.vendorName like '%"+fullTextSearch+"%'";
		List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao.findAllByQuery(appDetails, query);
		
		session.setAttribute("tier2Vendors", tier2Vendors);
		return mapping.findForward("tier2VendorList");
	}
	
}