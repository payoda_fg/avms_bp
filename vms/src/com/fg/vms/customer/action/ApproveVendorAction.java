/*
 * ApproveVendorAction.java 
 */
package com.fg.vms.customer.action;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.common.Country;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.impl.AnonymousVendorDaoImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.model.AnonymousVendor;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.pojo.AnonymousVendorForm;
import com.fg.vms.util.CommonUtils;

/**
 * Represents the approval of new vendors this includes registering the vendor
 * in vendor master. (e.g.. List the anonymous vendor, approve vendors through
 * application and by send the URL to approve through Email.)
 * 
 * @author vinoth
 * 
 */
public class ApproveVendorAction extends DispatchAction {

	/**
	 * View List of Anonymous Vendors to be approved.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward listVendors(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		/* Get the current user details */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

//		List<AnonymousVendor> anonymousVendors = null;
//		AnonymousVendorDaoImpl anonymousVendorDaoImpl = new AnonymousVendorDaoImpl();
		AnonymousVendorForm anonymousVendorForm = (AnonymousVendorForm) form;

//		/* Get the vendors list */
//		anonymousVendors = anonymousVendorDaoImpl.listOfVendors(
//				anonymousVendorForm, appDetails);
//		session.setAttribute("anonymousVendors", anonymousVendors);

		/* Get the NAICS Category list */
		// NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		// List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
		// .view(appDetails);
		// session.setAttribute("categoryName", naicsCategories);
		session.setAttribute("categoryId", 0);

		/* Get the country list */
		CommonDao commonDao = new CommonDaoImpl();
		List<Country> countries = commonDao.getCountries(appDetails);
		anonymousVendorForm.setCountries(countries);
		session.setAttribute("countries", countries);

		return mapping.findForward("viewanonymousvendors");
	}

	/**
	 * Approve selected vendor without viewing their details. This is based on
	 * their name and country details.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward approveVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		/* Get the current user details */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<AnonymousVendor> anonymousVendors = null;
		AnonymousVendorDaoImpl anonymousVendorDaoImpl = new AnonymousVendorDaoImpl();
		AnonymousVendorForm anonymousVendorForm = (AnonymousVendorForm) form;

		Integer selectedid = Integer.parseInt(request
				.getParameter("selectedId").toString()); // Get the selected
		// vendor id from
		// request

		Integer userId = (Integer) session.getAttribute("userId"); // Get the
		// users id
		// from
		// session

		/*
		 * Move the anonymous vendor to vendor master who got first level
		 * approval
		 */
		if (selectedid != 0) {
//			String url = request.getRequestURL().toString()
//					.replaceAll(request.getServletPath(), "").trim();
//			anonymousVendorDaoImpl.createChildTableData(userId, selectedid,
//					userDetails, url, anonymousVendorForm);
			anonymousVendorDaoImpl.approveNewVendor(selectedid, userDetails);
		}

		/* Get the List of Vendors */
//		anonymousVendors = anonymousVendorDaoImpl.listOfVendors(
//				anonymousVendorForm, userDetails);
//		session.setAttribute("anonymousVendors", anonymousVendors);

		return mapping.findForward("approve");
	}

	/**
	 * Anonymous vendor approval by customer after verifying their details.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 * 
	 */
	public ActionForward approveVendorByName(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		/* Get the current user details */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		AnonymousVendorDaoImpl anonymousVendorDaoImpl = new AnonymousVendorDaoImpl();
		AnonymousVendorForm anonymousVendorForm = (AnonymousVendorForm) form;

		Integer vendorId = anonymousVendorForm.getCurrentVendorId(); // Get the
		// current
		// vendor
		// id from
		// form

		String result = "";
		String result1 = "";

		/*
		 * Move the anonymous vendor to vendor master who got first level
		 * approval
		 */
		/*if (vendorId != 0) {
			String url = request.getRequestURL().toString()
					.replaceAll(request.getServletPath(), "").trim();
			result1 = anonymousVendorDaoImpl.createChildTableData(userId,
					vendorId, userDetails, url, anonymousVendorForm);
			if (result1.equals("success")) {
				result = anonymousVendorDaoImpl.approveNewVendor(vendorId,
						userDetails);
			}
		}*/

		/* Forward only successful vendor approval */
		if (result.equals("success") && result1.equals("success")) {
			return mapping.findForward("approve");
		} else if (result1.equals("failure")) {
			ActionErrors errors = new ActionErrors();
			errors.add("contanctEmail", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			// return mapping.getInputForward();
		}

		return mapping.findForward("approvefailure");
	}

	/**
	 * Search the vendors based vendor name, country, region, naics category,
	 * naics sub-category details.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward searchVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();

		/* Get the current user details */
//		UserDetailsDto userDetails = (UserDetailsDto) session
//				.getAttribute("userDetails");
//		List<SearchVendorDto> anonymousVendors = null;
//		AnonymousVendorDaoImpl anonymousVendorDaoImpl = new AnonymousVendorDaoImpl();
//		AnonymousVendorForm anonymousVendorForm = (AnonymousVendorForm) form;

		/* Search the vendors 
		anonymousVendors = anonymousVendorDaoImpl.searchVendorName(
				anonymousVendorForm, userDetails);

		session.setAttribute("anonymousVendors", anonymousVendors);*/

		return mapping.findForward("searchsuccess");
	}

	/**
	 * Edit selected vendors based on ID
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward retrive(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		/* Get the current user details */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Integer id = null;
		if (request.getParameter("id") != null) {
			id = Integer.parseInt(request.getParameter("id").toString());
		}

		if (session.getAttribute("anonymousvendor") != null) {
			id = Integer.parseInt(session.getAttribute("anonymousvendor")
					.toString());
		}

		/* View list of certificates and agencies */
		List<CertifyingAgency> certificateAgencies;
		List<Certificate> certificates = null;
		SearchDao search = new SearchDaoImpl();
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificateAgencies = search.listAgencies(appDetails);// List
		// of certifying
		// agencies.
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails); // List of
		// Certificates.
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);

		AnonymousVendorDaoImpl anonymousVendorDaoImpl = new AnonymousVendorDaoImpl();
		AnonymousVendorForm anonymousVendorForm = (AnonymousVendorForm) form;

		/* Retrieve the list of vendors based on details. */
		AnonymousVendor anonymousVendor = null;/*anonymousVendorDaoImpl.retriveVendor(
				anonymousVendorForm, id, appDetails);*/

		/* set values in customer information boxes */
		anonymousVendorForm.setCurrentVendorId(id);
		anonymousVendorForm.setVendorName(anonymousVendor.getVendorName());
		anonymousVendorForm.setDunsNumber(anonymousVendor.getDunsNumber());
		anonymousVendorForm.setTaxId(anonymousVendor.getTaxId());
		anonymousVendorForm.setCompanytype(anonymousVendor.getCompanyType());
		anonymousVendorForm.setAddress1(anonymousVendor.getAddress1());
		anonymousVendorForm.setAddress2(anonymousVendor.getAddress2());
		anonymousVendorForm.setAddress3(anonymousVendor.getAddress3());
		anonymousVendorForm.setCity(anonymousVendor.getCity());
		anonymousVendorForm.setCountry(anonymousVendor.getCountry());
		anonymousVendorForm.setState(anonymousVendor.getState());
		anonymousVendorForm.setProvince(anonymousVendor.getProvince());
		anonymousVendorForm.setPhone(anonymousVendor.getPhone());
		anonymousVendorForm.setRegion(anonymousVendor.getRegion());
		anonymousVendorForm.setZipcode(anonymousVendor.getZipCode());
		anonymousVendorForm.setMobile(anonymousVendor.getMobile());
		anonymousVendorForm.setFax(anonymousVendor.getFax());
		anonymousVendorForm.setWebsite(anonymousVendor.getWebsite());
		anonymousVendorForm.setEmailId(anonymousVendor.getEmailId());
		anonymousVendorForm.setHiddenEmailId(anonymousVendor.getEmailId());
		anonymousVendorForm.setNumberOfEmployees(anonymousVendor
				.getNumberOfEmployees());
		anonymousVendorForm.setUsername(RandomStringUtils.randomAlphabetic(5));
		String psw = RandomStringUtils.randomAlphanumeric(5);
		anonymousVendorForm.setPassword(psw);
		anonymousVendorForm.setConfirmPassword(psw);
		/* Convert double value to String */

		NumberFormat formatter = NumberFormat.getCurrencyInstance();

		anonymousVendorForm.setAnnualTurnover(formatter.format(anonymousVendor
				.getAnnualTurnover()));
		anonymousVendorForm.setYearOfEstablishment(anonymousVendor
				.getYearOfEstablishment());
		anonymousVendorForm.setDiverseCertificateName(anonymousVendor
				.getDiverseClassification());
		anonymousVendorForm
				.setEthnicity(anonymousVendor.getEthnicityId() != null ? anonymousVendor
						.getEthnicityId().getId().toString()
						: "0");
		if (anonymousVendor.getMinorityPercent() != null
				&& anonymousVendor.getMinorityPercent() != 0.0) {
			anonymousVendorForm.setMinorityPercent(String
					.valueOf(anonymousVendor.getMinorityPercent().intValue()));
		}
		if (anonymousVendor.getWomenPercent() != null
				&& anonymousVendor.getWomenPercent() != 0.0) {
			anonymousVendorForm.setWomenPercent(String.valueOf(anonymousVendor
					.getWomenPercent().intValue()));
		}

		anonymousVendorForm.setClassificationNotes(anonymousVendor
				.getDiverseNotes());

		/* For get values from dropdown boxes */
		boolean diverse = (anonymousVendor.getDiverseSupplier() != 0);

		if (diverse == true) {
			anonymousVendorForm.setDiverseSupplier("on");

			if (anonymousVendor.getCertAgencyId() != null) {
				anonymousVendorForm.setDivCertAgen(anonymousVendor
						.getCertAgencyId().getId());
			}
			if (anonymousVendor.getCertMasterId() != null) {
				anonymousVendorForm.setDivCertType(anonymousVendor
						.getCertMasterId().getId());
			}
			if (anonymousVendor.getExpiryDate() != null) {
				anonymousVendorForm.setExpDate(CommonUtils
						.convertDateToString(anonymousVendor.getExpiryDate()));
			}
			if (anonymousVendor.getContent() != null
					&& anonymousVendor.getContent().length() != 0) {
				anonymousVendorForm.setCertId(anonymousVendor.getId());
			}
			if (anonymousVendor.getEffectiveDate() != null) {
				anonymousVendorForm
						.setEffDate1(CommonUtils
								.convertDateToString(anonymousVendor
										.getEffectiveDate()));
			}
			if (anonymousVendor.getCertificateNumber() != null
					&& !anonymousVendor.getCertificateNumber().isEmpty()) {
				anonymousVendorForm.setCertificationNo1(anonymousVendor
						.getCertificateNumber());
			}
		} else {
			anonymousVendorForm.setDiverseSupplier("");
		}

		/* Getting contact information */
		anonymousVendorForm.setFirstName(anonymousVendor.getFirstName());
		anonymousVendorForm.setLastName(anonymousVendor.getLastName());
		anonymousVendorForm.setDesignation(anonymousVendor.getDesignation());
		anonymousVendorForm.setContactPhone(anonymousVendor.getPhoneNumber());
		anonymousVendorForm.setContactMobile(anonymousVendor.getMobileNumber());
		anonymousVendorForm.setContactFax(anonymousVendor.getFaxNumber());
		anonymousVendorForm.setContanctEmail(anonymousVendor.getEmail());
		anonymousVendorForm.setNaicsCode_1(anonymousVendor.getNaicsCode());
		anonymousVendorForm.setNaicsDesc(anonymousVendor.getNaicsDescription());
		anonymousVendorForm.setCapabilities(anonymousVendor.getCapabilities());

		/* Getting values in reference boxes */
		anonymousVendorForm.setReferenceName1(anonymousVendor
				.getReferenceName1());
		anonymousVendorForm.setReferenceAddress1(anonymousVendor
				.getReferenceAddress1());
		anonymousVendorForm.setReferenceMailId1(anonymousVendor
				.getReferenceMailId1());
		anonymousVendorForm.setReferenceMobile1(anonymousVendor
				.getReferenceMobile1());
		anonymousVendorForm.setReferencePhone1(anonymousVendor
				.getReferencePhone1());
		anonymousVendorForm.setReferenceName2(anonymousVendor
				.getReferenceName2());
		anonymousVendorForm.setReferenceAddress2(anonymousVendor
				.getReferenceAddress2());
		anonymousVendorForm.setReferenceMailId2(anonymousVendor
				.getReferenceMailId2());
		anonymousVendorForm.setReferenceMobile2(anonymousVendor
				.getReferenceMobile2());
		anonymousVendorForm.setReferencePhone2(anonymousVendor
				.getReferencePhone2());
		anonymousVendorForm.setReferenceName3(anonymousVendor
				.getReferenceName3());
		anonymousVendorForm.setReferenceAddress3(anonymousVendor
				.getReferenceAddress3());
		anonymousVendorForm.setReferenceMailId3(anonymousVendor
				.getReferenceMailId3());
		anonymousVendorForm.setReferenceMobile3(anonymousVendor
				.getReferenceMobile3());
		anonymousVendorForm.setReferencePhone3(anonymousVendor
				.getReferencePhone3());
		anonymousVendorForm.setReferenceCity1(anonymousVendor
				.getReferenceCity1());
		anonymousVendorForm.setReferenceCity2(anonymousVendor
				.getReferenceCity2());
		anonymousVendorForm.setReferenceCity3(anonymousVendor
				.getReferenceCity3());
		anonymousVendorForm
				.setReferenceZip1(anonymousVendor.getReferenceZip1());
		anonymousVendorForm
				.setReferenceZip2(anonymousVendor.getReferenceZip2());
		anonymousVendorForm
				.setReferenceZip3(anonymousVendor.getReferenceZip3());
		anonymousVendorForm.setReferenceState1(anonymousVendor
				.getReferenceState1());
		anonymousVendorForm.setReferenceState2(anonymousVendor
				.getReferenceState2());
		anonymousVendorForm.setReferenceState3(anonymousVendor
				.getReferenceState3());
		anonymousVendorForm.setReferenceCountry1(anonymousVendor
				.getReferenceCountry1());
		anonymousVendorForm.setReferenceCountry2(anonymousVendor
				.getReferenceCountry2());
		anonymousVendorForm.setReferenceCountry3(anonymousVendor
				.getReferenceCountry3());

		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities", ethnicityDao.list(appDetails, null));

		return mapping.findForward("retrievesuccess");
	}

	/**
	 * Approve the vendor based on URL received by customer through Email.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward approveVendorByURL(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();

		/* Get the current user details */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		AnonymousVendorDaoImpl anonymousVendorDaoImpl = new AnonymousVendorDaoImpl();
		AnonymousVendorForm anonymousVendorForm = (AnonymousVendorForm) form;

		Integer vendorId = anonymousVendorForm.getCurrentVendorId();

		String result = "";
		String result1 = "";

		/*
		 * Move the anonymous vendor to vendor master who got first level
		 * approval
		 */
		/*if (vendorId != 0) {
			String url = request.getRequestURL().toString()
					.replaceAll(request.getServletPath(), "").trim();
			 Create the vendor master based on anonymous vendor details. 
			result1 = anonymousVendorDaoImpl.createChildTableData(userId,
					vendorId, userDetails, url, anonymousVendorForm);
			if (result1.equals("success")) {

				 Approve vendor after created the vendor master successfully. 
				result = anonymousVendorDaoImpl.approveNewVendor(vendorId,
						userDetails);
			}
		}*/

		if (result.equals("success") && result1.equals("success")) {
			return mapping.findForward("approveURL");
		}

		return mapping.findForward("approvefailureURL");
	}
}
