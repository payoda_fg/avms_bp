package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.impl.TemplateDaoImpl;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.TemplateQuestions;
import com.fg.vms.customer.pojo.QuestionsForm;
import com.fg.vms.customer.pojo.TemplateQuestionsForm;

/**
 * 
 * @author vivek
 * 
 */
public class QuestionsAction extends DispatchAction {

    /**
     * Shows the page which contains the template field.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward showPage(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        List<Template> templateNameList = templateDaoImpl
                .allTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);
        return mapping.findForward("showpage");
    }

    /**
     * Save the status changed by the customer in templates page.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward savestatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        String result = null;
        if (request.getParameter("templateid") != null
                && request.getParameter("statusvalue") != null) {

            Integer templateid = Integer.parseInt(request.getParameter(
                    "templateid").toString());
            Byte statusvalue = Byte.parseByte(request
                    .getParameter("statusvalue"));
            result = templateDaoImpl.updatetemplateforactivestatus(templateid,
                    statusvalue, userId, userDetails);
        }

        if (result != null && result.equals("success")) {
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("updatestatus.ok");
            messages.add("statusupdate", msg);
            saveMessages(request, messages);
        }

        List<Template> templateNameList = templateDaoImpl
                .allTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);
        return mapping.findForward("showpage");
    }

    /**
     * Creates the template.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward create(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        QuestionsForm questionsForm = (QuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        String result = templateDaoImpl.createTemplate(questionsForm, userId,
                userDetails);

        List<Template> templateNameList = templateDaoImpl
                .listTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);

        if (result.equals("success")) {
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("template.ok");
            messages.add("template", msg);
            saveMessages(request, messages);
            questionsForm.reset(mapping, request);
        } else if (result.equals("templateExisted")) {
            ActionErrors errors = new ActionErrors();
            errors.add("templateName", new ActionMessage("template.existed"));
            saveErrors(request, errors);
            return mapping.getInputForward();
        }
        templateNameList = templateDaoImpl.listTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);

        return mapping.findForward(result);

    }

    /**
     * Retrieve the list of template names.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retrive(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        QuestionsForm questionsForm = (QuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        List<Template> templateNameList = templateDaoImpl
                .listTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);

        return mapping.findForward("success");

    }

    /**
     * Shows the template question Field.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward showTemplateQuestionpage(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        Integer id = Integer.parseInt(request.getParameter("id").toString());

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Template template = templateDaoImpl.retriveTemplate(id, userDetails);

        List<TemplateQuestions> templateQuestionsList = templateDaoImpl
                .listTemplateQuestions(userDetails, id);
        session.setAttribute("templateQuestionsList", templateQuestionsList);

        session.setAttribute("QuestionId", id);
        List<Template> templateNameList = templateDaoImpl
                .listTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);
        session.setAttribute("template", template);

        TemplateQuestionsForm templateQuestionsForm = (TemplateQuestionsForm) form;

        templateQuestionsForm.setTemplateId(id);
        return mapping.findForward("showTemplateQuestionpage");
    }

    /**
     * Creates the template questions based on the template name id.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward createtemplateQuestions(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        TemplateQuestionsForm templateQuestionsForm = (TemplateQuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        String result = templateDaoImpl.createTemplateQuestions(
                templateQuestionsForm, userId, userDetails);

        // For check unique question order

        /*
         * if (result.equals("unique")) { ActionErrors errors = new
         * ActionErrors(); errors.add("questionOrder", new
         * ActionMessage("questionOrder.unique")); saveErrors(request, errors);
         * return mapping.getInputForward(); }
         */

        List<TemplateQuestions> templateQuestionsList = templateDaoImpl
                .listTemplateQuestions(userDetails, userId);
        session.setAttribute("templateQuestionsList", templateQuestionsList);

        if (result.equals("success")) {
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("templateQuestion.ok");
            messages.add("templateQuestion", msg);
            saveMessages(request, messages);
            templateQuestionsForm.reset(mapping, request);
        }
        templateQuestionsList = templateDaoImpl.listTemplateQuestions(
                userDetails, userId);
        session.setAttribute("templateQuestionsList", templateQuestionsList);

        return mapping.findForward(result);
    }

    /**
     * Reterive the Template questions list.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retriveTemplateQuestions(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        TemplateQuestionsForm templateQuestionsForm = (TemplateQuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        List<TemplateQuestions> templateQuestionsList = templateDaoImpl
                .listTemplateQuestions(userDetails, userId);
        session.setAttribute("templateQuestionsList", templateQuestionsList);
        return mapping.findForward("success");

    }

    /**
     * Deletes the template name.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        String result = templateDaoImpl.deleteTemplate(
                Integer.parseInt(request.getParameter("id")), userDetails);
        List<Template> template = null;

        List<Template> templateNameList = templateDaoImpl
                .allTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);
        if (result.equals("reference")) {

            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("template.reference.delete");
            messages.add("templateReferenceDelete", msg);
            saveMessages(request, messages);
            return mapping.getInputForward();
        }
        if (result.equals("success")) {
            template = templateDaoImpl.listTemplateNames(userDetails);
            session.setAttribute("template", template);
        }

        return mapping.findForward(result);
    }

    /**
     * Deletes the Template question.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward deleteTemplateQuestion(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        String result = templateDaoImpl.deleteTemplateQuestion(
                Integer.parseInt(request.getParameter("id")), userDetails);
        List<TemplateQuestions> templateQuestions = null;
        if (result.equals("success")) {
            templateQuestions = templateDaoImpl.listTemplateQuestions(
                    userDetails, null);
            session.setAttribute("templateQuestions", templateQuestions);
        }

        List<TemplateQuestions> templateQuestionsList = templateDaoImpl
                .listTemplateQuestions(userDetails, null);
        session.setAttribute("templateQuestionsList", templateQuestionsList);
        return mapping.findForward(result);
    }

    /**
     * Reterives the template name for edit and update.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retriveTemplate(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        QuestionsForm questionsForm = (QuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        HttpSession session = request.getSession();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer id = Integer.parseInt(request.getParameter("id").toString());
        Template template = templateDaoImpl.retriveTemplate(id, userDetails);
        if (template != null) {

            questionsForm.setTemplateName(template.getTemplateName());

        }
        List<Template> templates = null;
        templates = templateDaoImpl.listTemplateNames(userDetails);
        session.setAttribute("templates", templates);
        return mapping.findForward("modify");
    }

    /**
     * Update the template name.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updateTemplate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        Integer currentUserId = (Integer) session.getAttribute("userId");
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        QuestionsForm questionsForm = (QuestionsForm) form;

        String result = templateDaoImpl.updateTemplate(questionsForm,
                questionsForm.getId(), userDetails);
        List<Template> template = null;

        if (result.equals("success")) {
            template = templateDaoImpl.listTemplateNames(userDetails);
            request.setAttribute("template", template);
            questionsForm.reset(mapping, request);
            session.removeAttribute("template");
        }
        if (result.equals("failure")) {
            ActionErrors errors = new ActionErrors();
            errors.add("error", new ActionMessage("error.failure"));
            saveErrors(request, errors);
            return mapping.getInputForward();
        }
        List<Template> templateNameList = templateDaoImpl
                .allTemplateNames(userDetails);
        session.setAttribute("templateNameList", templateNameList);
        return mapping.findForward(result);
    }

    /**
     * Reterives the template Questions for edit and update.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retriveQuestions(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        TemplateQuestionsForm templateQuestionsForm = (TemplateQuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        HttpSession session = request.getSession();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer id = Integer.parseInt(request.getParameter("id").toString());
        TemplateQuestions templateQuestions = templateDaoImpl
                .retriveTemplateQuestions(id, userDetails);
        if (templateQuestions != null) {
            templateQuestionsForm.setAnswerWeightage(templateQuestions
                    .getAnswerWeightage());
            templateQuestionsForm.setQuestionDescription(templateQuestions
                    .getQuestionDescription());
            templateQuestionsForm.setQuestionOrder(templateQuestions
                    .getQuestionOrder());
        }
        List<TemplateQuestions> templateQuestions2 = null;
        templateQuestions2 = templateDaoImpl.listTemplateQuestions(userDetails,
                id);
        session.setAttribute("templateQuestions", templateQuestions2);
        return mapping.findForward("modify");
    }

    /**
     * Update the Template Questions.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updateTemplateQuestions(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        Integer currentUserId = (Integer) session.getAttribute("userId");
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        TemplateQuestionsForm templateQuestionsForm = (TemplateQuestionsForm) form;

        String result = templateDaoImpl.updateTemplateQuestions(
                templateQuestionsForm, templateQuestionsForm.getId(),
                userDetails);
        List<TemplateQuestions> templateQuestions = null;

        if (result.equals("success")) {
            templateQuestions = templateDaoImpl.listTemplateQuestions(
                    userDetails, currentUserId);
            request.setAttribute("templateQuestions", templateQuestions);
            templateQuestionsForm.reset(mapping, request);
            session.removeAttribute("templateQuestions");
        }
        if (result.equals("failure")) {
            ActionErrors errors = new ActionErrors();
            errors.add("error", new ActionMessage("error.failure"));
            saveErrors(request, errors);
            return mapping.getInputForward();
        }
        List<TemplateQuestions> templateQuestionsList = templateDaoImpl
                .listTemplateQuestions(userDetails, currentUserId);
        session.setAttribute("templateQuestionsList", templateQuestionsList);
        return mapping.findForward(result);
    }

    /**
     * method to show the copy template page.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward showCopyTemplate(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        // QuestionsForm questionsForm = (QuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        String templateName = request.getParameter("templateName");

        Template template = templateDaoImpl.saveTemplate(templateName, userId,
                userDetails);

        session.setAttribute("templateId", template.getId());

        List<Template> templateNameList = templateDaoImpl
                .listSelectTemplateQuestions(userDetails);

        session.setAttribute("templateNameList", templateNameList);
        request.setAttribute("selectedTemplate", 0);
        return mapping.findForward("showCopyTemplatePage");
    }

    /**
     * Saves the selected template questions into the db.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward saveCopyTemplate(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        Integer userId = (Integer) session.getAttribute("userId");

        QuestionsForm questionsForm = (QuestionsForm) form;

        Integer templateId = (Integer) session.getAttribute("templateId");

        String result = templateDaoImpl.saveCopyTemplate(questionsForm, userId,
                userDetails, templateId);
        List<Template> templateNameList = templateDaoImpl
                .listSelectTemplateQuestions(userDetails);

        if (result.equals("success")) {
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("copyTemplateQuestions.ok");
            messages.add("template", msg);
            saveMessages(request, messages);
            questionsForm.reset(mapping, request);
        }
        session.setAttribute("templateNameList", templateNameList);
        request.setAttribute("selectedTemplate", 0);
        return mapping.findForward("showCopyTemplatePage");
    }

    /**
     * Reterive the template questions based on template id
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward templateQuestions(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        HttpSession session = request.getSession();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer id = Integer.parseInt(request.getParameter("templateId")
                .toString());
        TemplateQuestions templateQuestions = templateDaoImpl
                .retriveTemplateQuestions(id, userDetails);
        List<TemplateQuestions> templateQuestions2 = null;
        templateQuestions2 = templateDaoImpl.listTemplateQuestions(userDetails,
                id);
        request.setAttribute("templateQuestions", templateQuestions2);
        List<Template> templateNameList = templateDaoImpl
                .listSelectTemplateQuestions(userDetails);

        session.setAttribute("templateNameList", templateNameList);
        request.setAttribute("selectedTemplate", id);

        return mapping.findForward("showCopyTemplatePage");
    }

    /**
     * Show the capability assessment page.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward showCapabilityAssessmentPage(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        // TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        return mapping.findForward("showCapabilityAssessmentPage");
    }

    /**
     * method to copy template questions from one template to another template.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward copyFromExistingTemplate(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        QuestionsForm questionsForm = (QuestionsForm) form;
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        // String templateName = questionsForm.getTemplateName();
        Integer templateId = questionsForm.getId();

        templateDaoImpl.updateTemplate(questionsForm, templateId, userDetails);

        session.setAttribute("templateId", templateId);

        List<Template> templateNameList = templateDaoImpl
                .getTemplateNamesforCopyandMove(userDetails, templateId);

        session.setAttribute("templateNameList", templateNameList);
        request.setAttribute("selectedTemplate", 0);
        return mapping.findForward("showCopyTemplatePage");
    }

}
