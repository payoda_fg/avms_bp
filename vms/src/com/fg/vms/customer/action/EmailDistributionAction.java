/*
 * EmailDistributionAction.java
 */
package com.fg.vms.customer.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.impl.EmailDistributionDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.pojo.EmailDistributionForm;
import com.fg.vms.util.Constants;

/**
 * Represents Email Distribution configuration.
 * 
 * @author vinoth
 * 
 */
public class EmailDistributionAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * View Email Distribution page.
	 * 
	 * This method always initialize the list of users from database.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewEmailDistribution(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to view the list of active users..");

		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/*
		 * Get the active distribution list
		 */
		WorkflowConfigurationDaoImpl workflowConfigurationDaoImpl = new WorkflowConfigurationDaoImpl();
		List<EmailDistributionModel> distributionModels = workflowConfigurationDaoImpl
				.view(userDetails);

		session.setAttribute("distributionModels", distributionModels);
		logger.info("Forward to show the list of users page");

		return mapping.findForward("showEmailDistributionPage");
	}

	/**
	 * Save Email Distribution list.
	 * 
	 * This method used to save the list of users to particular email
	 * distribution group.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveEmailDistributionList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to save the list of active users in email distribution..");

		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId"); // Get the
																	// current
																	// user id
																	// from
																	// session

		EmailDistributionForm distributionForm = (EmailDistributionForm) form;

		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* New Email Distribution creation */
		String result = emailDistributionDaoImpl.createEmailDistribution(
				distributionForm, userId, userDetails);

		// For check unique email distribution name

		if (result.equals("unique")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emaildistribution.unique");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
			distributionForm.reset(mapping, request);
		}
		// For view the list of email distributions
		if (result.equals("success")) {
			session.getAttribute("user");

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emaildistribution.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			distributionForm.reset(mapping, request);
		}

		/*
		 * Get the active distribution list
		 */
		WorkflowConfigurationDaoImpl workflowConfigurationDaoImpl = new WorkflowConfigurationDaoImpl();
		List<EmailDistributionModel> distributionModels = workflowConfigurationDaoImpl
				.view(userDetails);

		session.setAttribute("distributionModels", distributionModels);

		logger.info("Forward to show the list of users  in email distribution page");

		return mapping.findForward("savesuccess");
	}

	/**
	 * Edit Email Distribution list.
	 * 
	 * This method used to edit the email distribution name to particular email
	 * distribution group.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward editEmailDistribution(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to edit the email distribution..");
		// System.out.println("Method to edit the email distribution..");
		HttpSession session = request.getSession();

		EmailDistributionForm distributionForm = (EmailDistributionForm) form;

		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* Edit Email Distribution. */
		if (request.getParameter("emailDistributionMasterId") != null) {
			// Get the Email Distribution object.
			EmailDistributionModel distributionModel = emailDistributionDaoImpl
					.retriveEmailDistribution(userDetails, Integer
							.parseInt(request
									.getParameter("emailDistributionMasterId")));

			List<EmailDistribution> distributions = emailDistributionDaoImpl
					.findByEmailDistributionId(
							userDetails,
							Integer.parseInt(request
									.getParameter("emailDistributionMasterId")),
							"vendorUser");
			List<EmailDistributionListDetailModel> detailModels = distributionModel != null ? distributionModel
					.getEmailDistributionDetailList() : null;
			session.setAttribute("distributionList", distributions);

			// Get the Modified users list.
			List<Users> users = null;
			UsersDao usersDao = new UsersDaoImpl();
			List<EmailDistribution> dUsersList = emailDistributionDaoImpl
					.findByEmailDistributionId(
							userDetails,
							Integer.parseInt(request
									.getParameter("emailDistributionMasterId")),
							"user");

			session.setAttribute("disUserList", dUsersList);
			/*Integer divisionId;
			CustomerApplicationSettings settings = (CustomerApplicationSettings) session
					.getAttribute("isDivisionStatus");

			if (settings.getIsDivision() != 0
					&& settings.getIsDivision() != null) {
				divisionId = (Integer) session
						.getAttribute("customerDivisionId");
			} else {
				divisionId = 0;
			}*/
			//Changed customerDivisionId to list of CustomerDivisionIds
			StringBuilder divisionIds = new StringBuilder();
			if (userDetails.getSettings().getIsDivision() != null && userDetails.getSettings().getIsDivision() != 0) 
			{
				if(userDetails.getCustomerDivisionIds() != null &&userDetails.getCustomerDivisionIds().length != 0 && userDetails.getIsGlobalDivision() == 0)
				{
					for(int i=0; i<userDetails.getCustomerDivisionIds().length; i++)
					{
						divisionIds.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
				}
			}

			users = usersDao.listUsers(userDetails, false, divisionIds.toString());
			List<Users> temp = new ArrayList<Users>();
			if (null != dUsersList) {
				for (EmailDistribution eDetailModel : dUsersList) {
					for (Users user : users) {

						if (eDetailModel.getId() != null
								&& eDetailModel.getId() == user.getId()) {
							temp.add(user);
						}
					}

				}
			}
			if (temp != null && temp.size() != 0) {
				users.removeAll(temp);
			}
			session.setAttribute("users", users);

			// Get modified vendor list.
			List<EmailDistribution> vendorContacts = null;

			vendorContacts = emailDistributionDaoImpl.viewVendorEmailList(
					userDetails, divisionIds.toString());
			List<EmailDistribution> temp1 = new ArrayList<EmailDistribution>();
			if (null != distributions) {
				for (EmailDistribution eDetailModel : distributions) {
					for (EmailDistribution vendorContact : vendorContacts) {
						if (eDetailModel.getId() != null
								&& eDetailModel.getId().equals(
										vendorContact.getId())) {
							temp1.add(vendorContact);
							continue;
						}
					}
				}
			}
			if (temp1 != null && temp1.size() != 0) {
				vendorContacts.removeAll(temp1);
			}
			session.setAttribute("vendorContacts", vendorContacts);

			if (distributionModel != null) {
				distributionForm.setEmailDistributionMasterId(distributionModel
						.getEmailDistributionMasterId());
				distributionForm.setEmailDistributionListName(distributionModel
						.getEmailDistributionListName());
				// distributionForm.setCheckbox(detailModel.getUserId());
			}
		}

		logger.info("Forward to show the email distribution page");

		return mapping.findForward("successedit");
	}

	/**
	 * Update Email Distribution list.
	 * 
	 * This method used to update the list of users to particular email
	 * distribution group.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward updateEmailDistributionList(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to save the list of active users in email distribution..");

		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId"); // Get the
																	// current
																	// user id
																	// from
																	// session

		EmailDistributionForm distributionForm = (EmailDistributionForm) form;

		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* New Email Distribution creation */
		String result = emailDistributionDaoImpl.updateEmailDistribution(
				distributionForm, userId, userDetails);

		/*
		 * Get the active distribution list
		 */
		WorkflowConfigurationDaoImpl workflowConfigurationDaoImpl = new WorkflowConfigurationDaoImpl();
		List<EmailDistributionModel> distributionModels = workflowConfigurationDaoImpl
				.view(userDetails);

		session.setAttribute("distributionModels", distributionModels);

		// For check unique email distribution name

		if (result.equals("unique")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emaildistribution.unique");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
			distributionForm.reset(mapping, request);
		}
		// For view the list of email distributions
		if (result.equals("success")) {
			session.getAttribute("user");

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emaildistribution.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			distributionForm.reset(mapping, request);
		}
		if (result.equals("failure")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage(
					"emaildistribution.updatefailed");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);

			return mapping.findForward("successedit");
		}

		logger.info("Forward to show the list of users  in email distribution page");

		return mapping.findForward("editsuccess");
	}

	/**
	 * Delete Email Distribution list.
	 * 
	 * This method used to remove the list of users to particular email
	 * distribution group.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteEmailDistribution(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method starts to delete the email distribution..");

		HttpSession session = request.getSession();
		// EmailDistributionForm distributionForm = (EmailDistributionForm)
		// form;

		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* Delete the email distribution from database. */
		String result = emailDistributionDaoImpl.deleteEmailDistributionList(
				Integer.parseInt(request
						.getParameter("emailDistributionMasterId")),
				userDetails);

		// For view the list of email distributions
		if (result.equals("success")) {
			session.getAttribute("user");

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("deleteemaildistribution.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			// distributionForm.reset(mapping, request);
		}
		if (result.equals("failure")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage(
					"emaildistribution.deletefailed");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
			// distributionForm.reset(mapping, request);
		}

		/*
		 * Get the active distribution list
		 */
		WorkflowConfigurationDaoImpl workflowConfigurationDaoImpl = new WorkflowConfigurationDaoImpl();
		List<EmailDistributionModel> distributionModels = workflowConfigurationDaoImpl
				.view(userDetails);

		session.setAttribute("distributionModels", distributionModels);

		logger.info("Forwards to the available list of users page");

		return mapping.findForward("deletesuccess");
	}

	/**
	 * Save Email Distribution list requested from workflow configuration
	 * settings page.
	 * 
	 * This method used to save the list of users to particular email
	 * distribution group.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward saveEmailDistributionListFromWorkflowConfig(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Method to save the list of active users in email distribution..");

		HttpSession session = request.getSession();
		Integer userId = (Integer) session.getAttribute("userId"); // Get the
																	// current
																	// user id
																	// from
																	// session

		EmailDistributionForm distributionForm = (EmailDistributionForm) form;

		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* New Email Distribution creation */
		String result = emailDistributionDaoImpl.createEmailDistribution(
				distributionForm, userId, userDetails);

		// For check unique email distribution name

		if (result.equals("unique")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emaildistribution.unique");
			messages.add("errorMsg", msg);
			saveMessages(request, messages);
			distributionForm.reset(mapping, request);
		}
		// For view the list of email distributions
		if (result.equals("success")) {
			session.getAttribute("user");

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("emaildistribution.ok");
			messages.add("successMsg", msg);
			saveMessages(request, messages);
			distributionForm.reset(mapping, request);
		}

		/*
		 * Get the active distribution list
		 */
		WorkflowConfigurationDaoImpl workflowConfigurationDaoImpl = new WorkflowConfigurationDaoImpl();
		List<EmailDistributionModel> distributionModels = workflowConfigurationDaoImpl
				.view(userDetails);

		session.setAttribute("distributionModels", distributionModels);

		logger.info("Forward to show the list of users  in email distribution page");

		return mapping.findForward("successconfig");
	}

	/**
	 * Get vendor contact details on ajax request.
	 * 
	 * This method always initialize the list of users from database.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward getVendorContacts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view the list of active vendors..");
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/*
		 * Get the List vendor Email IDs.
		 */
		List<EmailDistribution> vendorContacts = null;
		EmailDistributionDaoImpl emailDistributionDaoImpl = new EmailDistributionDaoImpl();

		/*Integer divisionId;
		CustomerApplicationSettings settings = (CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");

		if (settings.getIsDivision() != 0 && settings.getIsDivision() != null) {
			divisionId = (Integer) session.getAttribute("customerDivisionId");
		} else {
			divisionId = 0;
		}*/
		//Changed customerDivisionId to list of CustomerDivisionIds
		StringBuilder divisionIds = new StringBuilder();
		if (userDetails.getSettings().getIsDivision() != null && userDetails.getSettings().getIsDivision() != 0) 
		{
			if(userDetails.getCustomerDivisionIds() != null && userDetails.getCustomerDivisionIds().length != 0 && userDetails.getIsGlobalDivision() == 0)
			{
				for(int i=0; i<userDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(userDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
			}
		}
		
		vendorContacts = emailDistributionDaoImpl.viewVendorEmailList(
				userDetails, divisionIds.toString());
		session.setAttribute("vendorContacts", vendorContacts);
		logger.info("Forward to show the list of vendor contact");
		return mapping.findForward("getvendordata");
	}

	/**
	 * Get customer contact details on ajax request.
	 * 
	 * This method always initialize the list of users from database.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward getCustomerContacts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view the list of active customers..");
		List<Users> users = null;
		UsersDao usersDao = new UsersDaoImpl();
		HttpSession session = request.getSession();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		/*Integer divisionId;
		CustomerApplicationSettings settings = (CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");

		if (settings.getIsDivision() != 0 && settings.getIsDivision() != null) {
			divisionId = (Integer) session.getAttribute("customerDivisionId");
		} else {
			divisionId = 0;
		}*/
		//Changed customerDivisionId to list of CustomerDivisionIds
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		StringBuilder divisionIds = new StringBuilder();
		if (userDetails.getSettings().getIsDivision() != null && userDetails.getSettings().getIsDivision() != 0) 
		{
			if(userDetails.getCustomerDivisionIds() != null && userDetails.getCustomerDivisionIds().length != 0 && userDetails.getIsGlobalDivision() == 0)
			{
				for(int i=0; i<userDetails.getCustomerDivisionIds().length; i++)
				{
					divisionIds.append(userDetails.getCustomerDivisionIds()[i]);
					divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
			}
		}
		/*
		 * Get the List Email IDs for Customers Admin
		 */
		users = usersDao.listUsers(userDetails, false, divisionIds.toString());
		session.setAttribute("users", users);
		logger.info("Forward to show the list of customer contact");
		return mapping.findForward("getcustomerdata");
	}
}
