package com.fg.vms.customer.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.pojo.PrivilegeForm;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.customer.dao.VendorObjectsDao;
import com.fg.vms.customer.dao.VendorRolePrivilegesDao;
import com.fg.vms.customer.dao.impl.VendorObjectDaoImpl;
import com.fg.vms.customer.dao.impl.VendorRolePrivilegesDaoImpl;
import com.fg.vms.customer.dao.impl.VendorRolesDaoImpl;
import com.fg.vms.customer.dto.VendorRolesAndObjects;
import com.fg.vms.customer.model.VendorObjects;
import com.fg.vms.customer.model.VendorRolePrivileges;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;

import fr.improve.struts.taglib.layout.datagrid.Datagrid;

public class VendorPrivilegeAction extends DispatchAction {

    /** Logger of the system. */
    private Logger logger = Constants.logger;
    private static final String SUCCESS = "success";

    /**
     * Forward to privileges page
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward rolePrivileges(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        PrivilegeForm privilegeForm = (PrivilegeForm) form;
        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
        List<UserRolesForm> vendorRolesForms = vendorRolesDaoImpl
                .viewRoles(appDetails);

        session.setAttribute("vendorRoles", vendorRolesForms);
        session.setAttribute("roleName", 0);

        List<VendorObjects> objects = null;
        VendorObjectsDao configurationDaoImpl = new VendorObjectDaoImpl();
        objects = configurationDaoImpl.listObjects(appDetails);

        // Get an object list.
        List<?> aList = packageRolesPrivileges(objects, null);

        // Create a datagrid.
        Datagrid datagrid = Datagrid.getInstance();

        // Set the bean class for new objects. We suppose RolesAndObjects is the
        // class of the object in the List aList.
        datagrid.setDataClass(VendorRolesAndObjects.class);

        // Set the data
        datagrid.setData(aList);

        // Initialize the form
        privilegeForm.setDatagrid(datagrid);

        // forward to the privileges page.
        return mapping.findForward(SUCCESS);
    }

    /**
     * Save or Update the privileges by Role.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward persistPrivilegesByRole(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        PrivilegeForm privilegeForm = (PrivilegeForm) form;

        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        Integer userId = (Integer) session.getAttribute("userId");
        request.setAttribute("roleName", privilegeForm.getRoleName());
        logger.info("RoleName:" + (String) request.getAttribute("roleName"));

        VendorRolePrivilegesDao rolePrvilegesDao = new VendorRolePrivilegesDaoImpl();

        // Get the datagrid.
        Datagrid datagrid = privilegeForm.getDatagrid();

        // Deal with new subjects.
        Collection<?> privilegesAddedOrModified = datagrid.getModifiedData();
        Iterator<?> iterate = privilegesAddedOrModified.iterator();
        while (iterate.hasNext()) {
            VendorRolesAndObjects roleAndObject = (VendorRolesAndObjects) iterate
                    .next();
            if (!roleAndObject.getObjectName().equalsIgnoreCase("Privileges")) {
                rolePrvilegesDao.persistPrivilegesByRole(roleAndObject,
                        privilegeForm.getRoleName(), userId, appDetails);
            }
        }

        // List<RolePrivileges> privileges;
        // privileges = rolePrvilegesDao.retrievePrivilegesByRole(userId);
        // session.setAttribute("privileges", privileges);
        // forward to the privileges page
        // rolePrivileges = rolePrvilegesDao
        // .retrieveVendorPrivilegesByRole(userId., userDetails);
        // session.setAttribute("rolePrivileges", rolePrivileges);
        return mapping.findForward(SUCCESS);
    }

    /**
     * Retrieve the list of object privileges by role.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward viewPrivilegesByRole(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        PrivilegeForm privilegeForm = (PrivilegeForm) form;
        HttpSession session = request.getSession();
        UserDetailsDto appDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        RolesDao vendorRolesDaoImpl = new VendorRolesDaoImpl();
        List<UserRolesForm> vendorRolesForms = vendorRolesDaoImpl
                .viewRoles(appDetails);

        // get the selected role id
        Integer roleId = Integer.parseInt(request.getParameter("roleId")
                .toString());

        session.setAttribute("vendorRoles", vendorRolesForms);

        request.setAttribute("roleName", roleId.toString());
        List<VendorObjects> objects = null;
        VendorObjectsDao configurationDaoImpl = new VendorObjectDaoImpl();

        objects = configurationDaoImpl.listObjects(appDetails);
        VendorRolePrivilegesDao rolePrvilegesDaoImpl = new VendorRolePrivilegesDaoImpl();

        // Get an object list.
        List<?> aList = packageRolesPrivileges(objects,
                rolePrvilegesDaoImpl.retrievePrivilegesByRole(roleId,
                        appDetails));

        // Create a datagrid.
        Datagrid datagrid = Datagrid.getInstance();

        // Set the bean class for new objects. We suppose RolesAndObjects is the
        // class of the object in the List aList.
        datagrid.setDataClass(VendorRolesAndObjects.class);

        // Set the data
        datagrid.setData(aList);

        // Initialize the form
        privilegeForm.setDatagrid(datagrid);
        // forward to the privileges page.
        return mapping.findForward(SUCCESS);
    }

    /**
     * Convert VendorObjects and RolePrivileges to RoleAndPrivileges object
     * 
     * @param objects
     * @param privilegesByRole
     * @return an list of RoleAndPrivileges objects.
     */
    public List<VendorRolesAndObjects> packageRolesPrivileges(
            List<VendorObjects> objects,
            List<VendorRolePrivileges> privilegesByRole) {

        List<VendorRolesAndObjects> rolesAndObjects = new ArrayList<VendorRolesAndObjects>();

        // Store what are the VendorObjects present in RolePrivileges.
        List<VendorObjects> objectsInPrivilegs = new ArrayList<VendorObjects>();

        // Make list of RolesAndObjects objects based on objects and
        // privilegesByRole.
        if (privilegesByRole != null) {
            for (VendorRolePrivileges privileges : privilegesByRole) {
                for (VendorObjects object : objects) {
                    // Find objects present in privilegesByRole
                    if (privileges.getObjectId().getId().equals(object.getId())) {
                        objectsInPrivilegs.add(object);
                        break;
                    }
                }

            }
            // Set the list of privilegesByRole value to rolesAndObjects.
            for (VendorRolePrivileges privileges : privilegesByRole) {
                VendorRolesAndObjects roleAndObject = new VendorRolesAndObjects();
                roleAndObject.setAdd(CommonUtils.getBooleanValue(privileges
                        .getAdd()));
                roleAndObject.setDelete(CommonUtils.getBooleanValue(privileges
                        .getDelete()));
                roleAndObject.setEnable(CommonUtils.getBooleanValue(privileges
                        .getEnable()));
                roleAndObject.setModify(CommonUtils.getBooleanValue(privileges
                        .getModify()));
                roleAndObject.setObjectName(privileges.getObjectId()
                        .getObjectName());
                roleAndObject.setView(CommonUtils.getBooleanValue(privileges
                        .getView()));
                roleAndObject.setVisible(CommonUtils.getBooleanValue(privileges
                        .getVisible()));
                roleAndObject.setId(privileges.getObjectId().getId());
                rolesAndObjects.add(roleAndObject);

            }
            objects.removeAll(objectsInPrivilegs);
            for (VendorObjects object : objects) {
                VendorRolesAndObjects roleAndObject = new VendorRolesAndObjects();
                roleAndObject.setObjectName(object.getObjectName());
                roleAndObject.setId(object.getId());
                rolesAndObjects.add(roleAndObject);
            }
        }
        // Didn't assign privileges to role.
        if (privilegesByRole == null) {
            for (VendorObjects object : objects) {
                VendorRolesAndObjects roleAndObject = new VendorRolesAndObjects();
                roleAndObject.setObjectName(object.getObjectName());
                roleAndObject.setId(object.getId());
                rolesAndObjects.add(roleAndObject);

            }
        }
        return rolesAndObjects;
    }

}
