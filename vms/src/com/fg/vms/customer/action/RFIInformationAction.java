package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dao.RFInformationDao;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.NAICSubCategoryImpl;
import com.fg.vms.customer.dao.impl.NaicsCategoryDaoImpl;
import com.fg.vms.customer.dao.impl.RFInformationDaoImpl;
import com.fg.vms.customer.dto.Compliance;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.dto.RFIDocument;
import com.fg.vms.customer.dto.RFIPDto;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.pojo.RFIInformationForm;
import com.fg.vms.util.CommonUtils;

/**
 * @author pirabu
 * 
 */
public class RFIInformationAction extends DispatchAction {

	/**
	 * Show the RFI page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showRFIPage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		RFIInformationForm informationForm = (RFIInformationForm) form;
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
        CommonDao common = new CommonDaoImpl();
//		List<Currency> currencies = common.getCurrencies(appDetails);
//
//		informationForm.setCurrencies(currencies);

		NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
				.view(appDetails);

		informationForm.setCategories(naicsCategories);

		return mapping.findForward("success");
	}

	/**
	 * Show the RFP page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward showRFPPage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		RFIInformationForm informationForm = (RFIInformationForm) form;
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommonDao common = new CommonDaoImpl();
//		List<Currency> currencies = common.getCurrencies(appDetails);
//
//		informationForm.setCurrencies(currencies);

		NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
				.view(appDetails);

		informationForm.setCategories(naicsCategories);

		return mapping.findForward("rfpsuccess");
	}

	/**
	 * Showing the RFI dashboard.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showRFIDashboard(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		RFIInformationForm informationForm = (RFIInformationForm) form;
		RFInformationDao information = new RFInformationDaoImpl();
		List<RFIPDto> rfInformations = information
				.retrieveRFInformations(appDetails);
		informationForm.setRfInformations(rfInformations);

		return mapping.findForward("showdashboard");
	}

	/**
	 * Showing the RFP dashboard.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showRFPDashboard(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		RFIInformationForm informationForm = (RFIInformationForm) form;
		RFInformationDao information = new RFInformationDaoImpl();
		List<RFIPDto> rfInformations = information
				.retrieveRFPInformations(appDetails);
		informationForm.setRfInformations(rfInformations);

		return mapping.findForward("showdashboard");
	}

	/**
	 * Creating the RFI information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward createRFIInfo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		RFIInformationForm informationForm = (RFIInformationForm) form;

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		// System.out.println(informationForm.getFilelastrowcount());
		// Store the list of documents.

		List<?> documents = CommonUtils.packRFIDocuments(informationForm,
				RFIDocument.class);

		List<?> compliances = CommonUtils.packRFIDocuments(informationForm,
				Compliance.class);

		RFInformationDao information = new RFInformationDaoImpl();
		String result = information.persistRFI(informationForm, documents,
				compliances, userId, appDetails);

		if (result.equalsIgnoreCase("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("rfi.save.ok");
			messages.add("rfip", msg);
			saveMessages(request, messages);
		} else if (result.equalsIgnoreCase("unique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("rfiNumber", new ActionMessage("err.rfiNumber"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}
		return mapping.findForward("success");
	}

	/**
	 * Retrieving the sub-categories.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retriveSubCategories(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		RFIInformationForm informationForm = (RFIInformationForm) form;
		NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
		List<NAICSubCategory> subCategories;
		// get the selected category id
		Integer categoryId = Integer.parseInt(request
				.getParameter("categoryId").toString());
		subCategories = naicSubCategoryImpl.retrieveSubCategoryById(categoryId,
				appDetails);
		informationForm.setCategory(String.valueOf(categoryId));
		informationForm.setSubCategories(subCategories);
		return mapping.getInputForward();
	}

	/**
	 * Retrieving the vendors by category.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrieveAllVendorsByCategory(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		RFIInformationForm informationForm = (RFIInformationForm) form;
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CommonDao common = new CommonDaoImpl();
		Integer categoryId = Integer.parseInt(request
				.getParameter("categoryId").toString());
		Integer naicsSubCategoryId = Integer.parseInt(request.getParameter(
				"naicsSubCategoryId").toString());
		informationForm.setListOfVendors(common.getAllVendorsByCategory(
				categoryId, naicsSubCategoryId, appDetails));

		informationForm.setSubCategory(String.valueOf(naicsSubCategoryId));

		return mapping.getInputForward();
	}

	/**
	 * Retrieving the RFI information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */

	public ActionForward retriveRFI(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		RFIInformationForm informationForm = (RFIInformationForm) form;

		RFInformationDao information = new RFInformationDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer requedtIPId = Integer.parseInt(request.getParameter("id"));
		RFIPDto rfip = information.retriveRFIP(requedtIPId, appDetails);

		informationForm.setRfiNumber(rfip.getRequestInformation()
				.getRequestIPNumber());

		informationForm.setContactPerson(rfip.getRequestInformation()
				.getContactPerson());
		informationForm.setRfiDescription(rfip.getRequestInformation()
				.getRequestIPDescription());
		informationForm.setRfiStartDate(CommonUtils.convertDateToString(rfip
				.getRequestInformation().getRequestIPStartDate()));
		informationForm.setRfiEndDate(CommonUtils.convertDateToString(rfip
				.getRequestInformation().getRequestIPEndDate()));
		informationForm.setCurrency(rfip.getRequestInformation()
				.getRequestIPCurrency().getCurrencyName());
		informationForm.setEmailId(rfip.getRequestInformation().getEmailId());
		informationForm.setPhoneNumber(rfip.getRequestInformation()
				.getPhoneNumber());

		return mapping.findForward("viewRFI");
	}
}
