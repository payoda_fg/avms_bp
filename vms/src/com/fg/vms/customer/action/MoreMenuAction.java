/**
 * 
 */
package com.fg.vms.customer.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.model.WorkflowConfiguration;

/**
 * @author gpirabu
 * 
 */
public class MoreMenuAction extends DispatchAction {

	public ActionForward moreVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		WorkflowConfigurationDaoImpl configurationDaoImpl = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration workflowConfigurations = configurationDaoImpl.listWorkflowConfig(appDetails);
		if(workflowConfigurations.getFollowWF() !=null && workflowConfigurations.getFollowWF() == 1){
			if(workflowConfigurations.getIsDisplaySdf()!=null){
				session.setAttribute("isDisplaySDF", workflowConfigurations.getIsDisplaySdf());
				}else{
					session.setAttribute("isDisplaySDF", 0);
				}
			
		}else
		{
			session.setAttribute("isDisplaySDF", 1);	
		}
		
		return mapping.findForward("moreVendor");
	}

	public ActionForward moreAdmin(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		return mapping.findForward("moreAdmin");
	}

	public ActionForward moreReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		return mapping.findForward("moreReport");
	}

}
