/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.common.Country;
import com.fg.vms.customer.dao.AnonymousVendorDao;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.AnonymousVendorDaoImpl;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerBusinessType;
import com.fg.vms.customer.model.CustomerCertificateBusinesAreaConfig;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerLegalStructure;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.VendorUtil;

/**
 * This action class used for navigation between self registered vendor profile
 * menus back and forth.
 * 
 * @author vinoth
 * 
 */
public class SelfVendorAction extends DispatchAction {

	private Logger logger = Constants.logger;

	/**
	 * Method for navigate contact page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward contactNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor contact page");
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorDao vendorDao = new VendorDaoImpl();
		SearchDao searchDao = new SearchDaoImpl();
		// load password security questions list
		List<SecretQuestion> securityQnsList;
		Integer vendorId = null;
		List<VendorContact> contacts = null;
		VendorContact contact = null;
		List<VendorContact> businessContacts = null;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		CURDDao<?> cdao = new CURDTemplateImpl();
		securityQnsList = searchDao.listSecQns(userDetails);
		// populate countries list
		List<Country> countries = (List<Country>) cdao.list(userDetails,
				" From Country where isactive=1 order by countryname");
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		vendorForm.setOtherExpiryDate(null);
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		CURDDao<VendorContact> contactdao = new CURDTemplateImpl<VendorContact>();
		List<State> contactStates = dao.list(userDetails,
				" From State order by statename");
		vendorForm.setContactStates(contactStates);
		vendorForm.setId(vendorId);
		contacts = contactdao.list(userDetails,
				"from VendorContact where isPreparer = 1 and vendorId ="
						+ vendorId);
		if (null != contacts) {
			contact = contacts.get(0);
			vendorForm = VendorUtil.packVendorContactInfomation(vendorForm,
					contact);
		}
		businessContacts = vendorDao.getBusinessContactDetails(vendorId,
				userDetails);
		if (businessContacts != null) {
			VendorContact contact1 = businessContacts.get(0);
			vendorForm = VendorUtil.packVendorBusinessContactInfomation(
					vendorForm, contact1);
		} else {
			vendorForm.setPreparerFirstName(contact.getFirstName());
			vendorForm.setPreparerLastName(contact.getLastName());
			vendorForm.setPreparerTitle(contact.getDesignation());
			vendorForm.setPreparerPhone(contact.getPhoneNumber());
			vendorForm.setPreparerMobile(contact.getMobile());
			vendorForm.setPreparerFax(contact.getFax());
			vendorForm.setPreparerEmail(contact.getEmailId());
		}
		session.setAttribute("secretQnsList", securityQnsList);
		session.setAttribute("countryList", countries);

		return mapping.findForward("success");
	}

	/**
	 * Method for navigate company information page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward companyInfoNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor company information page");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorDao vendorDao = new VendorDaoImpl();
		CURDDao<?> cdao = new CURDTemplateImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				vendorId, vendorMaster, vendorCertificate, userDetails);
		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
				.list(userDetails,
						"From CustomerBusinessType where isactive=1 order by typeName");
		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
				.list(userDetails,
						"From CustomerLegalStructure where isactive=1 order by name");
		vendorForm.setLegalStructures(legalStructures);
		vendorForm.setBusinessTypes(businessTypes);
		vendorForm.setCompanytype(vendorMaster.getCompanyType());
		return mapping.findForward("companyinfo");
	}

	/**
	 * Method for navigate company address page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward companyAddressNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor company address page");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		
		String query = "From Country where isactive=1 order by countryname";
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}		
		List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
		session.setAttribute("countryList", countries);		
		
		Integer countryId = null;
		List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao1.list(userDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();
		
		List<State> statesList = null;
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
			}
		}		
		vendorForm.setPhyStates1(statesList);
		vendorForm.setMailingStates2(statesList);

		CustomerVendorAddressMaster phyAddress = documentsDao
				.getMailingAddress(vendorId, "p", userDetails);
		if (phyAddress != null) {
			vendorForm.setAddress1(phyAddress.getAddress());
			vendorForm.setCity(phyAddress.getCity());
			vendorForm.setState(phyAddress.getState());
			vendorForm.setProvince(phyAddress.getProvince());
			vendorForm.setRegion(phyAddress.getRegion());
			vendorForm.setCountry(phyAddress.getCountry());
			vendorForm.setZipcode(phyAddress.getZipCode());
			vendorForm.setMobile(phyAddress.getMobile());
			vendorForm.setPhone(phyAddress.getPhone());
			vendorForm.setFax(phyAddress.getFax());
			if (null != phyAddress.getCountry() && !phyAddress.getCountry().isEmpty()) {
				List<State> phyStates = null;				
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
					} else {
						phyStates = dao.list(userDetails, " From State where countryid=" + phyAddress.getCountry() + " and isactive=1 order by statename");
					}
				}
				vendorForm.setPhyStates1(phyStates);
			}
		}
		CustomerVendorAddressMaster addressMaster = documentsDao
				.getMailingAddress(vendorId, "m", userDetails);
		if (addressMaster != null) {
			vendorForm.setAddress2(addressMaster.getAddress());
			vendorForm.setCity2(addressMaster.getCity());
			vendorForm.setState2(addressMaster.getState());
			vendorForm.setProvince2(addressMaster.getProvince());
			vendorForm.setRegion2(addressMaster.getRegion());
			vendorForm.setCountry2(addressMaster.getCountry());
			vendorForm.setZipcode2(addressMaster.getZipCode());
			vendorForm.setMobile2(addressMaster.getMobile());
			vendorForm.setPhone2(addressMaster.getPhone());
			vendorForm.setFax2(addressMaster.getFax());
			if (null != addressMaster.getCountry() && !addressMaster.getCountry().isEmpty()) {
				List<State> mStates = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						mStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
					} else {
						mStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
					}
				}
				vendorForm.setMailingStates2(mStates);
			}
		}
		return mapping.findForward("addressinfo");
	}

	/**
	 * Method for navigate company owner page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward companyOwnerNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor company owner page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				vendorId, vendorMaster, vendorCertificate, userDetails);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities",
				ethnicityDao.list(userDetails, null));
		if (null != vendorForm.getCompanyOwnership()
				&& !vendorForm.getCompanyOwnership().isEmpty()) {
			vendorForm.setOwnerPublic("1");
			vendorForm.setOwnerPrivate("2");
			vendorForm.setCompanyOwnership(vendorForm.getCompanyOwnership());
		} else {
			vendorForm.setOwnerPublic("1");
			vendorForm.setOwnerPrivate("2");
			vendorForm.setCompanyOwnership(null);
		}
		return mapping.findForward("ownerinfo");
	}

	/**
	 * Method for navigate service area page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward serviceAreaNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor service area page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				vendorId, vendorMaster, vendorCertificate, userDetails);
		
		/*
		 * List of Vendor Keywords.
		 */
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		List<CustomerVendorKeywords> keywords = null;
		keywords = documentsDao.getVendorKeywords(vendorId, userDetails);
		session.setAttribute("vendorKeywords", keywords);
		return mapping.findForward("servicearea");
	}

	/**
	 * Method for navigate business area page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward businessAreaNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor business area page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();

		/**
		 * Get data from vendor master.
		 */
		vendorForm.setCompanyInformation(vendorMaster.getCompanyInformation());
		vendorForm.setVendorDescription(vendorMaster.getVendorDescription());

		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
				 
		// Show the list of Vendor Commodities.
		/*We need this code, Please Don't Remove Even in New Architecture.*/
		List<CustomerVendorCommodity> vendorCommodities = null;		
		vendorCommodities = documentsDao.getVendorCommodities(vendorId, userDetails);
		List<CommodityDto> commodities=documentsDao.listVendorCommodities(vendorId, userDetails);
		session.setAttribute("vendorCommodities", commodities);
//		session.setAttribute("vendorCommodities", vendorCommodities);

		/*
		 * Show list of service areas.
		 */
		List<GeographicalStateRegionDto> serviceAreas1 = null;
		serviceAreas1 = regionDao.getServiceAreasList(userDetails);
		session.setAttribute("serviceAreas1", serviceAreas1);

		/*
		 * Show list of business areas.
		 */
		CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
		List<CustomerServiceArea> serviceAreaList = curdDao.list(userDetails,
				" from CustomerServiceArea ");
		session.setAttribute("serviceAreaList", serviceAreaList);

		/*
		 * Get the selected vendor service and business area.
		 */
		CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
		List<VendorBusinessArea> businessAreas = curdDao1.list(userDetails,
				"from VendorBusinessArea where isActive = 1 and vendorId="
						+ vendorId);
		CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
		List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
				userDetails,
				"from CustomerVendorServiceArea where isActive = 1 and vendorId="
						+ vendorId);
		// Get vendor service area details
		vendorForm = VendorUtil.packVendorServiceAreas(vendorForm,
				serviceAreas, vendorId);
		// Get vendor business area details
		vendorForm = VendorUtil.packVendorBusinessAreas(vendorForm,
				businessAreas, vendorId);

		/*
		 * Get the list of certificate business area configuration for apply
		 * business rules in business area selection.
		 */
		CURDDao<CustomerCertificateBusinesAreaConfig> dao = new CURDTemplateImpl<CustomerCertificateBusinesAreaConfig>();
		List<CustomerCertificateBusinesAreaConfig> businessAreaconfig = dao
				.list(userDetails,
						"from CustomerCertificateBusinesAreaConfig where isActive = 1");
		session.setAttribute("businessAreaconfig", businessAreaconfig);

		/*
		 * Diverse certificate list of vendor
		 */
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(userDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);
		
		//To Get BP Market Sector List
		CommodityDaoImpl commodityDao=new CommodityDaoImpl();
		String query = "From MarketSector where isActive=1 order by sectorDescription";
		List<MarketSector> marketSectors = commodityDao.marketSectorAndCommodities(userDetails, query);
		if(marketSectors != null) {
			session.setAttribute("marketsectors", marketSectors);
		}

		return mapping.findForward("businessarea");
	}

	/**
	 * Method for navigate diverse classification page from menu and back
	 * button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward diverseClassificationNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor diverse classification page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		/**
		 * Load vendor data
		 */
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				vendorId, vendorMaster, vendorCertificate, userDetails);
		vendorForm.setId(vendorId);
		// get the list of certificates by diverse.
		List<Certificate> certificates = null;
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificates = certificate.listOfCertificatesByType((byte) 1,
				userDetails);
		session.setAttribute("certificateTypes", certificates);
		// Get vendor diverse classification list
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(userDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);
		// Certificate Type
		CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypesList = curdDao2.list(
				userDetails, " from CustomerCertificateType ");
		session.setAttribute("certificateTypesList", certificateTypesList);
		// certificate agencies list.
		SearchDao searchDao = new SearchDaoImpl();
		List<CertifyingAgency> certificateAgencies;
		certificateAgencies = searchDao.listAgencies(userDetails);
		session.setAttribute("certAgencyList", certificateAgencies);
		// ethnicities list
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities",
				ethnicityDao.list(userDetails, null));
		return mapping.findForward("diverseclassification");
	}

	/**
	 * Method for navigate biography and safety page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward biographyNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor biography and safety page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		vendorForm.setId(vendorId);
		vendorForm = VendorUtil.packBioGraphySafety(vendorForm, vendorId,
				userDetails);
		return mapping.findForward("businessbiography");
	}

	/**
	 * Method for navigate references page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward referencesNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor biography and safety page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		vendorForm.setId(vendorId);
		
		CURDDao<?> dao = new CURDTemplateImpl();
		
		String query = "From Country where isactive=1 order by countryname";
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}		
		List<com.fg.vms.customer.model.Country> countries = (List<com.fg.vms.customer.model.Country>) dao.list(userDetails, query);
		session.setAttribute("countryList", countries);
		
		Integer countryId = null;
		List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao.list(userDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();		

		List<State> statesList = null;
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
			}
		}
		vendorForm.setRefStates1(statesList);
		vendorForm.setRefStates2(statesList);
		vendorForm.setRefStates3(statesList);
		
		VendorDao vendorDao = new VendorDaoImpl();
		List<CustomerVendorreference> vendorreferences = vendorDao.getListReferences(vendorId, userDetails);
		vendorForm = VendorUtil.packVendorReference(vendorForm, vendorreferences, userDetails, countryId);
		return mapping.findForward("references");
	}

	/**
	 * Method for navigate to other quality certificates page from menu and back
	 * button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward otherCertificateNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor other quality certificates page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		vendorForm.setId(vendorId);
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorOtherCertificate> otherCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				vendorId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		otherCertificate = vendorInfo.getVendorOtherCert();
		List<VendorCertificate> vendorCetrificate = null;
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCetrificate = vendorInfo.getVendorCertificate();
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				vendorId, vendorMaster, vendorCetrificate, userDetails);
		session.setAttribute("vendorInfoDto", vendorInfo);
		session.setAttribute("otherCertificates", otherCertificate);
		List<Certificate> qualityCertificates = null;
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		qualityCertificates = certificate.listOfCertificatesByType((byte) 0,
				userDetails);
		session.setAttribute("qualityCertificates", qualityCertificates);
		return mapping.findForward("othercertificate");
	}

	/**
	 * Method for navigate to vendor documents page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward documentsNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor documents page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		vendorForm.setId(vendorId);
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		/*
		 * List of vendor documents.
		 */
		List<VendorDocuments> documents = null;
		documents = documentsDao.getVendorDocuments(vendorId, userDetails);
		session.setAttribute("vendorDocuments", documents);
		WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration configuration = configurationDao
				.listWorkflowConfig(userDetails);
		vendorForm.setConfiguration(configuration);
		int uploadRestriction = configuration.getDocumentUpload();		
		session.setAttribute("uploadRestriction", uploadRestriction);
		int documentMaxSize = configuration.getDocumentSize();
		session.setAttribute("documentMaxSize", documentMaxSize);
		
		return mapping.findForward("documents");
	}

	/**
	 * Method for navigate to vendor contact meeting information page from menu
	 * and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward contactMeetingNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show self registered vendor meeting information page..");
		HttpSession session = request.getSession();
		Integer vendorId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		vendorForm.setId(vendorId);
		CURDDao<CustomerVendorMeetingInfo> curdDao = new CURDTemplateImpl<CustomerVendorMeetingInfo>();
		List<CustomerVendorMeetingInfo> vendorMeetingInfo = curdDao.list(
				userDetails, "from CustomerVendorMeetingInfo where vendorId="
						+ vendorId);
		if (null != vendorMeetingInfo && vendorMeetingInfo.size() > 0) {
			CustomerVendorMeetingInfo meetingInfo = vendorMeetingInfo.get(0);
			vendorForm = VendorUtil.packVendorContactMeetingInfomation(
					vendorForm, meetingInfo);
		}
		
		//Always Display US States as Default Contact States.
		CURDDao<?> dao = new CURDTemplateImpl();
		Integer countryId = null;
		List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao.list(userDetails,"From Country where countryname = 'United States'");
		countryId = country.get(0).getId();		

		List<State> contactStates = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");		
		session.setAttribute("contactStates", contactStates);
		return mapping.findForward("meetinginfo");
	}

	/**
	 * Method for navigate submit self registration page from menu and back
	 * button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward submitNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Show self registered vendor submit registration page..");
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		
		//Getting Terms & Condition from Database, and Display It in the View Page.
		CURDDao<?> cdao1 = new CURDTemplateImpl();
		List<VendorContact> vendorContact = (List<VendorContact>) cdao1.list(userDetails,
						" From VendorContact where vendorId=" + vendorId);
		String firstName = (null != vendorContact.get(0).getFirstName()) ? vendorContact.get(0).getFirstName() : "";
		String lastName = (null != vendorContact.get(0).getLastName()) ? vendorContact.get(0).getLastName() : ""; 
		String name = firstName + " " + lastName;
		String designation = (null != vendorContact.get(0).getDesignation()) ? vendorContact.get(0).getDesignation() : "";
		
		if(null != userDetails && null != userDetails.getSettings())
		{
			if(null != userDetails.getSettings().getTermsCondition())
			{		
				String TC = userDetails.getSettings().getTermsCondition();				
				TC = TC.replace("#NAME", name);
				TC = TC.replace("#DESIGNATION", designation);
				vendorForm.setTermsCondition(TC);
			}
		}

		return mapping.findForward("submitregistration");
	}

	/**
	 * Method for submit self registration prime vendor data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward createPrimeVendor(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("save self registered prime vendor data.");
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result = null;
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer userId = -1;
		if (null != vendorForm.getId()) {
			result = anonymousVendorDao.savePrimeVendorData(userDetails,
					vendorForm, vendorForm.getId(), userId);
		}

		if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);

			return mapping.getInputForward();
		}
		return mapping.findForward("submitregistration");
	}
}
