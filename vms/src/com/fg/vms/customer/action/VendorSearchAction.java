/**
 * 
 */
package com.fg.vms.customer.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.parser.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CertificateDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.ReportSearchFieldsDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerSearchFilter;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.SegmentMaster;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SupplierType;

/**
 * Represent the controller of different type of supplier search functionality.
 * 
 * @author pirabu
 * 
 */
public class VendorSearchAction extends DispatchAction {

	private Logger logger = Constants.logger;

	private List<SearchVendorDto> vendorSearchReportList;

	/**
	 * Show Mail Notification page
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showVendorSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		SearchDao searchDao = new SearchDaoImpl();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchVendorForm searchVendorForm=(SearchVendorForm)form;
		session.removeAttribute("searchVendorsList");

		if (request.getParameter("searchType") != null) {
			char searchType = request.getParameter("searchType").charAt(0);
			Integer userId = (Integer) session.getAttribute("userId");
			List<CustomerSearchFilter> previousSearchDetailsList = searchDao
					.listPreviousSearchDetails(appDetails, searchType, userId);
			session.setAttribute("previousSearchList",
					previousSearchDetailsList);
		}

		vendorSearchReportList = new ArrayList<SearchVendorDto>();
		CertificateDao certificate = new CertificateDaoImpl();
		List<Certificate> certificates = null;
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails); // List of certificate types.
		List<CertifyingAgency> certificateAgencies = searchDao
				.listAgencies(appDetails);
		session.setAttribute("certificateTypes", certificates);
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		CURDDao<com.fg.vms.customer.model.StatusMaster> statusDao = new CURDTemplateImpl<com.fg.vms.customer.model.StatusMaster>();
		List<com.fg.vms.customer.model.StatusMaster> vendorStatusList = statusDao
				.list(appDetails,
						" From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");
		List<com.fg.vms.customer.model.StatusMaster> vendorStatus = new ArrayList<StatusMaster>();
		if (vendorStatusList != null && !vendorStatusList.isEmpty()) {
			for (StatusMaster master : vendorStatusList) {
				if (master.getId().equals('A') || master.getId().equals('B')) {
					vendorStatus.add(master);
				}
			}
		}

		/*
		 * Show list of Business areas.
		 */
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<GeographicalStateRegionDto> businessAreas = null;

		businessAreas = regionDao.getServiceAreasList(appDetails);
		session.setAttribute("businessAreas", businessAreas);

		/*
		 * Show list of commodity description.
		 */
		List<GeographicalStateRegionDto> commodityDescription = null;

		commodityDescription = regionDao.getCommodityDescription(appDetails);
		session.setAttribute("commodityDescription", commodityDescription);

		/*
		 * Show list of naics Code.
		 */
		/*
		 * List<NaicsCode> naicsCodeList=null;
		 * 
		 * naicsCodeList=regionDao.getNaicsCode(appDetails);
		 * session.setAttribute("naicsCodeList", naicsCodeList);
		 */
		/*
		 * Show list of certification type.
		 */
		List<GeographicalStateRegionDto> certificationTypes = null;

		certificationTypes = regionDao.getCertificationType(appDetails);
		session.setAttribute("certificationTypes", certificationTypes);
		String ar[] =new String[3];
		int index = 0;
		for(GeographicalStateRegionDto c:certificationTypes){
			if(c.getCertficateTypeId()!=null){
			if(c.getCertficateTypeDescription().equals("NGLCC") || c.getCertficateTypeDescription().equals("WBENC") || c.getCertficateTypeDescription().equals("NMSDC")){
				
				ar[index]=String.valueOf(c.getCertficateTypeId());
				index++;
				}
			}
		}
		try{		
		searchVendorForm.setCertificationType(ar);
		}catch(Exception ex){
			System.out.println(ex);
		}
		
		// Show List Commodity Group with Market Sector, Market SubSector, &
		// Commodity Description Hierarchy
		List<GeographicalStateRegionDto> vendorCommodity = null;

		vendorCommodity = regionDao.getCommodityGroup(appDetails);
		session.setAttribute("vendorCommodity", vendorCommodity);

		CURDDao<com.fg.vms.customer.model.State> daoContactStates = new CURDTemplateImpl<com.fg.vms.customer.model.State>();
		List<com.fg.vms.customer.model.State> contactStates = daoContactStates
				.list(appDetails,
						"From State where isactive=1 order by statename");
		session.setAttribute("contactStates", contactStates);

		request.setAttribute("certifyingAgencies", certificateAgencies);
		request.setAttribute("countries", countries);
		session.setAttribute("vendorStatus1", vendorStatus);
		session.setAttribute("vendorStatusList", vendorStatusList);
		session.setAttribute("saveCriteria", 1);

		CURDDao<SegmentMaster> curdDao1 = new CURDTemplateImpl<SegmentMaster>();
		List<SegmentMaster> segmentMasters = curdDao1.list(appDetails,
				" From SegmentMaster order by segmentName");
		session.setAttribute("bpSegmentSearchList", segmentMasters);

		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorsList = marketSectorDao
				.list(appDetails,
						"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorsList", marketSectorsList);

		return mapping.findForward("Success");
	}

	/**
	 * Implementing code for vendor searching
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward vendorSearch(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		vendorSearchReportList = new ArrayList<SearchVendorDto>();
		SearchDao searchDao = new SearchDaoImpl();
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		session.removeAttribute("vendorSearchSummary");
		String searchPageType = request.getParameter("searchPageType");
		String filterType = request.getParameter("filterType");
		String searchType = request.getParameter("type");

		Integer userId = (Integer) session.getAttribute("userId");
		String userName = searchDao.findUserNameByUserId(appDetails, userId);
		logger.info("Search Start Time Before DB:" + new Date() + ", Username:"
				+ userName);

		List<SearchVendorDto> vendorsList = null;

		if (searchPageType.equalsIgnoreCase("searchPrimeVendorProgress")) {
			session.setAttribute("searchtype", "vendorStatus");
		} else {
			session.setAttribute("searchtype", "vendorcriteria");
		}

		// set idDivision Status
		/*
		 * CustomerApplicationSettings settings = (CustomerApplicationSettings)
		 * session.getAttribute("isDivisionStatus");
		 * searchVendorForm.setIsDivision(settings.getIsDivision()); // set
		 * customer division id if (settings.getIsDivision() != 0 &&
		 * settings.getIsDivision() != null) {
		 * searchVendorForm.setCustomerDivisionId((Integer)
		 * session.getAttribute("customerDivisionId")); }
		 */

		if (filterType != null && filterType.equalsIgnoreCase("nonNMSDCWBENC")) { // Filtering
																					// Non-NMSDCWBENC
			List<String> searchCriterias = (List<String>) session
					.getAttribute("searchCriteria");
			searchVendorForm.setSearchCriterias(searchCriterias);

			String searchQuery = (String) session.getAttribute("searchQuery");
			searchVendorForm.setSearchQuery(searchQuery);

			String nonNMSDCWBENCQuery = (String) session
					.getAttribute("nonNMSDCWBENCSearchQuery");
			searchVendorForm.setNonNMSDCWBENCQuery(nonNMSDCWBENCQuery);

			vendorsList = searchDao.searchVendorFilteredByNonNMSDCWBENC(
					searchVendorForm, appDetails);
		} else { // For Others
			vendorsList = searchDao.searchVendor(searchVendorForm, 1,
					SupplierType.valueOf(searchVendorForm.getVendorType())
							.getIndex(), appDetails);
		}
		setVendorSearchReportList(vendorsList);

		Integer toatlVendorCount = 0;
		String searchSummary = "";

		if (searchPageType != null) {
			toatlVendorCount = searchDao.totalVendorCount(searchVendorForm,
					appDetails, searchPageType);
		}
		if (vendorsList.size() > 0)
			searchSummary = "Total match found, " + vendorsList.size()
					+ " out of " + toatlVendorCount + "";

		searchVendorForm.setVendorsList(vendorsList);

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				"From Country where isactive=1 order by countryname");
		searchVendorForm.setCountries(countries);
		searchVendorForm.setDiverseCertificateNames(null);
		searchVendorForm.setSelectCountries(null);
		searchVendorForm.setCertifyAgencies(null);

		session.setAttribute("searchVendorsList", vendorsList);
		session.setAttribute("vendorSearchSummary", searchSummary);
		session.setAttribute("searchCriteria",
				searchVendorForm.getSearchCriterias());
		session.setAttribute("criteriaJsonString",searchVendorForm.getCriteriaJsonString());
		session.setAttribute("searchQuery", searchVendorForm.getSearchQuery());

		// ------------------------- Codes for Non-NMSDC/WBENC Search Option
		// Starts Here -------------------------
		session.setAttribute("nonNMSDCWBENCSearchQuery",
				searchVendorForm.getNonNMSDCWBENCQuery());

		// To Get List of Certificates
		CertificateDao certificateDaoImpl = new CertificateDaoImpl();
		List<Certificate> certificates = null;
		certificates = certificateDaoImpl.listOfCertificates(appDetails);
		session.setAttribute("certificates", certificates);

		// To Get List of Certifying Agencies
		List<CertifyingAgency> certificateAgencies = null;
		certificateAgencies = searchDao.listAgencies(appDetails);
		session.setAttribute("certAgencyList", certificateAgencies);

		// ------------------------- Codes for Non-NMSDC/WBENC Search Option
		// Ends Here -------------------------

		if (searchVendorForm.getPrimeVendorStatus() != null
				&& searchVendorForm.getPrimeVendorStatus()
						.equalsIgnoreCase("P"))
			session.setAttribute("pendingReview", 1);
		else
			session.setAttribute("pendingReview", 0);

		if (searchType != null && searchType.equalsIgnoreCase("customize")) {
			session.setAttribute("searchConditions",
					searchVendorForm.getSearchConditions());
			session.setAttribute("whereConditionTableList",
					searchVendorForm.getFromtableForConditions());
		}

		logger.info("Search End Time After DB:" + new Date() + ", Username:"
				+ userName);

		return mapping.findForward("searchresult");
	}

	/**
	 * Show Mail Notification page
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward showSubVendorSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		// NaicsCategoryDao naicsCategoryDao = new NaicsCategoryDaoImpl();
		// List<NaicsCategoryDto> naicsCategories = naicsCategoryDao
		// .view(appDetails);
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		searchVendorForm.setVendorsList(vendorsList);
		// searchVendorForm.setCategories(naicsCategories);
		searchVendorForm.setCountries(countries);

		CertificateDaoImpl certificate = new CertificateDaoImpl();

		List<Certificate> certificates = null;
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails); // List of certificate types.
		session.setAttribute("certificateTypes", certificates);

		return mapping.findForward("showSubVendor");
	}

	/**
	 * Implementing the code for display the list type of vendors
	 * 
	 * @return the viewvendors view page
	 */
	public ActionForward displayVendors(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.info("Indide the Display vendors method...");
		List<VendorMaster> vendorMaster = null;
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<CertifyingAgency> certificateAgencies;
		List<Certificate> certificates = null;
		VendorDao vendorDao = new VendorDaoImpl();
		SearchDao searchDao = new SearchDaoImpl();
		vendorMaster = vendorDao.listVendors(appDetails);
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificateAgencies = searchDao.listAgencies(appDetails);
		certificates = certificate.listOfCertificates(appDetails);
		session.setAttribute("certAgencyList", certificateAgencies);
		session.setAttribute("certificateTypes", certificates);
		session.setAttribute("vendorsList", vendorMaster);

		return mapping.findForward("viewvendors");
	}

	/**
	 * Implementing code for sub vendor searching
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward subVendorSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<SearchVendorDto> vendorsList = null;

		VendorContact vendorUser = (VendorContact) session
				.getAttribute("vendorUser");
		Integer vendorId = vendorUser.getVendorId().getId();

		SearchDao searchDao = new SearchDaoImpl();
		vendorsList = searchDao.searchTire2Vendor(searchVendorForm, vendorId,
				1, appDetails);
		searchVendorForm.setVendorsList(vendorsList);

		CURDDao<?> dao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countries = (List<Country>) dao
				.list(appDetails,
						" From Country where isactive=1 order by countryname");
		List<State> states = (List<State>) dao.list(appDetails,
				" From State where countryid=" + searchVendorForm.getCountry());
		searchVendorForm.setStates(states);
		searchVendorForm.setCountries(countries);
		return mapping.findForward("showSubVendor");
	}

	public ActionForward getState(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String countryId = request.getParameter("id");
		if (countryId != null && !countryId.isEmpty()) {
			Integer id = Integer.parseInt(countryId);
			CURDDao<State> curdDao = new CURDTemplateImpl<State>();
			List<State> states = curdDao.list(appDetails,
					" From State where countryid=" + id + " and isactive=1");

			session.setAttribute("states", states);
		}
		return mapping.findForward("Success");
	}

	public ActionForward getStateByDefaultCountry(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Integer countryId = null;
		CURDDao<Country> curdCountryDao = new CURDTemplateImpl<Country>();
		List<Country> country = curdCountryDao.list(appDetails,
				"From Country where isDefault = 1");
		countryId = country.get(0).getId();

		if (null != countryId) {
			CURDDao<State> curdStateDao = new CURDTemplateImpl<State>();
			List<State> states = curdStateDao.list(appDetails,
					"From State where countryid=" + countryId
							+ " and isactive=1");
			session.setAttribute("states", states);
		}
		return mapping.findForward("Success");
	}

	public ActionForward getStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String statusId = request.getParameter("id");
		if (statusId != null && !statusId.isEmpty()) {
			// Integer id = Integer.parseInt(statusId);
			CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
			// select * from statusmaster s where s.disporder >( select
			// disporder from statusmaster where id='P') order by s.disporder

			List<StatusMaster> statusMasters = null;
			if (statusId.equalsIgnoreCase("I")) {
				statusMasters = curdDao
						.list(appDetails,
								"From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");
			} else {
				statusMasters = curdDao
						.list(appDetails,
								" From StatusMaster s where s.disporder > ( select disporder from StatusMaster where id='"
										+ statusId
										+ "' ) and id != 'S' order by s.disporder");
			}

			session.setAttribute("status", statusMasters);
		}
		return mapping.findForward("Success");
	}

	/**
	 * Show vendor search page for full text.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showVendorFullSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		vendorSearchReportList = new ArrayList<SearchVendorDto>();
		/*
		 * forward to full text search page.
		 */
		HttpSession session = request.getSession();
		session.setAttribute("searchtype", "keyword");
		session.removeAttribute("searchVendorsList");
		return mapping.findForward("fullsearch");
	}

	/**
	 * get vendor search for full text.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getVendorFullSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<SearchVendorDto> vendorsList = null;
		SearchDao searchDao = new SearchDaoImpl();
		session.setAttribute("searchVendorsList", null);
		Integer divisionId = null;
		// Set IsDivision Status

		if (appDetails.getSettings() != null
				&& appDetails.getSettings().getIsDivision() != null
				&& appDetails.getSettings().getIsDivision() != 0) {
			if (appDetails.getCustomerDivisionID() != null
					&& appDetails.getCustomerDivisionID() != 0
					&& appDetails.getIsGlobalDivision() == 0) {
				divisionId = appDetails.getCustomerDivisionID();
			} else
				divisionId = 0;
		} else {
			divisionId = 0;
		}
		if (null != request.getParameter("searchFullText")
				&& !request.getParameter("searchFullText").isEmpty()) {
			vendorsList = searchDao.searchVendorFullText(
					request.getParameter("searchFullText"), appDetails,
					divisionId);
			session.setAttribute("searchVendorsList", vendorsList);
		}
		setVendorSearchReportList(vendorsList);
		return mapping.findForward("Success");
	}

	/**
	 * To use view Prime vendor search page
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showPrimeVendorSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();

		session.removeAttribute("searchVendorsList");

		if (request.getParameter("searchType") != null) {
			char searchType = request.getParameter("searchType").charAt(0);
			Integer userId = (Integer) session.getAttribute("userId");
			List<CustomerSearchFilter> previousSearchDetailsList = searchDao
					.listPreviousSearchDetails(appDetails, searchType, userId);
			session.setAttribute("previousSearchList",
					previousSearchDetailsList);
		}

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");

		request.setAttribute("countries", countries);
		session.setAttribute("saveCriteria", 1);

		return mapping.findForward("primevendorsearch");
	}

	public ActionForward viewTier2VendorSpendReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();

		session.removeAttribute("searchVendorsList");

		/*
		 * if(request.getParameter("searchType") != null) { char searchType =
		 * request.getParameter("searchType").charAt(0); Integer userId =
		 * (Integer) session.getAttribute("userId"); List<CustomerSearchFilter>
		 * previousSearchDetailsList
		 * =searchDao.listPreviousSearchDetails(appDetails,searchType,userId);
		 * session.setAttribute("previousSearchList",
		 * previousSearchDetailsList); }
		 */

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");

		request.setAttribute("countries", countries);
		session.setAttribute("saveCriteria", 1);

		return mapping.findForward("viewtier2vendorspendreport");
	}

	public ActionForward primeVendorSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		HttpSession session = request.getSession();
		SearchDao searchDao = new SearchDaoImpl();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		session.removeAttribute("vendorSearchSummary");

		Integer userId = (Integer) session.getAttribute("userId");
		String userName = searchDao.findUserNameByUserId(appDetails, userId);
		logger.info("Search Start Time Before DB:" + new Date() + ", Username:"
				+ userName);

		List<SearchVendorDto> vendorsList = null;
		// Sets isDivision Status
		Byte isDivisionStatus = appDetails.getSettings().getIsDivision();
		searchVendorForm.setIsDivision(isDivisionStatus);

		// Sets Customer Division Id
		if (isDivisionStatus != 0 && isDivisionStatus != null) {
			searchVendorForm.setCustomerDivisionId((Integer) session
					.getAttribute("customerDivisionId"));
		}

		session.setAttribute("searchtype", "primeVendorSearch");

		vendorsList = searchDao.searchPrimeVendor(searchVendorForm, 1,
				SupplierType.valueOf(searchVendorForm.getVendorType())
						.getIndex(), appDetails);

		Integer toatlVendorCount = 0;
		String searchSummary = "";
		String searchPageType = request.getParameter("searchPageType");
		if (searchPageType != null) {
			toatlVendorCount = searchDao.totalVendorCount(searchVendorForm,
					appDetails, searchPageType);
		}
		if (vendorsList.size() > 0) {
			searchSummary = "Total match found, " + vendorsList.size()
					+ " out of " + toatlVendorCount + "";
		}
		searchVendorForm.setVendorsList(vendorsList);
		setVendorSearchReportList(vendorsList);

		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				"From Country where isactive=1 order by countryname");
		searchVendorForm.setCountries(countries);
		searchVendorForm.setDiverseCertificateNames(null);
		searchVendorForm.setSelectCountries(null);
		searchVendorForm.setCertifyAgencies(null);

		session.setAttribute("searchVendorsList", vendorsList);
		session.setAttribute("vendorSearchSummary", searchSummary);
		session.setAttribute("searchCriteria",
				searchVendorForm.getSearchCriterias());
		session.setAttribute("searchQuery", searchVendorForm.getSearchQuery());

		logger.info("Search End Time After DB:" + new Date() + ", Username:"
				+ userName);

		return mapping.findForward("searchresult");
	}

	public ActionForward tier2VendorSpendReportSearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		HttpSession session = request.getSession();
		SearchDao searchDao = new SearchDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<Tier2ReportDto> reportHistory = null;

		session.removeAttribute("vendorSearchSummary");

		Integer userId = (Integer) session.getAttribute("userId");
		String userName = searchDao.findUserNameByUserId(userDetails, userId);
		logger.info("Search tier2 vendor spend report Start Time Before DB:"
				+ new Date() + ", Username:" + userName);

		VendorContact vendorUser = (VendorContact) session
				.getAttribute("vendorUser");

		ReportDao reportDao = new ReportDaoImpl();
		if(request.getParameter("type") != null && request.getParameter("type").equals("report")){
			reportHistory = (List<Tier2ReportDto>) session.getAttribute("tier2ReportHistory");
		}else{
			reportHistory = searchDao.getSpendReportHistory(
				searchVendorForm, userDetails);
		}
		session.setAttribute("tier2ReportHistory", reportHistory);

		CURDDao<?> dao = new CURDTemplateImpl();
		List<com.fg.vms.customer.model.Country> countryList = (List<com.fg.vms.customer.model.Country>) dao
				.list(userDetails,
						"From Country where isactive=1 and isDefault=1 order by countryname");
		session.setAttribute("countryList", countryList);

		Integer countryId = null;
		countryId = countryList.get(0).getId();

		List<State> statesList = (List<State>) dao.list(userDetails,
				"From State where countryid=" + countryId
						+ " and isactive=1 order by statename");
		session.setAttribute("statesList", statesList);

		logger.info("Search tier2 vendor spend report Start Time Before DB:"
				+ new Date() + ", Username:" + userName);
		if(request.getParameter("type") != null && request.getParameter("type").equals("report")){
		return mapping.findForward("historyData");
		}
		return mapping.findForward("showReportListPage");
	}

	/**
	 * getVendorReport
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward getVendorReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorCriteriaSearch_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			if (getVendorSearchReportList() != null) {
				JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
						getVendorSearchReportList());
				InputStream is = JRLoader
						.getResourceInputStream("vendor_search_report.jasper");
				JasperPrint jasperPrint = JasperFillManager.fillReport(is,
						params, beanCollectionDataSource);
				servletOutputStream = httpServletResponse.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint,
						servletOutputStream);
			} else {
				List nullList = new ArrayList<Object>();
				JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
						nullList);
				InputStream is = JRLoader
						.getResourceInputStream("vendor_search_report.jasper");
				JasperPrint jasperPrint = JasperFillManager.fillReport(is,
						params, beanCollectionDataSource);
				servletOutputStream = httpServletResponse.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint,
						servletOutputStream);
			}
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * getVendorStatusReport
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward getVendorStatusReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "VendorStatusSearch_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getVendorSearchReportList());
			InputStream is = null;

			if (session.getAttribute("pendingReview") != null
					&& session.getAttribute("pendingReview").toString()
							.equalsIgnoreCase("1"))
				is = JRLoader
						.getResourceInputStream("vendor_status_pendingreview_search.jasper");
			else
				is = JRLoader
						.getResourceInputStream("vendor_status_report.jasper");

			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);
			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * getPrimeVendorReport
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward getPrimeVendorReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "PrimeVendorSearch_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());

			if (getVendorSearchReportList() != null) {
				JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
						getVendorSearchReportList());
				InputStream is = JRLoader
						.getResourceInputStream("prime_vendor_search.jasper");
				JasperPrint jasperPrint = JasperFillManager.fillReport(is,
						params, beanCollectionDataSource);
				servletOutputStream = httpServletResponse.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint,
						servletOutputStream);
			} else {
				List nullList = new ArrayList<Object>();
				JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
						nullList);
				InputStream is = JRLoader
						.getResourceInputStream("prime_vendor_search.jasper");
				JasperPrint jasperPrint = JasperFillManager.fillReport(is,
						params, beanCollectionDataSource);
				servletOutputStream = httpServletResponse.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint,
						servletOutputStream);
			}
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	/**
	 * To Generate Inactive Vendor Search Result Pdf File.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward generateInactiveVendorSearchResultPDFFile(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		Date date = new Date();
		String dateTime = CommonUtils.convertDateTimeToString(date);

		OutputStream servletOutputStream = null;

		try {
			String reportName = "InactiveVendorSearch_" + dateTime;
			HttpServletResponse httpServletResponse = response;
			httpServletResponse.setContentType("application/pdf	");
			httpServletResponse.setHeader("Content-Disposition",
					"attachment; filename=" + reportName + ".pdf");

			Map params = new HashMap();
			params.put("logo", Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ "/" + appDetails.getSettings().getLogoPath());
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					getVendorSearchReportList());

			InputStream is = JRLoader
					.getResourceInputStream("inactive_vendor_search_pdf.jasper");

			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					beanCollectionDataSource);
			servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,
					servletOutputStream);

			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			System.out.println("Exception occured - " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward(null);
	}

	public ActionForward generateVendorStatusSearchResultCSVFile(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		try {
			Date date = new Date();
			String dateTime = CommonUtils.convertDateTimeToString(date);
			String reportName = "VendorStatusSearch_" + dateTime;

			if (session.getAttribute("pendingReview") != null
					&& session.getAttribute("pendingReview").toString()
							.equalsIgnoreCase("1")) {
				CommonUtils.generateJasperCSVFile(appDetails, response,
						reportName,
						"vendor_status_pending_review_report_csv.jasper",
						getVendorSearchReportList());
			} else {
				CommonUtils.generateJasperCSVFile(appDetails, response,
						reportName, "vendor_status_report_csv.jasper",
						getVendorSearchReportList());
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	public ActionForward generateVendorCriteriaSearchResultCSVFile(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		try {
			Date date = new Date();
			String dateTime = CommonUtils.convertDateTimeToString(date);
			String reportName = "VendorCriteriaSearch_" + dateTime;

			CommonUtils.generateJasperCSVFile(appDetails, response, reportName,
					"vendor_criteria_report_csv.jasper",
					getVendorSearchReportList());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	public ActionForward generatePrimeVendorSearchResultCSVFile(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		try {
			Date date = new Date();
			String dateTime = CommonUtils.convertDateTimeToString(date);
			String reportName = "PrimeVendorSearch_" + dateTime;

			CommonUtils.generateJasperCSVFile(appDetails, response, reportName,
					"prime_vendor_search_csv.jasper",
					getVendorSearchReportList());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward("Success");
	}

	public ActionForward generateInactiveVendorSearchResultCSVFile(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		try {
			Date date = new Date();
			String dateTime = CommonUtils.convertDateTimeToString(date);
			String reportName = "InactiveVendorSearch_" + dateTime;

			CommonUtils.generateJasperCSVFile(appDetails, response, reportName,
					"inactive_vendor_search_csv.jasper",
					getVendorSearchReportList());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured - " + e.getMessage());
		}
		return mapping.findForward(null);
	}

	public List<SearchVendorDto> getVendorSearchReportList() {
		return vendorSearchReportList;
	}

	public void setVendorSearchReportList(
			List<SearchVendorDto> vendorSearchReportList) {
		this.vendorSearchReportList = vendorSearchReportList;
	}

	/**
	 * searchNaicsCode
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchNaicsCode(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();
		String searchValue = request.getParameter("term");
		List<String> naicsCodes = null;
		naicsCodes = searchDao.getNaicsCodes(userDetails, searchValue);
		request.setAttribute("naicsCodes", naicsCodes);
		return mapping.findForward("naicsdata");
	}

	/**
	 * searchNaicsDescription
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchNaicsDescription(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();
		String searchValue = request.getParameter("term");
		List<String> naicsDesc = null;
		naicsDesc = searchDao.getNaicsDescriptions(userDetails, searchValue);
		request.setAttribute("naicsDesc", naicsDesc);
		return mapping.findForward("naicsdata");
	}

	/**
	 * Search Vendor Name from Inactive Vendor Search for Auto Complete Control.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchVendorNameForAutoComplete(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();
		String searchValue = request.getParameter("term");
		List<String> vendorNameList = null;
		vendorNameList = searchDao.getVendorNamesList(userDetails, searchValue);
		request.setAttribute("vendorNameList",
				StringUtils.join(vendorNameList, ';'));
		return mapping.findForward("naicsdata");
	}

	/**
	 * getPreviousSearchResults
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward getPreviousSearchResults(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();

		String searchType = null;
		if (session.getAttribute("searchtype") != null) {
			searchType = session.getAttribute("searchtype").toString();
		}

		String valuefrom = request.getParameter("valueFrom");
		String previousResult = "yes";
		String result = "vendorstatusresult";

		if (valuefrom.equalsIgnoreCase("wizard")) {
			if (searchType.equalsIgnoreCase("vendorStatus")) {
				result = "vendorstatusresult";
			} else if (searchType.equalsIgnoreCase("vendorcriteria")) {
				result = "criteriasearchresult";
			} else if (searchType.equalsIgnoreCase("primeVendorSearch")) {
				result = "searchprimevendorresult";
			} else if (searchType.equalsIgnoreCase("vendorAllStatus")) {
				/*
				 * UserDetailsDto userDetails = (UserDetailsDto)
				 * session.getAttribute("userDetails"); SearchVendorForm
				 * searchVendorForm = (SearchVendorForm) form; SearchDao
				 * searchDao = new SearchDaoImpl();
				 * 
				 * List<SearchVendorDto> vendorsList = null; vendorsList =
				 * searchDao.searchByVendorStatus(userDetails,
				 * searchVendorForm);
				 * 
				 * Integer toatlVendorCount = 0; String searchSummary = "";
				 * toatlVendorCount =
				 * searchDao.totalVendorCount(searchVendorForm, userDetails,
				 * "statusBySearch");
				 * 
				 * if (vendorsList.size() > 0) searchSummary =
				 * "Total match found, " + vendorsList.size() + " out of " +
				 * toatlVendorCount + "";
				 * 
				 * searchVendorForm.setVendorsList(vendorsList);
				 * 
				 * session.setAttribute("searchVendorsList", vendorsList);
				 * session.setAttribute("vendorSearchSummary", searchSummary);
				 */

				result = "vendorStatusWiseResult";
			} else {
				result = "fullsearch";
			}
		} else {
			result = "searchresult";
			previousResult = "no";
		}
		session.setAttribute("previousResultStatus", previousResult);
		return mapping.findForward(result);
	}

	/**
	 * show searchByStatusRestult page
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchByStatusRestult(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		SearchDao searchDao = new SearchDaoImpl();
		String userName = searchDao.findUserNameByUserId(appDetails, userId);

		logger.info("Search Start Time Before UI Creation:" + new Date()
				+ ", Username:" + userName);
		return mapping.findForward("vendorstatussearchresult");
	}

	/**
	 * show searchByCriteriaRestult page
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchByCriteriaRestult(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		SearchDao searchDao = new SearchDaoImpl();
		String userName = searchDao.findUserNameByUserId(appDetails, userId);

		logger.info("Search Start Time Before UI Creation:" + new Date()
				+ ", Username:" + userName);
		return mapping.findForward("criteriasearchresult");
	}

	/**
	 * Show Prime Vendor Search Results.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchByPrimeVendorRestult(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		SearchDao searchDao = new SearchDaoImpl();
		String userName = searchDao.findUserNameByUserId(appDetails, userId);

		logger.info("Search Start Time Before UI Creation:" + new Date()
				+ ", Username:" + userName);
		return mapping.findForward("searchprimevendorresult");
	}

	/**
	 * saveSearchFilters
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward saveSearchFilters(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		VendorDao vendorDao = new VendorDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		String searchName = request.getParameter("searchName").toString();
		String searchType = request.getParameter("searchType").toString();
		String result;
		CustomerSearchFilter searchFilter = vendorDao.findByFilterName(
				userDetails, searchName, searchType, userId);
		if (searchFilter != null) {
			result = "unique";
		} else {
			StringBuilder criteria = new StringBuilder();
			List<String> criteriaList = (List<String>) session
					.getAttribute("searchCriteria");
			String jsonCriteriaStrig=(String)session.getAttribute("criteriaJsonString");
			for (String value : criteriaList) {
				criteria.append(value + ", ");
			}
			criteria.deleteCharAt(criteria.length() - 1);
			criteria.deleteCharAt(criteria.length() - 1);

			String sqlQuery = session.getAttribute("searchQuery").toString();
			if(jsonCriteriaStrig == null){
			searchFilter = vendorDao.saveSearchFilters(userDetails, searchName,
					searchType, criteria.toString(), sqlQuery, userId);
			}else{
			searchFilter = vendorDao.saveSearchFiltersInJsonForm(userDetails, searchName,
					searchType, criteria.toString(), sqlQuery, userId,jsonCriteriaStrig);
			}
			if (searchFilter != null) {
				session.setAttribute("saveCriteria", 0);
				result = "success";
			} else
				result = "fail";
		}

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", result);
		response.addHeader("Content-Type", "application/json");
		response.getOutputStream().write(
				jsonObject.toString().getBytes("UTF-8"));
		response.getOutputStream().flush();

		return null;

	}

	/**
	 * searchByPreviousFilterData
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchByPreviousFilterData(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String searchId = request.getParameter("searchId");
		String searchPageType = request.getParameter("searchPageType");
		List<SearchVendorDto> vendorsList = null;

		if (searchPageType.equalsIgnoreCase("searchPrimeVendorProgress")) {
			session.setAttribute("searchtype", "vendorStatus");
		} else if (searchPageType.equalsIgnoreCase("searchVendor")) {
			session.setAttribute("searchtype", "vendorcriteria");
		} else if (searchPageType.equalsIgnoreCase("primeVendorSearch")) {
			session.setAttribute("searchtype", "primeVendorSearch");
		}

		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		SearchDao searchDao = new SearchDaoImpl();

		CustomerApplicationSettings settings = (CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");
		searchVendorForm.setIsDivision(settings.getIsDivision());
		// set customer division id
		if (settings.getIsDivision() != 0 && settings.getIsDivision() != null) {
			searchVendorForm.setCustomerDivisionId((Integer) session
					.getAttribute("customerDivisionId"));
		}

		CustomerSearchFilter customerSearchFilter = searchDao
				.getCustomerSearchFilter(userDetails,
						Integer.parseInt(searchId));

		if (searchPageType.equalsIgnoreCase("primeVendorSearch"))
			vendorsList = searchDao.searchByPreviousDataForPrimeVendor(
					userDetails, customerSearchFilter.getSearchQuery());
		else
			vendorsList = searchDao.searchByPreviousData(userDetails,
					customerSearchFilter.getSearchQuery(),
					customerSearchFilter.getSearchType());

		setVendorSearchReportList(vendorsList);

		Integer toatlVendorCount = 0;
		String searchSummary = "";

		if (searchPageType != null) {
			toatlVendorCount = searchDao.totalVendorCount(searchVendorForm,
					userDetails, searchPageType);
		}
		if (vendorsList.size() > 0)
			searchSummary = "Total match found, " + vendorsList.size()
					+ " out of " + toatlVendorCount + "";

		session.setAttribute("searchVendorsList", vendorsList);

		String[] criterias = customerSearchFilter.getCriteria().split(", ");
		List<String> criteriaList = Arrays.asList(criterias);

		session.setAttribute("searchCriteria", criteriaList);
		session.setAttribute("vendorSearchSummary", searchSummary);
		session.setAttribute("saveCriteria", 0);
		return mapping.findForward("searchresult");
	}

	@SuppressWarnings({ "unchecked" })
	public ActionForward exportAllSupplierInformations(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		log.info("In exportAllSupplierInformations Method.");
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		HttpServletResponse httpServletResponse = response;
		httpServletResponse.setContentType("application/vnd.ms-excel");
		httpServletResponse.setHeader("Content-Disposition",
				"attachment; filename=AllVendorDetails.xls");

		SearchDao searchDao = new SearchDaoImpl();
		List<SearchVendorDto> vendorsList = null;
		List<LinkedHashMap<String, Object>> newVendorsList = null;

		try {
			vendorsList = (List<SearchVendorDto>) session
					.getAttribute("searchVendorsList");

			// Excel Export Starts Here
			// Create Workbook
			HSSFWorkbook wb = new HSSFWorkbook();

			String vendorId = "";
			if (null != vendorsList && vendorsList.size() > 0) {
				log.info("Total Vendor List:" + vendorsList.size());
				System.out.println("Total Vendor List:" + vendorsList.size());

				for (int i = 0; i < vendorsList.size(); i++) {
					if (null != vendorsList.get(i).getId()) {
						if (i == 0)
							vendorId += vendorsList.get(i).getId().toString();
						else
							vendorId += ","
									+ vendorsList.get(i).getId().toString();
					}
				}
				log.info("Vendor ID:" + vendorId);
				System.out.println("Vendor ID:" + vendorId);
				newVendorsList = searchDao.searchVendorAllDetails(userDetails,
						vendorId);

				// Set Header Font
				HSSFFont headerFont = wb.createFont();
				headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

				// Set Header Cell Style
				HSSFCellStyle headerStyle = wb.createCellStyle();
				headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				headerStyle
						.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);
				headerStyle.setFillBackgroundColor(HSSFColor.WHITE.index);
				headerStyle.setFont(headerFont);

				if (newVendorsList != null && !newVendorsList.isEmpty()) {
					// Create Sheet
					HSSFSheet sheet1 = wb.createSheet("VENDOR DETAILS");
					int cellNumber = 0;

					// Header will be always in first row.
					HSSFRow headerRow = sheet1.createRow(0);
					for (String key : newVendorsList.get(0).keySet()) {
						HSSFCell cell = headerRow.createCell(cellNumber);
						cell.setCellStyle(headerStyle);
						cell.setCellValue(key.toUpperCase());
						sheet1.autoSizeColumn(cellNumber);
						cellNumber++;
					}

					// Data row starts from second row.
					HSSFRow dataRow = null;
					int rowNumber = 1;
					cellNumber = 0;

					for (LinkedHashMap<String, Object> map : newVendorsList) {
						dataRow = sheet1.createRow(rowNumber);

						for (Map.Entry<String, Object> entry : map.entrySet()) {
							dataRow.createCell(cellNumber).setCellValue(
									(entry.getValue() != null ? entry
											.getValue().toString() : ""));
							// sheet1.autoSizeColumn(cellNumber);
							cellNumber++;
						}
						rowNumber++;
						cellNumber = 0;
					}
				}
			}

			wb.write(httpServletResponse.getOutputStream());
			httpServletResponse.getOutputStream().flush();
			httpServletResponse.getOutputStream().close();
		} catch (Exception ex) {
			System.out.println("Exception:" + ex);
			PrintExceptionInLogFile.printException(ex);
		} finally {
			httpServletResponse.getOutputStream().flush();
			httpServletResponse.getOutputStream().close();
		}
		return null;
	}

	/**
	 * deleteSearchFilter
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteSearchFilter(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		String searchId = request.getParameter("searchId");
		char searchType = request.getParameter("searchType").charAt(0);
		Integer userId = (Integer) session.getAttribute("userId");

		SearchDao searchDao = new SearchDaoImpl();
		VendorDao vendorDao = new VendorDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String jsonData = "";

		int result = vendorDao.deleteSearchFilter(userDetails, searchId);
		if (result > 0) {
			jsonData = "success";
			List<CustomerSearchFilter> previousSearchDetailsList = searchDao
					.listPreviousSearchDetails(userDetails, searchType, userId);
			session.setAttribute("previousSearchList",
					previousSearchDetailsList);
		} else {
			jsonData = "fail";
		}

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", jsonData);
		response.addHeader("Content-Type", "application/json");
		response.getOutputStream().write(
				jsonObject.toString().getBytes("UTF-8"));
		response.getOutputStream().flush();

		return null;
	}

	public ActionForward editSearchFilter(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		SearchDao searchDao = new SearchDaoImpl();
		SearchVendorForm searchVendorForm=(SearchVendorForm)form;
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<CustomerSearchFilter> previousSearchDetailsList = null;
		CustomerSearchFilter custSearchFilter = null;
		session.removeAttribute("searchVendorsList");

		if (request.getParameter("searchType") != null) {
			char searchType = request.getParameter("searchType").charAt(0);
			Integer userId = (Integer) session.getAttribute("userId");
			previousSearchDetailsList = searchDao.listPreviousSearchDetails(
					appDetails, searchType, userId);
			session.setAttribute("previousSearchList",
					previousSearchDetailsList);
		}

		if (previousSearchDetailsList != null) {
			for (CustomerSearchFilter customerSearchFilter : previousSearchDetailsList) {
				if (customerSearchFilter.getId() == Integer.valueOf(request
						.getParameter("searchId")).intValue()) {
					String criteria=customerSearchFilter.getCriteriaJson();
					HashMap<String, ArrayList<String>> map=getMapedObject(criteria);
					ArrayList<String> listValue=null;
					ObjectMapper mapper = new ObjectMapper();
				    for(Map.Entry<String, ArrayList<String>> m:map.entrySet()){
				    	
				    	if(m.getKey().equals("Legal Company Name")){
				    		searchVendorForm.setCompanyName(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("Keyword")){
				    		searchVendorForm.setKeyWord(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("BP Market Sector")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setBpMarketSectorId(m.getValue().toArray(arr));
				    	}
				    	else if(m.getKey().equals("BP Commodity")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setVendorCommodity(m.getValue().toArray(arr));
				    	}
				    	else if(m.getKey().equals("Business Area")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setBusinessAreaName(m.getValue().toArray(arr));
				    	}
				    	else if(m.getKey().equals("State")){
				    		searchVendorForm.setState(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("Certification Type")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setCertificationType(m.getValue().toArray(arr));
				    	}
				    	else if(m.getKey().equals("Diverse Classification")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setDiverseCertificateNames(m.getValue().toArray(arr));
				    	}
				    	else if(m.getKey().equals("First Name")){
				    		searchVendorForm.setFirstName(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("Last Name")){
				    		searchVendorForm.setLastName(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("Capabilities")){
				    		searchVendorForm.setCapabilities(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("Oil and Gas Industry Experience")){
				    		searchVendorForm.setOilAndGas(m.getValue().get(0));
				    	}
				    	else if(m.getKey().equals("BP Segment")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setBpSegmentIds(m.getValue().toArray(arr));
				    	}else if(m.getKey().equals("Duns Number")){
				    		searchVendorForm.setDunsNumber(m.getValue().get(0));
				    	}else if(m.getKey().equals("Tax ID")){
				    		searchVendorForm.setTaxId(m.getValue().get(0));
				    	}else if(m.getKey().equals("Number of Employees")){
				    		searchVendorForm.setNoOfEmployee(m.getValue().get(0));
				    	}else if(m.getKey().equals("Company Type")){
				    		searchVendorForm.setCompanyType(m.getValue().get(0));
				    	}else if(m.getKey().equals("Address")){
				    		searchVendorForm.setAddress(m.getValue().get(0));
				    	}else if(m.getKey().equals("Province")){
				    		searchVendorForm.setProvince(m.getValue().get(0));
				    	}else if(m.getKey().equals("Region")){
				    		searchVendorForm.setRegion(m.getValue().get(0));
				    	}else if(m.getKey().equals("Zip Code")){
				    		searchVendorForm.setZipCode(m.getValue().get(0));
				    	}else if(m.getKey().equals("Mobile")){
				    		searchVendorForm.setMobile(m.getValue().get(0));
				    	}else if(m.getKey().equals("Phone")){
				    		searchVendorForm.setPhone(m.getValue().get(0));
				    	}else if(m.getKey().equals("Fax")){
				    		searchVendorForm.setFax(m.getValue().get(0));
				    	}else if(m.getKey().equals("Website URL")){
				    		searchVendorForm.setWebSiteUrl(m.getValue().get(0));
				    	}else if(m.getKey().equals("Email")){
				    		searchVendorForm.setEmail(m.getValue().get(0));
				    	}else if(m.getKey().equals("Revenue Range")){
				    		searchVendorForm.setMinRevenueRange(Double.parseDouble(m.getValue().get(0)));
				    		searchVendorForm.setMaxRevenueRange(Double.parseDouble(m.getValue().get(1)));
				    	}else if(m.getKey().equals("Year Of Establishment")){
				    		searchVendorForm.setYearRelationId(m.getValue().get(0));
				    		searchVendorForm.setYear(Integer.parseInt(m.getValue().get(1)));
				    	}else if(m.getKey().equals("RFI RFP Note")){
				    		searchVendorForm.setRfiRfpNote(m.getValue().get(0));
				    	}else if(m.getKey().equals("NAICS Code")){
				    		searchVendorForm.setNaicCode(m.getValue().get(0));
				    	}else if(m.getKey().equals("NAICS Desc")){
				    		searchVendorForm.setNaicsDesc(m.getValue().get(0));
				    	}else if(m.getKey().equals("Certifying Agency")){
				    		listValue=m.getValue();
				    		String [] arr = listValue.toArray(new String[listValue.size()]);
				    		searchVendorForm.setCertificateAgencyIds(m.getValue().toArray(arr));
				    	}else if(m.getKey().equals("  Certification #")){
				    		searchVendorForm.setCertificationNumber(m.getValue().get(0));
				    	}else if(m.getKey().equals("Effective Date")){
				    		searchVendorForm.setEffectiveDate(m.getValue().get(0));
				    	}else if(m.getKey().equals("Expire Date")){
				    		searchVendorForm.setExpireDate(m.getValue().get(0));
				    	}else if(m.getKey().equals("Diversity Classification Notes")){
				    		searchVendorForm.setDiversClassificationNotes(m.getValue().get(0));
				    	}else if(m.getKey().equals("Designation")){
				    		searchVendorForm.setTitle(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Phone")){
				    		searchVendorForm.setContactPhone(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Mobile")){
				    		searchVendorForm.setContactMobile(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Fax")){
				    		searchVendorForm.setContactFax(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Email")){
				    		searchVendorForm.setContactEmail(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact First Name")){
				    		searchVendorForm.setContactFirstName(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Last Name")){
				    		searchVendorForm.setContactLastName(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Date")){
				    		searchVendorForm.setContactDate(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact State")){
				    		searchVendorForm.setContactState(m.getValue().get(0));
				    	}else if(m.getKey().equals("Contact Event")){
				    		searchVendorForm.setContactEvent(m.getValue().get(0));
				    	}
				    }
				}
			}
		}

		vendorSearchReportList = new ArrayList<SearchVendorDto>();
		CertificateDao certificate = new CertificateDaoImpl();
		List<Certificate> certificates = null;
		certificates = certificate.listOfCertificatesByType((byte) 1,
				appDetails); // List of certificate types.
		List<CertifyingAgency> certificateAgencies = searchDao
				.listAgencies(appDetails);
		session.setAttribute("certificateTypes", certificates);
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		List<com.fg.vms.customer.model.Country> countries = dao.list(
				appDetails,
				" From Country where isactive=1 order by countryname");
		CURDDao<com.fg.vms.customer.model.StatusMaster> statusDao = new CURDTemplateImpl<com.fg.vms.customer.model.StatusMaster>();
		List<com.fg.vms.customer.model.StatusMaster> vendorStatusList = statusDao
				.list(appDetails,
						" From StatusMaster where isactive=1 and id not in ('I','S') order by disporder");
		List<com.fg.vms.customer.model.StatusMaster> vendorStatus = new ArrayList<StatusMaster>();
		if (vendorStatusList != null && !vendorStatusList.isEmpty()) {
			for (StatusMaster master : vendorStatusList) {
				if (master.getId().equals('A') || master.getId().equals('B')) {
					vendorStatus.add(master);
				}
			}
		}

		/*
		 * Show list of Business areas.
		 */
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<GeographicalStateRegionDto> businessAreas = null;

		businessAreas = regionDao.getServiceAreasList(appDetails);
		session.setAttribute("businessAreas", businessAreas);

		/*
		 * Show list of commodity description.
		 */
		List<GeographicalStateRegionDto> commodityDescription = null;

		commodityDescription = regionDao.getCommodityDescription(appDetails);
		session.setAttribute("commodityDescription", commodityDescription);

		/*
		 * Show list of naics Code.
		 */
		/*
		 * List<NaicsCode> naicsCodeList=null;
		 * 
		 * naicsCodeList=regionDao.getNaicsCode(appDetails);
		 * session.setAttribute("naicsCodeList", naicsCodeList);
		 */
		/*
		 * Show list of certification type.
		 */
		List<GeographicalStateRegionDto> certificationTypes = null;

		certificationTypes = regionDao.getCertificationType(appDetails);
		session.setAttribute("certificationTypes", certificationTypes);

		// Show List Commodity Group with Market Sector, Market SubSector, &
		// Commodity Description Hierarchy
		List<GeographicalStateRegionDto> vendorCommodity = null;

		vendorCommodity = regionDao.getCommodityGroup(appDetails);
		session.setAttribute("vendorCommodity", vendorCommodity);

		CURDDao<com.fg.vms.customer.model.State> daoContactStates = new CURDTemplateImpl<com.fg.vms.customer.model.State>();
		List<com.fg.vms.customer.model.State> contactStates = daoContactStates
				.list(appDetails,
						"From State where isactive=1 order by statename");
		session.setAttribute("contactStates", contactStates);

		request.setAttribute("certifyingAgencies", certificateAgencies);
		request.setAttribute("countries", countries);
		session.setAttribute("vendorStatus1", vendorStatus);
		session.setAttribute("vendorStatusList", vendorStatusList);
		session.setAttribute("saveCriteria", 1);

		CURDDao<SegmentMaster> curdDao1 = new CURDTemplateImpl<SegmentMaster>();
		List<SegmentMaster> segmentMasters = curdDao1.list(appDetails,
				" From SegmentMaster order by segmentName");
		session.setAttribute("bpSegmentSearchList", segmentMasters);

		CURDDao<MarketSector> marketSectorDao = new CURDTemplateImpl<MarketSector>();
		List<MarketSector> marketSectorsList = marketSectorDao
				.list(appDetails,
						"From MarketSector WHERE isActive = 1 ORDER BY sectorDescription ASC");
		session.setAttribute("marketSectorsList", marketSectorsList);

		return mapping.findForward("Success");
	}

	/**
	 * showDynamicColumnsSelectPage
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showDynamicColumnsSelectPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		ReportDao reportDao = new ReportDaoImpl();

		List<ReportSearchFieldsDto> searchFeildsList = reportDao
				.getReportSearchFields(userDetails);
		session.setAttribute("searchFeildsList", searchFeildsList);

		return mapping.findForward("showDynamicColumnsSelectPage");
	}

	public ActionForward printLog(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getParameter("date") != null) {
			SearchDao searchDao = new SearchDaoImpl();
			HttpSession session = request.getSession();
			UserDetailsDto appDetails = (UserDetailsDto) session
					.getAttribute("userDetails");
			Integer userId = (Integer) session.getAttribute("userId");
			String userName = searchDao
					.findUserNameByUserId(appDetails, userId);

			logger.info("Search End Time After UI Creation:"
					+ request.getParameter("date") + ", Username:" + userName);
		}
		return mapping.getInputForward();
	}

	/**
	 * for showVendorStatusBySearch
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showVendorStatusBySearch(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		// For Setting Vendor Status Values for Vendor Status Field.
		CURDTemplateImpl<StatusMaster> statusDao = new CURDTemplateImpl<StatusMaster>();
		List<StatusMaster> vendorStatus = statusDao
				.list(userDetails,
						"From StatusMaster where isactive=1  and id not in ('S') order by disporder");
		session.setAttribute("vendorStatusList", vendorStatus);

		// For Setting Vendor Name Values for Vendor Name Field.
		/*
		 * VendorDao vendorDao = new VendorDaoImpl(); List<VendorMasterBean>
		 * vendorNamesList = vendorDao.retrieveVendorsList(userDetails);
		 * session.setAttribute("vendorNamesList", vendorNamesList);
		 */

		return mapping.findForward("showVendorStatusBySearchPage");
	}

	/**
	 * for searchByVendorStatus
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward searchByVendorStatus(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		SearchVendorForm searchVendorForm = (SearchVendorForm) form;
		SearchDao searchDao = new SearchDaoImpl();

		List<SearchVendorDto> vendorsList = null;
		vendorsList = searchDao.searchByVendorStatus(userDetails,
				searchVendorForm);

		setVendorSearchReportList(vendorsList);

		Integer toatlVendorCount = 0;
		String searchSummary = "";
		toatlVendorCount = searchDao.totalVendorCount(searchVendorForm,
				userDetails, "statusBySearch");

		if (vendorsList.size() > 0)
			searchSummary = "Total match found, " + vendorsList.size()
					+ " out of " + toatlVendorCount + "";

		searchVendorForm.setVendorsList(vendorsList);

		session.setAttribute("searchVendorsList", vendorsList);
		session.setAttribute("vendorSearchSummary", searchSummary);

		return null;
	}

	/**
	 * show searchByVendortatusRestult page
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward vendorStatusWiseSearchRestult(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		SearchDao searchDao = new SearchDaoImpl();
		String userName = searchDao.findUserNameByUserId(appDetails, userId);
		String searchPageType = request.getParameter("searchPageType");

		if (searchPageType.equalsIgnoreCase("searchByAllStatus"))
			session.setAttribute("searchtype", "vendorAllStatus");

		logger.info("Search Start Time Before UI Creation:" + new Date()
				+ ", Username:" + userName);
		return mapping.findForward("vendorStatusWiseSearchRestult");
	}
	
	public String downloadExcel(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		Integer id = Integer
				.parseInt(request.getParameter("id").toString());
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		SearchDao searchDao = new SearchDaoImpl();
		Tier2ReportMaster tier2ReportMaster=searchDao.getTier2ReportMaster(id, userDetails);
		String documentName="Tier2ReportUploadTemplate.xls";
		
		// File Location 		C:\vms\data\documents\ExcelTemplate
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename="+tier2ReportMaster.getFileName());
		response.setHeader("Pragma","no-cache");

		// Set content size
		String filePath=Constants.getString(Constants.UPLOAD_DOCUMENT_PATH)+ "/"+ "tier2reports" + "/" + tier2ReportMaster.getVendorId().getId();
		File file = new File(filePath,tier2ReportMaster.getTier2ReportMasterid()+"_"+tier2ReportMaster.getFileName());
		// response.setContentLength((int) file.length());
		// Open the file and output streams
		FileInputStream in = new FileInputStream(file);
		OutputStream out = response.getOutputStream();

		// Copy the contents of the file to the output stream
		int data = 0; 
		while((data = in.read()) != -1) 
        { 
			out.write(data); 
        }
		in.close();
		out.flush();
		out.close();
		return null;
	}
	
	public static HashMap<String, ArrayList<String>> getMapedObject(String criteria) throws org.json.simple.parser.ParseException{
		org.json.simple.parser.JSONParser parser = new JSONParser();
		org.json.simple.JSONObject newJObject = null;
		newJObject=(org.json.simple.JSONObject) parser.parse(criteria);
//		Iterator<java.util.Map.Entry<Long, String>> it = newJObject.entrySet().iterator();
		HashMap<String, ArrayList<String>> map=new HashMap<String, ArrayList<String>>();
//	    while (it.hasNext()) {
//	        java.util.Map.Entry<Long, String> pairs = it.next();
//	        JSONArray msg = (JSONArray) newJObject.get(pairs.getKey());
//	        for(int i=0;i<msg.size();i++){
//	        	JSONObject jo=(JSONObject) msg.get(i);
	        	Iterator<java.util.Map.Entry<Long, org.json.simple.JSONArray>> iterator = newJObject.entrySet().iterator();
	        	while (iterator.hasNext()) {
	        		Entry<Long, org.json.simple.JSONArray> pair = iterator.next();
	        		String key=String.valueOf(pair.getKey());
	        		JSONArray ar=pair.getValue();
	        		ArrayList<String> values=new ArrayList<String>();
	        		for(Object s:ar){
	        			values.add(s.toString());
	        		}
	        		map.put(key, values);
	        	}
//	        }
//	    }
		return map;
	}
}