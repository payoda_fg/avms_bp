/**
 * 
 */
package com.fg.vms.customer.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CertificationTypeDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificationTypeDaoImpl;
import com.fg.vms.customer.model.CertificationTypeDivision;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.pojo.CertificationTypeForm;
import com.fg.vms.util.Constants;

/**
 * @author venkatesh
 * 
 */
public class CertificationAction extends DispatchAction {
	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Show the email template page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward showCertificatePage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view the list of active CertificateType Messages..");

		List<CustomerCertificateType> certificationTypes = null;
		HttpSession session = request.getSession();
		CertificationTypeDao dao = new CertificationTypeDaoImpl();
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		certificationTypes = dao.listCertificationType(userDetails);
		session.setAttribute("certificationTypes", certificationTypes);
	   
		// To get the CertificationType Divisions
		List<CustomerDivision> customerDivisions = null;
		UsersDao usersDao = new UsersDaoImpl();
		//if Both Division(Division name="BOTH") is not needed in UI then pass true otherwise  pass false;
		customerDivisions = usersDao.listCustomerDivisons(userDetails, true);
		session.setAttribute("customerDivisions", customerDivisions);
		
		
		logger.info("Forward to show the list of CertificateType Messages");
		return mapping.findForward("showpage");
	}

	public ActionForward saveCertificationType(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside the create saveMessages method");

		HttpSession session = request.getSession();

		// Get the current user id from session
		Integer userId = (Integer) session.getAttribute("userId");
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CertificationTypeDao dao = new CertificationTypeDaoImpl();
		CertificationTypeForm certificationTypeForm = (CertificationTypeForm) form;
		String result = null;

		if (null != certificationTypeForm.getCertificateId()
				&& 0 != certificationTypeForm.getCertificateId()) {
			/* Id available, update the CertificationType. */
			result = dao.updateCertificationType(certificationTypeForm, userId,
					userDetails);
		} else {
			/* New Certification Type creation */
			result = dao.createCertificationType(certificationTypeForm, userId,
					userDetails);
		}
		/* For show the successful creation of CertificationType. */
		if (result.equals("success")) 
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("message.ok");
			messages.add("message", msg);
			saveMessages(request, messages);
			certificationTypeForm.reset(mapping, request);

			List<CustomerCertificateType> certificationTypes = null;
			certificationTypes = dao.listCertificationType(userDetails);
			session.setAttribute("certificationTypes", certificationTypes);
		} 
		else if (result.equals("failure")) 
		{
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		else if (result.equals("descNameExisted")) 
		{
            ActionErrors errors = new ActionErrors();
            errors.add("description", new ActionMessage("certificatetype.existed"));
            saveErrors(request, errors);
            return mapping.getInputForward();
        }
		logger.info("Forward to show the list of saveMessages page");

		return mapping.findForward("showpage");
	}

	/**
	 * Delete certificationType message.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteCertificationType(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside the deleteMessages method");

		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CertificationTypeDao dao = new CertificationTypeDaoImpl();
		CertificationTypeForm certificationTypeForm = (CertificationTypeForm) form;

		/* Delete existing certificationType message. */
		String result = dao.removeCertificationType(
				Integer.parseInt(request.getParameter("id")), userDetails);

		if (result.equals("reference")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage(
					"certificatetype.reference.delete");
			messages.add("certificateReferenceDelete", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		/* For show the successful creation message. */
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("message.delete.ok");
			messages.add("message", msg);
			saveMessages(request, messages);
			certificationTypeForm.reset(mapping, request);

			List<CustomerCertificateType> certificationTypes = null;
			certificationTypes = dao.listCertificationType(userDetails);
			session.setAttribute("certificationTypes", certificationTypes);
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		logger.info("Forward to show the list of email templates page");

		return mapping.findForward("showpage");
	}

	/**
	 * Edit certificationType message.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editCertificationType(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside the editMessages method");

		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CertificationTypeDao dao = new CertificationTypeDaoImpl();
		CertificationTypeForm certificationTypeForm = (CertificationTypeForm) form;
		Integer id = Integer.parseInt(request.getParameter("id").toString());

		/* Edit existing Certification Type. */
		CustomerCertificateType certificationType = dao.retrieveCertificationType(id,userDetails);

		if (null != certificationType) {
			
			CURDDao<CertificationTypeDivision> curdDao = new CURDTemplateImpl<CertificationTypeDivision>();
			List<CertificationTypeDivision> certificateDivisions = curdDao.list(
					userDetails,
					"from CertificationTypeDivision where isActive = 1 and certTypeId="
							+ id);
			certificationTypeForm = packCertificateDivisions(certificationTypeForm,
					certificateDivisions);
		
			certificationTypeForm.setCertificateId(certificationType.getId());
			certificationTypeForm.setDescription(certificationType
					.getCertificateTypeDesc());
			certificationTypeForm.setShortDescription(certificationType
					.getShortDescription());
			certificationTypeForm.setIsActive(certificationType.getIsActive()
					.toString());
			certificationTypeForm.setIsCertificateExpiryAlertRequired(certificationType.getIsCertificateExpiryAlertRequired());
		}

		logger.info("Forward to show the list of CertificationType page");

		return mapping.findForward("showpage");
	}
	
	/**
	 * Setting  Division For CertificationType
	 * 
	 * @param certificationTypeForm
	 * @param divisions
	 * @return
	 */
	private CertificationTypeForm packCertificateDivisions(
			CertificationTypeForm certificationTypeForm, List<CertificationTypeDivision> divisions) {

		String[] value = new String[divisions.size()];
		int i = 0;
		for (CertificationTypeDivision division : divisions) {
			value[i++] = division.getCustomerDivisionId().getId().toString();
		}
		certificationTypeForm.setCertifiacateDivision(value);
		return certificationTypeForm;
	}

}
