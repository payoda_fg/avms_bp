/**
 * CertifyingAgencyAction.java
 */
package com.fg.vms.customer.action;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CertifyingAgencyDao;
import com.fg.vms.customer.dao.impl.CertifyingAgencyDaoImpl;
import com.fg.vms.customer.dto.CertifyingAgencyDto;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.pojo.CertifyingAgencyForm;
import com.fg.vms.util.Constants;

import fr.improve.struts.taglib.layout.datagrid.Datagrid;

/**
 * @author vinoth
 * 
 */
public class CertifyingAgencyAction extends DispatchAction {
	private Logger logger = Constants.logger;
    /**
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward view(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

   HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");

	CertifyingAgencyDao certifyingAgencyDao = new CertifyingAgencyDaoImpl();
	List certifyingAgencies = certifyingAgencyDao.view(appDetails);

//	// Create a datagrid.
//	Datagrid datagrid = Datagrid.getInstance();
//
//	// Set the bean class for new objects. We suppose RolesAndObjects is the
//	// class of the object in the List aList.
//	datagrid.setDataClass(CertifyingAgencyDto.class);
//
//	// Set the data
//	datagrid.setData(certifyingAgencies);
//
//	// Initialize the form
//	certifyingAgencyForm.setCertifyingAcency(datagrid);
	session.setAttribute("certifyingAgencies", certifyingAgencies);

	return mapping.findForward("showpage");
    }

    /**
     * Save or Update the user details.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    @SuppressWarnings("rawtypes")
	public ActionForward saveOrUpdate(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	CertifyingAgencyForm certifyingAgencyForm = (CertifyingAgencyForm) form;

	// Get the datagrid.
	Datagrid datagrid = certifyingAgencyForm.getCertifyingAcency();

	CertifyingAgencyDao certifyingAgencyDao = new CertifyingAgencyDaoImpl();

	// Deal with new objects.
	Collection lc_added = datagrid.getAddedData();
	Integer id = 1;
	Iterator lc_it = lc_added.iterator();
	while (lc_it.hasNext()) {
	    CertifyingAgencyDto certifyingAgencyDto = (CertifyingAgencyDto) lc_it
		    .next();
	    // Save the new subject in the database, if there is not already
	    // a subject with this name
	    // and the subject is fully described.
	    certifyingAgencyDao.saveOrUpdate(certifyingAgencyDto, id,
		    appDetails);
	}

	// Deal with modified objects.
	// Here the code is not very useful has the object in the database
	// are the same as the object in the form
	// but the code is an example of how to do it.
	Collection lc_modified = datagrid.getModifiedData();
	lc_it = lc_modified.iterator();
	while (lc_it.hasNext()) {
	    CertifyingAgencyDto certifyingAgencyDto = (CertifyingAgencyDto) lc_it
		    .next();
	    certifyingAgencyDao.saveOrUpdate(certifyingAgencyDto, id,
		    appDetails);

	}

	// Update the datagrid.
	List certifyingAgencies = certifyingAgencyDao.view(appDetails);
	datagrid.setData(certifyingAgencies);

	// forward to the privileges page
	return mapping.findForward("showpage");
    }

    /**
     * Insert new certifying agency.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward insertagency(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	HttpSession session = request.getSession();
	String result = null;

	UserDetailsDto appDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer userId = (Integer) session.getAttribute("userId");

	CertifyingAgencyForm certifyingAgencyForm = (CertifyingAgencyForm) form;

	CertifyingAgencyDao certifyingAgencyDao = new CertifyingAgencyDaoImpl();

	if(null != certifyingAgencyForm.getId()
			&& 0 != certifyingAgencyForm.getId())
	{
	   result = certifyingAgencyDao.createagency(certifyingAgencyForm, userId, appDetails);
	}
	else
	{
	    result = certifyingAgencyDao.createagency(certifyingAgencyForm,
		userId, appDetails);
	}
	
	// For check unique object name
	if (result.equals("agencyNameUnique")) {
	    ActionErrors errors = new ActionErrors();
	    errors.add("agencyName", new ActionMessage("agencyName.unique"));
	    saveErrors(request, errors);
	    mapping.findForward(mapping.getInput());
	}
	
	if (result.equals("agencyShortNameUnique")) {
	    ActionErrors errors = new ActionErrors();
	    errors.add("agencyShortName", new ActionMessage("agencyShortName.unique"));
	    saveErrors(request, errors);
	    mapping.findForward(mapping.getInput());
	}

	// For show the list of objects and successful object insert message
	if (result.equals("success")) {

	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("agency.ok");
	    messages.add("agency", msg);
	    saveMessages(request, messages);
	   
	    certifyingAgencyForm.reset(mapping, request);
	   
	    List<CertifyingAgencyDto> certifyingAgencies = null;
	    certifyingAgencies =certifyingAgencyDao.view(appDetails);
	    session.setAttribute("certifyingAgencies", certifyingAgencies);
	    
	}
	
	if(result.equals("updatedAgency"))
	{
		ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("agency.update");
	    messages.add("agencyUpdated", msg);
	    saveMessages(request, messages);
	    
	    certifyingAgencyForm.reset(mapping, request);
	    
	    List<CertifyingAgencyDto> certifyingAgencies = null;
	    certifyingAgencies =certifyingAgencyDao.view(appDetails);
	    session.setAttribute("certifyingAgencies", certifyingAgencies);
	    
	}
	 

	return mapping.findForward("showpage");
    }
    
    /**
	 * Edit CertifyAgency.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editCertifyAgency(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		logger.info("Inside the editCertifyAgency method");

		HttpSession session = request.getSession();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		CertifyingAgencyDao certifyingAgencyDao = new CertifyingAgencyDaoImpl();
		
		CertifyingAgencyForm certifyingAgencyForm = (CertifyingAgencyForm) form;
		
		Integer id = Integer.parseInt(request.getParameter("id").toString());
		
		/* Edit existing CertifyAgency . */
		CertifyingAgency certifyAgency = certifyingAgencyDao.retrieveCertifyAgency(id,userDetails);
		
		if(null != certifyAgency)
		{
		certifyingAgencyForm.setId(certifyAgency.getId());
		certifyingAgencyForm.setAgencyName(certifyAgency.getAgencyName());
		certifyingAgencyForm.setAgencyShortName(certifyAgency.getAgencyShortName());
		certifyingAgencyForm.setPhone(certifyAgency.getPhone());
		certifyingAgencyForm.setFax(certifyAgency.getFax());
		certifyingAgencyForm.setAddress(certifyAgency.getAddress());
		}
		
		logger.info("Forward to show the list of Certify agnecy page");
		
		return mapping.findForward("showpage");
	}
	
	 /**
		 * Edit CertifyAgency.
		 * 
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws Exception
		 */
		public ActionForward deleteCertifyAgency(ActionMapping mapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			
			logger.info("Inside the deleteCertifyAgency method");

			HttpSession session = request.getSession();
			String result =null;
			/*
			 * Its holds the user details..(such as current customer database
			 * details and application settings)
			 */
			UserDetailsDto userDetails = (UserDetailsDto) session
					.getAttribute("userDetails");
			
			CertifyingAgencyDao certifyingAgencyDao = new CertifyingAgencyDaoImpl();
			
			CertifyingAgencyForm certifyingAgencyForm = (CertifyingAgencyForm) form;
			
			Integer id = Integer.parseInt(request.getParameter("id").toString());
			
			result = certifyingAgencyDao.deleteCertifyAgency(id, userDetails);
			

			if(result.equals("success"))
			{
				ActionMessages messages = new ActionMessages();
			    ActionMessage msg = new ActionMessage("agency.delete");
			    messages.add("agencyDeleted", msg);
			    saveMessages(request, messages);
			    
			    List<CertifyingAgencyDto> certifyingAgencies = null;
			    certifyingAgencies =certifyingAgencyDao.view(userDetails);
			    session.setAttribute("certifyingAgencies", certifyingAgencies);
			}
			if (result.equals("reference")) {

				ActionMessages messages = new ActionMessages();
				ActionMessage msg = new ActionMessage(
						"agency.reference");
				messages.add("agencyReferency", msg);
				saveMessages(request, messages);
				return mapping.getInputForward();
			}
			return mapping.findForward("showpage");
	
		}
}