package com.fg.vms.customer.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.util.LabelValueBean;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CapabilityAssessmentDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.CapabilityAssessmentDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.TemplateDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.AssessmentQuestionsDto;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.AssessmentAnswerForm;
import com.fg.vms.util.SendEmail;

/**
 * 
 * @author vivek
 * 
 */
public class CapabilityAssessmentVendorAction extends DispatchAction {

    /**
     * Method to show the capability assessment page with questions and data
     * type to the vendor.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward showCapabilityAssessmentPage(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        AssessmentAnswerForm assessmentAnswerForm = (AssessmentAnswerForm) form;
        HttpSession session = request.getSession();
        VendorContact vendorUser = (VendorContact) session
                .getAttribute("vendorUser");
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        CapabilityAssessmentDao capabilityAssessmentDaoImpl = new CapabilityAssessmentDaoImpl();

        Map<Template, List<AssessmentQuestionsDto>> assessmentQuestions;

        /* Get the list of templates based on vendor. */
        assessmentQuestions = capabilityAssessmentDaoImpl.getTemplate(
                userDetails, vendorUser.getVendorId());
        session.setAttribute("assessmentQuestions", assessmentQuestions);
        assessmentAnswerForm.setAssessmentQuestions(assessmentQuestions);
        return mapping.findForward("showAssessmentPage");
    }

    /**
     * Method to save the assessment answers to the database.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward saveAssessmentAnswers(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        Integer vendorUserId = (Integer) session.getAttribute("userId");
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        VendorContact vendorUser = (VendorContact) session
                .getAttribute("vendorUser");
        CapabilityAssessmentDao capabilityAssessmentDaoImpl = new CapabilityAssessmentDaoImpl();
        Integer vendorId = capabilityAssessmentDaoImpl.getVendorId(
                vendorUserId, userDetails);

        AssessmentAnswerForm assessmentAnswerForm = (AssessmentAnswerForm) form;

        String result = "";

        LabelValueBean[] labelValueBeans = assessmentAnswerForm.getLvbeans();

        /* Save the list of questions. */

        for (LabelValueBean labelValueBean : labelValueBeans) {

            if (labelValueBean.getValue() != null
                    && labelValueBean.getValue().length() != 0) {
                Integer templateQuestionId = Integer.parseInt(labelValueBean
                        .getLabel().toString());
                try {

                    String description = labelValueBean.getValue();

                    result = capabilityAssessmentDaoImpl.saveAssessmentAnswers(
                            vendorId, vendorUserId, null, templateQuestionId,
                            userDetails, description);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*
                 * String customerEmailId = ""; if (result.contains("success"))
                 * { customerEmailId = result.replace("success", ""); }
                 * 
                 * if (!result.equalsIgnoreCase("success") &&
                 * !result.equalsIgnoreCase("failure")) { customerEmailId =
                 * result.replace("success", ""); SendEmail email = new
                 * SendEmail(); result = email.sendEmailToCustomer(
                 * vendorUser.getFirstName(), userDetails, customerEmailId); }
                 */
            }
        }

        if (result.equals("success")) {
            ActionMessages messages = new ActionMessages();
            ActionMessage msg = new ActionMessage("saveAssessmentAnswers.ok");
            messages.add("assessment", msg);
            saveMessages(request, messages);
            Map<Template, List<AssessmentQuestionsDto>> assessmentQuestions;

            /* Get the list of templates based on vendor. */
            assessmentQuestions = capabilityAssessmentDaoImpl.getTemplate(
                    userDetails, vendorUser.getVendorId());

            /*
             * Send the mail to distribution list when capability assessment is
             * completed
             */
            if (assessmentQuestions != null && assessmentQuestions.size() == 0) {
                SendEmail mail = new SendEmail();
                WorkflowConfigurationDao conf = new WorkflowConfigurationDaoImpl();
                WorkflowConfiguration wfConf = conf
                        .listWorkflowConfig(userDetails);
                // Send email to distribution when Capability Assessment
                // completed
                if (wfConf != null && wfConf.getAssessmentCompleted() != null && wfConf.getAssessmentCompleted() == 1) {
                	//calling a method to get mailcontent from db
                	Integer customerDivisionId = null;
                	if(null != userDetails.getCustomerDivisionID())
                	{
                		customerDivisionId = userDetails.getCustomerDivisionID();	
                	}	
                	EmailTemplate emailTemplate=mail.getTemplates(9, userDetails, customerDivisionId);
                    String templateMessage=emailTemplate.getEmailTemplateMessage();
                    
                    //Replacing Parameters
                    templateMessage=templateMessage.replace("#vendorName", vendorUser.getVendorId().getVendorName().toString());
                    String subject=emailTemplate.getEmailTemplateSubject();
                   
                    mail.sendMailToDistribution(
                            conf.distributionListEmail(userDetails),
                            subject, templateMessage, userDetails);
                    conf.updateCustomerWfLog(vendorUser.getVendorId(),
                            "completed", userDetails);
                }
            }
            session.setAttribute("assessmentQuestions", assessmentQuestions);
            assessmentAnswerForm.setAssessmentQuestions(assessmentQuestions);
            assessmentAnswerForm.reset(mapping, request);
        }

        return mapping.findForward("showAssessmentPage");

    }

    /**
     * Method to review capability assessment answered by vendor
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward reviewTemplate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        AssessmentAnswerForm answerForm = (AssessmentAnswerForm) form;

        HttpSession session = request.getSession();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        List<VendorMaster> templatevendors = null;
        SearchDao search = new SearchDaoImpl();

        Integer customerId = null;
        if (session.getAttribute("userId") != null) {
            customerId = (Integer) session.getAttribute("userId");
        }

        templatevendors = search.reviewVendorsList(userDetails, customerId);
        answerForm.setTemplatevendors(templatevendors);

        Integer templatenameforreview = null;
        Integer isReviewed = null;
        Integer selectedvendorid = null;

        if (request.getParameter("templateid") != null
                && request.getParameter("vendorId") != null) {
            selectedvendorid = Integer.parseInt(request
                    .getParameter("vendorId").toString());
            templatenameforreview = Integer.parseInt(request.getParameter(
                    "templateid").toString());
            answerForm.setTemplateId(templatenameforreview.toString());
            session.setAttribute("selectedVendorForReview", selectedvendorid);
            session.setAttribute("templatenameforreview", templatenameforreview);
            session.setAttribute("isReviewedeforreview", 0);
        }

        /* Clears the session on loading and reset the page. */
        if (request.getParameter("vendorSelected") != null) {
            selectedvendorid = Integer.parseInt(request.getParameter(
                    "vendorSelected").toString());
            session.removeAttribute("templatenameforreview");
            session.removeAttribute("selectedVendorForReview");
            session.removeAttribute("isReviewedeforreview");

        } else if (answerForm.getVendorId() != null
                && answerForm.getVendorId() != 0) {

            /*
             * Set the vendor,template and IsReview in session for view their
             * search details.
             */
            selectedvendorid = Integer.parseInt(answerForm.getVendorId()
                    .toString());
            session.setAttribute("selectedVendorForReview", selectedvendorid);

            if (answerForm.getTemplateId() != null
                    && !answerForm.getTemplateId().equalsIgnoreCase("0")) {
                templatenameforreview = Integer.parseInt(answerForm
                        .getTemplateId());
                session.setAttribute("templatenameforreview",
                        templatenameforreview);
            }

            if (answerForm.getIsreviewed() != null) {
                isReviewed = Integer.parseInt((String.valueOf(answerForm
                        .getIsreviewed())));
                session.setAttribute("isReviewedeforreview", isReviewed);
            }
        }

        answerForm.setVendorId(selectedvendorid);

        /* Get the list of assessment templates. */
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
        List<Template> reviewtemplates = templateDaoImpl
                .listTemplatesBasedVendor(userDetails, selectedvendorid);
        session.setAttribute("reviewtemplates", reviewtemplates);

        if (request.getParameter("vendorSelected") != null) {
            return mapping.findForward("templateName");
        }
        return mapping.findForward("review");
    }

    /**
     * Method to view assessment email notification details
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward viewemaildetails(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        /* Get the list of assessment templates. */
        List<Template> assessmentemaildetailtemplate = templateDaoImpl
                .listAssessmentTemplates(userDetails);
        session.setAttribute("assessmentemaildetailtemplate",
                assessmentemaildetailtemplate);

        Integer getTemplate = null;

        /* Set the template in session for view their email details. */
        if (request.getParameter("getTemplate") != null) {
            getTemplate = Integer.parseInt(request.getParameter("getTemplate")
                    .toString());
            session.setAttribute("getTemplate", getTemplate);
        }

        return mapping.findForward("viewdetails");
    }

    /**
     * Method to view assessment score details
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward viewscore(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();

        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");
        TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();

        /* Get the list of assessment templates. */
        List<Template> assessmentemaildetailtemplate = templateDaoImpl
                .listAssessmentScoreTemplates(userDetails);
        session.setAttribute("assessmentemaildetailtemplate",
                assessmentemaildetailtemplate);

        Integer getTemplateForScore = null;

        /* Set the template in session for view their scores. */
        if (request.getParameter("getTemplateForScore") != null) {
            getTemplateForScore = Integer.parseInt(request.getParameter(
                    "getTemplateForScore").toString());
            session.setAttribute("getTemplateForScore", getTemplateForScore);
        }

        return mapping.findForward("viewscore");
    }

    /**
     * Method to view assessment score details
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward viewdetailsforscore(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();

        Integer scoreDetailTemplateId = null;
        Integer scoreDetailVendorId = null;

        /*
         * Get the template selected from assessment score detail page.
         */
        if (request.getParameter("templateid") != null) {
            scoreDetailTemplateId = Integer.parseInt(request.getParameter(
                    "templateid").toString());
            session.setAttribute("scoreDetailTemplateId", scoreDetailTemplateId);
        }

        if (request.getParameter("vendorId") != null) {
            scoreDetailVendorId = Integer.parseInt(request.getParameter(
                    "vendorId").toString());
            session.setAttribute("scoreDetailVendorId", scoreDetailVendorId);
        }

        return mapping.findForward("viewscoredetail");
    }

    /**
     * Method to get the templates based on vendor selected.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */

    public ActionForward getvendortemplates(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        AssessmentAnswerForm answerForm = (AssessmentAnswerForm) form;
        UserDetailsDto userDetails = (UserDetailsDto) session
                .getAttribute("userDetails");

        List<VendorMaster> templatevendors = null;
        SearchDao search = new SearchDaoImpl();

        Integer customerId = null;
        if (session.getAttribute("userId") != null) {
            customerId = (Integer) session.getAttribute("userId");
        }

        /* Get the list of vendors answered for template questions. */
        templatevendors = search.reviewVendorsList(userDetails, customerId);
        answerForm.setTemplatevendors(templatevendors);

        Integer selectedvendorid = Integer.parseInt(request
                .getParameter("vendorSelected"));

        /* Get the list of templates based on vendor. */
        if (selectedvendorid != null && selectedvendorid != 0) {
            TemplateDaoImpl templateDaoImpl = new TemplateDaoImpl();
            List<Template> reviewtemplates = templateDaoImpl
                    .listTemplatesBasedVendor(userDetails, selectedvendorid);
            session.setAttribute("reviewtemplates", reviewtemplates);
        }
        return mapping.findForward("review");
    }

}
