/*
 * AnonymousVendorAction.java 
 */
package com.fg.vms.customer.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.common.Country;
import com.fg.vms.customer.dao.AnonymousVendorDao;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.AnonymousVendorDaoImpl;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerBusinessType;
import com.fg.vms.customer.model.CustomerCertificateBusinesAreaConfig;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerLegalStructure;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.VendorUtil;

/**
 * Represent the Self registration data (eg.. create vendor, save and update
 * vendor information)
 * 
 * @author vinoth
 * 
 */
public class AnonymousVendorAction extends DispatchAction {

	/**
	 * Manage Anonymous vendor page
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward manageAnonymousVendors(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LoginDao login = new LoginDaoImpl();
		String requestUrl = request.getServerName();
		UserDetailsDto userDetail = login.authenticateServerURL(requestUrl);

		HttpSession session = request.getSession();
		session.setAttribute("primeSelfRegistration", null);
		session.setAttribute("tempLoginEmail", null);
		session.setAttribute("tempLoginPassword", null);
		session.setAttribute("contactStates", null);
		session.setAttribute("meetingVendor", null);
		UsersDao usersDao = new UsersDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<CustomerDivision> customerDivisions = null;
		//if Both Division(Division name="BOTH") is not needed in UI then pass true other wise  pass false; 
		customerDivisions = usersDao.listCustomerDivisons(userDetails, true);
		session.setAttribute("customerDivisions", customerDivisions);
		if (null != request.getParameter("prime")
				&& !request.getParameter("prime").isEmpty()) {
			session.setAttribute("primeSelfRegistration", "prime");
			return mapping.findForward("showprime");
		}

			return mapping.findForward("managesuccess");
		
	}

	/**
	 * Login from Email
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward emailLogin(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		LoginDao login = new LoginDaoImpl();			
		UsersDao usersDao = new UsersDaoImpl();
		
		// For Local & Cloud
//		String requestUrl = request.getServerName();
		
		// For staging / New Live
		String requestUrl = request.getRequestURI().toString().replaceAll(request.getServletPath(), "").trim();		

		/**
		 * If Requested URL is customer (/vmscustomer), then validate the url.
		 */
		UserDetailsDto userDetails = login.authenticateServerURL(requestUrl);
		
		/*
		 * Gets IsDivision Status
		 */
		com.fg.vms.customer.model.CustomerApplicationSettings applicationSettings = new com.fg.vms.customer.model.CustomerApplicationSettings();		
		applicationSettings = login.getCustomerApplicationSettings(userDetails);
		if (applicationSettings != null && applicationSettings.getIsDivision() != null) 
		{
			applicationSettings.setIsDivision(applicationSettings.getIsDivision());
		}
		session.setAttribute("isDivisionStatus", applicationSettings);
		
		List<CustomerDivision> customerDivisions = null;
		//if Both Division(Division name="BOTH") is not needed in UI then pass true otherwise  pass false;
		customerDivisions = usersDao.listCustomerDivisons(userDetails, false);
		session.setAttribute("customerDivisions", customerDivisions);
				
		
		/** validate the SLA period */
		if (login.validateSLAPeriod(userDetails.getCustomerSLA())
				.equalsIgnoreCase("ended")) {
			return mapping.findForward("slaperiodended");

		}
		if (userDetails.getDbName() != null) {
			userDetails.setHibernateCfgFileName("hibernate.customer.cfg.xml");
			userDetails.setUserType("customerOrVendor");
			userDetails.setSettings(login
					.getCustomerApplicationSettings(userDetails));
			session.setAttribute("userDetails", userDetails);
			session.setAttribute("userType", "customerOrVendor");
			List<SecretQuestion> secretQuestionList = login
					.getSecurityQuestions(userDetails);
			session.setAttribute("secretQuestionsList", secretQuestionList);
		}

		if (null != request.getParameter("prime")
				&& !request.getParameter("prime").isEmpty()) {
			session.setAttribute("primeSelfRegistration", "prime");
			return mapping.findForward("showprime");
		}

		return mapping.findForward("managesuccess");
	}

	/**
	 * New self registration page wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward selfRegistrationPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		LoginDao login = new LoginDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<SecretQuestion> secretQuestionList = login
				.getSecurityQuestions(userDetails);
		session.setAttribute("secretQnsList", secretQuestionList);
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String submitType = request.getParameter("submitType");
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeVendorContact(vendorForm, userId,
					vendorId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.contact.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			CURDDao<?> cdao = new CURDTemplateImpl();
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				SearchDao searchDao = new SearchDaoImpl();
				// load password security questions list
				List<SecretQuestion> securityQnsList;
				VendorContact contact = null;
				List<VendorContact> businessContacts = null;
				securityQnsList = searchDao.listSecQns(userDetails);
				// populate countries list
				List<Country> countries = (List<Country>) cdao.list(
						userDetails,
						" From Country where isactive=1 order by countryname");
				session.setAttribute("vendorId", vendorId);
				vendorForm.setOtherExpiryDate(null);
				CURDDao<State> dao = new CURDTemplateImpl<State>();
				List<State> contactStates = dao.list(userDetails,
						" From State order by statename");
				vendorForm.setContactStates(contactStates);
				vendorForm.setId(vendorId);
				contact = vendorDao.getSavedVendorData(
						vendorForm.getContanctEmail(), userDetails);
				vendorForm = VendorUtil.packVendorContactInfomation(vendorForm,
						contact);
				businessContacts = vendorDao.getBusinessContactDetails(
						vendorId, userDetails);
				if (businessContacts != null) {
					VendorContact contact1 = businessContacts.get(0);
					vendorForm = VendorUtil
							.packVendorBusinessContactInfomation(vendorForm,
									contact1);
				} else {
					vendorForm.setPreparerFirstName(contact.getFirstName());
					vendorForm.setPreparerLastName(contact.getLastName());
					vendorForm.setPreparerTitle(contact.getDesignation());
					vendorForm.setPreparerPhone(contact.getPhoneNumber());
					vendorForm.setPreparerMobile(contact.getMobile());
					vendorForm.setPreparerFax(contact.getFax());
					vendorForm.setPreparerEmail(contact.getEmailId());
				}
				session.setAttribute("secretQnsList", securityQnsList);
				session.setAttribute("countryList", countries);
				return mapping.findForward("savecontact");

			} else {
				/**
				 * Set vendor information for process next page
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorCertificate> vendorCertificate = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(vendorId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				vendorCertificate = vendorInfo.getVendorCertificate();
				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, vendorId, vendorMaster, vendorCertificate,
						userDetails);
				vendorForm.setId(vendorId);
				vendorForm.setCompanytype(vendorMaster.getCompanyType());
				List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
						.list(userDetails,
								"From CustomerBusinessType where isactive=1 order by typeName");
				List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
						.list(userDetails,
								"From CustomerLegalStructure where isactive=1 order by name");
				vendorForm.setLegalStructures(legalStructures);
				vendorForm.setBusinessTypes(businessTypes);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		return mapping.findForward("submitcontactinfo");

	}

	/**
	 * Save partial data from new self registration page wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveCompanyInformation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		String submitType = request.getParameter("submitType");
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeVendorInformation(vendorForm,
					userId, vendorId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.company.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			CURDDao<?> cdao = new CURDTemplateImpl();
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorCertificate> vendorCertificate = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(vendorId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				vendorCertificate = vendorInfo.getVendorCertificate();
				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, vendorId, vendorMaster, vendorCertificate,
						userDetails);
				vendorForm.setCompanytype(vendorMaster.getCompanyType());
				List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
						.list(userDetails,
								"From CustomerBusinessType where isactive=1 order by typeName");
				List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
						.list(userDetails,
								"From CustomerLegalStructure where isactive=1 order by name");
				vendorForm.setLegalStructures(legalStructures);
				vendorForm.setBusinessTypes(businessTypes);
				return mapping.findForward("savevendorinfo");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
				
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}		
				List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}		
				vendorForm.setPhyStates1(statesList);
				vendorForm.setMailingStates2(statesList);
				
				CustomerVendorAddressMaster phyAddress = documentsDao
						.getMailingAddress(vendorId, "p", userDetails);
				if (phyAddress != null) {
					vendorForm.setAddress1(phyAddress.getAddress());
					vendorForm.setCity(phyAddress.getCity());
					vendorForm.setState(phyAddress.getState());
					vendorForm.setProvince(phyAddress.getProvince());
					vendorForm.setRegion(phyAddress.getRegion());
					vendorForm.setCountry(phyAddress.getCountry());
					vendorForm.setZipcode(phyAddress.getZipCode());
					vendorForm.setMobile(phyAddress.getMobile());
					vendorForm.setPhone(phyAddress.getPhone());
					vendorForm.setFax(phyAddress.getFax());
					if (null != phyAddress.getCountry() && !phyAddress.getCountry().isEmpty()) {
						List<State> phyStates = null;				
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								phyStates = dao.list(userDetails, " From State where countryid=" + phyAddress.getCountry() + " and isactive=1 order by statename");
							}
						}						
						vendorForm.setPhyStates1(phyStates);
					}
				}
				CustomerVendorAddressMaster addressMaster = documentsDao
						.getMailingAddress(vendorId, "m", userDetails);
				if (addressMaster != null) {
					vendorForm.setAddress2(addressMaster.getAddress());
					vendorForm.setCity2(addressMaster.getCity());
					vendorForm.setState2(addressMaster.getState());
					vendorForm.setProvince2(addressMaster.getProvince());
					vendorForm.setRegion2(addressMaster.getRegion());
					vendorForm.setCountry2(addressMaster.getCountry());
					vendorForm.setZipcode2(addressMaster.getZipCode());
					vendorForm.setMobile2(addressMaster.getMobile());
					vendorForm.setPhone2(addressMaster.getPhone());
					vendorForm.setFax2(addressMaster.getFax());
					if (null != addressMaster.getCountry() && !addressMaster.getCountry().isEmpty()) {
						List<State> mStates = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								mStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								mStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
							}
						}						
						vendorForm.setMailingStates2(mStates);
					}
				}
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}

		return mapping.findForward("savecompanyinfo");
	}

	/**
	 * Save partial data from new self registration page wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveAddressInformation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CURDDao<?> cdao1 = new CURDTemplateImpl();
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			Integer countryId = null;
			List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails,"From Country where isDefault = 1");
			countryId = country.get(0).getId();
			vendorForm.setCountry2(countryId.toString());
			result = anonymousVendorDao.mergeVendorAddress(vendorForm, userId,
					vendorId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.address.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
			CURDDao<State> dao = new CURDTemplateImpl<State>();
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
				
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}				
				List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}
				vendorForm.setPhyStates1(statesList);
				vendorForm.setMailingStates2(statesList);
				
				CustomerVendorAddressMaster phyAddress = documentsDao
						.getMailingAddress(vendorId, "p", userDetails);
				if (phyAddress != null) {
					vendorForm.setAddress1(phyAddress.getAddress());
					vendorForm.setCity(phyAddress.getCity());
					vendorForm.setState(phyAddress.getState());
					vendorForm.setProvince(phyAddress.getProvince());
					vendorForm.setRegion(phyAddress.getRegion());
					vendorForm.setCountry(phyAddress.getCountry());
					vendorForm.setZipcode(phyAddress.getZipCode());
					vendorForm.setMobile(phyAddress.getMobile());
					vendorForm.setPhone(phyAddress.getPhone());
					vendorForm.setFax(phyAddress.getFax());
					if (null != phyAddress.getCountry() && !phyAddress.getCountry().isEmpty()) {
						List<State> phyStates = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								phyStates = dao.list(userDetails, " From State where countryid=" + phyAddress.getCountry() + " and isactive=1 order by statename");
							}
						}						
						vendorForm.setPhyStates1(phyStates);
					}
				}
				CustomerVendorAddressMaster addressMaster = documentsDao
						.getMailingAddress(vendorId, "m", userDetails);
				if (addressMaster != null) {
					vendorForm.setAddress2(addressMaster.getAddress());
					vendorForm.setCity2(addressMaster.getCity());
					vendorForm.setState2(addressMaster.getState());
					vendorForm.setProvince2(addressMaster.getProvince());
					vendorForm.setRegion2(addressMaster.getRegion());
					vendorForm.setCountry2(addressMaster.getCountry());
					vendorForm.setZipcode2(addressMaster.getZipCode());
					vendorForm.setMobile2(addressMaster.getMobile());
					vendorForm.setPhone2(addressMaster.getPhone());
					vendorForm.setFax2(addressMaster.getFax());
					if (null != addressMaster.getCountry() && !addressMaster.getCountry().isEmpty()) {
						List<State> mStates = null;
						if(userDetails.getWorkflowConfiguration() != null) {
							if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
								mStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
							} else {
								mStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
							}
						}
						vendorForm.setMailingStates2(mStates);
					}
				}
				return mapping.findForward("saveaddress");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorCertificate> vendorCertificate = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(vendorId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				vendorCertificate = vendorInfo.getVendorCertificate();
				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, vendorId, vendorMaster, vendorCertificate,
						userDetails);
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
				if (null != vendorForm.getCompanyOwnership()
						&& !vendorForm.getCompanyOwnership().isEmpty()) {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(vendorForm
							.getCompanyOwnership());
				} else {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(null);
				}
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("saveaddressinfo");
	}

	/**
	 * Save partial owner data from new self registration page wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveOwnerInformation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();

		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeVendorOwnerInfo(vendorForm,
					userId, vendorId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.owner.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			/*
			 * Load vendor information
			 */
			VendorDao vendorDao = new VendorDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			List<VendorCertificate> vendorCertificate = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					vendorId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			vendorCertificate = vendorInfo.getVendorCertificate();
			vendorForm = VendorUtil.packVendorInfomation(vendorForm,
					vendorInfo, vendorId, vendorMaster, vendorCertificate,
					userDetails);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
				if (null != vendorForm.getCompanyOwnership()
						&& !vendorForm.getCompanyOwnership().isEmpty()) {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(vendorForm
							.getCompanyOwnership());
				} else {
					vendorForm.setOwnerPublic("1");
					vendorForm.setOwnerPrivate("2");
					vendorForm.setCompanyOwnership(null);
				}
				return mapping.findForward("saveowner");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				List<Certificate> certificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				certificates = certificate.listOfCertificatesByType((byte) 1,
						userDetails);
				session.setAttribute("certificateTypes", certificates);
				// Get vendor diverse classification list
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				// Certificate Type
				CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
				List<CustomerCertificateType> certificateTypesList = curdDao2
						.list(userDetails, " from CustomerCertificateType ");
				session.setAttribute("certificateTypesList",
						certificateTypesList);
				// certificate agencies list.
				SearchDao searchDao = new SearchDaoImpl();
				List<CertifyingAgency> certificateAgencies;
				certificateAgencies = searchDao.listAgencies(userDetails);
				session.setAttribute("certAgencyList", certificateAgencies);
				// ethnicities list
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("saveownerinfo");
	}

	/**
	 * Save partial diverse classification data from new self registration page
	 * wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveDiverseClassification(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		List<DiverseCertificationTypesDto> diverseCertifications = new ArrayList<DiverseCertificationTypesDto>();
		if (getDeverseSupplier(vendorForm.getDeverseSupplier()) == (byte) 1) {
			diverseCertifications = CommonUtils.packageDiverseTypes(vendorForm);
		}
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeDiverseClassification(vendorForm,
					userId, vendorId, userDetails, url, diverseCertifications);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.diverse.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.reset(mapping, request);
			/**
			 * Load vendor data
			 */
			VendorDao vendorDao = new VendorDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			List<VendorCertificate> vendorCertificate = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					vendorId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			vendorCertificate = vendorInfo.getVendorCertificate();
			vendorForm = VendorUtil.packVendorInfomation(vendorForm,
					vendorInfo, vendorId, vendorMaster, vendorCertificate,
					userDetails);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			
			/*
			 * List of Vendor Keywords.
			 */
			VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
			List<CustomerVendorKeywords> keywords = null;
			keywords = documentsDao.getVendorKeywords(vendorId, userDetails);
			session.setAttribute("vendorKeywords", keywords);
			
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				session.setAttribute("diverseClassification", null);
				// get the list of certificates by diverse.
				List<Certificate> certificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				certificates = certificate.listOfCertificatesByType((byte) 1,
						userDetails);
				session.setAttribute("certificateTypes", certificates);
				// Get vendor diverse classification list
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				// Certificate Type
				CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
				List<CustomerCertificateType> certificateTypesList = curdDao2
						.list(userDetails, " from CustomerCertificateType ");
				session.setAttribute("certificateTypesList",
						certificateTypesList);
				// certificate agencies list.
				SearchDao searchDao = new SearchDaoImpl();
				List<CertifyingAgency> certificateAgencies;
				certificateAgencies = searchDao.listAgencies(userDetails);
				session.setAttribute("certAgencyList", certificateAgencies);
				// ethnicities list
				CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
				session.setAttribute("ethnicities",
						ethnicityDao.list(userDetails, null));
				return mapping.findForward("savediverseclassification");
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitdiverseclassification");
	}

	/**
	 * Save partial service area data from new self registration page wizard
	 * type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveServiceArea(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();
		List<NaicsCodesDto> nacisCodes = new ArrayList<NaicsCodesDto>();
		nacisCodes = CommonUtils.packageNiacsCodes(vendorForm);

		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeServiceArea(vendorForm, userId,
					vendorId, userDetails, url, nacisCodes);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.service.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			/**
			 * Load vendor data
			 */
			vendorForm.reset(mapping, request);
			VendorDao vendorDao = new VendorDaoImpl();
			CertificateDaoImpl certificate = new CertificateDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			List<VendorCertificate> vendorCertificate = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					vendorId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			vendorCertificate = vendorInfo.getVendorCertificate();
			vendorForm = VendorUtil.packVendorInfomation(vendorForm,
					vendorInfo, vendorId, vendorMaster, vendorCertificate,
					userDetails);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				/*
				 * List of Vendor Keywords.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				List<CustomerVendorKeywords> keywords = null;
				keywords = documentsDao.getVendorKeywords(vendorId, userDetails);
				session.setAttribute("vendorKeywords", keywords);
				return mapping.findForward("saveservicearea");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
				
				// Show the list of Vendor Commodities.
				/*We need this code, Please Don't Remove Even in New Architecture.*/
				List<CustomerVendorCommodity> vendorCommodities = null;
				vendorCommodities = documentsDao.getVendorCommodities(vendorId, userDetails);
				List<CommodityDto> commodities=documentsDao.listVendorCommodities(vendorId, userDetails);
				session.setAttribute("vendorCommodities", commodities);
//				session.setAttribute("vendorCommodities", vendorCommodities);

				List<GeographicalStateRegionDto> serviceAreas1 = null;
				serviceAreas1 = regionDao.getServiceAreasList(userDetails);
				session.setAttribute("serviceAreas1", serviceAreas1);

				CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
				List<CustomerServiceArea> serviceAreaList = curdDao.list(
						userDetails, " from CustomerServiceArea ");
				session.setAttribute("serviceAreaList", serviceAreaList);
				/*
				 * Get the selected vendor service and business area.
				 */
				CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
				List<VendorBusinessArea> businessAreas = curdDao1.list(
						userDetails,
						"from VendorBusinessArea where isActive = 1 and vendorId="
								+ vendorId);
				CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
				List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
						userDetails,
						"from CustomerVendorServiceArea where isActive = 1 and vendorId="
								+ vendorId);
				// Get vendor service area details
				vendorForm = VendorUtil.packVendorServiceAreas(vendorForm,
						serviceAreas, vendorId);
				// Get vendor business area details
				vendorForm = VendorUtil.packVendorBusinessAreas(vendorForm,
						businessAreas, vendorId);

				/*
				 * Get the list of certificate business area configuration for
				 * apply business rules in business area selection.
				 */
				CURDDao<CustomerCertificateBusinesAreaConfig> dao = new CURDTemplateImpl<CustomerCertificateBusinesAreaConfig>();
				List<CustomerCertificateBusinesAreaConfig> businessAreaconfig = dao
						.list(userDetails,
								"from CustomerCertificateBusinesAreaConfig where isActive = 1");
				session.setAttribute("businessAreaconfig", businessAreaconfig);

				/*
				 * Diverse certificate list of vendor
				 */
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				
				//To Get BP Market Sector List
				CommodityDaoImpl commodityDao=new CommodityDaoImpl();
				String query = "From MarketSector where isActive=1 order by sectorDescription";
				List<MarketSector> marketSectors = commodityDao.marketSectorAndCommodities(userDetails, query);
				if(marketSectors != null) {
					session.setAttribute("marketsectors", marketSectors);
				}
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitservicearea");
	}

	/**
	 * Save partial business area and commodity data from new self registration
	 * page wizard type.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveBusinessArea(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		String url = request.getRequestURL().toString()
				.replaceAll(request.getServletPath(), "").trim();

		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeBusinessArea(vendorForm, userId,
					vendorId, userDetails, url);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.business.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			/**
			 * Load vendor data
			 */
			VendorDao vendorDao = new VendorDaoImpl();
			RetriveVendorInfoDto vendorInfo = null;
			VendorMaster vendorMaster = null;
			List<VendorCertificate> vendorCertificate = null;
			vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
					vendorId, userDetails);
			vendorMaster = vendorInfo.getVendorMaster();
			vendorCertificate = vendorInfo.getVendorCertificate();
			vendorForm = VendorUtil.packVendorInfomation(vendorForm,
					vendorInfo, vendorId, vendorMaster, vendorCertificate,
					userDetails);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
				
				// Show the list of Vendor Commodities.
				/*We need this code, Please Don't Remove Even in New Architecture.*/
				List<CustomerVendorCommodity> vendorCommodities = null;				
				vendorCommodities = documentsDao.getVendorCommodities(vendorId, userDetails);
				List<CommodityDto> commodities=documentsDao.listVendorCommodities(vendorId, userDetails);
				session.setAttribute("vendorCommodities", commodities);
//				session.setAttribute("vendorCommodities", vendorCommodities);
				/*
				 * Show list of service areas.
				 */
				List<GeographicalStateRegionDto> serviceAreas1 = null;
				serviceAreas1 = regionDao.getServiceAreasList(userDetails);
				session.setAttribute("serviceAreas1", serviceAreas1);
				/*
				 * Show list of business areas.
				 */
				CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
				List<CustomerServiceArea> serviceAreaList = curdDao.list(
						userDetails, " from CustomerServiceArea ");
				session.setAttribute("serviceAreaList", serviceAreaList);

				/*
				 * Get the selected vendor service and business area.
				 */
				CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
				List<VendorBusinessArea> businessAreas = curdDao1.list(
						userDetails,
						"from VendorBusinessArea where isActive = 1 and vendorId="
								+ vendorId);
				CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
				List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
						userDetails,
						"from CustomerVendorServiceArea where isActive = 1 and vendorId="
								+ vendorId);
				// Get vendor service area details
				vendorForm = VendorUtil.packVendorServiceAreas(vendorForm,
						serviceAreas, vendorId);
				// Get vendor business area details
				vendorForm = VendorUtil.packVendorBusinessAreas(vendorForm,
						businessAreas, vendorId);

				/*
				 * Get the list of certificate business area configuration for
				 * apply business rules in business area selection.
				 */
				CURDDao<CustomerCertificateBusinesAreaConfig> dao = new CURDTemplateImpl<CustomerCertificateBusinesAreaConfig>();
				List<CustomerCertificateBusinesAreaConfig> businessAreaconfig = dao
						.list(userDetails,
								"from CustomerCertificateBusinesAreaConfig where isActive = 1");
				session.setAttribute("businessAreaconfig", businessAreaconfig);

				/*
				 * Diverse certificate list of vendor
				 */
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				List<Certificate> certificates2 = null;
				certificates2 = certificate.listOfCertificatesByVendor(
						userDetails, vendorMaster);
				session.setAttribute("diverseClassification", certificates2);
				
				//To Get BP Market Sector List
				CommodityDaoImpl commodityDao=new CommodityDaoImpl();
				String query = "From MarketSector where isActive=1 order by sectorDescription";
				List<MarketSector> marketSectors = commodityDao.marketSectorAndCommodities(userDetails, query);
				if(marketSectors != null) {
					session.setAttribute("marketsectors", marketSectors);
				}

				return mapping.findForward("savebusinessarea");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				vendorForm = VendorUtil.packBioGraphySafety(vendorForm,
						vendorId, userDetails);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitbusinessarea");
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	private Byte getDeverseSupplier(String string) {

		if (string != null && string.length() != 0) {
			if (string.equalsIgnoreCase("on")) {
				return (byte) 1;
			} else {
				return (byte) 0;
			}
		} else {
			return (byte) 0;
		}

	}

	/**
	 * Save business biography and safety.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveBusinessBiography(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeBusinessBiography(vendorForm,
					userId, vendorId, userDetails);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.biography.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.reset(mapping, request);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				vendorForm = VendorUtil.packBioGraphySafety(vendorForm,
						vendorId, userDetails);
				return mapping.findForward("savebusinessbiography");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}
				List<com.fg.vms.customer.model.Country> countries = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) cdao1.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}
				vendorForm.setRefStates1(statesList);
				vendorForm.setRefStates2(statesList);
				vendorForm.setRefStates3(statesList);				
				
				VendorDao vendorDao = new VendorDaoImpl();
				List<CustomerVendorreference> vendorreferences = vendorDao.getListReferences(vendorId, userDetails);
				vendorForm = VendorUtil.packVendorReference(vendorForm, vendorreferences, userDetails, countryId);				
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitbusinessbiography");
	}

	/**
	 * Save References data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveReferences(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeReferences(vendorForm, userId,
					vendorId, userDetails);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.reference.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				String query = "From Country where isactive=1 order by countryname";
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						query = "From Country where isactive=1 and isDefault=1 order by countryname";
					}
				}
				List<com.fg.vms.customer.model.Country> countries = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails, query);
				session.setAttribute("countryList", countries);
				
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) cdao1.list(userDetails,"From Country where isDefault=1");
				countryId = country.get(0).getId();		

				List<State> statesList = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						statesList = (List<State>) cdao1.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
					}
				}
				vendorForm.setRefStates1(statesList);
				vendorForm.setRefStates2(statesList);
				vendorForm.setRefStates3(statesList);
				
				VendorDao vendorDao = new VendorDaoImpl();
				List<CustomerVendorreference> vendorreferences = vendorDao
						.getListReferences(vendorId, userDetails);
				vendorForm = VendorUtil.packVendorReference(vendorForm, vendorreferences, userDetails, countryId);				
				return mapping.findForward("savereferences");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorOtherCertificate> otherCertificate = null;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(vendorId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				otherCertificate = vendorInfo.getVendorOtherCert();
				List<VendorCertificate> vendorCetrificate = null;

				vendorMaster = vendorInfo.getVendorMaster();
				vendorCetrificate = vendorInfo.getVendorCertificate();

				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, vendorId, vendorMaster, vendorCetrificate,
						userDetails);
				session.setAttribute("vendorInfoDto", vendorInfo);
				session.setAttribute("otherCertificates", otherCertificate);
				List<Certificate> qualityCertificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				qualityCertificates = certificate.listOfCertificatesByType(
						(byte) 0, userDetails);
				session.setAttribute("qualityCertificates", qualityCertificates);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitreferences");
	}

	/**
	 * Save other certificates data.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveOtherCertificates(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		String netWorldResult =null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeOtherCertificate(vendorForm,
					userId, vendorId, userDetails);
			netWorldResult =anonymousVendorDao.addVendorNetWorldCertificate(vendorForm, 
					userId, vendorId, userDetails);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result) && ( "success".equalsIgnoreCase(netWorldResult) || "saved".equalsIgnoreCase(netWorldResult))) {
			ActionMessages messages = new ActionMessages();
			ActionMessage netmsg=null;			
			ActionMessage msg = new ActionMessage("vendor.quality.save.ok");
			if("success".equalsIgnoreCase(netWorldResult)){
				netmsg =new ActionMessage("netwotld.save.ok");
				}else{
				netmsg =new ActionMessage("netwotld.saved.ok");
				}
			messages.add("vendor", msg);
			messages.add("networld",netmsg);
			saveMessages(request, messages);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			vendorForm = SupplierNavigationAction.getContactInformation(
					vendorForm, userDetails, vendorId);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDao vendorDao = new VendorDaoImpl();
				RetriveVendorInfoDto vendorInfo = null;
				VendorMaster vendorMaster = null;
				List<VendorOtherCertificate> otherCertificate = null;
				String networldcertificate =null;
				Boolean hasnetworldcsrtificate=false;
				vendorInfo = (RetriveVendorInfoDto) vendorDao
						.retriveVendorInfo(vendorId, userDetails);
				vendorMaster = vendorInfo.getVendorMaster();
				otherCertificate = vendorInfo.getVendorOtherCert();
				networldcertificate=vendorInfo.getIsNetWorldCertificate();
				hasnetworldcsrtificate=vendorInfo.getHasNetWoldcert();
				List<VendorCertificate> vendorCetrificate = null;

				vendorMaster = vendorInfo.getVendorMaster();
				vendorCetrificate = vendorInfo.getVendorCertificate();

				vendorForm = VendorUtil.packVendorInfomation(vendorForm,
						vendorInfo, vendorId, vendorMaster, vendorCetrificate,
						userDetails);
				session.setAttribute("vendorInfoDto", vendorInfo);
				session.setAttribute("otherCertificates", otherCertificate);
				if(hasnetworldcsrtificate){
					session.setAttribute("networldcertificate", networldcertificate);
					session.setAttribute("hasnetworldcsrtificate", hasnetworldcsrtificate);
					}
				List<Certificate> qualityCertificates = null;
				CertificateDaoImpl certificate = new CertificateDaoImpl();
				qualityCertificates = certificate.listOfCertificatesByType(
						(byte) 0, userDetails);
				session.setAttribute("qualityCertificates", qualityCertificates);
				return mapping.findForward("saveothercertificate");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				vendorForm.setId(vendorId);
				/*
				 * List of vendor documents.
				 */
				List<VendorDocuments> documents = null;
				documents = documentsDao.getVendorDocuments(vendorId,
						userDetails);
				session.setAttribute("vendorDocuments", documents);
				WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration configuration = configurationDao
						.listWorkflowConfig(userDetails);
				vendorForm.setConfiguration(configuration);
				int uploadRestriction = configuration.getDocumentUpload();
				session.setAttribute("uploadRestriction", uploadRestriction);
				int documentMaxSize = configuration.getDocumentSize();
				session.setAttribute("documentMaxSize", documentMaxSize);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}else if(netWorldResult.equals("failure")){
			ActionMessages messages = new ActionMessages();
			ActionMessage netmsg = new ActionMessage("transaction.failure");
			messages.add("networldfailure", netmsg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitothercertificate");
	}

	/**
	 * Save vendor documents.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveDocuments(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		String submitType = request.getParameter("submitType");
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {
			vendorForm.setSubmitType(submitType);
			result = anonymousVendorDao.mergeDocuments(vendorForm, userId,
					vendorId, userDetails);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.document.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				/**
				 * Information saved, stay in same page.
				 */
				VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
				vendorForm.setId(vendorId);
				/*
				 * List of vendor documents.
				 */
				List<VendorDocuments> documents = null;
				documents = documentsDao.getVendorDocuments(vendorId,
						userDetails);
				session.setAttribute("vendorDocuments", documents);
				WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration configuration = configurationDao
						.listWorkflowConfig(userDetails);
				vendorForm.setConfiguration(configuration);
				int uploadRestriction = configuration.getDocumentUpload();
				session.setAttribute("uploadRestriction", uploadRestriction);
				int documentMaxSize = configuration.getDocumentSize();
				session.setAttribute("documentMaxSize", documentMaxSize);
				return mapping.findForward("savedocuments");
			} else {
				/**
				 * Move to next page and update the data.
				 */
				CURDDao<CustomerVendorMeetingInfo> curdDao = new CURDTemplateImpl<CustomerVendorMeetingInfo>();
				List<CustomerVendorMeetingInfo> vendorMeetingInfo = curdDao
						.list(userDetails,
								"from CustomerVendorMeetingInfo where vendorId="
										+ vendorId);
				if (null != vendorMeetingInfo && vendorMeetingInfo.size() > 0) {
					CustomerVendorMeetingInfo meetingInfo = vendorMeetingInfo
							.get(0);
					VendorUtil.packVendorContactMeetingInfomation(vendorForm,
							meetingInfo);
				}
				
				//Always Display US States as Default Contact States.
				CURDDao<?> dao = new CURDTemplateImpl();
				Integer countryId = null;
				List<com.fg.vms.customer.model.Country> country = (List<com.fg.vms.customer.model.Country>) dao.list(userDetails,"From Country where countryname = 'United States'");
				countryId = country.get(0).getId();		

				List<State> contactStates = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
				session.setAttribute("contactStates", contactStates);
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		} else if (result.equals("hugeFileSize")) {
			// if File Size Higher than Specified Limit, return to input page with message
			int documentMaxSize=0;
			if(session.getAttribute("documentMaxSize") != null)
			{
				documentMaxSize=(Integer) session.getAttribute("documentMaxSize");
			}				
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.hugeFileSize", documentMaxSize);
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitdocuments");
	}

	/**
	 * Save vendor contact meeting information.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward saveMeetingInfo(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String result = null;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		VendorDao vendorDao = new VendorDaoImpl();
		String submitType = request.getParameter("submitType");
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {
			result = vendorDao.saveMeetingDetails(userDetails, vendorForm,
					vendorId);
		}
		if (null != submitType && submitType.equalsIgnoreCase("saveandexit")) {
			return mapping.findForward("logout");
		}
		if ("success".equalsIgnoreCase(result)) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
			vendorForm.setId(vendorId);
			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);
			if (submitType != null && !submitType.isEmpty()
					&& submitType.equalsIgnoreCase("save")) {
				return mapping.findForward("meetinginfo");
			}
			
			//Getting Terms & Condition from Database, and Display It in the View Page.
			List<VendorContact> vendorContact = (List<VendorContact>) cdao1.list(userDetails,
							" From VendorContact where vendorId=" + vendorId);
			String firstName = (null != vendorContact.get(0).getFirstName()) ? vendorContact.get(0).getFirstName() : "";
			String lastName = (null != vendorContact.get(0).getLastName()) ? vendorContact.get(0).getLastName() : ""; 
			String name = firstName + " " + lastName;
			String designation = (null != vendorContact.get(0).getDesignation()) ? vendorContact.get(0).getDesignation() : "";
			
			if(null != userDetails && null != userDetails.getSettings())
			{
				if(null != userDetails.getSettings().getTermsCondition())
				{		
					String TC = userDetails.getSettings().getTermsCondition();				
					TC = TC.replace("#NAME", name);
					TC = TC.replace("#DESIGNATION", designation);
					vendorForm.setTermsCondition(TC);
				}
			}
		} else if (result.equals("failure")) {
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitmeetinginfo");
	}

	/**
	 * Submit self registration.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ActionForward submitSelfRegistration(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		Integer userId = -1;
		List<String> vendorErrors = new ArrayList<String>();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {
			vendorErrors = anonymousVendorDao.submitRegistration(vendorForm,
					userId, vendorId, userDetails, vendorErrors);
		}
		if (null != vendorErrors && vendorErrors.size() > 0) {

			vendorForm.setVendorErrors(vendorErrors);

			CURDDao<?> cdao1 = new CURDTemplateImpl();
			List<CustomerVendorMenuStatus> menuStatus = (List<CustomerVendorMenuStatus>) cdao1
					.list(userDetails,
							" From CustomerVendorMenuStatus where vendorId="
									+ vendorId);
			session.setAttribute("menuStatus", menuStatus);

			// if validation fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("validation.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
			return mapping.getInputForward();
		}
		return mapping.findForward("submitregistration");
	}

	/**
	 * Exit the profile and goto self registration login page.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward selfRegLogout(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Inside the self registration exit...");
		HttpSession session = request.getSession();

		session.setAttribute("primeSelfRegistration", null);
		session.setAttribute("tempLoginEmail", null);
		session.setAttribute("tempLoginPassword", null);
		session.setAttribute("contactStates", null);
		session.setAttribute("meetingVendor", null);

		log.info("Successfully exit from self registration profile...");
		return mapping.findForward("logout");
	}

	/**
	 * To Save / Update PICS Certificate.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward savePicsCertificate(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.info("In savePicsCertificate");
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		AnonymousVendorDao anonymousVendorDao = new AnonymousVendorDaoImpl();
		
		String result = null;
		Integer userId = -1;
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorId = null;
		if (null != session.getAttribute("vendorId")) {
			vendorId = (Integer) session.getAttribute("vendorId");
		}
		if (vendorId != null && vendorId != 0) {			
			result = anonymousVendorDao.savePicsCertificate(vendorForm, userId, vendorId, userDetails);
		}		
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result",result);
		response.getWriter().println(jsonObject);
		return null;
	}
}
