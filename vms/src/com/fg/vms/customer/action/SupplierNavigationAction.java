/**
 * SupplierNavigationAction.java
 */
package com.fg.vms.customer.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CertificateDaoImpl;
import com.fg.vms.customer.dao.impl.CommodityDaoImpl;
import com.fg.vms.customer.dao.impl.EthnicityDaoImpl;
import com.fg.vms.customer.dao.impl.GeographicalStateRegionDaoImpl;
import com.fg.vms.customer.dao.impl.SearchDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDaoImpl;
import com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.dto.EmailHistoryDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.MeetingNotesDto;
import com.fg.vms.customer.dto.RFIRPFNotesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerBusinessType;
import com.fg.vms.customer.model.CustomerCertificateBusinesAreaConfig;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerLegalStructure;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.MeetingNotes;
import com.fg.vms.customer.model.RFIRPFNotes;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.customer.pojo.VendorStatusPojo;
import com.fg.vms.util.Constants;
import com.fg.vms.util.VendorUtil;

/**
 * Defined Supplier Action class for navigation among Supplier details like
 * Update contact, services, diverse classification, other classifications,
 * vendor classifications, vendor notes, references, documents, business
 * biography.
 * 
 * @author vinoth
 * 
 */
public class SupplierNavigationAction extends DispatchAction {

	private Logger logger = Constants.logger;

	/**
	 * Method for navigate contact information page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward contactNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor contact information page");
		HttpSession session = request.getSession();
		VendorDao vendorDao = new VendorDaoImpl();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		session.setAttribute("vendorInfoDto", vendorInfo);
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				supplierId, vendorMaster, vendorCertificate, userDetails);
		return mapping.findForward("contactinfo");
	}

	/**
	 * Method for navigate company information page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward companyInfoNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor company information page");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		return mapping.findForward("companyinfo");
	}

	/**
	 * Method for navigate company address page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward companyAddressNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor company address page");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		CURDDao<com.fg.vms.customer.model.Country> dao1 = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		
		String query = "From Country where isactive=1 order by countryname";
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}		
		List<com.fg.vms.customer.model.Country> countries = dao1.list(userDetails, query);
		session.setAttribute("countryList", countries);
		
		Integer countryId = null;
		List<Country> country = (List<Country>) dao1.list(userDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();		

		List<State> statesList = null;
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				statesList = (List<State>) dao.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
			}
		}		
		vendorForm.setPhyStates1(statesList);
		vendorForm.setMailingStates2(statesList);
		
		CustomerVendorAddressMaster phyAddress = documentsDao
				.getMailingAddress(supplierId, "p", userDetails);
		if (phyAddress != null) {
			vendorForm.setAddress1(phyAddress.getAddress());
			vendorForm.setCity(phyAddress.getCity());
			vendorForm.setState(phyAddress.getState());
			vendorForm.setProvince(phyAddress.getProvince());
			vendorForm.setRegion(phyAddress.getRegion());
			vendorForm.setCountry(phyAddress.getCountry());
			vendorForm.setZipcode(phyAddress.getZipCode());
			vendorForm.setMobile(phyAddress.getMobile());
			vendorForm.setPhone(phyAddress.getPhone());
			vendorForm.setFax(phyAddress.getFax());
			if (null != phyAddress.getCountry() && !phyAddress.getCountry().isEmpty()) {
				List<State> phyStates = null;				
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						phyStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
					} else {
						phyStates = dao.list(userDetails, " From State where countryid=" + phyAddress.getCountry() + " and isactive=1 order by statename");
					}
				}
				vendorForm.setPhyStates1(phyStates);
			}
		}
		CustomerVendorAddressMaster addressMaster = documentsDao
				.getMailingAddress(supplierId, "m", userDetails);
		if (addressMaster != null) {
			vendorForm.setAddress2(addressMaster.getAddress());
			vendorForm.setCity2(addressMaster.getCity());
			vendorForm.setState2(addressMaster.getState());
			vendorForm.setProvince2(addressMaster.getProvince());
			vendorForm.setRegion2(addressMaster.getRegion());
			vendorForm.setCountry2(addressMaster.getCountry());
			vendorForm.setZipcode2(addressMaster.getZipCode());
			vendorForm.setMobile2(addressMaster.getMobile());
			vendorForm.setPhone2(addressMaster.getPhone());
			vendorForm.setFax2(addressMaster.getFax());
			if (null != addressMaster.getCountry() && !addressMaster.getCountry().isEmpty()) {
				List<State> mStates = null;
				if(userDetails.getWorkflowConfiguration() != null) {
					if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
						mStates = dao.list(userDetails, " From State where countryid=" + countryId + " and isactive=1 order by statename");
					} else {
						mStates = dao.list(userDetails, " From State where countryid=" + addressMaster.getCountry() + " and isactive=1 order by statename");
					}
				}
				vendorForm.setMailingStates2(mStates);
			}
		}
		return mapping.findForward("addressinfo");
	}

	/**
	 * Method for navigate company owner page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward companyOwnerNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor company owner page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities",
				ethnicityDao.list(userDetails, null));
		if (null != vendorForm.getCompanyOwnership()
				&& !vendorForm.getCompanyOwnership().isEmpty()) {
			vendorForm.setOwnerPublic("1");
			vendorForm.setOwnerPrivate("2");
			vendorForm.setCompanyOwnership(vendorForm.getCompanyOwnership());
		} else {
			vendorForm.setOwnerPublic("1");
			vendorForm.setOwnerPrivate("2");
			vendorForm.setCompanyOwnership(null);
		}
		return mapping.findForward("ownerinfo");
	}

	/**
	 * Method for navigate diverse classification page from menu and back
	 * button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward diverseClassificationNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor diverse classification page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		/**
		 * Load vendor data
		 */
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		vendorForm.setId(supplierId);
		vendorForm
				.setDeverseSupplier(vendorMaster.getDeverseSupplier() == 1 ? "on"
						: "");
		// get the list of certificates by diverse.
		List<Certificate> certificates = null;
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		certificates = certificate.listOfCertificatesByType((byte) 1,
				userDetails);
		session.setAttribute("certificateTypes", certificates);
		// Get vendor diverse classification list
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(userDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);
		// Certificate Type
		CURDDao<CustomerCertificateType> curdDao2 = new CURDTemplateImpl<CustomerCertificateType>();
		List<CustomerCertificateType> certificateTypesList = curdDao2.list(
				userDetails, " from CustomerCertificateType ");
		session.setAttribute("certificateTypesList", certificateTypesList);
		// certificate agencies list.
		SearchDao searchDao = new SearchDaoImpl();
		List<CertifyingAgency> certificateAgencies;
		certificateAgencies = searchDao.listAgencies(userDetails);
		session.setAttribute("certAgencyList", certificateAgencies);
		// ethnicities list
		CURDDao<Ethnicity> ethnicityDao = new EthnicityDaoImpl<Ethnicity>();
		session.setAttribute("ethnicities",
				ethnicityDao.list(userDetails, null));
		return mapping.findForward("diverseclassification");
	}

	/**
	 * Method for navigate service area page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward serviceAreaNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor service area page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorForm.setVendorStatus(vendorMaster.getVendorStatus());
		
		/*
		 * List of Vendor Keywords.
		 */
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		List<CustomerVendorKeywords> keywords = null;
		keywords = documentsDao.getVendorKeywords(supplierId, userDetails);
		session.setAttribute("vendorKeywords", keywords);
		return mapping.findForward("servicearea");
	}

	/**
	 * Method for navigate business area page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActionForward businessAreaNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor business area page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		vendorForm.setVendorStatus(vendorMaster.getVendorStatus());
		
		/**
		 * Get data from vendor master.
		 */
		vendorForm.setCompanyInformation(vendorMaster.getCompanyInformation());
		vendorForm.setVendorDescription(vendorMaster.getVendorDescription());

		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		GeographicalStateRegionDao regionDao = new GeographicalStateRegionDaoImpl();
		List<CustomerVendorCommodity> vendorCommodities = null;
		// Show the list of Vendor Commodities.
		vendorCommodities = documentsDao.getVendorCommodities(supplierId,
				userDetails);
		List<CommodityDto> commodities=documentsDao.listVendorCommodities(supplierId, userDetails);
		session.setAttribute("vendorCommodities", commodities);
//		session.setAttribute("vendorCommodities", vendorCommodities);
		
		/*
		 * Show list of service areas.
		 */
		List<GeographicalStateRegionDto> serviceAreas1 = null;
		serviceAreas1 = regionDao.getServiceAreasList(userDetails);
		session.setAttribute("serviceAreas1", serviceAreas1);
		/*
		 * Show list of business areas.
		 */
		CURDDao<CustomerServiceArea> curdDao = new CURDTemplateImpl<CustomerServiceArea>();
		List<CustomerServiceArea> serviceAreaList = curdDao.list(userDetails,
				" from CustomerServiceArea ");
		session.setAttribute("serviceAreaList", serviceAreaList);
		/*
		 * Get the selected vendor service and business area.
		 */
		CURDDao<VendorBusinessArea> curdDao1 = new CURDTemplateImpl<VendorBusinessArea>();
		List<VendorBusinessArea> businessAreas = curdDao1.list(userDetails,
				"from VendorBusinessArea where isActive = 1 and vendorId="
						+ supplierId);
		CURDDao<CustomerVendorServiceArea> serviceDao = new CURDTemplateImpl<CustomerVendorServiceArea>();
		List<CustomerVendorServiceArea> serviceAreas = serviceDao.list(
				userDetails,
				"from CustomerVendorServiceArea where isActive = 1 and vendorId="
						+ supplierId);
		// Get vendor service area details
		vendorForm = VendorUtil.packVendorServiceAreas(vendorForm,
				serviceAreas, supplierId);
		// Get vendor business area details
		vendorForm = VendorUtil.packVendorBusinessAreas(vendorForm,
				businessAreas, supplierId);

		/*
		 * Get the list of certificate business area configuration for apply
		 * business rules in business area selection.
		 */
		CURDDao<CustomerCertificateBusinesAreaConfig> dao = new CURDTemplateImpl<CustomerCertificateBusinesAreaConfig>();
		List<CustomerCertificateBusinesAreaConfig> businessAreaconfig = dao
				.list(userDetails,
						"from CustomerCertificateBusinesAreaConfig where isActive = 1");
		session.setAttribute("businessAreaconfig", businessAreaconfig);

		/*
		 * Diverse certificate list of vendor
		 */
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		List<Certificate> certificates2 = null;
		certificates2 = certificate.listOfCertificatesByVendor(userDetails,
				vendorMaster);
		session.setAttribute("diverseClassification", certificates2);
		
		//To Get BP Market Sector List
		CommodityDaoImpl commodityDao=new CommodityDaoImpl();
		String query = "From MarketSector where isActive=1 order by sectorDescription";
		List<MarketSector> marketSectors = commodityDao.marketSectorAndCommodities(userDetails, query);
		if(marketSectors != null) {
			session.setAttribute("marketsectors", marketSectors);
		}
		
		String query1 = "select max(customer_commodity.createdon)commoditycreated,(select max(customer_vendorcommodity.CREATEDON) from "
				+ "customer_vendorcommodity where customer_vendorcommodity.VENDORID="+supplierId+")vendorcommodity "
				+ "from  customer_commodity";
		Date comodityCreated = null;
        Date vendorComodity = null;
		List comodityUpdtedResult=commodityDao.getUpdtedComodityResult(userDetails, query1);
		if(comodityUpdtedResult != null){
		for(Object comodityResult:comodityUpdtedResult){
			Object []row=(Object[]) comodityResult;
			comodityCreated = (Date) row[0];
			vendorComodity = (Date) row[1];
		}
		}
		
		if(comodityCreated != null && vendorComodity != null){
		if(comodityCreated.after(vendorComodity)){
			session.setAttribute("displayComodity", "active");
		}else{
			session.setAttribute("displayComodity", "nonactive");
		}
		}else{
			session.setAttribute("displayComodity", "nonactive");
		}
		
		
		return mapping.findForward("businessarea");
	}

	/**
	 * Method for navigate biography and safety page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward biographyNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor biography and safety page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		vendorForm.setId(supplierId);
		vendorForm = VendorUtil.packBioGraphySafety(vendorForm, supplierId,
				userDetails);
		return mapping.findForward("businessbiography");
	}

	/**
	 * Method for navigate references page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward referencesNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor references page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm.setId(supplierId);
		
		CURDDao<com.fg.vms.customer.model.Country> dao = new CURDTemplateImpl<com.fg.vms.customer.model.Country>();
		
		String query = "From Country where isactive=1 order by countryname";
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				query = "From Country where isactive=1 and isDefault=1 order by countryname";
			}
		}		
		List<com.fg.vms.customer.model.Country> countries = dao.list(userDetails, query);
		session.setAttribute("countryList", countries);
		
		Integer countryId = null;
		List<Country> country = (List<Country>) dao.list(userDetails,"From Country where isDefault=1");
		countryId = country.get(0).getId();		

		CURDDao<State> cdao1 = new CURDTemplateImpl<State>();
		List<State> statesList = null;
		if(userDetails.getWorkflowConfiguration() != null) {
			if(userDetails.getWorkflowConfiguration().getInternationalMode() != null && userDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
				statesList = (List<State>) cdao1.list(userDetails,"From State where countryid=" + countryId + " and isactive=1 order by statename");
			}
		}		
		vendorForm.setRefStates1(statesList);
		vendorForm.setRefStates2(statesList);
		vendorForm.setRefStates3(statesList);
		
		VendorDao vendorDao = new VendorDaoImpl();
		List<CustomerVendorreference> vendorreferences = vendorDao
				.getListReferences(supplierId, userDetails);
		vendorForm = VendorUtil.packVendorReference(vendorForm,	vendorreferences, userDetails, countryId);		
		
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		return mapping.findForward("references");
	}

	/**
	 * Method for navigate to other quality certificates page from menu and back
	 * button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward otherCertificateNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor other quality certificates page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm.setId(supplierId);
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorOtherCertificate> otherCertificate = null;
		String networldcertificate =null;
		Boolean hasnetworldcsrtificate=false;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		otherCertificate = vendorInfo.getVendorOtherCert();
		networldcertificate=vendorInfo.getIsNetWorldCertificate();
		hasnetworldcsrtificate=vendorInfo.getHasNetWoldcert();
		session.setAttribute("vendorInfoDto", vendorInfo);
		session.setAttribute("otherCertificates", otherCertificate);
		
		if(hasnetworldcsrtificate){
		session.setAttribute("networldcertificate", networldcertificate);
		session.setAttribute("hasnetworldcsrtificate", hasnetworldcsrtificate);
		}
		List<Certificate> qualityCertificates = null;
		CertificateDaoImpl certificate = new CertificateDaoImpl();
		qualityCertificates = certificate.listOfCertificatesByType((byte) 0,
				userDetails);
		session.setAttribute("qualityCertificates", qualityCertificates);
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		return mapping.findForward("othercertificate");
	}

	/**
	 * Method for navigate to vendor documents page from menu and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward documentsNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor documents page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		vendorForm.setId(supplierId);
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		/*
		 * List of vendor documents.
		 */
		List<VendorDocuments> documents = new ArrayList<VendorDocuments>();
		List<VendorDocuments> vendorDocuments = null;
		vendorDocuments = documentsDao.getVendorDocuments(supplierId, userDetails);
		for(VendorDocuments vendorDocument:vendorDocuments){
			String docSize=humanReadableByteCount(vendorDocument.getDocumentFilesize(),true);
			vendorDocument.setDocumentSize(docSize);
			documents.add(vendorDocument);
		}
		session.setAttribute("vendorDocuments", documents);
		WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
		WorkflowConfiguration configuration = configurationDao
				.listWorkflowConfig(userDetails);
		vendorForm.setConfiguration(configuration);
		int uploadRestriction = configuration.getDocumentUpload();		
		session.setAttribute("uploadRestriction", uploadRestriction);
		int documentMaxSize = configuration.getDocumentSize();
		session.setAttribute("documentMaxSize", documentMaxSize);
		
		return mapping.findForward("documents");
	}
	
	public static String humanReadableByteCount(double bytes, boolean si) {
	    int unit = si ? 1024 : 1000;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "KMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
	    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}
	/**
	 * Method for navigate to vendor contact meeting information page from menu
	 * and back button.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward contactMeetingNavigation(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Show vendor meeting information page..");
		HttpSession session = request.getSession();
		Integer supplierId = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		if (null != session.getAttribute("supplierId")) {
			supplierId = (Integer) session.getAttribute("supplierId");
		}
		vendorForm.setId(supplierId);
		VendorDao vendorDao = new VendorDaoImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		// To fetch Vendor Status
		List<VendorStatusPojo> vendorStatusList = null;

		vendorStatusList = vendorDao.listVendorStatus(userDetails, supplierId);

		vendorMaster = vendorInfo.getVendorMaster();
//		vendorForm.setVendorNotes(vendorMaster.getVendorNotes());
		vendorForm = getContactInformation(vendorForm, userDetails, supplierId);
		CURDDao<CustomerVendorMeetingInfo> curdDao = new CURDTemplateImpl<CustomerVendorMeetingInfo>();
		List<CustomerVendorMeetingInfo> vendorMeetingInfo = curdDao.list(
				userDetails, "from CustomerVendorMeetingInfo where vendorId="
						+ supplierId);
		if (null != vendorMeetingInfo && vendorMeetingInfo.size() > 0) {
			CustomerVendorMeetingInfo meetingInfo = vendorMeetingInfo.get(0);
			vendorForm = VendorUtil.packVendorContactMeetingInfomation(
					vendorForm, meetingInfo);

		}
		vendorForm.setVendorStatusList(vendorStatusList);
		CURDDao<State> dao = new CURDTemplateImpl<State>();
		List<State> contactStates = dao.list(userDetails,
				" From State order by statename");
		session.setAttribute("contactStates", contactStates);
		
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + supplierId + " ORDER BY v.CREATEDON DESC";
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		vendorForm.setVendorNotes(null);
		
		
		String queryrfi = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + supplierId + " ORDER BY v.CREATEDON DESC";
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, queryrfi);
		vendorForm.setRfiNotesList(rfiMeetingList);
		vendorForm.setVendorrfiNotes(null);
		
		//Starting Code To Retrieve Email History.
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		vendorForm.setVendorRfiRfpNotes(vendorMaster.getRfiRfpNotes());
		//Ending Code To Retrieve Email History.
		
		return mapping.findForward("meetinginfo");
	}

	/**
	 * Get contact information.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static VendorMasterForm getContactInformation(
			VendorMasterForm vendorForm, UserDetailsDto userDetails,
			Integer supplierId) {

		VendorDao vendorDao = new VendorDaoImpl();
		CURDDao<?> cdao = new CURDTemplateImpl();
		RetriveVendorInfoDto vendorInfo = null;
		VendorMaster vendorMaster = null;
		List<VendorCertificate> vendorCertificate = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(
				supplierId, userDetails);
		vendorMaster = vendorInfo.getVendorMaster();
		vendorCertificate = vendorInfo.getVendorCertificate();
		vendorForm = VendorUtil.packVendorInfomation(vendorForm, vendorInfo,
				supplierId, vendorMaster, vendorCertificate, userDetails);
		List<CustomerBusinessType> businessTypes = (List<CustomerBusinessType>) cdao
				.list(userDetails,
						"From CustomerBusinessType where isactive=1 order by typeName");
		List<CustomerLegalStructure> legalStructures = (List<CustomerLegalStructure>) cdao
				.list(userDetails,
						"From CustomerLegalStructure where isactive=1 order by name");
		vendorForm.setLegalStructures(legalStructures);
		vendorForm.setBusinessTypes(businessTypes);
		vendorForm.setCompanytype(vendorMaster.getCompanyType());
		if (vendorMaster.getVendorStatus() != null
				&& !vendorMaster.getVendorStatus().isEmpty()) {
			CURDDao<StatusMaster> curdDao = new CURDTemplateImpl<StatusMaster>();
			List<StatusMaster> statusMasters = curdDao
					.list(userDetails,
							" From StatusMaster s where s.disporder >= ( select disporder from StatusMaster where id='"
									+ vendorMaster.getVendorStatus()
									+ "' ) order by s.disporder");
			vendorForm.setStatusMasters(statusMasters);
		}

		return vendorForm;

	}
	
	/**
	 * Method for Edit the Meeting Information
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editMeetingNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		vendorForm.setId(currentVendorId);
		MeetingNotes meetingNotes = null;
		Integer vendorNotesId = vendorForm.getVendorNotesId();
		if(vendorNotesId != null && vendorNotesId != 0)
		{
			meetingNotes = vendorDao.editMeetingNotes(userDetails, vendorNotesId);
		}
		if(meetingNotes != null)
		{
			vendorForm.setVendorNotesId(meetingNotes.getId());
			vendorForm.setVendorNotes(meetingNotes.getNotes());
		}
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		
		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setRfiNotesList(rfiMeetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
		
		return mapping.findForward("meetinginfo");
	}
	
	/**
	 * Method for Save or Update the Meeting Info
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward save_Or_Update_MeetingNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorNotesId = vendorForm.getVendorNotesId();
		String vendorNotes = vendorForm.getVendorNotes().trim();
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		vendorForm.setId(currentVendorId);
		Integer currentUserId = (Integer) session.getAttribute("userId");
		
		MeetingNotesDto meetingDto = new MeetingNotesDto();
		if(vendorNotes != null && !vendorNotes.equalsIgnoreCase(""))
		{
			meetingDto.setMeetingNotesId(vendorNotesId);
			meetingDto.setVendorId(currentVendorId);
			meetingDto.setNotes(vendorNotes);
			meetingDto.setCreatedBy(String.valueOf(currentUserId));
		}
		String result = vendorDao.save_Or_Update_MeetingNotes(userDetails, meetingDto);
		if ("success".equalsIgnoreCase(result)) 
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.notes.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		} 
		else if(result.equals("update"))
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.notes.update.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		}
		else if (result.equals("failure")) 
		{
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
		}
		vendorForm.setVendorNotes(null);
		vendorForm.setVendorNotesId(null);
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + currentVendorId + " ORDER BY v.CREATEDON DESC";
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		
		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setRfiNotesList(rfiMeetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
		
		return mapping.findForward("meetinginfo");
	}
	
	
	public ActionForward saveRfiRfpNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		String vendorRfiRfpNotes = vendorForm.getVendorRfiRfpNotes().trim();
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		vendorForm.setId(currentVendorId);
		Integer currentUserId = (Integer) session.getAttribute("userId");
		
		VendorMaster vendorMaster = vendorDao.retrieveVendorById(userDetails, currentVendorId);
		vendorMaster.setRfiRfpNotes(vendorRfiRfpNotes);
		String result = vendorDao.updateVendorMaster(userDetails, vendorMaster);
		
		if ("success".equalsIgnoreCase(result)) 
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.rfirfp.notes.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		} 
		vendorForm.setVendorNotes(null);
		vendorForm.setVendorNotesId(null);
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + currentVendorId + " ORDER BY v.CREATEDON DESC";
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
		
		return mapping.findForward("meetinginfo");
	}
	
	/**
	 * Method for delete the Meeting Notes.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleteMeetingNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		vendorForm.setId(currentVendorId);
		Integer vendorNotesId = vendorForm.getVendorNotesId();
		String result = "failure";
		if(vendorNotesId != null && vendorNotesId != 0)
		{
			result = vendorDao.deleteMeetingNote(userDetails, vendorNotesId);
		}
		if ("deleted".equalsIgnoreCase(result)) 
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.notes.delete.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		} 
		else if (result.equals("failure")) 
		{
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
		}
		vendorForm.setVendorNotes(null);
		vendorForm.setVendorNotesId(null);
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setMeetingNotesList(meetingList);
		
		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setRfiNotesList(rfiMeetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
				
		return mapping.findForward("meetinginfo");
	}
	
	
	/**
	 * Method for Edit the RFI/RFP notes Information
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward editrfiMeetingNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		vendorForm.setId(currentVendorId);
		RFIRPFNotes rfiMeetingNotes = null;
		Integer vendorRfiNotesId = vendorForm.getVendorrfiNotesId();
		if(vendorRfiNotesId != null && vendorRfiNotesId != 0)
		{
			rfiMeetingNotes = vendorDao.editRfiMeetingNotes(userDetails, vendorRfiNotesId);
		}
		if(rfiMeetingNotes != null)
		{
			vendorForm.setVendorrfiNotesId(rfiMeetingNotes.getId());
			vendorForm.setVendorrfiNotes(rfiMeetingNotes.getNotes());

		}
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setRfiNotesList(rfiMeetingList);

		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setMeetingNotesList(meetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
		
		return mapping.findForward("meetinginfo");
	}
	
	/**
	 * Method for Save or Update the Meeting Info
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward save_Or_Update_rfiMeetingNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		Integer vendorrfiNotesId = vendorForm.getVendorrfiNotesId();
		String vendorRfiNotes = vendorForm.getVendorrfiNotes().trim();
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		vendorForm.setId(currentVendorId);
		Integer currentUserId = (Integer) session.getAttribute("userId");
		
		RFIRPFNotesDto meetingrfiDto = new RFIRPFNotesDto();
		if(vendorRfiNotes != null && !vendorRfiNotes.equalsIgnoreCase(""))
		{
			meetingrfiDto.setRfiNotesId(vendorrfiNotesId);
			meetingrfiDto.setVendorId(currentVendorId);
			meetingrfiDto.setNotes(vendorRfiNotes);
			meetingrfiDto.setCreatedBy(String.valueOf(currentUserId));
		}
		String result = vendorDao.save_Or_Update_RfiMeetingNotes(userDetails, meetingrfiDto);
		if ("success".equalsIgnoreCase(result)) 
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.rfi.notes.save.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		} 
		else if(result.equals("update"))
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.rfi.notes.update.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		}
		else if (result.equals("failure")) 
		{
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
		}
		vendorForm.setVendorrfiNotes(null);
		vendorForm.setVendorrfiNotesId(null);
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID = " + currentVendorId + " ORDER BY v.CREATEDON DESC";
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setRfiNotesList(rfiMeetingList);
		
		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setMeetingNotesList(meetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
		
		return mapping.findForward("meetinginfo");
	}
	/**
	 * Method for delete the Meeting Notes.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward deleterfiMeetingNotes(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer currentVendorId = (Integer) session.getAttribute("supplierId");
		VendorDao vendorDao = new VendorDaoImpl();
		VendorMasterForm vendorForm = (VendorMasterForm) form;
		vendorForm.setId(currentVendorId);
		Integer vendorRfiNotesId = vendorForm.getVendorrfiNotesId();
		String result = "failure";
		if(vendorRfiNotesId != null && vendorRfiNotesId != 0)
		{
			result = vendorDao.deleteRfiMeetingNote(userDetails, vendorRfiNotesId);
		}
		if ("deleted".equalsIgnoreCase(result)) 
		{
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("vendor.meeting.notes.delete.ok");
			messages.add("vendor", msg);
			saveMessages(request, messages);
		} 
		else if (result.equals("failure")) 
		{
			// if Transaction fails, return to input page with message
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("transaction.failure");
			messages.add("transactionFailure", msg);
			saveMessages(request, messages);
		}
		vendorForm.setVendorNotes(null);
		vendorForm.setVendorNotesId(null);
		String query = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_rfi_rpf_notes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<RFIRPFNotesDto> rfiMeetingList = vendorDao.setRfiMeetingInfo_To_Dto(userDetails, query);
		vendorForm.setRfiNotesList(rfiMeetingList);
		
		String query1 = "SELECT v.ID, v.VENDORID, v.NOTES, u.FIRSTNAME, v.CREATEDON FROM customer_vendornotes v INNER JOIN users u ON u.ID = v.CREATEDBY INNER JOIN customer_vendormaster m ON m.ID = v.VENDORID where m.ID="+currentVendorId;
		List<MeetingNotesDto> meetingList = vendorDao.setMeetingInfo_To_Dto(userDetails, query1);
		vendorForm.setMeetingNotesList(meetingList);
		
		//Starting Code To Retrieve Email History.
		RetriveVendorInfoDto vendorInfo = null;
		vendorInfo = (RetriveVendorInfoDto) vendorDao.retriveVendorInfo(currentVendorId, userDetails);
		
		StringBuilder vendorEmailIdList = new StringBuilder();		
		List<EmailHistoryDto> emailHistoryList = null;
		
		for(int i = 0; i < vendorInfo.getVendorContacts().size(); i++)
		{
			if(vendorInfo.getVendorContacts().get(i).getEmailId() != null)
			{
				if(i == 0)
					vendorEmailIdList.append("'" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
				else
					vendorEmailIdList.append(", '" + vendorInfo.getVendorContacts().get(i).getEmailId() + "'");
			}
		}		
		
		if(vendorEmailIdList != null && vendorEmailIdList.length() != 0)
			emailHistoryList = vendorDao.setEmailHistoryInfo_To_Dto(userDetails, vendorEmailIdList.toString());
		
		vendorForm.setEmailHistoryList(emailHistoryList);
		//Ending Code To Retrieve Email History.
				
		return mapping.findForward("meetinginfo");
	}
	
}