package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.List;

public class ReportSearchFieldsDto  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String category;
	private List<String> columnTitle;
	
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public List<String> getColumnTitle() {
		return columnTitle;
	}
	public void setColumnTitle(List<String> columnTitle) {
		this.columnTitle = columnTitle;
	}

}
