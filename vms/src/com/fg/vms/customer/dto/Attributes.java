/*
 * Attributes.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Attributes.
 * 
 * @author pirabu
 */
public class Attributes implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;
	/** The attr. */
    private List<String> attr = new ArrayList<String>();

    /**
     * Gets the attribute.
     * 
     * @return attributes
     */
    public List<String> getAttribute() {
	return this.attr;
    }

    /**
     * Sets the attribute.
     * 
     * @param iIndex
     *            the i index
     * @param attribute
     *            the attribute
     */
    public void setAttribute(int iIndex, String attribute) {
	if (attribute != null && !attribute.equals("")) {
	    if (this.attr.size() <= iIndex) {
		// Fill the list to the specified size
		for (int i = this.attr.size(); i < iIndex; i++) {
		    this.attr.add(null);
		}
		this.attr.add(attribute);
	    } else {
		this.attr.set(iIndex, attribute);
	    }
	}
    }

}
