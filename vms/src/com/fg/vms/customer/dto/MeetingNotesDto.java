package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Kaleswararao
 *
 */
public class MeetingNotesDto implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer meetingNotesId;
	private Integer vendorId;
	private String notes;
	private String createdBy;
	private String createdOn;
	/**
	 * @return the meetingNotesId
	 */
	public Integer getMeetingNotesId() {
		return meetingNotesId;
	}
	/**
	 * @param meetingNotesId the meetingNotesId to set
	 */
	public void setMeetingNotesId(Integer meetingNotesId) {
		this.meetingNotesId = meetingNotesId;
	}
	/**
	 * @return the vendorId
	 */
	public Integer getVendorId() {
		return vendorId;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	
}
