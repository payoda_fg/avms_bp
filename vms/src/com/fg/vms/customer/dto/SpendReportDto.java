/*
 * SpendReportDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * The Class SpendReportDto.
 * 
 * @author pirabu
 */
public class SpendReportDto implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The supplier. */
    private String supplier;

    /** The tire2 supplier. */
    private String tire2Supplier;

    /** The diverse type. */
    private String diverseType;

    /** The amount. */
    private Double amount;

    /** The previous year amount. */
    private Double previousYearAmount;

    /**
     * Gets the supplier.
     * 
     * @return the supplier
     */
    public String getSupplier() {
	return supplier;
    }

    /**
     * Sets the supplier.
     * 
     * @param supplier
     *            the supplier to set
     */
    public void setSupplier(String supplier) {
	this.supplier = supplier;
    }

    /**
     * Gets the tire2 supplier.
     * 
     * @return the tire2Supplier
     */
    public String getTire2Supplier() {
	return tire2Supplier;
    }

    /**
     * Sets the tire2 supplier.
     * 
     * @param tire2Supplier
     *            the tire2Supplier to set
     */
    public void setTire2Supplier(String tire2Supplier) {
	this.tire2Supplier = tire2Supplier;
    }

    /**
     * Gets the diverse type.
     * 
     * @return the diverseType
     */
    public String getDiverseType() {
	return diverseType;
    }

    /**
     * Sets the diverse type.
     * 
     * @param diverseType
     *            the diverseType to set
     */
    public void setDiverseType(String diverseType) {
	this.diverseType = diverseType;
    }

    /**
     * Gets the amount.
     * 
     * @return the amount
     */
    public Double getAmount() {
	return amount;
    }

    /**
     * Sets the amount.
     * 
     * @param amount
     *            the amount to set
     */
    public void setAmount(Double amount) {
	this.amount = amount;
    }

    /**
     * Gets the previous year amount.
     * 
     * @return the previousYearAmount
     */
    public Double getPreviousYearAmount() {
	return previousYearAmount;
    }

    /**
     * Sets the previous year amount.
     * 
     * @param previousYearAmount
     *            the previousYearAmount to set
     */
    public void setPreviousYearAmount(Double previousYearAmount) {
	this.previousYearAmount = previousYearAmount;
    }

}
