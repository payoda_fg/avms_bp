/*
 * CustomerUsersDivisionDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * The Class CustomerUsersDivisionDto.
 * 
 * @author shefeek.a
 */
public class CustomerUsersDivisionDto implements Serializable {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer userId;

	private String userName;

	private String userEmailId;

	private String customerDivisionIds;
	
	private String customerDivisionNames;

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userEmailId
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * @param userEmailId
	 *            the userEmailId to set
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * @return the customerDivisionIds
	 */
	public String getCustomerDivisionIds() {
		return customerDivisionIds;
	}

	/**
	 * @param customerDivisionIds
	 *            the customerDivisionIds to set
	 */
	public void setCustomerDivisionIds(String customerDivisionIds) {
		this.customerDivisionIds = customerDivisionIds;
	}

	/**
	 * @return the customerDivisionNames
	 */
	public String getCustomerDivisionNames() {
		return customerDivisionNames;
	}

	/**
	 * @param customerDivisionNames the customerDivisionNames to set
	 */
	public void setCustomerDivisionNames(String customerDivisionNames) {
		this.customerDivisionNames = customerDivisionNames;
	}
}
