/**
 * ClassificationDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * @author vinoth
 * 
 */
public class ClassificationDto implements Serializable{

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer classificationId;

	private Integer agencyId;

	private Integer certificateTypeId;

	private String agencyData;

	private String certificateTypeData;

	/**
	 * @return the classificationId
	 */
	public Integer getClassificationId() {
		return classificationId;
	}

	/**
	 * @param classificationId
	 *            the classificationId to set
	 */
	public void setClassificationId(Integer classificationId) {
		this.classificationId = classificationId;
	}

	/**
	 * @return the agencyId
	 */
	public Integer getAgencyId() {
		return agencyId;
	}

	/**
	 * @param agencyId
	 *            the agencyId to set
	 */
	public void setAgencyId(Integer agencyId) {
		this.agencyId = agencyId;
	}

	/**
	 * @return the certificateTypeId
	 */
	public Integer getCertificateTypeId() {
		return certificateTypeId;
	}

	/**
	 * @param certificateTypeId
	 *            the certificateTypeId to set
	 */
	public void setCertificateTypeId(Integer certificateTypeId) {
		this.certificateTypeId = certificateTypeId;
	}

	/**
	 * @return the agencyData
	 */
	public String getAgencyData() {
		return agencyData;
	}

	/**
	 * @param agencyData
	 *            the agencyData to set
	 */
	public void setAgencyData(String agencyData) {
		this.agencyData = agencyData;
	}

	/**
	 * @return the certificateTypeData
	 */
	public String getCertificateTypeData() {
		return certificateTypeData;
	}

	/**
	 * @param certificateTypeData
	 *            the certificateTypeData to set
	 */
	public void setCertificateTypeData(String certificateTypeData) {
		this.certificateTypeData = certificateTypeData;
	}

}
