/*
 * MailNotification.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Holds the status, country, active and date details.
 * 
 * 
 */
public class MailNotification implements Serializable {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The date. */
	private Date date;

	/** The status. */
	private String status;

	/** The id. */
	private int id;

	/** The status list. */
	private ArrayList statusList;

	/** The country. */
	private String country;

	/** The active. */
	private byte active;

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public byte getActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the active to set
	 */
	public void setActive(byte active) {
		this.active = active;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the status list.
	 * 
	 * @return the statusList
	 */
	public ArrayList getStatusList() {
		return statusList;
	}

	/**
	 * Sets the status list.
	 * 
	 * @param statusList
	 *            the statusList to set
	 */
	public void setStatusList(ArrayList statusList) {
		this.statusList = statusList;
	}

}
