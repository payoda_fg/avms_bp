/**
 * 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gpirabu
 * 
 */
public class VendorHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String info;

	private Date createdOn;

	private Date modifiedOn;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public VendorHistory(String info, Date createdOn, Date modifiedOn) {
		super();
		this.info = info;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	public VendorHistory() {

	}

}
