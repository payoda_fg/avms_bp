/**
 * 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * @author gpirabu
 * 
 */
public class EmailDistribution implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String vendorName;

	private String firstName;

	private String emailId;

	private Integer emailDistributionListDetailId;

	private Byte checkboxIsTo;

	private Byte checkboxIsCC;

	private Byte checkboxIsBCC;

	private Byte vendorCheckboxIsTo;

	private Byte vendorCheckboxIsCC;

	private Byte vendorCheckboxIsBCC;
	
	private Byte primrNonPrime;
	
	private String vendorStatus;

	public EmailDistribution(Integer id, String emailId) {
		super();
		this.id = id;
		this.emailId = emailId;
	}

	public EmailDistribution(Integer id, String vendorName, String firstName,
			String emailId, Byte primrNonPrime,String vendorStatus) {
		super();
		this.id = id;
		this.vendorName = vendorName;
		this.firstName = firstName;
		this.emailId = emailId;
		this.primrNonPrime = primrNonPrime;
		this.vendorStatus=vendorStatus;
	}

	public EmailDistribution(Integer id, String vendorName, String firstName,
			String emailId, Byte primrNonPrime, Integer emailDistributionListDetailId,
			Byte vendorCheckboxIsTo, Byte vendorCheckboxIsCC,
			Byte vendorCheckboxIsBCC,String vendorStatus) {
		super();
		this.id = id;
		this.vendorName = vendorName;
		this.firstName = firstName;
		this.emailId = emailId;
		this.primrNonPrime = primrNonPrime;
		this.emailDistributionListDetailId = emailDistributionListDetailId;
		this.vendorCheckboxIsTo = vendorCheckboxIsTo;
		this.vendorCheckboxIsCC = vendorCheckboxIsCC;
		this.vendorCheckboxIsBCC = vendorCheckboxIsBCC;
		this.vendorStatus=vendorStatus;

	}

	public EmailDistribution(Integer id, String firstName, String emailId,
			Integer emailDistributionListDetailId, Byte checkboxIsTo,
			Byte checkboxIsCC, Byte checkboxIsBCC) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.emailId = emailId;
		this.emailDistributionListDetailId = emailDistributionListDetailId;
		this.checkboxIsTo = checkboxIsTo;
		this.checkboxIsCC = checkboxIsCC;
		this.checkboxIsBCC = checkboxIsBCC;

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the emailDistributionListDetailId
	 */
	public Integer getEmailDistributionListDetailId() {
		return emailDistributionListDetailId;
	}

	/**
	 * @param emailDistributionListDetailId
	 *            the emailDistributionListDetailId to set
	 */
	public void setEmailDistributionListDetailId(
			Integer emailDistributionListDetailId) {
		this.emailDistributionListDetailId = emailDistributionListDetailId;
	}

	public Byte getCheckboxIsTo() {
		return checkboxIsTo;
	}

	public void setCheckboxIsTo(Byte checkboxIsTo) {
		this.checkboxIsTo = checkboxIsTo;
	}

	public Byte getCheckboxIsCC() {
		return checkboxIsCC;
	}

	public void setCheckboxIsCC(Byte checkboxIsCC) {
		this.checkboxIsCC = checkboxIsCC;
	}

	public Byte getCheckboxIsBCC() {
		return checkboxIsBCC;
	}

	public void setCheckboxIsBCC(Byte checkboxIsBCC) {
		this.checkboxIsBCC = checkboxIsBCC;
	}

	public Byte getVendorCheckboxIsTo() {
		return vendorCheckboxIsTo;
	}

	public void setVendorCheckboxIsTo(Byte vendorCheckboxIsTo) {
		this.vendorCheckboxIsTo = vendorCheckboxIsTo;
	}

	public Byte getVendorCheckboxIsCC() {
		return vendorCheckboxIsCC;
	}

	public void setVendorCheckboxIsCC(Byte vendorCheckboxIsCC) {
		this.vendorCheckboxIsCC = vendorCheckboxIsCC;
	}

	public Byte getVendorCheckboxIsBCC() {
		return vendorCheckboxIsBCC;
	}

	public void setVendorCheckboxIsBCC(Byte vendorCheckboxIsBCC) {
		this.vendorCheckboxIsBCC = vendorCheckboxIsBCC;
	}

	public Byte getPrimrNonPrime() {
		return primrNonPrime;
	}

	public void setPrimrNonPrime(Byte primrNonPrime) {
		this.primrNonPrime = primrNonPrime;
	}

	public String getVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

}
