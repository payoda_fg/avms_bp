package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class SearchReportDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private List<String> columnHeader= new ArrayList<String>();
	
	private LinkedHashMap<String,List<Object>> columnDetails = new LinkedHashMap<String,List<Object>>();

	public List<String> getColumnHeader() {
		return columnHeader;
	}

	public void setColumnHeader(List<String> columnHeader) {
		this.columnHeader = columnHeader;
	}

	public LinkedHashMap<String, List<Object>> getColumnDetails() {
		return columnDetails;
	}

	public void setColumnDetails(LinkedHashMap<String, List<Object>> columnDetails) {
		this.columnDetails = columnDetails;
	}
	
}
