/*
 * AssessmentQuestionsDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the assessment question details such as template questions, template
 * details and data type.
 * 
 * @author Vinoth
 */
public class AssessmentQuestionsDto implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** The template id. */
    private Integer templateId;

    /** The template question id. */
    private Integer templateQuestionId;

    /** The questions. */
    private String questions;

    /** The data type. */
    private String dataType;

    /**
     * Gets the template id.
     * 
     * @return the templateId
     */
    public Integer getTemplateId() {
	return templateId;
    }

    /**
     * Sets the template id.
     * 
     * @param templateId
     *            the templateId to set
     */
    public void setTemplateId(Integer templateId) {
	this.templateId = templateId;
    }

    /**
     * Gets the template question id.
     * 
     * @return the templateQuestionId
     */
    public Integer getTemplateQuestionId() {
	return templateQuestionId;
    }

    /**
     * Sets the template question id.
     * 
     * @param templateQuestionId
     *            the templateQuestionId to set
     */
    public void setTemplateQuestionId(Integer templateQuestionId) {
	this.templateQuestionId = templateQuestionId;
    }

    /**
     * Gets the questions.
     * 
     * @return the questions
     */
    public String getQuestions() {
	return questions;
    }

    /**
     * Sets the questions.
     * 
     * @param questions
     *            the questions to set
     */
    public void setQuestions(String questions) {
	this.questions = questions;
    }

    /**
     * Gets the data type.
     * 
     * @return the dataType
     */
    public String getDataType() {
	return dataType;
    }

    /**
     * Sets the data type.
     * 
     * @param dataType
     *            the dataType to set
     */
    public void setDataType(String dataType) {
	this.dataType = dataType;
    }

}
