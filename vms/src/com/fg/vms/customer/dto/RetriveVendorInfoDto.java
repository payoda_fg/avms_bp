/*
 * RetrieveVendorInfoDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.List;

import com.fg.vms.customer.model.CustomerWflog;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.customer.model.VendorOtherCertificate;

/**
 * The Class RetriveVendorInfoDto.
 * 
 * @author srinivasarao
 */
public class RetriveVendorInfoDto implements Serializable{

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The vendor master. */
	private VendorMaster vendorMaster;

	/** The vendor contacts. */
	private List<VendorContact> vendorContacts;

	/** The vendor certificate. */
	private List<VendorCertificate> vendorCertificate;

	/** The vendor naics. */
	private List<VendorNAICS> vendorNaics;

	/** The vendor other cert. */
	private List<VendorOtherCertificate> vendorOtherCert;

	/** The naics master. */
	private List<NaicsCode> naicsMaster;

	private CustomerWflog customerWflog;

	/** The vendor document list */
	private List<VendorDocuments> vendorDocuments;
	
	/** The ISNETworld Certificate */
	
	private String isNetWorldCertificate;
	
	private Boolean hasNetWoldcert;
	/**
	 * @return the vendorDocuments
	 */
	public List<VendorDocuments> getVendorDocuments() {
		return vendorDocuments;
	}

	/**
	 * @param vendorDocuments
	 *            the vendorDocuments to set
	 */
	public void setVendorDocuments(List<VendorDocuments> vendorDocuments) {
		this.vendorDocuments = vendorDocuments;
	}

	/**
	 * Gets the vendor master.
	 * 
	 * @return the vendor master
	 */
	public VendorMaster getVendorMaster() {
		return vendorMaster;
	}

	/**
	 * Sets the vendor master.
	 * 
	 * @param vendorMaster
	 *            the new vendor master
	 */
	public void setVendorMaster(VendorMaster vendorMaster) {
		this.vendorMaster = vendorMaster;
	}

	/**
	 * Gets the vendor contacts.
	 * 
	 * @return the vendor contacts
	 */
	public List<VendorContact> getVendorContacts() {
		return vendorContacts;
	}

	/**
	 * Sets the vendor contacts.
	 * 
	 * @param vendorContacts
	 *            the new vendor contacts
	 */
	public void setVendorContacts(List<VendorContact> vendorContacts) {
		this.vendorContacts = vendorContacts;
	}

	/**
	 * Gets the vendor certificate.
	 * 
	 * @return the vendor certificate
	 */
	public List<VendorCertificate> getVendorCertificate() {
		return vendorCertificate;
	}

	/**
	 * Sets the vendor certificate.
	 * 
	 * @param vendorCertificate
	 *            the new vendor certificate
	 */
	public void setVendorCertificate(List<VendorCertificate> vendorCertificate) {
		this.vendorCertificate = vendorCertificate;
	}

	/**
	 * Gets the vendor naics.
	 * 
	 * @return the vendor naics
	 */
	public List<VendorNAICS> getVendorNaics() {
		return vendorNaics;
	}

	/**
	 * Sets the vendor naics.
	 * 
	 * @param vendorNaics
	 *            the new vendor naics
	 */
	public void setVendorNaics(List<VendorNAICS> vendorNaics) {
		this.vendorNaics = vendorNaics;
	}

	/**
	 * @return the vendorOtherCert
	 */
	public List<VendorOtherCertificate> getVendorOtherCert() {
		return vendorOtherCert;
	}

	/**
	 * @param vendorOtherCert
	 *            the vendorOtherCert to set
	 */
	public void setVendorOtherCert(List<VendorOtherCertificate> vendorOtherCert) {
		this.vendorOtherCert = vendorOtherCert;
	}

	/**
	 * Gets the naics master.
	 * 
	 * @return the naics master
	 */
	public List<NaicsCode> getNaicsMaster() {
		return naicsMaster;
	}

	/**
	 * Sets the naics master.
	 * 
	 * @param naicsMaster
	 *            the new naics master
	 */
	public void setNaicsMaster(List<NaicsCode> naicsMaster) {
		this.naicsMaster = naicsMaster;
	}

	/**
	 * @return the customerWflog
	 */
	public CustomerWflog getCustomerWflog() {
		return customerWflog;
	}

	/**
	 * @param customerWflog
	 *            the customerWflog to set
	 */
	public void setCustomerWflog(CustomerWflog customerWflog) {
		this.customerWflog = customerWflog;
	}

	public String getIsNetWorldCertificate() {
		return isNetWorldCertificate;
	}

	public void setIsNetWorldCertificate(String isNetWorldCertificate) {
		this.isNetWorldCertificate = isNetWorldCertificate;
	}

	public Boolean getHasNetWoldcert() {
		return hasNetWoldcert;
	}

	public void setHasNetWoldcert(Boolean hasNetWoldcert) {
		this.hasNetWoldcert = hasNetWoldcert;
	}

}
