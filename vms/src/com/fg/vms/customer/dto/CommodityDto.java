/**
 * 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * @author vinoth
 * 
 */
public class CommodityDto implements Serializable{

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer commCategoryId;

	private String categoryCode;

	private String categoryDesc;
	
	private Integer commodityId;
	
	private Integer sectorId;
	
	private String commodityDescription;
	
	private String sectorDescription;
	
	private String commodityCode;
	
	private String sectorCode;
	
	private Integer vendorCommodityId;

	/**
	 * @return the commCategoryId
	 */
	public Integer getCommCategoryId() {
		return commCategoryId;
	}

	/**
	 * @param commCategoryId
	 *            the commCategoryId to set
	 */
	public void setCommCategoryId(Integer commCategoryId) {
		this.commCategoryId = commCategoryId;
	}

	/**
	 * @return the categoryCode
	 */
	public String getCategoryCode() {
		return categoryCode;
	}

	/**
	 * @param categoryCode
	 *            the categoryCode to set
	 */
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * @return the categoryDesc
	 */
	public String getCategoryDesc() {
		return categoryDesc;
	}

	/**
	 * @param categoryDesc
	 *            the categoryDesc to set
	 */
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	/**
	 * @return the commodityId
	 */
	public Integer getCommodityId() {
		return commodityId;
	}

	/**
	 * @param commodityId the commodityId to set
	 */
	public void setCommodityId(Integer commodityId) {
		this.commodityId = commodityId;
	}

	/**
	 * @return the sectorId
	 */
	public Integer getSectorId() {
		return sectorId;
	}

	/**
	 * @param sectorId the sectorId to set
	 */
	public void setSectorId(Integer sectorId) {
		this.sectorId = sectorId;
	}

	/**
	 * @return the commodityDescription
	 */
	public String getCommodityDescription() {
		return commodityDescription;
	}

	/**
	 * @param commodityDescription the commodityDescription to set
	 */
	public void setCommodityDescription(String commodityDescription) {
		this.commodityDescription = commodityDescription;
	}

	/**
	 * @return the sectorDescription
	 */
	public String getSectorDescription() {
		return sectorDescription;
	}

	/**
	 * @param sectorDescription the sectorDescription to set
	 */
	public void setSectorDescription(String sectorDescription) {
		this.sectorDescription = sectorDescription;
	}

	/**
	 * @return the commodityCode
	 */
	public String getCommodityCode() {
		return commodityCode;
	}

	/**
	 * @param commodityCode the commodityCode to set
	 */
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}

	/**
	 * @return the sectorCode
	 */
	public String getSectorCode() {
		return sectorCode;
	}

	/**
	 * @param sectorCode the sectorCode to set
	 */
	public void setSectorCode(String sectorCode) {
		this.sectorCode = sectorCode;
	}

	/**
	 * @return the vendorCommodityId
	 */
	public Integer getVendorCommodityId() {
		return vendorCommodityId;
	}

	/**
	 * @param vendorCommodityId the vendorCommodityId to set
	 */
	public void setVendorCommodityId(Integer vendorCommodityId) {
		this.vendorCommodityId = vendorCommodityId;
	}

}
