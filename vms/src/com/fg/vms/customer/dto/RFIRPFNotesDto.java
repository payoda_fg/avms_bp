package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Kaleswararao
 *
 */
public class RFIRPFNotesDto implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer rfiNotesId;
	private Integer vendorId;
	private String notes;
	private String createdBy;
	private String createdOn;
	
	public Integer getRfiNotesId() {
		return rfiNotesId;
	}
	public void setRfiNotesId(Integer rfiNotesId) {
		this.rfiNotesId = rfiNotesId;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	
}
