/*
 * NaicsCategoryDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the information of naics category description and active status.
 * 
 * @author vinoth
 */
public class NaicsCategoryDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Integer id;

    /** The naics category desc. */
    private String naicsCategoryDesc;

    /** The active. */
    private boolean active;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the naics category desc.
     * 
     * @return the naicsCategoryDesc
     */
    public String getNaicsCategoryDesc() {
	return naicsCategoryDesc;
    }

    /**
     * Sets the naics category desc.
     * 
     * @param naicsCategoryDesc
     *            the naicsCategoryDesc to set
     */
    public void setNaicsCategoryDesc(String naicsCategoryDesc) {
	this.naicsCategoryDesc = naicsCategoryDesc;
    }

    /**
     * Checks if is active.
     * 
     * @return the active
     */
    public boolean isActive() {
	return active;
    }

    /**
     * Sets the active.
     * 
     * @param active
     *            the active to set
     */
    public void setActive(boolean active) {
	this.active = active;
    }

}
