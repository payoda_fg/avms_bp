/**
 * 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author gpirabu
 * 
 */
public class CertificateExpiration implements Serializable {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;
	private String emailId;
	private Integer vendorId;
	private String contactName;
	private String lastName;
	private String supplierCompanyName;
	private String certificationNo;
	private String agencyName;
	private String categoryName;
	private Date expirationDate;
	private String linkToSite;
	private String customerWebSiteURL;
	private String customerName;
	private String customerAdminEmailId;
	private String certificateName;
	private Integer vendorCertificateId;
	private List<String> cc;
	private String applicationUrl;
	private String supportEmailId;

	public CertificateExpiration(Integer vendorId, String emailId,
			String contactName, String lastName, String supplierCompanyName,
			String certificationNo, String agencyName, String categoryName,
			Date expirationDate, String certificateName,
			Integer vendorCertificateId,String applicationUrl) {
		super();
		this.vendorId = vendorId;
		this.emailId = emailId;
		this.lastName = lastName;
		this.contactName = contactName + " " + lastName;
		this.supplierCompanyName = supplierCompanyName;
		this.certificationNo = certificationNo;
		this.agencyName = agencyName;
		this.categoryName = categoryName;
		this.expirationDate = expirationDate;
		this.certificateName = certificateName;
		this.vendorCertificateId = vendorCertificateId;
		this.applicationUrl = applicationUrl;

	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public Integer getVendorCertificateId() {
		return vendorCertificateId;
	}

	public void setVendorCertificateId(Integer vendorCertificateId) {
		this.vendorCertificateId = vendorCertificateId;
	}

	/**
	 * @return the vendorId
	 */
	public Integer getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName
	 *            the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the supplierCompanyName
	 */
	public String getSupplierCompanyName() {
		return supplierCompanyName;
	}

	/**
	 * @param supplierCompanyName
	 *            the supplierCompanyName to set
	 */
	public void setSupplierCompanyName(String supplierCompanyName) {
		this.supplierCompanyName = supplierCompanyName;
	}

	/**
	 * @return the certificationNo
	 */
	public String getCertificationNo() {
		return certificationNo;
	}

	/**
	 * @param certificationNo
	 *            the certificationNo to set
	 */
	public void setCertificationNo(String certificationNo) {
		this.certificationNo = certificationNo;
	}

	/**
	 * @return the agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * @param agencyName
	 *            the agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate
	 *            the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the linkToSite
	 */
	public String getLinkToSite() {
		return linkToSite;
	}

	/**
	 * @param linkToSite
	 *            the linkToSite to set
	 */
	public void setLinkToSite(String linkToSite) {
		this.linkToSite = linkToSite;
	}

	/**
	 * @return the customerWebSiteURL
	 */
	public String getCustomerWebSiteURL() {
		return customerWebSiteURL;
	}

	/**
	 * @param customerWebSiteURL
	 *            the customerWebSiteURL to set
	 */
	public void setCustomerWebSiteURL(String customerWebSiteURL) {
		this.customerWebSiteURL = customerWebSiteURL;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the customerAdminEmailId
	 */
	public String getCustomerAdminEmailId() {
		return customerAdminEmailId;
	}

	/**
	 * @param customerAdminEmailId
	 *            the customerAdminEmailId to set
	 */
	public void setCustomerAdminEmailId(String customerAdminEmailId) {
		this.customerAdminEmailId = customerAdminEmailId;
	}

	/**
	 * @return the cc
	 */
	public List<String> getCc() {
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public String getSupportEmailId() {
		return supportEmailId;
	}

	public void setSupportEmailId(String supportEmailId) {
		this.supportEmailId = supportEmailId;
	}
	
}