/*
 * Tier2ReportDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author vinoth
 * 
 */
public class Tier2ReportDto implements Serializable {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private Integer vendorId;

	private String vendorName;

	private String vendorUserName;

	private String reportPeriod;

	private String createdOn;
	
	private Date createdon;

	private String certificateName;
	
	private String sectorDescription;

	private Double indirectExp;

	private Double directExp;

	private String lastActivity;

	private Integer spendYear;

	private Double totalSales;

	private Double totalSalesToCompany;

	private String diversityPercent;

	private Integer reportNumber;

	private String ethnicity;
	
	private String subSector;

	private Integer tier2reportId;

	private Double grandTotal;

	private Integer ethnicityId;

	private Double count;

	private Double ethnicityPercent;

	private String certificateShortName;

	private String firstName;

	private String lastName;

	private String address1;

	private String city;

	private String state;

	private String zip;

	private String phone;

	private String fax;

	private String emailId;

	private String certAgencyName;

	private String CertShortName;

	private String certName;

	private String effDate;

	private String expDate;

	private String capablities;

	private String naicsDesc;

	private String statusName;

	private Double statusCount;

	private Double statusPercent;
	
	private String ownerName;
	
	private String diverseClassification;
	
	private String  naicsCapabilities;
	
	private String certificateAgencyDetails;
	
	private Byte isSubmitted;
	
	private String year;
	
	private String emailDate;
	
	private String certificateNumber;
	
	private String month;
	
	private Double percentage;
	
	private String businessTypeName;
	
	private String marketSector;
	
	private String certificateExpiryStatus;
	
	private String marketSubSector;
	
	private String website;
	
	private Integer marketSectorId;
	
	private Integer certificateTypeId;
	
	private String certificateTypeName;
	
	private Long recentAnnaulRevenue;
	
	private String commodityDisecription;
	
	private String fileName;
	
	public Tier2ReportDto() {
		super();
	}

	public Tier2ReportDto(Integer vendorId, String vendorName, String address1) {
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.address1 = address1;
	}

	public Tier2ReportDto(Integer vendorId, String vendorName, String address1,
			String city, String state, String zipcode) {
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.address1 = address1;
		this.city = city;
		this.state = state;
		this.zip = zipcode;
	}
	
	public Tier2ReportDto(Integer vendorId, String vendorName, String address1,
			String city, String state, String zipcode,Date createdon) {
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.address1 = address1;
		this.city = city;
		this.state = state;
		this.zip = zipcode;
		this.createdon = createdon;
	}

	public Tier2ReportDto(Integer reportId, Byte isSubmitted, String vendorName,
			String year, String reportPeriod, Double directExp, Double indirectExp,
				Double totalSales, Double totalSalesToCompany, String createdOn){
		super();
		this.tier2reportId = reportId;
		this.isSubmitted = isSubmitted;
		this.vendorName = vendorName;
		this.year = year;
		this.reportPeriod = reportPeriod;
		this.directExp = directExp;
		this.indirectExp = indirectExp;
		this.totalSales = totalSales;
		this.totalSalesToCompany = totalSalesToCompany;
		this.createdOn = createdOn;
	}
	
	public Tier2ReportDto(Integer reportId, Byte isSubmitted, String vendorName,
			String year, String reportPeriod, Double directExp, Double indirectExp,
				Double totalSales, Double totalSalesToCompany, String createdOn, String fileName){
		super();
		this.tier2reportId = reportId;
		this.isSubmitted = isSubmitted;
		this.vendorName = vendorName;
		this.year = year;
		this.reportPeriod = reportPeriod;
		this.directExp = directExp;
		this.indirectExp = indirectExp;
		this.totalSales = totalSales;
		this.totalSalesToCompany = totalSalesToCompany;
		this.createdOn = createdOn;
		this.fileName = fileName;
	}
	/**
	 * @return the vendorId
	 */
	public Integer getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the vendorUserName
	 */
	public String getVendorUserName() {
		return vendorUserName;
	}

	/**
	 * @param vendorUserName
	 *            the vendorUserName to set
	 */
	public void setVendorUserName(String vendorUserName) {
		this.vendorUserName = vendorUserName;
	}

	/**
	 * @return the reportPeriod
	 */
	public String getReportPeriod() {
		return reportPeriod;
	}

	/**
	 * @param reportPeriod
	 *            the reportPeriod to set
	 */
	public void setReportPeriod(String reportPeriod) {
		this.reportPeriod = reportPeriod;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the certificateName
	 */
	public String getCertificateName() {
		return certificateName;
	}

	/**
	 * @param certificateName
	 *            the certificateName to set
	 */
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	/**
	 * @return the indirectExp
	 */
	public Double getIndirectExp() {
		return indirectExp;
	}

	/**
	 * @param indirectExp
	 *            the indirectExp to set
	 */
	public void setIndirectExp(Double indirectExp) {
		this.indirectExp = indirectExp;
	}

	/**
	 * @return the directExp
	 */
	public Double getDirectExp() {
		return directExp;
	}

	/**
	 * @param directExp
	 *            the directExp to set
	 */
	public void setDirectExp(Double directExp) {
		this.directExp = directExp;
	}

	/**
	 * @return the lastActivity
	 */
	public String getLastActivity() {
		return lastActivity;
	}

	/**
	 * @param lastActivity
	 *            the lastActivity to set
	 */
	public void setLastActivity(String lastActivity) {
		this.lastActivity = lastActivity;
	}

	/**
	 * @return the spendYear
	 */
	public Integer getSpendYear() {
		return spendYear;
	}

	/**
	 * @param spendYear
	 *            the spendYear to set
	 */
	public void setSpendYear(Integer spendYear) {
		this.spendYear = spendYear;
	}

	/**
	 * @return the totalSales
	 */
	public Double getTotalSales() {
		return totalSales;
	}

	/**
	 * @param totalSales
	 *            the totalSales to set
	 */
	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}

	/**
	 * @return the totalSalesToCompany
	 */
	public Double getTotalSalesToCompany() {
		return totalSalesToCompany;
	}

	/**
	 * @param totalSalesToCompany
	 *            the totalSalesToCompany to set
	 */
	public void setTotalSalesToCompany(Double totalSalesToCompany) {
		this.totalSalesToCompany = totalSalesToCompany;
	}

	/**
	 * @return the diversityPercent
	 */
	public String getDiversityPercent() {
		return diversityPercent;
	}

	/**
	 * @param diversityPercent
	 *            the diversityPercent to set
	 */
	public void setDiversityPercent(String diversityPercent) {
		this.diversityPercent = diversityPercent;
	}

	/**
	 * @return the reportNumber
	 */
	public Integer getReportNumber() {
		return reportNumber;
	}

	/**
	 * @param reportNumber
	 *            the reportNumber to set
	 */
	public void setReportNumber(Integer reportNumber) {
		this.reportNumber = reportNumber;
	}

	/**
	 * @return the ethnicity
	 */
	public String getEthnicity() {
		return ethnicity;
	}

	/**
	 * @param ethnicity
	 *            the ethnicity to set
	 */
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	/**
	 * @return the tier2reportId
	 */
	public Integer getTier2reportId() {
		return tier2reportId;
	}

	/**
	 * @param tier2reportId
	 *            the tier2reportId to set
	 */
	public void setTier2reportId(Integer tier2reportId) {
		this.tier2reportId = tier2reportId;
	}

	/**
	 * @return the grandTotal
	 */
	public Double getGrandTotal() {
		return grandTotal;
	}

	/**
	 * @param grandTotal
	 *            the grandTotal to set
	 */
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	/**
	 * @return the ethnicityId
	 */
	public Integer getEthnicityId() {
		return ethnicityId;
	}

	/**
	 * @param ethnicityId
	 *            the ethnicityId to set
	 */
	public void setEthnicityId(Integer ethnicityId) {
		this.ethnicityId = ethnicityId;
	}

	/**
	 * @return the count
	 */
	public Double getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(Double count) {
		this.count = count;
	}

	/**
	 * @return the ethnicityPercent
	 */
	public Double getEthnicityPercent() {
		return ethnicityPercent;
	}

	/**
	 * @param ethnicityPercent
	 *            the ethnicityPercent to set
	 */
	public void setEthnicityPercent(Double ethnicityPercent) {
		this.ethnicityPercent = ethnicityPercent;
	}

	/**
	 * @return the certificateShortName
	 */
	public String getCertificateShortName() {
		return certificateShortName;
	}

	/**
	 * @param certificateShortName
	 *            the certificateShortName to set
	 */
	public void setCertificateShortName(String certificateShortName) {
		this.certificateShortName = certificateShortName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the certAgencyName
	 */
	public String getCertAgencyName() {
		return certAgencyName;
	}

	/**
	 * @param certAgencyName
	 *            the certAgencyName to set
	 */
	public void setCertAgencyName(String certAgencyName) {
		this.certAgencyName = certAgencyName;
	}

	/**
	 * @return the certShortName
	 */
	public String getCertShortName() {
		return CertShortName;
	}

	/**
	 * @param certShortName
	 *            the certShortName to set
	 */
	public void setCertShortName(String certShortName) {
		CertShortName = certShortName;
	}

	/**
	 * @return the certName
	 */
	public String getCertName() {
		return certName;
	}

	/**
	 * @param certName
	 *            the certName to set
	 */
	public void setCertName(String certName) {
		this.certName = certName;
	}

	/**
	 * @return the effDate
	 */
	public String getEffDate() {
		return effDate;
	}

	/**
	 * @param effDate
	 *            the effDate to set
	 */
	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}

	/**
	 * @return the expDate
	 */
	public String getExpDate() {
		return expDate;
	}

	/**
	 * @param expDate
	 *            the expDate to set
	 */
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	/**
	 * @return the capablities
	 */
	public String getCapablities() {
		return capablities;
	}

	/**
	 * @param capablities
	 *            the capablities to set
	 */
	public void setCapablities(String capablities) {
		this.capablities = capablities;
	}

	/**
	 * @return the naicsDesc
	 */
	public String getNaicsDesc() {
		return naicsDesc;
	}

	/**
	 * @param naicsDesc
	 *            the naicsDesc to set
	 */
	public void setNaicsDesc(String naicsDesc) {
		this.naicsDesc = naicsDesc;
	}

	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
	 * @return the statusCount
	 */
	public Double getStatusCount() {
		return statusCount;
	}

	/**
	 * @param statusCount
	 *            the statusCount to set
	 */
	public void setStatusCount(Double statusCount) {
		this.statusCount = statusCount;
	}

	/**
	 * @return the statusPercent
	 */
	public Double getStatusPercent() {
		return statusPercent;
	}

	/**
	 * @param statusPercent
	 *            the statusPercent to set
	 */
	public void setStatusPercent(Double statusPercent) {
		this.statusPercent = statusPercent;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getDiverseClassification() {
		return diverseClassification;
	}

	public void setDiverseClassification(String diverseClassification) {
		this.diverseClassification = diverseClassification;
	}

	public String getNaicsCapabilities() {
		return naicsCapabilities;
	}

	public void setNaicsCapabilities(String naicsCapabilities) {
		this.naicsCapabilities = naicsCapabilities;
	}

	public String getCertificateAgencyDetails() {
		return certificateAgencyDetails;
	}

	public void setCertificateAgencyDetails(String certificateAgencyDetails) {
		this.certificateAgencyDetails = certificateAgencyDetails;
	}
	 
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}

	public Byte getIsSubmitted() {
		return isSubmitted;
	}

	public void setIsSubmitted(Byte isSubmitted) {
		this.isSubmitted = isSubmitted;
	}

	/**
	 * @return the emailDate
	 */
	public String getEmailDate() {
		return emailDate;
	}

	/**
	 * @param emailDate the emailDate to set
	 */
	public void setEmailDate(String emailDate) {
		this.emailDate = emailDate;
	}

	/**
	 * @return the certificateNumber
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * @param certificateNumber the certificateNumber to set
	 */
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the percentage
	 */
	public Double getPercentage() {
		return percentage;
	}

	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	/**
	 * @return the businessTypeName
	 */
	public String getBusinessTypeName() {
		return businessTypeName;
	}

	/**
	 * @param businessTypeName the businessTypeName to set
	 */
	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}

	/**
	 * @return the marketSector
	 */
	public String getMarketSector() {
		return marketSector;
	}

	/**
	 * @param marketSector the marketSector to set
	 */
	public void setMarketSector(String marketSector) {
		this.marketSector = marketSector;
	}

	/**
	 * @return the certificateExpiryStatus
	 */
	public String getCertificateExpiryStatus() {
		return certificateExpiryStatus;
	}

	/**
	 * @param certificateExpiryStatus the certificateExpiryStatus to set
	 */
	public void setCertificateExpiryStatus(String certificateExpiryStatus) {
		this.certificateExpiryStatus = certificateExpiryStatus;
	}

	/**
	 * @return the marketSubSector
	 */
	public String getMarketSubSector() {
		return marketSubSector;
	}

	/**
	 * @param marketSubSector the marketSubSector to set
	 */
	public void setMarketSubSector(String marketSubSector) {
		this.marketSubSector = marketSubSector;
	}

	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * @return the marketSectorId
	 */
	public Integer getMarketSectorId() {
		return marketSectorId;
	}

	/**
	 * @param marketSectorId the marketSectorId to set
	 */
	public void setMarketSectorId(Integer marketSectorId) {
		this.marketSectorId = marketSectorId;
	}

	/**
	 * @return the certificateTypeId
	 */
	public Integer getCertificateTypeId() {
		return certificateTypeId;
	}

	/**
	 * @param certificateTypeId the certificateTypeId to set
	 */
	public void setCertificateTypeId(Integer certificateTypeId) {
		this.certificateTypeId = certificateTypeId;
	}

	/**
	 * @return the certificateTypeName
	 */
	public String getCertificateTypeName() {
		return certificateTypeName;
	}

	/**
	 * @param certificateTypeName the certificateTypeName to set
	 */
	public void setCertificateTypeName(String certificateTypeName) {
		this.certificateTypeName = certificateTypeName;
	}

	/**
	 * @return the recentAnnaulRevenue
	 */
	public Long getRecentAnnaulRevenue() {
		return recentAnnaulRevenue;
	}

	/**
	 * @param recentAnnaulRevenue the recentAnnaulRevenue to set
	 */
	public void setRecentAnnaulRevenue(Long recentAnnaulRevenue) {
		this.recentAnnaulRevenue = recentAnnaulRevenue;
	}

	public String getSubSector() {
		return subSector;
	}

	public void setSubSector(String subSector) {
		this.subSector = subSector;
	}

	public String getCommodityDisecription() {
		return commodityDisecription;
	}

	public void setCommodityDisecription(String commodityDisecription) {
		this.commodityDisecription = commodityDisecription;
	}

	public String getSectorDescription() {
		return sectorDescription;
	}

	public void setSectorDescription(String sectorDescription) {
		this.sectorDescription = sectorDescription;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}