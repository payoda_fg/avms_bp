/**
 * GeographicalStateRegionDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.List;

import com.fg.vms.customer.model.Region;
import com.fg.vms.customer.model.State;

/**
 * @author vinoth
 * 
 */
public class GeographicalStateRegionDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer geographicServiceAreaId;

	private Integer regionId;

	private Integer stateId;

	private String region;

	private String state;

	private String[] states;

	private String[] regions;

	private List<State> stateList;

	private List<Region> regionList;

	private String businessGroup;

	private String serviceArea;
	
	private String commodityCategory;
	
	private String commodityDescription;
	
	private Integer certficateTypeId;
	
	private String certficateTypeDescription;
	
	private String sectorCategory;
	
	private String sectorDescription;
	
	private String subSectorCategory;
	
	private String subSectorDescription;

	/**
	 * @return the geographicServiceAreaId
	 */
	public Integer getGeographicServiceAreaId() {
		return geographicServiceAreaId;
	}

	/**
	 * @param geographicServiceAreaId
	 *            the geographicServiceAreaId to set
	 */
	public void setGeographicServiceAreaId(Integer geographicServiceAreaId) {
		this.geographicServiceAreaId = geographicServiceAreaId;
	}

	/**
	 * @return the regionId
	 */
	public Integer getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId
	 *            the regionId to set
	 */
	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId
	 *            the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the states
	 */
	public String[] getStates() {
		return states;
	}

	/**
	 * @param states
	 *            the states to set
	 */
	public void setStates(String[] states) {
		this.states = states;
	}

	/**
	 * @return the regions
	 */
	public String[] getRegions() {
		return regions;
	}

	/**
	 * @param regions
	 *            the regions to set
	 */
	public void setRegions(String[] regions) {
		this.regions = regions;
	}

	/**
	 * @return the stateList
	 */
	public List<State> getStateList() {
		return stateList;
	}

	/**
	 * @param stateList
	 *            the stateList to set
	 */
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}

	/**
	 * @return the regionList
	 */
	public List<Region> getRegionList() {
		return regionList;
	}

	/**
	 * @param regionList
	 *            the regionList to set
	 */
	public void setRegionList(List<Region> regionList) {
		this.regionList = regionList;
	}

	/**
	 * @return the businessGroup
	 */
	public String getBusinessGroup() {
		return businessGroup;
	}

	/**
	 * @param businessGroup
	 *            the businessGroup to set
	 */
	public void setBusinessGroup(String businessGroup) {
		this.businessGroup = businessGroup;
	}

	/**
	 * @return the serviceArea
	 */
	public String getServiceArea() {
		return serviceArea;
	}

	/**
	 * @param serviceArea
	 *            the serviceArea to set
	 */
	public void setServiceArea(String serviceArea) {
		this.serviceArea = serviceArea;
	}

	/**
	 * @return commodityCategory
	 */
	public String getCommodityCategory() {
		return commodityCategory;
	}

	/**
	 * @param commodityCategory
	 */
	public void setCommodityCategory(String commodityCategory) {
		this.commodityCategory = commodityCategory;
	}

	/**
	 * @return commodityDescription
	 */
	public String getCommodityDescription() {
		return commodityDescription;
	}

	/**
	 * @param commodityDescription
	 */
	public void setCommodityDescription(String commodityDescription) {
		this.commodityDescription = commodityDescription;
	}

	public Integer getCertficateTypeId() {
		return certficateTypeId;
	}

	public void setCertficateTypeId(Integer certficateTypeId) {
		this.certficateTypeId = certficateTypeId;
	}

	public String getCertficateTypeDescription() {
		return certficateTypeDescription;
	}

	public void setCertficateTypeDescription(String certficateTypeDescription) {
		this.certficateTypeDescription = certficateTypeDescription;
	}

	/**
	 * @return the sectorCategory
	 */
	public String getSectorCategory() {
		return sectorCategory;
	}

	/**
	 * @param sectorCategory the sectorCategory to set
	 */
	public void setSectorCategory(String sectorCategory) {
		this.sectorCategory = sectorCategory;
	}

	/**
	 * @return the sectorDescription
	 */
	public String getSectorDescription() {
		return sectorDescription;
	}

	/**
	 * @param sectorDescription the sectorDescription to set
	 */
	public void setSectorDescription(String sectorDescription) {
		this.sectorDescription = sectorDescription;
	}

	/**
	 * @return the subSectorCategory
	 */
	public String getSubSectorCategory() {
		return subSectorCategory;
	}

	/**
	 * @param subSectorCategory the subSectorCategory to set
	 */
	public void setSubSectorCategory(String subSectorCategory) {
		this.subSectorCategory = subSectorCategory;
	}

	/**
	 * @return the subSectorDescription
	 */
	public String getSubSectorDescription() {
		return subSectorDescription;
	}

	/**
	 * @param subSectorDescription the subSectorDescription to set
	 */
	public void setSubSectorDescription(String subSectorDescription) {
		this.subSectorDescription = subSectorDescription;
	}
}
