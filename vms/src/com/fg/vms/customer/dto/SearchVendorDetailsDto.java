/**
 * SearchVendorDetailsDto.java
 */
package com.fg.vms.customer.dto;

/**
 * @author Asarudeen
 *
 */
public class SearchVendorDetailsDto {
	private boolean vendorName;
	private boolean ownerName;
	private boolean address;
	private boolean city;
	private boolean state;
	private boolean zipcode;
	private boolean phone;
	private boolean fax;
	private boolean emailId;
	private boolean agencyName;
	private boolean certName;
	private boolean effDate;
	private boolean expDate;
	private boolean capablities;
	private boolean naicsDesc;
	private boolean naicsCapabilities;
	private boolean diverseClassification;
	private boolean certificateAgencyDetails;
	
	/**
	 * @return the vendorName
	 */
	public boolean isVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(boolean vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @return the ownerName
	 */
	public boolean isOwnerName() {
		return ownerName;
	}
	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(boolean ownerName) {
		this.ownerName = ownerName;
	}
	/**
	 * @return the address
	 */
	public boolean isAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(boolean address) {
		this.address = address;
	}
	/**
	 * @return the city
	 */
	public boolean isCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(boolean city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public boolean isState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(boolean state) {
		this.state = state;
	}
	/**
	 * @return the zipcode
	 */
	public boolean isZipcode() {
		return zipcode;
	}
	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(boolean zipcode) {
		this.zipcode = zipcode;
	}
	/**
	 * @return the phone
	 */
	public boolean isPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(boolean phone) {
		this.phone = phone;
	}
	/**
	 * @return the fax
	 */
	public boolean isFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(boolean fax) {
		this.fax = fax;
	}
	/**
	 * @return the emailId
	 */
	public boolean isEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(boolean emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the agencyName
	 */
	public boolean isAgencyName() {
		return agencyName;
	}
	/**
	 * @param agencyName the agencyName to set
	 */
	public void setAgencyName(boolean agencyName) {
		this.agencyName = agencyName;
	}
	/**
	 * @return the certName
	 */
	public boolean isCertName() {
		return certName;
	}
	/**
	 * @param certName the certName to set
	 */
	public void setCertName(boolean certName) {
		this.certName = certName;
	}
	/**
	 * @return the effDate
	 */
	public boolean isEffDate() {
		return effDate;
	}
	/**
	 * @param effDate the effDate to set
	 */
	public void setEffDate(boolean effDate) {
		this.effDate = effDate;
	}
	/**
	 * @return the expDate
	 */
	public boolean isExpDate() {
		return expDate;
	}
	/**
	 * @param expDate the expDate to set
	 */
	public void setExpDate(boolean expDate) {
		this.expDate = expDate;
	}
	/**
	 * @return the capablities
	 */
	public boolean isCapablities() {
		return capablities;
	}
	/**
	 * @param capablities the capablities to set
	 */
	public void setCapablities(boolean capablities) {
		this.capablities = capablities;
	}
	/**
	 * @return the naicsDesc
	 */
	public boolean isNaicsDesc() {
		return naicsDesc;
	}
	/**
	 * @param naicsDesc the naicsDesc to set
	 */
	public void setNaicsDesc(boolean naicsDesc) {
		this.naicsDesc = naicsDesc;
	}
	/**
	 * @return the naicsCapabilities
	 */
	public boolean isNaicsCapabilities() {
		return naicsCapabilities;
	}
	/**
	 * @param naicsCapabilities the naicsCapabilities to set
	 */
	public void setNaicsCapabilities(boolean naicsCapabilities) {
		this.naicsCapabilities = naicsCapabilities;
	}
	/**
	 * @return the diverseClassification
	 */
	public boolean isDiverseClassification() {
		return diverseClassification;
	}
	/**
	 * @param diverseClassification the diverseClassification to set
	 */
	public void setDiverseClassification(boolean diverseClassification) {
		this.diverseClassification = diverseClassification;
	}
	/**
	 * @return the certificateAgencyDetails
	 */
	public boolean isCertificateAgencyDetails() {
		return certificateAgencyDetails;
	}
	/**
	 * @param certificateAgencyDetails the certificateAgencyDetails to set
	 */
	public void setCertificateAgencyDetails(boolean certificateAgencyDetails) {
		this.certificateAgencyDetails = certificateAgencyDetails;
	}
}
