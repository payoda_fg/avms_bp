/*
 * DiverseCertificationTypesDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

import org.apache.struts.upload.FormFile;

/**
 * The Class DiverseCertificationTypesDto.
 * 
 * @author srinivasarao
 */
public class DiverseCertificationTypesDto implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The div cert type. */
	private Integer divCertType;

	/** The div cert agent. */
	private Integer divCertAgent;

	private String certificateNumber;

	private String certType;

	private String effectiveDate;

	/** The exp date. */
	private String expDate;
	private String ethnicity;
	// private String certPath;

	/** The certificate. */
	private FormFile certificate;

	/**
	 * Gets the certificate.
	 * 
	 * @return the certificate
	 */
	public FormFile getCertificate() {
		return certificate;
	}

	/**
	 * Sets the certificate.
	 * 
	 * @param certificate
	 *            the certificate to set
	 */
	public void setCertificate(FormFile certificate) {
		this.certificate = certificate;
	}

	/**
	 * @return the certType
	 */
	public String getCertType() {
		return certType;
	}

	/**
	 * @param certType
	 *            the certType to set
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the div cert type.
	 * 
	 * @return the div cert type
	 */
	public Integer getDivCertType() {
		return divCertType;
	}

	/**
	 * Sets the div cert type.
	 * 
	 * @param divCertType
	 *            the new div cert type
	 */
	public void setDivCertType(Integer divCertType) {
		this.divCertType = divCertType;
	}

	/**
	 * Gets the div cert agent.
	 * 
	 * @return the div cert agent
	 */
	public Integer getDivCertAgent() {
		return divCertAgent;
	}

	/**
	 * Sets the div cert agent.
	 * 
	 * @param divCertAgent
	 *            the new div cert agent
	 */
	public void setDivCertAgent(Integer divCertAgent) {
		this.divCertAgent = divCertAgent;
	}

	/**
	 * @return the certificateNumber
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * @param certificateNumber
	 *            the certificateNumber to set
	 */
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * Gets the exp date.
	 * 
	 * @return the exp date
	 */
	public String getExpDate() {
		return expDate;
	}

	/**
	 * Sets the exp date.
	 * 
	 * @param expDate
	 *            the new exp date
	 */
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	// public String getCertPath() {
	// return certPath;
	// }
	//
	// public void setCertPath(String certPath) {
	// this.certPath = certPath;
	// }
}
