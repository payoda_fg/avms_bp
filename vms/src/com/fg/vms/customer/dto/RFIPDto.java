/*
 * RFIPDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.List;

import com.fg.vms.customer.model.RFIPCompliance;
import com.fg.vms.customer.model.RequestIP;
import com.fg.vms.customer.model.RequestIPVendors;

/**
 * The Class RFIPDto.
 * 
 * @author pirabu
 */
public class RFIPDto implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
    private Integer id;

    /** The rfip number. */
    private String rfipNumber;

    /** The start date. */
    private String startDate;

    /** The end date. */
    private String endDate;

    /** The description. */
    private String description;

    /** The request information. */
    private RequestIP requestInformation;

    /** The compliances. */
    private List<RFIPCompliance> compliances;

    /** The ip vendors. */
    private List<RequestIPVendors> ipVendors;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the rfip number.
     * 
     * @return the rfipNumber
     */
    public String getRfipNumber() {
	return rfipNumber;
    }

    /**
     * Sets the rfip number.
     * 
     * @param rfipNumber
     *            the rfipNumber to set
     */
    public void setRfipNumber(String rfipNumber) {
	this.rfipNumber = rfipNumber;
    }

    /**
     * Gets the start date.
     * 
     * @return the startDate
     */
    public String getStartDate() {
	return startDate;
    }

    /**
     * Sets the start date.
     * 
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(String startDate) {
	this.startDate = startDate;
    }

    /**
     * Gets the end date.
     * 
     * @return the endDate
     */
    public String getEndDate() {
	return endDate;
    }

    /**
     * Sets the end date.
     * 
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(String endDate) {
	this.endDate = endDate;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
	return description;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
	this.description = description;
    }

    /**
     * Gets the request information.
     * 
     * @return the requestInformation
     */
    public RequestIP getRequestInformation() {
	return requestInformation;
    }

    /**
     * Sets the request information.
     * 
     * @param requestInformation
     *            the requestInformation to set
     */
    public void setRequestInformation(RequestIP requestInformation) {
	this.requestInformation = requestInformation;
    }

    /**
     * Gets the compliances.
     * 
     * @return the compliances
     */
    public List<RFIPCompliance> getCompliances() {
	return compliances;
    }

    /**
     * Sets the compliances.
     * 
     * @param compliances
     *            the compliances to set
     */
    public void setCompliances(List<RFIPCompliance> compliances) {
	this.compliances = compliances;
    }

    /**
     * Gets the ip vendors.
     * 
     * @return the ipVendors
     */
    public List<RequestIPVendors> getIpVendors() {
	return ipVendors;
    }

    /**
     * Sets the ip vendors.
     * 
     * @param ipVendors
     *            the ipVendors to set
     */
    public void setIpVendors(List<RequestIPVendors> ipVendors) {
	this.ipVendors = ipVendors;
    }

}
