/**
 * 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the information of naics code description and active status.
 * 
 * @author vinoth
 */
public class NaicsCodeDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The naics code. */
	private String naicsCode;

	/** The naicsDesc. */
	private String naicsDesc;

	/** The naicsParent. */
	private Integer naicsParent;

	/** The active. */
	private Byte active;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the naicsCode
	 */
	public String getNaicsCode() {
		return naicsCode;
	}

	/**
	 * @param naicsCode
	 *            the naicsCode to set
	 */
	public void setNaicsCode(String naicsCode) {
		this.naicsCode = naicsCode;
	}

	/**
	 * @return the naicsDesc
	 */
	public String getNaicsDesc() {
		return naicsDesc;
	}

	/**
	 * @param naicsDesc
	 *            the naicsDesc to set
	 */
	public void setNaicsDesc(String naicsDesc) {
		this.naicsDesc = naicsDesc;
	}

	/**
	 * @return the naicsParent
	 */
	public Integer getNaicsParent() {
		return naicsParent;
	}

	/**
	 * @param naicsParent
	 *            the naicsParent to set
	 */
	public void setNaicsParent(Integer naicsParent) {
		this.naicsParent = naicsParent;
	}

	/**
	 * @return the active
	 */
	public Byte isActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Byte active) {
		this.active = active;
	}

}
