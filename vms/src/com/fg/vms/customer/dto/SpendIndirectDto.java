/*
 * SpendIndirectDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the information of diverse certificates, direct spend value,
 * pro rated amount and direct spend report 
 * 
 * @author vinoth
 */
public class SpendIndirectDto implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The diverse certificate. */
    private String diverseCertificate;

    /** The direct spend value. */
    private Double directSpendValue;

    /** The prorated_amount. */
    private Double prorated_amount;

    /** The direct_ spendpercent. */
    private Double direct_Spendpercent;

    /**
     * Gets the diverse certificate.
     * 
     * @return the diverseCertificate
     */
    public String getDiverseCertificate() {
	return diverseCertificate;
    }

    /**
     * Sets the diverse certificate.
     * 
     * @param diverseCertificate
     *            the diverseCertificate to set
     */
    public void setDiverseCertificate(String diverseCertificate) {
	this.diverseCertificate = diverseCertificate;
    }

    /**
     * Gets the direct spend value.
     * 
     * @return the directSpendValue
     */
    public Double getDirectSpendValue() {
	return directSpendValue;
    }

    /**
     * Sets the direct spend value.
     * 
     * @param directSpendValue
     *            the directSpendValue to set
     */
    public void setDirectSpendValue(Double directSpendValue) {
	this.directSpendValue = directSpendValue;
    }

    /**
     * Gets the prorated_amount.
     * 
     * @return the prorated_amount
     */
    public Double getProrated_amount() {
	return prorated_amount;
    }

    /**
     * Sets the prorated_amount.
     * 
     * @param prorated_amount
     *            the prorated_amount to set
     */
    public void setProrated_amount(Double prorated_amount) {
	this.prorated_amount = prorated_amount;
    }

    /**
     * Gets the direct_ spendpercent.
     * 
     * @return the direct_Spendpercent
     */
    public Double getDirect_Spendpercent() {
	return direct_Spendpercent;
    }

    /**
     * Sets the direct_ spendpercent.
     * 
     * @param direct_Spendpercent
     *            the direct_Spendpercent to set
     */
    public void setDirect_Spendpercent(Double direct_Spendpercent) {
	this.direct_Spendpercent = direct_Spendpercent;
    }

}
