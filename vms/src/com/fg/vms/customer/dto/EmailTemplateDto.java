/**
 * EmailTemplateDto.java
 *
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;


/**
 * The Email Template DTO class
 * 
 * @author Kaleswararao
 *
 */
public class EmailTemplateDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer emailTemplateId;
	private String emailTemplateName;
	private String emailTemplateSubject;
	private Integer templateCode;
	private String customerDivision;
	private String emailTemplateMessage;
	private String templateType;
	/**
	 * @return the emailTemplateId
	 */
	public Integer getEmailTemplateId() {
		return emailTemplateId;
	}
	/**
	 * @param emailTemplateId the emailTemplateId to set
	 */
	public void setEmailTemplateId(Integer emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}
	/**
	 * @return the emailTemplateName
	 */
	public String getEmailTemplateName() {
		return emailTemplateName;
	}
	/**
	 * @param emailTemplateName the emailTemplateName to set
	 */
	public void setEmailTemplateName(String emailTemplateName) {
		this.emailTemplateName = emailTemplateName;
	}
	/**
	 * @return the emailTemplateSubject
	 */
	public String getEmailTemplateSubject() {
		return emailTemplateSubject;
	}
	/**
	 * @param emailTemplateSubject the emailTemplateSubject to set
	 */
	public void setEmailTemplateSubject(String emailTemplateSubject) {
		this.emailTemplateSubject = emailTemplateSubject;
	}
	/**
	 * @return the templateCode
	 */
	public Integer getTemplateCode() {
		return templateCode;
	}
	/**
	 * @param templateCode the templateCode to set
	 */
	public void setTemplateCode(Integer templateCode) {
		this.templateCode = templateCode;
	}
	/**
	 * @return the customerDivision
	 */
	public String getCustomerDivision() {
		return customerDivision;
	}
	/**
	 * @param customerDivision the customerDivision to set
	 */
	public void setCustomerDivision(String customerDivision) {
		this.customerDivision = customerDivision;
	}
	/**
	 * @return the emailTemplateMessage
	 */
	public String getEmailTemplateMessage() {
		return emailTemplateMessage;
	}
	/**
	 * @param emailTemplateMessage the emailTemplateMessage to set
	 */
	public void setEmailTemplateMessage(String emailTemplateMessage) {
		this.emailTemplateMessage = emailTemplateMessage;
	}
	/**
	 * @return the templateType
	 */
	public String getTemplateType() {
		return templateType;
	}
	/**
	 * @param templateType the templateType to set
	 */
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
}
