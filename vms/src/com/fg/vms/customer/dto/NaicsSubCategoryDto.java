/*
 * NaicsSubCategoryDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the information of naics sub-category description and active status.
 * 
 * @author Vinoth
 */
public class NaicsSubCategoryDto implements Serializable {

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
    private Integer id;

    /** The naics sub category desc. */
    private String naicsSubCategoryDesc;

    /** The active. */
    private boolean active;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the naics sub category desc.
     * 
     * @return the naicsSubCategoryDesc
     */
    public String getNaicsSubCategoryDesc() {
	return naicsSubCategoryDesc;
    }

    /**
     * Sets the naics sub category desc.
     * 
     * @param naicsSubCategoryDesc
     *            the naicsSubCategoryDesc to set
     */
    public void setNaicsSubCategoryDesc(String naicsSubCategoryDesc) {
	this.naicsSubCategoryDesc = naicsSubCategoryDesc;
    }

    /**
     * Checks if is active.
     * 
     * @return the active
     */
    public boolean isActive() {
	return active;
    }

    /**
     * Sets the active.
     * 
     * @param active
     *            the active to set
     */
    public void setActive(boolean active) {
	this.active = active;
    }

}
