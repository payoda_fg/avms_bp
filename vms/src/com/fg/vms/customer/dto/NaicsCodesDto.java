/*
 * NaicsCodesDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * The Class NaicsCodesDto.
 * 
 * @author srinivasarao
 */
public class NaicsCodesDto implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
    private Integer id;

    /** The naics code. */
    private String naicsCode;

    /** The primary key. */
    private Byte primaryKey;

    /** The capabilitie. */
    private String capabilitie;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the naics code.
     * 
     * @return the naics code
     */
    public String getNaicsCode() {
	return naicsCode;
    }

    /**
     * Sets the naics code.
     * 
     * @param naicsCode
     *            the new naics code
     */
    public void setNaicsCode(String naicsCode) {
	this.naicsCode = naicsCode;
    }

    /**
     * Gets the primary key.
     * 
     * @return the primary key
     */
    public Byte getPrimaryKey() {
	return primaryKey;
    }

    /**
     * Sets the primary key.
     * 
     * @param primaryKey
     *            the new primary key
     */
    public void setPrimaryKey(Byte primaryKey) {
	this.primaryKey = primaryKey;
    }

    /**
     * Gets the capabilitie.
     * 
     * @return the capabilitie
     */
    public String getCapabilitie() {
	return capabilitie;
    }

    /**
     * Sets the capabilitie.
     * 
     * @param capabilitie
     *            the capabilitie to set
     */
    public void setCapabilitie(String capabilitie) {
	this.capabilitie = capabilitie;
    }

}
