/*
 * RFIDocument.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Represents the RFI and RFP Documents.
 * 
 * @author pirabu
 * 
 */
public class RFIDocument implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
    private Integer id;

    /** The title. */
    private String title;

    /** The Doc path. */
    private String DocPath;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the title.
     * 
     * @return the title
     */
    public String getTitle() {
	return title;
    }

    /**
     * Sets the title.
     * 
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
	this.title = title;
    }

    /**
     * Gets the doc path.
     * 
     * @return the docPath
     */
    public String getDocPath() {
	return DocPath;
    }

    /**
     * Sets the doc path.
     * 
     * @param docPath
     *            the docPath to set
     */
    public void setDocPath(String docPath) {
	DocPath = docPath;
    }

}
