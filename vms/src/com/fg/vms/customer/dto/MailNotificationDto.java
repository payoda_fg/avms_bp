/*
 * MailNotificationDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the vendor name, country and active details.
 * 
 * @author Vinoth
 */
public class MailNotificationDto implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The vendor name. */
    private String vendorName;

    /** The country. */
    private String country;

    /** The id. */
    private Integer id;

    /** The active. */
    private boolean active;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Checks if is active.
     * 
     * @return the active
     */
    public boolean isActive() {
	return active;
    }

    /**
     * Sets the active.
     * 
     * @param active
     *            the active to set
     */
    public void setActive(boolean active) {
	this.active = active;
    }

    /**
     * Gets the country.
     * 
     * @return the country
     */
    public String getCountry() {
	return country;
    }

    /**
     * Sets the country.
     * 
     * @param country
     *            the country to set
     */
    public void setCountry(String country) {
	this.country = country;
    }

    /**
     * Gets the vendor name.
     * 
     * @return the vendorName
     */
    public String getVendorName() {
	return vendorName;
    }

    /**
     * Sets the vendor name.
     * 
     * @param vendorName
     *            the vendorName to set
     */
    public void setVendorName(String vendorName) {
	this.vendorName = vendorName;
    }

}
