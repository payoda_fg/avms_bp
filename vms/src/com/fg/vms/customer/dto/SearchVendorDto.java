/*
 * SearchVendorDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.Date;

import com.fg.vms.common.Country;
import com.fg.vms.customer.model.AnonymousVendor;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;

/**
 * Holds the information of naics category, sub-category details and country,
 * status details.
 * 
 * @author vinoth
 */
public class SearchVendorDto implements Serializable {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The vendor. */
	private VendorMaster vendor;

	private NaicsCode naics;

	/** The anonymous vendor. */
	private AnonymousVendor anonymousVendor;

	/** The country. */
	private Country country;

	private com.fg.vms.customer.model.Country country2;

	private State state;

	/** The contact. */
	private VendorContact contact;

	private Integer id;
	private String countryName;
	private String naicsCode;
	private String duns;
	private String region;
	private String stateName;
	private String city;
	private String vendorName;

	private String modeOFReg;
	private String vendorCode;
	private String vendorStatus;

	private Date createdon;
	private Byte primenonprimevendor;
	private Byte divsersupplier;
	private String emailId;
	private Byte isapproved;
	private String companyEmailId;
	private String primeContactEmailId;	
	private String created;
	
	private String statusNote;
	private String statusDate;
	private String contactMobile;
	private String contactUser;
	
	private String diverseClassifcation;
	private String certificateType;
	private String businessType;
	private String yearOfEstd;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	
	private String vendorNotes;

	public SearchVendorDto(Integer id, String countryName, String naicsCode,
			String duns, String region, String stateName, String city,
			String vendorName) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.naicsCode = naicsCode;
		this.duns = duns;
		this.region = region;
		this.stateName = stateName;
		this.city = city;
		this.vendorName = vendorName;
	}

	public SearchVendorDto(Integer id, String countryName, String naicsCode,
			String duns, String region, String stateName, String city,
			String vendorName, String vendorCode, String modeOFReg,
			String vendorStatus, String companyEmailId, String primeContactEmailId) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.naicsCode = naicsCode;
		this.duns = duns;
		this.region = region;
		this.stateName = stateName;
		this.city = city;
		this.vendorName = vendorName;
		this.modeOFReg = modeOFReg;
		this.vendorCode = vendorCode;
		this.vendorStatus = vendorStatus;
		this.companyEmailId=companyEmailId;
		this.primeContactEmailId=primeContactEmailId;
	}
	public SearchVendorDto(Integer id, String countryName, String naicsCode,
			String duns, String region, String stateName, String city,
			String vendorName, String vendorCode, String modeOFReg,
			String vendorStatus, String companyEmailId, String primeContactEmailId,String createdon,
			String statusNote, String statusDate ,String contactMobile, String contactUser, String diverseClassifcation,
			String certificateType, String businessType, String yearOfEstd, String firstName, String lastName, String phoneNumber, String vendorNotes) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.naicsCode = naicsCode;
		this.duns = duns;
		this.region = region;
		this.stateName = stateName;
		this.city = city;
		this.vendorName = vendorName;
		this.modeOFReg = modeOFReg;
		this.vendorCode = vendorCode;
		this.vendorStatus = vendorStatus;
		this.companyEmailId=companyEmailId;
		this.primeContactEmailId=primeContactEmailId;
		this.created=createdon;
		this.statusNote = statusNote;
		this.statusDate = statusDate;
		this.contactMobile = contactMobile;
		this.contactUser = contactUser;
		this.diverseClassifcation = diverseClassifcation;
		this.certificateType = certificateType;
		this.businessType = businessType;
		this.yearOfEstd = yearOfEstd;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.vendorNotes = vendorNotes;
	}

	public SearchVendorDto(Integer id, String countryName, String naicsCode,
			String duns, String region, String stateName, String city,
			String vendorName, Date createdon, Byte primenonprimevendo,
			Byte divsersupplier, String emailId, Byte isapproved) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.naicsCode = naicsCode;
		this.duns = duns;
		this.region = region;
		this.stateName = stateName;
		this.city = city;
		this.vendorName = vendorName;
		this.createdon = createdon;
		this.primenonprimevendor = primenonprimevendo;
		this.divsersupplier = divsersupplier;
		this.emailId = emailId;
		this.isapproved = isapproved;

	}

	public Byte getIsapproved() {
		return isapproved;
	}

	public void setIsapproved(Byte isapproved) {
		this.isapproved = isapproved;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Byte getPrimenonprimevendor() {
		return primenonprimevendor;
	}

	public void setPrimenonprimevendor(Byte primenonprimevendor) {
		this.primenonprimevendor = primenonprimevendor;
	}

	public Byte getDivsersupplier() {
		return divsersupplier;
	}

	public void setDivsersupplier(Byte divsersupplier) {
		this.divsersupplier = divsersupplier;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

	public String getModeOFReg() {
		return modeOFReg;
	}

	public void setModeOFReg(String modeOFReg) {
		this.modeOFReg = modeOFReg;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getNaicsCode() {
		return naicsCode;
	}

	public void setNaicsCode(String naicsCode) {
		this.naicsCode = naicsCode;
	}

	public String getDuns() {
		return duns;
	}

	public void setDuns(String duns) {
		this.duns = duns;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Instantiates a new search vendor dto.
	 * 
	 * @param vendor
	 *            the vendor
	 * @param naics
	 *            the naics
	 * @param category
	 *            the category
	 * @param subCategory
	 *            the sub category
	 * @param anonymousVendor
	 *            the anonymous vendor
	 * @param contact
	 *            the contact
	 */
	public SearchVendorDto(VendorMaster vendor, NaicsCode naics,
			AnonymousVendor anonymousVendor, VendorContact contact,
			com.fg.vms.customer.model.Country country, State state) {
		super();
		this.vendor = vendor;
		this.naics = naics;
		this.country2 = country;
		this.state = state;
		this.anonymousVendor = anonymousVendor;
		this.contact = contact;
	}

	/**
	 * Gets the vendor.
	 * 
	 * @return the vendor
	 */
	public VendorMaster getVendor() {
		return vendor;
	}

	/**
	 * Sets the vendor.
	 * 
	 * @param vendor
	 *            the vendor to set
	 */
	public void setVendor(VendorMaster vendor) {
		this.vendor = vendor;
	}

	/**
	 * @return the naics
	 */
	public NaicsCode getNaics() {
		return naics;
	}

	/**
	 * @param naics
	 *            the naics to set
	 */
	public void setNaics(NaicsCode naics) {
		this.naics = naics;
	}

	/**
	 * Gets the anonymous vendor.
	 * 
	 * @return the anonymousVendor
	 */
	public AnonymousVendor getAnonymousVendor() {
		return anonymousVendor;
	}

	/**
	 * Sets the anonymous vendor.
	 * 
	 * @param anonymousVendor
	 *            the anonymousVendor to set
	 */
	public void setAnonymousVendor(AnonymousVendor anonymousVendor) {
		this.anonymousVendor = anonymousVendor;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the new country
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * Gets the contact.
	 * 
	 * @return the contact
	 */
	public VendorContact getContact() {
		return contact;
	}

	/**
	 * Sets the contact.
	 * 
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(VendorContact contact) {
		this.contact = contact;
	}

	public com.fg.vms.customer.model.Country getCountry2() {
		return country2;
	}

	public void setCountry2(com.fg.vms.customer.model.Country country2) {
		this.country2 = country2;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the companyEmailId
	 */
	public String getCompanyEmailId() {
		return companyEmailId;
	}

	/**
	 * @param companyEmailId the companyEmailId to set
	 */
	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}

	/**
	 * @return the primeContactEmailId
	 */
	public String getPrimeContactEmailId() {
		return primeContactEmailId;
	}

	/**
	 * @param primeContactEmailId the primeContactEmailId to set
	 */
	public void setPrimeContactEmailId(String primeContactEmailId) {
		this.primeContactEmailId = primeContactEmailId;
	}

	/**
	 * @return the created
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	public String getStatusNote() {
		return statusNote;
	}

	public void setStatusNote(String statusNote) {
		this.statusNote = statusNote;
	}

	public String getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}

	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public String getContactUser() {
		return contactUser;
	}

	public void setContactUser(String contactUser) {
		this.contactUser = contactUser;
	}

	public String getDiverseClassifcation() {
		return diverseClassifcation;
	}

	public void setDiverseClassifcation(String diverseClassifcation) {
		this.diverseClassifcation = diverseClassifcation;
	}

	public String getCertificateType() {
		return certificateType;
	}

	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getYearOfEstd() {
		return yearOfEstd;
	}

	public void setYearOfEstd(String yearOfEstd) {
		this.yearOfEstd = yearOfEstd;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getVendorNotes() {
		return vendorNotes;
	}

	public void setVendorNotes(String vendorNotes) {
		this.vendorNotes = vendorNotes;
	}
}
