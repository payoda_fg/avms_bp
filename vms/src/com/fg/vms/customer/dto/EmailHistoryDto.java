/**
 * 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * @author Asarudeen
 *
 */
public class EmailHistoryDto implements Serializable
{	
	private static final long serialVersionUID = 1L;
	
	private Integer emailHistoryId;
	private String emailId;
	private String addressType;
	private String emailSubject;
	private String emailMessage;
	private String emailDate;
	
	/**
	 * @return the emailHistoryId
	 */
	public Integer getEmailHistoryId() {
		return emailHistoryId;
	}
	
	/**
	 * @param emailHistoryId the emailHistoryId to set
	 */
	public void setEmailHistoryId(Integer emailHistoryId) {
		this.emailHistoryId = emailHistoryId;
	}
	
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	/**
	 * @return the addressType
	 */
	public String getAddressType() {
		return addressType;
	}
	
	/**
	 * @param addressType the addressType to set
	 */
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	
	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}
	
	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	/**
	 * @return the emailMessage
	 */
	public String getEmailMessage() {
		return emailMessage;
	}
	
	/**
	 * @param emailMessage the emailMessage to set
	 */
	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}
	
	/**
	 * @return the emailDate
	 */
	public String getEmailDate() {
		return emailDate;
	}
	
	/**
	 * @param emailDate the emailDate to set
	 */
	public void setEmailDate(String emailDate) {
		this.emailDate = emailDate;
	}	
}