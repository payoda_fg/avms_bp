/*
 * CertifyingAgencyDto.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the certifying agency details such as agency name, active status
 * details .
 * 
 * @author Vinoth
 */
public class CertifyingAgencyDto implements Serializable{

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The agency name. */
	private String agencyName;

	/** The agency short name. */
	private String agencyShortName;

	private String phone;

	private String fax;

	private String address;

	/** The active. */
	private boolean active;

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the agency name.
	 * 
	 * @return the agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * Sets the agency name.
	 * 
	 * @param agencyName
	 *            the agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * @return the agencyShortName
	 */
	public String getAgencyShortName() {
		return agencyShortName;
	}

	/**
	 * @param agencyShortName
	 *            the agencyShortName to set
	 */
	public void setAgencyShortName(String agencyShortName) {
		this.agencyShortName = agencyShortName;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the active
	 */
	public boolean isActive() {
		return active;

	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

}
