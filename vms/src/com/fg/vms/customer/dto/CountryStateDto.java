/**
 * CountryStateDto.java
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;
import java.util.List;

import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.State;

/**
 * @author asarudeen
 * 
 */
public class CountryStateDto implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private Integer geographicServiceStateId;

	private Integer countryId;

	private Integer stateId;

	private String country;

	private String state;

	private String[] states;

	private String[] countries;

	private List<State> stateList;

	private List<Country> countryList;

	private String serviceCountry;

	private String serviceState;

	/**
	 * @return the geographicServiceStateId
	 */
	public Integer getGeographicServiceStateId() {
		return geographicServiceStateId;
	}

	/*
	 * @param geographicServiceStateId 
	 * 							the geographicServiceStateId to set
	 */
	public void setGeographicServiceStateId(Integer geographicServiceStateId) {
		this.geographicServiceStateId = geographicServiceStateId;
	}

	/**
	 * @return the countryId
	 */
	public Integer getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId
	 *            the countryId to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId
	 *            the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the states
	 */
	public String[] getStates() {
		return states;
	}

	/**
	 * @param states
	 *            the states to set
	 */
	public void setStates(String[] states) {
		this.states = states;
	}

	/**
	 * @return the countries
	 */
	public String[] getCountries() {
		return countries;
	}

	/**
	 * @param countries
	 *            the countries to set
	 */
	public void setCountries(String[] countries) {
		this.countries = countries;
	}

	/**
	 * @return the stateList
	 */
	public List<State> getStateList() {
		return stateList;
	}

	/**
	 * @param stateList
	 *            the stateList to set
	 */
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}

	/**
	 * @return the countryList
	 */
	public List<Country> getCountryList() {
		return countryList;
	}

	/**
	 * @param countryList
	 *            the countryList to set
	 */
	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}

	/**
	 * @return the serviceCountry
	 */
	public String getServiceCountry() {
		return serviceCountry;
	}

	/**
	 * @param serviceCountry
	 *            the serviceCountry to set
	 */
	public void setServiceCountry(String serviceCountry) {
		this.serviceCountry = serviceCountry;
	}

	/**
	 * @return the serviceState
	 */
	public String getServiceState() {
		return serviceState;
	}

	/**
	 * @param serviceState
	 *            the serviceState to set
	 */
	public void setServiceState(String serviceState) {
		this.serviceState = serviceState;
	}
}
