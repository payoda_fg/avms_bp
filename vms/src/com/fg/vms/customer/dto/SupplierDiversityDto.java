package com.fg.vms.customer.dto;

import java.io.Serializable;

public class SupplierDiversityDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String firstName;

	private String lastName;

	private Integer requestorId;

	private String createdOn;

	private String requestedOn;

	private String oppurtunityEventName;

	private String sourceEvent;

	private String sourceEventStartDate;

	private Double estimateSpend;

	private String estimateWorkStartDate;

	private String lastResponseDate;

	private String commodityCode;

	private String scopOfWork;

	private String geography;

	private String miniSupplierRequirement;

	private String contractInsight;

	private Integer incumbentVendorId;

	private String business;

	public SupplierDiversityDto(Integer id,String firstName, String requestedOn,
			String oppurtunityEventName, String sourceEvent,
			String sourceEventStartDate, Double estimateSpend,
			String estimateWorkStartDate, String business) {
		this.id=id;
		this.firstName = firstName;
		this.requestedOn = requestedOn;
		this.oppurtunityEventName = oppurtunityEventName;
		this.sourceEvent = sourceEvent;
		this.sourceEventStartDate = sourceEventStartDate;
		this.estimateSpend = estimateSpend;
		this.estimateWorkStartDate = estimateWorkStartDate;
		this.business=business;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Integer requestorId) {
		this.requestorId = requestorId;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(String requestedOn) {
		this.requestedOn = requestedOn;
	}

	public String getOppurtunityEventName() {
		return oppurtunityEventName;
	}

	public void setOppurtunityEventName(String oppurtunityEventName) {
		this.oppurtunityEventName = oppurtunityEventName;
	}

	public String getSourceEvent() {
		return sourceEvent;
	}

	public void setSourceEvent(String sourceEvent) {
		this.sourceEvent = sourceEvent;
	}

	public String getSourceEventStartDate() {
		return sourceEventStartDate;
	}

	public void setSourceEventStartDate(String sourceEventStartDate) {
		this.sourceEventStartDate = sourceEventStartDate;
	}

	public Double getEstimateSpend() {
		return estimateSpend;
	}

	public void setEstimateSpend(Double estimateSpend) {
		this.estimateSpend = estimateSpend;
	}

	public String getEstimateWorkStartDate() {
		return estimateWorkStartDate;
	}

	public void setEstimateWorkStartDate(String estimateWorkStartDate) {
		this.estimateWorkStartDate = estimateWorkStartDate;
	}

	public String getLastResponseDate() {
		return lastResponseDate;
	}

	public void setLastResponseDate(String lastResponseDate) {
		this.lastResponseDate = lastResponseDate;
	}

	public String getCommodityCode() {
		return commodityCode;
	}

	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}

	public String getScopOfWork() {
		return scopOfWork;
	}

	public void setScopOfWork(String scopOfWork) {
		this.scopOfWork = scopOfWork;
	}

	public String getGeography() {
		return geography;
	}

	public void setGeography(String geography) {
		this.geography = geography;
	}

	public String getMiniSupplierRequirement() {
		return miniSupplierRequirement;
	}

	public void setMiniSupplierRequirement(String miniSupplierRequirement) {
		this.miniSupplierRequirement = miniSupplierRequirement;
	}

	public String getContractInsight() {
		return contractInsight;
	}

	public void setContractInsight(String contractInsight) {
		this.contractInsight = contractInsight;
	}

	public Integer getIncumbentVendorId() {
		return incumbentVendorId;
	}

	public void setIncumbentVendorId(Integer incumbentVendorId) {
		this.incumbentVendorId = incumbentVendorId;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
