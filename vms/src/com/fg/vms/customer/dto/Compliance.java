/*
 * Compilance.java 
 */
package com.fg.vms.customer.dto;

import java.io.Serializable;

/**
 * Holds the compliance details such as template questions, template details and
 * data type.
 * 
 * @author pirabu
 */
public class Compliance implements Serializable{

    /**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The compliance item. */
    private String complianceItem;

    /** The target. */
    private char target;

    /** The attributes. */
    private Object attributes[];

    /** The mandatory. */
    private char mandatory;

    /** The note. */
    private String note;

    /** The weightages. */
    private Object weightages[];

    /**
     * Gets the compliance item.
     * 
     * @return the complianceItem
     */
    public String getComplianceItem() {
	return complianceItem;
    }

    /**
     * Sets the compliance item.
     * 
     * @param complianceItem
     *            the complianceItem to set
     */
    public void setComplianceItem(String complianceItem) {
	this.complianceItem = complianceItem;
    }

    /**
     * Gets the attributes.
     * 
     * @return the attributes
     */
    public Object[] getAttributes() {
	return attributes;
    }

    /**
     * Sets the attributes.
     * 
     * @param attributes
     *            the attributes to set
     */
    public void setAttributes(Object[] attributes) {
	this.attributes = attributes;
    }

    /**
     * Gets the target.
     * 
     * @return the target
     */
    public char getTarget() {
	return target;
    }

    /**
     * Sets the target.
     * 
     * @param target
     *            the target to set
     */
    public void setTarget(char target) {
	this.target = target;
    }

    /**
     * Gets the mandatory.
     * 
     * @return the mandatory
     */
    public char getMandatory() {
	return mandatory;
    }

    /**
     * Sets the mandatory.
     * 
     * @param mandatory
     *            the mandatory to set
     */
    public void setMandatory(char mandatory) {
	this.mandatory = mandatory;
    }

    /**
     * Gets the note.
     * 
     * @return the note
     */
    public String getNote() {
	return note;
    }

    /**
     * Sets the note.
     * 
     * @param note
     *            the note to set
     */
    public void setNote(String note) {
	this.note = note;
    }

    /**
     * Gets the weightages.
     * 
     * @return the weightages
     */
    public Object[] getWeightages() {
	return weightages;
    }

    /**
     * Sets the weightages.
     * 
     * @param weightages
     *            the weightages to set
     */
    public void setWeightages(Object[] weightages) {
	this.weightages = weightages;
    }

}
