package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "customer_vendor_certificatedocument")
public class CustomerVendorCertificateDocument implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "certificate_id")
	private Integer certificateId;

	@Column(name = "vendorid")
	private Integer vendorId;

	@Column(name = "filename")
	private String fileName;

	@Column(name = "physicalpath")
	private String physicalPath;
	
	@Column(name = "certificate_othercertificate")
	private String certificateOtherCertificate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCertificateId() {
		return certificateId;
	}

	public void setCertificateId(Integer certificateId) {
		this.certificateId = certificateId;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPhysicalPath() {
		return physicalPath;
	}

	public void setPhysicalPath(String physicalPath) {
		this.physicalPath = physicalPath;
	}

	public String getCertificateOtherCertificate() {
		return certificateOtherCertificate;
	}

	public void setCertificateOtherCertificate(String certificateOtherCertificate) {
		this.certificateOtherCertificate = certificateOtherCertificate;
	}
}
