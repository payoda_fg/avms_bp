/**
 * CustomExportMaster.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model class is footprint for customizable export supplier report.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "avms_custom_report_field_selection")
public class CustomExportMaster implements Serializable {

	/**
	 * default serial version id.
	 */
	private static final long serialVersionUID = -6919640644258540296L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "CATEGORY")
	private String category;

	@Column(name = "INPUTTABLENAME")
	private String inputTableName;

	@Column(name = "INPUTCOLUMNNAME")
	private String columnName;

	@Column(name = "COLUMNTITLE")
	private String columnTitle;

	@Column(name = "OUTPUTTABLENAME")
	private String outputTableName;

	@Column(name = "OUTPUTSQL")
	private String outputSql;

	@Column(name = "WHERECONDITION")
	private String whCondition;

	@Column(name = "JOINTYPE")
	private String joinType;

	@Column(name = "TABLEORDER")
	private String tableOrder;

	@Column(name = "COLUMNORDER")
	private String columnOrder;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the inputTableName
	 */
	public String getInputTableName() {
		return inputTableName;
	}

	/**
	 * @param inputTableName
	 *            the inputTableName to set
	 */
	public void setInputTableName(String inputTableName) {
		this.inputTableName = inputTableName;
	}

	/**
	 * @return the outputTableName
	 */
	public String getOutputTableName() {
		return outputTableName;
	}

	/**
	 * @param outputTableName
	 *            the outputTableName to set
	 */
	public void setOutputTableName(String outputTableName) {
		this.outputTableName = outputTableName;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @return the columnTitle
	 */
	public String getColumnTitle() {
		return columnTitle;
	}

	/**
	 * @param columnTitle
	 *            the columnTitle to set
	 */
	public void setColumnTitle(String columnTitle) {
		this.columnTitle = columnTitle;
	}

	/**
	 * @return the outputSql
	 */
	public String getOutputSql() {
		return outputSql;
	}

	/**
	 * @param outputSql
	 *            the outputSql to set
	 */
	public void setOutputSql(String outputSql) {
		this.outputSql = outputSql;
	}

	/**
	 * @return the whCondition
	 */
	public String getWhCondition() {
		return whCondition;
	}

	/**
	 * @param whCondition
	 *            the whCondition to set
	 */
	public void setWhCondition(String whCondition) {
		this.whCondition = whCondition;
	}

	/**
	 * @return the joinType
	 */
	public String getJoinType() {
		return joinType;
	}

	/**
	 * @param joinType
	 *            the joinType to set
	 */
	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	/**
	 * @return the tableOrder
	 */
	public String getTableOrder() {
		return tableOrder;
	}

	/**
	 * @param tableOrder
	 *            the tableOrder to set
	 */
	public void setTableOrder(String tableOrder) {
		this.tableOrder = tableOrder;
	}

	/**
	 * @return the columnOrder
	 */
	public String getColumnOrder() {
		return columnOrder;
	}

	/**
	 * @param columnOrder
	 *            the columnOrder to set
	 */
	public void setColumnOrder(String columnOrder) {
		this.columnOrder = columnOrder;
	}

}
