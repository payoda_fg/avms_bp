/*
 * VendorCertificate.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Create model class for Vendor Certificate
 * 
 * @author srinivasarao
 * 
 */
@Entity
@Table(name = "customer_vendorcertificate")
public class VendorCertificate implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the VendorCertificate which serves as the primary
	 * key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CERTMASTERID", nullable = false)
	private Certificate certMasterId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "VENDORID", nullable = false)
	private VendorMaster vendorId;

	/** 0-Diverse, 1-Quality */
	@Column(name = "DIVERSER_QUALITY", nullable = false)
	private Byte diverseQuality;

	@Column(name = "CERTIFICATENUMBER")
	private String certificateNumber;

	@Column(name = "CERTIFICATETYPE")
	private String certificateType;

	@Column(name = "EFFECTIVEDATE")
	private Date effectiveDate;

	@Column(name = "EXPIRYDATE")
	private Date expiryDate;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CERTAGENCYID", nullable = false)
	private CertifyingAgency certAgencyId;

	@Column(name = "FILENAME")
	private String filename;

	@Column(name = "ETHNICITY_ID")
	private Integer ethnicityID;

	@Column(name = "CONTENT")
	@Lob
	private Blob content;

	@Column(name = "CONTENT_TYPE")
	private String contentType;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the certificateMasterId (@link Certificate)
	 */
	public Certificate getCertMasterId() {
		return certMasterId;
	}

	/**
	 * 
	 * @param certMasterId
	 *            the certMasterId to set
	 */
	public void setCertMasterId(Certificate certMasterId) {
		this.certMasterId = certMasterId;
	}

	/**
	 * 
	 * @return the vendorId
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * 
	 * @param vendorId
	 *            (@link VendorMaster) the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * 
	 * @return the diverseQuality
	 */
	public Byte isDiverseQuality() {
		return diverseQuality;
	}

	/**
	 * 
	 * @param diverseQuality
	 *            the diverseQuality to set
	 */
	public void setDiverseQuality(Byte diverseQuality) {
		this.diverseQuality = diverseQuality;
	}

	/**
	 * @return the certificateNumber
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * @param certificateNumber
	 *            the certificateNumber to set
	 */
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * @return the certificateType
	 */
	public String getCertificateType() {
		return certificateType;
	}

	/**
	 * @param certificateType
	 *            the certificateType to set
	 */
	public void setCertificateType(String certificateType) {
		this.certificateType = certificateType;
	}

	/**
	 * @return the diverseQuality
	 */
	public Byte getDiverseQuality() {
		return diverseQuality;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * 
	 * @return the certAgencyId
	 */
	public CertifyingAgency getCertAgencyId() {
		return certAgencyId;
	}

	/**
	 * 
	 * @param certAgencyId
	 *            the certAgencyId to set
	 */
	public void setCertAgencyId(CertifyingAgency certAgencyId) {
		this.certAgencyId = certAgencyId;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename
	 *            the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getEthnicityID() {
		return ethnicityID;
	}

	public void setEthnicityID(Integer ethnicityID) {
		this.ethnicityID = ethnicityID;
	}

	/**
	 * @return the content
	 */
	public Blob getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(Blob content) {
		this.content = content;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
