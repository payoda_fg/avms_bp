/*
 * Template.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * model class represents the template details
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "template")
public class Template implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the Template which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "TEMPLATENAME", nullable = false)
    private String templateName;

    @Column(name = "ASSESSQUESTREMARKS")
    private String assessQuestRemarks;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte isActive;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    /**
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * 
     * @return the tempaltename
     */
    public String getTemplateName() {
	return templateName;
    }

    /**
     * 
     * @param templateName
     *            the templatename to set
     */
    public void setTemplateName(String templateName) {
	this.templateName = templateName;
    }

    /**
     * 
     * @return the questionremarks
     */
    public String getAssessQuestRemarks() {
	return assessQuestRemarks;
    }

    /**
     * 
     * @param assessQuestRemarks
     *            the assessQuestRemarks to set
     */
    public void setAssessQuestRemarks(String assessQuestRemarks) {
	this.assessQuestRemarks = assessQuestRemarks;
    }

    /**
     * 
     * @return the active status
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * 
     * @param isActive
     *            the status to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * 
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * 
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * 
     * @return the createdOn date
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * 
     * @param createdOn
     *            the createdOn date to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * 
     * @return the modified user
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * 
     * @param modifiedBy
     *            the modified user to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * 
     * @return the modifiedOn date
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * 
     * @param modifiedOn
     *            the modifiedOn date to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
