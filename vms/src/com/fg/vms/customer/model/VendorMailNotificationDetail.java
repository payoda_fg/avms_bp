/*
 * VendorMailNotificationDetail.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents the email notification details (eg..vendor email notification
 * master details and vendor details etc.) of customer.
 * 
 * @author Vinoth
 */
@Entity
@Table(name = "vendor_email_notification_detail")
public class VendorMailNotificationDetail implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the VendorMailNotificationDetail which serves as
     * the primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "VENDOREMAILNOTIFICATIONMASTERID")
    private VendorEmailNotificationMaster vendorEmailNotificationMaster;

    @Column(name = "VENDOREMAILID")
    private String vendorEmailId;

    @ManyToOne
    @JoinColumn(name = "VENDORID")
    private VendorMaster vendorMaster;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the vendorEmailNotificationMaster
     */
    public VendorEmailNotificationMaster getVendorEmailNotificationMaster() {
	return vendorEmailNotificationMaster;
    }

    /**
     * @param vendorEmailNotificationMaster
     *            the vendorEmailNotificationMaster to set
     */
    public void setVendorEmailNotificationMaster(
	    VendorEmailNotificationMaster vendorEmailNotificationMaster) {
	this.vendorEmailNotificationMaster = vendorEmailNotificationMaster;
    }

    /**
     * @return the vendorEmailId
     */
    public String getVendorEmailId() {
	return vendorEmailId;
    }

    /**
     * @param vendorEmailId
     *            the vendorEmailId to set
     */
    public void setVendorEmailId(String vendorEmailId) {
	this.vendorEmailId = vendorEmailId;
    }

    /**
     * @return the vendorMaster
     */
    public VendorMaster getVendorMaster() {
	return vendorMaster;
    }

    /**
     * @param vendorMaster
     *            the vendorMaster to set
     */
    public void setVendorMaster(VendorMaster vendorMaster) {
	this.vendorMaster = vendorMaster;
    }

}
