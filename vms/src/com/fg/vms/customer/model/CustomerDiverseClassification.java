/**
 * CustomerDiverseClassification.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents customer vendor diverse classifications.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_vendordiverseclassifcation")
public class CustomerDiverseClassification implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the CustomerDiverseClassification which serves as
	 * the primary key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	/** 0-Diverse, 1-Quality */
	@Column(name = "DIVERSE_QUALITY", nullable = false)
	private Byte diverseQuality;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CERTMASTERID", nullable = false)
	private Certificate certMasterId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "VENDORID", nullable = false)
	private VendorMaster vendorId;

	@Column(name = "ACTIVE")
	private Byte isActive;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the diverseQuality
	 */
	public Byte getDiverseQuality() {
		return diverseQuality;
	}

	/**
	 * @param diverseQuality
	 *            the diverseQuality to set
	 */
	public void setDiverseQuality(Byte diverseQuality) {
		this.diverseQuality = diverseQuality;
	}

	/**
	 * @return the certMasterId
	 */
	public Certificate getCertMasterId() {
		return certMasterId;
	}

	/**
	 * @param certMasterId
	 *            the certMasterId to set
	 */
	public void setCertMasterId(Certificate certMasterId) {
		this.certMasterId = certMasterId;
	}

	/**
	 * @return the vendorId
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}