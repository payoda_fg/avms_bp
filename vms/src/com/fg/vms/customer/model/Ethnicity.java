/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "ethnicity")
public class Ethnicity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "Ethnicity")
    private String ethnicity;

    @Column(name = "CreatedBy")
    private Integer createdBy;

    @Column(name = "CreatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "ModifiedBy")
    private Integer modifiedBy;

    @Column(name = "ModifiedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;
    
    @Column(name = "ISACTIVE", columnDefinition = "tinyint(4) default 1")
	private short isactive;

    @OneToMany(mappedBy = "ethnicityId")
    private List<VendorMaster> customerVendormasterList;
    @OneToMany(mappedBy = "ethnicityId")
    private List<Tier2ReportIndirectExpenses> customerTier2reportindirectexpensesList;

    public Ethnicity() {
    }

    public Ethnicity(Integer id) {
        this.id = id;
    }

    public Ethnicity(Integer id, String ethnicity) {
        this.id = id;
        this.ethnicity = ethnicity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    /**
     * @return the customerVendormasterList
     */
    public List<VendorMaster> getCustomerVendormasterList() {
        return customerVendormasterList;
    }

    /**
     * @param customerVendormasterList
     *            the customerVendormasterList to set
     */
    public void setCustomerVendormasterList(
            List<VendorMaster> customerVendormasterList) {
        this.customerVendormasterList = customerVendormasterList;
    }

    /**
     * @return the customerTier2reportindirectexpensesList
     */
    public List<Tier2ReportIndirectExpenses> getCustomerTier2reportindirectexpensesList() {
        return customerTier2reportindirectexpensesList;
    }

    /**
     * @param customerTier2reportindirectexpensesList
     *            the customerTier2reportindirectexpensesList to set
     */
    public void setCustomerTier2reportindirectexpensesList(
            List<Tier2ReportIndirectExpenses> customerTier2reportindirectexpensesList) {
        this.customerTier2reportindirectexpensesList = customerTier2reportindirectexpensesList;
    }

    /**
     * @return the createdBy
     */
    public int getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public int getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(int modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
	 * @return the isactive
	 */
	public short getIsactive() {
		return isactive;
	}

	/**
	 * @param isactive the isactive to set
	 */
	public void setIsactive(short isactive) {
		this.isactive = isactive;
	}

	@Override
    public boolean equals(Object object) {

        if (!(object instanceof Ethnicity)) {
            return false;
        }
        Ethnicity other = (Ethnicity) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.rest.Ethnicity[ id=" + id + " ]";
    }

}
