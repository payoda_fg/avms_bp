package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author pirabu
 */
@Entity
@Table(name = "customer_tier2reportmaster")
public class Tier2ReportMaster implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "Tier2ReportMasterid")
	private Integer tier2ReportMasterid;
	@Column(name = "ReportingPeriod")
	private String reportingPeriod;
	@Column(name = "ReportingStartDate")
	@Temporal(TemporalType.DATE)
	private Date reportingStartDate;
	@Column(name = "ReportingEndDate")
	@Temporal(TemporalType.DATE)
	private Date reportingEndDate;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Column(name = "TotalSales")
	private Double totalSales;
	@Column(name = "TotalSalesToCompany")
	private Double totalSalesToCompany;
	@Column(name = "IndirectAllocationPercentage")
	private Float indirectAllocationPercentage;
	@Column(name = "IsSubmitted")
	private Integer isSubmitted;
	@Column(name = "CreatedBy")
	private Integer createdBy;
	@Column(name = "CreatedOn")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	@Column(name = "ModifiedBy")
	private Integer modifiedBy;
	@Column(name = "ModifiedOn")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;
	@OneToMany(mappedBy = "tier2ReoprtMasterid")
	private List<Tier2ReportIndirectExpenses> tier2ReportIndirectExpensesList;
	@OneToMany(mappedBy = "tier2ReoprtMasterid")
	private List<Tier2ReportDirectExpenses> tier2ReportDirectExpensesList;

	@JoinColumn(name = "vendorId", referencedColumnName = "ID")
	@ManyToOne
	private VendorMaster vendorId;

	@Column(name = "COMMENTS")
	private String comments;
	
	@Column(name = "FILENAME")
	private String fileName;
	
	@Column(name = "FILEPATH")
	private String filePath;
	
	public Tier2ReportMaster() {
	}

	public Tier2ReportMaster(Integer tier2ReportMasterid) {
		this.tier2ReportMasterid = tier2ReportMasterid;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getTier2ReportMasterid() {
		return tier2ReportMasterid;
	}

	public void setTier2ReportMasterid(Integer tier2ReportMasterid) {
		this.tier2ReportMasterid = tier2ReportMasterid;
	}

	public String getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(String reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	public Date getReportingStartDate() {
		return reportingStartDate;
	}

	public void setReportingStartDate(Date reportingStartDate) {
		this.reportingStartDate = reportingStartDate;
	}

	public Date getReportingEndDate() {
		return reportingEndDate;
	}

	public void setReportingEndDate(Date reportingEndDate) {
		this.reportingEndDate = reportingEndDate;
	}

	public Double getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}

	public Double getTotalSalesToCompany() {
		return totalSalesToCompany;
	}

	public void setTotalSalesToCompany(Double totalSalesToCompany) {
		this.totalSalesToCompany = totalSalesToCompany;
	}

	public Float getIndirectAllocationPercentage() {
		return indirectAllocationPercentage;
	}

	public void setIndirectAllocationPercentage(
			Float indirectAllocationPercentage) {
		this.indirectAllocationPercentage = indirectAllocationPercentage;
	}

	/**
	 * @return the isSubmitted
	 */
	public Integer getIsSubmitted() {
		return isSubmitted;
	}

	/**
	 * @param isSubmitted
	 *            the isSubmitted to set
	 */
	public void setIsSubmitted(Integer isSubmitted) {
		this.isSubmitted = isSubmitted;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public List<Tier2ReportIndirectExpenses> getTier2ReportIndirectExpensesList() {
		return tier2ReportIndirectExpensesList;
	}

	public void setTier2ReportIndirectExpensesList(
			List<Tier2ReportIndirectExpenses> tier2ReportIndirectExpensesList) {
		this.tier2ReportIndirectExpensesList = tier2ReportIndirectExpensesList;
	}

	public List<Tier2ReportDirectExpenses> getTier2ReportDirectExpensesList() {
		return tier2ReportDirectExpensesList;
	}

	public void setTier2ReportDirectExpensesList(
			List<Tier2ReportDirectExpenses> tier2ReportDirectExpensesList) {
		this.tier2ReportDirectExpensesList = tier2ReportDirectExpensesList;
	}

	public VendorMaster getVendorId() {
		return vendorId;
	}

	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (tier2ReportMasterid != null ? tier2ReportMasterid.hashCode()
				: 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Tier2ReportMaster)) {
			return false;
		}
		Tier2ReportMaster other = (Tier2ReportMaster) object;
		if ((this.tier2ReportMasterid == null && other.tier2ReportMasterid != null)
				|| (this.tier2ReportMasterid != null && !this.tier2ReportMasterid
						.equals(other.tier2ReportMasterid))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sam.Tier2ReportMaster[ tier2ReportMasterid="
				+ tier2ReportMasterid + " ]";
	}

}
