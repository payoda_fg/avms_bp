/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Model class represents configuring dashboard settings.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_dashboardsettings")
public class DashboardSettings implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Items which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DASHBOARDID")
	private Integer dashboardId;

	@Column(name = "REPORT_NAME")
	private String reportName;

	@Column(name = "DASHBOARD_ACTIVE")
	private String isDashboard;

	/**
	 * @return the dashboardId
	 */
	public Integer getDashboardId() {
		return dashboardId;
	}

	/**
	 * @param dashboardId
	 *            the dashboardId to set
	 */
	public void setDashboardId(Integer dashboardId) {
		this.dashboardId = dashboardId;
	}

	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName
	 *            the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @return the isDashboard
	 */
	public String getIsDashboard() {
		return isDashboard;
	}

	/**
	 * @param isDashboard
	 *            the isDashboard to set
	 */
	public void setIsDashboard(String isDashboard) {
		this.isDashboard = isDashboard;
	}

}
