package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "customer_vendornetworld_certificate")
public class CustomerVendornetworldCertificate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Auto generated ID for the VendorOtherCertificate which serves as the
	 * primary key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "VENDORID", nullable = false)
	private VendorMaster vendorId;
	
	@Column(name = "HASNETWORLDCERTIFICATE", nullable = false)
	private String hasNetWorldCertificate;
	
	@Column(name = "ORGINALFILE", nullable = true)
	private String orginalFile ;
	
	@Column(name = "FILESTOREDPATH", nullable = true)
	private String fileStoredPath ;
	
	@Column(name = "FILENAME", nullable = true)
	private String fileName ;
	
	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public VendorMaster getVendorId() {
		return vendorId;
	}

	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	public String getHasNetWorldCertificate() {
		return hasNetWorldCertificate;
	}

	public void setHasNetWorldCertificate(String hasNetWorldCertificate) {
		this.hasNetWorldCertificate = hasNetWorldCertificate;
	}

	public String getOrginalFile() {
		return orginalFile;
	}

	public void setOrginalFile(String orginalFile) {
		this.orginalFile = orginalFile;
	}

	public String getFileStoredPath() {
		return fileStoredPath;
	}

	public void setFileStoredPath(String fileStoredPath) {
		this.fileStoredPath = fileStoredPath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
