/*
 * CustomerIndirectSpendDataDetail.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents the indirect spend details (eg..spend amount, diverse certificate,
 * proRatedAmount etc.) for communicate with DB.
 * 
 * @author pirabu
 */
@Entity
@Table(name = "CUSTOMER_INDIRECT_SPEND_DATA_DETAIL")
public class CustomerIndirectSpendDataDetail implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the CustomerIndirectSpendDataDetail which serves as
     * the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SPENDDATAMASTERID")
    private SpendDataMaster spendUpload;

    @Column(name = "DIVERSECERTIFICATE")
    private String diverseCertificate;

    @ManyToOne
    @JoinColumn(name = "VENDORCERTIFICATEID")
    private VendorCertificate vendorCertificate;

    @Column(name = "INDIRECTSPENDVALUE")
    private String indirectSpendValue;

    @Column(name = "PRORATEAMTBASEDONSUPPLY")
    private String proRateAmtbasedonSupply;

    @Column(name = "INDIRECTAMTBASEDONDIRECTSALES")
    private String indirectAmtbasedonDirectSales;

    @Column(name = "KEYVALUE")
    private Integer keyValue;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the spendUpload
     */
    public SpendDataMaster getSpendUpload() {
	return spendUpload;
    }

    /**
     * @param spendUpload
     *            the spendUpload to set
     */
    public void setSpendUpload(SpendDataMaster spendUpload) {
	this.spendUpload = spendUpload;
    }

    /**
     * @return the diverseCertificate
     */
    public String getDiverseCertificate() {
	return diverseCertificate;
    }

    /**
     * @param diverseCertificate
     *            the diverseCertificate to set
     */
    public void setDiverseCertificate(String diverseCertificate) {
	this.diverseCertificate = diverseCertificate;
    }

    /**
     * @return the vendorCertificate
     */
    public VendorCertificate getVendorCertificate() {
	return vendorCertificate;
    }

    /**
     * @param vendorCertificate
     *            the vendorCertificate to set
     */
    public void setVendorCertificate(VendorCertificate vendorCertificate) {
	this.vendorCertificate = vendorCertificate;
    }

    /**
     * @return the indirectSpendValue
     */
    public String getIndirectSpendValue() {
	return indirectSpendValue;
    }

    /**
     * @param indirectSpendValue
     *            the indirectSpendValue to set
     */
    public void setIndirectSpendValue(String indirectSpendValue) {
	this.indirectSpendValue = indirectSpendValue;
    }

    /**
     * @return the proRateAmtbasedonSupply
     */
    public String getProRateAmtbasedonSupply() {
	return proRateAmtbasedonSupply;
    }

    /**
     * @param proRateAmtbasedonSupply
     *            the proRateAmtbasedonSupply to set
     */
    public void setProRateAmtbasedonSupply(String proRateAmtbasedonSupply) {
	this.proRateAmtbasedonSupply = proRateAmtbasedonSupply;
    }

    /**
     * @return the indirectAmtbasedonDirectSales
     */
    public String getIndirectAmtbasedonDirectSales() {
	return indirectAmtbasedonDirectSales;
    }

    /**
     * @param indirectAmtbasedonDirectSales
     *            the indirectAmtbasedonDirectSales to set
     */
    public void setIndirectAmtbasedonDirectSales(
	    String indirectAmtbasedonDirectSales) {
	this.indirectAmtbasedonDirectSales = indirectAmtbasedonDirectSales;
    }

    /**
     * @return the keyValue
     */
    public Integer getKeyValue() {
	return keyValue;
    }

    /**
     * @param keyValue
     *            the keyValue to set
     */
    public void setKeyValue(Integer keyValue) {
	this.keyValue = keyValue;
    }

}
