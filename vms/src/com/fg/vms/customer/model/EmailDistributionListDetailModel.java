/*
 * EmailDistributionListDetailModel.java 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fg.vms.admin.model.Users;

/**
 * Model Class represents the email distribution list details (such as
 * EmailDistributionMaster, UserID) for communicate with DB.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_emaildistributionlistdetail")
public class EmailDistributionListDetailModel implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Items which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EMAILDISTRIBUTIONLISTDETAILID")
	private Integer emailDistributionListDetailId;

	@ManyToOne
	@JoinColumn(name = "EMAILDISTRIBUTIONMASTERID")
	private EmailDistributionModel emailDistributionMasterId;

	@ManyToOne
	@JoinColumn(name = "USERID")
	private Users userId;

	@ManyToOne
	@JoinColumn(name = "FK_VENDORID")
	private VendorContact vendorId;

	@Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "ISTO")
	private Byte isTo;

	@Column(name = "ISCC")
	private Byte isCC;

	@Column(name = "ISBCC")
	private Byte isBCC;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	/**
	 * @return the emailDistributionListDetailId
	 */
	public Integer getEmailDistributionListDetailId() {
		return emailDistributionListDetailId;
	}

	/**
	 * @param emailDistributionListDetailId
	 *            the emailDistributionListDetailId to set
	 */
	public void setEmailDistributionListDetailId(
			Integer emailDistributionListDetailId) {
		this.emailDistributionListDetailId = emailDistributionListDetailId;
	}

	/**
	 * @return the emailDistributionMasterId
	 */
	public EmailDistributionModel getEmailDistributionMasterId() {
		return emailDistributionMasterId;
	}

	/**
	 * @param emailDistributionMasterId
	 *            the emailDistributionMasterId to set
	 */
	public void setEmailDistributionMasterId(
			EmailDistributionModel emailDistributionMasterId) {
		this.emailDistributionMasterId = emailDistributionMasterId;
	}

	/**
	 * @return the userId
	 */
	public Users getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Users userId) {
		this.userId = userId;
	}

	/**
	 * @return the vendorId
	 */
	public VendorContact getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorContact vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Byte getIsTo() {
		return isTo;
	}

	public void setIsTo(Byte isTo) {
		this.isTo = isTo;
	}

	public Byte getIsCC() {
		return isCC;
	}

	public void setIsCC(Byte isCC) {
		this.isCC = isCC;
	}

	public Byte getIsBCC() {
		return isBCC;
	}

	public void setIsBCC(Byte isBCC) {
		this.isBCC = isBCC;
	}

}
