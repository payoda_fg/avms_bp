/*
 * VendorEmailNotificationTemplateDetail.java 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Model class represents the Vendor email notification template details (eg.
 * vendorEmailMasterNotificationId, templateQuestionsId) for communicate with DB
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "vendor_email_notification_template_detail")
public class VendorEmailNotificationTemplateDetail implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the Certificate which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "VENDOREMAILNOTIFICATIONMASTERID", nullable = false)
    private VendorEmailNotificationMaster vendorEmailNotificationMasterID;

    @ManyToOne
    @JoinColumn(name = "TEMPLATEQUESTIONSID", nullable = false)
    private TemplateQuestions templateQuestionsId;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the vendorEmailNotificationMasterID
     */
    public VendorEmailNotificationMaster getVendorEmailNotificationMasterID() {
	return vendorEmailNotificationMasterID;
    }

    /**
     * @param vendorEmailNotificationMasterID
     *            the vendorEmailNotificationMasterID to set
     */
    public void setVendorEmailNotificationMasterID(
	    VendorEmailNotificationMaster vendorEmailNotificationMasterID) {
	this.vendorEmailNotificationMasterID = vendorEmailNotificationMasterID;
    }

    /**
     * @return the templateQuestionsId
     */
    public TemplateQuestions getTemplateQuestionsId() {
	return templateQuestionsId;
    }

    /**
     * @param templateQuestionsId
     *            the templateQuestionsId to set
     */
    public void setTemplateQuestionsId(TemplateQuestions templateQuestionsId) {
	this.templateQuestionsId = templateQuestionsId;
    }

}
