/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "passwordhistory")
public class PasswordHistory implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "CUSTOMERUSERID")
	private Integer customerUserID;
	@Column(name = "VENDORUSERID")
	private Integer vendorUserId;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "KEYVALUE")
	private Integer keyvalue;
	@Column(name = "CHANGEDBY")
	private Integer changedby;
	@Column(name = "CHANGEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date changedon;

	public PasswordHistory() {
	}

	public PasswordHistory(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerUserID() {
		return customerUserID;
	}

	public void setCustomerUserID(Integer customerUserID) {
		this.customerUserID = customerUserID;
	}

	public Integer getVendorUserId() {
		return vendorUserId;
	}

	public void setVendorUserId(Integer vendorUserId) {
		this.vendorUserId = vendorUserId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getKeyvalue() {
		return keyvalue;
	}

	public void setKeyvalue(Integer keyvalue) {
		this.keyvalue = keyvalue;
	}

	public Integer getChangedby() {
		return changedby;
	}

	public void setChangedby(Integer changedby) {
		this.changedby = changedby;
	}

	public Date getChangedon() {
		return changedon;
	}

	public void setChangedon(Date changedon) {
		this.changedon = changedon;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof PasswordHistory)) {
			return false;
		}
		PasswordHistory other = (PasswordHistory) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return " com.fg.vms.customer.model.PasswordHistory[ id=" + id + " ]";
	}

}
