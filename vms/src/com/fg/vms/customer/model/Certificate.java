/**
 * Certificate.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents the certificate details (eg. certificateName,
 * certificateShortName, Diverse Quality) for communicate with DB
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_certificatemaster")
public class Certificate implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Certificate which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "CERTIFICATENAME", nullable = false)
	private String certificateName;

	@Column(name = "CERTIFICATESHORTNAME", nullable = true)
	private String certificateShortName;

	@Column(name = "DIVERSE_QUALITY", nullable = false)
	private Byte diverseQuality;

	@Column(name = "ISACTIVE", columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;
	
	@Column(name = "ISETHINICITY", columnDefinition = "TINYINT(1) DEFAULT 0")
	private Byte isEthinicity;

	@Column(name = "CERTIFICATEUPLOAD", columnDefinition = "TINYINT(1) DEFAULT 0")
	private Byte certificateUpload;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the certificateName
	 */
	public String getCertificateName() {
		return certificateName;
	}

	/**
	 * @param certificateName
	 *            the certificateName to set
	 */
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	/**
	 * @return the certificateShortName
	 */
	public String getCertificateShortName() {
		return certificateShortName;
	}

	/**
	 * @param certificateShortName
	 *            the certificateShortName to set
	 */
	public void setCertificateShortName(String certificateShortName) {
		this.certificateShortName = certificateShortName;
	}

	/**
	 * @return the diverseQuality
	 */
	public Byte getDiverseQuality() {
		return diverseQuality;
	}

	/**
	 * @param diverseQuality
	 *            the diverseQuality to set
	 */
	public void setDiverseQuality(Byte diverseQuality) {
		this.diverseQuality = diverseQuality;
	}

	/**
	 * @return the diverseQuality
	 */
	/*
	 * public boolean isDiverseQuality() { return diverseQuality; }
	 *//**
	 * @param diverseQuality
	 *            the diverseQuality to set
	 */
	/*
	 * public void setDiverseQuality(boolean diverseQuality) {
	 * this.diverseQuality = diverseQuality; }
	 */
	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	public Byte getIsEthinicity() {
		return isEthinicity;
	}

	public void setIsEthinicity(Byte isEthinicity) {
		this.isEthinicity = isEthinicity;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Byte getCertificateUpload() {
		return certificateUpload;
	}

	public void setCertificateUpload(Byte certificateUpload) {
		this.certificateUpload = certificateUpload;
	}

}
