/**
 * CustomerCertificateType.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class for certificate type.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_certificatetype")
public class CustomerCertificateType implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the customer certificate type which serves as the
	 * primary key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "DESCRIPTION", nullable = false)
	private String certificateTypeDesc;

	@Column(name = "SHORTDESCRIPTION", nullable = false)
	private String shortDescription;

	@Column(name = "ISACTIVE", columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;
	
	@Column(name = "ISCERTIFICATEEXPIRYALERTREQUIRED", columnDefinition = "TINYINT(4) DEFAULT 0")
	private Byte isCertificateExpiryAlertRequired;

	public CustomerCertificateType() {
	}

	public CustomerCertificateType(Integer id, String certificateTypeDesc,
			String shortDescription, Byte isActive) {
		super();
		this.id = id;
		this.certificateTypeDesc = certificateTypeDesc;
		this.isActive = isActive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCertificateTypeDesc() {
		return certificateTypeDesc;
	}

	public void setCertificateTypeDesc(String certificateTypeDesc) {
		this.certificateTypeDesc = certificateTypeDesc;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public Byte getIsActive() {
		return isActive;
	}

	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Byte getIsCertificateExpiryAlertRequired() {
		return isCertificateExpiryAlertRequired;
	}

	public void setIsCertificateExpiryAlertRequired(
			Byte isCertificateExpiryAlertRequired) {
		this.isCertificateExpiryAlertRequired = isCertificateExpiryAlertRequired;
	}
}