/**
 * CustomerVendorStatusLog.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fg.vms.admin.model.Users;

/**
 * @author vinoth
 * 
 */
@Entity
@Table(name = "vendor_status_log")
public class CustomerVendorStatusLog implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the VendorMaster which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "VENDORID", nullable = false)
    private VendorMaster vendorId;

    @Column(name = "STATUS")
    private String vendorStatus;

    @Column(name = "DESCRIPTION")
    private String statusDescription;

    @ManyToOne
    @JoinColumn(name = "CREATEDBY", nullable = false)
    private Users createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the vendorId
     */
    public VendorMaster getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId
     *            the vendorId to set
     */
    public void setVendorId(VendorMaster vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * @return the vendorStatus
     */
    public String getVendorStatus() {
        return vendorStatus;
    }

    /**
     * @param vendorStatus
     *            the vendorStatus to set
     */
    public void setVendorStatus(String vendorStatus) {
        this.vendorStatus = vendorStatus;
    }

    /**
     * @return the createdBy
     */
    public Users getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Users createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

}
