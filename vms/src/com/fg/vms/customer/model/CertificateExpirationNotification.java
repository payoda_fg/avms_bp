/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "certificateexpirationnotification")
public class CertificateExpirationNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "VENDORID")
	private Integer vendorid;

	@Column(name = "EMAILID")
	private String emailId;

	@Column(name = "CERTIFICATENUMBER")
	private String certificatenumber;

	@Column(name = "AGENCYNAME")
	private String agencyname;

	@Column(name = "CERTIFICATENAME")
	private String certificateName;

	@Column(name = "VENDORCERTIFICATEID")
	private Integer vendorCertificateId;

	@Column(name = "EXPIRATIONDATE")
	@Temporal(TemporalType.DATE)
	private Date expirationdate;

	@Column(name = "EMAILDATE")
	@Temporal(TemporalType.DATE)
	private Date emaildate;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "SUBMITTEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date submittedon;

	@Column(name = "SUBMITTEDBY")	
	private Integer submittedby;

	public CertificateExpirationNotification() {
	}

	public CertificateExpirationNotification(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVendorid() {
		return vendorid;
	}

	public void setVendorid(Integer vendorid) {
		this.vendorid = vendorid;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCertificatenumber() {
		return certificatenumber;
	}

	public void setCertificatenumber(String certificatenumber) {
		this.certificatenumber = certificatenumber;
	}

	public String getAgencyname() {
		return agencyname;
	}

	public void setAgencyname(String agencyname) {
		this.agencyname = agencyname;
	}

	public Date getExpirationdate() {
		return expirationdate;
	}

	public void setExpirationdate(Date expirationdate) {
		this.expirationdate = expirationdate;
	}

	public Date getEmaildate() {
		return emaildate;
	}

	public void setEmaildate(Date emaildate) {
		this.emaildate = emaildate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getSubmittedon() {
		return submittedon;
	}

	public void setSubmittedon(Date submittedon) {
		this.submittedon = submittedon;
	}

	public Integer getSubmittedby() {
		return submittedby;
	}

	public void setSubmittedby(Integer submittedby) {
		this.submittedby = submittedby;
	}

	public Integer getVendorCertificateId() {
		return vendorCertificateId;
	}

	public void setVendorCertificateId(Integer vendorCertificateId) {
		this.vendorCertificateId = vendorCertificateId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof CertificateExpirationNotification)) {
			return false;
		}
		CertificateExpirationNotification other = (CertificateExpirationNotification) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.fg.vms.customer.model.CertificateExpirationNotification[ id="
				+ id + " ]";
	}

}
