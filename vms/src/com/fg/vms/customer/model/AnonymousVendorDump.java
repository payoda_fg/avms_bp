/**
 * AnonymousVendorDump.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fg.vms.admin.model.Users;

/**
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_anonymous_vendormaster_dump")
public class AnonymousVendorDump implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the AnonymousVendor which serves as the primary
	 * key.
	 */
	@Id
	@GeneratedValue
	@Column(name = "ID", nullable = false, unique = true)
	private Integer id;

	@Column(name = "VENDORNAME")
	private String vendorName;

	@Column(name = "DUNSNUMBER")
	private String dunsNumber;

	@Column(name = "TAXID")
	private String taxId;

	@Column(name = "COMPANY_TYPE")
	private String companyType;

	@Column(name = "ADDRESS1")
	private String address1;

	@Column(name = "ADDRESS2")
	private String address2;

	@Column(name = "ADDRESS3")
	private String address3;

	@Column(name = "CITY")
	private String city;

	@Column(name = "STATE")
	private String state;

	@Column(name = "PROVINCE")
	private String province;

	@Column(name = "ZIPCODE")
	private String zipCode;

	@Column(name = "REGION")
	private String region;

	@Column(name = "COUNTRY")
	private String country;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "MOBILE")
	private String mobile;

	@Column(name = "FAX")
	private String fax;

	@Column(name = "EMAILID")
	private String emailId;

	@Column(name = "WEBSITE")
	private String website;

	@Column(name = "NUMBEROFEMPLOYEES")
	private int numberOfEmployees;

	@Column(name = "ANNUALTURNOVER")
	private double annualTurnover;

	@Column(name = "YEAROFESTABLISHMENT")
	private int yearOfEstablishment;

	@Column(name = "PARENTVENDORID")
	private int parentVendorId;

	@Column(name = "ISAPPROVED")
	private Byte isApproved;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "APPROVEDBY")
	private Users approvedBy;

	@Column(name = "APPROVEDON")
	private Date approvedOn;

	@Column(name = "ISACTIVE", columnDefinition = "TINYINT(1) DEFAULT 1", nullable = false)
	private Byte isActive;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@Column(name = "EXPIRYDATE")
	private Date expiryDate;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CERTAGENCYID")
	private CertifyingAgency certAgencyId;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CERTMASTERID")
	private Certificate certMasterId;

	@Column(name = "DIVERSESUPPLIER", columnDefinition = "TINYINT(4) DEFAULT 0")
	private Byte diverseSupplier;

	@Column(name = "FILENAME")
	private String filename;

	@Column(name = "CONTENT")
	@Lob
	private Blob content;

	@Column(name = "CONTENT_TYPE")
	private String contentType;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "LASTNAME")
	private String lastName;

	@Column(name = "DESIGNATION")
	private String designation;

	@Column(name = "PHONENUMBER")
	private String phoneNumber;

	@Column(name = "MOBILENUMBER")
	private String mobileNumber;

	@Column(name = "FAXNUMBER")
	private String faxNumber;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "NAICSCODE")
	private String naicsCode;

	@Column(name = "NAICSDESCRIPTION")
	private String naicsDescription;

	@Column(name = "ISICCODE")
	private String isicCode;

	@Column(name = "ISICDESCRIPTION")
	private String isicDescription;

	@Column(name = "REFERENCENAME1")
	private String referenceName1;

	@Column(name = "REFERENCEADDRESS1")
	private String referenceAddress1;

	@Column(name = "REFERENCEPHONE1")
	private String referencePhone1;

	@Column(name = "REFERENCEEMAILID1")
	private String referenceMailId1;

	@Column(name = "REFERENCEMOBILE1")
	private String referenceMobile1;

	@Column(name = "REFERENCECITY1")
	private String referenceCity1;

	@Column(name = "REFERENCEZIP1")
	private String referenceZip1;

	@Column(name = "REFERENCESTATE1")
	private String referenceState1;

	@Column(name = "REFERENCECOUNTRY1")
	private String referenceCountry1;

	@Column(name = "REFERENCENAME2")
	private String referenceName2;

	@Column(name = "REFERENCEADDRESS2")
	private String referenceAddress2;

	@Column(name = "REFERENCEPHONE2")
	private String referencePhone2;

	@Column(name = "REFERENCEEMAILID2")
	private String referenceMailId2;

	@Column(name = "REFERENCEMOBILE2")
	private String referenceMobile2;

	@Column(name = "REFERENCECITY2")
	private String referenceCity2;

	@Column(name = "REFERENCEZIP2")
	private String referenceZip2;

	@Column(name = "REFERENCESTATE2")
	private String referenceState2;

	@Column(name = "REFERENCECOUNTRY2")
	private String referenceCountry2;

	@Column(name = "REFERENCENAME3")
	private String referenceName3;

	@Column(name = "REFERENCEADDRESS3")
	private String referenceAddress3;

	@Column(name = "REFERENCEPHONE3")
	private String referencePhone3;

	@Column(name = "REFERENCEEMAILID3")
	private String referenceMailId3;

	@Column(name = "REFERENCEMOBILE3")
	private String referenceMobile3;

	@Column(name = "REFERENCECITY3")
	private String referenceCity3;

	@Column(name = "REFERENCEZIP3")
	private String referenceZip3;

	@Column(name = "REFERENCESTATE3")
	private String referenceState3;

	@Column(name = "REFERENCECOUNTRY3")
	private String referenceCountry3;

	@Column(name = "CAPABILITIES")
	private String capabilities;

	@JoinColumn(name = "ETHNICITY_ID", referencedColumnName = "id")
	@ManyToOne
	private Ethnicity ethnicityId;

	@Column(name = "MINORITY_OWNERSHIP")
	private Double minorityPercent;

	@Column(name = "WOMEN_OWNERSHIP")
	private Double womenPercent;

	@Column(name = "DIVERSENOTES")
	private String diverseNotes;

	@Column(name = "CERTIFICATENUMBER")
	private String certificateNumber;

	@Column(name = "EFFECTIVEDATE")
	private Date effectiveDate;

	@Column(name = "DIVERSE_CLASSIFICATION")
	private Integer diverseClassification;

	@Column(name = "PASSWORD")
	private String password;

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the dunsNumber
	 */
	public String getDunsNumber() {
		return dunsNumber;
	}

	/**
	 * @param dunsNumber
	 *            the dunsNumber to set
	 */
	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	/**
	 * @return the taxId
	 */
	public String getTaxId() {
		return taxId;
	}

	/**
	 * @param taxId
	 *            the taxId to set
	 */
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	/**
	 * @return the companyType
	 */
	public String getCompanyType() {
		return companyType;
	}

	/**
	 * @param companyType
	 *            the companyType to set
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * @param address3
	 *            the address3 to set
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * @param website
	 *            the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * @return the numberOfEmployees
	 */
	public int getNumberOfEmployees() {
		return numberOfEmployees;
	}

	/**
	 * @param numberOfEmployees
	 *            the numberOfEmployees to set
	 */
	public void setNumberOfEmployees(int numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	/**
	 * @return the annualTurnover
	 */
	public double getAnnualTurnover() {
		return annualTurnover;
	}

	/**
	 * @param annualTurnover
	 *            the annualTurnover to set
	 */
	public void setAnnualTurnover(double annualTurnover) {
		this.annualTurnover = annualTurnover;
	}

	/**
	 * @return the yearOfEstablishment
	 */
	public int getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	/**
	 * @param yearOfEstablishment
	 *            the yearOfEstablishment to set
	 */
	public void setYearOfEstablishment(int yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	/**
	 * @return the parentVendorId
	 */
	public int getParentVendorId() {
		return parentVendorId;
	}

	/**
	 * @param parentVendorId
	 *            the parentVendorId to set
	 */
	public void setParentVendorId(int parentVendorId) {
		this.parentVendorId = parentVendorId;
	}

	/**
	 * @return the isApproved
	 */
	public Byte getIsApproved() {
		return isApproved;
	}

	/**
	 * @param isApproved
	 *            the isApproved to set
	 */
	public void setIsApproved(Byte isApproved) {
		this.isApproved = isApproved;
	}

	/**
	 * @return the approvedBy
	 */
	public Users getApprovedBy() {
		return approvedBy;
	}

	/**
	 * @param approvedBy
	 *            the approvedBy to set
	 */
	public void setApprovedBy(Users approvedBy) {
		this.approvedBy = approvedBy;
	}

	/**
	 * @return the approvedOn
	 */
	public Date getApprovedOn() {
		return approvedOn;
	}

	/**
	 * @param approvedOn
	 *            the approvedOn to set
	 */
	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the certAgencyId
	 */
	public CertifyingAgency getCertAgencyId() {
		return certAgencyId;
	}

	/**
	 * @param certAgencyId
	 *            the certAgencyId to set
	 */
	public void setCertAgencyId(CertifyingAgency certAgencyId) {
		this.certAgencyId = certAgencyId;
	}

	/**
	 * @return the certMasterId
	 */
	public Certificate getCertMasterId() {
		return certMasterId;
	}

	/**
	 * @param certMasterId
	 *            the certMasterId to set
	 */
	public void setCertMasterId(Certificate certMasterId) {
		this.certMasterId = certMasterId;
	}

	/**
	 * @return the diverseSupplier
	 */
	public Byte getDiverseSupplier() {
		return diverseSupplier;
	}

	/**
	 * @param diverseSupplier
	 *            the diverseSupplier to set
	 */
	public void setDiverseSupplier(Byte diverseSupplier) {
		this.diverseSupplier = diverseSupplier;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename
	 *            the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the content
	 */
	public Blob getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(Blob content) {
		this.content = content;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation
	 *            the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber
	 *            the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the naicsCode
	 */
	public String getNaicsCode() {
		return naicsCode;
	}

	/**
	 * @param naicsCode
	 *            the naicsCode to set
	 */
	public void setNaicsCode(String naicsCode) {
		this.naicsCode = naicsCode;
	}

	/**
	 * @return the naicsDescription
	 */
	public String getNaicsDescription() {
		return naicsDescription;
	}

	/**
	 * @param naicsDescription
	 *            the naicsDescription to set
	 */
	public void setNaicsDescription(String naicsDescription) {
		this.naicsDescription = naicsDescription;
	}

	/**
	 * @return the isicCode
	 */
	public String getIsicCode() {
		return isicCode;
	}

	/**
	 * @param isicCode
	 *            the isicCode to set
	 */
	public void setIsicCode(String isicCode) {
		this.isicCode = isicCode;
	}

	/**
	 * @return the isicDescription
	 */
	public String getIsicDescription() {
		return isicDescription;
	}

	/**
	 * @param isicDescription
	 *            the isicDescription to set
	 */
	public void setIsicDescription(String isicDescription) {
		this.isicDescription = isicDescription;
	}

	/**
	 * @return the referenceName1
	 */
	public String getReferenceName1() {
		return referenceName1;
	}

	/**
	 * @param referenceName1
	 *            the referenceName1 to set
	 */
	public void setReferenceName1(String referenceName1) {
		this.referenceName1 = referenceName1;
	}

	/**
	 * @return the referenceAddress1
	 */
	public String getReferenceAddress1() {
		return referenceAddress1;
	}

	/**
	 * @param referenceAddress1
	 *            the referenceAddress1 to set
	 */
	public void setReferenceAddress1(String referenceAddress1) {
		this.referenceAddress1 = referenceAddress1;
	}

	/**
	 * @return the referencePhone1
	 */
	public String getReferencePhone1() {
		return referencePhone1;
	}

	/**
	 * @param referencePhone1
	 *            the referencePhone1 to set
	 */
	public void setReferencePhone1(String referencePhone1) {
		this.referencePhone1 = referencePhone1;
	}

	/**
	 * @return the referenceMailId1
	 */
	public String getReferenceMailId1() {
		return referenceMailId1;
	}

	/**
	 * @param referenceMailId1
	 *            the referenceMailId1 to set
	 */
	public void setReferenceMailId1(String referenceMailId1) {
		this.referenceMailId1 = referenceMailId1;
	}

	/**
	 * @return the referenceMobile1
	 */
	public String getReferenceMobile1() {
		return referenceMobile1;
	}

	/**
	 * @param referenceMobile1
	 *            the referenceMobile1 to set
	 */
	public void setReferenceMobile1(String referenceMobile1) {
		this.referenceMobile1 = referenceMobile1;
	}

	/**
	 * @return the referenceCity1
	 */
	public String getReferenceCity1() {
		return referenceCity1;
	}

	/**
	 * @param referenceCity1
	 *            the referenceCity1 to set
	 */
	public void setReferenceCity1(String referenceCity1) {
		this.referenceCity1 = referenceCity1;
	}

	/**
	 * @return the referenceZip1
	 */
	public String getReferenceZip1() {
		return referenceZip1;
	}

	/**
	 * @param referenceZip1
	 *            the referenceZip1 to set
	 */
	public void setReferenceZip1(String referenceZip1) {
		this.referenceZip1 = referenceZip1;
	}

	/**
	 * @return the referenceState1
	 */
	public String getReferenceState1() {
		return referenceState1;
	}

	/**
	 * @param referenceState1
	 *            the referenceState1 to set
	 */
	public void setReferenceState1(String referenceState1) {
		this.referenceState1 = referenceState1;
	}

	/**
	 * @return the referenceCountry1
	 */
	public String getReferenceCountry1() {
		return referenceCountry1;
	}

	/**
	 * @param referenceCountry1
	 *            the referenceCountry1 to set
	 */
	public void setReferenceCountry1(String referenceCountry1) {
		this.referenceCountry1 = referenceCountry1;
	}

	/**
	 * @return the referenceName2
	 */
	public String getReferenceName2() {
		return referenceName2;
	}

	/**
	 * @param referenceName2
	 *            the referenceName2 to set
	 */
	public void setReferenceName2(String referenceName2) {
		this.referenceName2 = referenceName2;
	}

	/**
	 * @return the referenceAddress2
	 */
	public String getReferenceAddress2() {
		return referenceAddress2;
	}

	/**
	 * @param referenceAddress2
	 *            the referenceAddress2 to set
	 */
	public void setReferenceAddress2(String referenceAddress2) {
		this.referenceAddress2 = referenceAddress2;
	}

	/**
	 * @return the referencePhone2
	 */
	public String getReferencePhone2() {
		return referencePhone2;
	}

	/**
	 * @param referencePhone2
	 *            the referencePhone2 to set
	 */
	public void setReferencePhone2(String referencePhone2) {
		this.referencePhone2 = referencePhone2;
	}

	/**
	 * @return the referenceMailId2
	 */
	public String getReferenceMailId2() {
		return referenceMailId2;
	}

	/**
	 * @param referenceMailId2
	 *            the referenceMailId2 to set
	 */
	public void setReferenceMailId2(String referenceMailId2) {
		this.referenceMailId2 = referenceMailId2;
	}

	/**
	 * @return the referenceMobile2
	 */
	public String getReferenceMobile2() {
		return referenceMobile2;
	}

	/**
	 * @param referenceMobile2
	 *            the referenceMobile2 to set
	 */
	public void setReferenceMobile2(String referenceMobile2) {
		this.referenceMobile2 = referenceMobile2;
	}

	/**
	 * @return the referenceCity2
	 */
	public String getReferenceCity2() {
		return referenceCity2;
	}

	/**
	 * @param referenceCity2
	 *            the referenceCity2 to set
	 */
	public void setReferenceCity2(String referenceCity2) {
		this.referenceCity2 = referenceCity2;
	}

	/**
	 * @return the referenceZip2
	 */
	public String getReferenceZip2() {
		return referenceZip2;
	}

	/**
	 * @param referenceZip2
	 *            the referenceZip2 to set
	 */
	public void setReferenceZip2(String referenceZip2) {
		this.referenceZip2 = referenceZip2;
	}

	/**
	 * @return the referenceState2
	 */
	public String getReferenceState2() {
		return referenceState2;
	}

	/**
	 * @param referenceState2
	 *            the referenceState2 to set
	 */
	public void setReferenceState2(String referenceState2) {
		this.referenceState2 = referenceState2;
	}

	/**
	 * @return the referenceCountry2
	 */
	public String getReferenceCountry2() {
		return referenceCountry2;
	}

	/**
	 * @param referenceCountry2
	 *            the referenceCountry2 to set
	 */
	public void setReferenceCountry2(String referenceCountry2) {
		this.referenceCountry2 = referenceCountry2;
	}

	/**
	 * @return the referenceName3
	 */
	public String getReferenceName3() {
		return referenceName3;
	}

	/**
	 * @param referenceName3
	 *            the referenceName3 to set
	 */
	public void setReferenceName3(String referenceName3) {
		this.referenceName3 = referenceName3;
	}

	/**
	 * @return the referenceAddress3
	 */
	public String getReferenceAddress3() {
		return referenceAddress3;
	}

	/**
	 * @param referenceAddress3
	 *            the referenceAddress3 to set
	 */
	public void setReferenceAddress3(String referenceAddress3) {
		this.referenceAddress3 = referenceAddress3;
	}

	/**
	 * @return the referencePhone3
	 */
	public String getReferencePhone3() {
		return referencePhone3;
	}

	/**
	 * @param referencePhone3
	 *            the referencePhone3 to set
	 */
	public void setReferencePhone3(String referencePhone3) {
		this.referencePhone3 = referencePhone3;
	}

	/**
	 * @return the referenceMailId3
	 */
	public String getReferenceMailId3() {
		return referenceMailId3;
	}

	/**
	 * @param referenceMailId3
	 *            the referenceMailId3 to set
	 */
	public void setReferenceMailId3(String referenceMailId3) {
		this.referenceMailId3 = referenceMailId3;
	}

	/**
	 * @return the referenceMobile3
	 */
	public String getReferenceMobile3() {
		return referenceMobile3;
	}

	/**
	 * @param referenceMobile3
	 *            the referenceMobile3 to set
	 */
	public void setReferenceMobile3(String referenceMobile3) {
		this.referenceMobile3 = referenceMobile3;
	}

	/**
	 * @return the referenceCity3
	 */
	public String getReferenceCity3() {
		return referenceCity3;
	}

	/**
	 * @param referenceCity3
	 *            the referenceCity3 to set
	 */
	public void setReferenceCity3(String referenceCity3) {
		this.referenceCity3 = referenceCity3;
	}

	/**
	 * @return the referenceZip3
	 */
	public String getReferenceZip3() {
		return referenceZip3;
	}

	/**
	 * @param referenceZip3
	 *            the referenceZip3 to set
	 */
	public void setReferenceZip3(String referenceZip3) {
		this.referenceZip3 = referenceZip3;
	}

	/**
	 * @return the referenceState3
	 */
	public String getReferenceState3() {
		return referenceState3;
	}

	/**
	 * @param referenceState3
	 *            the referenceState3 to set
	 */
	public void setReferenceState3(String referenceState3) {
		this.referenceState3 = referenceState3;
	}

	/**
	 * @return the referenceCountry3
	 */
	public String getReferenceCountry3() {
		return referenceCountry3;
	}

	/**
	 * @param referenceCountry3
	 *            the referenceCountry3 to set
	 */
	public void setReferenceCountry3(String referenceCountry3) {
		this.referenceCountry3 = referenceCountry3;
	}

	/**
	 * @return the capabilities
	 */
	public String getCapabilities() {
		return capabilities;
	}

	/**
	 * @param capabilities
	 *            the capabilities to set
	 */
	public void setCapabilities(String capabilities) {
		this.capabilities = capabilities;
	}

	/**
	 * @return the ethnicityId
	 */
	public Ethnicity getEthnicityId() {
		return ethnicityId;
	}

	/**
	 * @param ethnicityId
	 *            the ethnicityId to set
	 */
	public void setEthnicityId(Ethnicity ethnicityId) {
		this.ethnicityId = ethnicityId;
	}

	/**
	 * @return the minorityPercent
	 */
	public Double getMinorityPercent() {
		return minorityPercent;
	}

	/**
	 * @param minorityPercent
	 *            the minorityPercent to set
	 */
	public void setMinorityPercent(Double minorityPercent) {
		this.minorityPercent = minorityPercent;
	}

	/**
	 * @return the womenPercent
	 */
	public Double getWomenPercent() {
		return womenPercent;
	}

	/**
	 * @param womenPercent
	 *            the womenPercent to set
	 */
	public void setWomenPercent(Double womenPercent) {
		this.womenPercent = womenPercent;
	}

	/**
	 * @return the diverseNotes
	 */
	public String getDiverseNotes() {
		return diverseNotes;
	}

	/**
	 * @param diverseNotes
	 *            the diverseNotes to set
	 */
	public void setDiverseNotes(String diverseNotes) {
		this.diverseNotes = diverseNotes;
	}

	/**
	 * @return the certificateNumber
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * @param certificateNumber
	 *            the certificateNumber to set
	 */
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the diverseClassification
	 */
	public Integer getDiverseClassification() {
		return diverseClassification;
	}

	/**
	 * @param diverseClassification
	 *            the diverseClassification to set
	 */
	public void setDiverseClassification(Integer diverseClassification) {
		this.diverseClassification = diverseClassification;
	}

}
