/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents customer vendor business biography/safety.
 * 
 * @author vinoth
 */
@Entity
@Table(name = "customer_vendorbusinessbiographysafety")
public class CustomerVendorbusinessbiographysafety implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "BUSINESSSAFETY")
	private String businesssafety;

	@Column(name = "QUESTIONNUMBER")
	private Integer questionnumber;

	@Column(name = "SEQUENCE")
	private Integer sequence;

	@Column(name = "ANSWERTYPE")
	private String answertype;

	@Column(name = "ANSWER")
	private String answer;

	@Column(name = "SETDESCRIPTION")
	private String setdescription;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "ISACTIVE")
	private Integer isactive;

	@Column(name = "CREATEDBY")
	private Integer createdby;

	@Column(name = "CREATEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedby;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedon;

	@JoinColumn(name = "VENDORID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private VendorMaster vendorid;

	public CustomerVendorbusinessbiographysafety() {
	}

	public CustomerVendorbusinessbiographysafety(Integer id) {
		this.id = id;
	}

	public CustomerVendorbusinessbiographysafety(Integer id,
			String businesssafety, Integer questionnumber, Integer sequence,
			String answertype, Integer isactive, Integer createdby,
			Date createdon) {
		this.id = id;
		this.businesssafety = businesssafety;
		this.questionnumber = questionnumber;
		this.sequence = sequence;
		this.answertype = answertype;
		this.isactive = isactive;
		this.createdby = createdby;
		this.createdon = createdon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBusinesssafety() {
		return businesssafety;
	}

	public void setBusinesssafety(String businesssafety) {
		this.businesssafety = businesssafety;
	}

	public Integer getQuestionnumber() {
		return questionnumber;
	}

	public void setQuestionnumber(Integer questionnumber) {
		this.questionnumber = questionnumber;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getAnswertype() {
		return answertype;
	}

	public void setAnswertype(String answertype) {
		this.answertype = answertype;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getSetdescription() {
		return setdescription;
	}

	public void setSetdescription(String setdescription) {
		this.setdescription = setdescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Integer getCreatedby() {
		return createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	public VendorMaster getVendorid() {
		return vendorid;
	}

	public void setVendorid(VendorMaster vendorid) {
		this.vendorid = vendorid;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof CustomerVendorbusinessbiographysafety)) {
			return false;
		}
		CustomerVendorbusinessbiographysafety other = (CustomerVendorbusinessbiographysafety) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "sample.pack.CustomerVendorbusinessbiographysafety[ id=" + id
				+ " ]";
	}

}
