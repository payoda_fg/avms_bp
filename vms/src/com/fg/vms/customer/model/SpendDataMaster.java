/**
 * SpendDataMaster.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Model class represents the upload files details
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER_SPENDDATA_UPLOADMASTER")
public class SpendDataMaster implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the SpendDataMaster which serves as the primary
     * key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "NONPRIMEVENDORID")
    private VendorMaster vendor;

    @ManyToOne
    @JoinColumn(name = "PRIMEVENDORID")
    private VendorMaster primeVendor;

    @Column(name = "DATAFORTHEYEAR")
    private Integer yearOfData;

    @Column(name = "SPEND_STARTDATE")
    private Date spendStartDate;

    @Column(name = "SPEND_ENDDATE")
    private Date spendEndDate;

    @Column(name = "DATEOFUPLOAD")
    private Date dateOfUpload;

    @Column(name = "UPLOADEDBY")
    private Integer uploadedBy;

    @Column(name = "ISDATAIMPORT")
    private Byte isDataImport;

    @Lob
    @Column(name = "IMPORTFILECONTENT")
    private Blob importfileContent;

    @Column(name = "totalPrimeSupplierSales")
    private String totalPrimeSupplierSales;

    @Column(name = "totalCustomerSales")
    private String totalCustomerSales;

    @Column(name = "ISAPPROVED")
    private Byte approved;

    @Column(name = "ISAPPROVEDBY")
    private Integer approvedBy;

    @Column(name = "APPROVEDON")
    private Date approvedOn;

    @Column(name = "KEYVALUE")
    private Integer keyValue;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the vendor
     */
    public VendorMaster getVendor() {
	return vendor;
    }

    /**
     * @param vendor
     *            the vendor to set
     */
    public void setVendor(VendorMaster vendor) {
	this.vendor = vendor;
    }

    /**
     * @return the yearOfData
     */
    public Integer getYearOfData() {
	return yearOfData;
    }

    /**
     * @param yearOfData
     *            the yearOfData to set
     */
    public void setYearOfData(Integer yearOfData) {
	this.yearOfData = yearOfData;
    }

    /**
     * @return the spendStartDate
     */
    public Date getSpendStartDate() {
	return spendStartDate;
    }

    /**
     * @param spendStartDate
     *            the spendStartDate to set
     */
    public void setSpendStartDate(Date spendStartDate) {
	this.spendStartDate = spendStartDate;
    }

    /**
     * @return the spendEndDate
     */
    public Date getSpendEndDate() {
	return spendEndDate;
    }

    /**
     * @param spendEndDate
     *            the spendEndDate to set
     */
    public void setSpendEndDate(Date spendEndDate) {
	this.spendEndDate = spendEndDate;
    }

    /**
     * @return the dateOfUpload
     */
    public Date getDateOfUpload() {
	return dateOfUpload;
    }

    /**
     * @param dateOfUpload
     *            the dateOfUpload to set
     */
    public void setDateOfUpload(Date dateOfUpload) {
	this.dateOfUpload = dateOfUpload;
    }

    /**
     * @return the uploadedBy
     */
    public Integer getUploadedBy() {
	return uploadedBy;
    }

    /**
     * @param uploadedBy
     *            the uploadedBy to set
     */
    public void setUploadedBy(Integer uploadedBy) {
	this.uploadedBy = uploadedBy;
    }

    /**
     * @return the isDataImport
     */
    public Byte getIsDataImport() {
	return isDataImport;
    }

    /**
     * @param isDataImport
     *            the isDataImport to set
     */
    public void setIsDataImport(Byte isDataImport) {
	this.isDataImport = isDataImport;
    }

    /**
     * @return the totalPrimeSupplierSales
     */
    public String getTotalPrimeSupplierSales() {
	return totalPrimeSupplierSales;
    }

    /**
     * @param totalPrimeSupplierSales
     *            the totalPrimeSupplierSales to set
     */
    public void setTotalPrimeSupplierSales(String totalPrimeSupplierSales) {
	this.totalPrimeSupplierSales = totalPrimeSupplierSales;
    }

    /**
     * @return the totalCustomerSales
     */
    public String getTotalCustomerSales() {
	return totalCustomerSales;
    }

    /**
     * @param totalCustomerSales
     *            the totalCustomerSales to set
     */
    public void setTotalCustomerSales(String totalCustomerSales) {
	this.totalCustomerSales = totalCustomerSales;
    }

    /**
     * @return the approved
     */
    public Byte getApproved() {
	return approved;
    }

    /**
     * @param approved
     *            the approved to set
     */
    public void setApproved(Byte approved) {
	this.approved = approved;
    }

    /**
     * @return the approvedBy
     */
    public Integer getApprovedBy() {
	return approvedBy;
    }

    /**
     * @param approvedBy
     *            the approvedBy to set
     */
    public void setApprovedBy(Integer approvedBy) {
	this.approvedBy = approvedBy;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
	return approvedOn;
    }

    /**
     * @param approvedOn
     *            the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
	this.approvedOn = approvedOn;
    }

    /**
     * @return the importfileContent
     */
    public Blob getImportfileContent() {
	return importfileContent;
    }

    /**
     * @param importfileContent
     *            the importfileContent to set
     */
    public void setImportfileContent(Blob importfileContent) {
	this.importfileContent = importfileContent;
    }

    public VendorMaster getPrimeVendor() {
	return primeVendor;
    }

    public void setPrimeVendor(VendorMaster primeVendor) {
	this.primeVendor = primeVendor;
    }

    /**
     * @return the keyValue
     */
    public Integer getKeyValue() {
	return keyValue;
    }

    /**
     * @param keyValue
     *            the keyValue to set
     */
    public void setKeyValue(Integer keyValue) {
	this.keyValue = keyValue;
    }

}
