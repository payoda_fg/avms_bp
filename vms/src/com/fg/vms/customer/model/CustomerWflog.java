/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * 
 * @author pirabu
 */
@Entity
@Table(name = "customer_wflog")
public class CustomerWflog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "send_email_vendor_created")
    private Character sendEmailVendorCreated;
    @Column(name = "make_capability")
    private Character makeCapability;
    @Column(name = "capability_completed")
    private Character capabilityCompleted;
    @Column(name = "send_email_vendor_approved")
    private Character sendEmailVendorApproved;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "vendorId", referencedColumnName = "ID")
    private VendorMaster vendorId;

    public CustomerWflog() {
    }

    public CustomerWflog(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getSendEmailVendorCreated() {
        return sendEmailVendorCreated;
    }

    public void setSendEmailVendorCreated(Character sendEmailVendorCreated) {
        this.sendEmailVendorCreated = sendEmailVendorCreated;
    }

    public Character getMakeCapability() {
        return makeCapability;
    }

    public void setMakeCapability(Character makeCapability) {
        this.makeCapability = makeCapability;
    }

    public Character getCapabilityCompleted() {
        return capabilityCompleted;
    }

    public void setCapabilityCompleted(Character capabilityCompleted) {
        this.capabilityCompleted = capabilityCompleted;
    }

    public Character getSendEmailVendorApproved() {
        return sendEmailVendorApproved;
    }

    public void setSendEmailVendorApproved(Character sendEmailVendorApproved) {
        this.sendEmailVendorApproved = sendEmailVendorApproved;
    }

    public VendorMaster getVendorId() {
        return vendorId;
    }

    public void setVendorId(VendorMaster vendorId) {
        this.vendorId = vendorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof CustomerWflog)) {
            return false;
        }
        CustomerWflog other = (CustomerWflog) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sam.CustomerWflog[ id=" + id + " ]";
    }

}
