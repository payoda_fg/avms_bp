/**
 * CustomerVendorServiceArea.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_vendorservicearea")
@NamedQueries({ @NamedQuery(name = "CustomerVendorServiceArea.findAll", query = "SELECT c FROM CustomerVendorServiceArea c") })
public class CustomerVendorServiceArea implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "ISACTIVE")
	private short isActive;
	@Column(name = "CREATEDBY")
	private int createdBy;
	@Column(name = "CREATEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;
	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;
	@JoinColumn(name = "VENDORID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private VendorMaster vendorId;
	@JoinColumn(name = "SERVICEAREAID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private CustomerServiceArea serviceAreaId;

	public CustomerVendorServiceArea() {
	}

	public CustomerVendorServiceArea(Integer id) {
		this.id = id;
	}

	public CustomerVendorServiceArea(Integer id, short isactive, int createdby,
			Date createdon) {
		this.id = id;
		this.isActive = isactive;
		this.createdBy = createdby;
		this.createdOn = createdon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isactive) {
		this.isActive = isactive;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdby) {
		this.createdBy = createdby;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdon) {
		this.createdOn = createdon;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedby) {
		this.modifiedBy = modifiedby;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedon) {
		this.modifiedOn = modifiedon;
	}

	public VendorMaster getVendorId() {
		return vendorId;
	}

	public void setVendorId(VendorMaster vendorid) {
		this.vendorId = vendorid;
	}

	public CustomerServiceArea getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(CustomerServiceArea serviceareaid) {
		this.serviceAreaId = serviceareaid;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof CustomerVendorServiceArea)) {
			return false;
		}
		CustomerVendorServiceArea other = (CustomerVendorServiceArea) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.fg.vms.customer.model.CustomerVendorServiceArea[ id=" + id
				+ " ]";
	}
}
