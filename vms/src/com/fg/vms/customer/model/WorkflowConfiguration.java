/*
 * WorkflowConfigurationModel.java 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Model class represents configuring workflow in customer vendor details
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_vendorwfconfig")
public class WorkflowConfiguration implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Items which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "WORKFLOWCONFIGID")
	private Integer WorkflowConfigId;

	@Column(name = "NAICSCODE_AM")
	private Integer naicsCodeAM;

	@Column(name = "FOLLOW_WF")
	private Integer followWF;

	@Column(name = "DISTRIBUTION_EMAIL")
	private Integer distributionEmail;

	@Column(name = "ASSESSMENT_REQUIRED")
	private Integer assessmentRequired;

	@Column(name = "ASSESSMENT_COMPLETED")
	private Integer assessmentCompleted;

	@Column(name = "APPROVAL_EMAIL")
	private Integer approvalEmail;

	@Column(name = "CERTIFICATE_EXPIRY_EMAIL")
	private Integer certificateEmail;

	@Column(name = "DISTRIBUTION_EMAIL_PERCENT")
	private Double distributionEmailPercent;

	@Column(name = "ASSESSMENT_REQUIRED_PERCENT")
	private Double assessmentRequiredPercent;

	@Column(name = "ASSESSMENT_COMPLETED_PERCENT")
	private Double assessmentCompletedPercent;

	@Column(name = "APPROVAL_EMAIL_PERCENT")
	private Double approvalEmailPercent;

	@OneToOne
	@JoinColumn(name = "EMAILDISTRIBUTIONMASTERID")
	private EmailDistributionModel emailDistributionMasterId;

	@OneToOne
	@JoinColumn(name = "TIER2EMAILDISTRIBUTIONMASTERID")
	private EmailDistributionModel tier2EmailDist;

	@OneToOne
	@JoinColumn(name = "ADMINGROUP")
	private EmailDistributionModel adminGroup;

	@Column(name = "DAYS_TIER2REPORTUPLOAD")
	private Integer daysUpload;

	@Column(name = "DAYS_CERTIFICATEEXPIRY")
	private Integer daysExpiry;

	@Column(name = "DAYS_INACTIVEVENDOR")
	private Integer daysInactive;

	@Column(name = "DAYS_REPORTDUE")
	private Integer reportDue;

	@Column(name = "DOCUMENT_UPLOAD")
	private Integer documentUpload;
	
	@Column(name = "DOCUMENT_SIZE")
	private Integer documentSize;
	
	@Column(name = "INTERNATIONAL_MODE")
	private Integer internationalMode;
	
	@Column(name = "BP_SEGMENT")
	private Integer bpSegment;
	
	@OneToOne
	@JoinColumn(name = "STATUS")
	private StatusMaster status;
	
	@Column(name = "CERTIFICATE_EXPIRY_EMAIL_SUMMARY_ALERT")
	private Integer certificateEmailSummaryAlert;
	
	@Column(name = "REPORT_NOT_SUBMITTED_REMINDER_DATE")
	private Integer reportNotSubmittedReminderDate;
	
	@Column(name = "CC_MAIL_ID")
	private String ccEmailId;
	
	@Column(name = "SUPPORT_MAIL_ID")
	private String supportMailId;
	
	
	
	@Column(name = "ISDISPLAYSDF")
	private Integer isDisplaySdf;
	public Integer getIsDisplaySdf() {
		return isDisplaySdf;
	}

	public void setIsDisplaySdf(Integer isDisplaySdf) {
		this.isDisplaySdf = isDisplaySdf;
	}

	/**
	 * @return the workflowConfigId
	 */
	public Integer getWorkflowConfigId() {
		return WorkflowConfigId;
	}

	/**
	 * @param workflowConfigId
	 *            the workflowConfigId to set
	 */
	public void setWorkflowConfigId(Integer workflowConfigId) {
		WorkflowConfigId = workflowConfigId;
	}

	/**
	 * @return the naicsCodeAM
	 */
	public Integer getNaicsCodeAM() {
		return naicsCodeAM;
	}

	/**
	 * @param naicsCodeAM
	 *            the naicsCodeAM to set
	 */
	public void setNaicsCodeAM(Integer naicsCodeAM) {
		this.naicsCodeAM = naicsCodeAM;
	}

	/**
	 * @return the followWF
	 */
	public Integer getFollowWF() {
		return followWF;
	}

	/**
	 * @param followWF
	 *            the followWF to set
	 */
	public void setFollowWF(Integer followWF) {
		this.followWF = followWF;
	}

	/**
	 * @return the distributionEmail
	 */
	public Integer getDistributionEmail() {
		return distributionEmail;
	}

	/**
	 * @param distributionEmail
	 *            the distributionEmail to set
	 */
	public void setDistributionEmail(Integer distributionEmail) {
		this.distributionEmail = distributionEmail;
	}

	/**
	 * @return the assessmentRequired
	 */
	public Integer getAssessmentRequired() {
		return assessmentRequired;
	}

	/**
	 * @param assessmentRequired
	 *            the assessmentRequired to set
	 */
	public void setAssessmentRequired(Integer assessmentRequired) {
		this.assessmentRequired = assessmentRequired;
	}

	/**
	 * @return the assessmentCompleted
	 */
	public Integer getAssessmentCompleted() {
		return assessmentCompleted;
	}

	/**
	 * @param assessmentCompleted
	 *            the assessmentCompleted to set
	 */
	public void setAssessmentCompleted(Integer assessmentCompleted) {
		this.assessmentCompleted = assessmentCompleted;
	}

	/**
	 * @return the approvalEmail
	 */
	public Integer getApprovalEmail() {
		return approvalEmail;
	}

	/**
	 * @param approvalEmail
	 *            the approvalEmail to set
	 */
	public void setApprovalEmail(Integer approvalEmail) {
		this.approvalEmail = approvalEmail;
	}

	/**
	 * @return the certificateEmail
	 */
	public Integer getCertificateEmail() {
		return certificateEmail;
	}

	/**
	 * @param certificateEmail
	 *            the certificateEmail to set
	 */
	public void setCertificateEmail(Integer certificateEmail) {
		this.certificateEmail = certificateEmail;
	}

	/**
	 * @return the distributionEmailPercent
	 */
	public Double getDistributionEmailPercent() {
		return distributionEmailPercent;
	}

	/**
	 * @param distributionEmailPercent
	 *            the distributionEmailPercent to set
	 */
	public void setDistributionEmailPercent(Double distributionEmailPercent) {
		this.distributionEmailPercent = distributionEmailPercent;
	}

	/**
	 * @return the assessmentRequiredPercent
	 */
	public Double getAssessmentRequiredPercent() {
		return assessmentRequiredPercent;
	}

	/**
	 * @param assessmentRequiredPercent
	 *            the assessmentRequiredPercent to set
	 */
	public void setAssessmentRequiredPercent(Double assessmentRequiredPercent) {
		this.assessmentRequiredPercent = assessmentRequiredPercent;
	}

	/**
	 * @return the assessmentCompletedPercent
	 */
	public Double getAssessmentCompletedPercent() {
		return assessmentCompletedPercent;
	}

	/**
	 * @param assessmentCompletedPercent
	 *            the assessmentCompletedPercent to set
	 */
	public void setAssessmentCompletedPercent(Double assessmentCompletedPercent) {
		this.assessmentCompletedPercent = assessmentCompletedPercent;
	}

	/**
	 * @return the approvalEmailPercent
	 */
	public Double getApprovalEmailPercent() {
		return approvalEmailPercent;
	}

	/**
	 * @param approvalEmailPercent
	 *            the approvalEmailPercent to set
	 */
	public void setApprovalEmailPercent(Double approvalEmailPercent) {
		this.approvalEmailPercent = approvalEmailPercent;
	}

	/**
	 * @return the emailDistributionMasterId
	 */
	public EmailDistributionModel getEmailDistributionMasterId() {
		return emailDistributionMasterId;
	}

	/**
	 * @param emailDistributionMasterId
	 *            the emailDistributionMasterId to set
	 */
	public void setEmailDistributionMasterId(
			EmailDistributionModel emailDistributionMasterId) {
		this.emailDistributionMasterId = emailDistributionMasterId;
	}

	public EmailDistributionModel getAdminGroup() {
		return adminGroup;
	}

	public void setAdminGroup(EmailDistributionModel adminGroup) {
		this.adminGroup = adminGroup;
	}

	/**
	 * @return the daysUpload
	 */
	public Integer getDaysUpload() {
		return daysUpload;
	}

	/**
	 * @param daysUpload
	 *            the daysUpload to set
	 */
	public void setDaysUpload(Integer daysUpload) {
		this.daysUpload = daysUpload;
	}

	/**
	 * @return the daysExpiry
	 */
	public Integer getDaysExpiry() {
		return daysExpiry;
	}

	/**
	 * @param daysExpiry
	 *            the daysExpiry to set
	 */
	public void setDaysExpiry(Integer daysExpiry) {
		this.daysExpiry = daysExpiry;
	}

	/**
	 * @return the daysInactive
	 */
	public Integer getDaysInactive() {
		return daysInactive;
	}

	/**
	 * @param daysInactive
	 *            the daysInactive to set
	 */
	public void setDaysInactive(Integer daysInactive) {
		this.daysInactive = daysInactive;
	}

	/**
	 * @return the reportDue
	 */
	public Integer getReportDue() {
		return reportDue;
	}

	/**
	 * @param reportDue
	 *            the reportDue to set
	 */
	public void setReportDue(Integer reportDue) {
		this.reportDue = reportDue;
	}

	/**
	 * @return the documentUpload
	 */
	public Integer getDocumentUpload() {
		return documentUpload;
	}

	/**
	 * @param documentUpload
	 *            the documentUpload to set
	 */
	public void setDocumentUpload(Integer documentUpload) {
		this.documentUpload = documentUpload;
	}

	public EmailDistributionModel getTier2EmailDist() {
		return tier2EmailDist;
	}

	public void setTier2EmailDist(EmailDistributionModel tier2EmailDist) {
		this.tier2EmailDist = tier2EmailDist;
	}

	/**
	 * @return the documentSize
	 */
	public Integer getDocumentSize() {
		return documentSize;
	}

	/**
	 * @param documentSize the documentSize to set
	 */
	public void setDocumentSize(Integer documentSize) {
		this.documentSize = documentSize;
	}

	/**
	 * @return the internationalMode
	 */
	public Integer getInternationalMode() {
		return internationalMode;
	}

	/**
	 * @param internationalMode the internationalMode to set
	 */
	public void setInternationalMode(Integer internationalMode) {
		this.internationalMode = internationalMode;
	}

	/**
	 * @return the bpSegment
	 */
	public Integer getBpSegment() {
		return bpSegment;
	}

	/**
	 * @param bpSegment the bpSegment to set
	 */
	public void setBpSegment(Integer bpSegment) {
		this.bpSegment = bpSegment;
	}

	/**
	 * @return the status
	 */
	public StatusMaster getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(StatusMaster status) {
		this.status = status;
	}

	/**
	 * @return the certificateEmailSummaryAlert
	 */
	public Integer getCertificateEmailSummaryAlert() {
		return certificateEmailSummaryAlert;
	}

	/**
	 * @param certificateEmailSummaryAlert the certificateEmailSummaryAlert to set
	 */
	public void setCertificateEmailSummaryAlert(Integer certificateEmailSummaryAlert) {
		this.certificateEmailSummaryAlert = certificateEmailSummaryAlert;
	}

	/**
	 * @return the reportNotSubmittedReminderDate
	 */
	public Integer getReportNotSubmittedReminderDate() {
		return reportNotSubmittedReminderDate;
	}

	/**
	 * @param reportNotSubmittedReminderDate the reportNotSubmittedReminderDate to set
	 */
	public void setReportNotSubmittedReminderDate(
			Integer reportNotSubmittedReminderDate) {
		this.reportNotSubmittedReminderDate = reportNotSubmittedReminderDate;
	}

	public String getCcEmailId() {
		return ccEmailId;
	}

	public void setCcEmailId(String ccEmailId) {
		this.ccEmailId = ccEmailId;
	}

	public String getSupportMailId() {
		return supportMailId;
	}

	public void setSupportMailId(String supportMailId) {
		this.supportMailId = supportMailId;
	}
	
}