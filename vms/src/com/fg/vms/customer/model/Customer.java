/**
 * Customer.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents the customer details (eg..custName, custCode, phone, emailId etc.)
 * for communicate with DB
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the Customer which serves as the primary key. */
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false, unique = true)
    private Integer id;

    @Column(name = "CUSTNAME", nullable = false)
    private String custName;

    @Column(name = "CUSTCODE", nullable = false)
    private String custCode;

    @Column(name = "DUNSNUMBER", nullable = false)
    private String dunsNumber;

    @Column(name = "TAXID")
    private String taxId;

    @Column(name = "ADDRESS1", nullable = false)
    private String address1;

    @Column(name = "ADDRESS2")
    private String address2;

    @Column(name = "ADDRESS3")
    private String address3;

    @Column(name = "CITY")
    private String city;

    @Column(name = "STATE")
    private String state;

    @Column(name = "PROVINCE")
    private String province;

    @Column(name = "ZIPCODE")
    private String zipCode;

    @Column(name = "REGION")
    private String region;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "MOBILE")
    private String mobile;

    @Column(name = "FAX")
    private String fax;

    @Column(name = "EMAILID")
    private String emailId;

    @Column(name = "WEBSITE")
    private String website;

    @Column(name = "DATABASENAME")
    private String databaseName;

    @Column(name = "USERNAME")
    private String UserName;

    @Column(name = "PASSWORD")
    private String dbpassword;

    @Column(name = "DATABASECONNECTIONSTRING")
    private String databaseIp;

    @Column(name = "APPLICATIONURL", nullable = false)
    private String applicationUrl;

    @Column(name = "APPLICATIONSERVER")
    private String applicationServer;

    @Column(name = "ISACTIVE")
    private Byte isActive;

    @Column(name = "CREATEDBY")
    private Integer createdBy;

    @Column(name = "CREATEDON")
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the custName
     */
    public String getCustName() {
	return custName;
    }

    /**
     * @param custName
     *            the custName to set
     */
    public void setCustName(String custName) {
	this.custName = custName;
    }

    /**
     * @return the custCode
     */
    public String getCustCode() {
	return custCode;
    }

    /**
     * @param custCode
     *            the custCode to set
     */
    public void setCustCode(String custCode) {
	this.custCode = custCode;
    }

    /**
     * @return the dunsNumber
     */
    public String getDunsNumber() {
	return dunsNumber;
    }

    /**
     * @param dunsNumber
     *            the dunsNumber to set
     */
    public void setDunsNumber(String dunsNumber) {
	this.dunsNumber = dunsNumber;
    }

    /**
     * @return the taxId
     */
    public String getTaxId() {
	return taxId;
    }

    /**
     * @param taxId
     *            the taxId to set
     */
    public void setTaxId(String taxId) {
	this.taxId = taxId;
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
	return address1;
    }

    /**
     * @param address1
     *            the address1 to set
     */
    public void setAddress1(String address1) {
	this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2() {
	return address2;
    }

    /**
     * @param address2
     *            the address2 to set
     */
    public void setAddress2(String address2) {
	this.address2 = address2;
    }

    /**
     * @return the address3
     */
    public String getAddress3() {
	return address3;
    }

    /**
     * @param address3
     *            the address3 to set
     */
    public void setAddress3(String address3) {
	this.address3 = address3;
    }

    /**
     * @return the city
     */
    public String getCity() {
	return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(String city) {
	this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
	return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(String state) {
	this.state = state;
    }

    /**
     * @return the province
     */
    public String getProvince() {
	return province;
    }

    /**
     * @param province
     *            the province to set
     */
    public void setProvince(String province) {
	this.province = province;
    }

    /**
     * @return the zipCode
     */
    public String getZipCode() {
	return zipCode;
    }

    /**
     * @param zipCode
     *            the zipCode to set
     */
    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    /**
     * @return the region
     */
    public String getRegion() {
	return region;
    }

    /**
     * @param region
     *            the region to set
     */
    public void setRegion(String region) {
	this.region = region;
    }

    /**
     * @return the country
     */
    public String getCountry() {
	return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(String country) {
	this.country = country;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
	return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(String phone) {
	this.phone = phone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
	return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    /**
     * @return the fax
     */
    public String getFax() {
	return fax;
    }

    /**
     * @param fax
     *            the fax to set
     */
    public void setFax(String fax) {
	this.fax = fax;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
	return emailId;
    }

    /**
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(String emailId) {
	this.emailId = emailId;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
	return website;
    }

    /**
     * @param website
     *            the website to set
     */
    public void setWebsite(String website) {
	this.website = website;
    }

    /**
     * @return the databaseName
     */
    public String getDatabaseName() {
	return databaseName;
    }

    /**
     * @param databaseName
     *            the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
	this.databaseName = databaseName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return UserName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	UserName = userName;
    }

    /**
     * @return the dbpassword
     */
    public String getDbpassword() {
	return dbpassword;
    }

    /**
     * @param dbpassword
     *            the dbpassword to set
     */
    public void setDbpassword(String dbpassword) {
	this.dbpassword = dbpassword;
    }

    /**
     * @return the databaseIp
     */
    public String getDatabaseIp() {
	return databaseIp;
    }

    /**
     * @param databaseIp
     *            the databaseIp to set
     */
    public void setDatabaseIp(String databaseIp) {
	this.databaseIp = databaseIp;
    }

    /**
     * @return the applicationUrl
     */
    public String getApplicationUrl() {
	return applicationUrl;
    }

    /**
     * @param applicationUrl
     *            the applicationUrl to set
     */
    public void setApplicationUrl(String applicationUrl) {
	this.applicationUrl = applicationUrl;
    }

    /**
     * @return the applicationServer
     */
    public String getApplicationServer() {
	return applicationServer;
    }

    /**
     * @param applicationServer
     *            the applicationServer to set
     */
    public void setApplicationServer(String applicationServer) {
	this.applicationServer = applicationServer;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
