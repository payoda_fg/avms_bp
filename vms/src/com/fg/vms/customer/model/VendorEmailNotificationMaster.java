/*
 * VendorEmailNotificationMaster.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents the email notification details (eg..email date, email content,
 * template assigned to vendor and final date for reply etc.) for communicate
 * with DB.
 * 
 * @author Vinoth
 */
@Entity
@Table(name = "vendor_email_notification_master")
public class VendorEmailNotificationMaster implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the VendorEmailNotificationMaster which serves as
	 * the primary key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "EMAILDATE")
	private Date emailDate;

	@Column(name = "EMAILCONTENT")
	private String emailContent;

	@Column(name = "EMAILSUBJECT")
	private String emailSubject;

	@ManyToOne
	@JoinColumn(name = "ASSESSMENTTEMPLATEMASTERID")
	private Template template;

	@Column(name = "FINALDATEOFREPLY")
	private Date finalDateOfReply;

	@Column(name = "EMAILNOTIFICATIONTYPE")
	private Integer emailNotificationType;

	@Column(name = "CREATEDBY")
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "FILETYPE")
	private String fileType;

	@Column(name = "FILENAME")
	private String filename;

	@Column(name = "PHYSICALPATH")
	private String physicalpath;

	@Column(name = "FILESIZE")
	private Integer filesize;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the emailDate
	 */
	public Date getEmailDate() {
		return emailDate;
	}

	/**
	 * @param emailDate
	 *            the emailDate to set
	 */
	public void setEmailDate(Date emailDate) {
		this.emailDate = emailDate;
	}

	/**
	 * @return the emailContent
	 */
	public String getEmailContent() {
		return emailContent;
	}

	/**
	 * @param emailContent
	 *            the emailContent to set
	 */
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}

	/**
	 * @param emailSubject
	 *            the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	/**
	 * @return the template
	 */
	public Template getTemplate() {
		return template;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * @return the finalDateOfReply
	 */
	public Date getFinalDateOfReply() {
		return finalDateOfReply;
	}

	/**
	 * @param finalDateOfReply
	 *            the finalDateOfReply to set
	 */
	public void setFinalDateOfReply(Date finalDateOfReply) {
		this.finalDateOfReply = finalDateOfReply;
	}

	/**
	 * @return the emailNotificationType
	 */
	public Integer getEmailNotificationType() {
		return emailNotificationType;
	}

	/**
	 * @param emailNotificationType
	 *            the emailNotificationType to set
	 */
	public void setEmailNotificationType(Integer emailNotificationType) {
		this.emailNotificationType = emailNotificationType;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getPhysicalpath() {
		return physicalpath;
	}

	public void setPhysicalpath(String physicalpath) {
		this.physicalpath = physicalpath;
	}

	public Integer getFilesize() {
		return filesize;
	}

	public void setFilesize(Integer filesize) {
		this.filesize = filesize;
	}

}
