/**
 * CustomerServiceArea.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents the customer service area details.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_servicearea")
public class CustomerServiceArea implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the customer service area which serves as the
	 * primary key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "SERVICEAREANAME")
	private String serviceArea;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the serviceArea
	 */
	public String getServiceArea() {
		return serviceArea;
	}

	/**
	 * @param serviceArea
	 *            the serviceArea to set
	 */
	public void setServiceArea(String serviceArea) {
		this.serviceArea = serviceArea;
	}

}
