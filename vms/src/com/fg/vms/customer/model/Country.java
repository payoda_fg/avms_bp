/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author gpirabu
 */
@Entity
@Table(name = "country")
@NamedQueries({ @NamedQuery(name = "Country.findAll", query = "SELECT c FROM Country c") })
public class Country implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "Id")
	private Integer id;
	@Basic(optional = false)
	@Column(name = "countryname")
	private String countryname;
	@Column(name = "shortname")
	private String shortname;
	@Column(name = "ISACTIVE")
	private short isactive;
	@Column(name = "createdby")
	private Integer createdby;
	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	@Column(name = "modifiedby")
	private Integer modifiedby;
	@Column(name = "modifiedon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedon;
	@OneToMany(mappedBy = "countryid")
	private List<State> stateList;
	@Column(name = "provincestate")
	private String provincestate;
	@Column(name = "isdefault", columnDefinition = "int default 0")
	private int isDefault;

	public Country() {
	}

	public Country(Integer id) {
		this.id = id;
	}

	public Country(Integer id, String countryname) {
		this.id = id;
		this.countryname = countryname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCountryname() {
		return countryname;
	}

	public void setCountryname(String countryname) {
		this.countryname = countryname;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public Integer getCreatedby() {
		return createdby;
	}

	public short getIsactive() {
		return isactive;
	}

	public void setIsactive(short isactive) {
		this.isactive = isactive;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	public List<State> getStateList() {
		return stateList;
	}

	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Country)) {
			return false;
		}
		Country other = (Country) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "demo.Country[ id=" + id + " ]";
	}

	/**
	 * @return the provincestate
	 */
	public String getProvincestate() {
		return provincestate;
	}

	/**
	 * @param provincestate the provincestate to set
	 */
	public void setProvincestate(String provincestate) {
		this.provincestate = provincestate;
	}

	/**
	 * @return the isDefault
	 */
	public int getIsDefault() {
		return isDefault;
	}

	/**
	 * @param isDefault the isDefault to set
	 */
	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}	
}
