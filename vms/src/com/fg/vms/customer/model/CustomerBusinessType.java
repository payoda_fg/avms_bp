/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "customer_businesstype")
public class CustomerBusinessType implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private Integer id;
	@Basic(optional = false)
	@Column(name = "TypeName")
	private String typeName;
	@Basic(optional = false)
	@Column(name = "TypeValue")
	private String typeValue;
	@Column(name = "isactive")
	private Short isactive;
	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	@Column(name = "createdby")
	private Integer createdby;
	@Column(name = "modifiedby")
	private Integer modifiedby;
	@Column(name = "modifiedon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedon;

	public CustomerBusinessType() {
	}

	public CustomerBusinessType(Integer id) {
		this.id = id;
	}

	public CustomerBusinessType(Integer id, String typeName, String typeValue) {
		this.id = id;
		this.typeName = typeName;
		this.typeValue = typeValue;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeValue() {
		return typeValue;
	}

	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}

	public Short getIsactive() {
		return isactive;
	}

	public void setIsactive(Short isactive) {
		this.isactive = isactive;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Integer getCreatedby() {
		return createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof CustomerBusinessType)) {
			return false;
		}
		CustomerBusinessType other = (CustomerBusinessType) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.fg.vms.customer.model.CustomerBusinessType[ id=" + id
				+ " ]";
	}

}
