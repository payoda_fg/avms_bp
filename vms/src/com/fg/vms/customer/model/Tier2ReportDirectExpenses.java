/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author pirabu
 */
@Entity
@Table(name = "customer_tier2reportdirectexpenses")
/*@SqlResultSetMapping(name = "direct", columns = {
        @ColumnResult(name = "primeVendor"), @ColumnResult(name = "t2vendor"),
        @ColumnResult(name = "ryear"), @ColumnResult(name = "rp"),
        @ColumnResult(name = "cn"), @ColumnResult(name = "directspendamt"),
        @ColumnResult(name = "totalexpenses"),
        @ColumnResult(name = "percentDiversityspend"),
        @ColumnResult(name = "submittedDate") })
@NamedNativeQueries({ @NamedNativeQuery(name = "TIER2_DIRECT_SPEND_REPORT", query = "SELECT  (pvm.VENDORNAME) primeVendor,t2v.VENDORNAME  t2vendor,"
        + "('2013')  ryear ,t2m.ReportingPeriod rp ,"
        + "  cm.CERTIFICATENAME cn, "
        + " sum(t2rd.DirExpenseAmt) directspendamt,"
        + "(select sum(ds.DirExpenseAmt) from customer_tier2reportdirectexpenses ds,  "
        + " customer_tier2reportmaster t2rm  where ds.Tier2ReoprtMasterid=t2rm.Tier2ReportMasterid and  t2rm.ReportingStartDate >= '2013/04/01'  AND  "
        + " t2rm.ReportingEndDate <= :endDate and t2rm.vendorId in (:vendorIds,2,3) ) totalexpenses,"

        + "((sum(t2rd.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)"
        + "    from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2rm"
        + "    where ds.Tier2ReoprtMasterid=t2rm.Tier2ReportMasterid and"
        + "    t2rm.ReportingStartDate >= '2013/04/01'  AND"
        + "    t2rm.ReportingEndDate <= '2013/06/30' and t2rm.vendorId in (:vendorIds,2,3))) percentDiversityspend,"

        + " DATE_FORMAT(t2m.CreatedOn, '%d-%m-%Y')  submittedDate"
        + " FROM customer_vendormaster pvm,customer_tier2reportmaster t2m,customer_tier2reportdirectexpenses t2rd, customer_vendormaster t2v,customer_certificatemaster cm "
        + " WHERE t2m.vendorId = pvm.ID  and "
        + " t2rd.Tier2ReoprtMasterid = t2m.Tier2ReportMasterid and  "
        + " t2v.ID = t2rd.Tier2_Vendorid  and "
        + " cm.ID = t2rd.CertificateId  and  "
        + " pvm.ID in (:vendorIds,2,3)  AND    t2m.ReportingStartDate >= '2013/04/01'  AND  "
        + " t2m.ReportingEndDate <= '2013/06/30'   "
        + "    "
        + " group by  pvm.VENDORNAME ,t2v.VENDORNAME , ryear,rp, cn ,submittedDate", resultSetMapping = "direct") })*/
public class Tier2ReportDirectExpenses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Tier2ReportDirExpId")
    private Integer tier2ReportDirExpId;

    // @Max(value=?) @Min(value=?)//if you know range of your decimal fields
    // consider using these annotations to enforce field validation
    @Column(name = "DirExpenseAmt")
    private Double dirExpenseAmt;
    @Column(name = "CreatedBy")
    private Integer createdBy;
    @Column(name = "CreatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Column(name = "ModifiedBy")
    private Integer modifiedBy;
    @Column(name = "ModifiedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;
    @JoinColumn(name = "ethnicityId", referencedColumnName = "id")
    @ManyToOne
    private Ethnicity ethnicityId;
    @JoinColumn(name = "Tier2ReoprtMasterid", referencedColumnName = "Tier2ReportMasterid")
    @ManyToOne
    private Tier2ReportMaster tier2ReoprtMasterid;

    @JoinColumn(name = "Tier2_Vendorid", referencedColumnName = "ID")
    @ManyToOne
    private VendorMaster tier2Vendorid;
    @JoinColumn(name = "CertificateId", referencedColumnName = "ID")
    @ManyToOne
    private Certificate certificateId;
    @JoinColumn(name = "marketSectorId", referencedColumnName = "ID")
    @ManyToOne
    private MarketSector marketSector;
    @JoinColumn(name = "marketSubSectorId", referencedColumnName = "ID")
    @ManyToOne
    private CustomerCommodityCategory marketSubSector;
    public CustomerCommodityCategory getMarketSubSector() {
		return marketSubSector;
	}

	public void setMarketSubSector(CustomerCommodityCategory marketSubSector) {
		this.marketSubSector = marketSubSector;
	}

	public Tier2ReportDirectExpenses() {
    }

    public Tier2ReportDirectExpenses(Integer tier2ReportDirExpId) {
        this.tier2ReportDirExpId = tier2ReportDirExpId;
    }

    public Integer getTier2ReportDirExpId() {
        return tier2ReportDirExpId;
    }

    public void setTier2ReportDirExpId(Integer tier2ReportDirExpId) {
        this.tier2ReportDirExpId = tier2ReportDirExpId;
    }

    public Double getDirExpenseAmt() {
        return dirExpenseAmt;
    }

    public void setDirExpenseAmt(Double dirExpenseAmt) {
        this.dirExpenseAmt = dirExpenseAmt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Tier2ReportMaster getTier2ReoprtMasterid() {
        return tier2ReoprtMasterid;
    }

    public void setTier2ReoprtMasterid(Tier2ReportMaster tier2ReoprtMasterid) {
        this.tier2ReoprtMasterid = tier2ReoprtMasterid;
    }

    /**
     * @return the tier2Vendorid
     */
    public VendorMaster getTier2Vendorid() {
        return tier2Vendorid;
    }

    /**
     * @param tier2Vendorid
     *            the tier2Vendorid to set
     */
    public void setTier2Vendorid(VendorMaster tier2Vendorid) {
        this.tier2Vendorid = tier2Vendorid;
    }

    /**
     * @return the certificateId
     */
    public Certificate getCertificateId() {
        return certificateId;
    }

    /**
     * @param certificateId
     *            the certificateId to set
     */
    public void setCertificateId(Certificate certificateId) {
        this.certificateId = certificateId;
    }

    /**
     * @return
     */
    public Ethnicity getEthnicityId() {
		return ethnicityId;
	}

	/**
	 * @param ethnicityId
	 */
	public void setEthnicityId(Ethnicity ethnicityId) {
		this.ethnicityId = ethnicityId;
	}

	public MarketSector getMarketSector() {
		return marketSector;
	}

	public void setMarketSector(MarketSector marketSector) {
		this.marketSector = marketSector;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (tier2ReportDirExpId != null ? tier2ReportDirExpId.hashCode()
                : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof Tier2ReportDirectExpenses)) {
            return false;
        }
        Tier2ReportDirectExpenses other = (Tier2ReportDirectExpenses) object;
        if ((this.tier2ReportDirExpId == null && other.tier2ReportDirExpId != null)
                || (this.tier2ReportDirExpId != null && !this.tier2ReportDirExpId
                        .equals(other.tier2ReportDirExpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sam.Tier2ReportDirectExpenses[ tier2ReportDirExpId="
                + tier2ReportDirExpId + " ]";
    }

}
