/*
 * Items.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Represents the item details (eg.itemDescription,itemCode,etc) for communicate
 * with DB
 * 
 * @author hemavaishnavi
 * 
 */

@Entity
@Table(name = "customer_itemmaster ")
public class Items implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the Items which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer itemid;

    @Column(name = "ITEMDESCRIPTION")
    private String itemDescription;

    @Column(name = "ITEMCODE", nullable = false)
    private String itemCode;

    @Column(name = "ITEMCATEGORY", nullable = false)
    private String itemCategory;

    @Column(name = "ITEMMANUFACTURER")
    private String itemManufacturer;

    @Column(name = "ITEMUOM", nullable = false)
    private String itemUom;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte isActive;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    /**
     * @return the itemid
     */
    public Integer getItemid() {
	return itemid;
    }

    /**
     * @param itemid
     *            the itemid to set
     */
    public void setItemid(Integer itemid) {
	this.itemid = itemid;
    }

    /**
     * @return the itemDescription
     */
    public String getItemDescription() {
	return itemDescription;
    }

    /**
     * @param itemDescription
     *            the itemDescription to set
     */
    public void setItemDescription(String itemDescription) {
	this.itemDescription = itemDescription;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
	return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(String itemCode) {
	this.itemCode = itemCode;
    }

    /**
     * @return the itemCategory
     */
    public String getItemCategory() {
	return itemCategory;
    }

    /**
     * @param itemCategory
     *            the itemCategory to set
     */
    public void setItemCategory(String itemCategory) {
	this.itemCategory = itemCategory;
    }

    /**
     * @return the itemManufacturer
     */
    public String getItemManufacturer() {
	return itemManufacturer;
    }

    /**
     * @param itemManufacturer
     *            the iitemManufacturer to set
     */
    public void setItemManufacturer(String itemManufacturer) {
	this.itemManufacturer = itemManufacturer;
    }

    /**
     * @return the itemUom
     */
    public String getItemUom() {
	return itemUom;
    }

    /**
     * @param itemUom
     *            the itemUom to set
     */
    public void setItemUom(String itemUom) {
	this.itemUom = itemUom;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date date) {
	this.modifiedOn = date;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }
}
