/*
 * Tier2ReportingPeriod.java 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This is model class for Tier2 reporting period in customer pages.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_tier2reportperiod")
public class Tier2ReportingPeriod implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Items which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer reportingPeriodId;

	@Column(name = "DATAUPLOADFREQUENCY", nullable = false)
	private String dataUploadFreq;

	@Column(name = "STARTDATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Column(name = "ENDDATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	/**
	 * @return the reportingPeriodId
	 */
	public Integer getReportingPeriodId() {
		return reportingPeriodId;
	}

	/**
	 * @param reportingPeriodId
	 *            the reportingPeriodId to set
	 */
	public void setReportingPeriodId(Integer reportingPeriodId) {
		this.reportingPeriodId = reportingPeriodId;
	}

	/**
	 * @return the dataUploadFreq
	 */
	public String getDataUploadFreq() {
		return dataUploadFreq;
	}

	/**
	 * @param dataUploadFreq
	 *            the dataUploadFreq to set
	 */
	public void setDataUploadFreq(String dataUploadFreq) {
		this.dataUploadFreq = dataUploadFreq;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
