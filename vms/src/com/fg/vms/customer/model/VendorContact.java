/*
 * VendorContact.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fg.vms.admin.model.SecretQuestion;

/**
 * Model class represents vendor contact information details.
 * 
 * @author srinivasarao
 */

@Entity
@Table(name = "customer_vendor_users")
public class VendorContact implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the VendorContact which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	/** The vendor id. */
	@JoinColumn(name = "VENDORID", referencedColumnName = "ID")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private VendorMaster vendorId;

	/** The vendor user role id. */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "VENDORUSERROLEID", nullable = false)
	private VendorRoles vendorUserRoleId;

	/** The first name. */
	@Column(name = "FIRSTNAME")
	private String firstName;

	/** The last name. */
	@Column(name = "LASTNAME")
	private String lastName;

	/** The designation. */
	@Column(name = "DESIGNATION")
	private String designation;

	/** The phone number. */
	@Column(name = "PHONENUMBER")
	private String phoneNumber;

	/** The mobile. */
	@Column(name = "MOBILE")
	private String mobile;

	/** The fax. */
	@Column(name = "FAX")
	private String fax;

	/** The email id. */
	@Column(name = "EMAILID", nullable = false, unique = true)
	private String emailId;

	/** The key value. */
	@Column(name = "KEYVALUE")
	private Integer keyValue;

	/** The Is allowed login. */
	@Column(name = "ISALLOWEDLOGIN")
	private Byte IsAllowedLogin;

	/** The login display name. */
	@Column(name = "LOGINDISPLAYNAME")
	private String loginDisplayName;


	/** The password. */
	@Column(name = "PASSWORD")
	private String password;

	/** The new password. */
	@Column(name = "NEWPASSWORD")
	private String newPassword;

	/** The secret question id. */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SECRETQUESTIONID")
	private SecretQuestion secretQuestionId;

	/** The sec que ans. */
	@Column(name = "SECRETQUESTIONANSWER")
	private String secQueAns;

	/** The is primary contact. */
	@Column(name = "ISPRIMARYCONTACT", nullable = false)
	private Byte isPrimaryContact;

	/** The is active. */
	@Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;

	/** The created by. */
	@Column(name = "CREATEDBY")
	private Integer createdBy;

	/** The created on. */
	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	/** The modified by. */
	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	/** The modified on. */
	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@Column(name = "ISBUSINESSCONTACT")
	private Byte isBusinessContact;

	@Column(name = "ISPREPARER")
	private Byte isPreparer;

	@Column(name = "ISSYSGENPASSWORD", columnDefinition = "TINYINT(1) DEFAULT 1")
	private Byte isSysGenPassword;
	
	@Column(name = "GENDER")
	 private String gender;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the vendor id.
	 * 
	 * @return the vendorId(@link VendorMaster)
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * Sets the vendor id.
	 * 
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * Gets the vendor user role id.
	 * 
	 * @return the vendorUserRoleId
	 */
	public VendorRoles getVendorUserRoleId() {
		return vendorUserRoleId;
	}

	/**
	 * Sets the vendor user role id.
	 * 
	 * @param vendorUserRoleId
	 *            the vendorUserRoleId to set
	 */
	public void setVendorUserRoleId(VendorRoles vendorUserRoleId) {
		this.vendorUserRoleId = vendorUserRoleId;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 * 
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 * 
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the designation.
	 * 
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * Sets the designation.
	 * 
	 * @param designation
	 *            the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * Gets the phone number.
	 * 
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 * 
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the mobile.
	 * 
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Sets the mobile.
	 * 
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Gets the fax.
	 * 
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax.
	 * 
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the email id.
	 * 
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * Sets the email id.
	 * 
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Gets the checks if is allowed login.
	 * 
	 * @return the checks if is allowed login
	 */
	public Byte getIsAllowedLogin() {
		return IsAllowedLogin;
	}

	/**
	 * Sets the checks if is allowed login.
	 * 
	 * @param isAllowedLogin
	 *            the new checks if is allowed login
	 */
	public void setIsAllowedLogin(Byte isAllowedLogin) {
		IsAllowedLogin = isAllowedLogin;
	}

	/**
	 * Gets the login display name.
	 * 
	 * @return the login display name
	 */
	public String getLoginDisplayName() {
		return loginDisplayName;
	}

	/**
	 * Sets the login display name.
	 * 
	 * @param loginDisplayName
	 *            the new login display name
	 */
	public void setLoginDisplayName(String loginDisplayName) {
		this.loginDisplayName = loginDisplayName;
	}

	

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the new password.
	 * 
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Sets the new password.
	 * 
	 * @param newPassword
	 *            the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * Gets the secret question id.
	 * 
	 * @return the secret question id
	 */
	public SecretQuestion getSecretQuestionId() {
		return secretQuestionId;
	}

	/**
	 * Sets the secret question id.
	 * 
	 * @param secretQuestionId
	 *            the new secret question id
	 */
	public void setSecretQuestionId(SecretQuestion secretQuestionId) {
		this.secretQuestionId = secretQuestionId;
	}

	/**
	 * Gets the sec que ans.
	 * 
	 * @return the sec que ans
	 */
	public String getSecQueAns() {
		return secQueAns;
	}

	/**
	 * Sets the sec que ans.
	 * 
	 * @param secQueAns
	 *            the new sec que ans
	 */
	public void setSecQueAns(String secQueAns) {
		this.secQueAns = secQueAns;
	}

	/**
	 * Gets the checks if is primary contact.
	 * 
	 * @return the checks if is primary contact
	 */
	public Byte getIsPrimaryContact() {
		return isPrimaryContact;
	}

	/**
	 * Sets the checks if is primary contact.
	 * 
	 * @param isPrimaryContact
	 *            the new checks if is primary contact
	 */
	public void setIsPrimaryContact(Byte isPrimaryContact) {
		this.isPrimaryContact = isPrimaryContact;
	}

	/**
	 * Gets the checks if is active.
	 * 
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 * 
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the created by.
	 * 
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 * 
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created on.
	 * 
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 * 
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the modified by.
	 * 
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * Sets the modified by.
	 * 
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modified on.
	 * 
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * Sets the modified on.
	 * 
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Gets the key value.
	 * 
	 * @return the keyValue
	 */
	public Integer getKeyValue() {
		return keyValue;
	}

	/**
	 * Sets the key value.
	 * 
	 * @param keyValue
	 *            the keyValue to set
	 */
	public void setKeyValue(Integer keyValue) {
		this.keyValue = keyValue;
	}

	/**
	 * @return the isBusinessContact
	 */
	public Byte getIsBusinessContact() {
		return isBusinessContact;
	}

	/**
	 * @param isBusinessContact
	 *            the isBusinessContact to set
	 */
	public void setIsBusinessContact(Byte isBusinessContact) {
		this.isBusinessContact = isBusinessContact;
	}

	/**
	 * @return the isPreparer
	 */
	public Byte getIsPreparer() {
		return isPreparer;
	}

	/**
	 * @param isPreparer
	 *            the isPreparer to set
	 */
	public void setIsPreparer(Byte isPreparer) {
		this.isPreparer = isPreparer;
	}

	public Byte getIsSysGenPassword() {
		return isSysGenPassword;
	}

	public void setIsSysGenPassword(Byte isSysGenPassword) {
		this.isSysGenPassword = isSysGenPassword;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

}
