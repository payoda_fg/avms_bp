/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "emailhistory")
public class EmailHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "EMAILTO")
	private String emailto;

	@Column(name = "EMAILADDRESSTYPE")
	private String emailAddressType;

	@Column(name = "EMAILSUBJECT")
	private String emailsubject;

	@Column(name = "EMAILBODY")
	private String emailbody;

	@Column(name = "EMAILDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date emaildate;

	@Column(name = "EMAILTYPE")
	private Short emailtype;

	public EmailHistory() {
	}

	public EmailHistory(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailto() {
		return emailto;
	}

	public void setEmailto(String emailto) {
		this.emailto = emailto;
	}

	public String getEmailAddressType() {
		return emailAddressType;
	}

	public void setEmailAddressType(String emailAddressType) {
		this.emailAddressType = emailAddressType;
	}

	public String getEmailsubject() {
		return emailsubject;
	}

	public void setEmailsubject(String emailsubject) {
		this.emailsubject = emailsubject;
	}

	public String getEmailbody() {
		return emailbody;
	}

	public void setEmailbody(String emailbody) {
		this.emailbody = emailbody;
	}

	public Date getEmaildate() {
		return emaildate;
	}

	public void setEmaildate(Date emaildate) {
		this.emaildate = emaildate;
	}

	public Short getEmailtype() {
		return emailtype;
	}

	public void setEmailtype(Short emailtype) {
		this.emailtype = emailtype;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof EmailHistory)) {
			return false;
		}
		EmailHistory other = (EmailHistory) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.fg.vms.customer.model.EmailHistory[ id=" + id + " ]";
	}

}
