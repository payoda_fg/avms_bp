package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fg.vms.admin.model.Users;

@Entity
@Table(name = "supplier_diversity")
public class SupplierDiversity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
	private Integer id;
	
	@JoinColumn(name = "REQUESTORID", referencedColumnName = "ID")
	@ManyToOne
    private Users requestorId;
	
	@Column(name = "BUSINESS")
    private String business;
    
    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    
    @Column(name = "REQUESTEDON")
	@Temporal(TemporalType.TIMESTAMP)
    private Date requestedOn;
    
    @Column(name = "OPPURTUNITYEVENTNAME")
    private String oppurtunityEventName;
    
    @Column(name = "SOURCEEVENT")
    private String sourceEvent;
    
    @Column(name = "SOURCEEVENTSTARTDATE", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
    private Date sourceEventStartDate;
    
    @Column(name = "ESTIMATESPEND")
    private Double estimateSpend;
    
    @Column(name = "ESTIMATEWORKSTARTDATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date estimateWorkStartDate;
    
    @Column(name = "LASTRESPONSEDATE", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
    private Date lastResponseDate;
    
    @Column(name = "COMMODITYCODE")
    private String commodityCode;
    
    @Column(name = "SCOPOFWORK")
	private String scopOfWork;
    
    @Column(name = "GEOGRAPHY")
    private String geography;
    
    @Column(name = "MINISUPPLIERREQUIREMENT")
	private String miniSupplierRequirement;
    
    @Column(name = "CONTRACTINSIGHT")
    private String contractInsight;
    
    @Column(name = "INCUMBENTVENDORID")
    private String incumbentVendorId;
    
    @Column(name = "ISACTIVE")
    private Integer isActive;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Users getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Users requestorId) {
		this.requestorId = requestorId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public String getOppurtunityEventName() {
		return oppurtunityEventName;
	}

	public void setOppurtunityEventName(String oppurtunityEventName) {
		this.oppurtunityEventName = oppurtunityEventName;
	}

	public String getSourceEvent() {
		return sourceEvent;
	}

	public void setSourceEvent(String sourceEvent) {
		this.sourceEvent = sourceEvent;
	}

	public Date getSourceEventStartDate() {
		return sourceEventStartDate;
	}

	public void setSourceEventStartDate(Date sourceEventStartDate) {
		this.sourceEventStartDate = sourceEventStartDate;
	}

	public Double getEstimateSpend() {
		return estimateSpend;
	}

	public void setEstimateSpend(Double estimateSpend) {
		this.estimateSpend = estimateSpend;
	}

	public Date getEstimateWorkStartDate() {
		return estimateWorkStartDate;
	}

	public void setEstimateWorkStartDate(Date estimateWorkStartDate) {
		this.estimateWorkStartDate = estimateWorkStartDate;
	}

	public Date getLastResponseDate() {
		return lastResponseDate;
	}

	public void setLastResponseDate(Date lastResponseDate) {
		this.lastResponseDate = lastResponseDate;
	}

	public String getCommodityCode() {
		return commodityCode;
	}

	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}

	public String getScopOfWork() {
		return scopOfWork;
	}

	public void setScopOfWork(String scopOfWork) {
		this.scopOfWork = scopOfWork;
	}

	public String getGeography() {
		return geography;
	}

	public void setGeography(String geography) {
		this.geography = geography;
	}

	public String getMiniSupplierRequirement() {
		return miniSupplierRequirement;
	}

	public void setMiniSupplierRequirement(String miniSupplierRequirement) {
		this.miniSupplierRequirement = miniSupplierRequirement;
	}

	public String getContractInsight() {
		return contractInsight;
	}

	public void setContractInsight(String contractInsight) {
		this.contractInsight = contractInsight;
	}

	public String getIncumbentVendorId() {
		return incumbentVendorId;
	}

	public void setIncumbentVendorId(String incumbentVendorId) {
		this.incumbentVendorId = incumbentVendorId;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
}
