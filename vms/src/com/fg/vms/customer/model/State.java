/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author gpirabu
 */
@Entity
@Table(name = "state")
@NamedQueries({ @NamedQuery(name = "State.findAll", query = "SELECT s FROM State s") })
public class State implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID")
	private Integer id;
	@Basic(optional = false)
	@Column(name = "STATENAME")
	private String statename;
	@Column(name = "ABBREV")
	private String abbrev;
	@Basic(optional = false)
	@Column(name = "ISACTIVE")
	private short isactive;
	@Basic(optional = false)
	@Column(name = "CREATEDBY")
	private int createdby;
	@Basic(optional = false)
	@Column(name = "CREATEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	@Column(name = "MODIFIEDBY")
	private Integer modifiedby;
	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedon;
	@JoinColumn(name = "COUNTRYID", referencedColumnName = "Id")
	@ManyToOne
	private Country countryid;

	public State() {
	}

	public State(Integer id) {
		this.id = id;
	}

	public State(Integer id, String statename, short isactive, int createdby,
			Date createdon) {
		this.id = id;
		this.statename = statename;
		this.isactive = isactive;
		this.createdby = createdby;
		this.createdon = createdon;
	}

	public String getAbbrev() {
		return abbrev;
	}

	public void setAbbrev(String abbrev) {
		this.abbrev = abbrev;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatename() {
		return statename;
	}

	public void setStatename(String statename) {
		this.statename = statename;
	}

	public short getIsactive() {
		return isactive;
	}

	public void setIsactive(short isactive) {
		this.isactive = isactive;
	}

	public int getCreatedby() {
		return createdby;
	}

	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	public Country getCountryid() {
		return countryid;
	}

	public void setCountryid(Country countryid) {
		this.countryid = countryid;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof State)) {
			return false;
		}
		State other = (State) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "demo.State[ id=" + id + " ]";
	}

}
