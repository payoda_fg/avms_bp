/*
 * VMSObjects.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents vms object details (eg. object name, object type,
 * object description) for communicate with DB
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "VMS_OBJECTS")
public class VMSObjects implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the VMSObjects which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "OBJECTNAME", nullable = false)
    private String objectName;

    @Column(name = "OBJECTTYPE", nullable = false)
    private String objectType;

    @Column(name = "OBJECTDESCRIPTION", nullable = false)
    private String objectDescription;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte isActive;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the objectName
     */
    public String getObjectName() {
	return objectName;
    }

    /**
     * @param objectName
     *            the objectName to set
     */
    public void setObjectName(String objectName) {
	this.objectName = objectName;
    }

    /**
     * @return the objectType
     */
    public String getObjectType() {
	return objectType;
    }

    /**
     * @param objectType
     *            the objectType to set
     */
    public void setObjectType(String objectType) {
	this.objectType = objectType;
    }

    /**
     * @return the objectDescription
     */
    public String getObjectDescription() {
	return objectDescription;
    }

    /**
     * @param objectDescription
     *            the objectDescription to set
     */
    public void setObjectDescription(String objectDescription) {
	this.objectDescription = objectDescription;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
