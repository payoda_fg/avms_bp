package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author gpirabu
 */
@Entity
@Table(name = "customer_legal_structure")
@NamedQueries({ @NamedQuery(name = "CustomerLegalStructure.findAll", query = "SELECT c FROM CustomerLegalStructure c") })
public class CustomerLegalStructure implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "Id")
	private Integer id;
	@Basic(optional = false)
	@Column(name = "name")
	private String name;
	@Basic(optional = false)
	@Column(name = "shortname")
	private String shortname;
	@Column(name = "isactive")
	private Short isactive;
	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	@Column(name = "createdby")
	private Integer createdby;
	@Column(name = "modifiedby")
	private Integer modifiedby;
	@Column(name = "modifiedon")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedon;

	public CustomerLegalStructure() {
	}

	public CustomerLegalStructure(Integer id) {
		this.id = id;
	}

	public CustomerLegalStructure(Integer id, String name, String shortname) {
		this.id = id;
		this.name = name;
		this.shortname = shortname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public Short getIsactive() {
		return isactive;
	}

	public void setIsactive(Short isactive) {
		this.isactive = isactive;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Integer getCreatedby() {
		return createdby;
	}

	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof CustomerLegalStructure)) {
			return false;
		}
		CustomerLegalStructure other = (CustomerLegalStructure) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "demo.CustomerLegalStructure[ id=" + id + " ]";
	}

}
