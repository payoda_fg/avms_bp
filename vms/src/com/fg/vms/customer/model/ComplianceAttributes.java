/*
 * ComplianceAttributes.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents RFI and RFP Compliance Attributes.
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER_REQUESTIPCOMPLIANCEATTRIBUTES")
public class ComplianceAttributes implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the ComplianceAttributes which serves as the
     * primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "REQUESTMASTERID", nullable = false)
    private RequestIP requestIP;

    @ManyToOne
    @JoinColumn(name = "REQUESTCOMPLIANCEID", nullable = false)
    private RFIPCompliance rfipCompliance;

    @Column(name = "ATTRIBUTEPARTICULARS", nullable = false)
    private String attributeParticulars;

    @Column(name = "WEIGTAGE", nullable = false)
    private BigDecimal weigtage;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the requestIP
     */
    public RequestIP getRequestIP() {
	return requestIP;
    }

    /**
     * @param requestIP
     *            the requestIP to set
     */
    public void setRequestIP(RequestIP requestIP) {
	this.requestIP = requestIP;
    }

    /**
     * @return the rfipCompliance
     */
    public RFIPCompliance getRfipCompliance() {
	return rfipCompliance;
    }

    /**
     * @param rfipCompliance
     *            the rfipCompliance to set
     */
    public void setRfipCompliance(RFIPCompliance rfipCompliance) {
	this.rfipCompliance = rfipCompliance;
    }

    /**
     * @return the attributeParticulars
     */
    public String getAttributeParticulars() {
	return attributeParticulars;
    }

    /**
     * @param attributeParticulars
     *            the attributeParticulars to set
     */
    public void setAttributeParticulars(String attributeParticulars) {
	this.attributeParticulars = attributeParticulars;
    }

    /**
     * @return the weigtage
     */
    public BigDecimal getWeigtage() {
	return weigtage;
    }

    /**
     * @param weigtage
     *            the weigtage to set
     */
    public void setWeigtage(BigDecimal weigtage) {
	this.weigtage = weigtage;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
