/*
 * Currency.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represent the Currency details.
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "customer_currencymaster")
public class Currency implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the Currency which serves as the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @Column(name = "CURRENCYNAME")
    private String currencyName;

    @Column(name = "CURRENCYSYMBOL")
    private String currencySymbol;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the currencyName
     */
    public String getCurrencyName() {
	return currencyName;
    }

    /**
     * @param currencyName
     *            the currencyName to set
     */
    public void setCurrencyName(String currencyName) {
	this.currencyName = currencyName;
    }

    /**
     * @return the currencySymbol
     */
    public String getCurrencySymbol() {
	return currencySymbol;
    }

    /**
     * @param currencySymbol
     *            the currencySymbol to set
     */
    public void setCurrencySymbol(String currencySymbol) {
	this.currencySymbol = currencySymbol;
    }

}
