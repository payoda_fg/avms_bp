/*
 * TemplateQuestions.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * model class represents the template questions details
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "templatequestions")
public class TemplateQuestions implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the TemplateQuestions which serves as the primary
     * key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TEMPLATEID")
    private Template template;

    @Column(name = "QUESTIONDESCRIPTION")
    private String questionDescription;

    @Column(name = "ANSWERDATATYPE")
    private String answerDatatype;

    @Column(name = "ANSWERWEIGHTAGE")
    private Float answerWeightage;

    @Column(name = "QUESTIONORDER")
    private Integer questionOrder;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte isActive;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    /**
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * 
     * @return the template
     */
    public Template getTemplate() {
	return template;
    }

    /**
     * 
     * @param template
     *            the template to set
     */
    public void setTemplate(Template template) {
	this.template = template;
    }

    /**
     * 
     * @return the questiondescription
     */
    public String getQuestionDescription() {
	return questionDescription;
    }

    /**
     * 
     * @param questionDescription
     *            the questiondescriptuion to set
     */
    public void setQuestionDescription(String questionDescription) {
	this.questionDescription = questionDescription;
    }

    /**
     * 
     * @return the answerDatatype
     */
    public String getAnswerDatatype() {
	return answerDatatype;
    }

    /**
     * 
     * @param answerDatatype
     *            the answerDatatype to set
     */
    public void setAnswerDatatype(String answerDatatype) {
	this.answerDatatype = answerDatatype;
    }

    /**
     * 
     * @return the answerWeightage
     */
    public Float getAnswerWeightage() {
	return answerWeightage;
    }

    /**
     * 
     * @param answerWeightage
     *            the answerWeightage to set
     */
    public void setAnswerWeightage(Float answerWeightage) {
	this.answerWeightage = answerWeightage;
    }

    /**
     * 
     * @return the questionOrder
     */
    public Integer getQuestionOrder() {
	return questionOrder;
    }

    /**
     * 
     * @param questionOrder
     *            the questionOrder to set
     */
    public void setQuestionOrder(Integer questionOrder) {
	this.questionOrder = questionOrder;
    }

    /**
     * 
     * @return the active status
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * 
     * @param isActive
     *            the status to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * 
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * 
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * 
     * @return the createdOn date
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * 
     * @param createdOn
     *            the createdOn date to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * 
     * @return the modifiedBy as user
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * 
     * @param modifiedBy
     *            modifiedBy user to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * 
     * @return the modifiedOn date
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * 
     * @param modifiedOn
     *            the modifiedOn date to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
