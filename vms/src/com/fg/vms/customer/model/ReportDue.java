/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author gpirabu
 */
@Entity
@Table(name = "reportdue")
public class ReportDue implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "VENDORID")
    private Integer vendorid;

    @Column(name = "EMAILID")
    private String emailid;

    @Column(name = "PROACTIVEEAMILDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date proactiveeamildate;

    @Column(name = "REPORTSTARTDATE")
    @Temporal(TemporalType.DATE)
    private Date reportstartdate;

    @Column(name = "REPORTENDDATE")
    @Temporal(TemporalType.DATE)
    private Date reportenddate;

    @Column(name = "REPORTPERIOD")
    private String reportperiod;

    @Column(name = "DUEDATE")
    @Temporal(TemporalType.DATE)
    private Date duedate;

    @Column(name = "ISSUBMITTED")
    private String issubmitted;

    @Column(name = "SUBMITTEDBY")
    private Integer submittedby;

    @Column(name = "SUBMITTEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date submittedon;

    public ReportDue() {
    }

    public ReportDue(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the vendorid
     */
    public Integer getVendorid() {
        return vendorid;
    }

    /**
     * @param vendorid
     *            the vendorid to set
     */
    public void setVendorid(Integer vendorid) {
        this.vendorid = vendorid;
    }

    /**
     * @return the emailid
     */
    public String getEmailid() {
        return emailid;
    }

    /**
     * @param emailid
     *            the emailid to set
     */
    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public Date getProactiveeamildate() {
        return proactiveeamildate;
    }

    public void setProactiveeamildate(Date proactiveeamildate) {
        this.proactiveeamildate = proactiveeamildate;
    }

    public Date getReportstartdate() {
        return reportstartdate;
    }

    public void setReportstartdate(Date reportstartdate) {
        this.reportstartdate = reportstartdate;
    }

    public Date getReportenddate() {
        return reportenddate;
    }

    public void setReportenddate(Date reportenddate) {
        this.reportenddate = reportenddate;
    }

    public String getReportperiod() {
        return reportperiod;
    }

    public void setReportperiod(String reportperiod) {
        this.reportperiod = reportperiod;
    }

    public Date getDuedate() {
        return duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    /**
     * @return the issubmitted
     */
    public String getIssubmitted() {
        return issubmitted;
    }

    /**
     * @param issubmitted
     *            the issubmitted to set
     */
    public void setIssubmitted(String issubmitted) {
        this.issubmitted = issubmitted;
    }

    public Integer getSubmittedby() {
        return submittedby;
    }

    public void setSubmittedby(Integer submittedby) {
        this.submittedby = submittedby;
    }

    public Date getSubmittedon() {
        return submittedon;
    }

    public void setSubmittedon(Date submittedon) {
        this.submittedon = submittedon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ReportDue)) {
            return false;
        }
        ReportDue other = (ReportDue) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.fg.vms.customer.model.ReportDue[ id=" + id + " ]";
    }

}
