/*
 * CustomerVendorAssessment.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents the customer vendor assessment details (eg..answerDescription,
 * reviewer details, answer weightage awarded etc.) of customer.
 * 
 * @author Vinoth
 */

@Entity
@Table(name = "customervendorassessment")
public class CustomerVendorAssessment implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the CustomerVendorAssessment which serves as the
     * primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TEMPLATEQUESTIONSID")
    private TemplateQuestions templateQuestions;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TEMPLATEID")
    private Template template;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "VENDOREMAILNOTIFICATIONMASTERID")
    private VendorEmailNotificationMaster vendorEmailNotificationMaster;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "VENDORID")
    private VendorMaster vendorMaster;

    @Column(name = "ANSWERDESCRIPTION")
    private String answerDescription;

    @Column(name = "ANSWEREDBY")
    private Integer answeredBy;

    @Column(name = "ANSWEREDON")
    private Date answeredOn;

    @Column(name = "ISREVIEWED")
    private Byte Isreviewed;

    @Column(name = "REVIEWEDBY")
    private Integer Reviewedby;

    @Column(name = "REVIEWEDON")
    private Date Reviewedon;

    @Column(name = "REVIEWERREMARKS")
    private String ReviewerRemarks;

    @Column(name = "ANSWERWEIGHTAGEAWARDED")
    private Float AnswerWeightageAwarded;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the vendorMaster
     */
    public VendorMaster getVendorMaster() {
	return vendorMaster;
    }

    /**
     * @param vendorMaster
     *            the vendorMaster to set
     */
    public void setVendorMaster(VendorMaster vendorMaster) {
	this.vendorMaster = vendorMaster;
    }

    /**
     * @return the answerDescription
     */
    public String getAnswerDescription() {
	return answerDescription;
    }

    /**
     * @param answerDescription
     *            the answerDescription to set
     */
    public void setAnswerDescription(String answerDescription) {
	this.answerDescription = answerDescription;
    }

    /**
     * @return the answeredBy
     */
    public Integer getAnsweredBy() {
	return answeredBy;
    }

    /**
     * @param answeredBy
     *            the answeredBy to set
     */
    public void setAnsweredBy(Integer answeredBy) {
	this.answeredBy = answeredBy;
    }

    /**
     * @return the answeredOn
     */
    public Date getAnsweredOn() {
	return answeredOn;
    }

    /**
     * @param answeredOn
     *            the answeredOn to set
     */
    public void setAnsweredOn(Date answeredOn) {
	this.answeredOn = answeredOn;
    }

    /**
     * @return the isreviewed
     */
    public Byte getIsreviewed() {
	return Isreviewed;
    }

    /**
     * @param isreviewed
     *            the isreviewed to set
     */
    public void setIsreviewed(Byte isreviewed) {
	Isreviewed = isreviewed;
    }

    /**
     * @return the reviewedby
     */
    public Integer getReviewedby() {
	return Reviewedby;
    }

    /**
     * @param reviewedby
     *            the reviewedby to set
     */
    public void setReviewedby(Integer reviewedby) {
	Reviewedby = reviewedby;
    }

    /**
     * @return the reviewedon
     */
    public Date getReviewedon() {
	return Reviewedon;
    }

    /**
     * @param reviewedon
     *            the reviewedon to set
     */
    public void setReviewedon(Date reviewedon) {
	Reviewedon = reviewedon;
    }

    /**
     * @return the reviewerRemarks
     */
    public String getReviewerRemarks() {
	return ReviewerRemarks;
    }

    /**
     * @param reviewerRemarks
     *            the reviewerRemarks to set
     */
    public void setReviewerRemarks(String reviewerRemarks) {
	ReviewerRemarks = reviewerRemarks;
    }

    /**
     * @return the answerWeightageAwarded
     */
    public Float getAnswerWeightageAwarded() {
	return AnswerWeightageAwarded;
    }

    /**
     * @param answerWeightageAwarded
     *            the answerWeightageAwarded to set
     */
    public void setAnswerWeightageAwarded(Float answerWeightageAwarded) {
	AnswerWeightageAwarded = answerWeightageAwarded;
    }

    /**
     * @return the templateQuestions
     */
    public TemplateQuestions getTemplateQuestions() {
	return templateQuestions;
    }

    /**
     * @param templateQuestions
     *            the templateQuestions to set
     */
    public void setTemplateQuestions(TemplateQuestions templateQuestions) {
	this.templateQuestions = templateQuestions;
    }

    /**
     * @return the template
     */
    public Template getTemplate() {
	return template;
    }

    /**
     * @param template
     *            the template to set
     */
    public void setTemplate(Template template) {
	this.template = template;
    }

    /**
     * @return the vendorEmailNotificationMaster
     */
    public VendorEmailNotificationMaster getVendorEmailNotificationMaster() {
	return vendorEmailNotificationMaster;
    }

    /**
     * @param vendorEmailNotificationMaster
     *            the vendorEmailNotificationMaster to set
     */
    public void setVendorEmailNotificationMaster(
	    VendorEmailNotificationMaster vendorEmailNotificationMaster) {
	this.vendorEmailNotificationMaster = vendorEmailNotificationMaster;
    }

}
