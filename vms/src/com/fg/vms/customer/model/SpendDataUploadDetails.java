/**
 * SpendDataUploadDetails.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * model class represents the upload data information
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER_SPENDDATA_UPLOADDETAIL")
public class SpendDataUploadDetails implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the SpendDataUploadDetails which serves as the
     * primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SPENDDATAMASTERID")
    private SpendDataMaster spendUpload;

    @Column(name = "SPENDTYPE")
    private String spendType;

    @Column(name = "DIVERSECERTIFICATE")
    private String diverseCertificate;

    @Column(name = "NAICSCODE")
    private String naicsCode;

    @Column(name = "SPENDDATE")
    private Date spendDate;

    @Column(name = "SPENDVALUE")
    private String spendValue;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "ISVALID")
    private Byte isValid;

    @Column(name = "VENDORNAICS")
    private Integer vendorNaics;

    @Column(name = "VENDORCERTIFICATEID")
    private Integer vendorCertificateId;

    @Column(name = "KEYVALUE")
    private Integer keyValue;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the spendUpload
     */
    public SpendDataMaster getSpendUpload() {
	return spendUpload;
    }

    /**
     * @param spendUpload
     *            the spendUpload to set
     */
    public void setSpendUpload(SpendDataMaster spendUpload) {
	this.spendUpload = spendUpload;
    }

    /**
     * @return the spendType
     */
    public String getSpendType() {
	return spendType;
    }

    /**
     * @param spendType
     *            the spendType to set
     */
    public void setSpendType(String spendType) {
	this.spendType = spendType;
    }

    /**
     * @return the diverseCertificate
     */
    public String getDiverseCertificate() {
	return diverseCertificate;
    }

    /**
     * @param diverseCertificate
     *            the diverseCertificate to set
     */
    public void setDiverseCertificate(String diverseCertificate) {
	this.diverseCertificate = diverseCertificate;
    }

    /**
     * @return the naicsCode
     */
    public String getNaicsCode() {
	return naicsCode;
    }

    /**
     * @param naicsCode
     *            the naicsCode to set
     */
    public void setNaicsCode(String naicsCode) {
	this.naicsCode = naicsCode;
    }

    /**
     * @return the spendDate
     */
    public Date getSpendDate() {
	return spendDate;
    }

    /**
     * @param spendDate
     *            the spendDate to set
     */
    public void setSpendDate(Date spendDate) {
	this.spendDate = spendDate;
    }

    /**
     * @return the spendValue
     */
    public String getSpendValue() {
	return spendValue;
    }

    /**
     * @param spendValue
     *            the spendValue to set
     */
    public void setSpendValue(String spendValue) {
	this.spendValue = spendValue;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
	return currency;
    }

    /**
     * @param currency
     *            the currency to set
     */
    public void setCurrency(String currency) {
	this.currency = currency;
    }

    /**
     * @return the vendorNaics
     */
    public Integer getVendorNaics() {
	return vendorNaics;
    }

    /**
     * @param vendorNaics
     *            the vendorNaics to set
     */
    public void setVendorNaics(Integer vendorNaics) {
	this.vendorNaics = vendorNaics;
    }

    /**
     * @return the vendorCertificateId
     */
    public Integer getVendorCertificateId() {
	return vendorCertificateId;
    }

    /**
     * @param vendorCertificateId
     *            the vendorCertificateId to set
     */
    public void setVendorCertificateId(Integer vendorCertificateId) {
	this.vendorCertificateId = vendorCertificateId;
    }

    public Byte getIsValid() {
	return isValid;
    }

    public void setIsValid(Byte isValid) {
	this.isValid = isValid;
    }

    /**
     * @return the keyValue
     */
    public Integer getKeyValue() {
	return keyValue;
    }

    /**
     * @param keyValue
     *            the keyValue to set
     */
    public void setKeyValue(Integer keyValue) {
	this.keyValue = keyValue;
    }

}
