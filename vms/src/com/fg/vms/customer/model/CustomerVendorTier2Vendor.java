/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "customer_vendortier2vendor")
public class CustomerVendorTier2Vendor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "PARENTVENDORID")
    private Integer parentvendorid;
    @Column(name = "CREATEDBY")
    private Integer createdby;
    @Column(name = "CREATEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    @Column(name = "ISACTIVE")
    private Short isactive;
    @Column(name = "MODIFIEDBY")
    private Integer modifiedby;
    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedon;
    @JoinColumn(name = "VENDORID", referencedColumnName = "ID")
    @ManyToOne
    private VendorMaster vendorid;

    public CustomerVendorTier2Vendor() {
    }

    public CustomerVendorTier2Vendor(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentvendorid() {
        return parentvendorid;
    }

    public void setParentvendorid(Integer parentvendorid) {
        this.parentvendorid = parentvendorid;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Short getIsactive() {
        return isactive;
    }

    public void setIsactive(Short isactive) {
        this.isactive = isactive;
    }

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifiedon() {
        return modifiedon;
    }

    public void setModifiedon(Date modifiedon) {
        this.modifiedon = modifiedon;
    }

    public VendorMaster getVendorid() {
        return vendorid;
    }

    public void setVendorid(VendorMaster vendorid) {
        this.vendorid = vendorid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof CustomerVendorTier2Vendor)) {
            return false;
        }
        CustomerVendorTier2Vendor other = (CustomerVendorTier2Vendor) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demo.CustomerVendorTier2Vendor[ id=" + id + " ]";
    }

}
