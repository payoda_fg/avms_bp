/*
 * Items.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author shefeek.a
 * 
 */

@Entity
@Table(name = " avms_custom_report_field_selection ")
public class CustomReportSearchFields implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the Items which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "Category")
    private String category;
    
    @Column(name = "inputtablename")
    private String inputTableName;
    
    @Column(name = "inputcolumnname")
    private String inputColumnName;
    
    @Column(name = "columntitle")
    private String columnTitle;
    
    @Column(name = "outputtablename")
    private String outputTableName;
    
    @Column(name = "outputsql")
    private String outputSql;
    
    @Column(name = "wherecondition")
    private String whereCondition;
    
    @Column(name = "jointype")
    private String joinType;
    
    @Column(name = "tableorder")
    private Integer tableOrder;
    
    @Column(name = "columnorder")
    private Integer columnOrder;
    
    @Column(name = "wherecondition2")
    private String whereCondition2;
    
    @Column(name = "wherecondition3")
    private String whereCondition3;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getInputTableName() {
		return inputTableName;
	}

	public void setInputTableName(String inputTableName) {
		this.inputTableName = inputTableName;
	}

	public String getInputColumnName() {
		return inputColumnName;
	}

	public void setInputColumnName(String inputColumnName) {
		this.inputColumnName = inputColumnName;
	}

	public String getColumnTitle() {
		return columnTitle;
	}

	public void setColumnTitle(String columnTitle) {
		this.columnTitle = columnTitle;
	}

	public String getOutputTableName() {
		return outputTableName;
	}

	public void setOutputTableName(String outputTableName) {
		this.outputTableName = outputTableName;
	}

	public String getOutputSql() {
		return outputSql;
	}

	public void setOutputSql(String outputSql) {
		this.outputSql = outputSql;
	}

	public String getWhereCondition() {
		return whereCondition;
	}

	public void setWhereCondition(String whereCondition) {
		this.whereCondition = whereCondition;
	}

	public String getJoinType() {
		return joinType;
	}

	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	public Integer getTableOrder() {
		return tableOrder;
	}

	public void setTableOrder(Integer tableOrder) {
		this.tableOrder = tableOrder;
	}

	public Integer getColumnOrder() {
		return columnOrder;
	}

	public void setColumnOrder(Integer columnOrder) {
		this.columnOrder = columnOrder;
	}

	public String getWhereCondition2() {
		return whereCondition2;
	}

	public void setWhereCondition2(String whereCondition2) {
		this.whereCondition2 = whereCondition2;
	}

	public String getWhereCondition3() {
		return whereCondition3;
	}

	public void setWhereCondition3(String whereCondition3) {
		this.whereCondition3 = whereCondition3;
	}
}
