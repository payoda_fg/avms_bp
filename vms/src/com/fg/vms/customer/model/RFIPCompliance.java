/**
 * RFIPCompliance.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents the RFI and RFP compliance details.
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER_REQUESTIPCOMPLIANCE")
public class RFIPCompliance implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the RFIPCompliance which serves as the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "REQUESTMASTERID", nullable = false)
    private RequestIP requestIP;

    @Column(name = "COMPLIANCEDESCRIPTION", nullable = false)
    private String complianceDescription;

    @Column(name = "COMPLIANCETARGET", nullable = false)
    private char complianceTarget;

    @Column(name = "COMPLIANCEMANDATORY", nullable = false)
    private char complianceMandatory;

    @Column(name = "NOTES", nullable = false)
    private String notes;

    @Column(name = "ORDEROFDISPLAY", nullable = false)
    private Integer orderOfDisplay;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the requestIP
     */
    public RequestIP getRequestIP() {
	return requestIP;
    }

    /**
     * @param requestIP
     *            the requestIP to set
     */
    public void setRequestIP(RequestIP requestIP) {
	this.requestIP = requestIP;
    }

    /**
     * @return the complianceDescription
     */
    public String getComplianceDescription() {
	return complianceDescription;
    }

    /**
     * @param complianceDescription
     *            the complianceDescription to set
     */
    public void setComplianceDescription(String complianceDescription) {
	this.complianceDescription = complianceDescription;
    }

    /**
     * @return the complianceTarget
     */
    public Character getComplianceTarget() {
	return complianceTarget;
    }

    /**
     * @param complianceTarget
     *            the complianceTarget to set
     */
    public void setComplianceTarget(Character complianceTarget) {
	this.complianceTarget = complianceTarget;
    }

    /**
     * @return the complianceMandatory
     */
    public char getComplianceMandatory() {
	return complianceMandatory;
    }

    /**
     * @param complianceMandatory
     *            the complianceMandatory to set
     */
    public void setComplianceMandatory(char complianceMandatory) {
	this.complianceMandatory = complianceMandatory;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
	return notes;
    }

    /**
     * @param notes
     *            the notes to set
     */
    public void setNotes(String notes) {
	this.notes = notes;
    }

    /**
     * @return the orderOfDisplay
     */
    public Integer getOrderOfDisplay() {
	return orderOfDisplay;
    }

    /**
     * @param orderOfDisplay
     *            the orderOfDisplay to set
     */
    public void setOrderOfDisplay(Integer orderOfDisplay) {
	this.orderOfDisplay = orderOfDisplay;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
