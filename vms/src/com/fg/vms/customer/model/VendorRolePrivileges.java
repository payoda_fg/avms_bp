/*
 * VendorRolePrivileges.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents vendor roles privileges details
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "customer_vendor_rolesprivileges")
public class VendorRolePrivileges implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the VendorRolePrivileges which serves as the
     * primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "OBJECTID", nullable = false)
    private VendorObjects objectId;

    @ManyToOne
    @JoinColumn(name = "VENDORUSERROLEID", nullable = false)
    private VendorRoles vendorUserRoleId;

    @Column(name = "ISADD", columnDefinition = "TINYINT(4) DEFAULT 0")
    private Byte add;

    @Column(name = "ISDELETE", columnDefinition = "TINYINT(4) DEFAULT 0")
    private Byte delete;

    @Column(name = "ISMODIFY", columnDefinition = "TINYINT(4) DEFAULT 0")
    private Byte modify;

    @Column(name = "ISVIEW", columnDefinition = "TINYINT(4) DEFAULT 0")
    private Byte view;

    @Column(name = "ISVISIBLE", columnDefinition = "TINYINT(4) DEFAULT 0")
    private Byte visible;

    @Column(name = "ISENABLE", columnDefinition = "TINYINT(4) DEFAULT 0")
    private Byte enable;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte isActive;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the objectId
     */
    public VendorObjects getObjectId() {
	return objectId;
    }

    /**
     * @param objectId
     *            the objectId to set
     */
    public void setObjectId(VendorObjects objectId) {
	this.objectId = objectId;
    }

    /**
     * @return the vendorUserRoleId
     */
    public VendorRoles getVendorUserRoleId() {
	return vendorUserRoleId;
    }

    /**
     * @param vendorUserRoleId
     *            the vendorUserRoleId to set
     */
    public void setVendorUserRoleId(VendorRoles vendorUserRoleId) {
	this.vendorUserRoleId = vendorUserRoleId;
    }

    /**
     * @return the add
     */
    public Byte getAdd() {
	return add;
    }

    /**
     * @param add
     *            the adde to set
     */
    public void setAdd(Byte add) {
	this.add = add;
    }

    /**
     * @return the delete
     */
    public Byte getDelete() {
	return delete;
    }

    /**
     * @param delete
     *            the delete to set
     */
    public void setDelete(Byte delete) {
	this.delete = delete;
    }

    /**
     * @return the modify
     */
    public Byte getModify() {
	return modify;
    }

    /**
     * @param modify
     *            the modify to set
     */
    public void setModify(Byte modify) {
	this.modify = modify;
    }

    /**
     * @return the view
     */
    public Byte getView() {
	return view;
    }

    /**
     * @param view
     *            the view to set
     */
    public void setView(Byte view) {
	this.view = view;
    }

    /**
     * @return the visible
     */
    public Byte getVisible() {
	return visible;
    }

    /**
     * @param visible
     *            the visible to set
     */
    public void setVisible(Byte visible) {
	this.visible = visible;
    }

    /**
     * @return the enable
     */
    public Byte getEnable() {
	return enable;
    }

    /**
     * @param enable
     *            the enable to set
     */
    public void setEnable(Byte enable) {
	this.enable = enable;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }
}
