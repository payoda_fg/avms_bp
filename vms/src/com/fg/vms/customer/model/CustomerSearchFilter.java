/*
 * Items.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author shefeek.a
 * 
 */

@Entity
@Table(name = " customer_savesearchfilter ")
public class CustomerSearchFilter implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the Items which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "SEARCHNAME", nullable = false)
    private String searchName;
    
    @Column(name = "SEARCHDATE", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date searchDate;
    
    @Column(name = "USERID", nullable = false)
    private Integer userId;

    @Column(name = "SEARCHTYPE", nullable = false)
    private String searchType;

    @Column(name = "CRITERIA")
    private String criteria;

    @Column(name = "SEARCHQUERY")
    private String searchQuery;
    
    @ManyToOne
	@JoinColumn(name = "CUSTOMERDIVISIONID", nullable = true)
	private CustomerDivision customerDivisionId;
    
    @Column(name = "CRITERIAJSON")
    private String criteriaJson;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	/**
	 * @return the customerDivisionId
	 */
	public CustomerDivision getCustomerDivisionId() {
		return customerDivisionId;
	}

	/**
	 * @param customerDivisionId the customerDivisionId to set
	 */
	public void setCustomerDivisionId(CustomerDivision customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getCriteriaJson() {
		return criteriaJson;
	}

	public void setCriteriaJson(String criteriaJson) {
		this.criteriaJson = criteriaJson;
	}
}
