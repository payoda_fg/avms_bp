/*
 *VendorMaster.java 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fg.vms.admin.model.Users;

/**
 * Model class represents Vendor Master Registration details
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "customer_vendormaster")
public class VendorMaster implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the VendorMaster which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "VENDORNAME", nullable = true)
	private String vendorName;

	@Column(name = "VENDORCODE", nullable = true, unique = true)
	private String vendorCode;

	@Column(name = "DUNSNUMBER", nullable = true)
	private String dunsNumber;

	@Column(name = "TAXID", nullable = true)
	private String taxId;

	@Column(name = "ADDRESS1")
	private String address1;

	@Column(name = "ADDRESS2")
	private String address2;

	@Column(name = "ADDRESS3")
	private String address3;

	@Column(name = "CITY")
	private String city;

	@Column(name = "STATE")
	private String state;

	@Column(name = "COMPANY_TYPE")
	private String companyType;

	@Column(name = "PROVINCE")
	private String province;

	@Column(name = "ZIPCODE")
	private String zipCode;

	@Column(name = "REGION")
	private String region;

	@Column(name = "COUNTRY")
	private String country;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "MOBILE")
	private String mobile;

	@Column(name = "FAX")
	private String fax;

	@Column(name = "EMAILID")
	private String emailId;

	@Column(name = "WEBSITE")
	private String website;

	@Column(name = "NUMBEROFEMPLOYEES", nullable = true)
	private Integer numberOfEmployees;

	@Column(name = "ANNUALTURNOVER", nullable = true)
	private Double annualTurnover;

	@Column(name = "YEAROFESTABLISHMENT")
	private Integer yearOfEstablishment;

	@Column(name = "DIVSERSUPPLIER", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte deverseSupplier;

	@Column(name = "PRIMENONPRIMEVENDOR", nullable = true)
	private Byte primeNonPrimeVendor;

	@Column(name = "PARENTVENDORID")
	private Integer parentVendorId;

	@Column(name = "ISAPPROVED", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 0")
	private Byte isApproved;

	@Column(name = "ISAPPROVED_DESCRIPTION")
	private String isApprovedDesc;

	@JoinColumn(name = "APPROVEDBY", referencedColumnName = "ID")
	@ManyToOne
	private Users approvedBy;

	@Column(name = "APPROVEDON")
	private Date approvedOn;

	@Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;

	@Column(name = "CREATEDBY")
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
	private List<VendorNAICS> vendornaicsList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
	private List<VendorCertificate> vendorcertificateList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
	private List<VendorOtherCertificate> vendorothercertificateList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
	private List<VendorDocuments> vendorDocuments;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
	private List<VendorContact> vendorUsersList;

	// @OneToOne(mappedBy = "vendorId", cascade =
	// CascadeType.ALL,fetch=FetchType.LAZY)
	// private CustomerWflog customerWflog;

	@Column(name = "isinvited")
	private Short isinvited;

	@Column(name = "anonymousvendorid")
	private Integer anonymousvendorid;

	@JoinColumn(name = "ETHNICITY_ID", referencedColumnName = "id")
	@ManyToOne
	private Ethnicity ethnicityId;

	@Column(name = "MINORITY_OWNERSHIP")
	private Double minorityPercent;

	@Column(name = "WOMEN_OWNERSHIP")
	private Double womenPercent;

	@Column(name = "DIVERSENOTES")
	private String diverseNotes;

	@Column(name = "VENDORNOTES")
	private String vendorNotes;

	@Column(name = "VENDORSTATUS")
	private String vendorStatus;

	@Column(name = "STATUSMODIFIEDON", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date statusModifiedOn;

	@Column(name = "VENDORDESCRIPTION")
	private String vendorDescription;

	@Column(name = "STATEINCORPORATION")
	private String stateIncorporation;

	@Column(name = "STATESALESTAXID")
	private String stateSalesTaxId;

	@Column(name = "COMPANYOWNERSHIP")
	private Short companyownership;

	@Column(name = "BUSINESSTYPE")
	private String businesstype;

	@OneToMany(mappedBy = "vendorid")
	private List<CustomerVendorSales> customerVendorSalesList;

	@OneToMany(mappedBy = "vendorid")
	private List<CustomerVendorOwner> customerVendorownerList;

	@Column(name = "ISPARTIALLYSUBMITTED")
	private String isPartiallySubmitted;

	@Column(name = "COMPANYINFORMATION")
	private String companyInformation;

	@JoinColumn(name = "CUSTOMERDIVISIONID", referencedColumnName = "id", nullable = true)
	@ManyToOne
	private CustomerDivision customerDivisionId;
	
	@Column(name = "BPSEGMENT")
	private Integer bpSegment;
	
	@Column(name = "RFIRFPNOTES")
	private String rfiRfpNotes;

	/**
	 * @return the vendorStatus
	 */
	public String getVendorStatus() {
		return vendorStatus;
	}

	/**
	 * @param vendorStatus
	 *            the vendorStatus to set
	 */
	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}

	/**
	 * @return the statusModifiedOn
	 */
	public Date getStatusModifiedOn() {
		return statusModifiedOn;
	}

	/**
	 * @param statusModifiedOn
	 *            the statusModifiedOn to set
	 */
	public void setStatusModifiedOn(Date statusModifiedOn) {
		this.statusModifiedOn = statusModifiedOn;
	}

	/**
	 * @return the ethnicityId
	 */
	public Ethnicity getEthnicityId() {
		return ethnicityId;
	}

	/**
	 * @param ethnicityId
	 *            the ethnicityId to set
	 */
	public void setEthnicityId(Ethnicity ethnicityId) {
		this.ethnicityId = ethnicityId;
	}

	/**
	 * @return the isinvited
	 */
	public Short getIsinvited() {
		return isinvited;
	}

	/**
	 * @param isinvited
	 *            the isinvited to set
	 */
	public void setIsinvited(Short isinvited) {
		this.isinvited = isinvited;
	}

	/**
	 * @return the anonymousvendorid
	 */
	public Integer getAnonymousvendorid() {
		return anonymousvendorid;
	}

	/**
	 * @param anonymousvendorid
	 *            the anonymousvendorid to set
	 */
	public void setAnonymousvendorid(Integer anonymousvendorid) {
		this.anonymousvendorid = anonymousvendorid;
	}

	/**
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * 
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * 
	 * @return the vendorCode
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * 
	 * @param vendorCode
	 *            the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	/**
	 * 
	 * @return the dunsNumber
	 */
	public String getDunsNumber() {
		return dunsNumber;
	}

	/**
	 * 
	 * @param dunsNumber
	 *            the dunsNumber to set
	 */
	public void setDunsNumber(String dunsNumber) {
		this.dunsNumber = dunsNumber;
	}

	/**
	 * 
	 * @return the taxId
	 */
	public String getTaxId() {
		return taxId;
	}

	/**
	 * 
	 * @param taxId
	 *            the taxId to set
	 */
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	/**
	 * 
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * 
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * 
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * 
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * 
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * 
	 * @param address3
	 *            the address3 to set
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	/**
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * 
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the companyType
	 */
	public String getCompanyType() {
		return companyType;
	}

	/**
	 * @param companyType
	 *            the companyType to set
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	/**
	 * 
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * 
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * 
	 * @return the zipcode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * 
	 * @param zipCode
	 *            the zipcode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * 
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * 
	 * @param region
	 *            the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * 
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * 
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * 
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * 
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * 
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * 
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * 
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * 
	 * @param website
	 *            the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * 
	 * @return the numberofEmployees
	 */
	public Integer getNumberOfEmployees() {
		return numberOfEmployees;
	}

	/**
	 * 
	 * @param numberOfEmployees
	 *            the numberOfEmployees to set
	 */
	public void setNumberOfEmployees(Integer numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	/**
	 * 
	 * @return the annualTurnover
	 */
	public Double getAnnualTurnover() {
		return annualTurnover;
	}

	/**
	 * 
	 * @param annualTurnover
	 *            the annualturnover to set
	 */
	public void setAnnualTurnover(Double annualTurnover) {
		this.annualTurnover = annualTurnover;
	}

	/**
	 * 
	 * @return the yearOfEstablishment
	 */
	public Integer getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	/**
	 * 
	 * @param yearOfEstablishment
	 *            the yearOfEstablishment to set
	 */
	public void setYearOfEstablishment(Integer yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	/**
	 * 
	 * @return the deverseSupplier
	 */
	public Byte getDeverseSupplier() {
		return deverseSupplier;
	}

	/**
	 * 
	 * @param deverseSupplier
	 *            the diverseSupplier to set
	 */
	public void setDeverseSupplier(Byte deverseSupplier) {
		this.deverseSupplier = deverseSupplier;
	}

	/**
	 * 
	 * @return the primeNonPrimeVendor
	 */
	public Byte getPrimeNonPrimeVendor() {
		return primeNonPrimeVendor;
	}

	/**
	 * 
	 * @param primeNonPrimeVendor
	 *            the primeNonPrimeVendor to set
	 */
	public void setPrimeNonPrimeVendor(Byte primeNonPrimeVendor) {
		this.primeNonPrimeVendor = primeNonPrimeVendor;
	}

	/**
	 * 
	 * @return the parentVendorId
	 */
	public Integer getParentVendorId() {
		return parentVendorId;
	}

	/**
	 * 
	 * @param parentVendorId
	 *            the parentVendorId to set
	 */
	public void setParentVendorId(Integer parentVendorId) {
		this.parentVendorId = parentVendorId;
	}

	/**
	 * 
	 * @return the isApproved
	 */
	public Byte getIsApproved() {
		return isApproved;
	}

	/**
	 * 
	 * @param isApproved
	 *            the isApproved to set
	 */
	public void setIsApproved(Byte isApproved) {
		this.isApproved = isApproved;
	}

	/**
	 * @return the isApprovedDesc
	 */
	public String getIsApprovedDesc() {
		return isApprovedDesc;
	}

	/**
	 * @param isApprovedDesc
	 *            the isApprovedDesc to set
	 */
	public void setIsApprovedDesc(String isApprovedDesc) {
		this.isApprovedDesc = isApprovedDesc;
	}

	/**
	 * 
	 * @return the approvedBy
	 */
	public Users getApprovedBy() {
		return approvedBy;
	}

	/**
	 * 
	 * @param approvedBy
	 *            the approvedBy to set
	 */
	public void setApprovedBy(Users approvedBy) {
		this.approvedBy = approvedBy;
	}

	/**
	 * 
	 * @return the approvedOn
	 */
	public Date getApprovedOn() {
		return approvedOn;
	}

	/**
	 * 
	 * @param approvedOn
	 *            the approvedOn to set
	 */
	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public List<VendorNAICS> getVendornaicsList() {
		return vendornaicsList;
	}

	public void setVendornaicsList(List<VendorNAICS> vendornaicsList) {
		this.vendornaicsList = vendornaicsList;
	}

	public List<VendorCertificate> getVendorcertificateList() {
		return vendorcertificateList;
	}

	public void setVendorcertificateList(
			List<VendorCertificate> vendorcertificateList) {
		this.vendorcertificateList = vendorcertificateList;
	}

	public List<VendorOtherCertificate> getVendorothercertificateList() {
		return vendorothercertificateList;
	}

	public void setVendorothercertificateList(
			List<VendorOtherCertificate> vendorothercertificateList) {
		this.vendorothercertificateList = vendorothercertificateList;
	}

	/**
	 * @return the vendorDocuments
	 */
	public List<VendorDocuments> getVendorDocuments() {
		return vendorDocuments;
	}

	/**
	 * @param vendorDocuments
	 *            the vendorDocuments to set
	 */
	public void setVendorDocuments(List<VendorDocuments> vendorDocuments) {
		this.vendorDocuments = vendorDocuments;
	}

	public List<VendorContact> getVendorUsersList() {
		return vendorUsersList;
	}

	public void setVendorUsersList(List<VendorContact> vendorUsersList) {
		this.vendorUsersList = vendorUsersList;
	}

	/**
	 * @return the minorityPercent
	 */
	public Double getMinorityPercent() {
		return minorityPercent;
	}

	/**
	 * @param minorityPercent
	 *            the minorityPercent to set
	 */
	public void setMinorityPercent(Double minorityPercent) {
		this.minorityPercent = minorityPercent;
	}

	/**
	 * @return the womenPercent
	 */
	public Double getWomenPercent() {
		return womenPercent;
	}

	/**
	 * @param womenPercent
	 *            the womenPercent to set
	 */
	public void setWomenPercent(Double womenPercent) {
		this.womenPercent = womenPercent;
	}

	/**
	 * @return the diverseNotes
	 */
	public String getDiverseNotes() {
		return diverseNotes;
	}

	/**
	 * @param diverseNotes
	 *            the diverseNotes to set
	 */
	public void setDiverseNotes(String diverseNotes) {
		this.diverseNotes = diverseNotes;
	}

	/**
	 * @return the vendorNotes
	 */
	public String getVendorNotes() {
		return vendorNotes;
	}

	/**
	 * @param vendorNotes
	 *            the vendorNotes to set
	 */
	public void setVendorNotes(String vendorNotes) {
		this.vendorNotes = vendorNotes;
	}

	/**
	 * @return the vendorDescription
	 */
	public String getVendorDescription() {
		return vendorDescription;
	}

	/**
	 * @param vendorDescription
	 *            the vendorDescription to set
	 */
	public void setVendorDescription(String vendorDescription) {
		this.vendorDescription = vendorDescription;
	}

	/**
	 * @return the stateIncorporation
	 */
	public String getStateIncorporation() {
		return stateIncorporation;
	}

	/**
	 * @param stateIncorporation
	 *            the stateIncorporation to set
	 */
	public void setStateIncorporation(String stateIncorporation) {
		this.stateIncorporation = stateIncorporation;
	}

	/**
	 * @return the stateSalesTaxId
	 */
	public String getStateSalesTaxId() {
		return stateSalesTaxId;
	}

	/**
	 * @param stateSalesTaxId
	 *            the stateSalesTaxId to set
	 */
	public void setStateSalesTaxId(String stateSalesTaxId) {
		this.stateSalesTaxId = stateSalesTaxId;
	}

	/**
	 * @return the companyownership
	 */
	public Short getCompanyownership() {
		return companyownership;
	}

	/**
	 * @param companyownership
	 *            the companyownership to set
	 */
	public void setCompanyownership(Short companyownership) {
		this.companyownership = companyownership;
	}

	/**
	 * @return the businesstype
	 */
	public String getBusinesstype() {
		return businesstype;
	}

	/**
	 * @param businesstype
	 *            the businesstype to set
	 */
	public void setBusinesstype(String businesstype) {
		this.businesstype = businesstype;
	}

	/**
	 * @return the customerVendorSalesList
	 */
	public List<CustomerVendorSales> getCustomerVendorSalesList() {
		return customerVendorSalesList;
	}

	/**
	 * @param customerVendorSalesList
	 *            the customerVendorSalesList to set
	 */
	public void setCustomerVendorSalesList(
			List<CustomerVendorSales> customerVendorSalesList) {
		this.customerVendorSalesList = customerVendorSalesList;
	}

	/**
	 * @return the customerVendorownerList
	 */
	public List<CustomerVendorOwner> getCustomerVendorownerList() {
		return customerVendorownerList;
	}

	/**
	 * @param customerVendorownerList
	 *            the customerVendorownerList to set
	 */
	public void setCustomerVendorownerList(
			List<CustomerVendorOwner> customerVendorownerList) {
		this.customerVendorownerList = customerVendorownerList;
	}

	/**
	 * @return the isPartiallySubmitted
	 */
	public String getIsPartiallySubmitted() {
		return isPartiallySubmitted;
	}

	/**
	 * @param isPartiallySubmitted
	 *            the isPartiallySubmitted to set
	 */
	public void setIsPartiallySubmitted(String isPartiallySubmitted) {
		this.isPartiallySubmitted = isPartiallySubmitted;
	}

	/**
	 * @return the companyInformation
	 */
	public String getCompanyInformation() {
		return companyInformation;
	}

	/**
	 * @param companyInformation
	 *            the companyInformation to set
	 */
	public void setCompanyInformation(String companyInformation) {
		this.companyInformation = companyInformation;
	}

	public CustomerDivision getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(CustomerDivision customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}

	/**
	 * @return the bpSegment
	 */
	public Integer getBpSegment() {
		return bpSegment;
	}

	/**
	 * @param bpSegment the bpSegment to set
	 */
	public void setBpSegment(Integer bpSegment) {
		this.bpSegment = bpSegment;
	}

	public String getRfiRfpNotes() {
		return rfiRfpNotes;
	}

	public void setRfiRfpNotes(String rfiRfpNotes) {
		this.rfiRfpNotes = rfiRfpNotes;
	}
	
}
