package com.fg.vms.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author gpirabu
 */
@Entity
@Table(name = "customer_vendorowner")
public class CustomerVendorOwner implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "OWNERNAME")
    private String ownername;
    @Column(name = "TITLE")
    private String title;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "EXTENSION")
    private String extension;
    @Column(name = "MOBILE")
    private String mobile;
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "OWNERSETHINICITY")
    private Integer ownersethinicity;
    // @Max(value=?) @Min(value=?)//if you know range of your decimal fields
    // consider using these annotations to enforce field validation
    @Column(name = "OWNERSHIPPERCENTAGE")
    private BigDecimal ownershippercentage;
    @Column(name = "ISACTIVE")
    private Short isactive;
    @Column(name = "CREATEDBY")
    private Integer createdby;
    @Column(name = "CREATEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    @Column(name = "MODIFIEDBY")
    private Integer modifiedby;
    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedon;
    @JoinColumn(name = "VENDORID", referencedColumnName = "ID")
    @ManyToOne
    private VendorMaster vendorid;

    public CustomerVendorOwner() {
    }

    public CustomerVendorOwner(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getOwnersethinicity() {
        return ownersethinicity;
    }

    public void setOwnersethinicity(Integer ownersethinicity) {
        this.ownersethinicity = ownersethinicity;
    }

    public BigDecimal getOwnershippercentage() {
        return ownershippercentage;
    }

    public void setOwnershippercentage(BigDecimal ownershippercentage) {
        this.ownershippercentage = ownershippercentage;
    }

    public Short getIsactive() {
        return isactive;
    }

    public void setIsactive(Short isactive) {
        this.isactive = isactive;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifiedon() {
        return modifiedon;
    }

    public void setModifiedon(Date modifiedon) {
        this.modifiedon = modifiedon;
    }

    public VendorMaster getVendorid() {
        return vendorid;
    }

    public void setVendorid(VendorMaster vendorid) {
        this.vendorid = vendorid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof CustomerVendorOwner)) {
            return false;
        }
        CustomerVendorOwner other = (CustomerVendorOwner) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demo.CustomerVendorOwner[ id=" + id + " ]";
    }

}
