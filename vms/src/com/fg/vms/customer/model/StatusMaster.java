/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gpirabu
 */
@Entity
@Table(name = "statusmaster")
@NamedQueries({
    @NamedQuery(name = "StatusMaster.findAll", query = "SELECT s FROM StatusMaster s")})
public class StatusMaster implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Character id;
    @Basic(optional = false)
    @Column(name = "disporder")
    private int disporder;
    @Basic(optional = false)
    @Column(name = "statusname")
    private String statusname;
    @Basic(optional = false)
    @Column(name = "statusdescription")
    private String statusdescription;
    @Basic(optional = false)
    @Column(name = "isactive")
    private short isactive;
    @Column(name = "createdby")
    private Integer createdby;
    @Column(name = "createdon")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;
    @Column(name = "iscertificateexpiryalertreq")
    private short iscertificateexpiryalertreq;

    public StatusMaster() {
    }

    public StatusMaster(Character id) {
        this.id = id;
    }

    public StatusMaster(Character id, int disporder, String statusname, String statusdescription, short isactive) {
        this.id = id;
        this.disporder = disporder;
        this.statusname = statusname;
        this.statusdescription = statusdescription;
        this.isactive = isactive;
    }

    public Character getId() {
        return id;
    }

    public void setId(Character id) {
        this.id = id;
    }

    public int getDisporder() {
        return disporder;
    }

    public void setDisporder(int disporder) {
        this.disporder = disporder;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public String getStatusdescription() {
        return statusdescription;
    }

    public void setStatusdescription(String statusdescription) {
        this.statusdescription = statusdescription;
    }

    public short getIsactive() {
        return isactive;
    }

    public void setIsactive(short isactive) {
        this.isactive = isactive;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public short getIscertificateexpiryalertreq() {
		return iscertificateexpiryalertreq;
	}

	public void setIscertificateexpiryalertreq(short iscertificateexpiryalertreq) {
		this.iscertificateexpiryalertreq = iscertificateexpiryalertreq;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StatusMaster)) {
            return false;
        }
        StatusMaster other = (StatusMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "demo.StatusMaster[ id=" + id + " ]";
    }
    
}
