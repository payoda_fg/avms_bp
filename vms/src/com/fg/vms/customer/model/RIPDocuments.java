/**
 * RIPDocuments.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represent the RFI and RFP Documents details.
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER_REQUESTIPDOCUMENTS")
public class RIPDocuments implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the RIPDocuments which serves as the primary key. */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "REQUESTMASTERID")
    private RequestIP requestIP;

    @Column(name = "REQUESTDOCTITLE")
    private String requestDocTitle;

    @Column(name = "DOCSTOREDPATH")
    private String docStoredPath;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the requestIP
     */
    public RequestIP getRequestIP() {
	return requestIP;
    }

    /**
     * @param requestIP
     *            the requestIP to set
     */
    public void setRequestIP(RequestIP requestIP) {
	this.requestIP = requestIP;
    }

    /**
     * @return the requestDocTitle
     */
    public String getRequestDocTitle() {
	return requestDocTitle;
    }

    /**
     * @param requestDocTitle
     *            the requestDocTitle to set
     */
    public void setRequestDocTitle(String requestDocTitle) {
	this.requestDocTitle = requestDocTitle;
    }

    /**
     * @return the docStoredPath
     */
    public String getDocStoredPath() {
	return docStoredPath;
    }

    /**
     * @param docStoredPath
     *            the docStoredPath to set
     */
    public void setDocStoredPath(String docStoredPath) {
	this.docStoredPath = docStoredPath;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
