/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents vendor references.
 * 
 * @author vinoth
 */
@Entity
@Table(name = "customer_vendorreference")
public class CustomerVendorreference implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@JoinColumn(name = "VENDORID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private VendorMaster vendorId;

	@Column(name = "REFERENCEID")
	private int referenceId;

	@Column(name = "REFERENCEADDRESS")
	private String referenceAddress;

	@Column(name = "REFERENCECITY")
	private String referenceCity;

	@Column(name = "REFERENCECOUNTRY")
	private String referenceCountry;

	@Column(name = "REFERENCEEMAILID")
	private String referenceEmailId;

	@Column(name = "REFERENCEMOBILE")
	private String referenceMobile;

	@Column(name = "REFERENCENAME")
	private String referenceName;
	
	@Column(name = "REFERENCECOMPANYNAME")
	private String referenceCompanyName;

	@Column(name = "REFERENCEPHONE")
	private String referencePhone;

	@Column(name = "REFERENCEEXTENSION")
	private String referenceExtension;

	@Column(name = "REFERENCESTATE")
	private String referenceState;

	@Column(name = "REFERENCEZIP")
	private String referenceZip;

	@Column(name = "REFERENCEPROVINCE")
	private String referenceProvince;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the vendorId
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the referenceId
	 */
	public int getReferenceId() {
		return referenceId;
	}

	/**
	 * @param referenceId
	 *            the referenceId to set
	 */
	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}

	/**
	 * @return the referenceAddress
	 */
	public String getReferenceAddress() {
		return referenceAddress;
	}

	/**
	 * @param referenceAddress
	 *            the referenceAddress to set
	 */
	public void setReferenceAddress(String referenceAddress) {
		this.referenceAddress = referenceAddress;
	}

	/**
	 * @return the referenceCity
	 */
	public String getReferenceCity() {
		return referenceCity;
	}

	/**
	 * @param referenceCity
	 *            the referenceCity to set
	 */
	public void setReferenceCity(String referenceCity) {
		this.referenceCity = referenceCity;
	}

	/**
	 * @return the referenceCountry
	 */
	public String getReferenceCountry() {
		return referenceCountry;
	}

	/**
	 * @param referenceCountry
	 *            the referenceCountry to set
	 */
	public void setReferenceCountry(String referenceCountry) {
		this.referenceCountry = referenceCountry;
	}

	/**
	 * @return the referenceEmailId
	 */
	public String getReferenceEmailId() {
		return referenceEmailId;
	}

	/**
	 * @param referenceEmailId
	 *            the referenceEmailId to set
	 */
	public void setReferenceEmailId(String referenceEmailId) {
		this.referenceEmailId = referenceEmailId;
	}

	/**
	 * @return the referenceMobile
	 */
	public String getReferenceMobile() {
		return referenceMobile;
	}

	/**
	 * @param referenceMobile
	 *            the referenceMobile to set
	 */
	public void setReferenceMobile(String referenceMobile) {
		this.referenceMobile = referenceMobile;
	}

	/**
	 * @return the referenceName
	 */
	public String getReferenceName() {
		return referenceName;
	}

	public String getReferenceCompanyName() {
		return referenceCompanyName;
	}

	public void setReferenceCompanyName(String referenceCompanyName) {
		this.referenceCompanyName = referenceCompanyName;
	}

	/**
	 * @param referenceName
	 *            the referenceName to set
	 */
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	/**
	 * @return the referencePhone
	 */
	public String getReferencePhone() {
		return referencePhone;
	}

	/**
	 * @param referencePhone
	 *            the referencePhone to set
	 */
	public void setReferencePhone(String referencePhone) {
		this.referencePhone = referencePhone;
	}

	/**
	 * @return the referenceExtension
	 */
	public String getReferenceExtension() {
		return referenceExtension;
	}

	/**
	 * @param referenceExtension
	 *            the referenceExtension to set
	 */
	public void setReferenceExtension(String referenceExtension) {
		this.referenceExtension = referenceExtension;
	}

	/**
	 * @return the referenceState
	 */
	public String getReferenceState() {
		return referenceState;
	}

	/**
	 * @param referenceState
	 *            the referenceState to set
	 */
	public void setReferenceState(String referenceState) {
		this.referenceState = referenceState;
	}

	/**
	 * @return the referenceZip
	 */
	public String getReferenceZip() {
		return referenceZip;
	}

	/**
	 * @param referenceZip
	 *            the referenceZip to set
	 */
	public void setReferenceZip(String referenceZip) {
		this.referenceZip = referenceZip;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the referenceProvince
	 */
	public String getReferenceProvince() {
		return referenceProvince;
	}

	/**
	 * @param referenceProvince
	 *            the referenceProvince to set
	 */
	public void setReferenceProvince(String referenceProvince) {
		this.referenceProvince = referenceProvince;
	}

}
