/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class for Vendor meeting information.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_vendormeetinginfo")
public class CustomerVendorMeetingInfo implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the VendorMaster which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "VENDORID", nullable = false)
	private VendorMaster vendorId;

	@Column(name = "CONTACTFIRSTNAME")
	private String contactFirstName;

	@Column(name = "CONTACTLASTNAME")
	private String contactLastName;

	@Column(name = "CONTACTDATE")
	private Date contactDate;

	@Column(name = "CONTACTSTATE")
	private Integer contactState;

	@Column(name = "CONTACTEVENT")
	private String contactEvent;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the vendorId
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName() {
		return contactFirstName;
	}

	/**
	 * @param contactFirstName
	 *            the contactFirstName to set
	 */
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return contactLastName;
	}

	/**
	 * @param contactLastName
	 *            the contactLastName to set
	 */
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	/**
	 * @return the contactDate
	 */
	public Date getContactDate() {
		return contactDate;
	}

	/**
	 * @param contactDate
	 *            the contactDate to set
	 */
	public void setContactDate(Date contactDate) {
		this.contactDate = contactDate;
	}

	/**
	 * @return the contactState
	 */
	public Integer getContactState() {
		return contactState;
	}

	/**
	 * @param contactState
	 *            the contactState to set
	 */
	public void setContactState(Integer contactState) {
		this.contactState = contactState;
	}

	/**
	 * @return the contactEvent
	 */
	public String getContactEvent() {
		return contactEvent;
	}

	/**
	 * @param contactEvent
	 *            the contactEvent to set
	 */
	public void setContactEvent(String contactEvent) {
		this.contactEvent = contactEvent;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

}
