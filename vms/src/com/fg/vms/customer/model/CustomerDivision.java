package com.fg.vms.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author SHEFEEK.A
 */
@Entity
@Table(name = "customer_division")
public class CustomerDivision implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2966550625914215559L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "divisionname", nullable = false)
	private String divisionname;

	@Column(name = "divisionshortname")
	private String divisionshortname;

	@Column(name = "isactive", nullable = false)
	private Byte isactive;

	@Column(name = "createdby", nullable = false)
	private int createdby;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdon", nullable = false, length = 19)
	private Date createdon;

	@Column(name = "modifiedby")
	private Integer modifiedby;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifiedon", length = 19)
	private Date modifiedon;
	
	@Column(name = "checklist", nullable = false)
	private String checkList;
	
	@Column(name = "certificationagencies", nullable = false)
	private String certificationAgencies;
	
	@Column(name = "REGISTRATIONREQUEST", nullable = false)
	private String registrationRequest;
	
	@Column(name = "REGISTRATIONQUESTION", nullable = false)
	private String registrationQuestion;

	@Column(name = "ISGLOBAL")
	private Byte isGlobal;
	
	public CustomerDivision() {
	}

	public CustomerDivision(String divisionname, Byte isactive, int createdby,
			Date createdon) {
		this.divisionname = divisionname;
		this.isactive = isactive;
		this.createdby = createdby;
		this.createdon = createdon;
	}

	public CustomerDivision(String divisionname, String divisionshortname,
			Byte isactive, int createdby, Date createdon, Integer modifiedby,
			Date modifiedon) {
		this.divisionname = divisionname;
		this.divisionshortname = divisionshortname;
		this.isactive = isactive;
		this.createdby = createdby;
		this.createdon = createdon;
		this.modifiedby = modifiedby;
		this.modifiedon = modifiedon;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDivisionname() {
		return this.divisionname;
	}

	public void setDivisionname(String divisionname) {
		this.divisionname = divisionname;
	}

	public String getDivisionshortname() {
		return this.divisionshortname;
	}

	public void setDivisionshortname(String divisionshortname) {
		this.divisionshortname = divisionshortname;
	}

	public Byte isIsactive() {
		return this.isactive;
	}

	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}

	public Byte getIsactive() {
		return isactive;
	}

	public int getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Integer getModifiedby() {
		return this.modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedon() {
		return this.modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}	

	public String getCheckList() {
		return checkList;
	}

	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}	

	public String getCertificationAgencies() {
		return certificationAgencies;
	}

	public void setCertificationAgencies(String certificationAgencies) {
		this.certificationAgencies = certificationAgencies;
	}	

	public String getRegistrationRequest() {
		return registrationRequest;
	}

	public void setRegistrationRequest(String registrationRequest) {
		this.registrationRequest = registrationRequest;
	}

	public String getRegistrationQuestion() {
		return registrationQuestion;
	}

	public void setRegistrationQuestion(String registrationQuestion) {
		this.registrationQuestion = registrationQuestion;
	}

	/**
	 * @return the isGlobal
	 */
	public Byte getIsGlobal() {
		return isGlobal;
	}

	/**
	 * @param isGlobal the isGlobal to set
	 */
	public void setIsGlobal(Byte isGlobal) {
		this.isGlobal = isGlobal;
	}
}
