/**
 * RequestIP.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class represents the RFI AND RFP information.
 * 
 * @author pirabu
 * 
 */
@Entity
@Table(name = "CUSTOMER_REQUESTIP_MASTER")
public class RequestIP implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the RequestIP which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "REQUEST_IP", nullable = false)
    private Character request_ip;

    @Column(name = "REQUESTIPNUMBER", unique = true, nullable = false)
    private String requestIPNumber;

    @Column(name = "REQUESTIPDATE", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestIPDate;

    @Column(name = "CONTACTPERSON")
    private String contactPerson;

    @Column(name = "PHONENUMBER")
    private String phoneNumber;

    @Column(name = "EMAILID", unique = true)
    private String emailId;

    @Column(name = "REQUESTIPSTARTDATE")
    private Date requestIPStartDate;

    @Column(name = "REQUESTIPENDDATE")
    private Date requestIPEndDate;

    @Column(name = "REQUESTIPDESCRIPTION")
    private String requestIPDescription;

    @OneToOne
    @JoinColumn(name = "REQUESTIPCURRENCY")
    private Currency requestIPCurrency;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte active;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the request_ip
     */
    public Character getRequest_ip() {
	return request_ip;
    }

    /**
     * @param request_ip
     *            the request_ip to set
     */
    public void setRequest_ip(Character request_ip) {
	this.request_ip = request_ip;
    }

    /**
     * @return the requestIPNumber
     */
    public String getRequestIPNumber() {
	return requestIPNumber;
    }

    /**
     * @param requestIPNumber
     *            the requestIPNumber to set
     */
    public void setRequestIPNumber(String requestIPNumber) {
	this.requestIPNumber = requestIPNumber;
    }

    /**
     * @return the requestIPDate
     */
    public Date getRequestIPDate() {
	return requestIPDate;
    }

    /**
     * @param requestIPDate
     *            the requestIPDate to set
     */
    public void setRequestIPDate(Date requestIPDate) {
	this.requestIPDate = requestIPDate;
    }

    /**
     * @return the contactPerson
     */
    public String getContactPerson() {
	return contactPerson;
    }

    /**
     * @param contactPerson
     *            the contactPerson to set
     */
    public void setContactPerson(String contactPerson) {
	this.contactPerson = contactPerson;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
	return phoneNumber;
    }

    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
	return emailId;
    }

    /**
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(String emailId) {
	this.emailId = emailId;
    }

    /**
     * @return the requestIPStartDate
     */
    public Date getRequestIPStartDate() {
	return requestIPStartDate;
    }

    /**
     * @param requestIPStartDate
     *            the requestIPStartDate to set
     */
    public void setRequestIPStartDate(Date requestIPStartDate) {
	this.requestIPStartDate = requestIPStartDate;
    }

    /**
     * @return the requestIPEndDate
     */
    public Date getRequestIPEndDate() {
	return requestIPEndDate;
    }

    /**
     * @param requestIPEndDate
     *            the requestIPEndDate to set
     */
    public void setRequestIPEndDate(Date requestIPEndDate) {
	this.requestIPEndDate = requestIPEndDate;
    }

    /**
     * @return the requestIPDescription
     */
    public String getRequestIPDescription() {
	return requestIPDescription;
    }

    /**
     * @param requestIPDescription
     *            the requestIPDescription to set
     */
    public void setRequestIPDescription(String requestIPDescription) {
	this.requestIPDescription = requestIPDescription;
    }

    /**
     * @return the requestIPCurrency
     */
    public Currency getRequestIPCurrency() {
	return requestIPCurrency;
    }

    /**
     * @param requestIPCurrency
     *            the requestIPCurrency to set
     */
    public void setRequestIPCurrency(Currency requestIPCurrency) {
	this.requestIPCurrency = requestIPCurrency;
    }

    /**
     * @return the active
     */
    public Byte getActive() {
	return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(Byte active) {
	this.active = active;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
