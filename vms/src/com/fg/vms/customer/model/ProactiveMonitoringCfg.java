/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author gpirabu
 */
/**
 * @author gpirabu
 * 
 */
@Entity
@Table(name = "customer_proactivemonitoringcfg")
public class ProactiveMonitoringCfg implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;

	@Column(name = "ReccurrencePattern")
	private Integer reccurrencePattern;

	@Column(name = "DailyOption")
	private Integer dailyOption;

	@Column(name = "DailyEveryDayNumber")
	private Integer dailyEveryDayNumber;

	@Column(name = "WeeklyWeekNumber")
	private Integer weeklyWeekNumber;

	@Column(name = "WeeklyWeekDays")
	private String weeklyWeekDays;

	@Column(name = "MonthlyOption")
	private Integer monthlyOption;

	@Column(name = "MonthlyDayNumber")
	private Integer monthlyDayNumber;

	@Column(name = "MonthlyWeekNumber")
	private String monthlyWeekNumber;

	@Column(name = "MonthlyWeekDay")
	private Integer monthlyWeekDay;

	@Column(name = "MonthlyMonthNumber")
	private Integer monthlyMonthNumber;

	@Column(name = "YearlyOption")
	private Integer yearlyOption;

	@Column(name = "YearlyDayNumber")
	private Integer yearlyDayNumber;

	@Column(name = "YearlyWeekNumber")
	private String yearlyWeekNumber;

	@Column(name = "YearlyWeekDayNumber")
	private Integer yearlyWeekDayNumber;

	@Column(name = "YearlyMonthNumber")
	private Integer yearlyMonthNumber;

	@Column(name = "RangeOccurrence")
	private Short rangeOccurrence;

	@Column(name = "RangeStartDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date rangeStartDate;

	@Column(name = "RangeOccurenceOption")
	private Integer rangeOccurenceOption;

	@Column(name = "NumberOfOccurrence")
	private Integer numberOfOccurrence;

	@Column(name = "OccurenceEndate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date occurenceEndate;
	@Column(name = "CreatedBy")
	private Integer createdBy;
	@Column(name = "CreatedOn")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	@Column(name = "ModifiedBy")
	private Integer modifiedBy;
	@Column(name = "ModifiedOn")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public ProactiveMonitoringCfg() {
	}

	public ProactiveMonitoringCfg(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReccurrencePattern() {
		return reccurrencePattern;
	}

	public void setReccurrencePattern(Integer reccurrencePattern) {
		this.reccurrencePattern = reccurrencePattern;
	}

	public Integer getDailyOption() {
		return dailyOption;
	}

	public void setDailyOption(Integer dailyOption) {
		this.dailyOption = dailyOption;
	}

	public Integer getDailyEveryDayNumber() {
		return dailyEveryDayNumber;
	}

	public void setDailyEveryDayNumber(Integer dailyEveryDayNumber) {
		this.dailyEveryDayNumber = dailyEveryDayNumber;
	}

	public Integer getWeeklyWeekNumber() {
		return weeklyWeekNumber;
	}

	public void setWeeklyWeekNumber(Integer weeklyWeekNumber) {
		this.weeklyWeekNumber = weeklyWeekNumber;
	}

	public String getWeeklyWeekDays() {
		return weeklyWeekDays;
	}

	public void setWeeklyWeekDays(String weeklyWeekDays) {
		this.weeklyWeekDays = weeklyWeekDays;
	}

	public Integer getMonthlyOption() {
		return monthlyOption;
	}

	public void setMonthlyOption(Integer monthlyOption) {
		this.monthlyOption = monthlyOption;
	}

	public Integer getMonthlyDayNumber() {
		return monthlyDayNumber;
	}

	public void setMonthlyDayNumber(Integer monthlyDayNumber) {
		this.monthlyDayNumber = monthlyDayNumber;
	}

	public String getMonthlyWeekNumber() {
		return monthlyWeekNumber;
	}

	public void setMonthlyWeekNumber(String monthlyWeekNumber) {
		this.monthlyWeekNumber = monthlyWeekNumber;
	}

	public Integer getMonthlyWeekDay() {
		return monthlyWeekDay;
	}

	public void setMonthlyWeekDay(Integer monthlyWeekDay) {
		this.monthlyWeekDay = monthlyWeekDay;
	}

	public Integer getMonthlyMonthNumber() {
		return monthlyMonthNumber;
	}

	public void setMonthlyMonthNumber(Integer monthlyMonthNumber) {
		this.monthlyMonthNumber = monthlyMonthNumber;
	}

	public Integer getYearlyOption() {
		return yearlyOption;
	}

	public void setYearlyOption(Integer yearlyOption) {
		this.yearlyOption = yearlyOption;
	}

	public Integer getYearlyDayNumber() {
		return yearlyDayNumber;
	}

	public void setYearlyDayNumber(Integer yearlyDayNumber) {
		this.yearlyDayNumber = yearlyDayNumber;
	}

	public String getYearlyWeekNumber() {
		return yearlyWeekNumber;
	}

	public void setYearlyWeekNumber(String yearlyWeekNumber) {
		this.yearlyWeekNumber = yearlyWeekNumber;
	}

	public Integer getYearlyWeekDayNumber() {
		return yearlyWeekDayNumber;
	}

	public void setYearlyWeekDayNumber(Integer yearlyWeekDayNumber) {
		this.yearlyWeekDayNumber = yearlyWeekDayNumber;
	}

	public Integer getYearlyMonthNumber() {
		return yearlyMonthNumber;
	}

	public void setYearlyMonthNumber(Integer yearlyMonthNumber) {
		this.yearlyMonthNumber = yearlyMonthNumber;
	}

	public Short getRangeOccurrence() {
		return rangeOccurrence;
	}

	public void setRangeOccurrence(Short rangeOccurrence) {
		this.rangeOccurrence = rangeOccurrence;
	}

	public Date getRangeStartDate() {
		return rangeStartDate;
	}

	public void setRangeStartDate(Date rangeStartDate) {
		this.rangeStartDate = rangeStartDate;
	}

	public Integer getRangeOccurenceOption() {
		return rangeOccurenceOption;
	}

	public void setRangeOccurenceOption(Integer rangeOccurenceOption) {
		this.rangeOccurenceOption = rangeOccurenceOption;
	}

	public Integer getNumberOfOccurrence() {
		return numberOfOccurrence;
	}

	public void setNumberOfOccurrence(Integer numberOfOccurrence) {
		this.numberOfOccurrence = numberOfOccurrence;
	}

	public Date getOccurenceEndate() {
		return occurenceEndate;
	}

	public void setOccurenceEndate(Date occurenceEndate) {
		this.occurenceEndate = occurenceEndate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof ProactiveMonitoringCfg)) {
			return false;
		}
		ProactiveMonitoringCfg other = (ProactiveMonitoringCfg) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "javaapplication1.CustomerProactivemonitoringcfg[ id=" + id
				+ " ]";
	}

}
