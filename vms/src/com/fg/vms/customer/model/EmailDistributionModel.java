/*
 * EmailDistributionModel.java 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Represents the email distribution details (Ex: EmailDistributionName, Active
 * Status) for communicate with DB
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_emaildistributionlistmaster")
public class EmailDistributionModel implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Items which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EMAILDISTRIBUTIONMASTERID")
	private Integer emailDistributionMasterId;

	@Column(name = "EMAILDISTRIBUTIONLISTNAME", nullable = false)
	private String emailDistributionListName;

	@Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "emailDistributionMasterId")
	private List<EmailDistributionListDetailModel> emailDistributionDetailList;

	/**
	 * @return the emailDistributionMasterId
	 */
	public Integer getEmailDistributionMasterId() {
		return emailDistributionMasterId;
	}

	/**
	 * @param emailDistributionMasterId
	 *            the emailDistributionMasterId to set
	 */
	public void setEmailDistributionMasterId(Integer emailDistributionMasterId) {
		this.emailDistributionMasterId = emailDistributionMasterId;
	}

	/**
	 * @return the emailDistributionListName
	 */
	public String getEmailDistributionListName() {
		return emailDistributionListName;
	}

	/**
	 * @param emailDistributionListName
	 *            the emailDistributionListName to set
	 */
	public void setEmailDistributionListName(String emailDistributionListName) {
		this.emailDistributionListName = emailDistributionListName;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the emailDistributionDetailList
	 */
	public List<EmailDistributionListDetailModel> getEmailDistributionDetailList() {
		return emailDistributionDetailList;
	}

	/**
	 * @param emailDistributionDetailList
	 *            the emailDistributionDetailList to set
	 */
	public void setEmailDistributionDetailList(
			List<EmailDistributionListDetailModel> emailDistributionDetailList) {
		this.emailDistributionDetailList = emailDistributionDetailList;
	}

}
