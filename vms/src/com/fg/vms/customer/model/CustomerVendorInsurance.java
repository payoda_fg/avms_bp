/**
 * CustomerVendorInsurance.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class for supplier insurance.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_supplierinsurance")
public class CustomerVendorInsurance implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the VendorMaster which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "VENDORID", nullable = false)
	private VendorMaster vendorId;

	@Column(name = "INSURANCE")
	private String insurance;

	@Column(name = "INSURANCELIMIT")
	private String limit;

	@Column(name = "PROVIDER")
	private String provider;

	@Column(name = "CARRIERNAME")
	private String carrierName;

	@Column(name = "EXPIRATIONDATE")
	private Date expirationDate;

	@Column(name = "POLICYNUMBER")
	private String policyNumber;

	@Column(name = "ADDITIONALINSURED")
	private String additionalInsured;

	@Column(name = "AGENT")
	private String agent;

	@Column(name = "PHONE")
	private String phoneInsurance;

	@Column(name = "EXTENSION")
	private String extension;

	@Column(name = "FAX")
	private String faxInsurance;

	@Column(name = "POLICYSTATUS")
	private String status;

	@Column(name = "FILENAME")
	private String fileName;

	@Column(name = "PATHNAME")
	private String pathName;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the vendorId
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the insurance
	 */
	public String getInsurance() {
		return insurance;
	}

	/**
	 * @param insurance
	 *            the insurance to set
	 */
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	/**
	 * @return the limit
	 */
	public String getLimit() {
		return limit;
	}

	/**
	 * @param limit
	 *            the limit to set
	 */
	public void setLimit(String limit) {
		this.limit = limit;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider
	 *            the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * @return the carrierName
	 */
	public String getCarrierName() {
		return carrierName;
	}

	/**
	 * @param carrierName
	 *            the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate
	 *            the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the policyNumber
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * @param policyNumber
	 *            the policyNumber to set
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * @return the additionalInsured
	 */
	public String getAdditionalInsured() {
		return additionalInsured;
	}

	/**
	 * @param additionalInsured
	 *            the additionalInsured to set
	 */
	public void setAdditionalInsured(String additionalInsured) {
		this.additionalInsured = additionalInsured;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent
	 *            the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return the phoneInsurance
	 */
	public String getPhoneInsurance() {
		return phoneInsurance;
	}

	/**
	 * @param phoneInsurance
	 *            the phoneInsurance to set
	 */
	public void setPhoneInsurance(String phoneInsurance) {
		this.phoneInsurance = phoneInsurance;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the faxInsurance
	 */
	public String getFaxInsurance() {
		return faxInsurance;
	}

	/**
	 * @param faxInsurance
	 *            the faxInsurance to set
	 */
	public void setFaxInsurance(String faxInsurance) {
		this.faxInsurance = faxInsurance;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the pathName
	 */
	public String getPathName() {
		return pathName;
	}

	/**
	 * @param pathName
	 *            the pathName to set
	 */
	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
