/*
 * CustomerIndirectSpendDataMaster.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents the indirect spend master details (eg..reporting year,
 * totalPrimeSupplierSales, totalCustomerSales etc.) for communicate with DB.
 * 
 * @author pirabu
 */
@Entity
@Table(name = "CUSTOMER_INDIRECT_SPEND_DATA_MASTER")
public class CustomerIndirectSpendDataMaster implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the CustomerIndirectSpendDataMaster which serves as
     * the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "vendorId")
    private VendorContact vendorContact;

    @Column(name = "reportingYear")
    private String reportingYear;

    @Column(name = "reportingPeriodStartDate")
    private Date reportingPeriodStartDate;

    @Column(name = "reportingPeriodEndDate")
    private Date reportingPeriodEndDate;

    @Column(name = "totalPrimeSupplierSales")
    private Double totalPrimeSupplierSales;

    @Column(name = "totalCustomerSales")
    private Double totalCustomerSales;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    public VendorContact getVendorContact() {
	return vendorContact;
    }

    public void setVendorContact(VendorContact vendorContact) {
	this.vendorContact = vendorContact;
    }

    public String getReportingYear() {
	return reportingYear;
    }

    public void setReportingYear(String reportingYear) {
	this.reportingYear = reportingYear;
    }

    /**
     * @return the reportingPeriodStartDate
     */
    public Date getReportingPeriodStartDate() {
	return reportingPeriodStartDate;
    }

    /**
     * @param reportingPeriodStartDate
     *            the reportingPeriodStartDate to set
     */
    public void setReportingPeriodStartDate(Date reportingPeriodStartDate) {
	this.reportingPeriodStartDate = reportingPeriodStartDate;
    }

    /**
     * @return the reportingPeriodEndDate
     */
    public Date getReportingPeriodEndDate() {
	return reportingPeriodEndDate;
    }

    /**
     * @param reportingPeriodEndDate
     *            the reportingPeriodEndDate to set
     */
    public void setReportingPeriodEndDate(Date reportingPeriodEndDate) {
	this.reportingPeriodEndDate = reportingPeriodEndDate;
    }

    public Double getTotalPrimeSupplierSales() {
	return totalPrimeSupplierSales;
    }

    public void setTotalPrimeSupplierSales(Double totalPrimeSupplierSales) {
	this.totalPrimeSupplierSales = totalPrimeSupplierSales;
    }

    public Double getTotalCustomerSales() {
	return totalCustomerSales;
    }

    public void setTotalCustomerSales(Double totalCustomerSales) {
	this.totalCustomerSales = totalCustomerSales;
    }

}
