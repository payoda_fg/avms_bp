/*
 * CustomerUser.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Model class for Customer User Login
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "CUSTOMER_USER")
public class CustomerUser implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the CustomerUser which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = " USERROLEID")
    private CustomerRoles roleId;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "USERLOGIN")
    private String userLogin;

    @Column(name = "USERPASSWORD")
    private String userPassword;

    @Column(name = "USEREMAILID")
    private String userEmailId;

    @Column(name = "CUSTOMERCONTACTID")
    private Integer customerContactId;

    @Column(name = "ISACTIVE")
    private Byte isActive;

    @Column(name = "CREATEDBY")
    private Integer createdBy;

    @Column(name = "CREATEDON")
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    private Date modifiedOn;

    /**
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * 
     * @return the roleId(@link CustomerRoles)
     */
    public CustomerRoles getRoleId() {
	return roleId;
    }

    /**
     * 
     * @param roleId
     *            the CustomerRoles as roleId to set
     */
    public void setRoleId(CustomerRoles roleId) {
	this.roleId = roleId;
    }

    /**
     * 
     * @return the userName
     */
    public String getUserName() {
	return userName;
    }

    /**
     * 
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

    /**
     * 
     * @return the userLogin
     */
    public String getUserLogin() {
	return userLogin;
    }

    /**
     * 
     * @param userLogin
     *            the loginId to set
     */
    public void setUserLogin(String userLogin) {
	this.userLogin = userLogin;
    }

    /**
     * 
     * @return the userPassword
     */
    public String getUserPassword() {
	return userPassword;
    }

    /**
     * 
     * @param userPassword
     *            the password to set
     */
    public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
    }

    /**
     * 
     * @return the userMailId
     */
    public String getUserEmailId() {
	return userEmailId;
    }

    /**
     * 
     * @param userMailId
     *            the mailId to set
     */
    public void setUserEmailId(String userMailId) {
	this.userEmailId = userMailId;
    }

    /**
     * 
     * @return the customerContactId
     */
    public Integer getCustomerContactId() {
	return customerContactId;
    }

    /**
     * 
     * @param customerContactId
     *            the customerContact id to set
     */
    public void setCustomerContactId(Integer customerContactId) {
	this.customerContactId = customerContactId;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
