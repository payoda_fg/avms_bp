/**
 * CustomerVendorSelfRegInfo.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_vendorselfregistrarinfo ")
public class CustomerVendorSelfRegInfo implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the VendorContact which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	/** The email id. */
	@Column(name = "EMAILID", nullable = false, unique = true)
	private String emailId;

	/** The password. */
	@Column(name = "PASSWORD")
	private String password;

	/** The key value. */
	@Column(name = "KEYVALUE")
	private Integer keyValue;

	/** The created on. */
	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	/** The valid upto. */
	@Column(name = "VALIDUPTO")
	private Date validUpto;

	/** The vendor id. */
	@Column(name = "VENDORID")
	private String vendorId;

	/** The vendor type. */
	@Column(name = "VENDORTYPE")
	private String vendorType;
	
	@ManyToOne
	@JoinColumn(name = "CUSTOMERDIVISIONID")
	private CustomerDivision customerDivisionId;
	
	/** The Count Value for Allowing User to Login Once After the Password is Invalid. */
	@Column(name = "COUNT", columnDefinition = "int DEFAULT 0")
	private Integer count;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the keyValue
	 */
	public Integer getKeyValue() {
		return keyValue;
	}

	/**
	 * @param keyValue
	 *            the keyValue to set
	 */
	public void setKeyValue(Integer keyValue) {
		this.keyValue = keyValue;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the validUpto
	 */
	public Date getValidUpto() {
		return validUpto;
	}

	/**
	 * @param validUpto
	 *            the validUpto to set
	 */
	public void setValidUpto(Date validUpto) {
		this.validUpto = validUpto;
	}

	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the vendorType
	 */
	public String getVendorType() {
		return vendorType;
	}

	/**
	 * @param vendorType
	 *            the vendorType to set
	 */
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}

	public CustomerDivision getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(CustomerDivision customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}

	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

}
