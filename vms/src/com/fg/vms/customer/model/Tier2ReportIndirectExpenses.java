/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author pirabu
 */
@Entity
@Table(name = "customer_tier2reportindirectexpenses")
public class Tier2ReportIndirectExpenses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Tier2ReportInDirExpId")
    private Integer tier2ReportInDirExpId;
    @JoinColumn(name = "ethnicityId", referencedColumnName = "id")
    @ManyToOne
    private Ethnicity ethnicityId;
    @Column(name = "IndirectExpenseAmt")
    private Double indirectExpenseAmt;
    @Column(name = "CreatedBy")
    private Integer createdBy;
    @Column(name = "CreatedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Column(name = "ModifiedBy")
    private Integer modifiedBy;
    @Column(name = "ModifiedOn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;
    @JoinColumn(name = "certificateId", referencedColumnName = "ID")
    @ManyToOne
    private Certificate certificateId;
    @JoinColumn(name = "Tier2ReoprtMasterid", referencedColumnName = "Tier2ReportMasterid")
    @ManyToOne
    private Tier2ReportMaster tier2ReoprtMasterid;
    
    @JoinColumn(name = "marketSectorId", referencedColumnName = "ID")
    @ManyToOne
    private MarketSector marketSector;
    
    @JoinColumn(name = "marketSubSectorId", referencedColumnName = "ID")
    @ManyToOne
    private CustomerCommodityCategory marketSubSector;
    
    public CustomerCommodityCategory getMarketSubSector() {
		return marketSubSector;
	}

	public void setMarketSubSector(CustomerCommodityCategory marketSubSector) {
		this.marketSubSector = marketSubSector;
	}

	@JoinColumn(name = "Tier2_Vendorid", referencedColumnName = "ID")
    @ManyToOne
    private VendorMaster tier2IndirectVendorId;
   
    public Tier2ReportIndirectExpenses() {
    }

    public Tier2ReportIndirectExpenses(Integer tier2ReportInDirExpId) {
        this.tier2ReportInDirExpId = tier2ReportInDirExpId;
    }

    public Integer getTier2ReportInDirExpId() {
        return tier2ReportInDirExpId;
    }

    public void setTier2ReportInDirExpId(Integer tier2ReportInDirExpId) {
        this.tier2ReportInDirExpId = tier2ReportInDirExpId;
    }

    public Double getIndirectExpenseAmt() {
        return indirectExpenseAmt;
    }

    public void setIndirectExpenseAmt(Double indirectExpenseAmt) {
        this.indirectExpenseAmt = indirectExpenseAmt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Certificate getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Certificate certificateId) {
        this.certificateId = certificateId;
    }

    public Tier2ReportMaster getTier2ReoprtMasterid() {
        return tier2ReoprtMasterid;
    }

    public void setTier2ReoprtMasterid(Tier2ReportMaster tier2ReoprtMasterid) {
        this.tier2ReoprtMasterid = tier2ReoprtMasterid;
    }

    /**
     * @return the ethnicityId
     */
    public Ethnicity getEthnicityId() {
        return ethnicityId;
    }

    /**
     * @param ethnicityId
     *            the ethnicityId to set
     */
    public void setEthnicityId(Ethnicity ethnicityId) {
        this.ethnicityId = ethnicityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tier2ReportInDirExpId != null ? tier2ReportInDirExpId
                .hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof Tier2ReportIndirectExpenses)) {
            return false;
        }
        Tier2ReportIndirectExpenses other = (Tier2ReportIndirectExpenses) object;
        if ((this.tier2ReportInDirExpId == null && other.tier2ReportInDirExpId != null)
                || (this.tier2ReportInDirExpId != null && !this.tier2ReportInDirExpId
                        .equals(other.tier2ReportInDirExpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sam.Tier2ReportIndirectExpenses[ tier2ReportInDirExpId="
                + tier2ReportInDirExpId + " ]";
    }

	public VendorMaster getTier2IndirectVendorId() {
		return tier2IndirectVendorId;
	}

	public void setTier2IndirectVendorId(VendorMaster tier2IndirectVendorId) {
		this.tier2IndirectVendorId = tier2IndirectVendorId;
	}

	public MarketSector getMarketSector() {
		return marketSector;
	}

	public void setMarketSector(MarketSector marketSector) {
		this.marketSector = marketSector;
	}

	
}
