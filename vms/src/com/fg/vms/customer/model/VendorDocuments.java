/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Model class represents the vendor document details (documentName,
 * documentType, documentDescription)
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_vendordocuments")
public class VendorDocuments implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Certificate which serves as the primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "VENDORID", nullable = false)
	private VendorMaster vendorId;

	@Column(name = "DOCUMENTDESCRIPTION", nullable = true)
	private String documentDescription;

	@Column(name = "DOCUMENTNAME", nullable = true)
	private String documentName;

	@Column(name = "DOCUMENTTYPE", nullable = false)
	private String documentType;

	@Column(name = "DOCUMENTFILENAME", nullable = false)
	private String documentFilename;

	@Column(name = "DOCUMENTPHYSICALPATH", nullable = false)
	private String documentPhysicalpath;

	@Column(name = "DOCUMENTFILESIZE", nullable = false)
	private Double documentFilesize;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;
	
	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;
	
	@Transient
	private String documentSize;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the vendorId
	 */
	public VendorMaster getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId
	 *            the vendorId to set
	 */
	public void setVendorId(VendorMaster vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the documentDescription
	 */
	public String getDocumentDescription() {
		return documentDescription;
	}

	/**
	 * @param documentDescription
	 *            the documentDescription to set
	 */
	public void setDocumentDescription(String documentDescription) {
		this.documentDescription = documentDescription;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName
	 *            the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * @return the documentType
	 */
	public String getDocumentType() {
		return documentType;
	}

	/**
	 * @param documentType
	 *            the documentType to set
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	/**
	 * @return the documentFilename
	 */
	public String getDocumentFilename() {
		return documentFilename;
	}

	/**
	 * @param documentFilename
	 *            the documentFilename to set
	 */
	public void setDocumentFilename(String documentFilename) {
		this.documentFilename = documentFilename;
	}

	/**
	 * @return the documentPhysicalpath
	 */
	public String getDocumentPhysicalpath() {
		return documentPhysicalpath;
	}

	/**
	 * @param documentPhysicalpath
	 *            the documentPhysicalpath to set
	 */
	public void setDocumentPhysicalpath(String documentPhysicalpath) {
		this.documentPhysicalpath = documentPhysicalpath;
	}

	/**
	 * @return the documentFilesize
	 */
	public Double getDocumentFilesize() {
		return documentFilesize;
	}

	/**
	 * @param documentFilesize
	 *            the documentFilesize to set
	 */
	public void setDocumentFilesize(Double documentFilesize) {
		this.documentFilesize = documentFilesize;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getDocumentSize() {
		return documentSize;
	}

	public void setDocumentSize(String documentSize) {
		this.documentSize = documentSize;
	}
}
