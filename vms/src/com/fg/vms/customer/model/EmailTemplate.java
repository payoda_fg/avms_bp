/**
 * 
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Model class that represents Email templates.
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "customer_emailtemplate")
public class EmailTemplate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5735525463955142203L;

	/** Auto generated ID for the Users which serves as the primary key. */
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	/** Name of email template. */
	@Column(name = "EMAILTEMPLATENAME")
	private String emailTemplateName;

	/** Message of the template message. */
	@Column(name = "EMAILTEMPLATEMESSAGE", nullable = false)
	private String emailTemplateMessage;
	
	/** subject of the template message. */
	@Column(name = "EMAILTEMPLATESUBJECT", nullable = false)
	private String emailTemplateSubject;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TEMPLATECODE")	
	private EmailType templateCode;	
	
	@Column(name = "ISACTIVE", columnDefinition = "TINYINT(4) DEFAULT 1")
	private Byte isActive;

	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	@Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	@Column(name = "MODIFIEDON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the emailTemplateName
	 */
	public String getEmailTemplateName() {
		return emailTemplateName;
	}

	/**
	 * @param emailTemplateName
	 *            the emailTemplateName to set
	 */
	public void setEmailTemplateName(String emailTemplateName) {
		this.emailTemplateName = emailTemplateName;
	}

	/**
	 * @return the emailTemplateMessage
	 */
	public String getEmailTemplateMessage() {
		return emailTemplateMessage;
	}

	/**
	 * @param emailTemplateMessage
	 *            the emailTemplateMessage to set
	 */
	public void setEmailTemplateMessage(String emailTemplateMessage) {
		this.emailTemplateMessage = emailTemplateMessage;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the emailTemplateSubject
	 */
	public String getEmailTemplateSubject() {
		return emailTemplateSubject;
	}

	/**
	 * @param emailTemplateSubject the emailTemplateSubject to set
	 */
	public void setEmailTemplateSubject(String emailTemplateSubject) {
		this.emailTemplateSubject = emailTemplateSubject;
	}

	/**
	 * @return the templateCode
	 */
	public EmailType getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode the templateCode to set
	 */
	public void setTemplateCode(EmailType templateCode) {
		this.templateCode = templateCode;
	}	
}
