/**
 * NaicsCategory.java
 */
package com.fg.vms.customer.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents the naics category master details (eg..category description, web
 * service URL, web service parameters etc.) for communicate with DB.
 * 
 * @author Vinoth
 */

@Entity
@Table(name = "CUSTOMER_NAICSCATEGORYMASTER")
public class NaicsCategory implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /** Auto generated ID for the NaicsCategory which serves as the primary key. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAICSCATEGORYDESC", nullable = false)
    private String naicsCategoryDesc;

    @Column(name = "ISACTIVE", columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte isActive;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    @Column(name = "ISWEBSERVICE")
    private Byte isWebService;

    @Column(name = "WEBSERVICEURL")
    private String webserivceURL;

    @Column(name = "WEBSERVICEPARAMETERS")
    private String webserviceParameters;

    /**
     * @return the naicsCategoryDesc
     */
    public String getNaicsCategoryDesc() {
	return naicsCategoryDesc;
    }

    /**
     * @param naicsCategoryDesc
     *            the naicsCategoryDesc to set
     */
    public void setNaicsCategoryDesc(String naicsCategoryDesc) {
	this.naicsCategoryDesc = naicsCategoryDesc;
    }

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

    public Byte getIsWebService() {
	return isWebService;
    }

    public void setIsWebService(Byte isWebService) {
	this.isWebService = isWebService;
    }

    public String getWebserivceURL() {
	return webserivceURL;
    }

    public void setWebserivceURL(String webserivceURL) {
	this.webserivceURL = webserivceURL;
    }

    public String getWebserviceParameters() {
	return webserviceParameters;
    }

    public void setWebserviceParameters(String webserviceParameters) {
	this.webserviceParameters = webserviceParameters;
    }
}
