/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.Tier2ReportDto;

/**
 * @author vinoth
 * 
 */
public interface ReportDashboardDao {

	/**
	 * Get total spend report.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getTotalSpendReport(UserDetailsDto appDetails,
			String year, String period);

	/**
	 * Get direct spend report.
	 * 
	 * @param appDetails
	 * @param spendYear
	 * @param reportingPeriod
	 * @return
	 */
	public List<Tier2ReportDto> getDirectSpendReport(UserDetailsDto appDetails,
			String spendYear, String reportingPeriod);

	/**
	 * Get indirect spend report.
	 * 
	 * @param appDetails
	 * @param spendYear
	 * @param reportingPeriod
	 * @return
	 */
	public List<Tier2ReportDto> getIndirectSpendReport(
			UserDetailsDto appDetails, String spendYear, String reportingPeriod);

	/**
	 * Get diversity spend report.
	 * 
	 * @param appDetails
	 * @param spendYear
	 * @param reportingPeriod
	 * @return
	 */
	public List<Tier2ReportDto> getDiversityReport(UserDetailsDto appDetails,
			String spendYear, String reportingPeriod);

	/**
	 * Get ethnicity analysis report.
	 * 
	 * @param appDetails
	 * @param spendYear
	 * @param reportingPeriod
	 * @return
	 */
	public List<Tier2ReportDto> getEthnicityReport(UserDetailsDto appDetails,
			String spendYear, String reportingPeriod);

	/**
	 * To Get Registered Vendor List in that Particular Period for Dashboard Report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getNewRegistrationsByMonthDashboardReport(UserDetailsDto appDetails);
	
	/**
	 * To Get the List of Vendors by Industry for All Statuses.
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorsByIndustryDashboardReport(UserDetailsDto appDetails);
	
	/**
	 * To Get the List of BP Vendors by Industry for Only BP Vendor.
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getBPVendorsByIndustryDashboardReport(UserDetailsDto appDetails);
	
	/**
	 * To Get the List of Vendors by NAICS Description for All Statuses.
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorsByNAICSDecriptionDashboardReport(UserDetailsDto appDetails);
	
	/**
	 * To Get the List of Vendors by BP Market Sector for All Statuses.
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorsByBPMarketSectorDashboardReport(UserDetailsDto appDetails);
}
