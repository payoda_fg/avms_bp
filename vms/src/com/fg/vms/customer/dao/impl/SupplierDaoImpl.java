/**
 * SupplierDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.SupplierDao;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class SupplierDaoImpl implements SupplierDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * 
	 */
	private static final long serialVersionUID = -7489304612137831099L;

	@Override
	public String createMenuStatusForVendor(VendorMaster vendorMaster,
			Integer userId, UserDetailsDto userDetails) {

		log.info("Inside the new status menu creation method.");
		String result = "success";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {

			/**
			 * To maintain the menu status for vendor wizard menu
			 * process.
			 */
			for (int i = 1; i <= 13; i++) {
				CustomerVendorMenuStatus menuStatus = new CustomerVendorMenuStatus();
				menuStatus.setTab(i);
				if (i == 1) {
					menuStatus.setStatus(1);
				} else {
					menuStatus.setStatus(0);
				}
				menuStatus.setCreatedBy(userId);
				menuStatus.setCreatedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				session.save(menuStatus);
			}
			
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public VendorMaster retreveVendorMaster(Integer supplierId,
			UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		VendorMaster vendorMaster = null;
		try {

			/* View/Retrieve the user based on unique VendorMaster id. */
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", supplierId)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}

			}
		}
		return vendorMaster;
	}

	@Override
	public VendorContact retreveVendorContact(Integer supplierId,
			UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		VendorContact vendorContact = null;
		try {

			/* View/Retrieve the user based on unique VendorContact id. */
			vendorContact = (VendorContact) session.createQuery(
					"From VendorContact where (isPrimaryContact=1 or isPreparer=1) and isActive=1 and vendorId='" + supplierId + "'")
					.uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}

			}
		}
		return vendorContact;
	}

	@Override
	public CustomerVendorMeetingInfo retreveVendorMeetingInfo(
			Integer supplierId, UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		CustomerVendorMeetingInfo meetingInfo = null;
		try {

			/*
			 * View/Retrieve the user based on unique CustomerVendorMeetingInfo
			 * id.
			 */
			meetingInfo = (CustomerVendorMeetingInfo) session.createQuery(
					"From  CustomerVendorMeetingInfo where vendorId='"
							+ supplierId +"'").uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}

			}
		}
		return meetingInfo;
	}

	@Override
	public CustomerVendorAddressMaster retreveAddressMaster(Integer supplierId,
			UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		CustomerVendorAddressMaster addressMaster=null;
		try {
			/*
			 * View/Retrieve the user based on unique CustomerVendorMeetingInfo
			 * id.
			 */
			 addressMaster = (CustomerVendorAddressMaster) session
					.createQuery(
							" From CustomerVendorAddressMaster where vendorId='"
									+ supplierId + "' and addressType='p' ").uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}

			}
		}
		return addressMaster;
	}
}
