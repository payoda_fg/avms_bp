/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author gpirabu
 * 
 */
public class EthnicityDaoImpl<T> implements CURDDao<T> {

	@Override
	public T save(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public T update(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			Ethnicity ethnicity = (Ethnicity) session.get(Ethnicity.class,
					((Ethnicity) entity).getId());
			if (ethnicity != null) {
				ethnicity.setEthnicity(((Ethnicity) entity).getEthnicity());
				ethnicity.setModifiedBy(((Ethnicity) entity).getModifiedBy());
				ethnicity.setModifiedOn(new Date());
				ethnicity.setIsactive((byte) 1);
				session.merge(ethnicity);
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public List<T> list(UserDetailsDto usDetails, String query) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<Ethnicity> ethnicities = null;
		String result = "";
		try {
			session.getTransaction().begin();
			ethnicities = session.createQuery(
					"From Ethnicity where isactive=1 order by ethnicity").list();
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (List<T>) ethnicities;
	}

	@Override
	public T delete(T entity, UserDetailsDto usDetails) {
		 /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(usDetails));

        String result = "";
        try {
            session.beginTransaction();
            //session.delete(entity);
            Ethnicity ethnicity = (Ethnicity) session.get(Ethnicity.class,
					((Ethnicity) entity).getId());
			if (ethnicity != null) {				
				ethnicity.setIsactive((byte) 0);
				session.merge(ethnicity);
			}
            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            exception.printStackTrace();
            session.getTransaction().rollback();
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return (T) result;
	}

	@Override
	public T find(UserDetailsDto usDetails, String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T update1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T delete1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public List<?> findAllByQuery(UserDetailsDto usDetails, String query) {
        // TODO Auto-generated method stub
        return null;
    }
    public List<Ethnicity> listEthnicities(UserDetailsDto usDetails) {
    	/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<Ethnicity> ethnicities = null;
		String result = "";
		try {
			session.getTransaction().begin();
			ethnicities = session.createQuery(
					"From Ethnicity order by ethnicity").list();
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}	
        return ethnicities;
    }

	@Override
	public List<T> listCommoditiesBasedOnSubSector(UserDetailsDto usDetails,
			Integer marketSubSectorId) {
		// TODO Auto-generated method stub
		return null;
	}
}
