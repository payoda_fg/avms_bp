/*
 * NaicsCategoryDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsCategoryDao;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.pojo.NaicsCategoryForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the Naics Category services implementation
 * 
 * @author vinoth
 * 
 */
public class NaicsCategoryDaoImpl implements NaicsCategoryDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NaicsCategoryDto> view(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<NaicsCategory> naicsCategories = null;
        List<NaicsCategoryDto> listNaicsCategoryDtos = null;
        try {
            session.beginTransaction();

            /* Get the list of active categories. */
            naicsCategories = session
                    .createCriteria(
                            com.fg.vms.customer.model.NaicsCategory.class)
                    .add(Restrictions.eq("isActive", (byte) 1))
                    .addOrder(Order.asc("naicsCategoryDesc")).list();
            listNaicsCategoryDtos = packAgencyDtos(naicsCategories);
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return listNaicsCategoryDtos;
    }

    /**
     * Package of Certificate agency details.
     * 
     * @param naicsCategories
     *            .
     * @return
     */
    private List<NaicsCategoryDto> packAgencyDtos(
            List<NaicsCategory> naicsCategories) {

        List<NaicsCategoryDto> listNaicsCategoryDtos = new ArrayList<NaicsCategoryDto>();
        for (NaicsCategory category : naicsCategories) {

            NaicsCategoryDto categoryDto = new NaicsCategoryDto();
            categoryDto.setId(category.getId());
            categoryDto.setNaicsCategoryDesc(category.getNaicsCategoryDesc());
            categoryDto.setActive(getBooleanValue(category.getIsActive()));

            listNaicsCategoryDtos.add(categoryDto);

        }
        return listNaicsCategoryDtos;

    }

    /**
     * Pass byte value to get booleanS.
     * 
     * @param value
     * @return
     */
    private boolean getBooleanValue(Byte value) {

        if (value == 1) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String submit(NaicsCategoryDto naicsCategoryDto, Integer id,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "success";
        try {
            session.beginTransaction();
            NaicsCategory naicsCategory;
            naicsCategory = (NaicsCategory) session
                    .createCriteria(NaicsCategory.class)
                    .add(Restrictions.eq("id", naicsCategoryDto.getId()))
                    .uniqueResult();
            if (naicsCategory != null) {

                log.info(" ============update naics category is starting========== ");

                naicsCategory.setNaicsCategoryDesc(naicsCategoryDto
                        .getNaicsCategoryDesc());

                naicsCategory.setIsActive(getByteValue(naicsCategoryDto
                        .isActive()));

                naicsCategory.setModifiedBy(id);
                naicsCategory.setModifiedOn(new Date());
                session.update(naicsCategory);

                log.info(" ============updated naics category successfully========== ");

                session.getTransaction().commit();
            }

            else {
                naicsCategory = new NaicsCategory();

                log.info(" ============save naics category is starting========== ");
                naicsCategory.setNaicsCategoryDesc(naicsCategoryDto
                        .getNaicsCategoryDesc());

                naicsCategory.setIsActive((byte) 1);

                naicsCategory.setCreatedBy(id);
                naicsCategory.setCreatedOn(new Date());
                naicsCategory.setModifiedBy(id);
                naicsCategory.setModifiedOn(new Date());
                session.save(naicsCategory);

                log.info(" ============saved naics category successfully========== ");
                session.getTransaction().commit();
            }

        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * Using boolean value to get byte.
     * 
     * @param value
     * @return
     */
    private Byte getByteValue(boolean value) {

        if (value) {
            return (byte) 1;
        } else {
            return (byte) 0;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String addCategory(NaicsCategoryForm naicsCategoryForm,
            int currentUserId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "unique";
        try {
            session.beginTransaction();

            /* To check unique entry in category. */
            NaicsCategory naicsCategory = (NaicsCategory) session
                    .createCriteria(NaicsCategory.class)
                    .add(Restrictions.eq("naicsCategoryDesc",
                            naicsCategoryForm.getNaicsCategoryDesc()))
                    .uniqueResult();

            if (naicsCategory != null) {
                return result;
            }

            NaicsCategory category = new NaicsCategory();

            log.info(" ============save naics category is starting========== ");
            category.setNaicsCategoryDesc(naicsCategoryForm
                    .getNaicsCategoryDesc());
            category.setIsActive((byte) 1);
            category.setCreatedBy(currentUserId);
            category.setCreatedOn(new Date());
            category.setModifiedBy(currentUserId);
            category.setModifiedOn(new Date());
            category.setWebserivceURL(naicsCategoryForm.getWebServiceURL());
            category.setWebserviceParameters(naicsCategoryForm
                    .getWebServiceParameters());
            category.setIsWebService(naicsCategoryForm.getIsWebService());
            category.setIsActive(naicsCategoryForm.getIsActive());
            session.save(category);

            log.info(" ============saved naics category successfully========== ");
            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NaicsCategory> listCategory(UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<NaicsCategory> categorymaster = null;
        try {
            session.beginTransaction();
            /* View the list of Naics categories. */
            categorymaster = session.createQuery(
                    "from NaicsCategory ORDER BY naicsCategoryDesc ASC").list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return categorymaster;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteCategory(Integer id, UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";
        NaicsCategory naicsCategory = null;
        try {
            session.beginTransaction();
            log.info(" ============delete naics category is starting========== ");
            naicsCategory = (NaicsCategory) session
                    .get(NaicsCategory.class, id);
            session.delete(naicsCategory);
            session.getTransaction().commit();
            log.info(" ============deleted naics category successfully========== ");
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NaicsCategory retriveCategory(Integer id, UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        NaicsCategory naicsCategory = null;
        try {
            session.beginTransaction();
            /* Get the naics category details based on Id */
            naicsCategory = (NaicsCategory) session
                    .createCriteria(NaicsCategory.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return naicsCategory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateSelectedCategory(NaicsCategoryForm categoryForm,
            Integer id, Integer currentUserId, UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";

        NaicsCategory naicsCategory = null;
        try {
            session.beginTransaction();
            naicsCategory = (NaicsCategory) session
                    .get(NaicsCategory.class, id);
            log.info(" ============update naics category is starting========== ");
            naicsCategory.setNaicsCategoryDesc(categoryForm
                    .getNaicsCategoryDesc());
            naicsCategory.setWebserivceURL(categoryForm.getWebServiceURL());
            naicsCategory.setWebserviceParameters(categoryForm
                    .getWebServiceParameters());
            naicsCategory.setIsWebService(categoryForm.getIsWebService());
            naicsCategory.setIsActive(categoryForm.getIsActive());
            naicsCategory.setModifiedBy(currentUserId);
            naicsCategory.setModifiedOn(new Date());

            session.update(naicsCategory);
            log.info(" ============updated naics category successfully========== ");
            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

}
