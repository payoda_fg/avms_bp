/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.SpendDataDTO;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.SpendUploadDao;
import com.fg.vms.customer.dto.SpendIndirectDto;
import com.fg.vms.customer.model.CustomerIndirectSpendDataDetail;
import com.fg.vms.customer.model.SpendDataMaster;
import com.fg.vms.customer.model.SpendDataUploadDetails;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.SpendUploadForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author pirabu
 * 
 */
public class SpendUploadDaoImpl implements SpendUploadDao {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.fg.vms.customer.dao.SpendUploadDao#uploadSpendData(com.fg.vms.customer
     * .pojo.SpendUploadForm, com.fg.vms.admin.dto.UserDetailsDto,
     * java.lang.Integer)
     */
    @Override
    public SpendDataMaster uploadSpendData(SpendUploadForm form,
            UserDetailsDto appDetails, Integer userId, String filePath,
            VendorContact currentVendor) {

        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        SpendDataMaster spendUpload = new SpendDataMaster();
        try {
            session.beginTransaction();
            VendorMaster primeVendor = (VendorMaster) session
                    .createCriteria(VendorMaster.class)
                    .add(Restrictions.eq("id", currentVendor.getVendorId()
                            .getId())).uniqueResult();

            VendorMaster tire2Vendor = (VendorMaster) session
                    .createCriteria(VendorMaster.class)
                    .add(Restrictions.eq("id",
                            Integer.parseInt(form.getVendor().toString())))
                    .uniqueResult();
            if (tire2Vendor == null) {
                return null;
            }

            // save the master details
            spendUpload.setVendor(tire2Vendor);
            spendUpload.setPrimeVendor(primeVendor);
            spendUpload.setYearOfData(Integer.parseInt(form.getYearOfData()));
            spendUpload.setSpendStartDate(CommonUtils.dateConvertValue(form
                    .getSpendStartDate()));
            spendUpload.setSpendEndDate(CommonUtils.dateConvertValue(form
                    .getSpendEndDate()));
            // spendUpload.setIndirectValue(form.getIndirectValue());
            // spendUpload
            // .setIndirectpercentage(form.getIndirectValuePercentage());
            spendUpload.setDateOfUpload(new java.util.Date());
            spendUpload.setUploadedBy(userId);
            spendUpload.setIsDataImport((byte) 1);

            Blob blob = null;
            try {

                blob = Hibernate.createBlob(form.getFile().getInputStream());
            } catch (FileNotFoundException e) {
                PrintExceptionInLogFile.printException(e);
            } catch (IOException e) {
                PrintExceptionInLogFile.printException(e);
            }

            spendUpload.setApproved((byte) 1);
            spendUpload.setApprovedBy(userId);
            spendUpload.setApprovedOn(new java.util.Date());
            spendUpload.setImportfileContent(blob);
            session.save(spendUpload);

            session.getTransaction().commit();

        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();

        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return spendUpload;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveUploadedSpendData(UserDetailsDto appDetails,
            Integer userId, ArrayList<SpendDataDTO> al_spendDataList,
            SpendDataMaster spendUpload) {
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        try {
            session.beginTransaction();
            // insert uploaded data into the table
            Random random = new java.util.Random();
            for (SpendDataDTO spendData : al_spendDataList) {
                SpendDataUploadDetails dataUpload = new SpendDataUploadDetails();

                Integer randVal = random.nextInt();
                Encrypt encrypt = new Encrypt();
                // encrypted spend value
                String spendValue = encrypt.encryptText(
                        Integer.toString(randVal) + "",
                        spendData.getSpentValue());

                dataUpload.setNaicsCode(spendData.getNaicsCode());
                dataUpload.setSpendDate(CommonUtils.dateConvertValue(spendData
                        .getSpenDate()));
                dataUpload.setSpendType(spendData.getCategory());
                dataUpload.setSpendValue(spendValue);
                dataUpload.setCurrency(spendData.getCurrency());
                dataUpload.setDiverseCertificate(spendData
                        .getDiverseCerificateCode());
                dataUpload.setSpendUpload(spendUpload);
                dataUpload.setIsValid(spendData.getIsValid());
                dataUpload.setKeyValue(randVal);
                session.save(dataUpload);

            }
            session.getTransaction().commit();
            return "spenduploadsuccess";

        } catch (HibernateException e) {
            session.getTransaction().rollback();
            PrintExceptionInLogFile.printException(e);
            return "failure";
        } catch (Exception e) {
            session.getTransaction().rollback();
            PrintExceptionInLogFile.printException(e);
            return "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveProRateSupply(
            ArrayList<SpendIndirectDto> al_spendIndirectDataList,
            SpendDataMaster spendDataMaster, UserDetailsDto appDetails) {

        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        try {
            session.beginTransaction();
            // insert uploaded data into the table
            for (SpendIndirectDto spendIndirectData : al_spendIndirectDataList) {

                CustomerIndirectSpendDataDetail customerIndirectSpendDataDetail = new CustomerIndirectSpendDataDetail();

                Random random = new java.util.Random();
                Integer randVal = random.nextInt();

                Encrypt encrypt = new Encrypt();

                // Encrypt Financial datas

                String directSpendValue = encrypt.encryptText(
                        Integer.toString(randVal) + "", spendIndirectData
                                .getDirectSpendValue().toString());
                String indirectAmtbasedonDirectSalese = encrypt.encryptText(
                        Integer.toString(randVal) + "", spendIndirectData
                                .getDirect_Spendpercent().toString());
                String proRateAmtbasedonSupply = encrypt.encryptText(
                        Integer.toString(randVal) + "", spendIndirectData
                                .getProrated_amount().toString());

                customerIndirectSpendDataDetail
                        .setIndirectAmtbasedonDirectSales(indirectAmtbasedonDirectSalese);
                customerIndirectSpendDataDetail
                        .setIndirectSpendValue(directSpendValue);
                customerIndirectSpendDataDetail
                        .setProRateAmtbasedonSupply(proRateAmtbasedonSupply);
                customerIndirectSpendDataDetail
                        .setDiverseCertificate(spendIndirectData
                                .getDiverseCertificate());
                customerIndirectSpendDataDetail.setSpendUpload(spendDataMaster);
                customerIndirectSpendDataDetail.setKeyValue(randVal);

                session.save(customerIndirectSpendDataDetail);
            }
            session.getTransaction().commit();
            return "spenduploadsuccess";

        } catch (HibernateException e) {
            session.getTransaction().rollback();
            PrintExceptionInLogFile.printException(e);
            return "failure";
        } catch (Exception e) {
            PrintExceptionInLogFile.printException(e);
            return "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateSpendDataMaster(SpendUploadForm spendUploadForm,
            UserDetailsDto appDetails, SpendDataMaster currentSpendMaster) {

        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        try {
            session.beginTransaction();
            Random random = new java.util.Random();
            if (currentSpendMaster != null) {

                Integer randVal = random.nextInt();
                Encrypt encrypt = new Encrypt();

                // Encrypt Financial datas

                String totalCustomerSales = encrypt.encryptText(
                        Integer.toString(randVal) + "",
                        CommonUtils.deformatMoney(
                                spendUploadForm.getTotalDirectSales()
                                        .toString()).toString());
                String totalPrimeSupplierSales = encrypt.encryptText(
                        Integer.toString(randVal) + "",
                        CommonUtils.deformatMoney(
                                spendUploadForm.getTotalPrimeSupplierSales()
                                        .toString()).toString());

                currentSpendMaster.setTotalCustomerSales(totalCustomerSales);
                currentSpendMaster
                        .setTotalPrimeSupplierSales(totalPrimeSupplierSales);
                currentSpendMaster.setKeyValue(randVal);
                session.merge(currentSpendMaster);

            }

            session.getTransaction().commit();
        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();
        } catch (Exception e) {
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<?> retrieveUploadedSpendData(UserDetailsDto userDetails,
            VendorContact currentUser) {
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(userDetails));
        List<SpendDataMaster> spendDataMasters = null;
        try {
            session.beginTransaction();
            spendDataMasters = session
                    .createCriteria(SpendDataMaster.class)
                    .add(Restrictions.eq("primeVendor",
                            currentUser.getVendorId())).list();

            session.getTransaction().commit();
        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();

        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return spendDataMasters;
    }
}
