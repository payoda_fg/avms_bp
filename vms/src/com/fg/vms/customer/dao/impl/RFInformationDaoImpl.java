/*
 * RFInformationDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.RFInformationDao;
import com.fg.vms.customer.dto.Compliance;
import com.fg.vms.customer.dto.RFIDocument;
import com.fg.vms.customer.dto.RFIPDto;
import com.fg.vms.customer.model.ComplianceAttributes;
import com.fg.vms.customer.model.Currency;
import com.fg.vms.customer.model.RFIPCompliance;
import com.fg.vms.customer.model.RIPDocuments;
import com.fg.vms.customer.model.RequestIP;
import com.fg.vms.customer.model.RequestIPVendors;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.RFIInformationForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author pirabu
 * 
 */
public class RFInformationDaoImpl implements RFInformationDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("deprecation")
    @Override
    public String persistRFI(RFIInformationForm information, List<?> documents,
            List<?> compliances, Integer userId, UserDetailsDto appDetails) {

        String result;

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        try {
            session.beginTransaction();
            // Set the RFI master details to entity from UI.
            RequestIP requestip = (RequestIP) session
                    .createCriteria(RequestIP.class)
                    .add(Restrictions.eq("requestIPNumber",
                            information.getRfiNumber())).uniqueResult();
            if (requestip != null) {
                return "unique";
            }
            RequestIP requestIP = new RequestIP();

            log.info(" ============save request ip is starting========== ");
            requestIP.setContactPerson(information.getContactPerson());
            requestIP.setEmailId(information.getEmailId());
            requestIP.setPhoneNumber(information.getPhoneNumber());
            requestIP.setRequestIPDate(new Date());
            requestIP.setRequestIPDescription(information.getRfiDescription());
            requestIP.setRequest_ip(information.getRfType().charAt(0));
            requestIP.setRequestIPNumber(information.getRfiNumber());
            requestIP
                    .setRequestIPEndDate(new Date(information.getRfiEndDate()));
            requestIP.setRequestIPStartDate(new Date(information
                    .getRfiStartDate()));

            // Get the currency object to set to RFI
            Currency currency = (Currency) session.get(Currency.class,
                    Integer.parseInt(information.getCurrency()));
            requestIP.setRequestIPCurrency(currency);
            requestIP.setActive((byte) 1);
            requestIP.setCreatedBy(userId);
            requestIP.setCreatedOn(new Date());
            requestIP.setModifiedBy(userId);
            requestIP.setModifiedOn(new Date());

            // Set the selected vendor to entity from UI.
            List<RequestIPVendors> ripVendors = new ArrayList<RequestIPVendors>();
            if (information.getVendors() != null
                    && information.getVendors().length != 0) {
                for (String vendorId : information.getVendors()) {
                    RequestIPVendors vendor = new RequestIPVendors();

                    VendorMaster master = (VendorMaster) session.get(
                            VendorMaster.class, Integer.parseInt(vendorId));

                    vendor.setVendor(master);
                    vendor.setRequestIP(requestIP);
                    vendor.setCreatedBy(userId);
                    vendor.setCreatedOn(new Date());
                    vendor.setModifiedBy(userId);
                    vendor.setModifiedOn(new Date());

                    ripVendors.add(vendor);

                }
            }

            // Set the RFI Documents information to entity
            List<RIPDocuments> ripDocuments = new ArrayList<RIPDocuments>();

            for (Object document : documents) {
                RIPDocuments doc = new RIPDocuments();

                doc.setRequestIP(requestIP);
                doc.setDocStoredPath(((RFIDocument) document).getDocPath());
                doc.setRequestDocTitle(((RFIDocument) document).getTitle());

                doc.setCreatedBy(userId);
                doc.setCreatedOn(new Date());
                doc.setModifiedBy(userId);
                doc.setModifiedOn(new Date());

                ripDocuments.add(doc);
            }
            // Set the RFI Compliance information
            List<RFIPCompliance> rfipCompliances = new ArrayList<RFIPCompliance>();
            // Set the RFI Compliance Attributes
            List<ComplianceAttributes> complianceAttributes = new ArrayList<ComplianceAttributes>();

            for (Object comp : compliances) {
                RFIPCompliance compliance = new RFIPCompliance();

                log.info(" ============save compliance is starting========== ");
                compliance.setComplianceDescription(((Compliance) comp)
                        .getComplianceItem());
                compliance.setComplianceMandatory(((Compliance) comp)
                        .getMandatory());
                compliance.setComplianceTarget(((Compliance) comp).getTarget());
                compliance.setNotes(((Compliance) comp).getNote());
                compliance.setRequestIP(requestIP);
                // compliance.setWeigtage(new BigDecimal(123));
                compliance.setOrderOfDisplay(1);
                compliance.setCreatedBy(userId);
                compliance.setCreatedOn(new Date());
                compliance.setModifiedBy(userId);
                compliance.setModifiedOn(new Date());

                for (int i = 0; i < ((Compliance) comp).getAttributes().length; i++) {
                    ComplianceAttributes attribute = new ComplianceAttributes();
                    attribute.setRfipCompliance(compliance);
                    attribute.setRequestIP(requestIP);
                    attribute.setAttributeParticulars(((Compliance) comp)
                            .getAttributes()[i].toString());
                    if (((Compliance) comp).getWeightages() != null
                            && ((Compliance) comp).getWeightages().length != 0) {
                        attribute.setWeigtage(new BigDecimal(
                                ((Compliance) comp).getWeightages()[i]
                                        .toString()));
                    }

                    attribute.setCreatedBy(userId);
                    attribute.setCreatedOn(new Date());
                    attribute.setModifiedBy(userId);
                    attribute.setModifiedOn(new Date());

                    complianceAttributes.add(attribute);
                }

                rfipCompliances.add(compliance);
            }

            // persist the RFI
            session.save(requestIP);
            for (RIPDocuments document : ripDocuments) {
                session.save(document);
            }

            for (RFIPCompliance compliance : rfipCompliances) {
                session.save(compliance);
            }

            for (RequestIPVendors ipVendors : ripVendors) {
                session.save(ipVendors);
            }
            for (ComplianceAttributes attributes : complianceAttributes) {
                session.save(attributes);
            }

            result = "success";
            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            result = "failure";
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RFIPDto> retrieveRFInformations(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        List<RequestIP> rfi = null;
        List<RFIPDto> rfInformations = new ArrayList<RFIPDto>();
        try {
            session.beginTransaction();
            rfi = session.createQuery(
                    "From RequestIP where request_ip='I' and active=1 ").list();
            session.getTransaction().commit();
            for (RequestIP rfInformation : rfi) {
                RFIPDto dashboard = new RFIPDto();

                log.info(" ============save dashboard is starting========== ");
                dashboard.setId(rfInformation.getId());
                dashboard.setRfipNumber(rfInformation.getRequestIPNumber());
                dashboard.setStartDate(CommonUtils
                        .convertDateToString(rfInformation
                                .getRequestIPStartDate()));
                dashboard.setEndDate(CommonUtils
                        .convertDateToString(rfInformation
                                .getRequestIPEndDate()));
                dashboard.setDescription(rfInformation
                        .getRequestIPDescription());

                rfInformations.add(dashboard);

            }
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return rfInformations;
    }

    @Override
    public RFIPDto retriveRFIP(Integer requestIPId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        RFIPDto rfipDto = new RFIPDto();
        try {
            session.beginTransaction();
            RequestIP requestIP = (RequestIP) session
                    .createCriteria(RequestIP.class)
                    .add(Restrictions.and(Restrictions.eq("id", requestIPId),
                            Restrictions.eq("active", (byte) 1)))
                    .uniqueResult();
            List<RFIPCompliance> compliances = session
                    .createCriteria(RFIPCompliance.class)
                    .add(Restrictions.eq("requestIP", requestIP)).list();
            List<RequestIPVendors> ipVendors = session
                    .createCriteria(RequestIPVendors.class)
                    .add(Restrictions.eq("requestIP", requestIP)).list();
            rfipDto.setRequestInformation(requestIP);
            rfipDto.setCompliances(compliances);
            rfipDto.setIpVendors(ipVendors);

            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return rfipDto;
    }

    @Override
    public List<RFIPDto> retrieveRFPInformations(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<RequestIP> rfi = null;
        List<RFIPDto> rfInformations = new ArrayList<RFIPDto>();
        try {
            session.beginTransaction();
            rfi = session.createQuery(
                    "From RequestIP where request_ip='P' and active=1 ").list();
            session.getTransaction().commit();
            for (RequestIP rfInformation : rfi) {
                RFIPDto dashboard = new RFIPDto();
                dashboard.setId(rfInformation.getId());
                dashboard.setRfipNumber(rfInformation.getRequestIPNumber());
                dashboard.setStartDate(CommonUtils
                        .convertDateToString(rfInformation
                                .getRequestIPStartDate()));
                dashboard.setEndDate(CommonUtils
                        .convertDateToString(rfInformation
                                .getRequestIPEndDate()));
                dashboard.setDescription(rfInformation
                        .getRequestIPDescription());

                rfInformations.add(dashboard);

            }
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return rfInformations;
    }
}
