/*
 * ItemsDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ItemsDao;
import com.fg.vms.customer.model.Items;
import com.fg.vms.customer.pojo.ItemForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the item services implementation.
 * 
 * @author hemavaishnavi
 * 
 */
public class ItemsDaoImpl implements ItemsDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * @{inheritDoc
     */
    @Override
    public String addItem(ItemForm itemsForm, Integer userid,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = findItemByItemCode(itemsForm, appDetails);
        try {
            session.beginTransaction();
            if (result.equals("submit")) {
                Items items = new Items();

                log.info(" ============save items is starting========== ");

                items.setItemDescription(itemsForm.getItemDescription());
                items.setItemCode(itemsForm.getItemCode());
                items.setItemCategory(itemsForm.getItemCategory());
                items.setItemManufacturer(itemsForm.getItemManufacturer());
                items.setItemUom(itemsForm.getItemUom());
                items.setIsActive((byte) 1);
                items.setCreatedBy(userid);
                items.setCreatedOn(new Date());
                items.setModifiedBy(userid);
                items.setModifiedOn(new Date());
                session.save(items);

                log.info(" ============saved items successfully========== ");

                session.getTransaction().commit();
                result = "success";
            } else {
                return result;
            }
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Items> listItems(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        session.beginTransaction();
        List<Items> items = null;
        try {
            items = session.createQuery(
                    "from Items where isActive=1 order by itemDescription asc")
                    .list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return items;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Items retriveItem(Integer itemId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        session.beginTransaction();
        Items item = null;
        try {
            /* Get the item details based on unique item Id */
            item = (Items) session.createCriteria(Items.class)
                    .add(Restrictions.eq("itemid", itemId)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return item;
    }

    /**
     * This method takes the itemCode and check the database to determine if the
     * itemCode is already registered in the system.
     * 
     * @param itemsForm
     *            Incoming itemCode from the item form
     * @return result already associated. If not, return submit
     */
    private String findItemByItemCode(ItemForm itemsForm,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        String result = "submit";
        try {
            session.beginTransaction();

            /* Get the item details based on unique item Id */
            Items itemcode = (Items) session.createCriteria(Items.class)
                    .add(Restrictions.eq("itemCode", itemsForm.getItemCode()))
                    .uniqueResult();

            if (itemcode != null) {
                result = "itemcode";
            }
        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateItem(ItemForm itemsForm, Integer itemid,
            Integer userid, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";

        Items item = null;
        try {
            session.beginTransaction();
            item = (Items) session.get(Items.class, itemid);

            log.info(" ============update items is starting========== ");

            item.setItemDescription(itemsForm.getItemDescription());
            item.setItemCode(itemsForm.getItemCode());
            item.setItemCategory(itemsForm.getItemCategory());
            item.setItemManufacturer(itemsForm.getItemManufacturer());
            item.setItemUom(itemsForm.getItemUom());
            item.setModifiedBy(userid);
            item.setModifiedOn(new Date());
            session.update(item);

            log.info(" ============updated items successfully========== ");

            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteItem(Integer itemid, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";
        Items items = null;
        try {
            session.beginTransaction();
            log.info(" ============update items is starting========== ");
            items = (Items) session.get(Items.class, itemid);
            items.setIsActive((byte) 0);
            session.update(items);
            session.getTransaction().commit();
            log.info(" ============updated items successfully========== ");
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }
}
