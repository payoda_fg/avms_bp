/*
 * VendorRolesDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorRolePrivileges;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the vendor role services implementation
 * 
 */
public class VendorRolesDaoImpl implements RolesDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserRolesForm> viewRoles(UserDetailsDto userDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(userDetails));

        List<VendorRoles> vendorRoles = null;
        List<UserRolesForm> roles = null;
        try {
            session.beginTransaction();
            vendorRoles = session.createCriteria(VendorRoles.class)
                    .add(Restrictions.eq("isActive", (byte) 1)).list();
            roles = packageRoles(vendorRoles);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            PrintExceptionInLogFile.printException(e);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return roles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteRole(Integer id, UserDetailsDto userDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(userDetails));
        session.beginTransaction();
        String result = "reference";
        VendorRoles vendorrole = null;
        try {

            /* Get the vendor role details based on Id */
            vendorrole = (VendorRoles) session
                    .createCriteria(VendorRoles.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            // userrole.setIsActive((byte) 0);
            if (vendorrole != null) {

                List<VendorContact> users = session
                        .createCriteria(VendorContact.class)
                        .add(Restrictions.eq("vendorUserRoleId", vendorrole))
                        .list();

                List<VendorRolePrivileges> rolePrivileges = session
                        .createCriteria(VendorRolePrivileges.class)
                        .add(Restrictions.eq("vendorUserRoleId", vendorrole))
                        .list();

                if ((users != null && users.size() != 0)
                        || (rolePrivileges != null && rolePrivileges.size() != 0)) {
                    return "reference";

                } else {
                    session.delete(vendorrole);
                    result = "deletesuccess";
                }

            }
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
            return "reference";

        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String addRole(UserRolesForm userRolesForm, Integer currentUser,
            UserDetailsDto userDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(userDetails));
        session.beginTransaction();
        String result = "";

        /* check the role already associated or not. */
        VendorRoles role = (VendorRoles) session
                .createCriteria(VendorRoles.class)
                .add(Restrictions.eq("roleName", userRolesForm.getRoleName()))
                .uniqueResult();
        if (role != null) {
            return "notUnique";
        }
        VendorRoles vendorrole = new VendorRoles();
        try {

            log.info(" ============save vendor role is starting========== ");

            vendorrole.setRoleName(userRolesForm.getRoleName());
            vendorrole.setIsActive((byte) 1);
            vendorrole.setCreatedBy(currentUser);
            vendorrole.setCreatedOn(new Date());
            vendorrole.setModifiedBy(currentUser);
            vendorrole.setModifiedOn(new Date());
            session.save(vendorrole);

            log.info(" ============saved vendor role successfully========== ");

            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateRole(UserRolesForm userRolesForm,
            Integer currentUserId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        session.beginTransaction();
        String result = "";
        VendorRoles vendorrole = new VendorRoles();
        try {
            vendorrole = (VendorRoles) session.get(VendorRoles.class,
                    currentUserId);

            log.info(" ============update vendor role into inactive is starting========== ");

            vendorrole.setRoleName(userRolesForm.getRoleName());

            session.update(vendorrole);

            log.info(" ============vendor role updated into inactive successfully========== ");

            session.getTransaction().commit();

            result = "success";
        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VendorRoles retriveRole(Integer id, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        session.beginTransaction();

        VendorRoles vendorRole = null;
        try {
            vendorRole = (VendorRoles) session.get(VendorRoles.class, id);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return vendorRole;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String checkDuplicateRole(String roleName, UserDetailsDto appDetails) {

        Session session = null;

        /* session object represents customer/FG user database dynamically. */
        if (appDetails.getUserType() != null
                && appDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();
        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(appDetails));
        }

        String result = "";
        try {
            session.beginTransaction();

            /* Get the unique vendor role */
            VendorRoles role = (VendorRoles) session
                    .createCriteria(VendorRoles.class)
                    .add(Restrictions.eq("roleName", roleName)).uniqueResult();
            if (role != null) {
                return "notUnique";
            }
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (appDetails.getUserType() != null
                    && !(appDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;

    }

    private List<UserRolesForm> packageRoles(List<VendorRoles> vendorRoles) {
        List<UserRolesForm> vendorRolesForms = new ArrayList<UserRolesForm>();
        for (VendorRoles vendorRole : vendorRoles) {
            UserRolesForm vendorRoleForm = new UserRolesForm();
            vendorRoleForm.setRoleName(vendorRole.getRoleName());
            vendorRoleForm.setId(vendorRole.getId());
            if (vendorRole.getIsActive().byteValue() == (byte) 1) {
                vendorRoleForm.setIsActive("on");
            } else {
                vendorRoleForm.setIsActive("");
            }
            vendorRolesForms.add(vendorRoleForm);
        }
        return vendorRolesForms;
    }
}
