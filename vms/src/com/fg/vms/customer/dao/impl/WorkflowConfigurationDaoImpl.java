/* 
 * WorkflowConfigurationDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.MailNotificationDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerWflog;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.WorkflowConfigurationForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SendEmail;

/**
 * This class implements the interface WorkflowConfigurationDao
 * 
 * @author vinoth
 * 
 */
public class WorkflowConfigurationDaoImpl implements WorkflowConfigurationDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EmailDistributionModel> view(UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<EmailDistributionModel> emailDistributionModels = null;
		try {
			session.beginTransaction();

			/* Get the list of active email distribution list. */
			emailDistributionModels = session
					.createCriteria(EmailDistributionModel.class)
					.add(Restrictions.eq("isActive", (byte) 1))
					.addOrder(Order.asc("emailDistributionListName")).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return emailDistributionModels;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createWorkflowConfigurationSettings(
			WorkflowConfigurationForm workflowConfigurationForm,
			int currentUserId, UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		try {
			session.beginTransaction();

			WorkflowConfiguration configurationModel = new WorkflowConfiguration();

			/* To get email distribution object. */
			EmailDistributionModel distributionModel = (EmailDistributionModel) session
					.get(EmailDistributionModel.class,
							workflowConfigurationForm
									.getEmailDistributionMasterId());

			/* To get email distribution object. */
			EmailDistributionModel tier2DistributionModel = (EmailDistributionModel) session
					.get(EmailDistributionModel.class,
							workflowConfigurationForm.getTier2ReportEmailDist());
			/* To get email distribution object. */
			EmailDistributionModel adminGroup = (EmailDistributionModel) session
					.get(EmailDistributionModel.class,
							workflowConfigurationForm.getAdminGroup());

			/* Save the email distribution details after validation. */

			log.info(" ============save workflow configuration settings is starting========== ");

			configurationModel.setNaicsCodeAM(workflowConfigurationForm
					.getNaicsCodeAM());
			configurationModel.setTier2EmailDist(tier2DistributionModel);
			configurationModel.setFollowWF(workflowConfigurationForm
					.getFollowWF());
			configurationModel.setDistributionEmail(workflowConfigurationForm
					.getDistributionEmail());
			configurationModel.setAssessmentRequired(workflowConfigurationForm
					.getAssessmentRequired());
			configurationModel.setAssessmentCompleted(workflowConfigurationForm
					.getAssessmentCompleted());
			configurationModel.setApprovalEmail(workflowConfigurationForm
					.getApprovalEmail());
			configurationModel.setCertificateEmail(workflowConfigurationForm
					.getCertificateEmail());
			configurationModel
					.setCertificateEmailSummaryAlert(workflowConfigurationForm
							.getCertificateEmailSummaryAlert());
			configurationModel
					.setDistributionEmailPercent(workflowConfigurationForm
							.getDistributionEmailPercent());
			configurationModel
					.setAssessmentRequiredPercent(workflowConfigurationForm
							.getAssessmentRequiredPercent());
			configurationModel
					.setAssessmentCompletedPercent(workflowConfigurationForm
							.getAssessmentCompletedPercent());
			configurationModel
					.setApprovalEmailPercent(workflowConfigurationForm
							.getApprovalEmailPercent());
			configurationModel.setEmailDistributionMasterId(distributionModel);
			configurationModel.setAdminGroup(adminGroup);
			configurationModel.setDaysUpload(workflowConfigurationForm
					.getDaysUpload());
			configurationModel.setDaysExpiry(workflowConfigurationForm
					.getDaysExpiry());
			configurationModel.setDaysInactive(workflowConfigurationForm
					.getDaysInactive());
			configurationModel.setReportDue(workflowConfigurationForm
					.getReportDueDays());
			configurationModel.setDocumentUpload(workflowConfigurationForm
					.getDocumentUpload());
			configurationModel.setDocumentSize(workflowConfigurationForm
					.getDocumentSize());
			configurationModel
					.setReportNotSubmittedReminderDate(workflowConfigurationForm
							.getReportNotSubmittedReminderDate());
			if (workflowConfigurationForm.getInternationalMode() != null) {
				configurationModel
						.setInternationalMode(workflowConfigurationForm
								.getInternationalMode());

				if (workflowConfigurationForm.getInternationalMode() == 0) {
					if (workflowConfigurationForm.getDefaultCountry() != null
							&& workflowConfigurationForm.getDefaultCountry() != 0) {
						// Setting Previous Default Country Value 'isDefault =
						// 0'
						Country oldDefaultCountry = (Country) session
								.createCriteria(Country.class)
								.add(Restrictions.eq("isDefault", 1))
								.uniqueResult();
						oldDefaultCountry.setModifiedby(currentUserId);
						oldDefaultCountry.setModifiedon(new Date());
						oldDefaultCountry.setIsDefault(0);
						session.merge(oldDefaultCountry);

						// Setting Current Default Country Value 'isDefault = 1'
						Country newDefaultCountry = (Country) session.get(
								Country.class,
								workflowConfigurationForm.getDefaultCountry());
						newDefaultCountry.setModifiedby(currentUserId);
						newDefaultCountry.setModifiedon(new Date());
						newDefaultCountry.setIsDefault(1);
						session.merge(newDefaultCountry);
					}
				}
			} else {
				configurationModel.setInternationalMode(0);
			}

			if (workflowConfigurationForm.getBpSegment() != null) {
				configurationModel.setBpSegment(workflowConfigurationForm
						.getBpSegment());
			} else {
				configurationModel.setBpSegment(0);
			}

			if (workflowConfigurationForm.getStatus() != null
					&& !workflowConfigurationForm.getStatus().equalsIgnoreCase(
							"0")) {
				/* To get Status. */
				StatusMaster statusMaster = (StatusMaster) session.createQuery(
						"From StatusMaster where id = '"
								+ workflowConfigurationForm.getStatus() + "'")
						.uniqueResult();
				configurationModel.setStatus(statusMaster);
			} else {
				configurationModel.setStatus(null);
			}

			if (workflowConfigurationForm.getIsDisplaySDF() != null) {
				configurationModel.setIsDisplaySdf(workflowConfigurationForm.getIsDisplaySDF());
			} else {
				configurationModel.setIsDisplaySdf(0);
			}
			configurationModel.setCcEmailId(workflowConfigurationForm
					.getCcMailId());

			configurationModel.setSupportMailId(workflowConfigurationForm
					.getSupportMailId());

			session.save(configurationModel);

			log.info(" ============saved email distribution successfully========== ");
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WorkflowConfiguration listWorkflowConfig(UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<WorkflowConfiguration> workflowConfigurations = null;
		WorkflowConfiguration workflowConfiguration = null;
		try {
			session.beginTransaction();

			/* Get the list of active workflow configuration list. */
			workflowConfigurations = session.createQuery(
					"from WorkflowConfiguration").list();
			if (workflowConfigurations != null
					&& workflowConfigurations.size() != 0) {
				workflowConfiguration = workflowConfigurations.get(0);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return workflowConfiguration;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateWorkflowConfigurationSettings(
			WorkflowConfigurationForm workflowConfigurationForm,
			int currentUserId, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		WorkflowConfiguration workflowConfiguration = null;
		try {
			session.beginTransaction();

			/* To get workflow configuration object. */
			workflowConfiguration = (WorkflowConfiguration) session.get(
					WorkflowConfiguration.class,
					workflowConfigurationForm.getWorkflowConfigId());

			/* To get the email distribution master object. */
			EmailDistributionModel distributionModel = (EmailDistributionModel) session
					.get(EmailDistributionModel.class,
							workflowConfigurationForm
									.getEmailDistributionMasterId());
			/* To get email distribution object. */
			EmailDistributionModel adminGroup = (EmailDistributionModel) session
					.get(EmailDistributionModel.class,
							workflowConfigurationForm.getAdminGroup());
			/* Update the workflow configuration details. */

			EmailDistributionModel tier2DistributionModel = (EmailDistributionModel) session
					.get(EmailDistributionModel.class,
							workflowConfigurationForm.getTier2ReportEmailDist());

			log.info(" ============update workflow configuration settings is starting========== ");

			workflowConfiguration.setNaicsCodeAM(workflowConfigurationForm
					.getNaicsCodeAM());
			workflowConfiguration.setFollowWF(workflowConfigurationForm
					.getFollowWF());
			if (workflowConfigurationForm.getFollowWF() == 0) {
				workflowConfiguration.setDistributionEmail(0);
				workflowConfiguration.setAssessmentRequired(0);
				workflowConfiguration.setAssessmentCompleted(0);
				workflowConfiguration.setApprovalEmail(0);
				workflowConfiguration.setCertificateEmail(0);
				workflowConfiguration.setCertificateEmailSummaryAlert(0);
			} else {
				workflowConfiguration
						.setDistributionEmail(workflowConfigurationForm
								.getDistributionEmail());
				workflowConfiguration
						.setAssessmentRequired(workflowConfigurationForm
								.getAssessmentRequired());
				workflowConfiguration
						.setAssessmentCompleted(workflowConfigurationForm
								.getAssessmentCompleted());
				workflowConfiguration
						.setApprovalEmail(workflowConfigurationForm
								.getApprovalEmail());
				workflowConfiguration
						.setCertificateEmail(workflowConfigurationForm
								.getCertificateEmail());
				workflowConfiguration
						.setCertificateEmailSummaryAlert(workflowConfigurationForm
								.getCertificateEmailSummaryAlert());
			}

			workflowConfiguration
					.setDistributionEmailPercent(workflowConfigurationForm
							.getDistributionEmailPercent());
			workflowConfiguration
					.setAssessmentRequiredPercent(workflowConfigurationForm
							.getAssessmentRequiredPercent());
			workflowConfiguration
					.setAssessmentCompletedPercent(workflowConfigurationForm
							.getAssessmentCompletedPercent());
			workflowConfiguration
					.setApprovalEmailPercent(workflowConfigurationForm
							.getApprovalEmailPercent());
			workflowConfiguration
					.setEmailDistributionMasterId(distributionModel);
			workflowConfiguration.setTier2EmailDist(tier2DistributionModel);
			workflowConfiguration.setAdminGroup(adminGroup);
			workflowConfiguration.setDaysUpload(workflowConfigurationForm
					.getDaysUpload());
			workflowConfiguration.setDaysExpiry(workflowConfigurationForm
					.getDaysExpiry());
			workflowConfiguration.setDaysInactive(workflowConfigurationForm
					.getDaysInactive());
			workflowConfiguration.setReportDue(workflowConfigurationForm
					.getReportDueDays());
			workflowConfiguration.setDocumentUpload(workflowConfigurationForm
					.getDocumentUpload());
			workflowConfiguration.setDocumentSize(workflowConfigurationForm
					.getDocumentSize());
			workflowConfiguration
					.setReportNotSubmittedReminderDate(workflowConfigurationForm
							.getReportNotSubmittedReminderDate());
			if (workflowConfigurationForm.getInternationalMode() != null) {
				workflowConfiguration
						.setInternationalMode(workflowConfigurationForm
								.getInternationalMode());

				if (workflowConfigurationForm.getInternationalMode() == 0) {
					if (workflowConfigurationForm.getDefaultCountry() != null
							&& workflowConfigurationForm.getDefaultCountry() != 0) {
						// Setting Previous Default Country Value 'isDefault =
						// 0'
						Country oldDefaultCountry = (Country) session
								.createCriteria(Country.class)
								.add(Restrictions.eq("isDefault", 1))
								.uniqueResult();
						oldDefaultCountry.setModifiedby(currentUserId);
						oldDefaultCountry.setModifiedon(new Date());
						oldDefaultCountry.setIsDefault(0);
						session.merge(oldDefaultCountry);

						// Setting Current Default Country Value 'isDefault = 1'
						Country newDefaultCountry = (Country) session.get(
								Country.class,
								workflowConfigurationForm.getDefaultCountry());
						newDefaultCountry.setModifiedby(currentUserId);
						newDefaultCountry.setModifiedon(new Date());
						newDefaultCountry.setIsDefault(1);
						session.merge(newDefaultCountry);
					}
				}
			} else {
				workflowConfiguration.setInternationalMode(0);
			}

			if (workflowConfigurationForm.getBpSegment() != null) {
				workflowConfiguration.setBpSegment(workflowConfigurationForm
						.getBpSegment());
			} else {
				workflowConfiguration.setBpSegment(0);
			}

			if (workflowConfigurationForm.getStatus() != null
					&& !workflowConfigurationForm.getStatus().equalsIgnoreCase(
							"0")) {
				/* To get Status. */
				StatusMaster statusMaster = (StatusMaster) session.createQuery(
						"From StatusMaster where id = '"
								+ workflowConfigurationForm.getStatus() + "'")
						.uniqueResult();
				workflowConfiguration.setStatus(statusMaster);
			} else {
				workflowConfiguration.setStatus(null);
			}

			if (workflowConfigurationForm.getIsDisplaySDF() != null) {
				workflowConfiguration.setIsDisplaySdf(workflowConfigurationForm.getIsDisplaySDF());
			} else {
				workflowConfiguration.setIsDisplaySdf(0);
			}
			workflowConfiguration.setCcEmailId(workflowConfigurationForm
					.getCcMailId());

			workflowConfiguration.setSupportMailId(workflowConfigurationForm
					.getSupportMailId());

			session.update(workflowConfiguration);

			session.getTransaction().commit();
			log.info(" ============updated email distribution successfully========== ");
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	@Override
	public List<String> distributionListEmail(UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<String> emails = new ArrayList<String>();
		try {
			session.beginTransaction();
			List<WorkflowConfiguration> workflowConfigurations = null;
			WorkflowConfiguration workflowConfiguration = null;
			/* Get the list of active workflow configuration list. */
			workflowConfigurations = session.createQuery(
					"from WorkflowConfiguration").list();
			if (workflowConfigurations != null
					&& workflowConfigurations.size() != 0) {
				workflowConfiguration = workflowConfigurations.get(0);
			}
			if (workflowConfiguration != null
					&& workflowConfiguration.getEmailDistributionMasterId() != null) {
				for (EmailDistributionListDetailModel distributionList : workflowConfiguration
						.getEmailDistributionMasterId()
						.getEmailDistributionDetailList()) {
					if (distributionList.getUserId() != null) {
						emails.add(distributionList.getUserId()
								.getUserEmailId());
					} else if (distributionList.getVendorId() != null) {
						emails.add(distributionList.getVendorId().getEmailId());
					}
				}
			}
			log.info(" ============updated email distribution successfully========== ");
			session.getTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emails;
	}

	@Override
	public void updateCustomerWfLog(VendorMaster vendor, String type,
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			log.info(" ============update cwfl is starting========== ");
			CustomerWflog cwfl = null;
			List<CustomerWflog> cwflList = session.createQuery(
					"From CustomerWflog where vendorId=" + vendor.getId())
					.list();
			if (cwflList != null && cwflList.size() != 0) {
				cwfl = cwflList.get(0);
			} else {
				cwfl = new CustomerWflog();
				cwfl.setVendorId(vendor);
			}
			if (type.equalsIgnoreCase("completed")) {
				cwfl.setCapabilityCompleted('Y');
			} else if (type.equalsIgnoreCase("approved")) {
				cwfl.setSendEmailVendorApproved('Y');
			}
			session.saveOrUpdate(cwfl);
			log.info(" ============updated cwfl successfully========== ");
			session.getTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	@Override
	public CustomerWflog findByVendorId(UserDetailsDto appDetails, Integer id) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<CustomerWflog> customerWflogs = null;
		CustomerWflog wflog = null;
		try {
			session.beginTransaction();

			/* Get the list of active workflow configuration list. */
			customerWflogs = session
					.createQuery("from CustomerWflog where vendorId.id=:id")
					.setParameter("id", id).list();
			if (customerWflogs != null && customerWflogs.size() != 0) {
				wflog = customerWflogs.get(0);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return wflog;
	}

	@Override
	public void followWorkFlow(Session session, UserDetailsDto appDetails,
			VendorMaster vendorMaster, String url, Integer userID) {
		List<VendorContact> contacts = session.createQuery(
				"From VendorContact where isPrimaryContact=1 and vendorId="
						+ vendorMaster.getId()).list();
		VendorContact contact = null;
		String password = null;
		Decrypt decrypt = new Decrypt();
		if (contacts != null && !contacts.isEmpty()) {
			contact = contacts.get(0);
			Integer keyvalue = contact.getKeyValue();
			password = decrypt.decryptText(String.valueOf(keyvalue.toString()),
					contact.getPassword());
		}
		List<WorkflowConfiguration> workflowConfigurations = null;
		WorkflowConfiguration workflowConfiguration = null;
		/*
		 * Get the list of active workflow configuration list.
		 */
		workflowConfigurations = session.createQuery(
				"from WorkflowConfiguration").list();
		if (workflowConfigurations != null
				&& workflowConfigurations.size() != 0) {
			workflowConfiguration = workflowConfigurations.get(0);
		}

		CustomerWflog cwfl = new CustomerWflog();
		cwfl.setVendorId(vendorMaster);
		if (workflowConfiguration != null) {
			// workflow is enable
			if (workflowConfiguration.getFollowWF() == 1) {
				System.out.println(" -------------"
						+ workflowConfiguration.getEmailDistributionMasterId()
								.getEmailDistributionDetailList().size());

				String companyName = appDetails.getCurrentCustomer()
						.getCustomerId().getCustName();

				// Send mail to distribution list and assign the
				// capability assessment
				if (null != workflowConfiguration.getDistributionEmail()
						&& workflowConfiguration.getDistributionEmail() == 1) {
					SendEmail mail = new SendEmail();
					WorkflowConfigurationDao conf = new WorkflowConfigurationDaoImpl();

					// calling a method to get template message from
					// db(customer_emailtemplate)
					Integer customerDivisionId = null;
					if (null != appDetails.getCustomerDivisionID()
							&& 0 != appDetails.getCustomerDivisionID()) {
						customerDivisionId = appDetails.getCustomerDivisionID();
					}
					EmailTemplate emailTemplate = mail.getTemplates(10,
							appDetails, customerDivisionId);
					String content = emailTemplate.getEmailTemplateMessage();
					content = content.replace("#companyName", companyName);
					content = content.replace("#url", url);
					String subject = emailTemplate.getEmailTemplateSubject();

					/*Commented*/
//					String result11 = mail.sendMailToDistribution(
//							conf.distributionListEmail(appDetails), subject,
//							content, appDetails);
//					if (result11.equalsIgnoreCase("success")) {
//						cwfl.setSendEmailVendorCreated('Y');
//					}
				}

				// Make Capability Statement when vendor created

				if (workflowConfiguration.getAssessmentRequired() != null
						&& workflowConfiguration.getAssessmentRequired() == 1) {
					String subject = "Capability Assessment ";

					String content = "You have been added to "
							+ companyName
							+ " Company Supplier Portal, please click here "
							+ url
							+ " and complete the Supplier Assessment to be consider as a preferred supplier for "
							+ "doing business with "
							+ companyName
							+ " Company.  Please find your login credentials in the body of this email"
							+ "\n\n User name : " + contact.getEmailId()
							+ "\n Password :" + password;
					MailNotificationDao notification = new MailNotificationDaoImpl();
					String result12 = notification.assignAssessmentToVendor(
							userID, vendorMaster, subject, content, contact,
							appDetails, session);
					if (result12.equalsIgnoreCase("success")) {
						cwfl.setMakeCapability('Y');
					}
				}
			}
		}
		session.save(cwfl);
	}

	@Override
	public void followWorkFlowForApprove(Session session,
			UserDetailsDto appDetails, VendorMaster vendorMaster, String url,String modifiedstatus,
			Integer userID) {
		List<VendorContact> contacts = session.createQuery(
				"From VendorContact where isPrimaryContact=1 and vendorId="
						+ vendorMaster.getId()).list();
		VendorContact contact = null;
		String password = null;
		String status=modifiedstatus;
		Decrypt decrypt = new Decrypt();
		if (contacts != null && !contacts.isEmpty()) {
			contact = contacts.get(0);
			Integer keyvalue = contact.getKeyValue();
			password = decrypt.decryptText(String.valueOf(keyvalue.toString()),
					contact.getPassword());
		}
		List<WorkflowConfiguration> workflowConfigurations = null;
		WorkflowConfiguration workflowConfiguration = null;
		/*
		 * Get the list of active workflow configuration list.
		 */
		workflowConfigurations = session.createQuery(
				"from WorkflowConfiguration").list();
		if (workflowConfigurations != null
				&& workflowConfigurations.size() != 0) {
			workflowConfiguration = workflowConfigurations.get(0);
		}

		CustomerWflog cwfl = new CustomerWflog();
		cwfl.setVendorId(vendorMaster);
		if (workflowConfiguration != null) {
			// workflow is enable
			if (workflowConfiguration.getFollowWF() == 1) {
				System.out.println(" -------------"
						+ workflowConfiguration.getEmailDistributionMasterId()
								.getEmailDistributionDetailList().size());
				if (workflowConfiguration.getApprovalEmail() != null
						&& workflowConfiguration.getApprovalEmail() == 1) {

					/*Commented*/
					StatusMaster oldStatusMaster =new StatusMaster();
					StatusMaster newStatusMaster =new StatusMaster();
					String oldStatus =vendorMaster.getVendorStatus();
					if(oldStatus!=null){
						oldStatusMaster = (StatusMaster) session.createQuery(
							"From StatusMaster where id = '"
									+oldStatus + "'")
							.uniqueResult();
					}
					if(status!=null){
						newStatusMaster = (StatusMaster) session.createQuery(
								"From StatusMaster where id = '"
										+status + "'")
								.uniqueResult();
					}
					String oldStatusName =oldStatusMaster.getStatusname();
					String newStatusName=newStatusMaster.getStatusname();
					if(oldStatusName!=null&&newStatusName!=null){
					SendEmail email = new SendEmail();
					String result12 = email.sendVendorStatusChange(
							vendorMaster.getId(), appDetails, url,newStatusName,oldStatusName, "ACTIVE");
					if (result12.equalsIgnoreCase("success")) {
						cwfl.setSendEmailVendorApproved('Y');
					}
					}
				}
			}
		}
		session.save(cwfl);
	}
}
