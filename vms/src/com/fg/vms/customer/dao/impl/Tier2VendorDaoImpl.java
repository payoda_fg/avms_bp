/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.Tier2VendorDao;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.model.AnonymousVendor;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerDiverseClassification;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorTier2Vendor;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author gpirabu
 * 
 */
public class Tier2VendorDaoImpl implements Tier2VendorDao {
	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String saveShortTire2Vendor(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId,
			List<DiverseCertificationTypesDto> certifications,
			List<NaicsCodesDto> naicsCodes, VendorContact currentVendor,
			UserDetailsDto userdetails, String url, String[] divCert,
			String address, String city, String contactPhone,
			String contanctEmail, String deverseSupplier, String firstName,
			String lastName, String middleName, String state,
			String vendorName, String zipcode, String country) {
		logger.info("Inside the save short Tire2 vendor method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		VendorMaster parentVendor = null;
		CommonDao commonDao = new CommonDaoImpl();
		// result = validateVendorCode(vendorForm.getVendorCode(), appDetails);

		if (result.equals("success")) {
			// String result1 = validateLoginId(vendorForm.getLoginId(),
			// appDetails);
			// if (result1.equalsIgnoreCase("submit")) {
			String emailExist = commonDao.checkDuplicateEmail(contanctEmail,
					userdetails);
			if (emailExist.equalsIgnoreCase("submit")) {

				Session session1 = HibernateUtilCustomer.buildSessionFactory()
						.openSession(
								DataBaseConnection.getConnection(userdetails));

				try {
					session1.beginTransaction();
					parentVendor = (VendorMaster) session1
							.createCriteria(VendorMaster.class)
							.add(Restrictions.eq("id", currentVendor
									.getVendorId().getId())).uniqueResult();
					vendorMaster = new VendorMaster();

					// Save VendorMaster Company Information
					vendorMaster.setParentVendorId(parentVendor.getId());
					vendorMaster.setVendorName(vendorName);

					vendorMaster.setNumberOfEmployees(Integer.parseInt("0"));
					vendorMaster.setAnnualTurnover(CommonUtils
							.deformatMoney("0"));
					vendorMaster.setAddress1(address);
					vendorMaster.setCity(city);
					vendorMaster.setCountry(country);
					vendorMaster.setState(state);
					vendorMaster.setPhone("");
					vendorMaster.setYearOfEstablishment(Integer.parseInt("0"));
					vendorMaster.setDeverseSupplier(CommonUtils
							.getCheckboxValue(deverseSupplier));

					vendorMaster.setPrimeNonPrimeVendor(CommonUtils
							.getByteValue(false));
					vendorMaster.setIsApproved((byte) 1);
					vendorMaster.setApprovedOn(new Date());

					// vendorMaster.setApprovedBy(userId);// no need for
					// approval process
					vendorMaster.setIsActive((byte) 1);
					vendorMaster.setIsinvited((short) 1);
					vendorMaster.setCreatedBy(userId);
					vendorMaster.setCreatedOn(new Date());
					vendorMaster.setModifiedBy(userId);
					vendorMaster.setModifiedOn(new Date());
					vendorMaster.setMinorityPercent(Double.valueOf(0));
					vendorMaster.setWomenPercent(Double.valueOf(0));
					vendorMaster.setDiverseNotes("");
					vendorMaster.setIsPartiallySubmitted("No");
					vendorMaster.setVendorStatus("S");
					
					if(userdetails.getSettings()!= null && userdetails.getSettings().getIsDivision() != null && userdetails.getSettings().getIsDivision() != 0)
					{
						if(userdetails.getCustomerDivisionID() != null)
						{
							if(userdetails.getCustomerDivisionID() != 0){
								CustomerDivision custDivision = (CustomerDivision)session1.get(CustomerDivision.class, userdetails.getCustomerDivisionID());									
								vendorMaster.setCustomerDivisionId(custDivision);
							}
							else
							{
								CustomerDivision custDivision = (CustomerDivision)session1.get(CustomerDivision.class,3);									
								vendorMaster.setCustomerDivisionId(custDivision);
							}
						}
					}
					
					// Ethnicity
					Ethnicity ethnicity = null;

					vendorMaster.setEthnicityId(ethnicity);

					session1.save(vendorMaster);

					CustomerVendorAddressMaster addressMaster = new CustomerVendorAddressMaster();
					addressMaster.setVendorId(vendorMaster);
					addressMaster.setAddressType("p");
					addressMaster.setAddress(address);
					addressMaster.setCity(city);
					addressMaster.setCountry(country);
					addressMaster.setState(state);
					addressMaster.setProvince("");
					addressMaster.setPhone("");
					addressMaster.setRegion("");
					addressMaster.setZipCode(zipcode);
					addressMaster.setMobile("");
					addressMaster.setFax("");
					addressMaster.setIsActive((byte) 1);
					addressMaster.setCreatedBy(userId);
					addressMaster.setCreatedOn(new Date());
					session1.save(addressMaster);

					CustomerVendorTier2Vendor customerVendorTier2Vendor = new CustomerVendorTier2Vendor();
					customerVendorTier2Vendor.setVendorid(vendorMaster);
					customerVendorTier2Vendor.setParentvendorid(parentVendor
							.getId());
					customerVendorTier2Vendor.setCreatedby(userId);
					customerVendorTier2Vendor.setCreatedon(new Date());
					customerVendorTier2Vendor.setIsactive((short) 1);

					session1.save(customerVendorTier2Vendor);

					// Save VendorContact Information
					VendorContact vendorContact = new VendorContact();
					VendorRoles roles = (VendorRoles) session1
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);

					vendorContact.setVendorId(vendorMaster);
					vendorContact.setVendorUserRoleId(roles);

					Random random = new java.util.Random();
					Integer randVal = random.nextInt();
					Encrypt encrypt = new Encrypt();
					String psw = RandomStringUtils.randomAlphanumeric(5);
					// convert password to encrypted password
					String encyppassword = encrypt.encryptText(
							Integer.toString(randVal) + "", psw);
					vendorContact.setFirstName(firstName);
					vendorContact.setLastName(lastName);
					vendorContact.setPhoneNumber(contactPhone);
					vendorContact.setDesignation("0");
					vendorContact.setMobile("0");
					vendorContact.setFax("0");
					vendorContact.setEmailId(contanctEmail);
					vendorContact.setIsAllowedLogin(CommonUtils
							.getByteValue(true));
					vendorContact.setIsPrimaryContact((byte) 1);
					// vendorContact.setLoginDisplayName("");
					// vendorContact.setLoginId(RandomStringUtils
					// .randomAlphabetic(5));
					vendorContact.setPassword(encyppassword);
					vendorContact.setKeyValue(randVal);
					vendorContact.setIsSysGenPassword((byte) 0);

					SecretQuestion secretQns = new SecretQuestion();
					secretQns = (SecretQuestion) session1.get(
							SecretQuestion.class, 1);

					vendorContact.setSecretQuestionId(secretQns);
					vendorContact.setSecQueAns("");
					vendorContact.setIsActive((byte) 1);
					vendorContact.setCreatedBy(userId);
					vendorContact.setCreatedOn(new Date());
					vendorContact.setModifiedBy(userId);
					vendorContact.setModifiedOn(new Date());
					vendorContact.setIsBusinessContact((byte) 0);
					vendorContact.setIsPreparer((byte) 1);
					session1.save(vendorContact);

					// Iterate each diverse classifications from UI
					if (divCert != null && divCert.length > 0) {
						for (String cert : divCert) {
							CustomerDiverseClassification classification1 = new CustomerDiverseClassification();
							Certificate certMaster = (Certificate) session1
									.get(Certificate.class,
											Integer.parseInt(cert));
							classification1.setCertMasterId(certMaster);
							classification1.setVendorId(vendorMaster);
							classification1.setDiverseQuality((byte) 0);
							classification1.setIsActive((byte) 1);
							classification1.setCreatedBy(userId);
							classification1.setCreatedOn(new Date());
							session1.save(classification1);
						}
					}

					logger.info("Transaction commited.");

					session1.getTransaction().commit();

				} catch (HibernateException ex) {
					// ex.printStackTrace();
					PrintExceptionInLogFile.printException(ex);
					session1.getTransaction().rollback();
					ex.printStackTrace();
					return "failure";
				} catch (Exception e) {
					PrintExceptionInLogFile.printException(e);
					e.printStackTrace();
				} finally {
					if (session1.isConnected()) {
						session1.close();
					}
				}
			} else {
				return emailExist;
			}

		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorMaster> tire2Vendors(UserDetailsDto appDetails) {
		List<VendorMaster> vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorMaster = session
					.createQuery(
							"from VendorMaster where isActive = 1 and primeNonPrimeVendor=0 AND deverseSupplier=1 "
									+ "and isApproved=1 ORDER BY vendorName ASC")
					.list();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnonymousVendor downloadAnonymousVendorCertificate(Integer id,
			UserDetailsDto userDetails) {
		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		AnonymousVendor certificate = null;
		try {
			session.beginTransaction();

			certificate = (AnonymousVendor) session
					.createCriteria(AnonymousVendor.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return certificate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String checkSubVendor(Integer parentVendorId, Integer vendorId,
			UserDetailsDto userDetails) {
		String vendors = "vendor";
		Session session = null;
		try {
			if (userDetails.getUserType() != null
					&& userDetails.getUserType().equalsIgnoreCase("FG")) {
				session = HibernateUtil.buildSessionFactory()
						.getCurrentSession();
			} else {
				session = HibernateUtilCustomer.buildSessionFactory()
						.openSession(
								DataBaseConnection.getConnection(userDetails));
			}

			session.beginTransaction();
			VendorMaster vendorMaster = (VendorMaster) session.get(
					VendorMaster.class, vendorId);
			if (vendorMaster.getParentVendorId().equals(parentVendorId)) {
				vendors = "subVendor";
			}
		} catch (HibernateException e) {
			if (session != null) {
				session.getTransaction().rollback();
			}
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendors;
	}

	@Override
	public Object idFromName(UserDetailsDto userDetails,
			String query) {
		// TODO Auto-generated method stub
		Session session = null;
		Object id=null;
		try {
			if (userDetails.getUserType() != null
					&& userDetails.getUserType().equalsIgnoreCase("FG")) {
				session = HibernateUtil.buildSessionFactory()
						.getCurrentSession();
			} else {
				session = HibernateUtilCustomer.buildSessionFactory()
						.openSession(
								DataBaseConnection.getConnection(userDetails));
			}

			session.beginTransaction();
			 id=session.createQuery(query).uniqueResult();
		} catch (HibernateException e) {
			if (session != null) {
				session.getTransaction().rollback();
			}
			PrintExceptionInLogFile.printException(e);
			return id;
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return id;
	}

	@Override
	public VendorMaster saveExcelTire2Vendor(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId,
			List<DiverseCertificationTypesDto> certifications,
			List<NaicsCodesDto> naicsCodes, VendorContact currentVendor,
			UserDetailsDto userdetails, String url, String divCert,
			String address, String city, String contactPhone,
			String contanctEmail, String deverseSupplier, String firstName,
			String lastName, String middleName, String state,
			String vendorName, String zipcode, String country) {
		// TODO Auto-generated method stub
		logger.info("Inside the save Excel Tire2 vendor method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		VendorMaster parentVendor = null;
		CommonDao commonDao = new CommonDaoImpl();
		// result = validateVendorCode(vendorForm.getVendorCode(), appDetails);

		if (result.equals("success")) {
			// String result1 = validateLoginId(vendorForm.getLoginId(),
			// appDetails);
			// if (result1.equalsIgnoreCase("submit")) {
			String emailExist = commonDao.checkDuplicateEmail(contanctEmail,
					userdetails);
			if (emailExist.equalsIgnoreCase("submit")) {

				Session session1 = HibernateUtilCustomer.buildSessionFactory()
						.openSession(
								DataBaseConnection.getConnection(userdetails));

				try {
					session1.beginTransaction();
					parentVendor = (VendorMaster) session1
							.createCriteria(VendorMaster.class)
							.add(Restrictions.eq("id", currentVendor
									.getVendorId().getId())).uniqueResult();
					
					State stateObj = (State) session1.createCriteria(State.class).add(Restrictions.eq("statename", state)).uniqueResult();
					stateObj = (State) session1.get(State.class, stateObj.getId());
					
					Country countryObj = (Country) session1.createCriteria(Country.class).add(Restrictions.eq("countryname", country)).uniqueResult();
					countryObj = (Country) session1.get(Country.class, countryObj.getId());
					
					StringBuffer phone=new StringBuffer(contactPhone);
					phone.insert(0, '(');
					phone.insert(4, ')');
					phone.insert(5,' ');
					phone.insert(9,'-');
					contactPhone=phone.toString();
					
					vendorMaster = new VendorMaster();

					// Save VendorMaster Company Information
					vendorMaster.setParentVendorId(parentVendor.getId());
					vendorMaster.setVendorName(vendorName);

					vendorMaster.setNumberOfEmployees(Integer.parseInt("0"));
					vendorMaster.setAnnualTurnover(CommonUtils
							.deformatMoney("0"));
					vendorMaster.setAddress1(address);
					vendorMaster.setCity(city);
					vendorMaster.setCountry(countryObj.getId().toString());
					vendorMaster.setState(stateObj.getId().toString());
					vendorMaster.setPhone("");
					vendorMaster.setYearOfEstablishment(Integer.parseInt("0"));
					vendorMaster.setDeverseSupplier(CommonUtils
							.getCheckboxValue(deverseSupplier));

					vendorMaster.setPrimeNonPrimeVendor(CommonUtils
							.getByteValue(false));
					vendorMaster.setIsApproved((byte) 1);
					vendorMaster.setApprovedOn(new Date());

					// vendorMaster.setApprovedBy(userId);// no need for
					// approval process
					vendorMaster.setIsActive((byte) 1);
					vendorMaster.setIsinvited((short) 1);
					vendorMaster.setCreatedBy(userId);
					vendorMaster.setCreatedOn(new Date());
					vendorMaster.setModifiedBy(userId);
					vendorMaster.setModifiedOn(new Date());
					vendorMaster.setMinorityPercent(Double.valueOf(0));
					vendorMaster.setWomenPercent(Double.valueOf(0));
					vendorMaster.setDiverseNotes("");
					vendorMaster.setIsPartiallySubmitted("No");
					vendorMaster.setVendorStatus("S");
					
					if(userdetails.getSettings()!= null && userdetails.getSettings().getIsDivision() != null && userdetails.getSettings().getIsDivision() != 0)
					{
						if(userdetails.getCustomerDivisionID() != null)
						{
							if(userdetails.getCustomerDivisionID() != 0){
								CustomerDivision custDivision = (CustomerDivision)session1.get(CustomerDivision.class, userdetails.getCustomerDivisionID());									
								vendorMaster.setCustomerDivisionId(custDivision);
							}
							else
							{
								CustomerDivision custDivision = (CustomerDivision)session1.get(CustomerDivision.class, 3);									
								vendorMaster.setCustomerDivisionId(custDivision);
							}
						}							
					}
					
					// Ethnicity
					Ethnicity ethnicity = null;

					vendorMaster.setEthnicityId(ethnicity);

					session1.save(vendorMaster);

					CustomerVendorAddressMaster addressMaster = new CustomerVendorAddressMaster();
					addressMaster.setVendorId(vendorMaster);
					addressMaster.setAddressType("p");
					addressMaster.setAddress(address);
					addressMaster.setCity(city);
					addressMaster.setCountry(countryObj.getId().toString());
					addressMaster.setState(stateObj.getId().toString());
					addressMaster.setProvince("");
					addressMaster.setPhone("");
					addressMaster.setRegion("");
					addressMaster.setZipCode(zipcode);
					addressMaster.setMobile("");
					addressMaster.setFax("");
					addressMaster.setIsActive((byte) 1);
					addressMaster.setCreatedBy(userId);
					addressMaster.setCreatedOn(new Date());
					
					session1.save(addressMaster);

					CustomerVendorTier2Vendor customerVendorTier2Vendor = new CustomerVendorTier2Vendor();
					customerVendorTier2Vendor.setVendorid(vendorMaster);
					customerVendorTier2Vendor.setParentvendorid(parentVendor
							.getId());
					customerVendorTier2Vendor.setCreatedby(userId);
					customerVendorTier2Vendor.setCreatedon(new Date());
					customerVendorTier2Vendor.setIsactive((short) 1);

					session1.save(customerVendorTier2Vendor);

					// Save VendorContact Information
					VendorContact vendorContact = new VendorContact();
					VendorRoles roles = (VendorRoles) session1
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);

					vendorContact.setVendorId(vendorMaster);
					vendorContact.setVendorUserRoleId(roles);

					Random random = new java.util.Random();
					Integer randVal = random.nextInt();
					Encrypt encrypt = new Encrypt();
					String psw = RandomStringUtils.randomAlphanumeric(5);
					// convert password to encrypted password
					String encyppassword = encrypt.encryptText(
							Integer.toString(randVal) + "", psw);
					vendorContact.setFirstName(firstName);
					vendorContact.setLastName(lastName);
					vendorContact.setPhoneNumber(contactPhone);
					vendorContact.setDesignation("0");
					vendorContact.setMobile("0");
					vendorContact.setFax("0");
					vendorContact.setEmailId(contanctEmail);
					vendorContact.setIsAllowedLogin(CommonUtils
							.getByteValue(true));
					vendorContact.setIsPrimaryContact((byte) 1);
					// vendorContact.setLoginDisplayName("");
					// vendorContact.setLoginId(RandomStringUtils
					// .randomAlphabetic(5));
					vendorContact.setPassword(encyppassword);
					vendorContact.setKeyValue(randVal);
					vendorContact.setIsSysGenPassword((byte) 0);

					SecretQuestion secretQns = new SecretQuestion();
					secretQns = (SecretQuestion) session1.get(
							SecretQuestion.class, 1);

					vendorContact.setSecretQuestionId(secretQns);
					vendorContact.setSecQueAns("");
					vendorContact.setIsActive((byte) 1);
					vendorContact.setCreatedBy(userId);
					vendorContact.setCreatedOn(new Date());
					vendorContact.setModifiedBy(userId);
					vendorContact.setModifiedOn(new Date());
					vendorContact.setIsBusinessContact((byte) 0);
					vendorContact.setIsPreparer((byte) 1);
					session1.save(vendorContact);

					// Iterate each diverse classifications from UI
					if (divCert != null ) {
							CustomerDiverseClassification classification1 = new CustomerDiverseClassification();
							Certificate certificate = (Certificate) session1.createCriteria(Certificate.class).add(Restrictions.eq("certificateName", divCert)).uniqueResult();
							certificate = (Certificate) session1
									.get(Certificate.class, certificate.getId());
							/*Certificate certMaster = (Certificate) session1
									.get(Certificate.class,
											Integer.parseInt(divCert));*/
							classification1.setCertMasterId(certificate);
							classification1.setVendorId(vendorMaster);
							classification1.setDiverseQuality((byte) 0);
							classification1.setIsActive((byte) 1);
							classification1.setCreatedBy(userId);
							classification1.setCreatedOn(new Date());
							session1.save(classification1);
					}

					logger.info("Transaction commited.");

					session1.getTransaction().commit();

				} catch (HibernateException ex) {
					// ex.printStackTrace();
					PrintExceptionInLogFile.printException(ex);
					session1.getTransaction().rollback();
					ex.printStackTrace();
				} catch (Exception e) {
					PrintExceptionInLogFile.printException(e);
					e.printStackTrace();
				} finally {
					if (session1.isConnected()) {
						session1.close();
					}
				}
			}
			
			else {
				/*if (contanctEmail == null) {
					return "userEmail";
				}*/
				Session session = HibernateUtilCustomer.buildSessionFactory().openSession(
						DataBaseConnection.getConnection(userdetails));
				try {
					session.beginTransaction();
					Users user = (Users) session.createCriteria(Users.class)
							.add(Restrictions.eq("userEmailId", contanctEmail).ignoreCase())
							.uniqueResult();

					VendorContact vendorUser = (VendorContact) session
							.createCriteria(VendorContact.class)
							.add(Restrictions.eq("emailId", contanctEmail).ignoreCase())
							.uniqueResult();
					// CustomerVendorSelfRegInfo selfRegInfo =
					// (CustomerVendorSelfRegInfo) session
					// .createQuery(
					// "from CustomerVendorSelfRegInfo where emailId = '"
					// + emailId + "'").uniqueResult();
					if (user != null && vendorUser != null) {
						result = "userEmail";
						/*vendorMaster=vendorUser.getVendorId();*/
					} else if (user != null) {
						result = "userEmail";
					} else if (vendorUser != null) {
						result = "userEmail";
						/*vendorMaster=vendorUser.getVendorId();*/
					}
				} catch (Exception e) {
					PrintExceptionInLogFile.printException(e);
				} finally {
					if (userdetails.getUserType() != null
							&& !(userdetails.getUserType().equalsIgnoreCase("FG"))) {
						try {
							session.connection().close();
							session.close();
						} catch (HibernateException e) {
							PrintExceptionInLogFile.printException(e);
						} catch (SQLException e) {
							PrintExceptionInLogFile.printException(e);
						}
					}
				}
				
				
				
			}

		}
		return vendorMaster;
	}

}
