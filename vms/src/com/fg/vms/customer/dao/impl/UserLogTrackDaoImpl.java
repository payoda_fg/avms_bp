/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.UserLogTrackDao;
import com.fg.vms.customer.pojo.UserLogTrackForm;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author venkatesh
 *
 * Method to list the LoginDetails of all Users 
 * 
 */
public class UserLogTrackDaoImpl implements UserLogTrackDao {
	@SuppressWarnings("deprecation")
	@Override
	public List<UserLogTrackForm> setUserLoginDetails(UserDetailsDto userDetails) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(userDetails);

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		session.beginTransaction();

		List<UserLogTrackForm> logTracks = new ArrayList<UserLogTrackForm>();

		try {
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT users.FIRSTNAME,users.LASTNAME,logintrack.USERNAME  AS emailid,userroles.ROLENAME, "
					+ " MAX(logintrack.LOGINTIME)lastlogin FROM logintrack INNER JOIN  users ON users.USEREMAILID=logintrack.USERNAME "
					+ " INNER JOIN userroles ON userroles.ID=users.USERROLEID  "
					+ " GROUP BY logintrack.USERNAME,users.FIRSTNAME,users.LASTNAME,userroles.ROLENAME "
					+ " order by lastlogin DESC;");
			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			if (rs != null) {
				while (rs.next()) {
					UserLogTrackForm logTrack = new UserLogTrackForm();

					logTrack.setFirstName(rs.getString(1));
					logTrack.setLastName(rs.getString(2));
					logTrack.setUserEmailId(rs.getString(3));
					logTrack.setRoleName(rs.getString(4));
					logTrack.setUserLogon(rs.getString(5));

					logTracks.add(logTrack);
				}
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return logTracks;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<UserLogTrackForm> getUserLoginTimes(UserDetailsDto userDetails,
			String cureentEmailId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(userDetails);

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		session.beginTransaction();

		List<UserLogTrackForm> loginTimes = new ArrayList<UserLogTrackForm>();

		try {			
			StringBuilder sqlQuery = new StringBuilder();
			String emailId = "\'" + cureentEmailId + "\'";
			
			sqlQuery.append("SELECT IFNULL(logintrack.LOGINTIME,'') AS LOGINTIME, users.USERNAME FROM logintrack "
					+ "INNER JOIN users ON users.USEREMAILID = logintrack.USERNAME "
					+ "WHERE logintrack.USERNAME = " + emailId + " "
					+ "ORDER BY logintrack.LOGINTIME DESC");

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			if (rs != null) {
				while (rs.next()) {
					UserLogTrackForm logTime = new UserLogTrackForm();
					logTime.setUserLogon(rs.getString(1));
					logTime.setUsername(rs.getString(2));
					loginTimes.add(logTime);
				}
			}
			
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return loginTimes;
	}
}