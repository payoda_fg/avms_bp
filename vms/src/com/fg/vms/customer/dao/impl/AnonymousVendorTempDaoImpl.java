/**
 * AnonymousVendorTempDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.AnonymousVendorTempDao;
import com.fg.vms.customer.model.AnonymousVendorDump;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.pojo.AnonymousVendorForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class AnonymousVendorTempDaoImpl implements AnonymousVendorTempDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnonymousVendorDump saveVendorInfo(
			AnonymousVendorForm anonymousVendorForm, int currentUserId,
			int vendorId, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Users custLoginId = null;
		AnonymousVendorDump anonymousVendor = new AnonymousVendorDump();
		try {
			session.beginTransaction();

			custLoginId = (Users) session.get(Users.class, 1);

			log.info(" ============save anonymous vendor is starting========== ");

			anonymousVendor.setVendorName(anonymousVendorForm.getVendorName());
			anonymousVendor.setDunsNumber(anonymousVendorForm.getDunsNumber());
			anonymousVendor.setTaxId(anonymousVendorForm.getTaxId());
			anonymousVendor
					.setCompanyType(anonymousVendorForm.getCompanytype());
			anonymousVendor.setAddress1(anonymousVendorForm.getAddress1());
			anonymousVendor.setCity(anonymousVendorForm.getCity());
			anonymousVendor.setCountry(anonymousVendorForm.getCountry());
			anonymousVendor.setState(anonymousVendorForm.getState());
			anonymousVendor.setProvince(anonymousVendorForm.getProvince());
			anonymousVendor.setPhone(anonymousVendorForm.getPhone());
			anonymousVendor.setRegion(anonymousVendorForm.getRegion());
			anonymousVendor.setZipCode(anonymousVendorForm.getZipcode());
			anonymousVendor.setMobile(anonymousVendorForm.getMobile());
			anonymousVendor.setFax(anonymousVendorForm.getFax());
			anonymousVendor.setWebsite(anonymousVendorForm.getWebsite());
			anonymousVendor.setEmailId(anonymousVendorForm.getEmailId());
			anonymousVendor.setNumberOfEmployees(anonymousVendorForm
					.getNumberOfEmployees());
			if (anonymousVendorForm.getAnnualTurnoverFormat() != null
					&& !anonymousVendorForm.getAnnualTurnoverFormat().isEmpty()) {
				anonymousVendor.setAnnualTurnover(Double
						.parseDouble(anonymousVendorForm
								.getAnnualTurnoverFormat()));
			}
			anonymousVendor.setYearOfEstablishment(anonymousVendorForm
					.getYearOfEstablishment());
			anonymousVendor.setParentVendorId(currentUserId);
			anonymousVendor.setIsApproved((byte) 0);
			anonymousVendor.setApprovedBy(custLoginId);
			anonymousVendor.setIsActive((byte) 1);

			// Added for new requirements

			if (anonymousVendorForm.getMinorityPercent() != null
					&& !anonymousVendorForm.getMinorityPercent().isEmpty())
				anonymousVendor.setMinorityPercent(Double
						.valueOf(anonymousVendorForm.getMinorityPercent()));
			if (anonymousVendorForm.getWomenPercent() != null
					&& !anonymousVendorForm.getWomenPercent().isEmpty())
				anonymousVendor.setWomenPercent(Double
						.valueOf(anonymousVendorForm.getWomenPercent()));
			anonymousVendor.setDiverseNotes(anonymousVendorForm
					.getClassificationNotes());
			anonymousVendor.setDiverseClassification(anonymousVendorForm
					.getDiverseCertificateName());

			// Ethnicity
			Ethnicity ethnicity = null;
			if (anonymousVendorForm.getEthnicity() != null
					&& !"0".endsWith(anonymousVendorForm.getEthnicity())) {
				ethnicity = (Ethnicity) session.get(Ethnicity.class,
						Integer.valueOf(anonymousVendorForm.getEthnicity()));
			}
			anonymousVendor.setEthnicityId(ethnicity);

			if (anonymousVendorForm.getDiverseSupplier() != null
					&& !anonymousVendorForm.getDiverseSupplier().isEmpty()) {

				anonymousVendor
						.setDiverseSupplier(getDeverseSupplier(anonymousVendorForm
								.getDiverseSupplier()));
				if (anonymousVendorForm.getCertificationNo1() != null
						&& !anonymousVendorForm.getCertificationNo1().isEmpty()) {
					anonymousVendor.setCertificateNumber(anonymousVendorForm
							.getCertificationNo1());
				}
				if (anonymousVendorForm.getEffDate1() != null
						&& !anonymousVendorForm.getEffDate1().isEmpty()) {
					anonymousVendor
							.setEffectiveDate(CommonUtils
									.dateConvertValue(anonymousVendorForm
											.getEffDate1()));
				}
				if (anonymousVendorForm.getExpDate() != null
						&& !anonymousVendorForm.getExpDate().isEmpty()) {
					anonymousVendor
							.setExpiryDate(CommonUtils
									.dateConvertValue(anonymousVendorForm
											.getExpDate()));
				}

				if (anonymousVendorForm.getDivCertAgen() != null) {
					CertifyingAgency certAgencyId = (CertifyingAgency) session
							.get(CertifyingAgency.class,
									anonymousVendorForm.getDivCertAgen());
					anonymousVendor.setCertAgencyId(certAgencyId);
				}
				if (anonymousVendorForm.getDivCertType() != null) {
					Certificate certMasterId = (Certificate) session.get(
							Certificate.class,
							anonymousVendorForm.getDivCertType());
					anonymousVendor.setCertMasterId(certMasterId);
				}

				if (anonymousVendorForm.getCertFile() != null
						&& anonymousVendorForm.getCertFile().getFileSize() != 0) {
					anonymousVendor
							.setContent(convertFormFileToBlob(anonymousVendorForm
									.getCertFile()));
					anonymousVendor.setContentType(anonymousVendorForm
							.getCertFile().getContentType());
					anonymousVendor.setFilename(anonymousVendorForm
							.getCertFile().getFileName());
				}
			} else {
				anonymousVendor.setDiverseSupplier((byte) 0);
			}
			String psw = RandomStringUtils.randomAlphanumeric(5);
			anonymousVendor.setPassword(psw);

			anonymousVendor.setFirstName(anonymousVendorForm.getFirstName());
			anonymousVendor.setLastName(anonymousVendorForm.getLastName());
			anonymousVendor
					.setDesignation(anonymousVendorForm.getDesignation());
			anonymousVendor.setPhoneNumber(anonymousVendorForm
					.getContactPhone());
			anonymousVendor.setMobileNumber(anonymousVendorForm
					.getContactMobile());
			anonymousVendor.setFaxNumber(anonymousVendorForm.getContactFax());
			anonymousVendor.setEmail(anonymousVendorForm.getContanctEmail());
			anonymousVendor.setNaicsCode(anonymousVendorForm.getNaicsCode_1());
			anonymousVendor.setNaicsDescription(anonymousVendorForm
					.getNaicsDesc());
			anonymousVendor.setCapabilities(anonymousVendorForm
					.getCapabilities());
			anonymousVendor.setIsicCode(anonymousVendorForm.getIsicCode());
			anonymousVendor.setIsicDescription(anonymousVendorForm
					.getIsicDesc());
			// For set the values in reference 1
			if (anonymousVendorForm.getReferenceName1() != null
					&& !anonymousVendorForm.getReferenceName1().isEmpty()) {
				anonymousVendor.setReferenceName1(anonymousVendorForm
						.getReferenceName1());
			}
			if (anonymousVendorForm.getReferenceAddress1() != null
					&& !anonymousVendorForm.getReferenceAddress1().isEmpty()) {
				anonymousVendor.setReferenceAddress1(anonymousVendorForm
						.getReferenceAddress1());
			}
			if (anonymousVendorForm.getReferenceMailId1() != null
					&& !anonymousVendorForm.getReferenceMailId1().isEmpty()) {
				anonymousVendor.setReferenceMailId1(anonymousVendorForm
						.getReferenceMailId1());
			}
			if (anonymousVendorForm.getReferenceMobile1() != null
					&& !anonymousVendorForm.getReferenceMobile1().isEmpty()) {
				anonymousVendor.setReferenceMobile1(anonymousVendorForm
						.getReferenceMobile1());
			}
			if (anonymousVendorForm.getReferencePhone1() != null
					&& !anonymousVendorForm.getReferencePhone1().isEmpty()) {
				anonymousVendor.setReferencePhone1(anonymousVendorForm
						.getReferencePhone1());
			}
			if (anonymousVendorForm.getReferenceCity1() != null
					&& !anonymousVendorForm.getReferenceCity1().isEmpty()) {
				anonymousVendor.setReferenceCity1(anonymousVendorForm
						.getReferenceCity1());
			}
			if (anonymousVendorForm.getReferenceZip1() != null
					&& !anonymousVendorForm.getReferenceZip1().isEmpty()) {
				anonymousVendor.setReferenceZip1(anonymousVendorForm
						.getReferenceZip1());
			}
			if (anonymousVendorForm.getReferenceState1() != null
					&& !anonymousVendorForm.getReferenceState1().isEmpty()) {
				anonymousVendor.setReferenceState1(anonymousVendorForm
						.getReferenceState1());
			}
			if (anonymousVendorForm.getReferenceCountry1() != null
					&& !anonymousVendorForm.getReferenceCountry1().isEmpty()) {
				anonymousVendor.setReferenceCountry1(anonymousVendorForm
						.getReferenceCountry1());
			}

			// For set the values in reference 2
			if (anonymousVendorForm.getReferenceName2() != null
					&& !anonymousVendorForm.getReferenceName2().isEmpty()) {
				anonymousVendor.setReferenceName2(anonymousVendorForm
						.getReferenceName2());
			}
			if (anonymousVendorForm.getReferenceAddress2() != null
					&& !anonymousVendorForm.getReferenceAddress2().isEmpty()) {
				anonymousVendor.setReferenceAddress2(anonymousVendorForm
						.getReferenceAddress2());
			}
			if (anonymousVendorForm.getReferenceMailId2() != null
					&& !anonymousVendorForm.getReferenceMailId2().isEmpty()) {
				anonymousVendor.setReferenceMailId2(anonymousVendorForm
						.getReferenceMailId2());
			}
			if (anonymousVendorForm.getReferenceMobile2() != null
					&& !anonymousVendorForm.getReferenceMobile2().isEmpty()) {
				anonymousVendor.setReferenceMobile2(anonymousVendorForm
						.getReferenceMobile2());
			}
			if (anonymousVendorForm.getReferencePhone2() != null
					&& !anonymousVendorForm.getReferencePhone2().isEmpty()) {
				anonymousVendor.setReferencePhone2(anonymousVendorForm
						.getReferencePhone2());
			}
			if (anonymousVendorForm.getReferenceCity2() != null
					&& !anonymousVendorForm.getReferenceCity2().isEmpty()) {
				anonymousVendor.setReferenceCity2(anonymousVendorForm
						.getReferenceCity2());
			}
			if (anonymousVendorForm.getReferenceZip2() != null
					&& !anonymousVendorForm.getReferenceZip2().isEmpty()) {
				anonymousVendor.setReferenceZip2(anonymousVendorForm
						.getReferenceZip2());
			}
			if (anonymousVendorForm.getReferenceState2() != null
					&& !anonymousVendorForm.getReferenceState2().isEmpty()) {
				anonymousVendor.setReferenceState2(anonymousVendorForm
						.getReferenceState2());
			}
			if (anonymousVendorForm.getReferenceCountry2() != null
					&& !anonymousVendorForm.getReferenceCountry2().isEmpty()) {
				anonymousVendor.setReferenceCountry2(anonymousVendorForm
						.getReferenceCountry2());
			}

			// For set the values in reference 3
			if (anonymousVendorForm.getReferenceName3() != null
					&& !anonymousVendorForm.getReferenceName3().isEmpty()) {
				anonymousVendor.setReferenceName3(anonymousVendorForm
						.getReferenceName3());
			}
			if (anonymousVendorForm.getReferenceAddress3() != null
					&& !anonymousVendorForm.getReferenceAddress3().isEmpty()) {
				anonymousVendor.setReferenceAddress3(anonymousVendorForm
						.getReferenceAddress3());
			}
			if (anonymousVendorForm.getReferenceMailId3() != null
					&& !anonymousVendorForm.getReferenceMailId3().isEmpty()) {
				anonymousVendor.setReferenceMailId3(anonymousVendorForm
						.getReferenceMailId3());
			}
			if (anonymousVendorForm.getReferenceMobile3() != null
					&& !anonymousVendorForm.getReferenceMobile3().isEmpty()) {
				anonymousVendor.setReferenceMobile3(anonymousVendorForm
						.getReferenceMobile3());
			}
			if (anonymousVendorForm.getReferencePhone3() != null
					&& !anonymousVendorForm.getReferencePhone3().isEmpty()) {
				anonymousVendor.setReferencePhone3(anonymousVendorForm
						.getReferencePhone3());
			}
			if (anonymousVendorForm.getReferenceCity3() != null
					&& !anonymousVendorForm.getReferenceCity3().isEmpty()) {
				anonymousVendor.setReferenceCity3(anonymousVendorForm
						.getReferenceCity3());
			}
			if (anonymousVendorForm.getReferenceZip3() != null
					&& !anonymousVendorForm.getReferenceZip3().isEmpty()) {
				anonymousVendor.setReferenceZip3(anonymousVendorForm
						.getReferenceZip3());
			}
			if (anonymousVendorForm.getReferenceState3() != null
					&& !anonymousVendorForm.getReferenceState3().isEmpty()) {
				anonymousVendor.setReferenceState3(anonymousVendorForm
						.getReferenceState3());
			}
			if (anonymousVendorForm.getReferenceCountry3() != null
					&& !anonymousVendorForm.getReferenceCountry3().isEmpty()) {
				anonymousVendor.setReferenceCountry3(anonymousVendorForm
						.getReferenceCountry3());
			}

			anonymousVendor.setCreatedBy(currentUserId);
			anonymousVendor.setCreatedOn(new Date());
			anonymousVendor.setModifiedBy(currentUserId);
			anonymousVendor.setModifiedOn(new Date());
			session.save(anonymousVendor);
			log.info(" ============saved anonymous vendor successfully========== ");
			session.getTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return anonymousVendor;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	private Blob convertFormFileToBlob(FormFile file) {
		Blob blob = null;
		try {
			if (file != null && file.getFileSize() != 0) {
				blob = Hibernate.createBlob(file.getInputStream());
			}

		} catch (FileNotFoundException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} catch (IOException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		}
		return blob;

	}

	// Convert string to byte
	private Byte getDeverseSupplier(String string) {

		if (string.equalsIgnoreCase("on")) {
			return (byte) 1;
		} else {
			return (byte) 0;
		}

	}

}
