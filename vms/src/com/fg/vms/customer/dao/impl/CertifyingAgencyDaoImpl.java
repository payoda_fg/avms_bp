/*
 * CertifyingAgencyDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CertifyingAgencyDao;
import com.fg.vms.customer.dto.CertifyingAgencyDto;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerClassificationCertifyingAgencies;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.pojo.CertifyingAgencyForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the Certifying Agency services implementation
 * 
 * @author vinoth
 * 
 */
public class CertifyingAgencyDaoImpl implements CertifyingAgencyDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CertifyingAgencyDto> view(UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<CertifyingAgency> certifyingAgencies = null;
		List<CertifyingAgencyDto> listCertifyingAgencies = null;
		try {
			session.beginTransaction();

			/*
			 * Get the list of certifying agencies based on active certifying
			 * agencies
			 */
			certifyingAgencies =
							(List<CertifyingAgency>)session.createQuery("from CertifyingAgency where isActive=1 order by agencyName").list();
			listCertifyingAgencies = packAgencyDtos(certifyingAgencies);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return listCertifyingAgencies;
	}

	/**
	 * Package of certifying agencies.
	 * 
	 * @param certifyingAgencies
	 * @return
	 */
	private List<CertifyingAgencyDto> packAgencyDtos(
			List<CertifyingAgency> certifyingAgencies) {

		List<CertifyingAgencyDto> listCertifyingAgencies = new ArrayList<CertifyingAgencyDto>();
		for (CertifyingAgency agency : certifyingAgencies) {

			CertifyingAgencyDto agencyDto = new CertifyingAgencyDto();
			agencyDto.setId(agency.getId());
			agencyDto.setAgencyName(agency.getAgencyName());
			agencyDto.setAgencyShortName(agency.getAgencyShortName());
			agencyDto.setPhone(agency.getPhone());
			agencyDto.setFax(agency.getFax());
			agencyDto.setActive(getBooleanValue(agency.getIsActive()));

			listCertifyingAgencies.add(agencyDto);

		}
		return listCertifyingAgencies;

	}

	/**
	 * Pass byte value to get booleanS.
	 * 
	 * @param value
	 * @return
	 */
	private boolean getBooleanValue(Byte value) {

		if (value == 1) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String saveOrUpdate(CertifyingAgencyDto certifyingAgencyDto,
			Integer id, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "success";
		try {
			session.beginTransaction();
			CertifyingAgency certifyingAgency;

			/*
			 * Get the list of certifying agencies based on certifying agency
			 * dto
			 */
			certifyingAgency = (CertifyingAgency) session
					.createCriteria(CertifyingAgency.class)
					.add(Restrictions.eq("id", certifyingAgencyDto.getId()))
					.uniqueResult();
			if (certifyingAgency != null) {

				log.info(" ============update certifying agency is starting========== ");

				certifyingAgency.setAgencyName(certifyingAgencyDto
						.getAgencyName());
				certifyingAgency.setAgencyShortName(certifyingAgencyDto
						.getAgencyShortName());
				certifyingAgency.setAddress(certifyingAgencyDto.getAddress());
				certifyingAgency.setPhone(certifyingAgencyDto.getPhone());
				certifyingAgency.setFax(certifyingAgencyDto.getFax());
				certifyingAgency.setIsActive(getByteValue(certifyingAgencyDto
						.isActive()));
				certifyingAgency.setModifiedBy(id);
				certifyingAgency.setModifiedOn(new Date());
				session.update(certifyingAgency);

				log.info(" ============updated certifying agency successfully========== ");
                
				session.getTransaction().commit();
	
			}

			else {
				certifyingAgency = new CertifyingAgency();

				log.info(" ============save certifying agency is starting========== ");

				certifyingAgency.setAgencyName(certifyingAgencyDto
						.getAgencyName());
				certifyingAgency.setAgencyShortName(certifyingAgencyDto
						.getAgencyShortName());
				certifyingAgency.setAddress(certifyingAgencyDto.getAddress());
				certifyingAgency.setPhone(certifyingAgencyDto.getPhone());
				certifyingAgency.setFax(certifyingAgencyDto.getFax());
				certifyingAgency.setIsActive((byte) 1);
				certifyingAgency.setCreatedBy(id);
				certifyingAgency.setCreatedOn(new Date());
				certifyingAgency.setModifiedBy(id);
				certifyingAgency.setModifiedOn(new Date());
				session.save(certifyingAgency);

				log.info(" ============saved certifying agency successfully========== ");
				session.getTransaction().commit();
			}

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return result;
	}

	/**
	 * Using boolean value to get byte.
	 * 
	 * @param value
	 * @return
	 */
	private Byte getByteValue(boolean value) {

		if (value) {
			return (byte) 1;
		} else {
			return (byte) 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public String createagency(CertifyingAgencyForm certifyingAgencyForm, int currentUserId, UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		String result = null;
		try 
		{
			session.beginTransaction();
			
			if(null != certifyingAgencyForm.getId() && 0 != certifyingAgencyForm.getId())
			{
				CertifyingAgency updateCertifyingAgency = (CertifyingAgency) session.createCriteria(CertifyingAgency.class)
															.add(Restrictions.eq("agencyName", certifyingAgencyForm.getAgencyName()))
															.add(Restrictions.ne("id", certifyingAgencyForm.getId())).uniqueResult();
				
				if(updateCertifyingAgency == null) 
				{
					updateCertifyingAgency = (CertifyingAgency) session.createCriteria(CertifyingAgency.class)
							.add(Restrictions.eq("agencyShortName", certifyingAgencyForm.getAgencyShortName()))
							.add(Restrictions.ne("id", certifyingAgencyForm.getId())).uniqueResult();
					
					if(updateCertifyingAgency == null) 
					{
						updateCertifyingAgency = (CertifyingAgency) session.createCriteria(CertifyingAgency.class)
													.add(Restrictions.eq("id", certifyingAgencyForm.getId())).uniqueResult();
						
						if (updateCertifyingAgency != null)
					    {
							log.info(" ============edit certifying agency is starting========== ");
							updateCertifyingAgency.setAgencyName(certifyingAgencyForm.getAgencyName());
					    	updateCertifyingAgency.setAgencyShortName(certifyingAgencyForm.getAgencyShortName());
					    	updateCertifyingAgency.setAddress(certifyingAgencyForm.getAddress());
					    	updateCertifyingAgency.setPhone(certifyingAgencyForm.getPhone());
					    	updateCertifyingAgency.setFax(certifyingAgencyForm.getFax());
					    	updateCertifyingAgency.setIsActive((byte) 1);
					    	updateCertifyingAgency.setCreatedBy(currentUserId);
					    	updateCertifyingAgency.setCreatedOn(new Date());
					    	updateCertifyingAgency.setModifiedBy(currentUserId);
					    	updateCertifyingAgency.setModifiedOn(new Date());
					    	session.update(updateCertifyingAgency);
		               
					    	session.getTransaction().commit();
		   			       	result = "updatedAgency";
					    }
					}
					else 
					{
						result="agencyShortNameUnique";
					}					
				} 
				else 
				{
					result="agencyNameUnique";
				}
			}			
			else 
			{
				/*
				 * Get the certifying agency details based on selected agency from
				 * UI. Check the unique Certifying agencies.
				 * 
				 */
				CertifyingAgency  certifyingAgency = (CertifyingAgency) session.createCriteria(CertifyingAgency.class)
                										.add(Restrictions.eq("agencyName", certifyingAgencyForm.getAgencyName())).uniqueResult();
                
            	if(certifyingAgency == null) 
	            {
            		certifyingAgency =(CertifyingAgency) session.createCriteria(CertifyingAgency.class)
        					.add(Restrictions.eq("agencyShortName", certifyingAgencyForm.getAgencyShortName())).uniqueResult();
            		
            		if(certifyingAgency == null) 
            		{
            			log.info(" ============save certifying agency is starting========== ");
            			CertifyingAgency  createCertifyingAgency=new CertifyingAgency();
                		createCertifyingAgency.setAgencyShortName(certifyingAgencyForm.getAgencyShortName());
                		createCertifyingAgency.setAgencyName(certifyingAgencyForm.getAgencyName());
                		createCertifyingAgency.setAddress(certifyingAgencyForm.getAddress());
                		createCertifyingAgency.setPhone(certifyingAgencyForm.getPhone());
                		createCertifyingAgency.setFax(certifyingAgencyForm.getFax());
                		createCertifyingAgency.setIsActive((byte) 1);
                		createCertifyingAgency.setCreatedBy(currentUserId);
                		createCertifyingAgency.setModifiedBy(currentUserId);
                		createCertifyingAgency.setCreatedOn(new Date());
                		createCertifyingAgency.setModifiedOn(new Date());
      
                		session.save(createCertifyingAgency);
                		session.getTransaction().commit();
                		result = "success";
                		log.info(" ============saved certifying agency succcessfully========== ");
            		} 
            		else 
            		{
            			result = "agencyShortNameUnique";
            		}	
	            }
            	else
            	{
            		if(certifyingAgency.getIsActive()==0)
            		{
            			certifyingAgency.setIsActive((byte) 1); 
            			session.update(certifyingAgency);
            			session.getTransaction().commit();
            			result="updatedAgency";
            		}
            		else 
            		{
            			result = "agencyNameUnique";
            		}
            	} 
   		  	}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	@Override
	public CertifyingAgency retrieveCertifyAgency(int certiftyAgencyId,
			UserDetailsDto userDetails) {
		
		
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CertifyingAgency certifyingAgency = null;
		try {
			/* View/Retrieve the message based on unique certificateType id. */
			certifyingAgency = (CertifyingAgency) session
					.createCriteria(CertifyingAgency.class)
					.add(Restrictions.eq("id", certiftyAgencyId)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
        
		return certifyingAgency;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String deleteCertifyAgency(int certiftyAgencyId,
			UserDetailsDto userDetails) {
		
		
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CertifyingAgency certifyingAgency = null;
		String result =null;
		try {
			/* Retrieve the message based on  certificateType id. */
			certifyingAgency = (CertifyingAgency) session
					.createCriteria(CertifyingAgency.class)
					.add(Restrictions.eq("id", certiftyAgencyId)).uniqueResult();
			
			
			List<VendorCertificate> referenceVendorCertifiacteAgencies= (List<VendorCertificate>) session
					.createCriteria(
							VendorCertificate.class)
					.add(Restrictions
							.eq("certAgencyId", certifyingAgency)).list();
			
			List<CustomerClassificationCertifyingAgencies> referenceClassificationAgencies = (List<CustomerClassificationCertifyingAgencies>) session
					.createCriteria(
							CustomerClassificationCertifyingAgencies.class)
					.add(Restrictions
							.eq("agencyId", certifyingAgency)).list();
			
		if((referenceVendorCertifiacteAgencies.size()!=0) || (referenceClassificationAgencies.size() != 0))
		{
				result="reference";
		}
		else
		{
				if (certifyingAgency != null) {
			
				certifyingAgency.setIsActive((byte) 0);
				session.merge(certifyingAgency);
				result = "success";
				session.getTransaction().commit();
			    } else {
				result = "failure";
			}
		}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
  	return result;
	}
}
