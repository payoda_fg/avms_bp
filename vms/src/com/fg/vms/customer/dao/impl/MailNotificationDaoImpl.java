/*
 * MailNotificationDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.MailNotificationDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dto.MailNotification;
import com.fg.vms.customer.dto.MailNotificationDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.dto.SpendIndirectDto;
import com.fg.vms.customer.model.CustomerIndirectSpendDataMaster;
import com.fg.vms.customer.model.CustomerVendorStatusLog;
import com.fg.vms.customer.model.EmailHistory;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.TemplateQuestions;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorEmailNotificationMaster;
import com.fg.vms.customer.model.VendorEmailNotificationTemplateDetail;
import com.fg.vms.customer.model.VendorMailNotificationDetail;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.MailForm;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.EmailType;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SendEmail;
import com.fg.vms.util.VendorStatus;

/**
 * Represents the mail notification services implementation.
 * 
 * @author Vinoth
 * 
 */
public class MailNotificationDaoImpl implements MailNotificationDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	private char[] message;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SearchVendorDto> searchVendorName(
			SearchVendorForm searchVendorForm, Integer approved,
			UserDetailsDto appDetails, Integer primeOrNonprime, String emailType) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();

		String joinedString = StringUtils.join(
				searchVendorForm.getDiverseCertificateNames(), ",");

		boolean diverseFlag = false;
		if (searchVendorForm.getDiverseCertificateNames() != null
				&& Arrays.asList(searchVendorForm.getDiverseCertificateNames())
						.contains("0")) {
			diverseFlag = true;
		}

		Iterator iterator = null;
		try {
			session.beginTransaction();
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("select vendor.ID,vendor.CITY,vendor.REGION,vendor.DUNSNUMBER,vendor.VENDORNAME, "
					+ "(select  naics.NAICSCODE from customer_naicscode naics, customer_vendornaics vendorNaics"
					+ " where vendorNaics.NAICSID=naics.NAICSID  and vendorNaics.VENDORID=vendor.ID and vendorNaics.ISPRIMARYKEY IN (1,2,3) )NAISCODE, ");

			sqlQuery.append(" country.countryname, state.STATENAME,vendor.CREATEDON,vendor.PRIMENONPRIMEVENDOR,vendor.DIVSERSUPPLIER,vendorContact.EMAILID,vendor.ISAPPROVED from"
					+ " customer_vendormaster vendor inner join customer_vendoraddressmaster vaddr on ");
			sqlQuery.append("vendor.ID=vaddr.VENDORID left outer join state on vaddr.STATE=state.ID left outer join country on vaddr.COUNTRY=country.Id and ");
			sqlQuery.append("vaddr.ADDRESSTYPE='p' and vendor.ISACTIVE=1 and (vendor.vendorStatus='A' or vendor.vendorStatus='B')"
					+ " INNER JOIN customer_vendor_users vendorContact ON vendorContact.VENDORID=vendor.ID AND (ISPRIMARYCONTACT=1 or ISPREPARER=1)");

			// if ("Assessment".equalsIgnoreCase(emailType)) {
			// sqlQuery.append(" and vendor.isinvited=0 ");
			// }
			if (primeOrNonprime != null && primeOrNonprime != 0) {
				sqlQuery.append(" AND (vendor.PRIMENONPRIMEVENDOR="
						+ primeOrNonprime
						+ " OR  vendor.PRIMENONPRIMEVENDOR is null OR  vendor.PRIMENONPRIMEVENDOR=0) ");
			} else {
				sqlQuery.append(" AND (vendor.PRIMENONPRIMEVENDOR="
						+ primeOrNonprime
						+ " OR  vendor.PRIMENONPRIMEVENDOR is null) ");
			}
			if ((searchVendorForm.getNaicCode() != null && searchVendorForm
					.getNaicCode().length() != 0)
					|| searchVendorForm.getNaicsDesc() != null
					&& searchVendorForm.getNaicsDesc().length() != 0) {
				sqlQuery.append("inner join customer_vendornaics vendorNaics on vendorNaics.VENDORID=vendor.ID inner join"
						+ " customer_naicscode naics on vendorNaics.NAICSID=naics.NAICSID and vendorNaics.ISPRIMARYKEY IN (1,2,3)");

			}
			if (searchVendorForm.getNaicCode() != null
					&& searchVendorForm.getNaicCode().length() != 0) {
				sqlQuery.append(" and naics.NAICSCODE ='"
						+ searchVendorForm.getNaicCode() + "'");

			}

			if (searchVendorForm.getNaicsDesc() != null
					&& searchVendorForm.getNaicsDesc().length() != 0) {
				sqlQuery.append(" and naics.NAICSDESCRIPTION LIKE '%"
						+ searchVendorForm.getNaicsDesc() + "%'");

			}

			if (searchVendorForm.getVendorName() != null
					&& searchVendorForm.getVendorName().length() != 0) {
				sqlQuery.append(" and vendor.VENDORNAME LIKE '%"
						+ CommonUtils.escape(searchVendorForm.getVendorName())
						+ "%'");

			}

			if (searchVendorForm.getCity() != null
					&& searchVendorForm.getCity().length() != 0) {
				sqlQuery.append(" and vaddr.CITY LIKE '%"
						+ searchVendorForm.getCity() + "%'");

			}

			if (searchVendorForm.getRegion() != null
					&& searchVendorForm.getRegion().length() != 0) {
				sqlQuery.append(" and vaddr.REGION LIKE '%"
						+ searchVendorForm.getRegion() + "%'");

			}

			if (searchVendorForm.getProvince() != null
					&& searchVendorForm.getProvince().length() != 0) {
				sqlQuery.append(" and vaddr.PROVINCE LIKE '%"
						+ searchVendorForm.getProvince() + "%'");

			}
			if (searchVendorForm.getState() != null
					&& searchVendorForm.getState().length() != 0) {
				sqlQuery.append(" and vaddr.STATE = "
						+ searchVendorForm.getState() + "");

			}

			if (searchVendorForm.getCountry() != null
					&& !searchVendorForm.getCountry().equalsIgnoreCase("0")) {
				sqlQuery.append(" and vaddr.COUNTRY=:country");

			}

			if (searchVendorForm.getDiverse() != null
					&& searchVendorForm.getDiverse().length() != 0) {
				sqlQuery.append(" and vendor.DIVSERSUPPLIER=:diverse");
			}

			if (searchVendorForm.getDiverseCertificateNames() != null
					&& searchVendorForm.getDiverseCertificateNames().length != 0) {
				sqlQuery.append(" INNER JOIN customer_vendordiverseclassifcation diverse ON diverse.VENDORID=vendor.ID AND diverse.CERTMASTERID IN ("
						+ joinedString + ") AND diverse.ACTIVE=1 ");
			}

			if (appDetails != null
					&& appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				
				if(appDetails.getCustomerDivisionIds() != null 
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					
					sqlQuery.append(" and vendor.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			sqlQuery.append(" GROUP BY vendor.VENDORNAME ORDER BY vendor.VENDORNAME ASC");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			System.out.println("MailNotificationDaoImpl @ 196:" + sqlQuery.toString());
			log.info("Query in Search Vendor Name:" + sqlQuery.toString());

			if (searchVendorForm.getCountry() != null
					&& !searchVendorForm.getCountry().equalsIgnoreCase("0")) {
				query.setParameter("country", searchVendorForm.getCountry());
			}

			if (searchVendorForm.getDiverse() != null
					&& searchVendorForm.getDiverse().length() != 0) {
				query.setParameter("diverse", searchVendorForm.getDiverse());
			}

			List vendors = query.list();
			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					Date createdon;
					Byte primenonprimevendor;
					Byte divsersupplier;
					String emailId;
					Byte isapproved;
					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					naicsCode = (String) searchVendors[5];
					countryName = (String) searchVendors[6];
					stateName = (String) searchVendors[7];
					createdon = (Date) searchVendors[8];
					primenonprimevendor = (Byte) searchVendors[9];
					divsersupplier = (Byte) searchVendors[10];
					emailId = (String) searchVendors[11];
					isapproved = (Byte) searchVendors[12];
					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, createdon, primenonprimevendor,
							divsersupplier, emailId, isapproved);
					vendorsList.add(vendorDto);
				}
			}

			session.getTransaction().commit();
		} catch (Exception exception) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorsList;
	}

	/**
	 * Get the list of mail notifications as package.
	 * 
	 * @param mailNotifications
	 * @return
	 */
	private List<MailNotificationDto> packAgencyDtos1(
			List<MailNotification> mailNotifications) {

		List<MailNotificationDto> listMailNotificationDtos = new ArrayList<MailNotificationDto>();
		for (MailNotification notification : mailNotifications) {

			MailNotificationDto notificationDto = new MailNotificationDto();
			notificationDto.setId(notification.getId());
			notificationDto.setCountry(notification.getCountry());
			notificationDto
					.setActive(getBooleanValue(notification.getActive()));

			listMailNotificationDtos.add(notificationDto);

		}
		return listMailNotificationDtos;

	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean getBooleanValue(Byte value) {
		if (value == 1) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Used for send mail to customer based on details given in UI.
	 */
	public String sendMail(MailForm mailForm, UserDetailsDto appDetails,
			String prefix) {
		String status;
		SendEmail email = new SendEmail();
		String filePath = null;
		String fileName = null;
		if (null != mailForm.getAttachment()
				&& mailForm.getAttachment().getFileSize() != 0) {
			String companyCode = appDetails.getCustomer().getCustCode();
			filePath = Constants.getString(Constants.EMAIL_ATTACHMENT_PATH)
					+ File.separator + companyCode + File.separator + prefix
					+ "_" + mailForm.getAttachment().getFileName();
			fileName = mailForm.getAttachment().getFileName();
		}
		String result = email.sendMail(mailForm.getTo(), mailForm.getCc(),
				mailForm.getBcc(), mailForm.getSubject(),
				mailForm.getMessage(), null, appDetails, filePath, fileName);
		if (result.equals("success")) {
			status = "success";
		} else {
			status = "error";
		}
		return status;

	}

	/**
	 * Save the assessment mail information before sending it to vendor.
	 */
	public VendorEmailNotificationMaster saveAssessmentMailInfo(
			MailForm mailForm, Integer userId, UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String address[] = null;
		String toAddress = mailForm.getTo();
		if (toAddress.contains(";")) {
			address = mailForm.getTo().split(";");
		}

		VendorEmailNotificationMaster vendorEmailNotificationMaster = null;

		try {
			session.beginTransaction();

			vendorEmailNotificationMaster = new VendorEmailNotificationMaster();

			log.info(" ============save vendor email notification is starting========== ");

			vendorEmailNotificationMaster.setEmailDate(new Date());

			vendorEmailNotificationMaster
					.setEmailContent(mailForm.getMessage());

			vendorEmailNotificationMaster
					.setEmailSubject(mailForm.getSubject());

			vendorEmailNotificationMaster.setFinalDateOfReply(CommonUtils
					.dateConvertValue(mailForm.getFinalDateForReply()));

			vendorEmailNotificationMaster.setEmailNotificationType(2);
			
			vendorEmailNotificationMaster.setCreatedBy(userId);

			Template template = (Template) session.get(Template.class,
					Integer.parseInt(mailForm.getTemplateId()));

			vendorEmailNotificationMaster.setTemplate(template);

			vendorEmailNotificationMaster.setCreatedOn(new Date());

			session.save(vendorEmailNotificationMaster);
			if (null != mailForm.getAttachment()
					&& mailForm.getAttachment().getFileSize() != 0) {
				String companyCode = appDetails.getCustomer().getCustCode();
				String filePath = Constants
						.getString(Constants.EMAIL_ATTACHMENT_PATH)
						+ "/"
						+ companyCode;
				File theDir = new File(filePath);

				// if the directory does not exist, create it
				if (!theDir.exists()) {
					theDir.mkdirs();
				}
				// Saving Image to appropriate path
				CommonUtils.saveAttachment(mailForm.getAttachment(), filePath,
						vendorEmailNotificationMaster.getId().toString());
				vendorEmailNotificationMaster
						.setFilename(vendorEmailNotificationMaster.getId()
								.toString()
								+ File.separator
								+ mailForm.getAttachment().getFileName());
				vendorEmailNotificationMaster.setFilesize(mailForm
						.getAttachment().getFileSize());
				vendorEmailNotificationMaster.setFileType(mailForm
						.getAttachment().getContentType());
				vendorEmailNotificationMaster.setPhysicalpath(filePath);
				session.merge(vendorEmailNotificationMaster);

			}
			log.info(" ============saved vendor email notification successfully========== ");

			/*
			 * Store templateQuestions and Email notification detail for get the
			 * correct number of template questions by vendor allocated at the
			 * time of appraisal.
			 */

			/* Get the list of template questions based on template id */
			List<TemplateQuestions> templateQuestions = session
					.createCriteria(TemplateQuestions.class)
					.add(Restrictions.eq("template", template)).list();

			/*
			 * Adding all the template questions into email notification
			 * template detail table at the time of appraisal
			 */

			for (TemplateQuestions tempQuestions : templateQuestions) {
				VendorEmailNotificationTemplateDetail emailNotificationTemplateDetail = new VendorEmailNotificationTemplateDetail();

				log.info(" ============save vendor email notification template detail is starting========== ");
				emailNotificationTemplateDetail
						.setVendorEmailNotificationMasterID(vendorEmailNotificationMaster);
				emailNotificationTemplateDetail
						.setTemplateQuestionsId(tempQuestions);
				session.save(emailNotificationTemplateDetail);

				log.info(" ============saved vendor email notification template detail successfully========== ");
			}

			String[] emailIds = mailForm.getTo().split(";");

			for (String emailId : emailIds) {

				/* Save vendor email notification details. */
				VendorMailNotificationDetail mailNotificationDetail = new VendorMailNotificationDetail();

				log.info(" ============save vendor email notification detail is starting========== ");
				mailNotificationDetail.setVendorEmailId(emailId);
				mailNotificationDetail
						.setVendorEmailNotificationMaster(vendorEmailNotificationMaster);

				VendorContact contact = (VendorContact) session
						.createCriteria(VendorContact.class)
						.add(Restrictions.eq("emailId", emailId))
						.uniqueResult();
				if (contact != null) { // modified for adding non existing
										// email-Id enter by user.
					mailNotificationDetail.setVendorMaster(contact
							.getVendorId());
				}

				session.save(mailNotificationDetail);

				log.info(" ============saved vendor email notification detail successfully========== ");
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorEmailNotificationMaster;
	}

	/**
	 * Save the assessment mail information before sending it to vendor.
	 */
	@Override
	public String assignAssessmentToVendor(Integer userId, VendorMaster vendor,
			String subject, String content, VendorContact contact,
			UserDetailsDto appDetails, Session session) {
		String result = "success";
		// Session session = HibernateUtilCustomer.buildSessionFactory()
		// .openSession(DataBaseConnection.getConnection(appDetails));

		VendorEmailNotificationMaster vendorEmailNotificationMaster = null;

		// try {
		// session.beginTransaction();

		vendorEmailNotificationMaster = new VendorEmailNotificationMaster();

		log.info(" ============save vendor email notification is starting========== ");

		vendorEmailNotificationMaster.setEmailDate(new Date());

		vendorEmailNotificationMaster.setEmailContent(content);

		vendorEmailNotificationMaster.setEmailSubject(subject);

		vendorEmailNotificationMaster.setFinalDateOfReply(null);

		vendorEmailNotificationMaster.setEmailNotificationType(2);

		List<Template> templates = session.createQuery(
				"From Template where isActive=1").list();
		Template template = null;
		if (templates != null && templates.size() != 0) {
			template = templates.get(0);
		}

		vendorEmailNotificationMaster.setCreatedBy(userId);

		vendorEmailNotificationMaster.setTemplate(template);

		vendorEmailNotificationMaster.setCreatedOn(new Date());

		session.save(vendorEmailNotificationMaster);

		log.info(" ============saved vendor email notification successfully========== ");

		/*
		 * Store templateQuestions and Email notification detail for get the
		 * correct number of template questions by vendor allocated at the time
		 * of appraisal.
		 */

		/* Get the list of template questions based on template id */
		List<TemplateQuestions> templateQuestions = session
				.createCriteria(TemplateQuestions.class)
				.add(Restrictions.eq("template", template)).list();

		/*
		 * Adding all the template questions into email notification template
		 * detail table at the time of appraisal
		 */

		for (TemplateQuestions tempQuestions : templateQuestions) {
			VendorEmailNotificationTemplateDetail emailNotificationTemplateDetail = new VendorEmailNotificationTemplateDetail();

			log.info(" ============save vendor email notification template detail is starting========== ");
			emailNotificationTemplateDetail
					.setVendorEmailNotificationMasterID(vendorEmailNotificationMaster);
			emailNotificationTemplateDetail
					.setTemplateQuestionsId(tempQuestions);
			session.save(emailNotificationTemplateDetail);

			log.info(" ============saved vendor email notification template detail successfully========== ");
		}

		/* Save vendor email notification details. */
		VendorMailNotificationDetail mailNotificationDetail = new VendorMailNotificationDetail();

		log.info(" ============save vendor email notification detail is starting========== ");
		mailNotificationDetail.setVendorEmailId(contact.getEmailId());
		mailNotificationDetail
				.setVendorEmailNotificationMaster(vendorEmailNotificationMaster);

		mailNotificationDetail.setVendorMaster(vendor);

		session.save(mailNotificationDetail);

		log.info(" ============saved vendor email notification detail successfully========== ");

		SendEmail mail = new SendEmail();
		result = mail.sendAssessmentToVendor(contact.getEmailId(), subject,
				content);
		// CURDDao<EmailHistory> curdTemplateHistory = new
		// CURDTemplateImpl<EmailHistory>();
		EmailHistory emailHistory = new EmailHistory();
		emailHistory.setEmailbody(content);
		emailHistory.setEmailsubject(subject);
		emailHistory.setEmailto(contact.getEmailId());
		emailHistory.setEmaildate(new Date());
		emailHistory.setEmailAddressType("TO");

		emailHistory.setEmailtype(EmailType.ASSESSMENTTOVENDOR.getIndex());

		session.save(emailHistory);
		// session.getTransaction().commit();

		// curdTemplateHistory.save(emailHistory, appDetails);
		// } catch (HibernateException e) {
		// /* print the exception in log file. */
		// PrintExceptionInLogFile.printException(e);
		// session.getTransaction().rollback();
		// } finally {
		// if (session.isConnected()) {
		// try {
		// session.connection().close();
		// session.close();
		// } catch (HibernateException e) {
		// PrintExceptionInLogFile.printException(e);
		// } catch (SQLException e) {
		// PrintExceptionInLogFile.printException(e);
		// }
		// }
		// }
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateApproval(Integer selectedId, Integer prime,
			Integer userId, Integer approvalStatus, String isApprovedDesc,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		VendorMaster vendorMaster = null;
		Users currentUserId = null;
		try {
			session.beginTransaction();
			currentUserId = (Users) session.createCriteria(Users.class)
					.add(Restrictions.eq("id", userId)).uniqueResult();

			vendorMaster = (VendorMaster) session.get(VendorMaster.class,
					selectedId);
			if (vendorMaster != null) {

				log.info(" ============update vendor master is starting========== ");
				vendorMaster.setPrimeNonPrimeVendor(Byte.parseByte(prime
						.toString()));
				vendorMaster.setIsApproved(Byte.parseByte(approvalStatus
						.toString()));
				vendorMaster.setApprovedOn(new Date());
				vendorMaster.setApprovedBy(currentUserId);
				vendorMaster.setIsApprovedDesc(isApprovedDesc);
				session.update(vendorMaster);
				session.getTransaction().commit();
				log.info(" ============updated vendor master successfully========== ");
			} else {
				return "failure";
			}

			result = "success";
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return result;
	}

	/**
	 * Get the vendor master details based in vendorId.
	 */
	@Override
	public VendorMaster getVendor(Integer id, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		VendorMaster vendorMaster = null;
		try {
			session.beginTransaction();
			vendorMaster = (VendorMaster) session.createCriteria(
					VendorMaster.class).uniqueResult();
			vendorMaster = (VendorMaster) session.get(VendorMaster.class, id);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorMaster;
	}

	/**
	 * Calculate spend indirect details based on total direct sales, percentage
	 * of total direct sales, reporting from date and to date, prime vendor Id.
	 */
	@Override
	public List<SpendIndirectDto> Calculate(Double totalDirectSales,
			Double percentageOfDirectSales, Date reportingQuaterFromDate,
			Date reportingQuaterToDate, Integer primeVendorId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List calculateResult = null;

		// java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDateQuaterFromDate = new java.sql.Date(
				reportingQuaterFromDate.getTime());
		java.sql.Date sqlDateQuaterToDate = new java.sql.Date(
				reportingQuaterToDate.getTime());
		List<SpendIndirectDto> spendIndirectDtos = new ArrayList<SpendIndirectDto>();

		try {
			session.beginTransaction();
			VendorMaster primeVendor = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", primeVendorId)).uniqueResult();
			calculateResult = (List) session
					.createSQLQuery(
							"SELECT customer_spenddata_uploaddetail.DIVERSECERTIFICATE,"
									+ "SUM(customer_spenddata_uploaddetail.SPENDVALUE)DIRECTSPENDVALUE,(SUM(customer_spenddata_uploaddetail.SPENDVALUE) * "
									+ percentageOfDirectSales
									+ ")/100,(SUM(customer_spenddata_uploaddetail.SPENDVALUE)/"
									+ totalDirectSales
									+ ")*100 "
									+ "FROM customer_spenddata_uploaddetail,customer_spenddata_uploadmaster WHERE ( customer_spenddata_uploadmaster.ID = customer_spenddata_uploaddetail.SPENDDATAMASTERID ) AND ( ( customer_spenddata_uploadmaster.PRIMEVENDORID = "
									+ primeVendor.getId()
									+ " ) "
									+ "AND ( customer_spenddata_uploadmaster.SPEND_STARTDATE >= '"
									+ sqlDateQuaterFromDate
									+ "' ) AND ( customer_spenddata_uploadmaster.SPEND_ENDDATE <= '"
									+ sqlDateQuaterToDate
									+ "' ) "
									+ "AND ( customer_spenddata_uploaddetail.ISVALID = 1 )) GROUP BY customer_spenddata_uploaddetail.DIVERSECERTIFICATE ")
					.list();
			if (calculateResult != null) {
				Iterator iterator = calculateResult.iterator();
				while (iterator.hasNext()) {
					SpendIndirectDto indirectDto = new SpendIndirectDto();
					Object[] indirectSpendValue = (Object[]) iterator.next();
					indirectDto
							.setDiverseCertificate((String) indirectSpendValue[0]);
					indirectDto
							.setDirectSpendValue((Double) indirectSpendValue[1]);
					indirectDto
							.setProrated_amount((Double) indirectSpendValue[2]);
					indirectDto
							.setDirect_Spendpercent((Double) indirectSpendValue[3]);
					spendIndirectDtos.add(indirectDto);
				}
			}

		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return spendIndirectDtos;
	}

	@Override
	public CustomerIndirectSpendDataMaster saveSpendIndirect(Integer vendorId,
			String reportingYear, Date reportingQuaterFromDate,
			Date reportingQuaterToDate, Double totalDirectSales,
			Double percentageOfDirectSales, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		CustomerIndirectSpendDataMaster customerIndirectSpendDataMaster = null;
		try {
			session.beginTransaction();
			customerIndirectSpendDataMaster = new CustomerIndirectSpendDataMaster();

			log.info(" ============save indirect spend data master is starting========== ");
			customerIndirectSpendDataMaster
					.setReportingPeriodEndDate(reportingQuaterToDate);
			customerIndirectSpendDataMaster
					.setReportingPeriodStartDate(reportingQuaterFromDate);
			customerIndirectSpendDataMaster.setReportingYear(reportingYear);
			customerIndirectSpendDataMaster
					.setTotalPrimeSupplierSales(totalDirectSales);
			customerIndirectSpendDataMaster
					.setTotalCustomerSales(percentageOfDirectSales);

			VendorContact vendorContact = new VendorContact();
			vendorContact.setId(vendorId);

			customerIndirectSpendDataMaster.setVendorContact(vendorContact);

			session.save(customerIndirectSpendDataMaster);

			log.info(" ============saved indirect spend data master successfully========== ");

		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return customerIndirectSpendDataMaster;
	}

	@Override
	public VendorEmailNotificationMaster saveMailNotificationInformation(
			MailForm mailForm, UserDetailsDto userDetails,
			Integer currentUserId, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		VendorEmailNotificationMaster result = null;

		VendorEmailNotificationMaster vendorEmailNotificationMaster = null;

		try {
			session.beginTransaction();

			vendorEmailNotificationMaster = new VendorEmailNotificationMaster();

			log.info(" ============save vendor email notification is starting========== ");

			vendorEmailNotificationMaster.setEmailDate(new Date());

			vendorEmailNotificationMaster
					.setEmailContent(mailForm.getMessage());

			vendorEmailNotificationMaster
					.setEmailSubject(mailForm.getSubject());

			// vendorEmailNotificationMaster.setCustomerVendorAssessment();

			vendorEmailNotificationMaster.setEmailNotificationType(1);

			vendorEmailNotificationMaster.setCreatedBy(currentUserId);

			vendorEmailNotificationMaster.setCreatedOn(new Date());

			session.save(vendorEmailNotificationMaster);

			if (null != mailForm.getAttachment()
					&& mailForm.getAttachment().getFileSize() != 0) {
				String companyCode = appDetails.getCustomer().getCustCode();
				String filePath = Constants
						.getString(Constants.EMAIL_ATTACHMENT_PATH)
						+ "/"
						+ companyCode;
				File theDir = new File(filePath);

				// if the directory does not exist, create it
				if (!theDir.exists()) {
					theDir.mkdirs();
				}
				// Saving Image to appropriate path
				CommonUtils.saveAttachment(mailForm.getAttachment(), filePath,
						vendorEmailNotificationMaster.getId().toString());
				vendorEmailNotificationMaster
						.setFilename(vendorEmailNotificationMaster.getId()
								.toString()
								+ File.separator
								+ mailForm.getAttachment().getFileName());
				vendorEmailNotificationMaster.setFilesize(mailForm
						.getAttachment().getFileSize());
				vendorEmailNotificationMaster.setFileType(mailForm
						.getAttachment().getContentType());
				vendorEmailNotificationMaster.setPhysicalpath(filePath);
				session.merge(vendorEmailNotificationMaster);

			}
			log.info(" ============saved vendor email notification successfully========== ");

			String[] emailIds = mailForm.getTo().split(";");

			for (String emailId : emailIds) {

				VendorMailNotificationDetail mailNotificationDetail = new VendorMailNotificationDetail();
				mailNotificationDetail.setVendorEmailId(emailId);
				mailNotificationDetail
						.setVendorEmailNotificationMaster(vendorEmailNotificationMaster);

				// mailNotificationDetail.setTemplate(template);

				VendorContact contact = (VendorContact) session
						.createCriteria(VendorContact.class)
						.add(Restrictions.eq("emailId", emailId))
						.uniqueResult();
				if (contact != null) {
					mailNotificationDetail.setVendorMaster(contact
							.getVendorId());
				}
				session.save(mailNotificationDetail);
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorEmailNotificationMaster;
	}

	/**
	 * Retrieve mail notification details based on customer
	 */
	@Override
	public List<?> retriveMailnotificationDetails(UserDetailsDto appDetails,
			Integer currentUserId) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<VendorMailNotificationDetail> listOfMailNotifications = null;
		try {
			session.getTransaction().begin();

			listOfMailNotifications = session
					.createQuery(
							"From VendorMailNotificationDetail where vendorEmailNotificationMaster.createdBy="
									+ currentUserId).list();

			session.getTransaction().commit();

		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return listOfMailNotifications;
	}

	@Override
	public String changeVendorStatus(Integer selectedId, Users user,
			String status,String bpSegemntId, String isApprovedDesc, UserDetailsDto appDetails,
			Integer prime, String url) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		VendorMaster vendorMaster = null;
		Users currentUserId = null;
		try {
			session.beginTransaction();

			vendorMaster = (VendorMaster) session.get(VendorMaster.class,
					selectedId);
			if (vendorMaster != null) {

				log.info(" ============update vendor master is starting========== ");
				WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
				if (null != status
						&& (status.equalsIgnoreCase(VendorStatus.ACTIVE
								.getIndex()) || status
								.equalsIgnoreCase(VendorStatus.BPSUPPLIER
										.getIndex()))) {
					vendorMaster.setIsActive((byte) 1);
					vendorMaster.setApprovedOn(new Date());
					vendorMaster.setApprovedBy(currentUserId);
					vendorMaster.setIsApproved((byte) 1);
					configuration.followWorkFlowForApprove(session, appDetails,
							vendorMaster, url,status, user.getId());
				} else if (status.equalsIgnoreCase(VendorStatus.INACTIVE
						.getIndex())) {
					vendorMaster.setIsActive((byte) 0);
				} else if (status.equalsIgnoreCase(VendorStatus.NEWREGISTRATION
						.getIndex()) || status.equalsIgnoreCase(VendorStatus.PENDINGREVIEW
								.getIndex())){
					vendorMaster.setIsActive((byte) 1);
				}
				if (prime != null && prime == 0) {
					vendorMaster.setPrimeNonPrimeVendor((byte) 0);
					configuration.followWorkFlow(session, appDetails,
							vendorMaster, url, user.getId());
				} else if (prime != null && prime == 1) {
					vendorMaster.setPrimeNonPrimeVendor((byte) 1);
				}
				vendorMaster.setVendorStatus(status);
				vendorMaster.setIsApprovedDesc(isApprovedDesc);
				if(bpSegemntId != null && bpSegemntId.length()>0)
					vendorMaster.setBpSegment(Integer.parseInt(bpSegemntId));
				else
					vendorMaster.setBpSegment(null);
				session.update(vendorMaster);
				log.info(" ============updated vendor master successfully========== ");

				CustomerVendorStatusLog log = new CustomerVendorStatusLog();

				log.setVendorId(vendorMaster);
				log.setVendorStatus(status);
				log.setCreatedBy(user);
				log.setCreatedOn(new Date());
				log.setStatusDescription(isApprovedDesc);

				session.save(log);

				session.getTransaction().commit();
			} else {
				return "failure";
			}

			result = "success";
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return result;
	}
}
