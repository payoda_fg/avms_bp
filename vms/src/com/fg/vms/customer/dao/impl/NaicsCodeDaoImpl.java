/*
 * NaicsCodeDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsCodeDao;
import com.fg.vms.customer.dto.NaicsCodeDto;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.customer.pojo.NaicsCodeForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * This is the implementation class for naics code interface.
 * 
 * @author vinoth
 * 
 */
public class NaicsCodeDaoImpl implements NaicsCodeDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String saveNaicsCode(NaicsCodeForm naicsCodeForm, Integer userId,
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		try {
			session.beginTransaction();

			NaicsCode naicsCode = new NaicsCode();

			/* Save the naics code details after validation. */

			log.info(" ============save naics code is starting========== ");

			naicsCode.setNaicsCode(naicsCodeForm.getNaicsCode());
			naicsCode.setNaicsDescription(naicsCodeForm.getNaicsDescription());
			naicsCode.setParentId(naicsCodeForm.getNaicsParent());
			naicsCode.setIsActive(naicsCodeForm.getIsActive());
			naicsCode.setCreatedBy(userId);
			naicsCode.setCreatedOn(new Date());
			session.save(naicsCode);

			log.info(" ============saved naics code successfully========== ");
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NaicsCode> listNaicsParents(UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<NaicsCode> naicsCodes = new ArrayList<NaicsCode>();

		try {

			/* Fetch the list of active naics codes. */

			// Criteria criteria = session.createCriteria(NaicsCode.class)
			// .setProjection(
			// Projections.distinct(Projections
			// .property("parentId")));

			String sqlQuery = "select DISTINCT {a.*} from customer_naicscode a, customer_naicscode b"
					+ " where a.NAICSID=b.PARENTID or a.PARENTID=0 order by a.NAICSDESCRIPTION asc";
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			query.addEntity("a", NaicsCode.class);
			Iterator<Object> iterator = query.list().iterator();
			while (iterator.hasNext()) {

				NaicsCode naics = (NaicsCode) iterator.next();

				naicsCodes.add(naics);
			}

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsCodes;
	}

	/**
	 * Pack of naics codes.
	 * 
	 * @param naicsCodes
	 * @return
	 */
	// private List<NaicsCodeDto> packNaicsCode(List<NaicsCode> naicsCodes) {
	//
	// List<NaicsCodeDto> codeDtos = new ArrayList<NaicsCodeDto>();
	// for (NaicsCode naicsCode : naicsCodes) {
	//
	// NaicsCodeDto codeDto = new NaicsCodeDto();
	// codeDto.setId(naicsCode.getId());
	// codeDto.setNaicsCode(naicsCode.getNaicsCode());
	// codeDto.setNaicsDesc(naicsCode.getNaicsDescription());
	// codeDto.setNaicsParent(naicsCode.getParentId());
	// codeDto.setActive(naicsCode.getIsActive());
	//
	// codeDtos.add(codeDto);
	//
	// }
	// return codeDtos;
	// }

	@Override
	public List<NaicsCode> listNaics(UserDetailsDto appDetails, String query) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<NaicsCode> naicsCodes = null;
		List<NaicsCodeDto> codeDtos = null;
		try {

			/* Fetch the list of active naics codes. */

			// Criteria criteria = session.createCriteria(NaicsCode.class)
			// .setProjection(
			// Projections.distinct(Projections
			// .property("parentId")));

			naicsCodes = session.createQuery(query).list();
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsCodes;
	}

	@Override
	public String editNaicsCode(String description, Integer id,
			Integer parentId, String naicscode, Integer userId,
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		try {
			session.beginTransaction();

			if (id != null) {
				NaicsCode naicsCode = (NaicsCode) session.get(NaicsCode.class,
						id);

				/* Save the naics code details after validation. */

				log.info(" ============save naics code is starting========== ");

				naicsCode.setNaicsDescription(description);
				naicsCode.setNaicsCode(naicscode);
				naicsCode.setParentId(parentId);
				naicsCode.setModifiedBy(userId);
				naicsCode.setModifiedOn(new Date());
				session.update(naicsCode);
			} else {
				NaicsCode naicsCode = new NaicsCode();
				naicsCode.setNaicsDescription(description);
				naicsCode.setNaicsCode(naicscode);
				naicsCode.setParentId(parentId);
				naicsCode.setIsActive((byte) 1);
				naicsCode.setCreatedBy(userId);
				naicsCode.setCreatedOn(new Date());
				session.save(naicsCode);
			}

			log.info(" ============saved naics code successfully========== ");
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public List<NaicsCode> sizeByCode(UserDetailsDto appDetails,
			Integer parentId) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<NaicsCode> naicsCodes = null;
		List<NaicsCodeDto> codeDtos = null;
		try {

			/* Fetch the list of active naics codes. */

			// Criteria criteria = session.createCriteria(NaicsCode.class)
			// .setProjection(
			// Projections.distinct(Projections
			// .property("parentId")));

			naicsCodes = session.createQuery(
					"from NaicsCode n where n.isActive=1 and n.parentId="
							+ parentId + " ORDER BY n.naicsDescription ASC")
					.list();
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsCodes;
	}

	@Override
	public String uniqueNaicsCode(UserDetailsDto appDetails, String naicscode) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<NaicsCode> naicsCodes = null;
		String result = "success";
		try {
			session.getTransaction().begin();
			/* Fetch the list of active naics codes. */

			naicsCodes = session.createQuery(
					"from NaicsCode n where n.naicsCode='" + naicscode + "'")
					.list();
			if (naicsCodes != null && naicsCodes.size() != 0) {
				result = "failure";
			} else {
				result = "success";
			}
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String deleteNaics(UserDetailsDto appDetails, Integer id) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		NaicsCode naicsCode = null;
		String result = "success";
		try {
			session.getTransaction().begin();
			/* Fetch the list of active naics codes. */

			naicsCode = (NaicsCode) session.get(NaicsCode.class, id);
			if (naicsCode != null) {
				session.delete(naicsCode);
			}
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			result = "failure";
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public List<NaicsCode> leafNode(UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<NaicsCode> naicsCode = new ArrayList<NaicsCode>();

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT DISTINCT {t1.*} FROM customer_naicscode AS t1 LEFT JOIN customer_naicscode as t2  "
					+ "ON t1.NAICSID = t2.PARENTID WHERE t2.NAICSID IS NULL ");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			query.addEntity("t1", NaicsCode.class);
			Iterator<Object> iterator = query.list().iterator();
			while (iterator.hasNext()) {

				NaicsCode naics = (NaicsCode) iterator.next();

				naicsCode.add(naics);
			}

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */

			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return naicsCode;
	}

	/**
	 * Delete the selected vendor NAICS record based on ajax request.
	 * 
	 * @param vendorNaicsID
	 * @param appDetails
	 */
	public void deleteVendorNaics(Integer vendorNaicsID,
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		VendorNAICS vendorNAICS = null;
		try {
			session.getTransaction().begin();
			/* Fetch the list of active naics codes. */
			vendorNAICS = (VendorNAICS) session.get(VendorNAICS.class,
					vendorNaicsID);
			if (vendorNAICS != null) {
				//session.delete(vendorNAICS);
				Query q = session.createQuery("delete VendorNAICS where id = "+vendorNaicsID);
				q.executeUpdate();
			}
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}
}
