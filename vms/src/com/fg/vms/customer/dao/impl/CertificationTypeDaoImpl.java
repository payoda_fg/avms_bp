/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CertificationTypeDao;
import com.fg.vms.customer.model.CertificationTypeDivision;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerClassificationCertificateTypes;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.pojo.CertificationTypeForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author venkatesh
 * 
 */
public class CertificationTypeDaoImpl implements CertificationTypeDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	@Override
	public String createCertificationType(
			CertificationTypeForm certificationTypeForm, Integer userId,
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = "";
		try {
			session.beginTransaction();
			/* To check unique Certification name. */
			CustomerCertificateType certificationType1 = (CustomerCertificateType) session
                    .createCriteria(CustomerCertificateType.class)
                    .add(Restrictions.eq("certificateTypeDesc",
                    		certificationTypeForm.getDescription())).uniqueResult();

            if (certificationType1 != null) 
            {
                result = "descNameExisted";
            } 
            else 
            {
            	CustomerCertificateType certificationType = new CustomerCertificateType();
    			/* Save the email template after validation. */
    			log.info(" ============save Messages starting========== ");

    			certificationType.setCertificateTypeDesc(certificationTypeForm
    					.getDescription());
    			certificationType.setShortDescription(certificationTypeForm
    					.getShortDescription());
    			certificationType.setIsActive(Byte.valueOf(certificationTypeForm
    					.getIsActive()));
    			certificationType.setIsCertificateExpiryAlertRequired(certificationTypeForm.getIsCertificateExpiryAlertRequired());
    			certificationType.setCreatedBy(userId);
    			certificationType.setCreatedOn(new Date());
    			session.save(certificationType);
    			
    			/* Save related CertificationType Divisions. */
    			if (certificationTypeForm.getCertifiacateDivision() != null
    					&& certificationTypeForm.getCertifiacateDivision().length != 0) {

    				for (String division : certificationTypeForm.getCertifiacateDivision()) {
    					CertificationTypeDivision certificationTypeDivisions = new CertificationTypeDivision();
    					CustomerDivision customerDivision = (CustomerDivision) session
    							.get(CustomerDivision.class,
    									Integer.parseInt(division));
    					certificationTypeDivisions.setCustomerDivisionId(customerDivision);
    					certificationTypeDivisions.setCertTypeId(certificationType);
    					certificationTypeDivisions.setIsActive(Byte.valueOf(certificationTypeForm
    							.getIsActive()));
    					certificationTypeDivisions.setCreatedBy(userId);
    					certificationTypeDivisions.setCreatedOn(new Date());
    					session.save(certificationTypeDivisions);
    				}
    			}

    			log.info(" ============saved Messages successfully========== ");

    			session.getTransaction().commit();
    			result = "success";
            }
			
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String updateCertificationType(
			CertificationTypeForm certificationTypeForm, Integer userId,
			UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CustomerCertificateType certificationType = null;
		try {

			certificationType = (CustomerCertificateType) session.get(
					CustomerCertificateType.class,
					certificationTypeForm.getCertificateId());

			if (null != certificationType) {
				/* Update the retrieved user details. */
				certificationType.setCertificateTypeDesc(certificationTypeForm
						.getDescription());
				certificationType.setShortDescription(certificationTypeForm
						.getShortDescription());
				certificationType.setIsActive(Byte
						.valueOf(certificationTypeForm.getIsActive()));
				certificationType.setIsCertificateExpiryAlertRequired(certificationTypeForm.getIsCertificateExpiryAlertRequired());
				certificationType.setModifiedBy(userId);
				certificationType.setModifiedOn(new Date());
				session.update(certificationType);
			}
			
			/* Update Division of CertificationType */
			if (certificationTypeForm.getCertifiacateDivision() != null
						&& certificationTypeForm.getCertifiacateDivision().length != 0) {

			session.createQuery(
					" delete CertificationTypeDivision where certTypeId="
						+ certificationType.getId()).executeUpdate();

			for (String division : certificationTypeForm.getCertifiacateDivision())
			{
				CertificationTypeDivision certificationTypeDivisions = new CertificationTypeDivision();
				CustomerDivision customerDivision = (CustomerDivision) session
						.get(CustomerDivision.class,
								Integer.parseInt(division));
				certificationTypeDivisions.setCustomerDivisionId(customerDivision);
				certificationTypeDivisions.setCertTypeId(certificationType);
				certificationTypeDivisions.setIsActive(Byte.valueOf(certificationTypeForm
						.getIsActive()));
				certificationTypeDivisions.setCreatedBy(userId);
				certificationTypeDivisions.setCreatedOn(new Date());
				session.save(certificationTypeDivisions);
			}
			} else {
			 session.createQuery("delete CertificationTypeDivision where certTypeId="
						+ certificationType.getId()).executeUpdate();
						}
		 
			session.getTransaction().commit();
			return "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";

		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public String viewCertificationType(int certificateId,
			UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		String message = null;
		List<CustomerCertificateType> certificationTypes = null;
		try {

			/* Fetch the list of active messages. */
			certificationTypes = session.createQuery(
					"from CustomerCertificateType where id=" + certificateId)
					.list();
			if (null != certificationTypes) {
				CustomerCertificateType certificationType = certificationTypes
						.get(0);
				message = certificationType.getShortDescription();
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return message;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerCertificateType> listCertificationType(
			UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		List<CustomerCertificateType> certificationTypes = null;
		try {

			/* Fetch the list of active messages. */
			certificationTypes = session.createQuery(
					"from CustomerCertificateType").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificationTypes;
	}

	@Override
	public CustomerCertificateType retrieveCertificationType(int certificateId,
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CustomerCertificateType certificationType = null;
		try {
			/* View/Retrieve the message based on unique certificateType id. */
			certificationType = (CustomerCertificateType) session
					.createCriteria(CustomerCertificateType.class)
					.add(Restrictions.eq("id", certificateId)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return certificationType;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String removeCertificationType(int certificateId,
			UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		String result = "";
		CustomerCertificateType certificationType = null;
		try {

			/* delete the selected message. */
			certificationType = (CustomerCertificateType) session
					.createCriteria(CustomerCertificateType.class)
					.add(Restrictions.eq("id", certificateId)).uniqueResult();

			List<CustomerClassificationCertificateTypes> certificateTypes = (List<CustomerClassificationCertificateTypes>) session
					.createCriteria(
							CustomerClassificationCertificateTypes.class)
					.add(Restrictions
							.eq("certificateTypeId", certificationType)).list();
			session.createQuery(
					" delete CertificationTypeDivision where certTypeId="
							+ certificationType.getId()).executeUpdate();

			if (null != certificateTypes && certificateTypes.size() > 0) {
				return "reference";
			}

			if (certificationType != null) {
				certificationType.setIsActive((byte) 0);
				session.merge(certificationType);
				result = "success";
				session.getTransaction().commit();
			} else {
				result = "failure";
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
}
