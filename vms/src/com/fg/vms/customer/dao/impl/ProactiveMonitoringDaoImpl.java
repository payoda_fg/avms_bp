/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.joda.time.DateTime;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CertificateDao;
import com.fg.vms.customer.dao.ProactiveMonitoringDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dto.CertificateExpiration;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.CertificateExpirationNotification;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.ProactiveMonitoringCfg;
import com.fg.vms.customer.model.ReportDue;
import com.fg.vms.customer.model.StatusMaster;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.ProactiveMonitoringForm;
import com.fg.vms.customer.pojo.Tier2ReportForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.EmailType;
import com.fg.vms.util.FrequencyPeriod;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SendEmail;

/**
 * @author gpirabu
 * 
 */
public class ProactiveMonitoringDaoImpl implements ProactiveMonitoringDao
{
	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * 
	 * @param value
	 * @return
	 */
	private Integer convertStrToInt(String value) {

		if (value != null && !value.trim().isEmpty()) {
			return Integer.parseInt(value);
		}
		return -1;

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fg.vms.customer.dao.ProactiveMonitoringDao#save(com.fg.vms.admin.
	 * dto.UserDetailsDto, com.fg.vms.customer.pojo.ProactiveMonitoringForm)
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public String save(UserDetailsDto appDetails, ProactiveMonitoringForm form,
			Integer userId) {
		String result = "";
		Session session = null;
		session = HibernateUtilCustomer.buildSessionFactory().openSession(
				DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();

			ProactiveMonitoringCfg monitoringCfg = null;
			List<ProactiveMonitoringCfg> cfgs = null;
			cfgs = session.createQuery("from ProactiveMonitoringCfg").list();
			if (cfgs != null && !cfgs.isEmpty()) {
				monitoringCfg = cfgs.get(0);
			} else {
				monitoringCfg = new ProactiveMonitoringCfg();
			}

			monitoringCfg.setDailyEveryDayNumber(convertStrToInt(form
					.getDailyEveryDayNumber()));
			monitoringCfg
					.setDailyOption(convertStrToInt(form.getDailyOption()));
			monitoringCfg.setMonthlyDayNumber(convertStrToInt(form
					.getMonthlyDayNumber()));
			monitoringCfg.setMonthlyMonthNumber(convertStrToInt(form
					.getMonthlyMonthNumber()));
			monitoringCfg.setMonthlyOption(convertStrToInt(form
					.getMonthlyOption()));
			monitoringCfg.setMonthlyWeekDay(convertStrToInt(form
					.getMonthlyWeekDay()));
			monitoringCfg.setMonthlyWeekNumber(form.getMonthlyWeekNumber());
			monitoringCfg.setNumberOfOccurrence(convertStrToInt(form
					.getNumberOfOccurrence()));
			monitoringCfg.setOccurenceEndate(CommonUtils.dateConvertValue(form
					.getOccurenceEndate()));
			monitoringCfg.setRangeOccurenceOption(convertStrToInt(form
					.getRangeOccurenceOption()));
			// monitoringCfg.setRangeOccurrence(form.getRangeOccurrence());
			monitoringCfg.setRangeStartDate(CommonUtils.dateConvertValue(form
					.getRangeStartDate()));
			monitoringCfg.setReccurrencePattern(convertStrToInt(form
					.getReccurrencePattern()));

			String weekDay = StringUtils.join(form.getWeeklyWeekDays(), ',');
			monitoringCfg.setWeeklyWeekDays(weekDay);
			monitoringCfg.setWeeklyWeekNumber(convertStrToInt(form
					.getWeeklyWeekNumber()));
			monitoringCfg.setYearlyDayNumber(convertStrToInt(form
					.getYearlyDayNumber()));
			monitoringCfg.setYearlyMonthNumber(convertStrToInt(form
					.getYearlyMonthNumber()));
			monitoringCfg.setYearlyOption(convertStrToInt(form
					.getYearlyOption()));
			monitoringCfg.setYearlyWeekDayNumber(convertStrToInt(form
					.getYearlyWeekDayNumber()));
			monitoringCfg.setYearlyWeekNumber(form.getYearlyWeekNumber());
			monitoringCfg.setCreatedOn(new Date());
			monitoringCfg.setCreatedBy(userId);

			if (cfgs != null && !cfgs.isEmpty()) {
				session.merge(monitoringCfg);
				// invokeTrigger(monitoringCfg, appDetails);
			} else {
				session.save(monitoringCfg);
				// createTrigger(monitoringCfg, appDetails);
			}

			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * 
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public ProactiveMonitoringCfg retrive(UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();

			List<ProactiveMonitoringCfg> cfgs = null;
			cfgs = session.createQuery("from ProactiveMonitoringCfg").list();
			if (cfgs != null && !cfgs.isEmpty()) {
				return cfgs.get(0);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return null;
	}

	/**
	 * 
	 */
	@Override
	public void notifyReportDue(UserDetailsDto userDetails) {
		VendorDao dao2 = new VendorDaoImpl();

		try {
			// SELECT vm.ID, vu.EMAILID FROM customer_vendor_users vu,
			// customer_vendormaster vm
			// WHERE vu.VENDORID = vm.ID AND vu.ISPRIMARYCONTACT = 1 AND
			// vm.ISACTIVE = 1 AND vm.PRIMENONPRIMEVENDOR = 1 AND vm.ISAPPROVED
			// = 1;

			String query = "Select new com.fg.vms.customer.dto.EmailDistribution(vm.id, vu.emailId) from VendorContact vu, VendorMaster vm "
					+ "where vu.vendorId = vm.id and vu.isPrimaryContact = 1 and vm.isActive = 1 and vm.primeNonPrimeVendor = 1 and vm.isApproved = 1";
			List<EmailDistribution> list = dao2.retriveEmailIds(userDetails,
					query);

			if (list != null && list.size() != 0 && !list.isEmpty()) {
				DateTime proactiveEamilDate = new DateTime();
				Date startDate = new Date();
				Date endDate = new Date();

				Tier2ReportingPeriodDao reportingPeriod = new Tier2ReportingPeriodDaoImpl();
				Tier2ReportingPeriod period = reportingPeriod
						.currentReportingPeriod(userDetails);

				if (period != null) {
					int year = Calendar.getInstance().get(Calendar.YEAR);
					Calendar c = Calendar.getInstance();

					switch (FrequencyPeriod.valueOf(period.getDataUploadFreq())) {
					case M:
						c.set(year, proactiveEamilDate.getMonthOfYear() - 1, 1);
						c.set(Calendar.DAY_OF_MONTH,
								c.getActualMinimum(Calendar.DAY_OF_MONTH));
						startDate = c.getTime();

						c.set(year, proactiveEamilDate.getMonthOfYear() - 1, 1);
						c.set(Calendar.DAY_OF_MONTH,
								c.getActualMaximum(Calendar.DAY_OF_MONTH));
						endDate = c.getTime();
						break;

					case Q:
						c.set(year, proactiveEamilDate.getMonthOfYear() - 3, 1);
						c.set(Calendar.DAY_OF_MONTH,
								c.getActualMinimum(Calendar.DAY_OF_MONTH));
						startDate = c.getTime();

						c.set(year, proactiveEamilDate.getMonthOfYear() - 1, 1);
						c.set(Calendar.DAY_OF_MONTH,
								c.getActualMaximum(Calendar.DAY_OF_MONTH));
						endDate = c.getTime();
						break;

					case A:
						break;
					}
				}

				WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration workflowConfiguration = configuration
						.listWorkflowConfig(userDetails);

				Calendar cal = new GregorianCalendar();
				cal.set(Calendar.DATE, cal.get(Calendar.DATE)
						+ workflowConfiguration.getDaysUpload()
						+ workflowConfiguration.getReportDue());
				Date dueDate = cal.getTime();

				CURDDao<ReportDue> curdDao = new CURDTemplateImpl<ReportDue>();
				List<String> emailIds = new ArrayList<String>();
				for (EmailDistribution distribution : list) {
					ReportDue reportDue = new ReportDue();

					reportDue.setEmailid(distribution.getEmailId());
					reportDue.setVendorid(distribution.getId());
					reportDue.setDuedate(dueDate);
					reportDue.setProactiveeamildate(new Date());
					reportDue.setReportenddate(endDate);
					reportDue.setReportstartdate(startDate);
					reportDue.setIssubmitted("NO");
					curdDao.save(reportDue, userDetails);

					emailIds.add(distribution.getEmailId());
				}

				SendEmail sendEmail = new SendEmail();
				String content = "Dear Supplier, this is just a friendly reminder that your Supplier Spend Report is due on: "
						+ dueDate;
				sendEmail
						.sendReportDueMail(
								StringUtils.join(emailIds.toArray(), ','),
								"Report Due", content, EmailType.REPORTDUE,
								userDetails);
			} else {
				log.info("There is no EmailId for Report Due Alert.");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		}
	}

	/**
	 * 
	 */
	@Override
	public void notifyCertificateExpiration(UserDetailsDto userDetails,
			WorkflowConfiguration workflowConfiguration, String appRoot) {
		log.info("Inside notifyCertificateExpiration Method.");

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE,
				cal.get(Calendar.DATE) + workflowConfiguration.getDaysExpiry());
		Date d1 = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(d1);

		CertificateDao certificateDao = new CertificateDaoImpl();
		LoginDao loginDao = new LoginDaoImpl();

		try {
			this.notifyCertificateExpires(userDetails);

			String customerName = userDetails.getCustomer().getCustName();
			String link = "http://";
			// CustomerContacts contacts = userDetails.getCurrentCustomer();

			CustomerApplicationSettings applicationSettings = loginDao
					.getCustomerApplicationSettings(userDetails);
			String logoPath = applicationSettings.getLogoPath();

			StringBuilder query = new StringBuilder(
					"Select new com.fg.vms.customer.dto.CertificateExpiration( vm.id,vu.emailId, vu.firstName,vu.lastName, "
							+ "vm.vendorName, vc.certificateNumber,ca.agencyName, cert.certificateName,vc.expiryDate,cert.certificateName,vc.id,c.applicationUrl) from VendorContact vu, "
							+ "VendorMaster vm, VendorCertificate vc,CertifyingAgency ca, "
							+ "Certificate cert, CustomerCertificateType cct, StatusMaster st, Customer c "
							+ "where vu.vendorId=vm.id and vm.id=vc.vendorId  and vm.primeNonPrimeVendor=0 "
							+ "and vm.parentVendorId=0 and ca.id=vc.certAgencyId "
							+ "and cert.id=vc.certMasterId AND vc.certificateType = cct.id AND cct.isCertificateExpiryAlertRequired = 1 "
							+ "and st.id = vm.vendorStatus and st.iscertificateexpiryalertreq = 1 "
							+ "and vu.isPrimaryContact=1 and vm.isActive=1  and vm.isApproved=1 "
							+ "and vc.expiryDate<='"
							+ date
							+ "' "
							+ "AND vc.id NOT IN (SELECT cen.vendorCertificateId FROM CertificateExpirationNotification cen WHERE cen.status = 'NO')");

			System.out.println("ProactiveMonitoringDaoImpl @342:" + query);

			List<CertificateExpiration> certificateExpirations = certificateDao
					.certificateExpiration(userDetails, query);

			if (certificateExpirations != null
					&& certificateExpirations.size() != 0) {
				for (int i = 0; i < certificateExpirations.size(); i++) {
					certificateExpirations.get(i).setCustomerName(customerName);
					certificateExpirations.get(i).setLinkToSite(link);
					// certificateExpirations.get(i).setCustomerAdminEmailId(contacts.getEmailId());

					/*
					 * if (workflowConfiguration.getAdminGroup() != null &&
					 * workflowConfiguration.getAdminGroup()
					 * .getEmailDistributionMasterId() != null) {
					 * CURDDao<EmailDistributionListDetailModel> curdDao = new
					 * CURDTemplateImpl<EmailDistributionListDetailModel>();
					 * List<EmailDistributionListDetailModel> detailModels =
					 * curdDao .list(userDetails,
					 * "from EmailDistributionListDetailModel where emailDistributionMasterId = "
					 * + workflowConfiguration .getAdminGroup()
					 * .getEmailDistributionMasterId());
					 * 
					 * if (detailModels != null && !detailModels.isEmpty()) {
					 * certificateExpirations.get(i).setCc(
					 * CommonUtils.packToEmailIds(detailModels)); } }
					 */

					if (workflowConfiguration.getCcEmailId() != null
							&& !workflowConfiguration.getCcEmailId().isEmpty()) {
						certificateExpirations.get(i).setCc(
								Arrays.asList(workflowConfiguration
										.getCcEmailId()));
					}
					certificateExpirations.get(i).setSupportEmailId(
							workflowConfiguration.getSupportMailId());
				}

				log.info("Calling sendCertificateExpirationAlert Method to send Email.");
				SendEmail sendEmail = new SendEmail();
				sendEmail.sendCertificateExpirationAlert(
						certificateExpirations, appRoot, logoPath, userDetails);
			} else {
				log.info("There is no Expired Certificates for Certificate Expiration Notification Alert.");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		}
	}

	@Override
	public void notifyReportDueNotSubmitted(UserDetailsDto userDetails) {
		try {
			String query = "From ReportDue where issubmitted='NO'";
			CURDDao<ReportDue> curdDao = new CURDTemplateImpl<ReportDue>();
			List<ReportDue> reportDues = curdDao.list(userDetails, query);

			if (!reportDues.isEmpty() && reportDues.size() > 0) {
				SendEmail sendEmail = new SendEmail();
				sendEmail.sendReportDueNextDay(reportDues, userDetails);
			} else {
				log.info("There is no Not Submitted Reports for Report Due Not Submitted Alert.");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fg.vms.customer.dao.ProactiveMonitoringDao#notifyCertificateExpires
	 * (com.fg.vms.admin.dto.UserDetailsDto)
	 */
	@Override
	public void notifyCertificateExpires(UserDetailsDto userDetails) {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) - 1);
		Date d1 = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(d1);

		try {
			String query = "From CertificateExpirationNotification where status='NO' and expirationdate='"
					+ date + "'";
			CURDDao<CertificateExpirationNotification> curdDao = new CURDTemplateImpl<CertificateExpirationNotification>();
			List<CertificateExpirationNotification> notifications = curdDao
					.list(userDetails, query);

			if (notifications != null && notifications.size() != 0
					&& !notifications.isEmpty()) {
				log.info("Calling sendCertificateExpirationNextDay Method to Send Email.");
				SendEmail sendEmail = new SendEmail();
				sendEmail.sendCertificateExpirationNextDay(notifications,
						userDetails);
			} else {
				log.info("There is no Expired Certificates for Certificate Expiration Reminder Alert.");
			}

			// for (CertificateExpirationNotification due : notifications) {
			// emailIds.add(due.getEmailId());
			//
			// }
			// String content="Dear Supplier,\n This is just a friendly reminder
			// that your _____________ Certificate expires on _______________.
			// Please update the Certification Expiration Date with the new date
			// and upload the new certificate. Please complete this task prior
			// to the Certification expiration date. Thank you very much.
			// if (emailIds != null && !emailIds.isEmpty()) {
			// SendEmail sendEmail = new SendEmail();
			// System.out.println(StringUtils.join(emailIds.toArray(), ','));
			// sendEmail.sendMail(StringUtils.join(emailIds.toArray(), ','),
			// null, null, "Certificate Expires ",
			// "Certificate Expires ",
			// EmailType.CERTIFICATEEXPIRATION, userDetails);
			// }
		} catch (Exception exception) {
			exception.printStackTrace();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		}
	}

	@Override
	public void certificateExpiryNotificationMonthlyReport(
			UserDetailsDto userDetails) {
		log.info("Calling certificateExpiryNotificationMonthlyReport in ProactiveMonitoringDaoImpl.");
		Tier2ReportForm reportForm = new Tier2ReportForm();
		ReportDao reportDao = new ReportDaoImpl();

		List<Tier2ReportDto> certificateExpiryReport = null;

		try {
			WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration workflowConfiguration = configuration
					.listWorkflowConfig(userDetails);

			Calendar cal1 = Calendar.getInstance();

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			cal.set(Calendar.DATE, 1);

			int year = cal.get(Calendar.YEAR);
			Date startDate = cal.getTime();

			cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
			Date endDate = cal.getTime();

			String excelName = "Certficate_Expiration_Notification_"
					+ new SimpleDateFormat("MMMM").format(startDate) + "_"
					+ year;
			String subject = "Certficate Expiration Notification Report Between ("
					+ CommonUtils.convertDateToString(startDate)
					+ " - "
					+ CommonUtils.convertDateToString(endDate) + ")";

			if (cal1.get(Calendar.DAY_OF_MONTH) == 1) {
				// To Set Start and End Date Values in the Report Form.
				reportForm.setStartDate(CommonUtils
						.convertDateToString(startDate));
				reportForm.setEndDate(CommonUtils.convertDateToString(endDate));

				// To Set Vendor Status Values in the Report Form Starts Here.
				CURDTemplateImpl<StatusMaster> dao = new CURDTemplateImpl<StatusMaster>();
				List<StatusMaster> vendorStatusMaster = dao
						.list(userDetails,
								"From StatusMaster where isactive = 1 and iscertificateexpiryalertreq = 1 order by disporder");

				if (vendorStatusMaster != null
						&& vendorStatusMaster.size() != 0) {
					String statusIds[] = new String[vendorStatusMaster.size()];
					for (int i = 0; i < vendorStatusMaster.size(); i++) {
						statusIds[i] = vendorStatusMaster.get(i).getId()
								.toString();
					}
					reportForm.setVendorStatusList(statusIds);
				}
				// To Set Vendor Status Values in the Report Form Ends Here.

				// Retrieving Data from Database
				certificateExpiryReport = reportDao
						.getCertificateExpirationNotificationEmailReport(
								reportForm, userDetails);

				if (workflowConfiguration.getAdminGroup() != null) {
					CURDDao<EmailDistributionListDetailModel> curdDao = new CURDTemplateImpl<EmailDistributionListDetailModel>();
					List<EmailDistributionListDetailModel> detailModels = curdDao
							.list(userDetails,
									" from EmailDistributionListDetailModel where emailDistributionMasterId = "
											+ workflowConfiguration
													.getAdminGroup()
													.getEmailDistributionMasterId());

					if (detailModels != null && !detailModels.isEmpty()) {
						if (certificateExpiryReport != null
								&& certificateExpiryReport.size() > 0) {
							new SendEmail()
									.sendCertificateExpirationNotificationMonthlyEmailReport(
											userDetails,
											CommonUtils
													.packToEmailIds(detailModels),
											CommonUtils
													.packCCEmailIds(detailModels),
											CommonUtils
													.packBCCEmailIds(detailModels),
											certificateExpiryReport,
											excelName,
											subject,
											getExcelAttachment(
													certificateExpiryReport,
													excelName));
							log.info("Sending certificateExpiryNotificationMonthlyReport Email with Data.");
						} else {
							new SendEmail()
									.sendZeroCertificateExpirationNotificationMonthlyEmailReport(
											userDetails,
											CommonUtils
													.packToEmailIds(detailModels),
											CommonUtils
													.packCCEmailIds(detailModels),
											CommonUtils
													.packBCCEmailIds(detailModels),
											subject);
							log.info("Sending certificateExpiryNotificationMonthlyReport Email without Data.");
						}
					}
				}
			}
		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
		}
	}

	private ByteArrayOutputStream getExcelAttachment(
			List<Tier2ReportDto> certificateExpirationNotificationList,
			String excelName) {
		List<String> columnTitle = new ArrayList<String>();

		columnTitle.add("VENDOR NAME");
		columnTitle.add("EMAIL ID");
		columnTitle.add("EMAIL DATE");
		columnTitle.add("CERTIFICATE NUMBER");
		columnTitle.add("CERTIFICATE NAME");
		columnTitle.add("AGENCY NAME");
		columnTitle.add("EXPIRATION DATE");
		columnTitle.add("IS RENEWED");

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFFont headerFont = wb.createFont();
			headerFont
					.setBoldweight(org.apache.poi.ss.usermodel.Font.BOLDWEIGHT_BOLD);

			// Set Header Cell Style
			HSSFCellStyle headerStyle = wb.createCellStyle();
			headerStyle.setFont(headerFont);

			HSSFSheet sheet1 = wb.createSheet(excelName);

			// Excel Column Header Part
			if (columnTitle != null && !columnTitle.isEmpty()) {
				// Create Sheet
				int cellNumber = 0;

				// Header will be always in first row.
				HSSFRow headerRow = sheet1.createRow(0);
				for (String titleValue : columnTitle) {
					HSSFCell cell = headerRow.createCell(cellNumber);
					cell.setCellStyle(headerStyle);
					cell.setCellValue(titleValue.toUpperCase());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;
				}
			}

			// Excel column values.
			if (certificateExpirationNotificationList != null
					&& !certificateExpirationNotificationList.isEmpty()) {
				int rowNumber = 1;

				for (Tier2ReportDto reportDto : certificateExpirationNotificationList) {
					int cellNumber = 0;

					HSSFRow contentRow = sheet1.createRow(rowNumber);

					HSSFCell cell = contentRow.createCell(cellNumber);
					cell.setCellValue(reportDto.getVendorName());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell1 = contentRow.createCell(cellNumber);
					cell1.setCellValue(reportDto.getEmailId());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell2 = contentRow.createCell(cellNumber);
					cell2.setCellValue(reportDto.getEmailDate());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell3 = contentRow.createCell(cellNumber);
					cell3.setCellValue(reportDto.getCertificateNumber());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell4 = contentRow.createCell(cellNumber);
					cell4.setCellValue(reportDto.getCertificateName());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell5 = contentRow.createCell(cellNumber);
					cell5.setCellValue(reportDto.getCertAgencyName());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell6 = contentRow.createCell(cellNumber);
					cell6.setCellValue(reportDto.getExpDate());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					HSSFCell cell7 = contentRow.createCell(cellNumber);
					cell7.setCellValue(reportDto.getCertificateExpiryStatus());
					sheet1.autoSizeColumn(cellNumber);
					cellNumber++;

					rowNumber++;
				}
			}
			wb.write(bos);
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
		return bos;
	}
}