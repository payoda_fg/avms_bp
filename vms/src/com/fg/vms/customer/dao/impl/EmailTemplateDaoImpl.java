/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.EmailTemplateDao;
import com.fg.vms.customer.dto.EmailTemplateDto;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.EmailTemplateParameters;
import com.fg.vms.customer.model.EmailType;
import com.fg.vms.customer.model.EmailtemplateDivsion;
import com.fg.vms.customer.pojo.EmailTemplateForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the email template implementation.
 * 
 * @author vinoth
 * 
 */
public class EmailTemplateDaoImpl implements EmailTemplateDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;
	
	public StringBuilder checkDivisionsCreate(EmailTemplateForm emailTemplateForm)
	{
		String customerDivisions="";
		StringBuilder query = new StringBuilder();		
		
		int size = emailTemplateForm.getEmailtemplateDivision().length;
		String []strArray = emailTemplateForm.getEmailtemplateDivision();
		for(int ctr=0; ctr<size; ctr++)	
		{
			if(ctr==0)
				customerDivisions = strArray[ctr];
			else
				customerDivisions += "," + strArray[ctr];
		}			
		query.append("select * from customer_emailtemplate cet, customer_emailtemplate_division ced where cet.TEMPLATECODE=" +emailTemplateForm.getEmailTemplateName());
		query.append(" and ced.EMAILTEMPLATEID=cet.ID AND ced.CUSTOMERDIVISIONID IN (" + customerDivisions +")");
		System.out.println(query.toString());			
			
		return query;
	}

	@Override
	public String createEmailTemplates(EmailTemplateForm emailTemplateForm,
			Integer userId, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		String result = "";
		if(emailTemplateForm.getEmailTemplateMessage()!=null)
		{
			String orginalMessage=emailTemplateForm.getEmailTemplateMessage();
			String splitedMessage[]=orginalMessage.split("<img src=\"");
			String changedMessage="";
			
			if(splitedMessage.length>=2)
				changedMessage=changedMessage+splitedMessage[0];
			for(int i=1;i<splitedMessage.length;i++){
					changedMessage=changedMessage+"<img src=\""+emailTemplateForm.getImageBaseUrl()+""+splitedMessage[i];
			}
			if(splitedMessage.length>=2)
				emailTemplateForm.setEmailTemplateMessage(changedMessage);
			
		}
		try {
			session.beginTransaction();
			EmailTemplate template = new EmailTemplate();

			/* Save the email template after validation. */
			log.info(" ============save email template is starting========== ");
			//From here onwords NewTemplate creation starting,
			EmailType emailType=retriveTemplateName(emailTemplateForm.getEmailTemplateName(), userDetails);
			template.setTemplateCode(emailType);
			template.setEmailTemplateName(emailType.getDescription());
			
			template.setEmailTemplateMessage(emailTemplateForm
					.getEmailTemplateMessage());
			template.setEmailTemplateSubject(emailTemplateForm.getSubject());
			template.setIsActive((byte) 1);
			template.setCreatedBy(userId);
			template.setCreatedOn(new Date());
			session.save(template);

			/* Save related emailTemplate Divisions. */
			if(null != userDetails.getSettings()) 
			{
				if(null != userDetails.getSettings().getIsDivision() && 0 != userDetails.getSettings().getIsDivision()) 
				{
					if (null != emailTemplateForm.getEmailtemplateDivision() && 0 != emailTemplateForm.getEmailtemplateDivision().length) 
					{
						List<?> check = null;
						StringBuilder query = checkDivisionsCreate(emailTemplateForm);
						check = session.createSQLQuery(query.toString()).list();
							
						if(!check.isEmpty())
						{
							return "templateExists";						
						}
						else
						{
							for(String division : emailTemplateForm.getEmailtemplateDivision()) 
							{
								EmailtemplateDivsion emailTemplateTypeDivisions = new EmailtemplateDivsion();
								CustomerDivision customerDivision = (CustomerDivision) session
										.get(CustomerDivision.class,
												Integer.parseInt(division));
								emailTemplateTypeDivisions
										.setCustomerDivisionId(customerDivision);
								emailTemplateTypeDivisions.setEmailTemplateId(template);
								emailTemplateTypeDivisions.setIsActive(template
										.getIsActive());
								emailTemplateTypeDivisions.setCreatedBy(userId);
								emailTemplateTypeDivisions.setCreatedOn(new Date());
								session.save(emailTemplateTypeDivisions);
							}
						}
					}										
				}
			}		
			
			/* Save params of emailTemplate . */
//			if (emailTemplateForm.getEmailtemplateParam() != null) {
//				/* To check unique Certification name. */
//				EmailTemplateParameters checkUniqueParam = (EmailTemplateParameters) session
//						.createCriteria(EmailTemplateParameters.class)
//						.add(Restrictions.eq("parameterName",
//								emailTemplateForm.getEmailtemplateParam()))
//						.uniqueResult();
//
//				if (checkUniqueParam == null) {
//					EmailTemplateParameters emailTemplateParameters = new EmailTemplateParameters();
//					emailTemplateParameters.setEmailTemplateId(template);
//					emailTemplateParameters.setParameterName(emailTemplateForm
//							.getEmailtemplateParam());
//					emailTemplateParameters.setIsActive(template.getIsActive());
//					emailTemplateParameters.setCreatedBy(userId);
//					emailTemplateParameters.setCreatedOn(new Date());
//					session.save(emailTemplateParameters);
//				}
//			}
			/* Save Parameters of EmailTemplate . */
			if (emailTemplateForm.getEmailTemplateParameters() != null) {
			
				String arrayParams[]=emailTemplateForm.getEmailTemplateParameters().split(",");
                String res=CheckTemplateParamUnity(arrayParams);
				if ((arrayParams != null) && (arrayParams.length!=0)) {
					
					for(String parameter : arrayParams)
					{
					EmailTemplateParameters emailTemplateParameters = new EmailTemplateParameters();
					emailTemplateParameters.setEmailTemplateId(template);
					emailTemplateParameters.setParameterName(parameter);
					emailTemplateParameters.setIsActive(template.getIsActive());
					emailTemplateParameters.setCreatedBy(userId);
					emailTemplateParameters.setCreatedOn(new Date());
					session.save(emailTemplateParameters);
					}
				}
			}
			log.info(" ============saved email template successfully========== ");
            
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmailTemplate> listTemplates(UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		List<EmailTemplate> emailTemplates = null;
		try {

			/* Fetch the list of active email templates. */
			emailTemplates = session
					.createQuery(
							"from EmailTemplate where isActive=1 ORDER BY emailTemplateName ASC")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailTemplates;
	}

	@Override
	public String deleteEmailTemplate(int templateId, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		String result = "";
		EmailTemplate template = null;
		List<EmailTemplateParameters> templateParameters=null;
		List<EmailtemplateDivsion> referenceDivisions = new ArrayList<EmailtemplateDivsion>();
		try {

			/* check for divisions for template before deactivating */
			referenceDivisions = session.createQuery(
					"from EmailtemplateDivsion where emailTemplateId="
							+ templateId).list();

			if (referenceDivisions.size() == 0) {

				/* deactive the selected email template. */
				template = (EmailTemplate) session
						.createCriteria(EmailTemplate.class)
						.add(Restrictions.eq("id", templateId)).uniqueResult();
				
				
				session.createQuery(
						" delete EmailTemplateParameters where emailTemplateId="
								+template.getId() ).executeUpdate();
						
				
				if (template != null) {
					session.delete(template);
					result = "success";
					session.getTransaction().commit();
				} else {
					result = "failure";
				}
			} else {
				result = "reference";
			}

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return result;
	}

	@Override
	public EmailTemplate editEmailTemplate(int templateId,
			UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		EmailTemplate template = null;
		try {
			/* View/Retrieve the template based on unique template id. */
			template = (EmailTemplate) session
					.createCriteria(EmailTemplate.class)
					.add(Restrictions.eq("id", templateId)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return template;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String updateEmailTemplates(EmailTemplateForm emailTemplateForm,
			Integer userId, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		EmailTemplate template = null;
		if(emailTemplateForm.getEmailTemplateMessage()!=null)
		{
			String orginalMessage=emailTemplateForm.getEmailTemplateMessage();
			String splitedMessage[]=orginalMessage.split("<img src=\"");
			String changedMessage="";
			
			if(splitedMessage.length>=2)
				changedMessage=changedMessage+splitedMessage[0];
			for(int i=1;i<splitedMessage.length;i++){
					changedMessage=changedMessage+"<img src=\""+emailTemplateForm.getImageBaseUrl()+""+splitedMessage[i];
			}
			if(splitedMessage.length>=2)
				emailTemplateForm.setEmailTemplateMessage(changedMessage);
			
		}
		try {
			session.beginTransaction();
			template = (EmailTemplate) session.get(EmailTemplate.class,
					emailTemplateForm.getTemplateId());

			if (null != template) {
				/* Update the retrieved user details. */
				
				EmailType emailType=retriveTemplateName(emailTemplateForm.
						getEmailTemplateName(), userDetails);
				template.setTemplateCode(emailType);
				template.setEmailTemplateName(emailType.getDescription());
				template.setEmailTemplateMessage(emailTemplateForm
						.getEmailTemplateMessage());
				template.setEmailTemplateSubject(emailTemplateForm.getSubject());
				template.setModifiedBy(userId);
				template.setModifiedOn(new Date());
				session.update(template);
				session.clear();
			}

			/* Update Division of Emailtemplate */
			if(null != userDetails.getSettings()) 
			{
				if(null != userDetails.getSettings().getIsDivision() && 0 != userDetails.getSettings().getIsDivision()) 
				{
					if (null != emailTemplateForm.getEmailtemplateDivision() && 0 != emailTemplateForm.getEmailtemplateDivision().length) 
					{
						String customerDivisions="";
						String []strArray = emailTemplateForm.getEmailtemplateDivision();
						
						for(int ctr=0; ctr<emailTemplateForm.getEmailtemplateDivision().length; ctr++) 
						{
							StringBuilder query = new StringBuilder();
							
							if(ctr==0)
								customerDivisions = strArray[ctr];
							else
								customerDivisions += "," + strArray[ctr];							
							
							query.append("SELECT * from customer_emailtemplate_division where CUSTOMERDIVISIONID =" + strArray[ctr]);
							query.append(" and EMAILTEMPLATEID in (select ID from customer_emailtemplate where TEMPLATECODE=" + emailTemplateForm.getEmailTemplateName()
									+")");
							System.out.println(query);
							Integer count = session.createSQLQuery(query.toString()).list().size();
							
							if(count == null || count == 0)
							{
								EmailtemplateDivsion emailTemplateTypeDivisions = new EmailtemplateDivsion();
								CustomerDivision customerDivision = (CustomerDivision) session
										.get(CustomerDivision.class, Integer.parseInt(strArray[ctr]));
								emailTemplateTypeDivisions.setCustomerDivisionId(customerDivision);
								emailTemplateTypeDivisions.setEmailTemplateId(template);
								emailTemplateTypeDivisions.setIsActive(template.getIsActive());
								emailTemplateTypeDivisions.setCreatedBy(userId);
								emailTemplateTypeDivisions.setCreatedOn(new Date());
								session.save(emailTemplateTypeDivisions);
							}			
						}
						
						//Delete Other Records of This Template
						StringBuilder query = new StringBuilder();
						query.append("delete from customer_emailtemplate_division where EMAILTEMPLATEID = " + template.getId());
						query.append(" and CUSTOMERDIVISIONID NOT IN (" + customerDivisions + ")");
						System.out.println("Delete:" + query.toString());
						session.createSQLQuery(query.toString()).executeUpdate();
					}
					else 
					{
						session.createQuery("delete EmailtemplateDivsion where emailTemplateId=" + template.getId()).executeUpdate();
					}
				}
			}					
          
			/* Update Parameters of Emailtemplate */
			if (emailTemplateForm.getEmailTemplateParameters() != null) {          
				session.createQuery(
						" delete EmailTemplateParameters where emailTemplateId="
								+ template.getId()).executeUpdate();
           
				String arrayParams[]=emailTemplateForm.getEmailTemplateParameters().split(",");
           
				if ((arrayParams != null) && (arrayParams.length!=0)) {				
					for(String parameter : arrayParams)
					{
						EmailTemplateParameters emailTemplateParameters = new EmailTemplateParameters();
						emailTemplateParameters.setEmailTemplateId(template);
						emailTemplateParameters.setParameterName(parameter);
						emailTemplateParameters.setIsActive(template.getIsActive());
						emailTemplateParameters.setCreatedBy(userId);
						emailTemplateParameters.setCreatedOn(new Date());
						session.save(emailTemplateParameters);
					}
				}
				else
				{
					session.createQuery("delete EmailTemplateParameters where emailTemplateId="	+ template.getId()).executeUpdate();  
				}
			}
			session.getTransaction().commit();
			return "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			System.out.println(exception);
			session.getTransaction().rollback();
			return "failure";
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			System.out.println(exception);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String showEmailTemplateMessage(int templateId,
			UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		String message = null;
		List<EmailTemplate> emailTemplates = null;
		try {

			/* Fetch the list of active email templates. */
			emailTemplates = session.createQuery(
					"from EmailTemplate where id=" + templateId).list();
			if (null != emailTemplates) {
				EmailTemplate template = emailTemplates.get(0);
				message = template.getEmailTemplateMessage();
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return message;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<EmailtemplateDivsion> listEmailTemplateDivisions(
			int templateId, UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();		
		List<EmailtemplateDivsion> emailTemplatesDivision = null;
		try {
			/* Fetch the list of Emailtemplate division(s). */
			emailTemplatesDivision = session.createQuery("from EmailtemplateDivsion where emailTemplateId="	+ templateId).list();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailTemplatesDivision;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<EmailTemplateParameters> listEmailTemplateParameters(
			int templateId, UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();		
		List<EmailTemplateParameters> emailTemplatesParams = null;

		try {
			/* Fetch the list of Emailtemplate Parameter(s). */
			if (templateId == 0) {
				emailTemplatesParams = session.createQuery("from EmailTemplateParameters where emailtemplateid IS NULL").list();

			} else {
				emailTemplatesParams = session.createQuery("from EmailTemplateParameters where emailTemplateId=" + templateId).list();
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailTemplatesParams;
	}
	
	public String CheckTemplateParamUnity(String param[])
	{
		String checkParams[]=param;
		String availability=null;
		if(checkParams.length!=0)
		{
			for(int i=0;i<=checkParams.length-1;i++)
			{
				for(int j=i;j<=checkParams.length-1;j++)
				{
					if(checkParams[j].equalsIgnoreCase(checkParams[i]))
					{
						availability="parameterExist";
						break;
					}
				}					
			}
		}	
		return availability;	
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<EmailType> listTemplateNames(UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		session.beginTransaction();
		List<EmailType> emailTemplateNames = null;
	
		try {
			/* Fetch the list of Emailtemplate name from Emailtype. */
			
			emailTemplateNames=session.createQuery("from EmailType where templateType='S' ORDER BY description  ASC").list();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailTemplateNames;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public EmailType retriveTemplateName(Integer templateCode,
			UserDetailsDto userDetails) {		
	  /* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		session.beginTransaction();
		EmailType emailType=null;
		try
		{		
		/* Fetch the Emailtype object based on templateId. */	       
		emailType=(EmailType)session.createCriteria(EmailType.class)
				.add(Restrictions.eq("id", templateCode)).uniqueResult();
			
		session.getTransaction().commit();
	} catch (HibernateException exception) {
		/* print the exception in log file. */
		PrintExceptionInLogFile.printException(exception);
		session.getTransaction().rollback();
	} finally {
		if (session.isConnected()) {
			try {
				session.connection().close();
				session.close();
			} catch (HibernateException e) {
				PrintExceptionInLogFile.printException(e);
			} catch (SQLException e) {
				PrintExceptionInLogFile.printException(e);
			}
		}
	}	
		return emailType;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<EmailTemplateDto> listTemplatesWithDivision(
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		session.beginTransaction();
		List<EmailTemplateDto> emailTemplateDtos=new ArrayList<EmailTemplateDto>();
		EmailTemplateDto emailTemplateDto=null;
		StringBuilder SqlQuery=new StringBuilder();
		try{
			SqlQuery.append("Select customer_emailtemplate.ID,"
					+ " customer_emailtemplate.EMAILTEMPLATESUBJECT,"
					+ "customer_emailtemplate.TEMPLATECODE,"
					+ "customer_emailtemplate.EMAILTEMPLATENAME,"
					+ "group_concat(customer_division.divisionname)as DIVISIONNAME, email_type.TEMPLATETYPE"
					+ " From customer_emailtemplate Left Join"
					+ " customer_emailtemplate_division"
					+ " On customer_emailtemplate_division.EMAILTEMPLATEID ="
					+ " customer_emailtemplate.ID left Join"
					+ " customer_division"
					+ " On customer_emailtemplate_division.CUSTOMERDIVISIONID ="
					+ "  customer_division.ID LEFT JOIN email_type ON email_type.ID = customer_emailtemplate.TEMPLATECODE"
					+ " group by"
					+ " customer_emailtemplate.ID,"
					+ " customer_emailtemplate.EMAILTEMPLATESUBJECT,"
					+ " customer_emailtemplate.TEMPLATECODE,"
					+ " customer_emailtemplate.EMAILTEMPLATENAME");
			System.out.println("SqlQuery EmailTemplateDaoImpl @724 : "+SqlQuery);
			List list=session.createSQLQuery(SqlQuery.toString()).list();
			Iterator itr=list.iterator();
			while(itr.hasNext())
			{
				emailTemplateDto=new EmailTemplateDto();
				Object[] obj=(Object[])itr.next();
				emailTemplateDto.setEmailTemplateId((Integer)obj[0]);
				emailTemplateDto.setEmailTemplateSubject(obj[1].toString());
				emailTemplateDto.setTemplateCode((Integer)obj[2]);
				emailTemplateDto.setEmailTemplateName(obj[3].toString());
				
				if(obj[4]!=null && !obj[4].toString().trim().equalsIgnoreCase(""))
					emailTemplateDto.setCustomerDivision(obj[4].toString());
				
				emailTemplateDto.setTemplateType(obj[5].toString());
				
				emailTemplateDtos.add(emailTemplateDto);
			}
		}catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailTemplateDtos;
	}

	@Override
	public String createNewEmailTemplates(EmailTemplateForm emailTemplateForm,
			Integer userId,UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		String result = "";
		int flage=0;
		if(emailTemplateForm.getEmailTemplateMessage()!=null)
		{
			String orginalMessage=emailTemplateForm.getEmailTemplateMessage();
			String splitedMessage[]=orginalMessage.split("<img src=\"");
			String changedMessage="";
			
			if(splitedMessage.length>=2)
				changedMessage=changedMessage+splitedMessage[0];
			for(int i=1;i<splitedMessage.length;i++){
					changedMessage=changedMessage+"<img src=\""+emailTemplateForm.getImageBaseUrl()+""+splitedMessage[i];
			}
			if(splitedMessage.length>=2)
				emailTemplateForm.setEmailTemplateMessage(changedMessage);
			
		}
		try {
			session.beginTransaction();
			EmailTemplate template = new EmailTemplate();
			List<EmailType> listOfEmailTypes=(List<EmailType>)session.createCriteria(EmailType.class).list();
			if(listOfEmailTypes!=null && listOfEmailTypes.size()!=0)
			{
				Iterator itr=listOfEmailTypes.iterator();
				while(itr.hasNext())
				{
					EmailType emailtype=(EmailType)itr.next();
					if(emailtype.getDescription().equalsIgnoreCase(emailTemplateForm.getEmailNewTemplateName().trim()))
					{
						flage=1;
						break;
					}
					
				}
			}
			if(flage==1)
			{
				result="duplicate";
				return result;
			}
			else{
			/* Save the email template after validation. */
			log.info(" ============save email new template is starting========== ");
			//From here onwords NewTemplate creation starting,
			EmailType emailType=new EmailType();
			emailType.setDescription(emailTemplateForm.getEmailNewTemplateName());
			emailType.setCreatedBy(userId);
			emailType.setCreatedOn(new Date());
			emailType.setTemplateType("G");
			session.save(emailType);
			
			template.setTemplateCode(emailType);
			template.setEmailTemplateName(emailType.getDescription());
			
			template.setEmailTemplateMessage(emailTemplateForm
					.getEmailTemplateMessage());
			template.setEmailTemplateSubject(emailTemplateForm.getSubject());
			template.setIsActive((byte) 1);
			template.setCreatedBy(userId);
			template.setCreatedOn(new Date());
			session.save(template);

			/* Save related emailTemplate Divisions. */
			if(null != userDetails.getSettings()) 
			{
				if(null != userDetails.getSettings().getIsDivision() && 0 != userDetails.getSettings().getIsDivision()) 
				{
					if (null != emailTemplateForm.getEmailtemplateDivision() && 0 != emailTemplateForm.getEmailtemplateDivision().length) 
					{
						List<?> check = null;
						StringBuilder query = checkDivisionsCreate(emailTemplateForm);
						check = session.createSQLQuery(query.toString()).list();
							
						if(!check.isEmpty())
						{
							return "templateExists";						
						}
						else
						{
							for(String division : emailTemplateForm.getEmailtemplateDivision()) 
							{
								EmailtemplateDivsion emailTemplateTypeDivisions = new EmailtemplateDivsion();
								CustomerDivision customerDivision = (CustomerDivision) session
										.get(CustomerDivision.class,
												Integer.parseInt(division));
								emailTemplateTypeDivisions
										.setCustomerDivisionId(customerDivision);
								emailTemplateTypeDivisions.setEmailTemplateId(template);
								emailTemplateTypeDivisions.setIsActive(template
										.getIsActive());
								emailTemplateTypeDivisions.setCreatedBy(userId);
								emailTemplateTypeDivisions.setCreatedOn(new Date());
								session.save(emailTemplateTypeDivisions);
							}
						}
					}										
				}
			}	
		}
			log.info(" ============saved email new template successfully========== ");
            
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String updateNewEmailTemplates(EmailTemplateForm emailTemplateForm,
			Integer userId, UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		EmailTemplate template = null;
		int flage=0;
		if(emailTemplateForm.getEmailTemplateMessage()!=null)
		{
			String orginalMessage=emailTemplateForm.getEmailTemplateMessage();
			String splitedMessage[]=orginalMessage.split("<img src=\"");
			String changedMessage="";
			if(splitedMessage.length>=2)
				changedMessage=changedMessage+splitedMessage[0];
			for(int i=1;i<splitedMessage.length;i++){
					changedMessage=changedMessage+"<img src=\""+emailTemplateForm.getImageBaseUrl()+""+splitedMessage[i];
			}
			if(splitedMessage.length>=2)
				emailTemplateForm.setEmailTemplateMessage(changedMessage);
			
		}
		try {
			session.beginTransaction();
			template = (EmailTemplate) session.get(EmailTemplate.class,
					emailTemplateForm.getTemplateId());
			List<EmailType> listOfEmailTypes=(List<EmailType>)session.createCriteria(EmailType.class).list();
			if(listOfEmailTypes!=null && listOfEmailTypes.size()!=0)
			{
				Iterator itr=listOfEmailTypes.iterator();
				while(itr.hasNext())
				{
					EmailType emailtype=(EmailType)itr.next();
					if(emailtype.getId()!=emailTemplateForm.getTemplateCode())
					if(emailtype.getDescription().equalsIgnoreCase(emailTemplateForm.getEmailNewTemplateName().trim()))
					{
						flage=1;
						break;
					}
					
				}
			}
			if(flage==1)
			{
				return "duplicate";
			}
			else{
			if (null != template) {
				/* Update the retrieved user details. */
				
				EmailType emailType=emailType=(EmailType)session.createCriteria(EmailType.class)
						.add(Restrictions.eq("id", emailTemplateForm.getTemplateCode())).uniqueResult();
				emailType.setDescription(emailTemplateForm.getEmailNewTemplateName());
				emailType.setTemplateType("G");
				emailType.setModifiedBy(userId);
				emailType.setModifiedOn(new Date());
				session.update(emailType);
				
				template.setTemplateCode(emailType);
				template.setEmailTemplateName(emailType.getDescription());
				template.setEmailTemplateMessage(emailTemplateForm
						.getEmailTemplateMessage());
				template.setEmailTemplateSubject(emailTemplateForm.getSubject());
				template.setModifiedBy(userId);
				template.setModifiedOn(new Date());
				session.update(template);
//				session.clear();
			}

			 //Update Division of Emailtemplate 
			if(null != userDetails.getSettings()) 
			{
				if(null != userDetails.getSettings().getIsDivision() && 0 != userDetails.getSettings().getIsDivision()) 
				{
					if (null != emailTemplateForm.getEmailtemplateDivision() && 0 != emailTemplateForm.getEmailtemplateDivision().length) 
					{
						String customerDivisions="";
						String []strArray = emailTemplateForm.getEmailtemplateDivision();
						
						for(int ctr=0; ctr<emailTemplateForm.getEmailtemplateDivision().length; ctr++) 
						{
							StringBuilder query = new StringBuilder();
							
							if(ctr==0)
								customerDivisions = strArray[ctr];
							else
								customerDivisions += "," + strArray[ctr];							
							
							query.append("SELECT * from customer_emailtemplate_division where CUSTOMERDIVISIONID =" + strArray[ctr]);
							query.append(" and EMAILTEMPLATEID in (select ID from customer_emailtemplate where TEMPLATECODE=" + emailTemplateForm.getTemplateCode()
									+")");
							System.out.println(query);
							Integer count = session.createSQLQuery(query.toString()).list().size();
							System.out.println("count: "+count);
							
							if(count == null || count == 0)
							{
								EmailtemplateDivsion emailTemplateTypeDivisions = new EmailtemplateDivsion();
								CustomerDivision customerDivision = (CustomerDivision) session
										.get(CustomerDivision.class, Integer.parseInt(strArray[ctr]));
								emailTemplateTypeDivisions.setCustomerDivisionId(customerDivision);
								emailTemplateTypeDivisions.setEmailTemplateId(template);
								emailTemplateTypeDivisions.setIsActive(template.getIsActive());
								emailTemplateTypeDivisions.setCreatedBy(userId);
								emailTemplateTypeDivisions.setCreatedOn(new Date());
								session.save(emailTemplateTypeDivisions);
							}			
						}
						
						//Delete Other Records of This Template
						StringBuilder query = new StringBuilder();
						query.append("delete from customer_emailtemplate_division where EMAILTEMPLATEID = " + template.getId());
						query.append(" and CUSTOMERDIVISIONID NOT IN (" + customerDivisions + ")");
						System.out.println("Delete:" + query.toString());
						session.createSQLQuery(query.toString()).executeUpdate();
					}
					else 
					{
						session.createQuery("delete EmailtemplateDivsion where emailTemplateId=" + template.getId()).executeUpdate();
					}
				}
			}	
		}
			session.getTransaction().commit();
			return "update";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			System.out.println(exception);
			session.getTransaction().rollback();
			return "failure";
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			System.out.println(exception);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}
}
