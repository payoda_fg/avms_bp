package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.VendorDocumentsDao;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.VendorHistory;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorInsurance;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

public class VendorDocumentsDaoImpl implements VendorDocumentsDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorCertificate downloadCertificate(Integer id,
			UserDetailsDto userDetails) {

		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		VendorCertificate certificate = null;
		try {
			session.beginTransaction();

			certificate = (VendorCertificate) session
					.createCriteria(VendorCertificate.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorOtherCertificate downloadOtherCertificate(Integer id,
			UserDetailsDto userDetails) {
		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		VendorOtherCertificate certificate = null;
		try {
			session.beginTransaction();

			certificate = (VendorOtherCertificate) session
					.createCriteria(VendorOtherCertificate.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificate;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VendorDocuments> getVendorDocuments(Integer vendorId,
			UserDetailsDto appDetails) {

		List<VendorDocuments> vendorDocuments = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorDocuments = session.createQuery(
					"from VendorDocuments where vendorId=" + vendorId).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorDocuments;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorDocuments downloadDocument(Integer id,
			UserDetailsDto userDetails) {
		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		VendorDocuments documents = null;
		try {
			session.beginTransaction();

			documents = (VendorDocuments) session
					.createCriteria(VendorDocuments.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return documents;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteDocument(int docId, UserDetailsDto appDetails) {
		String result = "success";

		VendorDocuments vendorDocuments = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			vendorDocuments = (VendorDocuments) session.get(
					VendorDocuments.class, docId);
			session.delete(vendorDocuments);
			result = vendorDocuments.getDocumentFilename();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public List<VendorDocuments> getVendorDocumentsList(
			VendorContact currentVendor, UserDetailsDto appDetails) {
		List<VendorDocuments> vendorDocuments = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorDocuments = session.createQuery(
					"from VendorDocuments where vendorId="
							+ currentVendor.getVendorId().getId()).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorDocuments;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GeographicalStateRegionDto> getVendorServiceAreas(
			Integer vendorId, UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<GeographicalStateRegionDto> regionDtos = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("select sa.ID, (select GROUP_CONCAT(region.REGIONNAME) from region, statebyregion where"
					+ " region.ID= statebyregion.REGIONID and statebyregion.STATEID=s.ID and"
					+ " region.ISACTIVE=1 and statebyregion.ISACTIVE=1)RegionName, s.STATENAME "
					+ "from state s,customer_vendorservicearea sa where sa.STATEID = s.ID and sa.VENDORID= "
					+ vendorId);

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					dto.setGeographicServiceAreaId(Integer.parseInt(objects[0]
							.toString()));
					dto.setRegion(objects[1].toString());
					dto.setState(objects[2].toString());
					regionDtos.add(dto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return regionDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerVendorCommodity> getVendorCommodities(Integer vendorId,
			UserDetailsDto appDetails) {

		List<CustomerVendorCommodity> vendorCommodities = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorCommodities = session.createQuery(
					"from CustomerVendorCommodity where vendorId=" + vendorId)
					.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorCommodities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteServiceArea(Integer serviceAreaId,
			UserDetailsDto appDetails) {

		CustomerVendorServiceArea serviceArea = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			serviceArea = (CustomerVendorServiceArea) session.get(
					CustomerVendorServiceArea.class, serviceAreaId);
			session.delete(serviceArea);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCommodity(Integer commodityId, UserDetailsDto appDetails) {

		CustomerVendorCommodity commodity = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			commodity = (CustomerVendorCommodity) session.get(
					CustomerVendorCommodity.class, commodityId);
			session.delete(commodity);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerVendorAddressMaster getMailingAddress(Integer vendorId,
			String addressType, UserDetailsDto appDetails) {

		CustomerVendorAddressMaster addressMaster = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			addressMaster = (CustomerVendorAddressMaster) session.createQuery(
					"from CustomerVendorAddressMaster where addressType = '"
							+ addressType + "' and vendorId=" + vendorId)
					.uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return addressMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerVendorInsurance downloadInsuranceDocument(
			Integer insuranceId, UserDetailsDto userDetails) {

		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		CustomerVendorInsurance insurance = null;
		try {
			session.beginTransaction();
			insurance = (CustomerVendorInsurance) session
					.createCriteria(CustomerVendorInsurance.class)
					.add(Restrictions.eq("id", insuranceId)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return insurance;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerVendorInsurance getListVendorInsurance(Integer vendorId,
			UserDetailsDto appDetails) {

		CustomerVendorInsurance insurance = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			insurance = (CustomerVendorInsurance) session.createQuery(
					"from CustomerVendorInsurance where vendorId=" + vendorId)
					.uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return insurance;
	}

	@Override
	public List<VendorHistory> modifiedDate(UserDetailsDto userDetails,
			Integer vendorId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<VendorHistory> vendorHistories = null;
		try {
			session.beginTransaction();
			Query query = session.createSQLQuery(
					"CALL records_modify_sp(:vendorId)").setParameter(
					"vendorId", vendorId);

			List result = query.list();
			if (null != result) {
				Iterator iterator = result.iterator();
				vendorHistories = new ArrayList<VendorHistory>();
				while (iterator.hasNext()) {
					Object[] objects = (Object[]) iterator.next();
					String info;
					Date createdOn;
					Date modifiedOn;
					info = (String) objects[0];
					createdOn = (Date) objects[1];
					modifiedOn = (Date) objects[2];
					vendorHistories.add(new VendorHistory(info, createdOn,
							modifiedOn));
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorHistories;
	}

	@Override
	public List<CommodityDto> listVendorCommodities(Integer vendorId,
			UserDetailsDto appDetails) {
		
		List<CommodityDto> listVendorCommodities=new ArrayList<CommodityDto>();
		CommodityDto commodityDto=null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		Iterator itr=null;

		try {
			session.beginTransaction();
			String query="select customer_vendorcommodity.id,"
					+ " customer_vendorcommodity.COMMODITYID,customer_commodity.COMMODITYCODE,"
					+ " IFNULL(customer_commodity.COMMODITYDESCRIPTION,''),"
					+ " IFNULL(customer_commoditycategory.CATEGORYDESCRIPTION,''),"
					+ " IFNULL(customer_marketsector.SECTORDESCRIPTION,'')"
					+ " from customer_vendorcommodity"
					+ " inner join customer_commodity on customer_vendorcommodity.COMMODITYID=customer_commodity.ID"
					+ " left join customer_commoditycategory on customer_commodity.COMMODITYCATEGORYID=customer_commoditycategory.ID"
					+ " left join customer_marketsector on customer_marketsector.ID=customer_commoditycategory.MARKETSECTORID"
					+ " where customer_vendorcommodity.VENDORID="+vendorId;
			System.out.println("@ VendorDocumentsDaoImpl 616 : "+query);
			SQLQuery sqlQuery=session.createSQLQuery(query);
			List list=sqlQuery.list();
			if(list!=null)
			{
				itr=list.iterator();
				while(itr.hasNext())
				{
					commodityDto=new CommodityDto();
					Object[] obj=(Object[])itr.next();
					commodityDto.setVendorCommodityId(Integer.parseInt(obj[0].toString()));
					commodityDto.setCommodityId(Integer.parseInt(obj[1].toString()));
					commodityDto.setCommodityCode(obj[2].toString());
					commodityDto.setCommodityDescription(obj[3].toString());
					commodityDto.setCategoryDesc(obj[4].toString());
					commodityDto.setSectorDescription(obj[5].toString());
					listVendorCommodities.add(commodityDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return listVendorCommodities;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<CustomerVendorKeywords> getVendorKeywords(Integer vendorId, UserDetailsDto appDetails) {
		List<CustomerVendorKeywords> vendorKeywords = null;

		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorKeywords = session.createQuery("from CustomerVendorKeywords where vendorId=" + vendorId).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorKeywords;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public String deleteKeywords(int keywordsId, UserDetailsDto appDetails) {
		String result = "success";

		CustomerVendorKeywords vendorKeyword = null;
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		
		try {
			session.beginTransaction();
			vendorKeyword = (CustomerVendorKeywords) session.get(CustomerVendorKeywords.class, keywordsId);
			session.delete(vendorKeyword);			
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
}