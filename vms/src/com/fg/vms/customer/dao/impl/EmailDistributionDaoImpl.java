/*
 * EmailDistributionDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.EmailDistributionDao;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.pojo.EmailDistributionForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class EmailDistributionDaoImpl implements EmailDistributionDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createEmailDistribution(
			EmailDistributionForm distributionForm, int currentUserId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "unique";
		try {
			session.beginTransaction();

			EmailDistributionModel emailDistributionModel = new EmailDistributionModel();

			/* Save the email distribution details after validation. */

			log.info(" ============save email distribution is starting========== ");

			/* To check unique email distribution. */
			EmailDistributionModel distributionModel = (EmailDistributionModel) session
					.createCriteria(EmailDistributionModel.class)
					.add(Restrictions.eq("emailDistributionListName",
							distributionForm.getEmailDistributionListName()))
					.uniqueResult();

			if (distributionModel == null) {

				emailDistributionModel
						.setEmailDistributionListName(distributionForm
								.getEmailDistributionListName());
				emailDistributionModel.setIsActive(Byte.valueOf((byte) 1));
				emailDistributionModel.setCreatedBy(currentUserId);
				emailDistributionModel.setCreatedOn(new Date());
				emailDistributionModel.setModifiedBy(currentUserId);
				emailDistributionModel.setModifiedOn(new Date());
				session.save(emailDistributionModel);
				// session.getTransaction().commit();
			} else {
				return result;
			}

			// Save in Email Distribution List detail table

			if (distributionForm.getCheckbox() != null) {
				String st[] = distributionForm.getCheckbox();
				for (int i = 0; i < st.length; i++) {
					/* To check unique email distribution. */
					EmailDistributionListDetailModel distributionListDetailModel = new EmailDistributionListDetailModel();

					EmailDistributionListDetailModel emailDistributionListDetailModel = (EmailDistributionListDetailModel) session
							.createCriteria(
									EmailDistributionListDetailModel.class)
							.add(Restrictions.eq("emailDistributionMasterId",
									emailDistributionModel))
							.add(Restrictions.eq("userId.id",
									Integer.parseInt(st[i]))).uniqueResult();

					if (emailDistributionListDetailModel == null) {
						Users userObject = null;
						userObject = (Users) session.get(Users.class,
								Integer.parseInt(st[i]));
						distributionListDetailModel
								.setEmailDistributionMasterId(emailDistributionModel);
						distributionListDetailModel.setUserId(userObject);
						distributionListDetailModel.setVendorId(null);
						distributionListDetailModel.setIsActive(Byte
								.valueOf((byte) 1));
						distributionListDetailModel.setCreatedBy(currentUserId);
						distributionListDetailModel.setCreatedOn(new Date());
						distributionListDetailModel
								.setModifiedBy(currentUserId);
						distributionListDetailModel.setModifiedOn(new Date());

						distributionListDetailModel.setIsTo(check(
								distributionForm.getCheckboxIsTo(),
								userObject.getId()));
						distributionListDetailModel.setIsCC(check(
								distributionForm.getCheckboxIsCC(),
								userObject.getId()));
						distributionListDetailModel.setIsBCC(check(
								distributionForm.getCheckboxIsBCC(),
								userObject.getId()));
						session.save(distributionListDetailModel);
					}
				}
			}
			if (distributionForm.getCheckbox2() != null) {
				String st1[] = distributionForm.getCheckbox2();
				for (int i = 0; i < st1.length; i++) {

					/* To check unique email distribution. */
					EmailDistributionListDetailModel distributionListDetailModel = new EmailDistributionListDetailModel();

					EmailDistributionListDetailModel emailDistributionListDetailModel = (EmailDistributionListDetailModel) session
							.createCriteria(
									EmailDistributionListDetailModel.class)
							.add(Restrictions.eq("emailDistributionMasterId",
									emailDistributionModel))
							.add(Restrictions.eq("userId.id",
									Integer.parseInt(st1[i].trim())))
							.uniqueResult();

					// Get vendor contact object.
					VendorContact vendorContact = (VendorContact) session.get(
							VendorContact.class,
							Integer.parseInt(st1[i].trim()));

					if (emailDistributionListDetailModel == null) {
						distributionListDetailModel
								.setEmailDistributionMasterId(emailDistributionModel);
						distributionListDetailModel.setUserId(null);
						distributionListDetailModel.setVendorId(vendorContact);
						distributionListDetailModel.setIsActive(Byte
								.valueOf((byte) 1));
						distributionListDetailModel.setCreatedBy(currentUserId);
						distributionListDetailModel.setCreatedOn(new Date());
						distributionListDetailModel
								.setModifiedBy(currentUserId);
						distributionListDetailModel.setModifiedOn(new Date());
						distributionListDetailModel.setIsTo(check(
								distributionForm.getVendorCheckboxIsTo(),
								vendorContact.getId()));
						distributionListDetailModel.setIsCC(check(
								distributionForm.getVendorCheckboxIsCC(),
								vendorContact.getId()));
						distributionListDetailModel.setIsBCC(check(
								distributionForm.getVendorCheckboxIsBCC(),
								vendorContact.getId()));
						session.save(distributionListDetailModel);
					}
				}
			}

			log.info(" ============saved email distribution successfully========== ");
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	@Override
	public List<EmailDistribution> viewVendorEmailList(UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<EmailDistribution> contacts = null;
		try {

			/* Fetch the list of active users. */
			// contacts = session.getNamedQuery("findByAllContact").list();
			// .createQuery(
			// "select new EmailDistribution(vm.vendorName,vc) from VendorMaster vm, VendorContact vc  where vm.id=vendorId.id and vc.isActive=1 and vc.isPrimaryContact = 1 ORDER BY vc.lastName ASC")
			// .list();
			contacts = session
					.createQuery(
							"select new com.fg.vms.customer.dto.EmailDistribution(vc.id,vendor.vendorName,vc.firstName,vc.emailId,vendor.primeNonPrimeVendor,status.statusname) "
									+ " from VendorContact vc join vc.vendorId vendor, StatusMaster status   "
									+ " where status.id= vendor.vendorStatus  and vc.isPrimaryContact = 1 ORDER BY vc.lastName ASC")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return contacts;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param divisionIds
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<EmailDistribution> viewVendorEmailList(
			UserDetailsDto appDetails, String divisionIds) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<EmailDistribution> contacts = null;
		try {

			/* Fetch the list of active users. */
			// contacts = session.getNamedQuery("findByAllContact").list();
			// .createQuery(
			// "select new EmailDistribution(vm.vendorName,vc) from VendorMaster vm, VendorContact vc  where vm.id=vendorId.id and vc.isActive=1 and vc.isPrimaryContact = 1 ORDER BY vc.lastName ASC")
			// .list();

			String query = "";
			if (divisionIds != null && divisionIds.length() != 0) {

				query = "select new com.fg.vms.customer.dto.EmailDistribution(vc.id,vendor.vendorName,vc.firstName,"
						+ "vc.emailId,vendor.primeNonPrimeVendor,status.statusname) "
						+ "from VendorContact vc join vc.vendorId vendor, StatusMaster status ,VendorMaster master "
						+ "where status.id= vendor.vendorStatus and "
						+ "((vendor.parentVendorId=0 and vendor.vendorStatus !='I') or (vendor.parentVendorId>0 and vendor.vendorStatus not in ('S','I'))) "
						+ "and vc.isPrimaryContact = 1  and master.id=vc.vendorId "
						+ "and master.customerDivisionId in("
						+ divisionIds
						+ ") " + " ORDER BY vc.lastName ASC";

			} else {
				query = "select Distinct new com.fg.vms.customer.dto.EmailDistribution(vc.id,vendor.vendorName,vc.firstName,"
						+ "vc.emailId,vendor.primeNonPrimeVendor,status.statusname) "
						+ "from VendorContact vc join vc.vendorId vendor, StatusMaster status "
						+ "where status.id= vendor.vendorStatus and "
						+ "((vendor.parentVendorId=0 and vendor.vendorStatus !='I') or (vendor.parentVendorId>0 and vendor.vendorStatus not in ('S','I'))) "
						+ "and vc.isPrimaryContact = 1 ORDER BY vc.lastName ASC";
			}
			contacts = session.createQuery(query).list();
			System.out.println("Email Distribution Dao Impl @ 284:"+query);

			/*
			 * "select new com.fg.vms.customer.dto.EmailDistribution(vc.id,vendor.vendorName,vc.firstName,"
			 * + "vc.emailId,vendor.primeNonPrimeVendor,status.statusname) " +
			 * " from VendorContact vc join vc.vendorId vendor, StatusMaster status   "
			 * +
			 * " where status.id= vendor.vendorStatus  and vc.isPrimaryContact = 1 ORDER BY vc.lastName ASC"
			 * )
			 */

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return contacts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EmailDistributionModel retriveEmailDistribution(
			UserDetailsDto appDetails, int emailDistId) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		EmailDistributionModel emailDistributionModel = null;
		try {

			/* View/Retrieve the user based on unique email distribution id. */
			emailDistributionModel = (EmailDistributionModel) session
					.createCriteria(EmailDistributionModel.class)
					.add(Restrictions.eq("emailDistributionMasterId",
							emailDistId)).uniqueResult();
			emailDistributionModel.getEmailDistributionDetailList().size();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailDistributionModel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EmailDistributionListDetailModel retriveEmailDistributionDetail(
			UserDetailsDto appDetails, int emailDistId) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		EmailDistributionListDetailModel detailModel = null;
		try {

			/*
			 * View/Retrieve the email distribution details based on unique
			 * email distribution id.
			 */
			detailModel = (EmailDistributionListDetailModel) session
					.createCriteria(EmailDistributionListDetailModel.class)
					.add(Restrictions.eq("emailDistributionMasterId",
							emailDistId)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return detailModel;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String updateEmailDistribution(
			EmailDistributionForm distributionForm, int currentUserId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "unique";
		try {
			session.beginTransaction();

			/* Update the email distribution details after validation. */

			log.info(" ============update email distribution is starting========== ");

			/* To check unique email distribution. */
			EmailDistributionModel distributionModel = retriveEmailDistribution(
					appDetails, distributionForm.getEmailDistributionMasterId());

			if (distributionModel != null) {
				distributionModel.setEmailDistributionListName(distributionForm
						.getEmailDistributionListName());
				distributionModel.setIsActive(Byte.valueOf((byte) 1));
				distributionModel.setModifiedBy(currentUserId);
				distributionModel.setModifiedOn(new Date());
				session.update(distributionModel);
			}

			/**
			 * For Customer Admin Edit/Update Process.
			 */
			// Delete in Email Distribution List detail table if it is unchecked
			List<EmailDistributionListDetailModel> deleteList = new ArrayList<EmailDistributionListDetailModel>();
			if (distributionForm.getCheckbox1() != null) {

				String st1[] = distributionForm.getCheckbox1();
				List<EmailDistributionListDetailModel> edldList = distributionModel
						.getEmailDistributionDetailList();

				List<EmailDistributionListDetailModel> edldListTemp = new ArrayList<EmailDistributionListDetailModel>();
				for (int i = 0; i < st1.length; i++) {
					for (EmailDistributionListDetailModel edl : edldList) {
						if (Integer.parseInt(st1[i]) == edl
								.getEmailDistributionListDetailId()) {
							edl.setIsTo(check(distributionForm
									.getCheckboxIsTo(), edl.getUserId().getId()));
							edl.setIsCC(check(distributionForm
									.getCheckboxIsCC(), edl.getUserId().getId()));
							edl.setIsBCC(check(distributionForm
									.getCheckboxIsBCC(), edl.getUserId()
									.getId()));
							session.update(edl);
							edldListTemp.add(edl);
						}
					}
				}
				if (edldListTemp != null && edldListTemp.size() != 0) {
					edldList.removeAll(edldListTemp);
					for (EmailDistributionListDetailModel edl : edldList) {
						if (edl.getUserId() != null) {
							deleteList.add(edl);
						}
					}
				}

			} else {
				List<EmailDistributionListDetailModel> edldList = distributionModel
						.getEmailDistributionDetailList();
				for (EmailDistributionListDetailModel edl : edldList) {
					if (edl.getUserId() != null) {
						deleteList.add(edl);
					}
				}
			}

			// Get the newly checked email list from form and save.
			if (distributionForm.getCheckbox() != null) {
				String st1[] = distributionForm.getCheckbox();
				for (int i = 0; i < st1.length; i++) {
					/* To check unique email distribution. */
					EmailDistributionListDetailModel distributionListDetailModel = new EmailDistributionListDetailModel();
					EmailDistributionListDetailModel emailDistributionListDetailModel = null;
					List<EmailDistributionListDetailModel> emailDistributionList = session
							.createCriteria(
									EmailDistributionListDetailModel.class)
							.add(Restrictions.eq("emailDistributionMasterId",
									distributionModel))
							.add(Restrictions.eq("userId.id",
									Integer.parseInt(st1[i]))).list();
					if (emailDistributionList != null
							&& emailDistributionList.size() != 0) {
						emailDistributionListDetailModel = emailDistributionList
								.get(0);
					}
					if (emailDistributionListDetailModel == null) {
						Users userObject = null;
						userObject = (Users) session.get(Users.class,
								Integer.parseInt(st1[i]));
						distributionListDetailModel
								.setEmailDistributionMasterId(distributionModel);
						distributionListDetailModel.setUserId(userObject);
						distributionListDetailModel.setVendorId(null);
						distributionListDetailModel.setIsActive(Byte
								.valueOf((byte) 1));
						distributionListDetailModel.setCreatedBy(currentUserId);
						distributionListDetailModel.setCreatedOn(new Date());
						distributionListDetailModel
								.setModifiedBy(currentUserId);
						distributionListDetailModel.setModifiedOn(new Date());

						distributionListDetailModel.setIsTo(check(
								distributionForm.getCheckboxIsTo(),
								userObject.getId()));
						distributionListDetailModel.setIsCC(check(
								distributionForm.getCheckboxIsCC(),
								userObject.getId()));
						distributionListDetailModel.setIsBCC(check(
								distributionForm.getCheckboxIsBCC(),
								userObject.getId()));

						session.save(distributionListDetailModel);
					}
				}
			}
			/**
			 * For Vendor Edit/Update Process.
			 */
			// Delete in Email Distribution List detail table if it is unchecked
			if (distributionForm.getCheckbox3() != null) {
				String st1[] = distributionForm.getCheckbox3();
				List<EmailDistributionListDetailModel> edldList = distributionModel
						.getEmailDistributionDetailList();
				List<EmailDistributionListDetailModel> edldListTemp = new ArrayList<EmailDistributionListDetailModel>();
				for (int i = 0; i < st1.length; i++) {
					for (EmailDistributionListDetailModel edl : edldList) {
						if (Integer.parseInt(st1[i]) == edl
								.getEmailDistributionListDetailId()) {

							edl.setIsTo(check(
									distributionForm.getVendorCheckboxIsTo(),
									edl.getVendorId().getId()));
							edl.setIsCC(check(
									distributionForm.getVendorCheckboxIsCC(),
									edl.getVendorId().getId()));
							edl.setIsBCC(check(
									distributionForm.getVendorCheckboxIsBCC(),
									edl.getVendorId().getId()));
							session.update(edl);
							edldListTemp.add(edl);
						}
					}
				}
				if (edldListTemp != null && edldListTemp.size() != 0) {
					edldList.removeAll(edldListTemp);
					for (EmailDistributionListDetailModel edl : edldList) {
						if (edl.getVendorId() != null) {
							deleteList.add(edl);
						}
					}
				}
			} else {
				List<EmailDistributionListDetailModel> edldList = distributionModel
						.getEmailDistributionDetailList();
				for (EmailDistributionListDetailModel edl : edldList) {
					if (edl.getVendorId() != null) {
						deleteList.add(edl);
					}
				}
			}
			// Get the newly checked email list from form and save.
			if (distributionForm.getCheckbox2() != null) {
				String st1[] = distributionForm.getCheckbox2();
				for (int i = 0; i < st1.length; i++) {
					/* To check unique email distribution. */
					EmailDistributionListDetailModel distributionListDetailModel = new EmailDistributionListDetailModel();
					EmailDistributionListDetailModel emailDistributionListDetailModel = null;
					List<EmailDistributionListDetailModel> emailDistributionList = session
							.createCriteria(
									EmailDistributionListDetailModel.class)
							.add(Restrictions.eq("emailDistributionMasterId",
									distributionModel))
							.add(Restrictions.eq("userId.id",
									Integer.parseInt(st1[i]))).list();
					if (emailDistributionList != null
							&& emailDistributionList.size() != 0) {
						emailDistributionListDetailModel = emailDistributionList
								.get(0);
					}
					if (emailDistributionListDetailModel == null) {
						VendorContact vendorContact = (VendorContact) session
								.get(VendorContact.class,
										Integer.parseInt(st1[i]));
						distributionListDetailModel
								.setEmailDistributionMasterId(distributionModel);
						distributionListDetailModel.setUserId(null);
						distributionListDetailModel.setVendorId(vendorContact);
						distributionListDetailModel.setIsActive(Byte
								.valueOf((byte) 1));
						distributionListDetailModel.setCreatedBy(currentUserId);
						distributionListDetailModel.setCreatedOn(new Date());
						distributionListDetailModel
								.setModifiedBy(currentUserId);
						distributionListDetailModel.setModifiedOn(new Date());
						distributionListDetailModel.setIsTo(check(
								distributionForm.getVendorCheckboxIsTo(),
								vendorContact.getId()));
						distributionListDetailModel.setIsCC(check(
								distributionForm.getVendorCheckboxIsCC(),
								vendorContact.getId()));
						distributionListDetailModel.setIsBCC(check(
								distributionForm.getVendorCheckboxIsBCC(),
								vendorContact.getId()));
						session.save(distributionListDetailModel);
					}
				}
			}
			for (EmailDistributionListDetailModel deleteModel : deleteList) {
				distributionModel.getEmailDistributionDetailList().clear();
				deleteModel = (EmailDistributionListDetailModel) session
						.merge(deleteModel);
				session.delete(deleteModel);
			}
			log.info(" ============saved email distribution successfully========== ");
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteEmailDistributionList(Integer emailDistributionId,
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = "";
		EmailDistributionModel distributionModel = null;
		session.beginTransaction();
		try {
			distributionModel = (EmailDistributionModel) session.get(
					EmailDistributionModel.class, emailDistributionId);
			session.delete(distributionModel);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmailDistribution> findByEmailDistributionId(
			UserDetailsDto appDetails, Integer id, String type) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<EmailDistribution> contacts = null;
		try {
			String query;
			if ("vendorUser".equalsIgnoreCase(type)) {
				query = "select new com.fg.vms.customer.dto.EmailDistribution(vc.id,vendor.vendorName,vc.firstName,vc.emailId,"
						+ "vendor.primeNonPrimeVendor,edldm.emailDistributionListDetailId,edldm.isTo,edldm.isCC,edldm.isBCC,status.statusname)"
						+ " from EmailDistributionListDetailModel edldm  join edldm.vendorId vc join vc.vendorId vendor join edldm.emailDistributionMasterId,StatusMaster status "
						+ "where status.id= vendor.vendorStatus and edldm.emailDistributionMasterId.id=:id and vc.isActive=1 and vc.isPrimaryContact = 1  ORDER BY vc.lastName ASC";

			} else {
				query = "select new com.fg.vms.customer.dto.EmailDistribution(user.id,user.userName,user.userEmailId,edldm.emailDistributionListDetailId,edldm.isTo,edldm.isCC,edldm.isBCC)"
						+ " from EmailDistributionListDetailModel edldm  join edldm.userId user join edldm.emailDistributionMasterId "
						+ "where edldm.emailDistributionMasterId.id=:id and user.isActive=1 ORDER BY user.userName ASC";
			}
			contacts = session.createQuery(query).setParameter("id", id).list();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return contacts;
	}

	/**
	 * Method to check weather the given array of integer value is equal to the
	 * user id , if so set 1 and if not set 0 for IsTo,IsCC,isBCC in Email
	 * distribution
	 * 
	 * @param arr
	 * @param id
	 * @return 0 or 1
	 */
	public Byte check(Integer arr[], Integer id) {
		if (arr != null && arr.length != 0) {
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].toString().equals(id.toString())) {
					return 1;
				}
			}
		}
		return (byte) 0;
	}

}
