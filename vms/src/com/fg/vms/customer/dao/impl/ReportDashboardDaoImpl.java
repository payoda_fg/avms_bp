/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.ReportDashboardDao;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.FrequencyPeriod;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class ReportDashboardDaoImpl implements ReportDashboardDao {

	final static String OLD_FORMAT = "MMM dd,yyyy";
	final static String NEW_FORMAT = "yyyy-MM-dd";

	@SuppressWarnings("deprecation")
	private String getCurrentQuarter(Tier2ReportingPeriod reportingPeriod) {
		String currentQuarter = null;
		int reportStartMonth = 0;
		int reportEndMonth = 3 - 1;
		Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
		
		if (reportingPeriod != null) 
		{
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
			switch (FrequencyPeriod.valueOf(reportingPeriod.getDataUploadFreq())) 
			{
				case M:
					c.set(reportYear, c.get(Calendar.MONTH) - 1, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));

					StringBuilder period = new StringBuilder();
					period.append(sdf.format(c.getTime()));
					
					c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
					period.append(" - " + sdf.format(c.getTime()));
					currentQuarter = period.toString();
					break;
					
				case Q:
					List<String> reportPeriod = CommonUtils.calculateReportingPeriods(reportingPeriod, reportYear);
					int index = CommonUtils.findCurrentQuarter(reportingPeriod);
					//For Getting Previous Quarter of Reporting Period 
					if(0 == index) 
					{
						//For Previous Year Last Quarter
						reportPeriod = CommonUtils.calculateReportingPeriods(reportingPeriod, reportYear-1);					
						currentQuarter = reportPeriod.get(3);
					} 
					else 
					{						
						currentQuarter = reportPeriod.get(index-1);
					}								
					break;
					
				case A:
					reportStartMonth = reportingPeriod.getStartDate().getMonth();
					reportEndMonth = reportingPeriod.getEndDate().getMonth();
	
					c.set(reportYear-1, reportStartMonth, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
					
					StringBuilder period1 = new StringBuilder();	
					period1.append(sdf.format(c.getTime()));
					c.set(reportYear-1, reportEndMonth, 1); // ------>
					c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
					period1.append(" - " + sdf.format(c.getTime()));
					currentQuarter = period1.toString();
					break;	
			}
		}		
		return currentQuarter;
	}
	
	private String getLast12Months() 
	{
		Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");

		StringBuilder period = new StringBuilder();
		Calendar c = Calendar.getInstance();
		c.set(reportYear, c.get(Calendar.MONTH) - 12, 1);
		
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));		
		period.append(sdf.format(c.getTime()));
		
		c.set(reportYear, c.get(Calendar.MONTH) - 1, 1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		period.append(" - " + sdf.format(c.getTime()));
		
		return period.toString();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getTotalSpendReport(UserDetailsDto appDetails,
			String year, String period) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String startDate = "";
			String endDate = "";
			String currentQuarter = null;
			Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
			Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
			Tier2ReportingPeriod reportingPeriod = periodDao.currentReportingPeriod(appDetails);
			
			currentQuarter = getCurrentQuarter(reportingPeriod);
			if (null != currentQuarter && !currentQuarter.isEmpty()) {
				String peString[] = currentQuarter.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}
			

			sqlQuery.append("SELECT customer_vendormaster.VENDORNAME,('"
					+ reportYear
					+ "')ryear,customer_tier2reportmaster.ReportingPeriod,"
					+ " (select IFNULL(sum(customer_tier2reportdirectexpenses.DirExpenseAmt),0) from customer_tier2reportdirectexpenses where"
					+ " customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)directspend,"
					+ " (select IFNULL(sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt),0) from customer_tier2reportindirectexpenses where"
					+ " customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)indirectspend,"
					+ " ('0')total, (DATE_FORMAT(customer_tier2reportmaster.createdon, '%m-%d-%Y')) createdon"
					+ " FROM customer_vendormaster,customer_tier2reportmaster WHERE (customer_tier2reportmaster.vendorId = customer_vendormaster.ID) "
					+ " And customer_tier2reportmaster.IsSubmitted = 1 ");

			if (!startDate.isEmpty() && !endDate.isEmpty()) {
				sqlQuery.append("and (customer_tier2reportmaster.ReportingStartDate>='"
						+ startDate
						+ "') and"
						+ " (customer_tier2reportmaster.ReportingEndDate<='"
						+ endDate + "')");
			}

			if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) {
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			System.out.println("5.TOTALSPEND (ReportDashboardDaoImpl @ 180):"+sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setVendorName(objects[0].toString());
					reportDto.setSpendYear(Integer.parseInt(objects[1]
							.toString()));
					reportDto.setReportPeriod(objects[2].toString());
					reportDto.setDirectExp(Double.parseDouble(objects[3]
							.toString()));
					if (objects[4].toString() != null
							&& !objects[4].toString().isEmpty()) {
						reportDto.setIndirectExp(Double.parseDouble(objects[4]
								.toString()));
					} else {
						reportDto.setIndirectExp((double) 0);
					}

					reportDto.setTotalSales(Double.parseDouble(objects[5]
							.toString()));
					reportDto.setCreatedOn(objects[6].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * convert period into dates.
	 * 
	 * @param date
	 * @return
	 */
	private String getReportingPeriod(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			if (date != null && !date.isEmpty()) {
				d = sdf.parse(date.trim());
				sdf.applyPattern(NEW_FORMAT);
				return sdf.format(d);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getDirectSpendReport(UserDetailsDto appDetails,
			String year, String period) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String startDate = "";
			String endDate = "";

			Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
			Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
			Tier2ReportingPeriod reportingPeriod = periodDao
					.currentReportingPeriod(appDetails);
			String currentQuarter = null;
			currentQuarter = getCurrentQuarter(reportingPeriod);
			if (null != currentQuarter && !currentQuarter.isEmpty()) {
				String peString[] = currentQuarter.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}
			

			sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,('"
					+ reportYear
					+ "')  ryear , "
					+ " customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME, "
					+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,"
					+ " (select sum(ds.DirExpenseAmt)"
					+ " from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id and"
					+ " t2m.ReportingStartDate >= '"
					+ startDate
					+ "'  AND"
					+ " t2m.ReportingEndDate <= '" + endDate + "'");

			sqlQuery.append(" And customer_tier2reportmaster.IsSubmitted = 1 ) totalexpenses,"
					+ " ((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)"
					+ " from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid "
					+ " and t2m.vendorid=pvm.id and t2m.ReportingStartDate >= '"
					+ startDate
					+ "' AND t2m.ReportingEndDate <= '"
					+ endDate
					+ "' And customer_tier2reportmaster.IsSubmitted = 1 ");

			sqlQuery.append(")*100) PercentDiversityspend,"
					+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%d-%m-%Y') SubmittedDate"
					+ " FROM customer_vendormaster pvm,customer_tier2reportmaster,customer_tier2reportdirectexpenses,"
					+ " customer_vendormaster t2v,customer_certificatemaster"
					+ " WHERE ( customer_tier2reportmaster.vendorId = pvm.ID ) and"
					+ " ( customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ) and"
					+ " ( t2v.ID = customer_tier2reportdirectexpenses.Tier2_Vendorid ) and"
					+ " ( customer_certificatemaster.ID = customer_tier2reportdirectexpenses.CertificateId ) and"
					+ " ( customer_certificatemaster.DIVERSE_QUALITY = 1 ) and "
					+ " ( ");

			sqlQuery.append(""
					+ " ( customer_tier2reportmaster.ReportingStartDate >= '"
					+ startDate
					+ "' ) AND"
					+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
					+ endDate
					+ "' ) And customer_tier2reportmaster.IsSubmitted = 1 ) ");
			
			if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) {
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and pvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append(" group by PrimeVendor ,t2vendor ,customer_tier2reportmaster.ReportingPeriod,"
					+ " customer_tier2reportmaster.CreatedOn ,customer_certificatemaster.CERTIFICATENAME");
			
			System.out.println("6.DIRECTSSPEND (ReportDashboardDaoImpl @ 341):"+sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());			

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();

				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setDirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(7)));
				reportDto.setDiversityPercent(rs.getString(8));
				reportDto.setCreatedOn(rs.getString(9));
				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getIndirectSpendReport(
			UserDetailsDto appDetails, String year, String period) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String startDate = "";
			String endDate = "";

			Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
			Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
			Tier2ReportingPeriod reportingPeriod = periodDao
					.currentReportingPeriod(appDetails);
			String currentQuarter = null;
			currentQuarter = getCurrentQuarter(reportingPeriod);
			if (null != currentQuarter && !currentQuarter.isEmpty()) {
				String peString[] = currentQuarter.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME t2vendor,('"
					+ reportYear
					+ "') ryear , "
					+ " customer_tier2reportmaster.ReportingPeriod, customer_certificatemaster.CERTIFICATENAME,"
					+ " sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)Indirectspendamt,"
					+ " (select sum(ds.IndirectExpenseAmt) from customer_tier2reportindirectexpenses ds, customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id and t2m.ReportingStartDate >= '"
					+ startDate + "'  AND" + " t2m.ReportingEndDate <= '"
					+ endDate + "' ");

			sqlQuery.append(" And customer_tier2reportmaster.IsSubmitted = 1 ) totalexpenses,"
					+ " ((sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt))/(select sum(ds.IndirectExpenseAmt)"
					+ " from customer_tier2reportindirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id and t2m.ReportingStartDate >= '"
					+ startDate
					+ "'  AND"
					+ " t2m.ReportingEndDate <= '"
					+ endDate + "'");

			sqlQuery.append("And customer_tier2reportmaster.IsSubmitted = 1 )*100) PercentDiversityspend,"
					+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%d-%m-%Y') SubmittedDate FROM customer_vendormaster  pvm "
					+ " INNER JOIN customer_tier2reportmaster ON customer_tier2reportmaster.vendorId = pvm.ID "
					+ " INNER JOIN customer_tier2reportindirectexpenses ON customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid "
					+ " INNER JOIN customer_certificatemaster ON customer_certificatemaster.ID = customer_tier2reportindirectexpenses.CertificateId "
					+ " LEFT OUTER JOIN customer_vendormaster t2v ON t2v.ID = customer_tier2reportindirectexpenses.Tier2_Vendorid"
					+ " WHERE (customer_certificatemaster.DIVERSE_QUALITY = 1) ");

			sqlQuery.append(" AND (( customer_tier2reportmaster.ReportingStartDate >= '"
					+ startDate
					+ "' ) AND ( customer_tier2reportmaster.ReportingEndDate <= '"
					+ endDate
					+ "' ) And customer_tier2reportmaster.IsSubmitted = 1 AND (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B'))");
			
			if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and pvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
					
			sqlQuery.append(" GROUP BY pvm.VENDORNAME, t2vendor, customer_certificatemaster.CERTIFICATENAME,"
					+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn "
					+ " ORDER BY pvm.VENDORNAME, t2vendor, customer_certificatemaster.CERTIFICATENAME,"
					+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn ");
			
			System.out.println("7.INDIRECTSPEND (ReportDashboardDaoImpl @ 383):"+sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());			

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(7)));
				reportDto.setDiversityPercent(rs.getString(8));
				reportDto.setCreatedOn(rs.getString(9));
				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getDiversityReport(UserDetailsDto appDetails,
			String currentYear, String period) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String startDate = "";
			String endDate = "";
			Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
			Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
			Tier2ReportingPeriod reportingPeriod = periodDao
					.currentReportingPeriod(appDetails);
			String currentQuarter = null;
			currentQuarter = getCurrentQuarter(reportingPeriod);

			if (null != currentQuarter && !currentQuarter.isEmpty()) {
				String peString[] = currentQuarter.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}
			
			sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,('"
					+ reportYear
					+ "') ryear,"
					+ " customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME,"
					+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,(0)indirectspendamt ,"
					+ " (select sum(ds.DirExpenseAmt)from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id and t2m.ReportingStartDate >= '"
					+ startDate
					+ "'  AND"
					+ " t2m.ReportingEndDate <= '"
					+ endDate + "' ");

			sqlQuery.append(" And customer_tier2reportmaster.IsSubmitted = 1 ) totalexpenses,"
					+ " ((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)from customer_tier2reportdirectexpenses ds, "
					+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id and t2m.ReportingStartDate >= '"
					+ startDate
					+ "' AND"
					+ " t2m.ReportingEndDate <= '"
					+ endDate + "'");

			sqlQuery.append(" And customer_tier2reportmaster.IsSubmitted = 1 )*100) PercentDiversity,"
					+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%d-%m-%Y') SubmittedDate FROM customer_vendormaster  pvm,"
					+ " customer_tier2reportmaster, customer_tier2reportdirectexpenses,customer_vendormaster  t2v,"
					+ " customer_certificatemaster WHERE ( customer_tier2reportmaster.vendorId = pvm.ID ) and"
					+ " ( customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ) and"
					+ " ( t2v.ID = customer_tier2reportdirectexpenses.Tier2_Vendorid ) and"
					+ " ( customer_certificatemaster.ID = customer_tier2reportdirectexpenses.CertificateId ) and"
					+ " ( ");

			sqlQuery.append(" ( customer_tier2reportmaster.ReportingStartDate >= '"
					+ startDate
					+ "' ) AND"
					+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
					+ endDate
					+ "' ) And customer_tier2reportmaster.IsSubmitted = 1 )");
			
			if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and pvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append(" group by pvm.VENDORNAME , customer_certificatemaster.CERTIFICATENAME, t2v.VENDORNAME , customer_tier2reportmaster.ReportingPeriod,"
					+ " customer_tier2reportmaster.CreatedOn");
			
			System.out.println("8.DIVERSITYSTATUS (ReportDashboardDaoImpl @ 585):"+sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());			

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setDirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(7)));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(8)));
				reportDto.setDiversityPercent(rs.getString(9));
				reportDto.setCreatedOn(rs.getString(10));
				reportDtos.add(reportDto);
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getEthnicityReport(UserDetailsDto appDetails,
			String year, String period) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String startDate = "";
			String endDate = "";
			// Integer reportYear = reportForm.getSpendYear();

			//Integer reportYear = Calendar.getInstance().get(Calendar.YEAR);
			Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
			Tier2ReportingPeriod reportingPeriod = periodDao
					.currentReportingPeriod(appDetails);
			String currentQuarter = null;
			currentQuarter = getCurrentQuarter(reportingPeriod);

			if (null != currentQuarter && !currentQuarter.isEmpty()) {
				String peString[] = currentQuarter.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("Select customer_vendormaster.VENDORNAME,"
					+ " customer_tier2reportmaster.ReportingPeriod,"
					+ "  sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)IndirectExpenseAmt"
					+ " From customer_tier2reportindirectexpenses Inner Join customer_tier2reportmaster "
					+ " On customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid "
					+ " Inner Join customer_vendormaster On customer_tier2reportmaster.vendorId = "
					+ " customer_vendormaster.ID And customer_tier2reportmaster.IsSubmitted = 1 "					
					+ " where ( customer_tier2reportmaster.ReportingStartDate>='"
					+ startDate
					+ "'and"
					+ " customer_tier2reportmaster.ReportingEndDate <='"
					+ endDate + "') ");

			if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append(" group by customer_vendormaster.VENDORNAME, "
					+ " customer_tier2reportmaster.ReportingPeriod ");
			
			sqlQuery.append(" order by customer_vendormaster.VENDORNAME ");

			System.out.println("9.ETHINCITYANALYSIS (ReportDashboardDaoImpl @ 688):"+sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());			

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				//reportDto.setVendorUserName(rs.getString(2));
				reportDto.setReportPeriod(rs.getString(2));
				//reportDto.setCertificateName(rs.getString(4));
				//reportDto.setEthnicity(rs.getString(5));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(3)));
				//reportDto.setDirectExp(Double.parseDouble(rs.getString(3)));
				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getNewRegistrationsByMonthDashboardReport(UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		
		String previousMonth = null;
		String startDate = null;
		String endDate = null;
		
		try 
		{			
			previousMonth = getLast12Months();

			if (null != previousMonth && !previousMonth.isEmpty()) {
				String peString[] = previousMonth.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}
			
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT DISTINCT DATE_FORMAT(cvm.CREATEDON, '%M %Y') AS 'MONTH', COUNT(*) AS 'COUNT', "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendormaster "
					+ "WHERE (ISPARTIALLYSUBMITTED = 'NO' OR ISPARTIALLYSUBMITTED IS NULL) AND ISACTIVE = 1 "
					+ "AND VENDORSTATUS NOT IN ('S', 'I') "
					+ "AND CREATEDON BETWEEN '" + startDate + " 00:00:00' AND '" + endDate + " 23:59:59' ");

			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null)
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendormaster cvm "
					+ "WHERE (cvm.ISPARTIALLYSUBMITTED = 'NO' OR cvm.ISPARTIALLYSUBMITTED IS NULL) "
					+ "AND cvm.ISACTIVE = 1 AND cvm.VENDORSTATUS NOT IN ('S', 'I') "
					+ "AND cvm.CREATEDON BETWEEN '" + startDate + " 00:00:00' AND '" + endDate + " 23:59:59' ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null)
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
					{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append("GROUP BY MONTH ORDER BY cvm.CREATEDON ASC");
			System.out.println("11.RegisteredVendorsDashboardReport (ReportDashboardDaoImpl @ 795):" + sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					
					reportDto.setMonth(objects[0].toString());
					reportDto.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2].toString()));	
					
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}
	
	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByIndustryDashboardReport(UserDetailsDto appDetails)
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		
		try 
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cbt.TypeName, COUNT(*) AS 'VENDORCOUNT', "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendormaster cvm1 "
					+ "INNER JOIN customer_businesstype cbt1 ON cbt1.Id = cvm1.BUSINESSTYPE ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("WHERE cvm1.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendormaster cvm "
					+ "INNER JOIN customer_businesstype cbt ON cbt.Id = cvm.BUSINESSTYPE ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("WHERE cvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
				}
			}
			
			sqlQuery.append("GROUP BY cvm.BUSINESSTYPE ORDER BY VENDORCOUNT DESC LIMIT 10");
			System.out.println("12.VendorsByIndustryDashboardReport (ReportDashboardDaoImpl @ 882):" + sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) 
			{
				iterator = reports.iterator();
				while (iterator.hasNext()) 
				{
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					
					reportDto.setBusinessTypeName(objects[0].toString());
					reportDto.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2].toString()));	
					
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}
	
	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getBPVendorsByIndustryDashboardReport(UserDetailsDto appDetails)
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		
		try 
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cbt.TypeName, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendormaster cvm1 "
					+ "INNER JOIN customer_businesstype cbt1 ON cbt1.Id = cvm1.BUSINESSTYPE WHERE cvm1.VENDORSTATUS = 'B' ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
								divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
								divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
					}
			}
			
			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendormaster cvm "
					+ "INNER JOIN customer_businesstype cbt ON cbt.Id = cvm.BUSINESSTYPE WHERE cvm.VENDORSTATUS = 'B' ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
								divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
								divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("AND cvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+")");
					}
			}
			
			sqlQuery.append("GROUP BY cvm.BUSINESSTYPE ORDER BY VENDORCOUNT DESC LIMIT 10");
			System.out.println("13.BPVendorsByIndustryDashboardReport (ReportDashboardDaoImpl @ 971):" + sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) 
			{
				iterator = reports.iterator();
				while (iterator.hasNext()) 
				{
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					
					reportDto.setBusinessTypeName(objects[0].toString());
					reportDto.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2].toString()));	
					
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}
	
	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByNAICSDecriptionDashboardReport(UserDetailsDto appDetails)
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		
		try 
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cnc.NAICSDESCRIPTION, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendornaics cvn1 "
					+ "INNER JOIN customer_naicscode cnc1 ON cnc1.NAICSID = cvn1.NAICSID "
					+ "INNER JOIN customer_vendormaster cvm1 ON cvm1.ID = cvn1.VENDORID "
					+ "WHERE cvm1.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
						
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i = 0; i < appDetails.getCustomerDivisionIds().length; i++)
					{
						divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID IN (" + divisionIds.toString()+ ") ");
				}
			}
			
			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendornaics cvn "
					+ "INNER JOIN customer_naicscode cnc ON cnc.NAICSID = cvn.NAICSID "
					+ "INNER JOIN customer_vendormaster cvm ON cvm.ID = cvn.VENDORID "
					+ "WHERE cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i = 0; i < appDetails.getCustomerDivisionIds().length; i++)
					{
						divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN (" + divisionIds.toString()+ ") ");
				}
			}
			
			sqlQuery.append("GROUP BY cnc.NAICSDESCRIPTION ORDER BY VENDORCOUNT DESC LIMIT 10");
			System.out.println("14.VendorsByNAICSDecriptionDashboardReport (ReportDashboardDaoImpl @ 1070):" + sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) 
			{
				iterator = reports.iterator();
				while (iterator.hasNext()) 
				{
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					
					reportDto.setNaicsDesc(objects[0].toString());
					reportDto.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2].toString()));	
					
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}
	
	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByBPMarketSectorDashboardReport(UserDetailsDto appDetails)
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		
		try 
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cms.SECTORDESCRIPTION, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendorcommodity cvc1 "
					+ "INNER JOIN customer_commodity cc1 ON cc1.ID = cvc1.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc1 ON ccc1.ID = cc1.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms1 ON cms1.ID = ccc1.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm1 ON cvm1.ID = cvc1.VENDORID "
					+ "WHERE cvm1.VENDORSTATUS IN ('B') ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i = 0; i < appDetails.getCustomerDivisionIds().length; i++)
					{
						divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID IN (" + divisionIds.toString()+ ") ");
				}
			}
			
			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendorcommodity cvc "
					+ "INNER JOIN customer_commodity cc ON cc.ID = cvc.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc ON ccc.ID = cc.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms ON cms.ID = ccc.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm ON cvm.ID = cvc.VENDORID "
					+ "WHERE cvm.VENDORSTATUS IN ('B') ");
			
			if (appDetails.getSettings().getIsDivision() != 0 && appDetails.getSettings().getIsDivision() != null) 
			{
				if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
				{
					StringBuilder divisionIds = new StringBuilder();
					for(int i = 0; i < appDetails.getCustomerDivisionIds().length; i++)
					{
						divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN (" + divisionIds.toString()+ ") ");
				}
			}
			
			sqlQuery.append("GROUP BY cms.SECTORDESCRIPTION ORDER BY VENDORCOUNT DESC LIMIT 10");
			System.out.println("15.VendorsByBPMarketSectorDashboardReport (ReportDashboardDaoImpl @ 1168):" + sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) 
			{
				iterator = reports.iterator();
				while (iterator.hasNext()) 
				{
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					
					reportDto.setMarketSector(objects[0].toString());
					reportDto.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2].toString()));	
					
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}
}