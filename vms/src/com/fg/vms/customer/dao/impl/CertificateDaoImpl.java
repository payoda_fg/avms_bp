/*
 * CertificateDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CertificateDao;
import com.fg.vms.customer.dto.CertificateExpiration;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertificateDivision;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerCertificateBusinesAreaConfig;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerClassificationCertificateTypes;
import com.fg.vms.customer.model.CustomerClassificationCertifyingAgencies;
import com.fg.vms.customer.model.CustomerDiverseClassification;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.pojo.CertificateForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the certificate services implementation
 * 
 * @author vinoth
 * 
 */
public class CertificateDaoImpl implements CertificateDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createCertificate(CertificateForm certificateForm,
			int currentUserId, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "unique";
		try {
			session.beginTransaction();

			/* To check unique certificate. */
			Certificate cert = (Certificate) session
					.createCriteria(Certificate.class)
					.add(Restrictions.eq("certificateName",
							certificateForm.getCertificateName()))
					.uniqueResult();
			if (cert != null) {
				return result;
			}
			Certificate certificate = new Certificate();
			/* Save the certificate details after validation. */
			log.info(" ============save certificate is starting========== ");
			certificate
					.setCertificateName(certificateForm.getCertificateName());
			certificate.setCertificateShortName(certificateForm
					.getCertificateShortName());
			certificate.setDiverseQuality(Byte.valueOf(certificateForm
					.getDiverseQuality()));
			certificate
					.setIsActive(Byte.valueOf(certificateForm.getIsActive()));
			certificate.setIsEthinicity(Byte.valueOf(certificateForm
					.getIsEthinicity()));
			certificate.setCertificateUpload(Byte.valueOf(certificateForm
					.getIsEthinicity()));
			certificate.setCreatedBy(currentUserId);
			certificate.setCreatedOn(new Date());
			certificate.setModifiedBy(currentUserId);
			certificate.setModifiedOn(new Date());
			session.save(certificate);

			// Save related certifying agencies.
			if (certificateForm.getCertificateAgency() != null
					&& certificateForm.getCertificateAgency().length != 0) {

				for (String agency : certificateForm.getCertificateAgency()) {
					CustomerClassificationCertifyingAgencies agencies = new CustomerClassificationCertifyingAgencies();
					CertifyingAgency certifyingAgency = (CertifyingAgency) session
							.get(CertifyingAgency.class,
									Integer.parseInt(agency));
					agencies.setCertificateId(certificate);
					agencies.setAgencyId(certifyingAgency);
					agencies.setIsActive(Byte.valueOf(certificateForm
							.getIsActive()));
					agencies.setCreatedBy(currentUserId);
					agencies.setCreatedOn(new Date());
					session.save(agencies);
				}
			}

			// Save related certificate types.
			if (certificateForm.getCertificateType() != null
					&& certificateForm.getCertificateType().length != 0) {

				for (String certificateType : certificateForm
						.getCertificateType()) {
					CustomerClassificationCertificateTypes certificateTypes = new CustomerClassificationCertificateTypes();
					CustomerCertificateType type = (CustomerCertificateType) session
							.get(CustomerCertificateType.class,
									Integer.parseInt(certificateType));
					certificateTypes.setCertificateId(certificate);
					certificateTypes.setCertificateTypeId(type);
					certificateTypes.setIsActive(Byte.valueOf(certificateForm
							.getIsActive()));
					certificateTypes.setCreatedBy(currentUserId);
					certificateTypes.setCreatedOn(new Date());
					session.save(certificateTypes);
				}

			}

			// Save related Customer Divisions.
			if (certificateForm.getCustomerDivision() != null
					&& certificateForm.getCustomerDivision().length != 0) {

				for (String division : certificateForm.getCustomerDivision()) {
					CertificateDivision divisions = new CertificateDivision();
					CustomerDivision customerDivision = (CustomerDivision) session
							.get(CustomerDivision.class,
									Integer.parseInt(division));
					divisions.setCustomerDivisionId(customerDivision);
					divisions.setCertMasterId(certificate);
					divisions.setIsActive(Byte.valueOf(certificateForm
							.getIsActive()));
					divisions.setCreatedBy(currentUserId);
					divisions.setCreatedOn(new Date());
					session.save(divisions);
				}
			}

			log.info(" ============saved certificate successfully========== ");

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Certificate> listOfCertificates(UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<Certificate> certificates = null;
		try {
			session.beginTransaction();
			/* Get the list of active certificates */
			certificates = session.createQuery(
					"from Certificate ORDER BY certificateName ASC").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificates;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public String deleteCertificate(Integer id, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		Certificate certificate = null;
		try {
			session.beginTransaction();
			certificate = (Certificate) session.get(Certificate.class, id);

			/*
			 * Get the list of certificates based on certificate master Id for
			 * delete purpose
			 */
			List<VendorCertificate> listOfCertificates = session
					.createCriteria(VendorCertificate.class)
					.add(Restrictions.eq("certMasterId", certificate)).list();

			if (listOfCertificates != null && listOfCertificates.size() != 0) {
				return "reference";
			}
			
			List<CustomerDiverseClassification> listOfVendorDiverseClassifications = session
					.createCriteria(CustomerDiverseClassification.class)
					.add(Restrictions.eq("certMasterId", certificate)).list();
			
			if (listOfVendorDiverseClassifications != null && listOfVendorDiverseClassifications.size() != 0) {
				return "reference";
			}
			
			List<CustomerCertificateBusinesAreaConfig> listOfCertificateBusinesAreaConfig = session
					.createCriteria(CustomerCertificateBusinesAreaConfig.class)
					.add(Restrictions.eq("certificateId", certificate)).list();
			
			if (listOfCertificateBusinesAreaConfig != null && listOfCertificateBusinesAreaConfig.size() != 0) {
				return "reference";
			}

			if (certificate != null && certificate.getId() != null) {
				session.createQuery(
						" delete CustomerClassificationCertificateTypes where certificateId="
								+ certificate.getId()).executeUpdate();
				session.createQuery(
						" delete CustomerClassificationCertifyingAgencies where certificateId="
								+ certificate.getId()).executeUpdate();
				session.createQuery(
						" delete CertificateDivision where certMasterId="
								+ certificate.getId()).executeUpdate();
				session.delete(certificate);
				session.getTransaction().commit();
			}

			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Certificate retriveCertificate(Integer id, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		Certificate certificates = null;
		try {
			session.beginTransaction();
			/* Get the certificate details based on selected certificate */
			certificates = (Certificate) session
					.createCriteria(Certificate.class)
					.add(Restrictions.eq("id", id)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return certificates;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateCertificate(CertificateForm certificateForm,
			Integer id, Integer currentUserId, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";

		Certificate certificate = null;
		try {
			session.beginTransaction();
			
			/* To check unique certificate. */
			certificate = (Certificate) session.createCriteria(Certificate.class).add(Restrictions.eq("certificateName", certificateForm.getCertificateName())).add(Restrictions.ne("id", id)).uniqueResult();
			
			if(certificate != null) {
				return "unique";
			}
			
			certificate = (Certificate) session.get(Certificate.class, id);
			
			/*
			 * Don't allow to change diverse or quality after selecting the
			 * certificate in the vendor creation
			 */
			if (!certificate.getDiverseQuality().equals(
					Byte.valueOf(certificateForm.getDiverseQuality()))) {

				/* Get the list of certificates based on certificateId */
				List<VendorCertificate> listOfCertificates = session
						.createCriteria(VendorCertificate.class)
						.add(Restrictions.eq("certMasterId", certificate))
						.list();

				if (listOfCertificates != null
						&& listOfCertificates.size() != 0) {
					return "reference";
				}
			}

			/* Update the certificate details. */

			log.info(" ============update certificate is starting========== ");
			certificate
					.setCertificateName(certificateForm.getCertificateName());
			certificate.setCertificateShortName(certificateForm
					.getCertificateShortName());
			certificate.setDiverseQuality(Byte.valueOf(certificateForm
					.getDiverseQuality()));
			certificate
					.setIsActive(Byte.valueOf(certificateForm.getIsActive()));
			certificate.setIsEthinicity(Byte.valueOf(certificateForm
					.getIsEthinicity()));
			certificate.setCertificateUpload(Byte.valueOf(certificateForm
					.getCertificateUpload()));
			certificate.setModifiedBy(currentUserId);
			certificate.setModifiedOn(new Date());

			session.update(certificate);

			// Update related certifying agencies.
			if (certificateForm.getCertificateAgency() != null
					&& certificateForm.getCertificateAgency().length != 0) {

				session.createQuery(
						" delete CustomerClassificationCertifyingAgencies where certificateId="
								+ certificate.getId()).executeUpdate();

				for (String agency : certificateForm.getCertificateAgency()) {
					CustomerClassificationCertifyingAgencies agencies = new CustomerClassificationCertifyingAgencies();
					CertifyingAgency certifyingAgency = (CertifyingAgency) session
							.get(CertifyingAgency.class,
									Integer.parseInt(agency));
					agencies.setCertificateId(certificate);
					agencies.setAgencyId(certifyingAgency);
					agencies.setIsActive(Byte.valueOf(certificateForm
							.getIsActive()));
					agencies.setCreatedBy(currentUserId);
					agencies.setCreatedOn(new Date());
					session.save(agencies);
				}
			}

			// Update related certificate types.
			if (certificateForm.getCertificateType() != null
					&& certificateForm.getCertificateType().length != 0) {

				session.createQuery(
						" delete CustomerClassificationCertificateTypes where certificateId="
								+ certificate.getId()).executeUpdate();

				for (String certificateType : certificateForm
						.getCertificateType()) {
					CustomerClassificationCertificateTypes certificateTypes = new CustomerClassificationCertificateTypes();
					CustomerCertificateType type = (CustomerCertificateType) session
							.get(CustomerCertificateType.class,
									Integer.parseInt(certificateType));
					certificateTypes.setCertificateId(certificate);
					certificateTypes.setCertificateTypeId(type);
					certificateTypes.setIsActive(Byte.valueOf(certificateForm
							.getIsActive()));
					certificateTypes.setCreatedBy(currentUserId);
					certificateTypes.setCreatedOn(new Date());
					session.save(certificateTypes);
				}

			}

			// Update related Customer Divisions.
			if (certificateForm.getCustomerDivision() != null
					&& certificateForm.getCustomerDivision().length != 0) {

				session.createQuery(
						" delete CertificateDivision where certMasterId="
								+ certificate.getId()).executeUpdate();

				for (String division : certificateForm.getCustomerDivision()) {
					CertificateDivision divisions = new CertificateDivision();
					CustomerDivision customerDivision = (CustomerDivision) session
							.get(CustomerDivision.class,
									Integer.parseInt(division));
					divisions.setCustomerDivisionId(customerDivision);
					divisions.setCertMasterId(certificate);
					divisions.setIsActive(Byte.valueOf(certificateForm
							.getIsActive()));
					divisions.setCreatedBy(currentUserId);
					divisions.setCreatedOn(new Date());
					session.save(divisions);
				}
			} else {
				session.createQuery(
						" delete CertificateDivision where certMasterId="
								+ certificate.getId()).executeUpdate();
			}

			log.info(" ============updated certificate details========== ");

			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Certificate> listOfCertificatesByType(Byte diverseOrQuality,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<Certificate> certificates = null;

		try {
			session.beginTransaction();
			/* Get the list of certificates based on Diverse/Quality. */
			if (appDetails != null && appDetails.getSettings() != null) {
				if (appDetails.getSettings().getIsDivision() != null
						&& appDetails.getSettings().getIsDivision() != 0) {
					if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0) 
					{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
								divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
								divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						certificates = session
								.createQuery(
										"from Certificate where diverseQuality="
												+ diverseOrQuality
												+ " and isActive=1 and id in (select certMasterId from CertificateDivision where "
												+ " customerDivisionId in("+ divisionIds.toString() +") "
												+ ") "
												+ " Order by certificateName")
								.list();
					}
					else{
						certificates = session
								.createQuery(
										"from Certificate where diverseQuality="
												+ diverseOrQuality
												+ " and isActive=1 Order by certificateName")
								.list();
					}
				} else {
					certificates = session
							.createQuery(
									"from Certificate where diverseQuality="
											+ diverseOrQuality
											+ " and isActive=1 Order by certificateName")
							.list();
				}
			}

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificates;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Certificate> listOfCertificatesByVendor(
			UserDetailsDto appDetails, VendorMaster master) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<Certificate> certificates = null;
		try {
			session.beginTransaction();
			/* Get the list of certificates based on Diverse/Quality. */
			certificates = session.createQuery(
					"from CustomerDiverseClassification where vendorId="
							+ master.getId() + " and isActive = 1").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificates;
	}

	@Override
	public List<CertificateExpiration> certificateExpiration(
			UserDetailsDto appDetails, StringBuilder query) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<CertificateExpiration> certificates = null;
		try {
			session.beginTransaction();
			/* Get the list of certificates based on Diverse/Quality. */
			certificates = session.createQuery(query.toString()).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificates;
	}

	@Override
	public String deleteVendorCertificate(Integer id, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String result = "success";
		VendorCertificate vendorCertificate = null;
		try {
			session.getTransaction().begin();
			/* Fetch the list of active naics codes. */
			vendorCertificate = (VendorCertificate) session.get(
					VendorCertificate.class, id);
			if (vendorCertificate != null) {
				// session.delete(vendorNAICS);
				Query q = session
						.createQuery("delete VendorCertificate where id = "
								+ id);
				q.executeUpdate();
			}
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
					result = "failure";
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
					result = "failure";
				}
			}
		}
		return result;
	}

	@Override
	public String deleteVendorOtherCertificate(Integer id,
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String result = "success";
		VendorOtherCertificate vendorOtherCertificate = null;
		try {
			session.getTransaction().begin();
			/* Fetch the list of active naics codes. */
			vendorOtherCertificate = (VendorOtherCertificate) session.get(
					VendorOtherCertificate.class, id);
			if (vendorOtherCertificate != null) {
				// session.delete(vendorNAICS);
				Query q = session
						.createQuery("delete VendorOtherCertificate where id = "
								+ id);
				q.executeUpdate();
			}
			// codeDtos = packNaicsCode(naicsCodes);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
					result = "failure";
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
					result = "failure";
				}
			}
		}
		return result;
	}
}
