package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CountryStateDao;
import com.fg.vms.customer.dto.CountryStateDto;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.pojo.CountryStateForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

public class CountryStateDaoImpl implements CountryStateDao 
{
	/** Logger of the system. */
	private static final Logger log = Constants.logger;
	
	/**
	 * This method helps in convert string array to string.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToString(String[] stringarray) 
	{
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) 
		{
			for (String st : stringarray) 
			{
				sb.append(st.trim()).append(',');
			}
			if (stringarray.length != 0) 
			{
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public void saveCountry(Integer userId, CountryStateForm countryStateForm, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		try 
		{
			session.beginTransaction();

			Country country = new Country();
			/* Save the Country details. */

			log.info(" ============Save Country Name is Starting========== ");

			//For Update
			if (countryStateForm.getCountryId() != null && countryStateForm.getCountryId() != 0) 
			{
				//System.out.println("Update");
				Country country1 = (Country) session.get(Country.class, countryStateForm.getCountryId());
				country1.setCountryname(countryStateForm.getCountryName());
				country1.setIsactive((byte)1);
				country1.setModifiedby(userId);
				country1.setModifiedon(new Date());	
				country1.setProvincestate(countryStateForm.getProvinceState());
				session.merge(country1);
			} 
			else //For Save 
			{
				if (countryStateForm.getCountryName() != null && !countryStateForm.getCountryName().isEmpty()) 
				{					
					String countryName=countryStateForm.getCountryName();					
					List<Country> countries = null;
					countries = session.createQuery("from Country where countryname='"+countryName+"'").list();
					//Updating Existing Country's IsActive = 0 to IsActive = 1
					if(countries != null  && countries.size() != 0) 
					{
						Country country11 = (Country)countries.get(0);
						int id=country11.getId();						
						Country country1 = (Country) session.get(Country.class, id);
						country1.setCountryname(countryStateForm.getCountryName());
						country1.setIsactive((byte)1);
						country1.setModifiedby(userId);
						country1.setModifiedon(new Date());	
						country1.setProvincestate(countryStateForm.getProvinceState());
						session.merge(country1);
					}
					else //For Creating New Country
					{
						country.setCountryname(countryStateForm.getCountryName());
						country.setIsactive((byte)1);
						country.setCreatedby(userId);
						country.setCreatedon(new Date());	
						country.setProvincestate(countryStateForm.getProvinceState());
						session.save(country);
					}
				}
			}

			log.info(" ============Saved Country Name successfully========== ");
			session.getTransaction().commit();

		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Country> listCountries(UserDetailsDto appDetails) 
	{
		List<Country> countries = null;

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		StringBuilder sqlQuery = new StringBuilder();
		
		try 
		{
			session.beginTransaction();
			sqlQuery.append("from Country where ");
			
			if(appDetails.getWorkflowConfiguration() != null) {
				if(appDetails.getWorkflowConfiguration().getInternationalMode() != null && appDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
					sqlQuery.append("isDefault=1 and ");
				}
			}
			
			sqlQuery.append("isActive = 1 ORDER BY countryname ASC");
			
			countries = session.createQuery(sqlQuery.toString()).list();
		} 
		catch (HibernateException ex) 
		{
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return countries;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void deleteCountry(Integer countryId, Integer userId, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		Country country = null;
		try 
		{
			session.beginTransaction();
			country = (Country) session.get(Country.class, countryId);
			country.setModifiedby(userId);
			country.setModifiedon(new Date());
			country.setIsactive((byte) 0);
			
			session.merge(country);
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Country> deleteCountryCheck(Integer countryId, Integer userId, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<Country> country = null;
		
		try
		{
			session.beginTransaction();
			country = session.createQuery("select c.countryname from Country c, State st where st.countryid="+countryId).list();
			session.getTransaction().commit();
		}
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return country;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<State> deleteStateCheck(Integer stateId, Integer userId, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<State> state = null;
		
		try
		{
			session.beginTransaction();
			state = session.createSQLQuery("SELECT st.STATENAME FROM state st "
					+ "INNER JOIN customer_vendoraddressmaster cvam ON cvam.STATE = st.ID "
					+ "INNER JOIN customer_vendorreference cvr ON cvr.REFERENCESTATE = st.ID "
					+ "WHERE st.ID = '"+stateId+"' ").setMaxResults(1).list();
			session.getTransaction().commit();
		}
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return state;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Country> retrieveCountries(Integer countryId, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		List<Country> country = null;
		
		try 
		{
			session.beginTransaction();
			/* Fetch the list of active Countries. */

			country = session.createQuery("from Country where isActive=1 and id="+ countryId).list();
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return country;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public void saveState(Integer userId, CountryStateForm countryStateForm, UserDetailsDto appDetails) 
	{		
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		try 
		{
			session.beginTransaction();

			//Update
			if (countryStateForm.getStateId() != null && countryStateForm.getStateId() != 0) 
			{				
				State state = (State) session.get(State.class, countryStateForm.getStateId());

				String temp = arrayToString(countryStateForm.getCountriesGroup());
								
				if (countryStateForm.getCountriesGroup() != null && countryStateForm.getCountriesGroup().length != 0 && !temp.isEmpty()) 
				{
					for (int i = 0; i < temp.split(",").length; i++) 
					{
						Country country = (Country) session.get(Country.class, Integer.parseInt(temp.split(",")[i]));						
						state.setCountryid(country);
						state.setStatename(countryStateForm.getStateName());
						state.setIsactive((byte) 1);
						state.setModifiedby(userId);
						state.setModifiedon(new Date());						
						session.merge(state);
					}
				}
			} 
			else //Save
			{				
				if (countryStateForm.getStateName() != null && !countryStateForm.getStateName().isEmpty()) 
				{
					String temp = arrayToString(countryStateForm.getCountriesGroup());
					String stateName=countryStateForm.getStateName();
					List<State> states = null;
					states = session.createQuery("from State where statename='"+stateName+"'").list();
					//Updating Existing State's IsActive = 0 to IsActive = 1
					if(states != null  && states.size() != 0) 
					{
						State state11 = (State)states.get(0);
						int id=state11.getId();
						State state1 = (State) session.get(State.class, id);					
						
						if (countryStateForm.getCountriesGroup() != null && countryStateForm.getCountriesGroup().length != 0 && !temp.isEmpty()) 
						{
							for (int i = 0; i < temp.split(",").length; i++) 
							{
								Country country = (Country) session.get(Country.class, Integer.parseInt(temp.split(",")[i]));						
								state1.setCountryid(country);
								state1.setStatename(countryStateForm.getStateName());
								state1.setIsactive((byte) 1);
								state1.setModifiedby(userId);
								state1.setModifiedon(new Date());						
								session.merge(state1);
							}
						}						
					}
					else //For Creating New State
					{												
						if (countryStateForm.getCountriesGroup() != null && countryStateForm.getCountriesGroup().length != 0 && !temp.isEmpty()) 
						{
							for (int i = 0; i < temp.split(",").length; i++) 
							{
								State state = new State();
								Country country = (Country) session.get(Country.class, Integer.parseInt(temp.split(",")[i]));							
								state.setCountryid(country);
								state.setStatename(countryStateForm.getStateName());
								state.setIsactive((byte) 1);
								state.setCreatedby(userId);
								state.setCreatedon(new Date());							
								session.save(state);
							}
						}
					}
				}
			}
			log.info(" ============Saved State Successfully========== ");
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<State> listStates(UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		List<State> states = null;
		try 
		{
			session.beginTransaction();
			/* Fetch the List of Active States. */

			states = session.createQuery("from State where isActive=1").list();
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return states;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void deleteState(Integer stateId, Integer userId, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		State state = null;
		try 
		{
			session.beginTransaction();
			state = (State) session.get(State.class, stateId);
			state.setModifiedby(userId);
			state.setModifiedon(new Date());
			state.setIsactive((byte) 0);
			session.merge(state);

			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<State> retrieveState(Integer stateId, UserDetailsDto appDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));

		List<State> states = null;
		try 
		{
			session.beginTransaction();
			/* Fetch the List of Active Area under States. */

			states = session.createQuery("from State where isActive=1 and id="+ stateId).list();
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return states;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<CountryStateDto> getStateByCountryList(UserDetailsDto appDetails) 
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<CountryStateDto> countryDto = new ArrayList<CountryStateDto>();
		Iterator<?> iterator = null;
		try 
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select countryname, IFNULL(STATENAME,'N/A') From country ct left join state st on "
					+ "ct.Id = st.COUNTRYID And st.ISACTIVE = 1 where ct.ISACTIVE = 1 ");

			if(appDetails.getWorkflowConfiguration() != null) {
				if(appDetails.getWorkflowConfiguration().getInternationalMode() != null && appDetails.getWorkflowConfiguration().getInternationalMode() == 0) {
					sqlQuery.append("AND ct.isdefault = 1 ");
				}
			}
			
			sqlQuery.append("Order by STATENAME");
			
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> reports = query.list();
			if (reports != null) 
			{
				iterator = reports.iterator();
				while (iterator.hasNext()) 
				{
					CountryStateDto dto = new CountryStateDto();
					Object[] objects = (Object[]) iterator.next();
					dto.setCountry(objects[0].toString());
					dto.setState(objects[1].toString());
					countryDto.add(dto);
				}
			}
		} 
		catch (HibernateException ex) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return countryDto;
	}

	/* Retrieve States from Country Id
	 * 
	 * @see com.fg.vms.customer.dao.CountryStateDao
	 * retriveStateByCountry(java.lang.Integer, com.fg.vms.admin.dto.UserDetailsDto)
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<State> retrieveStateByCountry(Integer countryId,
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory().
				openSession(DataBaseConnection.getConnection(appDetails));
		List<State> states = null;
		
		try 
		{
			session.beginTransaction();
			/* Fetch the List of Active Area under States. */

			states = session.createQuery
					("from State where isActive=1 and countryid="+countryId+" ORDER BY statename ASC").list();
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return states;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public Country listDefaultCountry(UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		
		Country defaultCountry = null;
		
		try {
			defaultCountry = (Country) session.createQuery("from Country where isDefault = 1").uniqueResult();
		} catch (HibernateException exception) {			
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) 
			{
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}		
		return defaultCountry;
	}
}