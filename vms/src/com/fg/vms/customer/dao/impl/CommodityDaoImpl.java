/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.model.CustomerCommodity;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.pojo.CommodityForm;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
/**
 * @author Kaleswararao
 *
 * @param <T>
 */
public class CommodityDaoImpl<T> implements CURDDao<T> {

	@Override
	public T save(T entity, UserDetailsDto usDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public T update(T entity, UserDetailsDto usDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			CustomerCommodity commodity = (CustomerCommodity) session.get(
					CustomerCommodity.class,
					((CustomerCommodity) entity).getId());
			if (commodity != null) {
				commodity.setCommodityCode(((CustomerCommodity) entity)
						.getCommodityCode());
				commodity.setCommodityDescription(((CustomerCommodity) entity)
						.getCommodityDescription());
				commodity.setCommCategoryId(((CustomerCommodity) entity)
						.getCommCategoryId());
				commodity.setIsActive((byte) 1);
				commodity.setModifiedBy(((CustomerCommodity) entity)
						.getModifiedBy());
				commodity.setModifiedOn(new Date());
				session.merge(commodity);
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public T delete(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			CustomerCommodity commodity = (CustomerCommodity) session.get(
					CustomerCommodity.class,
					((CustomerCommodity) entity).getId());
			if (commodity != null) {
				commodity.setIsActive((byte) 0);
				session.merge(commodity);
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list(UserDetailsDto usDetails, String query) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<CustomerCommodity> commodities = null;
		try {
			session.getTransaction().begin();
			commodities = session
					.createQuery(
							"From CustomerCommodity where isActive=1 order by commodityDescription")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (List<T>) commodities;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<T> listCommoditiesBasedOnSubSector(UserDetailsDto usDetails, Integer marketSubSectorId) {
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(usDetails));

		List<CustomerCommodity> commodities = null;
		try {			
			session.getTransaction().begin();
			commodities = session.createQuery(
							"From CustomerCommodity where commCategoryId="+ marketSubSectorId +" AND isActive=1 order by commodityDescription").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (List<T>) commodities;
	}
	
	//List the all commodities
	@SuppressWarnings({ "unchecked", "deprecation" })	
	public List<CustomerCommodity> listCommodites(UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(usDetails));

	    List<CustomerCommodity> commodities = null;
		try 
		{
			session.getTransaction().begin();
	        commodities = session.createQuery(
	        				"From CustomerCommodity order by commodityDescription").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return  commodities;
	}	

	@Override
	public T update1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T delete1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public T find(UserDetailsDto usDetails, String query) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		T value = null;
		try {
			session.getTransaction().begin();
			value = (T) session.createQuery(query).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return  value;
	}

    @Override
    public List<?> findAllByQuery(UserDetailsDto usDetails, String query) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public List<Ethnicity> listEthnicities(UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * Getting the Commodity list with Market Sectors....
	 * 
	 */
	public List<CommodityDto> listCommoditySectors(UserDetailsDto userDetails)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		List<CommodityDto> commodityList=new ArrayList<CommodityDto>();
		CommodityDto commodityDto;
		try 
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("select cm.ID,cm.SECTORDESCRIPTION,"
					+ "ccd.ID,IFNULL(ccd.CATEGORYDESCRIPTION,'N/A'),"
					+ "cd.ID,IFNULL(cd.COMMODITYDESCRIPTION,'N/A') from customer_marketsector cm "
					+ "left outer join customer_commoditycategory ccd on cm.ID=ccd.MARKETSECTORID and ccd.ISACTIVE=1 "
					+ "left outer join customer_commodity cd on cd.COMMODITYCATEGORYID=ccd.ID and cd.ISACTIVE=1 "
					+ "where cm.ISACTIVE=1");
			System.out.println("@ CommodityDaoImpl 293 : "+sqlQuery);
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List commodities=query.list();
			if(commodities !=null){
			Iterator itr=commodities.iterator();
			while(itr.hasNext())
			{
				commodityDto=new CommodityDto();
				Object[] objects=(Object[])itr.next();
				commodityDto.setSectorId(Integer.parseInt(objects[0].toString()));
				commodityDto.setSectorDescription(objects[1].toString());
				commodityDto.setCommCategoryId(Integer.parseInt(objects[2].toString()));
				commodityDto.setCategoryDesc(objects[3].toString());
				commodityDto.setCommodityId(Integer.parseInt(objects[4].toString()));
				commodityDto.setCommodityDescription(objects[5].toString());
				commodityList.add(commodityDto);
			}
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return  commodityList;
	}
	
	
	public List marketSectorAndCommodities(UserDetailsDto userDetails,String query)
	{
		
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		List list=new ArrayList();
		try 
		{
			session.getTransaction().begin();
			list=session.createQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;
	}
	public String saveMarketSector(UserDetailsDto userDetails, CommodityForm form,Integer userId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		Integer sectorId=form.getSectorId();
		List<MarketSector> sectorList=new ArrayList<MarketSector>();
		String result="Success";
		try 
		{
			session.getTransaction().begin();
			int flage=0;
			sectorList=session.createCriteria(MarketSector.class).list();
		// Find Sector Code Duplication both Update and Saving Times
			if(sectorList!=null)
			{
				MarketSector sector=null;
				Iterator itr=sectorList.iterator();
				if(sectorId!=null){
					while(itr.hasNext())
					{
						sector=(MarketSector)itr.next();
						if(sector.getId().intValue()!=sectorId.intValue()){
							if(sector.getSectorCode().equalsIgnoreCase(form.getSectorCode().trim())){
								flage=1; // 1 means Updating Time, Find Duplicate....
								break;
							}
						}
					}
				}
				else
				{
					while(itr.hasNext())
					{
						sector=(MarketSector)itr.next();
							if(sector.getSectorCode().equalsIgnoreCase(form.getSectorCode().trim())){
								flage=1; // 1 means Saving Time, Find Duplicate......
								break;
							}
					}
				}
			}
			if(flage==0){
				if(sectorId!=null && sectorId != 0)
				{
					MarketSector marketSector=(MarketSector)session.createCriteria(MarketSector.class).add(Restrictions.eq("id", sectorId)).uniqueResult();
					marketSector.setSectorCode(form.getSectorCode());
					marketSector.setSectorDescription(form.getSectorDescription());
					marketSector.setModifiedBy(userId);
					marketSector.setModifiedOn(new Date());
					session.update(marketSector);
					result="Updated";
				}
				else
				{
					MarketSector marketSector=new MarketSector();
					marketSector.setSectorCode(form.getSectorCode());
					marketSector.setSectorDescription(form.getSectorDescription());
					marketSector.setCreatedBy(userId);
					marketSector.setCreatedOn(new Date());
					marketSector.setIsActive((byte)1);
					session.save(marketSector);
				}
			}
			else
				result="Duplicated";
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			result="Failure";
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				}
			}
		}
		return result;
	}
	
	public MarketSector retriveMarketSector(UserDetailsDto userDetails,CommodityForm form, Integer sectorId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		MarketSector marketSector=null;
		try 
		{
			session.getTransaction().begin();
			marketSector=(MarketSector)session.createQuery("From MarketSector where isActive=1 and id="+sectorId).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);

				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return marketSector;
	}
	
	public String deleteMarketSector(UserDetailsDto userDetails,Integer sectorId,Integer userId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		MarketSector marketSector=null;
		List<CustomerCommodityCategory> subSector=null;
		String result="Deleted";
		try 
		{
			session.getTransaction().begin();
			marketSector=(MarketSector)session.createQuery("From MarketSector where isActive=1 and id="+sectorId).uniqueResult();
			subSector = (List<CustomerCommodityCategory>) session.createQuery(
					"From CustomerCommodityCategory where isActive=1 and marketSector="
							+ marketSector.getId()).list();
			if(subSector.size()==0)
			{
				marketSector.setIsActive((byte)0);
				marketSector.setModifiedBy(userId);
				marketSector.setModifiedOn(new Date());
				session.update(marketSector);
			}
			else
				result="Reference";
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result="Failure";
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					result="Failure";
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	public String saveSubSector(UserDetailsDto userDetails, CommodityForm form,Integer userId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		Integer sectorId=form.getSectorId();
		Integer subsectorId=form.getCommCategoryId();
		List<CustomerCommodityCategory> subsectorList=new ArrayList<CustomerCommodityCategory>();
		MarketSector marketSector=null;
		String result="Success";
		try 
		{
			session.getTransaction().begin();
			int flage=0;
			if(sectorId!=null)
			{
				marketSector=(MarketSector)session.createQuery("From MarketSector where id="+sectorId+" and isActive=1").uniqueResult();
			}
			subsectorList=session.createCriteria(CustomerCommodityCategory.class).list();
		// Find Sector Code Duplication both Update and Saving Times
			if(subsectorList!=null)
			{
				CustomerCommodityCategory subsector=null;
				Iterator itr=subsectorList.iterator();
				if(subsectorId!=null){
					while(itr.hasNext())
					{
						subsector=(CustomerCommodityCategory)itr.next();
						if(subsector.getId().intValue()!=subsectorId.intValue()){
							if(subsector.getCategoryCode().equalsIgnoreCase(form.getCategoryCode().trim())){
								flage=1; // 1 means Updating Time, Find Duplicate....
								break;
							}
						}
					}
				}
				else
				{
					while(itr.hasNext())
					{
						subsector=(CustomerCommodityCategory)itr.next();
						if(subsector.getCategoryCode().equalsIgnoreCase(form.getCategoryCode().trim())){
								flage=1; // 1 means Saving Time, Find Duplicate......
								break;
							}
					}
				}
			}
			if(flage==0){
				if(subsectorId!=null && subsectorId != 0)
				{
					CustomerCommodityCategory subsector=(CustomerCommodityCategory)session.createCriteria(CustomerCommodityCategory.class).add(Restrictions.eq("id", subsectorId)).uniqueResult();
					subsector.setCategoryCode(form.getCategoryCode());
					subsector.setCategoryDescription(form.getCategoryDesc());
					subsector.setMarketSector(marketSector);
					subsector.setModifiedBy(userId);
					subsector.setModifiedOn(new Date());
					session.update(subsector);
					result="Updated";
				}
				else
				{
					CustomerCommodityCategory subsector=new CustomerCommodityCategory();
					subsector.setCategoryCode(form.getCategoryCode());
					subsector.setCategoryDescription(form.getCategoryDesc());
					subsector.setMarketSector(marketSector);
					subsector.setCreatedBy(userId);
					subsector.setCreatedOn(new Date());
					subsector.setIsActive((byte)1);
					session.save(subsector);
					result="Success";
				}
			}
			else
				result="Duplicated";
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			result="Failure";
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				}
			}
		}
		return result;
	}
	
	public CustomerCommodityCategory retriveSubSector(UserDetailsDto userDetails,CommodityForm form, Integer subsectorId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		CustomerCommodityCategory subSector=null;
		try 
		{
			session.getTransaction().begin();
			subSector=(CustomerCommodityCategory)session.createQuery("From CustomerCommodityCategory where isActive=1 and id="+subsectorId).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);

				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return subSector;
	}
	
	public String deleteSubSector(UserDetailsDto userDetails,Integer subsectorId,Integer userId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		CustomerCommodityCategory subSector=null;
		List<CustomerCommodity> commodity=null;
		String result="Deleted";
		try 
		{
			session.getTransaction().begin();
			subSector=(CustomerCommodityCategory)session.createQuery("From CustomerCommodityCategory where isActive=1 and id="+subsectorId).uniqueResult();
			commodity=(List<CustomerCommodity>)session.createQuery("From CustomerCommodity where isActive=1 and commCategoryId="+subSector.getId()).list();
			if(commodity.size()==0)
			{
				subSector.setIsActive((byte)0);
				subSector.setModifiedBy(userId);
				subSector.setModifiedOn(new Date());
				session.update(subSector);
			}
			else
				result="Reference";
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result="Failure";
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					result="Failure";
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	public String saveCommodity(UserDetailsDto userDetails, CommodityForm form,Integer userId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		Integer subsectorId=form.getCommCategoryId();
		Integer commodityId=form.getCommodityId();
		List<CustomerCommodity> commodityList=new ArrayList<CustomerCommodity>();
		CustomerCommodityCategory subSector=null;
		String result="Success";
		try 
		{
			session.getTransaction().begin();
			int flage=0;
			if(subsectorId!=null)
			{
				subSector=(CustomerCommodityCategory)session.createQuery("From CustomerCommodityCategory where id="+subsectorId+" and isActive=1").uniqueResult();
			}
			commodityList=session.createCriteria(CustomerCommodity.class).list();
		// Find Sector Code Duplication both Update and Saving Times
			if(commodityList!=null)
			{
				CustomerCommodity commodity=null;
				Iterator itr=commodityList.iterator();
				if(commodityId!=null){
					while(itr.hasNext())
					{
						commodity=(CustomerCommodity)itr.next();
						if(commodity.getId().intValue()!=commodityId.intValue()){
							if(commodity.getCommodityCode().equalsIgnoreCase(form.getCommodityCode().trim())){
								flage=1; // 1 means Updating Time, Find Duplicate....
								break;
							}
						}
					}
				}
				else
				{
					while(itr.hasNext())
					{
						commodity=(CustomerCommodity)itr.next();
						if(commodity.getCommodityCode().equalsIgnoreCase(form.getCommodityCode().trim())){
							flage=1; // 1 means Updating Time, Find Duplicate....
							break;
						}
					}
				}
			}
			if(flage==0){
				if(commodityId!=null && commodityId != 0)
				{
					CustomerCommodity commodity=(CustomerCommodity)session.createCriteria(CustomerCommodity.class).add(Restrictions.eq("id", commodityId)).uniqueResult();
					commodity.setCommodityCode(form.getCommodityCode());
					commodity.setCommodityDescription(form.getCommodityDescription());
					commodity.setCommCategoryId(subSector);
					commodity.setModifiedBy(userId);
					commodity.setModifiedOn(new Date());
					session.update(commodity);
					result="Updated";
				}
				else
				{
					CustomerCommodity commodity=new CustomerCommodity();
					commodity.setCommodityCode(form.getCommodityCode());
					commodity.setCommodityDescription(form.getCommodityDescription());
					commodity.setCommCategoryId(subSector);
					commodity.setCreatedBy(userId);
					commodity.setCreatedOn(new Date());
					commodity.setIsActive((byte)1);
					session.save(commodity);
					result="Success";
				}
			}
			else
				result="Duplicated";
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			result="Failure";
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				}
			}
		}
		return result;
	}
	
	public CustomerCommodity retriveCommodity(UserDetailsDto userDetails,CommodityForm form, Integer commodityId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		CustomerCommodity commodity=null;
		try 
		{
			session.getTransaction().begin();
			commodity=(CustomerCommodity)session.createQuery("From CustomerCommodity where isActive=1 and id="+commodityId).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);

				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return commodity;
	}
	
	public String deleteCommodity(UserDetailsDto userDetails,Integer commodityId,Integer userId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		CustomerCommodity commodity=null;
		String result="Deleted";
		try 
		{
			session.getTransaction().begin();
			/*subSector=(CustomerCommodityCategory)session.createQuery("From CustomerCommodityCategory where isActive=1 and id="+subsectorId).uniqueResult();*/
			commodity=(CustomerCommodity)session.createQuery("From CustomerCommodity where isActive=1 and id="+commodityId).uniqueResult();
			if(commodity!=null){
				commodity.setIsActive((byte)0);
				commodity.setModifiedBy(userId);
				commodity.setModifiedOn(new Date());
				session.update(commodity);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result="Failure";
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					result="Failure";
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	public List<CommodityDto> listOfCommoditiesBasedOnSearchCommodityText(UserDetailsDto usDetails, String fullTextSearch) {
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(usDetails));
		String result=null;
		CommodityDto commodityDto = null;
		List<CommodityDto> commodityList = new ArrayList<CommodityDto>();
		try
		{
			String sqlQuery = "SELECT IFNULL(customer_marketsector.SECTORDESCRIPTION, '') AS SECTORDESCRIPTION,"
					       +" IFNULL(customer_commoditycategory.CATEGORYDESCRIPTION, '') AS CATEGORYDESCRIPTION,"
					       +" customer_commodity.ID, IFNULL(customer_commodity.COMMODITYCODE,'') AS CommodityCode, IFNULL(customer_commodity.COMMODITYDESCRIPTION, '') AS COMMODITYDESCRIPTION"
					       +" FROM customer_marketsector"
					       +" LEFT OUTER JOIN customer_commoditycategory ON customer_commoditycategory.MARKETSECTORID=customer_marketsector.ID"
					       +" AND customer_commoditycategory.ISACTIVE=1"
					       +" LEFT OUTER JOIN customer_commodity ON customer_commodity.COMMODITYCATEGORYID = customer_commoditycategory.ID"
					       +" AND customer_commodity.ISACTIVE=1"
					       +" WHERE (customer_marketsector.SECTORDESCRIPTION LIKE '%"+fullTextSearch+"%'"
					       +" OR customer_commoditycategory.CATEGORYDESCRIPTION LIKE '%"+fullTextSearch+"%'"
					       +" OR customer_commodity.COMMODITYDESCRIPTION LIKE '%"+fullTextSearch+"%') ORDER BY customer_marketsector.SECTORDESCRIPTION";
			System.out.println("CommodityDaoImpl @1001 : "+sqlQuery);
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List commodities=query.list();
			if(commodities !=null)
			{
				Iterator itr=commodities.iterator();
				while(itr.hasNext())
				{
					commodityDto=new CommodityDto();
					Object[] objects=(Object[])itr.next();
					commodityDto.setSectorDescription(objects[0].toString());
					commodityDto.setCategoryDesc(objects[1].toString());
					if(objects[2] != null)
						commodityDto.setCommodityId(Integer.parseInt(objects[2].toString()));
					
					commodityDto.setCommodityCode(objects[3].toString());
					commodityDto.setCommodityDescription(objects[4].toString());
					
					commodityList.add(commodityDto);
				}
			}
		}
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result="Failure";
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
					result="Failure";
				} catch (SQLException e) 
				{
					result="Failure";
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return commodityList;
	}
	
	public List getUpdtedComodityResult(UserDetailsDto userDetails,String query)
	{
		
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(userDetails));
		List list=new ArrayList();
		try 
		{
			session.getTransaction().begin();
			list=session.createSQLQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;
	}
}
