package com.fg.vms.customer.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.CountryStateDao;
import com.fg.vms.customer.dao.VendorDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dto.ClassificationDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.dto.EmailHistoryDto;
import com.fg.vms.customer.dto.MeetingNotesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.dto.RFIRPFNotesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.dto.SupplierDiversityDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertificateExpirationNotification;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerCommodity;
import com.fg.vms.customer.model.CustomerDiverseClassification;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerSearchFilter;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.CustomerVendorOwner;
import com.fg.vms.customer.model.CustomerVendorSales;
import com.fg.vms.customer.model.CustomerVendorSelfRegInfo;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendorStatusLog;
import com.fg.vms.customer.model.CustomerVendorTier2Vendor;
import com.fg.vms.customer.model.CustomerVendorbusinessbiographysafety;
import com.fg.vms.customer.model.CustomerVendornetworldCertificate;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.MeetingNotes;
import com.fg.vms.customer.model.RFIRPFNotes;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.SupplierDiversity;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.customer.pojo.CustomerServiceAreaInfo;
import com.fg.vms.customer.pojo.SupplierDiversityForm;
import com.fg.vms.customer.pojo.VendorAddressInfo;
import com.fg.vms.customer.pojo.VendorBussinessArea;
import com.fg.vms.customer.pojo.VendorBussinessBioDetails;
import com.fg.vms.customer.pojo.VendorContactInfo;
import com.fg.vms.customer.pojo.VendorDetailsBean;
import com.fg.vms.customer.pojo.VendorDiverseCertificate;
import com.fg.vms.customer.pojo.VendorMasterBean;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.customer.pojo.VendorOwnerDetails;
import com.fg.vms.customer.pojo.VendorStatusPojo;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PasswordUtil;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SendEmail;
import com.fg.vms.util.VendorStatus;
import com.fg.vms.util.VendorUtil;

/**
 * Implementation class for Vendor
 * 
 * @author srinivasarao
 * 
 */
public class VendorDaoImpl implements VendorDao {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Implementing the code for save the vendorMaster, NaicsCodes,
	 * DiverseCertifications and Other Quality Certifications information.
	 */
	public String saveVendor(VendorMasterForm vendorForm, Integer userId,
			List<DiverseCertificationTypesDto> certifications,
			List<NaicsCodesDto> naicsCodes, UserDetailsDto appDetails,
			String url, VendorContact currentVendor) {

		String result = "success";
		VendorMaster vendorMaster = null;
		VendorMaster parentVendor = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();

		logger.info("Start the new vendor saving....");
		CommonDao commonDao = new CommonDaoImpl();
		result = commonDao.validateVendorCode(vendorForm.getVendorCode(),
				appDetails);

		if (result.equals("success")) {
			String result1 = commonDao.validateLoginId(
					vendorForm.getContanctEmail(), appDetails);

			if (result1.equalsIgnoreCase("submit")) {
				String emailExist = commonDao.checkDuplicateEmail(
						vendorForm.getContanctEmail(), appDetails);
				if (emailExist.equalsIgnoreCase("submit")) {
					try {
						vendorMaster = new VendorMaster();
						vendorMaster = VendorUtil.packVendorMasterEntity(
								vendorForm, userId, vendorMaster);

						vendorMaster.setCreatedBy(userId);
						vendorMaster.setCreatedOn(new Date());
						// options for creating vendor by customer
						if (currentVendor == null) {
							vendorMaster.setParentVendorId(0);
							// vendorMaster.setPrimeNonPrimeVendor(CommonUtils
							// .getByteValue(true));
							// vendorMaster.setVendorNotes(vendorForm
							// .getVendorNotes());
							vendorMaster.setIsApproved((byte) 0);
							vendorMaster
									.setVendorStatus(VendorStatus.NEWREGISTRATION
											.getIndex());
							vendorMaster.setIsinvited((short) 1);
							vendorMaster.setIsActive((byte) 1);
							vendorMaster.setStatusModifiedOn(new Date());
						} else {

							// options for creating vendor by other vendor
							parentVendor = (VendorMaster) session
									.createCriteria(VendorMaster.class)
									.add(Restrictions.eq("id", currentVendor
											.getVendorId().getId()))
									.uniqueResult();
							vendorMaster
									.setParentVendorId(parentVendor.getId());
							vendorMaster.setPrimeNonPrimeVendor(CommonUtils
									.getByteValue(false));
							vendorMaster.setIsApproved((byte) 1);
							vendorMaster.setVendorStatus(VendorStatus.ACTIVE
									.getIndex());
							vendorMaster.setIsinvited((short) 1);
							vendorMaster.setIsActive((byte) 1);
							vendorMaster.setStatusModifiedOn(new Date());

						}

						session.save(vendorMaster);
						vendorMaster.setVendorCode(vendorMaster.getId()
								.toString());
						session.merge(vendorMaster);

						if (null != vendorForm.getBusinessArea()
								&& vendorForm.getBusinessArea().length != 0) {

							for (String business : vendorForm.getBusinessArea()) {

								VendorBusinessArea vendorBusinessArea = new VendorBusinessArea();
								CustomerBusinessArea businessArea = (CustomerBusinessArea) session
										.get(CustomerBusinessArea.class,
												Integer.parseInt(business));
								vendorBusinessArea.setVendorId(vendorMaster);
								vendorBusinessArea
										.setBusinessAreaId(businessArea);
								vendorBusinessArea.setIsActive((byte) 1);
								vendorBusinessArea.setCreatedBy(userId);
								vendorBusinessArea.setCreatedOn(new Date());
								session.save(vendorBusinessArea);
							}
						}
						if (null != vendorForm.getServiceArea()
								&& vendorForm.getServiceArea().length != 0) {

							for (Integer area : vendorForm.getServiceArea()) {
								CustomerServiceArea serviceArea = (CustomerServiceArea) session
										.get(CustomerServiceArea.class, area);
								CustomerVendorServiceArea serviceArea2 = new CustomerVendorServiceArea();
								serviceArea2.setVendorId(vendorMaster);
								serviceArea2.setServiceAreaId(serviceArea);
								serviceArea2.setIsActive((byte) 1);
								serviceArea2.setCreatedBy(userId);
								serviceArea2.setCreatedOn(new Date());
								session.save(serviceArea2);
							}
						}
						if (currentVendor != null) {
							CustomerVendorTier2Vendor customerVendorTier2Vendor = new CustomerVendorTier2Vendor();
							customerVendorTier2Vendor.setVendorid(vendorMaster);
							customerVendorTier2Vendor
									.setParentvendorid(parentVendor.getId());
							customerVendorTier2Vendor.setCreatedby(userId);
							customerVendorTier2Vendor.setCreatedon(new Date());
							customerVendorTier2Vendor.setIsactive((short) 1);

							session.save(customerVendorTier2Vendor);
						}

						if (vendorForm.getAnnualTurnover() != null
								&& !vendorForm.getAnnualTurnover().isEmpty()) {
							CustomerVendorSales vendorSales1 = new CustomerVendorSales();
							vendorSales1.setYearsales(CommonUtils
									.deformatMoney(vendorForm
											.getAnnualTurnover()));
							vendorSales1.setYearnumber(vendorForm
									.getAnnualYear1());
							vendorSales1.setVendorid(vendorMaster);
							vendorSales1.setCreatedby(userId);
							vendorSales1.setCreatedon(new Date());
							vendorSales1.setIsactive((short) 1);
							session.save(vendorSales1);
						}
						if (vendorForm.getSales2() != null
								&& !vendorForm.getSales2().isEmpty()) {
							CustomerVendorSales vendorSales2 = new CustomerVendorSales();
							vendorSales2.setYearsales(CommonUtils
									.deformatMoney(vendorForm.getSales2()));
							vendorSales2.setYearnumber(vendorForm
									.getAnnualYear2());
							vendorSales2.setVendorid(vendorMaster);
							vendorSales2.setCreatedby(userId);
							vendorSales2.setCreatedon(new Date());
							vendorSales2.setIsactive((short) 1);
							session.save(vendorSales2);
						}

						if (vendorForm.getSales3() != null
								&& !vendorForm.getSales3().isEmpty()) {
							CustomerVendorSales vendorSales3 = new CustomerVendorSales();
							vendorSales3.setYearsales(CommonUtils
									.deformatMoney(vendorForm.getSales3()));
							vendorSales3.setYearnumber(vendorForm
									.getAnnualYear3());
							vendorSales3.setVendorid(vendorMaster);
							vendorSales3.setCreatedby(userId);
							vendorSales3.setCreatedon(new Date());
							vendorSales3.setIsactive((short) 1);
							session.save(vendorSales3);
						}

						if (vendorForm.getCompanyOwnership().equalsIgnoreCase(
								"2")) {
							// persist Customer Vendor Owner information

							if (null != vendorForm.getOwnerName1()
									&& !vendorForm.getOwnerName1().isEmpty()) {
								CustomerVendorOwner owner1 = new CustomerVendorOwner();
								owner1 = VendorUtil.packVendorOwner1(
										vendorForm, userId, owner1,
										vendorMaster);
								owner1.setCreatedby(userId);
								owner1.setCreatedon(new Date());
								session.save(owner1);
							}

							if (null != vendorForm.getOwnerName2()
									&& !vendorForm.getOwnerName2().isEmpty()) {
								CustomerVendorOwner owner2 = new CustomerVendorOwner();
								owner2 = VendorUtil.packVendorOwner2(
										vendorForm, userId, owner2,
										vendorMaster);
								owner2.setCreatedby(userId);
								owner2.setCreatedon(new Date());
								session.save(owner2);
							}

							if (null != vendorForm.getOwnerName3()
									&& !vendorForm.getOwnerName3().isEmpty()) {
								CustomerVendorOwner owner3 = new CustomerVendorOwner();
								owner3 = VendorUtil.packVendorOwner3(
										vendorForm, userId, owner3,
										vendorMaster);
								owner3.setCreatedby(userId);
								owner3.setCreatedon(new Date());
								session.save(owner3);
							}
						}
						logger.info("Vendor master saved.");

						logger.info("Vendor address master start saving.");
						if (vendorForm.getAddress1() != null
								&& !vendorForm.getAddress1().isEmpty()) {

							CustomerVendorAddressMaster addressMaster = new CustomerVendorAddressMaster();
							addressMaster = VendorUtil.packVendorAddress1(
									vendorForm, userId, addressMaster,
									vendorMaster);
							session.save(addressMaster);
						}
						if (vendorForm.getAddress2() != null
								&& !vendorForm.getAddress2().isEmpty()) {

							CustomerVendorAddressMaster addressMaster1 = new CustomerVendorAddressMaster();
							addressMaster1 = VendorUtil.packVendorAddress2(
									vendorForm, userId, addressMaster1,
									vendorMaster);
							session.save(addressMaster1);
						} else {
							CustomerVendorAddressMaster addressMaster1;
							addressMaster1 = (CustomerVendorAddressMaster) session
									.createQuery(
											"from CustomerVendorAddressMaster where addressType = 'M' and vendorId="
													+ vendorMaster.getId())
									.uniqueResult();
							if (addressMaster1 != null) {
								session.delete(addressMaster1);
							}
						}
						logger.info("Vendor address master saved.");

						if (currentVendor == null) {
							CustomerVendorStatusLog log = new CustomerVendorStatusLog();
							Users user = (Users) session.get(Users.class,
									userId);
							log.setVendorId(vendorMaster);
							log.setVendorStatus(VendorStatus.NEWREGISTRATION
									.getIndex());
							log.setCreatedBy(user);
							log.setCreatedOn(new Date());
							session.save(log);
						}

						if (vendorForm.getCommodityCode() != null
								&& vendorForm.getCommodityCode().length != 0) {
							for (String commodityCode : vendorForm
									.getCommodityCode()) {
								CustomerVendorCommodity commodity = new CustomerVendorCommodity();
								CustomerCommodity comm = (CustomerCommodity) session
										.createQuery(
												" from CustomerCommodity where commodityCode='"
														+ commodityCode + "'")
										.uniqueResult();
								commodity.setVendorId(vendorMaster);
								commodity.setCommodityId(comm);
								commodity.setIsActive((byte) 1);
								commodity.setCreatedBy(userId);
								commodity.setCreatedOn(new Date());
								session.save(commodity);
							}
						}

						VendorUtil.saveBusinessBiography(vendorForm, userId,
								vendorMaster, session);
						logger.info("Biography and safety saved success..");

						logger.info("References start saving..");

						VendorUtil.saveReferences(vendorForm, userId,
								vendorMaster, session);

						logger.info("References saved successfully..");

						// Save VendorContact Information
						VendorContact vendorContact = new VendorContact();
						VendorRoles roles = (VendorRoles) session
								.createCriteria(VendorRoles.class)
								.add(Restrictions.eq("isActive", (byte) 1))
								.list().get(0);

						vendorContact.setVendorUserRoleId(roles);
						vendorContact.setIsSysGenPassword((byte) 0);
						vendorContact = VendorUtil
								.packVendorContact(vendorForm, userId,
										vendorContact, vendorMaster);

						SecretQuestion secretQns = new SecretQuestion();
						secretQns = (SecretQuestion) session
								.get(SecretQuestion.class,
										vendorForm.getUserSecQn());

						vendorContact.setSecretQuestionId(secretQns);
						vendorContact
								.setSecQueAns(vendorForm.getUserSecQnAns());

						session.save(vendorContact);
						// password History
						PasswordHistory passwordHistory = new PasswordHistory();
						passwordHistory
								.setKeyvalue(vendorContact.getKeyValue());
						passwordHistory.setChangedon(new Date());
						passwordHistory.setChangedby(userId);
						passwordHistory
								.setPassword(vendorContact.getPassword());
						passwordHistory.setVendorUserId(vendorContact.getId());
						session.save(passwordHistory);
						logger.info("Vendor contact information saved");

						// save Diverse Certifications for Vendor

						if (CommonUtils.getCheckboxValue(vendorForm
								.getDeverseSupplier()) == (byte) 1) {

							// Iterate each classifications from UI
							if (vendorForm.getCertificates() != null
									&& vendorForm.getCertificates().length > 0) {

								for (Integer cert : vendorForm
										.getCertificates()) {

									CustomerDiverseClassification classification1 = new CustomerDiverseClassification();
									Certificate certMaster = (Certificate) session
											.get(Certificate.class, cert);
									classification1.setCertMasterId(certMaster);
									classification1.setVendorId(vendorMaster);
									classification1.setDiverseQuality((byte) 0);
									classification1.setIsActive((byte) 1);
									classification1.setCreatedBy(userId);
									classification1.setCreatedOn(new Date());
									session.save(classification1);

								}
							}
							logger.info("Classification saved success..");

							if (certifications != null
									&& !certifications.isEmpty()) {
								for (DiverseCertificationTypesDto certificate : certifications) {

									VendorCertificate vendorCertificate = new VendorCertificate();
									Certificate certMasterId = (Certificate) session
											.get(Certificate.class, certificate
													.getDivCertType());
									CertifyingAgency certAgencyId = (CertifyingAgency) session
											.get(CertifyingAgency.class,
													certificate
															.getDivCertAgent());

									if (certMasterId != null
											&& certAgencyId != null
											&& vendorMaster != null) {
										vendorCertificate = VendorUtil
												.packVendorCertificate(
														vendorForm, userId,
														vendorCertificate,
														vendorMaster,
														certMasterId,
														certAgencyId,
														certificate);
										vendorCertificate.setCreatedBy(userId);
										vendorCertificate
												.setCreatedOn(new Date());
										session.save(vendorCertificate);
									}
								}
							}
						}

						// Save VendorNAICS with naicsCodes

						if (naicsCodes != null && naicsCodes.size() > 0) {
							for (NaicsCodesDto naicsCode : naicsCodes) {

								VendorNAICS vendorNaics = new VendorNAICS();

								NaicsCode naicsMaster = (NaicsCode) session
										.createCriteria(NaicsCode.class)
										.add(Restrictions.eq("naicsCode",
												naicsCode.getNaicsCode()))
										.uniqueResult();
								if (naicsMaster != null) {
									vendorNaics.setVendorId(vendorMaster);
									vendorNaics.setNaicsId(naicsMaster);
									vendorNaics.setPrimary(naicsCode
											.getPrimaryKey());
									vendorNaics.setCapabilities(naicsCode
											.getCapabilitie());
									vendorNaics.setCreatedBy(userId);
									vendorNaics.setCreatedOn(new Date());
									vendorNaics.setModifiedBy(userId);
									vendorNaics.setModifiedOn(new Date());
									session.save(vendorNaics);
									logger.info("Naics code saved");
								}

							}
						}

						// Save Other Quality Certifications
						List<VendorOtherCertificate> vendorOtherCerts = CommonUtils
								.packageOtherCertificates(vendorForm, userId);

						for (VendorOtherCertificate certificate : vendorOtherCerts) {
							certificate.setVendorId(vendorMaster);
							session.save(certificate);
						}

						logger.info("Other certificate saved");

						String companyCode = appDetails.getCustomer()
								.getCustCode();
						// Save vendor documents
						List<VendorDocuments> documents = CommonUtils
								.packageVendorDocuments(vendorForm,
										vendorMaster, userId, companyCode);
						if (documents != null && documents.size() != 0) {
							for (VendorDocuments docu : documents) {
								docu.setVendorId(vendorMaster);
								session.save(docu);
							}
						}
						session.getTransaction().commit();
					} catch (HibernateException ex) {
						session.getTransaction().rollback();
						PrintExceptionInLogFile.printException(ex);
						return "failure";
					} catch (Exception e) {
						PrintExceptionInLogFile.printException(e);
						return "failure";
					} finally {
						if (session.isConnected()) {
							try {
								session.connection().close();
								session.close();
							} catch (HibernateException e) {
								PrintExceptionInLogFile.printException(e);
							} catch (SQLException e) {
								PrintExceptionInLogFile.printException(e);
							}
						}
					}
				} else {
					return emailExist;
				}
			} else {
				return result1;
			}
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String mergeVendor(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId,
			List<DiverseCertificationTypesDto> certifications,
			List<NaicsCodesDto> naicsCodes, UserDetailsDto userDetails,
			VendorContact currentVendor, String url) {

		logger.info("Inside the merge  vendor method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {

			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			return "failure";
		}

		if (vendorMaster != null) {
			logger.info("=================== Vendor update start here =============");
			try {

				// update VendorMaster info
				vendorMaster.setVendorName(vendorForm.getVendorName());
				// vendorMaster.setVendorCode(vendorForm.getVendorCode());
				vendorMaster.setDunsNumber(vendorForm.getDunsNumber());
				vendorMaster.setTaxId(vendorForm.getTaxId());
				vendorMaster.setAddress1(vendorForm.getAddress1());
				vendorMaster.setCompanyType(vendorForm.getCompanytype());

				vendorMaster.setCity(vendorForm.getCity());
				vendorMaster.setCountry(vendorForm.getCountry());
				vendorMaster.setState(vendorForm.getState());
				vendorMaster.setProvince(vendorForm.getProvince());
				vendorMaster.setPhone(vendorForm.getPhone());
				vendorMaster.setRegion(vendorForm.getRegion());
				vendorMaster.setZipCode(vendorForm.getZipcode());
				vendorMaster.setMobile(vendorForm.getMobile());
				vendorMaster.setFax(vendorForm.getFax());
				vendorMaster.setWebsite(vendorForm.getWebsite());
				vendorMaster.setEmailId(vendorForm.getEmailId());
				if (vendorForm.getNumberOfEmployees() != null
						&& !vendorForm.getNumberOfEmployees().isEmpty()
						&& !vendorForm.getNumberOfEmployees().equalsIgnoreCase(
								"null")) {
					vendorMaster.setNumberOfEmployees(Integer
							.parseInt(vendorForm.getNumberOfEmployees()));
				}

				vendorMaster.setYearOfEstablishment(Integer.parseInt(vendorForm
						.getYearOfEstablishment()));
				vendorMaster.setDeverseSupplier(CommonUtils
						.getCheckboxValue(vendorForm.getDeverseSupplier()));
				vendorMaster.setModifiedBy(userId);
				vendorMaster.setModifiedOn(new Date());

				if (currentVendor == null) {
					if (null != vendorForm.getPrimeNonPrimeVendor()
							&& !vendorForm.getPrimeNonPrimeVendor().isEmpty()) {
						if (null == vendorMaster.getPrimeNonPrimeVendor()
								&& vendorForm.getPrimeNonPrimeVendor().equals(
										"0")) {

							WorkflowConfigurationDao configuration = new WorkflowConfigurationDaoImpl();
							configuration.followWorkFlow(session, userDetails,
									vendorMaster, url, userId);
						}
						vendorMaster.setPrimeNonPrimeVendor(Byte
								.parseByte(vendorForm.getPrimeNonPrimeVendor()
										.toString()));
					}
					// vendorMaster.setVendorNotes(vendorForm.getVendorNotes());
					if (null != vendorForm.getVendorStatus()
							&& !vendorForm.getVendorStatus().isEmpty()) {
						if (null != vendorForm.getVendorStatus()
								&& (vendorForm.getVendorStatus()
										.equalsIgnoreCase(
												VendorStatus.ACTIVE.getIndex()) || vendorForm
										.getVendorStatus().equalsIgnoreCase(
												VendorStatus.BPSUPPLIER
														.getIndex()))) {
							vendorMaster.setApprovedOn(new Date());
							// vendorMaster.setApprovedBy(userId);
							vendorMaster.setIsApproved((byte) 1);

						} else if (vendorForm.getVendorStatus()
								.equalsIgnoreCase(
										VendorStatus.INACTIVE.getIndex())) {
							vendorMaster.setIsActive((byte) 0);
						}
						vendorMaster.setVendorStatus(vendorForm
								.getVendorStatus());
					}
				}
				vendorMaster.setVendorDescription(vendorForm
						.getVendorDescription());
				vendorMaster.setCompanyInformation(vendorForm
						.getCompanyInformation());
				vendorMaster.setStateIncorporation(vendorForm
						.getStateIncorporation());
				vendorMaster
						.setStateSalesTaxId(vendorForm.getStateSalesTaxId());
				if (null != vendorForm.getCompanyOwnership()
						&& !vendorForm.getCompanyOwnership().isEmpty()) {
					vendorMaster.setCompanyownership(Short.valueOf(vendorForm
							.getCompanyOwnership()));
				}
				vendorMaster.setBusinesstype(vendorForm.getBusinessType());

				session.update(vendorMaster);

				logger.info("Vendor master update success.");

				logger.info("Vendor business area update start");
				if (null != vendorForm.getBusinessArea()
						&& vendorForm.getBusinessArea().length != 0) {
					session.createQuery(
							" delete VendorBusinessArea where vendorId="
									+ vendorMaster.getId()).executeUpdate();
					for (String business : vendorForm.getBusinessArea()) {

						VendorBusinessArea vendorBusinessArea = new VendorBusinessArea();
						CustomerBusinessArea businessArea = (CustomerBusinessArea) session
								.get(CustomerBusinessArea.class,
										Integer.parseInt(business));
						vendorBusinessArea.setVendorId(vendorMaster);
						vendorBusinessArea.setBusinessAreaId(businessArea);
						vendorBusinessArea.setIsActive((byte) 1);
						vendorBusinessArea.setCreatedBy(userId);
						vendorBusinessArea.setCreatedOn(new Date());
						session.save(vendorBusinessArea);
					}
				} else {
					session.createQuery(
							" delete VendorBusinessArea where vendorId="
									+ vendorMaster.getId()).executeUpdate();
				}
				if (null != vendorForm.getServiceArea()
						&& vendorForm.getServiceArea().length != 0) {
					session.createQuery(
							" delete CustomerVendorServiceArea where vendorId="
									+ vendorMaster.getId()).executeUpdate();
					for (Integer area : vendorForm.getServiceArea()) {
						CustomerServiceArea serviceArea = (CustomerServiceArea) session
								.get(CustomerServiceArea.class, area);
						CustomerVendorServiceArea serviceArea2 = new CustomerVendorServiceArea();
						serviceArea2.setVendorId(vendorMaster);
						serviceArea2.setServiceAreaId(serviceArea);
						serviceArea2.setIsActive((byte) 1);
						serviceArea2.setCreatedBy(userId);
						serviceArea2.setCreatedOn(new Date());
						session.save(serviceArea2);
					}
				} else {
					session.createQuery(
							" delete CustomerVendorServiceArea where vendorId="
									+ vendorMaster.getId()).executeUpdate();
				}
				logger.info("Vendor business area update success.");

				logger.info("Vendor sales update starting.");
				if (vendorForm.getAnnualTurnover() != null
						&& !vendorForm.getAnnualTurnover().isEmpty()) {
					CustomerVendorSales vendorSales1;
					if (vendorForm.getAnnualTurnoverId1() != null
							&& vendorForm.getAnnualTurnoverId1() != 0) {
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId1());
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getAnnualTurnover()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear1());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setModifiedby(userId);
						vendorSales1.setModifiedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.merge(vendorSales1);
					} else {
						vendorSales1 = new CustomerVendorSales();
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getAnnualTurnover()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear1());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setCreatedby(userId);
						vendorSales1.setCreatedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.save(vendorSales1);
					}

				} else {
					if (vendorForm.getAnnualTurnoverId1() != null
							&& vendorForm.getAnnualTurnoverId1() != 0) {
						CustomerVendorSales vendorSales1;
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId1());
						session.delete(vendorSales1);
					}
				}
				if (vendorForm.getSales2() != null
						&& !vendorForm.getSales2().isEmpty()) {
					CustomerVendorSales vendorSales1;
					if (vendorForm.getAnnualTurnoverId2() != null
							&& vendorForm.getAnnualTurnoverId2() != 0) {
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId2());
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales2()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear2());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setModifiedby(userId);
						vendorSales1.setModifiedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.merge(vendorSales1);
					} else {
						vendorSales1 = new CustomerVendorSales();
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales2()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear2());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setCreatedby(userId);
						vendorSales1.setCreatedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.save(vendorSales1);
					}
				} else {
					if (vendorForm.getAnnualTurnoverId2() != null
							&& vendorForm.getAnnualTurnoverId2() != 0) {
						CustomerVendorSales vendorSales1;
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId2());
						session.delete(vendorSales1);
					}
				}
				if (vendorForm.getSales3() != null
						&& !vendorForm.getSales3().isEmpty()) {
					CustomerVendorSales vendorSales1;
					if (vendorForm.getAnnualTurnoverId3() != null
							&& vendorForm.getAnnualTurnoverId3() != 0) {
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId3());
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales3()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear3());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setModifiedby(userId);
						vendorSales1.setModifiedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.merge(vendorSales1);
					} else {
						vendorSales1 = new CustomerVendorSales();
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales3()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear3());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setCreatedby(userId);
						vendorSales1.setCreatedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.save(vendorSales1);
					}
				} else {
					if (vendorForm.getAnnualTurnoverId3() != null
							&& vendorForm.getAnnualTurnoverId3() != 0) {
						CustomerVendorSales vendorSales1;
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId3());
						session.delete(vendorSales1);
					}
				}
				logger.info("Vendor sales update success.");

				logger.info("Vendor owner starts update ..");
				// update Customer Vendor Owner information

				if (vendorForm.getOwnerId1() != null
						&& vendorForm.getOwnerId1() != 0) {
					CustomerVendorOwner owner1 = (CustomerVendorOwner) session
							.get(CustomerVendorOwner.class,
									vendorForm.getOwnerId1());
					if (owner1 != null) {
						owner1 = VendorUtil.packVendorOwner1(vendorForm,
								userId, owner1, vendorMaster);
						owner1.setModifiedby(userId);
						owner1.setModifiedon(new Date());
						session.merge(owner1);
					}
				} else {
					if (null != vendorForm.getOwnerName1()
							&& !vendorForm.getOwnerName1().isEmpty()) {
						CustomerVendorOwner owner1 = new CustomerVendorOwner();
						owner1 = VendorUtil.packVendorOwner1(vendorForm,
								userId, owner1, vendorMaster);
						owner1.setCreatedby(userId);
						owner1.setCreatedon(new Date());
						session.save(owner1);
					}

				}
				if (vendorForm.getOwnerId2() != null
						&& vendorForm.getOwnerId2() != 0) {
					CustomerVendorOwner owner2 = (CustomerVendorOwner) session
							.get(CustomerVendorOwner.class,
									vendorForm.getOwnerId2());
					if (owner2 != null) {
						owner2 = VendorUtil.packVendorOwner2(vendorForm,
								userId, owner2, vendorMaster);
						owner2.setModifiedby(userId);
						owner2.setModifiedon(new Date());
						session.merge(owner2);
					}
				} else {
					if (null != vendorForm.getOwnerName2()
							&& !vendorForm.getOwnerName2().isEmpty()) {
						CustomerVendorOwner owner2 = new CustomerVendorOwner();
						owner2 = VendorUtil.packVendorOwner2(vendorForm,
								userId, owner2, vendorMaster);
						owner2.setCreatedby(userId);
						owner2.setCreatedon(new Date());
						session.save(owner2);
					}
				}

				if (vendorForm.getOwnerId3() != null
						&& vendorForm.getOwnerId3() != 0) {
					CustomerVendorOwner owner3 = (CustomerVendorOwner) session
							.get(CustomerVendorOwner.class,
									vendorForm.getOwnerId3());
					if (owner3 != null) {
						owner3 = VendorUtil.packVendorOwner3(vendorForm,
								userId, owner3, vendorMaster);
						owner3.setModifiedby(userId);
						owner3.setModifiedon(new Date());
						session.merge(owner3);
					}
				} else {
					if (null != vendorForm.getOwnerName3()
							&& !vendorForm.getOwnerName3().isEmpty()) {
						CustomerVendorOwner owner3 = new CustomerVendorOwner();
						owner3 = VendorUtil.packVendorOwner3(vendorForm,
								userId, owner3, vendorMaster);
						owner3.setCreatedby(userId);
						owner3.setCreatedon(new Date());
						session.save(owner3);
					}
				}
				logger.info("Vendor owner update success..");

				logger.info("Vendor address master start saving.");
				if (vendorForm.getAddress1() != null
						&& !vendorForm.getAddress1().isEmpty()) {
					CustomerVendorAddressMaster addressMaster;
					addressMaster = (CustomerVendorAddressMaster) session
							.createQuery(
									"from CustomerVendorAddressMaster where addressType = 'P' and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (addressMaster == null) {
						addressMaster = new CustomerVendorAddressMaster();
						addressMaster.setCreatedBy(userId);
						addressMaster.setCreatedOn(new Date());
					} else {
						addressMaster.setModifiedBy(userId);
						addressMaster.setModifiedOn(new Date());
					}
					addressMaster = VendorUtil.packVendorAddress1(vendorForm,
							userId, addressMaster, vendorMaster);

					session.merge(addressMaster);
				}
				if (vendorForm.getAddress2() != null
						&& !vendorForm.getAddress2().isEmpty()) {
					CustomerVendorAddressMaster addressMaster1;
					addressMaster1 = (CustomerVendorAddressMaster) session
							.createQuery(
									"from CustomerVendorAddressMaster where addressType = 'M' and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (addressMaster1 == null) {
						addressMaster1 = new CustomerVendorAddressMaster();
						addressMaster1.setCreatedBy(userId);
						addressMaster1.setCreatedOn(new Date());
					} else {
						addressMaster1.setModifiedBy(userId);
						addressMaster1.setModifiedOn(new Date());
					}
					addressMaster1 = VendorUtil.packVendorAddress2(vendorForm,
							userId, addressMaster1, vendorMaster);
					session.merge(addressMaster1);
				} else {
					CustomerVendorAddressMaster addressMaster1;
					addressMaster1 = (CustomerVendorAddressMaster) session
							.createQuery(
									"from CustomerVendorAddressMaster where addressType = 'M' and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (addressMaster1 != null) {
						session.delete(addressMaster1);
					}
				}
				logger.info("Vendor address master saved.");

				logger.info("Vendor status log update success.");

				if (vendorForm.getNewCommodityCode() != null
						&& vendorForm.getNewCommodityCode().length != 0) {
					// session.createQuery(
					// " delete CustomerVendorCommodity where vendorId="
					// + vendorMaster.getId()).executeUpdate();
					for (String commodityCode : vendorForm
							.getNewCommodityCode()) {
						CustomerVendorCommodity commodity = new CustomerVendorCommodity();
						CustomerCommodity comm = (CustomerCommodity) session
								.createQuery(
										" from CustomerCommodity where commodityCode='"
												+ commodityCode + "'")
								.uniqueResult();
						commodity.setVendorId(vendorMaster);
						commodity.setCommodityId(comm);
						commodity.setIsActive((byte) 1);
						commodity.setCreatedBy(userId);
						commodity.setCreatedOn(new Date());
						session.save(commodity);
					}
				}
				// update Vendor Certifications

				if (CommonUtils.getCheckboxValue(vendorForm
						.getDeverseSupplier()) == (byte) 1) {
					session.createQuery(
							" delete CustomerDiverseClassification where vendorId="
									+ vendorMaster.getId()).executeUpdate();
					// Iterate each classifications from UI
					if (vendorForm.getCertificates() != null
							&& vendorForm.getCertificates().length > 0) {

						for (Integer cert : vendorForm.getCertificates()) {

							CustomerDiverseClassification classification1 = new CustomerDiverseClassification();
							Certificate certMaster = (Certificate) session.get(
									Certificate.class, cert);
							classification1.setCertMasterId(certMaster);
							classification1.setVendorId(vendorMaster);
							classification1.setDiverseQuality((byte) 0);
							classification1.setIsActive((byte) 1);
							classification1.setCreatedBy(userId);
							classification1.setCreatedOn(new Date());
							classification1.setModifiedBy(userId);
							classification1.setModifiedOn(new Date());
							session.save(classification1);

						}
					}

					// update Vendor Certifications

					if (certifications != null && !certifications.isEmpty()) {
						for (DiverseCertificationTypesDto certificate : certifications) {

							VendorCertificate vendorCertificate = new VendorCertificate();
							CertifyingAgency certAgencyId = null;
							Certificate certMasterId = (Certificate) session
									.get(Certificate.class,
											certificate.getDivCertType());
							if (null != certificate.getDivCertAgent()) {
								certAgencyId = (CertifyingAgency) session.get(
										CertifyingAgency.class,
										certificate.getDivCertAgent());
							}

							if (certMasterId != null && certAgencyId != null
									&& vendorMaster != null) {
								if (certificate.getId() == null) {

									vendorCertificate = VendorUtil
											.packVendorCertificate(vendorForm,
													userId, vendorCertificate,
													vendorMaster, certMasterId,
													certAgencyId, certificate);
									vendorCertificate.setCreatedBy(userId);
									vendorCertificate.setCreatedOn(new Date());
								} else {
									vendorCertificate = (VendorCertificate) session
											.get(VendorCertificate.class,
													certificate.getId());
									// update status yes to
									// CertificateExpirationNotification table
									// when t2 customer vendor update the
									// certificate.
									if (vendorMaster.getIsinvited() != null
											&& vendorMaster.getIsinvited() == 0) {
										if (vendorCertificate != null) {
											List<CertificateExpirationNotification> notifications = session
													.createQuery(
															"From CertificateExpirationNotification where vendorid="
																	+ vendorMaster
																			.getId()
																	+ " and certificatenumber='"
																	+ vendorCertificate
																			.getCertificateNumber()
																	+ "' and status='NO'")
													.list();
											if (notifications != null
													&& !notifications.isEmpty()) {
												CertificateExpirationNotification notification = notifications
														.get(0);
												Date date = CommonUtils
														.dateConvertValue(certificate
																.getExpDate());
												if (date != null
														&& notification
																.getExpirationdate()
																.after(date)) {
													notification
															.setStatus("Yes");
													session.merge(notification);
												}
											}
										}
									}
									vendorCertificate = VendorUtil
											.packVendorCertificate(vendorForm,
													userId, vendorCertificate,
													vendorMaster, certMasterId,
													certAgencyId, certificate);
									vendorCertificate.setModifiedBy(userId);
									vendorCertificate.setModifiedOn(new Date());
								}
								session.saveOrUpdate(vendorCertificate);
							}
						}
					}
				}
				// update VendorNAICS codes

				if (naicsCodes != null && !naicsCodes.isEmpty()) {

					for (NaicsCodesDto codesDto : naicsCodes) {
						NaicsCode naicsMaster = (NaicsCode) session
								.createCriteria(NaicsCode.class)
								.add(Restrictions.eq("naicsCode",
										codesDto.getNaicsCode()))
								.uniqueResult();
						if (naicsMaster != null) {
							VendorNAICS vendorNaicsObj = null;
							if (null == codesDto.getId()) {
								vendorNaicsObj = new VendorNAICS();
								vendorNaicsObj.setVendorId(vendorMaster);
								vendorNaicsObj.setNaicsId(naicsMaster);
								vendorNaicsObj.setPrimary(codesDto
										.getPrimaryKey());
								vendorNaicsObj.setCapabilities(codesDto
										.getCapabilitie());
								vendorNaicsObj.setCreatedBy(userId);
								vendorNaicsObj.setCreatedOn(new Date());
								session.save(vendorNaicsObj);
							} else {
								vendorNaicsObj = (VendorNAICS) session.get(
										VendorNAICS.class, codesDto.getId());
								vendorNaicsObj.setVendorId(vendorMaster);
								vendorNaicsObj.setNaicsId(naicsMaster);
								vendorNaicsObj.setPrimary(codesDto
										.getPrimaryKey());
								vendorNaicsObj.setCapabilities(codesDto
										.getCapabilitie());
								vendorNaicsObj.setModifiedBy(userId);
								vendorNaicsObj.setModifiedOn(new Date());
								session.update(vendorNaicsObj);
							}
						}
					}
				}

				// Update Other Quality Certifications
				List<VendorOtherCertificate> vendorOtherCerts = (List<VendorOtherCertificate>) session
						.createCriteria(VendorOtherCertificate.class)
						.add(Restrictions.eq("vendorId", vendorMaster)).list();

				List<VendorOtherCertificate> vendorOtherCertsFromUI = CommonUtils
						.packageOtherCertificates(vendorForm, userId);

				if ((vendorOtherCerts != null && !vendorOtherCerts.isEmpty())
						|| (vendorOtherCertsFromUI != null && !vendorOtherCertsFromUI
								.isEmpty())) {
					// number of other certificate from DB
					int no_of_oc_db = vendorOtherCerts.size();
					// number of other certificate from UI
					int no_of_oc_ui = vendorOtherCertsFromUI.size();
					if (no_of_oc_db != 0 || no_of_oc_ui != 0) {
						for (int index = 0; index < no_of_oc_ui; index++) {
							if (index < no_of_oc_db) {
								VendorOtherCertificate certificate = vendorOtherCerts
										.get(index);
								VendorOtherCertificate certificateFromUI = vendorOtherCertsFromUI
										.get(index);
								certificate.setExpiryDate(certificateFromUI
										.getExpiryDate());
								certificate
										.setCertificationType(certificateFromUI
												.getCertificationType());

								if (certificateFromUI.getFilename() != null
										&& certificateFromUI.getFilename()
												.length() != 0) {

									certificate.setContent(certificateFromUI
											.getContent());
									certificate
											.setContentType(certificateFromUI
													.getContentType());
									certificate.setFilename(certificateFromUI
											.getFilename());
								}
								certificate.setVendorId(vendorMaster);
								certificate.setModifiedBy(userId);
								certificate.setModifiedOn(new Date());
								session.update(certificate);
							} else {
								VendorOtherCertificate certificateFromUI = vendorOtherCertsFromUI
										.get(index);
								certificateFromUI.setVendorId(vendorMaster);
								certificateFromUI.setCreatedBy(userId);
								certificateFromUI.setCreatedOn(new Date());
								session.save(certificateFromUI);
							}
						}
					}
				}

				logger.info("Quality and Other certificate updated success.");

				logger.info("Vendor documents updating process started.");

				logger.info("Vendor documents updated success.");
				session.getTransaction().commit();
				logger.info("Successfully commited the transaction.");
				result = "update";
			} catch (HibernateException ex) {
				session.getTransaction().rollback();
				PrintExceptionInLogFile.printException(ex);
				logger.info("Trancation rollback :" + ex);
				return "failure";
			} finally {
				if (session.isConnected()) {
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<VendorMaster> listVendors(UserDetailsDto appDetails) {
		List<VendorMaster> vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorMaster = session
					.createQuery(
							"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 ORDER BY vendorName ASC")
					.list();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "deprecation", "deprecation" })
	@Override
	public RetriveVendorInfoDto retriveVendorInfo(Integer vendorId,
			UserDetailsDto appDetails) {

		RetriveVendorInfoDto vendorInfo = new RetriveVendorInfoDto();
		VendorMaster vendorMaster = null;
		List<CustomerVendornetworldCertificate> cvwCertificate =null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorMaster = (VendorMaster) session.get(VendorMaster.class,
					vendorId);
			// Getting Naics Codes information
			String[] naicsCodes = new String[vendorMaster.getVendornaicsList()
					.size()];
			List<NaicsCode> naicsMasters = new ArrayList<NaicsCode>();
			int i;
			for (i = 0; i < vendorMaster.getVendornaicsList().size(); i++) {
				naicsCodes[i] = vendorMaster.getVendornaicsList().get(i)
						.getNaicsId().getNaicsCode();
			}
			for (i = 0; i < naicsCodes.length; i++) {
				NaicsCode naicsMasterValue = new NaicsCode();
				naicsMasterValue = (NaicsCode) session
						.createCriteria(NaicsCode.class)
						.add(Restrictions.eq("naicsCode", naicsCodes[i]))
						.uniqueResult();
				naicsMasters.add(naicsMasterValue);
			}

			if (vendorMaster.getEthnicityId() != null) {
				vendorMaster.getEthnicityId().getId();
			}
			
			vendorInfo.setVendorMaster(vendorMaster);
			vendorMaster.getVendorUsersList().size();
			vendorInfo.setVendorContacts(vendorMaster.getVendorUsersList());
			// will make hibernate initialize the collection for you instead of
			// the proxy
			vendorMaster.getVendorUsersList().size();
			vendorInfo.setVendorNaics(vendorMaster.getVendornaicsList());
			// will make hibernate initialize the collection for you instead of
			// the proxy
			vendorMaster.getVendorcertificateList().size();
			vendorInfo.setVendorCertificate(vendorMaster
					.getVendorcertificateList());
			// will make hibernate initialize the collection for you instead of
			// the proxy
			vendorMaster.getVendorothercertificateList().size();
			vendorInfo.setVendorOtherCert(vendorMaster
					.getVendorothercertificateList());
			// will make hibernate initialize the collection for you instead of
			// the proxy
			vendorMaster.getVendorDocuments().size();
			vendorMaster.getCustomerVendorownerList().size();
			vendorMaster.getCustomerVendorSalesList().size();
			vendorMaster.setVendorDocuments(vendorMaster.getVendorDocuments());
			vendorInfo.setNaicsMaster(naicsMasters);
			WorkflowConfigurationDao dao = new WorkflowConfigurationDaoImpl();
			vendorInfo.setCustomerWflog(dao.findByVendorId(appDetails,
					vendorMaster.getId()));
			cvwCertificate = ((List<CustomerVendornetworldCertificate>)session.createCriteria(CustomerVendornetworldCertificate.class).add(Restrictions.eq("vendorId", vendorMaster)).list());
			if(cvwCertificate!=null && !cvwCertificate.isEmpty()){
				CustomerVendornetworldCertificate cvwCert1 = cvwCertificate.get(0);
				if(cvwCert1.getHasNetWorldCertificate()!=null && cvwCert1.getHasNetWorldCertificate().equals("Yes") && cvwCert1.getHasNetWorldCertificate() !=null)
				{
				vendorInfo.setIsNetWorldCertificate(cvwCert1.getOrginalFile().toString());
				vendorInfo.setHasNetWoldcert(true);
				}else{
					vendorInfo.setHasNetWoldcert(false);
				}
				
			}else
			{
				vendorInfo.setHasNetWoldcert(false);
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorInfo;
	}

	/**
	 * Implementing code for get the list type of NaicsCategory objects and
	 * returns back to Action class.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<NaicsCategory> listNaicsCategory(UserDetailsDto appDetails) {
		List<NaicsCategory> listCategories = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			listCategories = session.createQuery(
					"from NaicsCategory where isActive=1").list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return listCategories;
	}

	/**
	 * Implementing code for get the list type of NAICSubCategory objects and
	 * return backs to action class.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<NAICSubCategory> listNaicsSubCategory(UserDetailsDto appDetails) {
		List<NAICSubCategory> naicsSubCategory = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			naicsSubCategory = session
					.createQuery(
							"from NAICSubCategory where isActive=1 and naicCategoryId=1")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsSubCategory;
	}

	/**
	 * Implementing code for get the list type of NAICSubCategory objects by
	 * passing the id as argument which return the list of NAICSubCategory
	 * values
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<NAICSubCategory> listNaicsSubCategory(int findNaicsCode,
			UserDetailsDto appDetails) {

		List<NAICSubCategory> naicsSubCategory = null;
		NaicsCategory naicsCategory = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			naicsCategory = (NaicsCategory) session.get(NaicsCategory.class,
					findNaicsCode);
			naicsSubCategory = session
					.createCriteria(NAICSubCategory.class)
					.add(Restrictions.eq("naicCategoryId",
							naicsCategory.getId())).list();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsSubCategory;
	}

	/**
	 * Implementing the code for get the list type of NaicsMaster objects
	 * Returns the list type of NAICSMaster class which has the properties like
	 * naicsCode, naicsDescription, isiccode and isicdescription
	 * 
	 * @param categoryId
	 *            the object of the NaicsCategory class
	 * @param subCategoryId
	 *            the object of the NaicsSubCategory class
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<NaicsMaster> listNAICSMaster(int categoryId, int subCategoryId,
			UserDetailsDto appDetails) {

		List<NaicsMaster> naicsMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		try {
			NaicsCategory naicsCategoryId = (NaicsCategory) session.get(
					NaicsCategory.class, categoryId);
			NAICSubCategory naicsSubCategoryId = (NAICSubCategory) session.get(
					NAICSubCategory.class, subCategoryId);
			naicsMaster = session
					.createQuery(
							"from NaicsMaster where naicsCategoryId=? and naicsSubCategoryId=? and isActive=1")
					.setParameter("naicsCategoryId", naicsCategoryId)
					.setParameter("naicsSubCategoryId", naicsSubCategoryId)
					.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsMaster;
	}

	/**
	 * Implements the code for retrive the vendor contact details for updation
	 */
	@Override
	public VendorContact retriveVendorContact(Integer contactId,
			UserDetailsDto appDetails) {
		VendorContact vendorContact = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			vendorContact = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("id", contactId)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorContact;
	}

	/**
	 * Implements the code for delete the vendorContact means make the status
	 * IsActive as 0.
	 */
	@Override
	public String deleteContact(Integer contactId, UserDetailsDto appDetails) {
		String result = "delete";

		VendorContact vendorContact = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			vendorContact = (VendorContact) session.get(VendorContact.class,
					contactId);
			List<VendorContact> list = session.createQuery(
					"From VendorContact where vendorId="
							+ vendorContact.getVendorId().getId()
							+ " and isActive=1").list();
			if (null != list && !list.isEmpty() && list.size() > 1) {
				vendorContact.setIsActive((byte) 0);
				session.update(vendorContact);
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * Implements the code for save the contact info into database
	 */
	@Override
	public String saveContact(VendorMaster vendorMaster,
			VendorContactInfo contactForm, Integer userId,
			UserDetailsDto appDetails) {

		String result = "saveContact";
		VendorContact vendorContact = new VendorContact();
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		Random random = new java.util.Random();
		try {
			session.beginTransaction();
			VendorRoles role = (VendorRoles) session
					.createCriteria(VendorRoles.class)
					.add(Restrictions.and(
							Restrictions.eq("id", contactForm.getRoleId()),
							Restrictions.eq("isActive", (byte) 1)))
					.uniqueResult();

			vendorContact = packVendorContact(contactForm, vendorContact,
					vendorMaster, userId, appDetails, session);
			vendorContact.setCreatedBy(userId);
			vendorContact.setCreatedOn(new Date());
			Integer randVal = random.nextInt();
			Encrypt encrypt = new Encrypt();
			// convert password to encrypted password
			String encyppassword = encrypt.encryptText(
					Integer.toString(randVal) + "",
					contactForm.getLoginpassword());
			vendorContact.setIsBusinessContact(CommonUtils
					.getCheckboxValue(contactForm.getIsBusinessContact()));
			vendorContact.setIsPrimaryContact(CommonUtils
					.getCheckboxValue(contactForm.getPrimaryContact()));
			vendorContact.setIsPreparer((byte) 0);
			vendorContact.setPassword(encyppassword);
			vendorContact.setKeyValue(randVal);
			vendorContact.setVendorUserRoleId(role);
			vendorContact.setIsSysGenPassword((byte) 0);
			vendorContact.setGender(contactForm.getGender());
			vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(true));
			session.merge(vendorContact);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "failure";
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param contactForm
	 * @param vendorContact
	 * @param vendorMaster
	 * @param userId
	 * @param contactId
	 * @param appDetails
	 * @return
	 */
	private VendorContact packVendorContact(VendorContactInfo contactForm,
			VendorContact vendorContact, VendorMaster vendorMaster,
			Integer userId, UserDetailsDto appDetails, Session session) {

		vendorContact.setFirstName(contactForm.getFirstName());
		vendorContact.setLastName(contactForm.getLastName());
		vendorContact.setPhoneNumber(contactForm.getContactPhone());
		vendorContact.setMobile(contactForm.getContactMobile());
		vendorContact.setFax(contactForm.getContactFax());
		VendorRoles vendorRoles = (VendorRoles) session.get(VendorRoles.class,
				contactForm.getRoleId());
		vendorContact.setVendorUserRoleId(vendorRoles);
		vendorContact.setEmailId(contactForm.getContanctEmail());
		vendorContact.setDesignation(contactForm.getDesignation());
		vendorContact.setIsAllowedLogin(CommonUtils
				.getCheckboxValue(contactForm.getLoginAllowed()));
		vendorContact.setIsBusinessContact(CommonUtils
				.getCheckboxValue(contactForm.getIsBusinessContact()));
		// vendorContact.setIsPrimaryContact(CommonUtils
		// .getCheckboxValue(contactForm.getPrimaryContact()));
		// vendorContact.setLoginDisplayName(contactForm.getLoginDisplayName());
		// if (contactForm.getLoginId() != null
		// && !contactForm.getLoginId().isEmpty()) {
		// vendorContact.setLoginId(contactForm.getLoginId());
		// }

		SecretQuestion secretQns = (SecretQuestion) session.get(
				SecretQuestion.class, contactForm.getUserSecQn());
		vendorContact.setSecretQuestionId(secretQns);
		vendorContact.setSecQueAns(contactForm.getUserSecQnAns());
		vendorContact.setVendorId(vendorMaster);
		vendorContact.setIsActive((byte) 1);
		return vendorContact;

	}

	/**
	 * Implements the code for save the contact info into database
	 */
	@Override
	public String updateContact(VendorMaster vendorMaster,
			VendorContactInfo contactForm, Integer userId, Integer contactId,
			UserDetailsDto appDetails) {

		String result = "updateContact";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String findUnique;
		CommonDao commonDao = new CommonDaoImpl();

		try {
			session.beginTransaction();

			VendorContact vendorContact = (VendorContact) session.get(
					VendorContact.class, Integer.parseInt(contactForm.getId()));
			if (vendorContact.getEmailId() != null
					&& contactForm.getContanctEmail() != null
					&& !vendorContact.getEmailId().equalsIgnoreCase(
							contactForm.getContanctEmail())) {
				findUnique = commonDao.validateLoginId(
						contactForm.getContanctEmail(), appDetails);
				if (!findUnique.equalsIgnoreCase("submit")) {
					return findUnique;

				}
			}
			if (CommonUtils.getCheckboxValue(contactForm.getPrimaryContact()) == 1) {
				VendorContact primeContact = (VendorContact) session
						.createCriteria(VendorContact.class)
						.add(Restrictions.and(
								Restrictions.eq("isPrimaryContact", (byte) 1),
								Restrictions.eq("vendorId", vendorMaster)))
						.uniqueResult();

				if (null != primeContact) {
					Criteria crit = session.createCriteria(VendorContact.class);
					crit.setProjection(Projections.rowCount());
					crit.add(Restrictions.eq("vendorId", vendorMaster));
					Long count = (Long) crit.uniqueResult();
					if (count > 1) {
						primeContact.setIsPrimaryContact((byte) 0);
						session.merge(primeContact);
					}
				}
				vendorContact.setIsPrimaryContact(CommonUtils
						.getCheckboxValue(contactForm.getPrimaryContact()));
			}
			Decrypt decrypt = new Decrypt();
			String decryptedPassword = null;
			Integer randVal = vendorContact.getKeyValue();
			if (null != vendorContact.getPassword()
					&& !vendorContact.getPassword().isEmpty()) {
				decryptedPassword = decrypt.decryptText(
						String.valueOf(randVal.toString()),
						vendorContact.getPassword());
			}

			vendorContact = packVendorContact(contactForm, vendorContact,
					vendorMaster, userId, appDetails, session);
			vendorContact.setModifiedBy(userId);
			vendorContact.setModifiedOn(new Date());

			// Random random = new java.util.Random();

			Encrypt encrypt = new Encrypt();
			// convert password to encrypted password
			String encyppassword = null;
			if (null != randVal) {
				encyppassword = encrypt.encryptText(Integer.toString(randVal)
						+ "", contactForm.getLoginpassword());
			}
			if (null != decryptedPassword
					&& !decryptedPassword
							.equals(contactForm.getLoginpassword())) {
				// password History
				List<PasswordHistory> histories = session.createQuery(
						" From PasswordHistory where vendorUserId="
								+ vendorContact.getId()
								+ " order by changedon desc limit 12").list();
				if (CommonUtils.checkPasswordLimitExist(histories,
						contactForm.getLoginpassword()).equals("exist")) {
					return "exist";
				} else {

					PasswordHistory passwordHistory = new PasswordHistory();
					passwordHistory.setKeyvalue(randVal);
					passwordHistory.setChangedon(new Date());
					passwordHistory.setChangedby(userId);
					passwordHistory.setPassword(encyppassword);
					passwordHistory.setVendorUserId(vendorContact.getId());
					session.save(passwordHistory);
				}
			}
			vendorContact.setPassword(encyppassword);
			vendorContact.setKeyValue(randVal);
			vendorContact.setGender(contactForm.getGender());
			session.merge(vendorContact);
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "failure";
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * implementing the code for getting the vendor master details (vinoth
	 * karthi)
	 */
	@SuppressWarnings("unchecked")
	public List<VendorMaster> listOfVendors(UserDetailsDto appDetails) {

		List<VendorMaster> vendorMasters = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorMasters = session.createQuery(
					"from VendorMaster where isActive=1").list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorMasters;
	}

	/**
	 * implementing the code for getting the uninvited vendor master details
	 * (vinoth)
	 */
	@SuppressWarnings("unchecked")
	public List<VendorMaster> listOfUninvitedVendors(UserDetailsDto appDetails,
			Integer invited) {

		List<VendorMaster> vendorMasters = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorMasters = session.createQuery(
					"from VendorMaster where isActive=1 and isinvited="
							+ invited).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorMasters;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VendorMaster> listVendorsforSpentData(VendorContact vendor,
			UserDetailsDto appDetails) {
		List<VendorMaster> vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		session.beginTransaction();
		try {
			vendorMaster = session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.and(Restrictions.eq("parentVendorId",
							vendor.getVendorId().getParentVendorId()),
							Restrictions.eq("isActive", (byte) 1))).list();

		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerApplicationSettings getCustomerApplicationSettings(
			UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		com.fg.vms.customer.model.CustomerApplicationSettings applicationSettings = null;
		try {
			session.beginTransaction();

			SQLQuery query = session
					.createSQLQuery("select * from customer_applicationsettings");
			query.addEntity(CustomerApplicationSettings.class);
			Iterator<?> iterator = query.list().iterator();

			while (iterator.hasNext()) {
				Object[] appSettings = (Object[]) iterator.next();
				applicationSettings = (CustomerApplicationSettings) appSettings[0];
			}

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return applicationSettings;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteVendor(Integer id, UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		VendorMaster vendor;
		try {
			session.beginTransaction();
			vendor = (VendorMaster) session.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.delete(vendor);
			session.getTransaction().commit();
			return "deleted";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "reference";

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countPrimeVendors(UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		Long count = null;
		try {
			session.beginTransaction();
			count = (long) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.and(
							Restrictions.eq("primeNonPrimeVendor", (byte) 1),
							Restrictions.eq("isActive", (byte) 1))).list()
					.size();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return count;
	}

	/**
	 * 
	 */
	@Override
	public List<EmailDistribution> retriveEmailIds(UserDetailsDto appDetails,
			String queryString) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<EmailDistribution> list = null;
		try {
			session.beginTransaction();

			Query query = session.createQuery(queryString);
			list = query.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;
	}

	/**
	 * This method helps in convert string array to string.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToString(String[] stringarray) {
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) {
			for (String st : stringarray) {
				sb.append(st.trim()).append(',');
			}
			if (stringarray.length != 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerVendorbusinessbiographysafety> getBiography(
			Integer vendorId, UserDetailsDto appDetails, String type) {

		List<CustomerVendorbusinessbiographysafety> safeties = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			safeties = session.createQuery(
					"from CustomerVendorbusinessbiographysafety where isActive=1 and vendorId="
							+ vendorId + " and businesssafety= '" + type + "'")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return safeties;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerVendorbusinessbiographysafety> getBiographySequence(
			Integer vendorId, UserDetailsDto appDetails, Integer questionnumber) {

		List<CustomerVendorbusinessbiographysafety> safeties = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			safeties = session.createQuery(
					"from CustomerVendorbusinessbiographysafety where questionnumber = "
							+ questionnumber).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return safeties;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerVendorreference> getListReferences(Integer vendorId,
			UserDetailsDto appDetails) {

		List<CustomerVendorreference> refs = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			refs = session
					.createQuery(
							"from CustomerVendorreference where vendorId = "
									+ vendorId).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return refs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorContact getSavedVendorData(String emailId,
			UserDetailsDto userDetails) {

		VendorContact vendorContact = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		try {
			session.beginTransaction();

			vendorContact = (VendorContact) session.createQuery(
					"from VendorContact where isPreparer = 1 and emailId = '"
							+ emailId + "'").uniqueResult();
			if (vendorContact != null) {
				vendorContact.getVendorId().getId();
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorContact;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorMaster getSavedVendorMasterData(Integer id,
			UserDetailsDto appDetails) {

		VendorMaster master = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();

			master = (VendorMaster) session.createQuery(
					"from VendorMaster where isPartiallySubmitted = 'Yes' and id = '"
							+ id + "'").uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return master;
	}

	@Override
	public String mergeMoreInfo(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userdetails,
			VendorContact currentVendor) {
		logger.info("Inside the merge  vendor method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userdetails));
		session.beginTransaction();
		try {

			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();

		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			return "failure";
		}

		if (vendorMaster != null) {
			logger.info("=================== Vendor update start here =============");

			try {

				logger.info("Vendor business biography starts update ..");
				if (vendorForm.getIsBpSupplier() != null) {

					session.createQuery(
							" delete CustomerVendorbusinessbiographysafety where vendorId="
									+ vendorMaster.getId()).executeUpdate();
					VendorUtil.saveBusinessBiography(vendorForm, userId,
							vendorMaster, session);
				}
				logger.info("Vendor business biography update success..");

				if (vendorForm.getReferenceName1() != null
						&& !vendorForm.getReferenceName1().isEmpty()) {

					session.createQuery(
							" delete CustomerVendorreference where vendorId="
									+ vendorMaster.getId()).executeUpdate();

					logger.info("References start updating..");

					VendorUtil.saveReferences(vendorForm, userId, vendorMaster,
							session);

					logger.info("References updated successfully..");
				}

				logger.info("Vendor documents updating process started.");

				// Update vendor documents
				List<VendorDocuments> vendorDocuments = (List<VendorDocuments>) session
						.createCriteria(VendorDocuments.class)
						.add(Restrictions.eq("vendorId", vendorMaster)).list();
				String companyCode = userdetails.getCustomer().getCustCode();
				List<VendorDocuments> vendorDocumentsUI = CommonUtils
						.packageVendorDocuments(vendorForm, vendorMaster,
								userId, companyCode);

				if ((vendorDocuments != null && vendorDocuments.size() != 0)
						|| (vendorDocumentsUI != null && vendorDocumentsUI
								.size() != 0)) {

					int noOfDocInDb = vendorDocuments.size();
					int noOdDocFromUI = vendorDocumentsUI.size();

					if (noOfDocInDb != 0 || noOdDocFromUI != 0) {

						for (int index = 0; index < noOdDocFromUI; index++) {
							if (index < noOfDocInDb) {
								VendorDocuments documents = vendorDocuments
										.get(index);
								VendorDocuments documentsFromUI = vendorDocumentsUI
										.get(index);
								documents.setDocumentName(vendorForm
										.getVendorDocName()[index]);
								documents.setDocumentDescription(vendorForm
										.getVendorDocDesc()[index]);
								documents.setModifiedOn(new Date());
								documents.setModifiedBy(userId);
								if (vendorForm.getVendorDocs() != null
										&& vendorForm.getVendorDocs()
												.get(index).getFileSize() != 0) {
									documents.setDocumentFilename(vendorForm
											.getVendorDocs().get(index)
											.getFileName());
									documents.setDocumentType(vendorForm
											.getVendorDocs().get(index)
											.getContentType());
									documents
											.setDocumentFilesize((double) vendorForm
													.getVendorDocs().get(index)
													.getFileSize());
								}
								documents.setVendorId(vendorMaster);
								String filePath = Constants
										.getString(Constants.UPLOAD_DOCUMENT_PATH)
										+ "/" + vendorMaster.getId();
								documents.setDocumentPhysicalpath(filePath);

								session.update(documents);
							} else {
								VendorDocuments documentsFromUI = vendorDocumentsUI
										.get(index);
								documentsFromUI.setCreatedBy(userId);
								documentsFromUI.setCreatedOn(new Date());
								documentsFromUI.setVendorId(vendorMaster);
								session.save(documentsFromUI);

							}
						}
					}
				}

				logger.info("Vendor documents updated success.");

				session.getTransaction().commit();
				logger.info("Successfully commited the transaction.");
				result = "update";
			} catch (HibernateException ex) {
				session.getTransaction().rollback();
				logger.info("Trancation rollback :" + ex);
				return "failure";
			} finally {
				if (session.isConnected()) {
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VendorContact> getBusinessContactDetails(
			Integer vendorMasterId, UserDetailsDto userDetails) {

		List<VendorContact> vendorContacts = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		try {
			session.beginTransaction();

			vendorContacts = session.createQuery(
					"from VendorContact where isBusinessContact = 1 and vendorId = "
							+ vendorMasterId).list();
			if (vendorContacts != null) {
				vendorContacts.get(0).getVendorId().getId();
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorContacts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ClassificationDto> getClassifcationDetails(
			VendorMasterForm vendorForm, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<ClassificationDto> classificationDtos = new ArrayList<ClassificationDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select 1 as id,cm.CERTIFICATENAME, group_concat(ca.ID, '-', ca.CERTAGENCYNAME SEPARATOR '|') Agency"
					+ " From customer_certificatemaster cm, customer_certificateagencymaster ca, customer_classification_certifyingagencies cca"
					+ " Where cm.ID = cca.CERTIFICATEID and ca.id = cca.AGENCYID And cm.ISACTIVE = 1 And ca.ISACTIVE = 1"
					+ " And cca.ISACTIVE = 1");
			if (vendorForm.getDivCertType1() != null
					&& !vendorForm.getDivCertType1().isEmpty()) {

				sqlQuery.append(" And cm.ID = " + vendorForm.getDivCertType1());

			} else if (vendorForm.getDivCertType2() != null
					&& !vendorForm.getDivCertType2().isEmpty()) {

				sqlQuery.append(" And cm.ID = " + vendorForm.getDivCertType2());

			} else if (vendorForm.getDivCertType3() != null
					&& !vendorForm.getDivCertType3().isEmpty()) {

				sqlQuery.append(" And cm.ID = " + vendorForm.getDivCertType3());
			}
			sqlQuery.append(" group by cm.CERTIFICATENAME union all"
					+ " Select 2 as id, cm.CERTIFICATENAME, group_concat(ct.ID, '-', ct.DESCRIPTION SEPARATOR '|') CertificateType"
					+ " From customer_certificatemaster cm, customer_certificatetype ct, customer_classification_certificatetypes cct"
					+ " Where cm.id = cct.CERTIFICATEID and ct.id = cct.CERTIFICATETYPEID And ct.ISACTIVE = 1 And cm.ISACTIVE = 1"
					+ " And cct.ISACTIVE = 1");
			if (vendorForm.getDivCertType1() != null
					&& !vendorForm.getDivCertType1().isEmpty()) {

				sqlQuery.append(" And cm.ID = " + vendorForm.getDivCertType1());

			} else if (vendorForm.getDivCertType2() != null
					&& !vendorForm.getDivCertType2().isEmpty()) {

				sqlQuery.append(" And cm.ID = " + vendorForm.getDivCertType2());

			} else if (vendorForm.getDivCertType3() != null
					&& !vendorForm.getDivCertType3().isEmpty()) {

				sqlQuery.append(" And cm.ID = " + vendorForm.getDivCertType3());
			}
			sqlQuery.append(" group by cm.CERTIFICATENAME ");
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> areas = query.list();
			if (areas != null) {
				iterator = areas.iterator();
				while (iterator.hasNext()) {
					ClassificationDto dto = new ClassificationDto();
					Object[] objects = (Object[]) iterator.next();
					dto.setClassificationId(Integer.parseInt(objects[0]
							.toString()));
					dto.setAgencyData(objects[1].toString());
					dto.setCertificateTypeData(objects[2].toString());
					classificationDtos.add(dto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			// System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return classificationDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String newRegistration(UserDetailsDto appDetails,
			VendorMasterForm vendorForm, String password, String vendorType) {
		String result = "";
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, c.get(Calendar.DATE) + 7);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();

			CustomerVendorSelfRegInfo selfRegInfo = (CustomerVendorSelfRegInfo) session
					.createQuery(
							"from CustomerVendorSelfRegInfo where emailId = '"
									+ vendorForm.getPreparerEmail() + "'")
					.uniqueResult();
			Users user = (Users) session
					.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId",
							vendorForm.getPreparerEmail()).ignoreCase())
					.uniqueResult();

			VendorContact vendorUser = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("emailId",
							vendorForm.getPreparerEmail()).ignoreCase())
					.uniqueResult();

			if (user != null && vendorUser != null) {
				result = "userEmail";
			} else if (user != null) {
				result = "userEmail";
			} else if (vendorUser != null) {
				result = "userEmail";
			}
			if (selfRegInfo != null
					|| (!result.isEmpty() && "userEmail".equals(result))) {
				result = "userEmail";
				return result;
			}

			if (vendorForm.getPreparerEmail() != null
					&& !vendorForm.getPreparerEmail().isEmpty()) {
				CustomerVendorSelfRegInfo regInfo = new CustomerVendorSelfRegInfo();
				Random random = new java.util.Random();
				Integer randVal = random.nextInt();
				Encrypt encrypt = new Encrypt();
				/* convert password to encrypted password. */
				String encyppassword = encrypt.encryptText(
						Integer.toString(randVal) + "", password);
				regInfo.setEmailId(vendorForm.getPreparerEmail());
				regInfo.setKeyValue(randVal);
				regInfo.setPassword(encyppassword);
				regInfo.setCreatedOn(new Date());
				regInfo.setValidUpto(c.getTime());
				regInfo.setVendorId(null);
				regInfo.setCount(Integer.parseInt("0"));

				if (vendorForm.getCustomerDivision() != null) {
					CustomerDivision customerDivision = (CustomerDivision) session
							.get(CustomerDivision.class,
									vendorForm.getCustomerDivision());
					regInfo.setCustomerDivisionId(customerDivision);
				}

				if (null != vendorType && !vendorType.isEmpty()) {
					regInfo.setVendorType("P");
				}
				session.save(regInfo);
			}
			result = "success";
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
			result = "failure";
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String saveMeetingDetails(UserDetailsDto appDetails,
			VendorMasterForm vendorForm, Integer vendorId) {
		Integer userId = -1;
		String result = "";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			VendorMaster master = (VendorMaster) session.createQuery(
					"From VendorMaster where id=" + vendorId).uniqueResult();

			if (null != vendorForm.getVendorNotes()
					&& !vendorForm.getVendorNotes().isEmpty()) {
				// master.setVendorNotes(vendorForm.getVendorNotes());
				session.merge(master);
			} else {
				master.setVendorNotes(null);
				session.merge(master);
			}

			if (vendorForm.getContactFirstName() != null
					&& !vendorForm.getContactFirstName().isEmpty()) {

				CustomerVendorMeetingInfo vendorMeetingInfo = (CustomerVendorMeetingInfo) session
						.createQuery(
								"From CustomerVendorMeetingInfo vm where vm.vendorId="
										+ vendorId).uniqueResult();
				if (vendorMeetingInfo != null) {
					vendorMeetingInfo = VendorUtil.packMeetingInformation(
							vendorForm, userId, vendorMeetingInfo, master);
					session.merge(vendorMeetingInfo);
				} else {
					CustomerVendorMeetingInfo meetingInfo = new CustomerVendorMeetingInfo();
					meetingInfo = VendorUtil.packMeetingInformation(vendorForm,
							userId, meetingInfo, master);
					session.save(meetingInfo);
				}
			} else {

				if (vendorForm.getContactLastName().isEmpty()
						&& vendorForm.getContactDate().isEmpty()) {
					CustomerVendorMeetingInfo vendorMeetingInfo = (CustomerVendorMeetingInfo) session
							.createQuery(
									"From CustomerVendorMeetingInfo vm where vm.vendorId="
											+ vendorId).uniqueResult();
					if (vendorMeetingInfo != null) {
						session.delete(vendorMeetingInfo);
					}
				}

			}
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=12 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(12);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(master);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=13 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(13);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(master);
					menuStatus1.setStatus(2);
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			result = "success";
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(ex);
			result = "failure";
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorMaster> listActiveVendors(UserDetailsDto appDetails) {
		List<VendorMaster> vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			vendorMaster = session
					.createQuery(
							"from VendorMaster where isActive = 1 and primeNonPrimeVendor=1 and (vendorStatus = 'A' OR vendorStatus = 'B') ORDER BY vendorName ASC")
					.list();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorMaster;
	}

	@Override
	public String savePrimeVendor(VendorMasterForm vendorForm, Integer userId,
			UserDetailsDto appDetails, String url) {

		CommonDao commonDao = new CommonDaoImpl();
		String emailExist = commonDao.checkDuplicateEmail(
				vendorForm.getContanctEmail(), appDetails);
		String result = "success";
		if (emailExist.equalsIgnoreCase("submit")) {

			Session session = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(appDetails));

			try {
				session.beginTransaction();
				VendorMaster vendorMaster = new VendorMaster();

				// Save VendorMaster Company Information
				vendorMaster.setParentVendorId(0);
				vendorMaster.setVendorName(vendorForm.getVendorName());

				vendorMaster.setNumberOfEmployees(Integer.parseInt("0"));
				vendorMaster.setAnnualTurnover(CommonUtils.deformatMoney("0"));

				vendorMaster.setPrimeNonPrimeVendor(CommonUtils
						.getByteValue(true));
				vendorMaster.setIsApproved((byte) 1);
				vendorMaster.setApprovedOn(new Date());
				vendorMaster.setEmailId(vendorForm.getEmailId());
				vendorMaster.setDeverseSupplier((byte) 0);

				// approval process
				vendorMaster.setIsActive((byte) 1);
				vendorMaster.setIsinvited((short) 1);
				vendorMaster.setCreatedBy(userId);
				vendorMaster.setCreatedOn(new Date());
				vendorMaster.setModifiedBy(userId);
				vendorMaster.setModifiedOn(new Date());
				vendorMaster
						.setVendorStatus(VendorStatus.BPSUPPLIER.getIndex());
				session.save(vendorMaster);
				vendorMaster.setVendorCode(vendorMaster.getId().toString());

				// Save Customer Division For the users
				if (appDetails.getSettings() != null
						&& appDetails.getSettings().getIsDivision() != null
						&& appDetails.getSettings().getIsDivision() != 0) {
					if (vendorForm.getCustomerDivision() != null
							&& vendorForm.getCustomerDivision() != 0) {
						CustomerDivision customerDivision = (CustomerDivision) session
								.get(CustomerDivision.class,
										vendorForm.getCustomerDivision());
						vendorMaster.setCustomerDivisionId(customerDivision);
					}
				}
				session.merge(vendorMaster);

				CustomerVendorAddressMaster addressMaster = new CustomerVendorAddressMaster();
				addressMaster.setVendorId(vendorMaster);
				addressMaster.setAddressType("p");
				addressMaster.setAddress(vendorForm.getAddress1());
				addressMaster.setCity(vendorForm.getCity());
				addressMaster.setCountry(vendorForm.getCountry());
				addressMaster.setState(vendorForm.getState());
				addressMaster.setProvince("");
				addressMaster.setPhone(vendorForm.getPhone());
				addressMaster.setZipCode(vendorForm.getZipcode());
				addressMaster.setFax(vendorForm.getFax());
				addressMaster.setIsActive((byte) 1);
				addressMaster.setCreatedBy(userId);
				addressMaster.setCreatedOn(new Date());
				session.save(addressMaster);

				// Save VendorContact Information
				VendorContact vendorContact = new VendorContact();
				VendorRoles roles = (VendorRoles) session
						.createCriteria(VendorRoles.class)
						.add(Restrictions.eq("isActive", (byte) 1)).list()
						.get(0);

				vendorContact.setVendorId(vendorMaster);
				vendorContact.setVendorUserRoleId(roles);

				Random random = new java.util.Random();
				Integer randVal = random.nextInt();
				Encrypt encrypt = new Encrypt();
				String psw = String.valueOf(PasswordUtil.generatePswd(8, 8, 1,
						1, 0));
				// convert password to encrypted password
				String encyppassword = encrypt.encryptText(
						Integer.toString(randVal) + "", psw);
				vendorContact.setFirstName(vendorForm.getFirstName());
				// vendorContact.setLastName(lastName);
				vendorContact.setPhoneNumber(vendorForm.getContactPhone());
				vendorContact.setDesignation(vendorForm.getDesignation());
				vendorContact.setEmailId(vendorForm.getContanctEmail());
				vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(true));
				vendorContact.setIsPrimaryContact((byte) 1);
				vendorContact.setPassword(encyppassword);
				vendorContact.setKeyValue(randVal);
				vendorContact.setIsSysGenPassword((byte) 0);

				SecretQuestion secretQns = new SecretQuestion();
				secretQns = (SecretQuestion) session.get(SecretQuestion.class,
						1);

				vendorContact.setSecretQuestionId(secretQns);
				vendorContact.setSecQueAns("");
				vendorContact.setIsActive((byte) 1);
				vendorContact.setCreatedBy(userId);
				vendorContact.setCreatedOn(new Date());
				vendorContact.setModifiedBy(userId);
				vendorContact.setModifiedOn(new Date());
				vendorContact.setIsBusinessContact((byte) 0);
				vendorContact.setIsPreparer((byte) 1);
				vendorContact.setGender(vendorForm.getGender());
				session.save(vendorContact);
				CustomerVendorMeetingInfo vendorMeetingInfo = new CustomerVendorMeetingInfo();
				vendorMeetingInfo.setVendorId(vendorMaster);
				vendorMeetingInfo.setContactFirstName(vendorForm
						.getContactFirstName());
				vendorMeetingInfo.setContactLastName(vendorForm
						.getContactLastName());
				vendorMeetingInfo.setContactEvent(vendorForm.getContactEvent());
				vendorMeetingInfo.setContactDate(CommonUtils
						.dateConvertValue(vendorForm.getContactDate()));
				vendorMeetingInfo.setContactState(vendorForm.getContactState());
				vendorMeetingInfo.setCreatedBy(userId);
				vendorMeetingInfo.setCreatedOn(new Date());
				session.save(vendorMeetingInfo);
				SendEmail sendEmail = new SendEmail();
				logger.info("Transaction commited.");

				session.getTransaction().commit();
				sendEmail.sendloginCredentials(vendorMaster.getId(),
						appDetails, url, VendorStatus.BPSUPPLIER.getIndex());
				result = "success";
			} catch (HibernateException ex) {
				// ex.printStackTrace();
				PrintExceptionInLogFile.printException(ex);
				session.getTransaction().rollback();
				ex.printStackTrace();
				return "failure";
			} catch (Exception e) {
				PrintExceptionInLogFile.printException(e);
				e.printStackTrace();
				return "failure";
			} finally {
				if (session.isConnected()) {
					session.close();
				}
			}
		} else {
			return emailExist;
		}
		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorMasterBean> listVendorsDetails(UserDetailsDto appDetails,
			Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorMasterBean> reportDtos = new ArrayList<VendorMasterBean>();

		try {
			String query = "SELECT customer_vendormaster.VENDORNAME, "
					+ "customer_vendormaster.DUNSNUMBER,   "
					+ "customer_vendormaster.YEAROFESTABLISHMENT,   "
					+ " case "
					+ " when customer_vendormaster.DIVSERSUPPLIER=1 then 'Yes' "
					+ "  else "
					+ "   'No' "
					+ "end DIVSERSUPPLIER  ,   "
					+ " customer_vendormaster.NUMBEROFEMPLOYEES,   "
					+ "case "
					+ " when customer_vendormaster.PRIMENONPRIMEVENDOR=1 then 'Yes' "
					+ " when customer_vendormaster.PRIMENONPRIMEVENDOR=2 then 'No' "
					+ "  end primevendor, "
					+ " (select name from customer_legal_structure where id = customer_vendormaster.COMPANY_TYPE)companytype,  "
					+ " customer_vendormaster.TAXID,   "
					+ "customer_vendormaster.COMPANYINFORMATION,   "
					+ " (select typename from customer_businesstype where id=customer_vendormaster.BUSINESSTYPE)businesstype,   "
					+ "   case "
					+ "  when customer_vendormaster.COMPANYOWNERSHIP=1 then 'Publicly Traded' "
					+ "  when customer_vendormaster.COMPANYOWNERSHIP=2 then 'Privately Owned' "
					+ "end CompanyOwnership,   "
					+ " customer_vendormaster.STATESALESTAXID,   "
					+ "  customer_vendormaster.STATEINCORPORATION, "
					+ "  customer_vendormaster.VENDORDESCRIPTION,   "
					+ " customer_vendormaster.WEBSITE,   "
					+ " customer_vendormaster.EMAILID, "
					+ " (select group_concat(concat(yearNumber,' - $',format(YearSales,2)) SEPARATOR ' / ')from customer_vendor_sales"
					+ " where vendorid=customer_vendormaster.id)revenue "
					+ "  FROM customer_vendormaster  " + " where id="
					+ vendorId + " ";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				VendorMasterBean reportDto = new VendorMasterBean();

				reportDto.setVendorName(rs.getString("VENDORNAME"));
				reportDto.setDunsNumber(rs.getString("DUNSNUMBER"));
				reportDto.setYearOfEstablishment(rs
						.getString("YEAROFESTABLISHMENT"));
				reportDto.setDeverseSupplier(rs.getString("DIVSERSUPPLIER"));
				reportDto.setNumberOfEmployees(rs
						.getString("NUMBEROFEMPLOYEES"));
				reportDto.setPrime((rs.getString("primevendor")));
				reportDto.setCompanytype(rs.getString("companytype"));
				reportDto.setTaxId(rs.getString("TAXID"));
				reportDto.setCompanyInformation(rs
						.getString("COMPANYINFORMATION"));
				reportDto.setBusinessType(rs.getString("businesstype"));
				reportDto.setCompanyOwnership(rs.getString("CompanyOwnership"));
				reportDto.setStateSalesTaxId(rs.getString("STATESALESTAXID"));
				reportDto.setStateIncorporation(rs
						.getString("STATEINCORPORATION"));
				reportDto.setVendorDescription(rs
						.getString("VENDORDESCRIPTION"));
				reportDto.setWebsite(rs.getString("WEBSITE"));
				reportDto.setEmailId(rs.getString("EMAILID"));
				reportDto.setAnnualTurnover(rs.getString("revenue"));
				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorAddressInfo> listVendorsAddressDetails(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorAddressInfo> reportDtos = new ArrayList<VendorAddressInfo>();

		try {
			String query = "select "
					+ " case "
					+ " when addresstype='p' then 'Physical' "
					+ " when addresstype='m' then 'Mailing' "
					+ " when addresstype='r' then 'Remit' "
					+ " end addresstype, "
					+ " address,city,(select statename from state where id=customer_vendoraddressmaster.state)state,"
					+ " (select countryname from country where country.id=customer_vendoraddressmaster.country)country, "
					+ " province,region,zipcode,fax,mobile,phone from customer_vendoraddressmaster "
					+ " where vendorid=" + vendorId + "";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				VendorAddressInfo reportDto = new VendorAddressInfo();

				reportDto.setAddresstype(rs.getString("addresstype"));
				reportDto.setAddress(rs.getString("address"));
				reportDto.setCity(rs.getString("city"));
				reportDto.setState(rs.getString("state"));
				reportDto.setCountry(rs.getString("country"));
				reportDto.setProvince(rs.getString("province"));
				reportDto.setRegion(rs.getString("region"));
				reportDto.setZipcode(rs.getString("zipcode"));
				reportDto.setFax(rs.getString("fax"));
				reportDto.setMobile(rs.getString("mobile"));
				reportDto.setPhone(rs.getString("phone"));

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorContactInfo> listVendorsContactInfo(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorContactInfo> reportDtos = new ArrayList<VendorContactInfo>();

		try {
			String query = "Select "
					+ " customer_vendor_users.FIRSTNAME, "
					+ "customer_vendor_users.LASTNAME, "
					+ "customer_vendor_users.DESIGNATION, "
					+ " customer_vendor_users.EMAILID, "
					+ " customer_vendor_users.MOBILE, customer_vendor_users.PHONENUMBER,"
					+ "(case "
					+ "  when customer_vendor_users.ISBUSINESSCONTACT=1 then 'Yes' "
					+ " else "
					+ "   'No' "
					+ " end)BusinessContact, "
					+ "(case when customer_vendor_users.ISPREPARER=1 then 'Yes' "
					+ " else "
					+ "  'No' "
					+ " end)Preparer "
					+ "From "
					+ " customer_vendor_users "
					+ " where (isbusinesscontact=1 or ispreparer=1) and vendorid="
					+ vendorId + " and isactive=1 ";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				VendorContactInfo reportDto = new VendorContactInfo();

				reportDto.setFirstName(rs.getString("FIRSTNAME"));
				reportDto.setLastName(rs.getString("LASTNAME"));
				reportDto.setDesignation(rs.getString("DESIGNATION"));
				reportDto.setContanctEmail(rs.getString("EMAILID"));
				reportDto.setContactMobile(rs.getString("MOBILE"));
				reportDto.setContactPhone(rs.getString("PHONENUMBER"));
				reportDto.setIsBusinessContact(rs.getString("BusinessContact"));
				reportDto.setIsPreparer(rs.getString("Preparer"));

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorOwnerDetails> listVendorsOwnerDetails(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorOwnerDetails> reportDtos = new ArrayList<VendorOwnerDetails>();

		try {
			String query = "Select "
					+ " customer_vendorowner.OWNERNAME, "
					+ " customer_vendorowner.TITLE, "
					+ " customer_vendorowner.EMAIL, "
					+ " customer_vendorowner.PHONE, "
					+ " customer_vendorowner.EXTENSION, "
					+ " customer_vendorowner.MOBILE, "
					+ "( case when customer_vendorowner.GENDER='M' then 'Male' "
					+ " when customer_vendorowner.GENDER='F' then 'Female' "
					+ " when customer_vendorowner.GENDER='T' then 'Transgender'"
					+ "end)Gender, "
					+ " customer_vendorowner.OWNERSHIPPERCENTAGE, "
					+ " ethnicity.Ethnicity, "
					+ " customer_vendorowner.isactive "
					+ " From "
					+ " customer_vendorowner Left Join "
					+ " ethnicity On customer_vendorowner.OWNERSETHINICITY = ethnicity.id "
					+ " where customer_vendorowner.OWNERNAME is not null and customer_vendorowner.isactive and customer_vendorowner.vendorid="
					+ vendorId + "";

			System.out.println("VendorDaoImpl @3244:" + query);
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Integer count = 1;
			while (rs.next()) {
				VendorOwnerDetails reportDto = new VendorOwnerDetails();

				reportDto.setOwnername(rs.getString("OWNERNAME"));
				reportDto.setTitle(rs.getString("TITLE"));
				reportDto.setEmail(rs.getString("EMAIL"));
				reportDto.setPhone(rs.getString("PHONE"));
				reportDto.setExtension(rs.getString("EXTENSION"));
				reportDto.setMobile(rs.getString("MOBILE"));
				reportDto.setGender(rs.getString("Gender"));
				reportDto.setOwnershippercentage(rs
						.getString("OWNERSHIPPERCENTAGE"));
				reportDto.setEthinicity(rs.getString("Ethnicity"));
				reportDto.setIsactive(rs.getString("isactive"));

				reportDto.setOwnerCount("Owner-" + count);
				count++;

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String vendorsDiverseClassification(UserDetailsDto appDetails,
			Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		String diverseCertificate = "";

		try {
			String query = "Select  group_concat(customer_certificatemaster.CERTIFICATENAME)diversecertificate "
					+ "	From "
					+ "  customer_vendordiverseclassifcation Left Join "
					+ "  customer_certificatemaster On customer_vendordiverseclassifcation.CERTMASTERID "
					+ "  = customer_certificatemaster.ID "
					+ " where customer_vendordiverseclassifcation.active =1  and vendorid="
					+ vendorId + "";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				CustomerDiverseClassification reportDto = new CustomerDiverseClassification();

				diverseCertificate = rs.getString("diversecertificate");

			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return diverseCertificate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorDiverseCertificate> listVendorsDivCertificate(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDiverseCertificate> reportDtos = new ArrayList<VendorDiverseCertificate>();

		try {
			String query = "Select "
					+ "customer_certificatemaster.CERTIFICATENAME, "
					+ " (select description from  customer_certificatetype where"
					+ " customer_certificatetype.id=customer_vendorcertificate.CERTIFICATETYPE)CERTIFICATETYPE, "
					+ " customer_vendorcertificate.CERTIFICATENUMBER, "
					+ "customer_vendorcertificate.EFFECTIVEDATE, "
					+ "customer_vendorcertificate.EXPIRYDATE, "
					+ "customer_certificateagencymaster.CERTAGENCYNAME, "
					+ "ethnicity.Ethnicity "
					+ "From "
					+ " customer_certificatemaster Right Join "
					+ "customer_vendorcertificate On customer_vendorcertificate.CERTMASTERID = "
					+ " customer_certificatemaster.ID Inner Join "
					+ " customer_certificateagencymaster On customer_vendorcertificate.CERTAGENCYID = "
					+ "customer_certificateagencymaster.ID Left Join "
					+ " ethnicity On customer_vendorcertificate.ETHNICITY_ID = ethnicity.id "
					+ "where customer_vendorcertificate.vendorid=" + vendorId
					+ " and customer_vendorcertificate.DIVERSER_QUALITY=1 "
					+ "order by customer_certificatemaster.CERTIFICATENAME";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				VendorDiverseCertificate reportDto = new VendorDiverseCertificate();

				reportDto.setCertiicateName(rs.getString("CERTIFICATENAME"));
				reportDto.setCertificateType(rs.getString("CERTIFICATETYPE"));
				reportDto.setCertificateNumber(rs
						.getString("CERTIFICATENUMBER"));
				reportDto.setEffectiveDate(rs.getString("EFFECTIVEDATE"));
				reportDto.setExpiryDate(rs.getString("EXPIRYDATE"));
				reportDto.setCertAgencyName(rs.getString("CERTAGENCYNAME"));
				reportDto.setEthinicity(rs.getString("Ethnicity"));

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<CustomerServiceAreaInfo> listVendorServiceArea(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<CustomerServiceAreaInfo> reportDtos = new ArrayList<CustomerServiceAreaInfo>();

		try {
			String query = "Select "
					+ " customer_servicearea.SERVICEAREANAME "
					+ "From "
					+ "customer_vendorservicearea Left Join "
					+ "customer_servicearea On customer_vendorservicearea.SERVICEAREAID = "
					+ "customer_servicearea.ID "
					+ "where customer_vendorservicearea.ISACTIVE=1 and customer_vendorservicearea.VENDORID="
					+ vendorId + "";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				CustomerServiceAreaInfo reportDto = new CustomerServiceAreaInfo();
				reportDto.setSerialNum(serialNum);
				reportDto.setServiceArea(rs.getString("SERVICEAREANAME"));
				reportDtos.add(reportDto);

				serialNum++;
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorBussinessArea> listVendorsBussinessArea(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorBussinessArea> reportDtos = new ArrayList<VendorBussinessArea>();

		try {
			String query = "Select "
					+ " customer_businessgroup.BUSINESSGROUPNAME, "
					+ " customer_businessarea.AREANAME "
					+ "From "
					+ "customer_businessarea Inner Join "
					+ "customer_businessgroup On customer_businessarea.BUSINESSGROUPID = "
					+ " customer_businessgroup.ID Inner Join "
					+ "vendor_businessarea On vendor_businessarea.BUSINESSAREAID = "
					+ " customer_businessarea.ID "
					+ "where vendor_businessarea.VENDORID=" + vendorId + " ";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				VendorBussinessArea reportDto = new VendorBussinessArea();

				reportDto.setBussinessGroupName(rs
						.getString("BUSINESSGROUPNAME"));
				reportDto.setAreaName(rs.getString("AREANAME"));
				reportDto.setSerialNum(serialNum);
				serialNum++;

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorBussinessBioDetails listVendorsBussinessBiography(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		VendorBussinessBioDetails reportDtos = new VendorBussinessBioDetails();
		List<VendorDetailsBean> bussinessList = new ArrayList<VendorDetailsBean>();
		List<VendorDetailsBean> bussinessSafetylist = new ArrayList<VendorDetailsBean>();

		try {
			String query = "Select "
					+ " customer_businessbiographysafety.description as questions, "
					+ " customer_vendorbusinessbiographysafety.BUSINESSSAFETY, "
					+ " customer_vendorbusinessbiographysafety.DESCRIPTION, "
					+ "  (case "
					+ " when customer_vendorbusinessbiographysafety.ANSWERTYPE='radio' then "
					+ "  case "
					+ " when customer_vendorbusinessbiographysafety.ANSWER=1 then 'Yes' "
					+ "  when customer_vendorbusinessbiographysafety.ANSWER=0 then 'No' "
					+ " when customer_vendorbusinessbiographysafety.ANSWER=2 then 'N/A' "
					+ "    end "
					+ " else "
					+ "  customer_vendorbusinessbiographysafety.ANSWER "
					+ " end) answers  "
					+ "From "
					+ " customer_businessbiographysafety Inner Join "
					+ "customer_vendorbusinessbiographysafety "
					+ " On customer_businessbiographysafety.questionnumber = "
					+ "customer_vendorbusinessbiographysafety.QUESTIONNUMBER And "
					+ " customer_businessbiographysafety.category = "
					+ " customer_vendorbusinessbiographysafety.BUSINESSSAFETY And "
					+ "customer_businessbiographysafety.sequencenumber = "
					+ "customer_vendorbusinessbiographysafety.SEQUENCE "
					+ "where  customer_vendorbusinessbiographysafety.vendorid="
					+ vendorId + " "
					+ "order by   customer_businessbiographysafety.category, "
					+ " customer_businessbiographysafety.questionnumber, "
					+ " customer_businessbiographysafety.sequencenumber";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setQuestions(rs.getString("questions"));
				reportDto.setBussinessSafety(rs.getString("BUSINESSSAFETY"));
				reportDto.setDescription(rs.getString("DESCRIPTION"));
				reportDto.setAnswers(rs.getString("answers"));

				reportDto.setSerialNum(serialNum);
				serialNum++;
				if (reportDto.getBussinessSafety().equalsIgnoreCase("b"))
					bussinessList.add(reportDto);
				else
					bussinessSafetylist.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		reportDtos.setBussinessList(bussinessList);
		reportDtos.setBussinessSafetylist(bussinessSafetylist);
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorDetailsBean> listVendorsReference(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDetailsBean> reportDtos = new ArrayList<VendorDetailsBean>();

		try {
			String query = "Select "
					+ "customer_vendorreference.REFERENCEADDRESS, "
					+ "customer_vendorreference.REFERENCECITY, "
					+ "(select statename from state where id=customer_vendorreference.REFERENCESTATE)statename, "
					+ "(select countryname from country where id=customer_vendorreference.REFERENCECOUNTRY)countryname, "
					+ "customer_vendorreference.REFERENCEEMAILID, "
					+ "customer_vendorreference.REFERENCEMOBILE, "
					+ "customer_vendorreference.REFERENCENAME, "
					+ "customer_vendorreference.REFERENCEPHONE, "
					+ "customer_vendorreference.REFERENCEEXTENSION, "
					+ "customer_vendorreference.REFERENCEZIP, customer_vendorreference.REFERENCECOMPANYNAME, "
					+ "customer_vendorreference.REFERENCEPROVINCE " + "From "
					+ "customer_vendorreference " + "where vendorid="
					+ vendorId + " ";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Integer count = 1;
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setRefAddress(rs.getString("REFERENCEADDRESS"));
				reportDto.setRefCity(rs.getString("REFERENCECITY"));
				reportDto.setStateName(rs.getString("statename"));
				reportDto.setCountryName(rs.getString("countryname"));
				reportDto.setRefEmailId(rs.getString("REFERENCEEMAILID"));
				reportDto.setRefMobile(rs.getString("REFERENCEMOBILE"));
				reportDto.setRefName(rs.getString("REFERENCENAME"));
				reportDto.setRefPhone(rs.getString("REFERENCEPHONE"));
				reportDto.setRefExtn(rs.getString("REFERENCEEXTENSION"));
				reportDto.setRefZip(rs.getString("REFERENCEZIP"));
				reportDto.setRefCompanyName(rs
						.getString("REFERENCECOMPANYNAME"));
				reportDto.setRefCountyProvince(rs
						.getString("REFERENCEPROVINCE"));
				reportDto.setReferenceCount("Reference-" + count);
				count++;

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorDetailsBean> listVendorsOtherCertificates(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDetailsBean> reportDtos = new ArrayList<VendorDetailsBean>();

		try {
			String query = "Select "
					+ "customer_certificatemaster.CERTIFICATENAME, "
					+ "customer_certificatemaster.CERTIFICATESHORTNAME, "
					+ "customer_vendorothercertificate.EXPIRYDATE "
					+ "From "
					+ "customer_vendorothercertificate Inner Join "
					+ "customer_certificatemaster "
					+ " On customer_vendorothercertificate.CERTIFICATIONTYPE = "
					+ " customer_certificatemaster.ID "
					+ "where customer_vendorothercertificate.vendorid="
					+ vendorId + " and"
					+ " customer_certificatemaster.DIVERSE_QUALITY=0";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setCertiicateName(rs.getString("CERTIFICATENAME"));
				reportDto.setCertificateShortName(rs
						.getString("CERTIFICATESHORTNAME"));
				reportDto.setExpiryDate(rs.getString("EXPIRYDATE"));
				reportDto.setSerialNum(serialNum);
				serialNum++;

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorDetailsBean> listVendorsMeetingInfo(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDetailsBean> reportDtos = new ArrayList<VendorDetailsBean>();

		try {
			String query = "Select "
					+ "customer_vendormeetinginfo.CONTACTFIRSTNAME, "
					+ "customer_vendormeetinginfo.CONTACTLASTNAME, "
					+ "customer_vendormeetinginfo.CONTACTDATE, "
					+ "customer_vendormeetinginfo.CONTACTEVENT, "
					+ "state.STATENAME "
					+ "From customer_vendormeetinginfo inner join state "
					+ "where customer_vendormeetinginfo.CONTACTSTATE = state.ID and vendorid="
					+ vendorId + "";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setFirstName(rs.getString("CONTACTFIRSTNAME"));
				reportDto.setLastName(rs.getString("CONTACTLASTNAME"));
				reportDto.setContactDate(rs.getString("CONTACTDATE"));
				reportDto.setContactEvent(rs.getString("CONTACTEVENT"));
				reportDto.setStateName(rs.getString("STATENAME"));

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getVendorName(UserDetailsDto appDetails, int vendorId) {
		String vendorName = null;
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		try {
			String query = "select VENDORNAME from customer_vendormaster where ID="
					+ vendorId;
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				vendorName = rs.getString("VENDORNAME");
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorName;
	}

	@SuppressWarnings("unchecked")
	public String populateCheckList(UserDetailsDto appDetails,
			Integer divisionId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		String checkList = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("Select checkList from CustomerDivision where id="
							+ divisionId);
			checkList = query.uniqueResult().toString();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return checkList;

	}

	@SuppressWarnings({ "deprecation", "finally" })
	public String populateCertificationAgencies(UserDetailsDto appDetails,
			Integer divisionId) {

		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		String certificationAgencies = "";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("Select certificationAgencies from CustomerDivision where id="
							+ divisionId);
			certificationAgencies = query.uniqueResult().toString();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
			return certificationAgencies;
		}
	}

	public String updatePrimeVendor(VendorMasterForm vendorForm,
			Integer userId, UserDetailsDto appDetails) {
		String result = "updatesuccess";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			VendorMaster vendorMaster = (VendorMaster) session.createQuery(
					" From VendorMaster where id='" + vendorForm.getId() + "'")
					.uniqueResult();
			if (vendorMaster == null) {
				vendorMaster = new VendorMaster();
			}

			// Save VendorMaster Company Information
			vendorMaster.setParentVendorId(0);
			vendorMaster.setVendorName(vendorForm.getVendorName());

			vendorMaster.setNumberOfEmployees(Integer.parseInt("0"));
			vendorMaster.setAnnualTurnover(CommonUtils.deformatMoney("0"));

			vendorMaster.setPrimeNonPrimeVendor(CommonUtils.getByteValue(true));
			vendorMaster.setIsApproved((byte) 1);
			vendorMaster.setApprovedOn(new Date());
			vendorMaster.setEmailId(vendorForm.getEmailId());
			vendorMaster.setDeverseSupplier((byte) 0);

			// approval process

			vendorMaster.setIsinvited((short) 1);
			vendorMaster.setCreatedBy(userId);
			vendorMaster.setCreatedOn(new Date());
			vendorMaster.setModifiedBy(userId);
			vendorMaster.setModifiedOn(new Date());
			if (vendorMaster.getId() != null) {
				vendorMaster.setVendorCode(vendorMaster.getId().toString());
			}

			if (vendorForm.getVendorStatus().equalsIgnoreCase(
					VendorStatus.INACTIVE.getIndex())) {
				vendorMaster.setIsActive((byte) 0);
				vendorMaster.setVendorStatus(VendorStatus.INACTIVE.getIndex());
			} else {
				vendorMaster.setIsActive((byte) 1);
				vendorMaster
						.setVendorStatus(VendorStatus.BPSUPPLIER.getIndex());
			}

			session.update(vendorMaster);

			// Save Customer Division For the users
			if (vendorForm.getCustomerDivision() != null) {
				CustomerDivision customerDivision = (CustomerDivision) session
						.get(CustomerDivision.class,
								vendorForm.getCustomerDivision());
				vendorMaster.setCustomerDivisionId(customerDivision);
			}
			session.merge(vendorMaster);

			CustomerVendorAddressMaster addressMaster = (CustomerVendorAddressMaster) session
					.createQuery(
							" From CustomerVendorAddressMaster where vendorId='"
									+ vendorForm.getId()
									+ "'  and addressType='p'").uniqueResult();
			if (addressMaster == null) {
				addressMaster = new CustomerVendorAddressMaster();
			}
			addressMaster.setVendorId(vendorMaster);
			addressMaster.setAddressType("p");
			addressMaster.setAddress(vendorForm.getAddress1());
			addressMaster.setCity(vendorForm.getCity());
			addressMaster.setCountry(vendorForm.getCountry());
			addressMaster.setState(vendorForm.getState());
			addressMaster.setProvince("");
			addressMaster.setPhone(vendorForm.getPhone());
			addressMaster.setZipCode(vendorForm.getZipcode());
			addressMaster.setFax(vendorForm.getFax());
			addressMaster.setIsActive((byte) 1);
			addressMaster.setCreatedBy(userId);
			addressMaster.setCreatedOn(new Date());
			session.update(addressMaster);

			// Save VendorContact Information
			VendorContact vendorContact = (VendorContact) session
					.createQuery(
							"From VendorContact where (isPrimaryContact=1 or isPreparer=1) and isActive=1 and vendorId='"
									+ vendorForm.getId() + "'").uniqueResult();
			VendorRoles roles = (VendorRoles) session
					.createCriteria(VendorRoles.class)
					.add(Restrictions.eq("isActive", (byte) 1)).list().get(0);

			vendorContact.setVendorId(vendorMaster);
			vendorContact.setVendorUserRoleId(roles);

			Random random = new java.util.Random();
			Integer randVal = random.nextInt();
			Encrypt encrypt = new Encrypt();
			String psw = String.valueOf(PasswordUtil
					.generatePswd(8, 8, 1, 1, 0));
			// convert password to encrypted password
			String encyppassword = encrypt.encryptText(
					Integer.toString(randVal) + "", psw);
			vendorContact.setFirstName(vendorForm.getFirstName());
			// vendorContact.setLastName(lastName);
			vendorContact.setPhoneNumber(vendorForm.getContactPhone());
			vendorContact.setDesignation(vendorForm.getDesignation());
			vendorContact.setEmailId(vendorForm.getContanctEmail());
			vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(true));
			vendorContact.setIsPrimaryContact((byte) 1);
			vendorContact.setPassword(encyppassword);
			vendorContact.setKeyValue(randVal);
			vendorContact.setGender(vendorForm.getGender());
			vendorContact.setIsSysGenPassword((byte) 0);

			SecretQuestion secretQns = new SecretQuestion();
			secretQns = (SecretQuestion) session.get(SecretQuestion.class, 1);

			vendorContact.setSecretQuestionId(secretQns);
			vendorContact.setSecQueAns("");
			vendorContact.setIsActive((byte) 1);
			vendorContact.setCreatedBy(userId);
			vendorContact.setCreatedOn(new Date());
			vendorContact.setModifiedBy(userId);
			vendorContact.setModifiedOn(new Date());
			vendorContact.setIsBusinessContact((byte) 0);
			vendorContact.setIsPreparer((byte) 1);
			session.update(vendorContact);

			CustomerVendorMeetingInfo vendorMeetingInfo = new CustomerVendorMeetingInfo();
			CustomerVendorMeetingInfo vendorMeetingInfo2 = (CustomerVendorMeetingInfo) session
					.createQuery(
							" From CustomerVendorMeetingInfo where vendorId='"
									+ vendorForm.getId() + "'").uniqueResult();
			if (vendorMeetingInfo2 != null) {
				vendorMeetingInfo = vendorMeetingInfo2;
			}

			vendorMeetingInfo.setVendorId(vendorMaster);
			vendorMeetingInfo.setContactFirstName(vendorForm
					.getContactFirstName());
			vendorMeetingInfo.setContactLastName(vendorForm
					.getContactLastName());
			vendorMeetingInfo.setContactEvent(vendorForm.getContactEvent());
			vendorMeetingInfo.setContactDate(CommonUtils
					.dateConvertValue(vendorForm.getContactDate()));
			vendorMeetingInfo.setContactState(vendorForm.getContactState());
			vendorMeetingInfo.setCreatedBy(userId);
			vendorMeetingInfo.setCreatedOn(new Date());
			session.saveOrUpdate(vendorMeetingInfo);

			session.getTransaction().commit();

			result = "success";
		} catch (HibernateException ex) {
			// ex.printStackTrace();
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			ex.printStackTrace();
			return "updatefailure";
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
			e.printStackTrace();
			return "updatefailure";
		} finally {
			if (session.isConnected()) {
				session.close();
			}
		}

		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorDetailsBean> listBusinessAreaCommodityInfo(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDetailsBean> reportDtos = new ArrayList<VendorDetailsBean>();

		try {
			String query = "SELECT IFNULL(customer_marketsector.SECTORDESCRIPTION,'') AS BP_Market_Sector, "
					+ "IFNULL(customer_commoditycategory.CATEGORYDESCRIPTION,'') AS BP_Market_SubSector, "
					+ "IFNULL(customer_commodity.COMMODITYDESCRIPTION,'') AS BP_Commodity_Description "
					+ "FROM customer_vendorcommodity INNER JOIN customer_commodity ON customer_vendorcommodity.COMMODITYID=customer_commodity.ID "
					+ "LEFT JOIN customer_commoditycategory ON customer_commodity.COMMODITYCATEGORYID=customer_commoditycategory.ID "
					+ "LEFT JOIN customer_marketsector ON customer_marketsector.ID=customer_commoditycategory.MARKETSECTORID "
					+ "WHERE customer_vendorcommodity.VENDORID="
					+ vendorId
					+ " AND customer_vendorcommodity.isactive=1 "
					+ "ORDER BY customer_commoditycategory.CATEGORYDESCRIPTION, customer_commodity.COMMODITYDESCRIPTION";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setSectorDesc(rs.getString("BP_Market_Sector"));
				reportDto.setCategoryDesc(rs.getString("BP_Market_SubSector"));
				reportDto.setCommoditydesc(rs
						.getString("BP_Commodity_Description"));
				reportDto.setSerialNum(serialNum);

				reportDtos.add(reportDto);
				serialNum++;
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorDetailsBean> listServiceAreaNaicsInfo(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDetailsBean> reportDtos = new ArrayList<VendorDetailsBean>();

		try {
			String query = "Select "
					+ " customer_naicscode.NAICSCODE, "
					+ "customer_naicscode.NAICSDESCRIPTION, "
					+ "customer_vendornaics.CAPABILITIES, "
					+ "(case "
					+ " when customer_vendornaics.ISPRIMARYKEY=1 then 'Primay Naics Code' "
					+ "else '' "
					+ "end)NAICS_Type "
					+ "from "
					+ "customer_naicscode Inner Join "
					+ "customer_vendornaics On customer_vendornaics.NAICSID =customer_naicscode.NAICSID "
					+ "where customer_vendornaics.vendorid=" + vendorId + "";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setNaicsCode(rs.getString("NAICSCODE"));
				reportDto.setNaicsdesc(rs.getString("NAICSDESCRIPTION"));
				reportDto.setCapabilities(rs.getString("CAPABILITIES"));
				reportDto.setNaicsType(rs.getString("NAICS_Type"));
				reportDto.setSerialNum(serialNum);

				reportDtos.add(reportDto);
				serialNum++;
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorStatusPojo> listVendorStatus(UserDetailsDto appDetails,
			Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorStatusPojo> reportDtos = new ArrayList<VendorStatusPojo>();

		try {
			String query = "select createdon as StatusDate,"
					+ "(case when status='B' then 'BP Vendor'"
					+ " when status='A' then 'Reviewed by Supplier Diversity'"
					+ " when status='P' then 'Pending Review' when status='N'"
					+ " then 'New Registration' end)status, description as Comments"
					+ " From vendor_status_log where vendorid=" + vendorId
					+ " order by createdon desc";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				VendorStatusPojo reportDto = new VendorStatusPojo();

				reportDto.setStatusDate(rs.getString("StatusDate"));
				reportDto.setStatus(rs.getString("status"));
				reportDto.setComments(rs.getString("Comments"));

				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("finally")
	@Override
	public String populateRegistrationRequest(UserDetailsDto appDetails,
			Integer divisionId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		String registrationRequest = "";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("Select registrationRequest from CustomerDivision where id="
							+ divisionId);
			registrationRequest = query.uniqueResult().toString();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
			return registrationRequest;
		}

	}

	@SuppressWarnings("finally")
	@Override
	public String populateRegistrationQuestions(UserDetailsDto appDetails,
			Integer divisionId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		String registrationQuestion = "";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("Select registrationQuestion from CustomerDivision where id="
							+ divisionId);
			registrationQuestion = query.uniqueResult().toString();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
			return registrationQuestion;
		}
	}

	@Override
	public String updateContact(UserDetailsDto appDetails,
			CustomerVendorSelfRegInfo selfRegInfo) {
		logger.info("Inside the Update Contact Method.");
		String result = "success";

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();

			CustomerVendorSelfRegInfo selfRegInfo1 = (CustomerVendorSelfRegInfo) session
					.createQuery(
							" From CustomerVendorSelfRegInfo where id="
									+ selfRegInfo.getId()).uniqueResult();
			selfRegInfo1.setCount(Integer.parseInt("1"));// Update 1 to not
															// allow same user
															// to go reset page
			session.update(selfRegInfo1);

			logger.info("Update Contact Transaction commited.");

			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			ex.printStackTrace();
			return "failure";
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				session.close();
			}
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	public CustomerSearchFilter saveSearchFilters(UserDetailsDto userDetails,
			String searchName, String searchType, String criteria,
			String sqlQuery, Integer userId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		CustomerSearchFilter searchFilter = null;
		try {

			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0) {
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {

						session.beginTransaction();
						CustomerDivision customerDivisionId = (CustomerDivision) session
								.createQuery(
										"From CustomerDivision where id='"
												+ userDetails
														.getCustomerDivisionIds()[i]
												+ "'").uniqueResult();

						searchFilter = new CustomerSearchFilter();
						searchFilter.setCriteria(criteria);
						searchFilter.setSearchDate(new Date());
						searchFilter.setSearchName(searchName);
						searchFilter.setSearchType(searchType);
						searchFilter.setSearchQuery(sqlQuery);
						searchFilter.setUserId(userId);
						searchFilter.setCustomerDivisionId(customerDivisionId);
						session.save(searchFilter);
						session.getTransaction().commit();
					}
				}
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return searchFilter;
	}

	@SuppressWarnings("deprecation")
	@Override
	public CustomerSearchFilter saveSearchFiltersInJsonForm(
			UserDetailsDto userDetails, String searchName, String searchType,
			String criteria, String sqlQuery, Integer userId,
			String criteriaJson) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		CustomerSearchFilter searchFilter = null;
		try {

			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0) {
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {

						session.beginTransaction();
						CustomerDivision customerDivisionId = (CustomerDivision) session
								.createQuery(
										"From CustomerDivision where id='"
												+ userDetails
														.getCustomerDivisionIds()[i]
												+ "'").uniqueResult();

						searchFilter = new CustomerSearchFilter();
						searchFilter.setCriteria(criteria);
						searchFilter.setSearchDate(new Date());
						searchFilter.setSearchName(searchName);
						searchFilter.setSearchType(searchType);
						searchFilter.setSearchQuery(sqlQuery);
						searchFilter.setUserId(userId);
						searchFilter.setCustomerDivisionId(customerDivisionId);
						searchFilter.setCriteriaJson(criteriaJson);
						session.save(searchFilter);
						session.getTransaction().commit();
					}
				}
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return searchFilter;
	}

	@SuppressWarnings("deprecation")
	@Override
	public CustomerSearchFilter findByFilterName(UserDetailsDto userDetails,
			String searchName, String searchType, Integer userId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		CustomerSearchFilter searchFilter = null;
		try {
			session.beginTransaction();

			StringBuilder query = new StringBuilder();
			query.append("From CustomerSearchFilter where searchName = '"
					+ searchName + "' and searchType = '" + searchType + "' "
					+ "and userId = " + userId);
			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					query.append(" and customerDivisionId in("
							+ divisionIds.toString() + ")");
				}
			}
			// else
			// query.append(" and customerDivisionId IS NULL");

			searchFilter = (CustomerSearchFilter) session.createQuery(
					query.toString()).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return searchFilter;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<VendorDetailsBean> listServiceAreaKeywordsInfo(
			UserDetailsDto appDetails, Integer vendorId) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<VendorDetailsBean> reportDtos = new ArrayList<VendorDetailsBean>();

		try {
			String query = "SELECT KEYWORD FROM customer_vendorkeywords WHERE VENDORID = "
					+ vendorId;

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int serialNum = 1;
			while (rs.next()) {
				VendorDetailsBean reportDto = new VendorDetailsBean();

				reportDto.setKeyword(rs.getString("KEYWORD"));
				reportDto.setSerialNum(serialNum);

				reportDtos.add(reportDto);
				serialNum++;
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@Override
	public int deleteSearchFilter(UserDetailsDto userDetails, String searchId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		int result = 0;
		try {
			session.beginTransaction();
			result = session.createQuery(
					"Delete From CustomerSearchFilter where id=" + searchId)
					.executeUpdate();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public List<MeetingNotesDto> setMeetingInfo_To_Dto(
			UserDetailsDto userDetails, String query) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		MeetingNotesDto meetingDto = null;
		List<MeetingNotesDto> meetingDtoList = new ArrayList<MeetingNotesDto>();
		try {
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			List list = sqlQuery.list();
			if (list != null && list.size() > 0) {
				Iterator itr = list.iterator();
				while (itr.hasNext()) {
					meetingDto = new MeetingNotesDto();
					Object[] obj = (Object[]) itr.next();
					meetingDto.setMeetingNotesId(Integer.parseInt(obj[0]
							.toString()));
					meetingDto.setVendorId(Integer.parseInt(obj[1].toString()));
					meetingDto.setNotes(obj[2].toString());
					meetingDto.setCreatedBy(obj[3].toString());
					meetingDto.setCreatedOn(obj[4].toString());

					meetingDtoList.add(meetingDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return meetingDtoList;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<RFIRPFNotesDto> setRfiMeetingInfo_To_Dto(
			UserDetailsDto userDetails, String query) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		RFIRPFNotesDto rfiMeetingDto = null;
		List<RFIRPFNotesDto> rfimeetingDtoList = new ArrayList<RFIRPFNotesDto>();
		try {
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			List list = sqlQuery.list();
			if (list != null && list.size() > 0) {
				Iterator itr = list.iterator();
				while (itr.hasNext()) {
					rfiMeetingDto = new RFIRPFNotesDto();
					Object[] obj = (Object[]) itr.next();
					rfiMeetingDto.setRfiNotesId(Integer.parseInt(obj[0]
							.toString()));

					rfiMeetingDto.setVendorId(Integer.parseInt(obj[1].toString()));
					rfiMeetingDto.setNotes(obj[2].toString());
					rfiMeetingDto.setCreatedBy(obj[3].toString());
					rfiMeetingDto.setCreatedOn(obj[4].toString());

					rfimeetingDtoList.add(rfiMeetingDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return rfimeetingDtoList;
	}	
	
	@Override
	public MeetingNotes editMeetingNotes(UserDetailsDto userDetails,
			Integer vendorNotesId) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		MeetingNotes meetingNotes = null;
		try {
			session.beginTransaction();
			meetingNotes = (MeetingNotes) session.get(MeetingNotes.class,
					vendorNotesId);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return meetingNotes;
	}
	
	@Override
	public RFIRPFNotes editRfiMeetingNotes(UserDetailsDto userDetails,
			Integer vendorRfiNotesId) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		RFIRPFNotes rfiMeetingNotes = null;
		try {
			session.beginTransaction();
			rfiMeetingNotes = (RFIRPFNotes) session.get(RFIRPFNotes.class,
					vendorRfiNotesId);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return rfiMeetingNotes;
	}

	@Override
	public String save_Or_Update_MeetingNotes(UserDetailsDto userDetails,
			MeetingNotesDto meetingDto) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		MeetingNotes meetingNotes = null;
		Integer vendorNotesId = null;
		VendorMaster vendorMaster = null;
		String result = "success";
		try {
			session.beginTransaction();
			if (meetingDto != null) {
				vendorNotesId = meetingDto.getMeetingNotesId();
				vendorMaster = (VendorMaster) session.get(VendorMaster.class,
						meetingDto.getVendorId());
			}
			if (vendorNotesId != null && vendorNotesId != 0) {
				meetingNotes = (MeetingNotes) session.get(MeetingNotes.class,
						vendorNotesId);
				meetingNotes.setNotes(meetingDto.getNotes());
				session.update(meetingNotes);
				result = "update";
			} else {
				meetingNotes = new MeetingNotes();

				meetingNotes.setVendorId(vendorMaster);
				meetingNotes.setNotes(meetingDto.getNotes());
				meetingNotes.setCreatedBy(Integer.parseInt(meetingDto
						.getCreatedBy()));
				meetingNotes.setCreatedOn(new Date());
				session.save(meetingNotes);

			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String save_Or_Update_RfiMeetingNotes(UserDetailsDto userDetails,
			RFIRPFNotesDto rfimeetingDto) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		RFIRPFNotes rfiMeetingNotes = null;
		Integer vendorRfiNotesId = null;
		VendorMaster vendorMaster = null;
		String result = "success";
		try {
			session.beginTransaction();
			if (rfimeetingDto != null) {
				vendorRfiNotesId = rfimeetingDto.getRfiNotesId();
				vendorMaster = (VendorMaster) session.get(VendorMaster.class,
						rfimeetingDto.getVendorId());
			}
			if (vendorRfiNotesId != null && vendorRfiNotesId != 0) {
				rfiMeetingNotes = (RFIRPFNotes) session.get(RFIRPFNotes.class,
						vendorRfiNotesId);
				rfiMeetingNotes.setNotes(rfimeetingDto.getNotes());
				session.update(rfiMeetingNotes);
				result = "update";
			} else {
				rfiMeetingNotes = new RFIRPFNotes();

				rfiMeetingNotes.setVendorId(vendorMaster);
				rfiMeetingNotes.setNotes(rfimeetingDto.getNotes());
				rfiMeetingNotes.setCreatedBy(Integer.parseInt(rfimeetingDto
						.getCreatedBy()));
				rfiMeetingNotes.setCreatedOn(new Date());
				session.save(rfiMeetingNotes);

			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	
	@Override
	public String deleteMeetingNote(UserDetailsDto userDetails,
			Integer vendorNotesId) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		MeetingNotes meetingNotes = null;
		String result = "deleted";
		try {
			session.beginTransaction();
			meetingNotes = (MeetingNotes) session.get(MeetingNotes.class,
					vendorNotesId);
			session.delete(meetingNotes);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	
	@Override
	public String deleteRfiMeetingNote(UserDetailsDto userDetails,
			Integer vendorRfiNotesId) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		RFIRPFNotes rfiNeetingNotes = null;
		String result = "deleted";
		try {
			session.beginTransaction();
			rfiNeetingNotes = (RFIRPFNotes) session.get(RFIRPFNotes.class,
					vendorRfiNotesId);
			session.delete(rfiNeetingNotes);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	public VendorMaster retrieveVendorById(UserDetailsDto userDetails,
			Integer vendorId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		VendorMaster vendorMaster = null;

		try {
			session.beginTransaction();
			vendorMaster = (VendorMaster) session.get(VendorMaster.class,
					vendorId);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorMaster;
	}

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Override
	public List<EmailHistoryDto> setEmailHistoryInfo_To_Dto(
			UserDetailsDto userDetails, String vendorPrimaryEmailId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		EmailHistoryDto emailHistoryDto = null;
		List<EmailHistoryDto> emailHistoryDtoList = new ArrayList<EmailHistoryDto>();

		try {
			session.beginTransaction();

			List list = session.createSQLQuery(
					"SELECT * FROM emailhistory e WHERE e.EMAILTO IN ("
							+ vendorPrimaryEmailId
							+ ") ORDER BY e.EMAILDATE DESC LIMIT 10").list();

			if (list != null && list.size() > 0) {
				Iterator itr = list.iterator();
				while (itr.hasNext()) {
					Object[] object = (Object[]) itr.next();
					emailHistoryDto = new EmailHistoryDto();

					emailHistoryDto.setEmailHistoryId(Integer
							.parseInt(object[0].toString()));
					emailHistoryDto.setEmailId(object[1].toString());
					emailHistoryDto.setAddressType(object[2].toString());
					emailHistoryDto.setEmailSubject(object[3].toString());
					emailHistoryDto.setEmailMessage(object[4].toString());
					emailHistoryDto.setEmailDate(object[5].toString());

					emailHistoryDtoList.add(emailHistoryDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return emailHistoryDtoList;
	}

	@SuppressWarnings("deprecation")
	public List<VendorMasterBean> retrieveVendorsList(UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<VendorMasterBean> vendorNamesList = new ArrayList<VendorMasterBean>();
		Iterator<?> iterator = null;

		try {
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT DISTINCT cvm.ID, cvm.VENDORNAME FROM customer_vendormaster cvm ");

			if (userDetails.getWorkflowConfiguration() != null
					&& userDetails.getWorkflowConfiguration()
							.getInternationalMode() != null
					&& userDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
				CountryStateDao countryStateDao = new CountryStateDaoImpl();
				Country defaultCountry = countryStateDao
						.listDefaultCountry(userDetails);

				if (defaultCountry != null && defaultCountry.getId() != null) {
					sqlQuery.append("INNER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' "
							+ "AND cvam.COUNTRY = "
							+ defaultCountry.getId()
							+ " ");
				}
			}

			sqlQuery.append("WHERE cvm.DIVSERSUPPLIER = 1 AND cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B','I') "
					+ "AND (cvm.PARENTVENDORID = 0 OR cvm.PARENTVENDORID IS NULL) ");

			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0
						&& userDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN ("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("ORDER BY cvm.VENDORNAME ASC");

			System.out.println("retrieveVendorsList(VendorDaoImpl @ 4895):"
					+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					VendorMasterBean vendorMasterBean = new VendorMasterBean();
					Object[] objects = (Object[]) iterator.next();

					vendorMasterBean.setVendorId(Integer.parseInt(objects[0]
							.toString()));
					vendorMasterBean.setVendorName(objects[1].toString());

					vendorNamesList.add(vendorMasterBean);
				}
			}
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendorNamesList;
	}

	@Override
	public String updateVendorMaster(UserDetailsDto userDetails,
			VendorMaster vendorMaster) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = null;
		try {
			session.beginTransaction();
			session.update(vendorMaster);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String saveSupplierDiversity(UserDetailsDto appDetails,
			SupplierDiversityForm supplierDiversityForm, Users users) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String result = "failure";
		SupplierDiversity supplierDiversity = new SupplierDiversity();
		if(supplierDiversityForm.getId() != null && supplierDiversityForm.getId() !=0){
			supplierDiversity.setId(supplierDiversityForm.getId());
		}
		supplierDiversity.setRequestorId(users);
		supplierDiversity.setCreatedOn(new Date());
		if(supplierDiversityForm.getBusiness()!=null){
		supplierDiversity.setBusiness(supplierDiversityForm.getBusiness());
		}
		
		supplierDiversity.setRequestedOn(CommonUtils
				.dateConvertValue(supplierDiversityForm.getRequestedOn()));
		supplierDiversity.setOppurtunityEventName(supplierDiversityForm
				.getOppurtunityEventName());
		supplierDiversity.setSourceEvent(CommonUtils
				.stringArrayToString(supplierDiversityForm.getSourceEvent()));
		supplierDiversity.setSourceEventStartDate(CommonUtils
				.dateConvertValue(supplierDiversityForm
						.getSourceEventStartDate()));
		supplierDiversity.setEstimateSpend(supplierDiversityForm
				.getEstimateSpend());
		supplierDiversity.setEstimateWorkStartDate(CommonUtils
				.dateConvertValue(supplierDiversityForm
						.getEstimateWorkStartDate()));
		supplierDiversity.setLastResponseDate(CommonUtils
				.dateConvertValue(supplierDiversityForm.getLastResponseDate()));
		
		if(supplierDiversityForm.getCommodityCode()!=null){
		supplierDiversity.setCommodityCode(CommonUtils
				.stringArrayToString(supplierDiversityForm.getCommodityCode()));
		}
		supplierDiversity.setScopOfWork(supplierDiversityForm.getScopOfWork());
		supplierDiversity.setGeography(supplierDiversityForm.getGeography());
		supplierDiversity.setMiniSupplierRequirement(supplierDiversityForm
				.getMiniSupplierRequirement());
		supplierDiversity.setContractInsight(supplierDiversityForm
				.getContractInsight());
		if(supplierDiversityForm.getIncumbentVendorId()!=null){
			
		supplierDiversity.setIncumbentVendorId(CommonUtils
				.integerArrayToString(supplierDiversityForm
						.getIncumbentVendorId()));
		}
		supplierDiversity.setIsActive(1);
		try {
			session.beginTransaction();
			session.saveOrUpdate(supplierDiversity);
			session.getTransaction().commit();
			if(supplierDiversityForm.getId() != null){
				result = "updated";
			}else{
			result = "success";
			}
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
			result="failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<SupplierDiversityDto> retrieveSupplierDiversityList(
			UserDetailsDto appDetails , Integer userId) {
		Integer requestorID =0;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SupplierDiversityDto> SupplierDiversityDtoList = new ArrayList<SupplierDiversityDto>();
		if(userId!=null){
			requestorID = userId;
		}
		try {
			session.beginTransaction();

			StringBuilder searchQuery = new StringBuilder();

			searchQuery
					.append("select u.FIRSTNAME, sd.REQUESTEDON, sd.OPPURTUNITYEVENTNAME,"
							+ " sd.SOURCEEVENT, sd.SOURCEEVENTSTARTDATE, sd.ESTIMATESPEND, sd.ESTIMATEWORKSTARTDATE, sd.BUSINESS, sd.ID"
							+ " from supplier_diversity sd INNER JOIN users u ON u.ID=sd.REQUESTORID where sd.ISACTIVE=1 and sd.REQUESTORID="+requestorID);

			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List supplierDiversitys = sqlQuery.list();
			Iterator iterator;

			if (supplierDiversitys != null) {
				iterator = supplierDiversitys.iterator();
				while (iterator.hasNext()) {
					Object[] supplierDiversity = (Object[]) iterator.next();
					String firstName;
					Date requestedOn;
					String oppurtunityEventName;
					String sourceEvent;
					Date sourceEventStartDate;
					Double estimateSpend;
					Date estimateWorkStartDate;
					String business;
					Integer supplierDiversityId;

					firstName = (String) supplierDiversity[0];
					requestedOn = (Date) supplierDiversity[1];
					oppurtunityEventName = (String) supplierDiversity[2];
					sourceEvent = (String) supplierDiversity[3];
					sourceEventStartDate = (Date) supplierDiversity[4];
					estimateSpend = (Double) supplierDiversity[5];
					estimateWorkStartDate = (Date) supplierDiversity[6];
					business = (String) supplierDiversity[7];
					supplierDiversityId = (Integer) supplierDiversity[8];
					SupplierDiversityDto supplierDiversityDto = new SupplierDiversityDto(supplierDiversityId,
							firstName, CommonUtils.convertDateTimeToStringFormated(requestedOn), oppurtunityEventName,
							sourceEvent, CommonUtils.convertDateTimeToStringFormated(sourceEventStartDate), estimateSpend,
							CommonUtils.convertDateTimeToStringFormated(estimateWorkStartDate),business);
					SupplierDiversityDtoList.add(supplierDiversityDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return SupplierDiversityDtoList;
	}

	@Override
	public SupplierDiversity retrieveSupplierDiversity(
			UserDetailsDto userDetails, Integer supplierDiversityId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		SupplierDiversity supplierDiversity=null;
		try {
			session.beginTransaction();
			supplierDiversity = (SupplierDiversity) session.get(SupplierDiversity.class,supplierDiversityId);
			session.getTransaction().commit();
			
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return supplierDiversity;
	}

	@Override
	public String deleteSupplierDiversity(
			UserDetailsDto userDetails, SupplierDiversity supplierDiversity) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = null;
		supplierDiversity.setIsActive(0);
		try {
			session.beginTransaction();
			session.saveOrUpdate(supplierDiversity);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	

	@Override
	public JSONArray retrieveCommoditySearchList(UserDetailsDto userDetails, String commoditySearchValue) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		JSONArray jarray=new JSONArray();
		
		try {
			session.beginTransaction();
			StringBuilder searchQuery = new StringBuilder();
			if(commoditySearchValue!=null &&!commoditySearchValue.equals("")){
			searchQuery
			.append(" select customer_commoditycategory.id,customer_commoditycategory.CATEGORYDESCRIPTION, customer_marketsector.ID, "+
					" customer_marketsector.SECTORDESCRIPTION from  customer_commoditycategory, customer_marketsector "+
					" where customer_commoditycategory.MARKETSECTORID=customer_marketsector.ID and customer_commoditycategory.ISACTIVE=1 and customer_marketsector.ISACTIVE=1 "+
					" and (customer_commoditycategory.CATEGORYDESCRIPTION like '%"+commoditySearchValue+"%' or customer_marketsector.SECTORDESCRIPTION  like '%"+commoditySearchValue+"%') "+
					" order by customer_commoditycategory.CATEGORYDESCRIPTION,customer_marketsector.SECTORDESCRIPTION ") ;
			}else{
				
				searchQuery
				.append(" select customer_commoditycategory.id,customer_commoditycategory.CATEGORYDESCRIPTION, customer_marketsector.ID, "+
						" customer_marketsector.SECTORDESCRIPTION from  customer_commoditycategory, customer_marketsector "+
						" where customer_commoditycategory.MARKETSECTORID=customer_marketsector.ID and customer_commoditycategory.ISACTIVE=1 and customer_marketsector.ISACTIVE=1 "+
						" order by customer_commoditycategory.CATEGORYDESCRIPTION,customer_marketsector.SECTORDESCRIPTION ") ;
				
			}
			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List cuommodityList= sqlQuery.list();
			Iterator iterator;
			
			JSONObject jObject = null;
			
			if (cuommodityList != null) {
				iterator = cuommodityList.iterator();
				while (iterator.hasNext()) {
					Object[] cuommodityDetails = (Object[]) iterator.next();
					
					jObject=new JSONObject();
					jObject.put("commodityId",  cuommodityDetails[0]  );
					jObject.put("commodityDesc", cuommodityDetails[1]  +" | "+ cuommodityDetails[3]);
					jarray.add(jObject);
				}
				
			}
			session.getTransaction().commit();
			//result = "success";
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		
		return jarray;
	}

	@Override
	public JSONArray retrieveVendorList(UserDetailsDto userDetails, String commodityCode) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		JSONArray jarray=new JSONArray();
		
		try {
			session.beginTransaction();
			StringBuilder searchQuery = new StringBuilder();
			searchQuery
			
			.append(" select distinct customer_vendormaster.ID,customer_vendormaster.VENDORNAME,customer_commodity.COMMODITYCATEGORYID "+
					" from customer_vendormaster,customer_vendorcommodity,customer_commodity "+
					" where customer_vendormaster.ID=customer_vendorcommodity.VENDORID and customer_commodity.ID=customer_vendorcommodity.COMMODITYID and "+
					" customer_vendorcommodity.ISACTIVE=1 and customer_vendormaster.ISACTIVE=1 and customer_vendormaster.DIVSERSUPPLIER=1 and "+
					" customer_commodity.COMMODITYCATEGORYID in ('"+commodityCode+"') "+
					" order by customer_vendormaster.VENDORNAME ") ;
			
			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List vendorList= sqlQuery.list();
			Iterator iterator;
			
			JSONObject jObject = null;
			
			if (vendorList != null) {
				iterator = vendorList.iterator();
				while (iterator.hasNext()) {
					Object[] vendorDetails = (Object[]) iterator.next();
					
					jObject=new JSONObject();
					jObject.put("vendorId",  vendorDetails[0] );
					jObject.put("VendorName", vendorDetails[1] );
					jarray.add(jObject);
				}
				
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		
		return jarray;
	}

	@Override
	public JSONArray retrieveCommodityCodeData(UserDetailsDto appDetails, Integer id) {
	
	 Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
	 JSONArray jarray=new JSONArray();
		
		try {
			session.beginTransaction();
			StringBuilder searchQuery = new StringBuilder();
			searchQuery
			
			.append(" select distinct customer_vendormaster.ID,customer_vendormaster.VENDORNAME,customer_commodity.COMMODITYCATEGORYID "+
					" from customer_vendormaster,customer_vendorcommodity,customer_commodity "+
					" where customer_vendormaster.ID=customer_vendorcommodity.VENDORID and customer_commodity.ID=customer_vendorcommodity.COMMODITYID and "+
					" customer_vendorcommodity.ISACTIVE=1 and customer_vendormaster.ISACTIVE=1 and customer_vendormaster.DIVSERSUPPLIER=1 and "+
					" customer_commodity.COMMODITYCATEGORYID in ('"+id+"') "+
					" order by customer_vendormaster.VENDORNAME ") ;
			
			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List vendorList= sqlQuery.list();
			Iterator iterator;
			
			JSONObject jObject = null;
			
			if (vendorList != null) {
				iterator = vendorList.iterator();
				while (iterator.hasNext()) {
					Object[] vendorDetails = (Object[]) iterator.next();
					
					jObject=new JSONObject();
					jObject.put("vendorId",  vendorDetails[0] );
					jObject.put("VendorName", vendorDetails[1] );
					jarray.add(jObject);
				}
				
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		
		return jarray;
	}
	
	@Override
	public Map<String, String> commodityUpdateSearchList(UserDetailsDto appDetails, String[] commodityData) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		//JSONArray jarray=new JSONArray();
		//List<String>  commandList= new ArrayList<String>();
		Map<String,String> commandList= new HashMap<String,String>();
		String cd=null;
		
		for (int i = 0; i < commodityData.length; i++) {
			 cd +="'"+ commodityData[i]+"',";
		}
		
		String command=cd.substring(4, cd.length()-1);
		
		try {
			session.beginTransaction();
			StringBuilder searchQuery = new StringBuilder();
			searchQuery
			.append(" select customer_commoditycategory.id,customer_commoditycategory.CATEGORYDESCRIPTION, customer_marketsector.ID, "+
					" customer_marketsector.SECTORDESCRIPTION from  customer_commoditycategory, customer_marketsector "+
					" where customer_commoditycategory.MARKETSECTORID=customer_marketsector.ID and customer_commoditycategory.ISACTIVE=1 and customer_marketsector.ISACTIVE=1 "+
					" and customer_commoditycategory.id in ("+command+") "+
					" order by customer_commoditycategory.CATEGORYDESCRIPTION,customer_marketsector.SECTORDESCRIPTION ") ;
			
			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List cuommodityList= sqlQuery.list();
			Iterator iterator;
			
			JSONObject jObject = null;
			
			if (cuommodityList != null) {
				iterator = cuommodityList.iterator();
				while (iterator.hasNext()) {
					Object[] cuommodityDetails = (Object[]) iterator.next();
					
					commandList.put(cuommodityDetails[0].toString(), cuommodityDetails[1]+"|"+cuommodityDetails[3]);
				//	commandList.add(cuommodityDetails[0].toString());
				//	commandList.add(cuommodityDetails[1]+"|"+cuommodityDetails[3]);

				}
				
			}
			session.getTransaction().commit();
			//result = "success";
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		
		return commandList;
	}

	@Override
	public Map<String, String> getVendorList(UserDetailsDto appDetails, String[] vendorData) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		Map<String,String> vendorListData= new HashMap<String,String>();
		String vd=null;
		
		for (int i = 0; i < vendorData.length; i++) {
			 vd +="'"+ vendorData[i]+"',";
		}
		
		String vendorId=vd.substring(4, vd.length()-1);
		
		try {
			session.beginTransaction();
			StringBuilder searchQuery = new StringBuilder();
			searchQuery
			.append(" select distinct customer_vendormaster.ID,customer_vendormaster.VENDORNAME "+
					" from customer_vendormaster,customer_vendorcommodity,customer_commodity "+
					" where customer_vendormaster.ID=customer_vendorcommodity.VENDORID and customer_commodity.ID=customer_vendorcommodity.COMMODITYID and "+ 
					" customer_vendorcommodity.ISACTIVE=1 and customer_vendormaster.ISACTIVE=1 and customer_vendormaster.DIVSERSUPPLIER=1 and  "+
					" customer_vendormaster.ID in ("+vendorId+") "+
					" order by customer_vendormaster.VENDORNAME") ;
			
			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List vendorList= sqlQuery.list();
			Iterator iterator;
			
			JSONObject jObject = null;
			
			if (vendorList != null) {
				iterator = vendorList.iterator();
				while (iterator.hasNext()) {
					Object[] vendorDetails = (Object[]) iterator.next();
					
					vendorListData.put(vendorDetails[0].toString(),  vendorDetails[1].toString());
				//	commandList.add(cuommodityDetails[0].toString());
				//	commandList.add(cuommodityDetails[1]+"|"+cuommodityDetails[3]);

				}
				
			}
			session.getTransaction().commit();
			//result = "success";
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		
		return vendorListData;
	}

		
	}
	
