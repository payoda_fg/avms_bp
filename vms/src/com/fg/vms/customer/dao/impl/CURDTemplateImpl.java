/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author gpirabu
 * 
 */
public class CURDTemplateImpl<T> implements CURDDao<T> {

	@Override
	public T save(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public T update(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();

			session.saveOrUpdate(entity);

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public T delete(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();

			session.delete(entity);

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public List<T> list(UserDetailsDto usDetails, String query) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<T> list = null;
		try {
			session.getTransaction().begin();
			list = session.createQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (List<T>) list;
	}

	@Override
	public T find(UserDetailsDto usDetails, String query) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		T object = null;

		try {
			session.getTransaction().begin();
			List<T> list = session.createQuery(query).list();
			if (list != null && !list.isEmpty()) {
				object = list.get(0);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return object;
	}

	@Override
	public T update1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T delete1(T entity, String name, UserDetailsDto usDetails) {
		
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();

			session.delete(entity);

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public List<?> findAllByQuery(UserDetailsDto userDetails, String query) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<?> list = null;
		try {
			session.beginTransaction();

			Query query2 = session.createQuery(query);
			list = query2.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;

	}

	@Override
	public List<Ethnicity> listEthnicities(UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> listCommoditiesBasedOnSubSector(UserDetailsDto usDetails,
			Integer marketSubSectorId) {
		// TODO Auto-generated method stub
		return null;
	}

}
