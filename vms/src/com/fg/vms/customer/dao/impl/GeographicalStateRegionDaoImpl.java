/**
 * GeographicalStateRegionDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.GeographicalStateRegionDao;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerBusinessGroup;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.Region;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.StateByRegion;
import com.fg.vms.customer.pojo.GeographicForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Implements the GeographicalStateRegionDao interface.
 * 
 * @author vinoth
 * 
 */
public class GeographicalStateRegionDaoImpl implements
		GeographicalStateRegionDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<State> listStates(UserDetailsDto appDetails) {

		List<State> states = null;

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			states = session.createQuery(
					"from State where isActive = 1 ORDER BY stateName ASC")
					.list();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return states;
	}

	/**
	 * This method helps in convert string array to string.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToString(String[] stringarray) {
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) {
			for (String st : stringarray) {
				sb.append(st.trim()).append(',');
			}
			if (stringarray.length != 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerBusinessGroup> listBusinessGroups(
			UserDetailsDto appDetails) {

		List<CustomerBusinessGroup> groups = null;

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			groups = session
					.createQuery(
							"from CustomerBusinessGroup where isActive = 1 ORDER BY businessGroupName ASC")
					.list();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return groups;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GeographicalStateRegionDto> listStateByRegions(
			UserDetailsDto appDetails, String query) {

		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<GeographicalStateRegionDto> stateRegionDtos = new ArrayList<GeographicalStateRegionDto>();

		session.beginTransaction();
		List<StateByRegion> stateByRegions = null;
		try {
			/* Fetch the list of active naics codes. */
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				GeographicalStateRegionDto stateRegionDto = new GeographicalStateRegionDto();
				stateRegionDto.setRegionId(Integer.parseInt(rs.getString(1)));
				stateRegionDto.setRegion(rs.getString(2));
				stateRegionDto.setStateId(Integer.parseInt(rs.getString(3)));
				stateRegionDtos.add(stateRegionDto);
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} catch (SQLException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return stateRegionDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Region> sizeByCode(UserDetailsDto appDetails, Integer regionId) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		session.beginTransaction();
		List<Region> byRegions = null;
		try {

			/* Fetch the list of active states under region codes. */

			byRegions = session.createQuery(
					"from Region n where n.isActive=1 and n.regionId="
							+ regionId).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return byRegions;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerBusinessGroup> retrieveBusinessGroup(Integer groupId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<CustomerBusinessGroup> groups = null;
		try {
			session.beginTransaction();
			/* Fetch the list of active business groups. */

			groups = session.createQuery(
					"from CustomerBusinessGroup where isActive=1 and id="
							+ groupId).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return groups;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteGroup(Integer groupId, Integer userId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		CustomerBusinessGroup group = null;
		try {
			session.beginTransaction();
			group = (CustomerBusinessGroup) session.get(
					CustomerBusinessGroup.class, groupId);
			group.setModifiedBy(userId);
			group.setModifiedOn(new Date());
			group.setIsActive((byte) 0);
			session.merge(group);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerBusinessArea> retrieveBusinessArea(Integer areaId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<CustomerBusinessArea> areas = null;
		try {
			session.beginTransaction();
			/* Fetch the list of active area under business group. */

			areas = session.createQuery(
					"from CustomerBusinessArea where isActive=1 and id="
							+ areaId).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return areas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteArea(Integer areaId, Integer userId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		CustomerBusinessArea businessArea = null;
		try {
			session.beginTransaction();
			businessArea = (CustomerBusinessArea) session.get(
					CustomerBusinessArea.class, areaId);
			businessArea.setModifiedBy(userId);
			businessArea.setModifiedOn(new Date());
			businessArea.setIsActive((byte) 0);
			session.merge(businessArea);

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State getStateName(Integer stateId, UserDetailsDto appDetails) {

		State state = null;

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			state = (State) session.createQuery(
					"from State where isActive = 1 and id=" + stateId)
					.uniqueResult();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return state;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GeographicalStateRegionDto> getStateByRegionList(
			UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<GeographicalStateRegionDto> regionDtos = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select BUSINESSGROUPNAME, AREANAME From customer_businessgroup bg, customer_businessarea ba"
					+ " Where bg.ID = ba.BUSINESSGROUPID And bg.ISACTIVE = 1 And ba.ISACTIVE = 1 Order by AREANAME ");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					dto.setRegion(objects[0].toString());
					dto.setState(objects[1].toString());
					regionDtos.add(dto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return regionDtos;
	}

	@Override
	public List<GeographicalStateRegionDto> getGeographicalRegionList(
			Integer vendorId, UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<GeographicalStateRegionDto> regionDtos = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("select (select GROUP_CONCAT(region.REGIONNAME)  from region,statebyregion where"
					+ "	region.ID= statebyregion.REGIONID and statebyregion.STATEID=s.ID and"
					+ " region.isactive=1 and statebyregion.ISACTIVE=1) regionname, S.stateName from  state s"
					+ " where s.isactive=1 and  s.ID NOT IN (Select stateId from customer_vendorservicearea Where vendorId = "
					+ vendorId + ") ");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setRegion(objects[0].toString());
					}
					if (objects[1] != null) {
						dto.setState(objects[1].toString());
					}
					regionDtos.add(dto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return regionDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveBusinessGroup(Integer userId,
			GeographicForm geographicForm, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();

			CustomerBusinessGroup group = new CustomerBusinessGroup();
			/* Save the region details. */

			log.info(" ============save business group is starting========== ");

			if (geographicForm.getRegionId() != null
					&& geographicForm.getRegionId() != 0) {
				CustomerBusinessGroup group1 = (CustomerBusinessGroup) session
						.get(CustomerBusinessGroup.class,
								geographicForm.getRegionId());
				group1.setBusinessGroupName(geographicForm.getRegion());
				group1.setIsActive((byte) 1);
				group1.setModifiedBy(userId);
				group1.setModifiedOn(new Date());
				session.merge(group1);
			} else {
				if(geographicForm.getIsActive().equalsIgnoreCase("no"))
				{
					CustomerBusinessGroup group1=(CustomerBusinessGroup)session.createQuery("From CustomerBusinessGroup where businessGroupName='" + 
																geographicForm.getRegion().toString() + "'").list().get(0);
					group1.setBusinessGroupName(geographicForm.getRegion());
					group1.setIsActive((byte) 1);
					group1.setModifiedBy(userId);
					group1.setModifiedOn(new Date());
					session.merge(group1);
				}
				else if (geographicForm.getRegion() != null
						&& !geographicForm.getRegion().isEmpty()) {
					group.setBusinessGroupName(geographicForm.getRegion());
					group.setIsActive((byte) 1);
					group.setCreatedBy(userId);
					group.setCreatedOn(new Date());
					session.save(group);
				}
			}

			log.info(" ============saved business group successfully========== ");
			session.getTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveServiceArea(Integer userId, GeographicForm geographicForm,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();

			if (geographicForm.getStateId() != null
					&& geographicForm.getStateId() != 0) {
				CustomerBusinessArea area = (CustomerBusinessArea) session
						.get(CustomerBusinessArea.class,
								geographicForm.getStateId());

				String temp = arrayToString(geographicForm.getRegions());
				if (geographicForm.getRegions() != null
						&& geographicForm.getRegions().length != 0
						&& !temp.isEmpty()) {

					for (int i = 0; i < temp.split(",").length; i++) {
						CustomerBusinessGroup group = (CustomerBusinessGroup) session
								.get(CustomerBusinessGroup.class,
										Integer.parseInt(temp.split(",")[i]));
						area.setGroupId(group);
						area.setAreaName(geographicForm.getState());
						area.setIsActive((byte) 1);
						area.setModifiedBy(userId);
						area.setModifiedOn(new Date());
						session.merge(area);
					}
				}
			} 
			else 
			{
				if(geographicForm.getIsActive().equalsIgnoreCase("no"))
				{
					CustomerBusinessArea area=(CustomerBusinessArea)session.createQuery("From CustomerBusinessArea where areaName='"+geographicForm.getState().toString()+"'").list().get(0);
					String temp = arrayToString(geographicForm.getRegions());
					if (geographicForm.getRegions() != null
							&& geographicForm.getRegions().length != 0
							&& !temp.isEmpty()) {

						for (int i = 0; i < temp.split(",").length; i++) {
							CustomerBusinessGroup group = (CustomerBusinessGroup) session
									.get(CustomerBusinessGroup.class,
											Integer.parseInt(temp.split(",")[i]));
							area.setGroupId(group);
							area.setAreaName(geographicForm.getState());
							area.setIsActive((byte) 1);
							area.setModifiedBy(userId);
							area.setModifiedOn(new Date());
							session.merge(area);

						}
					}
				}
				else if (geographicForm.getState() != null
						&& !geographicForm.getState().isEmpty()) {

					String temp = arrayToString(geographicForm.getRegions());
					if (geographicForm.getRegions() != null
							&& geographicForm.getRegions().length != 0
							&& !temp.isEmpty()) {

						for (int i = 0; i < temp.split(",").length; i++) {
							CustomerBusinessArea area = new CustomerBusinessArea();
							CustomerBusinessGroup group = (CustomerBusinessGroup) session
									.get(CustomerBusinessGroup.class, Integer
											.parseInt(temp.split(",")[i]));
							area.setGroupId(group);
							area.setAreaName(geographicForm.getState());
							area.setIsActive((byte) 1);
							area.setCreatedBy(userId);
							area.setCreatedOn(new Date());
							session.save(area);
						}
					}
				}
			}
			log.info(" ============saved business group successfully========== ");
			session.getTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerBusinessArea> listBusinessAreas(
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<CustomerBusinessArea> areas = null;
		try {
			session.beginTransaction();
			/* Fetch the list of active business areas. */

			areas = session.createQuery(
					"from CustomerBusinessArea where isActive=1").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return areas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GeographicalStateRegionDto> getServiceAreasList(
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<GeographicalStateRegionDto> areaDtos = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select BUSINESSGROUPNAME, group_concat(ba.ID, '-', ba.AREANAME SEPARATOR '|') From customer_businessgroup bg, "
					+ "customer_businessarea ba Where bg.ID = ba.BUSINESSGROUPID And bg.ISACTIVE = 1 "
					+ "And ba.ISACTIVE = 1 group by BUSINESSGROUPNAME Order by BUSINESSGROUPNAME,AREANAME ");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> areas = query.list();
			if (areas != null) {
				iterator = areas.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setBusinessGroup(objects[0].toString());
					}
					if (objects[1] != null) {
						dto.setServiceArea(objects[1].toString());
					}
					areaDtos.add(dto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return areaDtos;
	}

	/* (non-Javadoc)
	 * @see com.fg.vms.customer.dao.GeographicalStateRegionDao#checkDeleteArea()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String checkDeleteGroup(Integer groupId,Integer userId,UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String isDeletable="No";
		List<CustomerBusinessArea> customerbussinessarea=null;
          customerbussinessarea =session.createQuery("from CustomerBusinessArea area where area.groupId="+groupId).list();
          if(customerbussinessarea==null || customerbussinessarea.size()==0)
          {
        	  deleteGroup(groupId,userId,appDetails);
        	  isDeletable="Yes";
          }
		return isDeletable;
	}

	@Override
	public List<GeographicalStateRegionDto> getCommodityDescription(
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<GeographicalStateRegionDto> commodityDtos = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select ccg.CATEGORYDESCRIPTION,group_concat(ccm.ID,':',ccm.COMMODITYDESCRIPTION SEPARATOR '|') "
					+ "From customer_commodity ccm, customer_commoditycategory ccg "
					+ "	where ccg.ID=ccm.COMMODITYCATEGORYID And ccm.ISACTIVE = 1 And ccg.ISACTIVE = 1 "
					+ " group by ccg.CATEGORYDESCRIPTION order by ccg.CATEGORYDESCRIPTION, ccm.COMMODITYDESCRIPTION");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> commodity = query.list();
			if (commodity != null) {
				iterator = commodity.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setCommodityCategory(objects[0].toString());
					}
					if (objects[1] != null) {
						dto.setCommodityDescription(objects[1].toString());
					}
					commodityDtos.add(dto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return commodityDtos;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NaicsCode> getNaicsCode(
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<NaicsCode> naicsCode = new ArrayList<NaicsCode>();
		try {
			session.beginTransaction();
			naicsCode = session.createQuery(
					"from NaicsCode  where isActive=1 ").list();
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsCode;

	}

	@Override
	public List<GeographicalStateRegionDto> getCertificationType(
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<GeographicalStateRegionDto> certificationTypes = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			
			session.beginTransaction();
			StringBuilder sqlQuery = new StringBuilder();
			if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
			{
				StringBuilder divisionIds = new StringBuilder();
				for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++){
						divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
				}
				divisionIds.deleteCharAt(divisionIds.length() - 1);
				
				sqlQuery.append("Select DISTINCT certificationType.ID,certificationType.DESCRIPTION from customer_certificatetype certificationType "
						+ " INNER JOIN  customer_certificatetype_division  certficationDivision ON  certificationType.ID=certficationDivision.CERTIFICATETYPEID"
						+ " where certificationType.isActive=1 and certficationDivision.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
			}
			else
			{
				sqlQuery.append("Select certificationType.ID,certificationType.DESCRIPTION from customer_certificatetype certificationType where certificationType.isActive = 1");
			}
			System.out.println("GeogrphicalStateRegionDaoImpl @931: "+sqlQuery);
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
					List<?> certificate=query.list();
			session.getTransaction().commit();
			
			if (certificate != null) {
				iterator = certificate.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setCertficateTypeId((Integer)objects[0]);
					}
					if (objects[1] != null) {
						dto.setCertficateTypeDescription((objects[1].toString()));
					}
					certificationTypes.add(dto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificationTypes;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<GeographicalStateRegionDto> getCommodityGroup(UserDetailsDto appDetails) 
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(appDetails));
		List<GeographicalStateRegionDto> commodityGroup = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		
		try 
		{
			session.beginTransaction();
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT CONCAT(IFNULL(customer_marketsector.SECTORDESCRIPTION, '-'),':', "
					+ "IFNULL(customer_marketsector.ID,'-'),'|', IFNULL(customer_commoditycategory.CATEGORYDESCRIPTION, '-'),':', "
					+ "IFNULL(customer_commoditycategory.ID, '-')) AS SUBSECTOR_DESC, "
					+ "GROUP_CONCAT(customer_commodity.ID, ':', customer_commodity.COMMODITYDESCRIPTION SEPARATOR '|') "
					+ "AS COMMODITY_DESC FROM customer_commodity "
					+ "LEFT JOIN customer_commoditycategory ON customer_commodity.COMMODITYCATEGORYID = customer_commoditycategory.ID "
					+ "LEFT JOIN customer_marketsector ON customer_commoditycategory.MARKETSECTORID = customer_marketsector.ID "
					+ "GROUP BY customer_marketsector.SECTORDESCRIPTION, customer_commoditycategory.CATEGORYDESCRIPTION "
					+ "ORDER BY customer_marketsector.SECTORDESCRIPTION, customer_commoditycategory.CATEGORYDESCRIPTION, "
					+ "customer_commodity.COMMODITYDESCRIPTION");
			
			System.out.println("GeographicalStateRegionDaoImpl @ getCommodityGroup: " + sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> commodity=query.list();
			session.getTransaction().commit();
	
			if (commodity != null) 
			{
				iterator = commodity.iterator();
				while (iterator.hasNext()) 
				{
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setSubSectorDescription(objects[0].toString());
					}
					if (objects[1] != null) {
						dto.setCommodityDescription(objects[1].toString());
					}
					commodityGroup.add(dto);
				}
			}
		} 
		catch (HibernateException ex) 
		{
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try 
				{
					session.connection().close();
					session.close();
				} 
				catch (HibernateException e) 
				{
					PrintExceptionInLogFile.printException(e);
				} 
				catch (SQLException e) 
				{
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return commodityGroup;
	}
}