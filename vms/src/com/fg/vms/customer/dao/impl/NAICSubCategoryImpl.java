/*
 * NAICSubCategoryImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NAICSubCategoryDao;
import com.fg.vms.customer.dto.NaicsSubCategoryDto;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.pojo.NaicSubCategoryForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the Naics Sub-Category services implementation
 * 
 * @author vinoth
 * 
 */
public class NAICSubCategoryImpl implements NAICSubCategoryDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createNAICSubCategory(
            NaicSubCategoryForm naicSubCategoryForm, int userId,
            int currentCategoryId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";
        try {
            session.beginTransaction();

            NaicsCategory category = (NaicsCategory) session.get(
                    NaicsCategory.class, currentCategoryId);
            // setNaicCategoryId(naicSubCategoryForm.getNaicCategory());

            NAICSubCategory subCategory = new NAICSubCategory();

            /* Insert the naics sub-category details. */

            log.info(" ============save naics sub-category is starting========== ");

            subCategory.setId(naicSubCategoryForm.getId());
            subCategory.setNaicCategoryId(category);
            subCategory.setNaicSubCategoryDesc(naicSubCategoryForm
                    .getNaicSubCategoryDesc());
            subCategory.setIsActive(Byte.valueOf(naicSubCategoryForm
                    .getIsActive()));
            subCategory.setCreatedBy(currentCategoryId);
            subCategory.setCreatedOn(new Date());
            subCategory.setModifiedBy(currentCategoryId);
            subCategory.setModifiedOn(new Date());

            session.save(subCategory);

            log.info(" ============saved naics sub-category successfully========== ");

            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<NAICSubCategory> retrieveNAICSubCategories(
            UserDetailsDto appDetails, NaicsCategory category) {

        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(appDetails));

        /* Get the list of active sub-categories based on category. */
        List<NAICSubCategory> naicsub1 = null;
        StringBuilder query = new StringBuilder();
        query.append("FROM NAICSubCategory WHERE isActive = 1 ");
        if (category != null) {
            query.append(" AND naicCategoryId=" + category.getId());
        }
        query.append(" ORDER BY naicSubCategoryDesc ASC");
        try {
            session.beginTransaction();
            naicsub1 = session.createQuery(query.toString()).list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return naicsub1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteNAICSubCategory(Integer id, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";
        NAICSubCategory subCategory = null;
        try {
            session.beginTransaction();
            /* Delete/Remove sub-categories based on user selection. */
            log.info(" ============delete naics sub-category is starting========== ");

            subCategory = (NAICSubCategory) session.get(NAICSubCategory.class,
                    id);
            /*
             * Delete the template only if template do not have active template
             * questions.
             */
            List<NaicsMaster> masters = session
                    .createCriteria(NaicsMaster.class)
                    .add(Restrictions.eq("naicsSubCategoryId", subCategory))
                    .list();
            if (masters != null && masters.size() != 0) {
                return "reference";
            }

            session.delete(subCategory);
            session.getTransaction().commit();
            log.info(" ============deleted naics sub-category successfully========== ");

            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NAICSubCategory retrieveSubCategory(Integer id,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        NAICSubCategory subCategory = null;
        try {
            session.beginTransaction();
            /* Retrieve the selected sub-category details. */
            subCategory = (NAICSubCategory) session
                    .createCriteria(NAICSubCategory.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return subCategory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateNAICSubCategory(
            NaicSubCategoryForm naicSubCategoryForm, Integer id,
            int currentCategoryId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";

        NAICSubCategory subCategory = null;
        try {
            session.beginTransaction();
            subCategory = (NAICSubCategory) session.get(NAICSubCategory.class,
                    id);

            /* update/modify the selected sub-category details. */

            log.info(" ============update naics sub-category is starting========== ");

            subCategory.setNaicSubCategoryDesc(naicSubCategoryForm
                    .getNaicSubCategoryDesc());
            subCategory.setIsActive(Byte.valueOf(naicSubCategoryForm
                    .getIsActive()));
            subCategory.setModifiedBy(currentCategoryId);
            subCategory.setModifiedOn(new Date());

            session.update(subCategory);

            log.info(" ============updated naics sub-category successfully========== ");

            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NAICSubCategory> retrieveSubCategoryById(Integer categoryId,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<NAICSubCategory> naicsub = null;

        String sqlQuery = " FROM NAICSubCategory "
                + "WHERE isActive=1 AND naicCategoryId=:category ORDER BY naicSubCategoryDesc ASC";
        try {
            session.beginTransaction();
            /* Retrieve the active sub-category based on category. */
            NaicsCategory category = (NaicsCategory) session
                    .createCriteria(NaicsCategory.class)
                    .add(Restrictions.eq("id", categoryId)).uniqueResult();

            Query query = session.createQuery(sqlQuery);

            query.setParameter("category", category);
            naicsub = query.list();
            session.getTransaction().commit();
        } catch (Exception exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return naicsub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NaicsCategory retriveNaicsCategory(Integer categoryId,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        NaicsCategory category = null;
        try {
            session.beginTransaction();
            /* Load the list of active categories during the page load. */
            category = (NaicsCategory) session.get(NaicsCategory.class,
                    categoryId);

            session.getTransaction().commit();
        } catch (Exception exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return category;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NaicsSubCategoryDto> view(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<NAICSubCategory> naicSubCategories = null;
        List<NaicsSubCategoryDto> listSubCategoryDtos = null;
        try {
            session.beginTransaction();

            /* Retrieve the list of active sub-categories. */
            naicSubCategories = session
                    .createCriteria(
                            com.fg.vms.customer.model.NAICSubCategory.class)
                    .add(Restrictions.eq("isActive", (byte) 1)).list();
            listSubCategoryDtos = packageAgencyDtos(naicSubCategories);
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return listSubCategoryDtos;
    }

    /**
     * Package of Certificate agency details.
     * 
     * @param naicsSubCategories
     * @return
     */
    private List<NaicsSubCategoryDto> packageAgencyDtos(
            List<NAICSubCategory> naicsSubCategories) {

        List<NaicsSubCategoryDto> listSubCategoryDtos = new ArrayList<NaicsSubCategoryDto>();
        for (NAICSubCategory subCategory : naicsSubCategories) {

            NaicsSubCategoryDto subCategoryDto = new NaicsSubCategoryDto();
            subCategoryDto.setId(subCategory.getId());
            subCategoryDto.setNaicsSubCategoryDesc(subCategory
                    .getNaicSubCategoryDesc());
            subCategoryDto
                    .setActive(getBooleanValue(subCategory.getIsActive()));

            listSubCategoryDtos.add(subCategoryDto);

        }
        return listSubCategoryDtos;

    }

    /**
     * Convert byte value to boolean value.
     * 
     * @param value
     * @return
     */
    private boolean getBooleanValue(Byte value) {

        if (value == 1) {
            return true;
        } else {
            return false;
        }
    }

}
