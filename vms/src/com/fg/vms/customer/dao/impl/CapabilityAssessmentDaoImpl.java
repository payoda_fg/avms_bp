/*
 * CapabilityAssessmentDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CapabilityAssessmentDao;
import com.fg.vms.customer.dto.AssessmentQuestionsDto;
import com.fg.vms.customer.model.CustomerVendorAssessment;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.TemplateQuestions;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorEmailNotificationMaster;
import com.fg.vms.customer.model.VendorMailNotificationDetail;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

public class CapabilityAssessmentDaoImpl implements CapabilityAssessmentDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Template, List<AssessmentQuestionsDto>> getTemplate(
            UserDetailsDto userDetails, VendorMaster vendor) {

        Session session = null;
        Map<Template, List<AssessmentQuestionsDto>> listOfAssessmentQus = new HashMap<Template, List<AssessmentQuestionsDto>>();

        Iterator iterate = null;
        try {

            /* session object represents customer database dynamically */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();

            /* Get the vendor list based on vendor mail notification */
            List<VendorMailNotificationDetail> listDetails = session
                    .createCriteria(VendorMailNotificationDetail.class)
                    .add(Restrictions.eq("vendorMaster", vendor)).list();
            if (listDetails != null && !listDetails.isEmpty()) {
                for (VendorMailNotificationDetail detail : listDetails) {
                    List<AssessmentQuestionsDto> assessmentQuestions = new ArrayList<AssessmentQuestionsDto>();
                    String sqlQuery = "SELECT DISTINCT vendor_email_notification_master.ASSESSMENTTEMPLATEMASTERID,"
                            + "vendor_email_notification_template_detail.TEMPLATEQUESTIONSID, "
                            + "templatequestions.QUESTIONDESCRIPTION, templatequestions.ANSWERDATATYPE FROM templatequestions "
                            + "INNER JOIN vendor_email_notification_template_detail ON templatequestions.ID = vendor_email_notification_template_detail.TEMPLATEQUESTIONSID "
                            + "INNER JOIN vendor_email_notification_master ON vendor_email_notification_master.ID = vendor_email_notification_template_detail.VENDOREMAILNOTIFICATIONMASTERID "
                            + "INNER JOIN vendor_email_notification_detail ON vendor_email_notification_master.ID=vendor_email_notification_detail.VENDOREMAILNOTIFICATIONMASTERID "
                            + "WHERE vendor_email_notification_detail.VENDORID="
                            + "'"
                            + vendor.getId()
                            + "'"
                            + " AND templatequestions.ID NOT IN(SELECT a.TEMPLATEQUESTIONSID FROM  "
                            + "customervendorassessment a WHERE a.TEMPLATEID= "
                            + detail.getVendorEmailNotificationMaster()
                                    .getTemplate().getId()
                            + " and  a.VENDORID="
                            + vendor.getId()
                            + ") "
                            + "AND vendor_email_notification_master.ASSESSMENTTEMPLATEMASTERID ="
                            + detail.getVendorEmailNotificationMaster()
                                    .getTemplate().getId()
                            + " ORDER BY templatequestions.QUESTIONORDER";
                    System.out.println("CapAssessDaoImpl @ 89:"+sqlQuery);
                    Query query = session.createSQLQuery(sqlQuery);
                    iterate = query.list().iterator();
                    AssessmentQuestionsDto assessmentQuestionsDto = null;
                    if (iterate.hasNext()) {
                        while (iterate.hasNext()) {
                            Object[] result = (Object[]) iterate.next();
                            assessmentQuestionsDto = new AssessmentQuestionsDto();
                            assessmentQuestionsDto
                                    .setTemplateId((Integer) result[0]);
                            assessmentQuestionsDto
                                    .setTemplateQuestionId(Integer
                                            .parseInt(result[1].toString()));
                            assessmentQuestionsDto
                                    .setQuestions((String) result[2]);
                            assessmentQuestionsDto
                                    .setDataType((String) result[3]);

                            assessmentQuestions.add(assessmentQuestionsDto);
                        }
                    }
                    if (assessmentQuestions != null
                            && assessmentQuestions.size() != 0) {
                        listOfAssessmentQus.put(detail
                                .getVendorEmailNotificationMaster()
                                .getTemplate(), assessmentQuestions);
                    }
                }
            }
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            if (session != null && session.getTransaction() != null) {
                session.getTransaction().rollback();
            }

        } finally {
            if (session != null && session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return listOfAssessmentQus;
    }

    /**
     * {@inheritDoc}
     */
    public Integer getVendorId(Integer vendorUserId, UserDetailsDto userDetails) {

        Session session = null;
        Integer vendorId = null;

        try {

            /* session object represents customer database dynamically */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();

            /* Get the vendor contact detail based on vendor user */
            VendorContact vendorContact = (VendorContact) session
                    .createCriteria(VendorContact.class)
                    .add(Restrictions.eq("id", vendorUserId)).uniqueResult();

            VendorMaster vendorMaster = vendorContact.getVendorId();
            vendorId = vendorMaster.getId();

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
        } finally {
            if (session != null && session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return vendorId;
    }

    /**
     * {@inheritDoc}
     */
    public String getDataType(Integer templateQuestionsId,
            UserDetailsDto userDetails) {

        String dataType = null;
        Session session = null;
        try {

            /* session object represents customer database dynamically */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();

            /* get the template question based on template question id. */
            TemplateQuestions templateQuestions = (TemplateQuestions) session
                    .get(TemplateQuestions.class, templateQuestionsId);

            dataType = templateQuestions.getAnswerDatatype();

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            if (session != null && session.getTransaction() != null)
                session.getTransaction().rollback();
        } finally {
            if (session != null && session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return dataType;
    }

    /**
     * {@inheritDoc}
     */
    public String saveAssessmentAnswers(Integer vendorId, Integer vendorUserId,
            Integer templateId, Integer templateQuestionsId,
            UserDetailsDto userDetails, String description) {

        String result = "success";
        /* String result1 = ""; */
        Session session = null;

        try {

            /* session object represents customer database dynamically */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();
            CustomerVendorAssessment customerVendorAssessment = null;

            /* Get the vendor master details based on vendorId */
            VendorMaster vendorMaster = (VendorMaster) session.get(
                    VendorMaster.class, vendorId);

            /* get the template question based on template question id. */
            TemplateQuestions templateQuestions = (TemplateQuestions) session
                    .get(TemplateQuestions.class, templateQuestionsId);

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.append("SELECT {detail.*} "
                    + "FROM  template t INNER JOIN vendor_email_notification_master master ON master.ASSESSMENTTEMPLATEMASTERID = t.ID "
                    + "INNER JOIN vendor_email_notification_detail detail ON master.ID = detail.VENDOREMAILNOTIFICATIONMASTERID "
                    + "where detail.VENDORID=" + vendorMaster.getId()
                    + " and t.ID=" + templateQuestions.getTemplate().getId());

            SQLQuery query = session.createSQLQuery(sqlQuery.toString());
            query.addEntity("detail", VendorMailNotificationDetail.class);

            /* Get vendor email notification master details based on query. */
            VendorEmailNotificationMaster vendormEmailMasterId = ((VendorMailNotificationDetail) query
                    .list().get(0)).getVendorEmailNotificationMaster();

            if (description != null) {
                customerVendorAssessment = new CustomerVendorAssessment();

                log.info(" ============save customer vendor assessment is starting========== ");

                customerVendorAssessment.setAnswerDescription(description
                        .toString());
                customerVendorAssessment.setAnsweredBy(vendorUserId);
                customerVendorAssessment.setAnsweredOn(new Date());
                customerVendorAssessment.setTemplate(templateQuestions
                        .getTemplate());
                customerVendorAssessment
                        .setTemplateQuestions(templateQuestions);
                customerVendorAssessment.setIsreviewed((byte) 0);
                customerVendorAssessment.setReviewerRemarks("");
                customerVendorAssessment
                        .setAnswerWeightageAwarded(new Float(0));
                customerVendorAssessment
                        .setVendorEmailNotificationMaster(vendormEmailMasterId);
                customerVendorAssessment.setVendorMaster(vendorMaster);
                session.save(customerVendorAssessment);

                log.info(" ============saved customer vendor assessment successfully========== ");

                /* result1 = vendormEmailMasterId.getCreatedBy().toString(); */
                /* result = result1.concat(result); */
            }

            session.getTransaction().commit();

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            if (session != null && session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            result = "failure";
        } finally {
            if (session != null && session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    @Override
    public Boolean assessmentCompleted(VendorMaster vendor) {
        // TODO Auto-generated method stub
        return false;
    }
}
