/*
 * TemplateDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.TemplateDao;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.TemplateQuestions;
import com.fg.vms.customer.pojo.QuestionsForm;
import com.fg.vms.customer.pojo.TemplateQuestionsForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the template master services implementation
 * 
 * @author vivek
 * 
 */
public class TemplateDaoImpl implements TemplateDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Template> listTemplateNames(UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = null;
        try {
            session.beginTransaction();

            /* Retrieve the list of active templates. */
            templateResultList = session
                    .createQuery(
                            "from Template t where isActive=1 ORDER BY t.templateName ASC ")
                    .list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Template> allTemplateNames(UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = null;
        try {
            session.beginTransaction();

            /* Retrieve the list of all templates. */
            templateResultList = session.createQuery(
                    "from Template t ORDER BY t.templateName ASC ").list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createTemplate(QuestionsForm questionsForm,
            int currentUserId, UserDetailsDto userDetails) {
        Session session = null;
        String result = "";
        try {

            /* session object represents customer database dynamically. */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();

            /* To check unique template name. */
            Template template1 = (Template) session
                    .createCriteria(Template.class)
                    .add(Restrictions.eq("templateName",
                            questionsForm.getTemplateName())).uniqueResult();

            if (template1 != null) {
                result = "templateExisted";
            } else {
                Template template = new Template();

                /*
                 * Insert template details into database if template name is
                 * unique.
                 */
                log.info(" ============save template is starting========== ");

                template.setTemplateName(questionsForm.getTemplateName());
                template.setAssessQuestRemarks(questionsForm.getTemplateName());
                template.setIsActive((byte) 1);
                template.setCreatedBy(currentUserId);
                template.setCreatedOn(new Date());
                session.save(template);

                log.info(" ============saved template successfully========== ");
                result = "success";
                session.getTransaction().commit();
            }
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createTemplateQuestions(
            TemplateQuestionsForm templateQuestionsForm, int currentUserId,
            UserDetailsDto userDetails) {
        Session session = null;
        String result = "";
        try {

            /* session object represents customer database dynamically. */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();
            Template template = null;
            template = (Template) session
                    .createCriteria(Template.class)
                    .add(Restrictions.eq("id",
                            templateQuestionsForm.getTemplateId()))
                    .uniqueResult();
            TemplateQuestions templateQuestions = new TemplateQuestions();

            /* Insert the questions into database based on template. */

            log.info(" ============save template questions is starting========== ");
            templateQuestions.setQuestionOrder(templateQuestionsForm
                    .getQuestionOrder());
            templateQuestions.setQuestionDescription(templateQuestionsForm
                    .getQuestionDescription());
            templateQuestions.setAnswerWeightage(templateQuestionsForm
                    .getAnswerWeightage());
            templateQuestions.setAnswerDatatype(templateQuestionsForm
                    .getAnswerDatatype());
            templateQuestions.setIsActive((byte) 1);
            templateQuestions.setCreatedBy(currentUserId);
            templateQuestions.setCreatedOn(new Date());
            templateQuestions.setTemplate(template);
            session.save(templateQuestions);

            log.info(" ============saved template questions successfully========== ");
            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TemplateQuestions> listTemplateQuestions(
            UserDetailsDto userDetails, Integer id) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<TemplateQuestions> templateQuestionsResultList = null;
        try {
            session.beginTransaction();

            /* List of active template questions based on templates. */
            templateQuestionsResultList = session
                    .createQuery(
                            "from TemplateQuestions tq where isActive=1 AND templateId=:id ORDER BY tq.questionOrder ASC")
                    .setParameter("id", id).list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateQuestionsResultList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Template retriveTemplate(Integer id, UserDetailsDto userDetails) {

        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        session.beginTransaction();
        Template template = null;
        try {

            /* retrieve the selected template details */
            template = (Template) session.createCriteria(Template.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return template;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteTemplate(Integer id, UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";
        Template template = null;
        try {
            session.beginTransaction();
            template = (Template) session.get(Template.class, id);

            /*
             * Delete the template only if template do not have active template
             * questions.
             */
            List<TemplateQuestions> listQuestions = session
                    .createCriteria(TemplateQuestions.class)
                    .add(Restrictions.eq("template", template)).list();
            if (listQuestions != null && listQuestions.size() != 0) {
                return "reference";
            }
            // template.setIsActive((byte) 0);
            session.delete(template);
            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteTemplateQuestion(Integer id, UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";
        TemplateQuestions templateQuestions = null;
        try {
            session.beginTransaction();
            /* Delete/Deactive the template questions. */

            log.info(" ============dalete template questions is starting========== ");
            templateQuestions = (TemplateQuestions) session.get(
                    TemplateQuestions.class, id);
            templateQuestions.setIsActive((byte) 0);
            session.update(templateQuestions);
            session.getTransaction().commit();
            log.info(" ============deleted template questions successfully========== ");
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateTemplate(QuestionsForm questionsForm, Integer id,
            UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";

        Template template = null;
        try {
            session.beginTransaction();
            /* update the selected template details. */
            template = (Template) session.get(Template.class, id);

            log.info(" ============update template is starting========== ");
            template.setTemplateName(questionsForm.getTemplateName());
            template.setModifiedBy(userDetails.getCurrentCustomer().getId());
            template.setModifiedOn(new Date());
            session.update(template);

            log.info(" ============updated template successfully========== ");
            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();

        } catch (Exception exception) {
            result = "failure";
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TemplateQuestions retriveTemplateQuestions(Integer id,
            UserDetailsDto userDetails) {

        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        TemplateQuestions templateQuestions = null;
        try {
            session.beginTransaction();
            /* Retrieve the selected template question details. */
            templateQuestions = (TemplateQuestions) session
                    .createCriteria(TemplateQuestions.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateQuestions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateTemplateQuestions(
            TemplateQuestionsForm templateQuestionsForm, Integer id,
            UserDetailsDto userDetails) {

        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";

        TemplateQuestions templateQuestions = null;
        try {
            session.beginTransaction();
            /*
             * update/modify the selected template question details based on
             * template.
             */
            templateQuestions = (TemplateQuestions) session.get(
                    TemplateQuestions.class, id);

            log.info(" ============update template question is starting========== ");
            templateQuestions.setQuestionDescription(templateQuestionsForm
                    .getQuestionDescription());
            templateQuestions.setQuestionDescription(templateQuestionsForm
                    .getQuestionDescription());
            templateQuestions.setAnswerWeightage(templateQuestionsForm
                    .getAnswerWeightage());
            session.update(templateQuestions);

            log.info(" ============updated template question successfully========== ");

            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();

        } catch (Exception exception) {
            result = "failure";
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Template saveTemplate(String templateName, int currentUserId,
            UserDetailsDto userDetails) {
        Session session = null;
        Template template = null;
        try {

            /* session object represents customer database */
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            session.beginTransaction();

            /* To check unique template name. */
            Template template1 = (Template) session
                    .createCriteria(Template.class)
                    .add(Restrictions.eq("templateName", templateName))
                    .uniqueResult();
            if (template1 != null) {
                return template1;
            } else {
                template = new Template();

                /* Save the template details. */

                log.info(" ============save template is starting========== ");
                template.setTemplateName(templateName);
                template.setIsActive((byte) 1);
                template.setCreatedBy(currentUserId);
                template.setCreatedOn(new Date());
                session.save(template);

                log.info(" ============saved template successfully========== ");
            }
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return template;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveCopyTemplate(QuestionsForm questionsForm,
            int currentUserId, UserDetailsDto userDetails, Integer templateId) {

        Session session = null;
        String result = "";
        try {

            /* session object represents customer database */

            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));

            if (questionsForm.getQuestions() != null
                    && questionsForm.getQuestions().length > 0) {
                session.beginTransaction();
                for (String questions : questionsForm.getQuestions()) {
                    TemplateQuestions templateQuestion = new TemplateQuestions();
                    Integer questionId = Integer.parseInt(questions.toString());
                    templateQuestion = (TemplateQuestions) session.get(
                            TemplateQuestions.class, questionId);

                    /* To save the copied template details into new template. */

                    log.info(" ============save template questions is starting========== ");
                    TemplateQuestions templateQuestions = new TemplateQuestions();
                    templateQuestions.setQuestionDescription(templateQuestion
                            .getQuestionDescription());
                    Template template = new Template();
                    template.setId(templateId);
                    templateQuestions.setTemplate(template);
                    templateQuestions.setAnswerDatatype(templateQuestion
                            .getAnswerDatatype());
                    templateQuestions.setAnswerWeightage(templateQuestion
                            .getAnswerWeightage());
                    templateQuestions.setQuestionOrder(templateQuestion
                            .getQuestionOrder());
                    templateQuestions.setIsActive((byte) 1);
                    templateQuestions.setCreatedBy(currentUserId);
                    templateQuestions.setCreatedOn(new Date());
                    session.save(templateQuestions);

                    log.info(" ============saved template questions successfully========== ");

                }
                session.getTransaction().commit();
                result = "success";
            }
        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Template> listSelectTemplateQuestions(UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database . */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = new ArrayList<Template>();
        Iterator iterate = null;

        try {
            session.beginTransaction();

            /* Get the list of template questions based on template name. */
            Query query = session
                    .createSQLQuery("SELECT ID, TEMPLATENAME FROM template where ID in (select TEMPLATEID FROM templatequestions where ISACTIVE = 1) and ISACTIVE = 1 order by TEMPLATENAME asc");

            iterate = query.list().iterator();
            session.getTransaction().commit();
            if (iterate.hasNext()) {
                while (iterate.hasNext()) {
                    Object[] template = (Object[]) iterate.next();
                    Template templates = new Template();
                    templates.setId((Integer) template[0]);
                    templates.setTemplateName((String) template[1]);
                    templateResultList.add(templates);
                }
            }
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updatetemplateforactivestatus(Integer templateid,
            Byte statusvalue, Integer userid, UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        String result = "";

        Template template = null;
        try {
            session.beginTransaction();
            template = (Template) session.get(Template.class, templateid);

            /* Update template active statues on select the status. */
            log.info(" ============update template is starting========== ");
            template.setIsActive(statusvalue);
            template.setModifiedBy(userid);
            template.setModifiedOn(new Date());
            session.update(template);
            log.info(" ============template updated successfully========== ");
            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();

        } catch (Exception exception) {
            result = "failure";
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Template> getTemplateNamesforCopyandMove(
            UserDetailsDto userDetails, Integer currentTemplateId) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = null;
        try {
            session.beginTransaction();

            /* Get the list of templates based on template selected. */
            templateResultList = session
                    .createQuery(
                            "from Template t WHERE id != "
                                    + currentTemplateId
                                    + " AND t.isActive = 1 ORDER BY t.templateName ASC ")
                    .list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Template> listAssessmentTemplates(UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = new ArrayList<Template>();
        Iterator iterate = null;

        try {
            session.beginTransaction();

            /*
             * get the list of templates based on templates send to vendor for
             * capability assessment
             */
            String sqlQuery = "SELECT DISTINCT T.ID, T.TEMPLATENAME FROM template T INNER JOIN customervendorassessment CVA "
                    + "ON T.ID = CVA.TEMPLATEID "
                    + "WHERE T.ID IN (SELECT TEMPLATEID FROM templatequestions where ISACTIVE = 1) "
                    + "AND T.ISACTIVE = 1 ORDER BY T.TEMPLATENAME ASC";
            Query query = session.createSQLQuery(sqlQuery);

            iterate = query.list().iterator();
            session.getTransaction().commit();
            if (iterate.hasNext()) {
                while (iterate.hasNext()) {
                    Object[] template = (Object[]) iterate.next();
                    Template templates = new Template();
                    templates.setId((Integer) template[0]);
                    templates.setTemplateName((String) template[1]);
                    templateResultList.add(templates);
                }
            }
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Template> listAssessmentScoreTemplates(
            UserDetailsDto userDetails) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = new ArrayList<Template>();
        Iterator iterate = null;

        try {
            session.beginTransaction();

            /*
             * Get the list of templates where all the questions in template are
             * answered.
             */
            String sqlQuery = "SELECT DISTINCT template.ID, template.TEMPLATENAME FROM template INNER JOIN customervendorassessment "
                    + "ON template.ID = customervendorassessment.TEMPLATEID INNER JOIN templatequestions "
                    + "ON template.ID = templatequestions.TEMPLATEID WHERE template.ISACTIVE = 1 ";
            /*
             * +
             * "AND (SELECT COUNT(B.TEMPLATEQUESTIONSID) FROM CUSTOMERVENDORASSESSMENT B WHERE B.TEMPLATEID = CUSTOMERVENDORASSESSMENT.TEMPLATEID) "
             * +
             * "= (SELECT COUNT(C.ID) FROM TEMPLATEQUESTIONS C WHERE C.TEMPLATEID = TEMPLATEQUESTIONS.TEMPLATEID)"
             * ;
             */
            System.out.println("Template DaoImpl @ 970 : "+sqlQuery);
            Query query = session.createSQLQuery(sqlQuery);

            iterate = query.list().iterator();
            session.getTransaction().commit();
            if (iterate.hasNext()) {
                while (iterate.hasNext()) {
                    Object[] template = (Object[]) iterate.next();
                    Template templates = new Template();
                    templates.setId((Integer) template[0]);
                    templates.setTemplateName((String) template[1]);
                    templateResultList.add(templates);
                }
            }
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;
    }

    @Override
    public List<Template> listTemplatesBasedVendor(UserDetailsDto userDetails,
            Integer vendorId) {
        Session session = null;

        /* session object represents customer database dynamically. */

        session = HibernateUtilCustomer.buildSessionFactory().openSession(
                DataBaseConnection.getConnection(userDetails));

        List<Template> templateResultList = new ArrayList<Template>();
        Iterator iterate = null;

        try {
            session.beginTransaction();

            /*
             * get the list of templates based on templates send to vendor for
             * capability assessment
             */
            String sqlQuery = "SELECT DISTINCT T.ID, T.TEMPLATENAME FROM template T INNER JOIN customervendorassessment CVA "
                    + "ON T.ID = CVA.TEMPLATEID "
                    + "WHERE T.ID IN (SELECT TEMPLATEID FROM templatequestions where ISACTIVE = 1) "
                    + "AND CVA.VENDORID = "
                    + vendorId
                    + " AND T.ISACTIVE = 1 ORDER BY T.TEMPLATENAME ASC";
            System.out.println("TemplateDaoImpl @ 1028 : "+sqlQuery);
            Query query = session.createSQLQuery(sqlQuery);

            iterate = query.list().iterator();
            session.getTransaction().commit();
            if (iterate.hasNext()) {
                while (iterate.hasNext()) {
                    Object[] template = (Object[]) iterate.next();
                    Template templates = new Template();
                    templates.setId((Integer) template[0]);
                    templates.setTemplateName((String) template[1]);
                    templateResultList.add(templates);
                }
            }
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return templateResultList;

    }

//	public List<String> getAssessmentScoreDetails(
//			Integer scoreDetailTemplateId, Integer scoreDetailVendorId) {
//		
//		return null;
//	}
}
