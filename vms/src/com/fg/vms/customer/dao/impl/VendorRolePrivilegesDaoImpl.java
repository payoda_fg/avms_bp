/*
 * VendorRolePrivilegesDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.VendorRolePrivilegesDao;
import com.fg.vms.customer.dto.VendorRolesAndObjects;
import com.fg.vms.customer.model.VendorObjects;
import com.fg.vms.customer.model.VendorRolePrivileges;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the vendor role privileges services implementation
 * 
 */
public class VendorRolePrivilegesDaoImpl implements VendorRolePrivilegesDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public String persistPrivilegesByRole(
            VendorRolesAndObjects rolesAndObjects, String roleName,
            Integer userId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";
        try {
            session.beginTransaction();
            VendorRolePrivileges rolePrivileges = new VendorRolePrivileges();

            VendorRoles role = (VendorRoles) session.get(VendorRoles.class,
                    Integer.parseInt(roleName));

            /* Get the vendor object detail based on object name. */
            VendorObjects object = (VendorObjects) session
                    .createCriteria(VendorObjects.class)
                    .add(Restrictions.eq("objectName",
                            rolesAndObjects.getObjectName())).uniqueResult();

            /* Get the privileges detail based on unique vendor role and object. */
            VendorRolePrivileges privileges = (VendorRolePrivileges) session
                    .createCriteria(VendorRolePrivileges.class)
                    .add(Restrictions.and(
                            Restrictions.eq("vendorUserRoleId", role),
                            Restrictions.eq("objectId", object)))
                    .uniqueResult();

            if (privileges == null) {

                log.info(" ============save role privileges is starting========== ");

                rolePrivileges.setObjectId(object);

                rolePrivileges.setAdd(getByteValue(rolesAndObjects.isAdd()));

                rolePrivileges.setVendorUserRoleId(role);
                rolePrivileges.setDelete(getByteValue(rolesAndObjects
                        .isDelete()));
                rolePrivileges.setEnable(getByteValue(rolesAndObjects
                        .isEnable()));
                rolePrivileges.setModify(getByteValue(rolesAndObjects
                        .isModify()));
                rolePrivileges.setView(getByteValue(rolesAndObjects.isView()));
                rolePrivileges.setVisible(getByteValue(true));
                rolePrivileges.setIsActive((byte) 1);
                rolePrivileges.setCreatedBy(userId);
                rolePrivileges.setCreatedOn(new Date());
                rolePrivileges.setModifiedBy(userId);
                rolePrivileges.setModifiedOn(new Date());
                session.save(rolePrivileges);
                session.getTransaction().commit();
                result = "success";
            } else if (privileges != null) {
                privileges.setAdd(getByteValue(rolesAndObjects.isAdd()));

                privileges.setDelete(getByteValue(rolesAndObjects.isDelete()));
                privileges.setEnable(getByteValue(rolesAndObjects.isEnable()));
                privileges.setModify(getByteValue(rolesAndObjects.isModify()));
                privileges.setView(getByteValue(rolesAndObjects.isView()));
                privileges.setVisible(getByteValue(true));
                privileges.setModifiedBy(userId);
                privileges.setModifiedOn(new Date());
                session.update(privileges);
                session.getTransaction().commit();
                result = "success";
            }
        } catch (HibernateException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;

    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VendorRolePrivileges> retrievePrivilegesByRole(Integer roleId,
            UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        List<VendorRolePrivileges> privileges = null;
        session.beginTransaction();

        String sqlQuery = "SELECT rolePrivileges FROM VendorRolePrivileges rolePrivileges,VendorObjects vmsObject,VendorRoles vendorRole "
                + "WHERE rolePrivileges.objectId=vmsObject AND "
                + "rolePrivileges.vendorUserRoleId=vendorRole AND "
                + "rolePrivileges.vendorUserRoleId= :role and vmsObject.isActive=1";
        try {
            VendorRoles role = (VendorRoles) session.get(VendorRoles.class,
                    roleId);
            Query query = session.createQuery(sqlQuery);
            query.setParameter("role", role);
            privileges = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            PrintExceptionInLogFile.printException(e);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return privileges;
    }

    /**
     * Using boolean value to get byte.
     * 
     * @param value
     * @return
     */
    private Byte getByteValue(boolean value) {

        if (value) {
            return (byte) 1;
        } else {
            return (byte) 0;
        }
    }
}
