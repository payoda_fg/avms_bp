/*
 * AnonymousVendorDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.AnonymousVendorDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.model.AnonymousVendor;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertificateExpirationNotification;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerCommodity;
import com.fg.vms.customer.model.CustomerDiverseClassification;
import com.fg.vms.customer.model.CustomerServiceArea;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.CustomerVendorOwner;
import com.fg.vms.customer.model.CustomerVendorSales;
import com.fg.vms.customer.model.CustomerVendorServiceArea;
import com.fg.vms.customer.model.CustomerVendornetworldCertificate;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.VendorBusinessArea;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.customer.model.VendorOtherCertificate;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SelfRegistrationValidation;
import com.fg.vms.util.VendorStatus;
import com.fg.vms.util.VendorUtil;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * Represents the anonymous vendor services implementation
 * 
 * @author vinoth
 * 
 */
public class AnonymousVendorDaoImpl implements AnonymousVendorDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String approveNewVendor(Integer id, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		AnonymousVendor anonymousVendor = null;
		try {
			log.info(" ============approve anonymous vendor is starting========== ");
			anonymousVendor = (AnonymousVendor) session.get(
					AnonymousVendor.class, id);
			anonymousVendor.setIsActive((byte) 0);
			session.update(anonymousVendor);
			session.getTransaction().commit();
			log.info(" ============approved anonymous vendor successfully========== ");
			return "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String mergeVendorContact(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url) {
		log.info("Inside the merge vendor contact method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();

			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery("From CustomerVendorMenuStatus where tab=1 and vendorId=" + vendorId).uniqueResult();
				
				/**
				 * To maintain the menu status for self vendor registration process, For old Records in the Database.
				 */
				if(menuStatus == null) {					
					for (int i = 1; i <= 13; i++) {
						CustomerVendorMenuStatus menuStatus2 = new CustomerVendorMenuStatus();
						menuStatus2.setTab(i);
						if (i == 1) {
							menuStatus2.setStatus(2);
						} else {
							menuStatus2.setStatus(0);
						}
						menuStatus2.setCreatedBy(-1);
						menuStatus2.setCreatedOn(new Date());
						menuStatus2.setVendorId(vendorMaster);
						session.save(menuStatus2);
					}
					menuStatus = (CustomerVendorMenuStatus) session.createQuery("From CustomerVendorMenuStatus where tab=1 and vendorId=" + vendorId).uniqueResult();
				}				
				
				menuStatus.setTab(1);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);

					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=2 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(2);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);

			}
			// both email id is same
			if (null != vendorForm.getPreparerEmail()
					&& null != vendorForm.getContanctEmail()
					&& vendorForm
							.getPreparerEmail()
							.trim()
							.equalsIgnoreCase(
									vendorForm.getContanctEmail().trim())) {
				List<VendorContact> contacts = session.createQuery(
						"From VendorContact vc where  vc.vendorId="
								+ vendorMaster.getId()).list();
				/*
				 * if (null != contacts && contacts.size() > 1) { if
				 * (vendorForm.getPreparerEmail().equalsIgnoreCase(
				 * vendorForm.getContanctEmail())) { return "uniqueEmail"; } }
				 */
				VendorContact contact = null;
				if (null != contacts && !contacts.isEmpty()) {
					contact = contacts.get(0);
					contact = VendorUtil.packPreparerBusinessContact(
							vendorForm, userId, contact, vendorMaster);
					contact.setIsAllowedLogin(CommonUtils.getByteValue(true));
					contact.setIsBusinessContact(CommonUtils.getByteValue(true));
					contact.setIsPreparer(CommonUtils.getByteValue(true));
					contact.setIsPrimaryContact((byte) 1);
					contact.setIsSysGenPassword((byte) 0);
					SecretQuestion secretQns = new SecretQuestion();
					secretQns = (SecretQuestion) session.get(
							SecretQuestion.class, vendorForm.getUserSecQn());
					contact.setSecretQuestionId(secretQns);
					contact.setSecQueAns(vendorForm.getUserSecQnAns());
					VendorRoles roles = (VendorRoles) session
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);
					contact.setVendorUserRoleId(roles);
					session.merge(contact);
				} else {
					contact = new VendorContact();
					contact = VendorUtil.packPreparerBusinessContact(
							vendorForm, userId, contact, vendorMaster);
					contact.setIsAllowedLogin(CommonUtils.getByteValue(true));
					contact.setIsBusinessContact(CommonUtils.getByteValue(true));
					contact.setIsPreparer(CommonUtils.getByteValue(true));
					contact.setIsPrimaryContact((byte) 1);
					contact.setIsSysGenPassword((byte) 0);
					SecretQuestion secretQns = new SecretQuestion();
					secretQns = (SecretQuestion) session.get(
							SecretQuestion.class, vendorForm.getUserSecQn());
					contact.setSecretQuestionId(secretQns);
					contact.setSecQueAns(vendorForm.getUserSecQnAns());
					VendorRoles roles = (VendorRoles) session
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);
					contact.setVendorUserRoleId(roles);
					session.save(contact);
				}

			} else {
				// bussiness contact
				VendorContact businessContact = null;
				businessContact = (VendorContact) session.createQuery(
						"From VendorContact vc where vc.isBusinessContact = 1 and vc.vendorId="
								+ vendorMaster.getId()).uniqueResult();
				if (null != businessContact) {
					VendorRoles roles = (VendorRoles) session
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);
					businessContact.setVendorUserRoleId(roles);
					businessContact = VendorUtil.packVendorPreparerContact(
							vendorForm, userId, businessContact, vendorMaster);
					businessContact.setIsPrimaryContact((byte) 0);
					businessContact.setIsSysGenPassword((byte) 0);
					businessContact.setIsBusinessContact((byte) 1);
					businessContact.setIsPreparer(CommonUtils
							.getByteValue(false));
					session.merge(businessContact);
				} else {
					businessContact = new VendorContact();
					VendorRoles roles = (VendorRoles) session
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);
					businessContact.setVendorUserRoleId(roles);
					businessContact = VendorUtil.packVendorPreparerContact(
							vendorForm, userId, businessContact, vendorMaster);
					businessContact.setIsPrimaryContact((byte) 0);
					businessContact.setIsSysGenPassword((byte) 0);
					businessContact.setIsBusinessContact((byte) 1);
					businessContact.setIsPreparer(CommonUtils
							.getByteValue(false));
					session.save(businessContact);
				}
				// preparer
				List<VendorContact> preparerContacts = session.createQuery(
						"From VendorContact vc where  vc.isPreparer = 1 and vc.vendorId="
								+ vendorMaster.getId()).list();
				VendorContact preparerContact = null;
				if (null != preparerContacts && !preparerContacts.isEmpty()) {
					preparerContact = preparerContacts.get(0);
					preparerContact = VendorUtil.packVendorPreparerContact1(
							vendorForm, userId, preparerContact, vendorMaster);
					VendorRoles roles = (VendorRoles) session
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);
					preparerContact.setVendorUserRoleId(roles);
					preparerContact.setIsSysGenPassword((byte) 0);
					preparerContact.setIsPrimaryContact((byte) 1);
					preparerContact.setIsBusinessContact((byte) 0);
					preparerContact.setIsPreparer(CommonUtils
							.getByteValue(true));
					preparerContact.setIsAllowedLogin(CommonUtils
							.getByteValue(true));
					SecretQuestion secretQns = new SecretQuestion();
					secretQns = (SecretQuestion) session.get(
							SecretQuestion.class, vendorForm.getUserSecQn());
					preparerContact.setSecretQuestionId(secretQns);
					preparerContact.setSecQueAns(vendorForm.getUserSecQnAns());
					session.merge(preparerContact);
				} else {
					preparerContact = new VendorContact();
					preparerContact = VendorUtil.packVendorPreparerContact1(
							vendorForm, userId, preparerContact, vendorMaster);
					VendorRoles roles = (VendorRoles) session
							.createCriteria(VendorRoles.class)
							.add(Restrictions.eq("isActive", (byte) 1)).list()
							.get(0);
					preparerContact.setVendorUserRoleId(roles);
					preparerContact.setIsSysGenPassword((byte) 0);
					preparerContact.setIsPrimaryContact((byte) 1);
					preparerContact.setIsBusinessContact((byte) 0);
					preparerContact.setIsPreparer(CommonUtils
							.getByteValue(true));
					preparerContact.setIsAllowedLogin(CommonUtils
							.getByteValue(true));
					SecretQuestion secretQns = new SecretQuestion();
					secretQns = (SecretQuestion) session.get(
							SecretQuestion.class, vendorForm.getUserSecQn());
					preparerContact.setSecretQuestionId(secretQns);
					preparerContact.setSecQueAns(vendorForm.getUserSecQnAns());
					session.save(preparerContact);
				}
			}

			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String mergeVendorInformation(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url) {
		log.info("Inside the merge vendor information method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		Users user = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			if (vendorMaster != null) {
				log.info("=================== Vendor update start here =============");

				// update VendorMaster info
				vendorMaster.setVendorName(vendorForm.getVendorName());
				if (null != vendorForm.getDunsNumber()
						&& !vendorForm.getDunsNumber().isEmpty()
						&& !vendorForm.getDunsNumber().equalsIgnoreCase(
								"Optional")) {
					vendorMaster.setDunsNumber(vendorForm.getDunsNumber());
				} else {
					vendorMaster.setDunsNumber(null);
				}
				vendorMaster.setTaxId(vendorForm.getTaxId());

				if (null != vendorForm.getWebsite()
						&& !vendorForm.getWebsite().isEmpty()
						&& !vendorForm.getWebsite()
								.equalsIgnoreCase("Optional")) {
					vendorMaster.setWebsite(vendorForm.getWebsite());
				} else {
					vendorMaster.setWebsite(null);
				}

				if (null != vendorForm.getVendorStatus()
						&& !vendorForm.getVendorStatus().isEmpty()) {
					user = (Users) session.get(Users.class, userId);
					vendorMaster.setVendorStatus(vendorForm.getVendorStatus());
					if (null != vendorForm.getVendorStatus()
							&& (vendorForm.getVendorStatus().equalsIgnoreCase(
									VendorStatus.ACTIVE.getIndex()) || vendorForm
									.getVendorStatus().equalsIgnoreCase(
											VendorStatus.BPSUPPLIER.getIndex()))) {
						vendorMaster.setApprovedOn(new Date());
						vendorMaster.setApprovedBy(user);
						vendorMaster.setIsApproved((byte) 1);
					} else if (vendorForm.getVendorStatus().equalsIgnoreCase(
							VendorStatus.INACTIVE.getIndex())) {
						vendorMaster.setIsActive((byte) 0);
					}
				}

				if (null != vendorForm.getEmailId()
						&& !vendorForm.getEmailId().isEmpty()
						&& !vendorForm.getEmailId()
								.equalsIgnoreCase("Optional")) {
					vendorMaster.setEmailId(vendorForm.getEmailId());
				}

				if (vendorForm.getNumberOfEmployees() != null
						&& !vendorForm.getNumberOfEmployees().isEmpty()
						&& !vendorForm.getNumberOfEmployees().equalsIgnoreCase(
								"null")) {
					vendorMaster.setNumberOfEmployees(Integer
							.parseInt(vendorForm.getNumberOfEmployees()));
				} else {
					vendorMaster.setNumberOfEmployees(null);
				}
				if (null != vendorForm.getYearOfEstablishment()
						&& !vendorForm.getYearOfEstablishment().isEmpty()) {
					vendorMaster.setYearOfEstablishment(Integer
							.parseInt(vendorForm.getYearOfEstablishment()));
				} else {
					vendorMaster.setYearOfEstablishment(null);
				}

				if(vendorMaster.getDeverseSupplier() != null && vendorMaster.getDeverseSupplier() != 1)
					if(userDetails.getIsGlobalDivision() != null && userDetails.getIsGlobalDivision() != 0 )
						vendorMaster.setDeverseSupplier((byte)0);
					else
						vendorMaster.setDeverseSupplier(CommonUtils.getCheckboxValue("on"));
				
				vendorMaster.setModifiedBy(userId);
				vendorMaster.setModifiedOn(new Date());

				if (null != vendorForm.getStateIncorporation()
						&& !vendorForm.getStateIncorporation().isEmpty()
						&& !vendorForm.getStateIncorporation()
								.equalsIgnoreCase("Optional")) {
					vendorMaster.setStateIncorporation(vendorForm
							.getStateIncorporation());
				} else {
					vendorMaster.setStateIncorporation(null);
				}
				if (null != vendorForm.getStateSalesTaxId()
						&& !vendorForm.getStateSalesTaxId().isEmpty()
						&& !vendorForm.getStateSalesTaxId().equalsIgnoreCase(
								"Optional")) {
					vendorMaster.setStateSalesTaxId(vendorForm
							.getStateSalesTaxId());
				} else {
					vendorMaster.setStateSalesTaxId(null);
				}

				if (null != vendorForm.getCompanyOwnership()
						&& !vendorForm.getCompanyOwnership().isEmpty()) {
					vendorMaster.setCompanyownership(Short.valueOf(vendorForm
							.getCompanyOwnership()));
				}
				vendorMaster.setBusinesstype(vendorForm.getBusinessType());
				vendorMaster.setCompanyType(vendorForm.getCompanytype());
				log.info("Vendor sales update starting.");
				if (vendorForm.getAnnualTurnover() != null
						&& !vendorForm.getAnnualTurnover().isEmpty()
						&& !vendorForm.getAnnualTurnover().equalsIgnoreCase(
								"Optional")) {
					CustomerVendorSales vendorSales1;
					if (vendorForm.getAnnualTurnoverId1() != null
							&& vendorForm.getAnnualTurnoverId1() != 0) {
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId1());
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getAnnualTurnover()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear1());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setModifiedby(userId);
						vendorSales1.setModifiedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.merge(vendorSales1);
					} else {
						vendorSales1 = new CustomerVendorSales();
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getAnnualTurnover()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear1());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setCreatedby(userId);
						vendorSales1.setCreatedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.save(vendorSales1);
					}
				} else {
					if (vendorForm.getAnnualTurnoverId1() != null
							&& vendorForm.getAnnualTurnoverId1() != 0) {
						CustomerVendorSales vendorSales1;
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId1());
						if(vendorSales1 != null)
							session.delete(vendorSales1);
					}
				}
				if (vendorForm.getSales2() != null
						&& !vendorForm.getSales2().isEmpty()
						&& !vendorForm.getSales2().equalsIgnoreCase("Optional")) {
					CustomerVendorSales vendorSales1;
					if (vendorForm.getAnnualTurnoverId2() != null
							&& vendorForm.getAnnualTurnoverId2() != 0) {
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId2());
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales2()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear2());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setModifiedby(userId);
						vendorSales1.setModifiedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.merge(vendorSales1);
					} else {
						vendorSales1 = new CustomerVendorSales();
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales2()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear2());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setCreatedby(userId);
						vendorSales1.setCreatedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.save(vendorSales1);
					}
				} else {
					if (vendorForm.getAnnualTurnoverId2() != null
							&& vendorForm.getAnnualTurnoverId2() != 0) {
						CustomerVendorSales vendorSales1;
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId2());
						if(vendorSales1 != null)
							session.delete(vendorSales1);
					}
				}
				if (vendorForm.getSales3() != null
						&& !vendorForm.getSales3().isEmpty()
						&& !vendorForm.getSales3().equalsIgnoreCase("Optional")) {
					CustomerVendorSales vendorSales1;
					if (vendorForm.getAnnualTurnoverId3() != null
							&& vendorForm.getAnnualTurnoverId3() != 0) {
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId3());
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales3()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear3());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setModifiedby(userId);
						vendorSales1.setModifiedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.merge(vendorSales1);
					} else {
						vendorSales1 = new CustomerVendorSales();
						vendorSales1.setYearsales(CommonUtils
								.deformatMoney(vendorForm.getSales3()));
						vendorSales1.setYearnumber(vendorForm.getAnnualYear3());
						vendorSales1.setVendorid(vendorMaster);
						vendorSales1.setCreatedby(userId);
						vendorSales1.setCreatedon(new Date());
						vendorSales1.setIsactive((short) 1);
						session.save(vendorSales1);
					}
				} else {
					if (vendorForm.getAnnualTurnoverId3() != null
							&& vendorForm.getAnnualTurnoverId3() != 0) {
						CustomerVendorSales vendorSales1;
						vendorSales1 = (CustomerVendorSales) session.get(
								CustomerVendorSales.class,
								vendorForm.getAnnualTurnoverId3());
						if(vendorSales1 != null)
							session.delete(vendorSales1);
					}
				}
				if (null != vendorForm.getPrimeNonPrimeVendor()
						&& !vendorForm.getPrimeNonPrimeVendor().isEmpty()) {
					vendorMaster.setPrimeNonPrimeVendor(Byte.valueOf(vendorForm
							.getPrimeNonPrimeVendor()));
				}
				log.info("Vendor sales update success.");
				result = "success";
				session.update(vendorMaster);
				/**
				 * Update vendor menu status.
				 */
				if (null != vendorForm.getSubmitType()
						&& !vendorForm.getSubmitType().isEmpty()) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=2 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus.setTab(2);
					menuStatus.setModifiedBy(userId);
					menuStatus.setModifiedOn(new Date());
					menuStatus.setVendorId(vendorMaster);
					if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
						menuStatus.setStatus(2);
					} else {
						menuStatus.setStatus(1);
						/**
						 * page submitted, hence activate next menu.
						 */
						CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
								.createQuery(
										"From CustomerVendorMenuStatus where tab=3 and vendorId="
												+ vendorId).uniqueResult();
						menuStatus1.setTab(3);
						menuStatus1.setModifiedBy(userId);
						menuStatus1.setModifiedOn(new Date());
						menuStatus1.setVendorId(vendorMaster);
						if (menuStatus1.getStatus() == 0) {
							menuStatus1.setStatus(2);
						}
						session.merge(menuStatus1);
					}
					session.merge(menuStatus);
				}
			}
			log.info("Vendor master update success.");

			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String mergeVendorAddress(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url) {
		log.info("Inside the merge vendor address method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			vendorMaster.setAddress1(vendorForm.getAddress1());
			vendorMaster.setCity(vendorForm.getCity());
			vendorMaster.setCountry(vendorForm.getCountry());
			vendorMaster.setState(vendorForm.getState());
			vendorMaster.setProvince(vendorForm.getProvince());
			
			if(vendorMaster.getDeverseSupplier() != null && vendorMaster.getDeverseSupplier() != 1)
				if(userDetails.getIsGlobalDivision() != null && userDetails.getIsGlobalDivision() != 0 )
					vendorMaster.setDeverseSupplier((byte)0);
				else
					vendorMaster.setDeverseSupplier(CommonUtils.getCheckboxValue("on"));
			
			vendorMaster.setModifiedBy(userId);
			vendorMaster.setModifiedOn(new Date());
			session.merge(vendorMaster);
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=3 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(3);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=4 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(4);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			log.info("Vendor address master start saving.");
			if (vendorForm.getAddress1() != null
					&& !vendorForm.getAddress1().isEmpty()) {
				CustomerVendorAddressMaster addressMaster;
				addressMaster = (CustomerVendorAddressMaster) session
						.createQuery(
								"from CustomerVendorAddressMaster where addressType = 'P' and vendorId="
										+ vendorMaster.getId()).uniqueResult();
				if (addressMaster == null) {
					addressMaster = new CustomerVendorAddressMaster();
					addressMaster.setCreatedBy(userId);
					addressMaster.setCreatedOn(new Date());
				} else {
					addressMaster.setModifiedBy(userId);
					addressMaster.setModifiedOn(new Date());
				}
				addressMaster = VendorUtil.packVendorAddress1(vendorForm,
						userId, addressMaster, vendorMaster);
				session.merge(addressMaster);
			}
			if (vendorForm.getAddress2() != null
					&& !vendorForm.getAddress2().isEmpty()) {
				CustomerVendorAddressMaster addressMaster1;
				addressMaster1 = (CustomerVendorAddressMaster) session
						.createQuery(
								"from CustomerVendorAddressMaster where addressType = 'M' and vendorId="
										+ vendorMaster.getId()).uniqueResult();
				if (addressMaster1 == null) {
					addressMaster1 = new CustomerVendorAddressMaster();
					addressMaster1.setCreatedBy(userId);
					addressMaster1.setCreatedOn(new Date());
				} else {
					addressMaster1.setModifiedBy(userId);
					addressMaster1.setModifiedOn(new Date());
				}
				addressMaster1 = VendorUtil.packVendorAddress2(vendorForm,
						userId, addressMaster1, vendorMaster);
				session.merge(addressMaster1);
			} else {
				CustomerVendorAddressMaster addressMaster1;
				addressMaster1 = (CustomerVendorAddressMaster) session
						.createQuery(
								"from CustomerVendorAddressMaster where addressType = 'M' and vendorId="
										+ vendorMaster.getId()).uniqueResult();
				if (addressMaster1 != null) {
					session.delete(addressMaster1);
				}
			}
			log.info("Vendor address master saved.");
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String mergeVendorOwnerInfo(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url) {
		log.info("Inside the merge vendor owner information method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			if (vendorForm.getCompanyOwnership() != null
					&& !vendorForm.getCompanyOwnership().isEmpty()) {
				vendorMaster.setCompanyownership(Short.valueOf(vendorForm
						.getCompanyOwnership()));
				
				if(vendorMaster.getDeverseSupplier() != null && vendorMaster.getDeverseSupplier() != 1)
					if(userDetails.getIsGlobalDivision() != null && userDetails.getIsGlobalDivision() != 0 )
						vendorMaster.setDeverseSupplier((byte)0);
					else
						vendorMaster.setDeverseSupplier(CommonUtils.getCheckboxValue("on"));
				
				vendorMaster.setModifiedBy(userId);
				vendorMaster.setModifiedOn(new Date());
				session.merge(vendorMaster);
			}
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=4 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(4);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=5 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(5);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			// update Customer Vendor Owner information
			if (vendorForm.getOwnerId1() != null
					&& vendorForm.getOwnerId1() != 0) {
				CustomerVendorOwner owner1 = (CustomerVendorOwner) session.get(
						CustomerVendorOwner.class, vendorForm.getOwnerId1());
				if (owner1 != null) {
					owner1 = VendorUtil.packVendorOwner1(vendorForm, userId,
							owner1, vendorMaster);
					owner1.setModifiedby(userId);
					owner1.setModifiedon(new Date());
					session.merge(owner1);
				}
			} else {
				if (null != vendorForm.getOwnerName1()
						&& !vendorForm.getOwnerName1().isEmpty()) {
					CustomerVendorOwner owner1 = new CustomerVendorOwner();
					owner1 = VendorUtil.packVendorOwner1(vendorForm, userId,
							owner1, vendorMaster);
					owner1.setCreatedby(userId);
					owner1.setCreatedon(new Date());
					session.save(owner1);
				}
			}
			if (vendorForm.getOwnerId2() != null
					&& vendorForm.getOwnerId2() != 0) {
				CustomerVendorOwner owner2 = (CustomerVendorOwner) session.get(
						CustomerVendorOwner.class, vendorForm.getOwnerId2());
				if (owner2 != null) {
					owner2 = VendorUtil.packVendorOwner2(vendorForm, userId,
							owner2, vendorMaster);
					owner2.setModifiedby(userId);
					owner2.setModifiedon(new Date());
					session.merge(owner2);
				}
			} else {
				if (null != vendorForm.getOwnerName2()
						&& !vendorForm.getOwnerName2().isEmpty()) {
					CustomerVendorOwner owner2 = new CustomerVendorOwner();
					owner2 = VendorUtil.packVendorOwner2(vendorForm, userId,
							owner2, vendorMaster);
					owner2.setCreatedby(userId);
					owner2.setCreatedon(new Date());
					session.save(owner2);
				}
			}
			if (vendorForm.getOwnerId3() != null
					&& vendorForm.getOwnerId3() != 0) {
				CustomerVendorOwner owner3 = (CustomerVendorOwner) session.get(
						CustomerVendorOwner.class, vendorForm.getOwnerId3());
				if (owner3 != null) {
					owner3 = VendorUtil.packVendorOwner3(vendorForm, userId,
							owner3, vendorMaster);
					owner3.setModifiedby(userId);
					owner3.setModifiedon(new Date());
					session.merge(owner3);
				}
			} else {
				if (null != vendorForm.getOwnerName3()
						&& !vendorForm.getOwnerName3().isEmpty()) {
					CustomerVendorOwner owner3 = new CustomerVendorOwner();
					owner3 = VendorUtil.packVendorOwner3(vendorForm, userId,
							owner3, vendorMaster);
					owner3.setCreatedby(userId);
					owner3.setCreatedon(new Date());
					session.save(owner3);
				}
			}
			log.info("Vendor owner information saved.");
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String mergeServiceArea(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userDetails, String url,
			List<NaicsCodesDto> naicsCodes) {
		log.info("Inside the merge vendor service area method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=6 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(6);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=7 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(7);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			// update VendorNAICS codes
			List<VendorNAICS> vendorNAICSList = (List<VendorNAICS>) session
					.createCriteria(VendorNAICS.class)
					.add(Restrictions.eq("vendorId.id",
							vendorMaster.getId())).list();
			
			for(VendorNAICS naics : vendorNAICSList) {
				Query q = session.createQuery("delete from VendorNAICS where id = "+naics.getId());
				q.executeUpdate();
			}
			
			if (naicsCodes != null && !naicsCodes.isEmpty()) {
				for (NaicsCodesDto codesDto : naicsCodes) {
					NaicsCode naicsMaster = (NaicsCode) session
							.createCriteria(NaicsCode.class)
							.add(Restrictions.eq("naicsCode",
									codesDto.getNaicsCode())).uniqueResult();
					if (naicsMaster != null) {
						VendorNAICS vendorNaicsObj = null;
						vendorNaicsObj = new VendorNAICS();
						vendorNaicsObj.setVendorId(vendorMaster);
						vendorNaicsObj.setNaicsId(naicsMaster);
						vendorNaicsObj.setPrimary(codesDto.getPrimaryKey());
						vendorNaicsObj.setCapabilities(codesDto
								.getCapabilitie());
						vendorNaicsObj.setCreatedBy(userId);
						vendorNaicsObj.setCreatedOn(new Date());
						session.save(vendorNaicsObj);
					} 
				}
			}
			
			//Save or Update Vendor Keywords			
			List<CustomerVendorKeywords> vendorKeywords = (List<CustomerVendorKeywords>) session.createCriteria(CustomerVendorKeywords.class)
															.add(Restrictions.eq("vendorId", vendorMaster)).list();
			List<CustomerVendorKeywords> vendorKeywordsUI = CommonUtils.packageVendorKeywords(vendorForm, userId);
			
			if ((vendorKeywords != null && vendorKeywords.size() != 0) || (vendorKeywordsUI != null && vendorKeywordsUI.size() != 0)) {
				int noOfKeywordsInDb = vendorKeywords.size();
				int noOfKeywordsFromUI = vendorKeywordsUI.size();
				
				if (noOfKeywordsInDb != 0 || noOfKeywordsFromUI != 0) {
					for (int index = 0; index < noOfKeywordsFromUI; index++) {
						if (index < noOfKeywordsInDb) {
							CustomerVendorKeywords keywords = vendorKeywords.get(index);
							keywords.setKeyword(vendorForm.getVendorKeyword()[index]);
							keywords.setVendorId(vendorMaster);
							keywords.setModifiedOn(new Date());
							keywords.setModifiedBy(userId);
							session.update(keywords);
						} else {
							CustomerVendorKeywords keywordsFromUI = vendorKeywordsUI.get(index);							
							keywordsFromUI.setVendorId(vendorMaster);
							session.save(keywordsFromUI);
						}
					}
				}
			}
			
			if(vendorForm.getBpSegmentId() != null && vendorForm.getBpSegmentId() != 0) {
				vendorMaster.setBpSegment(vendorForm.getBpSegmentId());
				session.update(vendorMaster);
			}
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String mergeBusinessArea(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url) {
		log.info("Inside the merge vendor business area method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			
			/*vendorMaster.setCompanyInformation(StringEscapeUtils
					.escapeHtml(vendorForm.getCompanyInformation()));
			vendorMaster.setVendorDescription(StringEscapeUtils
					.escapeHtml(vendorForm.getVendorDescription()));*/
			
			//Changed escapeHtml conversion to Normal string.
			vendorMaster.setCompanyInformation(escapeHtml4(vendorForm.getCompanyInformation()));
			vendorMaster.setVendorDescription(escapeHtml4(vendorForm.getVendorDescription()));
			
			session.merge(vendorMaster);
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=7 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(7);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=8 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(8);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			log.info("Vendor business area update start");
			if (null != vendorForm.getBusinessArea()
					&& vendorForm.getBusinessArea().length != 0) {

				session.createQuery(
						" delete VendorBusinessArea where vendorId="
								+ vendorMaster.getId()).executeUpdate();
				for (String business : vendorForm.getBusinessArea()) {

					VendorBusinessArea vendorBusinessArea = new VendorBusinessArea();
					CustomerBusinessArea businessArea = (CustomerBusinessArea) session
							.get(CustomerBusinessArea.class,
									Integer.parseInt(business));
					vendorBusinessArea.setVendorId(vendorMaster);
					vendorBusinessArea.setBusinessAreaId(businessArea);
					vendorBusinessArea.setIsActive((byte) 1);
					vendorBusinessArea.setCreatedBy(userId);
					vendorBusinessArea.setCreatedOn(new Date());
					session.save(vendorBusinessArea);
				}
			} else {
				session.createQuery(
						" delete VendorBusinessArea where vendorId="
								+ vendorMaster.getId()).executeUpdate();
			}
			if (null != vendorForm.getServiceArea()
					&& vendorForm.getServiceArea().length != 0) {
				session.createQuery(
						" delete CustomerVendorServiceArea where vendorId="
								+ vendorMaster.getId()).executeUpdate();
				for (Integer area : vendorForm.getServiceArea()) {
					CustomerServiceArea serviceArea = (CustomerServiceArea) session
							.get(CustomerServiceArea.class, area);
					CustomerVendorServiceArea serviceArea2 = new CustomerVendorServiceArea();
					serviceArea2.setVendorId(vendorMaster);
					serviceArea2.setServiceAreaId(serviceArea);
					serviceArea2.setIsActive((byte) 1);
					serviceArea2.setCreatedBy(userId);
					serviceArea2.setCreatedOn(new Date());
					session.save(serviceArea2);
				}
			} else {
				session.createQuery(
						" delete CustomerVendorServiceArea where vendorId="
								+ vendorMaster.getId()).executeUpdate();
			}
			log.info("Vendor business area update success.");

			//Only BP Vendor has the Option to Save Vendor Commodities.
			/*if(vendorMaster.getVendorStatus() != null && vendorMaster.getVendorStatus().equalsIgnoreCase("B")) 
			{*/
				log.info("Vendor commodity update starts.");
				if (vendorForm.getNewCommodityCode() != null
						&& vendorForm.getNewCommodityCode().length != 0) {
					for (String commodityCode : vendorForm.getNewCommodityCode()) {
						CustomerVendorCommodity commodity = new CustomerVendorCommodity();
						CustomerCommodity comm = (CustomerCommodity) session
								.createQuery(
										" from CustomerCommodity where commodityCode='"
												+ commodityCode + "'")
								.uniqueResult();
						commodity.setVendorId(vendorMaster);
						commodity.setCommodityId(comm);
						commodity.setIsActive((byte) 1);
						commodity.setCreatedBy(userId);
						commodity.setCreatedOn(new Date());
						session.save(commodity);
					}
				}
				log.info("Vendor commodity updated successfully.");
			/*}*/			
			
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String mergeDiverseClassification(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url, List<DiverseCertificationTypesDto> certifications) {
		log.info("Inside the merge diverse classification method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();

			if (CommonUtils.getCheckboxValue(vendorForm.getDeverseSupplier()) == (byte) 1)
				vendorMaster.setDeverseSupplier((byte) 1);

			else
				vendorMaster.setDeverseSupplier((byte) 0);

			session.update(vendorMaster);

			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=5 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(5);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=6 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(6);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}

			// update Vendor Certifications
			if (CommonUtils.getCheckboxValue(vendorForm.getDeverseSupplier()) == (byte) 1) {
				session.createQuery(
						" delete CustomerDiverseClassification where vendorId="
								+ vendorMaster.getId()).executeUpdate();
				// Iterate each classifications from UI
				if (vendorForm.getCertificates() != null
						&& vendorForm.getCertificates().length > 0) {

					for (Integer cert : vendorForm.getCertificates()) {

						CustomerDiverseClassification classification1 = new CustomerDiverseClassification();
						Certificate certMaster = (Certificate) session.get(
								Certificate.class, cert);
						classification1.setCertMasterId(certMaster);
						classification1.setVendorId(vendorMaster);
						classification1.setDiverseQuality((byte) 0);
						classification1.setIsActive((byte) 1);
						classification1.setCreatedBy(userId);
						classification1.setCreatedOn(new Date());
						classification1.setModifiedBy(userId);
						classification1.setModifiedOn(new Date());
						session.save(classification1);

					}
				}

				// update Vendor Certifications
				if (certifications != null && !certifications.isEmpty()) {
					for (DiverseCertificationTypesDto certificate : certifications) {

						VendorCertificate vendorCertificate = new VendorCertificate();
						Certificate certMasterId = (Certificate) session
								.get(Certificate.class,
										certificate.getDivCertType());
						CertifyingAgency certAgencyId = null;
						if (null != certificate.getDivCertAgent()) {
							certAgencyId = (CertifyingAgency) session.get(
									CertifyingAgency.class,
									certificate.getDivCertAgent());
						}

						if (certMasterId != null && certAgencyId != null
								&& vendorMaster != null) {
							if (certificate.getId() == null) {

								vendorCertificate = VendorUtil
										.packVendorCertificate(vendorForm,
												userId, vendorCertificate,
												vendorMaster, certMasterId,
												certAgencyId, certificate);
								vendorCertificate.setCreatedBy(userId);
								vendorCertificate.setCreatedOn(new Date());
							} else {
								vendorCertificate = (VendorCertificate) session
										.get(VendorCertificate.class,
												certificate.getId());
								// update status yes to CertificateExpirationNotification table when t2 customer vendor update the certificate.
								if (vendorMaster.getIsinvited() != null && vendorMaster.getIsinvited() == 0)
								{
									if (vendorCertificate != null)
									{
										List<CertificateExpirationNotification> notificationsList = session.createQuery(
														"From CertificateExpirationNotification where vendorid=" + vendorMaster.getId()
														+ " and certificatenumber='" + vendorCertificate.getCertificateNumber() 
														+ "' and status='NO' and vendorCertificateId = "+ vendorCertificate.getId()).list();
										
										if (notificationsList != null && !notificationsList.isEmpty())
										{											
											Date date = CommonUtils.dateConvertValue(certificate.getExpDate());
											
											for(CertificateExpirationNotification notification : notificationsList)
											{
												if (date != null && date.after(notification.getExpirationdate()))
												{
													notification.setStatus("Yes");
													notification.setSubmittedby(userId);
													notification.setSubmittedon(new Date());
													session.merge(notification);
												}
											}
										}
									}
								}
								vendorCertificate = VendorUtil.packVendorCertificate(vendorForm,
												userId, vendorCertificate, vendorMaster, certMasterId, certAgencyId, certificate);
								vendorCertificate.setModifiedBy(userId);
								vendorCertificate.setModifiedOn(new Date());
							}
							session.saveOrUpdate(vendorCertificate);
						}
					}
				}
			}
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String mergeBusinessBiography(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails) {
		log.info("Inside the merge business biography and safety method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=8 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(8);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=9 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(9);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			log.info("Vendor business biography starts update ..");
			if (vendorForm.getIsBpSupplier() != null) {
				session.createQuery(
						" delete CustomerVendorbusinessbiographysafety where vendorId="
								+ vendorMaster.getId()).executeUpdate();
				VendorUtil.saveBusinessBiography(vendorForm, userId,
						vendorMaster, session);
			}
			log.info("Vendor business biography update success..");
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String mergeReferences(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userDetails) {
		log.info("Inside the merge vendor references method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		// Users custLoginId = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=9 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(9);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=10 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(10);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			if (vendorForm.getReferenceName1() != null
					&& !vendorForm.getReferenceName1().isEmpty()) {

				session.createQuery(
						" delete CustomerVendorreference where vendorId="
								+ vendorMaster.getId()).executeUpdate();

				log.info("References start updating..");

				VendorUtil.saveReferences(vendorForm, userId, vendorMaster,
						session);

				log.info("References updated successfully..");
			}
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String mergeOtherCertificate(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails) {
		log.info("Inside the merge vendor other cerificates method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=10 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(10);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=11 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(11);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			// Update Other Quality Certifications
			List<VendorOtherCertificate> vendorOtherCerts = (List<VendorOtherCertificate>) session
					.createCriteria(VendorOtherCertificate.class)
					.add(Restrictions.eq("vendorId", vendorMaster)).list();

			List<VendorOtherCertificate> vendorOtherCertsFromUI = CommonUtils
					.packageOtherCertificates(vendorForm, userId);

			if ((vendorOtherCerts != null && !vendorOtherCerts.isEmpty())
					|| (vendorOtherCertsFromUI != null && !vendorOtherCertsFromUI
							.isEmpty())) {
				// number of other certificate from DB
				int no_of_oc_db = vendorOtherCerts.size();
				// number of other certificate from UI
				int no_of_oc_ui = vendorOtherCertsFromUI.size();
				if (no_of_oc_db != 0 || no_of_oc_ui != 0) {
					for (int index = 0; index < no_of_oc_ui; index++) {
						if (index < no_of_oc_db) {
							VendorOtherCertificate certificate = vendorOtherCerts
									.get(index);
							VendorOtherCertificate certificateFromUI = vendorOtherCertsFromUI
									.get(index);
							certificate.setExpiryDate(certificateFromUI
									.getExpiryDate());
							certificate.setCertificationType(certificateFromUI
									.getCertificationType());

							if (certificateFromUI.getFilename() != null
									&& certificateFromUI.getFilename().length() != 0) {

								certificate.setContent(certificateFromUI
										.getContent());
								certificate.setContentType(certificateFromUI
										.getContentType());
								certificate.setFilename(certificateFromUI
										.getFilename());
							}
							certificate.setVendorId(vendorMaster);
							certificate.setModifiedBy(userId);
							certificate.setModifiedOn(new Date());
							session.update(certificate);
						} else {
							VendorOtherCertificate certificateFromUI = vendorOtherCertsFromUI
									.get(index);
							certificateFromUI.setVendorId(vendorMaster);
							certificateFromUI.setCreatedBy(userId);
							certificateFromUI.setCreatedOn(new Date());
							session.save(certificateFromUI);
						}
					}
				}
			}

			log.info("Quality and Other certificate updated success.");
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String mergeDocuments(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userDetails) {
		log.info("Inside the merge vendor documents method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();
			/**
			 * Update vendor menu status.
			 */
			if (null != vendorForm.getSubmitType()
					&& !vendorForm.getSubmitType().isEmpty()) {
				CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
						.createQuery(
								"From CustomerVendorMenuStatus where tab=11 and vendorId="
										+ vendorId).uniqueResult();
				menuStatus.setTab(11);
				menuStatus.setModifiedBy(userId);
				menuStatus.setModifiedOn(new Date());
				menuStatus.setVendorId(vendorMaster);
				if (vendorForm.getSubmitType().equalsIgnoreCase("save")) {
					menuStatus.setStatus(2);
				} else {
					menuStatus.setStatus(1);
					/**
					 * page submitted, hence activate next menu.
					 */
					CustomerVendorMenuStatus menuStatus1 = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab=12 and vendorId="
											+ vendorId).uniqueResult();
					menuStatus1.setTab(12);
					menuStatus1.setModifiedBy(userId);
					menuStatus1.setModifiedOn(new Date());
					menuStatus1.setVendorId(vendorMaster);
					if (menuStatus1.getStatus() == 0) {
						menuStatus1.setStatus(2);
					}
					session.merge(menuStatus1);
				}
				session.merge(menuStatus);
			}
			// Update vendor documents
			int existingDocumentSize=0;
			List<VendorDocuments> vendorDocuments = (List<VendorDocuments>) session
					.createCriteria(VendorDocuments.class)
					.add(Restrictions.eq("vendorId", vendorMaster)).list();
			String companyCode = userDetails.getCustomer().getCustCode();
			List<VendorDocuments> vendorDocumentsUI = CommonUtils
					.packageVendorDocuments(vendorForm, vendorMaster, userId,
							companyCode);
			if ((vendorDocuments != null && vendorDocuments.size() != 0)
					|| (vendorDocumentsUI != null && vendorDocumentsUI.size() != 0)) {
				int noOfDocInDb = vendorDocuments.size();
				int noOdDocFromUI = vendorDocumentsUI.size();
				if (noOfDocInDb != 0 || noOdDocFromUI != 0) {
					for (int index = 0; index < noOdDocFromUI; index++) {
						if (index < noOfDocInDb) {
							VendorDocuments documents = vendorDocuments
									.get(index);
							VendorDocuments documentsFromUI = vendorDocumentsUI
									.get(index);
							documents.setDocumentName(vendorForm
									.getVendorDocName()[index]);
							documents.setDocumentDescription(vendorForm
									.getVendorDocDesc()[index]);
							documents.setModifiedOn(new Date());
							documents.setModifiedBy(userId);
							if (vendorForm.getVendorDocs() != null
									&& vendorForm.getVendorDocs().get(index)
											.getFileSize() != 0) {
								documents.setDocumentFilename(vendorForm
										.getVendorDocs().get(index)
										.getFileName());
								documents.setDocumentType(vendorForm
										.getVendorDocs().get(index)
										.getContentType());								
								documents
										.setDocumentFilesize((double) vendorForm
												.getVendorDocs().get(index)
												.getFileSize());
								existingDocumentSize=vendorForm
										.getVendorDocs().get(index)
										.getFileSize();
								
							}
							documents.setVendorId(vendorMaster);
							String filePath = Constants
									.getString(Constants.UPLOAD_DOCUMENT_PATH)
									+ "/" + companyCode + "/" + vendorMaster.getId();
							documents.setDocumentPhysicalpath(filePath);
							session.update(documents);
						} else {
							VendorDocuments documentsFromUI = vendorDocumentsUI
									.get(index);
							//Checks File Size Must be Below the Specified Limit Otherwise it returnsError Message
							int documentMaxSize=0;
							//Gets Current Unsaved File Size
							int fileSize=vendorForm.getVendorDocs().get(index).getFileSize();
							//Adds Current File with Existing Files
							existingDocumentSize = existingDocumentSize + fileSize;
							
							//Getting Maximum File Size Allowed 
							WorkflowConfigurationDao configurationDao = new WorkflowConfigurationDaoImpl();
							WorkflowConfiguration configuration = configurationDao.listWorkflowConfig(userDetails);
							
							if(configuration != null)
							{
								vendorForm.setConfiguration(configuration);
								if(configuration.getDocumentSize() != null)
									documentMaxSize = configuration.getDocumentSize() * 1024 * 1024;
							}							
							
							if(fileSize > documentMaxSize || existingDocumentSize > documentMaxSize)
							{
								return "hugeFileSize";								
							}
							else
							{								
								documentsFromUI.setCreatedBy(userId);
								documentsFromUI.setCreatedOn(new Date());
								documentsFromUI.setVendorId(vendorMaster);
								session.save(documentsFromUI);
							}							
						}
					}
				}
			}
			log.info("Vendor documents updated success.");
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> submitRegistration(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			List<String> vendorErrors) {
		log.info("Inside the subit registration method.");
		VendorMaster vendorMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();

			/*
			 * Do the validation part in contact information page..
			 */
			List<VendorContact> preparerContacts = session.createQuery(
					"From VendorContact vc where  vc.isPreparer = 1 and vc.vendorId="
							+ vendorMaster.getId()).list();
			VendorContact businessContact = null;
			businessContact = (VendorContact) session.createQuery(
					"From VendorContact vc where vc.isBusinessContact = 1 and vc.vendorId="
							+ vendorMaster.getId()).uniqueResult();
			if (null != preparerContacts) {
				VendorContact contact = preparerContacts.get(0);
				List<String> vendorContactErrors = new ArrayList<String>();
				vendorContactErrors = SelfRegistrationValidation
						.validateContactInfo(contact, businessContact,
								vendorContactErrors);
				if (null != vendorContactErrors
						&& vendorContactErrors.size() > 0) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab = 1 and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (null != menuStatus) {
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorContactErrors);
			}

			/*
			 * Do the validation part in contact information page..
			 */
			if (null != vendorMaster) {
				List<String> vendorCompanyErrors = new ArrayList<String>();
				vendorCompanyErrors = SelfRegistrationValidation
						.validateCompanyInfo(vendorMaster, vendorCompanyErrors);
				if (null != vendorCompanyErrors
						&& vendorCompanyErrors.size() > 0) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab = 2 and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (null != menuStatus) {
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorCompanyErrors);
			}
			
			/*
			 * Do the validation part in Company Ownership information page..
			 */
			List<CustomerVendorOwner> customerVendorOwner = (List<CustomerVendorOwner>) session.createQuery("from CustomerVendorOwner where vendorid = " 
											+ vendorMaster.getId()).list();
			if (null != vendorMaster) {
				if(vendorMaster.getCompanyownership() != null && vendorMaster.getCompanyownership() == 2) {
					if(null != customerVendorOwner && customerVendorOwner.size() > 0) {
						List<String> vendorCompanyOwnershipErrors = new ArrayList<String>();
						vendorCompanyOwnershipErrors = SelfRegistrationValidation
								.validateCompanyOwnershipInfo(customerVendorOwner, vendorCompanyOwnershipErrors);
						
						if (null != vendorCompanyOwnershipErrors
								&& vendorCompanyOwnershipErrors.size() > 0) {
							CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
									.createQuery(
											"From CustomerVendorMenuStatus where tab = 4 and vendorId="
													+ vendorMaster.getId())
									.uniqueResult();
							if (null != menuStatus) {
								menuStatus.setStatus(3);
								session.merge(menuStatus);
							}
						}
						vendorErrors.addAll(vendorCompanyOwnershipErrors);
					} else {
						List<String> vendorCompanyOwnershipErrors = new ArrayList<String>();
						vendorCompanyOwnershipErrors.add("Owner Name in Company Ownership Information");
						vendorCompanyOwnershipErrors.add("Title in Company Ownership Information");
						vendorCompanyOwnershipErrors.add("Email Id in Company Ownership Information");
						vendorCompanyOwnershipErrors.add("Phone No in Company Ownership Information");
						//vendorCompanyOwnershipErrors.add("Mobile No in Company Ownership Information");
						vendorCompanyOwnershipErrors.add("Gender in Company Ownership Information");
						vendorCompanyOwnershipErrors.add("Ethnicity in Company Ownership Information");
						vendorCompanyOwnershipErrors.add("Ownership % in Company Ownership Information");
						vendorErrors.addAll(vendorCompanyOwnershipErrors);
					}
				}								
			}

			/*
			 * Do the validation part in address information page..
			 */
			CustomerVendorAddressMaster addressMaster1;
			addressMaster1 = (CustomerVendorAddressMaster) session.createQuery(
					"from CustomerVendorAddressMaster where addressType = 'P' and vendorId="
							+ vendorMaster.getId()).uniqueResult();
			if (null != vendorMaster) {
				List<String> vendorAddressErrors = new ArrayList<String>();
				if (null != addressMaster1) {
					vendorAddressErrors = SelfRegistrationValidation
							.validateAddressInfo(addressMaster1,
									vendorAddressErrors);
					
					if (addressMaster1.getCountry().isEmpty()) {
						vendorAddressErrors
								.add("Country in Company Address (Physical) Information");
					} 
					else
					{
						// Check the Country for Physical Address 
						Country addressCountry=(Country)session.createQuery("from Country c where c.id="+(Integer.parseInt(addressMaster1.getCountry()))).uniqueResult();
						String countryType = addressCountry.getProvincestate();
						if (countryType.equalsIgnoreCase("S") && addressMaster1.getState().isEmpty()) {
							vendorAddressErrors
									.add("State in Company Address (Physical) Information");
						} 
						
						if (countryType.equalsIgnoreCase("P") && addressMaster1.getProvince().isEmpty()) {
							vendorAddressErrors
									.add("Province in Company Address (Physical) Information");
						}
					}
				} 
				else {
					vendorAddressErrors
							.add("Address information (Physical) in Company Address Info.");
				}

				if (null != vendorAddressErrors
						&& vendorAddressErrors.size() > 0) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab = 3 and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (null != menuStatus) {
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorAddressErrors);
			}
			/* 
			 * Do the validation part in vendor naics information page..
			 */
			VendorNAICS naics;
			naics = (VendorNAICS) session.createQuery(
					"from VendorNAICS where primary = 1 and vendorId="
							+ vendorMaster.getId()).uniqueResult();
			if (null == naics) {
				List<String> vendorNAICSErrors = new ArrayList<String>();
				vendorNAICSErrors.add("Vendor Naics (Row 1) in service area.");

				if (null != vendorNAICSErrors && vendorNAICSErrors.size() > 0) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab = 6 and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (null != menuStatus) {
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorNAICSErrors);
			}

			/*
			 * Do the validation part in vendor naics information page..
			 */
			CustomerVendorreference vendorreference;
			vendorreference = (CustomerVendorreference) session.createQuery(
					"from CustomerVendorreference where referenceId = 1 and vendorId="
							+ vendorMaster.getId()).uniqueResult();
			List<String> vendorRefErrors = new ArrayList<String>();
			if (null == vendorreference) {
				vendorRefErrors.add("Vendor Reference1 in vendor references.");

				if (null != vendorRefErrors && vendorRefErrors.size() > 0) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab = 9 and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (null != menuStatus) {
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorRefErrors);

			} else {
				vendorRefErrors = SelfRegistrationValidation
						.validateReferenceInfo(vendorreference, vendorRefErrors);

				if (null == vendorreference.getReferenceCountry()) {
					vendorRefErrors
							.add("Country in Vendor Reference (Reference1) Information");
				} 
				else
				{
					//Check the Country for References
					Country referencecountry=(Country)session.createQuery("from Country c where c.id="+(Integer.parseInt(vendorreference.getReferenceCountry()))).uniqueResult();
					String countryType=referencecountry.getProvincestate().toString();
					if (countryType.equalsIgnoreCase("S")
							&& null == vendorreference.getReferenceState()) {
						vendorRefErrors
								.add("State in Vendor Reference (Reference1) Information");
					} 
					if (!countryType.equals("S")
							&& null == vendorreference.getReferenceProvince()) {
						vendorRefErrors
								.add("Province in Vendor Reference (Reference1) Information");
					}
				}
				if (null != vendorRefErrors && vendorRefErrors.size() > 0) {
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery(
									"From CustomerVendorMenuStatus where tab = 9 and vendorId="
											+ vendorMaster.getId())
							.uniqueResult();
					if (null != menuStatus) {
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorRefErrors);
			}

			List<CustomerVendorCommodity> commodityList = session.createQuery(
					"from CustomerVendorCommodity where  vendorId=" + vendorMaster.getId()).list();
			if (commodityList.isEmpty()) 
			{
				List<String> vendorCommoditErrors = new ArrayList<String>();	
				vendorCommoditErrors.add("Vendor Commodity (Row 1) in Business area.");
				if (null != vendorCommoditErrors && vendorCommoditErrors.size() > 0) 
				{
					CustomerVendorMenuStatus menuStatus = (CustomerVendorMenuStatus) session
							.createQuery("From CustomerVendorMenuStatus where tab = 7 and vendorId=" + vendorMaster.getId()).uniqueResult();
					if (null != menuStatus) 
					{
						menuStatus.setStatus(3);
						session.merge(menuStatus);
					}
				}
				vendorErrors.addAll(vendorCommoditErrors);
			}
			
			if (null != vendorErrors && vendorErrors.size() > 0) {
				session.getTransaction().commit();
				return vendorErrors;
			}
			vendorMaster.setVendorStatus(VendorStatus.NEWREGISTRATION
					.getIndex());
			vendorMaster.setIsPartiallySubmitted("No");
			vendorMaster.setStatusModifiedOn(new Date());
			vendorMaster.setModifiedBy(userId);
			session.merge(vendorMaster);
			session.getTransaction().commit();
			log.info("Successfully commited the transaction.");
		} catch (HibernateException ex) {
			session.getTransaction().rollback();
			log.info("Trancation rollback :" + ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorErrors;
	}

	@Override
	public String savePrimeVendorData(UserDetailsDto userDetails,
			VendorMasterForm vendorForm, Integer vendorId, Integer userId) {

		String result = "success";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		try {
			session.beginTransaction();
			VendorMaster vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("id", vendorId)).uniqueResult();

			// Update VendorMaster Company Information
			vendorMaster.setParentVendorId(0);
			vendorMaster.setVendorName(vendorForm.getVendorName());
			vendorMaster.setNumberOfEmployees(Integer.parseInt("0"));
			vendorMaster.setAnnualTurnover(CommonUtils.deformatMoney("0"));
			vendorMaster.setPrimeNonPrimeVendor(CommonUtils.getByteValue(true));
			vendorMaster.setIsApproved((byte) 1);
			vendorMaster.setApprovedOn(new Date());
			vendorMaster.setEmailId(vendorForm.getEmailId());
			vendorMaster.setIsPartiallySubmitted("No");
			vendorMaster.setDeverseSupplier((byte) 0);

			// approval process
			vendorMaster.setIsActive((byte) 1);
			vendorMaster.setIsinvited((short) 1);
			vendorMaster.setModifiedBy(userId);
			vendorMaster.setModifiedOn(new Date());
			vendorMaster.setVendorStatus(VendorStatus.BPSUPPLIER.getIndex());
			session.merge(vendorMaster);

			CustomerVendorAddressMaster addressMaster = new CustomerVendorAddressMaster();
			addressMaster.setVendorId(vendorMaster);
			addressMaster.setAddressType("p");
			addressMaster.setAddress(vendorForm.getAddress1());
			addressMaster.setCity(vendorForm.getCity());
			addressMaster.setCountry(vendorForm.getCountry());
			addressMaster.setState(vendorForm.getState());
			addressMaster.setProvince("");
			addressMaster.setPhone(vendorForm.getPhone());
			addressMaster.setZipCode(vendorForm.getZipcode());
			addressMaster.setFax(vendorForm.getFax());
			addressMaster.setIsActive((byte) 1);
			addressMaster.setCreatedBy(userId);
			addressMaster.setCreatedOn(new Date());
			session.save(addressMaster);

			// Update VendorContact Information
			List<VendorContact> contacts = session.createQuery(
					"From VendorContact vc where  vc.vendorId=" + vendorId)
					.list();
			VendorContact vendorContact = null;
			if (null != contacts) {
				vendorContact = contacts.get(0);

				VendorRoles roles = (VendorRoles) session
						.createCriteria(VendorRoles.class)
						.add(Restrictions.eq("isActive", (byte) 1)).list()
						.get(0);
				vendorContact.setVendorId(vendorMaster);
				vendorContact.setVendorUserRoleId(roles);
				vendorContact.setFirstName(vendorForm.getFirstName());
				vendorContact.setPhoneNumber(vendorForm.getContactPhone());
				vendorContact.setDesignation(vendorForm.getDesignation());
				vendorContact.setIsAllowedLogin(CommonUtils.getByteValue(true));
				vendorContact.setIsPrimaryContact((byte) 1);
				vendorContact.setIsSysGenPassword((byte) 0);

				SecretQuestion secretQns = new SecretQuestion();
				secretQns = (SecretQuestion) session.get(SecretQuestion.class,
						1);

				vendorContact.setSecretQuestionId(secretQns);
				vendorContact.setSecQueAns("");
				vendorContact.setIsActive((byte) 1);
				vendorContact.setModifiedBy(userId);
				vendorContact.setModifiedOn(new Date());
				vendorContact.setIsBusinessContact((byte) 0);
				vendorContact.setIsPreparer((byte) 1);
				vendorContact.setGender(vendorForm.getGender());
				session.merge(vendorContact);
			}
			CustomerVendorMeetingInfo vendorMeetingInfo = new CustomerVendorMeetingInfo();
			vendorMeetingInfo.setVendorId(vendorMaster);
			vendorMeetingInfo.setContactFirstName(vendorForm
					.getContactFirstName());
			vendorMeetingInfo.setContactLastName(vendorForm
					.getContactLastName());
			vendorMeetingInfo.setContactEvent(vendorForm.getContactEvent());
			vendorMeetingInfo.setContactDate(CommonUtils
					.dateConvertValue(vendorForm.getContactDate()));
			vendorMeetingInfo.setContactState(vendorForm.getContactState());
			vendorMeetingInfo.setCreatedBy(userId);
			vendorMeetingInfo.setCreatedOn(new Date());
			session.save(vendorMeetingInfo);
			log.info("Transaction commited.");

			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException ex) {
			// ex.printStackTrace();
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			ex.printStackTrace();
			return "failure";
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
			e.printStackTrace();
			return "failure";
		} finally {
			if (session.isConnected()) {
				session.close();
			}
		}

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String savePicsCertificate(VendorMasterForm vendorForm, Integer userId, Integer vendorId, UserDetailsDto userDetails) {
		log.info("Inside the Save PICS Ccerificate Method.");
		String result = "success";
		VendorMaster vendorMaster = null;
		List<VendorOtherCertificate> vendorOtherCerts = null;
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));		
		
		try {
			session.beginTransaction();
			
			vendorMaster = (VendorMaster) session.createCriteria(VendorMaster.class).add(Restrictions.eq("id", vendorId)).uniqueResult();
			
			vendorOtherCerts = (List<VendorOtherCertificate>) session.createCriteria(VendorOtherCertificate.class)
					.add(Restrictions.eq("vendorId", vendorMaster))
					.add(Restrictions.eq("isPics", 1)).list();
			
			if(null != vendorOtherCerts && !vendorOtherCerts.isEmpty()) {
				//Update
				VendorOtherCertificate certificate = vendorOtherCerts.get(0);
				certificate.setExpiryDate(CommonUtils.dateConvertValue(vendorForm.getPicsExpireDate()));
				certificate.setCertificationType(6);
				certificate.setContent(CommonUtils.convertFormFileToBlob(vendorForm.getPicsCertificate()));
				certificate.setContentType(vendorForm.getPicsCertificate().getContentType());
				certificate.setFilename(vendorForm.getPicsCertificate().getFileName());
				certificate.setVendorId(vendorMaster);
				certificate.setIsPics(1);
				certificate.setModifiedBy(userId);
				certificate.setModifiedOn(new Date());
				session.update(certificate);
			} else {
				//Save
				VendorOtherCertificate certificate1 = new VendorOtherCertificate();
				certificate1.setExpiryDate(CommonUtils.dateConvertValue(vendorForm.getPicsExpireDate()));
				certificate1.setCertificationType(6);
				certificate1.setContent(CommonUtils.convertFormFileToBlob(vendorForm.getPicsCertificate()));
				certificate1.setContentType(vendorForm.getPicsCertificate().getContentType());
				certificate1.setFilename(vendorForm.getPicsCertificate().getFileName());
				certificate1.setVendorId(vendorMaster);
				certificate1.setIsPics(1);
				certificate1.setCreatedBy(userId);
				certificate1.setCreatedOn(new Date());
				session.save(certificate1);
			}
			log.info("PICS Ccerificate Saved/Updated Success.");
			session.getTransaction().commit();
			log.info("Successfully Commited the Transaction.");
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();			
			return "failure";
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);			
			return "failure";
		} finally {
			if (session.isConnected()) {
				session.close();
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String addVendorNetWorldCertificate(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails) {
		String result = "success";
		VendorMaster vendorMaster = null;
		List<CustomerVendornetworldCertificate> cvwCertificate = null;
		String orginalName=null;
		String fileName=null;
		Boolean dbupade = true;
		Boolean hasFile = false;
		String filePath =null;
		int size=0;
		String hasNetWorldCertificate = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		if(vendorForm.getHasNetWorldCert()!=null){
			hasFile = vendorForm.getHasNetWorldCert();
		}else{
			dbupade=false;
			result="saved";
		}
		if(hasFile){
			InputStream is =null;
			OutputStream os =null;
			hasNetWorldCertificate="Yes";
			
			FormFile netWordlCertificate = vendorForm.getNetworldCert(); 
			size=netWordlCertificate.getFileSize();
			if(size>0){
			orginalName=netWordlCertificate.getFileName();
			String ext = FilenameUtils.getExtension(orginalName);
			fileName=vendorId.toString()+"certificate"+vendorForm.getVendorName().toString()+"."+ext;
			//netWordlCertificate.setFileName(fileName);
			filePath=Constants.getString(Constants.UPLOAD_DOCUMENT_PATH)+ "/"+ "VendorNetWorldcertificate" + "/" + vendorId;
			 File directory = new File(filePath);
			 if (! directory.exists()){
			        directory.mkdirs();
			    }
			
			try{
				
			
			
			File file = new File(filePath+"/"+fileName);
			os = new FileOutputStream(file);
			is = new BufferedInputStream(netWordlCertificate.getInputStream());
			int count;
			//size=netWordlCertificate.getFileSize();
			byte buf[] = new byte[size];
			 
			while ((count = is.read(buf)) > -1) {
			os.write(buf, 0, count);  
									}
			is.close(); 
			os.close();
			}catch(Exception ex){
				result = "failure";
				PrintExceptionInLogFile.printException(ex);
			}
			}else{
				dbupade=false;
				result="saved";
			}
		
		}else{
			hasNetWorldCertificate="No";
		}
		
		if(hasNetWorldCertificate!=null && dbupade){
			try
			{
			session.beginTransaction();
			vendorMaster = (VendorMaster) session.createCriteria(VendorMaster.class).add(Restrictions.eq("id", vendorId)).uniqueResult();
			cvwCertificate = ((List<CustomerVendornetworldCertificate>) session.createCriteria(CustomerVendornetworldCertificate.class).add(Restrictions.eq("vendorId", vendorMaster)).list());
			if(cvwCertificate!=null && cvwCertificate.isEmpty()){
				//save
				CustomerVendornetworldCertificate cvwCert = new CustomerVendornetworldCertificate();
				cvwCert.setVendorId(vendorMaster);
				cvwCert.setHasNetWorldCertificate(hasNetWorldCertificate);
				cvwCert.setOrginalFile(orginalName);
				cvwCert.setFileStoredPath(filePath);
				cvwCert.setFileName(fileName);
				cvwCert.setCreatedBy(userId);
				cvwCert.setCreatedOn(new Date());
				session.save(cvwCert);
			}else{
				//update
				CustomerVendornetworldCertificate cvwCert1 = cvwCertificate.get(0);
				cvwCert1.setVendorId(vendorMaster);
				cvwCert1.setHasNetWorldCertificate(hasNetWorldCertificate);
				cvwCert1.setOrginalFile(orginalName);
				cvwCert1.setFileStoredPath(filePath);
				cvwCert1.setFileName(fileName);
				cvwCert1.setModifiedBy(userId);
				cvwCert1.setModifiedOn(new Date());
				session.update(cvwCert1);
			}
			log.info("Networld Cerificate Saved/Updated Success.");
			session.getTransaction().commit();
			log.info("Successfully Commited the Transaction.");
			}catch (HibernateException ex) {
				PrintExceptionInLogFile.printException(ex);
				session.getTransaction().rollback();			
				return "failure";
			}  catch (Exception e) {
				PrintExceptionInLogFile.printException(e);			
				return "failure";
			}finally {
				if (session.isConnected()) {
					session.close();
				}
			}
			
		}
		
		
		return result;
	}
}
