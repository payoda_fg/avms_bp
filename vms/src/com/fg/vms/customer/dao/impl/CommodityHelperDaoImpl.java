/**
 * CommodityHelperDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CommodityDao;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class CommodityHelperDaoImpl implements CommodityDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerCommodityCategory getCommodity(Integer categoryId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		CustomerCommodityCategory commodityCategory = null;
		try {
			session.beginTransaction();

			commodityCategory = (CustomerCommodityCategory) session.get(
					CustomerCommodityCategory.class, categoryId);

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return commodityCategory;
	}

}
