/*
 * Tier2ReportingPeriodDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.pojo.Tier2ReportingPeriodForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The implementation class for Tier2ReportingDao interface,
 * 
 * @author vinoth
 * 
 */
public class Tier2ReportingPeriodDaoImpl implements Tier2ReportingPeriodDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String saveTier2Period(UserDetailsDto userDetails, Integer userId,
			Tier2ReportingPeriodForm periodForm) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		String result = "";
		try {
			session.beginTransaction();
			Tier2ReportingPeriod reportingPeriod1 = null;

			/* Save/Update the tier2 reporting period. */

			if (periodForm.getReportingPeriodId() != null
					&& periodForm.getReportingPeriodId() != 0) {

				log.info(" ============save tier2 reporting is starting========== ");
				/* To get reporting period object. */
				reportingPeriod1 = (Tier2ReportingPeriod) session.get(
						Tier2ReportingPeriod.class,
						periodForm.getReportingPeriodId());

				reportingPeriod1.setDataUploadFreq(periodForm
						.getFrequencyPeriod());
				reportingPeriod1.setStartDate(CommonUtils
						.dateConvertValue(periodForm.getFiscalStartDate()));
				reportingPeriod1.setEndDate(CommonUtils
						.dateConvertValue(periodForm.getFiscalEndDate()));
				session.update(reportingPeriod1);
				log.info(" ============updated tier2 reporting successfully========== ");
				result = "success";
			} else {

				log.info(" ============save tier2 reporting is starting========== ");
				Tier2ReportingPeriod reportingPeriod = new Tier2ReportingPeriod();
				reportingPeriod.setDataUploadFreq("Q");
				reportingPeriod.setStartDate(CommonUtils
						.dateConvertValue(periodForm.getFiscalStartDate()));
				reportingPeriod.setEndDate(CommonUtils
						.dateConvertValue(periodForm.getFiscalEndDate()));
				session.save(reportingPeriod);
				log.info(" ============saved tier2 reporting successfully========== ");
				result = "update";
			}

			session.getTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public Tier2ReportingPeriod currentReportingPeriod(
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Tier2ReportingPeriod reportingPeriod = null;
		try {
			session.beginTransaction();

			/* Save the email distribution details after validation. */

			log.info(" ============save tier2 reporting is starting========== ");

			List<Tier2ReportingPeriod> periods = session.createQuery(
					"From Tier2ReportingPeriod ").list();
			if (periods != null && periods.size() != 0) {
				reportingPeriod = periods.get(0);
			}

			session.getTransaction().commit();
			log.info(" ============saved tier2 reporting successfully========== ");
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportingPeriod;
	}

}
