/**
 * StateRegionDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.Region;
import com.fg.vms.customer.model.State;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class StateRegionDaoImpl<T> implements CURDDao<T> {

	@Override
	public T save(T entity, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T update(T entity, UserDetailsDto usDetails) {

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T delete(T entity, UserDetailsDto usDetails) {

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list(UserDetailsDto usDetails, String query) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<Region> regions = null;
		try {
			session.getTransaction().begin();
			regions = session.createQuery(
					"From Region where isActive=1 order by regionName").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (List<T>) regions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T update1(T entity, String name, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();

			if (name.equalsIgnoreCase("region")) {

				Region region = (Region) session.get(Region.class,
						((Region) entity).getId());
				if (region != null) {
					region.setRegionName(((Region) entity).getRegionName());
					region.setModifiedBy(((Region) entity).getModifiedBy());
					region.setModifiedOn(((Region) entity).getModifiedOn());
					session.merge(region);
				}

			}
			if (name.equalsIgnoreCase("state")) {

				State state = (State) session.get(State.class,
						((State) entity).getId());
				if (state != null) {
//					state.setStateName(((State) entity).getStateName());
//					state.setModifiedBy(((State) entity).getModifiedBy());
//					state.setModifiedOn(((State) entity).getModifiedOn());
//					session.merge(state);
				}
			}
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T delete1(T entity, String name, UserDetailsDto usDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();

			if (name.equalsIgnoreCase("region")) {

				Region region = (Region) session.get(Region.class,
						((Region) entity).getId());
				if (region != null) {
					region.setIsActive(((Region) entity).getIsActive());
					region.setModifiedBy(((Region) entity).getModifiedBy());
					region.setModifiedOn(((Region) entity).getModifiedOn());
					session.merge(region);
				}

			}
			if (name.equalsIgnoreCase("state")) {

				State state = (State) session.get(State.class,
						((State) entity).getId());
				if (state != null) {
//					state.setIsActive(((State) entity).getIsActive());
//					state.setModifiedBy(((State) entity).getModifiedBy());
//					state.setModifiedOn(((State) entity).getModifiedOn());
					session.merge(state);
				}
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@Override
	public T find(UserDetailsDto usDetails, String query) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public List<?> findAllByQuery(UserDetailsDto usDetails, String query) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public List<Ethnicity> listEthnicities(UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> listCommoditiesBasedOnSubSector(UserDetailsDto usDetails,
			Integer marketSubSectorId) {
		// TODO Auto-generated method stub
		return null;
	}

}
