/*
 * SearchDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.CountryStateDao;
import com.fg.vms.customer.dao.SearchDao;
import com.fg.vms.customer.dao.util.TupleToHeaderDataMapTransformer;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.model.CustomerCommodity;
import com.fg.vms.customer.model.CustomerSearchFilter;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.SegmentMaster;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SearchFields;

/**
 * @author pirabu
 * 
 */
public class SearchDaoImpl implements SearchDao {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	public static final String DIVERSE_CLASSIFICATION_FILTER_PLACEHOLDER = " DIVERSE_CLASSIFICATION_FILTER_PLACEHOLDER";

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
	@Override
	public List<SearchVendorDto> searchVendor(
			SearchVendorForm searchVendorForm, Integer approved, Integer from,
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		List<String> searchCriterias = new ArrayList<String>();
		List<String> whereConditionTableList = new ArrayList<String>();
		HashMap<String, ArrayList<String>> criteriaMap=new HashMap<String, ArrayList<String>>();
		ArrayList<String> list=null;
		try {
			session.beginTransaction();

			StringBuilder sqlQuery = new StringBuilder();
			StringBuilder conditions = new StringBuilder();

			sqlQuery.append("SELECT DISTINCT customer_vendormaster.ID, customer_vendormaster.CITY, customer_vendoraddressmaster.REGION, "
					+ " customer_vendormaster.DUNSNUMBER, customer_vendormaster.VENDORNAME, customer_vendormaster.EMAILID, "
					+ " (select customer_vendor_users.EMAILID from customer_vendor_users customer_vendor_users "
					+ " where customer_vendor_users.VENDORID=customer_vendormaster.ID and customer_vendor_users.ISPRIMARYCONTACT=1 "
					+ " and customer_vendor_users.ISACTIVE=1 limit 1)PrimeContactEmailId,"
					+ " (select  naics.NAICSCODE from customer_naicscode naics, customer_vendornaics "
					+ " where customer_vendornaics.NAICSID=naics.NAICSID  and customer_vendornaics.VENDORID=customer_vendormaster.ID "
					+ " and customer_vendornaics.ISPRIMARYKEY IN (1,2,3) )NAISCODE, "
					+ " (SELECT certificatemaster.CERTIFICATENAME FROM customer_certificatemaster certificatemaster, "
					+ " customer_vendordiverseclassifcation WHERE customer_vendordiverseclassifcation.CERTMASTERID = certificatemaster.ID "
					+ " AND customer_vendordiverseclassifcation.VENDORID = customer_vendormaster.ID LIMIT 1) DIVERSECLASSIFCATION, "
					+ " (SELECT certificatetype.DESCRIPTION FROM customer_certificatetype certificatetype, customer_vendorcertificate "
					+ " WHERE customer_vendorcertificate.CERTIFICATETYPE = certificatetype.ID AND customer_vendorcertificate.VENDORID = "
					+ " customer_vendormaster.ID LIMIT 1) CERTIFICATETYPE,"
					+ " country.countryname, state.STATENAME, customer_vendormaster.VENDORCODE, "
					+ " Case When customer_vendormaster.isinvited = 1 Then 'Customer' When customer_vendormaster.isinvited = 0 Then 'Self' End As Mode_of_registration, "
					+ " customer_vendormaster.createdon, CASE customer_vendormaster.VENDORSTATUS WHEN 'A' THEN 'Reviewed by Supplier Diversity' "
					+ " WHEN 'B' THEN 'BP Vendor' WHEN 'N' THEN 'New Registration' WHEN 'P' THEN 'Pending Review' END AS VENDORSTATUS, "
					+ " customer_businesstype.TypeName, IFNULL(customer_vendormaster.YEAROFESTABLISHMENT, '') YEAR, customer_vendor_users.FIRSTNAME, "
					+ " customer_vendor_users.LASTNAME, customer_vendor_users.PHONENUMBER, "
					+ " (SELECT cvn.NOTES from customer_vendornotes cvn where cvn.VENDORID = customer_vendormaster.ID ORDER BY cvn.CREATEDON DESC LIMIT 1) AS VENDORNOTES ");

			if (searchVendorForm.getPrimeVendorStatus() != null
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("P")) {
				sqlQuery.append(" , (SELECT vsl.DESCRIPTION FROM vendor_status_log vsl WHERE vsl.VENDORID = customer_vendormaster.ID ORDER BY vsl.CREATEDON DESC LIMIT 1) AS DESCRIPTION, "
						+ " (SELECT vsl.CREATEDON FROM vendor_status_log vsl WHERE vsl.VENDORID = customer_vendormaster.ID ORDER BY vsl.CREATEDON DESC LIMIT 1) AS CREATEDON, "
						+ " (SELECT customer_vendor_users.MOBILE FROM customer_vendor_users WHERE customer_vendor_users.VENDORID=customer_vendormaster.ID "
						+ " AND customer_vendor_users.ISPRIMARYCONTACT=1 AND customer_vendor_users.ISACTIVE=1 LIMIT 1) CONTACTMOBILE,"
						+ " (SELECT customer_vendor_users.FIRSTNAME FROM  customer_vendor_users WHERE customer_vendor_users.VENDORID=customer_vendormaster.ID "
						+ " AND customer_vendor_users.ISPRIMARYCONTACT=1 AND customer_vendor_users.ISACTIVE=1 LIMIT 1) CONTACTUSER");
			}

			sqlQuery.append(" From customer_vendormaster LEFT OUTER JOIN  customer_vendoraddressmaster  on customer_vendormaster.ID = "
					+ " customer_vendoraddressmaster.VENDORID and customer_vendoraddressmaster.ADDRESSTYPE = 'p' ");

			if (searchVendorForm.getPrimeVendorStatus() != null
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("P"))
				sqlQuery.append(" LEFT OUTER JOIN vendor_status_log ON vendor_status_log.VENDORID = customer_vendormaster.ID ");

			// from vendor status progress
			if (from == 1) {
				sqlQuery.append(" AND (customer_vendormaster.PARENTVENDORID=0 or customer_vendormaster.PARENTVENDORID is null) AND (customer_vendormaster.ISPARTIALLYSUBMITTED='No' or customer_vendormaster.ISPARTIALLYSUBMITTED is null) ");
			} else if (from == 0) {
				sqlQuery.append(" AND (customer_vendormaster.PARENTVENDORID=0 or customer_vendormaster.PARENTVENDORID is null) ");
			}
			sqlQuery.append(" LEFT OUTER JOIN state on state.ID=customer_vendoraddressmaster.STATE "
					+ " LEFT OUTER JOIN country ON country.Id=customer_vendoraddressmaster.COUNTRY "
					+ " LEFT OUTER JOIN customer_businesstype ON customer_businesstype.Id = customer_vendormaster.BUSINESSTYPE"
					+ " LEFT OUTER JOIN customer_vendor_sales ON customer_vendor_sales.VENDORID = customer_vendormaster.id"
					+ " LEFT OUTER JOIN customer_vendor_users ON customer_vendor_users.VENDORID = customer_vendormaster.ID "
					+ " AND customer_vendor_users.ISPRIMARYCONTACT=1 AND customer_vendor_users.ISACTIVE=1 ");

			if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("0")) {
				conditions
						.append(" and customer_vendormaster.VENDORSTATUS in ('A','B','N','P') and customer_vendormaster.ISACTIVE=1");
			} else if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("AB")) {
				conditions
						.append(" and customer_vendormaster.VENDORSTATUS IN ('A','B')"
								+ " and customer_vendormaster.ISACTIVE=1");
			} else if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("I")) {
				conditions.append(" and customer_vendormaster.VENDORSTATUS='"
						+ searchVendorForm.getPrimeVendorStatus()
						+ "' and customer_vendormaster.ISACTIVE=0");
			} else if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()) {
				conditions.append(" and customer_vendormaster.VENDORSTATUS='"
						+ searchVendorForm.getPrimeVendorStatus()
						+ "' and customer_vendormaster.ISACTIVE=1 ");
			}

			if (appDetails.getIsGlobalDivision() != 1) {
				conditions
						.append(" and customer_vendormaster.DIVSERSUPPLIER=1 ");
			}

			// Its needed only when International Mode = 0, with default country
			// selected.
			if (appDetails.getWorkflowConfiguration() != null) {
				if (appDetails.getWorkflowConfiguration()
						.getInternationalMode() != null) {
					if (appDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
						List<Country> country = (List<Country>) session
								.createQuery("From Country where isDefault=1")
								.list();
						if (country.get(0).getId() != null) {
							conditions
									.append(" and customer_vendoraddressmaster.COUNTRY = "
											+ country.get(0).getId() + " ");
						}
					}
				}
			}

			// To get the current user division wise result
			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					conditions
							.append(" AND customer_vendormaster.CUSTOMERDIVISIONID in("
									+ divisionIds.toString() + ") ");
				}
				// Changed customerDivisionId to list of CustomerDivisionIds
				/*
				 * if (appDetails.getCustomerDivisionID() != null &&
				 * appDetails.getCustomerDivisionID() != 0 &&
				 * appDetails.getIsGlobalDivision() == 0) { conditions.append(
				 * " AND customer_vendormaster.CUSTOMERDIVISIONID='" +
				 * appDetails.getCustomerDivisionID() + "'"); }
				 */
			}
			
			if(searchVendorForm.getMaxRevenueRange() != null && searchVendorForm.getMinRevenueRange() != null){
				if(searchVendorForm.getMaxRevenueRange() != 0.0d && searchVendorForm.getMinRevenueRange() != 0.0d){
				conditions.append(" AND customer_vendor_sales.YEARSALES between "+searchVendorForm.getMinRevenueRange()+" and "+searchVendorForm.getMaxRevenueRange()+" ");
				searchCriterias.add("Revenue Range" + ": " +searchVendorForm.getMinRevenueRange()+ ","+searchVendorForm.getMaxRevenueRange()+" ");
				list=new ArrayList<String>();
				list.add(searchVendorForm.getMinRevenueRange().toString());
				list.add(searchVendorForm.getMaxRevenueRange().toString());
				criteriaMap.put("Revenue Range", list);
				}
			}
			
			if(searchVendorForm.getEstablishedYear() != null && searchVendorForm.getYearRelationId() != null){
				if(searchVendorForm.getEstablishedYear() != 0 && searchVendorForm.getYearRelationId() != ""){
				conditions.append(" AND customer_vendormaster.YEAROFESTABLISHMENT "+searchVendorForm.getYearRelationId()+" "+searchVendorForm.getEstablishedYear()+" ");
				searchCriterias.add("Year Of Establishment" + ": " +searchVendorForm.getYearRelationId()+ ","+searchVendorForm.getEstablishedYear()+" ");
				list=new ArrayList<String>();
				list.add(searchVendorForm.getYearRelationId().toString());
				list.add(searchVendorForm.getEstablishedYear().toString());
				criteriaMap.put("Year Of Establishment", list);
				}
			}

			int index = 0;

			boolean flag = false;
			boolean isNaics = false;
			boolean isContact = false;
			boolean isMeetingInfo = false;
			boolean isVendorCommodityInfo = false;

			for (String option : searchVendorForm.getSearchFields()) {
				String searchFieldValue = null;
				if (searchVendorForm.getSearchFieldsValue() != null
						&& searchVendorForm.getSearchFieldsValue()[index] != null
						&& !searchVendorForm.getSearchFieldsValue()[index]
								.isEmpty()) {
					searchFieldValue = CommonUtils.escape(searchVendorForm
							.getSearchFieldsValue()[index]);
				}
				index++;

				if (searchFieldValue != null) {
					switch (SearchFields.valueOf(option)) {
					
					case RFIRFPNOTE:
						conditions
								.append(" and customer_vendormaster.RFIRFPNOTES LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;

					case LEGALCOMPANYNAME:
						conditions
								.append(" and customer_vendormaster.VENDORNAME LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case COMPANYCODE:
						conditions
								.append(" and customer_vendormaster.VENDORCODE ='"
										+ searchFieldValue + "'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case DUNSNUMBER:
						conditions
								.append(" and customer_vendormaster.DUNSNUMBER LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case TAXID:
						conditions
								.append(" and customer_vendormaster.TAXID = '"
										+ searchFieldValue + "'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case NUMBEROFEMPLOYEES:
						conditions
								.append(" and customer_vendormaster.NUMBEROFEMPLOYEES= "
										+ searchFieldValue + "");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;

					case COMPANYTYPE:
						sqlQuery.append(" inner join customer_legal_structure on customer_legal_structure.id=customer_vendormaster.COMPANY_TYPE ");
						conditions
								.append(" and customer_legal_structure.name LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case YEARESTABLISHED:
						conditions
								.append(" and customer_vendormaster.YEAROFESTABLISHMENT = "
										+ searchFieldValue);
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case ADDRESS:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}

						conditions
								.append(" and customer_vendoraddressmaster.ADDRESS LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case CITY:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.CITY LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case STATE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.STATE = "
										+ searchFieldValue + "");
						State state = getState(session,
								Integer.parseInt(searchFieldValue));
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + state.getStatename() + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case PROVINCE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.PROVINCE LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case REGION:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.REGION LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case COUNTRY:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						StringBuilder builder = new StringBuilder();
						if (searchVendorForm.getSelectCountries() != null
								&& searchVendorForm.getSelectCountries().length != 0) {
							list=new ArrayList<String>();
							for (String country : searchVendorForm
									.getSelectCountries()) {

								Country countryies = getCountry(session,
										Integer.parseInt(country));
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ countryies.getCountryname() + " ");
								
								list.add(countryies.getCountryname());
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
								builder.append("'" + country + "',");
							}
							builder.deleteCharAt(builder.length() - 1);
							conditions
									.append(" and customer_vendoraddressmaster.COUNTRY IN ("
											+ builder.toString() + ")");
						}

						break;
					case ZIPCODE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.ZIPCODE = '"
										+ searchFieldValue + "'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case MOBILE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.MOBILE LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;

					case PHONE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendormaster.PHONE = '"
										+ searchFieldValue + "'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case FAX:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.FAX = '"
										+ searchFieldValue + "'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case WEBSITEURL:
						conditions
								.append(" and customer_vendormaster.WEBSITE LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case EMAIL:
						conditions
								.append(" and customer_vendormaster.EMAILID LIKE '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						list=new ArrayList<String>();
						list.add(searchFieldValue);
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;
					case VENDORCLASSIFICATION:
						conditions
								.append(" and customer_vendormaster.PRIMENONPRIMEVENDOR="
										+ searchFieldValue);
						if (searchFieldValue.equalsIgnoreCase("0")) {
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": Non-Prime ");
							list=new ArrayList<String>();
							list.add("Non-Prime");
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						} else {
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": Prime ");
							list=new ArrayList<String>();
							list.add("Prime");
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						}
						break;
					case NAICSCODE:
						isNaics = true;
						break;
					case NAICSDESC:
						isNaics = true;
						break;
					case CAPABILITIES:
						isNaics = true;
						break;
					case DIVERSECLASSIFICATION:
						flag = true;
						break;
					case CERTIFYINGAGENCY:
						flag = true;
						break;
					case CERTIFICATION:
						flag = true;
						break;
					case EFFECTIVEDATE:
						flag = true;
						break;
					case EXPIREDATE:
						flag = true;
						break;
					case CERTIFICATIONTYPE:
						flag = true;
						break;
					case FIRSTNAME:
						isContact = true;
						break;
					case LASTNAME:
						isContact = true;
						break;
					case DESIGNATION:
						isContact = true;
						break;
					case CONTACTPHONE:
						isContact = true;
						break;
					case CONTACTMOBILE:
						isContact = true;
						break;
					case CONTACTFAX:
						isContact = true;
						break;
					case CONTACTEMAIL:
						isContact = true;
						break;
					case DIVERSESUPPLIER:
						conditions
								.append(" and customer_vendormaster.DIVSERSUPPLIER="
										+ searchFieldValue);
						if (searchFieldValue.equalsIgnoreCase("0")) {
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": Non Diverse ");
							list=new ArrayList<String>();
							list.add("Non Diverse");
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						} else {
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": Diverse ");
							list=new ArrayList<String>();
							list.add("Diverse");
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						}
						break;
					case BUSINESSAREA:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						StringBuilder businessAreas = new StringBuilder();
						StringBuilder busnessAreaNames = new StringBuilder();
						if (searchVendorForm.getBusinessAreaName() != null
								&& searchVendorForm.getBusinessAreaName().length != 0) {
							list=new ArrayList<String>();
							for (String area : searchVendorForm
									.getBusinessAreaName()) {
								businessAreas.append("'" + area + "',");

								busnessAreaNames.append(getBusinessAreanName(
										session, Integer.parseInt(area)));
								busnessAreaNames.append(", ");
								list.add(area);
							}
							businessAreas
									.deleteCharAt(businessAreas.length() - 1);
							busnessAreaNames.deleteCharAt(busnessAreaNames
									.length() - 2);
							sqlQuery.append(" inner join vendor_businessarea  on vendor_businessarea.vendorid=customer_vendormaster.id ");
							conditions
									.append(" and vendor_businessarea.BUSINESSAREAID in ("
											+ businessAreas.toString() + ")");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc()
									+ ": "
									+ busnessAreaNames.toString() + " ");
							
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						}
						break;
					case COMMODITYDESCRIPTION:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						StringBuilder commodityDescription = new StringBuilder();
						StringBuilder commodityDescriptions = new StringBuilder();
						if (searchVendorForm.getCommodityDescription() != null
								&& searchVendorForm.getCommodityDescription().length != 0) {
							list=new ArrayList<String>();
							for (String commodity : searchVendorForm
									.getCommodityDescription()) {
								commodityDescription.append("'" + commodity
										+ "',");
								commodityDescriptions
										.append(getCommodityDescription(
												session,
												Integer.parseInt(commodity)));
								commodityDescriptions.append(", ");
								list.add(commodity);
							}
							commodityDescription
									.deleteCharAt(commodityDescription.length() - 1);
							commodityDescriptions
									.deleteCharAt(commodityDescriptions
											.length() - 2);
							sqlQuery.append(" inner join customer_vendorcommodity on customer_vendorcommodity.vendorid=customer_vendormaster.id ");
							conditions
									.append(" and customer_vendorcommodity.COMMODITYID in ("
											+ commodityDescription.toString()
											+ ")");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc()
									+ ": "
									+ commodityDescriptions.toString() + " ");
							
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						}
						break;
					case KEYWORD:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						sqlQuery.append(" inner join customer_vendorkeywords on customer_vendorkeywords.VENDORID = customer_vendormaster.id ");
						String[] keywords = searchFieldValue.split(",");
						list=new ArrayList<String>();
						for (int ctr = 0; ctr < keywords.length; ctr++) {
							if (ctr == 0)
								conditions.append(" AND ( ");
							else
								conditions.append(" OR ");
							conditions
									.append("customer_vendorkeywords.KEYWORD LIKE '%"
											+ keywords[ctr].trim() + "%' ");
							list.add(keywords[ctr].trim());
						}
						conditions.append(") ");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						
						criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						break;

					case CONTACTFIRSTNAME:
						isMeetingInfo = true;
						break;

					case CONTACTLASTNAME:
						isMeetingInfo = true;
						break;

					case CONTACTDATE:
						isMeetingInfo = true;
						break;

					case CONTACTSTATE:
						isMeetingInfo = true;
						break;

					case CONTACTEVENT:
						isMeetingInfo = true;
						break;

					case BPSEGMENT:
						StringBuilder bpSegment = new StringBuilder();
						StringBuilder bpSegments = new StringBuilder();
						if (searchVendorForm.getBpSegmentIds() != null
								&& searchVendorForm.getBpSegmentIds().length != 0) {
							list=new ArrayList<String>();
							for (String segment : searchVendorForm
									.getBpSegmentIds()) {
								bpSegment.append("'" + segment + "',");
								bpSegments.append(getBpSegment(session,
										Integer.parseInt(segment)));
								bpSegments.append(", ");
								list.add(segment);
							}
							bpSegment.deleteCharAt(bpSegment.length() - 1);
							bpSegments.deleteCharAt(bpSegments.length() - 2);
							conditions
									.append(" and customer_vendormaster.BPSEGMENT in ("
											+ bpSegment.toString() + ")");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc()
									+ ": "
									+ bpSegments.toString()
									+ " ");
							
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						}
						break;

					case OILANDGASINDUSTRYEXPERIENCE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						sqlQuery.append(" INNER JOIN customer_vendorbusinessbiographysafety ON "
								+ " customer_vendorbusinessbiographysafety.VENDORID = customer_vendormaster.ID "
								+ " AND customer_vendorbusinessbiographysafety.QUESTIONNUMBER = 1 "
								+ " AND customer_vendorbusinessbiographysafety.SEQUENCE = 1 ");
						conditions
								.append(" AND customer_vendorbusinessbiographysafety.ANSWER = "
										+ searchFieldValue);
						if (searchFieldValue.equalsIgnoreCase("1")) {
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": Yes ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						} else {
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": No ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
						}
						break;

					case VENDORCOMMODITY:
						isVendorCommodityInfo = true;
						break;

					case BPMARKETSECTOR:
						isVendorCommodityInfo = true;
						break;

					default:
						break;
					}
				}
			}
			if (flag) {

				sqlQuery.append(" INNER JOIN customer_vendordiverseclassifcation ON customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID "
						+ "AND customer_vendordiverseclassifcation.ACTIVE=1 INNER JOIN customer_vendorcertificate ON customer_vendorcertificate.VENDORID=customer_vendormaster.id and "
						+ " customer_vendordiverseclassifcation.CERTMASTERID=customer_vendorcertificate.CERTMASTERID and customer_vendorcertificate.VENDORID=customer_vendordiverseclassifcation.VENDORID"
						+ " INNER JOIN customer_classification_certificatetypes ON customer_vendorcertificate.CERTMASTERID="
						+ "	customer_classification_certificatetypes.CERTIFICATEID and customer_vendordiverseclassifcation.CERTMASTERID=customer_classification_certificatetypes.CERTIFICATEID"
						+ " INNER JOIN customer_certificateagencymaster  ON customer_certificateagencymaster.ID=customer_vendorcertificate.CERTAGENCYID");

				/*
				 * sqlQuery.append(
				 * " INNER JOIN  customer_vendordiverseclassifcation ON customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID "
				 * +
				 * " AND customer_vendordiverseclassifcation.ACTIVE=1 INNER JOIN customer_certificatemaster certificatemaster"
				 * +
				 * " ON customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID AND certificatemaster.ID=customer_vendordiverseclassifcation.CERTMASTERID INNER JOIN  customer_vendorcertificate customer_vendorcertificate ON "
				 * +
				 * " certificatemaster.ID=customer_vendorcertificate.CERTMASTERID INNER JOIN customer_certificateagencymaster customer_certificateagencymaster "
				 * +
				 * " ON customer_certificateagencymaster.ID=customer_vendorcertificate.CERTAGENCYID INNER JOIN customer_classification_certificatetypes customer_classification_certificatetypes ON "
				 * +
				 * "certificatemaster.ID=customer_classification_certificatetypes.CERTIFICATEID INNER JOIN customer_certificatetype certificatetype ON "
				 * +
				 * "certificatetype.ID=customer_classification_certificatetypes.CERTIFICATETYPEID AND customer_vendorcertificate.CERTMASTERID=certificatemaster.ID "
				 * );
				 */
				sqlQuery.append("  ");
				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;
					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case DIVERSECLASSIFICATION:

							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable2().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable2().trim());
							}
							StringBuilder certificateNames = new StringBuilder();
							if (searchVendorForm.getDiverseCertificateNames() != null
									&& searchVendorForm
											.getDiverseCertificateNames().length != 0) {
								list=new ArrayList<String>();
								for (String clasification : searchVendorForm
										.getDiverseCertificateNames()) {
									certificateNames.append(getCerification(
											session,
											Integer.parseInt(clasification)));
									certificateNames.append(", ");
									list.add(clasification);
								}
								certificateNames.deleteCharAt(certificateNames
										.length() - 2);
								conditions
										.append(" and customer_vendordiverseclassifcation.CERTMASTERID IN ("
												+ StringUtils.join(
														searchVendorForm
																.getDiverseCertificateNames(),
														",") + ") ");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ certificateNames.toString() + " ");
								
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							break;
						case CERTIFYINGAGENCY:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable2().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable2().trim());
							}
							StringBuilder certifyAgencies = new StringBuilder();
							if (searchVendorForm.getCertifyAgencies() != null
									&& searchVendorForm.getCertifyAgencies().length != 0) {
								list=new ArrayList<String>();
								for (String agency : searchVendorForm
										.getCertifyAgencies()) {
									certifyAgencies.append(getAgency(session,
											Integer.parseInt(agency)));
									certifyAgencies.append(", ");
									list.add(agency);
								}
								certifyAgencies.deleteCharAt(certifyAgencies
										.length() - 2);
								conditions
										.append(" and customer_certificateagencymaster.ID IN ("
												+ StringUtils.join(
														searchVendorForm
																.getCertifyAgencies(),
														",") + ") ");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ certifyAgencies.toString() + " ");
								
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							break;
						case CERTIFICATION:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendorcertificate.CERTIFICATENUMBER ='"
											+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case EFFECTIVEDATE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendorcertificate.EFFECTIVEDATE=' "
											+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;

						case EXPIREDATE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendorcertificate.EXPIRYDATE ='"
											+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case CERTIFICATIONTYPE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable2().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable2().trim());
							}
							StringBuilder certificationType = new StringBuilder();
							StringBuilder typeName = new StringBuilder();
							if (searchVendorForm.getCertificationType() != null
									&& searchVendorForm.getCertificationType().length != 0) {
								list=new ArrayList<String>();
								for (String certificate : searchVendorForm
										.getCertificationType()) {
									certificationType.append("'" + certificate
											+ "',");
									typeName.append(getCertificationType(
											session,
											Integer.parseInt(certificate)));
									typeName.append(", ");
									list.add(certificate);
								}
								certificationType
										.deleteCharAt(certificationType
												.length() - 1);
								typeName.deleteCharAt(typeName.length() - 2);
								conditions
										.append(" and customer_classification_certificatetypes.CERTIFICATETYPEID in ("
												+ certificationType.toString()
												+ ")");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ typeName.toString() + " ");
								
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							break;

						default:
							break;
						}
					}
				}
			}
			if (isNaics) {
				sqlQuery.append(" INNER JOIN customer_vendornaics  on customer_vendornaics.VENDORID=customer_vendormaster.ID "
						+ "INNER JOIN customer_naicscode  on customer_vendornaics.NAICSID=customer_naicscode.NAICSID and customer_vendornaics.ISPRIMARYKEY IN (0,1,2,3) ");

				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;
					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case NAICSCODE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable2().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable2().trim());
							}
							StringBuilder naicsCodes = new StringBuilder();
							StringBuilder codes = new StringBuilder();
							if (searchVendorForm.getNaicsCode() != null
									&& searchVendorForm.getNaicsCode().length != 0) {

								String naics[] = searchVendorForm
										.getNaicsCode();
								String naics1[] = naics[0].toString()
										.split(",");
								for (String naicsCode : naics1) {
									codes.append(naicsCode + ",");
									naicsCodes.append("'" + naicsCode + "',");
								}
								naicsCodes
										.deleteCharAt(naicsCodes.length() - 1);
								codes.deleteCharAt(codes.length() - 1);
								conditions
										.append(" and customer_naicscode.NAICSCODE  in ("
												+ naicsCodes.toString() + ") ");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ codes.toString() + " ");
								list=new ArrayList<String>();
								list.add(codes.toString());
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							break;
						case NAICSDESC:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable2().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable2().trim());
							}
							StringBuilder naicsDescriptions = new StringBuilder();
							StringBuilder descriptions = new StringBuilder();

							if (searchVendorForm.getNaicsDescription() != null
									&& searchVendorForm.getNaicsDescription().length != 0) {

								String naics[] = searchVendorForm
										.getNaicsDescription();
								String naicsDesc[] = naics[0].toString().split(
										",");

								for (String description : naicsDesc) {
									if (description.startsWith(" ")) {
										description = description.substring(1);
									}
									descriptions.append(description + ",");
									naicsDescriptions.append("'" + description
											+ "',");
								}
								naicsDescriptions
										.deleteCharAt(naicsDescriptions
												.length() - 1);
								descriptions
										.deleteCharAt(descriptions.length() - 1);
								conditions
										.append(" and customer_naicscode.NAICSDESCRIPTION  in ("
												+ naicsDescriptions.toString()
												+ ") ");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ descriptions.toString() + " ");
								list=new ArrayList<String>();
								list.add(descriptions.toString());
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							/*
							 * sqlQuery.append(" and naics.NAICSDESCRIPTION LIKE '%"
							 * + searchFieldValue + "%'");
							 */
							break;

						case CAPABILITIES:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendornaics.CAPABILITIES LIKE '%"
											+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						default:
							break;
						}
					}
				}
			}
			if (isContact) {
				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;
					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case FIRSTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.FIRSTNAME LIKE '%"
											+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case LASTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.LASTNAME LIKE '%"
											+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case DESIGNATION:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.DESIGNATION LIKE '%"
											+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case CONTACTPHONE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.PHONENUMBER = '"
											+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case CONTACTMOBILE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.MOBILE = '"
											+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case CONTACTFAX:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.FAX = '"
											+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						case CONTACTEMAIL:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.EMAILID LIKE '%"
											+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;
						default:
							break;
						}
					}
				}
			}

			if (isMeetingInfo) {
				sqlQuery.append(" INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id ");

				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;

					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case CONTACTFIRSTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTFIRSTNAME LIKE '%"
											+ searchFieldValue + "%' ");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;

						case CONTACTLASTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTLASTNAME LIKE '%"
											+ searchFieldValue + "%' ");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;

						case CONTACTDATE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTDATE BETWEEN '"
											+ CommonUtils
													.convertDateToDBFormat(searchFieldValue)
											+ " 00:00:00' AND '"
											+ CommonUtils
													.convertDateToDBFormat(searchFieldValue)
											+ " 23:59:59'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;

						case CONTACTSTATE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTSTATE = "
											+ searchFieldValue);
							State state = getState(session,
									Integer.parseInt(searchFieldValue));
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc()
									+ ": "
									+ state.getStatename()
									+ " ");
							list=new ArrayList<String>();
							list.add(state.getId().toString());
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;

						case CONTACTEVENT:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTEVENT LIKE '%"
											+ searchFieldValue + "%' ");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							list=new ArrayList<String>();
							list.add(searchFieldValue);
							criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							break;

						default:
							break;
						}
					}
				}
			}

			if (isVendorCommodityInfo) {
				sqlQuery.append(" INNER JOIN customer_vendorcommodity cvcinfo ON cvcinfo.VENDORID = customer_vendormaster.ID ");

				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;

					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case VENDORCOMMODITY:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							StringBuilder vendorCommodity = new StringBuilder();
							StringBuilder vendorCommodities = new StringBuilder();
							if (searchVendorForm.getVendorCommodity() != null
									&& searchVendorForm.getVendorCommodity().length != 0) {
								list=new ArrayList<String>();
								for (String commodity : searchVendorForm
										.getVendorCommodity()) {
									vendorCommodity.append("'" + commodity
											+ "',");
									vendorCommodities
											.append(getVendorCommodity(session,
													Integer.parseInt(commodity)));
									vendorCommodities.append(", ");
									list.add(commodity);
								}
								vendorCommodity.deleteCharAt(vendorCommodity
										.length() - 1);
								vendorCommodities
										.deleteCharAt(vendorCommodities
												.length() - 2);
								conditions
										.append(" AND cvcinfo.COMMODITYID IN ("
												+ vendorCommodity.toString()
												+ ")");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ vendorCommodities.toString() + " ");
								
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							break;

						case BPMARKETSECTOR:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							StringBuilder bpMarketSector = new StringBuilder();
							StringBuilder bpMarketSectors = new StringBuilder();
							if (searchVendorForm.getBpMarketSectorId() != null
									&& searchVendorForm.getBpMarketSectorId().length != 0) {
								list=new ArrayList<String>();
								for (String marketSector : searchVendorForm
										.getBpMarketSectorId()) {
									bpMarketSector.append("'" + marketSector
											+ "',");
									bpMarketSectors.append(getBPMarketSector(
											session,
											Integer.parseInt(marketSector)));
									bpMarketSectors.append(", ");
									list.add(marketSector);
								}
								bpMarketSector.deleteCharAt(bpMarketSector
										.length() - 1);
								bpMarketSectors.deleteCharAt(bpMarketSectors
										.length() - 2);
								sqlQuery.append(" INNER JOIN customer_commodity ccinfo ON ccinfo.ID = cvcinfo.COMMODITYID "
										+ "INNER JOIN customer_commoditycategory cccinfo ON cccinfo.ID = ccinfo.COMMODITYCATEGORYID "
										+ "INNER JOIN customer_marketsector cmsinfo ON cmsinfo.ID = cccinfo.MARKETSECTORID ");
								conditions.append(" AND cmsinfo.ID IN ("
										+ bpMarketSector.toString() + ")");
								searchCriterias.add(SearchFields
										.valueOf(option).getDesc()
										+ ": "
										+ bpMarketSectors.toString() + " ");
								
								criteriaMap.put(SearchFields.valueOf(option).getDesc(), list);
							}
							break;

						default:
							break;
						}
					}
				}
			}
				
			//Convert criteriaMap to json
			ObjectMapper mapper = new ObjectMapper();
			String criteriaString=mapper.writeValueAsString(criteriaMap);
			searchVendorForm.setCriteriaJsonString(criteriaString);
			System.out.println(criteriaString);
			// Replace this place holder with diverse classification filter
			// condition.
			// If diverse classification filter condition is not required then
			// replace the place holder with empty string.
			sqlQuery.append(SearchDaoImpl.DIVERSE_CLASSIFICATION_FILTER_PLACEHOLDER);

			searchVendorForm.setSearchConditions(conditions.toString());
			searchVendorForm.setFromtableForConditions(whereConditionTableList);
			sqlQuery.append(" where 1=1 ").append(conditions);
			sqlQuery.append(" ORDER BY customer_vendormaster.createdon ASC");

			// Sets Query for NonNMSDCWBENC Filter
			searchVendorForm.setNonNMSDCWBENCQuery(sqlQuery.toString());

			// Replacing DIVERSE_CLASSIFICATION_FILTER_PLACEHOLDER with Empty
			// String for Search
			StringBuilder newSqlQuery = new StringBuilder(
					sqlQuery.toString()
							.replace(
									SearchDaoImpl.DIVERSE_CLASSIFICATION_FILTER_PLACEHOLDER,
									""));

			searchVendorForm.setSearchQuery(newSqlQuery.toString());
			System.out.println("Search query in Search Dao Impl @ 1025: "
					+ newSqlQuery.toString());
			logger.info("Query in Search Vendor:" + newSqlQuery.toString());
			SQLQuery query = session.createSQLQuery(newSqlQuery.toString());

			List vendors = query.list();
			Iterator iterator;

			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;
					String statusNote = null;
					String statusDate = null;
					String customerMobile = null;
					String contactUser = null;
					String diverseClassifcation = null;
					String certificateType = null;
					String businessType = null;
					String yearOfEstd = null;
					String firstName = null;
					String lastName = null;
					String phoneNumber = null;
					String vendorNotes;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					diverseClassifcation = (String) searchVendors[8];
					certificateType = (String) searchVendors[9];
					countryName = (String) searchVendors[10];
					stateName = (String) searchVendors[11];
					vendorCode = (String) searchVendors[12];
					modeOfReg = (String) searchVendors[13];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[14]);
					vendorStatus = (String) searchVendors[15];
					businessType = (String) searchVendors[16];
					yearOfEstd = (String) searchVendors[17];
					firstName = (String) searchVendors[18];
					lastName = (String) searchVendors[19];
					phoneNumber = (String) searchVendors[20];

					if (searchVendors[21] != null && searchVendors[21] != "") {
						vendorNotes = (String) searchVendors[21];
					} else {
						vendorNotes = "None";
					}

					if (searchVendorForm.getPrimeVendorStatus() != null
							&& searchVendorForm.getPrimeVendorStatus()
									.equalsIgnoreCase("P")) {
						statusNote = (String) searchVendors[22];
						statusDate = CommonUtils
								.convertDateToString((Date) searchVendors[23]);
						customerMobile = (String) searchVendors[24];
						contactUser = (String) searchVendors[25];
					}
					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, statusNote, statusDate, customerMobile,
							contactUser, diverseClassifcation, certificateType,
							businessType, yearOfEstd, firstName, lastName,
							phoneNumber, vendorNotes);
					vendorsList.add(vendorDto);
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			System.out.println("Exception: " + e);
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		searchVendorForm.setSearchCriterias(searchCriterias);
		return vendorsList;
	}

	private String getCertificationType(Session session, int id) {
		CustomerCertificateType type = (CustomerCertificateType) session.get(
				CustomerCertificateType.class, id);
		return type.getCertificateTypeDesc();
	}

	private String getAgency(Session session, int id) {
		CertifyingAgency agency = (CertifyingAgency) session.get(
				CertifyingAgency.class, id);
		return agency.getAgencyName();
	}

	private String getCerification(Session session, int id) {
		Certificate classification = (Certificate) session.get(
				Certificate.class, id);
		return classification.getCertificateName();
	}

	private String getCommodityDescription(Session session, int id) {
		CustomerCommodity commodity = (CustomerCommodity) session.get(
				CustomerCommodity.class, id);
		return commodity.getCommodityDescription();
	}

	private String getVendorCommodity(Session session, int id) {
		CustomerCommodity commodity = (CustomerCommodity) session.get(
				CustomerCommodity.class, id);
		return commodity.getCommodityDescription();
	}

	private String getBusinessAreanName(Session session, Integer id) {
		CustomerBusinessArea bArea = (CustomerBusinessArea) session.get(
				CustomerBusinessArea.class, id);
		return bArea.getAreaName();
	}

	private Country getCountry(Session session, int countryId) {
		return (Country) session.get(Country.class, countryId);
	}

	private State getState(Session session, int stateId) {
		return (State) session.get(State.class, stateId);
	}

	private String getBpSegment(Session session, int id) {
		SegmentMaster segment = (SegmentMaster) session.get(
				SegmentMaster.class, id);
		return segment.getSegmentName();
	}

	private String getBPMarketSector(Session session, int id) {
		MarketSector marketSector = (MarketSector) session.get(
				MarketSector.class, id);
		return marketSector.getSectorDescription();
	}

	/**
	 * Implementing code for vendor searching by provided form fields
	 */
	@Override
	public List<SearchVendorDto> searchTire2Vendor(
			SearchVendorForm searchVendorForm, Integer vendorId,
			Integer approved, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		Iterator iterator = null;
		String joinedString = StringUtils.join(
				searchVendorForm.getDiverseCertificateNames(), ",");
		try {
			session.beginTransaction();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("select vendor.ID,vendor.CITY,vendor.REGION,vendor.DUNSNUMBER,vendor.VENDORNAME, "
					+ "(select  naics.NAICSCODE from customer_naicscode naics, customer_vendornaics vendorNaics"
					+ " where vendorNaics.NAICSID=naics.NAICSID  and vendorNaics.VENDORID=vendor.ID and vendorNaics.ISPRIMARYKEY IN (1,2,3) )NAISCODE, ");

			sqlQuery.append(" country.countryname, state.STATENAME from customer_vendormaster vendor inner join customer_vendoraddressmaster vaddr on ");

			sqlQuery.append("vendor.ID=vaddr.VENDORID left outer join state on vaddr.STATE=state.ID inner join country on vaddr.COUNTRY=country.Id and ");
			sqlQuery.append("vaddr.ADDRESSTYPE='p' and vendor.PARENTVENDORID=:parentVendorID and vendor.ISACTIVE=1 and vendor.isinvited=1  ");

			if ((searchVendorForm.getNaicCode() != null && searchVendorForm
					.getNaicCode().length() != 0)
					|| searchVendorForm.getNaicsDesc() != null
					&& searchVendorForm.getNaicsDesc().length() != 0) {
				sqlQuery.append("inner join customer_vendornaics vendorNaics on vendorNaics.VENDORID=vendor.ID inner join"
						+ " customer_naicscode naics on vendorNaics.NAICSID=naics.NAICSID and vendorNaics.ISPRIMARYKEY IN (1,2,3)");

			}
			if (searchVendorForm.getNaicCode() != null
					&& searchVendorForm.getNaicCode().length() != 0) {
				sqlQuery.append(" and naics.NAICSCODE ='"
						+ searchVendorForm.getNaicCode() + "'");
			}

			if (searchVendorForm.getNaicsDesc() != null
					&& searchVendorForm.getNaicsDesc().length() != 0) {
				sqlQuery.append(" and naics.NAICSDESCRIPTION LIKE '%"
						+ searchVendorForm.getNaicsDesc() + "%'");

			}

			if (searchVendorForm.getVendorName() != null
					&& !searchVendorForm.getVendorName().isEmpty()) {
				sqlQuery.append(" and vendor.VENDORNAME LIKE '%"
						+ CommonUtils.escape(searchVendorForm.getVendorName())
						+ "%'");

			}

			if (searchVendorForm.getCity() != null
					&& searchVendorForm.getCity().length() != 0) {
				sqlQuery.append(" and vaddr.CITY LIKE '%"
						+ searchVendorForm.getCity() + "%'");

			}

			if (searchVendorForm.getRegion() != null
					&& searchVendorForm.getRegion().length() != 0) {
				sqlQuery.append(" and vaddr.REGION LIKE '%"
						+ searchVendorForm.getRegion() + "%'");

			}

			if (searchVendorForm.getProvince() != null
					&& searchVendorForm.getProvince().length() != 0) {
				sqlQuery.append(" and vaddr.PROVINCE LIKE '%"
						+ searchVendorForm.getProvince() + "%'");

			}
			if (searchVendorForm.getState() != null
					&& searchVendorForm.getState().length() != 0) {
				sqlQuery.append(" and vaddr.STATE = "
						+ searchVendorForm.getState() + "");

			}

			if (searchVendorForm.getCountry() != null
					&& !searchVendorForm.getCountry().equalsIgnoreCase("0")) {
				sqlQuery.append(" and vaddr.COUNTRY=:country");

			}

			if (searchVendorForm.getDiverse() != null
					&& searchVendorForm.getDiverse().length() != 0) {
				sqlQuery.append(" and vendor.DIVSERSUPPLIER=:diverse");
			}

			if (searchVendorForm.getDiverseCertificateNames() != null
					&& searchVendorForm.getDiverseCertificateNames().length != 0) {
				sqlQuery.append(" INNER JOIN customer_vendordiverseclassifcation diverse ON diverse.VENDORID=vendor.ID AND diverse.CERTMASTERID IN ("
						+ joinedString + ") AND diverse.ACTIVE=1 ");
			}

			sqlQuery.append(" ORDER BY vendor.VENDORNAME ASC");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			if (searchVendorForm.getCountry() != null
					&& !searchVendorForm.getCountry().equalsIgnoreCase("0")) {
				query.setParameter("country", searchVendorForm.getCountry());
			}

			if (searchVendorForm.getDiverse() != null
					&& searchVendorForm.getDiverse().length() != 0) {
				query.setParameter("diverse", searchVendorForm.getDiverse());
			}
			query.setParameter("parentVendorID", vendorId);

			List vendors = query.list();
			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					naicsCode = (String) searchVendors[5];
					countryName = (String) searchVendors[6];
					stateName = (String) searchVendors[7];

					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName);
					vendorsList.add(vendorDto);
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorsList;
	}

	/**
	 * Retrieve vendors list in Assessment Review
	 * 
	 * @return
	 */
	@Override
	public List<VendorMaster> reviewVendorsList(UserDetailsDto userDetails,
			Integer customerId) {

		List<VendorMaster> vendorMaster = new ArrayList<VendorMaster>();

		Session session = null;

		/* session object represents customer database dynamically. */

		session = HibernateUtilCustomer.buildSessionFactory().openSession(
				DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		try {
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT DISTINCT {vendorMaster.*} "
					+ "FROM   customer_vendormaster vendorMaster INNER JOIN vendor_email_notification_detail "
					+ "ON vendorMaster.ID = vendor_email_notification_detail.VENDORID and vendorMaster.CREATEDBY= "
					+ customerId
					+ " INNER JOIN vendor_email_notification_master "
					+ "ON vendor_email_notification_detail.VENDOREMAILNOTIFICATIONMASTERID = vendor_email_notification_master.ID"
					+ " order by vendorMaster.VENDORNAME ASC");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			query.addEntity("vendorMaster", VendorMaster.class);
			Iterator iterator = query.list().iterator();

			session.getTransaction().commit();
			while (iterator.hasNext()) {

				Object searchVendors = (Object) iterator.next();

				VendorMaster vendor = (VendorMaster) searchVendors;
				vendorMaster.add(vendor);
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CertifyingAgency> listAgencies(UserDetailsDto appDetails) {
		List<CertifyingAgency> cerrificateAgencies = null;

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			cerrificateAgencies = session
					.createQuery(
							"from CertifyingAgency where isActive=1 Order by agencyName")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return cerrificateAgencies;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SecretQuestion> listSecQns(UserDetailsDto appDetails) {
		List<SecretQuestion> secretQnsList = null;

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		try {
			session.beginTransaction();
			secretQnsList = session.createQuery(
					"from SecretQuestion where isActive=1").list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return secretQnsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List<SearchVendorDto> searchVendorFullText(String searchString,
			UserDetailsDto appDetails, Integer divisionId) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		Iterator iterator = null;

		try {
			String divisionValues = "";

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					divisionValues = divisionIds.toString();
				}
			}

			session.beginTransaction();
			Query query = session
					.createSQLQuery("CALL fulltxt_search_sp(:text,:divisionId)")
					.setParameter("text", searchString)
					.setParameter("divisionId", divisionValues);
			// setParameter("text",searchString);
			List vendors = query.list();
			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;
					String vendorNotes;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					countryName = (String) searchVendors[8];
					stateName = (String) searchVendors[9];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[10]);
					vendorCode = (String) searchVendors[11];
					modeOfReg = (String) searchVendors[12];
					vendorStatus = (String) searchVendors[13];

					if (searchVendors[14] != null && searchVendors[14] != "") {
						vendorNotes = (String) searchVendors[14];
					} else {
						vendorNotes = "None";
					}

					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, null, null, null, null, null, null,
							null, null, null, null, null, vendorNotes);
					vendorsList.add(vendorDto);
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GeographicalStateRegionDto> listOfExportFields(
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<GeographicalStateRegionDto> areaDtos = new ArrayList<GeographicalStateRegionDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select category, group_concat(ID, '-', COLUMNTITLE SEPARATOR '|') "
					+ " From avms_custom_report_field_selection group by category Order by TABLEORDER  ");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> areas = query.list();
			if (areas != null) {
				iterator = areas.iterator();
				while (iterator.hasNext()) {
					GeographicalStateRegionDto dto = new GeographicalStateRegionDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setBusinessGroup(objects[0].toString());
					}
					if (objects[1] != null) {
						dto.setServiceArea(objects[1].toString());
					}
					areaDtos.add(dto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return areaDtos;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Integer totalVendorCount(SearchVendorForm searchVendorForm,
			UserDetailsDto appDetails, String searchPageType) {
		/* session object represents customer database */
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);

		CountryStateDao countryStateDao = new CountryStateDaoImpl();
		Country defaultCountry = null;

		Integer totalCount = 0;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();
			if (searchPageType.equalsIgnoreCase("searchVendor")) {
				if (appDetails.getSettings().getIsDivision() != null
						&& appDetails.getSettings().getIsDivision() != 0) {
					if (appDetails.getCustomerDivisionIds() != null
							&& appDetails.getCustomerDivisionIds().length != 0
							&& appDetails.getIsGlobalDivision() == 0) {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' ");
							}
						}

						StringBuilder divisionIds = new StringBuilder();
						for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
							divisionIds.append(appDetails
									.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
								+ "AND cvm.VENDORSTATUS IN ('A','B') AND cvm.CUSTOMERDIVISIONID in("
								+ divisionIds.toString() + ") ");

						/*
						 * sqlQuery.append(
						 * "WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
						 * +
						 * "AND cvm.VENDORSTATUS IN ('A','B') AND cvm.CUSTOMERDIVISIONID= "
						 * + appDetails.getCustomerDivisionID() + " ");
						 */

						if (appDetails.getIsGlobalDivision() != 1) {
							sqlQuery.append(" AND cvm.DIVSERSUPPLIER=1 ");
						}

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND cvam.COUNTRY = "
									+ defaultCountry.getId()
									+ " AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) ");
						}
					} else {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' ");
							}
						}

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
								+ "AND cvm.VENDORSTATUS IN ('A','B') ");

						if (appDetails.getIsGlobalDivision() != 1) {
							sqlQuery.append(" AND cvm.DIVSERSUPPLIER=1 ");
						}

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND cvam.COUNTRY = "
									+ defaultCountry.getId()
									+ " AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) ");
						}
					}
				} else {
					sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

					if (appDetails.getWorkflowConfiguration() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() == 0) {
						defaultCountry = countryStateDao
								.listDefaultCountry(appDetails);
						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' ");
						}
					}

					sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
							+ "AND cvm.VENDORSTATUS IN ('A','B') ");

					if (appDetails.getIsGlobalDivision() != 1) {
						sqlQuery.append(" AND cvm.DIVSERSUPPLIER=1 ");
					}

					if (defaultCountry != null
							&& defaultCountry.getId() != null) {
						sqlQuery.append("AND cvam.COUNTRY = "
								+ defaultCountry.getId()
								+ " AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) ");
					}
				}
			} else if (searchPageType
					.equalsIgnoreCase("searchPrimeVendorProgress")) {
				if (appDetails.getSettings().getIsDivision() != null
						&& appDetails.getSettings().getIsDivision() != 0) {
					if (appDetails.getCustomerDivisionIds() != null
							&& appDetails.getCustomerDivisionIds().length != 0
							&& appDetails.getIsGlobalDivision() == 0) {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' ");
							}
						}

						StringBuilder divisionIds = new StringBuilder();
						for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
							divisionIds.append(appDetails
									.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
								+ "AND cvm.VENDORSTATUS IN ('A','B','N','P') AND cvm.CUSTOMERDIVISIONID in("
								+ divisionIds.toString() + ") ");

						/*
						 * sqlQuery.append(
						 * "WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
						 * +
						 * "AND cvm.VENDORSTATUS IN ('A','B','N','P') AND cvm.CUSTOMERDIVISIONID= "
						 * + appDetails.getCustomerDivisionID() + " ");
						 */

						if (appDetails.getIsGlobalDivision() != 1) {
							sqlQuery.append(" AND cvm.DIVSERSUPPLIER=1 ");
						}

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND cvam.COUNTRY = "
									+ defaultCountry.getId()
									+ " AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) ");
						}
					} else {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' ");
							}
						}

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
								+ "AND cvm.VENDORSTATUS IN ('A','B','N','P') ");

						if (appDetails.getIsGlobalDivision() != 1) {
							sqlQuery.append(" AND cvm.DIVSERSUPPLIER=1 ");
						}

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND cvam.COUNTRY = "
									+ defaultCountry.getId()
									+ " AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) ");
						}
					}
				} else {
					sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

					if (appDetails.getWorkflowConfiguration() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() == 0) {
						defaultCountry = countryStateDao
								.listDefaultCountry(appDetails);
						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' ");
						}
					}

					sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
							+ "AND cvm.VENDORSTATUS IN ('A','B','N','P') ");

					if (appDetails.getIsGlobalDivision() != 1) {
						sqlQuery.append("AND cvm.DIVSERSUPPLIER=1 ");
					}

					if (defaultCountry != null
							&& defaultCountry.getId() != null) {
						sqlQuery.append("AND cvam.COUNTRY = "
								+ defaultCountry.getId()
								+ " AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) ");
					}
				}
			} else if (searchPageType.equalsIgnoreCase("primeVendorSearch")) {
				if (appDetails.getSettings().getIsDivision() != null
						&& appDetails.getSettings().getIsDivision() != 0) {
					if (appDetails.getCustomerDivisionIds() != null
							&& appDetails.getCustomerDivisionIds().length != 0
							&& appDetails.getIsGlobalDivision() == 0) {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("INNER JOIN customer_vendoraddressmaster vaddr ON cvm.ID = vaddr.VENDORID AND vaddr.ADDRESSTYPE = 'p' ");
							}
						}

						StringBuilder divisionIds = new StringBuilder();
						for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
							divisionIds.append(appDetails
									.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
								+ "AND cvm.PRIMENONPRIMEVENDOR=1 AND cvm.VENDORSTATUS IN ('A','B','N','P') "
								+ "AND cvm.CUSTOMERDIVISIONID in("
								+ divisionIds.toString() + ") ");

						/*
						 * sqlQuery.append(
						 * "WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
						 * +
						 * "AND cvm.PRIMENONPRIMEVENDOR=1 AND cvm.VENDORSTATUS IN ('A','B','N','P') "
						 * + "AND cvm.CUSTOMERDIVISIONID= " +
						 * appDetails.getCustomerDivisionID() + " ");
						 */

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND vaddr.COUNTRY = "
									+ defaultCountry.getId());
						}
					} else {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("INNER JOIN customer_vendoraddressmaster vaddr ON cvm.ID = vaddr.VENDORID AND vaddr.ADDRESSTYPE = 'p' ");
							}
						}

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
								+ "AND cvm.PRIMENONPRIMEVENDOR=1 AND cvm.VENDORSTATUS IN ('A','B','N','P') ");

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND vaddr.COUNTRY = "
									+ defaultCountry.getId());
						}
					}
				} else {
					sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

					if (appDetails.getWorkflowConfiguration() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() == 0) {
						defaultCountry = countryStateDao
								.listDefaultCountry(appDetails);
						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("INNER JOIN customer_vendoraddressmaster vaddr ON cvm.ID = vaddr.VENDORID AND vaddr.ADDRESSTYPE = 'p' ");
						}
					}

					sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.isActive=1 "
							+ "AND cvm.PRIMENONPRIMEVENDOR=1 AND cvm.VENDORSTATUS IN ('A','B','N','P') ");

					if (defaultCountry != null
							&& defaultCountry.getId() != null) {
						sqlQuery.append("AND vaddr.COUNTRY = "
								+ defaultCountry.getId());
					}
				}
			} else if (searchPageType.equalsIgnoreCase("statusBySearch")) {
				if (appDetails.getSettings().getIsDivision() != null
						&& appDetails.getSettings().getIsDivision() != 0) {
					if (appDetails.getCustomerDivisionIds() != null
							&& appDetails.getCustomerDivisionIds().length != 0
							&& appDetails.getIsGlobalDivision() == 0) {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("INNER JOIN customer_vendoraddressmaster vaddr ON cvm.ID = vaddr.VENDORID AND vaddr.ADDRESSTYPE = 'p' ");
							}
						}

						StringBuilder divisionIds = new StringBuilder();
						for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
							divisionIds.append(appDetails
									.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);

						/*
						 * String vendorStatus = null;
						 * 
						 * if(!arrayToStringWithSingleQuote(searchVendorForm.
						 * getAllStatusList()).equalsIgnoreCase("") &&
						 * searchVendorForm.getAllStatusList().length != 0) {
						 * vendorStatus =
						 * arrayToStringWithSingleQuote(searchVendorForm
						 * .getAllStatusList()); } else { vendorStatus =
						 * "'N', 'P', 'A', 'B','I'"; }
						 */
						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) "
								+ " AND cvm.DIVSERSUPPLIER=1 AND cvm.VENDORSTATUS IN ('A','B','N','P','I') "
								+ "AND cvm.CUSTOMERDIVISIONID in("
								+ divisionIds.toString() + ") ");

						/*
						 * if(vendorStatus != null) {
						 * if(vendorStatus.contains("I")) {
						 * if(vendorStatus.equalsIgnoreCase("'I'")) {
						 * sqlQuery.append(" and cvm.isActive=0 "); } } else {
						 * sqlQuery.append(" and cvm.isActive=1 "); } }
						 */

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND vaddr.COUNTRY = "
									+ defaultCountry.getId());
						}
					} else {
						sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

						if (appDetails.getWorkflowConfiguration() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() != null
								&& appDetails.getWorkflowConfiguration()
										.getInternationalMode() == 0) {
							defaultCountry = countryStateDao
									.listDefaultCountry(appDetails);
							if (defaultCountry != null
									&& defaultCountry.getId() != null) {
								sqlQuery.append("INNER JOIN customer_vendoraddressmaster vaddr ON cvm.ID = vaddr.VENDORID AND vaddr.ADDRESSTYPE = 'p' ");
							}
						}

						/*
						 * String vendorStatus = null;
						 * if(!arrayToStringWithSingleQuote
						 * (searchVendorForm.getAllStatusList
						 * ()).equalsIgnoreCase("") &&
						 * searchVendorForm.getAllStatusList().length != 0) {
						 * vendorStatus =
						 * arrayToStringWithSingleQuote(searchVendorForm
						 * .getAllStatusList()); } else { vendorStatus =
						 * "'N', 'P', 'A', 'B','I'"; }
						 */

						sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) "
								+ "AND cvm.DIVSERSUPPLIER=1 AND cvm.VENDORSTATUS IN ('A','B','N','P','I') ");

						/*
						 * if(vendorStatus != null) {
						 * if(vendorStatus.contains("I")) {
						 * if(vendorStatus.equalsIgnoreCase("'I'")) {
						 * sqlQuery.append(" and cvm.isActive=0 "); } } else {
						 * sqlQuery.append(" and cvm.isActive=1 "); } }
						 */

						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("AND vaddr.COUNTRY = "
									+ defaultCountry.getId());
						}
					}
				} else {
					sqlQuery.append("SELECT COUNT(*) AS total FROM customer_vendormaster cvm ");

					if (appDetails.getWorkflowConfiguration() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() != null
							&& appDetails.getWorkflowConfiguration()
									.getInternationalMode() == 0) {
						defaultCountry = countryStateDao
								.listDefaultCountry(appDetails);
						if (defaultCountry != null
								&& defaultCountry.getId() != null) {
							sqlQuery.append("INNER JOIN customer_vendoraddressmaster vaddr ON cvm.ID = vaddr.VENDORID AND vaddr.ADDRESSTYPE = 'p' ");
						}
					}

					sqlQuery.append("WHERE (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) "
							+ "AND cvm.DIVSERSUPPLIER=1 AND cvm.VENDORSTATUS IN ('A','B','N','P','I') ");

					if (defaultCountry != null
							&& defaultCountry.getId() != null) {
						sqlQuery.append("AND vaddr.COUNTRY = "
								+ defaultCountry.getId());
					}
				}
			}
			System.out.println("Count SearchDaoImpl @2094:"
					+ sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				totalCount = rs.getInt("total");
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					connection.close();
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return totalCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchVendorDto> searchPrimeVendor(
			SearchVendorForm searchVendorForm, Integer approved, Integer from,
			UserDetailsDto appDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		List<String> searchCriterias = new ArrayList<String>();

		try {
			session.beginTransaction();
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT DISTINCT vendor.ID, vendor.CITY, vaddr.REGION, vendor.DUNSNUMBER, vendor.VENDORNAME, vendor.EMAILID, "
					+ " (select cu.EMAILID from customer_vendor_users cu where cu.VENDORID=vendor.ID and "
					+ " cu.ISPRIMARYCONTACT=1 and cu.ISACTIVE=1 limit 1)PrimeContactEmailId,"
					+ " (select  naics.NAICSCODE from customer_naicscode naics, customer_vendornaics vendorNaics"
					+ " where vendorNaics.NAICSID=naics.NAICSID  and vendorNaics.VENDORID=vendor.ID and vendorNaics.ISPRIMARYKEY IN (1,2,3) )NAISCODE, "
					+ " country.countryname, state.STATENAME, vendor.VENDORCODE, "
					+ " Case When vendor.isinvited = 1 Then 'Customer' When vendor.isinvited = 0 Then 'Self' End As Mode_of_registration, "
					+ " vendor.CREATEDON, CASE vendor.VENDORSTATUS WHEN 'A' THEN 'Reviewed by Supplier Diversity' "
					+ " WHEN 'B' THEN 'BP Vendor' WHEN 'N' THEN 'New Registration' WHEN 'P' THEN 'Pending Review' END AS VENDORSTATUS"
					+ " From ");

			sqlQuery.append(" customer_vendormaster vendor INNER JOIN  customer_vendoraddressmaster vaddr on vendor.ID = vaddr.VENDORID and vaddr.ADDRESSTYPE = 'p' ");

			sqlQuery.append(" left outer JOIN state state on state.ID=vaddr.STATE INNER JOIN country country ON country.Id=vaddr.COUNTRY ");
			if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("0")) {
				sqlQuery.append(" and vendor.VENDORSTATUS in ('A','B','N','P') ");
			} else if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("AB")) {
				sqlQuery.append(" and vendor.VENDORSTATUS IN ('A','B')"
						+ " and vendor.ISACTIVE=1");
			} else if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()
					&& searchVendorForm.getPrimeVendorStatus()
							.equalsIgnoreCase("I")) {
				sqlQuery.append(" and vendor.VENDORSTATUS='"
						+ searchVendorForm.getPrimeVendorStatus()
						+ "' and vendor.ISACTIVE=0");
			} else if (searchVendorForm.getPrimeVendorStatus() != null
					&& !searchVendorForm.getPrimeVendorStatus().isEmpty()) {
				sqlQuery.append(" and vendor.VENDORSTATUS='"
						+ searchVendorForm.getPrimeVendorStatus()
						+ "' and vendor.ISACTIVE=1 ");
			}
			sqlQuery.append(" and vendor.VENDORSTATUS IN ('A','B','N','P') and vendor.PRIMENONPRIMEVENDOR=1 "
					+ "and (vendor.ISPARTIALLYSUBMITTED='No' or vendor.ISPARTIALLYSUBMITTED is null) AND vendor.isactive = 1");

			// Its needed only when International Mode = 0, with default country
			// selected.
			if (appDetails.getWorkflowConfiguration() != null) {
				if (appDetails.getWorkflowConfiguration()
						.getInternationalMode() != null) {
					if (appDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
						List<Country> country = (List<Country>) session
								.createQuery("From Country where isDefault=1")
								.list();
						if (country.get(0).getId() != null) {
							sqlQuery.append(" and vaddr.COUNTRY = "
									+ country.get(0).getId() + " ");
						}
					}
				}
			}

			/** Filtering Based on Customer Division Id **/
			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {

					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and vendor.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			boolean isContact = false;
			for (String option : searchVendorForm.getSearchFields()) {
				String searchFieldValue = null;
				if (searchVendorForm.getSearchFieldsValue() != null
						&& searchVendorForm.getSearchFieldsValue()[from] != null
						&& !searchVendorForm.getSearchFieldsValue()[from]
								.isEmpty()) {
					searchFieldValue = CommonUtils.escape(searchVendorForm
							.getSearchFieldsValue()[from]);
				}
				from++;
				if (searchFieldValue != null) {
					switch (SearchFields.valueOf(option)) {

					case LEGALCOMPANYNAME:
						sqlQuery.append(" and vendor.VENDORNAME LIKE '%"
								+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						break;
					case CITY:
						sqlQuery.append(" and vaddr.CITY LIKE '%"
								+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						break;
					case STATE:
						sqlQuery.append(" and vaddr.STATE = "
								+ searchFieldValue + "");
						State state = getState(session,
								Integer.parseInt(searchFieldValue));
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + state.getStatename() + " ");
						break;
					case COUNTRY:
						StringBuilder builder = new StringBuilder();
						StringBuilder countryCriteria = new StringBuilder();
						if (searchVendorForm.getSelectCountries() != null
								&& searchVendorForm.getSelectCountries().length != 0) {
							for (String country : searchVendorForm
									.getSelectCountries()) {
								builder.append("'" + country + "',");
								Country countries = getCountry(session,
										Integer.parseInt(country));
								countryCriteria.append(countries
										.getCountryname() + ",");
							}
							builder.deleteCharAt(builder.length() - 1);
							countryCriteria.deleteCharAt(countryCriteria
									.length() - 1);
							sqlQuery.append(" and vaddr.COUNTRY IN ("
									+ builder.toString() + ")");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc()
									+ ": "
									+ countryCriteria.toString() + " ");
						}
						break;
					case FIRSTNAME:
						isContact = true;
						break;
					case LASTNAME:
						isContact = true;
						break;
					case CONTACTEMAIL:
						isContact = true;
						break;
					default:
						break;
					}
				}
			}
			from++;
			if (isContact) {
				sqlQuery.append(" INNER JOIN customer_vendor_users vendorContact ON vendorContact.VENDORID=vendor.ID ");

				from = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[from] != null
							&& !searchVendorForm.getSearchFieldsValue()[from]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[from];
					}
					from++;
					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case FIRSTNAME:
							sqlQuery.append(" and vendorContact.FIRSTNAME LIKE '%"
									+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						case LASTNAME:
							sqlQuery.append(" and vendorContact.LASTNAME LIKE '%"
									+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						case DESIGNATION:
							sqlQuery.append(" and vendorContact.DESIGNATION LIKE '%"
									+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						case CONTACTPHONE:
							sqlQuery.append(" and vendorContact.PHONENUMBER = '"
									+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						case CONTACTMOBILE:
							sqlQuery.append(" and vendorContact.MOBILE = '"
									+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						case CONTACTFAX:
							sqlQuery.append(" and vendorContact.FAX = '"
									+ searchFieldValue + "'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						case CONTACTEMAIL:
							sqlQuery.append(" and vendorContact.EMAILID LIKE '%"
									+ searchFieldValue + "%'");
							searchCriterias.add(SearchFields.valueOf(option)
									.getDesc() + ": " + searchFieldValue + " ");
							break;
						default:
							break;
						}
					}
				}
			}

			sqlQuery.append(" ORDER BY vendor.VENDORNAME ASC ");
			logger.info("Query in Search Vendor:" + sqlQuery.toString());
			searchVendorForm.setSearchQuery(sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List vendors = query.list();
			Iterator iterator;
			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					countryName = (String) searchVendors[8];
					stateName = (String) searchVendors[9];
					vendorCode = (String) searchVendors[10];
					modeOfReg = (String) searchVendors[11];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[12]);
					vendorStatus = (String) searchVendors[13];
					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, null, null, null, null, null, null,
							null, null, null, null, null, null);
					vendorsList.add(vendorDto);
				}
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		searchVendorForm.setSearchCriterias(searchCriterias);
		return vendorsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tier2ReportDto> getSpendReportHistory(
			SearchVendorForm searchVendorForm, UserDetailsDto userDetails) {
		/* session object represents customer database */
		Integer from = 0;
		Integer from1 = 0;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<Tier2ReportDto> tier2ReportDtoList = new ArrayList<Tier2ReportDto>();
		List<String> searchCriterias = new ArrayList<String>();

		try {
			session.beginTransaction();
			/* Get the list of active report master data */
			StringBuilder selectQuery = new StringBuilder();
			selectQuery
					.append("SELECT customer_tier2reportmaster.Tier2ReportMasterid, customer_tier2reportmaster.IsSubmitted, "
							+ "IFNULL(customer_vendor_users.FIRSTNAME, '') AS createdby, "
							+ "DATE_FORMAT(customer_tier2reportmaster.reportingStartDate, '%Y') AS YEAR, customer_tier2reportmaster.reportingPeriod, "
							+ "(SELECT SUM(customer_tier2reportdirectexpenses.DirExpenseAmt) FROM customer_tier2reportdirectexpenses "
							+ "WHERE customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)Direxpenseamt, "
							+ "(SELECT SUM(customer_tier2reportindirectexpenses.IndirectExpenseAmt) FROM customer_tier2reportindirectexpenses "
							+ "WHERE customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)indirectexpenseamt, "
							+ "customer_tier2reportmaster.totalSales, customer_tier2reportmaster.totalSalesToCompany, "
							+ "DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') CREATEDON,customer_tier2reportmaster.FILENAME "
							+ "FROM customer_tier2reportmaster "
							+ "INNER JOIN customer_vendormaster ON customer_vendormaster.ID = customer_tier2reportmaster.vendorId "
							+ "INNER JOIN customer_vendor_users ON customer_vendor_users.ID=customer_tier2reportmaster.CreatedBy");

			for (String option : searchVendorForm.getSearchFields()) {
				String searchFieldValue = null;
				if (searchVendorForm.getSearchFieldsValue() != null
						&& searchVendorForm.getSearchFieldsValue()[from] != null
						&& !searchVendorForm.getSearchFieldsValue()[from]
								.isEmpty()) {
					searchFieldValue = CommonUtils.escape(searchVendorForm
							.getSearchFieldsValue()[from]);
				}
				from++;
				if (searchFieldValue != null) {
					if (from1 == 0) {
						selectQuery.append(" WHERE ");
					}
					if (from1 > 0) {
						selectQuery.append(" AND ");
					}
					from1++;
					switch (SearchFields.valueOf(option)) {

					case YEAR:
						selectQuery
								.append("DATE_FORMAT(customer_tier2reportmaster.reportingStartDate, '%Y') ="
										+ searchFieldValue);

						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						break;
					case REPORTINGPERIOD:
						selectQuery
								.append("customer_tier2reportmaster.ReportingPeriod ='"
										+ searchFieldValue + "'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						break;
					case VENDORNAME:
						selectQuery
								.append("IFNULL(customer_vendor_users.FIRSTNAME, '') like '%"
										+ searchFieldValue + "%'");
						searchCriterias.add(SearchFields.valueOf(option)
								.getDesc() + ": " + searchFieldValue + " ");
						break;
					default:
						break;
					}

				}
			}

			System.out.println("Tier 2 Report History @ Report DaoImpl 5920:"
					+ selectQuery.toString());
			List historyList = session.createSQLQuery(selectQuery.toString())
					.list();
			Iterator iterator;

			if (historyList != null) {
				iterator = historyList.iterator();
				while (iterator.hasNext()) {
					Object[] reportHistory = (Object[]) iterator.next();
					Integer id;
					Byte isSubmitted;
					String vendorName;
					String year;
					String reportingPeriod;
					Double dirExpenseAmt;
					Double inDirExpenseAmt;
					Double totalSales;
					Double totalSalesToCompany;
					String createdOn;
					String fileName; 

					id = (Integer) reportHistory[0];
					isSubmitted = (Byte) reportHistory[1];
					;
					vendorName = (String) reportHistory[2];
					;
					year = (String) reportHistory[3];
					reportingPeriod = (String) reportHistory[4];
					dirExpenseAmt = (Double) reportHistory[5];
					inDirExpenseAmt = (Double) reportHistory[6];
					totalSales = (Double) reportHistory[7];
					totalSalesToCompany = (Double) reportHistory[8];
					createdOn = reportHistory[9].toString();
					if (reportHistory[10] != null) {
						fileName = reportHistory[10].toString();
					}else{
						fileName = "";
					}
					Tier2ReportDto tierReportDto = new Tier2ReportDto(id,
							isSubmitted, vendorName, year, reportingPeriod,
							dirExpenseAmt, inDirExpenseAmt, totalSales,
							totalSalesToCompany, createdOn, fileName);
					tier2ReportDtoList.add(tierReportDto);

				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return tier2ReportDtoList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getNaicsCodes(UserDetailsDto userDetails,
			String searchValue) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<String> naicsCodes = new ArrayList<String>();
		try {

			session.beginTransaction();
			naicsCodes = session.createQuery(
					"select naicsCode from NaicsCode  where isActive=1 and naicsCode like '"
							+ searchValue + "%'").list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsCodes;
	}

	@Override
	public List<String> getNaicsDescriptions(UserDetailsDto userDetails,
			String searchValue) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<String> naicsDesc = new ArrayList<String>();
		try {

			session.beginTransaction();
			naicsDesc = session
					.createQuery(
							"select naicsDescription from NaicsCode  where isActive=1 and naicsDescription like '"
									+ searchValue + "%'").list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return naicsDesc;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<String> getVendorNamesList(UserDetailsDto userDetails,
			String searchValue) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<String> vendorNamesList = new ArrayList<String>();

		try {
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT DISTINCT cvm.VENDORNAME FROM customer_vendormaster cvm ");

			if (userDetails.getWorkflowConfiguration() != null
					&& userDetails.getWorkflowConfiguration()
							.getInternationalMode() != null
					&& userDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
				CountryStateDao countryStateDao = new CountryStateDaoImpl();
				Country defaultCountry = countryStateDao
						.listDefaultCountry(userDetails);

				if (defaultCountry != null && defaultCountry.getId() != null) {
					sqlQuery.append("INNER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' "
							+ "AND cvam.COUNTRY = "
							+ defaultCountry.getId()
							+ " ");
				}
			}

			sqlQuery.append("WHERE cvm.DIVSERSUPPLIER = 1 AND cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B','I') "
					+ "AND (cvm.PARENTVENDORID = 0 OR cvm.PARENTVENDORID IS NULL) ");

			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0
						&& userDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN ("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("AND cvm.VENDORNAME like '" + searchValue + "%' ");

			sqlQuery.append("ORDER BY cvm.VENDORNAME ASC");

			System.out.println("getVendorNamesList(SearchDaoImpl @ 2522):"
					+ sqlQuery.toString());

			vendorNamesList = session.createSQLQuery(sqlQuery.toString())
					.list();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorNamesList;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<CustomerSearchFilter> listPreviousSearchDetails(
			UserDetailsDto userDetails, char searchType, Integer userId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<CustomerSearchFilter> previousSearchList = new ArrayList<CustomerSearchFilter>();

		try {
			session.beginTransaction();

			StringBuilder query = new StringBuilder();
			query.append(" From CustomerSearchFilter where searchType  like'"
					+ searchType + "%' and userId = " + userId);

			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0
						&& userDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					query.append(" and customerDivisionId in("
							+ divisionIds.toString() + ") ");

					// query.append(" and customerDivisionId = '" +
					// userDetails.getCustomerDivisionID() + "'");
				}
			}
			// else
			// query.append(" and customerDivisionId IS NULL");
			query.append(" group by searchName");

			previousSearchList = session.createQuery(query.toString()).list();

			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return previousSearchList;
	}

	@Override
	public CustomerSearchFilter getCustomerSearchFilter(
			UserDetailsDto userDetails, Integer searchId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		CustomerSearchFilter customerSearchFilter = null;
		try {
			session.beginTransaction();
			customerSearchFilter = (CustomerSearchFilter) session
					.createCriteria(CustomerSearchFilter.class)
					.add(Restrictions.eq("id", searchId)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return customerSearchFilter;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<SearchVendorDto> searchByPreviousData(
			UserDetailsDto userDetails, String searchQuery, String searchType) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		try {
			session.beginTransaction();
			List vendors = (List) session.createSQLQuery(searchQuery).list();
			Iterator iterator;

			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;
					String diverseClassifcation;
					String certificateType;
					String businessType;
					String yearOfEstd;
					String firstName;
					String lastName;
					String phoneNumber;
					String statusNote = null;
					String statusDate = null;
					String customerMobile = null;
					String contactUser = null;
					String vendorNotes;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					diverseClassifcation = (String) searchVendors[8];
					certificateType = (String) searchVendors[9];
					countryName = (String) searchVendors[10];
					stateName = (String) searchVendors[11];
					vendorCode = (String) searchVendors[12];
					modeOfReg = (String) searchVendors[13];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[14]);
					vendorStatus = (String) searchVendors[15];
					businessType = (String) searchVendors[16];
					yearOfEstd = (String) searchVendors[17];
					firstName = (String) searchVendors[18];
					lastName = (String) searchVendors[19];
					phoneNumber = (String) searchVendors[20];

					if (searchVendors[21] != null && searchVendors[21] != "") {
						vendorNotes = (String) searchVendors[21];
					} else {
						vendorNotes = "None";
					}

					// check the status is pending review or not.
					if (searchType.equalsIgnoreCase("VP")) {

						statusNote = (String) searchVendors[22];
						statusDate = CommonUtils
								.convertDateToString((Date) searchVendors[23]);
						customerMobile = (String) searchVendors[24];
						contactUser = (String) searchVendors[25];
					}

					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, statusNote, statusDate, customerMobile,
							contactUser, diverseClassifcation, certificateType,
							businessType, yearOfEstd, firstName, lastName,
							phoneNumber, vendorNotes);
					vendorsList.add(vendorDto);
				}

			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorsList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<SearchVendorDto> searchByPreviousDataForPrimeVendor(
			UserDetailsDto userDetails, String searchQuery) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		try {
			session.beginTransaction();
			List vendors = (List) session.createSQLQuery(searchQuery).list();
			Iterator iterator;

			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					countryName = (String) searchVendors[8];
					stateName = (String) searchVendors[9];
					vendorCode = (String) searchVendors[10];
					modeOfReg = (String) searchVendors[11];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[12]);
					vendorStatus = (String) searchVendors[13];
					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, null, null, null, null, null, null,
							null, null, null, null, null, null);
					vendorsList.add(vendorDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorsList;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<LinkedHashMap<String, Object>> searchVendorAllDetails(
			UserDetailsDto appDetails, String vendorId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<LinkedHashMap<String, Object>> vendors = null;

		try {
			session.beginTransaction();
			Query query = session.createSQLQuery(
					"CALL export_selected_vendor_sp(:vendorId)").setParameter(
					"vendorId", vendorId);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			query.setResultTransformer(new TupleToHeaderDataMapTransformer());
			vendors = query.list();

			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendors;
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public List<SearchVendorDto> searchVendorFilteredByNonNMSDCWBENC(
			SearchVendorForm searchVendorForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		String nonNMSDCWBENCQuery = "";
		String certId = "";
		String certAgencyId = "";

		try {
			if (searchVendorForm.getCertificateName() != null
					&& searchVendorForm.getCertificateAgency() != null) {
				for (int i = 0; i < searchVendorForm.getCertificateName().length; i++) {
					if (i == 0) {
						certId = searchVendorForm.getCertificateName()[i];
					} else {
						certId += ","
								+ searchVendorForm.getCertificateName()[i];
					}
				}

				for (int i = 0; i < searchVendorForm.getCertificateAgency().length; i++) {
					if (i == 0) {
						certAgencyId = searchVendorForm.getCertificateAgency()[i];
					} else {
						certAgencyId += ","
								+ searchVendorForm.getCertificateAgency()[i];
					}
				}
				nonNMSDCWBENCQuery = " INNER JOIN customer_vendordiverseclassifcation ON customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID "
						+ " AND customer_vendordiverseclassifcation.CERTMASTERID IN ("
						+ certId
						+ ")"
						+ " INNER JOIN customer_vendorcertificate ON customer_vendorcertificate.VENDORID=customer_vendormaster.ID "
						+ " AND customer_vendorcertificate.CERTMASTERID IN ("
						+ certId
						+ ") "
						+ " AND customer_vendorcertificate.CERTAGENCYID NOT IN ("
						+ certAgencyId + ") ";
			}
			session.beginTransaction();

			String sqlQuery = searchVendorForm.getNonNMSDCWBENCQuery().replace(
					SearchDaoImpl.DIVERSE_CLASSIFICATION_FILTER_PLACEHOLDER,
					nonNMSDCWBENCQuery);
			System.out.println("Search query in Search Dao Impl @ 2018: "
					+ sqlQuery.toString());
			logger.info("Query in Search Vendor:" + sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List vendors = query.list();
			Iterator iterator;

			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;
					String statusNote = null;
					String statusDate = null;
					String customerMobile = null;
					String contactUser = null;
					String diverseClassifcation = null;
					String certificateType = null;
					String businessType = null;
					String yearOfEstd = null;
					String firstName = null;
					String lastName = null;
					String phoneNumber = null;
					String vendorNotes;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					diverseClassifcation = (String) searchVendors[8];
					certificateType = (String) searchVendors[9];
					countryName = (String) searchVendors[10];
					stateName = (String) searchVendors[11];
					vendorCode = (String) searchVendors[12];
					modeOfReg = (String) searchVendors[13];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[14]);
					vendorStatus = (String) searchVendors[15];
					businessType = (String) searchVendors[16];
					yearOfEstd = (String) searchVendors[17];
					firstName = (String) searchVendors[18];
					lastName = (String) searchVendors[19];
					phoneNumber = (String) searchVendors[20];

					if (searchVendors[21] != null && searchVendors[21] != "") {
						vendorNotes = (String) searchVendors[21];
					} else {
						vendorNotes = "None";
					}

					if (searchVendorForm.getPrimeVendorStatus() != null
							&& searchVendorForm.getPrimeVendorStatus()
									.equalsIgnoreCase("P")) {
						statusNote = (String) searchVendors[22];
						statusDate = CommonUtils
								.convertDateToString((Date) searchVendors[23]);
						customerMobile = (String) searchVendors[24];
						contactUser = (String) searchVendors[25];
					}
					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, statusNote, statusDate, customerMobile,
							contactUser, diverseClassifcation, certificateType,
							businessType, yearOfEstd, firstName, lastName,
							phoneNumber, vendorNotes);
					vendorsList.add(vendorDto);
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			System.out.println("Exception: " + e);
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorsList;
	}

	@SuppressWarnings("deprecation")
	public String findUserNameByUserId(UserDetailsDto userDetails,
			Integer userId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String userName = null;
		Users users = null;

		try {
			users = (Users) session.createQuery(
					"from Users where id = " + userId).uniqueResult();

			if (users != null && users.getUserName() != null) {
				userName = users.getUserName();
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			System.out.println("Exception: " + e);
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return userName;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<SearchVendorDto> searchByVendorStatus(
			UserDetailsDto userDetails, SearchVendorForm searchVendorForm) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<SearchVendorDto> vendorsList = new ArrayList<SearchVendorDto>();
		try {

			StringBuilder searchQuery = new StringBuilder();

			searchQuery
					.append("SELECT DISTINCT customer_vendormaster.ID, customer_vendormaster.CITY, customer_vendoraddressmaster.REGION,"
							+ "  customer_vendormaster.DUNSNUMBER, customer_vendormaster.VENDORNAME, customer_vendormaster.EMAILID, "
							+ " (select customer_vendor_users.EMAILID from customer_vendor_users customer_vendor_users  "
							+ "where customer_vendor_users.VENDORID=customer_vendormaster.ID and customer_vendor_users.ISPRIMARYCONTACT=1"
							+ "  and customer_vendor_users.ISACTIVE=1 limit 1)PrimeContactEmailId, (select  naics.NAICSCODE from customer_naicscode naics,"
							+ " customer_vendornaics  where customer_vendornaics.NAICSID=naics.NAICSID  and customer_vendornaics.VENDORID=customer_vendormaster.ID"
							+ "  and customer_vendornaics.ISPRIMARYKEY IN (1,2,3) )NAISCODE,  (SELECT certificatemaster.CERTIFICATENAME FROM customer_certificatemaster"
							+ " certificatemaster,  customer_vendordiverseclassifcation WHERE customer_vendordiverseclassifcation.CERTMASTERID = certificatemaster.ID "
							+ " AND customer_vendordiverseclassifcation.VENDORID = customer_vendormaster.ID LIMIT 1) DIVERSECLASSIFCATION,  "
							+ "(SELECT certificatetype.DESCRIPTION FROM customer_certificatetype certificatetype, customer_vendorcertificate  WHERE"
							+ " customer_vendorcertificate.CERTIFICATETYPE = certificatetype.ID AND customer_vendorcertificate.VENDORID =  customer_vendormaster.ID LIMIT 1)"
							+ " CERTIFICATETYPE, country.countryname, state.STATENAME, customer_vendormaster.VENDORCODE,  Case When customer_vendormaster.isinvited = 1"
							+ " Then 'Customer' When customer_vendormaster.isinvited = 0 Then 'Self' End As Mode_of_registration,  customer_vendormaster.createdon,"
							+ " CASE customer_vendormaster.VENDORSTATUS WHEN 'A' THEN 'Reviewed by Supplier Diversity'  WHEN 'B' THEN 'BP Vendor' WHEN 'N' THEN 'New Registration' "
							+ " WHEN 'P' THEN 'Pending Review' WHEN 'I' THEN 'Inactive' WHEN 'S' THEN 'Sub Vendor' END AS VENDORSTATUS,  customer_businesstype.TypeName, "
							+ " IFNULL(customer_vendormaster.YEAROFESTABLISHMENT, '') YEAR, customer_vendor_users.FIRSTNAME,  customer_vendor_users.LASTNAME, customer_vendor_users.PHONENUMBER,"
							+ "  (SELECT cvn.NOTES from customer_vendornotes cvn where cvn.VENDORID = customer_vendormaster.ID ORDER BY cvn.CREATEDON DESC LIMIT 1) AS VENDORNOTES  From customer_vendormaster LEFT OUTER JOIN  "
							+ "customer_vendoraddressmaster  on customer_vendormaster.ID =  customer_vendoraddressmaster.VENDORID and customer_vendoraddressmaster.ADDRESSTYPE = 'p'  "
							+ "AND (customer_vendormaster.PARENTVENDORID=0 or customer_vendormaster.PARENTVENDORID is null) AND (customer_vendormaster.ISPARTIALLYSUBMITTED='No' or "
							+ "customer_vendormaster.ISPARTIALLYSUBMITTED is null)  LEFT OUTER JOIN state on state.ID=customer_vendoraddressmaster.STATE  LEFT OUTER JOIN country ON "
							+ "country.Id=customer_vendoraddressmaster.COUNTRY  LEFT OUTER JOIN customer_businesstype ON customer_businesstype.Id = customer_vendormaster.BUSINESSTYPE "
							+ "LEFT OUTER JOIN customer_vendor_users ON customer_vendor_users.VENDORID = customer_vendormaster.ID  AND customer_vendor_users.ISPRIMARYCONTACT=1 AND customer_vendor_users.ISACTIVE=1"
							+ "  where 1=1 ");

			String allVendorStatus = null;
			if (!arrayToStringWithSingleQuote(
					searchVendorForm.getAllStatusList()).equalsIgnoreCase("")
					&& searchVendorForm.getAllStatusList().length != 0) {
				allVendorStatus = arrayToStringWithSingleQuote(searchVendorForm
						.getAllStatusList());
				searchQuery
						.append(" and customer_vendormaster.VENDORSTATUS IN ("
								+ allVendorStatus + ") ");
			} else {
				searchQuery
						.append(" and customer_vendormaster.VENDORSTATUS  IN ('N', 'P', 'A', 'B','I') ");
			}

			if (allVendorStatus != null) {
				if (allVendorStatus.contains("I")) {
					if (allVendorStatus.equalsIgnoreCase("'I'")) {
						searchQuery
								.append(" and customer_vendormaster.ISACTIVE=0 ");
					}
				} else {
					searchQuery
							.append(" and customer_vendormaster.ISACTIVE=1 ");
				}

			}
			searchQuery.append(" and customer_vendormaster.DIVSERSUPPLIER=1  ");

			// Its needed only when International Mode = 0, with default country
			// selected.
			if (userDetails.getWorkflowConfiguration() != null) {
				if (userDetails.getWorkflowConfiguration()
						.getInternationalMode() != null) {
					if (userDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
						List<Country> country = (List<Country>) session
								.createQuery("From Country where isDefault=1")
								.list();
						if (country.get(0).getId() != null) {
							searchQuery
									.append(" and customer_vendoraddressmaster.COUNTRY = "
											+ country.get(0).getId() + " ");
						}
					}
				}
			}

			if (userDetails.getSettings().getIsDivision() != null
					&& userDetails.getSettings().getIsDivision() != 0) {
				if (userDetails.getCustomerDivisionIds() != null
						&& userDetails.getCustomerDivisionIds().length != 0
						&& userDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < userDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(userDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					searchQuery
							.append(" AND customer_vendormaster.CUSTOMERDIVISIONID in("
									+ divisionIds.toString() + ") ");
				}

			}

			if (searchVendorForm.getVendorNamesList() != null
					&& searchVendorForm.getVendorNamesList().length != 0) {
				if (!searchVendorForm.getVendorNamesList()[0]
						.equalsIgnoreCase("")) {
					StringBuilder vendorNamesList = new StringBuilder();
					String vendorNames[] = searchVendorForm
							.getVendorNamesList()[0].toString().split(";");

					for (String vendorName : vendorNames) {
						vendorNamesList.append("'" + vendorName + "',");
					}
					vendorNamesList.deleteCharAt(vendorNamesList.length() - 1);

					searchQuery
							.append(" AND customer_vendormaster.VENDORNAME IN ("
									+ vendorNamesList + ") ");
				}
			}

			searchQuery
					.append(" ORDER BY customer_vendormaster.createdon ASC ");
			System.out.println("Query@3162:" + searchQuery.toString());
			SQLQuery sqlQuery = session.createSQLQuery(searchQuery.toString());
			List vendors = sqlQuery.list();
			Iterator iterator;

			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Object[] searchVendors = (Object[]) iterator.next();
					Integer id;
					String countryName;
					String naicsCode;
					String duns;
					String region;
					String stateName;
					String city;
					String vendorName;
					String vendorCode;
					String modeOfReg;
					String vendorStatus;
					String companyEmailId;
					String primeContactEmailId;
					String createdon;
					String statusNote = null;
					String statusDate = null;
					String customerMobile = null;
					String contactUser = null;
					String diverseClassifcation = null;
					String certificateType = null;
					String businessType = null;
					String yearOfEstd = null;
					String firstName = null;
					String lastName = null;
					String phoneNumber = null;
					String vendorNotes;

					id = (Integer) searchVendors[0];
					city = (String) searchVendors[1];
					region = (String) searchVendors[2];
					duns = (String) searchVendors[3];
					vendorName = (String) searchVendors[4];
					companyEmailId = (String) searchVendors[5];
					primeContactEmailId = (String) searchVendors[6];
					naicsCode = (String) searchVendors[7];
					diverseClassifcation = (String) searchVendors[8];
					certificateType = (String) searchVendors[9];
					countryName = (String) searchVendors[10];
					stateName = (String) searchVendors[11];
					vendorCode = (String) searchVendors[12];
					modeOfReg = (String) searchVendors[13];
					createdon = CommonUtils
							.convertDateToString((Date) searchVendors[14]);
					vendorStatus = (String) searchVendors[15];
					businessType = (String) searchVendors[16];
					yearOfEstd = (String) searchVendors[17];
					firstName = (String) searchVendors[18];
					lastName = (String) searchVendors[19];
					phoneNumber = (String) searchVendors[20];

					if (searchVendors[21] != null && searchVendors[21] != "") {
						vendorNotes = (String) searchVendors[21];
					} else {
						vendorNotes = "None";
					}

					if (searchVendorForm.getPrimeVendorStatus() != null
							&& searchVendorForm.getPrimeVendorStatus()
									.equalsIgnoreCase("P")) {
						statusNote = (String) searchVendors[22];
						statusDate = CommonUtils
								.convertDateToString((Date) searchVendors[23]);
						customerMobile = (String) searchVendors[24];
						contactUser = (String) searchVendors[25];
					}
					SearchVendorDto vendorDto = new SearchVendorDto(id,
							countryName, naicsCode, duns, region, stateName,
							city, vendorName, vendorCode, modeOfReg,
							vendorStatus, companyEmailId, primeContactEmailId,
							createdon, statusNote, statusDate, customerMobile,
							contactUser, diverseClassifcation, certificateType,
							businessType, yearOfEstd, firstName, lastName,
							phoneNumber, vendorNotes);
					vendorsList.add(vendorDto);
				}
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			System.out.println("Exception: " + e);
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendorsList;
	}
	
	@Override
	public Tier2ReportMaster getTier2ReportMaster(Integer id,
			UserDetailsDto userDetails) {

		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		Tier2ReportMaster tier2ReportMaster = null;
		try {
			session.beginTransaction();

			tier2ReportMaster = (Tier2ReportMaster) session
					.createCriteria(Tier2ReportMaster.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return tier2ReportMaster;
	}
	
	/**
	 * This method helps in convert string array to string.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToString(String[] stringarray) {
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) {
			for (String st : stringarray) {
				sb.append(st.trim()).append(',');
			}
			if (stringarray.length != 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * This method helps in convert string array to string with single quote.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToStringWithSingleQuote(String[] stringarray) {
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) {
			for (String st : stringarray) {
				sb.append("'" + st.trim() + "'").append(',');
			}
			if (stringarray.length != 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}
}