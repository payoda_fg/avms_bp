/*
 * VendorObjectDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.VendorObjectsDao;
import com.fg.vms.customer.model.VendorObjects;
import com.fg.vms.customer.pojo.VendorObjectsForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the vendor object services implementation.
 */
public class VendorObjectDaoImpl implements VendorObjectsDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createObject(VendorObjectsForm configurationForm,
            int currentUserId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "unique";
        try {
            session.beginTransaction();

            /* Get the vendor object details and unique object name */
            VendorObjects object = (VendorObjects) session
                    .createCriteria(VendorObjects.class)
                    .add(Restrictions.eq("objectName",
                            configurationForm.getObjectName())).uniqueResult();

            if (object != null) {
                return result;
            }

            VendorObjects configuration = new VendorObjects();

            log.info(" ============save vendor object is starting========== ");

            configuration.setId(configurationForm.getId());
            configuration.setObjectName(configurationForm.getObjectName());
            configuration.setObjectType(configurationForm.getObjectType());
            configuration.setObjectDescription(configurationForm
                    .getObjectDescription());
            configuration.setIsActive(configurationForm.getIsActive());
            configuration.setCreatedBy(currentUserId);
            configuration.setCreatedOn(new Date());
            configuration.setModifiedBy(currentUserId);
            configuration.setModifiedOn(new Date());

            session.save(configuration);

            log.info(" ============vendor object saved successfully========== ");

            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException e) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VendorObjects> listObjects(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        session.beginTransaction();
        List<VendorObjects> objects = null;
        try {
            objects = session
                    .createQuery("from VendorObjects where isActive=1").list();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return objects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteObject(Integer id, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));
        session.beginTransaction();
        String result = "";
        VendorObjects configuration = null;
        try {

            log.info(" ============update into inactive vendor object is starting========== ");

            configuration = (VendorObjects) session
                    .get(VendorObjects.class, id);
            configuration.setIsActive((byte) 0);
            session.update(configuration);

            log.info(" ============vendor object updated into inactive successfully========== ");

            result = "success";
            session.getTransaction().commit();
        } catch (HibernateException e) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VendorObjects retriveObject(Integer id, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        session.beginTransaction();
        VendorObjects configuration = null;
        try {

            /* Get the vendor object details based on id */
            configuration = (VendorObjects) session
                    .createCriteria(VendorObjects.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return configuration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateObject(VendorObjectsForm configurationForm, Integer id,
            Integer currentUserId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        session.beginTransaction();
        String result = "";

        VendorObjects configuration = null;
        try {
            configuration = (VendorObjects) session
                    .get(VendorObjects.class, id);

            log.info(" ============update vendor object is starting========== ");

            configuration.setObjectName(configurationForm.getObjectName());
            configuration.setObjectType(configurationForm.getObjectType());
            configuration.setObjectDescription(configurationForm
                    .getObjectDescription());
            configuration.setIsActive(configurationForm.getIsActive());
            configuration.setModifiedBy(currentUserId);
            configuration.setModifiedOn(new Date());

            session.update(configuration);

            log.info(" ============update vendor object successful========== ");

            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException e) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(e);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;

    }
}
