/**
 * 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.common.Country;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorNAICS;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author pirabu
 * 
 */
public class CommonDaoImpl implements CommonDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Country> getCountries(UserDetailsDto appDetails) {

		List<Country> countries = new ArrayList<Country>();
		Locale[] locales = Locale.getAvailableLocales();
		for (Locale locale : locales) {
			try {
				String iso = locale.getISO3Country();
				String code = locale.getCountry();
				String name = locale.getDisplayCountry();

				if (!"".equals(iso) && !"".equals(code) && !"".equals(name)) {
					Country country = new Country(iso, code, name);
					if (!countries.contains(country)) {
						countries.add(country);
					}
				}
			} catch (MissingResourceException e) {

			}
		}
		Collections.sort(countries);
		return countries;
	}

	@Override
	public List<VendorMaster> getAllVendorsByCategory(Integer categoryId,
			Integer subCategoryId, UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<VendorMaster> vendors = new ArrayList<VendorMaster>();

		try {

			session.beginTransaction();

			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT * From customer_vendornaics vendorNaics INNER JOIN ");

			sqlQuery.append(" customer_naicsmaster naics on vendorNaics.NAICSID=naics.ID ");

			sqlQuery.append("INNER JOIN customer_vendormaster vendor ON vendorNaics.VENDORID=vendor.ID ");

			sqlQuery.append("INNER JOIN customer_naicscategorymaster category ON naics.NAICSCATEGORYID=category.ID ");

			sqlQuery.append("INNER JOIN customer_naicssubcategorymaster subCategory ON ");

			sqlQuery.append("subCategory.NAICSCATEGORYID=category.ID AND naics.NAICSSUBCATEGORYID=subCategory.ID ");

			sqlQuery.append("where category.ID=" + categoryId
					+ " and subCategory.ID=" + subCategoryId);

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			query.addEntity(VendorNAICS.class);

			query.addEntity(NaicsMaster.class);

			query.addEntity(VendorMaster.class);

			query.addEntity(NaicsCategory.class);

			query.addEntity(NAICSubCategory.class);

			Iterator iterator = query.list().iterator();

			while (iterator.hasNext()) {
				Object[] object = (Object[]) iterator.next();
				vendors.add((VendorMaster) object[2]);

			}

			session.getTransaction().commit();

		} catch (HibernateException e) {

			session.getTransaction().rollback();

			PrintExceptionInLogFile.printException(e);

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendors;

	}

	@Override
	public List<Integer> getSpendDataUploadedYears(UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<Integer> years = new ArrayList<Integer>();

		try {

			session.beginTransaction();

			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT DISTINCT DATAFORTHEYEAR From customer_spenddata_uploadmaster ORDER BY DATAFORTHEYEAR DESC");

			Query query = session.createSQLQuery(sqlQuery.toString());

			Iterator iterator = query.list().iterator();

			while (iterator.hasNext()) {
				Integer year = (Integer) iterator.next();
				years.add(year);

			}

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			PrintExceptionInLogFile.printException(e);

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return years;
	}

	@Override
	public String validateLoginId(String loginId, UserDetailsDto appDetails) {
		Session session = null;

		if (loginId == null) {
			return "userLogin";
		}
		if (appDetails.getUserType() != null
				&& appDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(appDetails));
		}
		String result = "submit";
		try {

			session.beginTransaction();
			Users userLogin = (Users) session.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", loginId).ignoreCase())
					.uniqueResult();

			VendorContact vendorLogin = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("emailId", loginId).ignoreCase())
					.uniqueResult();

			if (userLogin != null && vendorLogin != null) {

				result = "userLogin";
			} else if (userLogin != null) {

				result = "userLogin";

			} else if (vendorLogin != null) {
				result = "userLogin";
			}
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (appDetails.getUserType() != null
					&& !(appDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String checkDuplicateEmail(String emailId, UserDetailsDto appDetails) {
		Session session = null;

		if (emailId == null) {
			return "userEmail";
		}
		session = HibernateUtilCustomer.buildSessionFactory().openSession(
				DataBaseConnection.getConnection(appDetails));

		String result = "submit";
		try {
			session.beginTransaction();
			Users user = (Users) session.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", emailId).ignoreCase())
					.uniqueResult();

			VendorContact vendorUser = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("emailId", emailId).ignoreCase())
					.uniqueResult();
			// CustomerVendorSelfRegInfo selfRegInfo =
			// (CustomerVendorSelfRegInfo) session
			// .createQuery(
			// "from CustomerVendorSelfRegInfo where emailId = '"
			// + emailId + "'").uniqueResult();
			if (user != null && vendorUser != null) {
				result = "userEmail";
			} else if (user != null) {
				result = "userEmail";
			} else if (vendorUser != null) {
				result = "userEmail";
			}
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (appDetails.getUserType() != null
					&& !(appDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String validateVendorCode(String vendorCode,
			UserDetailsDto appDetails) {
		String validate = "success";
		VendorMaster vendorMaster = null;

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			vendorMaster = (VendorMaster) session
					.createCriteria(VendorMaster.class)
					.add(Restrictions.eq("vendorCode", vendorCode))
					.uniqueResult();
			// session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			// session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		if (vendorMaster != null) {
			validate = "vendorCode";
		}

		return validate;
	}
}
