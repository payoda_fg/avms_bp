package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.action.CustomerDivisionAction;
import com.fg.vms.customer.dao.CustomerDivisionDao;
import com.fg.vms.customer.dto.CustomerUsersDivisionDto;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerUserDivision;
import com.fg.vms.customer.pojo.CustomerDivisionForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the Customer Division implementation.
 * 
 * @author SHEFEEK.A
 * 
 */

public class CustomerDivisionDaoImpl implements CustomerDivisionDao {

	/** Logger of the system. */

	private static final Logger log = Constants.logger;

	@SuppressWarnings({ "deprecation" })
	@Override
	public String addDivision(CustomerDivisionForm customerDivisionForm,
			Integer currentUserId, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "unique";
		try {
			session.beginTransaction();

			/* To check unique division. */
			CustomerDivision division = (CustomerDivision) session
					.createCriteria(CustomerDivision.class)
					.add(Restrictions.eq("divisionname",
							customerDivisionForm.getDivisionName()))
					.uniqueResult();

			if (division != null) {
				return result;
			}
			CustomerDivision customerDivision = new CustomerDivision();
			/* Save the customer division details after validation. */
			log.info(" ============save division is starting========== ");
			customerDivision.setDivisionname(customerDivisionForm
					.getDivisionName());
			customerDivision.setDivisionshortname(customerDivisionForm
					.getDivisionShortName());
			customerDivision.setIsactive(Byte.valueOf(customerDivisionForm
					.getIsActive()));
			customerDivision.setCheckList(customerDivisionForm.getCheckList());
			customerDivision.setCreatedby(currentUserId);
			customerDivision.setCreatedon(new Date());
			customerDivision.setCertificationAgencies(customerDivisionForm
					.getCertificationAgencies());
			customerDivision.setRegistrationRequest(customerDivisionForm
					.getRegistrationRequest());
			customerDivision.setRegistrationQuestion(customerDivisionForm
					.getRegistrationQuestion());
			if(customerDivisionForm.getIsGlobal() != null && customerDivisionForm.getIsGlobal() != "0")
			{
				CustomerDivision isGlobalDivision = new UsersDaoImpl().getGlobalDivisionId(appDetails);
				if(isGlobalDivision == null)
				{
					customerDivision.setIsGlobal(Byte.valueOf(customerDivisionForm.getIsGlobal()));
				}
				else
					customerDivision.setIsGlobal((byte) 0);
			}
			else
				customerDivision.setIsGlobal((byte) 0);
			
			session.save(customerDivision);
			result = "success";
			session.beginTransaction().commit();

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	@SuppressWarnings("deprecation")
	@Override
	public String deleteDivision(int divisionId, Integer userId,
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		String result = "";
		CustomerDivision division = null;
		try {
			/* delete the selected customer division. */
			division = (CustomerDivision) session
					.createCriteria(CustomerDivision.class)
					.add(Restrictions.eq("id", divisionId)).uniqueResult();

			@SuppressWarnings("unchecked")
			List<Users> users = session.createCriteria(Users.class)
					.add(Restrictions.eq("customerDivisionId", division))
					.list();
			if (users != null && users.size() != 0) {
				return "reference";
			} else {
				if (division != null) {
					/* session.delete(division); */
					division.setModifiedby(userId);
					division.setModifiedon(new Date());
					division.setIsactive((byte) 0);
					session.merge(division);
					result = "success";
					session.getTransaction().commit();
				} else {
					result = "failure";
				}
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<CustomerDivision> retrieveDivision(UserDetailsDto userdetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userdetails));
		List<CustomerDivision> customerDivisions = null;

		try {
			session.beginTransaction();

			/*
			 * Get the list of customer divisions
			 */
			customerDivisions = session.createQuery(
					"From CustomerDivision ORDER BY divisionname ASC").list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return customerDivisions;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<CustomerDivision> retrieveDivisionAll(UserDetailsDto userdetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userdetails));
		List<CustomerDivision> customerDivisions = null;

		try {
			session.beginTransaction();

			/*
			 * Get the list of customer divisions
			 */
			customerDivisions = session
					.createQuery(
							"From CustomerDivision where isActive=1 ORDER BY divisionname ASC")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return customerDivisions;
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@Override
	public CustomerDivision editDivision(int divisionId,
			UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CustomerDivision division = null;
		try {
			/* View/Retrieve the based based on unique division id. */
			division = (CustomerDivision) session
					.createCriteria(CustomerDivision.class)
					.add(Restrictions.eq("id", divisionId)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
			return division;
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public String updateDivision(CustomerDivisionForm customerDivisionForm,
			Integer currentUser, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CustomerDivision division = null;
		try {
			division = (CustomerDivision) session
					.createCriteria(CustomerDivision.class)
					.add(Restrictions.eq("id", customerDivisionForm.getId()))
					.uniqueResult();
			if (customerDivisionForm.getIsActive().equalsIgnoreCase("0")) {
				@SuppressWarnings("unchecked")
				List<Users> users = session.createCriteria(Users.class)
						.add(Restrictions.eq("customerDivisionId", division))
						.list();
				if (users != null && users.size() != 0) {
					return "reference";
				}
			}
			String divisionName = customerDivisionForm.getDivisionName();
			if (divisionName.equals(CustomerDivisionAction.divisionName)) {
				division = (CustomerDivision) session.get(
						CustomerDivision.class, customerDivisionForm.getId());

				if (division != null) {
					/* Update the retrieved division details. */

					division.setDivisionname(customerDivisionForm
							.getDivisionName());
					division.setDivisionshortname(customerDivisionForm
							.getDivisionShortName());
					division.setIsactive(Byte.valueOf(customerDivisionForm
							.getIsActive()));
					division.setCheckList(customerDivisionForm.getCheckList());
					division.setCertificationAgencies(customerDivisionForm
							.getCertificationAgencies());
					division.setRegistrationRequest(customerDivisionForm
							.getRegistrationRequest());
					division.setRegistrationQuestion(customerDivisionForm
							.getRegistrationQuestion());
					division.setModifiedby(currentUser);
					division.setModifiedon(new Date());
					if(customerDivisionForm.getIsGlobal() != null && customerDivisionForm.getIsGlobal() != "0")
					{
						CustomerDivision isGlobalDivision = new UsersDaoImpl().getGlobalDivisionId(userDetails);
						if(isGlobalDivision != null && isGlobalDivision.getId() == division.getId() )
						{
							division.setIsGlobal(Byte.valueOf(customerDivisionForm.getIsGlobal()));
						}
						else
							division.setIsGlobal((byte) 0);
					}
					else
						division.setIsGlobal((byte) 0);
					session.update(division);
				}
				session.getTransaction().commit();
				return "update";
			} else {

				// To check unique division.
				division = (CustomerDivision) session
						.createCriteria(CustomerDivision.class)
						.add(Restrictions.eq("divisionname",
								customerDivisionForm.getDivisionName()))
						.uniqueResult();
				if (division != null) {

					return "unique";

				}
				division = (CustomerDivision) session.get(
						CustomerDivision.class, customerDivisionForm.getId());

				if (division != null) {
					/* Update the retrieved division details. */

					division.setDivisionname(customerDivisionForm
							.getDivisionName());
					division.setDivisionshortname(customerDivisionForm
							.getDivisionShortName());
					division.setIsactive(Byte.valueOf(customerDivisionForm
							.getIsActive()));
					division.setModifiedby(currentUser);
					division.setModifiedon(new Date());
					session.update(division);
				}
				session.getTransaction().commit();
				return "success";
			}

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";

		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			return "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Users> checkCustomerDivisionStatus(UserDetailsDto userDetails,
			Integer userId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<Users> customerDivisionStatus = null;
		try {
			session.beginTransaction();
			/*
			 * check the customer division status in table
			 */
			customerDivisionStatus = session.createSQLQuery(
					"select customerDivisionId from Users where id=" + userId)
					.list();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return customerDivisionStatus;
	}

	@Override
	public Long checkDivisionStatus(UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		Long division = null;
		try {
			session.beginTransaction();
			/*
			 * Get the list of customer divisions
			 */

			division = (Long) session.createQuery(
					"Select count(*) from CustomerDivision where isactive=1").uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return division;
	}

	@Override
	public String assignDivisionIdToUsers(UserDetailsDto userDetails,
			Integer userId, String userIds, Integer divisionId) {
		String result = "";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();
		try {
			CustomerDivision customerDivision = (CustomerDivision) session
					.get(CustomerDivision.class, divisionId);
			
				for (String usersId : userIds.split(",")) 
				{
					Users users = (Users) session.get(Users.class, Integer.parseInt(usersId));
					CustomerUserDivision customerUserDivision = (CustomerUserDivision) session
							.createCriteria(CustomerUserDivision.class)
							.add(Restrictions.eq("customerDivisionId", customerDivision))
							.add(Restrictions.eq("userId", users)).uniqueResult();
					
					if(customerUserDivision != null)
					{
						customerUserDivision.setCustomerDivisionId(customerDivision);
						customerUserDivision.setModifiedBy(userId);
						customerUserDivision.setModifiedOn(new Date());
						session.update(customerUserDivision);
					}
					else
					{
						CustomerUserDivision userDivision = new CustomerUserDivision();
						userDivision.setCustomerDivisionId(customerDivision);
						userDivision.setUserId(users);
						userDivision.setIsActive((byte) 1);
						userDivision.setCreatedBy(userId);
						userDivision.setCreatedOn(new Date());
						session.save(userDivision);
					}
				}
				
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {

			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public List<CustomerUsersDivisionDto> listUsers(UserDetailsDto userDetails, Integer divisionId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		
		List<CustomerUsersDivisionDto> divisionByUsers = new ArrayList<CustomerUsersDivisionDto>();
		CustomerUsersDivisionDto  customerUsersDivisionDto = null;
		try {
			session.beginTransaction();
			/*
			 * check the customer division status in table
			 */
			String query = "select u.ID, u.USERNAME, u.USEREMAILID, group_concat(d.DIVISIONNAME) as CURRENT_DIVISION, "
						+" group_concat(d.ID) as DIVISION_ID 	from users u"
						+ "	INNER JOIN customer_userdivision cu ON cu.USERID=u.ID LEFT JOIN customer_division d "
						+ " ON d.ID=cu.CUSTOMERDIVISIONID where u.id IN (select USERID from customer_userdivision"
						+ " where CUSTOMERDIVISIONID = "+ divisionId +" and ISACTIVE= 1 )  and cu.ISACTIVE= 1 "
						+ " and u.ISACTIVE=1 group by cu.USERID  order by u.USERNAME ASC";
			
			SQLQuery sqlQuery=session.createSQLQuery(query);
			List list=sqlQuery.list();
			Iterator iterator = null;
			if(list!=null)
			{
				iterator=list.iterator();
				while(iterator.hasNext())
				{
					customerUsersDivisionDto=new CustomerUsersDivisionDto();
					Object[] obj=(Object[])iterator.next();
					if(obj[0] != null)
						customerUsersDivisionDto.setUserId(Integer.parseInt(obj[0].toString()));
					if(obj[1] != null)
						customerUsersDivisionDto.setUserName(obj[1].toString());
					if(obj[2] != null)
						customerUsersDivisionDto.setUserEmailId(obj[2].toString());
					if(obj[3] != null)
						customerUsersDivisionDto.setCustomerDivisionNames(obj[3].toString());
					if(obj[4] != null)
						customerUsersDivisionDto.setCustomerDivisionIds(obj[4].toString());
					
					divisionByUsers.add(customerUsersDivisionDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return divisionByUsers;
	}

	@Override
	public String removeDivisionIdFromUsers(UserDetailsDto userDetails,
			Integer userId, String userIds, Integer divisionId) {

		String result = "";
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		session.beginTransaction();

		try {
			CustomerDivision customerDivision = (CustomerDivision) session
					.get(CustomerDivision.class, divisionId);
			
				for (String usersId : userIds.split(",")) 
				{
					Users users = (Users) session.get(Users.class, Integer.parseInt(usersId));
					CustomerUserDivision customerUserDivision = (CustomerUserDivision) session
							.createCriteria(CustomerUserDivision.class)
							.add(Restrictions.eq("customerDivisionId", customerDivision))
							.add(Restrictions.eq("userId", users)).uniqueResult();
					
					if(customerUserDivision != null)
					{
						customerUserDivision.setModifiedBy(userId);
						customerUserDivision.setModifiedOn(new Date());
						customerUserDivision.setIsActive((byte) 0);
						session.update(customerUserDivision);
					}
				}
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {

			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public List<CustomerUsersDivisionDto> listUsersByDivision(
			UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<CustomerUsersDivisionDto> divisionUsers = new ArrayList<CustomerUsersDivisionDto>();
		CustomerUsersDivisionDto  customerUsersDivisionDto = null;
		try {
			session.beginTransaction();
			
			
			String query = "select u.ID, u.USERNAME, u.USEREMAILID, group_concat(d.DIVISIONNAME) as CURRENT_DIVISION, "
					+ " group_concat(d.ID) as DIVISION_IDS "
					+ " From customer_userdivision cu LEFT JOIN customer_division d ON d.ID=cu.CUSTOMERDIVISIONID "
					+ " LEFT JOIN users u ON u.id=cu.USERID WHERE cu.ISACTIVE=1 and u.ISACTIVE=1  group by cu.USERID  order by u.USERNAME ";
			
			SQLQuery sqlQuery=session.createSQLQuery(query);
			List list=sqlQuery.list();
			Iterator iterator = null;
			if(list!=null)
			{
				iterator=list.iterator();
				while(iterator.hasNext())
				{
					customerUsersDivisionDto=new CustomerUsersDivisionDto();
					Object[] obj=(Object[])iterator.next();
					if(obj[0] != null)
						customerUsersDivisionDto.setUserId(Integer.parseInt(obj[0].toString()));
					if(obj[1] != null)
						customerUsersDivisionDto.setUserName(obj[1].toString());
					if(obj[2] != null)
						customerUsersDivisionDto.setUserEmailId(obj[2].toString());
					if(obj[3] != null)
						customerUsersDivisionDto.setCustomerDivisionNames(obj[3].toString());
					if(obj[4] != null)
						customerUsersDivisionDto.setCustomerDivisionIds(obj[4].toString());
					
					divisionUsers.add(customerUsersDivisionDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return divisionUsers;
	}
}