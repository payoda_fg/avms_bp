/**
 * CommodityDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * @author vinoth
 * 
 */
public class CommodityCategoryDaoImpl<T> implements CURDDao<T> {

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public T save(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public T update(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			CustomerCommodityCategory commodityCategory = (CustomerCommodityCategory) session
					.get(CustomerCommodityCategory.class,
							((CustomerCommodityCategory) entity).getId());
			if (commodityCategory != null) {
				commodityCategory
						.setCategoryCode(((CustomerCommodityCategory) entity)
								.getCategoryCode());
				commodityCategory
						.setCategoryDescription(((CustomerCommodityCategory) entity)
								.getCategoryDescription());
				commodityCategory
						.setModifiedBy(((CustomerCommodityCategory) entity)
								.getModifiedBy());
				commodityCategory.setModifiedOn(new Date());				
				commodityCategory.setIsActive(((CustomerCommodityCategory) entity).getIsActive());
				session.merge(commodityCategory);
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public T delete(T entity, UserDetailsDto usDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		String result = "";
		try {
			session.beginTransaction();
			CustomerCommodityCategory commodityCategory = (CustomerCommodityCategory) session
					.get(CustomerCommodityCategory.class,
							((CustomerCommodityCategory) entity).getId());
			if (commodityCategory != null) {
				commodityCategory.setIsActive((byte) 0);
				session.merge(commodityCategory);
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (T) result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<T> list(UserDetailsDto usDetails, String query) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<CustomerCommodityCategory> commodityCategories = null;
		try {
			session.getTransaction().begin();
			commodityCategories = session
					.createQuery(
							"From CustomerCommodityCategory where isActive=1 order by categoryDescription")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return (List<T>) commodityCategories;
	}
    
	//List the all commoditycategories
	@SuppressWarnings({ "unchecked", "deprecation" })	
	public List<CustomerCommodityCategory> listCommodityCategories(UserDetailsDto usDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(usDetails));

		List<CustomerCommodityCategory> commodityCategories = null;
		try 
		{
			session.getTransaction().begin();
			commodityCategories = session
					.createQuery(
							"From CustomerCommodityCategory order by categoryDescription")
					.list();
			session.getTransaction().commit();
		} 
		catch (HibernateException exception) 
		{
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} 
		finally 
		{
			if (session.isConnected()) 
			{
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return  commodityCategories;
	}
	
	@Override
	public T update1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T delete1(T entity, String name, UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T find(UserDetailsDto usDetails, String query) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public List<?> findAllByQuery(UserDetailsDto usDetails, String query) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public List<Ethnicity> listEthnicities(UserDetailsDto usDetails) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> listCommoditiesBasedOnSubSector(UserDetailsDto usDetails,
			Integer marketSubSectorId) {
		// TODO Auto-generated method stub
		return null;
	}

}
