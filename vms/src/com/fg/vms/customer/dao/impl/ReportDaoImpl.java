/*
 * ReportDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.upload.FormFile;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dto.ReportSearchFieldsDto;
import com.fg.vms.customer.dto.SearchReportDto;
import com.fg.vms.customer.dto.SpendReportDto;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.CustomReportSearchFields;
import com.fg.vms.customer.model.CustomerCommodityCategory;
import com.fg.vms.customer.model.DashboardSettings;
import com.fg.vms.customer.model.Ethnicity;
import com.fg.vms.customer.model.IndirectProcurementMarketsectors;
import com.fg.vms.customer.model.MarketSector;
import com.fg.vms.customer.model.ReportDue;
import com.fg.vms.customer.model.SpendDataUploadDetails;
import com.fg.vms.customer.model.Tier2ReportDirectExpenses;
import com.fg.vms.customer.model.Tier2ReportIndirectExpenses;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.ReportForm;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.customer.pojo.Tier2ReportForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SearchFields;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import com.mysql.jdbc.Connection;

/**
 * Represents the implementation of reportdao services.
 * 
 * @author srinivasarao
 * 
 */
public class ReportDaoImpl implements ReportDao {

	final static String OLD_FORMAT = "MMM dd,yyyy";
	final static String NEW_FORMAT = "yyyy-MM-dd";

	@Override
	public StringBuffer spendDataReport(UserDetailsDto userDetails) {

		StringBuffer buffer = new StringBuffer();

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		try {
			session.beginTransaction();

			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT {primevendor.*},{nonprimevendor.*} , ");
			sqlQuery.append("{detail.*} From customer_spenddata_uploaddetail as detail  ");
			sqlQuery.append("INNER JOIN customer_spenddata_uploadmaster  ON "
					+ "(detail.SPENDDATAMASTERID = customer_spenddata_uploadmaster.ID) ");
			sqlQuery.append("AND detail.ISVALID=1 INNER JOIN customer_vendormaster as primevendor ON (customer_spenddata_uploadmaster.PRIMEVENDORID = primevendor.ID) ");
			sqlQuery.append("INNER JOIN customer_vendormaster as nonprimevendor ON (customer_spenddata_uploadmaster.NONPRIMEVENDORID = nonprimevendor.ID)");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			query.addEntity("primevendor", VendorMaster.class);
			query.addEntity("nonprimevendor", VendorMaster.class);
			query.addEntity("detail", SpendDataUploadDetails.class);
			List<?> listof_spendvalue = query.list();

			session.getTransaction().commit();

			Iterator<?> iterator = listof_spendvalue.iterator();
			/* List of Decrypted spend value. */
			List<SpendReportDto> listDecryptedSpendValues = new ArrayList<SpendReportDto>();
			while (iterator.hasNext()) {
				SpendReportDto objSpendReportDto = new SpendReportDto();
				Object[] object = (Object[]) iterator.next();
				objSpendReportDto.setSupplier(((VendorMaster) object[0])
						.getVendorName());
				objSpendReportDto.setTire2Supplier(((VendorMaster) object[1])
						.getVendorName());
				objSpendReportDto
						.setDiverseType(((SpendDataUploadDetails) object[2])
								.getDiverseCertificate());
				/* get the key value */
				Integer keyvalue = ((SpendDataUploadDetails) object[2])
						.getKeyValue();
				Decrypt decrypt = new Decrypt();
				/* Using the key value to decrypt the spend value. */
				String spendAmt = decrypt.decryptText(String.valueOf(keyvalue
						.toString()), ((SpendDataUploadDetails) object[2])
						.getSpendValue().toString());

				objSpendReportDto.setAmount(Double.parseDouble(spendAmt));

				listDecryptedSpendValues.add(objSpendReportDto);
			}

			// List of Diverse classifications.
			Map<String, String> diverseCertificates = new HashMap<String, String>();
			// list of non prime vendors
			Map<String, String> tire2Vendors = new HashMap<String, String>();

			/*
			 * Collect the Diverse classifications and non prime vendors from
			 * the listDecryptedSpendValues
			 */
			for (SpendReportDto dto : listDecryptedSpendValues) {
				diverseCertificates.put(dto.getDiverseType(),
						dto.getDiverseType());
				tire2Vendors
						.put(dto.getTire2Supplier(), dto.getTire2Supplier());

			}
			// Group by tire2 supplier
			ImmutableListMultimap<String, SpendReportDto> spendGroupByTire2Vendor = Multimaps
					.index(listDecryptedSpendValues,
							new Function<SpendReportDto, String>() {
								public String apply(SpendReportDto spend) {
									return spend.getTire2Supplier();

								}
							});

			// Get a set of the Tire 2 vendor
			Set<Entry<String, String>> setTire2Vendors = tire2Vendors
					.entrySet();
			// Get an iterator
			Iterator<Entry<String, String>> iteratorTire2Vendors = setTire2Vendors
					.iterator();
			// list of spend data for report.
			List<SpendReportDto> listOfSpendValues = new ArrayList<SpendReportDto>();
			// Spend value group by tire2 vendor and diverse certificate.
			while (iteratorTire2Vendors.hasNext()) {
				Map.Entry<String, String> tire2 = (Map.Entry<String, String>) iteratorTire2Vendors
						.next();

				List<SpendReportDto> listOfSpendByTire2Vendor = spendGroupByTire2Vendor
						.get(tire2.getKey());

				// Group by Diverse certifivate
				ImmutableListMultimap<String, SpendReportDto> spendGroupByDiverse = Multimaps
						.index(listOfSpendByTire2Vendor,
								new Function<SpendReportDto, String>() {
									public String apply(SpendReportDto spend) {
										return spend.getDiverseType();

									}
								});

				// Get a set of the Diverse Certificate
				Set<Entry<String, String>> setDiverse = diverseCertificates
						.entrySet();
				// Get an iterator
				Iterator<Entry<String, String>> iteratorDiverse = setDiverse
						.iterator();

				while (iteratorDiverse.hasNext()) {
					Map.Entry<String, String> diverse = (Map.Entry<String, String>) iteratorDiverse
							.next();
					List<SpendReportDto> listOfSpendByDiverse = spendGroupByDiverse
							.get(diverse.getKey());
					if (listOfSpendByDiverse != null
							&& listOfSpendByDiverse.size() != 0) {

						Double sumOfspendValue = 0.0;
						SpendReportDto objSpendReport = null;
						for (SpendReportDto spend : listOfSpendByDiverse) {
							objSpendReport = new SpendReportDto();
							objSpendReport.setSupplier(spend.getSupplier());
							objSpendReport.setTire2Supplier(spend
									.getTire2Supplier());
							objSpendReport.setDiverseType(spend
									.getDiverseType());
							sumOfspendValue += spend.getAmount();
							objSpendReport.setAmount(sumOfspendValue);
						}
						listOfSpendValues.add(objSpendReport);
					}

				}

			}

			if (listOfSpendValues != null && listOfSpendValues.size() != 0) {

				buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
				buffer.append("<rows> \n <head>  \n");
				buffer.append("<column width=\"300\" type=\"ro\" align=\"left\" sort=\"str\">Supplier</column> \n");
				buffer.append("<column width=\"150\" type=\"ro\" align=\"left\" sort=\"str\">Tier 2 supplier</column> \n");
				buffer.append("<column width=\"160\" type=\"ro\" align=\"center\" sort=\"str\">Diverse Classification</column> \n");
				buffer.append("<column width=\"*\" type=\"edn\" format=\"$0,000.00\" align=\"right\" sort=\"str\">Amount</column> \n");
				buffer.append("<settings><colwidth>px</colwidth></settings> \n");
				buffer.append("</head> \n");

				int i = 1;
				for (SpendReportDto ss : listOfSpendValues) {
					buffer.append("<row id=\"" + i + "\">\n");
					buffer.append("<cell>" + ss.getSupplier() + "</cell>\n");
					buffer.append("<cell>" + ss.getTire2Supplier()
							+ "</cell>\n");
					buffer.append("<cell>" + ss.getDiverseType() + "</cell>\n");
					buffer.append("<cell>" + ss.getAmount() + "</cell>\n");
					// buffer.append("<cell>" + noOfSupplier + "</cell>\n");
					buffer.append("</row>\n");
					i++;
				}
				buffer.append("</rows>");
			} else {
				buffer.append("empty");
			}

		} catch (HibernateException ex) {
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return buffer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringBuffer diversityReport(UserDetailsDto userDetails,
			SearchVendorForm searchVendorForm) {

		StringBuffer buffer = new StringBuffer();
		List<SpendReportDto> listOfSpendValues = new ArrayList<SpendReportDto>();

		Integer vendor = searchVendorForm.getVendorId();
		Integer year = searchVendorForm.getYear();
		Integer previousYear = searchVendorForm.getYear() - 1;

		List<SpendReportDto> listOfSpendValuesByYear = getSpendValuesByYear(
				vendor, year, userDetails);

		List<SpendReportDto> listOfSpendValuesByPreviousYear = getSpendValuesByYear(
				vendor, previousYear, userDetails);

		List<SpendReportDto> removeListOfSpendValues = new ArrayList<SpendReportDto>();

		for (SpendReportDto objSpendValueByYear : listOfSpendValuesByYear) {
			if (listOfSpendValuesByPreviousYear != null
					&& listOfSpendValuesByPreviousYear.size() != 0) {

				for (SpendReportDto objSpendValueByPreyear : listOfSpendValuesByPreviousYear) {
					if (objSpendValueByYear.getDiverseType().equalsIgnoreCase(
							objSpendValueByPreyear.getDiverseType())
							&& objSpendValueByYear.getSupplier()
									.equalsIgnoreCase(
											objSpendValueByPreyear
													.getSupplier())) {
						SpendReportDto objSpendValue = new SpendReportDto();
						objSpendValue
								.setAmount(objSpendValueByYear.getAmount());
						objSpendValue.setDiverseType(objSpendValueByYear
								.getDiverseType());
						objSpendValue.setSupplier(objSpendValueByYear
								.getSupplier());
						objSpendValue
								.setPreviousYearAmount(objSpendValueByPreyear
										.getAmount());

						listOfSpendValues.add(objSpendValue);
						removeListOfSpendValues.add(objSpendValueByYear);
						listOfSpendValuesByPreviousYear
								.remove(objSpendValueByPreyear);

						break;
					}

				}
			} else {
				listOfSpendValues.add(objSpendValueByYear);
			}

		}
		if (listOfSpendValuesByPreviousYear != null
				&& listOfSpendValuesByPreviousYear.size() != 0) {
			for (SpendReportDto objSpendValueByPreyear : listOfSpendValuesByPreviousYear) {
				SpendReportDto objSpendValue = new SpendReportDto();
				objSpendValue.setAmount(0.0);
				objSpendValue.setDiverseType(objSpendValueByPreyear
						.getDiverseType());
				objSpendValue.setSupplier(objSpendValueByPreyear.getSupplier());
				objSpendValue.setPreviousYearAmount(objSpendValueByPreyear
						.getAmount());

				listOfSpendValues.add(objSpendValue);
			}
		}
		listOfSpendValuesByYear.removeAll(removeListOfSpendValues);
		for (SpendReportDto objSpendValueByyear : listOfSpendValuesByYear) {
			SpendReportDto objSpendValue = new SpendReportDto();
			objSpendValue.setAmount(objSpendValueByyear.getAmount());
			objSpendValue.setDiverseType(objSpendValueByyear.getDiverseType());
			objSpendValue.setSupplier(objSpendValueByyear.getSupplier());
			objSpendValue.setPreviousYearAmount(0.0);

			listOfSpendValues.add(objSpendValue);
		}
		int i = 1;
		if (listOfSpendValues != null && listOfSpendValues.size() != 0) {
			buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
			buffer.append("<rows> \n <head> \n");

			buffer.append("<column width=\"300\" type=\"ro\" align=\"left\" sort=\"str\">Supplier</column> \n");
			buffer.append("<column width=\"150\" type=\"ro\" align=\"center\" sort=\"str\">Diverse Classification</column> \n");
			buffer.append("<column width=\"200\" type=\"edn\" format=\"$0,000.00\" align=\"right\" sort=\"str\">Previous Year Amount</column> \n");
			buffer.append("<column width=\"*\" type=\"edn\" format=\"$0,000.00\" align=\"right\" sort=\"str\">Amount</column> \n");
			buffer.append("<settings><colwidth>px</colwidth></settings> \n");
			buffer.append("</head> \n");

			for (SpendReportDto spend : listOfSpendValues) {

				buffer.append("<row id=\"" + i + "\">\n");

				buffer.append("<cell>" + spend.getSupplier() + "</cell>\n");
				buffer.append("<cell>" + spend.getDiverseType() + "</cell>\n");
				buffer.append("<cell>" + spend.getPreviousYearAmount()
						+ "</cell>\n");
				buffer.append("<cell>" + spend.getAmount() + "</cell>\n");
				buffer.append("</row>\n");
				i++;
			}
			buffer.append("</rows>");
		} else {
			buffer.append("empty");
		}

		return buffer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringBuffer yearwiseReport(UserDetailsDto userDetails,
			SearchVendorForm searchVendorForm) {

		StringBuffer buffer = new StringBuffer();
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DataBaseConnection
					.getConnection(userDetails);

			Integer year = searchVendorForm.getYear();
			Integer previousYear = searchVendorForm.getYear() - 1;

			String sqlQuery = " select `CUSTOMER_VENDORMASTER`.VENDORNAME supplier,DATAFORTHEYEAR,"
					+ " sum(`CUSTOMER_SPENDDATA_UPLOADDETAIL`.SPENDVALUE) CurrentYearValue from `CUSTOMER_SPENDDATA_UPLOADDETAIL`,"
					+ " `CUSTOMER_VENDORMASTER`,  `CUSTOMER_SPENDDATA_UPLOADMASTER` where (`CUSTOMER_SPENDDATA_UPLOADDETAIL`.SPENDDATAMASTERID=`CUSTOMER_SPENDDATA_UPLOADMASTER`.ID)"
					+ " and (`CUSTOMER_SPENDDATA_UPLOADMASTER`.VENDORID= `CUSTOMER_VENDORMASTER`.ID) and (`CUSTOMER_VENDORMASTER`.PARENTVENDORID>0) and"
					+ " (`CUSTOMER_SPENDDATA_UPLOADMASTER`.DATAFORTHEYEAR= '"
					+ year
					+ "'"
					+ ") group by  `CUSTOMER_VENDORMASTER`.VENDORNAME,supplier,datafortheyear"
					+ " union all"
					+ " select `CUSTOMER_VENDORMASTER`.VENDORNAME supplier,DATAFORTHEYEAR,"
					+ " sum(`CUSTOMER_SPENDDATA_UPLOADDETAIL`.SPENDVALUE) CurrentYearValue from `CUSTOMER_SPENDDATA_UPLOADDETAIL`,"
					+ " `CUSTOMER_VENDORMASTER`,  `CUSTOMER_SPENDDATA_UPLOADMASTER`"
					+ " where (`CUSTOMER_SPENDDATA_UPLOADDETAIL`.SPENDDATAMASTERID=`CUSTOMER_SPENDDATA_UPLOADMASTER`.ID) and"
					+ " (`CUSTOMER_SPENDDATA_UPLOADMASTER`.VENDORID= `CUSTOMER_VENDORMASTER`.ID) and"
					+ " (`CUSTOMER_VENDORMASTER`.PARENTVENDORID>0) and (`CUSTOMER_SPENDDATA_UPLOADMASTER`.DATAFORTHEYEAR='"
					+ previousYear
					+ "'"
					+ ")"
					+ " group by  `CUSTOMER_VENDORMASTER`.VENDORNAME,supplier,datafortheyear";

			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sqlQuery);

			int i = 1;
			if (result.next() == true) {
				buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
				buffer.append("<rows> \n <head> \n");
				buffer.append("<column width=\"265\" type=\"ro\" align=\"left\" sort=\"str\">Supplier</column> \n");
				buffer.append("<column width=\"265\" type=\"ro\" align=\"right\" sort=\"str\">Data For The Year</column> \n");
				buffer.append("<column width=\"265\" type=\"ro\" align=\"right\" sort=\"str\">CurrentYearValue</column> \n");
				buffer.append("<settings><colwidth>px</colwidth></settings> \n");
				buffer.append("</head> \n");
				String supplier = result.getString(1);
				String dataForTheYear = result.getString(2);
				String currentYearValue = result.getString(3);
				buffer.append("<row id=\"" + i + "\">\n");
				buffer.append("<cell>" + supplier + "</cell>\n");
				buffer.append("<cell>" + dataForTheYear + "</cell>\n");
				buffer.append("<cell>" + currentYearValue + "</cell>\n");
				buffer.append("</row>\n");
				i++;

				while (result.next()) {
					supplier = result.getString(1);
					dataForTheYear = result.getString(2);
					currentYearValue = result.getString(3);

					buffer.append("<row id=\"" + i + "\">\n");
					buffer.append("<cell>" + supplier + "</cell>\n");
					buffer.append("<cell>" + dataForTheYear + "</cell>\n");
					buffer.append("<cell>" + currentYearValue + "</cell>\n");
					buffer.append("</row>\n");
					i++;
				}
				buffer.append("</rows>");
			} else {
				buffer.append("empty");
			}
		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				PrintExceptionInLogFile.printException(e);
			}
		}
		return buffer;
	}

	/**
	 * Get the spend data by year.
	 * 
	 * @param vendor
	 * @param year
	 * @return
	 */
	public static List<SpendReportDto> getSpendValuesByYear(Integer vendor,
			Integer year, UserDetailsDto userDetails) {

		Connection connection = null;
		// list of spend data for report.
		List<SpendReportDto> listOfSpendValues = new ArrayList<SpendReportDto>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DataBaseConnection
					.getConnection(userDetails);

			String query = "SELECT spend_detail.keyvalue, spend_detail.SPENDVALUE amount,t2vendor.VENDORNAME supplier, spend_detail.`DIVERSECERTIFICATE` diverse "
					+ "FROM  CUSTOMER_VENDORMASTER vendor,CUSTOMER_VENDORMASTER t2vendor,"
					+ "CUSTOMER_SPENDDATA_UPLOADMASTER spend_master,CUSTOMER_SPENDDATA_UPLOADDETAIL spend_detail "
					+ "WHERE (vendor.ID =  spend_master.PRIMEVENDORID) and (t2vendor.ID =  spend_master.NONPRIMEVENDORID) "
					+ "and ( spend_master.ID =  spend_detail.SPENDDATAMASTERID) and ( spend_master.DATAFORTHEYEAR = "
					+ year
					+ ") and ( spend_master.primevendorID = "
					+ vendor
					+ "	) and spend_detail.isvalid=1 "
					+ "ORDER BY vendor.VENDORNAME ASC,  spend_detail.DIVERSECERTIFICATE ASC";

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			// List of Decrypted spend value.
			List<SpendReportDto> listDecryptedSpendValues = new ArrayList<SpendReportDto>();

			while (rs.next()) {

				SpendReportDto reportDto = new SpendReportDto();
				// get the key value
				String keyvalue = rs.getString(1);
				Decrypt decrypt = new Decrypt();
				// Using the key value to decrypt the spend value.
				String spendAmt = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						(rs.getString(2).toString()));

				reportDto.setAmount(Double.parseDouble(spendAmt));
				reportDto.setSupplier(rs.getString(3));
				reportDto.setDiverseType(rs.getString(4));
				listDecryptedSpendValues.add(reportDto);

			}

			// List of Diverse classifications.
			Map<String, String> diverseCertificates = new HashMap<String, String>();
			// list of non prime vendors
			Map<String, String> supplier = new HashMap<String, String>();

			// Collect the Diverse classifications and non prime vendors
			// from
			// the listDecryptedSpendValues
			for (SpendReportDto dto : listDecryptedSpendValues) {
				diverseCertificates.put(dto.getDiverseType(),
						dto.getDiverseType());
				supplier.put(dto.getSupplier(), dto.getSupplier());

			}
			// Group by supplier
			ImmutableListMultimap<String, SpendReportDto> spendGroupByTire2Vendor = Multimaps
					.index(listDecryptedSpendValues,
							new Function<SpendReportDto, String>() {
								public String apply(SpendReportDto spend) {
									return spend.getSupplier();

								}
							});

			// Get a set of the prime vendor
			Set<Entry<String, String>> setPrimeVendors = supplier.entrySet();
			// Get an iterator
			Iterator<Entry<String, String>> iteratorPrimeVendors = setPrimeVendors
					.iterator();

			// Spend value group by tire2 vendor and diverse certificate.
			while (iteratorPrimeVendors.hasNext()) {
				Map.Entry<String, String> tire2 = (Map.Entry<String, String>) iteratorPrimeVendors
						.next();

				List<SpendReportDto> listOfSpendByTire2Vendor = spendGroupByTire2Vendor
						.get(tire2.getKey());

				// Group by Diverse certificate
				ImmutableListMultimap<String, SpendReportDto> spendGroupByDiverse = Multimaps
						.index(listOfSpendByTire2Vendor,
								new Function<SpendReportDto, String>() {
									public String apply(SpendReportDto spend) {
										return spend.getDiverseType();

									}
								});

				// Get a set of the Diverse Certificate
				Set<Entry<String, String>> setDiverse = diverseCertificates
						.entrySet();
				// Get an iterator
				Iterator<Entry<String, String>> iteratorDiverse = setDiverse
						.iterator();

				while (iteratorDiverse.hasNext()) {
					Map.Entry<String, String> diverse = (Map.Entry<String, String>) iteratorDiverse
							.next();
					List<SpendReportDto> listOfSpendByDiverse = spendGroupByDiverse
							.get(diverse.getKey());
					if (listOfSpendByDiverse != null
							&& listOfSpendByDiverse.size() != 0) {

						Double sumOfspendValue = 0.0;
						Double sumOfPreviousYearSpendValue = 0.0;
						SpendReportDto objSpendReport = null;
						for (SpendReportDto spend : listOfSpendByDiverse) {
							objSpendReport = new SpendReportDto();
							objSpendReport.setSupplier(spend.getSupplier());

							objSpendReport.setDiverseType(spend
									.getDiverseType());
							sumOfspendValue += spend.getAmount();
							// sumOfPreviousYearSpendValue += spend
							// .getPreviousYearAmount();
							objSpendReport.setAmount(sumOfspendValue);
							objSpendReport
									.setPreviousYearAmount(sumOfPreviousYearSpendValue);
						}
						listOfSpendValues.add(objSpendReport);
					}

				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listOfSpendValues;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringBuffer indirectSpendReport(UserDetailsDto userDetails,
			ReportForm reportForm) {

		StringBuffer buffer = new StringBuffer();

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		try {
			session.beginTransaction();

			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT indirectspenddeteil.DIVERSECERTIFICATE,indirectspenddeteil.INDIRECTSPENDVALUE,");
			sqlQuery.append("indirectspenddeteil.KEYVALUE, vendor.VENDORNAME FROM customer_spenddata_uploadmaster spendmaster ");
			sqlQuery.append("INNER JOIN customer_indirect_spend_data_detail indirectspenddeteil "
					+ "ON spendmaster.ID = indirectspenddeteil.SPENDDATAMASTERID ");
			sqlQuery.append("INNER JOIN customer_vendormaster vendor ");
			sqlQuery.append("ON vendor.ID = spendmaster.PRIMEVENDORID where ");

			if (reportForm.getOption().equalsIgnoreCase("Supplier")) {
				sqlQuery.append(" spendmaster.PRIMEVENDORID IN (");

				for (int index = 0; index < reportForm.getSupplier().length; index++) {

					sqlQuery.append(Integer.parseInt(reportForm.getSupplier()[index]));
					if (index != reportForm.getSupplier().length - 1) {
						sqlQuery.append(",");
					}
				}

				sqlQuery.append(") ");
			}
			if (reportForm.getOption().equalsIgnoreCase("Quarter")
					&& reportForm.getStartDate() != null
					&& reportForm.getEndDate() != null
					&& reportForm.getEndDate().length() != 0
					&& reportForm.getStartDate().length() != 0) {
				java.sql.Date startDate = new java.sql.Date(CommonUtils
						.dateConvertValue(reportForm.getStartDate()).getTime());
				java.sql.Date endDate = new java.sql.Date(CommonUtils
						.dateConvertValue(reportForm.getEndDate()).getTime());
				sqlQuery.append("spendmaster.SPEND_STARTDATE >='" + startDate
						+ "' and spendmaster.SPEND_ENDDATE <='" + endDate + "'");
			}
			if (reportForm.getOption().equalsIgnoreCase("Year")) {
				sqlQuery.append(" spendmaster.DATAFORTHEYEAR in ( ");
				for (int index = 0; index < reportForm.getYear().length; index++) {

					sqlQuery.append(Integer.parseInt(reportForm.getYear()[index]));
					if (index != reportForm.getYear().length - 1) {
						sqlQuery.append(",");
					}
				}

				sqlQuery.append(") ");

			}

			if (reportForm.getOption().equalsIgnoreCase("YearToDate")) {

				Date today = new Date();
				Date yearStartDate = new Date();
				yearStartDate.setDate(01);
				yearStartDate.setMonth(0);
				java.sql.Date startDate = new java.sql.Date(
						yearStartDate.getTime());
				java.sql.Date endDate = new java.sql.Date(today.getTime());
				sqlQuery.append("spendmaster.SPEND_STARTDATE between '"
						+ startDate + "' and '" + endDate
						+ "' or spendmaster.SPEND_ENDDATE between '"
						+ startDate + "' and '" + endDate + "'");

			}

			Query query = session.createSQLQuery(sqlQuery.toString());

			List<?> listof_spendvalue = query.list();

			session.getTransaction().commit();

			Iterator<?> iterator = listof_spendvalue.iterator();
			// List of Decrypted spend value.
			List<SpendReportDto> listDecryptedSpendValues = new ArrayList<SpendReportDto>();
			while (iterator.hasNext()) {
				SpendReportDto objSpendReportDto = new SpendReportDto();
				Object[] object = (Object[]) iterator.next();
				objSpendReportDto.setDiverseType((object[0]).toString());

				// get the key value
				Integer keyvalue = Integer.parseInt((object[2]).toString());
				Decrypt decrypt = new Decrypt();
				// Using the key value to decrypt the spend value.
				String spendAmt = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						(object[1]).toString());

				objSpendReportDto.setAmount(Double.parseDouble(spendAmt));
				objSpendReportDto.setSupplier((object[3]).toString());

				listDecryptedSpendValues.add(objSpendReportDto);
			}

			// List of Diverse classifications.
			Map<String, String> diverseCertificates = new HashMap<String, String>();
			// list of non prime vendors
			Map<String, String> supplier = new HashMap<String, String>();

			// Collect the Diverse classifications and non prime vendors
			// from
			// the listDecryptedSpendValues
			for (SpendReportDto dto : listDecryptedSpendValues) {
				diverseCertificates.put(dto.getDiverseType(),
						dto.getDiverseType());
				supplier.put(dto.getSupplier(), dto.getSupplier());

			}
			// Group by tire2 supplier
			ImmutableListMultimap<String, SpendReportDto> spendGroupByTire2Vendor = Multimaps
					.index(listDecryptedSpendValues,
							new Function<SpendReportDto, String>() {
								public String apply(SpendReportDto spend) {
									return spend.getSupplier();

								}
							});

			// Get a set of the Supplier
			Set<Entry<String, String>> listOfSupplier = supplier.entrySet();
			// Get an iterator
			Iterator<Entry<String, String>> iteratorListOfSupplier = listOfSupplier
					.iterator();
			// list of spend data for report.
			List<SpendReportDto> listOfSpendValues = new ArrayList<SpendReportDto>();
			// Spend value group by supplier and diverse certificate.
			while (iteratorListOfSupplier.hasNext()) {
				Map.Entry<String, String> primeVendor = (Map.Entry<String, String>) iteratorListOfSupplier
						.next();

				List<SpendReportDto> listOfSpendByPrimeSupplier = spendGroupByTire2Vendor
						.get(primeVendor.getKey());

				// Group by Diverse certifivate
				ImmutableListMultimap<String, SpendReportDto> spendGroupByDiverse = Multimaps
						.index(listOfSpendByPrimeSupplier,
								new Function<SpendReportDto, String>() {
									public String apply(SpendReportDto spend) {
										return spend.getDiverseType();

									}
								});

				// Get a set of the Diverse Certificate
				Set<Entry<String, String>> listOfDiverse = diverseCertificates
						.entrySet();
				// Get an iterator
				Iterator<Entry<String, String>> iteratorDiverse = listOfDiverse
						.iterator();

				while (iteratorDiverse.hasNext()) {
					Map.Entry<String, String> diverse = (Map.Entry<String, String>) iteratorDiverse
							.next();
					List<SpendReportDto> listOfSpendByDiverse = spendGroupByDiverse
							.get(diverse.getKey());
					if (listOfSpendByDiverse != null
							&& listOfSpendByDiverse.size() != 0) {

						Double sumOfspendValue = 0.0;
						SpendReportDto objSpendReport = null;
						for (SpendReportDto spend : listOfSpendByDiverse) {
							objSpendReport = new SpendReportDto();
							objSpendReport.setSupplier(spend.getSupplier());

							objSpendReport.setDiverseType(spend
									.getDiverseType());
							sumOfspendValue += spend.getAmount();
							objSpendReport.setAmount(sumOfspendValue);
						}
						listOfSpendValues.add(objSpendReport);
					}

				}

			}

			/**
			 * build the xml using spend data
			 */
			if (listOfSpendValues != null && listOfSpendValues.size() != 0) {
				buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
				buffer.append("<rows> \n <head>  \n");
				buffer.append("<column width=\"300\" type=\"ro\" align=\"left\" sort=\"str\">Supplier</column> \n");

				buffer.append("<column width=\"160\" type=\"ro\" align=\"center\" sort=\"str\">Diverse Classification</column> \n");
				buffer.append("<column width=\"*\" type=\"edn\" format=\"$0,000.00\" align=\"right\" sort=\"str\">Amount</column> \n");

				buffer.append("<settings><colwidth>px</colwidth></settings> \n");
				buffer.append("</head> \n");

				int i = 1;// row id.
				for (SpendReportDto ss : listOfSpendValues) {
					buffer.append("<row id=\"" + i + "\">\n");
					buffer.append("<cell>" + ss.getSupplier() + "</cell>\n");
					buffer.append("<cell>" + ss.getDiverseType() + "</cell>\n");
					buffer.append("<cell>" + ss.getAmount() + "</cell>\n");
					buffer.append("</row>\n");
					i++;
				}
				buffer.append("</rows>");
			} else {
				buffer.append("empty");
			}

		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return buffer;
	}

	@Override
	public List<Certificate> tier2Certificates(UserDetailsDto userDetails,
			VendorMaster vendor) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<Certificate> certificates = new ArrayList<Certificate>();
		try {
			session.beginTransaction();
			VendorMaster master = (VendorMaster) session.get(
					VendorMaster.class, vendor.getId());
			for (VendorCertificate certificate : master
					.getVendorcertificateList()) {
				certificates.add(certificate.getCertMasterId());
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return certificates;
	}

	@Override
	public Tier2ReportMaster saveTier2Report(Tier2ReportForm form,
			UserDetailsDto userDetails, VendorContact vendor, String submitType) {
		/* session object represents customer database1 */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Tier2ReportMaster master = new Tier2ReportMaster();

		try {
			session.getTransaction().begin();

			master.setReportingPeriod(form.getReportingPeriod());
			String period = form.getReportingPeriod();
			Date startDate = null;
			Date endDate = null;
			if (period != null && !period.isEmpty()) {
				SimpleDateFormat sdf = new SimpleDateFormat(NEW_FORMAT);
				String peString[] = period.split("-");
				startDate = new Date(sdf.parse(getReportingPeriod(peString[0]))
						.getTime());
				endDate = new Date(sdf.parse(getReportingPeriod(peString[1]))
						.getTime());
				master.setReportingStartDate(startDate);
				master.setReportingEndDate(endDate);
			}

			master.setVendorId(vendor.getVendorId());

			master.setTotalSales(Double.valueOf(form
					.getTotalUsSalesSelectedQtr()));
			master.setTotalSalesToCompany(Double.valueOf(form
					.getTotalUsSalestoSelectedQtr()));
			master.setIndirectAllocationPercentage(Float.valueOf(form
					.getIndirectPer()));
			master.setComments(form.getComments());

			int isSubmitted = 0;
			if (submitType.equalsIgnoreCase("submit")) {
				isSubmitted = 1;
				updateReportDue(startDate, endDate, userDetails, vendor);
			} else if (submitType.equalsIgnoreCase("save")) {
				isSubmitted = 0;
			}
			master.setIsSubmitted(isSubmitted);
			master.setCreatedBy(vendor.getId());
			master.setCreatedOn(new Date());
			session.persist(master);
			int i = 0;
			if (null != form.getDirectAmt() && form.getDirectAmt().length != 0
					&& null != form.getTier2vendorName()
					&& form.getTier2vendorName().length != 0
					&& null != form.getCertificateDrt()
					&& form.getCertificateDrt().length != 0) {
				int indexDrt = 0;
				for (String amount : form.getDirectAmt()) {
					if (null != amount && !amount.isEmpty()
							&& !form.getTier2vendorName()[i].isEmpty()
							&& !form.getCertificateDrt()[i].isEmpty()) {
						Tier2ReportDirectExpenses directExpenses = new Tier2ReportDirectExpenses();
						directExpenses.setDirExpenseAmt(Double.valueOf(amount));
						if (form.getTier2vendorName() != null
								&& form.getTier2vendorName().length > i) {

							VendorMaster tier2Vendorid = (VendorMaster) session
									.get(VendorMaster.class,
											Integer.valueOf(form
													.getTier2vendorName()[i]));
							directExpenses.setTier2Vendorid(tier2Vendorid);
						}
						if (null != form.getCertificateDrt()
								&& form.getCertificateDrt().length > i) {

							Certificate certificate = (Certificate) session
									.get(Certificate.class,
											Integer.valueOf(form
													.getCertificateDrt()[i]));
							directExpenses.setCertificateId(certificate);
						}
						directExpenses.setTier2ReoprtMasterid(master);
						directExpenses.setCreatedBy(vendor.getId());
						directExpenses.setCreatedOn(new Date());

						CustomerCommodityCategory marketSubSector = null;
						if (form.getMarketSectorDrt() != null
								&& !"".equalsIgnoreCase(form
										.getMarketSectorDrt()[i])) {
							marketSubSector = (CustomerCommodityCategory) session.get(
									CustomerCommodityCategory.class, Integer.valueOf(form
											.getMarketSectorDrt()[i]));

							directExpenses.setMarketSubSector(marketSubSector);
						}
						directExpenses.setMarketSubSector(marketSubSector);

						Ethnicity ethnicity = null;
						if (form.getEthnicityDrt() != null
								&& !"".equalsIgnoreCase(form.getEthnicityDrt()[indexDrt])) {
							ethnicity = (Ethnicity) session.get(
									Ethnicity.class, Integer.valueOf(form
											.getEthnicityDrt()[indexDrt]));

							directExpenses.setEthnicityId(ethnicity);
						}
						session.persist(directExpenses);
					}
					i++;
					indexDrt++;
				}
			}

			if (form.getCertificate() != null
					&& form.getCertificate().length != 0) {
				int index = 0;
				// int ecount = 0;
				for (String certificateId : form.getCertificate()) {
					if (certificateId != null && !certificateId.isEmpty()) {
						Tier2ReportIndirectExpenses indirectExpenses = new Tier2ReportIndirectExpenses();
						indirectExpenses.setIndirectExpenseAmt((Double
								.valueOf(form.getIndirectAmt()[index])));
						Certificate certificate = (Certificate) session.get(
								Certificate.class,
								Integer.valueOf(certificateId));

						CustomerCommodityCategory marketSubSector = null;
						if (form.getMarketSector() != null
								&& !"".equalsIgnoreCase(form.getMarketSector()[index])) {
							marketSubSector = (CustomerCommodityCategory) session.get(
									CustomerCommodityCategory.class, Integer.valueOf(form
											.getMarketSector()[index]));

							indirectExpenses.setMarketSubSector(marketSubSector);
						}
						indirectExpenses.setMarketSubSector(marketSubSector);

						Ethnicity ethnicity = null;
						if (form.getEthnicity() != null
								&& !"".equalsIgnoreCase(form.getEthnicity()[index])) {
							ethnicity = (Ethnicity) session
									.get(Ethnicity.class, Integer.valueOf(form
											.getEthnicity()[index]));

							indirectExpenses.setEthnicityId(ethnicity);
						}
						if (form.getTier2indirectvendorName() != null
								&& form.getTier2indirectvendorName().length > index) {

							VendorMaster tier2IndirectVendorid = (VendorMaster) session
									.get(VendorMaster.class,
											Integer.valueOf(form
													.getTier2indirectvendorName()[index]));
							indirectExpenses
									.setTier2IndirectVendorId(tier2IndirectVendorid);

						}

						indirectExpenses.setCertificateId(certificate);
						indirectExpenses.setTier2ReoprtMasterid(master);
						indirectExpenses.setCreatedBy(vendor.getId());
						indirectExpenses.setCreatedOn(new Date());
						session.persist(indirectExpenses);
						index++;
					}
				}
			}
			session.getTransaction().commit();
			// return true;
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			ex.printStackTrace();
			// return false;
		} catch (ParseException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		// return null;
		return master;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportMaster> viewTier2ReportByVendor(
			UserDetailsDto userDetails, Integer vendorId) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<Tier2ReportMaster> tier2Reports = null;
		try {
			session.getTransaction().begin();
			tier2Reports = session.createQuery(
					"From Tier2ReportMaster where vendorId=" + vendorId).list();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();

		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return tier2Reports;
	}

	/**
	 * This method helps in convert string array to string.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToString(String[] stringarray) {
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) {
			for (String st : stringarray) {
				sb.append(st.trim()).append(',');
			}
			if (stringarray.length != 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * This method helps in convert string array to string with single quote.
	 * 
	 * @param value
	 * @return
	 */
	public String arrayToStringWithSingleQuote(String[] stringarray) {
		StringBuilder sb = new StringBuilder();
		if (stringarray != null && stringarray.length != 0) {
			for (String st : stringarray) {
				sb.append("'" + st.trim() + "'").append(',');
			}
			if (stringarray.length != 0) {
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getTotalSalesReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();

			if (period != null && !period.equalsIgnoreCase("0")) {
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("SELECT customer_vendormaster.VENDORNAME,('"
					+ reportYear
					+ "')ryear,customer_tier2reportmaster.ReportingPeriod,"
					+ " (select IFNULL(sum(customer_tier2reportdirectexpenses.DirExpenseAmt),0)"
					+ " from customer_tier2reportdirectexpenses"
					+ " where customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)directspend,"
					+ " (select IFNULL(sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt),0) from customer_tier2reportindirectexpenses"
					+ " where customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)indirectspend,"
					+ " ('0')total,"
					+ " (DATE_FORMAT(customer_tier2reportmaster.createdon, '%m-%d-%Y')) createdon"
					+ " FROM customer_vendormaster,customer_tier2reportmaster"
					+ " WHERE (customer_tier2reportmaster.vendorId = customer_vendormaster.ID)"
					+ " And customer_tier2reportmaster.IsSubmitted = 1 and customer_vendormaster.isactive=1 ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and ( customer_vendormaster.id in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ) ");
			}

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append(" and (customer_tier2reportmaster.ReportingStartDate>='"
						+ startDate
						+ " 00:00:00') and (customer_tier2reportmaster.ReportingEndDate<='"
						+ endDate + " 23:59:59') ");
			} else {
				sqlQuery.append(" And (year(customer_tier2reportmaster.reportingstartdate) = "
						+ reportYear
						+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
						+ reportYear + ") ");
			}

			sqlQuery.append("and (customer_vendormaster.vendorstatus = 'A' OR customer_vendormaster.vendorstatus = 'B')");

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" order by customer_vendormaster.VENDORNAME,customer_tier2reportmaster.createdon");

			System.out
					.println("1.Report in Total Spend (ReportDaoImpl @ 1186):"
							+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setVendorName(objects[0].toString());
					reportDto.setSpendYear(Integer.parseInt(objects[1]
							.toString()));
					reportDto.setReportPeriod(objects[2].toString());
					reportDto.setDirectExp(Double.parseDouble(objects[3]
							.toString()));
					if (objects[4].toString() != null
							&& !objects[4].toString().isEmpty()) {
						reportDto.setIndirectExp(Double.parseDouble(objects[4]
								.toString()));
					} else {
						reportDto.setIndirectExp((double) 0);
					}

					reportDto.setTotalSales(Double.parseDouble(objects[5]
							.toString()));
					reportDto.setCreatedOn(objects[6].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> directSpendReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();

			if (period != null && !"0".equalsIgnoreCase(period)) {
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,('"
					+ reportYear
					+ "') ryear, customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME,"
					+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,"
					+ " (select sum(ds.DirExpenseAmt) from customer_tier2reportdirectexpenses ds, "
					+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and"
					+ " t2m.vendorid=pvm.id ");

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append("and t2m.ReportingStartDate >= '" + startDate
						+ " 00:00:00' AND" + " t2m.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ");
			} else {
				sqlQuery.append(" And (year(t2m.reportingstartdate) = "
						+ reportYear + " OR  year(t2m.reportingenddate) = "
						+ reportYear + ") ");
			}

			sqlQuery.append("And t2m.IsSubmitted = 1 ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(" ) totalexpenses,"
					+ " ((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)"
					+ " from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id ");

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append("and t2m.ReportingStartDate >= '" + startDate
						+ " 00:00:00' " + " AND t2m.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ");
			} else {
				sqlQuery.append(" And (year(t2m.reportingstartdate) = "
						+ reportYear + " OR  year(t2m.reportingenddate) = "
						+ reportYear + ") ");
			}

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(" And t2m.IsSubmitted = 1 )*100) PercentDiversityspend, DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') SubmittedDate"
					+ " FROM customer_vendormaster pvm,customer_tier2reportmaster,customer_tier2reportdirectexpenses,"
					+ " customer_vendormaster t2v,customer_certificatemaster "
					+ " WHERE ( customer_tier2reportmaster.vendorId = pvm.ID )"
					+ " and ( customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid )"
					+ " and ( t2v.ID = customer_tier2reportdirectexpenses.Tier2_Vendorid ) and"
					+ " ( customer_certificatemaster.ID = customer_tier2reportdirectexpenses.CertificateId ) and"
					+ " ( customer_certificatemaster.DIVERSE_QUALITY = 1 ) and customer_tier2reportmaster.IsSubmitted=1");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and ( pvm.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ) ");
			}

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append(" AND (( customer_tier2reportmaster.ReportingStartDate >= '"
						+ startDate
						+ " 00:00:00' ) AND"
						+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ) ");
			} else {
				sqlQuery.append(" And ((year(customer_tier2reportmaster.reportingstartdate) = "
						+ reportYear
						+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
						+ reportYear + ") ");
			}

			sqlQuery.append(" And customer_tier2reportmaster.IsSubmitted = 1 and pvm.isactive=1"
					+ " and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B'))");

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("and pvm.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" group by  PrimeVendor ,t2vendor ,customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn ,customer_certificatemaster.CERTIFICATENAME"
					+ " order by PrimeVendor ,t2vendor ,customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn ,customer_certificatemaster.CERTIFICATENAME");

			System.out
					.println("2.Report in Direct Spend (ReportDaoImpl @ 1367):"
							+ sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();

				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setDirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(7)));
				reportDto.setDiversityPercent(rs.getString(8));
				reportDto.setCreatedOn(rs.getString(9));
				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			ex.printStackTrace();
			session.getTransaction().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> indirectSpendReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();

			if (period != null && !period.equalsIgnoreCase("0")) {
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor, t2v.VENDORNAME t2vendor, ('"
					+ reportYear
					+ "') ryear ,  customer_tier2reportmaster.ReportingPeriod,"
					+ " customer_certificatemaster.CERTIFICATENAME, sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)Indirectspendamt,"
					+ " (select sum(ds.IndirectExpenseAmt) from customer_tier2reportindirectexpenses ds,"
					+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and"
					+ " t2m.vendorid=pvm.id ");

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append("and t2m.ReportingStartDate >= '" + startDate
						+ " 00:00:00' " + " AND t2m.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ");
			} else {
				sqlQuery.append(" And (year(t2m.reportingstartdate) = "
						+ reportYear + " OR  year(t2m.reportingenddate) = "
						+ reportYear + ") ");
			}

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses, ((sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt))/(select sum(ds.IndirectExpenseAmt)"
					+ " from customer_tier2reportindirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id ");

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append("and t2m.ReportingStartDate >= '" + startDate
						+ " 00:00:00' " + " AND t2m.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ");
			} else {
				sqlQuery.append(" And (year(t2m.reportingstartdate) = "
						+ reportYear + " OR  year(t2m.reportingenddate) = "
						+ reportYear + ") ");
			}

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(" And t2m.IsSubmitted = 1)*100) PercentDiversityspend, DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y')"
					+ " SubmittedDate FROM customer_vendormaster  pvm "
					+ " INNER JOIN customer_tier2reportmaster ON customer_tier2reportmaster.vendorId = pvm.ID "
					+ " INNER JOIN customer_tier2reportindirectexpenses ON customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid "
					+ " INNER JOIN customer_certificatemaster ON customer_certificatemaster.ID = customer_tier2reportindirectexpenses.CertificateId "
					+ " LEFT OUTER JOIN customer_vendormaster t2v ON t2v.ID = customer_tier2reportindirectexpenses.Tier2_Vendorid"
					+ " WHERE (customer_certificatemaster.DIVERSE_QUALITY = 1) ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append(" and ( pvm.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ) ");
			}

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append(" AND (( customer_tier2reportmaster.ReportingStartDate >= '"
						+ startDate
						+ " 00:00:00' ) AND"
						+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ) ");
			} else {
				sqlQuery.append(" And ((year(customer_tier2reportmaster.reportingstartdate) = "
						+ reportYear
						+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
						+ reportYear + ") ");
			}

			sqlQuery.append(" AND customer_tier2reportmaster.IsSubmitted = 1 "
					+ " AND (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B'))");

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {

				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and pvm.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" GROUP BY pvm.VENDORNAME, t2vendor, customer_certificatemaster.CERTIFICATENAME,"
					+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn "
					+ " ORDER BY pvm.VENDORNAME, t2vendor, customer_certificatemaster.CERTIFICATENAME,"
					+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn ");

			System.out
					.println("3.Report in InDirect Spend (ReportDaoImpl @ 1538):"
							+ sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(7)));
				reportDto.setDiversityPercent(rs.getString(8));
				reportDto.setCreatedOn(rs.getString(9));
				reportDtos.add(reportDto);
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getDiversityReport(Tier2ReportForm reportForm, UserDetailsDto appDetails, String type)
	{
		java.sql.Connection connection = DataBaseConnection.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		
		try
		{
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();
			Integer reportType = reportForm.getReportType();

			if (period != null && !period.equalsIgnoreCase("0"))
			{
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			if (reportType != null && reportType == 0) // Direct Expenses
			{
				sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,('"
						+ reportYear
						+ "') ryear,"
						+ " customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME,"
						+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,(0)indirectspendamt ,"
						+ " (select sum(ds.DirExpenseAmt)from customer_tier2reportdirectexpenses ds,"
						+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and"
						+ " t2m.vendorid=pvm.id ");

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("and t2m.ReportingStartDate >= '"
							+ startDate + " 00:00:00' "
							+ " AND t2m.ReportingEndDate <= '" + endDate
							+ " 23:59:59' ");
				}
				else
				{
					sqlQuery.append(" And (year(t2m.reportingstartdate) = "
							+ reportYear + " OR  year(t2m.reportingenddate) = "
							+ reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND ds.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append(" and t2m.vendorId in (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses, ((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)"
						+ " from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
						+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id ");

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("and t2m.ReportingStartDate >= '"
							+ startDate + " 00:00:00' "
							+ " AND t2m.ReportingEndDate <= '" + endDate
							+ " 23:59:59' ");
				}
				else
				{
					sqlQuery.append(" And (year(t2m.reportingstartdate) = "
							+ reportYear + " OR  year(t2m.reportingenddate) = "
							+ reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND ds.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append(" and t2m.vendorId in (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 )*100) PercentDiversity,"
						+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') SubmittedDate");
				if(type.equals("ethnicity")){
					sqlQuery.append(",e.Ethnicity");
				}
				sqlQuery.append(" FROM");
				if(type.equals("ethnicity")){
					sqlQuery.append(" ethnicity e,");
				}
				sqlQuery.append(" customer_vendormaster  pvm, customer_tier2reportmaster, customer_tier2reportdirectexpenses,customer_vendormaster  t2v,"
						+ " customer_certificatemaster WHERE ( customer_tier2reportmaster.vendorId = pvm.ID ) and"
						+ " ( customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ) and"
						+ " ( t2v.ID = customer_tier2reportdirectexpenses.Tier2_Vendorid ) and"
						+ " ( customer_certificatemaster.ID = customer_tier2reportdirectexpenses.CertificateId ) ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append(" and ( ( pvm.ID in (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ) ) ");
				}

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append(" AND ( customer_tier2reportmaster.ReportingStartDate >= '"
							+ startDate
							+ " 00:00:00' ) AND"
							+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
							+ endDate + " 23:59:59' ) ");
				}
				else
				{
					sqlQuery.append(" And (year(customer_tier2reportmaster.reportingstartdate) = "
							+ reportYear
							+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
							+ reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}

				sqlQuery.append(" And (customer_tier2reportmaster.IsSubmitted = 1 )"
						+ " and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B')");

				if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
				{
					if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
					{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
						{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("and pvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
						if(type.endsWith("ethnicity")){
							sqlQuery.append("and (e.id =customer_tier2reportdirectexpenses.ethnicityId)");
						}
					}
				}

				sqlQuery.append(" group by");
				if(type.equals("ethnicity")){
					sqlQuery.append(" e.Ethnicity,");
				}
				sqlQuery.append(" customer_certificatemaster.CERTIFICATENAME, pvm.VENDORNAME , t2v.VENDORNAME ,"
						+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn"
						+ " order by");
				if(type.equals("ethnicity")){
					sqlQuery.append(" e.Ethnicity,");
				}
						sqlQuery.append(" customer_certificatemaster.CERTIFICATENAME, pvm.VENDORNAME, t2v.VENDORNAME ,"
						+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn ");
				
				System.out.println("4.Report in Diversity (Direct) ReportDaoImpl @ 1736:" + sqlQuery.toString());

			}
			else if(reportType != null && reportType == 1) // Indirect Expenses
			{
				sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor, t2v.VENDORNAME t2vendor,('"
						+ reportYear
						+ "') ryear , customer_tier2reportmaster.ReportingPeriod,"
						+ " customer_certificatemaster.CERTIFICATENAME,(0) directspendamt, sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)Indirectspendamt,"
						+ " (select sum(ds.IndirectExpenseAmt)from customer_tier2reportindirectexpenses ds,  customer_tier2reportmaster t2m"
						+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.vendorid=pvm.id ");

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("and t2m.ReportingStartDate >= '"
							+ startDate + " 00:00:00' "
							+ " AND t2m.ReportingEndDate <= '" + endDate
							+ " 23:59:59' ");
				}
				else
				{
					sqlQuery.append(" And (year(t2m.reportingstartdate) = "
							+ reportYear + " OR  year(t2m.reportingenddate) = "
							+ reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND ds.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append(" and t2m.vendorId in (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses,"
						+ " ((sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt))/(select sum(ds.IndirectExpenseAmt)"
						+ " from customer_tier2reportindirectexpenses ds,customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid"
						+ " and t2m.vendorid=pvm.id ");

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("and t2m.ReportingStartDate >= '"
							+ startDate + " 00:00:00' "
							+ " AND t2m.ReportingEndDate <= '" + endDate
							+ " 23:59:59' ");
				}
				else
				{
					sqlQuery.append(" And (year(t2m.reportingstartdate) = "
							+ reportYear + " OR  year(t2m.reportingenddate) = "
							+ reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND ds.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}

				if(!arrayToString(reportForm.getTier2VendorNames()).contains(str)
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append(" and t2m.vendorId in (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 )*100) PercentDiversity, DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y')"
						+ " SubmittedDate,ethnicity.Ethnicity FROM customer_vendormaster  pvm "
						+ " inner join customer_tier2reportmaster on customer_tier2reportmaster.vendorId = pvm.ID "
						+ " inner join customer_tier2reportindirectexpenses on customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid "
						+ " inner join customer_certificatemaster on customer_certificatemaster.ID = customer_tier2reportindirectexpenses.CertificateId "
						+ " inner join ethnicity on ethnicity.id = customer_tier2reportindirectexpenses.ethnicityId"
						+ " left outer join customer_vendormaster t2v on t2v.ID = customer_tier2reportindirectexpenses.Tier2_Vendorid"
						+ " WHERE (customer_tier2reportmaster.IsSubmitted = 1 )  and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B') ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append(" and ( ( pvm.ID in (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ) ) ");
				}

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append(" AND ( customer_tier2reportmaster.ReportingStartDate >= '"
							+ startDate
							+ " 00:00:00' ) AND"
							+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
							+ endDate + " 23:59:59' ) ");
				}
				else
				{
					sqlQuery.append(" And (year(customer_tier2reportmaster.reportingstartdate) = "
							+ reportYear
							+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
							+ reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportindirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}

				if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
				{
					if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
					{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
						{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("and pvm.CUSTOMERDIVISIONID in("+ divisionIds.toString()+") ");
					}
				}	
					
				sqlQuery.append(" group by ethnicity.Ethnicity, customer_certificatemaster.CERTIFICATENAME, pvm.VENDORNAME, t2v.VENDORNAME, "
						+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn "
						+ " order by ethnicity.Ethnicity, customer_certificatemaster.CERTIFICATENAME, pvm.VENDORNAME, t2v.VENDORNAME, "
						+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn ");
				
				System.out.println("4.Report in Diversity (Indirect) ReportDaoImpl @ 1861:"+sqlQuery.toString());
			}
			else // Both
			{		
				sqlQuery.append("SELECT vm.VENDORNAME, (SELECT cvm.VENDORNAME FROM customer_vendormaster cvm WHERE cvm.ID = de.Tier2_Vendorid LIMIT 1)Tier2VendorName, "
						+ "('" + reportYear + "') ryear, t2m.ReportingPeriod, cc.CERTIFICATENAME, de.DirExpenseAmt, (0) IndirectExpenseAmt, "
						+ "(IFNULL((SELECT SUM(IFNULL(customer_tier2reportdirectexpenses.DirExpenseAmt,0)) FROM customer_tier2reportdirectexpenses, "
						+ "customer_tier2reportmaster WHERE customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid=t2m.vendorId),0) + "
						+ "IFNULL((SELECT SUM(IFNULL(customer_tier2reportindirectexpenses.IndirectExpenseAmt,0)) "
						+ "FROM customer_tier2reportindirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportindirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1  AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0))totalexpenses, "
						+ "ROUND((((de.DirExpenseAmt)/(IFNULL((SELECT SUM(IFNULL(customer_tier2reportdirectexpenses.DirExpenseAmt,0)) "
						+ "FROM customer_tier2reportdirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0) + "
						+ "IFNULL((SELECT SUM(IFNULL(customer_tier2reportindirectexpenses.IndirectExpenseAmt,0)) "
						+ "FROM customer_tier2reportindirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportindirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0)))*100),2)Percent, "
						+ "DATE_FORMAT(t2m.CreatedOn, '%m-%d-%Y') SubmittedDate ");
				if(type.equals("ethnicity")){
					sqlQuery.append(" ,e.Ethnicity ");
				}
				sqlQuery.append("FROM");
				if(type.equals("ethnicity")){
					sqlQuery.append(" ethnicity e,");
				}
				sqlQuery.append(" customer_tier2reportmaster t2m, customer_vendormaster vm, customer_tier2reportdirectexpenses de, customer_certificatemaster cc "
						+ "WHERE t2m.vendorId = vm.ID ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND t2m.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND t2m.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(t2m.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(t2m.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND de.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND de.Tier2ReoprtMasterid = t2m.Tier2ReportMasterid AND cc.ID = de.CertificateId "
						+ "AND t2m.IsSubmitted = 1 AND (vm.VENDORSTATUS = 'A' OR vm.VENDORSTATUS = 'B') ");
				
				if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
				{
					if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
					{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
						{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("AND vm.CUSTOMERDIVISIONID IN (" + divisionIds.toString() + ") ");
						if(type.equals("ethnicity")){
							sqlQuery.append("and e.id = de.ethnicityId ");
						}
					}
				}
				
				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str) 
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append("AND vm.ID IN (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ");
				}
				
				sqlQuery.append(" UNION ALL ");
				sqlQuery.append("SELECT vm.VENDORNAME, (SELECT cvm.VENDORNAME FROM customer_vendormaster cvm WHERE cvm.ID = ie.Tier2_Vendorid LIMIT 1)Tier2VendorName, "
						+ "('" + reportYear + "') ryear, t2m.ReportingPeriod, cc.CERTIFICATENAME, (0) DirExpenseAmt, ie.IndirectExpenseAmt, "
						+ "(IFNULL((SELECT SUM(IFNULL(customer_tier2reportdirectexpenses.DirExpenseAmt,0)) "
						+ "FROM customer_tier2reportdirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0) + "
						+ "IFNULL((SELECT SUM(IFNULL(customer_tier2reportindirectexpenses.IndirectExpenseAmt,0)) "
						+ "FROM customer_tier2reportindirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportindirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0))totalexpenses, "
						+ "ROUND((((ie.IndirectExpenseAmt)/(IFNULL((SELECT SUM(IFNULL(customer_tier2reportdirectexpenses.DirExpenseAmt,0)) "
						+ "FROM customer_tier2reportdirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");

				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0) + "
						+ "IFNULL((SELECT SUM(IFNULL(customer_tier2reportindirectexpenses.IndirectExpenseAmt,0)) "
						+ "FROM customer_tier2reportindirectexpenses, customer_tier2reportmaster "
						+ "WHERE customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ");
				
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND customer_tier2reportmaster.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND customer_tier2reportmaster.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(customer_tier2reportmaster.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(customer_tier2reportmaster.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND customer_tier2reportindirectexpenses.CertificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND customer_tier2reportmaster.IsSubmitted = 1 AND customer_tier2reportmaster.Vendorid = t2m.vendorId),0)))*100),2)Percent, "
						+ "DATE_FORMAT(t2m.CreatedOn, '%m-%d-%Y') SubmittedDate ");
				if(type.equals("ethnicity")){
					sqlQuery.append(" ,ethnicity e ");
				}
				sqlQuery.append("FROM");
				if(type.equals("ethnicity")){
					sqlQuery.append(" Ethnicity e,");
				}
				sqlQuery.append(" customer_tier2reportmaster t2m, customer_vendormaster vm, customer_tier2reportindirectexpenses ie, customer_certificatemaster cc "
						+ "WHERE t2m.vendorId = vm.ID ");
				if(type.equals("ethnicity")){
					sqlQuery.append(" and e.id = ie.ethnicityId ");
				}
				if (null != startDate && !startDate.isEmpty() && null != endDate && !endDate.isEmpty())
				{
					sqlQuery.append("AND t2m.reportingstartdate >= '" + startDate + " 00:00:00' "
							+ "AND t2m.reportingenddate <= '" + endDate + " 23:59:59' ");
				}
				else
				{
					sqlQuery.append("AND (YEAR(t2m.reportingstartdate) = " + reportYear + " "
							+ "OR YEAR(t2m.reportingenddate) = " + reportYear + ") ");
				}
				
				if(!arrayToStringWithSingleQuote(reportForm.getCertificate()).equalsIgnoreCase("") && reportForm.getCertificate().length != 0)
				{
					sqlQuery.append("AND ie.certificateId IN (" + arrayToStringWithSingleQuote(reportForm.getCertificate()) + ") ");
				}
				
				sqlQuery.append("AND ie.Tier2ReoprtMasterid = t2m.Tier2ReportMasterid AND cc.ID = ie.CertificateId "
						+ "AND t2m.IsSubmitted = 1 AND (vm.VENDORSTATUS = 'A' OR vm.VENDORSTATUS = 'B') ");
				
				if (appDetails.getSettings().getIsDivision() != null && appDetails.getSettings().getIsDivision() != 0) 
				{
					if(appDetails.getCustomerDivisionIds() != null && appDetails.getCustomerDivisionIds().length != 0 && appDetails.getIsGlobalDivision() == 0)
					{
						StringBuilder divisionIds = new StringBuilder();
						for(int i=0; i<appDetails.getCustomerDivisionIds().length; i++)
						{
							divisionIds.append(appDetails.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("AND vm.CUSTOMERDIVISIONID IN (" + divisionIds.toString() + ") ");
					}
				}
				
				if (!arrayToString(reportForm.getTier2VendorNames()).contains(str) 
						&& !arrayToString(reportForm.getTier2VendorNames()).equals("")
						&& reportForm.getTier2VendorNames().length != 0)
				{
					sqlQuery.append("AND vm.ID IN (" + arrayToString(reportForm.getTier2VendorNames()) + " ) ");
				}
				
				sqlQuery.append("ORDER BY 1, 2, 10 ");
				
				System.out.println("4.Report in Diversity (Both) ReportDaoImpl @ 2129:" + sqlQuery.toString());
			}
			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setDirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(7)));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(8)));
				reportDto.setDiversityPercent(rs.getString(9));
				reportDto.setCreatedOn(rs.getString(10));
				if(type.equals("ethnicity")) {
					if(rs.getString(11) != null){
					reportDto.setEthnicity(rs.getString(11));
					}
				}
				reportDtos.add(reportDto);
			}
		} catch (HibernateException ex) {
			// print the exception in log file. 
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			System.out.println(e);
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}


	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getDiversityReportBySectorAndEthnicity(Tier2ReportForm reportForm,
			UserDetailsDto appDetails, String type) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			List<String> startDates = new LinkedList<String>();
			List<String> endDates = new LinkedList<String>();
			;
			String[] periods = reportForm.getReportPeriod();
			Integer[] reportYear = reportForm.getReportSpendYears();
			Integer reportType = reportForm.getReportType();

			if (periods != null) {
				for (String period : periods) {
					if (period != null && !period.equalsIgnoreCase("0")) {
						String peString[] = period.split("-");
						startDates.add(getReportingPeriod(peString[0]));
						endDates.add(getReportingPeriod(peString[1]));
					}
				}
			}

			if (reportType != null && reportType == 0) // Direct Expenses
			{
				sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,year(customer_tier2reportmaster.ReportingEndDate) ryear,"
						+ " customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME,"
						+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,(0)indirectspendamt,"
				+" (case when customer_tier2reportmaster.CreatedOn < '2017-08-16' then  customer_marketsector.SECTORDESCRIPTION "
				+" when customer_tier2reportmaster.CreatedOn >= '2017-08-16' then  customer_commoditycategory.CATEGORYDESCRIPTION end )SECTORDESCRIPTION, "
				+ " ethnicity.Ethnicity, (select sum(ifnull(ds.DirExpenseAmt,0)) from customer_tier2reportdirectexpenses ds,"
						+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}


					sqlQuery.append("AND ds.CertificateId=customer_tier2reportdirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses, round(((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)"
						+ " from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
						+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("AND ds.CertificateId=customer_tier2reportdirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 )*100),3) PercentDiversity,"
						+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') SubmittedDate");
				sqlQuery.append(" FROM");
				sqlQuery.append(" customer_vendormaster  pvm inner join  customer_tier2reportmaster on customer_tier2reportmaster.vendorId = pvm.ID "
						+ "AND (customer_tier2reportmaster.IsSubmitted = 1) ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(customer_tier2reportmaster.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  customer_tier2reportmaster.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(customer_tier2reportmaster.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(customer_tier2reportmaster.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("inner join  customer_tier2reportdirectexpenses on customer_tier2reportmaster.Tier2ReportMasterid="
						+ "customer_tier2reportdirectexpenses.Tier2ReoprtMasterid inner join customer_vendormaster  t2v"
						+ " on customer_tier2reportdirectexpenses.Tier2_Vendorid=t2v.ID inner join customer_certificatemaster on"
						+ " customer_tier2reportdirectexpenses.CertificateId=customer_certificatemaster.ID ");

				if (!arrayToStringWithSingleQuote(reportForm.getCertificate())
						.equalsIgnoreCase("")
						&& reportForm.getCertificate().length != 0) {
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN ("
							+ arrayToStringWithSingleQuote(reportForm
									.getCertificate()) + ") ");
				}

				sqlQuery.append("left outer join customer_marketsector on customer_marketsector.ID=customer_tier2reportdirectexpenses.marketSectorId "
						+" LEFT OUTER JOIN customer_commoditycategory ON customer_commoditycategory.ID=customer_tier2reportdirectexpenses.marketSubSectorId "
						+ "left outer join ethnicity on ethnicity.id=customer_tier2reportdirectexpenses.ethnicityId");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" where (pvm.ID in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " )) ");
				}

				sqlQuery.append(" and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B')and pvm.CUSTOMERDIVISIONID in(2,1) "
						+ " group by customer_certificatemaster.CERTIFICATENAME,"
						+ " pvm.VENDORNAME, t2v.VENDORNAME,customer_tier2reportmaster.ReportingPeriod,customer_tier2reportmaster.CreatedOn,"
						+ " (case when customer_tier2reportmaster.CreatedOn < '2017-08-16' then  customer_marketsector.SECTORDESCRIPTION "
						+ " when customer_tier2reportmaster.CreatedOn >= '2017-08-16' then  customer_commoditycategory.CATEGORYDESCRIPTION end ), "
						+ " ethnicity.Ethnicity order by customer_certificatemaster.CERTIFICATENAME,pvm.VENDORNAME,year(customer_tier2reportmaster.ReportingEndDate),"
						+ " t2v.VENDORNAME,customer_tier2reportmaster.ReportingPeriod,customer_tier2reportmaster.CreatedOn");

				System.out
						.println("4.Report in Diversity (Direct) ReportDaoImpl @ 1736:"
								+ sqlQuery.toString());

			} else if (reportType != null && reportType == 1) // Indirect
																// Expenses
			{
				sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor, t2v.VENDORNAME t2vendor,year(customer_tier2reportmaster.ReportingEndDate) ryear,"
						+ " customer_tier2reportmaster.ReportingPeriod,"
						+ " customer_certificatemaster.CERTIFICATENAME,(0) directspendamt, sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)Indirectspendamt,"
						+ " (case "
						+ "   when customer_tier2reportmaster.CreatedOn < '2017-08-16' "
						+ "	then  customer_marketsector.SECTORDESCRIPTION "
						+ "		when customer_tier2reportmaster.CreatedOn >= '2017-08-16' "
						+ "	   then  customer_commoditycategory.CATEGORYDESCRIPTION  "
						+ "	 end )SECTORDESCRIPTION, "
						+ " ethnicity.Ethnicity,(select sum(ifnull(ds.Indirectexpenseamt,0)) from customer_tier2reportindirectexpenses ds,  customer_tier2reportmaster t2m"
						+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("AND ds.CertificateId=customer_tier2reportindirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses,"
						+ " round(((sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt))/(select sum(ds.IndirectExpenseAmt)"
						+ " from customer_tier2reportindirectexpenses ds,customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("AND ds.CertificateId=customer_tier2reportindirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 )*100),3) PercentDiversity, DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y')"
						+ " SubmittedDate FROM customer_vendormaster  pvm "
						+ " inner join customer_tier2reportmaster on customer_tier2reportmaster.vendorId = pvm.ID AND (customer_tier2reportmaster.IsSubmitted = 1) ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(customer_tier2reportmaster.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  customer_tier2reportmaster.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(customer_tier2reportmaster.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(customer_tier2reportmaster.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append(" inner join customer_tier2reportindirectexpenses on customer_tier2reportmaster.Tier2ReportMasterid = customer_tier2reportindirectexpenses.Tier2ReoprtMasterid "
						+ " inner join customer_vendormaster  t2v on customer_tier2reportindirectexpenses.Tier2_Vendorid=t2v.ID inner join customer_certificatemaster on "
						+ " customer_tier2reportindirectexpenses.CertificateId=customer_certificatemaster.ID");

				if (!arrayToStringWithSingleQuote(reportForm.getCertificate())
						.equalsIgnoreCase("")
						&& reportForm.getCertificate().length != 0) {
					sqlQuery.append(" AND customer_tier2reportindirectexpenses.CertificateId IN ("
							+ arrayToStringWithSingleQuote(reportForm
									.getCertificate()) + ") ");
				}
				
				
				sqlQuery.append(" left outer join customer_marketsector on customer_marketsector.ID=customer_tier2reportindirectexpenses.marketSectorId "
						+" LEFT OUTER JOIN customer_commoditycategory ON customer_commoditycategory.ID=customer_tier2reportindirectexpenses.marketSubSectorId "
						+ "left outer join ethnicity on ethnicity.id=customer_tier2reportindirectexpenses.ethnicityId");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" WHERE ( pvm.ID in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ) ");
				}
				
				sqlQuery.append(" and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B')and pvm.CUSTOMERDIVISIONID in(2,1) "
						+ "group by customer_certificatemaster.CERTIFICATENAME,"
						+ "pvm.VENDORNAME, t2v.VENDORNAME,customer_tier2reportmaster.ReportingPeriod,"
						+ "customer_tier2reportmaster.CreatedOn, "
						+ " (case "
						+ " when customer_tier2reportmaster.CreatedOn < '2017-08-16' "
						+ " then  customer_marketsector.SECTORDESCRIPTION "
						+ " when customer_tier2reportmaster.CreatedOn >= '2017-08-16' "
						+ " then  customer_commoditycategory.CATEGORYDESCRIPTION "
						+ " end ), "
						+ " ethnicity.Ethnicity order by customer_certificatemaster.CERTIFICATENAME, "
						+ " pvm.VENDORNAME,year(customer_tier2reportmaster.ReportingEndDate), t2v.VENDORNAME ,"
						+ " customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn");

				System.out
						.println("4.Report in Diversity (Indirect) ReportDaoImpl @ 1861:"
								+ sqlQuery.toString());
			} else // Both
			{
				//In Both Direct
				sqlQuery.append("select primevendor,t2vendor,ryear,reportingperiod,certificatename,sum(directspendamt)directspendamt,"
						+ "sum(indirectspendamt)indirectspendamt,SECTORDESCRIPTION,ethnicity,sum(totalexpenses)totalexpenses,"
						+ "round(((sum(directspendamt)+sum(indirectspendamt))/sum(totalexpenses))*100,3)PercentDiversity,submitteddate from ("
						+ "SELECT (pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,year(customer_tier2reportmaster.ReportingEndDate) ryear,"
						+ " customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME,"
						+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,(0)indirectspendamt, "
						+ " (case "
						+ "  when customer_tier2reportmaster.CreatedOn < '2017-08-16' "
						+ "	then  customer_marketsector.SECTORDESCRIPTION "
						+ "	when customer_tier2reportmaster.CreatedOn >= '2017-08-16' "
						+ "	then  customer_commoditycategory.CATEGORYDESCRIPTION "
						+ "	end )SECTORDESCRIPTION, "
						+ " ethnicity.Ethnicity, (select sum(ifnull(ds.DirExpenseAmt,0)) from customer_tier2reportdirectexpenses ds,"
						+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}


					sqlQuery.append("AND ds.CertificateId=customer_tier2reportdirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses, round(((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)"
						+ " from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
						+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("AND ds.CertificateId=customer_tier2reportdirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 )*100),3) PercentDiversity,"
						+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') SubmittedDate");
				sqlQuery.append(" FROM");
				sqlQuery.append(" customer_vendormaster  pvm inner join  customer_tier2reportmaster on customer_tier2reportmaster.vendorId = pvm.ID "
						+ "AND (customer_tier2reportmaster.IsSubmitted = 1) ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(customer_tier2reportmaster.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  customer_tier2reportmaster.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(customer_tier2reportmaster.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(customer_tier2reportmaster.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("inner join  customer_tier2reportdirectexpenses on customer_tier2reportmaster.Tier2ReportMasterid="
						+ "customer_tier2reportdirectexpenses.Tier2ReoprtMasterid inner join customer_vendormaster  t2v"
						+ " on customer_tier2reportdirectexpenses.Tier2_Vendorid=t2v.ID inner join customer_certificatemaster on"
						+ " customer_tier2reportdirectexpenses.CertificateId=customer_certificatemaster.ID ");

				if (!arrayToStringWithSingleQuote(reportForm.getCertificate())
						.equalsIgnoreCase("")
						&& reportForm.getCertificate().length != 0) {
					sqlQuery.append("AND customer_tier2reportdirectexpenses.CertificateId IN ("
							+ arrayToStringWithSingleQuote(reportForm
									.getCertificate()) + ") ");
				}

				sqlQuery.append("left outer join customer_marketsector on customer_marketsector.ID=customer_tier2reportdirectexpenses.marketSectorId "
						+ " LEFT OUTER JOIN customer_commoditycategory ON customer_commoditycategory.ID=customer_tier2reportdirectexpenses.marketSubSectorId "
						+ "left outer join ethnicity on ethnicity.id=customer_tier2reportdirectexpenses.ethnicityId");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" where (pvm.ID in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " )) ");
				}

				sqlQuery.append(" and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B')and pvm.CUSTOMERDIVISIONID in(2,1) "
						+ "group by customer_certificatemaster.CERTIFICATENAME,"
						+ "pvm.VENDORNAME, t2v.VENDORNAME,customer_tier2reportmaster.ReportingPeriod,customer_tier2reportmaster.CreatedOn,"
						+ " (case "
						+ " when customer_tier2reportmaster.CreatedOn < '2017-08-16' "
						+ " then  customer_marketsector.SECTORDESCRIPTION "
						+ " when customer_tier2reportmaster.CreatedOn >= '2017-08-16' "
						+ " then  customer_commoditycategory.CATEGORYDESCRIPTION "
						+ " end ), "
						+ " ethnicity.Ethnicity,customer_tier2reportmaster.CreatedOn");
						

				System.out
						.println("4.Report in Diversity (Direct) ReportDaoImpl @ 1736:"
								+ sqlQuery.toString());
				
				sqlQuery.append(" union all ");
				
				//In Both Indirect
				sqlQuery.append("SELECT (pvm.VENDORNAME) PrimeVendor, t2v.VENDORNAME t2vendor,year(customer_tier2reportmaster.ReportingEndDate) ryear,"
						+ " customer_tier2reportmaster.ReportingPeriod,"
						+ " customer_certificatemaster.CERTIFICATENAME,(0) directspendamt, sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)Indirectspendamt,"
						+ " (case "
						+ " when customer_tier2reportmaster.CreatedOn < '2017-08-16' "
						+ " then  customer_marketsector.SECTORDESCRIPTION "
						+ " when customer_tier2reportmaster.CreatedOn >= '2017-08-16' "
						+ " then  customer_commoditycategory.CATEGORYDESCRIPTION "  
						+ " end )SECTORDESCRIPTION, "
						+ " ethnicity.Ethnicity,(select sum(ifnull(ds.Indirectexpenseamt,0)) from customer_tier2reportindirectexpenses ds,  customer_tier2reportmaster t2m"
						+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("AND ds.CertificateId=customer_tier2reportindirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 ) totalexpenses,"
						+ " round(((sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt))/(select sum(ds.IndirectExpenseAmt)"
						+ " from customer_tier2reportindirectexpenses ds,customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(t2m.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  t2m.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(t2m.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(t2m.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append("AND ds.CertificateId=customer_tier2reportindirectexpenses.CertificateId ");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" and t2m.vendorId in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ");
				}

				sqlQuery.append(" And t2m.IsSubmitted = 1 )*100),3) PercentDiversity, DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y')"
						+ " SubmittedDate FROM customer_vendormaster  pvm "
						+ " inner join customer_tier2reportmaster on customer_tier2reportmaster.vendorId = pvm.ID AND (customer_tier2reportmaster.IsSubmitted = 1) ");

				if (startDates.size() > 0 && endDates.size() > 0) {
					sqlQuery.append("and (");
					for (int i = 0; i < startDates.size(); i++) {
						sqlQuery.append("(customer_tier2reportmaster.ReportingStartDate >= '"
								+ startDates.get(i)
								+ " 00:00:00' and  customer_tier2reportmaster.ReportingEndDate <= '"
								+ endDates.get(i) + " 23:59:59' )");
						if (startDates.size() > 1 && endDates.size() > 1) {
							if (i < startDates.size() - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				} else {
					if (reportYear != null && reportYear.length > 0) {
						sqlQuery.append("and (");
						for (int i = 0; i < reportYear.length; i++) {
							sqlQuery.append("(year(customer_tier2reportmaster.ReportingStartDate) = "
									+ reportYear[i]
									+ " or year(customer_tier2reportmaster.ReportingEndDate) = "
									+ reportYear[i] + ")");
							if (i < reportYear.length - 1) {
								sqlQuery.append(" or ");
							}
						}
					}
					sqlQuery.append(") ");
				}

				sqlQuery.append(" inner join customer_tier2reportindirectexpenses on customer_tier2reportmaster.Tier2ReportMasterid = customer_tier2reportindirectexpenses.Tier2ReoprtMasterid "
						+ " inner join customer_vendormaster  t2v on customer_tier2reportindirectexpenses.Tier2_Vendorid=t2v.ID inner join customer_certificatemaster on "
						+ " customer_tier2reportindirectexpenses.CertificateId=customer_certificatemaster.ID");

				if (!arrayToStringWithSingleQuote(reportForm.getCertificate())
						.equalsIgnoreCase("")
						&& reportForm.getCertificate().length != 0) {
					sqlQuery.append(" AND customer_tier2reportindirectexpenses.CertificateId IN ("
							+ arrayToStringWithSingleQuote(reportForm
									.getCertificate()) + ") ");
				}
				
				
				sqlQuery.append(" left outer join customer_marketsector on customer_marketsector.ID=customer_tier2reportindirectexpenses.marketSectorId "
						+ "LEFT OUTER JOIN customer_commoditycategory ON customer_commoditycategory.ID=customer_tier2reportindirectexpenses.marketSubSectorId "
						+ "left outer join ethnicity on ethnicity.id=customer_tier2reportindirectexpenses.ethnicityId");

				if (!arrayToString(reportForm.getTier2VendorNames()).contains(
						str)
						&& !arrayToString(reportForm.getTier2VendorNames())
								.equals("")
						&& reportForm.getTier2VendorNames().length != 0) {
					sqlQuery.append(" WHERE ( pvm.ID in ("
							+ arrayToString(reportForm.getTier2VendorNames())
							+ " ) ) ");
				}
				
				sqlQuery.append(" and (pvm.vendorstatus = 'A' OR pvm.vendorstatus = 'B')and pvm.CUSTOMERDIVISIONID in(2,1) "
						+ " group by customer_certificatemaster.CERTIFICATENAME,"
						+ " pvm.VENDORNAME, t2v.VENDORNAME,customer_tier2reportmaster.ReportingPeriod,"
						+ " (case "
						+ "	when customer_tier2reportmaster.CreatedOn < '2017-08-16' "
						+ "	then  customer_marketsector.SECTORDESCRIPTION "
						+ "	when customer_tier2reportmaster.CreatedOn >= '2017-08-16' "
						+ "	then  customer_commoditycategory.CATEGORYDESCRIPTION "
						+ "	end ),ethnicity.Ethnicity,customer_tier2reportmaster.CreatedOn");
										

				System.out
						.println("4.Report in Diversity (Indirect) ReportDaoImpl @ 1861:"
								+ sqlQuery.toString());
				
				sqlQuery.append(") as a group by certificatename,primevendor,t2vendor,ryear,reportingperiod,"
						+ " SECTORDESCRIPTION ,"
						+ " ethnicity, submitteddate "
						+ " order by  certificatename,primevendor,t2vendor,ryear,reportingperiod,SECTORDESCRIPTION,ethnicity,submitteddate");
				
				System.out.println("4.Report in Diversity (Both) ReportDaoImpl @ 1861:"+ sqlQuery.toString());
				
			}

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				reportDto.setVendorUserName(rs.getString(2));
				reportDto.setSpendYear(rs.getInt(3));
				reportDto.setReportPeriod(rs.getString(4));
				reportDto.setCertificateName(rs.getString(5));
				reportDto.setDirectExp(Double.parseDouble(rs.getString(6)));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(7)));
				reportDto.setSectorDescription(rs.getString(8));
				reportDto.setEthnicity(rs.getString(9));
				reportDto.setTotalSales(Double.parseDouble(rs.getString(10)));
				reportDto.setDiversityPercent(rs.getString(11));
				reportDto.setCreatedOn(rs.getString(12));
				if (type.equals("ethnicity")) {
					if (rs.getString(11) != null) {
						reportDto.setEthnicity(rs.getString(11));
					}
				}
				reportDtos.add(reportDto);
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			System.out.println(e);
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getEthnicityBreakdownReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();
			// Integer reportType = reportForm.getReportType();

			if (period != null && !period.equalsIgnoreCase("0")) {
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			// if (reportType != null && reportType == 0) {
			// sqlQuery.append("Select customer_vendormaster.VENDORNAME, customer_vendormaster1.VENDORNAME As VENDORNAME1,"
			// +
			// " customer_tier2reportmaster.ReportingPeriod, customer_certificatemaster.CERTIFICATENAME,"
			// +
			// " ethnicity.Ethnicity, sum(customer_tier2reportdirectexpenses.DirExpenseAmt)DirectSpendAmt"
			// +
			// " From customer_vendormaster Inner Join customer_tier2reportmaster "
			// +
			// " On customer_vendormaster.ID = customer_tier2reportmaster.vendorId "
			// + " Inner Join customer_tier2reportdirectexpenses "
			// +
			// " On customer_tier2reportmaster.Tier2ReportMasterid = customer_tier2reportdirectexpenses.Tier2ReoprtMasterid"
			// + " Inner Join customer_vendormaster customer_vendormaster1"
			// +
			// " On customer_tier2reportdirectexpenses.Tier2_Vendorid = customer_vendormaster1.ID Inner Join"
			// +
			// " ethnicity On customer_vendormaster1.ETHNICITY_ID = ethnicity.id"
			// + " Inner Join customer_certificatemaster"
			// +
			// " On customer_tier2reportdirectexpenses.CertificateId = customer_certificatemaster.ID"
			// + " where customer_certificatemaster.DIVERSE_QUALITY = 1 ");
			//
			// if (null != startDate && !startDate.isEmpty()
			// && null != endDate && !endDate.isEmpty()) {
			// sqlQuery.append(" AND ( customer_tier2reportmaster.ReportingStartDate >= '"
			// + startDate
			// + " 00:00:00' ) AND"
			// + " ( customer_tier2reportmaster.ReportingEndDate <= '"
			// + endDate + " 23:59:59' ) ");
			// } else {
			// sqlQuery.append(" And (year(customer_tier2reportmaster.reportingstartdate) = "
			// + reportYear
			// + " OR  year(customer_tier2reportmaster.reportingenddate) = "
			// + reportYear + ") ");
			// }
			//
			// if (!arrayToString(reportForm.getTier2VendorNames()).contains(
			// str)
			// && !arrayToString(reportForm.getTier2VendorNames())
			// .equals("")
			// && reportForm.getTier2VendorNames().length != 0) {
			// sqlQuery.append("and customer_vendormaster.ID in ("
			// + arrayToString(reportForm.getTier2VendorNames())
			// + " ) ");
			// }
			//
			// sqlQuery.append(" And customer_tier2reportmaster.IsSubmitted = 1"
			// +
			// " and (customer_vendormaster.vendorstatus = 'A' OR customer_vendormaster.vendorstatus = 'B')"
			// + " group by customer_vendormaster.VENDORNAME,"
			// +
			// " customer_certificatemaster.CERTIFICATENAME, customer_tier2reportmaster.ReportingPeriod,"
			// + " customer_vendormaster1.VENDORNAME, ethnicity.Ethnicity"
			// + " order by Vendorname, certificatename, directspendamt desc");
			//
			// Statement stmt = connection.createStatement();
			// ResultSet rs = stmt.executeQuery(sqlQuery.toString());
			// System.out.println("5.Report in Ethnicity Report(1):"+sqlQuery.toString());
			//
			// while (rs.next()) {
			// Tier2ReportDto reportDto = new Tier2ReportDto();
			// reportDto.setVendorName(rs.getString(1));
			// reportDto.setVendorUserName(rs.getString(2));
			// reportDto.setReportPeriod(rs.getString(3));
			// reportDto.setCertificateName(rs.getString(4));
			// reportDto.setEthnicity(rs.getString(5));
			// reportDto.setDirectExp(Double.parseDouble(rs.getString(6)));
			// reportDtos.add(reportDto);
			// }
			//
			// } else {
			sqlQuery.append("Select customer_vendormaster.VENDORNAME, customer_tier2reportmaster.ReportingPeriod, "
					+ " sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)IndirectSpendamt "
					+ " From customer_tier2reportindirectexpenses Inner Join customer_tier2reportmaster "
					+ " On customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid "
					+ " Inner Join customer_vendormaster On customer_tier2reportmaster.vendorId = customer_vendormaster.ID And "
					+ " customer_tier2reportmaster.IsSubmitted = 1 where");

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append(" ( customer_tier2reportmaster.ReportingStartDate >= '"
						+ startDate
						+ " 00:00:00' ) AND"
						+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ) ");
			} else {
				sqlQuery.append(" (year(customer_tier2reportmaster.reportingstartdate) = "
						+ reportYear
						+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
						+ reportYear + ") ");
			}

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and customer_vendormaster.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("  group by customer_vendormaster.VENDORNAME,"
					+ " customer_tier2reportmaster.ReportingPeriod "
					+ " order by customer_vendormaster.VENDORNAME ");

			System.out
					.println("5.Report in Ethnicity Report (ReportDaoImpl @2022):"
							+ sqlQuery.toString());
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				Tier2ReportDto reportDto = new Tier2ReportDto();
				reportDto.setVendorName(rs.getString(1));
				reportDto.setReportPeriod(rs.getString(2));
				// reportDto.setCertificateName(rs.getString(3));
				// reportDto.setEthnicity(rs.getString(4));
				reportDto.setIndirectExp(Double.parseDouble(rs.getString(3)));
				reportDtos.add(reportDto);
			}
			// }

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} catch (SQLException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getSpendDashboardReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();

			if (period != null && !period.equalsIgnoreCase("0")) {
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("select t.Vendorname,t.Tier2Reportmasterid,t.slno,t.T2Vendor,t.Certificatename,t.ethnicty,t.ReportingPeriod,"
					+ " t.DirExpenseAmt,t.indirectexpenseamt,tm.TotalSales,tm.TotalSalesToCompany, (t.DirExpenseAmt+t.indirectexpenseamt) total"
					+ " from(Select(1)slno,customer_vendormaster.VENDORNAME,customer_vendormaster1.VENDORNAME As T2VENDOR,"
					+ " customer_certificatemaster.CERTIFICATENAME,ethnicity.Ethnicity as ethnicty,"
					+ " customer_tier2reportmaster.Tier2Reportmasterid,customer_tier2reportmaster.ReportingPeriod,"
					+ " customer_tier2reportdirectexpenses.DirExpenseAmt,(0)indirectexpenseamt"
					+ " From customer_vendormaster Inner Join customer_tier2reportmaster"
					+ " On customer_vendormaster.ID = customer_tier2reportmaster.vendorId Inner Join"
					+ " customer_tier2reportdirectexpenses On customer_tier2reportmaster.Tier2ReportMasterid ="
					+ " customer_tier2reportdirectexpenses.Tier2ReoprtMasterid Inner Join customer_vendormaster customer_vendormaster1"
					+ " On customer_tier2reportdirectexpenses.Tier2_Vendorid = customer_vendormaster1.ID Inner Join"
					+ " ethnicity On customer_vendormaster1.ETHNICITY_ID = ethnicity.id Inner Join"
					+ " customer_certificatemaster On customer_tier2reportdirectexpenses.CertificateId = customer_certificatemaster.ID"
					+ " where customer_tier2reportmaster.IsSubmitted = 1"
					+ " and (customer_vendormaster.vendorstatus = 'A' OR customer_vendormaster.vendorstatus = 'B') ");

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append(" AND ( customer_tier2reportmaster.ReportingStartDate >= '"
						+ startDate
						+ " 00:00:00' ) AND"
						+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ) ");
			} else {
				sqlQuery.append(" And (year(customer_tier2reportmaster.reportingstartdate) = "
						+ reportYear
						+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
						+ reportYear + ") ");
			}

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and customer_vendormaster.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " )");
			}

			sqlQuery.append(" union all"
					+ " Select (2)slno,customer_vendormaster.VENDORNAME,'' As tier2Vendor,customer_certificatemaster.CERTIFICATENAME,"
					+ " ethnicity.Ethnicity as ethnicty,customer_tier2reportmaster.Tier2Reportmasterid,customer_tier2reportmaster.ReportingPeriod,"
					+ " (0)DirExpenseAmt,customer_tier2reportindirectexpenses.IndirectExpenseAmt"
					+ " From customer_vendormaster Inner Join customer_tier2reportmaster"
					+ " On customer_vendormaster.ID = customer_tier2reportmaster.vendorId"
					+ " Inner Join customer_tier2reportindirectexpenses"
					+ " On customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid"
					+ " Inner Join ethnicity On customer_tier2reportindirectexpenses.ethnicityId = ethnicity.id"
					+ " Inner Join customer_certificatemaster On customer_tier2reportindirectexpenses.certificateId = customer_certificatemaster.ID"
					+ " where customer_tier2reportmaster.IsSubmitted = 1"
					+ " and (customer_vendormaster.vendorstatus = 'A' OR customer_vendormaster.vendorstatus = 'B')");

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			if (null != startDate && !startDate.isEmpty() && null != endDate
					&& !endDate.isEmpty()) {
				sqlQuery.append(" AND ( customer_tier2reportmaster.ReportingStartDate >= '"
						+ startDate
						+ " 00:00:00' ) AND"
						+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
						+ endDate + " 23:59:59' ) ");
			} else {
				sqlQuery.append(" And (year(customer_tier2reportmaster.reportingstartdate) = "
						+ reportYear
						+ " OR  year(customer_tier2reportmaster.reportingenddate) = "
						+ reportYear + ") ");
			}

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and customer_vendormaster.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(") as t,customer_tier2reportmaster tm where t.Tier2Reportmasterid=tm.Tier2ReportMasterid"
					+ " order by t.vendorname,t.tier2reportmasterid,t.slno");

			System.out
					.println("6.Spend Data Dashboard Report (ReportDaoImpl @2183):"
							+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setVendorName(objects[0].toString());
					reportDto.setTier2reportId(Integer.parseInt(objects[1]
							.toString()));
					reportDto.setReportNumber(Integer.parseInt(objects[2]
							.toString()));
					reportDto.setVendorUserName(objects[3].toString());
					reportDto.setCertificateName(objects[4].toString());
					reportDto.setEthnicity(objects[5].toString());
					reportDto.setReportPeriod(objects[6].toString());
					reportDto.setDirectExp(Double.parseDouble(objects[7]
							.toString()));
					reportDto.setIndirectExp(Double.parseDouble(objects[8]
							.toString()));

					if (objects[9] != null) {
						reportDto.setTotalSales(Double.parseDouble(objects[9]
								.toString()));
					} else {
						reportDto.setTotalSales((double) 0);
					}

					if (objects[10] != null) {
						reportDto.setTotalSalesToCompany(Double
								.parseDouble(objects[10].toString()));
					} else {
						reportDto.setTotalSalesToCompany((double) 0);
					}

					reportDto.setGrandTotal(Double.parseDouble(objects[11]
							.toString()));
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getAgencyReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			String str = "all";
			String startDate = "";
			String endDate = "";
			String period = reportForm.getReportingPeriod();
			Integer reportYear = reportForm.getSpendYear();

			if (period != null && !period.equalsIgnoreCase("0")) {
				String peString[] = period.split("-");
				startDate = getReportingPeriod(peString[0]);
				endDate = getReportingPeriod(peString[1]);
			}

			sqlQuery.append("SELECT 1,(pvm.VENDORNAME) PrimeVendor,t2v.VENDORNAME  t2vendor,('"
					+ reportYear
					+ "') ryear,"
					+ " customer_tier2reportmaster.ReportingPeriod,customer_certificatemaster.CERTIFICATENAME,"
					+ " sum(customer_tier2reportdirectexpenses.DirExpenseAmt)directspendamt,(0)indirectspendamt ,"
					+ " (select sum(ds.DirExpenseAmt)from customer_tier2reportdirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.ReportingStartDate >= '"
					+ startDate
					+ "'  AND"
					+ " t2m.ReportingEndDate <= '"
					+ endDate + "' ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(") totalexpenses,"
					+ " ((sum(customer_tier2reportdirectexpenses.DirExpenseAmt))/(select sum(ds.DirExpenseAmt)from customer_tier2reportdirectexpenses ds, "
					+ " customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.ReportingStartDate >= '"
					+ startDate + "' AND" + " t2m.ReportingEndDate <= '"
					+ endDate + "'");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(")*100) PercentDiversity,"
					+ " DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') SubmittedDate FROM customer_vendormaster  pvm,"
					+ " customer_tier2reportmaster, customer_tier2reportdirectexpenses,customer_vendormaster  t2v,"
					+ " customer_certificatemaster WHERE ( customer_tier2reportmaster.vendorId = pvm.ID ) and"
					+ " ( customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ) and"
					+ " ( t2v.ID = customer_tier2reportdirectexpenses.Tier2_Vendorid ) and"
					+ " ( customer_certificatemaster.ID = customer_tier2reportdirectexpenses.CertificateId ) and"
					+ " ( ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("(pvm.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ) AND");
			}

			sqlQuery.append(" ( customer_tier2reportmaster.ReportingStartDate >= '"
					+ startDate
					+ "' ) AND"
					+ " ( customer_tier2reportmaster.ReportingEndDate <= '"
					+ endDate
					+ "' ))"
					+ " group by pvm.VENDORNAME , customer_certificatemaster.CERTIFICATENAME, t2v.VENDORNAME , customer_tier2reportmaster.ReportingPeriod,"
					+ " customer_tier2reportmaster.CreatedOn union all SELECT 2,(pvm.VENDORNAME) PrimeVendor, '',('"
					+ reportYear
					+ "') ryear ,"
					+ " customer_tier2reportmaster.ReportingPeriod, customer_certificatemaster.CERTIFICATENAME,(0),"
					+ " sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt)Indirectspendamt,"
					+ " (select sum(ds.IndirectExpenseAmt)from customer_tier2reportindirectexpenses ds,  customer_tier2reportmaster t2m"
					+ " where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid and t2m.ReportingStartDate >= '"
					+ startDate
					+ "' AND t2m.ReportingEndDate <= '"
					+ endDate
					+ "' ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(" ) totalexpenses, ((sum(customer_tier2reportindirectexpenses.IndirectExpenseAmt))/(select sum(ds.IndirectExpenseAmt)from"
					+ " customer_tier2reportindirectexpenses ds,customer_tier2reportmaster t2m where ds.Tier2ReoprtMasterid=t2m.Tier2ReportMasterid"
					+ "	and t2m.ReportingStartDate >= '"
					+ startDate
					+ "' AND t2m.ReportingEndDate <= '" + endDate + "' ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("and t2m.vendorId in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ");
			}

			sqlQuery.append(")*100) PercentDiversity,DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') SubmittedDate"
					+ " FROM customer_vendormaster  pvm, customer_tier2reportmaster, customer_tier2reportindirectexpenses,"
					+ " customer_certificatemaster WHERE ( customer_tier2reportmaster.vendorId = pvm.ID ) and  "
					+ " ( customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid ) and"
					+ " ( customer_certificatemaster.ID = customer_tier2reportindirectexpenses.CertificateId ) and"
					+ " ( ");

			if (!arrayToString(reportForm.getTier2VendorNames()).contains(str)
					&& !arrayToString(reportForm.getTier2VendorNames()).equals(
							"") && reportForm.getTier2VendorNames().length != 0) {
				sqlQuery.append("(pvm.ID in ("
						+ arrayToString(reportForm.getTier2VendorNames())
						+ " ) ) AND");
			}

			sqlQuery.append(" ( customer_tier2reportmaster.ReportingStartDate >= '"
					+ startDate
					+ "' ) AND "
					+ "( customer_tier2reportmaster.ReportingEndDate <= '"
					+ endDate
					+ "' )) "
					+ " group by pvm.VENDORNAME ,customer_certificatemaster.CERTIFICATENAME,"
					+ "customer_tier2reportmaster.ReportingPeriod, customer_tier2reportmaster.CreatedOn");

			System.out.println("AGENCY REPORT (ReportDaoImpl @2391):"
					+ sqlQuery);
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setReportNumber(Integer.parseInt(objects[0]
							.toString()));
					reportDto.setVendorName(objects[1].toString());
					reportDto.setVendorUserName(objects[2].toString());
					reportDto.setSpendYear(Integer.parseInt(objects[3]
							.toString()));
					reportDto.setReportPeriod(objects[4].toString());
					reportDto.setCertificateName(objects[5].toString());
					reportDto.setDirectExp(Double.parseDouble(objects[6]
							.toString()));
					reportDto.setIndirectExp(Double.parseDouble(objects[7]
							.toString()));
					reportDto.setTotalSales(Double.parseDouble(objects[8]
							.toString()));
					reportDto.setDiversityPercent(objects[9].toString());
					reportDto.setCreatedOn(objects[10].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private String getReportingPeriod(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			if (date != null && !date.isEmpty()) {
				d = sdf.parse(date.trim());
				sdf.applyPattern(NEW_FORMAT);
				return sdf.format(d);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getEthnicityDashboardReport(
			UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("select cv.ethnicity_id, et.ethnicity,count(*) count,"
					+ " (round((((select count(*) from customer_vendormaster cv1 where cv1.ETHNICITY_ID=cv.ETHNICITY_ID "
					+ " and cv1.primenonprimevendor = 0 and cv1.ISAPPROVED = 1)/ (select count(*) from customer_vendormaster cv2,"
					+ " ethnicity et2 where cv2.ethnicity_id=et2.id and cv2.primenonprimevendor = 0 "
					+ " and cv2.ISAPPROVED = 1))*100),0))percent from customer_vendormaster cv,ethnicity et"
					+ " where cv.ethnicity_id=et.id and ((primenonprimevendor=0 or primenonprimevendor=1)");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" and DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append(" ) and cv.ISAPPROVED = 1");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("and cv.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" group by et.ethnicity order by et.ethnicity");

			System.out.println("4.ETHNICITYDASHBOARD (ReportDaoImpl @2503):"
					+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setEthnicityId(Integer.parseInt(objects[0]
							.toString()));
					reportDto.setEthnicity(objects[1].toString());
					reportDto
							.setCount(Double.parseDouble(objects[2].toString()));
					reportDto.setEthnicityPercent(Double.parseDouble(objects[3]
							.toString()));
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getDiversityDashboardReport(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cc.CERTIFICATENAME, cc.CERTIFICATESHORTNAME, COUNT(*) COUNT, (ROUND(CONCAT(100*(COUNT(*)/( SELECT COUNT(*) "
					+ " FROM customer_vendordiverseclassifcation cvc1, customer_certificatemaster cc1, customer_vendormaster cv1 "
					+ " WHERE cvc1.CERTMASTERID=cc1.ID AND cv1.ID=cvc1.VENDORID "
					+ " AND (cc1.id IN ( SELECT crc1.CERTMASTERID FROM certificate_report_config crc1 WHERE crc1.DASHBOARDID=2)) ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND cv1.DIVSERSUPPLIER =1 ");
			}

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cv1.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}

			}
			sqlQuery.append(" and (cv1.VENDORSTATUS = 'A' OR cv1.VENDORSTATUS='B'))),'%'),2)) percent "
					+ " FROM customer_vendordiverseclassifcation cvc, customer_certificatemaster cc, customer_vendormaster cv "
					+ " WHERE cvc.CERTMASTERID=cc.ID AND cv.ID=cvc.VENDORID AND (cv.VENDORSTATUS = 'A' OR cv.VENDORSTATUS='B') "
					+ " AND (cc.id IN ( SELECT crc.CERTMASTERID FROM certificate_report_config crc WHERE crc.DASHBOARDID=2)) ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND cv.DIVSERSUPPLIER=1 ");
			}

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" AND cv.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}

			}
			sqlQuery.append(" GROUP BY cc.CERTIFICATENAME,cc.CERTIFICATESHORTNAME "
					+ " ORDER BY cc.CERTIFICATENAME,cc.CERTIFICATESHORTNAME ");

			System.out.println("2.DIVERSITYDASHBOARD (ReportDaoImpl @2609):"
					+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setCertificateName(objects[0].toString());
					reportDto.setCertificateShortName(objects[1].toString());
					reportDto
							.setCount(Double.parseDouble(objects[2].toString()));
					reportDto.setDiversityPercent(objects[3].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Tier2ReportDto> getSearchReport(
			SearchVendorForm searchVendorForm, UserDetailsDto appDetails) {
		java.sql.Connection connection = DataBaseConnection
				.getConnection(appDetails);
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(connection);

		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		List<String> whereConditionTableList = new ArrayList<String>();
		// Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			/*
			 * sqlQuery.append(
			 * " Select DISTINCT  cv.ID, cv.VENDORNAME, (SELECT group_concat(customer_vendorowner.OWNERNAME SEPARATOR"
			 * +
			 * " ',') FROM customer_vendorowner WHERE cv.ID = customer_vendorowner.VENDORID)OWNER, cvam.ADDRESS,"
			 * + " cvam.CITY, state.STATENAME, cvam.ZIPCODE, cvam.PHONE," +
			 * " cvam.FAX, cv.EMAILID, cam.CERTAGENCYNAME, cm.CERTIFICATENAME,"
			 * +
			 * " IFNULL(DATE_FORMAT(cvc.EFFECTIVEDATE, '%d-%m-%Y'),0) effectivedate, IFNULL(DATE_FORMAT(cvc.EXPIRYDATE, '%d-%m-%Y'),0) expirydate,"
			 * + " cvn.CAPABILITIES, naics.NAICSDESCRIPTION");
			 * 
			 * sqlQuery.append(
			 * " from customer_vendormaster cv Left Join customer_vendoraddressmaster cvam On "
			 * + " cv.ID = cvam.VENDORID and cvam.ADDRESSTYPE='m'" +
			 * " left outer join state On cvam.STATE = state.ID Left Join customer_vendorcertificate cvc On "
			 * +
			 * " cv.ID = cvc.VENDORID left outer join customer_certificatemaster cm On"
			 * +
			 * " cvc.CERTMASTERID = cm.ID left outer join customer_certificateagencymaster cam On "
			 * +
			 * " cvc.CERTAGENCYID = cam.ID Left Join customer_vendornaics cvn On "
			 * +
			 * " cv.ID =cvn.VENDORID  left outer join customer_naicscode naics On cvn.NAICSID = naics.NAICSID "
			 * );
			 */
			sqlQuery.append("SELECT DISTINCT customer_vendormaster.ID, customer_vendormaster.VENDORNAME, "
					+ "(SELECT GROUP_CONCAT(customer_vendorowner.OWNERNAME SEPARATOR ',') "
					+ "FROM customer_vendorowner "
					+ "WHERE customer_vendormaster.ID = customer_vendorowner.VENDORID AND ownername IS NOT NULL AND TRIM(ownername)<>\"\")OWNER, "
					+ " customer_vendoraddressmaster.ADDRESS, customer_vendoraddressmaster.CITY, state.STATENAME, customer_vendoraddressmaster.ZIPCODE, customer_vendoraddressmaster.PHONE, customer_vendoraddressmaster.FAX, customer_vendormaster.EMAILID,"
					+ "(SELECT GROUP_CONCAT(CONCAT('| ',customer_naicscode.NAICSCODE,'-',customer_naicscode.NAICSDESCRIPTION,',', IFNULL(customer_vendornaics.CAPABILITIES,''),'| ')) "
					+ " FROM customer_vendornaics,customer_naicscode WHERE customer_vendornaics.vendorid=customer_vendormaster.id AND "
					+ "customer_naicscode.NAICSID=customer_vendornaics.NAICSID)Naics_Capabilities, "
					+ "(SELECT GROUP_CONCAT(customer_certificatemaster.CERTIFICATENAME) "
					+ "	FROM customer_certificatemaster,customer_vendordiverseclassifcation  WHERE customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.id AND "
					+ "customer_certificatemaster.id=customer_vendordiverseclassifcation.CERTMASTERID)DiverseClassification, "
					+ "(SELECT GROUP_CONCAT(CONCAT('| ',customer_certificatemaster.CERTIFICATENAME,' - ',customer_certificateagencymaster.CERTAGENCYNAME,' - ', IFNULL(DATE_FORMAT(customer_vendorcertificate.EFFECTIVEDATE, '%m/%d/%Y'),0),' - ', IFNULL(DATE_FORMAT(customer_vendorcertificate.EXPIRYDATE, '%m/%d/%Y'),0),'|')) "
					+ "FROM customer_certificateagencymaster,customer_vendorcertificate,customer_certificatemaster "
					+ "WHERE customer_certificateagencymaster.id=customer_vendorcertificate.CERTAGENCYID AND "
					+ "customer_vendorcertificate.CERTMASTERID=customer_certificatemaster.id AND "
					+ "customer_vendorcertificate.VENDORID=customer_vendormaster.id)Certificate_Agency_EffectiveDate_ExpiryDate "
					+ "FROM customer_vendormaster  "
					+ "LEFT JOIN customer_vendoraddressmaster ON customer_vendormaster.ID = customer_vendoraddressmaster.VENDORID AND customer_vendoraddressmaster.ADDRESSTYPE='m' "
					+ "	LEFT OUTER JOIN state ON customer_vendoraddressmaster.STATE = state.ID ");

			sqlQuery.append("Left Join customer_vendorcertificate On "
					+ " customer_vendormaster.ID = customer_vendorcertificate.VENDORID left outer join customer_certificatemaster On"
					+ " customer_vendorcertificate.CERTMASTERID = customer_certificatemaster.ID left outer join customer_certificateagencymaster  On "
					+ " customer_vendorcertificate.CERTAGENCYID = customer_certificateagencymaster.ID Left Join customer_vendornaics On "
					+ " customer_vendormaster.ID =customer_vendornaics.VENDORID  left outer join customer_naicscode On customer_vendornaics.NAICSID = customer_naicscode.NAICSID ");

			// Add Query based on Certification Type
			sqlQuery.append(" INNER JOIN customer_vendordiverseclassifcation ON customer_vendordiverseclassifcation.VENDORID=customer_vendormaster.ID "
					+ " AND customer_vendordiverseclassifcation.ACTIVE=1 INNER JOIN customer_vendorcertificate certificate ON "
					+ " certificate.VENDORID=customer_vendormaster.id and customer_vendordiverseclassifcation.CERTMASTERID=certificate.CERTMASTERID "
					+ " and certificate.VENDORID=customer_vendordiverseclassifcation.VENDORID INNER JOIN customer_classification_certificatetypes "
					+ " ON certificate.CERTMASTERID=customer_classification_certificatetypes.CERTIFICATEID "
					+ " and customer_vendordiverseclassifcation.CERTMASTERID=customer_classification_certificatetypes.CERTIFICATEID ");

			StringBuilder conditions = new StringBuilder();
			// if (searchVendorForm.getVendorType() != null
			// && SupplierType.valueOf(searchVendorForm.getVendorType())
			// .getIndex() == 2) {
			//
			// } else {
			// conditions.append(" And cv.PRIMENONPRIMEVENDOR ="
			// + SupplierType
			// .valueOf(searchVendorForm.getVendorType())
			// .getIndex());
			// }
			/*
			 * if (searchVendorForm.getPrimeVendorStatus() != null &&
			 * !searchVendorForm.getPrimeVendorStatus().isEmpty()) { if
			 * (searchVendorForm.getPrimeVendorStatus().equalsIgnoreCase( "A")
			 * || searchVendorForm.getPrimeVendorStatus()
			 * .equalsIgnoreCase("B")) conditions
			 * .append(" and customer_vendormaster.VENDORSTATUS='" +
			 * searchVendorForm.getPrimeVendorStatus() + "'"); else conditions
			 * .append(
			 * " and (customer_vendormaster.VENDORSTATUS='A' or customer_vendormaster.VENDORSTATUS='B') "
			 * ); }
			 */

			if (searchVendorForm.getSearchVendorStatus() != null
					&& searchVendorForm.getSearchVendorStatus().length != 0) {
				StringBuilder vendorStatus = new StringBuilder();

				for (String status : searchVendorForm.getSearchVendorStatus()) {
					if (!status.equalsIgnoreCase("0"))
						vendorStatus.append("'" + status + "',");
				}

				if (vendorStatus.toString() != null
						&& vendorStatus.length() > 0) {
					vendorStatus.deleteCharAt(vendorStatus.length() - 1);
					conditions
							.append(" and customer_vendormaster.VENDORSTATUS in  ("
									+ vendorStatus.toString() + ")");
				} else
					conditions
							.append(" and customer_vendormaster.VENDORSTATUS in('A','B','P','N') ");
			} else
				conditions
						.append(" and customer_vendormaster.VENDORSTATUS in('A','B','P','N') ");

			// To get the current user division wise result
			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" AND customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			int index = 0;

			boolean flag = false;
			boolean isMeetingInfo = false;
			boolean isVendorCommodityInfo = false;

			for (String option : searchVendorForm.getSearchFields()) {
				String searchFieldValue = null;
				if (searchVendorForm.getSearchFieldsValue() != null
						&& searchVendorForm.getSearchFieldsValue()[index] != null
						&& !searchVendorForm.getSearchFieldsValue()[index]
								.isEmpty()) {
					searchFieldValue = searchVendorForm.getSearchFieldsValue()[index];
				}
				index++;
				if (searchFieldValue != null) {
					switch (SearchFields.valueOf(option)) {
					case LEGALCOMPANYNAME:
						conditions
								.append(" and customer_vendormaster.VENDORNAME LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case COMPANYCODE:
						conditions
								.append(" and customer_vendormaster.VENDORCODE ='"
										+ searchFieldValue + "'");
						break;
					case DUNSNUMBER:
						conditions
								.append(" and customer_vendormaster.DUNSNUMBER LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case TAXID:
						conditions
								.append(" and customer_vendormaster.TAXID = '"
										+ searchFieldValue + "'");
						break;
					case NUMBEROFEMPLOYEES:
						conditions
								.append(" and customer_vendormaster.NUMBEROFEMPLOYEES= "
										+ searchFieldValue + "");
						break;

					case COMPANYTYPE:
						conditions
								.append(" and customer_vendormaster.COMPANY_TYPE='"
										+ searchFieldValue + "'");
						break;
					case YEARESTABLISHED:
						conditions
								.append(" and customer_vendormaster.YEAROFESTABLISHMENT = "
										+ searchFieldValue);
						break;
					case ADDRESS:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}

						conditions
								.append(" and customer_vendoraddressmaster.ADDRESS LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case CITY:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.CITY LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case STATE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.STATE = "
										+ searchFieldValue + "");
						/*
						 * conditions.append(" and state.STATENAME LIKE '%" +
						 * searchFieldValue + "%'");
						 */
						break;

					case PROVINCE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.PROVINCE LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case REGION:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						sqlQuery.append(" and customer_vendoraddressmaster.REGION LIKE '%"
								+ searchFieldValue + "%'");
						break;
					case COUNTRY:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						StringBuilder builder = new StringBuilder();
						if (searchVendorForm.getSelectCountries() != null
								&& searchVendorForm.getSelectCountries().length != 0) {
							for (String country : searchVendorForm
									.getSelectCountries()) {
								builder.append("'" + country + "',");
							}
							builder.deleteCharAt(builder.length() - 1);
							conditions
									.append(" and customer_vendoraddressmaster.COUNTRY IN ("
											+ builder.toString() + ")");
						}
						break;
					case ZIPCODE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.ZIPCODE = '"
										+ searchFieldValue + "'");
						break;
					case MOBILE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.MOBILE LIKE '%"
										+ searchFieldValue + "%'");
						break;

					case PHONE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.PHONE = '"
										+ searchFieldValue + "'");
						break;
					case FAX:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendoraddressmaster.FAX = '"
										+ searchFieldValue + "'");
						break;
					case WEBSITEURL:
						conditions
								.append(" and customer_vendormaster.WEBSITE LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case EMAIL:
						conditions
								.append(" and customer_vendormaster.EMAILID LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case NAICSCODE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable2().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable2().trim());
						}
						StringBuilder naicsCodes = new StringBuilder();
						if (searchVendorForm.getNaicsCode() != null
								&& searchVendorForm.getNaicsCode().length != 0) {

							String naics[] = searchVendorForm.getNaicsCode();
							String naics1[] = naics[0].toString().split(",");
							for (String naicsCode : naics1) {
								naicsCodes.append("'" + naicsCode + "',");
							}
							naicsCodes.deleteCharAt(naicsCodes.length() - 1);
							conditions
									.append(" and customer_naicscode.NAICSCODE  in ("
											+ naicsCodes.toString() + ") ");
						}
						break;
					case NAICSDESC:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable2().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable2().trim());
						}
						StringBuilder naicsDescriptions = new StringBuilder();
						if (searchVendorForm.getNaicsDescription() != null
								&& searchVendorForm.getNaicsDescription().length != 0) {

							String naics[] = searchVendorForm
									.getNaicsDescription();
							String naicsDesc[] = naics[0].toString().split(",");

							for (String description : naicsDesc) {
								if (description.startsWith(" ")) {
									description = description.substring(1);
								}
								naicsDescriptions.append("'" + description
										+ "',");
							}
							naicsDescriptions.deleteCharAt(naicsDescriptions
									.length() - 1);
							conditions
									.append(" and customer_naicscode.NAICSDESCRIPTION  in ("
											+ naicsDescriptions.toString()
											+ ") ");
						}

						break;

					/*
					 * case NAICSCODE: StringBuilder naicsCodes = new
					 * StringBuilder(); if (searchVendorForm.getNaicsCode() !=
					 * null && searchVendorForm.getNaicsCode().length != 0) {
					 * for (String naicsCode : searchVendorForm .getNaicsCode())
					 * { naicsCodes.append("'" + naicsCode + "',"); }
					 * naicsCodes.deleteCharAt(naicsCodes.length() - 1);
					 * conditions.append(" and naics.NAICSCODE  in (" +
					 * naicsCodes.toString() + ")"); }
					 * conditions.append(" and naics.NAICSCODE = '" +
					 * searchFieldValue + "'"); break; case NAICSDESC:
					 * StringBuilder naicsDescriptions = new StringBuilder(); if
					 * (searchVendorForm.getNaicsDescription() != null &&
					 * searchVendorForm.getNaicsDescription().length != 0) { for
					 * (String description : searchVendorForm
					 * .getNaicsDescription()) { naicsDescriptions .append("'" +
					 * description + "',"); }
					 * naicsDescriptions.deleteCharAt(naicsDescriptions
					 * .length() - 1);
					 * conditions.append(" and naics.NAICSDESCRIPTION  in (" +
					 * naicsDescriptions.toString() + ")"); break; }
					 */
					/*
					 * conditions.append(" and naics.NAICSDESCRIPTION LIKE '%" +
					 * searchFieldValue + "%'");
					 */

					case CAPABILITIES:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendornaics.CAPABILITIES LIKE '%"
										+ searchFieldValue + "%'");
						break;
					case DIVERSECLASSIFICATION:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						if (searchVendorForm.getDiverseCertificateNames() != null
								&& searchVendorForm
										.getDiverseCertificateNames().length != 0) {
							conditions
									.append(" and customer_vendorcertificate.CERTMASTERID IN ("
											+ StringUtils.join(
													searchVendorForm
															.getDiverseCertificateNames(),
													",") + ") ");
						}
						break;
					case CERTIFYINGAGENCY:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						if (searchVendorForm.getCertifyAgencies() != null
								&& searchVendorForm.getCertifyAgencies().length != 0) {
							conditions
									.append(" and customer_certificateagencymaster.ID IN ("
											+ StringUtils.join(searchVendorForm
													.getCertifyAgencies(), ",")
											+ ") ");
						}
						break;
					case CERTIFICATION:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendorcertificate.CERTIFICATENUMBER ="
										+ searchFieldValue);
						break;
					case EFFECTIVEDATE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendorcertificate.EFFECTIVEDATE=' "
										+ searchFieldValue + "'");
						break;

					case EXPIREDATE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						conditions
								.append(" and customer_vendorcertificate.EXPIRYDATE ='"
										+ searchFieldValue + "'");
						break;
					case FIRSTNAME:
						flag = true;
						break;
					case LASTNAME:
						flag = true;
						break;
					case DESIGNATION:
						flag = true;
						break;
					case CONTACTPHONE:
						flag = true;
						break;
					case CONTACTMOBILE:
						flag = true;
						break;
					case CONTACTFAX:
						flag = true;
						break;
					case CONTACTEMAIL:
						flag = true;
						break;
					case DIVERSESUPPLIER:
						conditions
								.append(" and customer_vendormaster.DIVSERSUPPLIER="
										+ searchFieldValue);
						break;
					case BUSINESSAREA:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}

						StringBuilder busnessAreas = new StringBuilder();
						if (searchVendorForm.getBusinessAreaName() != null
								&& searchVendorForm.getBusinessAreaName().length != 0) {
							for (String area : searchVendorForm
									.getBusinessAreaName()) {
								busnessAreas.append("'" + area + "',");
							}
							busnessAreas
									.deleteCharAt(busnessAreas.length() - 1);
							sqlQuery.append(" inner join vendor_businessarea on vendor_businessarea.vendorid=customer_vendormaster.id");
							conditions
									.append(" and vendor_businessarea.BUSINESSAREAID in ("
											+ busnessAreas.toString() + ")");
						}
						break;

					case COMMODITYDESCRIPTION:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						StringBuilder commodityDescription = new StringBuilder();
						if (searchVendorForm.getCommodityDescription() != null
								&& searchVendorForm.getCommodityDescription().length != 0) {
							for (String commodity : searchVendorForm
									.getCommodityDescription()) {
								commodityDescription.append("'" + commodity
										+ "',");
							}
							commodityDescription
									.deleteCharAt(commodityDescription.length() - 1);
							sqlQuery.append(" inner join customer_vendorcommodity on customer_vendorcommodity.vendorid=customer_vendormaster.id");
							conditions
									.append(" and customer_vendorcommodity.COMMODITYID in ("
											+ commodityDescription.toString()
											+ ") ");
						}
						break;

					case CERTIFICATIONTYPE:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable2().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable2().trim());
						}
						StringBuilder certificationType = new StringBuilder();
						if (searchVendorForm.getCertificationType() != null
								&& searchVendorForm.getCertificationType().length != 0) {
							for (String certificate : searchVendorForm
									.getCertificationType()) {
								certificationType.append("'" + certificate
										+ "',");
							}
							certificationType.deleteCharAt(certificationType
									.length() - 1);
							conditions
									.append(" and customer_classification_certificatetypes.CERTIFICATETYPEID  in ("
											+ certificationType.toString()
											+ ") ");
						}
						break;

					case KEYWORD:
						if (!(whereConditionTableList.contains(SearchFields
								.valueOf(option).getFromTable1().trim()))) {
							whereConditionTableList.add(SearchFields
									.valueOf(option).getFromTable1().trim());
						}
						sqlQuery.append(" inner join customer_vendorkeywords  on customer_vendorkeywords.VENDORID = customer_vendormaster.id ");
						String[] keywords = searchFieldValue.split(",");
						for (int ctr = 0; ctr < keywords.length; ctr++) {
							if (ctr == 0)
								conditions.append("AND ( ");
							else
								conditions.append("OR ");
							conditions
									.append("customer_vendorkeywords.KEYWORD LIKE '%"
											+ keywords[ctr].trim() + "%' ");
						}
						conditions.append(") ");
						break;

					case CONTACTFIRSTNAME:
						isMeetingInfo = true;
						break;

					case CONTACTLASTNAME:
						isMeetingInfo = true;
						break;

					case CONTACTDATE:
						isMeetingInfo = true;
						break;

					case CONTACTSTATE:
						isMeetingInfo = true;
						break;

					case CONTACTEVENT:
						isMeetingInfo = true;
						break;

					case BPSEGMENT:
						StringBuilder bpSegment = new StringBuilder();
						if (searchVendorForm.getBpSegmentIds() != null
								&& searchVendorForm.getBpSegmentIds().length != 0) {
							for (String segment : searchVendorForm
									.getBpSegmentIds()) {
								bpSegment.append("'" + segment + "',");
							}
							bpSegment.deleteCharAt(bpSegment.length() - 1);
							conditions
									.append(" and customer_vendormaster.BPSEGMENT in ("
											+ bpSegment.toString() + ")");
						}
						break;

					case VENDORCOMMODITY:
						isVendorCommodityInfo = true;
						break;

					case BPMARKETSECTOR:
						isVendorCommodityInfo = true;
						break;

					default:
						break;
					}
				}
			}
			if (flag) {
				sqlQuery.append(" left outer join customer_vendor_users "
						+ " on customer_vendormaster.ID=customer_vendor_users.VENDORID  ");

				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;
					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case FIRSTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.FIRSTNAME LIKE '%"
											+ searchFieldValue + "%'");
							break;
						case LASTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.LASTNAME LIKE '%"
											+ searchFieldValue + "%'");
							break;
						case DESIGNATION:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.DESIGNATION LIKE '%"
											+ searchFieldValue + "%'");
							break;
						case CONTACTPHONE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.PHONENUMBER = '"
											+ searchFieldValue + "'");
							break;
						case CONTACTMOBILE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.MOBILE = '"
											+ searchFieldValue + "'");
							break;
						case CONTACTFAX:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.FAX = '"
											+ searchFieldValue + "'");
							break;
						case CONTACTEMAIL:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" and customer_vendor_users.EMAILID LIKE '%"
											+ searchFieldValue + "%'");
							break;
						default:
							break;
						}
					}
				}
			}

			if (isMeetingInfo) {
				sqlQuery.append(" INNER JOIN customer_vendormeetinginfo ON customer_vendormeetinginfo.VENDORID = customer_vendormaster.id ");

				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;

					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case CONTACTFIRSTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTFIRSTNAME LIKE '%"
											+ searchFieldValue + "%' ");
							break;

						case CONTACTLASTNAME:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTLASTNAME LIKE '%"
											+ searchFieldValue + "%' ");
							break;

						case CONTACTDATE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTDATE BETWEEN '"
											+ CommonUtils
													.convertDateToDBFormat(searchFieldValue)
											+ " 00:00:00' AND '"
											+ CommonUtils
													.convertDateToDBFormat(searchFieldValue)
											+ " 23:59:59'");
							break;

						case CONTACTSTATE:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTSTATE = "
											+ searchFieldValue);
							break;

						case CONTACTEVENT:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							conditions
									.append(" AND customer_vendormeetinginfo.CONTACTEVENT LIKE '%"
											+ searchFieldValue + "%' ");
							break;

						default:
							break;
						}
					}
				}
			}

			if (isVendorCommodityInfo) {
				sqlQuery.append(" INNER JOIN customer_vendorcommodity cvcinfo ON cvcinfo.VENDORID = customer_vendormaster.ID ");

				index = 0;
				for (String option : searchVendorForm.getSearchFields()) {
					String searchFieldValue = null;
					if (searchVendorForm.getSearchFieldsValue() != null
							&& searchVendorForm.getSearchFieldsValue()[index] != null
							&& !searchVendorForm.getSearchFieldsValue()[index]
									.isEmpty()) {
						searchFieldValue = searchVendorForm
								.getSearchFieldsValue()[index];
					}
					index++;

					if (searchFieldValue != null) {
						switch (SearchFields.valueOf(option)) {
						case VENDORCOMMODITY:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							StringBuilder vendorCommodity = new StringBuilder();
							if (searchVendorForm.getVendorCommodity() != null
									&& searchVendorForm.getVendorCommodity().length != 0) {
								for (String commodity : searchVendorForm
										.getVendorCommodity()) {
									vendorCommodity.append("'" + commodity
											+ "',");
								}
								vendorCommodity.deleteCharAt(vendorCommodity
										.length() - 1);
								conditions
										.append(" AND cvcinfo.COMMODITYID IN ("
												+ vendorCommodity.toString()
												+ ")");
							}
							break;

						case BPMARKETSECTOR:
							if (!(whereConditionTableList.contains(SearchFields
									.valueOf(option).getFromTable1().trim()))) {
								whereConditionTableList
										.add(SearchFields.valueOf(option)
												.getFromTable1().trim());
							}
							StringBuilder bpMarketSector = new StringBuilder();
							if (searchVendorForm.getBpMarketSectorId() != null
									&& searchVendorForm.getBpMarketSectorId().length != 0) {
								for (String marketSector : searchVendorForm
										.getBpMarketSectorId()) {
									bpMarketSector.append("'" + marketSector
											+ "',");
								}
								bpMarketSector.deleteCharAt(bpMarketSector
										.length() - 1);
								sqlQuery.append(" INNER JOIN customer_commodity ccinfo ON ccinfo.ID = cvcinfo.COMMODITYID "
										+ "INNER JOIN customer_commoditycategory cccinfo ON cccinfo.ID = ccinfo.COMMODITYCATEGORYID "
										+ "INNER JOIN customer_marketsector cmsinfo ON cmsinfo.ID = cccinfo.MARKETSECTORID ");
								conditions.append(" AND cmsinfo.ID IN ("
										+ bpMarketSector.toString() + ")");
							}
							break;

						default:
							break;
						}
					}
				}
			}

			searchVendorForm.setSearchConditions(conditions.toString());
			searchVendorForm.setFromtableForConditions(whereConditionTableList);
			sqlQuery.append(" where 1=1 ").append(conditions);

			// Its needed only when International Mode = 0, with default country
			// selected.
			if (appDetails.getWorkflowConfiguration() != null) {
				if (appDetails.getWorkflowConfiguration()
						.getInternationalMode() != null) {
					if (appDetails.getWorkflowConfiguration()
							.getInternationalMode() == 0) {
						List<Country> country = (List<Country>) session
								.createQuery("From Country where isDefault=1")
								.list();
						if (country.get(0).getId() != null) {
							sqlQuery.append(" AND customer_vendoraddressmaster.COUNTRY = "
									+ country.get(0).getId() + " ");
						}
					}
				}
			}

			// Check isDivision is active & check the condition based on
			// customer division id
			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" and customer_vendormaster.DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append(" order by customer_vendormaster.vendorname");
			System.out
					.println("SQL Query for supp report (ReportDaoImpl @3474): "
							+ sqlQuery);
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List vendors = query.list();
			// Statement stmt = connection.createStatement();
			Iterator iterator;
			if (vendors != null) {
				iterator = vendors.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setVendorId((Integer) objects[0]);
					reportDto.setVendorName((String) objects[1]);
					reportDto.setOwnerName((String) objects[2]);
					// reportDto.setFirstName((String) objects[2]);
					// reportDto.setLastName((String) objects[3]);
					reportDto.setAddress1((String) objects[3]);
					reportDto.setCity((String) objects[4]);
					reportDto.setState((String) objects[5]);
					reportDto.setZip((String) objects[6]);
					reportDto.setPhone((String) objects[7]);
					reportDto.setFax((String) objects[8]);
					reportDto.setEmailId((String) objects[9]);

					if (((String) objects[10]) != null
							&& !((String) objects[10]).isEmpty()) {
						reportDto.setNaicsCapabilities(((String) objects[10])
								.replace("|", " "));
					} else
						reportDto.setNaicsCapabilities("");

					if (((String) objects[11]) != null
							&& !((String) objects[11]).isEmpty()) {
						reportDto
								.setDiverseClassification(((String) objects[11])
										.replace("|", " "));
					} else
						reportDto.setDiverseClassification("");

					if (((String) objects[12]) != null
							&& !((String) objects[12]).isEmpty()) {
						reportDto
								.setCertificateAgencyDetails(((String) objects[12])
										.replace("|", " "));
					} else
						reportDto.setCertificateAgencyDetails("");

					/*
					 * if (((String) objects[12]) != null && !((String)
					 * objects[12]).isEmpty()) { reportDto.setEffDate((String)
					 * objects[12]); } else { reportDto.setEffDate(" "); } if
					 * ((String) objects[13] != null && !((String)
					 * objects[13]).isEmpty()) { reportDto.setExpDate((String)
					 * objects[13]); } else { reportDto.setExpDate(" "); }
					 * reportDto.setCapablities((String) objects[14]);
					 * reportDto.setNaicsDesc((String) objects[15]);
					 */

					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getSupplierStateDashboardReport(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT state.STATENAME, COUNT(*)statecnt,round(100*(count(*)/(SELECT COUNT(*) FROM customer_vendormaster v,"
					+ " customer_vendoraddressmaster a, country c, state s where v.id=a.VENDORID AND a.ADDRESSTYPE='P'");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and v.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" AND a.ISACTIVE = 1 AND v.ISACTIVE=1 AND v.ISPARTIALLYSUBMITTED='No' and a.state=s.id AND a.country=c.id");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND v.DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append(" AND (v.vendorstatus='A' OR v.vendorstatus='B'))),2) percent "
					+ " FROM customer_vendormaster,customer_vendoraddressmaster, country, state WHERE customer_vendormaster.id="
					+ " customer_vendoraddressmaster.VENDORID AND customer_vendoraddressmaster.ADDRESSTYPE='P' AND"
					+ " customer_vendoraddressmaster.state=state.id AND customer_vendoraddressmaster.country=country.id");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND customer_vendormaster.DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append(" AND ((vendorstatus='A' OR vendorstatus='B')) and "
					+ " customer_vendoraddressmaster.ISACTIVE = 1 AND customer_vendormaster.ISACTIVE=1 AND "
					+ " customer_vendormaster.ISPARTIALLYSUBMITTED='No'");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" GROUP BY state.STATENAME ");
			System.out.println("3.SUPPLIERDASHBOARD (ReportDaoImpl @3561):"
					+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setState(objects[0].toString());
					reportDto
							.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setDiversityPercent(objects[2].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getAgencyDashboardReport(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("select certagencyname, certagencyshortname, sum(direxpenseamt), ((sum(direxpenseamt)/total)*100)percent"
					+ " from customer_certificateagencymaster a1, customer_vendorcertificate b1, customer_tier2reportdirectexpenses c1, customer_vendormaster d1,"
					+ " (Select sum(direxpenseamt)as total from customer_certificateagencymaster a, customer_vendorcertificate b,"
					+ " customer_tier2reportdirectexpenses c, customer_vendormaster d where a.ID=b.CERTAGENCYID and "
					+ " d.id=b.VENDORID and b.CERTMASTERID=c.CertificateId");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and d.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(")temp where a1.ID=b1.CERTAGENCYID and b1.CERTMASTERID=c1.CertificateId and d1.id=b1.VENDORID");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and d1.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(" group by certagencyname");
			System.out.println("1.AGENCYDASHBOARD (ReportDaoImpl @3538):"
					+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setCertAgencyName(objects[0].toString());
					reportDto.setCertificateShortName(objects[1].toString());
					reportDto
							.setCount(Double.parseDouble(objects[2].toString()));
					reportDto.setDiversityPercent(objects[3].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Tier2ReportMaster> getReportMasterList(
			UserDetailsDto appDetails, VendorContact contact) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportMaster> reportMasters = null;

		try {
			session.beginTransaction();
			/* Get the list of active report master data */
			reportMasters = session.createQuery(
					"from Tier2ReportMaster Where isSubmitted=1 and vendorId="
							+ contact.getVendorId().getId()).list();
			session.getTransaction().commit();

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportMasters;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tier2ReportMaster retriveReportmaster(Integer id,
			UserDetailsDto appDetails) {
		Tier2ReportMaster reportMaster = null;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		try {
			session.beginTransaction();
			reportMaster = (Tier2ReportMaster) session
					.createCriteria(Tier2ReportMaster.class)
					.add(Restrictions.eq("tier2ReportMasterid", id))
					.uniqueResult();
			reportMaster.getTier2ReportDirectExpensesList().size();
			reportMaster.getTier2ReportIndirectExpensesList().size();
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Tier2ReportMaster updateTier2Report(Tier2ReportForm form,
			UserDetailsDto userDetails, VendorContact tire2Vendor,
			String submitType) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Tier2ReportMaster master = null;

		try {
			session.getTransaction().begin();

			master = (Tier2ReportMaster) session.get(Tier2ReportMaster.class,
					form.getTier2ReportMasterId());

			master.setReportingPeriod(form.getReportingPeriod());
			String period = form.getReportingPeriod();
			Date startDate = null;
			Date endDate = null;
			if (period != null && !period.isEmpty()) {
				SimpleDateFormat sdf = new SimpleDateFormat(NEW_FORMAT);
				String peString[] = period.split("-");
				startDate = new Date(sdf.parse(getReportingPeriod(peString[0]))
						.getTime());
				endDate = new Date(sdf.parse(getReportingPeriod(peString[1]))
						.getTime());
				master.setReportingStartDate(startDate);
				master.setReportingEndDate(endDate);
			}

			master.setVendorId(tire2Vendor.getVendorId());

			master.setTotalSales(Double.valueOf(form
					.getTotalUsSalesSelectedQtr()));
			master.setTotalSalesToCompany(Double.valueOf(form
					.getTotalUsSalestoSelectedQtr()));
			master.setIndirectAllocationPercentage(Float.valueOf(form
					.getIndirectPer()));
			master.setComments(form.getComments());
			int isSubmitted = 0;
			if (submitType.equalsIgnoreCase("submit")) {
				isSubmitted = 1;
				updateReportDue(startDate, endDate, userDetails, tire2Vendor);

			} else if (submitType.equalsIgnoreCase("save")) {
				isSubmitted = 0;
			}
			master.setIsSubmitted(isSubmitted);
			master.setModifiedBy(tire2Vendor.getId());
			master.setModifiedOn(new Date());
			session.merge(master);
			int i = 0;
			List<Tier2ReportDirectExpenses> directExpensesList = null;
			// Direct saving and deleting starting form here

			if (form.getDirectAmt() != null && form.getDirectAmt().length != 0) {
				directExpensesList = session
						.createQuery(
								" from Tier2ReportDirectExpenses where tier2ReoprtMasterid=:reportMasterId")
						.setParameter("reportMasterId", master).list();
				int directFromUI = form.getDirectAmt().length;
				int directFromDB = directExpensesList != null
						&& directExpensesList.size() != 0 ? directExpensesList
						.size() : 0;
				if (directFromUI <= directFromDB) {
					for (i = 0; i < directFromDB; i++) {
						if (i < directFromUI) {

							String amt = form.getDirectAmt()[i];
							if (amt != null && !amt.isEmpty()) {

								Tier2ReportDirectExpenses directExpenses = (Tier2ReportDirectExpenses) session
										.get(Tier2ReportDirectExpenses.class,
												directExpensesList
														.get(i)
														.getTier2ReportDirExpId());
								directExpenses.setDirExpenseAmt(Double
										.valueOf(amt));
								if (form.getTier2vendorName() != null
										&& form.getTier2vendorName().length > i) {

									VendorMaster tier2Vendorid = (VendorMaster) session
											.get(VendorMaster.class,
													Integer.valueOf(form
															.getTier2vendorName()[i]));
									directExpenses
											.setTier2Vendorid(tier2Vendorid);
								}
								if (form.getCertificateDrt() != null
										&& form.getCertificateDrt().length > i) {

									Certificate certificate = (Certificate) session
											.get(Certificate.class,
													Integer.valueOf(form
															.getCertificateDrt()[i]));
									directExpenses
											.setCertificateId(certificate);
								}
								directExpenses.setTier2ReoprtMasterid(master);
								directExpenses.setModifiedBy(tire2Vendor
										.getId());
								directExpenses.setModifiedOn(new Date());
								MarketSector marketSector = null;
								CustomerCommodityCategory marketSubSector =null;
								if (form.getMarketSectorDrt() != null
										&& !"".equalsIgnoreCase(form
												.getMarketSectorDrt()[i])) {
									String ssDate=Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
								    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(ssDate); 
								 if(directExpenses.getCreatedOn().after(date)){
										marketSubSector = (CustomerCommodityCategory) session.get(
												CustomerCommodityCategory.class,
												Integer.valueOf(form
														.getMarketSectorDrt()[i]));

										directExpenses.setMarketSubSector(marketSubSector);
										
									}else{
									marketSector = (MarketSector) session.get(
											MarketSector.class,
											Integer.valueOf(form
													.getMarketSectorDrt()[i]));

									directExpenses
											.setMarketSector(marketSector);
									}
								}
								directExpenses.setMarketSubSector(marketSubSector);
								directExpenses.setMarketSector(marketSector);
								Ethnicity ethnicity = null;
								if (form.getEthnicityDrt() != null
										&& !"".equalsIgnoreCase(form
												.getEthnicityDrt()[i])) {
									ethnicity = (Ethnicity) session.get(
											Ethnicity.class,
											Integer.valueOf(form
													.getEthnicityDrt()[i]));

									directExpenses.setEthnicityId(ethnicity);
								}

								session.merge(directExpenses);
							}
						} else {

							Tier2ReportDirectExpenses directExpenses = (Tier2ReportDirectExpenses) session
									.get(Tier2ReportDirectExpenses.class,
											directExpensesList.get(i)
													.getTier2ReportDirExpId());
							session.delete(directExpenses);
						}
					}

				} else {
					for (i = 0; i < directFromUI; i++) {
						String amount = form.getDirectAmt()[i];
						if (amount != null && !amount.isEmpty()) {
							if (i < directFromDB) {

								Tier2ReportDirectExpenses directExpenses = (Tier2ReportDirectExpenses) session
										.get(Tier2ReportDirectExpenses.class,
												directExpensesList
														.get(i)
														.getTier2ReportDirExpId());
								directExpenses.setDirExpenseAmt(Double
										.valueOf(amount));
								if (form.getTier2vendorName() != null
										&& form.getTier2vendorName().length > i) {

									VendorMaster tier2Vendorid = (VendorMaster) session
											.get(VendorMaster.class,
													Integer.valueOf(form
															.getTier2vendorName()[i]));
									directExpenses
											.setTier2Vendorid(tier2Vendorid);
								}
								if (form.getCertificateDrt() != null
										&& form.getCertificateDrt().length > i) {

									Certificate certificate = (Certificate) session
											.get(Certificate.class,
													Integer.valueOf(form
															.getCertificateDrt()[i]));
									directExpenses
											.setCertificateId(certificate);
								}
								directExpenses.setTier2ReoprtMasterid(master);
								directExpenses.setModifiedBy(tire2Vendor
										.getId());
								directExpenses.setModifiedOn(new Date());
								session.merge(directExpenses);
							} else {

								Tier2ReportDirectExpenses directExpenses = new Tier2ReportDirectExpenses();
								directExpenses.setDirExpenseAmt(Double
										.valueOf(amount));
								if (form.getTier2vendorName() != null
										&& form.getTier2vendorName().length > i) {

									VendorMaster tier2Vendorid = (VendorMaster) session
											.get(VendorMaster.class,
													Integer.valueOf(form
															.getTier2vendorName()[i]));
									directExpenses
											.setTier2Vendorid(tier2Vendorid);
								}
								if (form.getCertificateDrt() != null
										&& form.getCertificateDrt().length > i) {

									Certificate certificate = (Certificate) session
											.get(Certificate.class,
													Integer.valueOf(form
															.getCertificateDrt()[i]));
									directExpenses
											.setCertificateId(certificate);
								}
								directExpenses.setTier2ReoprtMasterid(master);
								directExpenses
										.setCreatedBy(tire2Vendor.getId());
								directExpenses.setCreatedOn(new Date());
								MarketSector marketSector = null;
								CustomerCommodityCategory marketSubSector =null;
								if (form.getMarketSectorDrt() != null
										&& !"".equalsIgnoreCase(form
												.getMarketSectorDrt()[i])) {
									String ssDate=Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
								    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(ssDate); 
								 if(directExpenses.getCreatedOn().after(date)){
										marketSubSector = (CustomerCommodityCategory) session.get(
												CustomerCommodityCategory.class,
												Integer.valueOf(form
														.getMarketSectorDrt()[i]));

										directExpenses.setMarketSubSector(marketSubSector);
										
									}else{
									marketSector = (MarketSector) session.get(
											MarketSector.class,
											Integer.valueOf(form
													.getMarketSectorDrt()[i]));

									directExpenses
											.setMarketSector(marketSector);
									}
								}
								directExpenses.setMarketSubSector(marketSubSector);
								directExpenses.setMarketSector(marketSector);
								Ethnicity ethnicity = null;
								if (form.getEthnicityDrt() != null
										&& !"".equalsIgnoreCase(form
												.getEthnicityDrt()[i])) {
									ethnicity = (Ethnicity) session.get(
											Ethnicity.class,
											Integer.valueOf(form
													.getEthnicityDrt()[i]));

									directExpenses.setEthnicityId(ethnicity);
								}
								session.save(directExpenses);

							}
						}
					}
				}
			}
			// int index = 0;
			// int ecount = 0;
			// Indirect saving and deleting starting form here
			if (form.getCertificate() != null
					&& form.getCertificate().length != 0) {

				List<Tier2ReportIndirectExpenses> indirectExpensesList = session
						.createQuery(
								" from Tier2ReportIndirectExpenses where tier2ReoprtMasterid=:reportMasterId")
						.setParameter("reportMasterId", master).list();

				int certificateFromUI = form.getCertificate().length;
				int certificateFromDB = indirectExpensesList != null
						&& indirectExpensesList.size() != 0 ? indirectExpensesList
						.size() : 0;

				if (certificateFromUI <= certificateFromDB) {
					for (i = 0; i < certificateFromDB; i++) {
						if (i < certificateFromUI) {

							String cert = form.getCertificate()[i];
							if (cert != null && !cert.isEmpty()) {

								Tier2ReportIndirectExpenses indirectExpenses = (Tier2ReportIndirectExpenses) session
										.get(Tier2ReportIndirectExpenses.class,
												indirectExpensesList
														.get(i)
														.getTier2ReportInDirExpId());
								indirectExpenses.setIndirectExpenseAmt((Double
										.valueOf(form.getIndirectAmt()[i])));
								if (form.getTier2indirectvendorName() != null
										&& form.getTier2indirectvendorName().length > i) {

									VendorMaster tier2IndirectVendorid = (VendorMaster) session
											.get(VendorMaster.class,
													Integer.valueOf(form
															.getTier2indirectvendorName()[i]));
									indirectExpenses
											.setTier2IndirectVendorId(tier2IndirectVendorid);
								}

								MarketSector marketSector = null;
								CustomerCommodityCategory marketSubSector =null;
								if (form.getMarketSector() != null
										&& !"".equalsIgnoreCase(form
												.getMarketSector()[i])) {
									
									
									String ssDate=Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
								    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(ssDate); 
								    if(indirectExpenses.getCreatedOn()==null){
										   marketSubSector = (CustomerCommodityCategory) session.get(
													CustomerCommodityCategory.class,
													Integer.valueOf(form
															.getMarketSector()[i]));

											indirectExpenses.setMarketSubSector(marketSubSector);
									   }
								    else if(indirectExpenses.getCreatedOn().after(date)){
										marketSubSector = (CustomerCommodityCategory) session.get(
												CustomerCommodityCategory.class,
												Integer.valueOf(form
														.getMarketSector()[i]));

										indirectExpenses.setMarketSubSector(marketSubSector);
										
									}else{
									marketSector = (MarketSector) session.get(
											MarketSector.class,
											Integer.valueOf(form
													.getMarketSector()[i]));

									indirectExpenses
											.setMarketSector(marketSector);
									}
								}
								indirectExpenses.setMarketSector(marketSector);
								indirectExpenses.setMarketSubSector(marketSubSector);
								
								Certificate certificate = (Certificate) session
										.get(Certificate.class,
												Integer.valueOf(cert));
								Ethnicity ethnicity = null;
								// if ("Women Owned Business"
								// .equalsIgnoreCase(certificate
								// .getCertificateName())
								// || "Women Business Enterprise"
								// .equalsIgnoreCase(certificate
								// .getCertificateName())) {
								// ethnicity = (Ethnicity) session
								// .createQuery(
								// "from Ethnicity e where e.ethnicity='Caucasian'")
								// .list().get(0);
								//
								// indirectExpenses.setEthnicityId(ethnicity);
								// } else
								if (form.getEthnicity() != null
										&& !"".equalsIgnoreCase(form
												.getEthnicity()[i])) {
									ethnicity = (Ethnicity) session
											.get(Ethnicity.class, Integer
													.valueOf(form
															.getEthnicity()[i]));
									indirectExpenses.setEthnicityId(ethnicity);
								}

								indirectExpenses.setCertificateId(certificate);
								indirectExpenses.setTier2ReoprtMasterid(master);
								indirectExpenses.setModifiedBy(tire2Vendor
										.getId());
								indirectExpenses.setModifiedOn(new Date());
								session.merge(indirectExpenses);
							}
						} else {

							Tier2ReportIndirectExpenses indirectExpenses = (Tier2ReportIndirectExpenses) session
									.get(Tier2ReportIndirectExpenses.class,
											indirectExpensesList.get(i)
													.getTier2ReportInDirExpId());
							session.delete(indirectExpenses);
						}
					}

				} else {
					for (i = 0; i < certificateFromUI; i++) {
						String cert = form.getCertificate()[i];
						if (cert != null && !cert.isEmpty()) {
							if (i < certificateFromDB) {
								Tier2ReportIndirectExpenses indirectExpenses = (Tier2ReportIndirectExpenses) session
										.get(Tier2ReportIndirectExpenses.class,
												indirectExpensesList
														.get(i)
														.getTier2ReportInDirExpId());
								indirectExpenses.setIndirectExpenseAmt((Double
										.valueOf(form.getIndirectAmt()[i])));
								if (form.getTier2indirectvendorName() != null
										&& form.getTier2indirectvendorName().length > i) {

									VendorMaster tier2IndirectVendorid = (VendorMaster) session
											.get(VendorMaster.class,
													Integer.valueOf(form
															.getTier2indirectvendorName()[i]));
									indirectExpenses
											.setTier2IndirectVendorId(tier2IndirectVendorid);
								}
								Certificate certificate = (Certificate) session
										.get(Certificate.class,
												Integer.valueOf(cert));
								Ethnicity ethnicity = null;
								if (form.getEthnicity() != null
										&& !"".equalsIgnoreCase(form
												.getEthnicity()[i])) {
									ethnicity = (Ethnicity) session
											.get(Ethnicity.class, Integer
													.valueOf(form
															.getEthnicity()[i]));

									indirectExpenses.setEthnicityId(ethnicity);
								}

								indirectExpenses.setCertificateId(certificate);
								indirectExpenses.setTier2ReoprtMasterid(master);
								indirectExpenses.setModifiedBy(tire2Vendor
										.getId());
								indirectExpenses.setModifiedOn(new Date());
								session.merge(indirectExpenses);

							} else {

								Tier2ReportIndirectExpenses indirectExpenses = new Tier2ReportIndirectExpenses();
								indirectExpenses.setIndirectExpenseAmt((Double
										.valueOf(form.getIndirectAmt()[i])));
								if (form.getTier2indirectvendorName() != null
										&& form.getTier2indirectvendorName().length > i) {

									VendorMaster tier2IndirectVendorid = (VendorMaster) session
											.get(VendorMaster.class,
													Integer.valueOf(form
															.getTier2indirectvendorName()[i]));
									indirectExpenses
											.setTier2IndirectVendorId(tier2IndirectVendorid);
								}
								Certificate certificate = (Certificate) session
										.get(Certificate.class,
												Integer.valueOf(cert));
								Ethnicity ethnicity = null;
								if (form.getEthnicity() != null

								&& !"".equalsIgnoreCase(form.getEthnicity()[i])) {
									ethnicity = (Ethnicity) session
											.get(Ethnicity.class, Integer
													.valueOf(form
															.getEthnicity()[i]));
									indirectExpenses.setEthnicityId(ethnicity);
								}
								MarketSector marketSector = null;
								CustomerCommodityCategory marketSubSector =null;
								if (form.getMarketSector() != null
										&& !"".equalsIgnoreCase(form
												.getMarketSector()[i])) {
									
									
									String ssDate=Constants.getString(Constants.SECTOR_SUBSECTOR_DATE);
								    Date date=new SimpleDateFormat("yyyy-MM-dd").parse(ssDate); 
								   if(indirectExpenses.getCreatedOn()==null){
									   marketSubSector = (CustomerCommodityCategory) session.get(
												CustomerCommodityCategory.class,
												Integer.valueOf(form
														.getMarketSector()[i]));

										indirectExpenses.setMarketSubSector(marketSubSector);
								   }
								   else if(indirectExpenses.getCreatedOn().after(date)){
										marketSubSector = (CustomerCommodityCategory) session.get(
												CustomerCommodityCategory.class,
												Integer.valueOf(form
														.getMarketSector()[i]));

										indirectExpenses.setMarketSubSector(marketSubSector);
										
									}else{
									marketSector = (MarketSector) session.get(
											MarketSector.class,
											Integer.valueOf(form
													.getMarketSector()[i]));

									indirectExpenses
											.setMarketSector(marketSector);
									}
								}
								indirectExpenses.setMarketSector(marketSector);
								indirectExpenses.setMarketSubSector(marketSubSector);

								indirectExpenses.setCertificateId(certificate);
								indirectExpenses.setTier2ReoprtMasterid(master);
								indirectExpenses.setCreatedBy(tire2Vendor
										.getId());
								indirectExpenses.setCreatedOn(new Date());
								session.save(indirectExpenses);
							}
						}
					}
				}

			}
			session.getTransaction().commit();
			// return true;
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			// return false;
		} catch (ParseException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		// return null;
		return master;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DashboardSettings> listDashboardSettings(
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<DashboardSettings> dashboardSettings = null;
		try {
			session.beginTransaction();

			/* Get the list of active workflow configuration list. */
			dashboardSettings = session.createQuery(
					"from DashboardSettings where isDashboard=1").list();
			/*
			 * if (dashboardSettings != null && dashboardSettings.size() != 0) {
			 * settings = dashboardSettings.get(0); }
			 */
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return dashboardSettings;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createDashboardSettings(Tier2ReportForm reportForm,
			Integer userId, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		String result = "";
		try {
			session.beginTransaction();

			DashboardSettings settings = new DashboardSettings();

			/* Save the dashboard settings. */

			if (reportForm.getAgencyDashboard().equalsIgnoreCase("on")) {
				settings.setIsDashboard("1");
			} else {
				settings.setIsDashboard("0");
			}

			// if (reportForm.getDiversityDashboard().equalsIgnoreCase("on")) {
			// settings.setDiversityReport("1");
			// } else {
			// settings.setDiversityReport("0");
			// }
			// if (reportForm.getEthnicityDashboard().equalsIgnoreCase("on")) {
			// settings.setEthnicityReport("1");
			// } else {
			// settings.setEthnicityReport("0");
			// }
			// if (reportForm.getSupplierDashboard().equalsIgnoreCase("on")) {
			// settings.setSupplierStateReport("1");
			// } else {
			// settings.setSupplierStateReport("0");
			// }

			if (reportForm.getDashboardId() != null
					&& reportForm.getDashboardId() != 0) {
				session.update(settings);
			} else {
				session.save(settings);
			}

			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateDashboardSettings(Tier2ReportForm reportForm,
			Integer userId, UserDetailsDto userDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		String result = "";
		DashboardSettings agencySettings = null;
		DashboardSettings diversitySettings = null;
		DashboardSettings ethnicitySettings = null;
		DashboardSettings supplierSettings = null;
		DashboardSettings totalSpendDbSettings = null;
		DashboardSettings directDbSettings = null;
		DashboardSettings indirectDbSettings = null;
		DashboardSettings diversityDbSettings = null;
		DashboardSettings ethnicityDbSettings = null;
		DashboardSettings spendDbSettings = null;
		DashboardSettings statusSettings = null;
		DashboardSettings bpVendorsCountSettings = null;

		try {
			session.beginTransaction();

			/* Save the dashboard settings. */

			if (reportForm.getAgencyDashboard() != null) {

				agencySettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Agency Dashboard%")).uniqueResult();
				if (reportForm.getAgencyDashboard().equalsIgnoreCase("on")
						&& !reportForm.getAgencyDashboard().isEmpty()) {
					agencySettings.setIsDashboard("1");
				} else {
					agencySettings.setIsDashboard("0");
				}
				session.update(agencySettings);
			}
			if (reportForm.getDiversityDashboard() != null) {
				diversitySettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Diversity Dashboard%")).uniqueResult();
				if (reportForm.getDiversityDashboard().equalsIgnoreCase("on")
						&& !reportForm.getDiversityDashboard().isEmpty()) {
					diversitySettings.setIsDashboard("1");
				} else {
					diversitySettings.setIsDashboard("0");
				}
				session.update(diversitySettings);
			}
			if (reportForm.getEthnicityDashboard() != null) {
				ethnicitySettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Ethnicity Dashboard%")).uniqueResult();
				if (reportForm.getEthnicityDashboard().equalsIgnoreCase("on")
						&& !reportForm.getEthnicityDashboard().isEmpty()) {
					ethnicitySettings.setIsDashboard("1");
				} else {
					ethnicitySettings.setIsDashboard("0");
				}
				session.update(ethnicitySettings);
			}
			if (reportForm.getSupplierDashboard() != null) {
				supplierSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Supplier Dashboard%")).uniqueResult();
				if (reportForm.getSupplierDashboard().equalsIgnoreCase("on")
						&& !reportForm.getSupplierDashboard().isEmpty()) {
					supplierSettings.setIsDashboard("1");
				} else {
					supplierSettings.setIsDashboard("0");
				}
				session.update(supplierSettings);
			}
			if (reportForm.getTotalSpend() != null) {
				totalSpendDbSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName", "%Total Spend%"))
						.uniqueResult();
				if (reportForm.getTotalSpend().equalsIgnoreCase("on")
						&& !reportForm.getTotalSpend().isEmpty()) {
					totalSpendDbSettings.setIsDashboard("1");
				} else {
					totalSpendDbSettings.setIsDashboard("0");
				}
				session.update(totalSpendDbSettings);
			}
			if (reportForm.getDirectSpend() != null) {
				directDbSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName", "%Directs Spend%"))
						.uniqueResult();
				if (reportForm.getDirectSpend().equalsIgnoreCase("on")
						&& !reportForm.getDirectSpend().isEmpty()) {
					directDbSettings.setIsDashboard("1");
				} else {
					directDbSettings.setIsDashboard("0");
				}
				session.update(directDbSettings);
			}
			if (reportForm.getIndirectSpend() != null) {
				indirectDbSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions
								.like("reportName", "%Indirect Spend%"))
						.uniqueResult();
				if (reportForm.getIndirectSpend().equalsIgnoreCase("on")
						&& !reportForm.getIndirectSpend().isEmpty()) {
					indirectDbSettings.setIsDashboard("1");
				} else {
					indirectDbSettings.setIsDashboard("0");
				}
				session.update(indirectDbSettings);
			}
			if (reportForm.getDiversityStatus() != null) {
				diversityDbSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Diversity Status%")).uniqueResult();
				if (reportForm.getDiversityStatus().equalsIgnoreCase("on")
						&& !reportForm.getDiversityStatus().isEmpty()) {
					diversityDbSettings.setIsDashboard("1");
				} else {
					diversityDbSettings.setIsDashboard("0");
				}
				session.update(diversityDbSettings);
			}
			if (reportForm.getEthnicityAnalysis() != null) {
				ethnicityDbSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Ethnicity Analysis%")).uniqueResult();
				if (reportForm.getEthnicityAnalysis().equalsIgnoreCase("on")
						&& !reportForm.getEthnicityAnalysis().isEmpty()) {
					ethnicityDbSettings.setIsDashboard("1");
				} else {
					ethnicityDbSettings.setIsDashboard("0");
				}
				session.update(ethnicityDbSettings);
			}
			if (reportForm.getSpendDashboard() != null) {
				spendDbSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Spend Dashboard%")).uniqueResult();
				if (reportForm.getSpendDashboard().equalsIgnoreCase("on")
						&& !reportForm.getSpendDashboard().isEmpty()) {
					spendDbSettings.setIsDashboard("1");
				} else {
					spendDbSettings.setIsDashboard("0");
				}
				session.update(spendDbSettings);
			}
			if (reportForm.getVendorStatus() != null) {
				statusSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%Status Dashboard%")).uniqueResult();
				if (reportForm.getVendorStatus().equalsIgnoreCase("on")
						&& !reportForm.getVendorStatus().isEmpty()) {
					statusSettings.setIsDashboard("1");
				} else {
					statusSettings.setIsDashboard("0");
				}
				session.update(statusSettings);
			}
			if (reportForm.getBpVendorsCountDashboard() != null) {
				bpVendorsCountSettings = (DashboardSettings) session
						.createCriteria(DashboardSettings.class)
						.add(Restrictions.like("reportName",
								"%BP Vendors Count%")).uniqueResult();
				if (reportForm.getBpVendorsCountDashboard().equalsIgnoreCase(
						"on")
						&& !reportForm.getBpVendorsCountDashboard().isEmpty()) {
					bpVendorsCountSettings.setIsDashboard("1");
				} else {
					bpVendorsCountSettings.setIsDashboard("0");
				}
				session.update(bpVendorsCountSettings);
			}
			session.getTransaction().commit();
			result = "success";

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;

	}

	private void updateReportDue(Date startDate, Date endDate,
			UserDetailsDto userDetails, VendorContact tire2Vendor) {
		// update the issubmited field in reportdue table
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String startDateStr = format.format(startDate);
		String endDateStr = format.format(endDate);

		CURDDao<ReportDue> curdDao = new CURDTemplateImpl<ReportDue>();
		String query = " from ReportDue where issubmitted='NO' AND vendorid="
				+ tire2Vendor.getVendorId().getId() + " AND reportstartdate='"
				+ startDateStr + "'AND reportenddate='" + endDateStr + "'";
		ReportDue due = curdDao.find(userDetails, query);

		if (due != null) {
			due.setIssubmitted("YES");
			due.setSubmittedby(tire2Vendor.getId());
			due.setSubmittedon(new Date());
			curdDao.update(due, userDetails);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getStatusDashboardReport(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("Select statusname, count(*),(round(concat(100*(count(*)/(Select count(*) from customer_vendormaster "
					+ "where customer_vendormaster.VENDORSTATUS IS NOT NULL and customer_vendormaster.VENDORSTATUS not in ('I','S') ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" and customer_vendormaster.DIVSERSUPPLIER=1 ");
			}

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" )),'%'),2)) percent from customer_vendormaster cv,statusmaster s Where s.id = cv.VENDORSTATUS "
					+ "and cv.VENDORSTATUS not in ('I','S') ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" and cv.DIVSERSUPPLIER=1 ");
			}

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and cv.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" group by VENDORSTATUS order by statusname ");
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			System.out.println("10.STATUSDASHBOARD (ReportDaoImpl @ 4542):"
					+ sqlQuery.toString());
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setStatusName(objects[0].toString());
					reportDto.setStatusCount(Double.parseDouble(objects[1]
							.toString()));
					reportDto.setStatusPercent(Double.parseDouble(objects[2]
							.toString()));
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> findReportingPeriod(UserDetailsDto appDetails,
			Integer year) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<String> list = null;
		try {
			session.getTransaction().begin();
			String query = " select distinct reportingPeriod from Tier2ReportMaster "
					+ "  where isSubmitted=1  and (year(reportingStartDate)="
					+ year
					+ " or year(reportingEndDate)="
					+ year
					+ ")"
					+ "   order by reportingStartDate";
			list = session.createQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;
	}

	@Override
	public List<String> findReportingPeriodForMultipleYears(
			UserDetailsDto appDetails, String year) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<String> list = null;
		try {
			session.getTransaction().begin();
			String query = " select distinct reportingPeriod from Tier2ReportMaster "
					+ "  where isSubmitted=1  and (year(reportingStartDate) IN "
					+ "("
					+ year
					+ ")"
					+ " or year(reportingEndDate) IN "
					+ "("
					+ year + ")" + ")" + "   order by reportingStartDate";
			list = session.createQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;
	}

	@Override
	public List<String> spendYear(UserDetailsDto appDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		List<String> list = null;
		try {
			session.getTransaction().begin();
			String query = " select distinct year(reportingStartDate)  from Tier2ReportMaster where isSubmitted=1"
					+ " union select distinct year(reportingEndDate)  from Tier2ReportMaster where isSubmitted=1 ";
			list = session.createQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tier2ReportDto> getBpVendorCountDashboardReport(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;
		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT state.STATENAME,customer_certificatemaster.CERTIFICATENAME, customer_certificatemaster.CERTIFICATESHORTNAME, COUNT(*)bpvendorcount, "
					+ " round(100*(count(*)/(SELECT COUNT(*) FROM customer_vendoraddressmaster a INNER JOIN state s ON a.STATE = s.ID INNER JOIN "
					+ " customer_vendordiverseclassifcation d ON a.VENDORID = d.VENDORID INNER JOIN customer_certificatemaster c ON d.CERTMASTERID = c.ID "
					+ " INNER JOIN customer_vendormaster v ON a.VENDORID =v.ID AND d.VENDORID = v.ID WHERE a.ADDRESSTYPE = 'P' ");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(" AND d.ACTIVE = 1 AND a.ISACTIVE = 1 AND v.ISACTIVE=1 AND v.ISPARTIALLYSUBMITTED='No' AND v.VENDORSTATUS='B' AND "
					+ " ((v.primenonprimevendor=0 OR v.primenonprimevendor=1)");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND v.DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append(" ))),2)percent FROM customer_vendoraddressmaster "
					+ " INNER JOIN state ON customer_vendoraddressmaster.STATE = state.ID INNER JOIN customer_vendordiverseclassifcation "
					+ " ON customer_vendoraddressmaster.VENDORID = customer_vendordiverseclassifcation.VENDORID INNER JOIN customer_certificatemaster "
					+ " ON customer_vendordiverseclassifcation.CERTMASTERID = customer_certificatemaster.ID INNER JOIN customer_vendormaster ON "
					+ " customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID AND customer_vendordiverseclassifcation.VENDORID = "
					+ " customer_vendormaster.ID WHERE customer_vendoraddressmaster.ADDRESSTYPE = 'P' AND customer_vendordiverseclassifcation.ACTIVE = 1 "
					+ " AND customer_vendoraddressmaster.ISACTIVE = 1 AND customer_vendormaster.ISACTIVE=1 AND customer_vendormaster.ISPARTIALLYSUBMITTED='No' "
					+ " AND customer_vendormaster.VENDORSTATUS='B' AND ((customer_vendormaster.primenonprimevendor=0 OR customer_vendormaster.primenonprimevendor=1) ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND customer_vendormaster.DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append(" ) ");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" and customer_vendormaster.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}
			sqlQuery.append(" GROUP BY state.STATENAME,customer_certificatemaster.CERTIFICATENAME, customer_certificatemaster.CERTIFICATESHORTNAME "
					+ " ORDER BY state.STATENAME,customer_certificatemaster.CERTIFICATENAME, customer_certificatemaster.CERTIFICATESHORTNAME");
			System.out
					.println("11.BPVENDORCOUNTDASHBOARD (ReportDaoImpl @4725):"
							+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setState(objects[0].toString());
					reportDto.setCertificateName(objects[1].toString());
					reportDto.setCertificateShortName(objects[2].toString());
					reportDto
							.setCount(Double.parseDouble(objects[3].toString()));
					reportDto.setDiversityPercent(objects[4].toString());
					reportDtos.add(reportDto);
				}
			}

		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@Override
	public Tier2ReportMaster getPreviousQuarterDetails(
			UserDetailsDto appDetails, Integer vendorId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		Tier2ReportMaster reportMaster = null;

		try {
			session.beginTransaction();

			/*
			 * Get the previous quarter details
			 */
			reportMaster = (Tier2ReportMaster) session
					.createQuery(
							"From Tier2ReportMaster where isSubmitted=1 and "
									+ "vendorId=" + vendorId
									+ "  ORDER BY reportingStartDate DESC ")
					.setMaxResults(1).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportMaster;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getVendorCommoditiesNotSavedReport(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		// Old Query
		/*
		 * SELECT statusname,vcount,totalcount,
		 * ROUND((vcount/totalcount)*100,2)vpercentage FROM (SELECT
		 * statusmaster.statusname, COUNT(*)vcount, (SELECT COUNT(*) FROM
		 * customer_vendormaster WHERE CUSTOMERDIVISIONID = '1' AND
		 * (VENDORSTATUS='B' OR VENDORSTATUS='A') AND ISACTIVE = 1 AND
		 * DIVSERSUPPLIER=1 AND (ISPARTIALLYSUBMITTED='No' OR
		 * ISPARTIALLYSUBMITTED IS NULL))totalcount FROM
		 * customer_vendormaster,statusmaster WHERE
		 * statusmaster.id=customer_vendormaster.VENDORSTATUS AND
		 * customer_vendormaster.CUSTOMERDIVISIONID= '1' AND
		 * (customer_vendormaster.VENDORSTATUS='B' OR
		 * customer_vendormaster.VENDORSTATUS='A') AND
		 * (customer_vendormaster.ISPARTIALLYSUBMITTED='No' OR
		 * customer_vendormaster.ISPARTIALLYSUBMITTED IS NULL) AND
		 * customer_vendormaster.ISACTIVE = 1 AND
		 * customer_vendormaster.DIVSERSUPPLIER=1 AND customer_vendormaster.ID
		 * NOT IN (SELECT VENDORID FROM customer_vendorcommodity) GROUP BY
		 * statusmaster.statusname) AS t
		 */

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT ID, STATUSNAME, VENDORNAME, FIRSTNAME, LASTNAME, EMAILID, PHONENUMBER, CITY, "
					+ "STATENAME, totalcount, vcount, ROUND((vcount/totalcount)*100,2) AS PERCENTAGE FROM "
					+ "(SELECT cvm.ID, sm.statusname, IFNULL(cvm.VENDORNAME, '') AS VENDORNAME, IFNULL(cvu.FIRSTNAME, '') AS FIRSTNAME, "
					+ "IFNULL(cvu.LASTNAME, '') AS LASTNAME, IFNULL(cvu.EMAILID, '') AS EMAILID, IFNULL(cvu.PHONENUMBER, '') AS PHONENUMBER, "
					+ "IFNULL(cvm.CITY, '') AS CITY, IFNULL(st.STATENAME, '') AS STATENAME, (SELECT COUNT(*) FROM customer_vendormaster "
					+ "WHERE ");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") AND ");
				}
			}
			sqlQuery.append("VENDORSTATUS IN ('B','A','N','P') AND ISACTIVE = 1 ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append("AND (ISPARTIALLYSUBMITTED='No' OR ISPARTIALLYSUBMITTED IS NULL))totalcount, "
					+ "(SELECT COUNT(*) FROM customer_vendormaster AS cv WHERE (cv.VENDORSTATUS=cvm.VENDORSTATUS) ");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" AND (cv.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ")) ");
				}
			}

			sqlQuery.append("AND (cv.ISPARTIALLYSUBMITTED='No' OR cv.ISPARTIALLYSUBMITTED IS NULL) "
					+ "AND cv.ISACTIVE = 1 AND cv.DIVSERSUPPLIER=1 "
					+ "AND cv.ID NOT IN (SELECT VENDORID FROM customer_vendorcommodity))vcount FROM customer_vendormaster cvm "
					+ "INNER JOIN statusmaster sm ON cvm.VENDORSTATUS = sm.id "
					+ "LEFT OUTER JOIN customer_vendor_users cvu ON cvm.ID = cvu.VENDORID AND cvu.ISPRIMARYCONTACT=1 "
					+ "LEFT OUTER JOIN state st ON cvm.STATE = st.ID WHERE cvm.VENDORSTATUS IN ('B','A','N','P') "
					+ "AND cvm.ISACTIVE = 1 ");

			if (appDetails.getIsGlobalDivision() != 1) {
				sqlQuery.append(" AND cvm.DIVSERSUPPLIER=1 ");
			}

			sqlQuery.append("AND (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) "
					+ "AND cvm.ID NOT IN (SELECT VENDORID FROM customer_vendorcommodity) ");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("ORDER BY sm.statusname, cvm.VENDORNAME ASC) AS t");
			System.out
					.println("11.VendorCommoditiesNotSavedReport (ReportDaoImpl @ 4905):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setVendorId(Integer.parseInt(objects[0]
							.toString()));
					reportDto.setStatusName(objects[1].toString());
					reportDto.setVendorName(objects[2].toString());
					reportDto.setFirstName(objects[3].toString());
					reportDto.setLastName(objects[4].toString());
					reportDto.setEmailId(objects[5].toString());
					reportDto.setPhone(objects[6].toString());
					reportDto.setCity(objects[7].toString());
					reportDto.setState(objects[8].toString());
					reportDto
							.setCount(Double.parseDouble(objects[9].toString()));
					reportDto.setStatusCount(Double.parseDouble(objects[10]
							.toString()));
					reportDto.setStatusPercent(Double.parseDouble(objects[11]
							.toString()));
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getCertificateExpirationNotificationEmailReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		String startDate = CommonUtils.convertDateToDBFormat(reportForm
				.getStartDate());
		String endDate = CommonUtils.convertDateToDBFormat(reportForm
				.getEndDate());

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT IFNULL(vm.VENDORNAME, '') AS VENDORNAME, cen.EMAILID, "
					+ "(DATE_FORMAT(cen.EMAILDATE, '%m-%d-%Y')) AS EMAILDATE, cen.CERTIFICATENUMBER, "
					+ "IFNULL(cen.CERTIFICATENAME, '') AS CERTIFICATENAME, IFNULL(cen.AGENCYNAME, '') AS AGENCYNAME, "
					+ "(DATE_FORMAT(cen.EXPIRATIONDATE, '%m-%d-%Y')) AS EXPIRATIONDATE, IFNULL(cen.STATUS, '') AS ISRENEWED "
					+ "FROM certificateexpirationnotification cen, customer_vendormaster vm "
					+ "WHERE cen.VENDORID = vm.ID "
					+ "AND cen.EMAILDATE BETWEEN '"
					+ startDate
					+ "' AND '"
					+ endDate + "' ");

			if (reportForm.getIsRenewed() != null
					&& reportForm.getIsRenewed() != 0
					&& reportForm.getIsRenewed() != 1) {
				if (reportForm.getIsRenewed() == 2)
					sqlQuery.append("AND cen.STATUS = 'Yes' ");
				else if (reportForm.getIsRenewed() == 3)
					sqlQuery.append("AND cen.STATUS = 'No' ");
			}

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("AND vm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("AND vm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings() != null) {
				if (appDetails.getSettings().getIsDivision() != 0
						&& appDetails.getSettings().getIsDivision() != null) {
					if (appDetails.getCustomerDivisionIds() != null
							&& appDetails.getCustomerDivisionIds().length != 0
							&& appDetails.getIsGlobalDivision() == 0) {
						StringBuilder divisionIds = new StringBuilder();
						for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
							divisionIds.append(appDetails
									.getCustomerDivisionIds()[i]);
							divisionIds.append(",");
						}
						divisionIds.deleteCharAt(divisionIds.length() - 1);
						sqlQuery.append("AND vm.CUSTOMERDIVISIONID in("
								+ divisionIds.toString() + ") ");
					}
				}
			}

			sqlQuery.append("ORDER BY cen.EMAILDATE DESC");
			System.out
					.println("12.CertificateExpirationNotificationEmailReport (ReportDaoImpl @ 5023):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();
					reportDto.setVendorName(objects[0].toString());
					reportDto.setEmailId(objects[1].toString());
					reportDto.setEmailDate(objects[2].toString());
					if (objects[3] != null) {
						reportDto.setCertificateNumber(objects[3].toString());
					}
					reportDto.setCertificateName(objects[4].toString());
					reportDto.setCertAgencyName(objects[5].toString());
					reportDto.setExpDate(objects[6].toString());
					reportDto.setCertificateExpiryStatus(objects[7].toString());
					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getRegisteredVendorsReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		String startDate = CommonUtils.convertDateToDBFormat(reportForm
				.getStartDate());
		String endDate = CommonUtils.convertDateToDBFormat(reportForm
				.getEndDate());

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cvm.ID, IFNULL(cvm.VENDORNAME, '') AS VENDORNAME, IFNULL(cvu.FIRSTNAME, '') AS FIRSTNAME, "
					+ "IFNULL(cvu.LASTNAME, '') AS LASTNAME, IFNULL(cvu.EMAILID, '') AS EMAILID, IFNULL(cvu.PHONENUMBER, '') AS PHONENUMBER, "
					+ "DATE_FORMAT(cvm.CREATEDON, '%m-%d-%Y') CREATEDON, IFNULL(cvm.CITY, '') AS CITY, "
					+ "IFNULL(st.STATENAME, '') AS STATENAME, IFNULL(sm.statusname, '') AS VENDORSTATUS FROM customer_vendormaster cvm "
					+ "INNER JOIN statusmaster sm ON cvm.VENDORSTATUS = sm.id "
					+ "LEFT OUTER JOIN customer_vendor_users cvu ON cvm.ID = cvu.VENDORID AND cvu.ISPRIMARYCONTACT=1 "
					+ "LEFT OUTER JOIN state st ON cvm.STATE = st.ID "
					+ "WHERE (cvm.ISPARTIALLYSUBMITTED = 'NO' OR cvm.ISPARTIALLYSUBMITTED IS NULL) AND cvm.ISACTIVE = 1 "
					+ "AND cvm.VENDORSTATUS NOT IN ('S', 'I') "
					+ "AND cvm.CREATEDON BETWEEN '"
					+ startDate
					+ " 00:00:00' AND '" + endDate + " 23:59:59' ");

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" AND cvm.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("ORDER BY cvm.VENDORNAME");
			System.out
					.println("13.RegisteredVendorsReport (ReportDaoImpl @ 5075):"
							+ sqlQuery.toString());
			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setVendorId(Integer.parseInt(objects[0]
							.toString()));
					reportDto.setVendorName(objects[1].toString());
					reportDto.setFirstName(objects[2].toString());
					reportDto.setLastName(objects[3].toString());
					reportDto.setEmailId(objects[4].toString());
					reportDto.setPhone(objects[5].toString());
					reportDto.setCreatedOn(objects[6].toString());
					reportDto.setCity(objects[7].toString());
					reportDto.setState(objects[8].toString());
					reportDto.setStatusName(objects[9].toString());

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByIndustryReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cbt.TypeName, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendormaster cvm1 "
					+ "INNER JOIN customer_businesstype cbt1 ON cbt1.Id = cvm1.BUSINESSTYPE ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append(" AND cvm1.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendormaster cvm "
					+ "INNER JOIN customer_businesstype cbt ON cbt.Id = cvm.BUSINESSTYPE ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != 0
					&& appDetails.getSettings().getIsDivision() != null) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID in("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("GROUP BY cvm.BUSINESSTYPE ORDER BY VENDORCOUNT DESC");
			System.out
					.println("14.VendorsByBusinessTypeReport (ReportDaoImpl @ 5192:"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setBusinessTypeName(objects[0].toString());
					reportDto
							.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2]
							.toString()));

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByNaicsReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cnc.NAICSDESCRIPTION, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendornaics cvn1 "
					+ "INNER JOIN customer_naicscode cnc1 ON cnc1.NAICSID = cvn1.NAICSID "
					+ "INNER JOIN customer_vendormaster cvm1 ON cvm1.ID = cvn1.VENDORID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' FROM customer_vendornaics cvn "
					+ "INNER JOIN customer_naicscode cnc ON cnc.NAICSID = cvn.NAICSID "
					+ "INNER JOIN customer_vendormaster cvm ON cvm.ID = cvn.VENDORID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("GROUP BY cnc.NAICSDESCRIPTION ORDER BY cnc.NAICSDESCRIPTION ASC");
			System.out
					.println("15.VendorsByNaicsDescriptionReport (ReportDaoImpl @ 5384):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setNaicsDesc(objects[0].toString());
					reportDto
							.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2]
							.toString()));

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByBPMarketSectorReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cms.SECTORDESCRIPTION, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendorcommodity cvc1 "
					+ "INNER JOIN customer_commodity cc1 ON cc1.ID = cvc1.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc1 ON ccc1.ID = cc1.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms1 ON cms1.ID = ccc1.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm1 ON cvm1.ID = cvc1.VENDORID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' "
					+ "FROM customer_vendorcommodity cvc "
					+ "INNER JOIN customer_commodity cc ON cc.ID = cvc.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc ON ccc.ID = cc.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms ON cms.ID = ccc.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm ON cvm.ID = cvc.VENDORID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("GROUP BY cms.SECTORDESCRIPTION ORDER BY cms.SECTORDESCRIPTION ASC");
			System.out
					.println("16.VendorsByBPMarketSectorReport (ReportDaoImpl @ 5496):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			System.out.println(query);
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setMarketSector(objects[0].toString());
					reportDto
							.setCount(Double.parseDouble(objects[1].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[2]
							.toString()));

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByBPMarketSubsectorReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cms.SECTORDESCRIPTION,ipm.REPORTNAME, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendorcommodity cvc1 "
					+ "INNER JOIN customer_commodity cc1 ON cc1.ID = cvc1.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc1 ON ccc1.ID = cc1.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms1 ON cms1.ID = ccc1.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm1 ON cvm1.ID = cvc1.VENDORID "
					+ "INNER JOIN indirect_procurement_marketsectors ipm1 ON ipm1.MARKETSECTORID = cms1.ID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' "
					+ "FROM customer_vendorcommodity cvc "
					+ "INNER JOIN customer_commodity cc ON cc.ID = cvc.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc ON ccc.ID = cc.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms ON cms.ID = ccc.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm ON cvm.ID = cvc.VENDORID "
					+ "INNER JOIN indirect_procurement_marketsectors ipm ON ipm.MARKETSECTORID = cms.ID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("GROUP BY cms.SECTORDESCRIPTION, ccc.CATEGORYDESCRIPTION ORDER BY cms.SECTORDESCRIPTION, ccc.CATEGORYDESCRIPTION ASC");
			System.out
					.println("16.VendorsByBPMarketSectorReport (ReportDaoImpl @ 5496):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			System.out.println(query);
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setMarketSector(objects[0].toString());
					reportDto.setSubSector(objects[1].toString());
					reportDto
							.setCount(Double.parseDouble(objects[2].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[3]
							.toString()));

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> getVendorsByBPCommodityGroupReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cms.SECTORDESCRIPTION,ccc.CATEGORYDESCRIPTION,cc.COMMODITYDESCRIPTION, COUNT(*) AS VENDORCOUNT, "
					+ "(ROUND(CONCAT(100*(COUNT(*)/(SELECT COUNT(*) FROM customer_vendorcommodity cvc1 "
					+ "INNER JOIN customer_commodity cc1 ON cc1.ID = cvc1.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc1 ON ccc1.ID = cc1.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms1 ON cms1.ID = ccc1.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm1 ON cvm1.ID = cvc1.VENDORID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm1.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm1.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append(")),'%'),2)) AS 'PERCENTAGE' "
					+ "FROM customer_vendorcommodity cvc "
					+ "INNER JOIN customer_commodity cc ON cc.ID = cvc.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc ON ccc.ID = cc.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms ON cms.ID = ccc.MARKETSECTORID "
					+ "INNER JOIN customer_vendormaster cvm ON cvm.ID = cvc.VENDORID ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("WHERE cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN("
							+ divisionIds.toString() + ") ");
				}
			}

			sqlQuery.append("GROUP BY cms.SECTORDESCRIPTION, ccc.CATEGORYDESCRIPTION, cc.COMMODITYDESCRIPTION ORDER BY cms.SECTORDESCRIPTION, ccc.CATEGORYDESCRIPTION, cc.COMMODITYDESCRIPTION ASC");
			System.out
					.println("16.VendorsByBPCommodityGroupReport (ReportDaoImpl @ 5496):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			System.out.println(query);
			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setMarketSector(objects[0].toString());
					reportDto.setSubSector(objects[1].toString());
					reportDto.setCommodityDisecription(objects[2].toString());
					reportDto
							.setCount(Double.parseDouble(objects[3].toString()));
					reportDto.setPercentage(Double.parseDouble(objects[4]
							.toString()));

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportDtos;
	}

	@SuppressWarnings({ "deprecation" })
	@Override
	public List<ReportSearchFieldsDto> getReportSearchFields(
			UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<ReportSearchFieldsDto> searchReportFieldsList = new ArrayList<ReportSearchFieldsDto>();

		Iterator<?> iterator = null;
		try {
			session.beginTransaction();
			// searchReportFieldsList=session.createQuery("From CustomReportSearchFields").list();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("select Category,group_concat(id ,':',columntitle SEPARATOR ',') from avms_custom_report_field_selection group by Category");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());
			List<?> searchFields = query.list();
			if (searchFields != null) {
				iterator = searchFields.iterator();
				while (iterator.hasNext()) {
					ReportSearchFieldsDto dto = new ReportSearchFieldsDto();
					Object[] objects = (Object[]) iterator.next();
					if (objects[0] != null) {
						dto.setCategory(objects[0].toString());
					}
					if (objects[1] != null) {
						dto.setColumnTitle(Arrays.asList(objects[1].toString()
								.split(",")));
					}
					searchReportFieldsList.add(dto);
				}
			}
			session.close();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return searchReportFieldsList;
	}

	@Override
	public CustomReportSearchFields getSearchFields(UserDetailsDto userDetails,
			Integer id) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		CustomReportSearchFields reportSearchFields = null;
		try {
			session.beginTransaction();

			reportSearchFields = (CustomReportSearchFields) session
					.createCriteria(CustomReportSearchFields.class)
					.add(Restrictions.eq("id", id)).uniqueResult();

			session.close();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return reportSearchFields;
	}

	@Override
	public void getSearchReportByDynamicColumns(UserDetailsDto userDetails,
			SearchReportDto searchReportDto, String searchQuery) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Map<String, List<Object>> columnDetails = searchReportDto
				.getColumnDetails();
		List<String> columnHeader = searchReportDto.getColumnHeader();

		for (int colCtr = 0; colCtr < columnHeader.size(); colCtr++) {
			columnDetails
					.put(columnHeader.get(colCtr), new ArrayList<Object>());
		}
		try {
			session.beginTransaction();
			System.out.println("ReportDaoImpl @5143:" + searchQuery);
			List searchDetails = session.createSQLQuery(searchQuery).list();
			List<Object> colData = null;
			Iterator iterator;
			if (searchDetails != null) {
				for (Object data : searchDetails) {
					colData = new ArrayList<Object>();
					if (data instanceof Object[]) {
						Object[] objects = (Object[]) data;
						if (objects != null) {
							for (int ctr = 0; ctr < objects.length; ctr++) {
								columnDetails.get(columnHeader.get(ctr)).add(
										objects[ctr]);
							}
						}
					} else {
						columnDetails.get(columnHeader.get(0)).add(data);
					}
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			System.out.println(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Tier2ReportMaster saveTier2ReportMasterOnly(Tier2ReportForm form,
			UserDetailsDto userDetails, VendorContact vendor, String submitType) {
		// TODO Auto-generated method stub
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		Tier2ReportMaster master = null;
		List<Tier2ReportMaster> masterList = (List<Tier2ReportMaster>) session
				.createCriteria(Tier2ReportMaster.class)
				.add(Restrictions.eq("reportingPeriod",
						form.getReportingPeriod()))
				.add(Restrictions.eq("vendorId", vendor.getVendorId())).list();
		try {
			session.getTransaction().begin();
			master = new Tier2ReportMaster();
			master.setReportingPeriod(form.getReportingPeriod());
			String period = form.getReportingPeriod();
			Date startDate = null;
			Date endDate = null;
			if (period != null && !period.isEmpty()) {
				SimpleDateFormat sdf = new SimpleDateFormat(NEW_FORMAT);
				String peString[] = period.split("-");
				startDate = new Date(sdf.parse(getReportingPeriod(peString[0]))
						.getTime()); /*
									 * sdf.parse(getReportingPeriod(peString[0]))
									 * .getTime());
									 */
				endDate = new Date(sdf.parse(getReportingPeriod(peString[1]))
						.getTime());
				master.setReportingStartDate(startDate);
				master.setReportingEndDate(endDate);
			}

			master.setVendorId(vendor.getVendorId());

			master.setTotalSales(Double.valueOf(form
					.getTotalUsSalesSelectedQtr()));
			master.setTotalSalesToCompany(Double.valueOf(form
					.getTotalUsSalestoSelectedQtr()));
			master.setIndirectAllocationPercentage(Float.valueOf(form
					.getIndirectPer()));
			master.setComments(form.getComments());

			int isSubmitted = 0;
			if (submitType.equalsIgnoreCase("submit")) {
				isSubmitted = 1;
				updateReportDue(startDate, endDate, userDetails, vendor);
			} else if (submitType.equalsIgnoreCase("save")) {
				isSubmitted = 0;
			}
			master.setIsSubmitted(isSubmitted);
			master.setCreatedBy(vendor.getId());
			master.setCreatedOn(new Date());
			session.save(master);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			ex.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return master;
	}

	@Override
	public String saveIndirectExpenditure(Double indirectAmount,
			String ethnicityName, VendorMaster tier2IndirectVendorid,
			String certificateName, Tier2ReportMaster tier2Reportmaster,
			UserDetailsDto userDetails, VendorContact vendor) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = "success";

		try {
			session.getTransaction().begin();
			Tier2ReportIndirectExpenses indirectExpenses = new Tier2ReportIndirectExpenses();
			Ethnicity ethnicity = (Ethnicity) session
					.createCriteria(Ethnicity.class)
					.add(Restrictions.eq("ethnicity", ethnicityName))
					.uniqueResult();
			ethnicity = (Ethnicity) session.get(Ethnicity.class,
					ethnicity.getId());

			Certificate certificate = (Certificate) session
					.createCriteria(Certificate.class)
					.add(Restrictions.eq("certificateName", certificateName))
					.uniqueResult();
			certificate = (Certificate) session.get(Certificate.class,
					certificate.getId());

			indirectExpenses.setIndirectExpenseAmt(indirectAmount);
			indirectExpenses.setEthnicityId(ethnicity);
			indirectExpenses.setTier2IndirectVendorId(tier2IndirectVendorid);
			indirectExpenses.setCertificateId(certificate);
			indirectExpenses.setTier2ReoprtMasterid(tier2Reportmaster);
			indirectExpenses.setCreatedBy(vendor.getId());
			indirectExpenses.setCreatedOn(new Date());
			session.save(indirectExpenses);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "fail";
			ex.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String saveDirectExpenditure(Double directAmount,String ethnicityName,
			VendorMaster tier2DirectVendorid, String certificateName,
			Tier2ReportMaster tier2Reportmaster, UserDetailsDto userDetails,
			VendorContact vendor) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = "success";
		try {
			session.getTransaction().begin();
			Tier2ReportDirectExpenses directExpenses = new Tier2ReportDirectExpenses();
			Ethnicity ethnicity = (Ethnicity) session
					.createCriteria(Ethnicity.class)
					.add(Restrictions.eq("ethnicity", ethnicityName))
					.uniqueResult();
			Certificate certificate = (Certificate) session
					.createCriteria(Certificate.class)
					.add(Restrictions.eq("certificateName", certificateName))
					.uniqueResult();
			certificate = (Certificate) session.get(Certificate.class,
					certificate.getId());
			
			directExpenses.setDirExpenseAmt(directAmount);
			directExpenses.setTier2Vendorid(tier2DirectVendorid);
			directExpenses.setCertificateId(certificate);
			directExpenses.setTier2ReoprtMasterid(tier2Reportmaster);
			directExpenses.setCreatedBy(vendor.getId());
			directExpenses.setCreatedOn(new Date());
			if(ethnicity != null){
			directExpenses.setEthnicityId(ethnicity);
			}
			session.save(directExpenses);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "fail";
			ex.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@Override
	public String deleteUnsubmittedRecords(UserDetailsDto appDetails,
			Integer masterId) {
		// TODO Auto-generated method stub
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String result = "success";
		Tier2ReportMaster master = null;
		Tier2ReportDirectExpenses directExpenses = null;
		Tier2ReportIndirectExpenses indirectExpenses = null;

		try {
			session.getTransaction().begin();
			master = (Tier2ReportMaster) session.get(Tier2ReportMaster.class,
					masterId);
			String query = null;
			if (master != null) {
				directExpenses = (Tier2ReportDirectExpenses) session
						.createCriteria(Tier2ReportDirectExpenses.class).add(
								Restrictions.eq("tier2ReoprtMasterid", master));
				indirectExpenses = (Tier2ReportIndirectExpenses) session
						.createCriteria(Tier2ReportIndirectExpenses.class).add(
								Restrictions.eq("tier2ReoprtMasterid", master));
				/*
				 * Tier2ReportMaster master1 = (Tier2ReportMaster) session
				 * .createCriteria(Tier2ReportMaster.class)
				 * .add(Restrictions.eq("reportingPeriod", form
				 * .getReportingPeriod().toString()))
				 * .add(Restrictions.eq("vendorId", vendor.getVendorId()))
				 * .uniqueResult();
				 */
			}
			result = "success";
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			result = "fail";
			ex.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return null;
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public List<Tier2ReportDto> getSpendReportHistory(
			UserDetailsDto userDetails, VendorContact contact) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		List<Tier2ReportDto> tier2ReportDtoList = new ArrayList<Tier2ReportDto>();

		try {
			session.beginTransaction();
			/* Get the list of active report master data */
			String selectQuery = "SELECT customer_tier2reportmaster.Tier2ReportMasterid, customer_tier2reportmaster.IsSubmitted, "
					+ "IFNULL(customer_vendor_users.FIRSTNAME, '') AS createdby, "
					+ "DATE_FORMAT(customer_tier2reportmaster.reportingStartDate, '%Y') AS YEAR, customer_tier2reportmaster.reportingPeriod, "
					+ "(SELECT SUM(customer_tier2reportdirectexpenses.DirExpenseAmt) FROM customer_tier2reportdirectexpenses "
					+ "WHERE customer_tier2reportdirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)Direxpenseamt, "
					+ "(SELECT SUM(customer_tier2reportindirectexpenses.IndirectExpenseAmt) FROM customer_tier2reportindirectexpenses "
					+ "WHERE customer_tier2reportindirectexpenses.Tier2ReoprtMasterid = customer_tier2reportmaster.Tier2ReportMasterid)indirectexpenseamt, "
					+ "customer_tier2reportmaster.totalSales, customer_tier2reportmaster.totalSalesToCompany, "
					+ "DATE_FORMAT(customer_tier2reportmaster.CreatedOn, '%m-%d-%Y') CREATEDON "
					+ "FROM customer_tier2reportmaster "
					+ "INNER JOIN customer_vendormaster ON customer_vendormaster.ID = customer_tier2reportmaster.vendorId "
					+ "INNER JOIN customer_vendor_users ON customer_vendor_users.ID=customer_tier2reportmaster.CreatedBy "
					+ "WHERE customer_tier2reportmaster.vendorId = "
					+ contact.getVendorId().getId();

			System.out.println("Tier 2 Report History @ Report DaoImpl 5920:"
					+ selectQuery.toString());
			List historyList = session.createSQLQuery(selectQuery).list();
			Iterator iterator;

			if (historyList != null) {
				iterator = historyList.iterator();
				while (iterator.hasNext()) {
					Object[] reportHistory = (Object[]) iterator.next();
					Integer id;
					Byte isSubmitted;
					String vendorName;
					String year;
					String reportingPeriod;
					Double dirExpenseAmt;
					Double inDirExpenseAmt;
					Double totalSales;
					Double totalSalesToCompany;
					String createdOn;

					id = (Integer) reportHistory[0];
					isSubmitted = (Byte) reportHistory[1];
					;
					vendorName = (String) reportHistory[2];
					;
					year = (String) reportHistory[3];
					reportingPeriod = (String) reportHistory[4];
					dirExpenseAmt = (Double) reportHistory[5];
					inDirExpenseAmt = (Double) reportHistory[6];
					totalSales = (Double) reportHistory[7];
					totalSalesToCompany = (Double) reportHistory[8];
					createdOn = reportHistory[9].toString();
					Tier2ReportDto tierReportDto = new Tier2ReportDto(id,
							isSubmitted, vendorName, year, reportingPeriod,
							dirExpenseAmt, inDirExpenseAmt, totalSales,
							totalSalesToCompany, createdOn);
					tier2ReportDtoList.add(tierReportDto);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return tier2ReportDtoList;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> getIndirectProcurementCommoditiesReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT DISTINCT IFNULL(cvm.VENDORNAME, '') AS VENDORNAME, "
					+ "CASE cvm.VENDORSTATUS WHEN 'A' THEN 'Reviewed by Supplier Diversity' WHEN 'B' THEN 'BP Vendor' "
					+ "WHEN 'N' THEN 'New Registration' WHEN 'P' THEN 'Pending Review' END AS VENDORSTATUS, IFNULL(cbt.TypeName, '') AS BUSINESSTYPE, "
					+ "(SELECT cvs.YEARSALES FROM customer_vendor_sales cvs WHERE cvs.VENDORID = cvm.ID "
					+ "AND cvs.YEARNUMBER = (SELECT MAX(cvs1.YEARNUMBER) FROM customer_vendor_sales cvs1 WHERE cvs1.VENDORID = cvs.VENDORID) "
					+ "ORDER BY cvs.YEARNUMBER DESC LIMIT 1) AS ANNUALREVENUE, "
					+ "GROUP_CONCAT(cms.SECTORDESCRIPTION SEPARATOR ',') AS SECTORDESCRIPTION, "
					+ "GROUP_CONCAT(ccc.CATEGORYDESCRIPTION SEPARATOR ',') AS CATEGORYDESCRIPTION, "
					+ "(SELECT GROUP_CONCAT(certificatemaster.CERTIFICATENAME SEPARATOR ',') AS CERTIFICATENAME FROM customer_certificatemaster certificatemaster, "
					+ "customer_vendorcertificate cvcert WHERE cvcert.CERTMASTERID = certificatemaster.ID AND cvcert.VENDORID = cvm.ID ");

			if (!arrayToStringWithSingleQuote(
					reportForm.getCertificateTypeList()).equalsIgnoreCase("")
					&& reportForm.getCertificateTypeList().length != 0) {
				sqlQuery.append("AND cvcert.CERTIFICATETYPE IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getCertificateTypeList()) + ")");
			}

			sqlQuery.append(") DIVERSECLASSIFCATION, IFNULL(st.STATENAME, '') AS STATENAME, IFNULL(cvu.FIRSTNAME, '') AS FIRSTNAME, IFNULL(cvu.LASTNAME, '') AS LASTNAME, "
					+ "IFNULL(cvu.PHONENUMBER, '') AS PHONENUMBER, IFNULL(cvm.EMAILID, '') AS EMAILID, IFNULL(cvm.WEBSITE, '') AS WEBSITE "
					+ "FROM customer_vendormaster cvm "
					+ "LEFT OUTER JOIN customer_vendoraddressmaster cvam ON cvm.ID = cvam.VENDORID AND cvam.ADDRESSTYPE = 'p' "
					+ "LEFT OUTER JOIN state st ON st.ID=cvam.STATE "
					+ "LEFT OUTER JOIN customer_vendor_users cvu ON cvu.VENDORID = cvm.ID AND cvu.ISPRIMARYCONTACT=1 AND cvu.ISACTIVE=1 "
					+ "INNER JOIN customer_businesstype cbt ON cbt.Id = cvm.BUSINESSTYPE "
					+ "INNER JOIN customer_vendorcommodity cvc ON cvc.VENDORID = cvm.ID "
					+ "INNER JOIN customer_commodity cc ON cc.ID = cvc.COMMODITYID "
					+ "INNER JOIN customer_commoditycategory ccc ON ccc.ID = cc.COMMODITYCATEGORYID "
					+ "INNER JOIN customer_marketsector cms ON cms.ID = ccc.MARKETSECTORID ");

			if (!arrayToStringWithSingleQuote(
					reportForm.getCertificateTypeList()).equalsIgnoreCase("")
					&& reportForm.getCertificateTypeList().length != 0) {
				sqlQuery.append("INNER JOIN customer_vendorcertificate cvcert ON cvcert.VENDORID = cvm.ID "
						+ "AND cvcert.CERTIFICATETYPE IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getCertificateTypeList()) + ") ");
			}

			sqlQuery.append("WHERE cvm.ISACTIVE=1 AND cvm.DIVSERSUPPLIER=1 AND (cvm.PARENTVENDORID=0 OR cvm.PARENTVENDORID IS NULL) "
					+ "AND (cvm.ISPARTIALLYSUBMITTED='No' OR cvm.ISPARTIALLYSUBMITTED IS NULL) ");

			if (!arrayToStringWithSingleQuote(reportForm.getVendorStatusList())
					.equalsIgnoreCase("")
					&& reportForm.getVendorStatusList().length != 0) {
				sqlQuery.append("AND cvm.VENDORSTATUS IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getVendorStatusList()) + ") ");
			} else {
				sqlQuery.append("AND cvm.VENDORSTATUS IN ('N', 'P', 'A', 'B') ");
			}

			if (appDetails.getSettings().getIsDivision() != null
					&& appDetails.getSettings().getIsDivision() != 0) {
				if (appDetails.getCustomerDivisionIds() != null
						&& appDetails.getCustomerDivisionIds().length != 0
						&& appDetails.getIsGlobalDivision() == 0) {
					StringBuilder divisionIds = new StringBuilder();
					for (int i = 0; i < appDetails.getCustomerDivisionIds().length; i++) {
						divisionIds
								.append(appDetails.getCustomerDivisionIds()[i]);
						divisionIds.append(",");
					}
					divisionIds.deleteCharAt(divisionIds.length() - 1);
					sqlQuery.append("AND cvm.CUSTOMERDIVISIONID IN ("
							+ divisionIds.toString() + ") ");
				}
			}

			if (!arrayToStringWithSingleQuote(
					reportForm.getIndirectCommoditiesList()).equalsIgnoreCase(
					"")
					&& reportForm.getIndirectCommoditiesList().length != 0) {
				sqlQuery.append("AND cms.ID IN ("
						+ arrayToStringWithSingleQuote(reportForm
								.getIndirectCommoditiesList()) + ") ");
			}

			sqlQuery.append("GROUP BY cvm.VENDORNAME ORDER BY cvm.VENDORNAME ASC");

			System.out
					.println("17.BP Market Sector Search Report (ReportDaoImpl @ 6050):"
							+ sqlQuery.toString());

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> reports = query.list();
			if (reports != null) {
				iterator = reports.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setVendorName(objects[0].toString());
					reportDto.setStatusName(objects[1].toString());
					reportDto.setBusinessTypeName(objects[2].toString());

					if (objects[3] != null)
						reportDto.setRecentAnnaulRevenue(Long
								.parseLong(objects[3].toString()));
					else
						reportDto.setRecentAnnaulRevenue((long) 0);

					reportDto.setMarketSector(objects[4].toString());
					reportDto.setMarketSubSector(objects[5].toString());

					if (objects[6] != null)
						reportDto.setDiverseClassification(objects[6]
								.toString());

					reportDto.setState(objects[7].toString());
					reportDto.setFirstName(objects[8].toString());
					reportDto.setLastName(objects[9].toString());
					reportDto.setPhone(objects[10].toString());
					reportDto.setEmailId(objects[11].toString());
					reportDto.setWebsite(objects[12].toString());

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Tier2ReportDto> listIndirectProcurementCommodities(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();
			sqlQuery.append("SELECT cms.ID, cms.SECTORDESCRIPTION FROM customer_marketsector cms"
					+ " INNER JOIN indirect_procurement_marketsectors ipms ON ipms.MARKETSECTORID = cms.ID");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> marketSectors = query.list();
			if (marketSectors != null) {
				iterator = marketSectors.iterator();
				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					if (objects[0] != null)
						reportDto.setMarketSectorId(Integer.parseInt(objects[0]
								.toString()));

					if (objects[1] != null)
						reportDto.setMarketSector(objects[1].toString());

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@SuppressWarnings("deprecation")
	public String saveIndirectProcurementCommodityList(
			UserDetailsDto appDetails, Integer userId,
			Tier2ReportForm reportForm, String reportName) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		IndirectProcurementMarketsectors indirectProcurementMarketsectors = null;
		MarketSector marketSector = null;

		try {
			session.beginTransaction();
			session.createQuery(
					"delete IndirectProcurementMarketsectors where reportName = '"
							+ reportName + "'").executeUpdate();

			for (String indirectProcurementCommodity : reportForm
					.getIndirectCommoditiesList()) {
				marketSector = (MarketSector) session.get(MarketSector.class,
						Integer.parseInt(indirectProcurementCommodity));
				indirectProcurementMarketsectors = new IndirectProcurementMarketsectors();
				indirectProcurementMarketsectors
						.setMarketSectorId(marketSector);
				indirectProcurementMarketsectors.setReportName(reportName);
				indirectProcurementMarketsectors.setCreatedBy(userId);
				indirectProcurementMarketsectors.setCreatedOn(new Date());
				session.save(indirectProcurementMarketsectors);
			}
			session.getTransaction().commit();
			return "Success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "Failed";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public List<Tier2ReportDto> listIndirectProcurementCertificateTypes(
			UserDetailsDto appDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		List<Tier2ReportDto> reportDtos = new ArrayList<Tier2ReportDto>();
		Iterator<?> iterator = null;

		try {
			session.getTransaction().begin();
			StringBuilder sqlQuery = new StringBuilder();

			sqlQuery.append("SELECT cct.ID, cct.DESCRIPTION FROM customer_certificatetype cct "
					+ "INNER JOIN indirect_procurement_certificatetypes ipct ON ipct.CERTIFICATETYPEID = cct.ID");

			SQLQuery query = session.createSQLQuery(sqlQuery.toString());

			List<?> certificateTypes = query.list();
			if (certificateTypes != null) {
				iterator = certificateTypes.iterator();

				while (iterator.hasNext()) {
					Tier2ReportDto reportDto = new Tier2ReportDto();
					Object[] objects = (Object[]) iterator.next();

					reportDto.setCertificateTypeId(Integer.parseInt(objects[0]
							.toString()));
					reportDto.setCertificateTypeName(objects[1].toString());

					reportDtos.add(reportDto);
				}
			}
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			System.out.println(ex);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return reportDtos;
	}

	@Override
	public Tier2ReportMaster updateTier2ReportMasterOnly(
			Tier2ReportMaster tier2ReportMaster, UserDetailsDto userDetails, FormFile formFile) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		Tier2ReportMaster master = tier2ReportMaster;
		try {
			session.getTransaction().begin();
			
			String filePath = Constants.getString(Constants.UPLOAD_DOCUMENT_PATH)
					+ "/"+ "tier2reports" + "/" + master.getVendorId().getId();
			File theDir = new File(filePath);

			// if the directory does not exist, create it
			if (!theDir.exists()) {
				theDir.mkdirs();
			}
			String fileFullPath=saveAttachment(formFile, filePath,master.getTier2ReportMasterid()+"");
			master.setFileName(formFile.getFileName());
			master.setFilePath(fileFullPath);
			session.saveOrUpdate(master);
			session.getTransaction().commit();
		} catch (HibernateException ex) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(ex);
			session.getTransaction().rollback();
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return master;
	}
	
	public static String saveAttachment(FormFile file, String filePath,
			String prefix) {
		File fileToCreate = null;
		// Get the file name
		if (file != null) {
			String fileName = file.getFileName();
			// Save file to c:/vms/data/logos
			if (!fileName.equals("")) {
				// Create file
				fileToCreate = new File(filePath, prefix + "_" + fileName);
				// If file does not exists create file
				if (!fileToCreate.exists()) {
					FileOutputStream fileOutStream;
					try {
						// fileToCreate.createNewFile();
						fileOutStream = new FileOutputStream(fileToCreate);
						fileOutStream.write(file.getFileData());
						fileOutStream.flush();
						fileOutStream.close();
					} catch (FileNotFoundException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (IOException e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		}
		
		return fileToCreate.getAbsolutePath();
	}

}