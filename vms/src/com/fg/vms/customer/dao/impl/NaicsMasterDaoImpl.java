/*
 * NaicsMasterDaoImpl.java 
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dao.NaicsMasterDao;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.pojo.NaicsMasterForm;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the Naics Master services implementation
 * 
 * @author vinoth
 * 
 */
public class NaicsMasterDaoImpl implements NaicsMasterDao {

    /** Logger of the system. */
    private static final Logger log = Constants.logger;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NaicsMaster> listOfNAICS(UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<NaicsMaster> masters = null;
        try {
            session.beginTransaction();
            /* Get the list of active naics master records. */
            masters = session.createQuery(
                    "from NaicsMaster where isActive=" + (byte) 1).list();

            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return masters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveNaicsMaster(NaicsMasterForm naicsMasterForm, int userId,
            int currentCategoryId, int subCategoryId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "unique";
        try {
            session.beginTransaction();

            /* To check unique naics code. */
            NaicsMaster master = (NaicsMaster) session
                    .createCriteria(NaicsMaster.class)
                    .add(Restrictions.eq("naicsCode",
                            naicsMasterForm.getNaicsCode())).uniqueResult();

            if (master != null) {
                return result;
            }

            NaicsCategory category = (NaicsCategory) session.get(
                    NaicsCategory.class, currentCategoryId);

            NAICSubCategory subCategory = (NAICSubCategory) session.get(
                    NAICSubCategory.class, subCategoryId);

            NaicsMaster naicsMaster = new NaicsMaster();

            /* Insert the naics master details entered in form. */

            log.info(" ============save naics master is starting========== ");

            naicsMaster.setNaicsCategoryId(category);
            naicsMaster.setNaicsSubCategoryId(subCategory);

            naicsMaster.setNaicsCode(naicsMasterForm.getNaicsCode());
            naicsMaster.setNaicsDescription(naicsMasterForm
                    .getNaicsDescription());
            naicsMaster.setIsicCode(naicsMasterForm.getIsicCode());
            naicsMaster
                    .setIsicDescription(naicsMasterForm.getIsicDescription());
            naicsMaster.setIsActive(naicsMasterForm.getIsActive());
            naicsMaster.setCreatedBy(currentCategoryId);
            naicsMaster.setCreatedOn(new Date());
            naicsMaster.setModifiedBy(currentCategoryId);
            naicsMaster.setModifiedOn(new Date());

            session.save(naicsMaster);

            log.info(" ============saved naics master successfully========== ");
            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteRecord(Integer id, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";
        try {
            session.beginTransaction();
            NaicsMaster naicsMaster = null;

            /* Remove the naics active record. */
            log.info(" ============ delete naics master is starting========== ");

            naicsMaster = (NaicsMaster) session.get(NaicsMaster.class, id);

            /*
             * Delete the template only if template do not have active template
             * questions.
             */
            /*
             * List<Customer> customers = session
             * .createCriteria(Customer.class)
             * .add(Restrictions.eq("naicsSubCategoryId", naicsMaster)).list();
             * 
             * if (customers != null && customers.size() != 0) { return
             * "reference"; }
             */

            session.delete(naicsMaster);
            session.getTransaction().commit();
            log.info(" ============ naics master deleted successfully========== ");

            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NaicsMaster> search(int naicsCategoryId,
            int naicsSubCategoryId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<NaicsMaster> naicsMasters = null;
        try {
            session.beginTransaction();
            NaicsCategory category = (NaicsCategory) session.get(
                    NaicsCategory.class, naicsCategoryId);

            NAICSubCategory subCategory = (NAICSubCategory) session.get(
                    NAICSubCategory.class, naicsSubCategoryId);

            Criteria crit = session.createCriteria(NaicsMaster.class);

            if (category != null && subCategory != null) {
                crit.add(
                        Restrictions.and(Restrictions.eq("naicsCategoryId",
                                category), Restrictions.eq(
                                "naicsSubCategoryId", subCategory))).add(
                        Restrictions.eq("isActive", (byte) 1));
            } else if (category != null) {
                crit.add(
                        Restrictions.or(Restrictions.eq("naicsCategoryId",
                                category), Restrictions.eq(
                                "naicsSubCategoryId", subCategory))).add(
                        Restrictions.eq("isActive", (byte) 1));
            }
            naicsMasters = crit.list();
            session.getTransaction().commit();

        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);

        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return naicsMasters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NaicsMaster retriveNaicsMaster(Integer id, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        NaicsMaster naicsMaster = null;
        try {
            session.beginTransaction();

            /* Retrieve the selected master record and their details. */
            naicsMaster = (NaicsMaster) session
                    .createCriteria(NaicsMaster.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }

        return naicsMaster;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateNAICSMaster(NaicsMasterForm naicsMasterForm,
            Integer id, Integer categoryid, Integer subCategoryid,
            Integer currentUserId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        String result = "";

        NaicsMaster naicsMaster = null;
        try {
            session.beginTransaction();

            /* To check unique naics code. */
            /*
             * NaicsMaster master = (NaicsMaster) session
             * .createCriteria(NaicsMaster.class)
             * .add(Restrictions.eq("naicsCode",
             * naicsMasterForm.getNaicsCode())).uniqueResult();
             * 
             * if (master != null) { return result; }
             */

            NaicsCategory category = (NaicsCategory) session.get(
                    NaicsCategory.class, categoryid);

            NAICSubCategory subCategory = (NAICSubCategory) session.get(
                    NAICSubCategory.class, subCategoryid);

            naicsMaster = (NaicsMaster) session.get(NaicsMaster.class, id);

            /* Update the retrieved master record details. */

            log.info(" ============update naics master is starting========== ");
            naicsMaster.setNaicsCategoryId(category);
            naicsMaster.setNaicsSubCategoryId(subCategory);

            naicsMaster.setNaicsCode(naicsMasterForm.getNaicsCode());
            naicsMaster.setNaicsDescription(naicsMasterForm
                    .getNaicsDescription());
            naicsMaster.setIsicCode(naicsMasterForm.getIsicCode());
            naicsMaster
                    .setIsicDescription(naicsMasterForm.getIsicDescription());
            naicsMaster.setIsActive(naicsMasterForm.getIsActive());

            naicsMaster.setModifiedBy(currentUserId);
            naicsMaster.setModifiedOn(new Date());

            session.update(naicsMaster);

            log.info(" ============updated naics master successfully========== ");

            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<NaicsMaster> naicsMastersByIds(int naicsCategoryId,
            int naicsSubCategoryId, UserDetailsDto appDetails) {

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        List<NaicsMaster> naicsMasters = null;
        try {
            session.beginTransaction();
            NaicsCategory category = (NaicsCategory) session.get(
                    NaicsCategory.class, naicsCategoryId);

            NAICSubCategory subCategory = (NAICSubCategory) session.get(
                    NAICSubCategory.class, naicsSubCategoryId);

            /* Retrieve naics master details based on category and sub category */
            naicsMasters = session
                    .createQuery(
                            "from NaicsMaster where naicsCategoryId=:category and  naicsSubCategoryId=:subCategory and isActive =1")
                    .setParameter("category", category)
                    .setParameter("subCategory", subCategory).list();

            session.getTransaction().commit();

        } catch (HibernateException exception) {
            session.getTransaction().rollback();
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return naicsMasters;
    }

    /**
     * {@inheritDoc}
     */
    public NaicsMaster naicsMaster(Integer naicsId, UserDetailsDto appDetails) {

        NaicsMaster naicsMaster = null;

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(appDetails));

        try {
            session.beginTransaction();

            /* Get the naics master details based on Id */
            naicsMaster = (NaicsMaster) session
                    .createCriteria(NaicsMaster.class)
                    .add(Restrictions.eq("id", naicsId)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (session.isConnected()) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
            }
        }
        return naicsMaster;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String checkDuplicateNaicsCode(String naicsCode,
            UserDetailsDto userDetails) {
        String result = "success";
        NaicsMaster naicsMaster = null;

        /* session object represents customer database */
        Session session = HibernateUtilCustomer.buildSessionFactory()
                .openSession(DataBaseConnection.getConnection(userDetails));

        try {
            session.beginTransaction();

            /* Check the unique naics master code */
            naicsMaster = (NaicsMaster) session
                    .createCriteria(NaicsMaster.class)
                    .add(Restrictions.eq("naicsCode", naicsCode))
                    .uniqueResult();
            if (naicsMaster != null) {
                result = "duplicate";
            }

        } catch (HibernateException exception) {

            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
            return "failure";
        }

        return result;
    }

}
