/*
 * VendorUserDaoImpl.java
 */
package com.fg.vms.customer.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dao.VendorUserDao;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.customer.pojo.VendorUserForm;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the vendor user services implementation
 * 
 * @author hemavaishnavi
 * 
 */
public class VendorUserDaoImpl implements VendorUserDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addVendor(VendorUserForm vendorForm, Integer userid,
			UserDetailsDto appDetails, VendorContact currentUser) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		/* Get the user details based on unique user name and Email Id. */
		String result = findUserByUsernameandEmail(vendorForm, appDetails);

		try {
			Random random = new java.util.Random();
			Integer randVal = random.nextInt();
			Encrypt encrypt = new Encrypt();

			/* convert password to encrypted password */
			String encyppassword = encrypt.encryptText(
					Integer.toString(randVal) + "", vendorForm.getPassword());
			session.beginTransaction();
			if (result.equals("submit")) {
				VendorContact vendors = new VendorContact();
				VendorRoles vendorRole = (VendorRoles) session.get(
						VendorRoles.class, vendorForm.getRoleId());
				VendorMaster vendor = (VendorMaster) session.get(
						VendorMaster.class, currentUser.getVendorId().getId());
				SecretQuestion sq = (SecretQuestion) session.get(
						SecretQuestion.class, vendorForm.getSecId());

				log.info(" ============save vendor user is starting========== ");

				vendors.setVendorId(vendor);
				vendors.setVendorUserRoleId(vendorRole);
				vendors.setFirstName(vendorForm.getFirstName());
				vendors.setLastName(vendorForm.getLastName());
				vendors.setDesignation(vendorForm.getDesignation());
				vendors.setPhoneNumber(vendorForm.getPhonenumber());
				vendors.setMobile(vendorForm.getMobile());
				vendors.setFax(vendorForm.getFax());
				vendors.setEmailId(vendorForm.getEmailId());
				vendors.setIsAllowedLogin((byte) 1);
				vendors.setIsSysGenPassword((byte)0);
				// vendors.setLoginDisplayName(vendorForm.getLoginDisplayName());
				// vendors.setLoginId(vendorForm.getLoginId());
				vendors.setPassword(encyppassword);
				vendors.setKeyValue(randVal);
				vendors.setSecretQuestionId(sq);
				vendors.setSecQueAns(vendorForm.getSecretQuestionAnswer());
				vendors.setIsPrimaryContact((byte) 0);
				vendors.setIsActive((byte) 1);
				vendors.setCreatedBy(userid);
				vendors.setCreatedOn(new Date());
				vendors.setModifiedBy(userid);
				vendors.setModifiedOn(new Date());
				vendors.setIsPreparer((byte) 0);
				vendors.setGender(vendorForm.getGender());
				session.save(vendors);

				// password History
				PasswordHistory passwordHistory = new PasswordHistory();
				passwordHistory.setKeyvalue(randVal);
				passwordHistory.setChangedon(new Date());
				passwordHistory.setChangedby(userid);
				passwordHistory.setPassword(encyppassword);
				passwordHistory.setVendorUserId(vendors.getId());
				session.save(passwordHistory);
				log.info(" ============ vendor user saved successfully========== ");

				session.getTransaction().commit();
				result = "success";
			} else {
				return result;
			}
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			result = "failure";
		} catch (Exception e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VendorContact> listVendors(Integer vendorId,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		session.beginTransaction();
		List<VendorContact> vendors = null;
		try {
			vendors = session.createQuery(
					"from VendorContact where isActive=1 and vendorId="
							+ vendorId).list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return vendors;
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public String deleteVendor(Integer vendorid, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		VendorContact vendors = null;
		try {
			session.beginTransaction();
			log.info(" ============update vendor user into inactive is starting========== ");
			vendors = (VendorContact) session
					.get(VendorContact.class, vendorid);
			vendors.setIsActive((byte) 0);
			session.update(vendors);
			session.getTransaction().commit();
			log.info(" ============vendor user updated into inactive users successfully========== ");

			result = "success";
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateVendor(VendorUserForm vendorForm, Integer userid,
			Integer vendorid, UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));

		String result = "";
		VendorContact vendor = null;
		try {
			session.beginTransaction();

			vendor = (VendorContact) session.get(VendorContact.class, vendorid);
			VendorRoles vendorRole = (VendorRoles) session.get(
					VendorRoles.class, vendorForm.getRoleId());

			// Random random = new java.util.Random();
			Integer randVal = vendor.getKeyValue();
			Encrypt encrypt = new Encrypt();
			Decrypt decrypt = new Decrypt();
			String decryptedPassword = decrypt.decryptText(
					String.valueOf(randVal.toString()), vendor.getPassword());
			/* convert password to encrypted password. */
			String encyppassword = encrypt.encryptText(
					Integer.toString(randVal) + "", vendorForm.getPassword());

			if (!decryptedPassword.equals(vendorForm.getPassword())) {
				// password History
				List<PasswordHistory> histories = session.createQuery(
						" From PasswordHistory where vendorUserId="
								+ vendor.getId()
								+ " order by changedon desc limit 12").list();
				if (CommonUtils.checkPasswordLimitExist(histories,
						vendorForm.getPassword()).equals("exist")) {
					return "exist";
				} else {
					PasswordHistory passwordHistory = new PasswordHistory();
					passwordHistory.setKeyvalue(randVal);
					passwordHistory.setChangedon(new Date());
					passwordHistory.setChangedby(userid);
					passwordHistory.setPassword(encyppassword);
					passwordHistory.setVendorUserId(vendor.getId());
					session.save(passwordHistory);
				}
			}
			// convert password to encrypted password

			SecretQuestion sq = (SecretQuestion) session.get(
					SecretQuestion.class, vendorForm.getSecId());

			log.info(" ============save vendor is starting========== ");

			vendor.setVendorUserRoleId(vendorRole);
			vendor.setFirstName(vendorForm.getFirstName());
			vendor.setLastName(vendorForm.getLastName());
			vendor.setDesignation(vendorForm.getDesignation());
			vendor.setPhoneNumber(vendorForm.getPhonenumber());
			vendor.setMobile(vendorForm.getMobile());
			vendor.setFax(vendorForm.getFax());
			vendor.setEmailId(vendorForm.getEmailId());
			vendor.setIsAllowedLogin((byte) 1);
			// vendor.setLoginDisplayName(vendorForm.getLoginDisplayName());
			// vendor.setLoginId(vendorForm.getLoginId());
			vendor.setPassword(encyppassword);
			vendor.setNewPassword("");
			vendor.setKeyValue(randVal);
			vendor.setSecretQuestionId(sq);
			vendor.setSecQueAns(vendorForm.getSecretQuestionAnswer());
			vendor.setGender(vendorForm.getGender());
			// vendor.setIsPrimaryContact((byte) 0);
			vendor.setIsActive((byte) 1);
			vendor.setCreatedBy(userid);
			vendor.setCreatedOn(new Date());
			vendor.setModifiedBy(userid);
			vendor.setModifiedOn(new Date());
			session.update(vendor);

			log.info(" ============vendor saved successfully========== ");

			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			result = "failure";
			session.getTransaction().rollback();
		} catch (Exception e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorContact retriveVendor(Integer vendorid,
			UserDetailsDto appDetails) {

		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		session.beginTransaction();
		VendorContact vendor = null;
		try {

			/* Get the vendor contact details based on unique vendorId. */
			vendor = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("id", vendorid)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}

		return vendor;
	}

	/**
	 * This method takes the username and emailid and check the database to
	 * determine if the user is already registered in the system.
	 * 
	 * @param usersForm
	 *            Incoming username and password from the user's form
	 * @param userDetails
	 * 
	 * @return result already associated. If not, return submit
	 */

	private static String findUserByUsernameandEmail(VendorUserForm vendorForm,
			UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));
		String result = "submit";
		try {

			session.beginTransaction();

			/* Get the user details based on unique userLogin. */
			// Users userLogin = (Users) session.createCriteria(Users.class)
			// .add(Restrictions.eq("userLogin", vendorForm.getLoginId()))
			// .uniqueResult();

			/* Get the user details based on unique Email Id. */
			Users userEmailId = (Users) session
					.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", vendorForm.getEmailId()))
					.uniqueResult();

			/* Get the vendor contact details based on unique loginId. */
			// VendorContact vendorLogin = (VendorContact) session
			// .createCriteria(VendorContact.class)
			// .add(Restrictions.eq("loginId", vendorForm.getLoginId()))
			// .uniqueResult();

			/* Get the vendor contact details based on unique Email Id. */
			VendorContact vendorEmail = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("emailId", vendorForm.getEmailId()))
					.uniqueResult();

			if (userEmailId != null && vendorEmail != null) {
				result = "both";
			} else if (userEmailId != null) {
				result = "userEmail";
			} else if (vendorEmail != null) {
				result = "userEmail";
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}
	
	public String checkAdimMenuApplicable(UserDetailsDto appDetails,Integer vendorId)
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		String isAdimMenuApplicable = "yes";
		try{
			session.beginTransaction();
			VendorMaster vendor = new VendorMaster();
			String query = "From VendorMaster v where v.primeNonPrimeVendor = 0 and v.vendorStatus IN('A','B') and v.id = "+vendorId;
			vendor = (VendorMaster)session.createQuery(query).uniqueResult();
			if(vendor!=null)
			{
				isAdimMenuApplicable = "not";
			}
		}catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return isAdimMenuApplicable;
	}
}
