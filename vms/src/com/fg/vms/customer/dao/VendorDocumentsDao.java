/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.CommodityDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.VendorHistory;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorCommodity;
import com.fg.vms.customer.model.CustomerVendorInsurance;
import com.fg.vms.customer.model.CustomerVendorKeywords;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorDocuments;
import com.fg.vms.customer.model.VendorOtherCertificate;

/**
 * @author gpirabu
 * 
 */
public interface VendorDocumentsDao {
	public List<VendorDocuments> getVendorDocuments(Integer vendorId,
			UserDetailsDto appDetails);

	/**
	 * Download certificate.
	 * 
	 * @param id
	 *            the id
	 * @param userDetails
	 *            the user details
	 * @return the vendor certificate
	 */
	public VendorCertificate downloadCertificate(Integer id,
			UserDetailsDto userDetails);

	/**
	 * Download vendor documents.
	 * 
	 * @param id
	 * @param userDetails
	 * @return
	 */
	public VendorDocuments downloadDocument(Integer id,
			UserDetailsDto userDetails);

	/**
	 * Download other certificate.
	 * 
	 * @param id
	 *            the id
	 * @param userDetails
	 *            the user details
	 * @return the vendor other certificate
	 */
	public VendorOtherCertificate downloadOtherCertificate(Integer id,
			UserDetailsDto userDetails);

	/**
	 * Delete the vendor document
	 * 
	 * @param docId
	 * @param appDetails
	 * @return
	 */
	public String deleteDocument(int docId, UserDetailsDto appDetails);

	/**
	 * Get vendor service area details.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getVendorServiceAreas(
			Integer vendorId, UserDetailsDto appDetails);

	/**
	 * Get vendor commodity details.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public List<CustomerVendorCommodity> getVendorCommodities(Integer vendorId,
			UserDetailsDto appDetails);

	/**
	 * Delete the service area.
	 * 
	 * @param vendorId
	 * @param stateId
	 * @param appDetails
	 */
	public void deleteServiceArea(Integer serviceAreaId,
			UserDetailsDto appDetails);

	/**
	 * Delete the commodity.
	 * 
	 * @param commodityId
	 * @param appDetails
	 */
	public void deleteCommodity(Integer commodityId, UserDetailsDto appDetails);

	public List<VendorDocuments> getVendorDocumentsList(
			VendorContact currentVendor, UserDetailsDto appDetails);

	/**
	 * Get the Insurance list for vendor.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public CustomerVendorInsurance getListVendorInsurance(Integer vendorId,
			UserDetailsDto appDetails);

	/**
	 * Download vendor insurance documents.
	 * 
	 * @param id
	 * @param userDetails
	 * @return
	 */
	public CustomerVendorInsurance downloadInsuranceDocument(
			Integer insuranceId, UserDetailsDto userDetails);

	/**
	 * Get the mailing address of vendor.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public CustomerVendorAddressMaster getMailingAddress(Integer vendorId,
			String addressType, UserDetailsDto appDetails);

	public List<VendorHistory> modifiedDate(UserDetailsDto userDetails,
			Integer vendorId);
	
	public List<CommodityDto> listVendorCommodities(Integer vendorId,
			UserDetailsDto appDetails);

	/**
	 * To Get List of Vendor Keywords for 1 Vendor.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public List<CustomerVendorKeywords> getVendorKeywords(Integer vendorId, UserDetailsDto appDetails);

	/**
	 * To Delete Vendor Keyword.
	 * @param keywordsId
	 * @param appDetails
	 * @return
	 */
	public String deleteKeywords(int keywordsId, UserDetailsDto appDetails);
}
