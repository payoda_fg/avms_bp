package com.fg.vms.customer.dao;

import java.util.LinkedHashMap;
import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.model.CustomerSearchFilter;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorCertificate;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.SearchVendorForm;

/**
 * The Interface SearchDao.
 * 
 * @author pirabu
 */
public interface SearchDao {

	/**
	 * Search vendor.
	 * 
	 * @param searchVendorForm
	 *            the search vendor form
	 * @param approved
	 *            the approved
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<SearchVendorDto> searchVendor(
			SearchVendorForm searchVendorForm, Integer approved, Integer prime,
			UserDetailsDto appDetails);

	/**
	 * Search tire2 vendor.
	 * 
	 * @param form
	 *            the form
	 * @param vendorId
	 *            the vendor id
	 * @param approve
	 *            the approve
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<SearchVendorDto> searchTire2Vendor(SearchVendorForm form,
			Integer vendorId, Integer approve, UserDetailsDto appDetails);

	/**
	 * Review vendors list.
	 * 
	 * @param userDetails
	 *            the user details
	 * @param customerId
	 *            the customer id
	 * @return the list
	 */
	public List<VendorMaster> reviewVendorsList(UserDetailsDto userDetails,
			Integer customerId);

	/**
	 * Implementing the code for get the list of agencies.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<CertifyingAgency> listAgencies(UserDetailsDto appDetails);

	/**
	 * Implementing the code for getting the Password Secret Questions.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<SecretQuestion> listSecQns(UserDetailsDto appDetails);

	/**
	 * Implementing the code for getting the vendor based on search string.
	 * 
	 * @param string
	 * @param appDetails
	 * @return
	 */
	public List<SearchVendorDto> searchVendorFullText(String string,
			UserDetailsDto appDetails, Integer divisionId);

	/**
	 * Get the list of export fields with category.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> listOfExportFields(
			UserDetailsDto appDetails);

	/**
	 * Get the total count of available vendors.
	 * 
	 * @param appDetails
	 * @return
	 */
	public Integer totalVendorCount(SearchVendorForm searchVendorForm,
			UserDetailsDto appDetails, String searchPageType);

	/**
	 * @param searchVendorForm
	 * @param i
	 * @param index
	 * @param appDetails
	 * @return
	 */

	List<SearchVendorDto> searchPrimeVendor(SearchVendorForm searchVendorForm,
			Integer approved, Integer from, UserDetailsDto appDetails);

	/**
	 * getNaicsCodes
	 * 
	 * @param userDetails
	 * @param searchValue
	 * @return
	 */
	public List<String> getNaicsCodes(UserDetailsDto userDetails,
			String searchValue);

	/**
	 * getNaicsDescriptions
	 * 
	 * @param userDetails
	 * @param searchValue
	 * @return
	 */
	public List<String> getNaicsDescriptions(UserDetailsDto userDetails,
			String searchValue);

	/**
	 * getVendorNamesList
	 * 
	 * @param userDetails
	 * @param searchValue
	 * @return
	 */
	public List<String> getVendorNamesList(UserDetailsDto userDetails,
			String searchValue);

	/**
	 * listPreviousSearchDetails
	 * 
	 * @param userDetails
	 * @param searchType
	 *            TODO
	 * @param userId
	 * @return
	 */
	public List<CustomerSearchFilter> listPreviousSearchDetails(
			UserDetailsDto userDetails, char searchType, Integer userId);

	/**
	 * getCustomerSearchFilter
	 * 
	 * @param userDetails
	 * @param searchId
	 * @return
	 */
	public CustomerSearchFilter getCustomerSearchFilter(
			UserDetailsDto userDetails, Integer searchId);

	/**
	 * searchByPreviousData
	 * 
	 * @param userDetails
	 * @param searchQuery
	 * @param searchType
	 * @return
	 */
	public List<SearchVendorDto> searchByPreviousData(
			UserDetailsDto userDetails, String searchQuery, String searchType);

	/**
	 * Search All Vendor Details.
	 * 
	 * @param appDetails
	 * @param vendorId
	 * @return
	 */
	public List<LinkedHashMap<String, Object>> searchVendorAllDetails(
			UserDetailsDto appDetails, String vendorId);

	/**
	 * Search Vendor Based on Non-NMSDC/WBENC
	 * 
	 * @param searchVendorForm
	 * @param appDetails
	 * @return
	 */
	public List<SearchVendorDto> searchVendorFilteredByNonNMSDCWBENC(
			SearchVendorForm searchVendorForm, UserDetailsDto appDetails);

	/**
	 * for searchByPreviousDataForPrimeVendor
	 * 
	 * @param userDetails
	 * @param searchQuery
	 * @return
	 */
	public List<SearchVendorDto> searchByPreviousDataForPrimeVendor(
			UserDetailsDto userDetails, String searchQuery);

	/**
	 * To Get UserName based on UserId.
	 * 
	 * @param userDetails
	 * @param userId
	 * @return
	 */
	public String findUserNameByUserId(UserDetailsDto userDetails,
			Integer userId);

	/**
	 * for search By VendorStatus
	 * 
	 * @param userDetails
	 * @param searchVendorForm
	 * @return
	 */
	public List<SearchVendorDto> searchByVendorStatus(
			UserDetailsDto userDetails, SearchVendorForm searchVendorForm);

	public List<Tier2ReportDto> getSpendReportHistory(SearchVendorForm searchVendorForm,
			UserDetailsDto userDetails);
	
	public Tier2ReportMaster getTier2ReportMaster(Integer id,
			UserDetailsDto userDetails);
}
