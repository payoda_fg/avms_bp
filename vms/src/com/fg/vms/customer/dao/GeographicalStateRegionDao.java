/**
 * GeographicalStateRegionDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.GeographicalStateRegionDto;
import com.fg.vms.customer.model.CustomerBusinessArea;
import com.fg.vms.customer.model.CustomerBusinessGroup;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.model.Region;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.pojo.GeographicForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * geographical page processing such as create new geography list and view the
 * list of active categories.
 * 
 * @author vinoth
 * 
 */
public interface GeographicalStateRegionDao {

	/**
	 * Function to list the active state details.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<State> listStates(UserDetailsDto appDetails);

	/**
	 * Function to list the active business groups.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<CustomerBusinessGroup> listBusinessGroups(
			UserDetailsDto appDetails);

	/**
	 * Function to list the active state by regions.
	 * 
	 * @param appDetails
	 * @param query
	 * @return
	 */
	public List<GeographicalStateRegionDto> listStateByRegions(
			UserDetailsDto appDetails, String query);

	/**
	 * Function to list the region.
	 * 
	 * @param appDetails
	 * @param regionId
	 * @return
	 */
	public List<Region> sizeByCode(UserDetailsDto appDetails, Integer regionId);

	/**
	 * Retrieve the business group.
	 * 
	 * @param regionId
	 * @param appDetails
	 * @return
	 */
	public List<CustomerBusinessGroup> retrieveBusinessGroup(Integer regionId,
			UserDetailsDto appDetails);

	/**
	 * Delete selected business group.
	 * 
	 * @param regionId
	 * @param appDetails
	 */
	public void deleteGroup(Integer regionId, Integer userId,
			UserDetailsDto appDetails);

	/**
	 * Retrieve business area.
	 * 
	 * @param regionId
	 * @param appDetails
	 * @return
	 */
	public List<CustomerBusinessArea> retrieveBusinessArea(Integer regionId,
			UserDetailsDto appDetails);

	/**
	 * Delete selected state.
	 * 
	 * @param stateId
	 * @param appDetails
	 */
	public void deleteArea(Integer stateId, Integer userId,
			UserDetailsDto appDetails);

	/**
	 * Get the State Name based on State Id.
	 * 
	 * @param stateId
	 * @param appDetails
	 * @return
	 */
	public State getStateName(Integer stateId, UserDetailsDto appDetails);

	/**
	 * Function to get all states based on regions.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getStateByRegionList(
			UserDetailsDto appDetails);

	/**
	 * Function to get all states based on regions.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getGeographicalRegionList(
			Integer vendorId, UserDetailsDto appDetails);

	/**
	 * Save Business Group.
	 * 
	 * @param userId
	 * @param geographicForm
	 * @param appDetails
	 */
	public void saveBusinessGroup(Integer userId,
			GeographicForm geographicForm, UserDetailsDto appDetails);

	/**
	 * Save Service Area.
	 * 
	 * @param userId
	 * @param geographicForm
	 * @param appDetails
	 */
	public void saveServiceArea(Integer userId, GeographicForm geographicForm,
			UserDetailsDto appDetails);

	/**
	 * List business areas.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<CustomerBusinessArea> listBusinessAreas(
			UserDetailsDto appDetails);

	/**
	 * List of service areas.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getServiceAreasList(
			UserDetailsDto appDetails);
	
	/**
	 * check service areas before deletion.
	 * 
	 * @param appDetails
	 * @return
	 */
	public String checkDeleteGroup(Integer groupId,Integer userId,UserDetailsDto appDetails);
	
	/**
	 * List of commodity description.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getCommodityDescription(UserDetailsDto appDetails);
	
	/**
	 * List of NAICS Code.
	 * 
	 * @param appDetails
	 * @return
	 */
	
	public List<NaicsCode> getNaicsCode(UserDetailsDto appDetails);
	/**
	 * List of certification types.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getCertificationType(
			UserDetailsDto appDetails);

	/**
	 * List Commodity Group with Market Sector, Market SubSector, & Commodity Description Hierarchy
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<GeographicalStateRegionDto> getCommodityGroup(UserDetailsDto appDetails);
	
}
