/**
 * CountryStateDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.CountryStateDto;
import com.fg.vms.customer.model.Country;
import com.fg.vms.customer.model.State;
import com.fg.vms.customer.pojo.CountryStateForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * CountryState page processing such as create new country list and view the
 * list of active categories.
 * 
 * @author asarudeen
 * 
 */
public interface CountryStateDao
{
	/**
	 * Save Country.
	 * 
	 * @param userId
	 * @param countryStateForm
	 * @param appDetails
	 */
	public void saveCountry(Integer userId, CountryStateForm countryStateForm, UserDetailsDto appDetails);
	
	/**
	 * Lists Countries.
	 * 
	 * @param appDetails
	 */
	public List<Country> listCountries(UserDetailsDto appDetails);
	
	/**
	 * Delete Country.
	 * 
	 * @param countryId
	 * @param userId
	 * @param appDetails
	 */
	public void deleteCountry(Integer countryId, Integer userId, UserDetailsDto appDetails);
	
	/**
	 * Retrieve Countries.
	 * 
	 * @param countryId
	 * @param appDetails
	 */
	public List<Country> retrieveCountries(Integer countryId, UserDetailsDto appDetails);
	
	/**
	 * Save State.
	 * 
	 * @param userId
	 * @param countryStateForm
	 * @param appDetails
	 */
	public void saveState(Integer userId, CountryStateForm countryStateForm, UserDetailsDto appDetails);
	
	/**
	 * Lists States.
	 * 
	 * @param appDetails
	 */
	public List<State> listStates(UserDetailsDto appDetails);
	
	/**
	 * Delete State.
	 * 
	 * @param stateId
	 * @param userId
	 * @param appDetails
	 */
	public void deleteState(Integer stateId, Integer userId, UserDetailsDto appDetails);
	
	/**
	 * Retrieve States.
	 * 
	 * @param stateId
	 * @param appDetails
	 */
	public List<State> retrieveState(Integer stateId, UserDetailsDto appDetails);
	
	/**
	 * Function to get all states based on Countries.
	 * @param appDetails
	 */
	public List<CountryStateDto> getStateByCountryList(UserDetailsDto appDetails);
	
	/**
	 * Delete Country Check.
	 * 
	 * @param countryId
	 * @param userId
	 * @param appDetails
	 */
	public List<Country> deleteCountryCheck(Integer countryId, Integer userId, UserDetailsDto appDetails);
	
	/**
	 * Delete State Check.
	 * 
	 * @param stateId
	 * @param userId
	 * @param appDetails
	 */
	public List<State> deleteStateCheck(Integer stateId, Integer userId, UserDetailsDto appDetails);
	
	/*
	 * Retrieve States By Selected Country
	 * 
	 * @param CountryId
	 * @param appDetails
	 */
	public List<State> retrieveStateByCountry(Integer countryId,UserDetailsDto appDetails);
	
	/**
	 * Retrieve Default Country.
	 * 
	 * @param appDetails
	 * @return
	 */
	public Country listDefaultCountry(UserDetailsDto appDetails);
}

