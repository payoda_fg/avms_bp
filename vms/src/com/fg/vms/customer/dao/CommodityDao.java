/**
 * CommodityDao.java
 */
package com.fg.vms.customer.dao;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.CustomerCommodityCategory;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * commodity category page processing such as create new category and view the
 * list of active commodity categories.
 * 
 * @author vinoth
 * 
 */
public interface CommodityDao {

	/**
	 * Get Object of commodity category.
	 * 
	 * @param appDetails
	 * @return
	 */
	public CustomerCommodityCategory getCommodity(Integer categoryId,
			UserDetailsDto appDetails);

}
