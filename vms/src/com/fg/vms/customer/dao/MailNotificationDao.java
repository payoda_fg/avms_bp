/*
 * MailNotificationDao.java
 */
package com.fg.vms.customer.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dto.SearchVendorDto;
import com.fg.vms.customer.dto.SpendIndirectDto;
import com.fg.vms.customer.model.CustomerIndirectSpendDataMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorEmailNotificationMaster;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.MailForm;
import com.fg.vms.customer.pojo.SearchVendorForm;

public interface MailNotificationDao {

	public List<SearchVendorDto> searchVendorName(
			SearchVendorForm searchVendorForm, Integer approved,
			UserDetailsDto appDetails, Integer primeOrNonprime, String emailType);

	// public List<MailNotificationDto> view(UserDetailsDto appDetails);

	public String sendMail(MailForm mailform, UserDetailsDto appDetails,String prefix);

	/**
	 * Method to update the approval details
	 * 
	 * @param selectedId
	 * @param prime
	 * @param userId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public String updateApproval(Integer selectedId, Integer prime,
			Integer userId, Integer approvalStatus, String isApprovedDesc,
			UserDetailsDto appDetails);

	public String changeVendorStatus(Integer selectedId, Users user,
			String status,String bpSegemntId, String isApprovedDesc, UserDetailsDto appDetails,
			Integer prime,String url);

	/**
	 * 
	 * @param id
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public VendorMaster getVendor(Integer id, UserDetailsDto appDetails);

	/**
	 * 
	 * @param mailForm
	 * @param userDetails
	 * @param currentUserId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public VendorEmailNotificationMaster saveMailNotificationInformation(
			MailForm mailForm, UserDetailsDto userDetails,
			Integer currentUserId, UserDetailsDto appDetails);

	/**
	 * 
	 * @param totalDirectSales
	 * @param percentageOfDirectSales
	 * @param reportingQuaterFromDate
	 * @param reportingQuaterToDate
	 * @param primeVendorId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public List<SpendIndirectDto> Calculate(Double totalDirectSales,
			Double percentageOfDirectSales, Date reportingQuaterFromDate,
			Date reportingQuaterToDate, Integer primeVendorId,
			UserDetailsDto appDetails);

	/**
	 * 
	 * @param vendorId
	 * @param reportingYear
	 * @param reportingQuaterFromDate
	 * @param reportingQuaterToDate
	 * @param totalDirectSales
	 * @param percentageOfDirectSales
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public CustomerIndirectSpendDataMaster saveSpendIndirect(Integer vendorId,
			String reportingYear, Date reportingQuaterFromDate,
			Date reportingQuaterToDate, Double totalDirectSales,
			Double percentageOfDirectSales, UserDetailsDto appDetails);

	/**
	 * 
	 * @param mailForm
	 * @param userId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 */
	public VendorEmailNotificationMaster saveAssessmentMailInfo(MailForm mailForm, Integer userId,
			UserDetailsDto appDetails);

	/**
	 * 
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @param currentUserId
	 * @return
	 */
	public List<?> retriveMailnotificationDetails(UserDetailsDto userDetails,
			Integer currentUserId);

	public String assignAssessmentToVendor(Integer userId, VendorMaster vendor,
			String subject, String content, VendorContact contact,
			UserDetailsDto appDetails, Session session);
}
