/**
 * 
 */
package com.fg.vms.customer.dao;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.ProactiveMonitoringCfg;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.ProactiveMonitoringForm;

/**
 * @author pirabu
 * 
 */
public interface ProactiveMonitoringDao {
    public String save(UserDetailsDto userDetails, ProactiveMonitoringForm form, Integer userId);

    public ProactiveMonitoringCfg retrive(UserDetailsDto userDetails);

    public void notifyReportDue(UserDetailsDto userDetails);

    public void notifyCertificateExpiration(UserDetailsDto userDetails, WorkflowConfiguration workflowConfiguration, String appRoot);

    public void notifyCertificateExpires(UserDetailsDto userDetails);

    public void notifyReportDueNotSubmitted(UserDetailsDto userDetails);

	public void certificateExpiryNotificationMonthlyReport(UserDetailsDto userDetails);
}