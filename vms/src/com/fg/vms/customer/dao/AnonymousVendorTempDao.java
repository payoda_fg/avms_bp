/**
 * AnonymousVendorTempDao.java
 */
package com.fg.vms.customer.dao;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.AnonymousVendorDump;
import com.fg.vms.customer.pojo.AnonymousVendorForm;

/**
 * @author vinoth
 * 
 */
public interface AnonymousVendorTempDao {

	/**
	 * Method to save an anonymous vendor partial data.
	 * 
	 * @param anonymousVendorForm
	 * @param currentUserId
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public AnonymousVendorDump saveVendorInfo(
			AnonymousVendorForm anonymousVendorForm, int currentUserId,
			int vendorId, UserDetailsDto userDetails);

}
