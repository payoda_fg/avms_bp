package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.VendorRolesAndObjects;
import com.fg.vms.customer.model.VendorRolePrivileges;

public interface VendorRolePrivilegesDao {

	/**
	 * Persist or merge privileges by role.
	 * 
	 * @param rolesAndObjects
	 * @param roleName
	 * @param id
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public String persistPrivilegesByRole(
			VendorRolesAndObjects rolesAndObjects, String roleName, Integer id,
			UserDetailsDto appDetails);

	/**
	 * Retrieve privileges by role.
	 * 
	 * @param roleId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public List<VendorRolePrivileges> retrievePrivilegesByRole(Integer roleId,
			UserDetailsDto appDetails);

}
