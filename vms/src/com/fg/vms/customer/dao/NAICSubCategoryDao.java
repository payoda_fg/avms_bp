/*
 * NAICSubCategoryDao.java 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.NaicsSubCategoryDto;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.pojo.NaicSubCategoryForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * Naics sub category page processing such as list of active Naics
 * sub-categories, save, update and modify Naics sub-category details
 * 
 * @author vinoth
 * 
 */
public interface NAICSubCategoryDao {

    /**
     * Create an NAIC Sub-Category into database. Sub-categories saved into
     * database based on categories. One category may have more than one
     * sub-categories.
     * 
     * @param naicSubCategoryForm
     * @param currentCategoryId
     * @param appDetails
     * @return result
     */
    public String createNAICSubCategory(
	    NaicSubCategoryForm naicSubCategoryForm, int userId,
	    int currentCategoryId, UserDetailsDto appDetails);

    /**
     * Retrieve list of Naics sub-category details from database.
     * 
     * @param appDetails
     * @param category
     *            - Its hold the current user application details.
     * 
     * @return objects
     */
    public List<NAICSubCategory> retrieveNAICSubCategories(
	    UserDetailsDto appDetails, NaicsCategory category);

    /**
     * Delete/Remove the selected Naics Sub-Category from the database.
     * 
     * @param id
     * @param appDetails
     * @return result
     */
    public String deleteNAICSubCategory(Integer id, UserDetailsDto appDetails);

    /**
     * Retrieve/View the selected Naics sub-category details from database.
     * 
     * @param id
     * @param appDetails
     * @return result
     */
    public NAICSubCategory retrieveSubCategory(Integer id,
	    UserDetailsDto appDetails);

    /**
     * Update/Modify the selected Naics sub-category details into database.
     * 
     * @param id
     * @param appDetails
     * @param configurationForm
     * @param currentUserId
     * 
     * @return result
     */
    public String updateNAICSubCategory(
	    NaicSubCategoryForm naicSubCategoryForm, Integer id,
	    int currentCategoryId, UserDetailsDto appDetails);

    /**
     * Retrieve and load the sub-categories based on the category selected by
     * user. This gets the category as request and send sub-categories as
     * response.
     * 
     * @param appDetails
     * @param roleId
     * 
     * @return
     */
    public List<NAICSubCategory> retrieveSubCategoryById(Integer categoryId,
	    UserDetailsDto appDetails);

    /**
     * Retrieve the list of categories will be loaded during the page load.
     * 
     * @param categoryId
     * @param appDetails
     * @return
     */
    public NaicsCategory retriveNaicsCategory(Integer categoryId,
	    UserDetailsDto appDetails);

    /**
     * View the list of sub-categories those are all active.
     * 
     * @param appDetails
     * @return
     */
    public List<NaicsSubCategoryDto> view(UserDetailsDto appDetails);

}
