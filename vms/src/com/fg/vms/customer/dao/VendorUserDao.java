package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.pojo.VendorUserForm;

public interface VendorUserDao {
    /**
     * Method to register a new Vendor
     * 
     * @param vendorForm
     * @param userid
     * @param userDetails
     *            - Its hold the current user application details.
     * @param currentUser
     * @return result
     */
    public String addVendor(VendorUserForm vendorForm, Integer userid,
	    UserDetailsDto userDetails, VendorContact currentUser);

    /**
     * Method to retrieve the all vendors in the DB.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return vendors
     */
    public List<VendorContact> listVendors(Integer vendorId,
	    UserDetailsDto appDetails);

    /**
     * Method to delete the selected item
     * 
     * @param vendorid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String deleteVendor(Integer vendorid, UserDetailsDto appDetails);

    /**
     * Method to update item details.
     * 
     * @param vendorForm
     * @param userid
     * @param vendorid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String updateVendor(VendorUserForm vendorForm, Integer userid,
	    Integer vendorid, UserDetailsDto appDetails);

    /**
     * Method to retrieve the particular vendor based on vendorId.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * @param vendorid
     * 
     * @return result
     */
    public VendorContact retriveVendor(Integer vendorid,
	    UserDetailsDto appDetails);
    
    
    /**
     * @param appDetails
     * @param vendorId
     * @return
     */
    public String checkAdimMenuApplicable(UserDetailsDto appDetails,Integer vendorId);
}
