/**
 * 
 */
package com.fg.vms.customer.dao.util;

import java.util.LinkedHashMap;

import org.hibernate.transform.BasicTransformerAdapter;

/**
 * @author Asarudeen
 *
 */
public class TupleToHeaderDataMapTransformer extends BasicTransformerAdapter {
	private static final long serialVersionUID = 1L;
	LinkedHashMap<String, Object> map;
	  
	public TupleToHeaderDataMapTransformer() {}
	  
	@Override
	public Object transformTuple(Object[] rowData, String[] aliasNames) {
		if(rowData==null)
			return null;
	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		for(int ctr=0; ctr<rowData.length; ctr++) {
			map.put(aliasNames[ctr], rowData[ctr]);
		}
   
		return map;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}