package com.fg.vms.customer.dao;

import java.util.List;

import org.apache.struts.upload.FormFile;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.ReportSearchFieldsDto;
import com.fg.vms.customer.dto.SearchReportDto;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.CustomReportSearchFields;
import com.fg.vms.customer.model.DashboardSettings;
import com.fg.vms.customer.model.Tier2ReportMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.ReportForm;
import com.fg.vms.customer.pojo.SearchVendorForm;
import com.fg.vms.customer.pojo.Tier2ReportForm;

/**
 * Represents the service layer of creation reports
 * 
 * @author srinivasarao
 */
public interface ReportDao {

	/**
	 * Spend data report.
	 * 
	 * @param userDetails
	 *            the user details
	 * @return the string buffer
	 */
	public StringBuffer spendDataReport(UserDetailsDto userDetails);

	/**
	 * Diversity report.
	 * 
	 * @param userDetails
	 *            the user details
	 * @param searchVendorForm
	 *            the search vendor form
	 * @return the string buffer
	 */
	public StringBuffer diversityReport(UserDetailsDto userDetails,
			SearchVendorForm searchVendorForm);

	/**
	 * Year wise report.
	 * 
	 * @param userDetails
	 *            the user details
	 * @param searchVendorForm
	 *            the search vendor form
	 * @return the string buffer
	 */
	public StringBuffer yearwiseReport(UserDetailsDto userDetails,
			SearchVendorForm searchVendorForm);

	/**
	 * Indirect spend report.
	 * 
	 * @param userDetails
	 *            the user details
	 * @param reportForm
	 *            the report form
	 * @return the string buffer
	 */
	public StringBuffer indirectSpendReport(UserDetailsDto userDetails,
			ReportForm reportForm);

	public List<Certificate> tier2Certificates(UserDetailsDto userDetails,
			VendorMaster vendor);

	public Tier2ReportMaster saveTier2Report(Tier2ReportForm form,
			UserDetailsDto userDetails, VendorContact tire2Vendor,
			String submitType);

	public List<Tier2ReportMaster> viewTier2ReportByVendor(
			UserDetailsDto userDetails, Integer vendorId);

	/**
	 * List tier2 spend report master details to get total sales report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getTotalSalesReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails);

	/**
	 * List tier2 spend report master details to get spend (Direct) report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> directSpendReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails);

	/**
	 * List tier2 spend report master details to get spend (Indirect) report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> indirectSpendReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails);

	/**
	 * List tier2 spend report master details to get diversity report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getDiversityReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails,String type);

	/**
	 * List tier2 spend report master details to get ethnicity breakdown report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getEthnicityBreakdownReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails);

	/**
	 * List tier2 spend report master details to get spend dashboard report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getSpendDashboardReport(
			Tier2ReportForm reportForm, UserDetailsDto appDetails);

	/**
	 * List tier2 spend report master details to get agency based report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getAgencyReport(Tier2ReportForm reportForm,
			UserDetailsDto appDetails);

	/**
	 * List tier2 Ethnicity Dashboard report master details.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getEthnicityDashboardReport(
			UserDetailsDto appDetails);

	/**
	 * List tier2 Diversity Dashboard report master details.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getDiversityDashboardReport(
			UserDetailsDto appDetails);

	/**
	 * List vendor based report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getSearchReport(
			SearchVendorForm searchVendorForm, UserDetailsDto appDetails);

	/**
	 * List Supplier count by state dashboard.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getSupplierStateDashboardReport(
			UserDetailsDto appDetails);

	/**
	 * List Supplier Agency by Spend report dashboard.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getAgencyDashboardReport(
			UserDetailsDto appDetails);

	/**
	 * List Tier2 Report Master data.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportMaster> getReportMasterList(
			UserDetailsDto appDetails, VendorContact tire2Vendor);

	/**
	 * Retrieve Tier2 Report Master Details.
	 * 
	 * @param id
	 * @param appDetails
	 * @return
	 */
	public Tier2ReportMaster retriveReportmaster(Integer id,
			UserDetailsDto appDetails);

	/**
	 * Update Tier2 Report.
	 * 
	 * @param form
	 * @param userDetails
	 * @param tire2Vendor
	 * @return
	 */
	public Tier2ReportMaster updateTier2Report(Tier2ReportForm form,
			UserDetailsDto userDetails, VendorContact tire2Vendor,
			String submitType);

	/**
	 * Get the dashboard details.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<DashboardSettings> listDashboardSettings(
			UserDetailsDto appDetails);

	/**
	 * Create Dashboard Settings.
	 * 
	 * @param reportForm
	 * @param userId
	 * @param userDetails
	 * @return
	 */
	public String createDashboardSettings(Tier2ReportForm reportForm,
			Integer userId, UserDetailsDto userDetails);

	/**
	 * Update Dashboard Settings.
	 * 
	 * @param reportForm
	 * @param userId
	 * @param userDetails
	 * @return
	 */
	public String updateDashboardSettings(Tier2ReportForm reportForm,
			Integer userId, UserDetailsDto userDetails);

	/**
	 * Status Dashboard.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getStatusDashboardReport(
			UserDetailsDto appDetails);

	/**
	 * 
	 * @param appDetails
	 * @param year
	 * @return
	 */
	public List<String> findReportingPeriod(UserDetailsDto appDetails,
			Integer year);
	
	
	public List<String> findReportingPeriodForMultipleYears(UserDetailsDto appDetails,
			String year);

	/**
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<String> spendYear(UserDetailsDto appDetails);
	
	/**
	 * List BP Vendor count by state dashboard.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getBpVendorCountDashboardReport(
			UserDetailsDto appDetails);	

	/**
	 * List previous Quarter Details.
	 * 
	 * @param appDetails
	 * @return
	 */
	public Tier2ReportMaster getPreviousQuarterDetails(
			UserDetailsDto appDetails, Integer vendorId);

	/**
	 * To Get Count of Vendors Commodities Not Saved Report.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorCommoditiesNotSavedReport(UserDetailsDto appDetails);
	
	/**
	 * To Get Count of Certificate Expiration Notification Email Report.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getCertificateExpirationNotificationEmailReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);
	
	/**
	 * To Retrieve Registered Vendors Report Data.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getRegisteredVendorsReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);
	
	/**
	 * To Retrieve Vendors By Business Type Data.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorsByIndustryReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);
	
	/**
	 * To Retrieve Vendors By NAICS Description Report Data.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorsByNaicsReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);
	
	/**
	 * To Retrieve Vendors By BP Market Sector Report Data.
	 * 
	 * @param reportForm
	 * @param appDetails
	 * @return
	 */
	public List<Tier2ReportDto> getVendorsByBPMarketSectorReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);
	
	
	public List<Tier2ReportDto> getVendorsByBPMarketSubsectorReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);
	
	public List<Tier2ReportDto> getVendorsByBPCommodityGroupReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);

	/**
	 * getReportSearchFields
	 * 
	 * getReportSearchFields
	 * @param userDetails
	 * @return
	 */
	public List<ReportSearchFieldsDto> getReportSearchFields(UserDetailsDto userDetails);

	/**
	 * getSearchFields
	 * 
	 * @param userDetails
	 * @param id
	 * @return
	 */
	public CustomReportSearchFields getSearchFields(UserDetailsDto userDetails,
			Integer id);

	public void getSearchReportByDynamicColumns(UserDetailsDto userDetails,SearchReportDto searchReportDto,String searchQUery);
	
	public Tier2ReportMaster saveTier2ReportMasterOnly(Tier2ReportForm form,
			UserDetailsDto userDetails, VendorContact vendor, String submitType);
	
	
	public String saveIndirectExpenditure(Double indirectAmount,
			String ethnicityName, VendorMaster tier2IndirectVendorid,
			String certificateName,Tier2ReportMaster tier2Reportmaster, UserDetailsDto userDetails,
			VendorContact vendor);
	
	public String saveDirectExpenditure(Double directAmount,String ethnicityName,
			VendorMaster tier2DirectVendorid, String certificateName,
			Tier2ReportMaster tier2Reportmaster, UserDetailsDto userDetails,
			VendorContact vendor);
	
	
	public String deleteUnsubmittedRecords(UserDetailsDto appDetails,Integer masterId);

	public List<Tier2ReportDto> getSpendReportHistory(UserDetailsDto userDetails, VendorContact contact);

	public List<Tier2ReportDto> getIndirectProcurementCommoditiesReport(Tier2ReportForm reportForm, UserDetailsDto appDetails);

	public List<Tier2ReportDto> listIndirectProcurementCommodities(UserDetailsDto appDetails);
	
	public String saveIndirectProcurementCommodityList(UserDetailsDto appDetails, Integer userId, Tier2ReportForm reportForm, String reportName);
	
	public List<Tier2ReportDto> listIndirectProcurementCertificateTypes(UserDetailsDto appDetails);
	
	public List<Tier2ReportDto> getDiversityReportBySectorAndEthnicity(Tier2ReportForm reportForm,
			UserDetailsDto appDetails, String type);
	
	public Tier2ReportMaster updateTier2ReportMasterOnly(Tier2ReportMaster tier2ReportMaster, UserDetailsDto userDetails, FormFile formFile);
}