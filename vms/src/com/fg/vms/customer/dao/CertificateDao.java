/*
 * CertificateDao.java 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.CertificateExpiration;
import com.fg.vms.customer.model.Certificate;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.CertificateForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * certificate page processing such as get list of certificates, update and save
 * certificates.
 * 
 * @author vinoth
 * 
 */
public interface CertificateDao {

	/**
	 * Create new certificate entry into the database. This certificate will be
	 * used by vendors to choose their diverse classification type.
	 * 
	 * @param certificateForm
	 * @param currentUserId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public String createCertificate(CertificateForm certificateForm,
			int currentUserId, UserDetailsDto appDetails);

	/**
	 * Retrieve list of active certificates from database.
	 * 
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * 
	 * @return certificate
	 */
	public List<Certificate> listOfCertificates(UserDetailsDto appDetails);

	/**
	 * Remove the selected certificate by the administrator level user.
	 * 
	 * @param id
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public String deleteCertificate(Integer id, UserDetailsDto appDetails);

	/**
	 * Retrieve the particular certificate based on id for update purpose.
	 * 
	 * @param id
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public Certificate retriveCertificate(Integer id, UserDetailsDto appDetails);

	/**
	 * Update certificate details (eg.. certificate name, short name, diverse
	 * type).
	 * 
	 * @param certificateForm
	 * @param id
	 * @param currentUserId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public String updateCertificate(CertificateForm certificateForm,
			Integer id, Integer currentUserId, UserDetailsDto appDetails);

	/**
	 * Get the list of active certificates from database based on diverse type.
	 * 
	 * @param diverseOrQuality
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public List<Certificate> listOfCertificatesByType(Byte diverseOrQuality,
			UserDetailsDto appDetails);

	/**
	 * Get the list of active certificates from database based on vendor.
	 * 
	 * @param diverseOrQuality
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public List<Certificate> listOfCertificatesByVendor(
			UserDetailsDto appDetails, VendorMaster master);

	public List<CertificateExpiration> certificateExpiration(
			UserDetailsDto appDetails, StringBuilder query);

	/**
	 * Delete Vendor Certificate
	 * 
	 * @param id
	 * @param appDetails
	 * @return
	 */
	public String deleteVendorCertificate(Integer id, UserDetailsDto appDetails);

	/**
	 * Delete Vendor Other/quality Certificate
	 * 
	 * @param id
	 * @param appDetails
	 * @return
	 */
	public String deleteVendorOtherCertificate(Integer id,
			UserDetailsDto appDetails);
}
