/**
 * SupplierDao.java
 */
package com.fg.vms.customer.dao;

import java.io.Serializable;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.CustomerVendorAddressMaster;
import com.fg.vms.customer.model.CustomerVendorMeetingInfo;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;

/**
 * Interface for supplier.
 * 
 * @author vinoth
 * 
 */
public interface SupplierDao extends Serializable {

	/**
	 * Create menu status for existing vendor don't have it.
	 * 
	 * @param vendorMaster
	 * @param userId
	 */
	public String createMenuStatusForVendor(VendorMaster vendorMaster,
			Integer userId, UserDetailsDto userDetails);

	public VendorMaster retreveVendorMaster(Integer supplierId,
			UserDetailsDto userDetails);

	public VendorContact retreveVendorContact(Integer supplierId,
			UserDetailsDto userDetails);

	public CustomerVendorMeetingInfo retreveVendorMeetingInfo(
			Integer supplierId, UserDetailsDto userDetails);

	public CustomerVendorAddressMaster retreveAddressMaster(Integer supplierId,
			UserDetailsDto userDetails);

}
