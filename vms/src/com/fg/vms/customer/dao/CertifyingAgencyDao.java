package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.CertifyingAgencyDto;
import com.fg.vms.customer.model.CertifyingAgency;
import com.fg.vms.customer.pojo.CertifyingAgencyForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * anonymous vendor page processing such as create new agency and view the list
 * of active agencies.
 * 
 * @author vinoth
 * 
 */
public interface CertifyingAgencyDao {

    /**
     * View list of active certifying agencies. Based on these list vendor can
     * choose their agency.
     * 
     * @param userDetails
     * 
     * @return
     */
    public List<CertifyingAgencyDto> view(UserDetailsDto appDetails);

    /**
     * Update a certifying agency. This agency details will be used by vendors.
     * 
     * @param certifyingAgencyDto
     * @param id
     * @param userDetails
     * 
     * @return
     */
    public String saveOrUpdate(CertifyingAgencyDto certifyingAgencyDto,
	    Integer id, UserDetailsDto appDetails);

    /**
     * Create a certifying agency. This agency details will be used by vendors.
     * 
     * @param currentUserId
     * @param appDetails
     *            - Its hold the current user application details.
     * @param configurationForm
     * 
     * @return
     */
    public String createagency(CertifyingAgencyForm certifyingAgencyForm,
	    int currentUserId, UserDetailsDto appDetails);
    
    /**
	 * Edit the selected certifyAgency.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public CertifyingAgency retrieveCertifyAgency(int certiftyAgencyId,
			UserDetailsDto userDetails);
	
	 /**
	  * Edit the selected certifyAgency.
      * 
	  * @param parseInt
	  * @param userDetails
	  * @return
	  */
	public String deleteCertifyAgency(int certiftyAgencyId,
				UserDetailsDto userDetails);
}
