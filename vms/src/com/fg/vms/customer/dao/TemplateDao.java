/*
 * TemplateDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.TemplateQuestions;
import com.fg.vms.customer.pojo.QuestionsForm;
import com.fg.vms.customer.pojo.TemplateQuestionsForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * Template Master page processing such as list of active templates,list of all
 * templates, list of vendor answered templates, list of templates all the
 * questions answered, save, update and modify template details.
 * 
 * @author vivek
 * 
 */
public interface TemplateDao {

    /**
     * Creates the new template.
     * 
     * @param questionsForm
     * @param currentUserId
     * @param userDetails
     * @return
     */
    public String createTemplate(QuestionsForm questionsForm,
	    int currentUserId, UserDetailsDto userDetails);

    /**
     * List all the active template name.
     * 
     * @param userDetails
     * @return
     */
    public List<Template> listTemplateNames(UserDetailsDto userDetails);

    /**
     * List all the template name regardless of active status.
     * 
     * @param userDetails
     * @return
     */
    public List<Template> allTemplateNames(UserDetailsDto userDetails);

    /**
     * Creates the question based on template name.
     * 
     * @param templateQuestionsForm
     * @param currentUserId
     * @param userDetails
     * @return
     */

    public String createTemplateQuestions(
	    TemplateQuestionsForm templateQuestionsForm, int currentUserId,
	    UserDetailsDto userDetails);

    /**
     * Fetches the template question list based on the template.
     * 
     * @param userDetails
     * @return
     */
    public List<TemplateQuestions> listTemplateQuestions(
	    UserDetailsDto userDetails, Integer id);

    /**
     * Retrieves the Template for edit or update.
     * 
     * @param id
     * @param userDetails
     * @return
     */
    public Template retriveTemplate(Integer id, UserDetailsDto userDetails);

    /**
     * Delete/Remove the selected template name. This will work only when
     * template do not have any active questions.
     * 
     * @param id
     * @param userDetails
     * @return
     */
    public String deleteTemplate(Integer id, UserDetailsDto userDetails);

    /**
     * Delete/Remove the active template questions.
     * 
     * @param id
     * @param userDetails
     * @return
     */
    public String deleteTemplateQuestion(Integer id, UserDetailsDto userDetails);

    /**
     * Update/modify the selected template name.
     * 
     * @param questionsForm
     * @param id
     * @param userDetails
     * @return
     */
    public String updateTemplate(QuestionsForm questionsForm, Integer id,
	    UserDetailsDto userDetails);

    /**
     * Updates the selected templates active status. This can change the status
     * of template into either active or inactive.
     * 
     * @param questionsForm
     * @param id
     * @param userDetails
     * @return
     */
    public String updatetemplateforactivestatus(Integer templateid,
	    Byte statusvalue, Integer userid, UserDetailsDto userDetails);

    /**
     * Retrieve the selected Template Questions for edit or update purpose.
     * 
     * @param id
     * @param userDetails
     * @return
     */
    public TemplateQuestions retriveTemplateQuestions(Integer id,
	    UserDetailsDto userDetails);

    /**
     * Update the selected Template Questions based on template.
     * 
     * @param templateQuestionsForm
     * @param id
     * @param userDetails
     * @return
     */
    public String updateTemplateQuestions(
	    TemplateQuestionsForm templateQuestionsForm, Integer id,
	    UserDetailsDto userDetails);

    /**
     * Saves the new template name for copying the existing template questions.
     * 
     * @param templateName
     * @param currentUserId
     * @param userDetails
     * @return
     */

    public Template saveTemplate(String templateName, int currentUserId,
	    UserDetailsDto userDetails);

    /**
     * copy the template questions and save under the new template name in db.
     * 
     * @param questionsForm
     * @param currentUserId
     * @param userDetails
     * @param templateId
     * @return
     */

    public String saveCopyTemplate(QuestionsForm questionsForm,
	    int currentUserId, UserDetailsDto userDetails, Integer templateId);

    /**
     * Fetches the template questions based on the template name.
     * 
     * @param userDetails
     * @return
     */

    public List<Template> listSelectTemplateQuestions(UserDetailsDto userDetails);

    /**
     * Fetches all the template name list except current template for copy and
     * move template from one template to others.
     * 
     * @param userDetails
     * @return
     */
    List<Template> getTemplateNamesforCopyandMove(UserDetailsDto userDetails,
	    Integer currentTemplateId);

    /**
     * Fetches the template name list who got assigned Assessment templates
     * 
     * @param userDetails
     * @return
     */
    List<Template> listAssessmentTemplates(UserDetailsDto userDetails);

    /**
     * Fetches the template name list who answered all the questions in a
     * template
     * 
     * @param userDetails
     * @return
     */
    List<Template> listAssessmentScoreTemplates(UserDetailsDto userDetails);
    
    /**
     * Fetches the template name list based on vendor selected in assessment review page.
     * 
     * @param userDetails
     * @param vendorId
     * @return
     */
    List<Template> listTemplatesBasedVendor(UserDetailsDto userDetails, Integer vendorId);
    
    /**
     * Fetches the list of score details for selected vendors
     * 
     * @param userDetails
     * @param vendorId
     * @return
     */
//    List<String> getAssessmentScoreDetails(
//			Integer scoreDetailTemplateId, Integer scoreDetailVendorId);

}
