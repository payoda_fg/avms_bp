package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.pojo.UserLogTrackForm;

public interface UserLogTrackDao {

	/**
	 * Setting User Login Details
	 * 
	 * @param userDetails
	 * @return
	 */
	public abstract List<UserLogTrackForm> setUserLoginDetails(
			UserDetailsDto userDetails);

	/**
	 * Getting User Login Details
	 * 
	 * @param userDetails
	 * @param cureentEmailId
	 * @return
	 */
	public abstract List<UserLogTrackForm> getUserLoginTimes(UserDetailsDto userDetails,
			String cureentEmailId);
}
