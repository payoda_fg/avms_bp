/**
 * EmailTemplateDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.EmailTemplateDto;
import com.fg.vms.customer.model.EmailTemplate;
import com.fg.vms.customer.model.EmailTemplateParameters;
import com.fg.vms.customer.model.EmailType;
import com.fg.vms.customer.model.EmailtemplateDivsion;
import com.fg.vms.customer.pojo.EmailTemplateForm;

/**
 * Interface for Email Template.
 * 
 * @author vinoth
 * 
 */
public interface EmailTemplateDao {

	/**
	 * Create Email Template.
	 * 
	 * @param emailTemplateForm
	 * @param userId
	 * @param userDetails
	 * @return
	 */
	public String createEmailTemplates(EmailTemplateForm emailTemplateForm,
			Integer userId, UserDetailsDto userDetails);

	/**
	 * List active email templates.
	 * 
	 * @param userDetails
	 * @return
	 */
	public List<EmailTemplate> listTemplates(UserDetailsDto userDetails);

	/**
	 * Delete the selected email template.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public String deleteEmailTemplate(int templateId, UserDetailsDto userDetails);

	/**
	 * Edit the selected email template.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public EmailTemplate editEmailTemplate(int templateId,
			UserDetailsDto userDetails);

	/**
	 * Update the email template.
	 * 
	 * @param emailTemplateForm
	 * @param userId
	 * @param userDetails
	 * @return
	 */
	public String updateEmailTemplates(EmailTemplateForm emailTemplateForm,
			Integer userId, UserDetailsDto userDetails);

	/**
	 * Show the email template message based on email template id.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public String showEmailTemplateMessage(int templateId,
			UserDetailsDto userDetails);
	
	/**
	 * Show the division(s) based on email template id.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public List<EmailtemplateDivsion> listEmailTemplateDivisions(int templateId,
			UserDetailsDto userDetails);
    
	/**
	 * Show the EmailTemplateParameter(s) based on email template id.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public List<EmailTemplateParameters> listEmailTemplateParameters(int templateId,
			UserDetailsDto userDetails);
	
	/**
	 * Show the EmailTemplateNames based on email template id.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public List<EmailType> listTemplateNames(UserDetailsDto userDetails);
	
	/**
	 * Method for get TemplateName from(Emailtype) based templateCode
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public EmailType retriveTemplateName(Integer templateCode,UserDetailsDto userDetails);	
	
	/**
	 * Method for get Active Email Templates and Divisions
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public List<EmailTemplateDto> listTemplatesWithDivision(UserDetailsDto userDetails);
	
	/**
	 * Method for Save New Email Templates with Divisions
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public String createNewEmailTemplates(EmailTemplateForm emailTemplateForm,Integer userId, UserDetailsDto userDetails);
	
	/**
	 * Method for Updating Email Templates and Divisions
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public String updateNewEmailTemplates(EmailTemplateForm emailTemplateForm,Integer userId,UserDetailsDto userDetails);
}
