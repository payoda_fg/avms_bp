/*
 *  Tier2ReportingPeriodDao.java
 */
package com.fg.vms.customer.dao;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.pojo.Tier2ReportingPeriodForm;

/**
 * The interface for Tier2Reporting Period.
 * 
 * @author vinoth
 * 
 */
public interface Tier2ReportingPeriodDao {

	/**
	 * Method to save the tier2 reporting period form.
	 * 
	 * @param userDetails
	 * @param userId
	 * @param periodForm
	 * @return
	 */
	public String saveTier2Period(UserDetailsDto userDetails, Integer userId,
			Tier2ReportingPeriodForm periodForm);

	public Tier2ReportingPeriod currentReportingPeriod(
			UserDetailsDto userDetails);

}
