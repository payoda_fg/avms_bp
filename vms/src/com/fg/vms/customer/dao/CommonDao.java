/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.common.Country;
import com.fg.vms.customer.model.VendorMaster;

/**
 * Represents the commonly used details (such as Currency details, County ..)
 * 
 * @author pirabu
 * 
 */
public interface CommonDao {

   
    /**
     * Retrieve all the countries
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return
     */
    public List<Country> getCountries(UserDetailsDto appDetails);

    /**
     * Retrive all the vendor names by category for assign the RFI.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return
     */
    public List<VendorMaster> getAllVendorsByCategory(Integer categoryId,
	    Integer subCategoryId, UserDetailsDto appDetails);

    /**
     * This method to retrieve the year of spend data upload
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * @return list of years
     */
    public List<Integer> getSpendDataUploadedYears(UserDetailsDto appDetails);
    
    
    /**
     * Validate login id.
     * 
     * @param loginId
     *            the login id
     * @param appDetails
     *            the app details
     * @return the string
     */
    public String validateLoginId(String loginId, UserDetailsDto appDetails);

    /**
     * Check duplicate email.
     * 
     * @param emailId
     *            the email id
     * @param appDetails
     *            the app details
     * @return the string
     */
    public String checkDuplicateEmail(String emailId, UserDetailsDto appDetails);
    
    /**
     * Implementing the code for validating the property vendor code as it is
     * already existing or not in database.
     * 
     * @param vendorCode
     *            the vendor code
     * @param appDetails
     *            the app details
     * @return the string
     */
    public String validateVendorCode(String vendorCode,
            UserDetailsDto appDetails);
}
