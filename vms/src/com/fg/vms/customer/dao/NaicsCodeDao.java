/*
 * NaicsCodeDao.java 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.NaicsCode;
import com.fg.vms.customer.pojo.NaicsCodeForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * Naics Code page processing such as list of active Naics codes, save, update
 * and modify Naics master details
 * 
 * @author vinoth
 * 
 */
public interface NaicsCodeDao {

	/**
	 * Save a Naics Code details into database. This will be used by vendor at
	 * the time of registration and search vendors (Unique Code).
	 * 
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @param naicsMasterForm
	 * @param currentUserId
	 * 
	 * @return
	 */
	public String saveNaicsCode(NaicsCodeForm naicsCodeForm, Integer userId,
			UserDetailsDto appDetails);

	public String editNaicsCode(String description, Integer id,
			Integer parentId, String naicscode, Integer userId,
			UserDetailsDto appDetails);

	/**
	 * Fetch list of active naics parents.
	 * 
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public List<NaicsCode> listNaicsParents(UserDetailsDto appDetails);

	public List<NaicsCode> listNaics(UserDetailsDto appDetails, String query);

	public List<NaicsCode> sizeByCode(UserDetailsDto appDetails,
			Integer parentId);

	public List<NaicsCode> leafNode(UserDetailsDto appDetails);

	public String uniqueNaicsCode(UserDetailsDto appDetails, String naicscode);

	public String deleteNaics(UserDetailsDto appDetails, Integer id);

}
