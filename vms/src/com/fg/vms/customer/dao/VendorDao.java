/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dto.ClassificationDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.dto.EmailHistoryDto;
import com.fg.vms.customer.dto.MeetingNotesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.dto.RFIRPFNotesDto;
import com.fg.vms.customer.dto.RetriveVendorInfoDto;
import com.fg.vms.customer.dto.SupplierDiversityDto;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerSearchFilter;
import com.fg.vms.customer.model.CustomerVendorSelfRegInfo;
import com.fg.vms.customer.model.CustomerVendorbusinessbiographysafety;
import com.fg.vms.customer.model.CustomerVendorreference;
import com.fg.vms.customer.model.MeetingNotes;
import com.fg.vms.customer.model.NAICSubCategory;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.model.RFIRPFNotes;
import com.fg.vms.customer.model.SupplierDiversity;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.CustomerServiceAreaInfo;
import com.fg.vms.customer.pojo.SupplierDiversityForm;
import com.fg.vms.customer.pojo.VendorAddressInfo;
import com.fg.vms.customer.pojo.VendorBussinessArea;
import com.fg.vms.customer.pojo.VendorBussinessBioDetails;
import com.fg.vms.customer.pojo.VendorContactInfo;
import com.fg.vms.customer.pojo.VendorDetailsBean;
import com.fg.vms.customer.pojo.VendorDiverseCertificate;
import com.fg.vms.customer.pojo.VendorMasterBean;
import com.fg.vms.customer.pojo.VendorMasterForm;
import com.fg.vms.customer.pojo.VendorOwnerDetails;
import com.fg.vms.customer.pojo.VendorStatusPojo;

/**
 * The Interface VendorDao.
 * 
 * @author srinivasarao
 */
public interface VendorDao {

	/**
	 * Save vendor.
	 * 
	 * @param vendorForm
	 *            the vendor form
	 * @param userId
	 *            the user id
	 * @param certifications
	 *            the certifications
	 * @param naicsCodes
	 *            the naics codes
	 * @param userdetails
	 *            the userdetails
	 * @param currentVendor
	 * @return the string
	 */
	public String saveVendor(VendorMasterForm vendorForm, Integer userId,
			List<DiverseCertificationTypesDto> certifications,
			List<NaicsCodesDto> naicsCodes, UserDetailsDto userdetails,
			String url, VendorContact currentVendor);

	public CustomerSearchFilter saveSearchFiltersInJsonForm(
			UserDetailsDto userDetails, String searchName, String searchType,
			String criteria, String sqlQuery, Integer userId,
			String criteriaJson);

	
	public String updateVendorMaster(UserDetailsDto userDetails, VendorMaster vendorMaster);
	/**
	 * Merge the vendor information.
	 * 
	 * @param vendorForm
	 *            the vendor form
	 * @param userId
	 *            the user id
	 * @param vendorId
	 *            the vendor id
	 * @param certifications
	 *            the certifications
	 * @param naicsCodes
	 *            the naics codes
	 * @param userdetails
	 *            the userdetails
	 * @param currentVendor
	 * @return the string
	 */
	public String mergeVendor(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId,
			List<DiverseCertificationTypesDto> certifications,
			List<NaicsCodesDto> naicsCodes, UserDetailsDto userdetails,
			VendorContact currentVendor, String url);

	public String mergeMoreInfo(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userdetails,
			VendorContact currentVendor);

	/**
	 * List naics category.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<NaicsCategory> listNaicsCategory(UserDetailsDto appDetails);

	/**
	 * List naics sub category.
	 * 
	 * @param findNaicsCode
	 *            the find naics code
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<NAICSubCategory> listNaicsSubCategory(int findNaicsCode,
			UserDetailsDto appDetails);

	/**
	 * List naics sub category.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<NAICSubCategory> listNaicsSubCategory(UserDetailsDto appDetails);

	/**
	 * List naics master.
	 * 
	 * @param categoryId
	 *            the category id
	 * @param subCategoryId
	 *            the sub category id
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<NaicsMaster> listNAICSMaster(int categoryId, int subCategoryId,
			UserDetailsDto appDetails);

	/**
	 * get list of prime vendor vendors.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<VendorMaster> listVendors(UserDetailsDto appDetails);

	/**
	 * List of uninvited vendors.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<VendorMaster> listOfUninvitedVendors(UserDetailsDto appDetails,
			Integer invited);

	/**
	 * List vendorsfor spent data.
	 * 
	 * @param vendor
	 *            the vendor
	 * @param appDetails
	 *            the app details
	 * @return the list
	 */
	public List<VendorMaster> listVendorsforSpentData(VendorContact vendor,
			UserDetailsDto appDetails);

	/**
	 * Retrive vendor info.
	 * 
	 * @param vendorId
	 *            the vendor id
	 * @param appDetails
	 *            the app details
	 * @return the retrive vendor info dto
	 */
	public RetriveVendorInfoDto retriveVendorInfo(Integer vendorId,
			UserDetailsDto appDetails);

	/**
	 * Retrive vendor contact.
	 * 
	 * @param id
	 *            the id
	 * @param appDetails
	 *            the app details
	 * @return the vendor contact
	 */
	public VendorContact retriveVendorContact(Integer id,
			UserDetailsDto appDetails);

	/**
	 * Retrive vendor primary contact email id based on sql query.
	 * 
	 * @param appDetails
	 * @param query
	 * @return
	 */
	public List<EmailDistribution> retriveEmailIds(UserDetailsDto appDetails,
			String query);

	/**
	 * Delete contact.
	 * 
	 * @param contactId
	 *            the contact id
	 * @param appDetails
	 *            the app details
	 * @return the string
	 */
	public String deleteContact(Integer contactId, UserDetailsDto appDetails);

	/**
	 * Save contact.
	 * 
	 * @param vendorMaster
	 *            the vendor master
	 * @param contactInfo
	 *            the contact info
	 * @param userId
	 *            the user id
	 * @param appDetails
	 *            the app details
	 * @return the string
	 */
	public String saveContact(VendorMaster vendorMaster,
			VendorContactInfo contactInfo, Integer userId,
			UserDetailsDto appDetails);

	/**
	 * Update contact.
	 * 
	 * @param vendorMaster
	 *            the vendor master
	 * @param contactInfo
	 *            the contact info
	 * @param userId
	 *            the user id
	 * @param contactId
	 *            the contact id
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String updateContact(VendorMaster vendorMaster,
			VendorContactInfo contactInfo, Integer userId, Integer contactId,
			UserDetailsDto userDetails);

	/**
	 * Gets the customer application settings.
	 * 
	 * @param userDetails
	 *            the user details
	 * @return the customer application settings
	 */
	public CustomerApplicationSettings getCustomerApplicationSettings(
			UserDetailsDto userDetails);

	/**
	 * Delete the vendor.
	 * 
	 * @param id
	 *            the id
	 * @param appDetails
	 *            the app details
	 * @return the string
	 */
	public String deleteVendor(Integer id, UserDetailsDto appDetails);

	/**
	 * Count prime vendors.
	 * 
	 * @param appDetails
	 *            the app details
	 * @return the long
	 */
	public Long countPrimeVendors(UserDetailsDto appDetails);

	/**
	 * Get biography list.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public List<CustomerVendorbusinessbiographysafety> getBiography(
			Integer vendorId, UserDetailsDto appDetails, String type);

	/**
	 * Get biography sequence list.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @param questionnumber
	 * @return
	 */
	public List<CustomerVendorbusinessbiographysafety> getBiographySequence(
			Integer vendorId, UserDetailsDto appDetails, Integer questionnumber);

	/**
	 * Get the list of references based on vendor.
	 * 
	 * @param vendorId
	 * @param appDetails
	 * @return
	 */
	public List<CustomerVendorreference> getListReferences(Integer vendorId,
			UserDetailsDto appDetails);

	/**
	 * Get the saved vendor data.
	 * 
	 * @param vendorForm
	 * @param userDetails
	 * @return
	 */
	public VendorContact getSavedVendorData(String emailId,
			UserDetailsDto userDetails);

	/**
	 * Get the saved vendor data.
	 * 
	 * @param id
	 * @param appDetails
	 * @return
	 */
	public VendorMaster getSavedVendorMasterData(Integer id,
			UserDetailsDto appDetails);

	/**
	 * Get business contact details.
	 * 
	 * @param id
	 * @param appDetails
	 * @return
	 */
	public List<VendorContact> getBusinessContactDetails(Integer id,
			UserDetailsDto appDetails);

	/**
	 * Get the agency and certificate type data based on classification for ajax
	 * drop down.
	 * 
	 * @param vendorForm
	 * @param appDetails
	 * @return
	 */
	public List<ClassificationDto> getClassifcationDetails(
			VendorMasterForm vendorForm, UserDetailsDto appDetails);

	/**
	 * Method to get the Email Id and send Login credentials to vendor by self
	 * registration.
	 * 
	 * @param appDetails
	 * @param vendorForm
	 * @param prime
	 * @return
	 */
	public String newRegistration(UserDetailsDto appDetails,
			VendorMasterForm vendorForm, String password, String vendorType);

	/**
	 * Save meeting details.
	 * 
	 * @param appDetails
	 * @param vendorForm
	 * @return
	 */
	public String saveMeetingDetails(UserDetailsDto appDetails,
			VendorMasterForm vendorForm, Integer vendorId);

	/**
	 * List all the active and Bp vendors.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<VendorMaster> listActiveVendors(UserDetailsDto appDetails);

	public String savePrimeVendor(VendorMasterForm vendorForm, Integer userId,
			UserDetailsDto appDetails, String url);

	public List<VendorMasterBean> listVendorsDetails(UserDetailsDto appDetails,
			Integer vendorId);

	public List<VendorAddressInfo> listVendorsAddressDetails(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorContactInfo> listVendorsContactInfo(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorOwnerDetails> listVendorsOwnerDetails(
			UserDetailsDto appDetails, Integer vendorId);

	public String vendorsDiverseClassification(UserDetailsDto appDetails,
			Integer vendorId);

	public List<VendorDiverseCertificate> listVendorsDivCertificate(
			UserDetailsDto appDetails, Integer vendorId);

	public List<CustomerServiceAreaInfo> listVendorServiceArea(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorBussinessArea> listVendorsBussinessArea(
			UserDetailsDto appDetails, Integer vendorId);

	public VendorBussinessBioDetails listVendorsBussinessBiography(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorDetailsBean> listVendorsReference(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorDetailsBean> listVendorsOtherCertificates(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorDetailsBean> listVendorsMeetingInfo(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorDetailsBean> listBusinessAreaCommodityInfo(
			UserDetailsDto appDetails, Integer vendorId);

	public List<VendorDetailsBean> listServiceAreaNaicsInfo(
			UserDetailsDto appDetails, Integer vendorId);

	/**
	 * Method to get the Vendor Name.
	 * 
	 * @param appDetails
	 * @param vendorId
	 * @return
	 */
	public String getVendorName(UserDetailsDto appDetails, int vendorId);

	/**
	 * Method to update Prime vendor
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param appDetails
	 * @return
	 */
	public String updatePrimeVendor(VendorMasterForm vendorForm,
			Integer userId, UserDetailsDto appDetails);

	/**
	 * Method to get the Vendor Status.
	 * 
	 * @param appDetails
	 * @param vendorId
	 * @return
	 */
	public List<VendorStatusPojo> listVendorStatus(UserDetailsDto appDetails,
			Integer vendorId);

	/**
	 * Method to get Registration Requests.
	 * 
	 * @param appDetails
	 * @param divisionId
	 * @return
	 */
	public String populateRegistrationRequest(UserDetailsDto appDetails,
			Integer divisionId);

	/**
	 * Method to get registration Questions.
	 * 
	 * @param appDetails
	 * @param divisionId
	 * @return
	 */
	public String populateRegistrationQuestions(UserDetailsDto appDetails,
			Integer divisionId);

	/**
	 * Update Self Registration Information Count.
	 * 
	 * @param appDetails
	 * @param selfRegInfo
	 * @return the string
	 */
	public String updateContact(UserDetailsDto appDetails,
			CustomerVendorSelfRegInfo selfRegInfo);

	/**
	 * saveSearchFilters
	 * 
	 * @param userDetails
	 * @param searchName
	 * @param searchType
	 * @param criteria
	 * @param sqlQuery
	 * @param userId
	 * @return
	 */
	public CustomerSearchFilter saveSearchFilters(UserDetailsDto userDetails,
			String searchName, String searchType, String criteria,
			String sqlQuery, Integer userId);

	/**
	 * @param userDetails
	 * @param searchName
	 * @param searchType
	 * @param userId
	 * @return
	 */
	public CustomerSearchFilter findByFilterName(UserDetailsDto userDetails,
			String searchName, String searchType, Integer userId);

	/**
	 * @param appDetails
	 * @param vendorId
	 * @return
	 */
	public List<VendorDetailsBean> listServiceAreaKeywordsInfo(
			UserDetailsDto appDetails, Integer vendorId);

	/**
	 * deleteSearchFilter
	 * 
	 * @param userDetails
	 * @param searchId
	 * @return
	 */
	public int deleteSearchFilter(UserDetailsDto userDetails, String searchId);

	/**
	 * set List of Metting Info to Dto
	 * 
	 * @param userDetails
	 * @param searchId
	 * @return
	 */
	public List<MeetingNotesDto> setMeetingInfo_To_Dto(
			UserDetailsDto userDetails, String query);

	public MeetingNotes editMeetingNotes(UserDetailsDto userDetails,
			Integer vendorNotesId);

	public RFIRPFNotes editRfiMeetingNotes(UserDetailsDto userDetails,
					Integer vendorRfiNotesId);

	public String save_Or_Update_MeetingNotes(UserDetailsDto userDetails,
			MeetingNotesDto meetingDto);
	
	public String save_Or_Update_RfiMeetingNotes(UserDetailsDto userDetails,
			RFIRPFNotesDto meetingDto);
			
	public String deleteMeetingNote(UserDetailsDto userDetails,
			Integer vendorNotesId);
	
	public String deleteRfiMeetingNote(UserDetailsDto userDetails,
			Integer vendorRfiNotesId);

	/**
	 * Retrieve Vendor Details By Vendor Id.
	 * 
	 * @param userDetails
	 * @param vendorId
	 * @return
	 */
	public VendorMaster retrieveVendorById(UserDetailsDto userDetails,
			Integer vendorId);

	/**
	 * Set List of Email History Info to DTO.
	 * 
	 * @param userDetails
	 * @param vendorPrimaryEmailId
	 * @return
	 */
	public List<EmailHistoryDto> setEmailHistoryInfo_To_Dto(
			UserDetailsDto userDetails, String vendorPrimaryEmailId);

	/**
	 * To Retrieve list of Vendors.
	 * 
	 * @param userDetails
	 * @param vendorId
	 * @return
	 */
	public List<VendorMasterBean> retrieveVendorsList(UserDetailsDto userDetails);
	
	public String saveSupplierDiversity(UserDetailsDto appDetails, SupplierDiversityForm supplierDiversityForm, Users users);
	
	public List<SupplierDiversityDto> retrieveSupplierDiversityList(UserDetailsDto userDetails , Integer userId);
	
	public SupplierDiversity retrieveSupplierDiversity(UserDetailsDto userDetails, Integer supplierDiversityId);
	
	public String deleteSupplierDiversity(UserDetailsDto userDetails, SupplierDiversity supplierDiversity);
	
	public JSONArray retrieveCommoditySearchList(UserDetailsDto appDetails, String commoditySearchValue);

	public JSONArray retrieveVendorList(UserDetailsDto appDetails, String commodityCode);

	public JSONArray retrieveCommodityCodeData(UserDetailsDto appDetails, Integer id);

	public Map<String, String> commodityUpdateSearchList(UserDetailsDto appDetails, String[] commodityData);

	public Map<String, String> getVendorList(UserDetailsDto appDetails, String[] vendorData);

	public List<RFIRPFNotesDto> setRfiMeetingInfo_To_Dto(UserDetailsDto userDetails,
			String query);

}