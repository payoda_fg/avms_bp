/*
 * EmailDistributionDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.EmailDistribution;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.pojo.EmailDistributionForm;

/**
 * Create new email distribution group to the database.
 * 
 * @author vinoth
 * 
 */
public interface EmailDistributionDao {

    /**
     * Method to Save a new Email Distribution
     * 
     * @param itemsForm
     * @param userid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String createEmailDistribution(
            EmailDistributionForm distributionForm, int currentUserId,
            UserDetailsDto appDetails);

    /**
     * Method to get the List of vendor Email Ids to create Email Distribution.
     * 
     * @return
     */
    public List<EmailDistribution> viewVendorEmailList(UserDetailsDto appDetails);
    
    /**
     * @param appDetails
     * @param divisionIds
     * @return
     */
    public List<EmailDistribution> viewVendorEmailList(UserDetailsDto appDetails,String divisionIds);

    public List<EmailDistribution> findByEmailDistributionId(
            UserDetailsDto appDetails, Integer id,String type);

    /**
     * Method to retrieve the selected email distribution.
     * 
     * @param appDetails
     * @param emailDistId
     * @return
     */
    public EmailDistributionModel retriveEmailDistribution(
            UserDetailsDto appDetails, int emailDistId);

    /**
     * Method to retrieve the selected email distribution details.
     * 
     * @param appDetails
     * @param emailDistId
     * @return
     */
    public EmailDistributionListDetailModel retriveEmailDistributionDetail(
            UserDetailsDto appDetails, int emailDistId);

    /**
     * Method to Update a existing Email Distribution
     * 
     * @param itemsForm
     * @param userid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String updateEmailDistribution(
            EmailDistributionForm distributionForm, int currentUserId,
            UserDetailsDto appDetails);

    /**
     * Method to delete the email distribution master record.
     * 
     * @param emailDistributionId
     * @param appDetails
     *            - Its hold the current user application details.
     * @return
     */
    String deleteEmailDistributionList(Integer emailDistributionId,
            UserDetailsDto appDetails);
}
