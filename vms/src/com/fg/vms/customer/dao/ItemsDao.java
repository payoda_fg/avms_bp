package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.Items;
import com.fg.vms.customer.pojo.ItemForm;

/**
 * Interface
 * 
 * @author hemavaishnavi
 * 
 */
public interface ItemsDao {
    /**
     * Method to register a new Item
     * 
     * @param itemsForm
     * @param userid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String addItem(ItemForm itemsForm, Integer userid,
	    UserDetailsDto appDetails);

    /**
     * Method to retrieve the all items in the DB.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return items
     */
    public List<Items> listItems(UserDetailsDto appDetails);

    /**
     * Method to delete the selected item
     * 
     * @param itemid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String deleteItem(Integer itemid, UserDetailsDto appDetails);

    /**
     * Method to update item details.
     * 
     * @param itemsForm
     * @param itemid
     * @param userid
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String updateItem(ItemForm itemsForm, Integer itemid,
	    Integer userid, UserDetailsDto appDetails);

    /**
     * Method to retrieve the particular item based on ItemId.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * @param ItemId
     * 
     * @return result
     */
    public Items retriveItem(Integer itemId, UserDetailsDto appDetails);

}
