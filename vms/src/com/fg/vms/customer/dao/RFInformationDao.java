package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.RFIPDto;
import com.fg.vms.customer.pojo.RFIInformationForm;

/**
 * 
 * @author pirabu
 * 
 */
public interface RFInformationDao {

	/**
	 * Persist the RFInformation (such as documents, compliance).
	 * 
	 * @param informationForm
	 * @param documents
	 * @param compliances
	 * @param userId
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public String persistRFI(RFIInformationForm informationForm,
			List<?> documents, List<?> compliances, Integer userId,
			UserDetailsDto appDetails);

	/**
	 * Retrieve all the RF information.
	 * 
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * 
	 * @return
	 */
	public List<RFIPDto> retrieveRFInformations(UserDetailsDto appDetails);

	/**
	 * 
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return
	 */
	public List<RFIPDto> retrieveRFPInformations(UserDetailsDto appDetails);

	    /**
     * 
     * @param requedtIPId
     * @param appDetails
     *            - Its hold the current user application details.
     * @return
     */
	public RFIPDto retriveRFIP(Integer requedtIPId, UserDetailsDto appDetails);

}
