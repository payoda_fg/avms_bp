/*
 * NaicsCategoryDao.java 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.NaicsCategoryDto;
import com.fg.vms.customer.model.NaicsCategory;
import com.fg.vms.customer.pojo.NaicsCategoryForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * Naics Category page processing such as list of active categories, save,
 * update and modify category details
 * 
 * @author vinoth
 * 
 */
public interface NaicsCategoryDao {

    /**
     * View list of active NAICS categories. This list will be used by vendors.
     * 
     * @param appDetails
     * @return
     */
    public List<NaicsCategoryDto> view(UserDetailsDto appDetails);

    /**
     * Update Naics category master.
     * 
     * @param naicsCategoryDto
     * @param id
     * @param appDetails
     * @return
     */
    public String submit(NaicsCategoryDto naicsCategoryDto, Integer id,
	    UserDetailsDto appDetails);

    /**
     * Add new Naics Category. This will be used by vendor on registration.
     * 
     * @param usersForm
     * @param userDetails
     *            - Its hold the current user application details.
     * @param userid
     * 
     * @return result
     */

    public String addCategory(NaicsCategoryForm naicsCategoryForm,
	    int currentUserId, UserDetailsDto appDetails);

    /**
     * Used to retrieve list of categories from database. This will be used for
     * search vendors based on NAICS Category.
     * 
     * @param userDetails
     *            - Its hold the current user application details.
     * 
     * @return categories
     */
    public List<NaicsCategory> listCategory(UserDetailsDto userDetails);

    /**
     * Delete the selected category by the user. This category will no long
     * appear in category list.
     * 
     * @param id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String deleteCategory(Integer id, UserDetailsDto userDetails);

    /**
     * Retrieve the particular category selected by user for update purpose.
     * 
     * @param id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public NaicsCategory retriveCategory(Integer id, UserDetailsDto userDetails);

    /**
     * Update the selected category details selected by user.
     * 
     * @param configurationForm
     * @param id
     * @param currentUserId
     * @param userDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String updateSelectedCategory(NaicsCategoryForm categoryForm,
	    Integer id, Integer currentUserId, UserDetailsDto userDetails);
}
