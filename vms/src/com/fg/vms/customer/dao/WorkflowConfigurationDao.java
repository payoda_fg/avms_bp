/* 
 * WorkflowConfigurationDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;

import org.hibernate.Session;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.CustomerWflog;
import com.fg.vms.customer.model.EmailDistributionModel;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.customer.pojo.WorkflowConfigurationForm;

/**
 * The interface class for Workflow Configuration.
 * 
 * @author vinoth
 * 
 */
public interface WorkflowConfigurationDao {

	/**
	 * View list of active email distribution list. This list will be used by
	 * vendors.
	 * 
	 * @param appDetails
	 * @return
	 */
	public List<EmailDistributionModel> view(UserDetailsDto appDetails);

	/**
	 * Save Workflow configuration settings
	 * 
	 * @param workflowConfigurationForm
	 * @param currentUserId
	 * @param appDetails
	 * @return
	 */
	public String createWorkflowConfigurationSettings(
			WorkflowConfigurationForm workflowConfigurationForm,
			int currentUserId, UserDetailsDto appDetails);

	/**
	 * Update Workflow configuration settings
	 * 
	 * @param workflowConfigurationForm
	 * @param currentUserId
	 * @param appDetails
	 * @return
	 */
	public String updateWorkflowConfigurationSettings(
			WorkflowConfigurationForm workflowConfigurationForm,
			int currentUserId, UserDetailsDto appDetails);

	/**
	 * To view the list of Workflow configurations.
	 * 
	 * @param appDetails
	 * @return
	 */
	public WorkflowConfiguration listWorkflowConfig(UserDetailsDto appDetails);

	/**
	 * To get the list of email in distribution list.
	 * 
	 * @param distribution
	 * @return
	 */
	public List<String> distributionListEmail(UserDetailsDto appDetails);

	public void updateCustomerWfLog(VendorMaster vendor, String type,
			UserDetailsDto appDetails);

	public CustomerWflog findByVendorId(UserDetailsDto appDetails, Integer id);

	public void followWorkFlow(Session session, UserDetailsDto appDetails,
			VendorMaster vendorMaster, String url, Integer user);

	public void followWorkFlowForApprove(Session session,
			UserDetailsDto appDetails, VendorMaster vendorMaster, String url,String modifiedstatus,
			Integer user);
}
