/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.Ethnicity;

/**
 * @author gpirabu
 * 
 */
public interface CURDDao<T> {

    public T save(T entity, UserDetailsDto usDetails);

    public T update(T entity, UserDetailsDto usDetails);

    public T delete(T entity, UserDetailsDto usDetails);

    public List<T> list(UserDetailsDto usDetails, String query);
    
    public List<?> findAllByQuery(UserDetailsDto usDetails, String query);
    
    public T update1(T entity, String name, UserDetailsDto usDetails);
    
    public T delete1(T entity, String name, UserDetailsDto usDetails);

    public T find(UserDetailsDto usDetails, String query);
    
    public List<Ethnicity> listEthnicities(UserDetailsDto usDetails);

	public List<T> listCommoditiesBasedOnSubSector(UserDetailsDto usDetails, Integer marketSubSectorId);

}
