/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.model.AnonymousVendor;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.pojo.VendorMasterForm;

/**
 * @author gpirabu
 * 
 */
public interface Tier2VendorDao {

    /**
     * Save tire2 vendor short form.
     * 
     * @param vendorForm
     *            the vendor form
     * @param userId
     *            the user id
     * @param vendorId
     *            the vendor id
     * @param certifications
     *            the certifications
     * @param naicsCodes
     *            the naics codes
     * @param currentVendor
     *            the current vendor
     * @param userdetails
     *            the userdetails
     * @return the string
     */
    public String saveShortTire2Vendor(VendorMasterForm vendorForm,
            Integer userId, Integer vendorId,
            List<DiverseCertificationTypesDto> certifications,
            List<NaicsCodesDto> naicsCodes, VendorContact currentVendor,
            UserDetailsDto userdetails, String url, String[] divCert,
            String address, String city, String contactPhone,
            String contanctEmail, String deverseSupplier, String firstName,
            String lastName, String middleName, String state,
            String vendorName, String zipcode,String country);

    /**
     * Tire2 vendors.
     * 
     * @param appDetails
     *            the app details
     * @return the list
     */
    public List<VendorMaster> tire2Vendors(UserDetailsDto appDetails);

    /**
     * Download anonymous vendor certificate.
     * 
     * @param id
     *            the id
     * @param userDetails
     *            the user details
     * @return the anonymous vendor
     */
    public AnonymousVendor downloadAnonymousVendorCertificate(Integer id,
            UserDetailsDto userDetails);

    /**
     * Check sub vendor.
     * 
     * @param parentVendorId
     *            the parent vendor id
     * @param vendorId
     *            the vendor id
     * @param userDetails
     *            the user details
     * @return the string
     */
    public String checkSubVendor(Integer parentVendorId, Integer vendorId,
            UserDetailsDto userDetails);
    
    
	public Object idFromName(UserDetailsDto usdeDetailsDto,String query);
	
	/**
     * Save tire2 vendor short form.
     * 
     * @param vendorForm
     *            the vendor form
     * @param userId
     *            the user id
     * @param vendorId
     *            the vendor id
     * @param certifications
     *            the certifications
     * @param naicsCodes
     *            the naics codes
     * @param currentVendor
     *            the current vendor
     * @param userdetails
     *            the userdetails
     * @return the string
     */
    public VendorMaster saveExcelTire2Vendor(VendorMasterForm vendorForm,
            Integer userId, Integer vendorId,
            List<DiverseCertificationTypesDto> certifications,
            List<NaicsCodesDto> naicsCodes, VendorContact currentVendor,
            UserDetailsDto userdetails, String url, String divCert,
            String address, String city, String contactPhone,
            String contanctEmail, String deverseSupplier, String firstName,
            String lastName, String middleName, String state,
            String vendorName, String zipcode,String country);

}
