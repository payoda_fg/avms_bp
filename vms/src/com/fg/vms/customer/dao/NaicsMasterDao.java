/*
 * NaicsMasterDao.java 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.NaicsMaster;
import com.fg.vms.customer.pojo.NaicsMasterForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * Naics Master page processing such as list of active Naics codes, save, update
 * and modify Naics master details
 * 
 * @author vinoth
 * 
 */
public interface NaicsMasterDao {

    /**
     * Save a Naics Master details into database. This will be used by vendor at
     * the time of registration and search vendors (Unique Code).
     * 
     * @param appDetails
     *           - Its hold the current user application details.
     * @param certificateForm
     * @param currentUserId
     * 
     * @return
     */
    public String saveNaicsMaster(NaicsMasterForm naicsMasterForm, int userId,
	    int currentCategoryId, int subCategoryId, UserDetailsDto appDetails);

    /**
     * Delete/Remove the Selected NAICS Master Record and it's details.
     * 
     * @param id
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String deleteRecord(Integer id, UserDetailsDto appDetails);

    /**
     * Retrieve list of active NAICS Master Records from database.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return certificate
     */
    public List<NaicsMaster> listOfNAICS(UserDetailsDto appDetails);

    /**
     * Search category and sub category based on unique naicscode from database.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return certificate
     */
    public List<NaicsMaster> search(int categoryId, int subCategoryId,
	    UserDetailsDto appDetails);

    public List<NaicsMaster> naicsMastersByIds(int categoryId,
	    int subCategoryId, UserDetailsDto appDetails);

    /**
     * Retrieve the selected NAICS Master Record and their details.
     * 
     * @param id
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public NaicsMaster retriveNaicsMaster(Integer id, UserDetailsDto appDetails);

    /**
     * Update the selected NAICS Master Record and their details.
     * 
     * @param naicsMasterForm
     * @param currentUserId
     * @param appDetails
     *            - Its hold the current user application details.
     * @param id
     * @return result
     */
    public String updateNAICSMaster(NaicsMasterForm naicsMasterForm,
	    Integer id, Integer categoryid, Integer subCategoryid,
	    Integer currentUserId, UserDetailsDto appDetails);

    /**
     * Implemented code for get the NaicsMaster object for Find Naics Codes
     * 
     * @param naicsId
     * @param appDetails
     * @return
     */
    public NaicsMaster naicsMaster(Integer naicsId, UserDetailsDto appDetails);
    
    /**
     * Check duplicate Naics code.
     * 
     * @param naicsCode
     *            the Naics code
     * @param userDetails
     *            the user details
     * @return the string
     */
    public String checkDuplicateNaicsCode(String naicsCode,
			UserDetailsDto userDetails);

}
