/*
 * CapabilityAssessmentDao.java
 */
package com.fg.vms.customer.dao;

import java.util.List;
import java.util.Map;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.AssessmentQuestionsDto;
import com.fg.vms.customer.model.Template;
import com.fg.vms.customer.model.VendorMaster;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * capability assessment page processing such as get template names and save
 * assessment answers.
 * 
 * @author Vinoth
 * 
 */
public interface CapabilityAssessmentDao {

	/**
	 * Get the List of active template names.
	 * 
	 * @return
	 */
	public Map<Template, List<AssessmentQuestionsDto>> getTemplate(
			UserDetailsDto userDetails, VendorMaster vendor);

	/**
	 * verify and get the unique vendor ID. This vendor id is used to get the
	 * contact details of the vendor
	 * 
	 * @return
	 */
	public Integer getVendorId(Integer vendorUserId, UserDetailsDto userDetails);

	/**
	 * Get the Data type of the questions from the templates. Based on this data
	 * type vendor will answer the questions.
	 * 
	 * @return
	 */
	public String getDataType(Integer templateQuestionsId,
			UserDetailsDto userDetails);

	/**
	 * Save the assessment answers which is answered by the vendor.
	 * 
	 * @return
	 */
	public String saveAssessmentAnswers(Integer vendorId, Integer vendorUserId,
			Integer templateId, Integer templateQuestionsId,
			UserDetailsDto userDetails, String description);

	/**
	 * 
	 * @param vendor
	 * @return
	 */
	public Boolean assessmentCompleted(VendorMaster vendor);

}
