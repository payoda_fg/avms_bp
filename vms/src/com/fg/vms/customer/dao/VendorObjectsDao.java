package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.VendorObjects;
import com.fg.vms.customer.pojo.VendorObjectsForm;

public interface VendorObjectsDao {
    /**
     * Method to create an object
     * 
     * @param configurationForm
     * @param currentUserId
     * @param appDetails
     *            - Its hold the current user application details.
     * @return
     */
    public String createObject(VendorObjectsForm configurationForm,
	    int currentUserId, UserDetailsDto appDetails);

    /**
     * Method used to retrieve list of objects from DB.
     * 
     * @param appDetails
     *            - Its hold the current user application details.
     * 
     * @return objects
     */
    public List<VendorObjects> listObjects(UserDetailsDto appDetails);

    /**
     * Method to delete the selected object
     * 
     * @param id
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String deleteObject(Integer id, UserDetailsDto appDetails);

    /**
     * Method to retrieve the particular object based on id.
     * 
     * @param id
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public VendorObjects retriveObject(Integer id, UserDetailsDto appDetails);

    /**
     * Method to update object details.
     * 
     * @param configurationForm
     * @param id
     * @param currentUserId
     * @param appDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String updateObject(VendorObjectsForm configurationForm, Integer id,
	    Integer currentUserId, UserDetailsDto appDetails);

}
