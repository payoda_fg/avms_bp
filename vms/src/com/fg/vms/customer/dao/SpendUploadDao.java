/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.ArrayList;
import java.util.List;

import com.fg.vms.admin.dto.SpendDataDTO;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.SpendIndirectDto;
import com.fg.vms.customer.model.SpendDataMaster;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.pojo.SpendUploadForm;

/**
 * Defines the functionality that should be provided by the Service Layer to
 * vendor user and customer for upload spend information..
 * 
 * @author pirabu
 */
public interface SpendUploadDao {

    /**
     * Upload spend data.
     * 
     * @param form
     *            the form
     * @param userDetails
     *            the user details
     * @param userId
     *            the user id
     * @param filePath
     *            the file path
     * @param currentVendor
     *            - Its hold the current user application details.
     * @return the spend data master
     */
    public SpendDataMaster uploadSpendData(SpendUploadForm form,
	    UserDetailsDto userDetails, Integer userId, String filePath,
	    VendorContact currentVendor);

    /**
     * Save uploaded spend data.
     * 
     * @param userDetails
     *            the user details
     * @param userId
     *            the user id
     * @param al_spendDataList
     *            the al_spend data list
     * @param spendUpload
     *            the spend upload
     * @return the string
     */
    public String saveUploadedSpendData(UserDetailsDto userDetails,
	    Integer userId, ArrayList<SpendDataDTO> al_spendDataList,
	    SpendDataMaster spendUpload);

    /**
     * Save pro rate supply.
     * 
     * @param al_spendIndirectDataList
     *            the al_spend indirect data list
     * @param spendDataMaster
     *            the spend data master
     * @param appDetails
     *            the app details
     * @return the string
     */
    public String saveProRateSupply(
	    ArrayList<SpendIndirectDto> al_spendIndirectDataList,
	    SpendDataMaster spendDataMaster, UserDetailsDto appDetails);

    /**
     * Update totalPrimeSupplierSales and totalCustomerSales in SpendDataMaster
     * table.
     * 
     * @param spendUploadForm
     *            the spend upload form
     * @param userDetails
     *            the user details
     * @param currentSpendMaster
     *            the current spend master
     */
    public void updateSpendDataMaster(SpendUploadForm spendUploadForm,
	    UserDetailsDto userDetails, SpendDataMaster currentSpendMaster);

    /**
     * Retrieve uploaded spend data.
     * 
     * @param userDetails
     *            the user details
     * @param currentUser
     *            the current user
     * @return the list
     */
    public List<?> retrieveUploadedSpendData(UserDetailsDto userDetails,
	    VendorContact currentUser);

}
