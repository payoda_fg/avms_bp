/*
 * AnonymousVendorDao.java 
 */
package com.fg.vms.customer.dao;

import java.io.FileNotFoundException;
import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.dto.DiverseCertificationTypesDto;
import com.fg.vms.customer.dto.NaicsCodesDto;
import com.fg.vms.customer.pojo.VendorMasterForm;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * anonymous vendor page processing such as get approve new vendors, save
 * anonymous vendor search the vendors got first level approval.
 * 
 * @author vinoth
 * 
 */
public interface AnonymousVendorDao {

	/**
	 * First level approval of anonymous vendor after verifying their details.
	 * 
	 * @param id
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public String approveNewVendor(Integer id, UserDetailsDto userDetails);

	/**
	 * Merge vendor contact.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeVendorContact(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url);

	/**
	 * Merge vendor information.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeVendorInformation(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url);

	/**
	 * Merge vendor address.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeVendorAddress(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url);

	/**
	 * Merge vendor owner information.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeVendorOwnerInfo(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url);

	/**
	 * Merge vendor service area.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeServiceArea(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userDetails, String url,
			List<NaicsCodesDto> naicsCodes);

	/**
	 * Merge Business area.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeBusinessArea(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url);

	/**
	 * Merge diverse classification data.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param url
	 * @return
	 */
	public String mergeDiverseClassification(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			String url, List<DiverseCertificationTypesDto> certifications);

	/**
	 * Merge business biography and safety data.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @return
	 */
	public String mergeBusinessBiography(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails);

	/**
	 * Merge vendor references data.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @return
	 */
	public String mergeReferences(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userDetails);

	/**
	 * Merge vendor other certificates.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @return
	 */
	public String mergeOtherCertificate(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails);
	
	/**
	 * Add vendor netWorld  certificates.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @return
	 * @throws FileNotFoundException 
	 */
	public String addVendorNetWorldCertificate(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails);

	/**
	 * Merge vendor documents.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @return
	 */
	public String mergeDocuments(VendorMasterForm vendorForm, Integer userId,
			Integer vendorId, UserDetailsDto userDetails);

	/**
	 * Submit self registration.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @param vendorErrors
	 * @return
	 */
	public List<String> submitRegistration(VendorMasterForm vendorForm,
			Integer userId, Integer vendorId, UserDetailsDto userDetails,
			List<String> vendorErrors);

	/**
	 * Save Self registered prime vendor data.
	 * 
	 * @param userDetails
	 * @param vendorForm
	 * @param id
	 * @param userId
	 * @return
	 */
	public String savePrimeVendorData(UserDetailsDto userDetails,
			VendorMasterForm vendorForm, Integer vendorId, Integer userId);

	/**
	 * Save/Update Pics Certificate.
	 * 
	 * @param vendorForm
	 * @param userId
	 * @param vendorId
	 * @param userDetails
	 * @return
	 */
	public String savePicsCertificate(VendorMasterForm vendorForm, Integer userId, Integer vendorId, UserDetailsDto userDetails);

}
