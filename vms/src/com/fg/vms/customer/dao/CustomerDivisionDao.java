/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.dto.CustomerUsersDivisionDto;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.pojo.CustomerDivisionForm;

/**
 * interface for Customer Divisions
 * 
 * @author SHEFEEK.A
 * 
 */
public interface CustomerDivisionDao {

	/**
	 * add the new customer division
	 * 
	 * @param customerDivisionForm
	 * @param userId
	 * @param userdetails
	 * @return
	 */
	public String addDivision(CustomerDivisionForm customerDivisionForm,
			Integer userId, UserDetailsDto userdetails);

	/**
	 * update the customer division
	 * 
	 * @param customerDivisionForm
	 * @param currentUser
	 * @param userdetails
	 * @return
	 */
	public String updateDivision(CustomerDivisionForm customerDivisionForm,
			Integer currentUser, UserDetailsDto userdetails);

	/**
	 * edit the selected customer division
	 * 
	 * @param divisionId
	 * @param userDetails
	 * @return
	 */
	public CustomerDivision editDivision(int divisionId,
			UserDetailsDto userDetails);

	/**
	 * * Delete the selected customer division
	 * 
	 * @param divisionId
	 * @param userId
	 * @param userdetails
	 * @return
	 */
	public String deleteDivision(int divisionId, Integer userId,
			UserDetailsDto userdetails);

	/**
	 * Retrieve Customer Division List
	 * 
	 * @param userdetails
	 * @return
	 */
	public List<CustomerDivision> retrieveDivision(UserDetailsDto userdetails);

	/**
	 * Retrieve Customer Division List for All
	 * 
	 * @param userdetails
	 * @return
	 */
	public List<CustomerDivision> retrieveDivisionAll(UserDetailsDto userdetails);

	/**
	 * To check customer division status for current user
	 * @param userDetails
	 * @param userId 
	 * @return
	 */
	public List<Users> checkCustomerDivisionStatus(
			UserDetailsDto userDetails, Integer userId);

	/**
	 * @param userDetails
	 * @return
	 */
	public Long checkDivisionStatus(UserDetailsDto userDetails);

	/**
	 * @param userDetails
	 * @param userId
	 * @param userIds
	 * @param divisionId
	 * @return
	 */
	public String assignDivisionIdToUsers(UserDetailsDto userDetails,
			Integer userId, String userIds, Integer divisionId);

	/**
	 * @param userDetails
	 * @param divisionId
	 * @return
	 */
	public List<CustomerUsersDivisionDto> listUsers(UserDetailsDto userDetails, Integer divisionId);

	/**
	 * @param userDetails
	 * @param userId
	 * @param userIds
	 * @param divisionId
	 * @return
	 */
	public String removeDivisionIdFromUsers(UserDetailsDto userDetails,
			Integer userId, String userIds, Integer divisionId);

	/**
	 * @param userDetails
	 * @return
	 */
	public List<CustomerUsersDivisionDto> listUsersByDivision(
			UserDetailsDto userDetails);
}
