/**
 * 
 */
package com.fg.vms.customer.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.customer.model.CustomerCertificateType;
import com.fg.vms.customer.pojo.CertificationTypeForm;

/**
 * @author venkatesh
 * 
 */
public abstract interface CertificationTypeDao {

	/**
	 * Create Messages.
	 * 
	 * @param certificationTypeForm
	 * @param userId
	 * @param userDetails
	 * @return
	 */
	public abstract String createCertificationType(
			CertificationTypeForm certificationTypeForm, Integer userId,
			UserDetailsDto userDetails);

	/**
	 * List active Messages.
	 * 
	 * @param userDetails
	 * @return
	 */
	public List<CustomerCertificateType> listCertificationType(
			UserDetailsDto userDetails);

	/**
	 * Delete the selected Messages.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public String removeCertificationType(int certificateId,
			UserDetailsDto userDetails);

	/**
	 * Edit the selected Message.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public CustomerCertificateType retrieveCertificationType(int certificateId,
			UserDetailsDto userDetails);

	/**
	 * Update the Message.
	 * 
	 * @param emailTemplateForm
	 * @param userId
	 * @param userDetails
	 * @return
	 */
	public String updateCertificationType(
			CertificationTypeForm emailTemplateForm, Integer userId,
			UserDetailsDto userDetails);

	/**
	 * Show the message based on certificateId.
	 * 
	 * @param parseInt
	 * @param userDetails
	 * @return
	 */
	public String viewCertificationType(int certificateId,
			UserDetailsDto userDetails);

}
