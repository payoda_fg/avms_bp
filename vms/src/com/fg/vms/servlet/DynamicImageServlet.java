package com.fg.vms.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fg.vms.util.Constants;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Servlet implementation class DynamicImageServlet
 */
public class DynamicImageServlet extends HttpServlet {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			// Get image file.
			String file = request.getParameter("file");
			if ((new File(Constants.getString(Constants.UPLOAD_LOGO_PATH)
					+ File.separator + file)).exists()) {

				BufferedInputStream in = new BufferedInputStream(
						new FileInputStream(
								Constants.getString(Constants.UPLOAD_LOGO_PATH)
										+ File.separator + file));

				// Get image contents.
				byte[] bytes = new byte[in.available()];
				in.read(bytes);
				in.close();
				// Write image contents to response.
				response.getOutputStream().write(bytes);
			}
		} catch (IOException e) {
			PrintExceptionInLogFile.printException(e);
		}
	}
}
