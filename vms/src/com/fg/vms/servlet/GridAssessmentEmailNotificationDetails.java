/*
 * GridAssessmentEmailNotificationDetails.java
 */
package com.fg.vms.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorServlet;
import com.dhtmlx.connector.GridConnector;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The Class GridAssessmentEmailNotificationDetails.
 * 
 * @author Vinoth
 */
public class GridAssessmentEmailNotificationDetails extends ConnectorServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The db_name. */
    String db_name;

    /** The user details. */
    UserDetailsDto userDetails;

    /** The gettemplateemaildetail. */
    Integer gettemplateemaildetail;
    String serverUrl;

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#configure()
     */
    @Override
    protected void configure() {
        Connection conn = DataBaseConnection.getConnection(userDetails);

        GridConnector c = new GridConnector(conn);
       // c.dynamic_loading(100);
        c.event.attach(new RenderingAssessment());

        String sqlQuery = "SELECT customervendorassessment.ID as row_id,customer_vendormaster.VENDORNAME, "
                + "DATE_FORMAT(vendor_email_notification_master.EMAILDATE, '%m/%d/%Y') AS EMAILDATE , customervendorassessment.ISREVIEWED, "
                + "(CASE WHEN ISNULL(customervendorassessment.REVIEWEDBY) then '' else  users.USERNAME end) ReviewedBy,round( avg(customervendorassessment.ANSWERWEIGHTAGEAWARDED),2) as ANSWERWEIGHTAGEAWARDED,"
                + "(CASE WHEN customervendorassessment.ISREVIEWED = 0 THEN 'review^"
                + serverUrl
                + "/assessmentreview.do?method=reviewTemplate&templateid="
                + gettemplateemaildetail
                + "' ELSE '' END) AS ReviewNow "
                + ",customer_vendormaster.ID as vendorId FROM customervendorassessment  INNER JOIN customer_vendormaster "
                + "ON customervendorassessment.VENDORID = customer_vendormaster.ID "
                + "INNER JOIN vendor_email_notification_master "
                + "ON customervendorassessment.VENDOREMAILNOTIFICATIONMASTERID = vendor_email_notification_master.ID "
                + "INNER JOIN users ON (customervendorassessment.REVIEWEDBY = users.ID OR isNull(customervendorassessment.REVIEWEDBY)) ";

        if (gettemplateemaildetail != null) {
            sqlQuery = sqlQuery
                    + " WHERE customervendorassessment.TEMPLATEID = "
                    + gettemplateemaildetail +" group by customer_vendormaster.VENDORNAME";
        }

        c.render_sql(
                sqlQuery,
                "customervendorassessment.ID(row_id)",
                "VENDORNAME,EMAILDATE,ISREVIEWED,ReviewedBy,ANSWERWEIGHTAGEAWARDED,ReviewNow,vendorId");

        gettemplateemaildetail = null;
        try {
            conn.close();
        } catch (SQLException e) {
            PrintExceptionInLogFile.printException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        userDetails = (UserDetailsDto) session.getAttribute("userDetails");

        serverUrl = req.getRequestURL().toString()
                .replaceAll(req.getServletPath(), "").trim();
        if (req.getParameter("getTemplate") != null) {
            gettemplateemaildetail = Integer.parseInt(req.getParameter(
                    "getTemplate").toString());
        }
        super.doGet(req, res);
    }

}
