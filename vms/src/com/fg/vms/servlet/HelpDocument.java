package com.fg.vms.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fg.vms.util.Constants;

/**
 * Its used to display the customer support pdf files.
 * 
 * @author gpirabu
 * 
 */
public class HelpDocument extends HttpServlet {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	private static void copy(InputStream is, OutputStream os)
			throws IOException {
		byte buffer[] = new byte[8192];
		int bytesRead;
		while ((bytesRead = is.read(buffer)) != -1) {
			os.write(buffer, 0, bytesRead);
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String file = request.getParameter("file");

		if (null != file
				&& !file.isEmpty()
				&& (new File(
						Constants
								.getString(Constants.UPLOAD_CUSTOMERDOCUMENT_PATH)
								+ File.separator + file)).exists()) {
			FileInputStream baos = new FileInputStream(
					Constants.getString(Constants.UPLOAD_CUSTOMERDOCUMENT_PATH)
							+ File.separator + file);
			// set some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control",
					"must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");
			response.setContentType("application/pdf");
			OutputStream os = response.getOutputStream();
			copy(baos, os);
			os.flush();
			os.close();
		}
	}
}
