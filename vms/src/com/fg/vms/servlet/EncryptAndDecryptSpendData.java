/*
 * EncryptAndDecryptSpendData.java
 */
package com.fg.vms.servlet;

import java.util.Random;

import com.dhtmlx.connector.ConnectorBehavior;
import com.dhtmlx.connector.DataAction;
import com.dhtmlx.connector.DataItem;
import com.dhtmlx.connector.GridDataItem;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the data to encrypted and decrypt the data .
 * 
 * @author pirabu
 */
public class EncryptAndDecryptSpendData extends ConnectorBehavior {

    /**
     * Encrypt the data.
     * 
     * @param action
     *            the action
     */
    @Override
    public void beforeProcessing(DataAction action) {
        Random random = new java.util.Random();
        Integer randVal = random.nextInt();

        Encrypt encrypt = new Encrypt();

        /* Encrypted spend value. */
        try {
            if (!action.get_value("SPENDVALUE").isEmpty()) {
                /* Get the Spend value form Grid. */
                String spendAmtFromGrid = action.get_value("SPENDVALUE");
                /* Remove the comma in spend value form Grid. */
                String afterRemoved = "";
                for (int i = 0; i < spendAmtFromGrid.length(); i++) {
                    if (spendAmtFromGrid.charAt(i) != ',') {
                        afterRemoved += spendAmtFromGrid.charAt(i);
                    }
                }
                String temp = afterRemoved.replace('$', ' ');

                Double spendAmt = Double.parseDouble(temp.toString().trim());

                String spendValue = encrypt.encryptText(
                        Integer.toString(randVal) + "", spendAmt.toString());
                action.set_value("SPENDVALUE", spendValue.toString());
                action.set_value("KEYVALUE", randVal.toString());
            }
            if (action.get_value("ISVALID").equalsIgnoreCase("Yes")) {
                (action).set_value("ISVALID", "1");
            } else {
                action.set_value("ISVALID", "0");
            }
            if (action.get_value("SPENDVALUE").isEmpty()) {
                action.invalid();
            }
        } catch (Exception e) {
            PrintExceptionInLogFile.printException(e);
            action.invalid();
        }

    }

    /**
     * Decrypt the data.
     * 
     * @param data
     *            the data
     */
    @Override
    public void beforeRender(DataItem data) {

        String keyvalue = data.get_value("KEYVALUE");
        Decrypt decrypt = new Decrypt();
        String spendValue = data.get_value("SPENDVALUE");

        try {
            spendValue = decrypt.decryptText(
                    String.valueOf(keyvalue.toString()),
                    data.get_value("SPENDVALUE"));
        } catch (Exception e) {
            PrintExceptionInLogFile.printException(e);
        }

        ((GridDataItem) data).set_value("SPENDVALUE", spendValue.toString());

        /* Show the IS Valid field in grid yes or no instead of 0 or 1 */
        if (data.get_value("ISVALID").equalsIgnoreCase("1")) {
            ((GridDataItem) data).set_value("ISVALID", "Yes");
        } else {

            ((GridDataItem) data).set_value("ISVALID", "No");

        }
    }

}
