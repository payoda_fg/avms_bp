/**
 * GridReviewTemplates.java
 */
package com.fg.vms.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorServlet;
import com.dhtmlx.connector.GridConnector;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The Class GridReviewTemplates.
 * 
 * @author Vinoth
 */
public class GridReviewTemplates extends ConnectorServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The db_name. */
    String db_name;

    /** The user details. */
    UserDetailsDto userDetails;

    /** The customer id. */
    Integer customerId = null;

    /** The selected vendor for review. */
    Integer selectedVendorForReview = null;

    /** The request. */
    HttpServletRequest request;

    /** The templatenameforreview. */
    Integer templateNameForReview = null;

    /** The isReviewedeforreview. */
    Integer isReviewedeforreview = null;

    /** The current date. */
    Date currentDate = new Date(new java.util.Date().getTime());

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#configure()
     */
    @Override
    protected void configure() {
        Connection conn = DataBaseConnection.getConnection(userDetails);

        GridConnector c = new GridConnector(conn);
        c.dynamic_loading(100);
        c.event.attach(new ReviewerDetails(request));

        String sqlQuery = "SELECT DISTINCT cva.ID as row_id, templatequestions.QUESTIONDESCRIPTION,"
                + "(CASE cva.ANSWERDESCRIPTION WHEN '0' THEN 'No' WHEN '1' THEN 'Yes'"
                + "ELSE cva.ANSWERDESCRIPTION END) AS ANSWERDESCRIPTION,"
                + "templatequestions.ANSWERWEIGHTAGE,cva.ISREVIEWED,"
                + "cva.REVIEWERREMARKS,cva.ANSWERWEIGHTAGEAWARDED,cva.REVIEWEDBY,cva.REVIEWEDON "
                + "FROM customervendorassessment cva INNER JOIN customer_vendor_users ON "
                + "cva.ANSWEREDBY = customer_vendor_users.ID INNER JOIN vendor_email_notification_master VENM "
                + "ON VENM.id = cva.VENDOREMAILNOTIFICATIONMASTERID "
                + "and VENM.ASSESSMENTTEMPLATEMASTERID = cva.TEMPLATEID "
                + "INNER JOIN templatequestions ON templatequestions.ID = cva.TEMPLATEQUESTIONSID "
                + "WHERE VENM.CREATEDBY = "
                + customerId
                + " AND cva.VENDORID = " + selectedVendorForReview;

        if (isReviewedeforreview != null && isReviewedeforreview == 0) {
            sqlQuery = sqlQuery + " AND cva.ISREVIEWED = 0";
        } else if (isReviewedeforreview != null && isReviewedeforreview == 1) {
            sqlQuery = sqlQuery + " AND cva.ISREVIEWED = 1";
        }

        if (templateNameForReview != null && templateNameForReview != 0) {
            sqlQuery = sqlQuery + " AND VENM.ASSESSMENTTEMPLATEMASTERID = "
                    + templateNameForReview;
        }

        sqlQuery = sqlQuery + " ORDER BY templatequestions.QUESTIONORDER ASC";

        c.render_sql(
                sqlQuery,
                "cva.ID(row_id)",
                "QUESTIONDESCRIPTION,ANSWERDESCRIPTION,ANSWERWEIGHTAGE,ISREVIEWED,REVIEWERREMARKS,ANSWERWEIGHTAGEAWARDED,REVIEWEDBY,REVIEWEDON");
        templateNameForReview = null;
        isReviewedeforreview = null;

        try {
            conn.close();
        } catch (SQLException e) {
            PrintExceptionInLogFile.printException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        request = req;
        HttpSession session = req.getSession();
        userDetails = (UserDetailsDto) session.getAttribute("userDetails");
        if (session.getAttribute("userId") != null) {
            customerId = (Integer) session.getAttribute("userId");
        }
        if (session.getAttribute("selectedVendorForReview") != null) {
            selectedVendorForReview = (Integer) session
                    .getAttribute("selectedVendorForReview");
        }
        if (session.getAttribute("templatenameforreview") != null) {
            templateNameForReview = Integer.parseInt(session.getAttribute(
                    "templatenameforreview").toString());
        }
        if (session.getAttribute("isReviewedeforreview") != null) {
            isReviewedeforreview = Integer.parseInt(session.getAttribute(
                    "isReviewedeforreview").toString());
            session.removeAttribute("templatenameforreview");
            session.removeAttribute("isReviewedeforreview");
        }
        super.doGet(req, res);
    }
}
