/**
 * GridAssessmentScoreDetail.java
 */
package com.fg.vms.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorServlet;
import com.dhtmlx.connector.GridConnector;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The Class GridAssessmentScoreDetail.
 * 
 * @author Vinoth
 */
public class GridAssessmentScoreDetail extends ConnectorServlet {

    /** Default serial version ID. */
    private static final long serialVersionUID = 1L;

    /** The db_name. */
    String db_name;

    /** The user details. */
    UserDetailsDto userDetails;

    /** The score detail template id. */
    Integer scoreDetailTemplateId;

    /** The score detail vendor id. */
    Integer scoreDetailVendorId;

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#configure()
     */
    @Override
    protected void configure() {
        Connection conn = DataBaseConnection.getConnection(userDetails);

        GridConnector c = new GridConnector(conn);
        c.dynamic_loading(100);
        String sqlQuery = "SELECT customervendorassessment.ID as row_id, templatequestions.QUESTIONORDER,"
                + "templatequestions.QUESTIONDESCRIPTION,"
                + "(CASE customervendorassessment.ANSWERDESCRIPTION WHEN '0' THEN 'No' WHEN '1' THEN 'Yes'"
                + "ELSE customervendorassessment.ANSWERDESCRIPTION END) AS ANSWERDESCRIPTION,"
                + "customervendorassessment.REVIEWERREMARKS,customervendorassessment.ANSWERWEIGHTAGEAWARDED "
                + "FROM templatequestions INNER JOIN customervendorassessment "
                + "ON templatequestions.ID = customervendorassessment.TEMPLATEQUESTIONSID ";

        if (scoreDetailTemplateId != null) {
            sqlQuery = sqlQuery
                    + " WHERE customervendorassessment.TEMPLATEID = "
                    + scoreDetailTemplateId;
        }

        if (scoreDetailVendorId != null) {
            sqlQuery = sqlQuery + " AND customervendorassessment.VENDORID = "
                    + scoreDetailVendorId;
        }

        sqlQuery = sqlQuery + " ORDER BY templatequestions.QUESTIONORDER ASC";

        c.render_sql(
                sqlQuery,
                "customervendorassessment.ID(row_id)",
                "QUESTIONORDER,QUESTIONDESCRIPTION,ANSWERDESCRIPTION,REVIEWERREMARKS,ANSWERWEIGHTAGEAWARDED");

        scoreDetailTemplateId = null;
        scoreDetailVendorId = null;
        try {
            conn.close();
        } catch (SQLException e) {
            PrintExceptionInLogFile.printException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        userDetails = (UserDetailsDto) session.getAttribute("userDetails");

        if (session.getAttribute("scoreDetailTemplateId") != null) {
            scoreDetailTemplateId = (Integer) session
                    .getAttribute("scoreDetailTemplateId");
        }

        if (session.getAttribute("scoreDetailVendorId") != null) {
            scoreDetailVendorId = (Integer) session
                    .getAttribute("scoreDetailVendorId");
        }

        super.doGet(req, res);
    }

}
