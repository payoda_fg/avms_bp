/**
 * GridTemplateQuestions.java
 */
package com.fg.vms.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorServlet;
import com.dhtmlx.connector.GridConnector;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The Class GridTemplateQuestions.
 * 
 * @author vinoth
 */
public class GridTemplateQuestions extends ConnectorServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The db_name. */
    String db_name;

    /** The user details. */
    UserDetailsDto userDetails;

    /** The template id. */
    Integer templateId = null;

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#configure()
     */
    @Override
    protected void configure() {
        Connection conn = DataBaseConnection.getConnection(userDetails);

        GridConnector c = new GridConnector(conn);
        c.dynamic_loading(100);
        c.render_sql(
                "SELECT ID as row_id, QUESTIONORDER,QUESTIONDESCRIPTION,ANSWERDATATYPE,ANSWERWEIGHTAGE FROM templatequestions WHERE TEMPLATEID= "
                        + templateId + " ORDER BY QUESTIONORDER ASC",
                "ID(row_id)",
                "QUESTIONORDER,QUESTIONDESCRIPTION,ANSWERDATATYPE,ANSWERWEIGHTAGE");
        // close db connections
        try {
            conn.close();
        } catch (SQLException e) {
            PrintExceptionInLogFile.printException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        HttpSession session = req.getSession();
        userDetails = (UserDetailsDto) session.getAttribute("userDetails");
        if (session.getAttribute("QuestionId") != null) {
            templateId = (Integer) session.getAttribute("QuestionId");
            session.removeAttribute("QuestionId");
        }
        super.doGet(req, res);
    }

}
