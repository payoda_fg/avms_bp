/**
 * GridAssessmentScoreCard.java
 */
package com.fg.vms.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorServlet;
import com.dhtmlx.connector.GridConnector;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The Class GridAssessmentScoreCard.
 * 
 * @author Vinoth
 */
public class GridAssessmentScoreCard extends ConnectorServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The db_name. */
    String db_name;

    /** The user details. */
    UserDetailsDto userDetails;

    /** The gettemplateofscore. */
    Integer templateOfScore;

    String serverUrl;

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#configure()
     */
    @Override
    protected void configure() {
        Connection conn = DataBaseConnection.getConnection(userDetails);

        GridConnector c = new GridConnector(conn);
        c.dynamic_loading(100);
        c.event.attach(new RenderingAssessmentScoreDetail());
        String sqlQuery = "SELECT customervendorassessment.ID as row_id,customer_vendormaster.VENDORNAME AS VENDORNAME, "
                + "DATE_FORMAT(vendor_email_notification_master.EMAILDATE, '%m/%d/%Y') AS EMAILDATE, "
                + " ROUND( avg(customervendorassessment.ANSWERWEIGHTAGEAWARDED),2) as ANSWERWEIGHTAGEAWARDED,"
                + "'viewdetails^"
                + serverUrl
                + "/scoreviewdetails.do?method=viewdetailsforscore&templateid="
                + templateOfScore
                + "' AS Details "
                + ",customer_vendormaster.ID as vendorId FROM customervendorassessment INNER JOIN customer_vendormaster "
                + "ON customervendorassessment.VENDORID = customer_vendormaster.ID "
                + "INNER JOIN vendor_email_notification_master "
                + "ON customervendorassessment.VENDOREMAILNOTIFICATIONMASTERID = vendor_email_notification_master.ID "
                + "INNER JOIN users ON vendor_email_notification_master.CREATEDBY = users.ID ";

        if (templateOfScore != null) {
            sqlQuery = sqlQuery
                    + " WHERE customervendorassessment.TEMPLATEID = "
                    + templateOfScore;
        }

        sqlQuery = sqlQuery
                + " group by customer_vendormaster.VENDORNAME ORDER BY customer_vendormaster.VENDORNAME ASC ";

        c.render_sql(sqlQuery, "customervendorassessment.ID(row_id)",
                "VENDORNAME,EMAILDATE,ANSWERWEIGHTAGEAWARDED,Details,vendorId");

        templateOfScore = null;
        try {
            conn.close();
        } catch (SQLException e) {
            PrintExceptionInLogFile.printException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        userDetails = (UserDetailsDto) session.getAttribute("userDetails");
        serverUrl = req.getRequestURL().toString()
                .replaceAll(req.getServletPath(), "").trim();
        if (session.getAttribute("getTemplateForScore") != null) {
            templateOfScore = (Integer) session
                    .getAttribute("getTemplateForScore");
        }
        super.doGet(req, res);
    }

}
