/*
 * RenderingSpendIndirectData.java
 */
package com.fg.vms.servlet;

import java.text.NumberFormat;

import com.dhtmlx.connector.ConnectorBehavior;
import com.dhtmlx.connector.DataItem;
import com.dhtmlx.connector.GridDataItem;

/**
 * Represents the spend value to be encrypted and decrypt.
 * 
 * @author pirabu
 */
public class RenderingSpendIndirectData extends ConnectorBehavior {

    /**
     * Encrypt the data.
     * 
     * @param data
     *            the data
     */
    // @Override
    // public void beforeProcessing(DataAction action) {
    // // Integer randVal = new java.util.Random().nextInt();
    // //
    // // Encrypt encrypt = new Encrypt();
    //
    // // encrypted spend value
    //
    // try {
    // if (!action.get_value("SPENDVALUE").isEmpty()) {
    // // Get the Spend value form Grid.
    // String spendAmtFromGrid = action.get_value("SPENDVALUE");
    // // Remove the comma in spend value form Grid
    // String afterRemoved = "";
    // for (int i = 0; i < spendAmtFromGrid.length(); i++) {
    // if (spendAmtFromGrid.charAt(i) != ',') {
    // afterRemoved += spendAmtFromGrid.charAt(i);
    // }
    // }
    // String temp = afterRemoved.replace('$', ' ');
    //
    // Double spendAmt = Double.parseDouble(temp.toString().trim());
    // //System.out.println(spendAmt);
    // // String spendValue = encrypt.encryptText(
    // // Integer.toString(randVal) + "", spendAmt.toString());
    // action.set_value("SPENDVALUE", spendAmt.toString());
    // //action.set_value("KEYVALUE", randVal.toString());
    // }
    // if (action.get_value("ISVALID").equalsIgnoreCase("Yes")) {
    // (action).set_value("ISVALID", "1");
    // } else {
    // action.set_value("ISVALID", "0");
    // }
    // if (action.get_value("SPENDVALUE").isEmpty()) {
    // action.invalid();
    // }
    // } catch (Exception e) {
    // // e.printStackTrace();
    // action.invalid();
    // }
    //
    // }

    /**
     * Decrypt the data.
     */
    @Override
    public void beforeRender(DataItem data) {

        String indirectAmtBasedOnDirectsales = data
                .get_value("INDIRECTAMTBASEDONDIRECTSALES");
        String indirectSpendValue = data.get_value("INDIRECTSPENDVALUE");
        String proRateAmtBasedOnSupply = data
                .get_value("PRORATEAMTBASEDONSUPPLY");
        NumberFormat formatter = null;
        try {

            formatter = NumberFormat.getCurrencyInstance();
        } catch (Exception e) {

            e.printStackTrace();
        }

        ((GridDataItem) data)
                .set_value("INDIRECTAMTBASEDONDIRECTSALES", formatter
                        .format(Double
                                .parseDouble(indirectAmtBasedOnDirectsales
                                        .toString())));
        ((GridDataItem) data).set_value("INDIRECTSPENDVALUE", formatter
                .format(Double.parseDouble(indirectSpendValue.toString())));
        ((GridDataItem) data)
                .set_value("PRORATEAMTBASEDONSUPPLY", formatter.format(Double
                        .parseDouble(proRateAmtBasedOnSupply.toString())));

    }
}
