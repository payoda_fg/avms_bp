/**
 * ReviewerDetails.java
 */
package com.fg.vms.servlet;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorBehavior;
import com.dhtmlx.connector.DataAction;

/**
 * The Class ReviewerDetails.
 * 
 * @author Vinoth
 */
public class ReviewerDetails extends ConnectorBehavior {

    /** The request. */
    HttpServletRequest request;

    /**
     * Update the reviewer Details.
     * 
     * @param action
     *            the action
     */
    @Override
    public void beforeProcessing(DataAction action) {

        HttpSession session = request.getSession();

        Integer userId = (Integer) session.getAttribute("userId");

        Date currentDate = new Date(new java.util.Date().getTime());

        try {
            if (userId != null && userId != 0) {
                action.set_value("REVIEWEDBY", userId.toString());
                action.set_value("REVIEWEDON", currentDate.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            action.invalid();
        }

    }

    /**
     * To get the request object.
     * 
     * @param request
     *            the request
     */
    public ReviewerDetails(HttpServletRequest request) {
        this.request = request;
    }

}
