/*
 * RenderingAssessment.java
 */
package com.fg.vms.servlet;

import com.dhtmlx.connector.ConnectorBehavior;
import com.dhtmlx.connector.DataItem;
import com.dhtmlx.connector.GridDataItem;

/**
 * The Class RenderingAssessment.
 */
public class RenderingAssessment extends ConnectorBehavior {

    /**
     * Decrypt the data.
     * 
     * @param data
     *            the data
     */
    @Override
    public void beforeRender(DataItem data) {

	String vendorId = data.get_value("vendorId");
	String review = "";
	if (data.get_value("ReviewNow") != null
		&& data.get_value("ReviewNow").length() != 0) {
	    review = data.get_value("ReviewNow") + "&vendorId="
		    + Integer.parseInt(vendorId);
	}

	((GridDataItem) data).set_value("ReviewNow", review);

    }
}
