/**
 * GridSpendReport.java
 */
package com.fg.vms.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dhtmlx.connector.ConnectorServlet;
import com.dhtmlx.connector.GridConnector;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * The Class GridSpendReport.
 * 
 * @author pirabu
 */
public class GridSpendReport extends ConnectorServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The db_name. */
    String db_name;

    /** The user details. */
    UserDetailsDto userDetails;

    /** The selected spend master id. */
    Integer selectedSpendMasterId = null;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure() {
        Connection conn = DataBaseConnection.getConnection(userDetails);

        GridConnector c = new GridConnector(conn);
        c.dynamic_loading(100);
        c.render_sql(
                "SELECT a.ID as row_id ,a.VENDORNAME AS SUPPLIER, b.VENDORNAME AS TIER2SUPPLIER, "
                        + "CUSTOMER_SPENDDATA_UPLOADDETAIL.DIVERSECERTIFICATE AS DIVERSECERTIFICATE, "
                        + "sum( CUSTOMER_SPENDDATA_UPLOADDETAIL.SPENDVALUE ) AS AMOUNT "
                        + " FROM customer_vendormaster AS a, customer_vendormaster AS b, "
                        + "customer_spenddata_uploaddetail, customer_spenddata_uploadmaster WHERE (a.ID = customer_spenddata_uploadmaster.PRIMEVENDORID"
                        + ") AND ( b.ID = customer_spenddata_uploadmaster.VENDORID ) AND"
                        + " ( customer_spenddata_uploadmaster.ID = customer_spenddata_uploaddetail.SPENDDATAMASTERID "
                        + ") GROUP BY SUPPLIER, TIER2SUPPLIER, DIVERSECERTIFICATE ORDER BY SUPPLIER, TIER2SUPPLIER, DIVERSECERTIFICATE ",
                "ID(row_id)",
                "a.VENDORNAME(SUPPLIER),b.VENDORNAME(TIER2SUPPLIER),customer_spenddata_uploaddetail.DIVERSECERTIFICATE(DIVERSECERTIFICATE),customer_spenddata_uploaddetail.SPENDVALUE(AMOUNT)");
        // close db connections
        try {
            conn.close();
        } catch (SQLException e) {
            PrintExceptionInLogFile.printException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.dhtmlx.connector.ConnectorServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        HttpSession session = req.getSession();
        userDetails = (UserDetailsDto) session.getAttribute("userDetails");

        super.doGet(req, res);
    }
}
