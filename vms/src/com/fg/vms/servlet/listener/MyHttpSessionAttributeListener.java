package com.fg.vms.servlet.listener;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class MyHttpSessionAttributeListener implements
		HttpSessionAttributeListener {

	// Should be same as the default value in web.xml & DB.
	private static final Integer DEFAULT_SESSION_TIMEOUT = 60;

	@Override
	public void attributeAdded(HttpSessionBindingEvent arg) {
		setSession(arg);
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent arg) {
		setSession(arg);
	}

	private void setSession(HttpSessionBindingEvent arg) {
		if ("sessionTimeout".equalsIgnoreCase(arg.getName())) {
			Integer min = DEFAULT_SESSION_TIMEOUT;
			try {
				min = new Integer(arg.getValue().toString());
				//System.out.println(min);
				if(min == 0)
					min=DEFAULT_SESSION_TIMEOUT;
			} catch (Exception e) {
				System.err.println(e);
			}
			arg.getSession().setMaxInactiveInterval(min * 60);
		}
	}
}
