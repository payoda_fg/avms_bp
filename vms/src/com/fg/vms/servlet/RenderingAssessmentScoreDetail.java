/**
 * RenderingAssessmentScoreDetail.java
 */
package com.fg.vms.servlet;

import com.dhtmlx.connector.ConnectorBehavior;
import com.dhtmlx.connector.DataItem;
import com.dhtmlx.connector.GridDataItem;

/**
 * The Class RenderingAssessmentScoreDetail.
 * 
 * @author Vinoth
 */
public class RenderingAssessmentScoreDetail extends ConnectorBehavior {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.dhtmlx.connector.ConnectorBehavior#beforeRender(com.dhtmlx.connector
     * .DataItem)
     */
    @Override
    public void beforeRender(DataItem data) {

	String vendorId = data.get_value("vendorId");
	String review = "";
	if (data.get_value("Details") != null
		&& data.get_value("Details").length() != 0) {
	    review = data.get_value("Details") + "&vendorId="
		    + Integer.parseInt(vendorId);
	}

	((GridDataItem) data).set_value("Details", review);

    }

}
