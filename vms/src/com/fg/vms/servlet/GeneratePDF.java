/**
 * 
 */
package com.fg.vms.servlet;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.fg.vms.util.Constants;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * This class assists in exporting data into portable document format.
 * 
 * @author vinoth
 * 
 */
public class GeneratePDF extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger logger = Constants.logger;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// response.setContentType("text/html;charset=UTF-8");
		try {
			String appRoot = request.getSession().getServletContext()
					.getRealPath("")
					+ "/";
			String file = request.getParameter("imagePath");

			File destDir = new File(appRoot + "temp" + File.separator
					+ file.split("/")[0]);
			File srcFile = new File(
					Constants.getString(Constants.UPLOAD_LOGO_PATH)
							+ File.separator + file);
			FileUtils.copyFileToDirectory(srcFile, destDir);

			String pdfBuffer = request.getParameter("pdfBuffer");

			String fileName = request.getParameter("fileName");
			if ("Tier2SpendReport".equalsIgnoreCase(fileName)
					|| "SpendDataDashboard".equalsIgnoreCase(fileName)
					|| "Vendors".equalsIgnoreCase(fileName)
					|| "SearchVendors".equalsIgnoreCase(fileName)) {
				pdfBuffer = pdfBuffer.replaceAll("<TD>", "<td>");
				pdfBuffer = pdfBuffer.replaceAll("</TD>", "</td>");
				pdfBuffer = pdfBuffer.replaceAll("<TR>", "<tr>");
				pdfBuffer = pdfBuffer.replaceAll("</TR>", "</tr>");
				pdfBuffer = pdfBuffer.replaceAll("<TBODY>", "<tbody>");
				pdfBuffer = pdfBuffer.replaceAll("</TBODY>", "</tbody>");
				pdfBuffer = pdfBuffer.replaceAll("<B>", "<b>");
				pdfBuffer = pdfBuffer.replaceAll("</B>", "</b>");
				pdfBuffer = pdfBuffer.replaceAll("<TD ", "<td ");
				pdfBuffer = pdfBuffer.replaceAll("colSpan=2", "colspan=\"2\"");
				pdfBuffer = pdfBuffer.replaceAll("colSpan=3", "colspan=\"3\"");
				pdfBuffer = pdfBuffer.replaceAll("align=right",
						"align=\"right\"");
				pdfBuffer = pdfBuffer.replaceAll("</TABLE>", "</table>");
				pdfBuffer = pdfBuffer.replaceAll("</DIV>", "</div>");
				if (fileName != null
						&& fileName.equalsIgnoreCase("SpendDataDashboard")) {
					pdfBuffer = pdfBuffer
							.replace(
									"<DIV style=\"DISPLAY: block; HEIGHT: 300px; OVERFLOW: auto\" id=content>",
									"<div style=\"display: block; height: 300px; overflow: auto\" id=\"content\">");
					pdfBuffer = pdfBuffer
							.replace(
									"<H2 class=StepTitle>Spend Data Dashboard Report</H2>",
									"<h2 class=\"StepTitle\">Spend Data Dashboard Report</h2>");
					pdfBuffer = pdfBuffer.replace("<TABLE", "<table");
					pdfBuffer = pdfBuffer.replace("align=center",
							"align=\"center\"");
					pdfBuffer = pdfBuffer
							.replace(
									"id=report class=main-table cellSpacing=4 cellPadding=4",
									"id=\"report\" class=\"main-table\" cellSpacing=\"4\" cellPadding=\"4\"");
				} else if (fileName != null
						&& "Vendors".equalsIgnoreCase(fileName)) {
					pdfBuffer = pdfBuffer.replace("<TD class=header>", "<td>");
					pdfBuffer = pdfBuffer.replace("<td class=header>", "<td>");
					pdfBuffer = pdfBuffer.replace("<table>", "<table cellSpacing=\"2\" cellPadding=\"2\" border=\"1\">");
				} else if (fileName != null
						&& "SearchVendors".equalsIgnoreCase(fileName)) {
					pdfBuffer = pdfBuffer.replace(
							"<td class=\"header\">Action</td>", "");
					pdfBuffer = pdfBuffer.replace(
							"<td class=header>Action</td>", "");
					fileName = "Vendors";
					pdfBuffer = pdfBuffer.replace("<TD class=header>", "<td>");
					pdfBuffer = pdfBuffer.replace("<td class=header>", "<td>");
					pdfBuffer = pdfBuffer.replaceAll("<TR id=", "<tr id=\"");
					pdfBuffer = pdfBuffer.replaceAll("><td><A", "\"><td><A");
					pdfBuffer = pdfBuffer
							.replaceAll(">\r\n<td><A", "\"><td><a");
					pdfBuffer = pdfBuffer.replaceAll("</A>", "</a>");
					pdfBuffer = pdfBuffer.replace("<table>", "<table cellSpacing=\"2\" cellPadding=\"2\" border=\"1\">");
				}
			}
			pdfBuffer = pdfBuffer.replace("&", " &amp; ");
			pdfBuffer = pdfBuffer.replace(" & ", " &amp; ");
			pdfBuffer = pdfBuffer.replace("&id=", "&amp;id");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss");
			fileName += "_" + sdf.format(new Date()).toString();
			ITextRenderer renderer = new ITextRenderer();
			ITextFontResolver fontResolver=renderer.getFontResolver();
			fontResolver.addFont("verdana.ttf","UTF-8",true);
			fontResolver.addFont("verdanab.ttf","UTF-8",true);
			ServletOutputStream outputStream = response.getOutputStream();
			renderer.setDocumentFromString(pdfBuffer.toString());
			renderer.layout();
			response.setContentType("application/octet-stream");
			response.setHeader("Content-disposition", "attachment;filename="
					+ fileName + ".pdf");
			response.setHeader("Cache-Control", "public");
			response.setHeader("Pragma", "public");
			renderer.createPDF(outputStream);
			outputStream.flush();
			outputStream.close();

		} catch (Exception e) {
			PrintExceptionInLogFile.printException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	public String getServletInfo() {
		return "Short description";
	}

}
