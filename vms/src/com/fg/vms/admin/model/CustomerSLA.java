/*
 * CustomerSLA.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Indicates that the SLA details for Customer
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "customersla")
public class CustomerSLA implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the CustomerSLA which serves as the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CUSTOMERID")
    private Customer customerId;

    @Column(name = "SLASTARTDATE", nullable = false)
    private Date startDate;

    @Column(name = "SLAENDDATE", nullable = false)
    private Date endDate;

    @Column(name = "NUMBEROFLICENCEDUSERS", nullable = false)
    private Integer licencedUsers;

    @Column(name = "NUMBEROFLICENCEDVENDORS", nullable = false)
    private Integer licencedVendors;

    @Column(name = "ISACTIVE", columnDefinition = "TINYINT(1) DEFAULT 1", nullable = false)
    private Byte isActive;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * 
     * @return
     */
    public Integer getId() {
	return id;
    }

    /**
     * 
     * @param id
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * 
     * @return
     */
    public Date getStartDate() {
	return startDate;
    }

    /**
     * 
     * @param startDate
     */
    public void setStartDate(Date startDate) {
	this.startDate = startDate;
    }

    /**
     * 
     * @return
     */
    public Date getEndDate() {
	return endDate;
    }

    /**
     * 
     * @param endDate
     */
    public void setEndDate(Date endDate) {
	this.endDate = endDate;
    }

    /**
     * 
     * @return
     */
    public Integer getLicencedUsers() {
	return licencedUsers;
    }

    /**
     * 
     * @param licencedUsers
     */
    public void setLicencedUsers(Integer licencedUsers) {
	this.licencedUsers = licencedUsers;
    }

    public Integer getLicencedVendors() {
	return licencedVendors;
    }

    public void setLicencedVendors(Integer licencedVendors) {
	this.licencedVendors = licencedVendors;
    }

    /**
     * 
     * @return
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * 
     * @param isActive
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * 
     * @return
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * 
     * @param createdBy
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * 
     * @return
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * 
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * 
     * @return
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * 
     * @param modifiedBy
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * 
     * @return
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * 
     * @param modifiedOn
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

    public Customer getCustomerId() {
	return customerId;
    }

    public void setCustomerId(Customer customerId) {
	this.customerId = customerId;
    }

}
