/**
 * 
 */
package com.fg.vms.admin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pirabu
 * 
 */
@Entity
@Table(name = "objects")
public class CommonObjects {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the VMSObjects which serves as the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @Column(name = "OBJECTNAME", nullable = false, unique = true)
    private String objectName;

    @Column(name = "OBJECTTYPE", nullable = false)
    private String objectType;

    @Column(name = "OBJECTDESCRIPTION", nullable = false)
    private String objectDescription;

    @Column(name = "USERTYPE", nullable = false)
    private Byte userType;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the objectName
     */
    public String getObjectName() {
	return objectName;
    }

    /**
     * @param objectName
     *            the objectName to set
     */
    public void setObjectName(String objectName) {
	this.objectName = objectName;
    }

    /**
     * @return the objectType
     */
    public String getObjectType() {
	return objectType;
    }

    /**
     * @param objectType
     *            the objectType to set
     */
    public void setObjectType(String objectType) {
	this.objectType = objectType;
    }

    /**
     * @return the objectDescription
     */
    public String getObjectDescription() {
	return objectDescription;
    }

    /**
     * @param objectDescription
     *            the objectDescription to set
     */
    public void setObjectDescription(String objectDescription) {
	this.objectDescription = objectDescription;
    }

    /**
     * @return the userType
     */
    public Byte getUserType() {
	return userType;
    }

    /**
     * @param userType
     *            the userType to set
     */
    public void setUserType(Byte userType) {
	this.userType = userType;
    }

}
