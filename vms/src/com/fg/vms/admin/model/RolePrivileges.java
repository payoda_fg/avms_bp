/*
 * RolePrivileges.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents the user roles details (eg. add, delete, modify, view etc.) for
 * communicate with DB.
 * 
 * @author pirabu
 * 
 */

@Entity
@Table(name = "rolesprivileges")
public class RolePrivileges implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the RolePrivileges which serves as the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "OBJECTID", referencedColumnName = "ID")
    private VMSObjects objectId;

    @ManyToOne
    @JoinColumn(name = "ROLEID")
    private UserRoles roleId;

    @Column(name = "ISADD", columnDefinition = "TINYINT(1) DEFAULT 0")
    private Byte add;

    @Column(name = "ISDELETE", columnDefinition = "TINYINT(1) DEFAULT 0")
    private Byte delete;

    @Column(name = "ISMODIFY", columnDefinition = "TINYINT(1) DEFAULT 0")
    private Byte modify;

    @Column(name = "ISVIEW", columnDefinition = "TINYINT(1) DEFAULT 0")
    private Byte view;

    @Column(name = "ISVISIBLE", columnDefinition = "TINYINT(1) DEFAULT 0")
    private Byte visible;

    @Column(name = "ISENABLE", columnDefinition = "TINYINT(1) DEFAULT 0")
    private Byte enable;

    @Column(name = "ISACTIVE", columnDefinition = "TINYINT(1) DEFAULT 1", nullable = false)
    private Byte isActive;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the objectId
     */
    public VMSObjects getObjectId() {
	return objectId;
    }

    /**
     * @param objectId
     *            the objectId to set
     */
    public void setObjectId(VMSObjects objectId) {
	this.objectId = objectId;
    }

    /**
     * @return the roleId
     */
    public UserRoles getRoleId() {
	return roleId;
    }

    /**
     * @param roleId
     *            the roleId to set
     */
    public void setRoleId(UserRoles roleId) {
	this.roleId = roleId;
    }

    /**
     * @return the add
     */
    public Byte getAdd() {
	return add;
    }

    /**
     * @param add
     *            the add to set
     */
    public void setAdd(Byte add) {
	this.add = add;
    }

    /**
     * @return the delete
     */
    public Byte getDelete() {
	return delete;
    }

    /**
     * @param delete
     *            the delete to set
     */
    public void setDelete(Byte delete) {
	this.delete = delete;
    }

    /**
     * @return the modify
     */
    public Byte getModify() {
	return modify;
    }

    /**
     * @param modify
     *            the modify to set
     */
    public void setModify(Byte modify) {
	this.modify = modify;
    }

    /**
     * @return the view
     */
    public Byte getView() {
	return view;
    }

    /**
     * @param view
     *            the view to set
     */
    public void setView(Byte view) {
	this.view = view;
    }

    /**
     * @return the visible
     */
    public Byte getVisible() {
	return visible;
    }

    /**
     * @param visible
     *            the visible to set
     */
    public void setVisible(Byte visible) {
	this.visible = visible;
    }

    /**
     * @return the enable
     */
    public Byte getEnable() {
	return enable;
    }

    /**
     * @param enable
     *            the enable to set
     */
    public void setEnable(Byte enable) {
	this.enable = enable;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
