/**
 * CustomerContacts.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents the contact details (eg..first name, phone,emailId etc.) of
 * customer.
 * 
 * @author pirabu
 */
@Entity
@Table(name = "customercontacts")
public class CustomerContacts implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the CustomerContacts which serves as the primary
     * key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CUSTOMERID")
    private Customer customerId;

    @Column(name = "FIRSTNAME", nullable = false)
    private String firstName;

    @Column(name = "LASTNAME")
    private String lastName;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "PHONENUMBER", nullable = false)
    private String phoneNumber;

    @Column(name = "MOBILE", nullable = false)
    private String mobile;

    @Column(name = "FAX", nullable = false)
    private String fax;

    @Column(name = "EMAILID", nullable = false)
    private String emailId;

    @Column(name = "ISALLOWEDLOGIN")
    private Byte IsAllowedLogin;

    @Column(name = "LOGINDISPLAYNAME")
    private String loginDisplayName;

    @Column(name = "LOGINID")
    private String loginId;

    @Column(name = "PASSWORD")
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SECRETQUESTIONID")
    private SecretQuestion secretQuestionId;

    @Column(name = "SECRETQUESTIONANSWER")
    private String secQueAns;

    @Column(name = "ISPRIMARYCONTACT")
    private Byte isPrimaryContact;

    @Column(name = "ISACTIVE", columnDefinition = "TINYINT(1) DEFAULT 1", nullable = false)
    private Byte isActive;

    @Column(name = "CREATEDBY", nullable = false)
    private Integer createdBy;

    @Column(name = "CREATEDON", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "MODIFIEDBY")
    private Integer modifiedBy;

    @Column(name = "MODIFIEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the customerId
     */
    public Customer getCustomerId() {
	return customerId;
    }

    /**
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(Customer customerId) {
	this.customerId = customerId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
	return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
	return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    /**
     * @return the designation
     */
    public String getDesignation() {
	return designation;
    }

    /**
     * @param designation
     *            the designation to set
     */
    public void setDesignation(String designation) {
	this.designation = designation;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
	return phoneNumber;
    }

    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
	return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    /**
     * @return the fax
     */
    public String getFax() {
	return fax;
    }

    /**
     * @param fax
     *            the fax to set
     */
    public void setFax(String fax) {
	this.fax = fax;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
	return emailId;
    }

    /**
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(String emailId) {
	this.emailId = emailId;
    }

    /**
     * 
     * @return
     */
    public Byte getIsAllowedLogin() {
	return IsAllowedLogin;
    }

    /**
     * 
     * @param isAllowedLogin
     */
    public void setIsAllowedLogin(Byte isAllowedLogin) {
	IsAllowedLogin = isAllowedLogin;
    }

    /**
     * 
     * @return
     */
    public String getLoginDisplayName() {
	return loginDisplayName;
    }

    /**
     * 
     * @param loginDisplayName
     */
    public void setLoginDisplayName(String loginDisplayName) {
	this.loginDisplayName = loginDisplayName;
    }

    /**
     * 
     * @return
     */
    public String getLoginId() {
	return loginId;
    }

    /**
     * 
     * @param loginId
     */
    public void setLoginId(String loginId) {
	this.loginId = loginId;
    }

    /**
     * 
     * @return
     */
    public String getPassword() {
	return password;
    }

    /**
     * 
     * @param password
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * 
     * @return
     */
    public SecretQuestion getSecretQuestionId() {
	return secretQuestionId;
    }

    /**
     * 
     * @param secretQuestionId
     */
    public void setSecretQuestionId(SecretQuestion secretQuestionId) {
	this.secretQuestionId = secretQuestionId;
    }

    /**
     * 
     * @return
     */
    public String getSecQueAns() {
	return secQueAns;
    }

    /**
     * 
     * @param secQueAns
     */
    public void setSecQueAns(String secQueAns) {
	this.secQueAns = secQueAns;
    }

    /**
     * 
     * @return
     */
    public Byte getIsPrimaryContact() {
	return isPrimaryContact;
    }

    /**
     * 
     * @param isPrimaryContact
     */
    public void setIsPrimaryContact(Byte isPrimaryContact) {
	this.isPrimaryContact = isPrimaryContact;
    }

    /**
     * @return the isActive
     */
    public Byte getIsActive() {
	return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(Byte isActive) {
	this.isActive = isActive;
    }

    /**
     * @return the createdBy
     */
    public Integer getCreatedBy() {
	return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
	this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
	return createdOn;
    }

    /**
     * @param createdOn
     *            the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
    }

    /**
     * @return the modifiedBy
     */
    public Integer getModifiedBy() {
	return modifiedBy;
    }

    /**
     * @param modifiedBy
     *            the modifiedBy to set
     */
    public void setModifiedBy(Integer modifiedBy) {
	this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
	return modifiedOn;
    }

    /**
     * @param modifiedOn
     *            the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
    }

}
