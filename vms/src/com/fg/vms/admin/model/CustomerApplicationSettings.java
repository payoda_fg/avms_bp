/*
 * CustomerApplicationSettings.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Model class for Customer page application settings.
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "customer_applicationsettings")
public class CustomerApplicationSettings implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/**
	 * Auto generated ID for the CustomerApplicationSettings which serves as the
	 * primary key.
	 */
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CUSTOMERID")
	private Customer customerId;

	@Column(name = "LOGO_PATH")
	private String logoPath;

	@Column(name = "HEADER_PRIMARY_COLOR")
	private String headerPrimaryColor;

	@Column(name = "BODY_BACKGROUND_COLOR")
	private String bodyBackgroundColor;

	@Column(name = "BODY_TEXT_COLOR")
	private String bodyTextColor;

	@Column(name = "MENU_SELECT_COLOR")
	private String menuSelectColor;

	@Column(name = "MENU_BAR_COLOR")
	private String menuBarColor;

	@Column(name = "MENU_BUTTON_COLOR")
	private String menuButtonColor;

	@Column(name = "ISDEFAULT")
	private Byte isDefault;

	@Column(name = "ISDIVISION")
	private Byte isDivision;

	@Column(name = "SESSIONTIMEOUT", columnDefinition = "int default 1")
	private int sessionTimeout;
	
	@Column(name = "TERMS_CONDITION", columnDefinition="TEXT", length = 2000)
	private String termsCondition;
	
	@Column(name = "PRIVACY_TERMS", columnDefinition="TEXT", length = 2000)
	private String privacyTerms;
	
	@Column(name = "PRIME_TRAINING_URL")
	private String primeTrainingUrl;
	
	@Column(name = "NONPRIME_TRAINING_URL")
	private String nonPrimeTrainingUrl;

	/**
	 * 
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the customerId
	 */
	public Customer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	/**
	 * 
	 * @return
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * 
	 * @param logoPath
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * 
	 * @return
	 */
	public String getHeaderPrimaryColor() {
		return headerPrimaryColor;
	}

	/**
	 * 
	 * @param headerPrimaryColor
	 */
	public void setHeaderPrimaryColor(String headerPrimaryColor) {
		this.headerPrimaryColor = headerPrimaryColor;
	}

	/**
	 * 
	 * @return
	 */
	public String getBodyBackgroundColor() {
		return bodyBackgroundColor;
	}

	/**
	 * 
	 * @param bodyBackgroundColor
	 */
	public void setBodyBackgroundColor(String bodyBackgroundColor) {
		this.bodyBackgroundColor = bodyBackgroundColor;
	}

	/**
	 * 
	 * @return
	 */
	public String getBodyTextColor() {
		return bodyTextColor;
	}

	/**
	 * 
	 * @param bodyTextColor
	 */
	public void setBodyTextColor(String bodyTextColor) {
		this.bodyTextColor = bodyTextColor;
	}

	/**
	 * 
	 * @return
	 */
	public String getMenuSelectColor() {
		return menuSelectColor;
	}

	/**
	 * 
	 * @param menuSelectColor
	 */
	public void setMenuSelectColor(String menuSelectColor) {
		this.menuSelectColor = menuSelectColor;
	}

	/**
	 * 
	 * @return
	 */
	public String getMenuButtonColor() {
		return menuButtonColor;
	}

	/**
	 * 
	 * @param menuButtonColor
	 */
	public void setMenuButtonColor(String menuButtonColor) {
		this.menuButtonColor = menuButtonColor;
	}

	/**
	 * 
	 * @return
	 */
	public Byte getIsDefault() {
		return isDefault;
	}

	/**
	 * 
	 * @param isDefault
	 */
	public void setIsDefault(Byte isDefault) {
		this.isDefault = isDefault;
	}

	public String getMenuBarColor() {
		return menuBarColor;
	}

	public void setMenuBarColor(String menuBarColor) {
		this.menuBarColor = menuBarColor;
	}

	public Byte getIsDivision() {
		return isDivision;
	}

	public void setIsDivision(Byte isDivision) {
		this.isDivision = isDivision;
	}

	/**
	 * @return the sessionTimeout
	 */
	public int getSessionTimeout() {
		return sessionTimeout;
	}

	/**
	 * @param sessionTimeout
	 *            the sessionTimeout to set
	 */
	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	/**
	 * @return the termsCondition
	 */
	public String getTermsCondition() {
		return termsCondition;
	}

	/**
	 * @param termsCondition the termsCondition to set
	 */
	public void setTermsCondition(String termsCondition) {
		this.termsCondition = termsCondition;
	}

	/**
	 * @return the privacyTerms
	 */
	public String getPrivacyTerms() {
		return privacyTerms;
	}

	/**
	 * @param privacyTerms the privacyTerms to set
	 */
	public void setPrivacyTerms(String privacyTerms) {
		this.privacyTerms = privacyTerms;
	}

	/**
	 * @return the primeTrainingUrl
	 */
	public String getPrimeTrainingUrl() {
		return primeTrainingUrl;
	}

	/**
	 * @param primeTrainingUrl the primeTrainingUrl to set
	 */
	public void setPrimeTrainingUrl(String primeTrainingUrl) {
		this.primeTrainingUrl = primeTrainingUrl;
	}

	/**
	 * @return the nonPrimeTrainingUrl
	 */
	public String getNonPrimeTrainingUrl() {
		return nonPrimeTrainingUrl;
	}

	/**
	 * @param nonPrimeTrainingUrl the nonPrimeTrainingUrl to set
	 */
	public void setNonPrimeTrainingUrl(String nonPrimeTrainingUrl) {
		this.nonPrimeTrainingUrl = nonPrimeTrainingUrl;
	}
}