/**
 * LoginTrack.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents the tracking of login details (like username, loginstatus,
 * logintime) for communicate with DB
 * 
 * @author vinoth
 * 
 */
@Entity
@Table(name = "logintrack")
public class LoginTrack implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;

    /**
     * Auto generated ID for the LoginTrack which serves as the primary key.
     */
    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false, unique = true)
    private Integer id;

    @Column(name = "USERNAME", nullable = false)
    private String userName;

    @Column(name = "LOGINSTATUS", nullable = false)
    private Byte loginStatus;

    @Column(name = "LOGINTIME", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loginTime;

    /**
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

    /**
     * @return the loginStatus
     */
    public Byte getLoginStatus() {
	return loginStatus;
    }

    /**
     * @param loginStatus
     *            the loginStatus to set
     */
    public void setLoginStatus(Byte loginStatus) {
	this.loginStatus = loginStatus;
    }

    /**
     * @return the loginTime
     */
    public Date getLoginTime() {
	return loginTime;
    }

    /**
     * @param loginTime
     *            the loginTime to set
     */
    public void setLoginTime(Date loginTime) {
	this.loginTime = loginTime;
    }

}
