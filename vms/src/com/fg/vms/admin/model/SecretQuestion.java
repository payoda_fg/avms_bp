/*
 * SecretQuestion.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * model class for password security questions.
 * 
 * @author srinivasarao
 * 
 */

@Entity
@Table(name = "passwordsecretquestion")
public class SecretQuestion implements Serializable {

    /** The version of the serializable footprint of this Class. */
    private static final long serialVersionUID = 1L;
    
    /**
     * Auto generated ID for the SecretQuestion which serves as the primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "SECRETQUESTIONPARTICULAR", nullable = false)
    private String secQuestionParticular;

    @Column(name = "ISACTIVE", nullable = false, columnDefinition = "TINYINT(4) DEFAULT 1")
    private Byte IsActive;

    /**
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * 
     * @param id
     *            the Id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * 
     * @return the secquestionparticular
     */
    public String getSecQuestionParticular() {
	return secQuestionParticular;
    }

    /**
     * 
     * @param secQuestionParticular
     *            set to secQuestionParticular
     */
    public void setSecQuestionParticular(String secQuestionParticular) {
	this.secQuestionParticular = secQuestionParticular;
    }

    /**
     * 
     * @return isActive value
     */
    public Byte getIsActive() {
	return IsActive;
    }

    /**
     * 
     * @param isActive
     *            set to isActive
     */
    public void setIsActive(Byte isActive) {
	IsActive = isActive;
    }

}
