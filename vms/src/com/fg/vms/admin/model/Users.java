/**
 * Users.java
 */
package com.fg.vms.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fg.vms.customer.model.CustomerDivision;

/**
 * 
 * Represents the user details (eg. userName, userLogin, UserPassword) for
 * communicate with DB
 * 
 * @author vinoth
 * 
 */

@Entity
@Table(name = "users")
public class Users implements Serializable {

	/** The version of the serializable footprint of this Class. */
	private static final long serialVersionUID = 1L;

	/** Auto generated ID for the Users which serves as the primary key. */
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	/** Role of the user. */
	@ManyToOne
	@JoinColumn(name = "USERROLEID")
	private UserRoles userRoleId;

	/** Name of the user. */
	@Column(name = "USERNAME", nullable = false)
	private String userName;

	/** user password. */
	@Column(name = "USERPASSWORD", nullable = false)
	private String userPassword;

	/** new user password. */
	@Column(name = "NEWPASSWORD")
	private String newPassword;

	/** Random key value for decrypt the user password. */
	@Column(name = "KEYVALUE", nullable = false)
	private Integer keyvalue;

	/** User Email ID. */
	@Column(name = "USEREMAILID", nullable = false, unique = true)
	private String userEmailId;

	/** Entity is active or not. */
	@Column(name = "ISACTIVE", columnDefinition = "TINYINT(1) DEFAULT 1")
	private Byte isActive;

	/** Current user Id. */
	@Column(name = "CREATEDBY", nullable = false)
	private Integer createdBy;

	/** Date of created. */
	@Column(name = "CREATEDON", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date createdOn;

	/** Current user Id. */
	@Column(name = "MODIFIEDBY")
	private Integer modifiedBy;

	/** Date of modified. */
	@Column(name = "MODIFIEDON")
	private Date modifiedOn;

	/** The secret question id. */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SECRETQUESTIONID")
	private SecretQuestion secretQuestionId;

	/** The sec que ans. */
	@Column(name = "SECRETQUESTIONANSWER")
	private String secQueAns;

	/** The department. */
	@Column(name = "DEPARTMENT")
	private String department;

	/** The division. */
	@Column(name = "DIVISION")
	private String division;

	/** The phoneNumber. */
	@Column(name = "PHONENUMBER")
	private String phoneNumber;

	/** The extension. */
	@Column(name = "EXTENSION")
	private String extension;

	/** The firstName. */
	@Column(name = "FIRSTNAME")
	private String firstName;

	/** The lastName. */
	@Column(name = "LASTNAME")
	private String lastName;

	/** The timeZone. */
	@ManyToOne
	@JoinColumn(name = "TIMEZONE")
	private CustomerTimeZoneMaster timeZone;

	/** The title. */
	@Column(name = "TITLE")
	private String title;

	@Column(name = "ISSYSGENPASSWORD", columnDefinition = "TINYINT(1) DEFAULT 0")
	private Byte isSysGenPassword;

	@ManyToOne
	@JoinColumn(name = "CUSTOMERDIVISIONID", nullable = true)
	private CustomerDivision customerDivisionId;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userPassword
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * @param userPassword
	 *            the userPassword to set
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword
	 *            the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the keyvalue
	 */
	public Integer getKeyvalue() {
		return keyvalue;
	}

	/**
	 * @param keyvalue
	 *            the keyvalue to set
	 */
	public void setKeyvalue(Integer keyvalue) {
		this.keyvalue = keyvalue;
	}

	/**
	 * @return the userEmailId
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * @param userEmailId
	 *            the userEmailId to set
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * @return the isActive
	 */
	public Byte getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the userRoleId
	 */
	public UserRoles getUserRoleId() {
		return userRoleId;
	}

	/**
	 * @param userRoleId
	 *            the userRoleId to set
	 */
	public void setUserRoleId(UserRoles userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 * @return the secretQuestionId
	 */
	public SecretQuestion getSecretQuestionId() {
		return secretQuestionId;
	}

	/**
	 * @param secretQuestionId
	 *            the secretQuestionId to set
	 */
	public void setSecretQuestionId(SecretQuestion secretQuestionId) {
		this.secretQuestionId = secretQuestionId;
	}

	/**
	 * @return the secQueAns
	 */
	public String getSecQueAns() {
		return secQueAns;
	}

	/**
	 * @param secQueAns
	 *            the secQueAns to set
	 */
	public void setSecQueAns(String secQueAns) {
		this.secQueAns = secQueAns;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department
	 *            the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the division
	 */
	public String getDivision() {
		return division;
	}

	/**
	 * @param division
	 *            the division to set
	 */
	public void setDivision(String division) {
		this.division = division;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the timeZone
	 */
	public CustomerTimeZoneMaster getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone
	 *            the timeZone to set
	 */
	public void setTimeZone(CustomerTimeZoneMaster timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public Byte getIsSysGenPassword() {
		return isSysGenPassword;
	}

	public void setIsSysGenPassword(Byte isSysGenPassword) {
		this.isSysGenPassword = isSysGenPassword;
	}

	public CustomerDivision getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(CustomerDivision customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}
}