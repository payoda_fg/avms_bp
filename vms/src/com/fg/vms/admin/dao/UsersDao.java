/*
 * UsersDao.java
 */
package com.fg.vms.admin.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.CustomerTimeZoneMaster;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.UsersForm;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerStakeholdersRegistration;
import com.fg.vms.customer.model.CustomerUserDivision;

/**
 * Defines the functionality that should be provided by the Service Layer to FG
 * user and customer for create and modify the user information.
 * 
 * @author vinoth
 * 
 */
/**
 * @author shefeek
 *
 */
public interface UsersDao {

	/**
	 * Method to register a new user into database and activate them.
	 * 
	 * @param usersForm
	 *            the users form
	 * @param currentUserId
	 *            the current user id
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public String createUser(UsersForm usersForm, int currentUserId,
			UserDetailsDto userDetails);

	/**
	 * Method to retrieve the all active users from database.
	 * 
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @param b 
	 * @param divisionIds
	 *            TODO
	 * @return users
	 */
	public List<Users> listUsers(UserDetailsDto userDetails, Boolean b, String divisionIds);
	
	/**
	 * @param userDetails
	 * @param b
	 * @return
	 */
	public List<Users> listUsers(UserDetailsDto userDetails, Boolean b);

	
	/**
	 * Method to remove/delete the user from database.
	 * 
	 * @param id
	 * @param userDetails
	 * @param currentUserId
	 * @return
	 */
	public String deleteUser(Integer id, UserDetailsDto userDetails, Integer currentUserId);

	/**
	 * Method to retrieve/view the selected user based on user id.
	 * 
	 * @param id
	 *            the id
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public Users retriveUser(Integer id, UserDetailsDto userDetails);

	/**
	 * Method to modify/update user details into database.
	 * 
	 * @param usersForm
	 *            the users form
	 * @param id
	 *            the id
	 * @param currentUserId
	 *            the current user id
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return result
	 */
	public String updateUser(UsersForm usersForm, Integer id,
			Integer currentUserId, UserDetailsDto userDetails);

	/**
	 * This method takes the username and emailid and check the database to
	 * determine if the user is already registered in the system.
	 * 
	 * @param userLogin
	 *            the user login
	 * @param emailId
	 *            the email id
	 * @param appDetails
	 *            - Its hold the current user application details.
	 * @return the string
	 */
	public String checkUserLoginAndEmailIdExist(String userLogin,
			String emailId, UserDetailsDto appDetails);

	/**
	 * List the Time Zone Master.
	 * 
	 * @param userDetails
	 * @return
	 */
	public List<CustomerTimeZoneMaster> listTimeZones(UserDetailsDto userDetails);
	
	/**
	 * List the Customer Division.
	 * 
	 * @param userDetails
	 * @param isBoth
	 * @return
	 */
	public List<CustomerDivision> listCustomerDivisons(UserDetailsDto userDetails, boolean isBoth);

	public Users authenticate(String userEmailId, UserDetailsDto userDetails);

	/**
	 * 
	 * @param user
	 * @param userDetails
	 * @return
	 */
	public String save(Users user, UserDetailsDto userDetails);
	

	/**
	 * 
	 * @param userform
	 * @param userDetails
	 * @return
	 */
	public String saveStakeHolder(UsersForm form, UserDetailsDto userDetails);
	
	
	/**
	 * 
	 * @param userform
	 * @param userDetails
	 * @return
	 */
	public String checkDuplicateEmailInStakeHolders(UserDetailsDto userDetails, String emailId);
	
	
	public List<CustomerStakeholdersRegistration> notApprovedStakeholders(UserDetailsDto userDetails);
	
	public CustomerStakeholdersRegistration retriveStakeHolder(UserDetailsDto userDetails, Integer id);
	
	public String stakeHolderApproved(UserDetailsDto userDetails, Integer stakeHolderId, String appRoot, Integer currentUserId, String isApproved);

	List<CustomerUserDivision> listUserDivisionsByUser(UserDetailsDto userDetails, Integer userId);

	public CustomerDivision getGlobalDivisionId(UserDetailsDto userDetails);

}
