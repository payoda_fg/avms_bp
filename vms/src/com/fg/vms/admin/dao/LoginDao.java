/**
 * LoginDao.java
 */
package com.fg.vms.admin.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.CustomerSLA;
import com.fg.vms.admin.model.LoginTrack;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.LoginForm;
import com.fg.vms.customer.model.VendorContact;

/**
 * Defines the functionality that should be provided by the Service Layer of the
 * login page processing such as application Url and SLA Period.
 * 
 * @author pirabu
 * 
 */
public interface LoginDao {

	/**
	 * Verify the valid customer by server url.
	 * 
	 * @return
	 */
	public UserDetailsDto authenticateServerURL(String applicationUrl);

	/**
	 * Method to retrieve the valid user details by userLogin for login process.
	 * 
	 * @param userLogin
	 *            the user login
	 * @param userDetails
	 *            the user details
	 * @return users
	 */
	public Users authenticate(String userLogin, UserDetailsDto userDetails);

	/**
	 * Get customer Application Settings
	 * 
	 * @param userDetail
	 * @return
	 */
	public com.fg.vms.customer.model.CustomerApplicationSettings getCustomerApplicationSettings(
			UserDetailsDto userDetail);

	/**
	 * Represent validate the SLA period using the applicationUrl.
	 * 
	 * @param applicationUrl
	 * @return
	 */
	public String validateSLAPeriod(CustomerSLA customerSLA);

	/**
	 * Method to send the new password to user.
	 * 
	 * @param loginForm
	 *            the login form
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String sendNewPassword(LoginForm loginForm,
			UserDetailsDto userDetails, String appRoot);

	public String resetPassword(LoginForm loginForm, UserDetailsDto userDetails);

	/**
	 * Method to modify/update the password into database.
	 * 
	 * @param userLogin
	 *            the user login
	 * @param password
	 *            the password
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return the string
	 */
	public String updatePassword(String userLogin, String password,
			UserDetailsDto userDetails);

	/**
	 * Method to log all the entries in process of user login.
	 * 
	 * @param userLogin
	 *            the user login.
	 * @param status
	 *            the status.
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return the login track
	 */
	public LoginTrack logEntry(String userLogin, int status,
			UserDetailsDto userDetails);

	/**
	 * Method to authenticate vendor based on active status.
	 * 
	 * @param userLogin
	 *            the user login
	 * @param userDetails
	 *            - Its hold the current user application details.
	 * @return the vendor contact
	 */
	public VendorContact authenticateVendor(String userLogin,
			UserDetailsDto userDetails);

	/**
	 * Method to get list of Security Questions to be displayed on forgot
	 * password screen.
	 * 
	 * @param userDetails
	 * @return list of Security Questions.
	 */
	public List<SecretQuestion> getSecurityQuestions(UserDetailsDto userDetails);

	/**
	 * Method is used to reset the self registered password and put new entry in
	 * vendor users and vendor master tables.
	 * 
	 * @param loginForm
	 * @param userDetails
	 * @return
	 */
	public String resetPasswordAndSaveVendorData(LoginForm loginForm,
			UserDetailsDto userDetails);
}
