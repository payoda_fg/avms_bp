/**
 * LoginDaoImpl.java
 */
package com.fg.vms.admin.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.admin.model.CustomerSLA;
import com.fg.vms.admin.model.LoginTrack;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.LoginForm;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerUserDivision;
import com.fg.vms.customer.model.CustomerVendorMenuStatus;
import com.fg.vms.customer.model.CustomerVendorSelfRegInfo;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.GenerateNewPassword;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PasswordUtil;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SendEmail;

/**
 * Represents the Login services implementation.
 * 
 * @author pirabu
 * 
 */
public class LoginDaoImpl implements LoginDao {

	/** Logger of the system. */
	private Logger logger = Constants.logger;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserDetailsDto authenticateServerURL(String applicationUrl) {

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		Customer customers = null;
		/*
		 * Its holds the application details such as contact information,
		 * profile configuration..
		 */
		UserDetailsDto appDetails = new UserDetailsDto();

		try {
			session.beginTransaction();

			/*
			 * Retrieve the active customer object using the applicationURL.
			 * currently we use the default for all customer so we get the list
			 * of customer.
			 */
			customers = (Customer) session.createCriteria(Customer.class)
					.add(Restrictions.eq("applicationUrl", applicationUrl))
					.add(Restrictions.eq("isActive", (byte) 1)).uniqueResult();

			/* Get the first customer object information. */
			if (customers != null) {				
				logger.info("customers != null");
				/*
				 * Retrieve the particular customer primary contact information.
				 */
				appDetails.setCustomer(customers);
				CustomerContacts contacts = (CustomerContacts) session
						.createCriteria(CustomerContacts.class)
						.add(Restrictions.and(
								Restrictions.eq("isPrimaryContact", (byte) 1),
								Restrictions.eq("customerId", customers)))
						.uniqueResult();

				CustomerSLA customerSLA = (CustomerSLA) session
						.createCriteria(CustomerSLA.class)
						.add(Restrictions.eq("customerId", customers))
						.uniqueResult();

				appDetails.setCustomerSLA(customerSLA);
				appDetails.setDbName(customers.getDatabaseName());
				appDetails.setDatabaseIp(customers.getDatabaseIp());
				appDetails.setDbUsername(customers.getUserName());// Database
				// User
				// name.
				appDetails.setDbPassword(customers.getDbpassword());
				appDetails.setCurrentCustomer(contacts);

			} else{
				logger.info("customers == null for "+applicationUrl);
			}

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			logger.info("ERROR: -> "+exception);
		}
		return appDetails;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Users authenticate(String userLogin, UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		Users user = null;
		try {
			session.beginTransaction();

			/*
			 * Authenticate user by check the unique user login and active
			 * status
			 */
			user = (Users) session
					.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", userLogin).ignoreCase())
					.add(Restrictions.eq("isActive", (byte) 1)).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public com.fg.vms.customer.model.CustomerApplicationSettings getCustomerApplicationSettings(
			UserDetailsDto userDetails) {

		Session session = null;

		session = HibernateUtilCustomer.buildSessionFactory().openSession(
				DataBaseConnection.getConnection(userDetails));

		/* Its holds the application settings */
		com.fg.vms.customer.model.CustomerApplicationSettings settings = null;
		try {
			session.beginTransaction();
			/*
			 * Retrieve the particular customer application settings such as
			 * text color, background color..
			 */
			settings = (com.fg.vms.customer.model.CustomerApplicationSettings) session
					.createCriteria(
							com.fg.vms.customer.model.CustomerApplicationSettings.class)
					.uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {

			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return settings;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String validateSLAPeriod(CustomerSLA customerSLA) {
		String result = "SLA Period is not ended";
		if (customerSLA != null) {
			/* check the sla period */
			if ((CommonUtils.dateConvertValue(CommonUtils
					.convertDateToString(customerSLA.getEndDate())))
					.compareTo(new Date()) == -1) {
				return "ended";
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public String sendNewPassword(LoginForm loginForm,
			UserDetailsDto userDetails, String appRoot) {

		System.out.println("Inside sendNewPassword method");
		UsersDao usersDaoImpl = new UsersDaoImpl();

		String subject = "New Password";
		String password = String.valueOf(PasswordUtil.generatePswd(8, 8, 1, 1,
				0));

		System.out.println("Email id is " + loginForm.getUserEmailId());
		Users user = authenticate(loginForm.getUserEmailId(), userDetails);

		if (user != null) {

			if (null != user.getUserEmailId()
					&& loginForm.getUserEmailId().equalsIgnoreCase(
							user.getUserEmailId())) {

				if (null != user.getSecretQuestionId()
						&& loginForm.getSecurityQuestion().equals(
								user.getSecretQuestionId().getId().toString())) {

					if (null != user.getSecQueAns()
							&& loginForm.getSecurityAnswer().equalsIgnoreCase(
									user.getSecQueAns())) {
						
						if(null != user.getKeyvalue() && null != user.getUserPassword())
						{
							//set divisionid for userDetails
							if(null != userDetails.getSettings()) 
							{
								if(null != userDetails.getSettings().getIsDivision() && 0 != userDetails.getSettings().getIsDivision())
								{
									List<CustomerUserDivision> userDivisionList = usersDaoImpl.listUserDivisionsByUser(userDetails, user.getId());
									
									if(userDivisionList != null)
									{
										String [] customerDivisionIds = new String[userDivisionList.size()];
										for(int i = 0; i < userDivisionList.size(); i++)
										{
											customerDivisionIds[i] = String.valueOf(userDivisionList.get(i).getCustomerDivisionId().getId());
										}
										userDetails.setCustomerDivisionIds(customerDivisionIds);
										
										if(userDivisionList.size() == 1)
										{
											if(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal() != null && userDivisionList.get(0).getCustomerDivisionId().getIsGlobal() != 0)
											{
												userDetails.setIsGlobalDivision(Integer.valueOf(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal()));
											}												
											else
											{
												userDetails.setIsGlobalDivision(0);
											}
										}
										else
										{
											userDetails.setIsGlobalDivision(0);
										}
									}
									else
									{
										return "customerDivisionIdNotFound";
									}									
									/*if(null != user.getCustomerDivisionId() && null != user.getCustomerDivisionId().getId() && 0 != user.getCustomerDivisionId().getId())
									{
										userDetails.setCustomerDivisionID(user.getCustomerDivisionId().getId());
									}
									else
									{
										return "customerDivisionIdNotFound";
									}*/
								}
							}
							/* Send New Password when user EmailId is valid. */
							SendEmail email = new SendEmail();
							email.sendNewPassword(
									loginForm.getUserEmailId().trim(), subject,
									loginForm.getUserEmailId(), password,
									user.getUserName(), appRoot, userDetails);

							updatePassword(loginForm.getUserEmailId(), password,
									userDetails);
							return "success";
						} else {
							return "InvalidKeyvaluePassword";
						}						
					} else {
						return "InvalidSecurityAnswer";
					}
				} else {
					return "InvalidSecurityQuestion";
				}

			} else {
				return "InvalidEmailID";
			}
		} else if (userDetails.getHibernateCfgFileName().equalsIgnoreCase(
				"hibernate.customer.cfg.xml")) {
			/* Authenticate VendorUser */
			VendorContact vendorUser = authenticateVendor(
					loginForm.getUserEmailId(), userDetails);
			if (vendorUser != null) {

				if (null != vendorUser.getEmailId()
						&& loginForm.getUserEmailId().equalsIgnoreCase(
								vendorUser.getEmailId())) {

					if (null != vendorUser.getSecretQuestionId()
							&& loginForm.getSecurityQuestion().equals(
									vendorUser.getSecretQuestionId().getId()
											.toString())) {

						if (null != vendorUser.getSecQueAns()
								&& loginForm.getSecurityAnswer()
										.equalsIgnoreCase(
												vendorUser.getSecQueAns())) {
							
							if(null != vendorUser.getKeyValue() && null != vendorUser.getPassword()) {
								/* Send New Password when user EmailId is valid. */
								SendEmail email = new SendEmail();
								email.sendNewPassword(loginForm.getUserEmailId()
										.trim(), subject, loginForm
										.getUserEmailId().trim(), password,
										vendorUser.getFirstName(), appRoot,
										userDetails);

								updatePassword(loginForm.getUserEmailId(),
										password, userDetails);

								return "success";
							} else {
								return "InvalidKeyvaluePassword";
							}							
						} else {
							return "InvalidSecurityAnswer";
						}
					} else {
						return "InvalidSecurityQuestion";
					}
				} else {
					return "InvalidEmailID";
				}
			}
		}
		return "InvalidEmailID";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updatePassword(String userLogin, String password,
			UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {

			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		session.beginTransaction();
		String result = "failure";

		Users users = null;
		try {

			users = (Users) session
					.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", userLogin).ignoreCase())
					.add(Restrictions.eq("isActive", (byte) 1)).uniqueResult();

			Encrypt encrypt = new Encrypt();
			Decrypt decrypt = new Decrypt();
			// Random random = new java.util.Random();

			if (users != null) {

				/* Update the user details. */
				Integer randVal = users.getKeyvalue();

				/* convert password to encrypted password. */
				String encryptPassword = encrypt.encryptText(
						Integer.toString(randVal) + "", password);

				String decryptedPassword = decrypt.decryptText(
						String.valueOf(users.getKeyvalue().toString()),
						users.getUserPassword()); // Decrypt the old password.

				/** Encrypt the old password with new keyvalue. */
				String encryptOldPassword = encrypt.encryptText(
						Integer.toString(randVal) + "", decryptedPassword);

				users.setUserPassword(encryptPassword);
				users.setNewPassword(encryptPassword);
				users.setIsSysGenPassword((byte) 1);
				users.setKeyvalue(randVal);
				users.setModifiedOn(new Date());
				session.update(users);
				session.getTransaction().commit();
				return "success";
			}

			/* Get unique vendor details based on user login and active status. */
			VendorContact vendorUser = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("emailId", userLogin).ignoreCase())
					.add(Restrictions.eq("isActive", (byte) 1)).uniqueResult();

			if (vendorUser != null) {

				/* Update the user details. */
				Integer randVal = vendorUser.getKeyValue();

				/* convert password to encrypted password. */
				String encryptPassword = encrypt.encryptText(
						Integer.toString(randVal) + "", password);

				/* Update the vendor. */
				String decryptedPassword = decrypt.decryptText(
						String.valueOf(vendorUser.getKeyValue().toString()),
						vendorUser.getPassword()); // Decrypt the old password.

				/** Encrypt the old password with new keyvalue. */
				String encryptOldPassword = encrypt.encryptText(
						Integer.toString(randVal) + "", decryptedPassword);

				vendorUser.setPassword(encryptPassword);
				vendorUser.setNewPassword(encryptPassword);
				vendorUser.setIsSysGenPassword((byte) 1);
				vendorUser.setKeyValue(randVal);
				vendorUser.setModifiedOn(new Date());
				session.update(vendorUser);
				session.getTransaction().commit();
				return "success";
			}

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";

		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			return "failure";
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoginTrack logEntry(String userLogin, int status,
			UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));

		}

		Byte loginStatus = (byte) status;
		LoginTrack track = new LoginTrack();
		try {
			session.beginTransaction();

			/* Track the current person logged in and saves into database. */
			track.setUserName(userLogin);
			track.setLoginTime(new Date());
			track.setLoginStatus(loginStatus);
			session.save(track);

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return track;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VendorContact authenticateVendor(String loginId,
			UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		VendorContact user = null;
		try {
			session.beginTransaction();

			/* Get the unique user based on user login and active status. */

			// user = (VendorContact)
			// session.createCriteria(VendorContact.class)
			// .add(Restrictions.eq("loginId", loginId))
			// .add(Restrictions.eq("isActive", (byte) 1)).uniqueResult();

			VendorMaster master = (VendorMaster) session
					.createQuery(
							"Select vm  From VendorMaster vm,VendorContact vc where vm.id=vc.vendorId and vc.emailId='"
									+ loginId
									+ "' and vc.isActive=1 and (vm.isActive=1 OR(vm.isActive=0 AND vm.vendorStatus='I')) ")
					.uniqueResult();
			if (master != null) {
				master.getVendorUsersList().size();
				if (master.getVendorUsersList() != null
						&& !master.getVendorUsersList().isEmpty())
					for (VendorContact contact : master.getVendorUsersList()) {
						if (contact.getEmailId().equalsIgnoreCase(loginId)) {
							user = contact;
						}
					}

			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return user;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<SecretQuestion> getSecurityQuestions(UserDetailsDto userDetails) {

		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		List<SecretQuestion> secretQuestionsList = null;
		try {
			secretQuestionsList = session.createQuery("from SecretQuestion")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		}
		return secretQuestionsList;

	}

	@Override
	public String resetPassword(LoginForm loginForm, UserDetailsDto userDetails) {
		System.out.println("Inside sendNewPassword method");

		String subject = "New Password";
		String password = GenerateNewPassword.generateRandomPassword();

		System.out.println("Email id is " + loginForm.getUserEmailId());
		Users user = authenticate(loginForm.getUserEmailId(), userDetails);

		if (user != null) {

			if (loginForm.getUserEmailId().equalsIgnoreCase(
					user.getUserEmailId())) {

				if (loginForm.getSecurityQuestion().equals(
						user.getSecretQuestionId().getId().toString())) {

					if (loginForm.getSecurityAnswer().equalsIgnoreCase(
							user.getSecQueAns())) {

						// if (loginForm.getUserLogin()
						// .equals(user.getUserLogin())) {

						Decrypt decrypt = new Decrypt();
						Integer randVal = user.getKeyvalue();
						String decryptedPassword = decrypt.decryptText(
								String.valueOf(randVal.toString()),
								user.getUserPassword());
						if (loginForm.getOldPassword()
								.equals(decryptedPassword)) {
							CURDDao<PasswordHistory> dao = new CURDTemplateImpl<PasswordHistory>();
							List<PasswordHistory> histories = dao
									.list(userDetails,
											" From PasswordHistory where customerUserID="
													+ user.getId()
													+ " order by changedon desc limit 12");
							Encrypt encrypt = new Encrypt();

							/* convert password to encrypted password. */
							String encyppassword = null;
							try {
								encyppassword = encrypt.encryptText(
										Integer.toString(randVal) + "",
										loginForm.getNewPassword());
							} catch (Exception e) {
								PrintExceptionInLogFile.printException(e);
							}
							if (CommonUtils.checkPasswordLimitExist(histories,
									loginForm.getNewPassword()).equals(
									"success")) {
								PasswordHistory passwordHistory = new PasswordHistory();
								passwordHistory.setKeyvalue(randVal);
								passwordHistory.setChangedon(new Date());
								passwordHistory.setChangedby(user.getId());
								passwordHistory.setPassword(encyppassword);
								passwordHistory.setCustomerUserID(user.getId());
								dao.save(passwordHistory, userDetails);
							} else {
								return "exist";
							}
							CURDDao<Users> dao1 = new CURDTemplateImpl<Users>();
							user.setUserPassword(encyppassword);
							user.setKeyvalue(randVal);
							user.setNewPassword(encyppassword);
							user.setIsSysGenPassword((byte) 0);
							dao1.update(user, userDetails);
							return "success";

						} else {
							return "OldPasswordInvalid";
						}

						// } else {
						// return "InvalidLoginId";
						// }
					} else {
						return "InvalidSecurityAnswer";
					}
				} else {
					return "InvalidSecurityQuestion";
				}

			} else {
				return "InvalidEmailID";
			}
		} else if (userDetails.getHibernateCfgFileName().equalsIgnoreCase(
				"hibernate.customer.cfg.xml")) {
			/* Authenticate VendorUser */
			VendorContact vendorUser = authenticateVendor(
					loginForm.getUserEmailId(), userDetails);
			if (vendorUser != null) {

				if (loginForm.getUserEmailId().equalsIgnoreCase(
						vendorUser.getEmailId())) {

					if (loginForm.getSecurityQuestion()
							.equals(vendorUser.getSecretQuestionId().getId()
									.toString())) {

						if (loginForm.getSecurityAnswer().equalsIgnoreCase(
								vendorUser.getSecQueAns())) {

							// if (loginForm.getUserLogin().equals(
							// vendorUser.getLoginId())) {
							Decrypt decrypt = new Decrypt();
							Integer randVal = vendorUser.getKeyValue();
							String decryptedPassword = decrypt.decryptText(
									String.valueOf(randVal.toString()),
									vendorUser.getPassword());
							if (loginForm.getOldPassword().equals(
									decryptedPassword)) {
								CURDDao<PasswordHistory> dao = new CURDTemplateImpl<PasswordHistory>();
								List<PasswordHistory> histories = dao
										.list(userDetails,
												" From PasswordHistory where vendorUserId="
														+ vendorUser.getId()
														+ " order by changedon desc limit 12");
								Encrypt encrypt = new Encrypt();

								/* convert password to encrypted password. */
								String encyppassword = null;
								try {
									encyppassword = encrypt.encryptText(
											Integer.toString(randVal) + "",
											loginForm.getNewPassword());
								} catch (Exception e) {
									PrintExceptionInLogFile.printException(e);
								}
								if (CommonUtils.checkPasswordLimitExist(
										histories, loginForm.getNewPassword())
										.equals("success")) {
									PasswordHistory passwordHistory = new PasswordHistory();
									passwordHistory.setKeyvalue(randVal);
									passwordHistory.setChangedon(new Date());
									passwordHistory.setChangedby(vendorUser
											.getId());
									passwordHistory.setPassword(encyppassword);
									passwordHistory.setVendorUserId(vendorUser
											.getId());
									dao.save(passwordHistory, userDetails);
								} else {
									return "exist";
								}
								CURDDao<VendorContact> dao2 = new CURDTemplateImpl<VendorContact>();
								vendorUser.setPassword(encyppassword);
								vendorUser.setKeyValue(randVal);
								vendorUser.setNewPassword(encyppassword);
								vendorUser.setIsSysGenPassword((byte) 0);
								dao2.update(vendorUser, userDetails);
								return "success";

							} else {
								return "OldPasswordInvalid";
							}

							// } else {
							// return "InvalidLoginId";
							// }
						} else {
							return "InvalidSecurityAnswer";
						}
					} else {
						return "InvalidSecurityQuestion";
					}
				} else {
					return "InvalidEmailID";
				}
			}
		}
		return "InvalidEmailID";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String resetPasswordAndSaveVendorData(LoginForm loginForm,
			UserDetailsDto appDetails) {
		String result = "success";
		VendorMaster vendorMaster = null;
		Integer userId = -1;
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(appDetails));
		session.beginTransaction();
		CommonDao commonDao = new CommonDaoImpl();
		String emailExist = commonDao.checkDuplicateEmail(
				loginForm.getUserEmailId(), appDetails);
		if (emailExist.equalsIgnoreCase("submit")) {
			try {

				CustomerVendorSelfRegInfo selfRegInfo = (CustomerVendorSelfRegInfo) session
						.createQuery(
								"from CustomerVendorSelfRegInfo where emailId = '"
										+ loginForm.getUserEmailId()
										+ "' and vendorId is null")
						.uniqueResult();
				// Save VendorContact Information
				Decrypt decrypt = new Decrypt();
				Integer randValue = selfRegInfo.getKeyValue();
				String decryptedPassword = decrypt.decryptText(
						String.valueOf(randValue.toString()),
						selfRegInfo.getPassword());
				if (loginForm.getOldPassword().equals(decryptedPassword)) {
					/*
					 * Save vendor master
					 */
					vendorMaster = new VendorMaster();
					vendorMaster.setCreatedBy(userId);
					vendorMaster.setCreatedOn(new Date());
					vendorMaster.setParentVendorId((Integer) 0);
					vendorMaster.setIsApproved((byte) 0);
					vendorMaster.setIsActive((byte) 1);
					vendorMaster.setIsPartiallySubmitted("Yes");
					vendorMaster.setDeverseSupplier((byte) 1);
					vendorMaster.setIsinvited((short) 0);
					if (null != selfRegInfo.getVendorType()
							&& !selfRegInfo.getVendorType().isEmpty()) {
						vendorMaster.setPrimeNonPrimeVendor((byte) 1);
					}
					else
					{
						vendorMaster.setPrimeNonPrimeVendor((byte) 0);
					}
					session.save(vendorMaster);
					vendorMaster.setVendorCode(vendorMaster.getId().toString());

					if (appDetails.getSettings() != null
							&& appDetails.getSettings().getIsDivision() != null
							&& appDetails.getSettings().getIsDivision() != 0) {
						if (selfRegInfo.getCustomerDivisionId() != null
								&& selfRegInfo.getCustomerDivisionId().getId() != null) {
							CustomerDivision customerDivision = (CustomerDivision) session
									.get(CustomerDivision.class, selfRegInfo
											.getCustomerDivisionId().getId());
							vendorMaster
									.setCustomerDivisionId(customerDivision);
							//To set  customer division isGlobal or not in userDetails
							if (selfRegInfo.getCustomerDivisionId().getIsGlobal() != null 
									&& selfRegInfo.getCustomerDivisionId().getIsGlobal() != 0)
								vendorMaster.setDeverseSupplier((byte) 0);
						}
					}

					session.merge(vendorMaster);

					/**
					 * To maintain the menu status for self vendor registration
					 * process.
					 */
					for (int i = 1; i <= 13; i++) {
						CustomerVendorMenuStatus menuStatus = new CustomerVendorMenuStatus();
						menuStatus.setTab(i);
						if (i == 1) {
							menuStatus.setStatus(2);
						} else {
							menuStatus.setStatus(0);
						}
						menuStatus.setCreatedBy(userId);
						menuStatus.setCreatedOn(new Date());
						menuStatus.setVendorId(vendorMaster);
						session.save(menuStatus);
					}
					/*
					 * Update the VendorID to self registration table.
					 */
					if (null != selfRegInfo) {
						selfRegInfo
								.setVendorId(vendorMaster.getId().toString());
						session.merge(selfRegInfo);
					}
					if (null != loginForm.getUserEmailId()) {
						/*
						 * Save vendor contact.
						 */
						VendorContact vendorContact = new VendorContact();
						vendorContact.setVendorId(vendorMaster);
						Random random = new java.util.Random();
						Integer randVal = random.nextInt();
						Encrypt encrypt = new Encrypt();
						// convert password to encrypted password
						String encyppassword = null;
						if (loginForm.getNewPassword() != null
								&& !loginForm.getNewPassword().isEmpty()) {
							encyppassword = encrypt.encryptText(
									Integer.toString(randVal) + "",
									loginForm.getNewPassword());
						}
						vendorContact.setEmailId(loginForm.getUserEmailId());
						vendorContact.setIsAllowedLogin(CommonUtils
								.getByteValue(true));
						vendorContact.setIsBusinessContact(CommonUtils
								.getByteValue(true));
						vendorContact.setIsPreparer(CommonUtils
								.getByteValue(true));
						vendorContact.setPassword(encyppassword);
						vendorContact.setKeyValue(randVal);
						vendorContact.setIsPrimaryContact((byte) 1);
						SecretQuestion secretQns = new SecretQuestion();
						secretQns = (SecretQuestion) session.get(
								SecretQuestion.class, Integer
										.parseInt(loginForm
												.getSecurityQuestion()));
						vendorContact.setSecretQuestionId(secretQns);
						vendorContact.setSecQueAns(loginForm
								.getSecurityAnswer());
						vendorContact.setIsSysGenPassword((byte) 0);
						VendorRoles roles = (VendorRoles) session
								.createCriteria(VendorRoles.class)
								.add(Restrictions.eq("isActive", (byte) 1))
								.list().get(0);
						vendorContact.setVendorUserRoleId(roles);
						vendorContact.setIsActive((byte) 1);
						vendorContact.setCreatedBy(userId);
						vendorContact.setCreatedOn(new Date());
						// vendorContact.setModifiedBy(userId);
						// vendorContact.setModifiedOn(new Date());
						session.save(vendorContact);
					}
				} else {
					return "OldPasswordInvalid";
				}
				session.getTransaction().commit();
			} catch (HibernateException ex) {
				ex.printStackTrace();
				session.getTransaction().rollback();
				PrintExceptionInLogFile.printException(ex);
				return "failure";
			} catch (Exception e) {
				PrintExceptionInLogFile.printException(e);
				return "failure";
			} finally {
				if (session.isConnected()) {
					try {
						session.connection().close();
						session.close();
					} catch (HibernateException e) {
						PrintExceptionInLogFile.printException(e);
					} catch (SQLException e) {
						PrintExceptionInLogFile.printException(e);
					}
				}
			}
		} else {
			return emailExist;
		}
		return result;
	}

}
