package com.fg.vms.admin.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.UserRoles;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * User role operations
 * 
 * @author vinoth karthi
 * 
 */
public class UserRolesDaoImpl implements RolesDao {

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserRolesForm> viewRoles(UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		List<UserRoles> userRoles = null;
		List<UserRolesForm> userRolesForms = null;
		try {
			session.beginTransaction();
			userRoles = session.createCriteria(UserRoles.class)
					.add(Restrictions.eq("isActive", (byte) 1))
					.addOrder(Order.asc("roleName")).list();
			userRolesForms = packageUserRoles(userRoles);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userRolesForms;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteRole(Integer id, UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		String result = "";
		UserRoles userrole = null;
		try {
			userrole = (UserRoles) session.createCriteria(UserRoles.class)
					.add(Restrictions.eq("id", id)).uniqueResult();
			// userrole.setIsActive((byte) 0);
			if (userrole != null) {
				@SuppressWarnings("unchecked")
				List<Users> users = session.createCriteria(Users.class)
						.add(Restrictions.eq("userRoleId", userrole)).list();

				@SuppressWarnings("unchecked")
				List<RolePrivileges> rolePrivileges = session
						.createCriteria(RolePrivileges.class)
						.add(Restrictions.eq("roleId", userrole)).list();

				if ((users != null && users.size() != 0)
						|| (rolePrivileges != null && rolePrivileges.size() != 0)) {
					return "reference";

				} else {

					session.delete(userrole);
					result = "deletesuccess";
				}

			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "reference";

		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addRole(UserRolesForm userRolesForm, Integer currentUser,
			UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {

			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		String result = "";
		UserRoles userroles = (UserRoles) session
				.createCriteria(UserRoles.class)
				.add(Restrictions.eq("roleName", userRolesForm.getRoleName()))
				.uniqueResult();
		if (userroles != null) {
			return "notUnique";
		}
		UserRoles userrole = new UserRoles();
		try {
			userrole.setRoleName(userRolesForm.getRoleName());
			
//			if (userDetails.getUserType() == null){
				userrole.setRoleDesc(userRolesForm.getRoleDesc());
//			}
			userrole.setIsActive((byte) 1);

			userrole.setCreatedBy(currentUser);
			userrole.setCreatedOn(new Date());
			userrole.setModifiedBy(currentUser);
			userrole.setModifiedOn(new Date());
			session.save(userrole);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateRole(UserRolesForm userRolesForm,
			Integer currentUserId, UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		String result = "";
		UserRoles userrole = new UserRoles();
		try {
			userrole = (UserRoles) session.get(UserRoles.class, currentUserId);
			userrole.setRoleName(userRolesForm.getRoleName());
			userrole.setRoleDesc(userRolesForm.getRoleDesc());
			session.update(userrole);
			session.getTransaction().commit();

			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserRoles retriveRole(Integer id, UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		UserRoles userRoles = null;
		try {
			userRoles = (UserRoles) session.get(UserRoles.class, id);
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return userRoles;
	}

	/**
	 * 
	 * @param userRoles
	 * @return
	 */
	private static List<UserRolesForm> packageUserRoles(
			List<UserRoles> userRoles) {
		List<UserRolesForm> userRolesForms = new ArrayList<UserRolesForm>();
		for (UserRoles userRole : userRoles) {
			UserRolesForm userRoleForm = new UserRolesForm();
			userRoleForm.setRoleName(userRole.getRoleName());
			userRoleForm.setId(userRole.getId());
			if (userRole.getIsActive().byteValue() == (byte) 1) {
				userRoleForm.setIsActive("on");
			} else {
				userRoleForm.setIsActive("");
			}
			userRolesForms.add(userRoleForm);
		}
		return userRolesForms;
	}

	@Override
	public String checkDuplicateRole(String roleName, UserDetailsDto appDetails) {
		Session session = null;

		if (appDetails.getUserType() != null
				&& appDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(appDetails));
		}

		String result = "success";
		try {
			session.beginTransaction();
			UserRoles userroles = (UserRoles) session
					.createCriteria(UserRoles.class)
					.add(Restrictions.eq("roleName", roleName)).uniqueResult();
			if (userroles != null) {
				return "notUnique";
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (appDetails.getUserType() != null
					&& !(appDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

}
