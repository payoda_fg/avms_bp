/*
 * CustomerDaoImpl.java
 */
package com.fg.vms.admin.dao.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.CustomerDao;
import com.fg.vms.admin.dto.CustomerInfoDto;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerApplicationSettings;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.admin.model.CustomerSLA;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.UserRoles;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.admin.pojo.CustomerContactForm;
import com.fg.vms.admin.pojo.CustomerForm;
import com.fg.vms.ajax.validation.ValidationDao;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorObjects;
import com.fg.vms.customer.model.VendorRolePrivileges;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.ibatis.common.jdbc.ScriptRunner;

/**
 * Represents the customer services implementation.
 * 
 * @author pirabu
 * 
 */
public class CustomerDaoImpl implements CustomerDao, ValidationDao {

	/** Logger of the system. */
	private static final Logger log = Constants.logger;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String saveCustomer(CustomerForm customerForm, Integer userId,
			UserDetailsDto userDetails, String appRoot) {

		/* session object represents FG master database */
		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		Session customerSession = null;
		// databaseName =
		// this.generateDatabaseName(customerForm.getCompanyname(),
		// userId);
		// applicationURL = this.generateApplicationURL(customerForm, request);
		// development purpose

		String result = "success";
		Customer customerInfo = null;
		String dbnameresult = "unique";
		try {

			session.beginTransaction(); // begin the transaction.

			// register the new customer

			/* for check database name unique */
			Customer customer1 = (Customer) session
					.createCriteria(Customer.class)
					.add(Restrictions.eq("databaseName",
							customerForm.getDatabaseName())).uniqueResult();

			/* database name is exist, back to input page */
			if (customer1 != null) {
				return dbnameresult;
			}

			UserDetailsDto appDetails = new UserDetailsDto();
			appDetails.setDatabaseIp(customerForm.getDatabaseIp().trim());
			appDetails.setDbUsername(customerForm.getUserName().trim());
			appDetails.setDbPassword(customerForm.getDbpassword().trim());
			int isDBCreated = 0;
			Connection connection = null;
			try {
				boolean isDbExist = false;
				connection = DataBaseConnection.getConnection(appDetails);

				ResultSet resultSet = connection.getMetaData().getCatalogs();

				// iterate each catalog in the ResultSet
				while (resultSet.next()) {
					// Get the database name, which is at position 1
					String databaseName = resultSet.getString(1);

					if (databaseName.equalsIgnoreCase(customerForm
							.getDatabaseName().trim())) {
						isDbExist = true;
						return dbnameresult;
					}
				}
				resultSet.close();
				if (!isDbExist) {
					// Create a Statement class to execute the SQL statement
					Statement stmt = connection.createStatement();

					// Execute the SQL statement and get the results in a
					// Resultset
					isDBCreated = stmt.executeUpdate("CREATE DATABASE "
							+ customerForm.getDatabaseName().trim());
				}
				connection.close();
			} catch (SQLException e) {
				PrintExceptionInLogFile.printException(e);
				connection.close();
			}
			if (isDBCreated != 1) {
				return "failure";
			}
			appDetails.setDbName(customerForm.getDatabaseName().trim());
			/* customerSession object represents Customer database */
			customerSession = HibernateUtilCustomer.buildSessionFactory()
					.openSession(DataBaseConnection.getConnection(appDetails));

			/* Execute the sql script to create all the customer tables. */

			ScriptRunner runner = new ScriptRunner(
					DataBaseConnection.getConnection(appDetails), false, true);
			try {

				runner.runScript(new BufferedReader(new FileReader(appRoot
						+ "script/customerDB.sql")));

			} catch (FileNotFoundException e) {
				PrintExceptionInLogFile.printException(e);
			}
			customerSession.beginTransaction();

			/* validate the customer code */
			result = checkDuplicateCustCode(customerForm.getCompanycode(),
					userDetails);

			/* if customer code is unique then process to save customer. */
			if (result.equals("success")) {
				log.info(" ============save customer is starting========== ");

				/* set the customer information from form to entity */
				customerInfo = packCustomerEntity(customerForm, userId);

				/* set the contact information from form to entity */
				CustomerContacts customerContacts = packContactEntity(
						customerForm, userId);

				SecretQuestion secretQuestion = (SecretQuestion) session.get(
						SecretQuestion.class, customerForm.getUserSecQn());
				customerContacts.setSecretQuestionId(secretQuestion);
				customerContacts.setCustomerId(customerInfo);

				/*
				 * create new customer application setting object to persist the
				 * application settings
				 */
				CustomerApplicationSettings applicationSettings = new CustomerApplicationSettings();
				applicationSettings.setCustomerId(customerInfo);
				applicationSettings.setIsDefault(CommonUtils
						.getByteValue(customerForm.getDefaultSet()));
				applicationSettings
						.setLogoPath(customerForm.getLogoImagePath());

				CustomerSLA customerSLA = new CustomerSLA();

				customerSLA.setCustomerId(customerInfo);
				customerSLA.setStartDate(CommonUtils
						.dateConvertValue(customerForm.getSlaStartDate()));
				customerSLA.setEndDate(CommonUtils
						.dateConvertValue(customerForm.getSlaEndDate()));
				customerSLA.setLicencedUsers(customerForm.getLicencedUsers());
				customerSLA.setLicencedVendors(customerForm
						.getLicencedVendors());
				customerSLA.setIsActive((byte) 1);
				customerSLA.setCreatedBy(userId);
				customerSLA.setCreatedOn(new Date());
				customerSLA.setModifiedBy(userId);
				customerSLA.setModifiedOn(new Date());

				session.save(customerContacts);
				session.save(applicationSettings);
				session.save(customerSLA);

				/* Assign the admin role when customer registration. */
				UserRoles userRole = new UserRoles();
				userRole.setId(1);
				userRole.setRoleName("admin");
				userRole.setCreatedBy(1);
				userRole.setCreatedOn(new Date());
				userRole.setIsActive((byte) 1);
				customerSession.save(userRole);

				/* Persist Customer User information into customer db. */
				// Users users = (Users) session.createCriteria(Users.class)
				// .add(Restrictions.eq("id", userId)).uniqueResult();
				Random random = new java.util.Random();
				Integer randVal = random.nextInt();
				Encrypt encrypt = new Encrypt();

				/* convert password to encrypted password */
				String encyppassword = encrypt.encryptText(
						Integer.toString(randVal) + "",
						customerContacts.getPassword());
				Users user = new Users();
				user.setUserName(customerContacts.getLoginDisplayName());
				// user.setUserLogin(customerContacts.getLoginId());
				user.setUserPassword(encyppassword);
				user.setUserEmailId(customerContacts.getEmailId());
				user.setKeyvalue(randVal);

				/* Assign the admin role when customer registration. */
				VendorRoles vendorRole = new VendorRoles();
				vendorRole.setId(1);
				vendorRole.setRoleName("admin");
				vendorRole.setCreatedBy(1);
				vendorRole.setCreatedOn(new Date());
				vendorRole.setIsActive((byte) 1);
				customerSession.save(vendorRole);

				user.setUserRoleId(userRole);
				user.setIsActive((byte) 1);
				user.setCreatedBy(userId);
				user.setCreatedOn(new Date());
				user.setModifiedBy(userId);
				user.setModifiedOn(new Date());

				/*
				 * persist data into Customer DB. Currently We are using the
				 * Admin entity to persist the customer entity in customer db.
				 * If any changes in the Customer and CustomerContact entity for
				 * Customer, then use entity under the customer package.
				 */

				customerSession.save(user);
				Customer customer = packCustomerEntity(customerForm, userId);
				CustomerContacts contacts = packContactEntity(customerForm,
						userId);

				com.fg.vms.customer.model.CustomerApplicationSettings settings = new com.fg.vms.customer.model.CustomerApplicationSettings();

				settings.setCustomerId(customer);
				settings.setIsDefault(CommonUtils.getByteValue(customerForm
						.getDefaultSet()));
				settings.setLogoPath(customerForm.getLogoImagePath());

				VMSObjects object = (VMSObjects) customerSession
						.createCriteria(VMSObjects.class)
						.add(Restrictions.and(Restrictions.eq("objectName",
								"Role Privileges"), Restrictions.eq("isActive",
								(byte) 1))).uniqueResult();
				/* Assign Role Privileges object to customer admin */
				RolePrivileges privileges = new RolePrivileges();
				privileges.setRoleId(userRole);
				privileges.setCreatedBy(userId);
				privileges.setCreatedOn(new Date());
				privileges.setVisible((byte) 1);
				privileges.setAdd((byte) 1);
				privileges.setDelete((byte) 0);
				privileges.setModify((byte) 1);
				privileges.setEnable((byte) 0);
				privileges.setView((byte) 1);
				privileges.setObjectId(object);
				privileges.setIsActive((byte) 1);

				VendorObjects vendorObject = (VendorObjects) customerSession
						.createCriteria(VendorObjects.class)
						.add(Restrictions.and(
								Restrictions.eq("objectName", "Privileges"),
								Restrictions.eq("isActive", (byte) 1)))
						.uniqueResult();
				/* Assign Role Privileges object to vendor admin */
				VendorRolePrivileges vendorRolePrivileges = new VendorRolePrivileges();
				vendorRolePrivileges.setVisible((byte) 1);
				vendorRolePrivileges.setAdd((byte) 1);
				vendorRolePrivileges.setDelete((byte) 0);
				vendorRolePrivileges.setModify((byte) 1);
				vendorRolePrivileges.setEnable((byte) 0);
				vendorRolePrivileges.setView((byte) 1);
				vendorRolePrivileges.setVendorUserRoleId(vendorRole);
				vendorRolePrivileges.setObjectId(vendorObject);
				vendorRolePrivileges.setCreatedBy(userId);
				vendorRolePrivileges.setCreatedOn(new Date());
				vendorRolePrivileges.setIsActive((byte) 1);

				List<SecretQuestion> question = customerSession.createQuery(
						" From SecretQuestion ").list();

				if (question != null && !question.isEmpty())
					contacts.setSecretQuestionId(question.get(0));
				contacts.setCustomerId(customer);
				customerSession.save(customer);
				customerSession.save(contacts);
				customerSession.save(settings);
				customerSession.save(privileges);
				customerSession.save(vendorRolePrivileges);

				session.getTransaction().commit();

				customerSession.getTransaction().commit();

			} else {
				return result;
			}
			log.info("=============== customer profile saved =========");

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			if (customerSession != null
					&& customerSession.getTransaction() != null)
				customerSession.getTransaction().rollback();
			try {

				try {
					UserDetailsDto appDetails = new UserDetailsDto();
					appDetails.setDatabaseIp(customerForm.getDatabaseIp()
							.trim());
					appDetails.setDbUsername(customerForm.getUserName().trim());
					appDetails.setDbPassword(customerForm.getDbpassword()
							.trim());
					Connection connection = DataBaseConnection
							.getConnection(appDetails);
					// Create a Statement class to execute the SQL statement
					Statement stmt = connection.createStatement();

					// Execute the SQL statement and get the results in a
					// Resultset
					stmt.executeUpdate("DROP DATABASE "
							+ customerForm.getDatabaseName().trim());
					connection.close();
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}

			} catch (Exception e) {
				session.getTransaction().rollback();
				PrintExceptionInLogFile.printException(e);
			}
			result = "failure";
		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (customerSession != null && customerSession.isConnected()) {
				try {
					customerSession.connection().close();
					customerSession.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateCustomer(CustomerForm customerForm, Integer userId,
			UserDetailsDto userDetails) {

		/* session object represents FG master database */
		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		/* customerSession object represents Customer database */
		Session customerSession = null;

		String result = "success";
		Customer customerInfo = null;
		try {

			session.beginTransaction(); // begin the transaction.

			/** Retrieve the customer when modify the customer. */
			customerInfo = (Customer) session.createCriteria(Customer.class)
					.add(Restrictions.eq("id", customerForm.getId()))
					.uniqueResult();

			if (customerInfo != null) { // Merge the customer information.

				UserDetailsDto appDetails = new UserDetailsDto();

				appDetails.setDatabaseIp(customerForm.getDatabaseIp().trim());
				appDetails.setDbUsername(customerForm.getUserName().trim());
				appDetails.setDbPassword(customerForm.getDbpassword().trim());
				appDetails.setDbName(customerInfo.getDatabaseName().trim());
				customerSession = HibernateUtilCustomer.buildSessionFactory()
						.openSession(
								DataBaseConnection.getConnection(appDetails));

				customerSession.beginTransaction();

				customerInfo.setCustName(customerForm.getCompanyname());
				customerInfo.setDunsNumber(customerForm.getDunsnum());
				customerInfo.setCustCode(customerForm.getCompanycode());
				customerInfo.setTaxId(customerForm.getTaxid());
				customerInfo.setAddress1(customerForm.getAddress1());
				customerInfo.setAddress2(customerForm.getAddress2());
				customerInfo.setAddress3(customerForm.getAddress3());
				customerInfo.setCity(customerForm.getCity());
				customerInfo.setState(customerForm.getState());
				customerInfo.setProvince(customerForm.getProvince());
				customerInfo.setZipCode(customerForm.getZipcode());
				customerInfo.setRegion(customerForm.getRegion());
				customerInfo.setCountry(customerForm.getCountry());
				customerInfo.setPhone(customerForm.getPhone());
				customerInfo.setFax(customerForm.getFax());
				customerInfo.setWebsite(customerForm.getWebSite());
				customerInfo.setMobile(customerForm.getMobile());
				customerInfo.setEmailId(customerForm.getEmail());
				customerInfo.setDatabaseIp(customerForm.getDatabaseIp());
				customerInfo.setDbpassword(customerForm.getDbpassword());
				customerInfo.setUserName(customerForm.getUserName());
				customerInfo.setDatabaseName(customerForm.getDatabaseName());
				customerInfo
						.setApplicationUrl(customerForm.getApplicationUrl());
				customerInfo.setApplicationServer(customerForm
						.getApplicationServer());
				customerInfo.setIsActive(((byte) 1));
				customerInfo.setModifiedBy(userId);
				customerInfo.setModifiedOn(new Date());

				/** Retrieve the primary contact information to merge. */
				CustomerContacts customerContacts = (CustomerContacts) session
						.createCriteria(CustomerContacts.class)
						.add(Restrictions.eq("id", customerForm.getContactId()))
						.uniqueResult();
				/* Merge the primary contact information to the customer. */
				if (customerContacts != null) {

					customerContacts.setCustomerId(customerInfo);
					customerContacts.setFirstName(customerForm.getFirstName());
					customerContacts.setLastName(customerForm.getLastName());
					customerContacts.setDesignation(customerForm
							.getDesignation());
					customerContacts.setPhoneNumber(customerForm
							.getContactPhone());
					customerContacts.setMobile(customerForm.getContactMobile());
					customerContacts.setFax(customerForm.getContactFax());
					customerContacts
							.setEmailId(customerForm.getContanctEmail());
					customerContacts.setIsPrimaryContact((byte) 1);
					customerContacts.setIsAllowedLogin(CommonUtils
							.getByteValue(true));
					customerContacts.setLoginDisplayName(customerForm
							.getLoginDisplayName());
					// customerContacts.setLoginId(customerForm.getLoginId());
					customerContacts.setPassword(customerForm
							.getLoginpassword());

					SecretQuestion secretQuestion = new SecretQuestion();
					secretQuestion = (SecretQuestion) session.get(
							SecretQuestion.class, customerForm.getUserSecQn());

					customerContacts.setSecretQuestionId(secretQuestion);
					customerContacts.setSecQueAns(customerForm
							.getUserSecQnAns());

					customerContacts.setIsActive((byte) 1);

					customerContacts.setModifiedBy(userId);
					customerContacts.setModifiedOn(new Date());
					session.update(customerContacts);
				}
				/* Retrieve the exist application setting for update */
				CustomerApplicationSettings applicationSettings = (CustomerApplicationSettings) session
						.createCriteria(CustomerApplicationSettings.class)
						.add(Restrictions.eq("customerId", customerInfo))
						.uniqueResult();

				applicationSettings.setCustomerId(customerInfo);
				applicationSettings
						.setLogoPath(customerForm.getLogoImagePath());

				/* Retrieve the customer service level agreement */
				CustomerSLA customerSLA = (CustomerSLA) session
						.createCriteria(CustomerSLA.class)
						.add(Restrictions.eq("customerId", customerInfo))
						.uniqueResult();

				customerSLA.setCustomerId(customerInfo);
				customerSLA.setStartDate(CommonUtils
						.dateConvertValue(customerForm.getSlaStartDate()));
				customerSLA.setEndDate(CommonUtils
						.dateConvertValue(customerForm.getSlaEndDate()));
				customerSLA.setLicencedUsers(customerForm.getLicencedUsers());
				customerSLA.setLicencedVendors(customerForm
						.getLicencedVendors());
				customerSLA.setIsActive((byte) 1);
				customerSLA.setModifiedBy(userId);
				customerSLA.setModifiedOn(new Date());

				session.update(applicationSettings);
				session.update(customerSLA);

				/*
				 * update the user information into customer database when
				 * Change the contact information
				 */
				Users updateUser = (Users) customerSession
						.createCriteria(Users.class)
						.add(Restrictions.eq("userLogin",
								customerForm.getHiddenLoginId()))
						.uniqueResult();
				if (updateUser != null) {

					// Random random = new java.util.Random();
					Integer randVal = updateUser.getKeyvalue();

					Encrypt encrypt = new Encrypt();
					/* convert password to encrypted password */
					String encyppassword = encrypt.encryptText(
							Integer.toString(randVal) + "",
							customerContacts.getPassword());
					updateUser.setUserName(customerContacts
							.getLoginDisplayName());
					// updateUser.setUserLogin(customerContacts.getLoginId());
					updateUser.setUserPassword(encyppassword);
					updateUser.setNewPassword("");
					updateUser.setUserEmailId(customerContacts.getEmailId());
					updateUser.setKeyvalue(randVal);

					updateUser.setIsActive((byte) 1);

					customerSession.update(updateUser); // update user data into
					// Customer DB.
				}
				session.getTransaction().commit();
				customerSession.getTransaction().commit();
				log.info("=============== customer profile updated =========");

			}

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			if (customerSession != null
					&& customerSession.getTransaction() != null)
				customerSession.getTransaction().rollback();
			result = "failure";
		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (customerSession != null && customerSession.isConnected()) {
				try {
					customerSession.connection().close();
					customerSession.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateCustomerInfoByCustomer(CustomerForm customerForm,
			Integer userId, UserDetailsDto userDetails) {

		/* session object represents FG master database */
		Session fgSession = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		/* customerSession object represents Customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Customer customerInfo;
		String result = "failure";
		try {
			session.beginTransaction();
			fgSession.beginTransaction();
			customerInfo = (Customer) session.createCriteria(Customer.class)
					.add(Restrictions.eq("id", customerForm.getId()))
					.uniqueResult();
			if (customerInfo != null) {
				/* check the new user name is exist. */
				if (!customerForm.getHiddenEmailId().equalsIgnoreCase(
						customerForm.getContanctEmail())) {
					String uniqueUserName = checkLoginUserName(customerForm,
							userDetails);
					if (!uniqueUserName.equalsIgnoreCase("submit")) {
						return uniqueUserName;
					}

				}

				customerInfo.setCustName(customerForm.getCompanyname());
				customerInfo.setDunsNumber(customerForm.getDunsnum());
				customerInfo.setCustCode(customerForm.getCompanycode());
				customerInfo.setTaxId(customerForm.getTaxid());
				customerInfo.setAddress1(customerForm.getAddress1());
				// customerInfo.setAddress2(customerForm.getAddress2());
				// customerInfo.setAddress3(customerForm.getAddress3());
				customerInfo.setCity(customerForm.getCity());
				customerInfo.setState(customerForm.getState());
				customerInfo.setProvince(customerForm.getProvince());
				customerInfo.setZipCode(customerForm.getZipcode());
				customerInfo.setRegion(customerForm.getRegion());
				customerInfo.setCountry(customerForm.getCountry());
				customerInfo.setPhone(customerForm.getPhone());
				customerInfo.setFax(customerForm.getFax());
				customerInfo.setWebsite(customerForm.getWebSite());
				customerInfo.setMobile(customerForm.getMobile());

				customerInfo.setEmailId(customerForm.getEmail());
				customerInfo.setIsActive(((byte) 1));
				customerInfo.setModifiedBy(userId);
				customerInfo.setModifiedOn(new Date());

				session.update(customerInfo);

				/* Update customer contact information in to customer database. */
				CustomerContacts customerContacts = (CustomerContacts) session
						.createCriteria(CustomerContacts.class)
						.add(Restrictions.eq("id", customerForm.getContactId()))
						.uniqueResult();

				if (customerContacts != null) {

					/* Get the user from the customer Database. */
					Users user = (Users) session
							.createCriteria(Users.class)
							.add(Restrictions.eq("userEmailId",
									customerContacts.getEmailId()))
							.uniqueResult();

					if (user != null) {
						// Random random = new java.util.Random();
						Integer randVal = user.getKeyvalue();

						Encrypt encrypt = new Encrypt();
						/* convert password to encrypted password */
						String encyppassword = encrypt.encryptText(
								Integer.toString(randVal) + "",
								customerForm.getLoginpassword());
						user.setUserName(customerForm.getLoginDisplayName());
						// user.setUserLogin(customerForm.getLoginId());
						user.setUserPassword(encyppassword);
						user.setNewPassword("");
						user.setUserEmailId(customerForm.getContanctEmail());
						user.setKeyvalue(randVal);
						session.update(user);

					}
					// customerContacts.setCustomerId(customerInfo);
					customerContacts.setFirstName(customerForm.getFirstName());
					customerContacts.setLastName(customerForm.getLastName());
					customerContacts.setDesignation(customerForm
							.getDesignation());
					customerContacts.setPhoneNumber(customerForm
							.getContactPhone());
					customerContacts.setMobile(customerForm.getContactMobile());
					customerContacts.setFax(customerForm.getContactFax());
					customerContacts
							.setEmailId(customerForm.getContanctEmail());
					customerContacts.setIsPrimaryContact((byte) 1);
					customerContacts.setIsAllowedLogin(CommonUtils
							.getByteValue(true));
					customerContacts.setLoginDisplayName(customerForm
							.getLoginDisplayName());
					// customerContacts.setLoginId(customerForm.getLoginId());
					customerContacts.setPassword(customerForm
							.getLoginpassword());

					SecretQuestion secretQuestion = new SecretQuestion();
					secretQuestion = (SecretQuestion) session.get(
							SecretQuestion.class, customerForm.getUserSecQn());

					customerContacts.setSecretQuestionId(secretQuestion);
					customerContacts.setSecQueAns(customerForm
							.getUserSecQnAns());

					customerContacts.setIsActive((byte) 1);

					customerContacts.setModifiedBy(userId);
					customerContacts.setModifiedOn(new Date());
					session.update(customerContacts);
				}

				List<com.fg.vms.customer.model.CustomerApplicationSettings> applicationSettings = (List<com.fg.vms.customer.model.CustomerApplicationSettings>) session
						.createQuery(
								"From com.fg.vms.customer.model.CustomerApplicationSettings ")
						.list();

				if (applicationSettings != null
						&& applicationSettings.size() != 0) {

					applicationSettings.get(0).setCustomerId(customerInfo);
					applicationSettings.get(0).setLogoPath(
							customerForm.getLogoImagePath());
					applicationSettings.get(0).setMenuSelectColor(
							customerForm.getMenuColor());
					applicationSettings.get(0).setIsDivision(
							customerForm.getIsDivision());
					applicationSettings.get(0).setSessionTimeout(
							customerForm.getSessionTimeout());
					applicationSettings.get(0).setTermsCondition(
							customerForm.getTermsCondition());
					applicationSettings.get(0).setPrivacyTerms(
							customerForm.getPrivacyTerms());
					applicationSettings.get(0).setPrimeTrainingUrl(customerForm.getPrimeTrainingUrl());
					applicationSettings.get(0).setNonPrimeTrainingUrl(customerForm.getNonPrimeTrainingUrl());
					session.update(applicationSettings.get(0));
				}

				CustomerContacts contacts = (CustomerContacts) fgSession
						.createCriteria(CustomerContacts.class)
						.add(Restrictions.eq("emailId",
								customerForm.getHiddenEmailId()))
						.uniqueResult();
				CustomerApplicationSettings settings = null;
				Customer customer = null;
				if (contacts != null) {
					customer = (Customer) fgSession
							.createCriteria(Customer.class)
							.add(Restrictions.eq("id", contacts.getCustomerId()
									.getId())).uniqueResult();

					contacts.setFirstName(customerForm.getFirstName());
					contacts.setLastName(customerForm.getLastName());
					contacts.setDesignation(customerForm.getDesignation());
					contacts.setPhoneNumber(customerForm.getContactPhone());
					contacts.setFax(customerForm.getContactFax());
					contacts.setMobile(customerForm.getContactMobile());
					contacts.setEmailId(customerForm.getContanctEmail());
					contacts.setLoginDisplayName(customerForm
							.getLoginDisplayName());
					// contacts.setLoginId(customerForm.getLoginId());
					contacts.setPassword(customerForm.getLoginpassword());
					SecretQuestion secretQuestion = new SecretQuestion();
					secretQuestion = (SecretQuestion) fgSession.get(
							SecretQuestion.class, customerForm.getUserSecQn());
					contacts.setSecretQuestionId(secretQuestion);
					contacts.setSecQueAns(customerForm.getUserSecQnAns());

					fgSession.update(contacts);
					if (customer != null) {
						settings = (CustomerApplicationSettings) fgSession
								.createCriteria(
										CustomerApplicationSettings.class)
								.add(Restrictions.eq("customerId", customer))
								.uniqueResult();

						customer.setCustName(customerForm.getCompanyname());
						customer.setDunsNumber(customerForm.getDunsnum());
						customer.setCustCode(customerForm.getCompanycode());
						customer.setTaxId(customerForm.getTaxid());
						customer.setAddress1(customerForm.getAddress1());
						customer.setAddress2(customerForm.getAddress2());
						customer.setAddress3(customerForm.getAddress3());
						customer.setCity(customerForm.getCity());
						customer.setState(customerForm.getState());
						customer.setProvince(customerForm.getProvince());
						customer.setZipCode(customerForm.getZipcode());
						customer.setRegion(customerForm.getRegion());
						customer.setCountry(customerForm.getCountry());
						customer.setPhone(customerForm.getPhone());
						customer.setFax(customerForm.getFax());
						customer.setWebsite(customerForm.getWebSite());
						customer.setMobile(customerForm.getMobile());
						customer.setIsActive(((byte) 1));
						customer.setModifiedBy(userId);
						customer.setModifiedOn(new Date());
						fgSession.update(customer);
					}
				}

				if (settings != null) {
					settings.setCustomerId(customer);
					settings.setLogoPath(customerForm.getLogoImagePath());
					settings.setMenuSelectColor(customerForm.getMenuColor());
					settings.setSessionTimeout(customerForm.getSessionTimeout());
					fgSession.update(settings);
				}
				session.getTransaction().commit();

				fgSession.getTransaction().commit();

				result = "success";
				log.info("=============== customer profile updated =========");
			}
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
			fgSession.getTransaction().rollback();
		} catch (Exception ex) {
			PrintExceptionInLogFile.printException(ex);
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> displayCustomers(UserDetailsDto userDetails) {

		log.info("Inside the diaplayCustomer method.....");
		List<Customer> customers = null;

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		try {
			session.beginTransaction();
			customers = session.createQuery(
					"from Customer where isActive=1 ORDER BY custName ASC")
					.list();

			if (customers != null) {
				log.info("No of Cusomers:" + customers.size());
			} else {
				log.info("No of Cusomers:" + 0);
			}

			session.getTransaction().commit();
			log.info("Transaction commited...");
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		}

		return customers;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> displaySingleCustomer(UserDetailsDto userDetails) 
	{
		log.info("Inside the displaySingleCustomer method.....");
		List<Customer> customers = null;

		Session session = HibernateUtil.buildSessionFactory().getCurrentSession();

		try 
		{
			session.beginTransaction();
			//Retrieve Only BP
			customers = session.createQuery("from Customer where id = 1 AND isActive = 1 ORDER BY custName ASC").list();

			if (customers != null) 
			{
				log.info("No of Cusomers:" + customers.size());
			} 
			else 
			{
				log.info("No of Cusomers:" + 0);
			}

			session.getTransaction().commit();
			log.info("Transaction commited...");
		} 
		catch (HibernateException exception) 
		{
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		}
		return customers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerInfoDto retriveCustomer(Integer customerId,
			UserDetailsDto userDetails) {

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		Customer customer = null;
		List<CustomerContacts> contacts;
		List<CustomerSLA> customerSLA;
		List<CustomerApplicationSettings> applicationSettings;
		CustomerInfoDto customerInfoDto = new CustomerInfoDto();

		try {
			session.beginTransaction();
			/* Get the customer objects. */
			customer = (Customer) session.get(Customer.class, customerId);

			/* Get the customer contacts. */
			contacts = session.createCriteria(CustomerContacts.class)
					.add(Restrictions.eq("customerId", customer))
					.add(Restrictions.eq("isActive", (byte) 1)).list();

			customerSLA = session.createCriteria(CustomerSLA.class)
					.add(Restrictions.eq("customerId", customer)).list();

			/* Get the customer application settings */
			applicationSettings = session
					.createCriteria(CustomerApplicationSettings.class)
					.add(Restrictions.eq("customerId", customer)).list();
			customerInfoDto.setCustomer(customer);
			customerInfoDto.setContacts(contacts);
			if (customerSLA != null && customerSLA.size() != 0) {
				customerInfoDto.setCustomerSLA(customerSLA.get(0));
			}
			customerInfoDto.setApplicationSettings(applicationSettings.get(0));

			session.getTransaction().commit();
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		}
		return customerInfoDto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerInfoDto retriveCustomerInfoFromCustomerDB(
			UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Customer customer = null;
		List<CustomerContacts> contacts;
		List<com.fg.vms.customer.model.CustomerApplicationSettings> applicationSettings;
		CustomerInfoDto customerInfoDto = new CustomerInfoDto();

		try {
			session.beginTransaction();
			/* Get the customer objects. */
			customer = (Customer) session.createCriteria(Customer.class)
					.uniqueResult();

			/* Get the customer contacts. */
			contacts = session.createCriteria(CustomerContacts.class)
					.add(Restrictions.eq("customerId", customer))
					.add(Restrictions.eq("isActive", (byte) 1)).list();

			/* Get the customer application settings */
			applicationSettings = session
					.createCriteria(
							com.fg.vms.customer.model.CustomerApplicationSettings.class)
					.add(Restrictions.eq("customerId", customer)).list();
			customerInfoDto.setCustomer(customer);
			customerInfoDto.setContacts(contacts);
			customerInfoDto.setSettings(applicationSettings.get(0));

			session.getTransaction().commit();
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return customerInfoDto;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SecretQuestion> displaySecretQuestions(
			UserDetailsDto userDetails) {

		List<SecretQuestion> secretQnsList = null;
		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {

			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		try {
			session.beginTransaction();
			secretQnsList = session.createQuery(
					" from SecretQuestion where isActive=1").list();
			session.getTransaction().commit();

		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return secretQnsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addContact(Customer customer,
			CustomerContactForm contactForm, Integer currentUser,
			UserDetailsDto userDetails) {

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();
		String result;
		try {
			session.beginTransaction();
			CustomerContacts contact = new CustomerContacts();
			contact.setFirstName(contactForm.getFirstName());
			contact.setLastName(contactForm.getLastName());
			contact.setDesignation(contactForm.getDesignation());
			contact.setEmailId(contactForm.getContanctEmail());
			contact.setFax(contactForm.getContactFax());
			contact.setIsAllowedLogin((byte) 0);
			contact.setIsPrimaryContact((byte) 0);
			contact.setIsActive((byte) 1);
			contact.setLoginDisplayName(contactForm.getLoginDisplayName());
			contact.setLoginId(contactForm.getLoginId());
			contact.setPassword(contactForm.getLoginpassword());
			contact.setPhoneNumber(contactForm.getContactPhone());
			contact.setCreatedBy(currentUser);
			contact.setCreatedOn(new Date());
			contact.setMobile(contactForm.getContactMobile());
			contact.setModifiedBy(currentUser);
			contact.setModifiedOn(new Date());

			SecretQuestion secretQuestion = new SecretQuestion();
			secretQuestion = (SecretQuestion) session.get(SecretQuestion.class,
					contactForm.getUserSecQn());

			contact.setSecretQuestionId(secretQuestion);
			contact.setSecQueAns(contactForm.getUserSecQnAns());

			contact.setCustomerId(customer);
			session.save(contact);
			result = "success";
			session.getTransaction().commit();

		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
			return "failure";
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerContacts retriveContact(Integer contactId,
			UserDetailsDto userDetails) {
		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();
		CustomerContacts contact = null;
		try {
			session.beginTransaction();
			/* Get the contact object. */
			contact = (CustomerContacts) session.get(CustomerContacts.class,
					contactId);

			session.getTransaction().commit();
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		}

		return contact;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateContact(Customer customer,
			CustomerContactForm contactForm, Integer currentUser,
			UserDetailsDto userDetails) {
		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();
		String result;
		try {
			session.beginTransaction();
			CustomerContacts contact = (CustomerContacts) session.get(
					CustomerContacts.class, contactForm.getId());
			contact.setFirstName(contactForm.getFirstName());
			contact.setLastName(contactForm.getLastName());
			contact.setDesignation(contactForm.getDesignation());
			contact.setEmailId(contactForm.getContanctEmail());
			contact.setFax(contactForm.getContactFax());

			contact.setIsAllowedLogin(CommonUtils.getByteValue(contactForm
					.isLoginAllowed()));
			contact.setIsPrimaryContact((byte) 0);
			contact.setIsActive((byte) 1);
			contact.setLoginDisplayName(contactForm.getLoginDisplayName());
			contact.setLoginId(contactForm.getLoginId());
			contact.setPassword(contactForm.getLoginpassword());

			contact.setPhoneNumber(contactForm.getContactPhone());

			contact.setMobile(contactForm.getContactMobile());
			contact.setModifiedBy(currentUser);
			contact.setModifiedOn(new Date());

			SecretQuestion secretQuestion = new SecretQuestion();
			secretQuestion = (SecretQuestion) session.get(SecretQuestion.class,
					contactForm.getUserSecQn());

			contact.setSecretQuestionId(secretQuestion);
			contact.setSecQueAns(contactForm.getUserSecQnAns());

			// contact.setCustomerId(customer);
			session.update(contact);
			result = "success";
			session.getTransaction().commit();

		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
			return "failure";
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteContact(Integer contactId, UserDetailsDto userDetails) {
		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();
		String result;
		try {
			session.beginTransaction();
			CustomerContacts contact = (CustomerContacts) session.get(
					CustomerContacts.class, contactId);

			contact.setIsActive((byte) 0);

			session.update(contact);
			result = "success";
			session.getTransaction().commit();

		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
			return "failure";
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteCustomer(Integer customerId, UserDetailsDto userDetails) {

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();
		Customer customer = null;
		String result;
		try {
			session.beginTransaction();
			/* Get the customer objects. */
			customer = (Customer) session.get(Customer.class, customerId);

			customer.setIsActive((byte) 0);
			session.update(customer);
			session.getTransaction().commit();
			result = "delete";
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
			return "failure";
		}
		return result;
	}

	/**
	 * This method is used to pack the customer information from form to Entity
	 * 
	 * @param customerForm
	 * @param request
	 * @param userId
	 * @return the customer entity
	 */
	private Customer packCustomerEntity(CustomerForm customerForm,
			Integer userId) {

		Customer customerInfo = new Customer();
		customerInfo.setCustName(customerForm.getCompanyname());
		customerInfo.setDunsNumber(customerForm.getDunsnum());
		customerInfo.setCustCode(customerForm.getCompanycode());
		customerInfo.setTaxId(customerForm.getTaxid());
		customerInfo.setAddress1(customerForm.getAddress1());
		customerInfo.setAddress2(customerForm.getAddress2());
		customerInfo.setAddress3(customerForm.getAddress3());
		customerInfo.setCity(customerForm.getCity());
		customerInfo.setState(customerForm.getState());
		customerInfo.setProvince(customerForm.getProvince());
		customerInfo.setZipCode(customerForm.getZipcode());
		customerInfo.setRegion(customerForm.getRegion());
		customerInfo.setCountry(customerForm.getCountry());
		customerInfo.setPhone(customerForm.getPhone());
		customerInfo.setMobile(customerForm.getMobile());
		customerInfo.setFax(customerForm.getFax());
		customerInfo.setEmailId(customerForm.getEmail());
		customerInfo.setWebsite(customerForm.getWebSite());

		customerInfo.setDatabaseIp(customerForm.getDatabaseIp());

		customerInfo.setUserName(customerForm.getUserName());
		customerInfo.setDbpassword(customerForm.getDbpassword());
		customerInfo.setDatabaseName(customerForm.getDatabaseName());
		customerInfo.setApplicationUrl(customerForm.getApplicationUrl() + "."
				+ Constants.getString(Constants.DOMAIN_NAME).toString());
		customerInfo.setApplicationServer(customerForm.getApplicationServer());
		customerInfo.setIsActive(((byte) 1));
		customerInfo.setCreatedBy(userId);
		customerInfo.setCreatedOn(new Date());
		customerInfo.setModifiedBy(userId);
		customerInfo.setModifiedOn(new Date());

		return customerInfo;

	}

	/**
	 * This method used to set the form values to entity
	 * 
	 * @param customerForm
	 * @param request
	 * @param session
	 * @param userId
	 * @return customerContact
	 */
	private CustomerContacts packContactEntity(CustomerForm customerForm,
			Integer userId) {

		CustomerContacts customerContacts = new CustomerContacts();

		customerContacts.setFirstName(customerForm.getFirstName());
		customerContacts.setLastName(customerForm.getLastName());
		customerContacts.setDesignation(customerForm.getDesignation());
		customerContacts.setPhoneNumber(customerForm.getContactPhone());
		customerContacts.setMobile(customerForm.getContactMobile());
		customerContacts.setFax(customerForm.getContactFax());
		customerContacts.setEmailId(customerForm.getContanctEmail());
		customerContacts.setIsPrimaryContact((byte) 1);
		customerContacts.setIsAllowedLogin(CommonUtils.getByteValue(true));
		customerContacts
				.setLoginDisplayName(customerForm.getLoginDisplayName());
		// customerContacts.setLoginId(customerForm.getLoginId());
		customerContacts.setPassword(customerForm.getLoginpassword());

		// SecretQuestion secretQuestion;
		// secretQuestion = (SecretQuestion) session.get(SecretQuestion.class,
		// customerForm.getUserSecQn());

		// customerContacts.setSecretQuestionId(secretQuestion);
		customerContacts.setSecQueAns(customerForm.getUserSecQnAns());

		customerContacts.setIsActive((byte) 1);
		customerContacts.setCreatedBy(userId);
		customerContacts.setCreatedOn(new Date());
		customerContacts.setModifiedBy(userId);
		customerContacts.setModifiedOn(new Date());

		return customerContacts;

	}

	/**
	 * Verify the user name is already existed or not when update the customer
	 * information by customer.
	 * 
	 * @param form
	 * @param userDetails
	 * @return
	 */
	private static String checkLoginUserName(CustomerForm form,
			UserDetailsDto userDetails) {
		Session session = null;

		session = HibernateUtilCustomer.buildSessionFactory().openSession(
				DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		List<Users> userLogin = session.createCriteria(Users.class)
				.add(Restrictions.eq("userEmailId", form.getContanctEmail()))
				.list();

		List<VendorContact> vendorLogin = session
				.createCriteria(VendorContact.class)
				.add(Restrictions.eq("emailId", form.getContanctEmail()))
				.list();

		// session.getTransaction().commit();
		String result = "submit";
		if (userLogin != null && userLogin.size() != 0) {
			return "available";
		}
		if (vendorLogin != null && vendorLogin.size() != 0) {
			return "available";
		}
		return result;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.fg.vms.admin.dao.CustomerDao#checkDuplicateCustCode(java.lang.String,
	 *      com.fg.vms.admin.dto.UserDetailsDto)
	 */
	@Override
	public String checkDuplicateCustCode(String compCode,
			UserDetailsDto userDetails) {
		String result = "success";
		List<Customer> customerCode = null;

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		try {
			session.beginTransaction();

			customerCode = session.createCriteria(Customer.class)
					.add(Restrictions.eq("custCode", compCode)).list();

		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		}

		if (customerCode != null && customerCode.size() != 0) {
			result = "duplicate";
		}

		return result;
	}

	@Override
	public String checkDuplicateDatabaseName(String databaseName,
			UserDetailsDto userDetails) {
		String result = "success";
		List<Customer> customerCode = null;

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		try {
			session.beginTransaction();
			customerCode = session.createCriteria(Customer.class)
					.add(Restrictions.eq("databaseName", databaseName)).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		}

		if (customerCode != null && customerCode.size() != 0) {
			result = "duplicate";
		}

		return result;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.fg.vms.ajax.validation.ValidationDao#validateLoginId(java.lang.String
	 *      , com.fg.vms.admin.dto.UserDetailsDto)
	 */
	@Override
	public String validateLoginId(String loginId, UserDetailsDto appDetails) {
		Session session = null;

		session = HibernateUtil.buildSessionFactory().getCurrentSession();
		String result = "success";
		try {
			session.beginTransaction();

			/* Check the unique user login */
			List<CustomerContacts> customer = session
					.createCriteria(CustomerContacts.class)
					.add(Restrictions.eq("emailId", loginId)).list();

			if (customer != null && customer.size() != 0) {
				/* loginId is Exist. */
				result = "duplicate";
			}
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		}

		return result;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.fg.vms.ajax.validation.ValidationDao#checkDuplicateEmail(java.lang
	 *      .String, com.fg.vms.admin.dto.UserDetailsDto)
	 */
	@Override
	public String checkDuplicateEmail(String emailId, UserDetailsDto appDetails) {
		Session session = null;

		session = HibernateUtil.buildSessionFactory().getCurrentSession();

		String result = "success";
		try {
			session.beginTransaction();

			/* Check the unique user EmailId */
			List<CustomerContacts> customer = session
					.createCriteria(CustomerContacts.class)
					.add(Restrictions.eq("emailId", emailId)).list();

			if (customer != null && customer.size() != 0) {
				/* EmailId is Exist. */
				result = "duplicate";
			}
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String checkDuplicateURL(String url, UserDetailsDto userDetails) {

		Session session = null;
		String result = "success";
		try {

			session = HibernateUtil.buildSessionFactory().getCurrentSession();

			session.beginTransaction();

			List<Customer> customer = session.createCriteria(Customer.class)
					.add(Restrictions.eq("applicationUrl", url))
					.add(Restrictions.eq("isActive", (byte) 1)).list();
			if (customer != null && customer.size() != 0) {
				/* EmailId is Exist. */
				result = "duplicate";
			}

		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			if (session != null)
				session.getTransaction().rollback();
			return "failure";
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String checkDuplicateDunsNo(String dunsNo, UserDetailsDto userDetails) {
		String result = "success";
		List<Customer> dunsNumber = null;

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		try {
			session.beginTransaction();

			dunsNumber = session.createCriteria(Customer.class)
					.add(Restrictions.eq("dunsNumber", dunsNo)).list();

		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		}

		if (dunsNumber != null && dunsNumber.size() != 0) {
			result = "duplicate";
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String checkDuplicateTaxId(String taxId, UserDetailsDto userDetails) {
		String result = "success";
		List<Customer> taxNumber = null;

		Session session = HibernateUtil.buildSessionFactory()
				.getCurrentSession();

		try {
			session.beginTransaction();
			taxNumber = session.createCriteria(Customer.class)
					.add(Restrictions.eq("taxId", taxId)).list();
		} catch (HibernateException exception) {
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";
		}

		if (taxNumber != null && taxNumber.size() != 0) {
			result = "duplicate";
		}

		return result;
	}

	@Override
	public CustomerContacts findCustomerContact(UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		List<CustomerContacts> contacts;
		CustomerContacts contact = null;
		try {
			session.beginTransaction();

			/* Get the customer contacts. */
			contacts = session.createCriteria(CustomerContacts.class)
					.add(Restrictions.eq("isActive", (byte) 1))
					.add(Restrictions.eq("isPrimaryContact", (byte) 1)).list();

			if (contacts != null && !contacts.isEmpty()) {
				contact = contacts.get(0);
			}

			session.getTransaction().commit();
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return contact;
	}

}
