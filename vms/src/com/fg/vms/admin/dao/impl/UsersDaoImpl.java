/*
 * UsersDaoImpl.java 
 */
package com.fg.vms.admin.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.CustomerTimeZoneMaster;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.UserRoles;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.UsersForm;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerStakeholdersRegistration;
import com.fg.vms.customer.model.CustomerUserDivision;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PasswordUtil;
import com.fg.vms.util.PrintExceptionInLogFile;
import com.fg.vms.util.SendEmail;

/**
 * Represents the Users service implementations.
 * 
 * @author vinoth
 * @param <T>
 * 
 */
public class UsersDaoImpl<T> implements UsersDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createUser(UsersForm usersForm, int currentUserId,
			UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		String result = checkUserLoginAndEmailIdExist(usersForm.getUserLogin(),
				usersForm.getUserEmailId(), userDetails);
		try {
			Random random = new java.util.Random();
			Integer randVal = random.nextInt();

			Encrypt encrypt = new Encrypt();

			/* convert password to encrypted password. */
			String encyppassword = encrypt
					.encryptText(Integer.toString(randVal) + "",
							usersForm.getUserPassword());

			session.beginTransaction();
			if (result.equals("submit")) {
				Users users = new Users();

				UserRoles userRole = (UserRoles) session.get(UserRoles.class,
						usersForm.getRoleId());

				SecretQuestion secuQuestion = (SecretQuestion) session.get(
						SecretQuestion.class, usersForm.getSecretQuestionId());

				/* Insert the user details into database */
				users.setUserRoleId(userRole);
				users.setUserName(usersForm.getUserName());
				// users.setUserLogin(usersForm.getUserLogin());
				users.setUserPassword(encyppassword);
				users.setKeyvalue(randVal);
				users.setUserEmailId(usersForm.getUserEmailId());
				users.setIsActive((byte) 1);
				users.setCreatedBy(currentUserId);
				users.setCreatedOn(new Date());
				users.setSecretQuestionId(secuQuestion);
				users.setSecQueAns(usersForm.getSecQueAns());
				users.setDepartment(usersForm.getDepartment());
				users.setDivision(usersForm.getDivision());
				users.setPhoneNumber(usersForm.getPhoneNumber());
				users.setExtension(usersForm.getExtension());
				users.setFirstName(usersForm.getFirstName());
				users.setLastName(usersForm.getLastName());
				users.setTitle(usersForm.getTitle());
				users.setIsSysGenPassword((byte) 0);

				if (usersForm.getTimeZone() != null) {
					CustomerTimeZoneMaster timeZoneMaster = (CustomerTimeZoneMaster) session
							.get(CustomerTimeZoneMaster.class,
									usersForm.getTimeZone());
					users.setTimeZone(timeZoneMaster);
				}
				session.save(users);
			//Changed customerDivisionId to list of CustomerDivisionIds
				/*if (usersForm.getCustomerDivision() != null) {
					CustomerDivision customerDivision = (CustomerDivision) session
							.get(CustomerDivision.class,
									usersForm.getCustomerDivision());
					users.setCustomerDivisionId(customerDivision);
				}*/
				
				
				if (usersForm.getCustomerDivisionId() != null
						&& usersForm.getCustomerDivisionId().length != 0) {

					for (String division : usersForm.getCustomerDivisionId()) {
						CustomerUserDivision userDivision = new CustomerUserDivision();
						CustomerDivision customerDivision = (CustomerDivision) session
								.get(CustomerDivision.class,
										Integer.parseInt(division));
						
						userDivision.setCustomerDivisionId(customerDivision);
						userDivision.setUserId(users);
						userDivision.setIsActive((byte) 1);
						userDivision.setCreatedBy(currentUserId);
						userDivision.setCreatedOn(new Date());
						session.save(userDivision);
					}
				}
				
				// password History
				PasswordHistory passwordHistory = new PasswordHistory();
				passwordHistory.setKeyvalue(randVal);
				passwordHistory.setChangedon(new Date());
				passwordHistory.setChangedby(currentUserId);
				passwordHistory.setPassword(encyppassword);
				passwordHistory.setCustomerUserID(users.getId());
				session.save(passwordHistory);
				session.getTransaction().commit();
				result = "success";
			} else {
				return result;
			}

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	 @Override
	 public List<Users> listUsers(UserDetailsDto userDetails, Boolean isAdmin) {
	  Session session = null;

	  if (userDetails.getUserType() != null
	    && userDetails.getUserType().equalsIgnoreCase("FG")) {
	   session = HibernateUtil.buildSessionFactory().getCurrentSession();
	  } else {
	   session = HibernateUtilCustomer.buildSessionFactory().openSession(
	     DataBaseConnection.getConnection(userDetails));
	  }
	  session.beginTransaction();
	  List<Users> users = null;
	  try {

	   /* Fetch the list of active users. */
	   if (isAdmin) {
	    users = session.createQuery("from Users ORDER BY userName ASC")
	      .list();
	   } else {
	    users = session.createQuery(
	      "from Users where isActive=1 ORDER BY userName ASC")
	      .list();
	   }

	   session.getTransaction().commit();
	  } catch (HibernateException exception) {
	   /* print the exception in log file. */
	   PrintExceptionInLogFile.printException(exception);
	   session.getTransaction().rollback();
	  } finally {
	   if (userDetails.getUserType() != null
	     && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
	    try {
	     session.connection().close();
	     session.close();
	    } catch (HibernateException e) {
	     e.printStackTrace();
	    } catch (SQLException e) {
	     e.printStackTrace();
	    }
	   }
	  }
	  return users;
	 }
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Users> listUsers(UserDetailsDto userDetails, Boolean isAdmin, String divisionIds)
	{
		Session session = null;

		if (userDetails.getUserType() != null && userDetails.getUserType().equalsIgnoreCase("FG"))
		{
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		}
		else
		{
			session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		}
		
		session.beginTransaction();
		List<Users> users = null;
		try
		{
			StringBuilder sqlQuery = new StringBuilder();
			
			sqlQuery.append("SELECT DISTINCT u FROM CustomerUserDivision cud "
					+ "INNER JOIN cud.userId u "
					+ "WHERE u.isActive = 1 AND cud.isActive = 1 ");
			
			if (divisionIds != null && divisionIds.length() != 0)
			{
				sqlQuery.append("AND cud.customerDivisionId IN (" + divisionIds + ") ");
			}
			
			sqlQuery.append("ORDER BY u.userName ASC");		
			
			/* Fetch the list of active users. */
			if (isAdmin)
			{
				users = session.createQuery("from Users ORDER BY userName ASC").list();
			}
			else
			{
				users = session.createQuery(sqlQuery.toString()).list();
				// "from Users where isActive=1 and customerDivisionId in(1,2) ORDER BY userName ASC")						
			}
			session.getTransaction().commit();
		}
		catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		}
		finally
		{
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return users;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String deleteUser(Integer id, UserDetailsDto userDetails, Integer currentUserId) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		String result = "";
		Users user = null;
		try {

			/* delete the selected user */

			user = (Users) session.createCriteria(Users.class)
					.add(Restrictions.eq("id", id)).uniqueResult();
			if (user != null) {
				user.setIsActive((byte)0);
				user.setModifiedBy(currentUserId);
				user.setModifiedOn(new Date());
				session.update(user);
				//session.delete(user);
				result = "success";
				session.getTransaction().commit();
			} else {
				result = "failure";
			}
			// users.setIsActive((byte) 0);

		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			result = "failure";
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Users retriveUser(Integer id, UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		session.beginTransaction();
		Users user = null;
		try {

			/* View/Retrieve the user based on unique user id. */
			user = (Users) session.createCriteria(Users.class)
					.add(Restrictions.eq("id", id)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String updateUser(UsersForm usersForm, Integer id,
			Integer currentUserId, UserDetailsDto userDetails) {

		Session session = null;
		String result="";
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		if (!usersForm.getUserEmailId().equalsIgnoreCase(usersForm.getHiddenEmailId())) {
			 result = checkUserLoginAndEmailIdExist(
					usersForm.getUserLogin(), usersForm.getUserEmailId(),
					userDetails);
		}

		if (result.equals("userEmail")) {
			return result;
		}
		session.beginTransaction();
		Decrypt decrypt = new Decrypt();
		Users users = null;
		try {

			users = (Users) session.get(Users.class, id);
			UserRoles userRole = (UserRoles) session.get(UserRoles.class,
					usersForm.getRoleId());

			// Random random = new java.util.Random();
			Integer randVal = users.getKeyvalue();
			Encrypt encrypt = new Encrypt();

			String decryptedPassword = decrypt
					.decryptText(String.valueOf(randVal.toString()),
							users.getUserPassword());
			/* convert password to encrypted password. */
			String encyppassword = encrypt
					.encryptText(Integer.toString(randVal) + "",
							usersForm.getUserPassword());
			if (!decryptedPassword.equals(usersForm.getUserPassword())) {
				// password History
				// select * from passwordhistory p where p.customeruserid=6
				// order by p.CHANGEDON desc limit 12
				List<PasswordHistory> histories = session.createQuery(
						" From PasswordHistory where customerUserID="
								+ users.getId()
								+ " order by changedon desc limit 12").list();
				if (CommonUtils.checkPasswordLimitExist(histories,
						usersForm.getUserPassword()).equals("success")) {
					PasswordHistory passwordHistory = new PasswordHistory();
					passwordHistory.setKeyvalue(randVal);
					passwordHistory.setChangedon(new Date());
					passwordHistory.setChangedby(currentUserId);
					passwordHistory.setPassword(encyppassword);
					passwordHistory.setCustomerUserID(users.getId());
					session.save(passwordHistory);
				} else {
					return "exist";
				}
			}

			SecretQuestion secuQuestion = (SecretQuestion) session.get(
					SecretQuestion.class, usersForm.getSecretQuestionId());
			/* Update the retrieved user details. */
			users.setUserName(usersForm.getUserName());
			// users.setUserLogin(usersForm.getUserLogin());
			users.setUserPassword(encyppassword);
			users.setNewPassword("");
			users.setKeyvalue(randVal);
			users.setUserEmailId(usersForm.getUserEmailId());
			users.setUserRoleId(userRole);
			users.setModifiedBy(currentUserId);
			users.setModifiedOn(new Date());
			users.setIsActive(Byte.valueOf(usersForm.getActive()));
			users.setDepartment(usersForm.getDepartment());
			users.setDivision(usersForm.getDivision());
			users.setPhoneNumber(usersForm.getPhoneNumber());
			users.setExtension(usersForm.getExtension());
			users.setFirstName(usersForm.getFirstName());
			users.setLastName(usersForm.getLastName());
			users.setTitle(usersForm.getTitle());
			users.setSecretQuestionId(secuQuestion);
			users.setSecQueAns(usersForm.getSecQueAns());

			if (usersForm.getTimeZone() != null) {
				CustomerTimeZoneMaster timeZoneMaster = (CustomerTimeZoneMaster) session
						.get(CustomerTimeZoneMaster.class,
								usersForm.getTimeZone());
				users.setTimeZone(timeZoneMaster);
			}
			session.update(users);
			//Changed customerDivisionId to list of CustomerDivisionIds
			/*if (usersForm.getCustomerDivision() != null) {
				CustomerDivision customerDivision = (CustomerDivision) session
						.get(CustomerDivision.class,
								usersForm.getCustomerDivision());
				users.setCustomerDivisionId(customerDivision);
			}*/
			
			
			if (usersForm.getCustomerDivisionId() != null
					&& usersForm.getCustomerDivisionId().length != 0) {
				
				StringBuilder updatedDivisions = new StringBuilder();
				
				for (String division : usersForm.getCustomerDivisionId()) {
					
					updatedDivisions.append(division);
					updatedDivisions.append(",");
					
					CustomerDivision customerDivision = (CustomerDivision) session
							.get(CustomerDivision.class,
									Integer.parseInt(division));
					
					CustomerUserDivision customerUserDivision = (CustomerUserDivision) session
							.createCriteria(CustomerUserDivision.class)
							.add(Restrictions.eq("customerDivisionId", customerDivision))
							.add(Restrictions.eq("userId", users)).uniqueResult();
					
					if(customerUserDivision != null)
					{
						customerUserDivision.setModifiedBy(currentUserId);
						customerUserDivision.setModifiedOn(new Date());
						session.update(customerUserDivision);
					}
					else
					{
						CustomerUserDivision userDivision = new CustomerUserDivision();
						userDivision.setCustomerDivisionId(customerDivision);
						userDivision.setUserId(users);
						userDivision.setIsActive((byte) 1);
						userDivision.setCreatedBy(currentUserId);
						userDivision.setCreatedOn(new Date());
						session.save(userDivision);
					}
				}
				
				updatedDivisions.deleteCharAt(updatedDivisions.length() - 1);
				session.createQuery("delete CustomerUserDivision where userId="
						+ users.getId() +" and customerDivisionId not in("
						+ updatedDivisions.toString()+")").executeUpdate();
				
			}
			else
			{
				session.createQuery("delete CustomerUserDivision where userId="
						+ users.getId()).executeUpdate();
			}
			session.getTransaction().commit();
			return "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			return "failure";

		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			return "failure";
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String checkUserLoginAndEmailIdExist(String userLogin,
			String emailId, UserDetailsDto appDetails) {

		Session session = null;
		if (appDetails.getUserType() != null
				&& appDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(appDetails));
		}
		String result = "submit";
		try {
			session.beginTransaction();

			/* Check the unique user login */
			// Users user = (Users) session.createCriteria(Users.class)
			// .add(Restrictions.eq("userLogin", userLogin))
			// .uniqueResult();

			/* Check the unique user EmailId */
			Users userEmailId = (Users) session.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", emailId))
					.uniqueResult();

			VendorContact vendorEmailId = (VendorContact) session
					.createCriteria(VendorContact.class)
					.add(Restrictions.eq("emailId", emailId)).uniqueResult();

			/*
			 * if (user != null && userEmailId != null) { Check both user login
			 * and user Email validation fails. return "both"; } else if (user
			 * != null) { Check user login validation fails. return "userLogin";
			 * }
			 */
			if (userEmailId != null || vendorEmailId != null) {
				/* Check user Email validation fails. */
				return "userEmail";
			}
		} catch (HibernateException e) {
			PrintExceptionInLogFile.printException(e);
		} finally {
			if (appDetails.getUserType() != null
					&& !(appDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerTimeZoneMaster> listTimeZones(UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		List<CustomerTimeZoneMaster> zoneMasters = null;
		try {

			/* Fetch the list of active time zones. */
			zoneMasters = session
					.createQuery(
							"from CustomerTimeZoneMaster where isActive=1 ORDER BY zoneDesc ASC")
					.list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return zoneMasters;
	}

	@Override
	public Users authenticate(String userEmailId, UserDetailsDto userDetails) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		Users user = null;
		try {
			session.beginTransaction();
			/*
			 * Authenticate user by check the unique user login and active
			 * status
			 */
			user = (Users) session
					.createCriteria(Users.class)
					.add(Restrictions.eq("userEmailId", userEmailId)
							.ignoreCase()).uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			try {
				session.connection().close();
				session.close();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return user;
	}

	@Override
	public String save(Users user, UserDetailsDto userDetails) {
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		String result = "failure";
		try {
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<CustomerDivision> listCustomerDivisons(
			UserDetailsDto userDetails, boolean noBothDivision) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		List<CustomerDivision> customerDivisions = null;
		try {
			/* Fetch the list of active time zones. */
			
		StringBuilder searchQuery = new StringBuilder();
		
		searchQuery.append("from CustomerDivision where isactive=1 ");
		
		if(noBothDivision)
			searchQuery.append(" and divisionname not in('BOTH') ");
		
		searchQuery.append("ORDER BY divisionname ASC");
		
		customerDivisions = session
				.createQuery(searchQuery.toString()).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return customerDivisions;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String saveStakeHolder(UsersForm form, UserDetailsDto userDetails) 
	{
		/* session object represents customer database */
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		String result = "failure";
		
		try {
			session.beginTransaction();
			
			CustomerStakeholdersRegistration holderObject = new CustomerStakeholdersRegistration();
			holderObject.setEmailId(form.getStackHolderEmailId());
			holderObject.setFirstName(form.getStackHolderFirstName());
			if(form.getStackHolderLastName() != null)
				holderObject.setLastName(form.getStackHolderLastName());
			holderObject.setManagerName(form.getStackHolderManagerName());
			holderObject.setReason(form.getStackHolderReason());
			holderObject.setCreatedBy(0);
			holderObject.setCreatedOn(new Date());
			holderObject.setIsApproved("Not Approved");
			
			if(form.getCustomerDivision() != null && form.getCustomerDivision() != 0) {
				CustomerDivision customerDivObj = (CustomerDivision) session.createCriteria(CustomerDivision.class)
						.add(Restrictions.eq("id", form.getCustomerDivision())).uniqueResult();
				holderObject.setCustomerDivisionId(customerDivObj);
			}
			session.save(holderObject);
			
			session.getTransaction().commit();
			result = "success";
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			exception.printStackTrace();
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String checkDuplicateEmailInStakeHolders(UserDetailsDto userDetails, String emailId) 
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		String result = "mailnotexisted";
		
		try {
			session.beginTransaction();
			CustomerStakeholdersRegistration stackHolder = (CustomerStakeholdersRegistration) 
					session.createCriteria(CustomerStakeholdersRegistration.class)
					.add(Restrictions.eq("emailId", emailId).ignoreCase()).uniqueResult();
			
			if(stackHolder != null)
			{
				result = "mailexisted";
			}
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<CustomerStakeholdersRegistration> notApprovedStakeholders(UserDetailsDto userDetails) 
	{
		List<CustomerStakeholdersRegistration> stakeHolders = null;
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		
		try {
			session.beginTransaction();
			String query = "from CustomerStakeholdersRegistration where isApproved='Not Approved' order by firstName";
			stakeHolders = (List<CustomerStakeholdersRegistration> ) session.createQuery(query).list();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return stakeHolders;
	}

	@SuppressWarnings("deprecation")
	@Override
	public CustomerStakeholdersRegistration retriveStakeHolder(UserDetailsDto userDetails, Integer id) 
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		CustomerStakeholdersRegistration stakeHolder = null;
		
		try 
		{
			session.beginTransaction();
			stakeHolder = (CustomerStakeholdersRegistration) session.createCriteria(CustomerStakeholdersRegistration.class).add(Restrictions.eq("id", id)).uniqueResult();
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return stakeHolder;
	}

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Override
	public String stakeHolderApproved(UserDetailsDto userDetails, Integer stakeHolderId, String appRoot, Integer currentUserId, String isApproved) 
	{
		Session session = HibernateUtilCustomer.buildSessionFactory().openSession(DataBaseConnection.getConnection(userDetails));
		String psw = String.valueOf(PasswordUtil.generatePswd(8, 8, 1, 1, 0));
		String finalResult = "success";
		String result = "success";
		Integer customerDivisionId = null;
		
		try 
		{
			session.beginTransaction();
			CustomerStakeholdersRegistration stakeHolder = retriveStakeHolder(userDetails, stakeHolderId);
			
			// Save this data into User , send credentials to this stake holder user and update Stakeholder
			if(stakeHolder !=null && isApproved.equalsIgnoreCase("Approved"))
			{
				if(stakeHolder.getCustomerDivisionId() != null)
					customerDivisionId = stakeHolder.getCustomerDivisionId().getId();
				
				Users newUser = new Users();
				newUser.setUserEmailId(stakeHolder.getEmailId());
				newUser.setCreatedOn(new Date());
				newUser.setCreatedBy(0);
				// This is system generated password but we set 0 because of
				// user may be reset the password when complete the profile.
				newUser.setIsSysGenPassword((byte) 0);
				newUser.setUserName("-1");
				newUser.setFirstName(stakeHolder.getFirstName());
				if(stakeHolder.getLastName()!=null)
					newUser.setLastName(stakeHolder.getLastName());
				newUser.setSecQueAns("green");
				newUser.setIsActive((byte) 0);	
				
				Random random = new java.util.Random();
				Integer randVal = random.nextInt();

				Encrypt encrypt = new Encrypt();

				/* convert password to encrypted password. */
				String encypPassword = encrypt.encryptText(Integer.toString(randVal) + "", psw);
				CURDDao<?> curdDao = new CURDTemplateImpl();
				UserRoles role = (UserRoles) curdDao.find(userDetails, "from UserRoles where roleName='BP Stakeholder'");
				SecretQuestion question = (SecretQuestion) curdDao.find(userDetails, "from SecretQuestion where secQuestionParticular='What is your favorite color?'");				
				newUser.setSecretQuestionId(question);
				newUser.setUserRoleId(role);				
				newUser.setUserPassword(encypPassword);
				newUser.setKeyvalue(randVal);
				newUser.setCustomerDivisionId(stakeHolder.getCustomerDivisionId());
				
				session.save(newUser);
				
				if(result.equalsIgnoreCase("success"))
				{
					result = new SendEmail().sendRegistrationApprovedAlert(userDetails, stakeHolder.getEmailId(),psw ,
							customerDivisionId);
					if(result.equalsIgnoreCase("success"))
					{
						stakeHolder.setIsApproved("Approved");
						stakeHolder.setApprovedBy(currentUserId);
						stakeHolder.setApprovedOn(new Date());
						session.update(stakeHolder);
					}
					else if(result.equalsIgnoreCase("templateNotFound"))
					{
						session.flush();
						session.clear();
						session.getTransaction().rollback();
						return  result;
					}
					else
					{
						session.flush();
						session.clear();
						session.getTransaction().rollback();
						finalResult = "mailNotSending";
					}
				}
				else{
					session.flush();
					session.clear();
					session.getTransaction().rollback();
					finalResult = "userNotSaved";
				}
			}
			// Send unapproval mail to stake holder user and update stake holder.
			else if(stakeHolder !=null && isApproved.equalsIgnoreCase("UnApproved"))
			{

				String emailId = stakeHolder.getEmailId();
				if(stakeHolder.getCustomerDivisionId() != null)
					customerDivisionId = stakeHolder.getCustomerDivisionId().getId();
				
				result = new SendEmail().sendRegistrationUnApprovedAlert(userDetails, emailId, customerDivisionId);
				if(result.equalsIgnoreCase("success")){
					result = "success";
					stakeHolder.setApprovedBy(currentUserId);
					stakeHolder.setApprovedOn(new Date());
					stakeHolder.setIsApproved("Unapproved");
					session.update(stakeHolder);
				}
				else
					result = "unapprovedmailsentfail";	
			}
			else
			{
				finalResult = "noStakeHolder";
			}
			if(result.equalsIgnoreCase("success"))
			{
				session.getTransaction().commit();
				finalResult = "success";
			}
			else if(result.equalsIgnoreCase("templateNotFound"))
			{
				session.flush();
				session.clear();
				session.getTransaction().rollback();
				return  result;
			}
			else
			{
				finalResult = "failure";
				session.getTransaction().rollback();
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			finalResult = "failure";
			session.getTransaction().rollback();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return finalResult;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerUserDivision> listUserDivisionsByUser(UserDetailsDto userDetails, Integer userId) {

		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		List<CustomerUserDivision> customerUserDivisions = null;
		try {

			customerUserDivisions = (List<CustomerUserDivision>)session.
					createQuery("from CustomerUserDivision  where userId="+ userId).list();
			
			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return customerUserDivisions;
	}

	@Override
	public CustomerDivision getGlobalDivisionId(UserDetailsDto userDetails) {
		Session session = HibernateUtilCustomer.buildSessionFactory()
				.openSession(DataBaseConnection.getConnection(userDetails));

		session.beginTransaction();
		CustomerDivision customerDivision = null;
		try {
			customerDivision = (CustomerDivision) session.createQuery(
					"From CustomerDivision  where isGlobal=1 ")
					.uniqueResult();

			session.getTransaction().commit();
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (session.isConnected()) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					PrintExceptionInLogFile.printException(e);
				} catch (SQLException e) {
					PrintExceptionInLogFile.printException(e);
				}
			}
		}
		return customerDivision;
	}
}
