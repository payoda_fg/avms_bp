/**
 * 
 */
package com.fg.vms.admin.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.RolePrvilegesDao;
import com.fg.vms.admin.dto.RolesAndObjects;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.UserRoles;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.customer.model.VendorRolePrivileges;
import com.fg.vms.customer.model.VendorRoles;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * 
 * @author vinoth
 * 
 */
public class RolePrivilegesDaoImpl implements RolePrvilegesDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String persistPrivilegesByRole(RolesAndObjects rolesAndObjects,
			String roleName, Integer userId, UserDetailsDto userDetails) {

		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		String result = "";
		try {
			session.beginTransaction();
			RolePrivileges rolePrivileges = new RolePrivileges();

			UserRoles role = (UserRoles) session.get(UserRoles.class,
					Integer.parseInt(roleName));
			VMSObjects object = (VMSObjects) session
					.createCriteria(VMSObjects.class)
					.add(Restrictions.eq("objectName",
							rolesAndObjects.getObjectName())).uniqueResult();
			RolePrivileges privileges = (RolePrivileges) session
					.createCriteria(RolePrivileges.class)
					.add(Restrictions.and(Restrictions.eq("roleId", role),
							Restrictions.eq("objectId", object)))
					.uniqueResult();
			if (privileges == null) {

				rolePrivileges.setObjectId(object);

				rolePrivileges.setAdd(getByteValue(rolesAndObjects.isAdd()));
				rolePrivileges.setRoleId(role);
				rolePrivileges.setDelete(getByteValue(rolesAndObjects
						.isDelete()));
				rolePrivileges.setEnable(getByteValue(rolesAndObjects
						.isEnable()));
				rolePrivileges.setModify(getByteValue(rolesAndObjects
						.isModify()));
				rolePrivileges.setView(getByteValue(rolesAndObjects.isView()));
				rolePrivileges.setVisible(getByteValue(rolesAndObjects
						.isVisible()));
				rolePrivileges.setIsActive((byte) 1);
				rolePrivileges.setCreatedBy(userId);
				rolePrivileges.setCreatedOn(new Date());
				rolePrivileges.setModifiedBy(userId);
				rolePrivileges.setModifiedOn(new Date());
				session.save(rolePrivileges);
				session.getTransaction().commit();
				result = "success";
			} else if (privileges != null) {
				privileges.setAdd(getByteValue(rolesAndObjects.isAdd()));
				privileges.setDelete(getByteValue(rolesAndObjects.isDelete()));
				privileges.setEnable(getByteValue(rolesAndObjects.isEnable()));
				privileges.setModify(getByteValue(rolesAndObjects.isModify()));
				privileges.setView(getByteValue(rolesAndObjects.isView()));
				privileges
						.setVisible(getByteValue(rolesAndObjects.isVisible()));
				privileges.setModifiedBy(userId);
				privileges.setModifiedOn(new Date());
				session.update(privileges);
				session.getTransaction().commit();
				result = "success";
			}
		} catch (HibernateException exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
			result = "failure";
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RolePrivileges> retrievePrivilegesByRole(Integer roleId,
			UserDetailsDto userDetails) {

		Session session = null;
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		List<RolePrivileges> privileges = null;
		session.beginTransaction();

		String sqlQuery = "SELECT rolePrivileges FROM RolePrivileges rolePrivileges,VMSObjects vmsObject,UserRoles userRole "
				+ "WHERE rolePrivileges.objectId=vmsObject AND "
				+ "rolePrivileges.roleId=userRole AND "
				+ "rolePrivileges.roleId= :role and vmsObject.isActive=1 ORDER BY vmsObject.objectName ASC";
		try {
			UserRoles role = (UserRoles) session.get(UserRoles.class, roleId);
			Query query = session.createQuery(sqlQuery);
			query.setParameter("role", role);
			privileges = query.list();
			session.getTransaction().commit();
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return privileges;
	}

	/**
	 * Using boolean value to get byte.
	 * 
	 * @param value
	 * @return
	 */
	private Byte getByteValue(boolean value) {

		if (value) {
			return (byte) 1;
		} else {
			return (byte) 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VendorRolePrivileges> retrieveVendorPrivilegesByRole(
			Integer roleId, UserDetailsDto userDetails) {
		Session session = null;

		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}

		List<VendorRolePrivileges> privileges = null;
		session.beginTransaction();

		String sqlQuery = "SELECT rolePrivileges FROM VendorRolePrivileges rolePrivileges,VendorObjects vmsObject,VendorRoles userRole "
				+ "WHERE rolePrivileges.objectId=vmsObject AND "
				+ "rolePrivileges.vendorUserRoleId=userRole AND "
				+ "rolePrivileges.vendorUserRoleId= :role and vmsObject.isActive=1 ORDER BY vmsObject.objectName ASC";
		try {
			VendorRoles role = (VendorRoles) session.get(VendorRoles.class,
					roleId);
			Query query = session.createQuery(sqlQuery);
			query.setParameter("role", role);
			privileges = query.list();
			session.getTransaction().commit();
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return privileges;
	}
	
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override	
	public List<RolePrivileges> retrievePrivilegesByRoleAndApplication(Integer roleId,Integer applicationcategoryid,UserDetailsDto userDetails)
	{
		String applicationcategoryname = null;
    	if(applicationcategoryid == 1)
    		applicationcategoryname = "v";
    	else if(applicationcategoryid == 2)
    		applicationcategoryname = "a";
    	else if(applicationcategoryid == 3)
    		applicationcategoryname = "r";
    	else if(applicationcategoryid == 4)
    		applicationcategoryname = "d";
    	else if(applicationcategoryid == 5)
    		applicationcategoryname = "p";
    	
		Session session = null;
		
		if (userDetails.getUserType() != null
				&& userDetails.getUserType().equalsIgnoreCase("FG")) {
			session = HibernateUtil.buildSessionFactory().getCurrentSession();
		} else {
			session = HibernateUtilCustomer.buildSessionFactory().openSession(
					DataBaseConnection.getConnection(userDetails));
		}
		List<RolePrivileges> privileges = null;
		session.beginTransaction();

		String sqlQuery = "SELECT rolePrivileges FROM RolePrivileges rolePrivileges,VMSObjects vmsObject,UserRoles userRole "
				+ "WHERE rolePrivileges.objectId=vmsObject AND "
				+ "rolePrivileges.roleId=userRole AND "
				+ "rolePrivileges.roleId= :role and vmsObject.isActive=1  and vmsObject.applicationCategory=  '"+applicationcategoryname+"' "
						+ "ORDER BY vmsObject.objectName ASC";
		try {
			UserRoles role=(UserRoles)session.get(UserRoles.class, roleId);
			Query query = session.createQuery(sqlQuery);
			query.setParameter("role", role);
			privileges = query.list();
			session.getTransaction().commit();
		} catch (Exception exception) {
			/* print the exception in log file. */
			PrintExceptionInLogFile.printException(exception);
			session.getTransaction().rollback();
		} finally {
			if (userDetails.getUserType() != null
					&& !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
				try {
					session.connection().close();
					session.close();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return privileges;
	}
}
