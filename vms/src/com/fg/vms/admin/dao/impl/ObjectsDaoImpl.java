/*
 * ObjectsDaoImpl.java 
 */
package com.fg.vms.admin.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.fg.vms.admin.dao.ObjectsDao;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.admin.pojo.ObjectsForm;
import com.fg.vms.admin.pojo.PrivilegeForm;
import com.fg.vms.util.DataBaseConnection;
import com.fg.vms.util.HibernateUtil;
import com.fg.vms.util.HibernateUtilCustomer;
import com.fg.vms.util.PrintExceptionInLogFile;

/**
 * Represents the Object service implementations.
 * 
 * @author vinoth
 * 
 */
public class ObjectsDaoImpl implements ObjectsDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public String createObject(ObjectsForm configurationForm,
            int currentUserId, UserDetailsDto userDetails) {

        Session session = null;

        if (userDetails.getUserType() != null
                && userDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();
        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));
        }

        String result = "unique";
        try {
            session.beginTransaction();

            /* To check unique objects */
            VMSObjects object = (VMSObjects) session
                    .createCriteria(VMSObjects.class)
                    .add(Restrictions.eq("objectName",
                            configurationForm.getObjectName())).uniqueResult();

            if (object != null) {
                return result;
            }

            VMSObjects configuration = new VMSObjects();

            /* Insert into new and unique objects. */
            configuration.setId(configurationForm.getId());
            configuration.setObjectName(configurationForm.getObjectName());
            configuration.setObjectType(configurationForm.getObjectType());
            configuration.setObjectDescription(configurationForm
                    .getObjectDescription());
            configuration.setIsActive(Byte.valueOf(configurationForm
                    .getIsActive()));
            configuration.setCreatedBy(currentUserId);
            configuration.setCreatedOn(new Date());
            configuration.setModifiedBy(currentUserId);
            configuration.setModifiedOn(new Date());

            session.save(configuration);
            session.getTransaction().commit();
            result = "success";

        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
            result = "failure";
        } finally {
            if (userDetails.getUserType() != null
                    && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
                
            }
        }
        return result;

    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<VMSObjects> listObjects(UserDetailsDto userDetails) {

        Session session = null;

        if (userDetails.getUserType() != null
                && userDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();

        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));
        }
        session.beginTransaction();
        List<VMSObjects> objects = null;
        try {

            /* Get the List of active VMS objects */
            objects = session.createQuery(
                    "from VMSObjects where isActive=1 ORDER BY objectName ASC")
                    .list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (userDetails.getUserType() != null
                    && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
                ;
            }
        }
        return objects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteObject(Integer id, UserDetailsDto userDetails) {

        Session session = null;

        if (userDetails.getUserType() != null
                && userDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();
        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));
        }

        session.beginTransaction();
        String result = "";
        VMSObjects configuration = null;
        try {

            /* Update the selected object. */
            configuration = (VMSObjects) session.get(VMSObjects.class, id);
            configuration.setIsActive((byte) 0);
            session.update(configuration);
            result = "success";
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (userDetails.getUserType() != null
                    && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
                ;
            }
        }
        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VMSObjects retriveObject(Integer id, UserDetailsDto userDetails) {

        Session session;

        if (userDetails.getUserType() != null
                && userDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();
        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));
        }

        session.beginTransaction();
        VMSObjects configuration = null;
        try {

            /* Retrieve the objects based on unique id. */
            configuration = (VMSObjects) session
                    .createCriteria(VMSObjects.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (userDetails.getUserType() != null
                    && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
                ;
            }
        }

        return configuration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateObject(ObjectsForm configurationForm, Integer id,
            Integer currentUserId, UserDetailsDto userDetails) {

        Session session = null;

        if (userDetails.getUserType() != null
                && userDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();
        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));
        }
        session.beginTransaction();
        String result = "";

        VMSObjects configuration = null;
        try {
            configuration = (VMSObjects) session.get(VMSObjects.class, id);

            /* Update the selected object details. */
            configuration.setObjectName(configurationForm.getObjectName());
            configuration.setObjectType(configurationForm.getObjectType());
            configuration.setObjectDescription(configurationForm
                    .getObjectDescription());
            configuration.setIsActive(Byte.valueOf(configurationForm
                    .getIsActive()));
            configuration.setModifiedBy(currentUserId);
            configuration.setModifiedOn(new Date());

            session.update(configuration);
            session.getTransaction().commit();
            result = "success";
        } catch (HibernateException exception) {
            /* print the exception in log file. */
            PrintExceptionInLogFile.printException(exception);
            result = "failure";
            session.getTransaction().rollback();
        } finally {
            if (userDetails.getUserType() != null
                    && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
                ;
            }
        }
        return result;

    }
    
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
	@Override    
    public List<VMSObjects> listObjectsByApplicationCategory(UserDetailsDto userDetails,PrivilegeForm form) {
    	
    	int applicationcategoryid=Integer.parseInt(form.getApplicationcategoryname().toString());
    	String applicationcategoryname=null;
    	
    	if(applicationcategoryid == 1)
    		applicationcategoryname = "v";
    	else if(applicationcategoryid == 2)
    		applicationcategoryname = "a";
    	else if(applicationcategoryid == 3)
    		applicationcategoryname = "r";
    	else if(applicationcategoryid == 4)
    		applicationcategoryname = "d";
    	else if(applicationcategoryid == 5)
    		applicationcategoryname = "p";
    	
        Session session = null;

        if (userDetails.getUserType() != null
                && userDetails.getUserType().equalsIgnoreCase("FG")) {
            session = HibernateUtil.buildSessionFactory().getCurrentSession();

        } else {
            session = HibernateUtilCustomer.buildSessionFactory().openSession(
                    DataBaseConnection.getConnection(userDetails));
        }
        session.beginTransaction();
        List<VMSObjects> objects = null;
        try {
        	//Get the List of active VMS objects 
            objects = session.createQuery("from VMSObjects where isActive=1 and applicationcategory = '" + applicationcategoryname + "' ORDER BY objectName ASC").list();
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            // print the exception in log file. 
            PrintExceptionInLogFile.printException(exception);
            session.getTransaction().rollback();
        } finally {
            if (userDetails.getUserType() != null
                    && !(userDetails.getUserType().equalsIgnoreCase("FG"))) {
                try {
                    session.connection().close();
                    session.close();
                } catch (HibernateException e) {
                    PrintExceptionInLogFile.printException(e);
                } catch (SQLException e) {
                    PrintExceptionInLogFile.printException(e);
                }
                ;
            }
        }
         return objects;
    }
}
