/**
 * CustomerDao.java
 */
package com.fg.vms.admin.dao;

import java.util.List;

import com.fg.vms.admin.dto.CustomerInfoDto;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.pojo.CustomerContactForm;
import com.fg.vms.admin.pojo.CustomerForm;

/**
 * Defines the functionality that should be provided by the Service Layer to FG
 * user and customer for create the customer and modify the customer
 * information.
 * 
 * @author pirabu
 * 
 */
public interface CustomerDao {

	/**
	 * Retrieve all the Secret Questions from the database.
	 * 
	 * @param userDetails
	 *            the user details
	 * @return the list
	 */
	public List<SecretQuestion> displaySecretQuestions(
			UserDetailsDto userDetails);

	/**
	 * This Method used to persist/merge the customer information.
	 * 
	 * @param customerForm
	 *            the customer form
	 * @param userId
	 *            the user id
	 * @param request
	 *            the request
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String saveCustomer(CustomerForm customerForm, Integer userId,
			UserDetailsDto userDetails, String appRoot);

	/**
	 * Retrieve all the active customers.
	 * 
	 * @param userDetails
	 *            the user details
	 * @return the list
	 */
	public List<Customer> displayCustomers(UserDetailsDto userDetails);
	
	/**
	 * Retrieve Only One Customer.
	 * 
	 * @param userDetails
	 * @return
	 */
	public List<Customer> displaySingleCustomer(UserDetailsDto userDetails);

	/**
	 * Retrieve the particular customer from the database.
	 * 
	 * @param customerId
	 *            the customer id
	 * @param userDetails
	 *            the user details
	 * @return the customer info dto
	 */
	public CustomerInfoDto retriveCustomer(Integer customerId,
			UserDetailsDto userDetails);

	/**
	 * Add new contact to customer.
	 * 
	 * @param customer
	 *            the customer
	 * @param contactForm
	 *            the contact form
	 * @param currentUser
	 *            the current user
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String addContact(Customer customer,
			CustomerContactForm contactForm, Integer currentUser,
			UserDetailsDto userDetails);

	/**
	 * Merge the customer contact information.
	 * 
	 * @param customer
	 *            the customer
	 * @param contactForm
	 *            the contact form
	 * @param currentUser
	 *            the current user
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String updateContact(Customer customer,
			CustomerContactForm contactForm, Integer currentUser,
			UserDetailsDto userDetails);

	/**
	 * delete the customer contact.
	 * 
	 * @param contactId
	 *            the contact id
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String deleteContact(Integer contactId, UserDetailsDto userDetails);

	/**
	 * Retrive contact.
	 * 
	 * @param contactId
	 *            the contact id
	 * @param userDetails
	 *            the user details
	 * @return the customer contacts
	 */
	public CustomerContacts retriveContact(Integer contactId,
			UserDetailsDto userDetails);

	/**
	 * Delete particular customer.
	 * 
	 * @param customerId
	 *            the customer id
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String deleteCustomer(Integer customerId, UserDetailsDto userDetails);

	/**
	 * Retrieve the customer information form customer database.
	 * 
	 * @param customerId
	 *            the customer id
	 * @param userDetails
	 *            the user details
	 * @return the customer info dto
	 */
	public CustomerInfoDto retriveCustomerInfoFromCustomerDB(
			UserDetailsDto userDetails);

	/**
	 * Update the customer information.
	 * 
	 * @param customerForm
	 *            the customer form
	 * @param userId
	 *            the user id
	 * @param request
	 *            the request
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String updateCustomerInfoByCustomer(CustomerForm customerForm,
			Integer userId, UserDetailsDto userDetails);

	/**
	 * Update customer.
	 * 
	 * @param customerForm
	 *            the customer form
	 * @param userId
	 *            the user id
	 * @param request
	 *            the request
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String updateCustomer(CustomerForm customerForm, Integer userId,
			UserDetailsDto userDetails);

	/**
	 * Check duplicate company code.
	 * 
	 * @param compCode
	 *            the company code
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String checkDuplicateCustCode(String compCode,
			UserDetailsDto userDetails);

	/**
	 * Check duplicate database name.
	 * 
	 * @param databaseName
	 *            the database name
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String checkDuplicateDatabaseName(String databaseName,
			UserDetailsDto userDetails);

	/**
	 * Check duplicate url.
	 * 
	 * @param url
	 *            the url
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String checkDuplicateURL(String url, UserDetailsDto userDetails);

	/**
	 * Check duplicate Duns Number.
	 * 
	 * @param dunsNo
	 *            the Duns Number
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String checkDuplicateDunsNo(String dunsNo, UserDetailsDto userDetails);

	/**
	 * Check duplicate tax id.
	 * 
	 * @param taxId
	 *            the tax id
	 * @param userDetails
	 *            the user details
	 * @return the string
	 */
	public String checkDuplicateTaxId(String taxId, UserDetailsDto userDetails);

	public CustomerContacts findCustomerContact(UserDetailsDto userDetails);
}
