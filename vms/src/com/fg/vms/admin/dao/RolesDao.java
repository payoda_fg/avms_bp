/**
 * 
 */
package com.fg.vms.admin.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.pojo.UserRolesForm;

/**
 * Maintain user roles.
 * 
 * @author vinoth karthi
 */

public interface RolesDao {

    /**
     * Retrieve the list of user roles.
     * 
     * @param userdetails
     *            - Its hold the current user application details.
     * @return the list
     */
    public List<UserRolesForm> viewRoles(UserDetailsDto userdetails);

    /**
     * Delete the selected role.
     * 
     * @param id
     *            the id
     * @param userdetails
     *            -Its hold the current user application details.
     * @return the string
     */
    public String deleteRole(Integer id, UserDetailsDto userdetails);

    /**
     * Insert a new Role.
     * 
     * @param userRolesForm
     *            the user roles form
     * @param currentUser
     *            the current user
     * @param userdetails
     *            - Its hold the current user application details.
     * @return the string
     */
    public String addRole(UserRolesForm userRolesForm, Integer currentUser,
	    UserDetailsDto userdetails);

    /**
     * Modify the existing role.
     * 
     * @param userRolesForm
     *            the user roles form
     * @param currentUserId
     *            the current user id
     * @param userdetails
     *            - Its hold the current user application details.
     * @return the string
     */
    public String updateRole(UserRolesForm userRolesForm,
	    Integer currentUserId, UserDetailsDto userdetails);

    /**
     * Retrieve the selected role.
     * 
     * @param id
     *            the id
     * @param userdetails
     *            - Its hold the current user application details.
     * @return the user roles
     */
    public Object retriveRole(Integer id, UserDetailsDto userdetails);

    /**
     * Check duplicate role.
     * 
     * @param roleName
     *            the role name
     * @param userdetails
     *            - Its hold the current user application details.
     * @return the string
     */
    public String checkDuplicateRole(String roleName, UserDetailsDto userdetails);
}
