/*
 * ObjectsDao.java 
 */
package com.fg.vms.admin.dao;

import java.util.List;

import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.admin.pojo.ObjectsForm;
import com.fg.vms.admin.pojo.PrivilegeForm;

/**
 * Defines the functionality that should be provided by the Service Layer to FG
 * user and customer for create and modify the object information.
 * 
 * @author vinoth
 * 
 */
public interface ObjectsDao {

    /**
     * Method to create new objects and activate them.
     * 
     * @param configurationForm
     * @param currentUserId
     * @param userDetails
     *            - Its hold the current user application details.
     * @return
     */
    public String createObject(ObjectsForm configurationForm,
	    int currentUserId, UserDetailsDto userDetails);

    /**
     * Method used to view/retrieve list of objects from database.
     * 
     * @param userDetails
     *            Its hold the current user application details.
     * @return objects
     */
    public List<VMSObjects> listObjects(UserDetailsDto userDetails);

    /**
     * Method to remove/delete the selected object from database.
     * 
     * @param id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String deleteObject(Integer id, UserDetailsDto userDetails);

    /**
     * Method to view/retrieve the selected object based on object id.
     * 
     * @param id
     *            - The id to retrieve object.
     * @param userDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public VMSObjects retriveObject(Integer id, UserDetailsDto userDetails);

    /**
     * Method to modify/update object details.
     * 
     * @param configurationForm
     * @param id
     *            - The id to merge.
     * @param currentUserId
     *            - The current user id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return result
     */
    public String updateObject(ObjectsForm configurationForm, Integer id,
	    Integer currentUserId, UserDetailsDto userDetails);
    
    /**
     * @param userDetails
     * @param form
     * @return
     */
    public List<VMSObjects> listObjectsByApplicationCategory(UserDetailsDto userDetails,PrivilegeForm form);

}
