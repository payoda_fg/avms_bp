package com.fg.vms.admin.dao;

import java.util.List;

import com.fg.vms.admin.dto.RolesAndObjects;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.customer.model.VendorRolePrivileges;

/**
 * Defines the functionality that should be provided by the Service Layer to
 * admin to persist or merge and view privileges by role.
 * 
 * @author pirabu
 * 
 */
public interface RolePrvilegesDao {

    /**
     * Persist or merge privileges by role.
     * 
     * @param rolesAndObjects
     *            the roles and objects
     * @param roleName
     *            the role name
     * @param id
     *            the id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return the string
     */
    public String persistPrivilegesByRole(RolesAndObjects rolesAndObjects,
	    String roleName, Integer id, UserDetailsDto userDetails);

    /**
     * Retrieve privileges by role.
     * 
     * @param roleId
     *            the role id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return the list
     */
    public List<RolePrivileges> retrievePrivilegesByRole(Integer roleId,
	    UserDetailsDto userDetails);

    /**
     * Retrieve vendor privileges by role.
     * 
     * @param roleId
     *            the role id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return the list
     */
    public List<VendorRolePrivileges> retrieveVendorPrivilegesByRole(
	    Integer roleId, UserDetailsDto userDetails);
    
    /**
     * Retrieve privileges by Role and ApplicationCategory.
     * 
     * @param roleId
     *            the role id
     * @param userDetails
     *            - Its hold the current user application details.
     * @return the list
     */
    public List<RolePrivileges> retrievePrivilegesByRoleAndApplication(Integer roleId,Integer applicationId,UserDetailsDto userDetails);

}
