/**
 * UserDetailsDto.java
 */
package com.fg.vms.admin.dto;

import java.io.Serializable;

import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerApplicationSettings;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.admin.model.CustomerSLA;
import com.fg.vms.admin.model.Users;
import com.fg.vms.customer.model.WorkflowConfiguration;

/**
 * Represents the user details such as db name, application settings etc.
 * 
 * @author pirabu
 * 
 */
public class UserDetailsDto implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /** The db name. */
    private String dbName;

    /** The user. */
    private Users user;

    /** The db username. */
    private String dbUsername;

    /** The db password. */
    private String dbPassword;

    private String databaseIp;

    /** The current customer. */
    private CustomerContacts currentCustomer;

    private Customer customer;
    
    private Integer customerDivisionID;
    
    private Integer isGlobalDivision;
    
    private String [] customerDivisionIds;
    
    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param customer
     *            the customer to set
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getCustomerDivisionID() {
		return customerDivisionID;
	}

	public void setCustomerDivisionID(Integer customerDivisionID) {
		this.customerDivisionID = customerDivisionID;
	}

	/** The application settings. */
    private com.fg.vms.admin.model.CustomerApplicationSettings applicationSettings;

    private CustomerSLA customerSLA;

    /** The hibernate cfg file name. */
    private String hibernateCfgFileName;

    /** The current user is FG user or Customer or vendor user. */
    private String userType;

    /** The settings. */
    private com.fg.vms.customer.model.CustomerApplicationSettings settings;
    
    private WorkflowConfiguration workflowConfiguration;

    /**
     * Gets the db name.
     * 
     * @return the dbName
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Sets the db name.
     * 
     * @param dbName
     *            the dbName to set
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * @return the databaseIp
     */
    public String getDatabaseIp() {
        return databaseIp;
    }

    /**
     * @param databaseIp
     *            the databaseIp to set
     */
    public void setDatabaseIp(String databaseIp) {
        this.databaseIp = databaseIp;
    }

    /**
     * Gets the user.
     * 
     * @return the user
     */
    public Users getUser() {
        return user;
    }

    /**
     * Sets the user.
     * 
     * @param user
     *            the user to set
     */
    public void setUser(Users user) {
        this.user = user;
    }

    /**
     * Gets the hibernate cfg file name.
     * 
     * @return the hibernateCfgFileName
     */
    public String getHibernateCfgFileName() {
        return hibernateCfgFileName;
    }

    /**
     * Sets the hibernate cfg file name.
     * 
     * @param hibernateCfgFileName
     *            the hibernateCfgFileName to set
     */
    public void setHibernateCfgFileName(String hibernateCfgFileName) {
        this.hibernateCfgFileName = hibernateCfgFileName;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType
     *            the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * Gets the application settings.
     * 
     * @return the applicationSettings
     */
    public CustomerApplicationSettings getApplicationSettings() {
        return applicationSettings;
    }

    /**
     * Sets the application settings.
     * 
     * @param applicationSettings
     *            the applicationSettings to set
     */
    public void setApplicationSettings(
            CustomerApplicationSettings applicationSettings) {
        this.applicationSettings = applicationSettings;
    }

    /**
     * @return the customerSLA
     */
    public CustomerSLA getCustomerSLA() {
        return customerSLA;
    }

    /**
     * @param customerSLA
     *            the customerSLA to set
     */
    public void setCustomerSLA(CustomerSLA customerSLA) {
        this.customerSLA = customerSLA;
    }

    /**
     * Gets the settings.
     * 
     * @return the settings
     */
    public com.fg.vms.customer.model.CustomerApplicationSettings getSettings() {
        return settings;
    }

    /**
     * Sets the settings.
     * 
     * @param settings
     *            the new settings
     */
    public void setSettings(
            com.fg.vms.customer.model.CustomerApplicationSettings settings) {
        this.settings = settings;
    }

    /**
     * Gets the db username.
     * 
     * @return the dbUsername
     */
    public String getDbUsername() {
        return dbUsername;
    }

    /**
     * Sets the db username.
     * 
     * @param dbUsername
     *            the dbUsername to set
     */
    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    /**
     * Gets the db password.
     * 
     * @return the dbPassword
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * Sets the db password.
     * 
     * @param dbPassword
     *            the dbPassword to set
     */
    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    /**
     * Gets the current customer.
     * 
     * @return the currentCustomer
     */
    public CustomerContacts getCurrentCustomer() {
        return currentCustomer;
    }

    /**
     * Sets the current customer.
     * 
     * @param currentCustomer
     *            the currentCustomer to set
     */
    public void setCurrentCustomer(CustomerContacts currentCustomer) {
        this.currentCustomer = currentCustomer;
    }

	/**
	 * @return the workflowConfiguration
	 */
	public WorkflowConfiguration getWorkflowConfiguration() {
		return workflowConfiguration;
	}

	/**
	 * @param workflowConfiguration the workflowConfiguration to set
	 */
	public void setWorkflowConfiguration(WorkflowConfiguration workflowConfiguration) {
		this.workflowConfiguration = workflowConfiguration;
	}

	/**
	 * @return the isGlobalDivision
	 */
	public Integer getIsGlobalDivision() {
		return isGlobalDivision;
	}

	/**
	 * @param isGlobalDivision the isGlobalDivision to set
	 */
	public void setIsGlobalDivision(Integer isGlobalDivision) {
		this.isGlobalDivision = isGlobalDivision;
	}

	/**
	 * @return the customerDivisionIds
	 */
	public String[] getCustomerDivisionIds() {
		return customerDivisionIds;
	}

	/**
	 * @param customerDivisionIds the customerDivisionIds to set
	 */
	public void setCustomerDivisionIds(String[] customerDivisionIds) {
		this.customerDivisionIds = customerDivisionIds;
	}
	
	
}
