/*
 * RolesAndObjects.jav
 */
package com.fg.vms.admin.dto;

import java.io.Serializable;

/**
 * This class represents assign the Objects privileges to role.
 * 
 * @author pirabu
 * 
 */
public class RolesAndObjects implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
    private Integer id;

    /** The object name. */
    private String objectName;

    /** The add. */
    private boolean add;

    /** The delete. */
    private boolean delete;

    /** The modify. */
    private boolean modify;

    /** The view. */
    private boolean view;

    /** The enable. */
    private boolean enable;

    /** The visible. */
    private boolean visible;
    
    /** The object Description.  */
    private String objectDesc;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the object name.
     * 
     * @return the objectName
     */
    public String getObjectName() {
	return objectName;
    }

    /**
     * Sets the object name.
     * 
     * @param objectName
     *            the objectName to set
     */
    public void setObjectName(String objectName) {
	this.objectName = objectName;
    }

    /**
     * Checks if is adds the.
     * 
     * @return the add
     */
    public boolean isAdd() {
	return add;
    }

    /**
     * Sets the adds the.
     * 
     * @param add
     *            the add to set
     */
    public void setAdd(boolean add) {
	this.add = add;
    }

    /**
     * Checks if is delete.
     * 
     * @return the delete
     */
    public boolean isDelete() {
	return delete;
    }

    /**
     * Sets the delete.
     * 
     * @param delete
     *            the delete to set
     */
    public void setDelete(boolean delete) {
	this.delete = delete;
    }

    /**
     * Checks if is modify.
     * 
     * @return the modify
     */
    public boolean isModify() {
	return modify;
    }

    /**
     * Sets the modify.
     * 
     * @param modify
     *            the modify to set
     */
    public void setModify(boolean modify) {
	this.modify = modify;
    }

    /**
     * Checks if is view.
     * 
     * @return the view
     */
    public boolean isView() {
	return view;
    }

    /**
     * Sets the view.
     * 
     * @param view
     *            the view to set
     */
    public void setView(boolean view) {
	this.view = view;
    }

    /**
     * Checks if is enable.
     * 
     * @return the enable
     */
    public boolean isEnable() {
	return enable;
    }

    /**
     * Sets the enable.
     * 
     * @param enable
     *            the enable to set
     */
    public void setEnable(boolean enable) {
	this.enable = enable;
    }

    /**
     * Checks if is visible.
     * 
     * @return the visible
     */
    public boolean isVisible() {
	return visible;
    }

    /**
     * Sets the visible.
     * 
     * @param visible
     *            the visible to set
     */
    public void setVisible(boolean visible) {
	this.visible = visible;
    }

	/**
	 * @return the objectDesc
	 */
	public String getObjectDesc() {
		return objectDesc;
	}

	/**
	 * @param objectDesc the objectDesc to set
	 */
	public void setObjectDesc(String objectDesc) {
		this.objectDesc = objectDesc;
	}

}
