/*
 * SpendDataDTO.java
 */
package com.fg.vms.admin.dto;

/**
 * Its holds the uploaded spend data details.
 * 
 * @author anand
 * 
 */
public class SpendDataDTO {

    /** The category. */
    private String category;

    /** The spen date. */
    private String spenDate;

    /** The naics code. */
    private String naicsCode;

    /** The spent value. */
    private String spentValue;

    /** The currency. */
    private String currency;

    /** The diverse cerificate code. */
    private String diverseCerificateCode;

    /** The is valid. */
    private Byte isValid;

    /**
     * Gets the category.
     * 
     * @return the category
     */
    public String getCategory() {
	return category;
    }

    /**
     * Sets the category.
     * 
     * @param category
     *            the category to set
     */
    public void setCategory(String category) {
	this.category = category;
    }

    /**
     * Gets the spen date.
     * 
     * @return the spenDate
     */
    public String getSpenDate() {
	return spenDate;
    }

    /**
     * Sets the spen date.
     * 
     * @param spenDate
     *            the spenDate to set
     */
    public void setSpenDate(String spenDate) {
	this.spenDate = spenDate;
    }

    /**
     * Gets the naics code.
     * 
     * @return the naicsCode
     */
    public String getNaicsCode() {
	return naicsCode;
    }

    /**
     * Sets the naics code.
     * 
     * @param naicsCode
     *            the naicsCode to set
     */
    public void setNaicsCode(String naicsCode) {
	this.naicsCode = naicsCode;
    }

    /**
     * Gets the spent value.
     * 
     * @return the spentValue
     */
    public String getSpentValue() {
	return spentValue;
    }

    /**
     * Gets the checks if is valid.
     * 
     * @return the isValid
     */
    public Byte getIsValid() {
	return isValid;
    }

    /**
     * Sets the checks if is valid.
     * 
     * @param isValid
     *            the isValid to set
     */
    public void setIsValid(Byte isValid) {
	this.isValid = isValid;
    }

    /**
     * Sets the spent value.
     * 
     * @param spentValue
     *            the spentValue to set
     */
    public void setSpentValue(String spentValue) {
	this.spentValue = spentValue;
    }

    /**
     * Gets the currency.
     * 
     * @return the currency
     */
    public String getCurrency() {
	return currency;
    }

    /**
     * Sets the currency.
     * 
     * @param currency
     *            the currency to set
     */
    public void setCurrency(String currency) {
	this.currency = currency;
    }

    /**
     * Gets the diverse cerificate code.
     * 
     * @return the diverseCerificateCode
     */
    public String getDiverseCerificateCode() {
	return diverseCerificateCode;
    }

    /**
     * Sets the diverse cerificate code.
     * 
     * @param diverseCerificateCode
     *            the diverseCerificateCode to set
     */
    public void setDiverseCerificateCode(String diverseCerificateCode) {
	this.diverseCerificateCode = diverseCerificateCode;
    }

}
