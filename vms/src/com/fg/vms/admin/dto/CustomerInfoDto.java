/*
 * CustomerInfoDto.java
 */
package com.fg.vms.admin.dto;

import java.util.List;

import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerApplicationSettings;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.admin.model.CustomerSLA;

/**
 * Holds the customer information such as customer contact details, company
 * details, application settings and SLA details..
 * 
 * @author pirabu
 * 
 */
public class CustomerInfoDto {

    /** The customer. */
    private Customer customer;
    
    /** The contacts. */
    private List<CustomerContacts> contacts;

    /** The customer sla. */
    private CustomerSLA customerSLA;
    
    /** The application settings. */
    private CustomerApplicationSettings applicationSettings;

    /** The customer application settings. */
    private com.fg.vms.customer.model.CustomerApplicationSettings settings;

    /**
     * Gets the customer.
     *
     * @return the customer
     */
    public Customer getCustomer() {
	return customer;
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer to set
     */
    public void setCustomer(Customer customer) {
	this.customer = customer;
    }

    /**
     * Gets the contacts.
     *
     * @return the contacts
     */
    public List<CustomerContacts> getContacts() {
	return contacts;
    }

    /**
     * Sets the contacts.
     *
     * @param contacts the contacts to set
     */
    public void setContacts(List<CustomerContacts> contacts) {
	this.contacts = contacts;
    }

    /**
     * Gets the customer sla.
     *
     * @return the customerSLA
     */
    public CustomerSLA getCustomerSLA() {
	return customerSLA;
    }

    /**
     * Sets the customer sla.
     *
     * @param customerSLA the customerSLA to set
     */
    public void setCustomerSLA(CustomerSLA customerSLA) {
	this.customerSLA = customerSLA;
    }

    /**
     * Gets the application settings.
     *
     * @return the applicationSettings
     */
    public CustomerApplicationSettings getApplicationSettings() {
	return applicationSettings;
    }

    /**
     * Sets the application settings.
     *
     * @param applicationSettings the applicationSettings to set
     */
    public void setApplicationSettings(
	    CustomerApplicationSettings applicationSettings) {
	this.applicationSettings = applicationSettings;
    }

    /**
     * Gets the settings.
     *
     * @return the settings
     */
    public com.fg.vms.customer.model.CustomerApplicationSettings getSettings() {
	return settings;
    }

    /**
     * Sets the settings.
     *
     * @param settings the settings to set
     */
    public void setSettings(
	    com.fg.vms.customer.model.CustomerApplicationSettings settings) {
	this.settings = settings;
    }

}
