/*
 * UserRolesForm.java
 */
package com.fg.vms.admin.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * The Class UserRolesForm.
 */
public class UserRolesForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The role name. */
	private String roleName;

	private String hiddenRoleName;

	private String roleDesc;

	/** The is active. */
	private String isActive;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the checks if is active.
	 * 
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 * 
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the role name.
	 * 
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Sets the role name.
	 * 
	 * @param roleName
	 *            the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the hiddenRoleName
	 */
	public String getHiddenRoleName() {
		return hiddenRoleName;
	}

	/**
	 * @param hiddenRoleName
	 *            the hiddenRoleName to set
	 */
	public void setHiddenRoleName(String hiddenRoleName) {
		this.hiddenRoleName = hiddenRoleName;
	}

	/**
	 * @return the roleDesc
	 */
	public String getRoleDesc() {
		return roleDesc;
	}

	/**
	 * @param roleDesc
	 *            the roleDesc to set
	 */
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.id = null;
		this.roleName = "";
		this.hiddenRoleName = "";
		this.roleDesc = "";
		super.reset(mapping, request);
	}
}
