/**
 * LoginForm.java
 */
package com.fg.vms.admin.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

/**
 * The Class LoginForm.
 * 
 * @author pirabu
 */
public class LoginForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The user login. */
	//private String userLogin;

	/** The user password. */
	private String userPassword;

	/** The user email id. */
	private String userEmailId;

	private String securityQuestion;

	private String securityAnswer;

	private String oldPassword;

	private String newPassword;

	private String confirmPassword;
	
	private Integer customerDivisionId;

	/**
	 * Gets the user email id.
	 * 
	 * @return the userEmailId
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * Sets the user email id.
	 * 
	 * @param userEmailId
	 *            the userEmailId to set
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	

	/**
	 * Gets the user password.
	 * 
	 * @return the userPassword
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Sets the user password.
	 * 
	 * @param userPassword
	 *            the userPassword to set
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getSecurityQuestion() {
		return securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	public String getSecurityAnswer() {
		return securityAnswer;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	

	public Integer getCustomerDivisionId() {
		return customerDivisionId;
	}

	public void setCustomerDivisionId(Integer customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.userEmailId = "";
		this.userPassword = "";
		this.confirmPassword = "";
		this.newPassword = "";
		this.oldPassword = "";
		this.securityAnswer = "";
		this.securityQuestion = "";

	}

}