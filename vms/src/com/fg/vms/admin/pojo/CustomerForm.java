/**
 * CustomerForm.java
 */
package com.fg.vms.admin.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

/**
 * Represents the details (eg..custName, custCode, phone, emailId etc.) of
 * customer and contacts of customer for interact with UI.
 * 
 * @author pirabu
 * 
 */
/**
 * @author shefeek
 * 
 */
public class CustomerForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The database name. */
	private String databaseName;

	/** The application URL */
	private String applicationUrl;

	/** The user name. */
	private String userName;

	/** The dbpassword. */
	private String dbpassword;

	/** The database ip. */
	private String databaseIp;

	/** The id. */
	private Integer id;

	/** The contact id. */
	private Integer contactId;

	/** The company logo. */
	private FormFile companyLogo;

	/** The logo image path. */
	private String logoImagePath;

	/** The default set. */
	private boolean defaultSet;

	/** The background. */
	private String background;

	/** The text color. */
	private String textColor;

	/** The heading. */
	private String heading;

	/** The roll over. */
	private String rollOver;

	/** The menu bar. */
	private String menuBar;

	/** The menu button. */
	private String menuButton;

	/** The companyname. */
	private String companyname;

	/** The companycode. */
	private String companycode;

	private String hiddenCustCode;

	/** The dunsnum. */
	private String dunsnum;

	/** The taxid. */
	private String taxid;

	/** The address1. */
	private String address1;

	/** The address2. */
	private String address2;

	/** The address3. */
	private String address3;

	/** The city. */
	private String city;

	/** The state. */
	private String state;

	/** The province. */
	private String province;

	/** The zipcode. */
	private String zipcode;

	/** The region. */
	private String region;

	/** The country. */
	private String country;

	/** The phone. */
	private String phone;

	/** The mobile. */
	private String mobile;

	/** The fax. */
	private String fax;

	/** The email. */
	private String email;

	/** The web site. */
	private String webSite;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The designation. */
	private String designation;

	/** The contact phone. */
	private String contactPhone;

	/** The contact mobile. */
	private String contactMobile;

	/** The contact fax. */
	private String contactFax;

	/** The contanct email. */
	private String contanctEmail;

	private String hiddenEmailId;

	/** The login allowed. */
	private boolean loginAllowed;

	/** The login display name. */
	private String loginDisplayName;

	/** The login id. */
	// private String loginId;

	// This is hidden field on update customer information page;
	/** The hidden login id. */
	private String hiddenLoginId;

	/** The loginpassword. */
	private String loginpassword;

	/** The confirm password. */
	private String confirmPassword;

	/** The primary contact. */
	private boolean primaryContact;

	/** The user sec qn. */
	private int userSecQn;

	/** The user sec qn ans. */
	private String userSecQnAns;

	/** The sla start date. */
	private String slaStartDate;

	/** The sla end date. */
	private String slaEndDate;

	/** The licenced users. */
	private int licencedUsers;

	/** The licenced vendors. */
	private int licencedVendors;

	/** The fiscal start date. */
	private String fiscalStartDate;

	/** The fiscal end date. */
	private String fiscalEndDate;

	/** The spend data upload frequency. */
	private String spendDataUploadFrequency;

	/** The hidden duns number. */
	private String hiddenDunsNumber;

	/** The hidden tax id. */
	private String hiddenTaxId;

	private String menuColor;

	private String applicationServer;

	/** The is division status */
	private Byte isDivision;

	/** The Session Timeout. */
	private int sessionTimeout;
	
	/** The Terms & Condition. */
	private String termsCondition;
	
	/** The Terms & Condition. */
	private String privacyTerms;
	
	private String primeTrainingUrl;
	
	private String nonPrimeTrainingUrl;

	public String getApplicationServer() {
		return applicationServer;
	}

	public void setApplicationServer(String applicationServer) {
		this.applicationServer = applicationServer;
	}

	/**
	 * @return the menuColor
	 */
	public String getMenuColor() {
		return menuColor;
	}

	/**
	 * @param menuColor
	 *            the menuColor to set
	 */
	public void setMenuColor(String menuColor) {
		this.menuColor = menuColor;
	}

	/**
	 * Gets the database name.
	 * 
	 * @return the databaseName
	 */
	public String getDatabaseName() {
		return databaseName;
	}

	/**
	 * Sets the database name.
	 * 
	 * @param databaseName
	 *            the databaseName to set
	 */
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	/**
	 * Gets the applicationUrl.
	 * 
	 * @return the applicationUrl
	 */
	public String getApplicationUrl() {
		return applicationUrl;
	}

	/**
	 * Sets the applicationUrl.
	 * 
	 * @param applicationUrl
	 *            the applicationUrl to set
	 */
	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the dbpassword.
	 * 
	 * @return the dbpassword
	 */
	public String getDbpassword() {
		return dbpassword;
	}

	/**
	 * Sets the dbpassword.
	 * 
	 * @param dbpassword
	 *            the dbpassword to set
	 */
	public void setDbpassword(String dbpassword) {
		this.dbpassword = dbpassword;
	}

	/**
	 * Gets the database ip.
	 * 
	 * @return the databaseIp
	 */
	public String getDatabaseIp() {
		return databaseIp;
	}

	/**
	 * Sets the database ip.
	 * 
	 * @param databaseIp
	 *            the databaseIp to set
	 */
	public void setDatabaseIp(String databaseIp) {
		this.databaseIp = databaseIp;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the contact id.
	 * 
	 * @return the contactId
	 */
	public Integer getContactId() {
		return contactId;
	}

	/**
	 * Sets the contact id.
	 * 
	 * @param contactId
	 *            the contactId to set
	 */
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	/**
	 * Gets the companyname.
	 * 
	 * @return the companyname
	 */
	public String getCompanyname() {
		return companyname;
	}

	/**
	 * Sets the companyname.
	 * 
	 * @param companyname
	 *            set to companyname property
	 */
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	/**
	 * Gets the dunsnum.
	 * 
	 * @return the dunsnumber
	 */
	public String getDunsnum() {
		return dunsnum;
	}

	/**
	 * Sets the dunsnum.
	 * 
	 * @param dunsnum
	 *            set to dunsnum property
	 */
	public void setDunsnum(String dunsnum) {
		this.dunsnum = dunsnum;
	}

	/**
	 * Gets the taxid.
	 * 
	 * @return the taxid
	 */
	public String getTaxid() {
		return taxid;
	}

	/**
	 * Sets the taxid.
	 * 
	 * @param taxid
	 *            set to taxid property
	 */
	public void setTaxid(String taxid) {
		this.taxid = taxid;
	}

	/**
	 * Gets the address1.
	 * 
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * Sets the address1.
	 * 
	 * @param address1
	 *            set to address1 property
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * Gets the address2.
	 * 
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * Sets the address2.
	 * 
	 * @param address2
	 *            set to address2 property
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * Gets the address3.
	 * 
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * Sets the address3.
	 * 
	 * @param address3
	 *            set to address3 property
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            set to city property
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the state.
	 * 
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 * 
	 * @param state
	 *            set to State property
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the province.
	 * 
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 * 
	 * @param province
	 *            set to province property
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * Gets the region.
	 * 
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Sets the region.
	 * 
	 * @param region
	 *            set to region property
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            set to country property
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the phone.
	 * 
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 * 
	 * @param phone
	 *            set to phone property
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the zipcode.
	 * 
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Sets the zipcode.
	 * 
	 * @param zipcode
	 *            set to zipcode property
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * Gets the fax.
	 * 
	 * @return the fax number
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax.
	 * 
	 * @param fax
	 *            set to fax number property
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the web site.
	 * 
	 * @return the webSite
	 */
	public String getWebSite() {
		return webSite;
	}

	/**
	 * Sets the web site.
	 * 
	 * @param webSite
	 *            set to webSite property
	 */
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the customer firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 * 
	 * @param firstName
	 *            set to firstName property
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the customer lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 * 
	 * @param lastName
	 *            set to lastName to lastName property
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the designation.
	 * 
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * Sets the designation.
	 * 
	 * @param designation
	 *            the designation to set
	 */

	public void setDesignation(String designation) {
		this.designation = designation;

	}

	/**
	 * Gets the contact phone.
	 * 
	 * @return the customer contact phone number
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * Sets the contact phone.
	 * 
	 * @param contactPhone
	 *            set to contactPhone property
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * Gets the contact fax.
	 * 
	 * @return the customer contact fax number
	 */
	public String getContactFax() {
		return contactFax;
	}

	/**
	 * Sets the contact fax.
	 * 
	 * @param contactFax
	 *            set to contactFax property
	 */
	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	/**
	 * Gets the contanct email.
	 * 
	 * @return the customer contact email id
	 */
	public String getContanctEmail() {
		return contanctEmail;
	}

	/**
	 * Sets the contanct email.
	 * 
	 * @param contanctEmail
	 *            set to contactEmail property
	 */
	public void setContanctEmail(String contanctEmail) {
		this.contanctEmail = contanctEmail;
	}

	/**
	 * Gets the companycode.
	 * 
	 * @return the company code
	 */
	public String getCompanycode() {
		return companycode;
	}

	/**
	 * Sets the companycode.
	 * 
	 * @param companycode
	 *            set to companycode property
	 */
	public void setCompanycode(String companycode) {
		this.companycode = companycode;
	}

	/**
	 * Gets the mobile.
	 * 
	 * @return the company Mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Sets the mobile.
	 * 
	 * @param mobile
	 *            set to mobile property
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the company email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            set to email property
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the contact mobile.
	 * 
	 * @return the customer contact mobile
	 */
	public String getContactMobile() {
		return contactMobile;
	}

	/**
	 * Sets the contact mobile.
	 * 
	 * @param contactMobile
	 *            set to contact mobile property
	 */
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	/**
	 * Gets the background.
	 * 
	 * @return the background
	 */
	public String getBackground() {
		return background;
	}

	/**
	 * Sets the background.
	 * 
	 * @param background
	 *            the background to set
	 */
	public void setBackground(String background) {
		this.background = background;
	}

	/**
	 * Gets the text color.
	 * 
	 * @return the textColor
	 */
	public String getTextColor() {
		return textColor;
	}

	/**
	 * Sets the text color.
	 * 
	 * @param textColor
	 *            the textColor to set
	 */
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	/**
	 * Gets the heading.
	 * 
	 * @return the heading
	 */
	public String getHeading() {
		return heading;
	}

	/**
	 * Sets the heading.
	 * 
	 * @param heading
	 *            the new heading
	 */
	public void setHeading(String heading) {
		this.heading = heading;
	}

	/**
	 * Gets the roll over.
	 * 
	 * @return the roll over
	 */
	public String getRollOver() {
		return rollOver;
	}

	/**
	 * Sets the roll over.
	 * 
	 * @param rollOver
	 *            the new roll over
	 */
	public void setRollOver(String rollOver) {
		this.rollOver = rollOver;
	}

	/**
	 * Gets the menu bar.
	 * 
	 * @return the menu bar
	 */
	public String getMenuBar() {
		return menuBar;
	}

	/**
	 * Sets the menu bar.
	 * 
	 * @param menuBar
	 *            the new menu bar
	 */
	public void setMenuBar(String menuBar) {
		this.menuBar = menuBar;
	}

	/**
	 * Gets the menu button.
	 * 
	 * @return the menu button
	 */
	public String getMenuButton() {
		return menuButton;
	}

	/**
	 * Sets the menu button.
	 * 
	 * @param menuButton
	 *            the new menu button
	 */
	public void setMenuButton(String menuButton) {
		this.menuButton = menuButton;
	}

	/**
	 * Gets the default set.
	 * 
	 * @return the default set
	 */
	public boolean getDefaultSet() {
		return defaultSet;
	}

	/**
	 * Sets the default set.
	 * 
	 * @param defaultSet
	 *            the new default set
	 */
	public void setDefaultSet(boolean defaultSet) {
		this.defaultSet = defaultSet;
	}

	/**
	 * Gets the company logo.
	 * 
	 * @return the company logo
	 */
	public FormFile getCompanyLogo() {
		return companyLogo;
	}

	/**
	 * Sets the company logo.
	 * 
	 * @param companyLogo
	 *            the new company logo
	 */
	public void setCompanyLogo(FormFile companyLogo) {
		this.companyLogo = companyLogo;
	}

	/**
	 * Gets the user sec qn.
	 * 
	 * @return the user sec qn
	 */
	public int getUserSecQn() {
		return userSecQn;
	}

	/**
	 * Sets the user sec qn.
	 * 
	 * @param userSecQn
	 *            the new user sec qn
	 */
	public void setUserSecQn(int userSecQn) {
		this.userSecQn = userSecQn;
	}

	/**
	 * Gets the user sec qn ans.
	 * 
	 * @return the user sec qn ans
	 */
	public String getUserSecQnAns() {
		return userSecQnAns;
	}

	/**
	 * Sets the user sec qn ans.
	 * 
	 * @param userSecQnAns
	 *            the new user sec qn ans
	 */
	public void setUserSecQnAns(String userSecQnAns) {
		this.userSecQnAns = userSecQnAns;
	}

	/**
	 * Gets the sla start date.
	 * 
	 * @return the sla start date
	 */
	public String getSlaStartDate() {
		return slaStartDate;
	}

	/**
	 * Sets the sla start date.
	 * 
	 * @param slaStartDate
	 *            the new sla start date
	 */
	public void setSlaStartDate(String slaStartDate) {
		this.slaStartDate = slaStartDate;
	}

	/**
	 * Gets the sla end date.
	 * 
	 * @return the sla end date
	 */
	public String getSlaEndDate() {
		return slaEndDate;
	}

	/**
	 * Sets the sla end date.
	 * 
	 * @param slaEndDate
	 *            the new sla end date
	 */
	public void setSlaEndDate(String slaEndDate) {
		this.slaEndDate = slaEndDate;
	}

	/**
	 * Gets the licenced users.
	 * 
	 * @return the licenced users
	 */
	public int getLicencedUsers() {
		return licencedUsers;
	}

	/**
	 * Sets the licenced users.
	 * 
	 * @param licencedUsers
	 *            the new licenced users
	 */
	public void setLicencedUsers(int licencedUsers) {
		this.licencedUsers = licencedUsers;
	}

	/**
	 * Gets the serialversionuid.
	 * 
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the login allowed.
	 * 
	 * @return the login allowed
	 */
	public boolean getLoginAllowed() {
		return loginAllowed;
	}

	/**
	 * Sets the login allowed.
	 * 
	 * @param loginAllowed
	 *            the new login allowed
	 */
	public void setLoginAllowed(boolean loginAllowed) {
		this.loginAllowed = loginAllowed;
	}

	/**
	 * Gets the login display name.
	 * 
	 * @return the login display name
	 */
	public String getLoginDisplayName() {
		return loginDisplayName;
	}

	/**
	 * Sets the login display name.
	 * 
	 * @param loginDisplayName
	 *            the new login display name
	 */
	public void setLoginDisplayName(String loginDisplayName) {
		this.loginDisplayName = loginDisplayName;
	}

	/**
	 * Gets the login id.
	 * 
	 * @return the login id
	 */
	// public String getLoginId() {
	// return loginId;
	// }
	//
	// /**
	// * Sets the login id.
	// *
	// * @param loginId
	// * the new login id
	// */
	// public void setLoginId(String loginId) {
	// this.loginId = loginId;
	// }

	/**
	 * Gets the hidden login id.
	 * 
	 * @return the hiddenLoginId
	 */
	public String getHiddenLoginId() {
		return hiddenLoginId;
	}

	/**
	 * Sets the hidden login id.
	 * 
	 * @param hiddenLoginId
	 *            the hiddenLoginId to set
	 */
	public void setHiddenLoginId(String hiddenLoginId) {
		this.hiddenLoginId = hiddenLoginId;
	}

	/**
	 * Gets the loginpassword.
	 * 
	 * @return the loginpassword
	 */
	public String getLoginpassword() {
		return loginpassword;
	}

	/**
	 * Sets the loginpassword.
	 * 
	 * @param loginpassword
	 *            the new loginpassword
	 */
	public void setLoginpassword(String loginpassword) {
		this.loginpassword = loginpassword;
	}

	/**
	 * Gets the primary contact.
	 * 
	 * @return the primary contact
	 */
	public boolean getPrimaryContact() {
		return primaryContact;
	}

	/**
	 * Sets the primary contact.
	 * 
	 * @param primaryContact
	 *            the new primary contact
	 */
	public void setPrimaryContact(boolean primaryContact) {
		this.primaryContact = primaryContact;
	}

	/**
	 * Gets the confirm password.
	 * 
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Sets the confirm password.
	 * 
	 * @param confirmPassword
	 *            the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * Gets the logo image path.
	 * 
	 * @return the logo image path
	 */
	public String getLogoImagePath() {
		return logoImagePath;
	}

	/**
	 * Sets the logo image path.
	 * 
	 * @param logoImagePath
	 *            the new logo image path
	 */
	public void setLogoImagePath(String logoImagePath) {
		this.logoImagePath = logoImagePath;
	}

	/**
	 * Gets the licenced vendors.
	 * 
	 * @return the licenced vendors
	 */
	public int getLicencedVendors() {
		return licencedVendors;
	}

	/**
	 * Sets the licenced vendors.
	 * 
	 * @param licencedVendors
	 *            the new licenced vendors
	 */
	public void setLicencedVendors(int licencedVendors) {
		this.licencedVendors = licencedVendors;
	}

	/**
	 * Gets the fiscal start date.
	 * 
	 * @return the fiscalStartDate
	 */
	public String getFiscalStartDate() {
		return fiscalStartDate;
	}

	/**
	 * Sets the fiscal start date.
	 * 
	 * @param fiscalStartDate
	 *            the fiscalStartDate to set
	 */
	public void setFiscalStartDate(String fiscalStartDate) {
		this.fiscalStartDate = fiscalStartDate;
	}

	/**
	 * Gets the fiscal end date.
	 * 
	 * @return the fiscalEndDate
	 */
	public String getFiscalEndDate() {
		return fiscalEndDate;
	}

	/**
	 * Sets the fiscal end date.
	 * 
	 * @param fiscalEndDate
	 *            the fiscalEndDate to set
	 */
	public void setFiscalEndDate(String fiscalEndDate) {
		this.fiscalEndDate = fiscalEndDate;
	}

	/**
	 * Gets the spend data upload frequency.
	 * 
	 * @return the spendDataUploadFrequency
	 */
	public String getSpendDataUploadFrequency() {
		return spendDataUploadFrequency;
	}

	/**
	 * Sets the spend data upload frequency.
	 * 
	 * @param spendDataUploadFrequency
	 *            the spendDataUploadFrequency to set
	 */
	public void setSpendDataUploadFrequency(String spendDataUploadFrequency) {
		this.spendDataUploadFrequency = spendDataUploadFrequency;
	}

	/**
	 * @return the hiddenEmailId
	 */
	public String getHiddenEmailId() {
		return hiddenEmailId;
	}

	/**
	 * @param hiddenEmailId
	 *            the hiddenEmailId to set
	 */
	public void setHiddenEmailId(String hiddenEmailId) {
		this.hiddenEmailId = hiddenEmailId;
	}

	/**
	 * @return the hiddenCustCode
	 */
	public String getHiddenCustCode() {
		return hiddenCustCode;
	}

	/**
	 * @param hiddenCustCode
	 *            the hiddenCustCode to set
	 */
	public void setHiddenCustCode(String hiddenCustCode) {
		this.hiddenCustCode = hiddenCustCode;
	}

	/**
	 * @return the hiddenDunsNumber
	 */
	public String getHiddenDunsNumber() {
		return hiddenDunsNumber;
	}

	/**
	 * @param hiddenDunsNumber
	 *            the hiddenDunsNumber to set
	 */
	public void setHiddenDunsNumber(String hiddenDunsNumber) {
		this.hiddenDunsNumber = hiddenDunsNumber;
	}

	/**
	 * @return the hiddenTaxId
	 */
	public String getHiddenTaxId() {
		return hiddenTaxId;
	}

	/**
	 * @param hiddenTaxId
	 *            the hiddenTaxId to set
	 */
	public void setHiddenTaxId(String hiddenTaxId) {
		this.hiddenTaxId = hiddenTaxId;
	}

	/**
	 * @return the isDivision status
	 */
	public Byte getIsDivision() {
		return isDivision;
	}

	/**
	 * @param isDivison
	 *            the isDivision is set
	 */
	public void setIsDivision(Byte isDivision) {
		this.isDivision = isDivision;
	}

	/**
	 * @return the sessionTimeout
	 */
	public int getSessionTimeout() {
		return sessionTimeout;
	}

	/**
	 * @param sessionTimeout
	 *            the sessionTimeout to set
	 */
	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	/**
	 * @return the termsCondition
	 */
	public String getTermsCondition() {
		return termsCondition;
	}

	/**
	 * @param termsCondition the termsCondition to set
	 */
	public void setTermsCondition(String termsCondition) {
		this.termsCondition = termsCondition;
	}

	/**
	 * @return the privacyTerms
	 */
	public String getPrivacyTerms() {
		return privacyTerms;
	}

	/**
	 * @param privacyTerms the privacyTerms to set
	 */
	public void setPrivacyTerms(String privacyTerms) {
		this.privacyTerms = privacyTerms;
	}

	/**
	 * @return the primeTrainingUrl
	 */
	public String getPrimeTrainingUrl() {
		return primeTrainingUrl;
	}

	/**
	 * @param primeTrainingUrl the primeTrainingUrl to set
	 */
	public void setPrimeTrainingUrl(String primeTrainingUrl) {
		this.primeTrainingUrl = primeTrainingUrl;
	}

	/**
	 * @return the nonPrimeTrainingUrl
	 */
	public String getNonPrimeTrainingUrl() {
		return nonPrimeTrainingUrl;
	}

	/**
	 * @param nonPrimeTrainingUrl the nonPrimeTrainingUrl to set
	 */
	public void setNonPrimeTrainingUrl(String nonPrimeTrainingUrl) {
		this.nonPrimeTrainingUrl = nonPrimeTrainingUrl;
	}

	/**
	 * reset the customer form fields.
	 * 
	 * @param request
	 *            the request
	 * @param map
	 *            the map
	 */
	public void reset(HttpServletRequest request, ActionMapping map) {

		this.companycode = "";
		this.companyname = "";
		this.address1 = "";
		this.address2 = "";
		this.address3 = "";
		this.city = "";
		this.contactFax = "";
		this.contactMobile = "";
		this.contactPhone = "";
		this.contanctEmail = "";
		this.country = "";
		this.dunsnum = "";
		this.email = "";
		this.fax = "";
		this.firstName = "";
		this.lastName = "";
		this.userSecQnAns = "";
		// this.loginId = "";
		this.loginpassword = "";
		this.spendDataUploadFrequency = "Y";
		this.databaseName = "";
		this.userName = "";
		this.dbpassword = "";
		this.databaseIp = "";
		this.applicationUrl = "";
		this.termsCondition = "";
		this.privacyTerms = "";
		this.primeTrainingUrl = "";
		this.nonPrimeTrainingUrl = "";
	}
}