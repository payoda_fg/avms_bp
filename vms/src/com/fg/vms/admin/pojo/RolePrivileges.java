/**
 * RolePrivileges.java
 */
package com.fg.vms.admin.pojo;

/**
 * The Class RolePrivileges.
 * 
 * @author pirabu
 */
public class RolePrivileges {

    /** The id. */
    private Integer id;

    /** The object name. */
    private String objectName;

    /** The add. */
    private String add;

    /** The delete. */
    private String delete;

    /** The edit. */
    private String edit;

    /** The view. */
    private String view;

    /** The visible. */
    private String visible;

    /** The enable. */
    private String enable;

    /** The is active. */
    private String isActive;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the object name.
     * 
     * @return the objectName
     */
    public String getObjectName() {
	return objectName;
    }

    /**
     * Sets the object name.
     * 
     * @param objectName
     *            the objectName to set
     */
    public void setObjectName(String objectName) {
	this.objectName = objectName;
    }

    /**
     * Gets the adds the.
     * 
     * @return the add
     */
    public String getAdd() {
	return add;
    }

    /**
     * Sets the adds the.
     * 
     * @param add
     *            the add to set
     */
    public void setAdd(String add) {
	this.add = add;
    }

    /**
     * Gets the delete.
     * 
     * @return the delete
     */
    public String getDelete() {
	return delete;
    }

    /**
     * Sets the delete.
     * 
     * @param delete
     *            the delete to set
     */
    public void setDelete(String delete) {
	this.delete = delete;
    }

    /**
     * Gets the edits the.
     * 
     * @return the edit
     */
    public String getEdit() {
	return edit;
    }

    /**
     * Sets the edits the.
     * 
     * @param edit
     *            the edit to set
     */
    public void setEdit(String edit) {
	this.edit = edit;
    }

    /**
     * Gets the view.
     * 
     * @return the view
     */
    public String getView() {
	return view;
    }

    /**
     * Sets the view.
     * 
     * @param view
     *            the view to set
     */
    public void setView(String view) {
	this.view = view;
    }

    /**
     * Gets the visible.
     * 
     * @return the visible
     */
    public String getVisible() {
	return visible;
    }

    /**
     * Sets the visible.
     * 
     * @param visible
     *            the visible to set
     */
    public void setVisible(String visible) {
	this.visible = visible;
    }

    /**
     * Gets the enable.
     * 
     * @return the enable
     */
    public String getEnable() {
	return enable;
    }

    /**
     * Sets the enable.
     * 
     * @param enable
     *            the enable to set
     */
    public void setEnable(String enable) {
	this.enable = enable;
    }

    /**
     * Gets the checks if is active.
     * 
     * @return the isActive
     */
    public String getIsActive() {
	return isActive;
    }

    /**
     * Sets the checks if is active.
     * 
     * @param isActive
     *            the isActive to set
     */
    public void setIsActive(String isActive) {
	this.isActive = isActive;
    }

}
