/**
 * PrivilegeForm.java
 */
package com.fg.vms.admin.pojo;

import org.apache.struts.validator.ValidatorForm;

import fr.improve.struts.taglib.layout.datagrid.Datagrid;

/**
 * The Class PrivilegeForm.
 * 
 * @author vinoth
 */
public class PrivilegeForm extends ValidatorForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    /** The role name. */
    private String roleName;

    /** The datagrid. */
    private Datagrid datagrid;
    
    /** The Application Category Name. */
    private String applicationcategoryname;

    /**
	 * @return the applicationcategoryname
	 */
	public String getApplicationcategoryname() {
		return applicationcategoryname;
	}

	/**
	 * @param applicationcategoryname the applicationcategoryname to set
	 */
	public void setApplicationcategoryname(String applicationcategoryname) {
		this.applicationcategoryname = applicationcategoryname;
	}
	
	/**
     * Gets the role name.
     * 
     * @return the roleName
     */
    public String getRoleName() {
	return roleName;
    }

    /**
     * Sets the role name.
     * 
     * @param roleName
     *            the roleName to set
     */
    public void setRoleName(String roleName) {
	this.roleName = roleName;
    }

    /**
     * Gets the datagrid.
     * 
     * @return the datagrid
     */
    public Datagrid getDatagrid() {
	return datagrid;
    }

    /**
     * Sets the datagrid.
     * 
     * @param datagrid
     *            the datagrid to set
     */
    public void setDatagrid(Datagrid datagrid) {
	this.datagrid = datagrid;
    }

}
