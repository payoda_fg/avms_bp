/*
 * CustomerContactForm.java
 */
package com.fg.vms.admin.pojo;

import org.apache.struts.validator.ValidatorForm;

/**
 * CustomerContact bean class for Add more than one Contacts to Customer.
 * 
 * @author srinivasarao
 * 
 */
public class CustomerContactForm extends ValidatorForm {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Integer id;

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The title. */
    private String title;

    /** The designation. */
    private String designation;

    /** The contact phone. */
    private String contactPhone;

    /** The contact mobile. */
    private String contactMobile;

    /** The contact fax. */
    private String contactFax;

    /** The contanct email. */
    private String contanctEmail;
    private String hiddenEmailId;

    /** The login allowed. */
    private boolean loginAllowed;

    /** The login display name. */
    private String loginDisplayName;

    /** The login id. */
    private String loginId;

    private String hiddenLoginId;

    /** The loginpassword. */
    private String loginpassword;

    /** The primary contact. */
    private Boolean primaryContact;

    /** The confirm password. */
    private String confirmPassword;

    /** The user sec qn. */
    private Integer userSecQn;

    /** The user sec qn ans. */
    private String userSecQnAns;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the first name.
     * 
     * @return the first name
     */
    public String getFirstName() {
	return firstName;
    }

    /**
     * Sets the first name.
     * 
     * @param firstName
     *            the new first name
     */
    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    /**
     * Gets the last name.
     * 
     * @return the last name
     */
    public String getLastName() {
	return lastName;
    }

    /**
     * Sets the last name.
     * 
     * @param lastName
     *            the new last name
     */
    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    /**
     * Gets the title.
     * 
     * @return the title
     */
    public String getTitle() {
	return title;
    }

    /**
     * Gets the designation.
     * 
     * @return the designation
     */
    public String getDesignation() {
	return designation;
    }

    /**
     * Sets the designation.
     * 
     * @param designation
     *            the designation to set
     */
    public void setDesignation(String designation) {
	this.designation = designation;
    }

    /**
     * Sets the title.
     * 
     * @param title
     *            the new title
     */
    public void setTitle(String title) {
	this.title = title;
    }

    /**
     * Checks if is login allowed.
     * 
     * @return the loginAllowed
     */
    public boolean isLoginAllowed() {
	return loginAllowed;
    }

    /**
     * Sets the login allowed.
     * 
     * @param loginAllowed
     *            the loginAllowed to set
     */
    public void setLoginAllowed(boolean loginAllowed) {
	this.loginAllowed = loginAllowed;
    }

    /**
     * Gets the contact phone.
     * 
     * @return the contact phone
     */
    public String getContactPhone() {
	return contactPhone;
    }

    /**
     * Sets the contact phone.
     * 
     * @param contactPhone
     *            the new contact phone
     */
    public void setContactPhone(String contactPhone) {
	this.contactPhone = contactPhone;
    }

    /**
     * Gets the contact mobile.
     * 
     * @return the contact mobile
     */
    public String getContactMobile() {
	return contactMobile;
    }

    /**
     * Sets the contact mobile.
     * 
     * @param contactMobile
     *            the new contact mobile
     */
    public void setContactMobile(String contactMobile) {
	this.contactMobile = contactMobile;
    }

    /**
     * Gets the contact fax.
     * 
     * @return the contact fax
     */
    public String getContactFax() {
	return contactFax;
    }

    /**
     * Sets the contact fax.
     * 
     * @param contactFax
     *            the new contact fax
     */
    public void setContactFax(String contactFax) {
	this.contactFax = contactFax;
    }

    /**
     * Gets the contanct email.
     * 
     * @return the contanct email
     */
    public String getContanctEmail() {
	return contanctEmail;
    }

    /**
     * Sets the contanct email.
     * 
     * @param contanctEmail
     *            the new contanct email
     */
    public void setContanctEmail(String contanctEmail) {
	this.contanctEmail = contanctEmail;
    }

    /**
     * Gets the login display name.
     * 
     * @return the login display name
     */
    public String getLoginDisplayName() {
	return loginDisplayName;
    }

    /**
     * Sets the login display name.
     * 
     * @param loginDisplayName
     *            the new login display name
     */
    public void setLoginDisplayName(String loginDisplayName) {
	this.loginDisplayName = loginDisplayName;
    }

    /**
     * Gets the login id.
     * 
     * @return the login id
     */
    public String getLoginId() {
	return loginId;
    }

    /**
     * Sets the login id.
     * 
     * @param loginId
     *            the new login id
     */
    public void setLoginId(String loginId) {
	this.loginId = loginId;
    }

    /**
     * Gets the loginpassword.
     * 
     * @return the loginpassword
     */
    public String getLoginpassword() {
	return loginpassword;
    }

    /**
     * Sets the loginpassword.
     * 
     * @param loginpassword
     *            the new loginpassword
     */
    public void setLoginpassword(String loginpassword) {
	this.loginpassword = loginpassword;
    }

    /**
     * Gets the confirm password.
     * 
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
	return confirmPassword;
    }

    /**
     * Sets the confirm password.
     * 
     * @param confirmPassword
     *            the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
	this.confirmPassword = confirmPassword;
    }

    /**
     * Gets the primary contact.
     * 
     * @return the primary contact
     */
    public Boolean getPrimaryContact() {
	return primaryContact;
    }

    /**
     * Sets the primary contact.
     * 
     * @param primaryContact
     *            the new primary contact
     */
    public void setPrimaryContact(Boolean primaryContact) {
	this.primaryContact = primaryContact;
    }

    /**
     * Gets the user sec qn.
     * 
     * @return the userSecQn
     */
    public Integer getUserSecQn() {
	return userSecQn;
    }

    /**
     * Sets the user sec qn.
     * 
     * @param userSecQn
     *            the userSecQn to set
     */
    public void setUserSecQn(Integer userSecQn) {
	this.userSecQn = userSecQn;
    }

    /**
     * Gets the user sec qn ans.
     * 
     * @return the user sec qn ans
     */
    public String getUserSecQnAns() {
	return userSecQnAns;
    }

    /**
     * Sets the user sec qn ans.
     * 
     * @param userSecQnAns
     *            the new user sec qn ans
     */
    public void setUserSecQnAns(String userSecQnAns) {
	this.userSecQnAns = userSecQnAns;
    }

    /**
     * @return the hiddenEmailId
     */
    public String getHiddenEmailId() {
	return hiddenEmailId;
    }

    /**
     * @param hiddenEmailId
     *            the hiddenEmailId to set
     */
    public void setHiddenEmailId(String hiddenEmailId) {
	this.hiddenEmailId = hiddenEmailId;
    }

    /**
     * @return the hiddenLoginId
     */
    public String getHiddenLoginId() {
	return hiddenLoginId;
    }

    /**
     * @param hiddenLoginId
     *            the hiddenLoginId to set
     */
    public void setHiddenLoginId(String hiddenLoginId) {
	this.hiddenLoginId = hiddenLoginId;
    }

}
