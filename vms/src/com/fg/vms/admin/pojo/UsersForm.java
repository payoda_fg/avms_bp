/**
 * UsersForm.java
 */
package com.fg.vms.admin.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import com.fg.vms.admin.model.UserRoles;

/**
 * Represents the User Details (eg. userName, userLogin, userPassword) for
 * interact with UI
 * 
 * @author vinoth
 * 
 */

public class UsersForm extends ValidatorForm {

	/**
	 * The version of the serializable footprint of this Class.
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private Integer id;

	/** The role id. */
	private Integer roleId;

	/** The user name. */
	private String userName;

	/** The user login. */
	private String userLogin;

	/** The hidden user login. */
	private String hiddenUserLogin;

	/** The user password. */
	private String userPassword;

	/** The user email id. */
	private String userEmailId;

	/** The hidden email id. */
	private String hiddenEmailId;

	/** The user roles. */
	private UserRoles userRoles;

	/** The confirm password. */
	private String confirmPassword;

	/** The active. */
	private String active;

	private Integer secretQuestionId;

	private String secQueAns;

	private String department;

	private String division;

	private String phoneNumber;

	private String extension;

	private String firstName;

	private String lastName;

	private Integer timeZone;

	private String title;

	private Integer stackHolder;
	
	private Integer customerDivision;
	
	//Add Multiple division to 1 user
	
	private String[] customerDivisionId;
	
	private String stackHolderEmailId; 
	
	private String stackHolderFirstName;
	
	private String stackHolderLastName;
	
	private String stackHolderManagerName;
	
	private String stackHolderReason;
	
	private String isApprove;
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the role id.
	 * 
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * Sets the role id.
	 * 
	 * @param roleId
	 *            the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the user login.
	 * 
	 * @return the userLogin
	 */
	public String getUserLogin() {
		return userLogin;
	}

	/**
	 * Sets the user login.
	 * 
	 * @param userLogin
	 *            the userLogin to set
	 */
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	/**
	 * Gets the hidden user login.
	 * 
	 * @return the hiddenUserLogin
	 */
	public String getHiddenUserLogin() {
		return hiddenUserLogin;
	}

	/**
	 * Sets the hidden user login.
	 * 
	 * @param hiddenUserLogin
	 *            the hiddenUserLogin to set
	 */
	public void setHiddenUserLogin(String hiddenUserLogin) {
		this.hiddenUserLogin = hiddenUserLogin;
	}

	/**
	 * Gets the user password.
	 * 
	 * @return the userPassword
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Sets the user password.
	 * 
	 * @param userPassword
	 *            the userPassword to set
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * Gets the confirm password.
	 * 
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Sets the confirm password.
	 * 
	 * @param confirmPassword
	 *            the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * Gets the user email id.
	 * 
	 * @return the userEmailId
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * Sets the user email id.
	 * 
	 * @param userEmailId
	 *            the userEmailId to set
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * Gets the hidden email id.
	 * 
	 * @return the hiddenEmailId
	 */
	public String getHiddenEmailId() {
		return hiddenEmailId;
	}

	/**
	 * Sets the hidden email id.
	 * 
	 * @param hiddenEmailId
	 *            the hiddenEmailId to set
	 */
	public void setHiddenEmailId(String hiddenEmailId) {
		this.hiddenEmailId = hiddenEmailId;
	}

	/**
	 * Gets the user roles.
	 * 
	 * @return the userRoles
	 */
	public UserRoles getUserRoles() {
		return userRoles;
	}

	/**
	 * Sets the user roles.
	 * 
	 * @param userRoles
	 *            the userRoles to set
	 */
	public void setUserRoles(UserRoles userRoles) {
		this.userRoles = userRoles;
	}

	/**
	 * @return the active
	 */
	public String getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department
	 *            the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the division
	 */
	public String getDivision() {
		return division;
	}

	/**
	 * @param division
	 *            the division to set
	 */
	public void setDivision(String division) {
		this.division = division;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the timeZone
	 */
	public Integer getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone
	 *            the timeZone to set
	 */
	public void setTimeZone(Integer timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The mapping used to select this instance
	 * @param request
	 *            The servlet request which is processing
	 */
	public Integer getSecretQuestionId() {
		return secretQuestionId;
	}

	public void setSecretQuestionId(Integer secretQuestionId) {
		this.secretQuestionId = secretQuestionId;
	}

	public String getSecQueAns() {
		return secQueAns;
	}

	public void setSecQueAns(String secQueAns) {
		this.secQueAns = secQueAns;
	}

	public Integer getStackHolder() {
		return stackHolder;
	}

	public void setStackHolder(Integer stackHolder) {
		this.stackHolder = stackHolder;
	}

	public Integer getCustomerDivision() {
		return customerDivision;
	}

	public void setCustomerDivision(Integer customerDivision) {
		this.customerDivision = customerDivision;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.userLogin = "";
		this.userName = "";
		this.userPassword = "";
		this.confirmPassword = "";
		this.userEmailId = "";
		this.userLogin = "";
		this.roleId = 0;
		this.secretQuestionId = 0;
		this.secQueAns = "";
		this.department = "";
		this.division = "";
		this.phoneNumber = "";
		this.extension = "";
		this.firstName = "";
		this.lastName = "";
		this.timeZone = 0;
		this.title = "";
		this.stackHolder = null;
		this.customerDivision=null;
		this.stackHolderEmailId = "";
		this.stackHolderFirstName = "";
		this.stackHolderLastName = "";
		this.stackHolderManagerName = "";
		this.stackHolderReason = "";
		this.isApprove = "";
		this.customerDivisionId = null;
		
	}

	/**
	 * @return the stackHolderEmailId
	 */
	public String getStackHolderEmailId() {
		return stackHolderEmailId;
	}

	/**
	 * @param stackHolderEmailId the stackHolderEmailId to set
	 */
	public void setStackHolderEmailId(String stackHolderEmailId) {
		this.stackHolderEmailId = stackHolderEmailId;
	}

	/**
	 * @return the stackHolderFirstName
	 */
	public String getStackHolderFirstName() {
		return stackHolderFirstName;
	}

	/**
	 * @param stackHolderFirstName the stackHolderFirstName to set
	 */
	public void setStackHolderFirstName(String stackHolderFirstName) {
		this.stackHolderFirstName = stackHolderFirstName;
	}

	/**
	 * @return the stackHolderLastName
	 */
	public String getStackHolderLastName() {
		return stackHolderLastName;
	}

	/**
	 * @param stackHolderLastName the stackHolderLastName to set
	 */
	public void setStackHolderLastName(String stackHolderLastName) {
		this.stackHolderLastName = stackHolderLastName;
	}

	/**
	 * @return the stackHolderManagerName
	 */
	public String getStackHolderManagerName() {
		return stackHolderManagerName;
	}

	/**
	 * @param stackHolderManagerName the stackHolderManagerName to set
	 */
	public void setStackHolderManagerName(String stackHolderManagerName) {
		this.stackHolderManagerName = stackHolderManagerName;
	}

	/**
	 * @return the stackHolderReason
	 */
	public String getStackHolderReason() {
		return stackHolderReason;
	}

	/**
	 * @param stackHolderReason the stackHolderReason to set
	 */
	public void setStackHolderReason(String stackHolderReason) {
		this.stackHolderReason = stackHolderReason;
	}

	/**
	 * @return the isApprove
	 */
	public String getIsApprove() {
		return isApprove;
	}

	/**
	 * @param isApprove the isApprove to set
	 */
	public void setIsApprove(String isApprove) {
		this.isApprove = isApprove;
	}

	/**
	 * @return the customerDivisionId
	 */
	public String[] getCustomerDivisionId() {
		return customerDivisionId;
	}

	/**
	 * @param customerDivisionId the customerDivisionId to set
	 */
	public void setCustomerDivisionId(String[] customerDivisionId) {
		this.customerDivisionId = customerDivisionId;
	}
}
