/*
 * ViewCustomerForm.java
 */
package com.fg.vms.admin.pojo;

import org.apache.struts.action.ActionForm;

/**
 * The Class ViewCustomerForm.
 */
public class ViewCustomerForm extends ActionForm {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** The message. */
    private String message;

    /**
     * Gets the message.
     * 
     * @return the message
     */
    public String getMessage() {
	return message;
    }

    /**
     * Sets the message.
     * 
     * @param message
     *            the new message
     */
    public void setMessage(String message) {
	this.message = message;
    }

}
