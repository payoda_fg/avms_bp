/*
 * RolePrivilegesForm.java
 */
package com.fg.vms.admin.pojo;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * The Class RolePrivilegesForm.
 */
public class RolePrivilegesForm extends ActionForm {

    /**
     * The version of the serializable footprint of this Class.
     */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Integer id;

    /** The role name. */
    private String roleName;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
	return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * Gets the role name.
     * 
     * @return the roleName
     */
    public String getRoleName() {
	return roleName;
    }

    /**
     * Sets the role name.
     * 
     * @param roleName
     *            the roleName to set
     */
    public void setRoleName(String roleName) {
	this.roleName = roleName;
    }

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	this.roleName="0";
    	this.id=null;
    }
}
