/*
 * SaveCurrentCustomer.java
 */
package com.fg.vms.admin.pojo;

import org.apache.struts.action.ActionForm;

/**
 * The Class SaveCurrentCustomer.
 */
public class SaveCurrentCustomer extends ActionForm {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The current customer. */
    private String currentCustomer;

    /**
     * Gets the current customer.
     * 
     * @return the current customer
     */
    public String getCurrentCustomer() {
	return currentCustomer;
    }

    /**
     * Sets the current customer.
     * 
     * @param currentCustomer
     *            the new current customer
     */
    public void setCurrentCustomer(String currentCustomer) {
	this.currentCustomer = currentCustomer;
    }

}
