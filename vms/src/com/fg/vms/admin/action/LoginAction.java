/**
 * LoginAction.java
 */
package com.fg.vms.admin.action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.actions.DispatchAction;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.RolePrvilegesDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dao.impl.RolePrivilegesDaoImpl;
import com.fg.vms.admin.dao.impl.UserRolesDaoImpl;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.LoginTrack;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.LoginForm;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.ReportDashboardDao;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.ReportDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDashboardDaoImpl;
import com.fg.vms.customer.dao.impl.Tier2ReportingPeriodDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerUserDivision;
import com.fg.vms.customer.model.DashboardSettings;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.model.VendorContact;
import com.fg.vms.customer.model.VendorMaster;
import com.fg.vms.customer.model.VendorRolePrivileges;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.FrequencyPeriod;
import com.fg.vms.util.VendorStatus;

/**
 * This class used for User Authentication and put the user details (such as
 * database details, user details..) into session.
 * 
 * @author pirabu
 * 
 */
public class LoginAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Validate the user Login (user name) and password.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward authenticate(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LoginForm loginInfo = (LoginForm) form;
		LoginDao loginDaoImpl = new LoginDaoImpl();
		HttpSession session = request.getSession();
		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Decrypt decrypt = new Decrypt();
		String result = "";
		String decryptedPassword = null;
		String decryptedNewPassword = null;
		Integer userId = null;
		Integer roleId = null;
		Byte isSysPassGenerated = 0;
        int userRoleId=0;
		/*
		 * Using the userLogin name to retrieve the user object.
		 */
		Users user = loginDaoImpl.authenticate(loginInfo.getUserEmailId(),
				userDetails);
		Integer customerDivisionId = null;

		LoginTrack track = null;
		Integer count = 0;
		/** Get the customer Role Privileges to set the privileges to user. */
		List<RolePrivileges> privileges;
		/** Get the Vendor Role Privileges to set the privileges to user. */
		List<VendorRolePrivileges> rolePrivileges;

		VendorContact vendorUser = null;
		UsersDao usersDaoImpl = new UsersDaoImpl();
		/**
		 * Authenticate user name and password when login as customer or FG
		 * admin.
		 */
		if (user != null) {
			CustomerApplicationSettings settings = (CustomerApplicationSettings) session
					.getAttribute("isDivisionStatus");
			if (settings.getIsDivision() != 0 && settings.getIsDivision() != null) {
				
				List<CustomerUserDivision> userDivisionList = usersDaoImpl.listUserDivisionsByUser(userDetails, user.getId());
				
				if(userDivisionList != null){
					String [] customerDivisionIds = new String[userDivisionList.size()];
					for(int i=0; i<userDivisionList.size(); i++){
						customerDivisionIds[i]=String.valueOf(userDivisionList.get(i).getCustomerDivisionId().getId());
					}
					userDetails.setCustomerDivisionIds(customerDivisionIds);
					if(userDivisionList.size() == 1)
					{
						if(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal()!= null && userDivisionList.get(0).getCustomerDivisionId().getIsGlobal() != 0)
							userDetails.setIsGlobalDivision(Integer.valueOf(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal()));
						else
							userDetails.setIsGlobalDivision(0);
					}
					else
						userDetails.setIsGlobalDivision(0);
				} 
				else
				{
					ActionErrors errors = new ActionErrors();
					errors.add("login", new ActionMessage("err.noDivision"));
					saveErrors(request, errors);
					return mapping.findForward("customerloginpage");
				}
					
			//Changed userDetails CustomerDivisionID to list of CustomerDivisionIds	
				
				/*if (user.getCustomerDivisionId() != null) {
					if(user.getCustomerDivisionId().getId() != null
							&& user.getCustomerDivisionId().getId() == 3)
					{
						session.setAttribute("customerDivisionId",0);
						userDetails.setCustomerDivisionID((Integer)session.getAttribute("customerDivisionId"));
					}
					else if (user.getCustomerDivisionId().getId() != null
							&& user.getCustomerDivisionId().getId() != 0) {
						customerDivisionId = user.getCustomerDivisionId()
								.getId();
						session.setAttribute("customerDivisionId",
								customerDivisionId);
						userDetails.setCustomerDivisionID((Integer) session
								.getAttribute("customerDivisionId"));
					}
					//To set  customer division isGlobal or not in userDetails
					if(user.getCustomerDivisionId().getIsGlobal() != null && user.getCustomerDivisionId().getIsGlobal() != 0)
						userDetails.setIsGlobalDivision(Integer.valueOf(user.getCustomerDivisionId().getIsGlobal()));
					else
						userDetails.setIsGlobalDivision(0);
				} else {
					ActionErrors errors = new ActionErrors();
					errors.add("login", new ActionMessage("err.noDivision"));
					saveErrors(request, errors);
					return mapping.findForward("customerloginpage");
				}*/
			}
			
			// Decrypt the password to authenticate user.
			Integer keyvalue = user.getKeyvalue();
			userId = user.getId();
			isSysPassGenerated = user.getIsSysGenPassword();

			decryptedPassword = decrypt
					.decryptText(String.valueOf(keyvalue.toString()),
							user.getUserPassword());
			if (user.getNewPassword() != null
					&& user.getNewPassword().length() != 0) {
				decryptedNewPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						user.getNewPassword());
			}

		} else if (userDetails.getUserType() != null
				&& "customerOrVendor".equalsIgnoreCase(userDetails
						.getUserType())) {
			/**
			 * Authenticate vendor user name and password when login as vendor.
			 * customerOrVendor represent the application act as customer.
			 */
			vendorUser = loginDaoImpl.authenticateVendor(
					loginInfo.getUserEmailId(), userDetails);

			if (null != vendorUser
					&& null != vendorUser.getVendorId()
							.getIsPartiallySubmitted()
					&& vendorUser.getVendorId().getIsPartiallySubmitted()
							.equalsIgnoreCase("yes")) {

				ActionErrors errors = new ActionErrors();
				errors.add("login", new ActionMessage("err.partialdata"));
				saveErrors(request, errors);
				if (!userDetails.getDbName().equalsIgnoreCase(
						Constants.getString(Constants.MASTER_DB_NAME))) {
					return mapping.findForward("customerloginpage");
				}
			}

			if (vendorUser != null) {
				userId = vendorUser.getId();
				roleId = vendorUser.getVendorUserRoleId().getId();
				Integer keyvalue = vendorUser.getKeyValue();
				isSysPassGenerated = vendorUser.getIsSysGenPassword();

				decryptedPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						vendorUser.getPassword());
				if (vendorUser.getNewPassword() != null
						&& vendorUser.getNewPassword().length() != 0) {
					decryptedNewPassword = decrypt.decryptText(
							String.valueOf(keyvalue.toString()),
							vendorUser.getNewPassword());
				}
				CustomerApplicationSettings settings = (CustomerApplicationSettings) session
						.getAttribute("isDivisionStatus");				
				if (settings.getIsDivision() != 0
						&& settings.getIsDivision() != null) {
					 if (vendorUser.getVendorId() != null
							&& vendorUser.getVendorId().getCustomerDivisionId() != null
							&& vendorUser.getVendorId().getCustomerDivisionId()
									.getId() != null) {
						customerDivisionId = vendorUser.getVendorId()
								.getCustomerDivisionId().getId();
						session.setAttribute("customerDivisionId",
								customerDivisionId);
						userDetails.setCustomerDivisionID((Integer) session
								.getAttribute("customerDivisionId"));
					} else {
						ActionErrors errors = new ActionErrors();
						errors.add("login", new ActionMessage("err.noDivision"));
						saveErrors(request, errors);
						return mapping.findForward("customerloginpage");
					}
					//To set  customer division isGlobal or not in userDetails
					if(vendorUser.getVendorId().getCustomerDivisionId().getIsGlobal() != null && vendorUser.getVendorId().getCustomerDivisionId().getIsGlobal() != 0)
						userDetails.setIsGlobalDivision(Integer.valueOf(vendorUser.getVendorId().getCustomerDivisionId().getIsGlobal()));
					else
						userDetails.setIsGlobalDivision(0);
				}
			}
		}

		if (decryptedPassword != null) {
			/**
			 * Validate password from login page and decryptedPassword from
			 * database, then forward to home page.
			 */
			if (loginInfo.getUserPassword().equals(decryptedPassword)) {

				track = loginDaoImpl.logEntry(loginInfo.getUserEmailId(), 0,
						userDetails);

				/*
				 * If sys generated password is created using forgot password
				 * option
				 */
				if (isSysPassGenerated == 1) {
					return mapping.findForward("resetPassword");
				}
				/* Get the admin role name when user login as FG */
				if (userDetails.getUserType() != null
						&& userDetails.getUserType().equalsIgnoreCase("FG")) {

					String roleName = new UserRolesDaoImpl()
							.viewRoles(userDetails).get(0).getRoleName();
					session.setAttribute("adminRoleName", roleName);
				} else if (user != null) {
					// reset password 90 days once.
					CURDDao<PasswordHistory> curdDao = new CURDTemplateImpl<PasswordHistory>();
					PasswordHistory history = curdDao.find(
							userDetails,
							"from PasswordHistory where customerUserID="
									+ user.getId()
									+ " order by changedon desc limit 1");

					if (history != null && history.getChangedon() != null) {
						Date past = history.getChangedon(); // June 20th, 2010
						Date today = new Date(); // July 24th
						int days = Days.daysBetween(new DateTime(past),
								new DateTime(today)).getDays();
						if (days > 90)
							return mapping.findForward("resetPassword");
					}
				}
				session.setAttribute("userId", userId);
				session.setAttribute("currentUser", user);
				session.setAttribute("vendorUser", vendorUser);
				RolePrvilegesDao rolePrvilegesDao = new RolePrivilegesDaoImpl();
				session.removeAttribute("count");
				
				//Setting Workflow Configuration in the Session to Access in the Whole Application
				WorkflowConfigurationDaoImpl configurationDaoImpl = new WorkflowConfigurationDaoImpl();
				WorkflowConfiguration workflowConfigurations = configurationDaoImpl.listWorkflowConfig(userDetails);
				userDetails.setWorkflowConfiguration(workflowConfigurations);
				
				/**
				 * If the user is customer or FG, then retrieve the role
				 * privileges from customer or VMS_MASTER database.
				 */
				if (user != null) {
					privileges = rolePrvilegesDao.retrievePrivilegesByRole(user
							.getUserRoleId().getId(), userDetails);
					// To hide/show menu based on sub-menus
					setMenusInHomePage(privileges, session);
					session.setAttribute("privileges", privileges);
					result = "success";
					/**
					 * If the user is customer user, then forward to customer
					 * home page.
					 */
					if (!(userDetails.getUserType() != null && userDetails
							.getUserType().equalsIgnoreCase("FG"))) {

						ReportDao reportDao = new ReportDaoImpl();
						ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
						Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
						Tier2ReportingPeriod reportingPeriod = periodDao
								.currentReportingPeriod(userDetails);

						int reportStartMonth = 0;
						int reportEndMonth = 3 - 1;
						Integer year = Calendar.getInstance()
								.get(Calendar.YEAR);
						String currentQuarter = null;
						if (reportingPeriod != null) {
							Calendar c = Calendar.getInstance();
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MMM dd, yyyy");
							switch (FrequencyPeriod.valueOf(reportingPeriod
									.getDataUploadFreq())) {

							case M:
								c.set(year, c.get(Calendar.MONTH) - 1, 1); // ------>
								c.set(Calendar.DAY_OF_MONTH,
										c.getActualMinimum(Calendar.DAY_OF_MONTH));

								StringBuilder period = new StringBuilder();

								period.append(sdf.format(c.getTime()));
								c.set(Calendar.DAY_OF_MONTH,
										c.getActualMaximum(Calendar.DAY_OF_MONTH));
								period.append(" - " + sdf.format(c.getTime()));
								currentQuarter = period.toString();
								break;
							case Q:
								List<String> reportPeriod = CommonUtils
										.calculateReportingPeriods(
												reportingPeriod, year);
								int index = CommonUtils
										.findCurrentQuarter(reportingPeriod);
								//For Getting Previous Quarter of Reporting Period 
								if(0 == index) {
									//For Previous Year Last Quarter
									reportPeriod = CommonUtils
											.calculateReportingPeriods(reportingPeriod, year-1);					
									currentQuarter = reportPeriod.get(3);
								} else {
									currentQuarter = reportPeriod.get(index-1);
								}								
								break;
							case A:
								reportStartMonth = reportingPeriod
										.getStartDate().getMonth();
								reportEndMonth = reportingPeriod.getEndDate()
										.getMonth();

								c.set(year-1, reportStartMonth, 1); // ------>
								c.set(Calendar.DAY_OF_MONTH,
										c.getActualMinimum(Calendar.DAY_OF_MONTH));
								StringBuilder period1 = new StringBuilder();

								period1.append(sdf.format(c.getTime()));
								c.set(year-1, reportEndMonth, 1); // ------>
								c.set(Calendar.DAY_OF_MONTH,
										c.getActualMaximum(Calendar.DAY_OF_MONTH));
								period1.append(" - " + sdf.format(c.getTime()));
								currentQuarter = period1.toString();
								break;

							}
						}
						boolean hasDashBoard = true;
						if (privileges != null) {
							for (RolePrivileges rolePriv : privileges) {
								if (rolePriv.getObjectId().getObjectName().equalsIgnoreCase("Dashboard View") && rolePriv.getView() != 1) {
									/*hasDashBoard = false;*/
									/*Nothing To Do*/
								}
								if (rolePriv.getObjectId().getObjectName().equalsIgnoreCase("Display Dashboard") && rolePriv.getView() != 1) {
									hasDashBoard = false;
								}
							}
						}
						
						if(null != currentQuarter && !currentQuarter.isEmpty() ) {
							session.setAttribute("currentQuarter", currentQuarter);
						}
						
						List<DashboardSettings> dashboardSettings = reportDao
								.listDashboardSettings(userDetails);
						
						if(null != user.getUserRoleId() && null != user.getUserRoleId().getId())
						{
							userRoleId=user
									.getUserRoleId().getId();
						}
						
						// Start User Role Based Dash Board Displaying
						CURDDao<RolePrivileges> curdDao = new CURDTemplateImpl<RolePrivileges>();
						List<RolePrivileges> rolePrivilege = curdDao.list(userDetails,
								        " from RolePrivileges r where r.view=1 and r.roleId="+userRoleId
										+ " and r.objectId.applicationCategory='D' and r.objectId.objectName not like '%Dashboard View%' "
										+ " and r.objectId.objectName not like '%Display Dashboard%'");
						
						if(rolePrivilege.size()>0 && rolePrivilege!=null && hasDashBoard == true)
						{
							Iterator itr=rolePrivilege.iterator();
							RolePrivileges privilege=new RolePrivileges();
							while(itr.hasNext())
							{
								privilege = (RolePrivileges)itr.next();
								if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Diversity Analysis Dashboard"))
								{
									session.setAttribute("diversityview", "1");
									List<Tier2ReportDto> diversityDashboard = null;
									diversityDashboard = reportDao
											.getDiversityDashboardReport(userDetails);
									session.setAttribute("diversityDashboard",
											diversityDashboard);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendor Status Breakdown Dashboard"))
								{
									session.setAttribute(
											"ststusdashboardvalue", "1");
									List<Tier2ReportDto> statusDashboard = null;
									statusDashboard = reportDao
											.getStatusDashboardReport(userDetails);
									session.setAttribute("statusDashboard",
											statusDashboard);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Supplier Count By State Dashboard"))
								{
									session.setAttribute("supplierdashboard",
											"1");
									List<Tier2ReportDto> supplierStateDashboard = null;
									supplierStateDashboard = reportDao
											.getSupplierStateDashboardReport(userDetails);
									session.setAttribute(
											"supplierStateDashboard",
											supplierStateDashboard);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendor Count By State Dashboard"))
								{
									session.setAttribute(
											"bpVendorsCountDashboard", "1");
									List<Tier2ReportDto> bpVendorsCount = null;
									bpVendorsCount = reportDao
											.getBpVendorCountDashboardReport(userDetails);
									session.setAttribute("bpVendorsCount",
											bpVendorsCount);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Spend By Agency Dashboard"))
								{
									session.setAttribute("agencydashboardGV",
											"1");
									List<Tier2ReportDto> agencyDashboard = null;
									agencyDashboard = reportDao
											.getAgencyDashboardReport(userDetails);
									session.setAttribute("agencyDashboard",
											agencyDashboard);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Total Spend Dashboard"))
								{
									session.setAttribute("totalSpendDashboard",
											"1");
									List<Tier2ReportDto> totalSalesDB = null;
									totalSalesDB = dashboardDao
											.getTotalSpendReport(userDetails,
													year.toString(),
													currentQuarter);
									session.setAttribute("totalSalesDB",
											totalSalesDB);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Direct Spend Dashboard"))
								{
									session.setAttribute("directDashboard", "1");
									List<Tier2ReportDto> directSpendDB = null;
									directSpendDB = dashboardDao
											.getDirectSpendReport(userDetails,
													year.toString(),
													currentQuarter);
									session.setAttribute("directSpendDBG",
											directSpendDB);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Indirect Spend Dashboard"))
								{
									session.setAttribute("indirectDashboard",
											"1");
									List<Tier2ReportDto> indirectSpendDB = null;
									indirectSpendDB = dashboardDao
											.getIndirectSpendReport(
													userDetails,
													year.toString(),
													currentQuarter);
									session.setAttribute("indirectSpendDBG",
											indirectSpendDB);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Tier 2 Vendor Name Report Dashboard"))
								{
									session.setAttribute("diversitydashboard",
											"1");
									List<Tier2ReportDto> diversityDBG = null;
									diversityDBG = dashboardDao
											.getDiversityReport(userDetails,
													year.toString(),
													currentQuarter);
									session.setAttribute("diversityDBG",
											diversityDBG);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Tier 2 Reporting Summary Dashboard"))
								{
									session.setAttribute("ethnicityAnalysis",
											"1");
									List<Tier2ReportDto> ethnicityDBG = null;
									ethnicityDBG = dashboardDao
											.getEthnicityReport(userDetails,
													year.toString(),
													currentQuarter);
									session.setAttribute("ethnicityDBG",
											ethnicityDBG);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("New Registrations By Month Dashboard"))
								{
									session.setAttribute("newRegistrationsByMonthDashboard", "1");
									List<Tier2ReportDto> newRegistrationsByMonth = null;
									newRegistrationsByMonth = dashboardDao.getNewRegistrationsByMonthDashboardReport(userDetails);
									session.setAttribute("newRegistrationsByMonth", newRegistrationsByMonth);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendors By Business Type"))
								{
									session.setAttribute("vendorsByIndustryDashboard", "1");
									List<Tier2ReportDto> vendorsByIndustry = null;
									vendorsByIndustry = dashboardDao.getVendorsByIndustryDashboardReport(userDetails);
									session.setAttribute("vendorsByIndustry", vendorsByIndustry);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendors By Business Type"))
								{
									session.setAttribute("bpVendorsByIndustryDashboard", "1");
									List<Tier2ReportDto> bpVendorsByIndustry = null;
									bpVendorsByIndustry = dashboardDao.getBPVendorsByIndustryDashboardReport(userDetails);
									session.setAttribute("bpVendorsByIndustry", bpVendorsByIndustry);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendors By NAICS Description"))
								{
									session.setAttribute("vendorsByNaicsDescriptionDashboard", "1");
									List<Tier2ReportDto> vendorsByNaicsDescription = null;
									vendorsByNaicsDescription = dashboardDao.getVendorsByNAICSDecriptionDashboardReport(userDetails);
									session.setAttribute("vendorsByNaicsDescription", vendorsByNaicsDescription);
								}
								else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendors By BP Market Sector"))
								{
									session.setAttribute("vendorsByBPMarketSectorDashboard", "1");
									List<Tier2ReportDto> vendorsByBPMarketSector = null;
									vendorsByBPMarketSector = dashboardDao.getVendorsByBPMarketSectorDashboardReport(userDetails);
									session.setAttribute("vendorsByBPMarketSector", vendorsByBPMarketSector);
								}
							}
							
							result = "customerhome";
						} else {
							result = "noDashBoard";						
						}
						// End User Role Based Dash Board Displaying
					}

					/**
					 * If the user is Vendor , then retrieve the role privileges
					 * from customer database in vendor role privileges table.
					 */
				} else if (vendorUser != null) {
					VendorMaster vendor = vendorUser.getVendorId();
					rolePrivileges = rolePrvilegesDao
							.retrieveVendorPrivilegesByRole(roleId, userDetails);
					session.setAttribute("rolePrivileges", rolePrivileges);
					
					if (null != vendor.getVendorStatus()
							&& (vendor.getVendorStatus().equals(
									VendorStatus.NEWREGISTRATION.getIndex()) || vendor
									.getVendorStatus().equals(
											VendorStatus.PENDINGREVIEW
													.getIndex()))) {
						session.setAttribute("primeType", 0);
						return mapping.findForward("nonprimevendor");
					} else if (vendor.getPrimeNonPrimeVendor() != null
							&& vendor.getPrimeNonPrimeVendor() == (byte) 0
							&& vendor.getIsApproved() == 1
							&& null != vendor.getDeverseSupplier()
							&& vendor.getDeverseSupplier() == (byte) 1) { // Diverse
																			// Suppliers
						session.setAttribute("primeType", 0);
						return mapping.findForward("nonprimevendor");
					} else if (null == vendor.getPrimeNonPrimeVendor()
							&& null != vendor.getVendorStatus()
							&& vendor.getVendorStatus().equals(
									VendorStatus.PENDINGREVIEW.getIndex())
							&& vendor.getIsApproved() == 0) {
						result = "tire2assessment";
					} else if (vendor.getIsApproved() == (byte) 1
							&& null != vendor.getPrimeNonPrimeVendor()
							&& vendor.getPrimeNonPrimeVendor() == (byte) 1
							&& null != vendor.getDeverseSupplier()
							&& (vendor.getDeverseSupplier() == (byte) 0 || vendor
									.getDeverseSupplier() == (byte) 1)) {
						result = "tire2home"; // Prime Diverse Supplier or Prime
												// Non-Diverse Supplier
					}					
				}

			} else { /* back to login page. */

				/** If the password is wrong, then track the log entry */
				track = loginDaoImpl.logEntry(loginInfo.getUserEmailId(), 1,
						userDetails);
				ActionErrors errors = new ActionErrors();

				errors.add("login", new ActionMessage("error.password.invalid"));
				saveErrors(request, errors);

				/*
				 * if (null != session.getAttribute("count")) { count =
				 * Integer.parseInt(session.getAttribute("count") .toString()) +
				 * 1; session.setAttribute("count", count); } if (count == 3) {
				 * usersDaoImpl = new UsersDaoImpl();
				 * usersDaoImpl.deleteUser(user.getId(), userDetails); }
				 */
				/**
				 * If the user is customer user, then forward to customer login
				 * page.
				 */
				if (!userDetails.getDbName().equalsIgnoreCase(
						Constants.getString(Constants.MASTER_DB_NAME))) {
					return mapping.findForward("customerloginpage");
				}
				// Return back to the previous state
				return mapping.getInputForward();
			}
		}
		/** If the userLogin name is wrong, then track the log entry */
		if (user == null && vendorUser == null) {
			track = loginDaoImpl.logEntry(loginInfo.getUserEmailId(), 2,
					userDetails);
			ActionErrors errors = new ActionErrors();

			errors.add("login", new ActionMessage(
					"error.usernamepassword.invalid"));
			saveErrors(request, errors);
			if (!userDetails.getDbName().equalsIgnoreCase(
					Constants.getString(Constants.MASTER_DB_NAME))) {
				return mapping.findForward("customerloginpage");
			}
			// Return back to the previous state
			return mapping.getInputForward();
		}
		session.setAttribute("tempLoginEmail", null);
		session.setAttribute("tempLoginPassword", null);
		// saveToken(request);
		return mapping.findForward(result);
	}

	/**
	 * Redirect to main page (login page).
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward loginPage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		logger.info("Server Name:" + request.getServerName());

		LoginDao login = new LoginDaoImpl();
		session.setAttribute("count", 0);
		// For Local & Cloud
//		 String requestUrl = request.getServerName();

		// For staging / New Live
		String requestUrl = request.getRequestURI().toString()
				.replaceAll(request.getServletPath(), "").trim();

		logger.info("Request URL:" + request.getRequestURL());
		/**
		 * If Requested URL is FG admin URI(/vms), then set the default
		 * application settings.
		 */
		// For Local & Cloud
//		 if (requestUrl != null && (requestUrl.equalsIgnoreCase(Constants
//		 .getString(Constants.DOMAIN_NAME)))){
		// For staging / New Live
		if (requestUrl != null && requestUrl.equalsIgnoreCase("/avms")) {
			 
		 	logger.info("_______FG__________");
			UserDetailsDto userDetail = new UserDetailsDto();
			com.fg.vms.customer.model.CustomerApplicationSettings applicationSettings = new com.fg.vms.customer.model.CustomerApplicationSettings();
			applicationSettings.setLogoPath("fg/firstgenesis-logo.png");
			userDetail.setSettings(applicationSettings);
			userDetail
					.setDbUsername(Constants.getString(Constants.DB_USERNAME));
			userDetail
					.setDbPassword(Constants.getString(Constants.DB_PASSWORD));
			userDetail.setDbName(Constants.getString(Constants.MASTER_DB_NAME));
			/* hibernate.cfg.xml represent the application act as FG. */
			userDetail.setHibernateCfgFileName("hibernate.cfg.xml");
			userDetail.setUserType("FG");
			/* Update the user details in session. */
			if (session.getAttribute("userDetails") != null) {
				session.removeAttribute("userDetails");
				session.setAttribute("userDetails", userDetail);
			} else {
				session.setAttribute("userDetails", userDetail);
			}
			session.setAttribute("sessionTimeout",userDetail.getSettings().getSessionTimeout());
			session.setAttribute("userType", "fg");
			return mapping.findForward("loginpage");

		} else {
			logger.info("_______customerOrVendor__________");
			com.fg.vms.customer.model.CustomerApplicationSettings applicationSettings = new com.fg.vms.customer.model.CustomerApplicationSettings();
			UserDetailsDto userDetail = login.authenticateServerURL(requestUrl);
			userDetail.setSettings(applicationSettings);
			applicationSettings = login
					.getCustomerApplicationSettings(userDetail);
			if (applicationSettings != null
					&& applicationSettings.getIsDivision() != null) {
				applicationSettings.setIsDivision(applicationSettings
						.getIsDivision());
			}
			session.setAttribute("isDivisionStatus", applicationSettings);

			/**
			 * If Requested URL is customer (/vmscustomer), then validate the
			 * url.
			 */
			UserDetailsDto userDetails = login
					.authenticateServerURL(requestUrl);
			/** validate the SLA period */
			if (login.validateSLAPeriod(userDetails.getCustomerSLA())
					.equalsIgnoreCase("ended")) {
				return mapping.findForward("slaperiodended");

			}
			/** Forward to customer login page. */
			if (userDetails.getDbName() != null) {
				logger.info("_______userDetails.getDbName() != null__________");
				/*
				 * hibernate.customer.cfg.xml represent the application act as
				 * customer.
				 */
				userDetails
						.setHibernateCfgFileName("hibernate.customer.cfg.xml");
				userDetails.setUserType("customerOrVendor");
				userDetails.setSettings(login
						.getCustomerApplicationSettings(userDetails));
				session.setAttribute("userDetails", userDetails);
				session.setAttribute("sessionTimeout",userDetails.getSettings().getSessionTimeout());
				session.setAttribute("userType", "customerOrVendor");

				List<SecretQuestion> secretQuestionList = login
						.getSecurityQuestions(userDetails);

				session.setAttribute("secretQuestionsList", secretQuestionList);	
				StringBuffer url=new StringBuffer(request.getRequestURL());
				url.replace(url.lastIndexOf("/"), url.length(), "/emailtemplate.do?parameter=showImageUploadPage");
				
				//Removing Http or Https Part from Request Url Part Starts Here
				String protocolPart = url.toString().split(":")[0];
				System.out.println("Request Url Protocol:" + protocolPart);
				if(protocolPart.equals("http")){
					url.replace(0, 5, "");
				} else { // https
					url.replace(0, 6, "");
				}
				System.out.println("Final Url for Image Upload in Email Template:" + url.toString());
				//Removing Http or Https Part from Request Url Part Ends Here
				
				session.setAttribute("imageUrl", url.toString());
				return mapping.findForward("customerloginpage");
			} else{
				logger.info("_______userDetails.getDbName() == null__________");
			}
		}
		return mapping.findForward("incorrectURL");
	}

	/**
	 * Validate the user Login (user name) and password.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return the action forward
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward authenticateCustomerforApproval(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result = "";
		LoginForm loginInfo = (LoginForm) form;
		LoginDao loginDaoImpl = new LoginDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Users user = loginDaoImpl.authenticate(loginInfo.getUserEmailId(),
				userDetails);

		String decryptedPassword = null;
		String decryptedNewPassword = null;
		Integer userId = null;
		if (user != null) {
			// Decrypt the password to authenticate user.
			Integer keyvalue = user.getKeyvalue();
			userId = user.getId();
			Decrypt decrypt = new Decrypt();
			decryptedPassword = decrypt
					.decryptText(String.valueOf(keyvalue.toString()),
							user.getUserPassword());
			if (user.getNewPassword() != null
					&& user.getNewPassword().length() != 0) {
				decryptedNewPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						user.getNewPassword());
			}

		}
		if (decryptedPassword != null) {

			if (loginInfo.getUserPassword().equals(decryptedPassword)
					|| (decryptedNewPassword != null && loginInfo
							.getUserPassword().equals(decryptedNewPassword))) {

				session.setAttribute("userId", userId);
				session.setAttribute("currentUser", user);
				result = "success";
				if (!userDetails.getDbName().equalsIgnoreCase(
						Constants.getString(Constants.MASTER_DB_NAME))) {
					result = "customerloginapproval";
				}

			} else {
				ActionErrors errors = new ActionErrors();
				errors.add("login", new ActionMessage("error.password.invalid"));
				saveErrors(request, errors);
				// Return back to the previous state
				return mapping.getInputForward();
			}
		}
		if (user == null) {

			ActionErrors errors = new ActionErrors();

			errors.add("login", new ActionMessage(
					"error.usernamepassword.invalid"));
			saveErrors(request, errors);

			// Return back to the previous state
			return mapping.getInputForward();
		}

		return mapping.findForward(result);
	}

	/**
	 * login page for approve vendors by customer.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return the action forward
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward loginforapproval(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession();
		logger.info("Server Name:" + request.getServerName());
		session.setAttribute("anonymousvendor", request.getParameter("id"));
		LoginDao login = new LoginDaoImpl();
		session.setAttribute("count", 0);

		String requestUrl = /* request.getServerName(); */

		request.getRequestURI().toString()
				.replaceAll(request.getServletPath(), "").trim();

		logger.info("Request URL:"
				+ request.getRequestURL().toString()
						.replaceAll(request.getServletPath(), "").trim());
		if (requestUrl != null
				&& (requestUrl.equalsIgnoreCase(Constants
						.getString(Constants.DOMAIN_NAME)))

		/* requestUrl.equalsIgnoreCase("/avms") */) {

			/**
			 * If Requested URL is customer (/vmscustomer), then validate the
			 * url.
			 */
			UserDetailsDto userDetails = login
					.authenticateServerURL(requestUrl);
			/** validate the SLA period */
			if (login.validateSLAPeriod(userDetails.getCustomerSLA())
					.equalsIgnoreCase("ended")) {
				return mapping.findForward("slaperiodended");

			}
			if (userDetails.getDbName() != null) {
				userDetails
						.setHibernateCfgFileName("hibernate.customer.cfg.xml");
				userDetails.setUserType("customerOrVendor");
				userDetails.setSettings(login
						.getCustomerApplicationSettings(userDetails));
				session.setAttribute("userDetails", userDetails);
				session.setAttribute("userType", "customerOrVendor");
				return mapping.findForward("approvalloginpage");
			}
		}
		return mapping.findForward("incorrectURL");
	}

	/**
	 * Forward to Non prime vendor home page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward nonprimeVendorHomePage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		return mapping.findForward("nonprimevendor");

	}

	/**
	 * Forward to customer home page.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward customerHomePage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		return mapping.findForward("customerhome");

	}

	public ActionForward tire2HomePage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		String result = "tire2assessment";
		VendorContact currentVendor = (VendorContact) session
				.getAttribute("vendorUser");
		VendorMaster vendor = currentVendor.getVendorId();
		if (null != vendor.getVendorStatus()
				&& (vendor.getVendorStatus().equals(
						VendorStatus.NEWREGISTRATION.getIndex()) || vendor
						.getVendorStatus().equals(
								VendorStatus.PENDINGREVIEW.getIndex()))) {
			return mapping.findForward("nonprimevendor");
		} else if (vendor.getPrimeNonPrimeVendor() != null
				&& vendor.getPrimeNonPrimeVendor() == (byte) 0
				&& vendor.getIsApproved() == 1
				&& null != vendor.getDeverseSupplier()
				&& vendor.getDeverseSupplier() == (byte) 1) { // Diverse
																// Suppliers
			return mapping.findForward("nonprimevendor");
		} else if (null == vendor.getPrimeNonPrimeVendor()
				&& null != vendor.getVendorStatus()
				&& vendor.getVendorStatus().equals(
						VendorStatus.PENDINGREVIEW.getIndex())
				&& vendor.getIsApproved() == 0) {
			result = "tire2assessment";
		} else if (vendor.getIsApproved() == (byte) 1
				&& null != vendor.getPrimeNonPrimeVendor()
				&& vendor.getPrimeNonPrimeVendor() == (byte) 1
				&& null != vendor.getDeverseSupplier()
				&& (vendor.getDeverseSupplier() == (byte) 0 || vendor
						.getDeverseSupplier() == (byte) 1)) {
			result = "tire2home"; // Prime Diverse Supplier or Prime
									// Non-Diverse Supplier
		}
		return mapping.findForward(result);

	}

	public ActionForward adminHomePage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return mapping.findForward("adminHome");
	}

	/**
	 * Method which prevent submit on page refresh.
	 */
	@Override
	protected void saveToken(HttpServletRequest request) {
		// TODO Auto-generated method stub
		super.saveToken(request);
	}

	/**
	 * Method which set menu headers(Vendor,report etc) in homepage.
	 */
	private void setMenusInHomePage(List<RolePrivileges> privList,
			HttpSession session) {
		boolean haveVendorSection = false;
		boolean haveReportSection = false;
		boolean haveAdminSection = false;

		if (privList != null) {
			for (RolePrivileges priv : privList) {
				if (priv.getObjectId().getApplicationCategory() != null) {
					if (priv.getObjectId().getApplicationCategory()
							.equalsIgnoreCase("V")
							&& priv.getVisible() == (byte) 1) {
						haveVendorSection = true;
					}
					if (priv.getObjectId().getApplicationCategory()
							.equalsIgnoreCase("R")
							&& priv.getVisible() == (byte) 1) {
						if(priv.getObjectId().getObjectName().equalsIgnoreCase("Dashboard View") 
								|| priv.getObjectId().getObjectName().equalsIgnoreCase("Display Dashboard")) {
							//Nothing to do...
						}
						else {
							haveReportSection = true;
						}							
					}
					if (priv.getObjectId().getApplicationCategory()
							.equalsIgnoreCase("A")
							&& priv.getVisible() == (byte) 1) {
						haveAdminSection = true;
					}
				}
			}
		}
		session.setAttribute("haveVendorSection", haveVendorSection);
		session.setAttribute("haveReportSection", haveReportSection);
		session.setAttribute("haveAdminSection", haveAdminSection);

	}
}
