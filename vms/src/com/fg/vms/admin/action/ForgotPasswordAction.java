/**
 * ForgotPasswordAction.java
 */
package com.fg.vms.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.pojo.LoginForm;
import com.fg.vms.util.Constants;

/**
 * Send new Password to user email id when request the forgot password.
 * 
 * @author pirabu
 * 
 */
public class ForgotPasswordAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * send new password.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward sendNewPassword(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside the sendNewPassword method...");

		// LoginForm loginForm = (LoginForm) form;

		/** loginDaoImpl object used for access the login services. */
		LoginDao loginDaoImpl = new LoginDaoImpl();
		HttpSession session = request.getSession();

		String emailId = request.getParameter("userEmailId");
		Integer securityQuestion = Integer.parseInt(request
				.getParameter("securityQuestion"));
		String securityAnswer = request.getParameter("securityAnswer");

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String appRoot = getServlet().getServletContext().getRealPath("") + "/";

		LoginForm loginForm = new LoginForm();
		// loginForm.setUserLogin(loginId);
		loginForm.setUserEmailId(emailId);
		loginForm.setSecurityQuestion(securityQuestion.toString());
		loginForm.setSecurityAnswer(securityAnswer);

		String result = loginDaoImpl.sendNewPassword(loginForm, userDetails,
				appRoot);

		System.out.println("Result is " + result);
		if (result.equals("success")) {
			session.setAttribute("msg", "Password Sent successfully");
			return mapping.findForward("successForgotPage");
		} else if (result.equals("InvalidEmailID")) {
			session.setAttribute("msg", "Invalid email id");
			return mapping.findForward("successForgotPage");
		} else if (result.equals("InvalidSecurityQuestion")) {
			session.setAttribute("msg", "Security question is wrong");
			return mapping.findForward("successForgotPage");
		} else if (result.equalsIgnoreCase("InvalidSecurityAnswer")) {
			session.setAttribute("msg", "Security answer is wrong");
			return mapping.findForward("successForgotPage");
		} else if (result.equalsIgnoreCase("InvalidLoginId")) {
			session.setAttribute("msg", "Invalid login ID");
			return mapping.findForward("successForgotPage");
		} else if (result.equalsIgnoreCase("customerDivisionIdNotFound")) {
			session.setAttribute("msg", "Division ID is Missing, Contact Administrator.");
			return mapping.findForward("successForgotPage");
		} else if (result.equals("InvalidKeyvaluePassword")) {
			session.setAttribute("msg", "Invalid Email Id, Please Contact www.support.avmsystem.com");
			return mapping.findForward("successForgotPage");
		}
		return null;
	}

	public ActionForward resetPassword(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the resetPassword method...");

		LoginForm loginForm = (LoginForm) form;

		/** loginDaoImpl object used for access the login services. */
		LoginDao loginDaoImpl = new LoginDaoImpl();
		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String result = loginDaoImpl.resetPassword(loginForm, userDetails);

		System.out.println("Result is " + result);
		if (result.equals("success")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("password.reset.ok");
			messages.add("resetpassword", msg);
			saveMessages(request, messages);
			loginForm.reset(mapping, request);

			if (userDetails.getUserType() != null
					&& userDetails.getUserType().equalsIgnoreCase("FG")) {
				result = "loginpage";
			}
			// loginForm.reset(mapping, request);
			return mapping.findForward(result);
		} else if (result.equals("InvalidLoginId")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userLogin", new ActionMessage("err.loginid.invalid"));
			saveErrors(request, errors);
		} else if (result.equalsIgnoreCase("InvalidEmailID")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userEmailId", new ActionMessage("err.emailId.invalid"));
			saveErrors(request, errors);
		} else if (result.equals("InvalidSecurityQuestion")) {
			ActionErrors errors = new ActionErrors();
			errors.add("securityQuestion", new ActionMessage(
					"err.secQues.invalid"));
			saveErrors(request, errors);
		} else if (result.equalsIgnoreCase("InvalidSecurityAnswer")) {
			ActionErrors errors = new ActionErrors();
			errors.add("securityAnswer", new ActionMessage(
					"err.secAnsw.invalid"));
			saveErrors(request, errors);
		} else if ("OldPasswordInvalid".equalsIgnoreCase(result)) {
			ActionErrors errors = new ActionErrors();
			errors.add("oldPassword", new ActionMessage("err.oldpsw.invalid"));
			saveErrors(request, errors);
		} else if ("exist".equalsIgnoreCase(result)) {
			ActionErrors errors = new ActionErrors();
			errors.add("newPassword", new ActionMessage("err.newpsw.invalid"));
			saveErrors(request, errors);
		}
		if (!result.equals("success")) {
			return mapping.getInputForward();
		}
		return null;
	}

	/**
	 * redirect to forgot password screen.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 */
	public ActionForward forgotPasswordPage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("forgotpassword");
	}

	/**
	 * Save the self registered vendor into vendor users.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 */
	public ActionForward resetSelfVendorPassword(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Inside save vendor user using self registration.");

		LoginForm loginForm = (LoginForm) form;

		/** loginDaoImpl object used for access the login services. */
		LoginDao loginDaoImpl = new LoginDaoImpl();
		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String vendorType = null;
		if (null != request.getParameter("prime")
				&& !request.getParameter("prime").isEmpty()) {
			vendorType = request.getParameter("prime");
		}

		String result = loginDaoImpl.resetPasswordAndSaveVendorData(loginForm,
				userDetails);

		if (result.equals("success")) {

			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("password.reset.ok");
			messages.add("resetpassword", msg);
			saveMessages(request, messages);
			loginForm.reset(mapping, request);

			if (null != vendorType && !vendorType.isEmpty()) {
				return mapping.findForward("selfprimesuccess");
			} else {
				return mapping.findForward("selfsuccess");
			}

		} else if (result.equalsIgnoreCase("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userEmailId", new ActionMessage("err.emailId.invalid"));
			saveErrors(request, errors);
		} else if ("OldPasswordInvalid".equalsIgnoreCase(result)) {
			ActionErrors errors = new ActionErrors();
			errors.add("oldPassword", new ActionMessage("err.oldpsw.invalid"));
			saveErrors(request, errors);
		}
		if (!result.equals("success")) {
			return mapping.getInputForward();
		}
		return null;
	}

}
