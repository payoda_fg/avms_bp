/*
 * UsersAction.java 
 */
package com.fg.vms.admin.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.fg.vms.admin.dao.RolePrvilegesDao;
import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dao.UsersDao;
import com.fg.vms.admin.dao.impl.RolePrivilegesDaoImpl;
import com.fg.vms.admin.dao.impl.UserRolesDaoImpl;
import com.fg.vms.admin.dao.impl.UsersDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.CustomerTimeZoneMaster;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.model.UserRoles;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.admin.pojo.UsersForm;
import com.fg.vms.customer.dao.CURDDao;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.CustomerDivisionDao;
import com.fg.vms.customer.dao.ReportDao;
import com.fg.vms.customer.dao.ReportDashboardDao;
import com.fg.vms.customer.dao.Tier2ReportingPeriodDao;
import com.fg.vms.customer.dao.WorkflowConfigurationDao;
import com.fg.vms.customer.dao.impl.CURDTemplateImpl;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.CustomerDivisionDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDaoImpl;
import com.fg.vms.customer.dao.impl.ReportDashboardDaoImpl;
import com.fg.vms.customer.dao.impl.Tier2ReportingPeriodDaoImpl;
import com.fg.vms.customer.dao.impl.WorkflowConfigurationDaoImpl;
import com.fg.vms.customer.dto.Tier2ReportDto;
import com.fg.vms.customer.model.CustomerApplicationSettings;
import com.fg.vms.customer.model.CustomerDivision;
import com.fg.vms.customer.model.CustomerStakeholdersRegistration;
import com.fg.vms.customer.model.CustomerUserDivision;
import com.fg.vms.customer.model.EmailDistributionListDetailModel;
import com.fg.vms.customer.model.PasswordHistory;
import com.fg.vms.customer.model.Tier2ReportingPeriod;
import com.fg.vms.customer.model.WorkflowConfiguration;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.Decrypt;
import com.fg.vms.util.Encrypt;
import com.fg.vms.util.FrequencyPeriod;
import com.fg.vms.util.PasswordUtil;
import com.fg.vms.util.SendEmail;

/**
 * Represents the user configurations (eg.. Creating new users, Modifying the
 * user details, deactivating the user accounts)
 * 
 * @author vinoth
 * 
 */
public class UsersAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Creation of new customer in VMS application.
	 * 
	 * This method always initialize the list of users from database.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward create(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Inside the create user method");

		HttpSession session = request.getSession();
		// Get the current user id from session

		Integer userId = (Integer) session.getAttribute("userId");
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");

		UsersForm userForm = (UsersForm) form;
		UsersDaoImpl usersDaoImpl = new UsersDaoImpl();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		/*Removed BP Global Division while selecting check all in  customerDivisionIds.*/
		String globalDivisionId = "";
		if(session.getAttribute("isGlobalDivisionId") != null)
			globalDivisionId =  String.valueOf(session.getAttribute("isGlobalDivisionId"));
		
		if(userForm.getCustomerDivisionId() != null && userForm.getCustomerDivisionId().length >1)
		{
			String [] divisionIds = userForm.getCustomerDivisionId();
			List<String> copyDivisionIds = new ArrayList<String>();
			for(int i=0; i<divisionIds.length; i++)
			{
				if(!divisionIds[i].equalsIgnoreCase(globalDivisionId)){
					copyDivisionIds.add(divisionIds[i]);
				}
			}
			userForm.setCustomerDivisionId(copyDivisionIds.toArray(new String[0]));
		}

		/* New user creation */
		String result = usersDaoImpl.createUser(userForm, userId, userDetails);
		List<Users> users = null;

		/* For check unique user login and email id. */
		if (result.equals("both")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userLogin", new ActionMessage("userLogin.unique"));
			errors.add("userEmailId", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		/* For check unique user login. */
		if (result.equals("userLogin")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userLogin", new ActionMessage("userLogin.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		/* For check unique email id. */
		if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userEmailId", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		/* For check unique database name. */
		if (result.equals("databaseName")) {
			ActionErrors errors = new ActionErrors();
			errors.add("databaseName", new ActionMessage("databaseName.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		/* For show the successful user registration message. */
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("user.ok");
			messages.add("user", msg);
			saveMessages(request, messages);
			userForm.reset(mapping, request);
		}
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "User")) {
			/*
			 * Get the list of users available in database based on current user
			 * role, if the user is admin role then display all the active and
			 * inactive users, else show only active users.
			 */
			if ((session.getAttribute("adminRoleName") != null && session
					.getAttribute("currentUser") != null)
					&& session
							.getAttribute("adminRoleName")
							.toString()
							.equalsIgnoreCase(
									((Users) session
											.getAttribute("currentUser"))
											.getUserRoleId().getRoleName())) {
				/* Get the list of users available in database */
				users = usersDaoImpl.listUsers(userDetails, true);

			} else {
				/* Get the list of active users available in database */
				users = usersDaoImpl.listUsers(userDetails, false);
			}
		}
		session.setAttribute("users", users);

		logger.info("Forward to show the list of users page");
		return mapping.findForward(result);
	}

	/**
	 * View the list of active users
	 * 
	 * <p>
	 * This method loads the list of active users
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewUsers(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method to view the list of active users..");

		List<Users> users = null;
		List<CustomerTimeZoneMaster> zoneMasters = null;
		List<CustomerDivision> customerDivisions = null;
		HttpSession session = request.getSession();
		UsersDao usersDao = new UsersDaoImpl();

		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		zoneMasters = usersDao.listTimeZones(userDetails);
		session.setAttribute("zoneMasters", zoneMasters);
		// to get the customer divisions
		//if Both Division(Division name="BOTH") is not needed in UI then pass true otherwise  pass false;
		customerDivisions = usersDao.listCustomerDivisons(userDetails, false);
		session.setAttribute("customerDivisions", customerDivisions);
		
		CustomerDivision globalDivision = usersDao.getGlobalDivisionId(userDetails);
		if(globalDivision != null && globalDivision.getIsGlobal() != 0)
		{
			session.setAttribute("isGlobalDivisionId",globalDivision.getId());
		}
		else
			session.removeAttribute("isGlobalDivisionId");
		
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "User")) {
			/*
			 * Get the list of users available in database based on current user
			 * role.
			 */
			if ((session.getAttribute("adminRoleName") != null && session
					.getAttribute("currentUser") != null)
					&& session
							.getAttribute("adminRoleName")
							.toString()
							.equalsIgnoreCase(
									((Users) session
											.getAttribute("currentUser"))
											.getUserRoleId().getRoleName())) {
				/* Get the list of users available in database */
				users = usersDao.listUsers(userDetails, true);

			} else {
				/* Get the list of active users available in database */
				users = usersDao.listUsers(userDetails, false);
			}
			session.setAttribute("users", users);

		}
		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		// check whether the current user have the permission to add.
		if (CommonUtils.isAdd(privileges, "User")) {
			/* To get the Roles of the user */
			List<UserRolesForm> userRolesForms = userRolesDaoImpl
					.viewRoles(userDetails);
			session.setAttribute("userRoles", userRolesForms);
		}
		logger.info("Forward to show the list of users page");
		return mapping.findForward("success");
	}

	/**
	 * Deactivate/Remove the user account
	 * 
	 * <p>
	 * This method is used to delete the user from database
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Method starts to delete the user..");

		HttpSession session = request.getSession();
		UsersDaoImpl usersDaoImpl = new UsersDaoImpl();

		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer currentUserId = (Integer) session.getAttribute("userId");
		String result = "";
		// check whether the current user have the permission to delete.
		if (CommonUtils.isDelete(privileges, "User")) {
			/* Delete the user from database. */
			result = usersDaoImpl.deleteUser(
					Integer.parseInt(request.getParameter("id")), userDetails, currentUserId);
		}
		List<Users> users = null;
		if (result.equals("success")) {
			// check whether the current user have the permission to view.
			if (CommonUtils.isView(privileges, "User")) {
				/*
				 * Get the list of users available in database based on current
				 * user role.
				 */
				if ((session.getAttribute("adminRoleName") != null && session
						.getAttribute("currentUser") != null)
						&& session
								.getAttribute("adminRoleName")
								.toString()
								.equalsIgnoreCase(
										((Users) session
												.getAttribute("currentUser"))
												.getUserRoleId().getRoleName())) {
					/* Get the list of users available in database */
					users = usersDaoImpl.listUsers(userDetails, true);

				} else {
					/* Get the list of active users available in database */
					users = usersDaoImpl.listUsers(userDetails, false);
				}
				session.setAttribute("users", users);
			}

			/* For show user deletion successful message. */
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("delete.user");
			messages.add("deleteUser", msg);
			saveMessages(request, messages);

		}

		logger.info("Forwards to the available list of users page");
		return mapping.findForward(result);
	}

	/**
	 * Retrieve the selected user details
	 * 
	 * <p>
	 * This method is used to retrieve/view the selected user details
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrive(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		UsersForm usersForm = (UsersForm) form;
		UsersDao usersDaoImpl = new UsersDaoImpl();
		HttpSession session = request.getSession();
		List<CustomerTimeZoneMaster> zoneMasters = null;
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		CustomerDivisionDao dao = new CustomerDivisionDaoImpl();
		List<CustomerDivision> customerDivisions = null;
		customerDivisions = dao.retrieveDivisionAll(userDetails);
		session.setAttribute("customerDivisions", customerDivisions);
		String option=request.getParameter("var");
		boolean isEditUser = false;
		if(option != null && request.getParameter("var").equalsIgnoreCase("userEdit"))
		{
			isEditUser = true;
			session.setAttribute("EditUsersInfo", "EditUsersInfo");
		}
		else
		{
			session.removeAttribute("EditUsersInfo");
		}
		
		CustomerDivision globalDivision = usersDaoImpl.getGlobalDivisionId(userDetails);
		if(globalDivision != null && globalDivision.getIsGlobal() != 0)
		{
			session.setAttribute("isGlobalDivisionId",globalDivision.getId());
		}
		else
		{
			session.removeAttribute("isGlobalDivisionId");
		}
		
		// check whether the current user have the permission to modify.
		if (CommonUtils.isModify(privileges, "User") || request.getParameter("var").equalsIgnoreCase("userEdit")) {
			// Get the user id from request
			Integer id = Integer
					.parseInt(request.getParameter("id").toString());

			/* Retrieve the particular user details. */
			Users user = usersDaoImpl.retriveUser(id, userDetails);

			/* Set the values to respective fields. */
			if (user != null) {
				usersForm.setId(user.getId());
				usersForm.setUserName(user.getUserName());
				// usersForm.setUserLogin(user.getUserLogin());

				// usersForm.setHiddenUserLogin(user.getUserLogin());
				usersForm.setUserEmailId(user.getUserEmailId());
				usersForm.setHiddenEmailId(user.getUserEmailId());
				usersForm.setRoleId(user.getUserRoleId().getId());				
				usersForm.setActive(user.getIsActive().toString());
				usersForm.setSecretQuestionId(user.getSecretQuestionId()
						.getId());
				usersForm.setSecQueAns(user.getSecQueAns());
				usersForm.setDepartment(user.getDepartment());
				usersForm.setDivision(user.getDivision());
				usersForm.setPhoneNumber(user.getPhoneNumber());
				usersForm.setExtension(user.getExtension());
				usersForm.setFirstName(user.getFirstName());
				usersForm.setLastName(user.getLastName());
				usersForm.setTitle(user.getTitle());
				if (user.getTimeZone() != null) {
					usersForm.setTimeZone(user.getTimeZone().getId());
				}
				//Changed customerDivisionId to list of CustomerDivisionIds
				/*if (user.getCustomerDivisionId() != null) {
					usersForm.setCustomerDivision(user.getCustomerDivisionId()
							.getId());
				}*/

				/* Decrypt password using key value. */
				Integer keyvalue = user.getKeyvalue();
				Decrypt decrypt = new Decrypt();
				String decryptedPassword = decrypt.decryptText(
						String.valueOf(keyvalue.toString()),
						user.getUserPassword());
				usersForm.setUserPassword(decryptedPassword);
				usersForm.setConfirmPassword(decryptedPassword);
				
				//To set user customer division id's into userForm.
				List<CustomerUserDivision> userDivisionList = usersDaoImpl.listUserDivisionsByUser(userDetails, user.getId());
				if(userDivisionList != null){
					String [] customerDivisionIds = new String[userDivisionList.size()];
					for(int i=0; i<userDivisionList.size(); i++){
						customerDivisionIds[i]=String.valueOf(userDivisionList.get(i).getCustomerDivisionId().getId());
					}
					usersForm.setCustomerDivisionId(customerDivisionIds);
					if(isEditUser)
						session.setAttribute("hiddenDivisionIds", customerDivisionIds);
				} 
			}
			/* To get the Roles of the user */
			RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
			List<UserRolesForm> userRolesForms = userRolesDaoImpl
					.viewRoles(userDetails);
			session.setAttribute("userRoles", userRolesForms);
		}
		List<Users> users = null;
		zoneMasters = usersDaoImpl.listTimeZones(userDetails);
		session.setAttribute("zoneMasters", zoneMasters);
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "User")) {
			/*
			 * Get the list of users available in database based on current user
			 * role.
			 */
			if ((session.getAttribute("adminRoleName") != null && session
					.getAttribute("currentUser") != null)
					&& session
							.getAttribute("adminRoleName")
							.toString()
							.equalsIgnoreCase(
									((Users) session
											.getAttribute("currentUser"))
											.getUserRoleId().getRoleName())) {
				/* Get the list of users available in database */
				users = usersDaoImpl.listUsers(userDetails, true);

			} else {
				/* Get the list of active users available in database */
				users = usersDaoImpl.listUsers(userDetails, false);
			}
			session.setAttribute("users", users);
		}
		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		if (CommonUtils.isAdd(privileges, "User")
				|| CommonUtils.isModify(privileges, "User")) {
			/* To get the Roles of the user */
			List<UserRolesForm> userRolesForms = userRolesDaoImpl
					.viewRoles(userDetails);
			session.setAttribute("userRoles", userRolesForms);
		}
		logger.info(" Forward to the available list of users page");
		return mapping.findForward("modify");
	}

	/**
	 * Update the selected user details
	 * 
	 * <p>
	 * This method is used to modify/update the selected user details
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info(" Method starts update the retrieved user details..");

		HttpSession session = request.getSession();
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		// Get the user id from request
		Integer currentUserId = (Integer) session.getAttribute("userId");
		UsersDao usersDaoImpl = new UsersDaoImpl();

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		UsersForm userForm = (UsersForm) form;
		
		/*Removed BP Global Division while selecting check all in  customerDivisionIds.*/
		String globalDivisionId = "";
		if(session.getAttribute("isGlobalDivisionId") != null)
			globalDivisionId =  String.valueOf(session.getAttribute("isGlobalDivisionId"));
		
		if(session.getAttribute("hiddenDivisionIds") != null && userForm.getCustomerDivisionId() == null  )
		{
			String [] hiddenDivisions =(String[]) session.getAttribute("hiddenDivisionIds");
			userForm.setCustomerDivisionId(hiddenDivisions);
		}	
		if(userForm.getCustomerDivisionId() != null && userForm.getCustomerDivisionId().length >1)
		{
			String [] divisionIds = userForm.getCustomerDivisionId();
			List<String> copyDivisionIds = new ArrayList<String>();
			for(int i=0; i<divisionIds.length; i++)
			{
				if(!divisionIds[i].equalsIgnoreCase(globalDivisionId)){
					copyDivisionIds.add(divisionIds[i]);
				}
			}
			userForm.setCustomerDivisionId(copyDivisionIds.toArray(new String[0]));
		}
		/* Updating the user detail. */
		String result = usersDaoImpl.updateUser(userForm, userForm.getId(),
				currentUserId, userDetails);

		List<Users> users = null;
		if (result.equals("success")) {

			// check whether the current user have the permission to view.
			if (CommonUtils.isView(privileges, "User")) {
				/*
				 * Get the list of users available in database based on current
				 * user role.
				 */
				if ((session.getAttribute("adminRoleName") != null && session
						.getAttribute("currentUser") != null)
						&& session
								.getAttribute("adminRoleName")
								.toString()
								.equalsIgnoreCase(
										((Users) session
												.getAttribute("currentUser"))
												.getUserRoleId().getRoleName())) {
					/* Get the list of users available in database */
					users = usersDaoImpl.listUsers(userDetails, true);

				} else {
					/* Get the list of active users available in database */
					users = usersDaoImpl.listUsers(userDetails, false);
				}

				session.setAttribute("users", users);
			}
		//Changed customerDivisionId to list of CustomerDivisionIds	
			List<CustomerUserDivision> userDivisionList = usersDaoImpl.listUserDivisionsByUser(userDetails, userForm.getId());
			
			if(userDivisionList != null){
				String [] customerDivisionIds = new String[userDivisionList.size()];
				for(int i=0; i<userDivisionList.size(); i++){
					customerDivisionIds[i]=String.valueOf(userDivisionList.get(i).getCustomerDivisionId().getId());
				}
				userDetails.setCustomerDivisionIds(customerDivisionIds);
				
				if(userDivisionList.size() == 1)
				{
					if(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal()!= null && userDivisionList.get(0).getCustomerDivisionId().getIsGlobal() != 0)
						userDetails.setIsGlobalDivision(Integer.valueOf(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal()));
					else
						userDetails.setIsGlobalDivision(0);
				}
				else
					userDetails.setIsGlobalDivision(0);
			}
			else
			{
				userDetails.setCustomerDivisionIds(null);
				userDetails.setIsGlobalDivision(0);
			}
			userForm.reset(mapping, request);
			session.removeAttribute("user");

			/* For show the user update success message. */
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("update.user");
			messages.add("updateUser", msg);
			saveMessages(request, messages);
			session.removeAttribute("hiddenDivisionIds");
		} else if (result.equals("failure")) {

			/* For show the user update failure message. */
			ActionErrors errors = new ActionErrors();
			errors.add("error", new ActionMessage("error.failure"));
			saveErrors(request, errors);

			/* Forward to the error page. */
			return mapping.getInputForward();
		} else if (result.equals("exist")) {
			/* For show the user update failure message. */
			ActionErrors errors = new ActionErrors();
			errors.add("userPassword", new ActionMessage("error.exist"));
			saveErrors(request, errors);

			/* Forward to the error page. */
			return mapping.getInputForward();
		}
		/* For check unique email id. */
		else if (result.equals("userEmail")) {
			ActionErrors errors = new ActionErrors();
			errors.add("userEmailId", new ActionMessage("userEmailId.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		logger.info(" Forward to the available list of users page");
		return mapping.findForward(result);
	}

	/**
	 * Authenticate the email id and password if its valid then view the
	 * customer user. Suppose the user already complete profile then login to
	 * application.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public ActionForward viewCustomerUser(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		UsersForm usersForm = (UsersForm) form;
		UsersDao usersDaoImpl = new UsersDaoImpl();
		HttpSession session = request.getSession();
		Decrypt decrypt = new Decrypt();
		
		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");

		Users user = usersDaoImpl.authenticate(usersForm.getUserEmailId(), userDetails);		
		if (user != null)
		{
			CustomerApplicationSettings appSettings = (CustomerApplicationSettings) session.getAttribute("isDivisionStatus");
			if (appSettings.getIsDivision() != 0 && appSettings.getIsDivision() != null)
			{
				List<CustomerUserDivision> userDivisionList = usersDaoImpl.listUserDivisionsByUser(userDetails, user.getId());
				
				if(userDivisionList != null)
				{
					String [] customerDivisionIds = new String[userDivisionList.size()];
					for(int i=0; i<userDivisionList.size(); i++)
					{
						customerDivisionIds[i]=String.valueOf(userDivisionList.get(i).getCustomerDivisionId().getId());
					}
					userDetails.setCustomerDivisionIds(customerDivisionIds);
					
					if(userDivisionList.size() == 1)
					{
						if(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal() != null && userDivisionList.get(0).getCustomerDivisionId().getIsGlobal() != 0)
							userDetails.setIsGlobalDivision(Integer.valueOf(userDivisionList.get(0).getCustomerDivisionId().getIsGlobal()));
						else
							userDetails.setIsGlobalDivision(0);
					}
					else
						userDetails.setIsGlobalDivision(0);
				}				
				else
				{
					ActionErrors errors = new ActionErrors();
					errors.add("login", new ActionMessage("err.noDivision"));
					saveErrors(request, errors);
					return mapping.getInputForward();
				}
			}
			
			// Decrypt the password to authenticate user.
			Integer keyvalue = user.getKeyvalue();

			String decryptedPassword = decrypt.decryptText(String.valueOf(keyvalue.toString()), user.getUserPassword());
			if (null != decryptedPassword)
			{
				/**
				 * Validate password from login page and decryptedPassword from
				 * database, then forward to fill the user information.
				 */
				if (usersForm.getUserPassword().equals(decryptedPassword))
				{
					if (null != user.getIsActive() && user.getIsActive().equals((byte) 1))
					{
						// reset password 90 days once.
						CURDDao<PasswordHistory> curdDao = new CURDTemplateImpl<PasswordHistory>();
						PasswordHistory history = curdDao.find(userDetails, "from PasswordHistory where customerUserID="
										+ user.getId() + " order by changedon desc limit 1");

						if (history != null && history.getChangedon() != null)
						{
							Date past = history.getChangedon(); // June 20th, 2010
							Date today = new Date(); // July 24th
							int days = Days.daysBetween(new DateTime(past), new DateTime(today)).getDays();
							if (days > 90)
								return mapping.findForward("resetPassword");
						}
						
						session.setAttribute("userId", user.getId());
						session.setAttribute("currentUser", user);
						RolePrvilegesDao rolePrvilegesDao = new RolePrivilegesDaoImpl();
						List<RolePrivileges> privileges;
						privileges = rolePrvilegesDao.retrievePrivilegesByRole(user.getUserRoleId().getId(), userDetails);
						//To hide/show menu based on sub-menus
						setMenusInHomePage(privileges,session);
						session.setAttribute("privileges", privileges);
						
						//Setting Workflow Configuration in the Session to Access in the Whole Application
						WorkflowConfigurationDaoImpl configurationDaoImpl = new WorkflowConfigurationDaoImpl();
						WorkflowConfiguration workflowConfigurations = configurationDaoImpl.listWorkflowConfig(userDetails);
						userDetails.setWorkflowConfiguration(workflowConfigurations);
						
						/**
						 * If the user is customer user, then forward to
						 * customer home page.
						 */
						if (!(userDetails.getUserType() != null && userDetails.getUserType().equalsIgnoreCase("FG")))
						{
							ReportDao reportDao = new ReportDaoImpl();
							ReportDashboardDao dashboardDao = new ReportDashboardDaoImpl();
							Tier2ReportingPeriodDao periodDao = new Tier2ReportingPeriodDaoImpl();
							Tier2ReportingPeriod reportingPeriod = periodDao.currentReportingPeriod(userDetails);

							int reportStartMonth = 0;
							int reportEndMonth = 3 - 1;
							Integer year = Calendar.getInstance().get(Calendar.YEAR);
							String currentQuarter = "";
							if (reportingPeriod != null)
							{
								Calendar c = Calendar.getInstance();
								SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
								
								switch (FrequencyPeriod.valueOf(reportingPeriod.getDataUploadFreq()))
								{
									case M:
										c.set(year, c.get(Calendar.MONTH) - 1, 1); // ------>
										c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
	
										StringBuilder period = new StringBuilder();
										period.append(sdf.format(c.getTime()));
										c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
										period.append(" - " + sdf.format(c.getTime()));
										currentQuarter = period.toString();
										break;
										
									case Q:
										List<String> reportPeriod = CommonUtils.calculateReportingPeriods(reportingPeriod, year);
										int index = CommonUtils.findCurrentQuarter(reportingPeriod);
										//For Getting Previous Quarter of Reporting Period 
										if(0 == index)
										{
											//For Previous Year Last Quarter
											reportPeriod = CommonUtils.calculateReportingPeriods(reportingPeriod, year-1);
											currentQuarter = reportPeriod.get(3);
										}
										else
										{
											currentQuarter = reportPeriod.get(index-1);
										}
										break;
										
									case A:
										reportStartMonth = reportingPeriod.getStartDate().getMonth();
										reportEndMonth = reportingPeriod.getEndDate().getMonth();
	
										c.set(year-1, reportStartMonth, 1); // ------>
										c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
										
										StringBuilder period1 = new StringBuilder();
										period1.append(sdf.format(c.getTime()));
										c.set(year-1, reportEndMonth, 1); // ------>
										c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
										period1.append(" - " + sdf.format(c.getTime()));
										currentQuarter = period1.toString();
										break;
								}
							}
							
							boolean hasDashBoard = true;
							if (privileges != null)
							{
								for (RolePrivileges rolePriv : privileges)
								{
									if (rolePriv.getObjectId().getObjectName().equalsIgnoreCase("Dashboard View") && rolePriv.getView() != 1)
									{
										/*hasDashBoard = false;*/
										/*Nothing To Do*/
									}
									if (rolePriv.getObjectId().getObjectName().equalsIgnoreCase("Display Dashboard") && rolePriv.getView() != 1)
									{
										hasDashBoard = false;
									}
								}
							}
							
							if(null != currentQuarter && !currentQuarter.isEmpty() )
							{
								session.setAttribute("currentQuarter", currentQuarter);
							}
							
							int userRoleId=0;
							if(null != user.getUserRoleId() && null != user.getUserRoleId().getId())
							{
								userRoleId=user.getUserRoleId().getId();
							}

							//List<DashboardSettings> dashboardSettings = reportDao.listDashboardSettings(userDetails);
							
							// Start User Role Based Dash Board Displaying
							CURDDao<RolePrivileges> curdDao1 = new CURDTemplateImpl<RolePrivileges>();
							List<RolePrivileges> rolePrivilege = curdDao1.list(userDetails, "from RolePrivileges r where r.view=1 and r.roleId=" + userRoleId
											+ " and r.objectId.applicationCategory='D' and r.objectId.objectName not like '%Dashboard View%' "
											+ " and r.objectId.objectName not like '%Display Dashboard%'");
							
							if(rolePrivilege.size()>0 && rolePrivilege!=null && hasDashBoard == true)
							{
								Iterator itr=rolePrivilege.iterator();
								RolePrivileges privilege=new RolePrivileges();
								while(itr.hasNext())
								{
									privilege = (RolePrivileges)itr.next();
									if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Diversity Analysis Dashboard"))
									{
										session.setAttribute("diversityview", "1");
										List<Tier2ReportDto> diversityDashboard = null;
										diversityDashboard = reportDao.getDiversityDashboardReport(userDetails);
										session.setAttribute("diversityDashboard", diversityDashboard);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendor Status Breakdown Dashboard"))
									{
										session.setAttribute("ststusdashboardvalue", "1");
										List<Tier2ReportDto> statusDashboard = null;
										statusDashboard = reportDao.getStatusDashboardReport(userDetails);
										session.setAttribute("statusDashboard", statusDashboard);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Supplier Count By State Dashboard"))
									{
										session.setAttribute("supplierdashboard", "1");
										List<Tier2ReportDto> supplierStateDashboard = null;
										supplierStateDashboard = reportDao.getSupplierStateDashboardReport(userDetails);
										session.setAttribute("supplierStateDashboard", supplierStateDashboard);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendor Count By State Dashboard"))
									{
										session.setAttribute("bpVendorsCountDashboard", "1");
										List<Tier2ReportDto> bpVendorsCount = null;
										bpVendorsCount = reportDao.getBpVendorCountDashboardReport(userDetails);
										session.setAttribute("bpVendorsCount", bpVendorsCount);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Spend By Agency Dashboard"))
									{
										session.setAttribute("agencydashboardGV", "1");
										List<Tier2ReportDto> agencyDashboard = null;
										agencyDashboard = reportDao.getAgencyDashboardReport(userDetails);
										session.setAttribute("agencyDashboard", agencyDashboard);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Total Spend Dashboard"))
									{
										session.setAttribute("totalSpendDashboard", "1");
										List<Tier2ReportDto> totalSalesDB = null;
										totalSalesDB = dashboardDao.getTotalSpendReport(userDetails, year.toString(), currentQuarter);
										session.setAttribute("totalSalesDB", totalSalesDB);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Direct Spend Dashboard"))
									{
										session.setAttribute("directDashboard", "1");
										List<Tier2ReportDto> directSpendDB = null;
										directSpendDB = dashboardDao.getDirectSpendReport(userDetails, year.toString(), currentQuarter);
										session.setAttribute("directSpendDBG", directSpendDB);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Prime Supplier Report by Tier 2 Indirect Spend Dashboard"))
									{
										session.setAttribute("indirectDashboard", "1");
										List<Tier2ReportDto> indirectSpendDB = null;
										indirectSpendDB = dashboardDao.getIndirectSpendReport(userDetails, year.toString(), currentQuarter);
										session.setAttribute("indirectSpendDBG", indirectSpendDB);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Tier 2 Vendor Name Report Dashboard"))
									{
										session.setAttribute("diversitydashboard", "1");
										List<Tier2ReportDto> diversityDBG = null;
										diversityDBG = dashboardDao.getDiversityReport(userDetails, year.toString(), currentQuarter);
										session.setAttribute("diversityDBG", diversityDBG);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Tier 2 Reporting Summary Dashboard"))
									{
										session.setAttribute("ethnicityAnalysis", "1");
										List<Tier2ReportDto> ethnicityDBG = null;
										ethnicityDBG = dashboardDao.getEthnicityReport(userDetails, year.toString(), currentQuarter);
										session.setAttribute("ethnicityDBG", ethnicityDBG);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("New Registrations By Month Dashboard"))
									{
										session.setAttribute("newRegistrationsByMonthDashboard", "1");
										List<Tier2ReportDto> newRegistrationsByMonth = null;
										newRegistrationsByMonth = dashboardDao.getNewRegistrationsByMonthDashboardReport(userDetails);
										session.setAttribute("newRegistrationsByMonth", newRegistrationsByMonth);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendors By Business Type"))
									{
										session.setAttribute("vendorsByIndustryDashboard", "1");
										List<Tier2ReportDto> vendorsByIndustry = null;
										vendorsByIndustry = dashboardDao.getVendorsByIndustryDashboardReport(userDetails);
										session.setAttribute("vendorsByIndustry", vendorsByIndustry);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendors By Business Type"))
									{
										session.setAttribute("bpVendorsByIndustryDashboard", "1");
										List<Tier2ReportDto> bpVendorsByIndustry = null;
										bpVendorsByIndustry = dashboardDao.getBPVendorsByIndustryDashboardReport(userDetails);
										session.setAttribute("bpVendorsByIndustry", bpVendorsByIndustry);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("Vendors By NAICS Description"))
									{
										session.setAttribute("vendorsByNaicsDescriptionDashboard", "1");
										List<Tier2ReportDto> vendorsByNaicsDescription = null;
										vendorsByNaicsDescription = dashboardDao.getVendorsByNAICSDecriptionDashboardReport(userDetails);
										session.setAttribute("vendorsByNaicsDescription", vendorsByNaicsDescription);
									}
									else if(privilege.getObjectId().getObjectName().equalsIgnoreCase("BP Vendors By BP Market Sector"))
									{
										session.setAttribute("vendorsByBPMarketSectorDashboard", "1");
										List<Tier2ReportDto> vendorsByBPMarketSector = null;
										vendorsByBPMarketSector = dashboardDao.getVendorsByBPMarketSectorDashboardReport(userDetails);
										session.setAttribute("vendorsByBPMarketSector", vendorsByBPMarketSector);
									}
								}
								return mapping.findForward("customerhome");
							}
							else
							{
								return mapping.findForward("noDashBoard");						
							}
							// End User Role Based Dash Board Displaying
						}
						return mapping.findForward("customerhome");
					}
					else
					{
						/* Set the values to respective fields. */
						usersForm.setId(user.getId());
						if (null != user.getUserName() && !user.getUserName().isEmpty() && user.getUserName().equals("-1"))
						{
							usersForm.setUserName("");
						}
						else
						{
							usersForm.setUserName(user.getUserName());
						}
						usersForm.setUserEmailId(user.getUserEmailId());
						usersForm.setHiddenEmailId(user.getUserEmailId());
						if (user.getUserRoleId() != null)
						{
							usersForm.setRoleId(user.getUserRoleId().getId());
							usersForm.setStackHolder(user.getUserRoleId().getId());
						}
						// usersForm.setActive(user.getIsActive().toString());
						if (user.getSecretQuestionId() != null)
						{
							usersForm.setSecretQuestionId(user.getSecretQuestionId().getId());
						}
						usersForm.setSecQueAns(user.getSecQueAns());
						usersForm.setDepartment(user.getDepartment());
						usersForm.setDivision(user.getDivision());
						usersForm.setPhoneNumber(user.getPhoneNumber());
						usersForm.setExtension(user.getExtension());
						usersForm.setFirstName(user.getFirstName());
						usersForm.setLastName(user.getLastName());
						usersForm.setTitle(user.getTitle());
						if (user.getTimeZone() != null)
						{
							usersForm.setTimeZone(user.getTimeZone().getId());
						}
						
						/* Decrypt password using key value. */
						usersForm.setUserPassword(decryptedPassword);
						usersForm.setConfirmPassword(decryptedPassword);
						
						/* To get the Roles of the user */
						RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
						List<UserRolesForm> userRolesForms = userRolesDaoImpl.viewRoles(userDetails);
						session.setAttribute("userRoles", userRolesForms);
						return mapping.findForward("success");
					}
				}
				else
				{
					ActionErrors errors = new ActionErrors();
					errors.add("login", new ActionMessage("error.password.invalid"));
					saveErrors(request, errors);
					return mapping.getInputForward();
				}
			}
			else
			{
				ActionErrors errors = new ActionErrors();
				errors.add("login", new ActionMessage("error.password.invalid"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
		}
		else
		{
			ActionErrors errors = new ActionErrors();
			errors.add("login", new ActionMessage("error.usernamepassword.invalid"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}
	}

	/**
	 * save the customer user details.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveCustomerUser(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info(" Method starts update the retrieved user details..");

		HttpSession session = request.getSession();
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		// Get the user id from request
		Integer currentUserId = (Integer) session.getAttribute("userId");
		UsersDaoImpl usersDaoImpl = new UsersDaoImpl();

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		UsersForm userForm = (UsersForm) form;
		userForm.setActive("1");
		/* Updating the user detail. */
		String result = usersDaoImpl.updateUser(userForm, userForm.getId(),
				currentUserId, userDetails);

		if (result.equals("success")) {
			WorkflowConfigurationDao dao = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration configuration = dao
					.listWorkflowConfig(userDetails);
			if (null != configuration.getAdminGroup()) {
				CURDDao<EmailDistributionListDetailModel> curdDao = new CURDTemplateImpl<EmailDistributionListDetailModel>();
				List<EmailDistributionListDetailModel> detailModels = curdDao
						.list(userDetails,
								" from EmailDistributionListDetailModel where emailDistributionMasterId ="
										+ configuration.getAdminGroup()
												.getEmailDistributionMasterId());

				if (null != detailModels && !detailModels.isEmpty()) {

					new SendEmail().sendCustomerUserRegistrationAlert(
							userDetails, userForm,
							CommonUtils.packToEmailIds(detailModels),
							CommonUtils.packCCEmailIds(detailModels),
							CommonUtils.packBCCEmailIds(detailModels));
				}
			}

			userForm.reset(mapping, request);
			/* For show the user update success message. */
		} else if (result.equals("failure")) {

			/* For show the user update failure message. */
			ActionErrors errors = new ActionErrors();
			errors.add("error", new ActionMessage("error.failure"));
			saveErrors(request, errors);
			/* Forward to the error page. */
			return mapping.getInputForward();
		} else if (result.equals("exist")) {
			/* For show the user update failure message. */
			ActionErrors errors = new ActionErrors();
			errors.add("userPassword", new ActionMessage("error.exist"));
			saveErrors(request, errors);
			/* Forward to the error page. */
			return mapping.getInputForward();
		}

		logger.info(" Forward to the available list of users page");
		return mapping.findForward(result);
	}

	/**
	 * Send the login Credentials to customer user to complete the profile.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward sendLoginCredentials(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result = "";
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String psw = String.valueOf(PasswordUtil.generatePswd(8, 8, 1, 1, 0));
		String emailId = request.getParameter("emailId");
		String divisionId = request.getParameter("customerDivision");
		if (null != emailId && !emailId.isEmpty()) {
			CommonDao commonDao = new CommonDaoImpl();
			result = commonDao.checkDuplicateEmail(emailId, userDetails);
			if (null != result && !result.isEmpty() && "submit".equals(result)) {
				Users user = new Users();
				user.setUserEmailId(emailId);
				user.setCreatedOn(new Date());
				user.setCreatedBy(0);
				// This is system generated password but we set 0 because of
				// user may be reset the password when complete the profile.
				user.setIsSysGenPassword((byte) 0);
				user.setUserName("-1");
				user.setSecQueAns("green");
				user.setIsActive((byte) 0);				
				Random random = new java.util.Random();
				Integer randVal = random.nextInt();

				Encrypt encrypt = new Encrypt();

				/* convert password to encrypted password. */
				String encypPassword = encrypt.encryptText(
						Integer.toString(randVal) + "", psw);
				CURDDao<?> curdDao = new CURDTemplateImpl();
				UserRoles role = (UserRoles) curdDao.find(userDetails,
						"from UserRoles where roleName='BP Stakeholder'");
				SecretQuestion question = (SecretQuestion) curdDao.find(
						userDetails,
						"from SecretQuestion where secQuestionParticular='What is your favorite color?'");				
				user.setSecretQuestionId(question);
				user.setUserRoleId(role);				
				user.setUserPassword(encypPassword);
				user.setKeyvalue(randVal);
				
				//Checking Division Id
				if(!("undefined").equalsIgnoreCase(divisionId) && !divisionId.equals(null))
				{
					if(!("0").equals(divisionId))
					{
						CustomerDivision id = (CustomerDivision) curdDao.find(userDetails,
								"from CustomerDivision where id="+divisionId);
						user.setCustomerDivisionId(id);
					}
				}				
				UsersDao dao = new UsersDaoImpl();
				result = dao.save(user, userDetails);
			}
		} else {
			result = "failure";
		}

		if (result.equals("userEmail")) {
			session.setAttribute("registration", "This Email ID already exist.");
		} else if (result.equals("failure")) {
			session.setAttribute("registration", "Registration failed.");
		} else if (result.equals("success")) {
			SendEmail sendEmail = new SendEmail();
			String appRoot = getServlet().getServletContext().getRealPath("")
					+ "/";
			sendEmail.sendLoginCredentialToUser(emailId, "User Registration",
					psw, appRoot, userDetails);
			session.setAttribute(
					"registration",
					"Email sent successfully. Use Credentials from Email to enter additional particulars.");
		}
		return mapping.findForward("registration");
	}

	/**
	 * Fetch the list of users from database.
	 * 
	 * <p>
	 * This method is used to get the list of users from database.
	 * 
	 * @param users
	 * @return
	 */
	public List<UsersForm> packageUsers(List<Users> users) {

		List<UsersForm> listOfUsers = new ArrayList<UsersForm>();

		/* Iterates the list of user */
		for (Users user : users) {
			UsersForm usersForm = new UsersForm();
			usersForm.setId(user.getId());
			usersForm.setUserName(user.getUserName());
			usersForm.setUserEmailId(user.getUserEmailId());
			// usersForm.setUserLogin(user.getUserLogin());
			usersForm.setUserPassword(user.getUserPassword());
			usersForm.setRoleId(user.getUserRoleId().getId());
			usersForm.setUserRoles(user.getUserRoleId());
			listOfUsers.add(usersForm);
		}
		return listOfUsers;
	}
	
	/**
	 * Method which set menu headers(Vendor,report etc) in homepage.
	 */
	private void setMenusInHomePage(List<RolePrivileges> privList,HttpSession session)
	{
		boolean haveVendorSection=false;
		boolean haveReportSection=false;
		boolean haveAdminSection=false;
		
		if(privList !=null)
		{
			for(RolePrivileges priv:privList)
			{
				if(priv.getObjectId().getApplicationCategory() != null)
				{
					if(priv.getObjectId().getApplicationCategory().equalsIgnoreCase("V") && priv.getVisible()==(byte)1)
					{
						haveVendorSection=true;
					}
					if(priv.getObjectId().getApplicationCategory().equalsIgnoreCase("R") && priv.getVisible()==(byte)1)
					{
						if(priv.getObjectId().getObjectName().equalsIgnoreCase("Dashboard View") 
								|| priv.getObjectId().getObjectName().equalsIgnoreCase("Display Dashboard")) {
							//Nothing to do...
						}
						else {
							haveReportSection = true;
						}
					}
					if(priv.getObjectId().getApplicationCategory().equalsIgnoreCase("A") && priv.getVisible()==(byte)1)
					{
						haveAdminSection=true;
					}
				}
			}
		}
		
		session.setAttribute("haveVendorSection", haveVendorSection);
		session.setAttribute("haveReportSection", haveReportSection);
		session.setAttribute("haveAdminSection", haveAdminSection);
		
	}
	
	/**
	 * Send the login Credentials to customer user to complete the profile.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */	
	@SuppressWarnings("rawtypes")
	public ActionForward sendStakeHolderLoginCredentials(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		UsersDao userDao = new UsersDaoImpl();
		CommonDao commonDao = new CommonDaoImpl();
		String result = null;
		
		String emailId = request.getParameter("emailId");
		String domain = emailId.substring(emailId.indexOf('@'));
		
		UsersForm userForm = (UsersForm) form;
		userForm.setStackHolderEmailId(emailId);
		
		if (null != emailId && !emailId.isEmpty() && domain.equalsIgnoreCase("@BP.COM")) 
		{
			//Check the Email Address exist in User and VendorConatact tables
			result = commonDao.checkDuplicateEmail(emailId, userDetails);
			
			if (null != result && !result.isEmpty() && "submit".equals(result)) 
			{
				// Check the Email Address exist in Stake Holder table
				result = userDao.checkDuplicateEmailInStakeHolders(userDetails, emailId);
				
				if(null != result && !result.isEmpty() && "mailnotexisted".equalsIgnoreCase(result))
				{
					result = userDao.saveStakeHolder(userForm, userDetails);
				}
			}
		}
		
		if(result != null && result.equalsIgnoreCase("success"))
		{
			session.setAttribute("registration", "Registration submitted successfully, You will get login credentials email once approved by administrator.");
			WorkflowConfigurationDao dao = new WorkflowConfigurationDaoImpl();
			WorkflowConfiguration configuration = dao.listWorkflowConfig(userDetails);
			if (null != configuration.getAdminGroup()) 
			{
				CURDDao<EmailDistributionListDetailModel> curdDao = new CURDTemplateImpl<EmailDistributionListDetailModel>();
				List<EmailDistributionListDetailModel> detailModels = curdDao.list(userDetails,
								" from EmailDistributionListDetailModel where emailDistributionMasterId ="
										+ configuration.getAdminGroup().getEmailDistributionMasterId());

				if (null != detailModels && !detailModels.isEmpty()) 
				{
					String mailStatus = new SendEmail().sendAlertMailOnUserCreation(userDetails, userForm, CommonUtils.packToEmailIds(detailModels),
							CommonUtils.packCCEmailIds(detailModels), CommonUtils.packBCCEmailIds(detailModels));
					 
					 if(!(mailStatus.equalsIgnoreCase("success")))
							session.setAttribute("registration", "Registration submitted successfully, Email not send. please contact admin.");
				}
			}
			userForm.reset(mapping, request);
		}
		else if(result != null && result.equalsIgnoreCase("mailexisted"))
		{
			session.setAttribute("registration", "This email id is already available in the stakeholders list.");
		}
		else if(result != null && !result.equalsIgnoreCase("submit"))
		{
			session.setAttribute("registration", "This email id is already available in the system.");
		}
		return mapping.findForward("registration");
	}
	
	/**
	 * View the Not Approved Stake Holders
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward viewNotApprovedStakeholders(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		UsersDao usersDao = new UsersDaoImpl();
		
		List<CustomerStakeholdersRegistration> notApprovedList = usersDao.notApprovedStakeholders(userDetails);
		session.setAttribute("displayApproval", "no");
		session.setAttribute("notApprovedStakeHolders", notApprovedList);
		return mapping.findForward("viewNotApprovals");
	}
	
	/**
	 * Edit the Not Approved Stake Holders
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward editNotApprovedStakeHolders(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		UsersDao usersDao = new UsersDaoImpl();
		CustomerStakeholdersRegistration stakeHolder = null;
		UsersForm userForm = (UsersForm)form;
		
		Integer stakeHolderId = Integer.parseInt(request.getParameter("stakeHolderId"));		
		if(stakeHolderId != null && stakeHolderId != 0) {
			stakeHolder = usersDao.retriveStakeHolder(userDetails, stakeHolderId);
		}			
		
		if(stakeHolder != null)
		{
			userForm.setStackHolder(stakeHolder.getId());
			userForm.setStackHolderFirstName(stakeHolder.getFirstName());
			if(stakeHolder.getLastName() != null)
			  userForm.setStackHolderLastName(stakeHolder.getLastName());
			userForm.setStackHolderManagerName(stakeHolder.getManagerName());
			userForm.setStackHolderReason(stakeHolder.getReason());
			userForm.setStackHolderEmailId(stakeHolder.getEmailId());
			if(stakeHolder.getCustomerDivisionId() != null)
				userForm.setDivision(stakeHolder.getCustomerDivisionId().getDivisionname());
		}
		session.setAttribute("displayApproval", "yes");
		return mapping.findForward("viewNotApprovals");
	}
	
	/**
	 * Edit the Not Approved Stake Holders
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public ActionForward updateApproveOrUnapproveStakeHolder(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{		
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session.getAttribute("userDetails");
		Integer currentUserId = (Integer) session.getAttribute("userId");
		
		UsersDao userDao = new UsersDaoImpl();
		UsersForm userForm = (UsersForm) form;
		String appRoot = getServlet().getServletContext().getRealPath("")+ "/";		
		
		String result = "failure";
		if(userForm != null)
		{
			if(userForm.getIsApprove() !=null && userForm.getStackHolder() != null)
			{
				result = userDao.stakeHolderApproved(userDetails, userForm.getStackHolder(), appRoot, currentUserId, userForm.getIsApprove());
			}
		}
		JSONObject jsonObject = new JSONObject();
		
		if(result.equalsIgnoreCase("success") && userForm.getIsApprove() !=null && userForm.getIsApprove().equalsIgnoreCase("Approved"))
		{
			jsonObject.put("result", "approvedsuccess");
		}
		else if(result.equalsIgnoreCase("templateNotFound"))
		{
			jsonObject.put("result", "templateNotFound");
		}
		else if(result.equalsIgnoreCase("success") && userForm.getIsApprove() !=null && userForm.getIsApprove().equalsIgnoreCase("UnApproved"))
		{
			jsonObject.put("result", "unapprovedsuccess");
		}
		else
		{
			jsonObject.put("result", "fail");
		}
		
		userForm.reset(mapping, request);
		response.addHeader("Content-Type", "application/json");
        response.getOutputStream().write(jsonObject.toString().getBytes("UTF-8"));
        response.getOutputStream().flush();
		return null;
	}
}
