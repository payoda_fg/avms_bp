/**
 * 
 */
package com.fg.vms.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dao.impl.UserRolesDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.UserRoles;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.util.CommonUtils;

/**
 * Maintain user roles
 * 
 * @author vinoth karthi
 * 
 */
public class UserRolesAction extends DispatchAction {

	private static final String SUCCESS = "success";

	/**
	 * Retrieve a list of roles in the user role table.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewUserRoles(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "Roles")) {
			List<UserRolesForm> userRolesForms = userRolesDaoImpl
					.viewRoles(userDetails);
			session.setAttribute("userRoles", userRolesForms);
		}
		return mapping.findForward(SUCCESS);
	}

	/**
	 * Delete a particular role.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		UserRolesDaoImpl userRolesDaoImpl = new UserRolesDaoImpl();
		HttpSession session = request.getSession();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");

		String result = "";
		// check whether the current user have the permission to delete.
		if (CommonUtils.isDelete(privileges, "Roles")) {
			result = userRolesDaoImpl.deleteRole(
					Integer.parseInt(request.getParameter("id")), userDetails);

		}
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "Roles")) {
			List<UserRolesForm> userRolesForms = null;
			userRolesForms = userRolesDaoImpl.viewRoles(userDetails);
			request.setAttribute("userRoles", userRolesForms);
		}

		if (result != null) {
			if (result.equals("deletesuccess")) {
				ActionMessages messages = new ActionMessages();
				ActionMessage msg = new ActionMessage("delete.userrole");
				messages.add("userrole", msg);
				saveMessages(request, messages);
			}
			if (result.equalsIgnoreCase("reference")) {
				ActionErrors errors = new ActionErrors();
				errors.add("referencerole", new ActionMessage("reference"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}

		} else {
			return mapping.getInputForward();
		}

		return mapping.findForward(result);
	}

	/**
	 * Add a new role to db.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward addUserRole(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		HttpSession session = request.getSession();
		UserRolesForm userRolesForm = (UserRolesForm) form;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		String result = userRolesDaoImpl.addRole(userRolesForm,
				Integer.parseInt(session.getAttribute("userId").toString()),
				userDetails);
		List<UserRolesForm> userRolesForms = null;
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "Roles")) {
			userRolesForms = userRolesDaoImpl.viewRoles(userDetails);
			request.setAttribute("userRoles", userRolesForms);
		}
		if (result.equals("success")) {
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("add.userrole");
			messages.add("userrole", msg);
			saveMessages(request, messages);
			userRolesForm.reset(mapping, request);
		} else if (result.equalsIgnoreCase("notUnique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("roleName", new ActionMessage("roleName.unique"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		return mapping.findForward(result);
	}

	/**
	 * Modify the existing role name.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		UserRolesDaoImpl userRolesDaoImpl = new UserRolesDaoImpl();
		HttpSession session = request.getSession();
		UserRolesForm userRolesForm = (UserRolesForm) form;
		List<UserRolesForm> userRolesForms = null;
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		if (!userRolesForm.getHiddenRoleName().equalsIgnoreCase(
				userRolesForm.getRoleName())) {
			if (userRolesDaoImpl.checkDuplicateRole(
					userRolesForm.getRoleName(), userDetails).equalsIgnoreCase(
					"notUnique")) {

				ActionErrors errors = new ActionErrors();
				errors.add("roleName", new ActionMessage("roleName.unique"));
				saveErrors(request, errors);
				// check whether the current user have the permission to view.
				if (CommonUtils.isView(privileges, "Roles")) {
					userRolesForms = userRolesDaoImpl.viewRoles(userDetails);
					request.setAttribute("userRoles", userRolesForms);
				}
				return mapping.getInputForward();

			}
		}
		String result = "";
		// check whether the current user have the permission to modify.
		if (CommonUtils.isModify(privileges, "Roles")) {

			result = userRolesDaoImpl.updateRole(userRolesForm,
					userRolesForm.getId(), userDetails);
		}
		if (result.equals("success")) {
			// check whether the current user have the permission to view.
			if (CommonUtils.isView(privileges, "Roles")) {
				userRolesForms = userRolesDaoImpl.viewRoles(userDetails);
				request.setAttribute("userRoles", userRolesForms);
			}
			session.removeAttribute("userRole");
			userRolesForm.reset(mapping, request);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("update.userrole");
			messages.add("userrole", msg);
			saveMessages(request, messages);
		} else {
			mapping.getInputForward();
		}
		return mapping.findForward("success");
	}

	/**
	 * To retrieve a particular role for modifying.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrive(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		UserRolesForm roleForm = (UserRolesForm) form;

		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		List<RolePrivileges> privileges = (List<RolePrivileges>) session
				.getAttribute("privileges");
		// check whether the current user have the permission to modify.
		if (CommonUtils.isModify(privileges, "Roles")) {
			Integer id = Integer
					.parseInt(request.getParameter("id").toString());
			UserRoles userRole = (UserRoles) userRolesDaoImpl.retriveRole(id,
					userDetails);
			if (userRole != null) {
				roleForm.setId(userRole.getId());
				roleForm.setHiddenRoleName(userRole.getRoleName());
				roleForm.setRoleName(userRole.getRoleName());
				roleForm.setRoleDesc(userRole.getRoleDesc());
			}
		}
		// check whether the current user have the permission to view.
		if (CommonUtils.isView(privileges, "Roles")) {
			List<UserRolesForm> userRolesForms = null;
			userRolesForms = userRolesDaoImpl.viewRoles(userDetails);
			request.setAttribute("userRoles", userRolesForms);
		}
		// forward to edit page
		return mapping.findForward("edituserrole");
	}

}
