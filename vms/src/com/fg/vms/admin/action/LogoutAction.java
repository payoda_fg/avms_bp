/**
 * LogoutAction.java
 */
package com.fg.vms.admin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.fg.vms.util.Constants;
import com.fg.vms.util.FileUploadProcess;

/**
 * Represent logout the application and invalidate all the session values.
 * 
 * @author pirabu
 * 
 */
public class LogoutAction extends Action {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Remove all sessions while logout the application.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 * 
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.info("Inside the Logout action class...");
		HttpSession session = request.getSession();

		String appRoot = getServlet().getServletContext().getRealPath("") + "/";
		/** remove the current session folder */
		if (FileUploadProcess.checkFolderAvailable(appRoot + "xml", session
				.getId().toString())) {
			FileUploadProcess.removeFolder(appRoot + "xml", session.getId()
					.toString());

		}
		session.removeAttribute("currentUser");
		session.removeAttribute("userId");
		session.removeAttribute("privileges");
		session.invalidate();
		logger.info("Successfully  Logout application...");
		return mapping.findForward("logout");
	}

}
