/*
 * CustomerAction.java
 */
package com.fg.vms.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.fg.vms.admin.dao.CustomerDao;
import com.fg.vms.admin.dao.LoginDao;
import com.fg.vms.admin.dao.impl.CustomerDaoImpl;
import com.fg.vms.admin.dao.impl.LoginDaoImpl;
import com.fg.vms.admin.dto.CustomerInfoDto;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.Customer;
import com.fg.vms.admin.model.CustomerApplicationSettings;
import com.fg.vms.admin.model.CustomerContacts;
import com.fg.vms.admin.model.CustomerSLA;
import com.fg.vms.admin.model.SecretQuestion;
import com.fg.vms.admin.pojo.CustomerContactForm;
import com.fg.vms.admin.pojo.CustomerForm;
import com.fg.vms.common.Country;
import com.fg.vms.customer.dao.CommonDao;
import com.fg.vms.customer.dao.CustomerDivisionDao;
import com.fg.vms.customer.dao.impl.CommonDaoImpl;
import com.fg.vms.customer.dao.impl.CustomerDivisionDaoImpl;
import com.fg.vms.util.CommonUtils;
import com.fg.vms.util.Constants;
import com.fg.vms.util.FileUploadProcess;
import com.fg.vms.util.SendEmail;

/**
 * Represent the Customer Configuration (eg.. Personalize your profile, Customer
 * Information, Primary Contact Information and user information)
 * 
 * @author pirabu
 * 
 */
public class CustomerAction extends DispatchAction {

	/** Logger of the system. */
	private Logger logger = Constants.logger;

	/**
	 * Forward to customer registration page when click the create customer
	 * button.
	 * 
	 * <p>
	 * This method always initialize the default application settings, country
	 * list and secret question list.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		logger.info("Inside the view method to initialize the application"
				+ " settings...");

		CustomerForm customerForm = (CustomerForm) form;
		HttpSession session = request.getSession();

		/*
		 * Its holds the user details..(such as current customer database
		 * details and application settings)
		 */
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerDao customerDao = new CustomerDaoImpl();

		/* Set the default application settings into session. */
		// session.setAttribute("background", "51C0F0");
		// session.setAttribute("secondary", "DCEDFD");
		// session.setAttribute("heading", "004C75");
		// session.setAttribute("rollover", "CEEBFF");
		// session.setAttribute("menubar", "0075C4");
		// session.setAttribute("menubutton", "0090F5");

		/* Security questions. */
		List<SecretQuestion> securityQnsList;

		/* Retrieve the secret question from the master table. */
		securityQnsList = customerDao.displaySecretQuestions(appDetails);
		session.setAttribute("secretQnsList", securityQnsList);

		/* Populate countries list */
		CommonDao commonDao = new CommonDaoImpl();
		List<Country> countries = commonDao.getCountries(appDetails);
		session.setAttribute("countryList", countries);
		customerForm.reset(mapping, request);
		logger.info("Forward to customer registration page");
		return mapping.findForward("viewpage");
	}

	/**
	 * Save the customer information.
	 * <p>
	 * This method used to save the customer information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward create(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Customer method is starting..");

		CustomerForm customerForm = (CustomerForm) form;
		HttpSession session = request.getSession();

		/* Currently login user id */
		Integer userId = (Integer) session.getAttribute("userId");

		FormFile uploadImage = null;
		uploadImage = customerForm.getCompanyLogo();

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* Executed on when update the customer profile. */
		if (customerForm.getLogoImagePath() == null) {

			if (uploadImage.getFileSize() == 0) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.size"));
				saveErrors(request, errors);
				logger.info("Uploaded image size is zero");
				return mapping.getInputForward();

			} else if (!uploadImage.getContentType().contains("image")) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.content"));
				saveErrors(request, errors);
				logger.info("Uploaded file is not an image");
				return mapping.getInputForward();
			}
			customerForm.setLogoImagePath(customerForm.getCompanycode() + "/"
					+ uploadImage.getFileName());
		}
		String filePath;

		/*
		 * Save uploaded image into physical path (Its executed on both update
		 * and save)
		 */
		if (uploadImage != null && uploadImage.getFileSize() != 0) {
			if (!uploadImage.getContentType().contains("image")) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.content"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
			filePath = FileUploadProcess.createUploadImageFilePath(
					Constants.getString(Constants.UPLOAD_LOGO_PATH),
					customerForm.getCompanycode());
			/*
			 * Check the folder is available
			 */
			if (FileUploadProcess.checkFolderAvailable(
					Constants.getString(Constants.UPLOAD_LOGO_PATH),
					customerForm.getCompanycode())) {
				CommonUtils.saveImage(uploadImage, filePath);

			} else { /* Create new folder */
				FileUploadProcess.createFolder(
						Constants.getString(Constants.UPLOAD_LOGO_PATH),
						customerForm.getCompanycode());
				CommonUtils.saveImage(uploadImage, filePath);
			}
			/* construct the logo path and set it to customerForm */
			customerForm.setLogoImagePath(customerForm.getCompanycode() + "/"
					+ uploadImage.getFileName());
			logger.info("File Path :::" + filePath);
		}

		// CommonUtils.saveImage(uploadImage, filePath);
		CustomerDao customerDao = new CustomerDaoImpl();
		logger.info("Calling the saveCustomer method to persist the object.");
		String appRoot = getServlet().getServletContext().getRealPath("") + "/";
		customerForm.setApplicationServer(request.getServerName());
		String result = customerDao.saveCustomer(customerForm, userId,
				userDetails, appRoot);

		// login user name already available, forward to input page
		if (result.equals("unique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("databaseName", new ActionMessage("databaseName.unique"));
			saveErrors(request, errors);
			logger.info("Database name should be unique.");
			return mapping.getInputForward();
		}
		// Customer code already available, forward to input page
		if (result.equals("custCode")) {
			ActionErrors errors = new ActionErrors();
			errors.add("companycode", new ActionMessage("err.customer.code"));
			saveErrors(request, errors);
			logger.info("Customer code  should be unique.");
			return mapping.getInputForward();

		} else if (result.equals("failure")) {
			// Return to input page when Transaction fails.
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("customer.save.failure");
			messages.add("customerSaveFailure", msg);
			saveMessages(request, messages);

			return mapping.getInputForward();

		} else if (result.equals("success")) {
			// Reset the form and send the message to user when customer
			// information saved successfully.
			customerForm.reset(mapping, request);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("customer.save.ok");
			messages.add("customer", msg);
			saveMessages(request, messages);
			List<Customer> customers = null;
			customers = customerDao.displayCustomers(userDetails);

			SendEmail email = new SendEmail();

			String emailId = customerForm.getContanctEmail();
			String url = "http://"
					+ customerForm.getApplicationUrl()
					+ "."
					+ Constants.getString(Constants.DOMAIN_NAME).toString()
							.replace("www.", "");
			String customerName = customerForm.getCompanyname();
			email.sendCredentialsToNewCustomer(customerName, url, emailId,
					customerForm.getContanctEmail(),
					customerForm.getLoginpassword(),userDetails);

			// Reset the Application Settings
			// LoginDao login = new LoginDaoImpl();
			// userDetails.setSettings(login
			// .getCustomerApplicationSettings(userDetails));
			session.setAttribute("customersList", customers);
			logger.info("Customer information successfuly persisted.");
			return mapping.findForward(result);
		}
		return mapping.findForward("success");
	}

	/**
	 * Update the customer information.
	 * <p>
	 * This method used to Update the customer information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("Customer method is starting..");

		CustomerForm customerForm = (CustomerForm) form;
		HttpSession session = request.getSession();

		/* Currently login user id */
		Integer userId = (Integer) session.getAttribute("userId");

		FormFile uploadImage = null;
		uploadImage = customerForm.getCompanyLogo();

		/*
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		/* Executed on when update the customer profile. */
		if (customerForm.getLogoImagePath() == null) {

			if (uploadImage.getFileSize() == 0) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.size"));
				saveErrors(request, errors);
				logger.info("Uploaded image size is zero");
				return mapping.getInputForward();

			} else if (!uploadImage.getContentType().contains("image")) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.content"));
				saveErrors(request, errors);
				logger.info("Uploaded file is not an image");
				return mapping.getInputForward();
			}
			customerForm.setLogoImagePath(customerForm.getCompanycode() + "/"
					+ uploadImage.getFileName());
		}
		String filePath;

		/*
		 * Save uploaded image into physical path (Its executed on both update
		 * and save)
		 */
		if (uploadImage != null && uploadImage.getFileSize() != 0) {
			if (!uploadImage.getContentType().contains("image")) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.content"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
			filePath = FileUploadProcess.createUploadImageFilePath(
					Constants.getString(Constants.UPLOAD_LOGO_PATH),
					customerForm.getCompanycode());
			/*
			 * Check the folder is available
			 */
			if (FileUploadProcess.checkFolderAvailable(
					Constants.getString(Constants.UPLOAD_LOGO_PATH),
					customerForm.getCompanycode())) {
				CommonUtils.saveImage(uploadImage, filePath);

			} else { /* Create new folder */
				FileUploadProcess.createFolder(
						Constants.getString(Constants.UPLOAD_LOGO_PATH),
						customerForm.getCompanycode());
				CommonUtils.saveImage(uploadImage, filePath);
			}
			/* construct the logo path and set it to customerForm */
			customerForm.setLogoImagePath(customerForm.getCompanycode() + "/"
					+ uploadImage.getFileName());
			logger.info("File Path :::" + filePath);
		}

		// CommonUtils.saveImage(uploadImage, filePath);
		CustomerDao customerDao = new CustomerDaoImpl();
		logger.info("Calling the saveCustomer method to persist the object.");
		customerForm.setApplicationServer(request.getServerName());
		String result = customerDao.updateCustomer(customerForm, userId,
				userDetails);

		// login user name already available, forward to input page
		if (result.equals("unique")) {
			ActionErrors errors = new ActionErrors();
			errors.add("databaseName", new ActionMessage("databaseName.unique"));
			saveErrors(request, errors);
			logger.info("Database name should be unique.");
			return mapping.getInputForward();
		}
		// Customer code already available, forward to input page
		if (result.equals("custCode")) {
			ActionErrors errors = new ActionErrors();
			errors.add("companycode", new ActionMessage("err.customer.code"));
			saveErrors(request, errors);
			logger.info("Customer code  should be unique.");
			return mapping.getInputForward();

		} else if (result.equals("failure")) {
			// Return to input page when Transaction fails.
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("customer.save.failure");
			messages.add("customerSaveFailure", msg);
			saveMessages(request, messages);

			return mapping.getInputForward();

		} else if (result.equals("success")) {
			// Reset the form and send the message to user when customer
			// information saved successfully.
			customerForm.reset(mapping, request);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("customer.save.ok");
			messages.add("customer", msg);
			saveMessages(request, messages);
			List<Customer> customers = null;
			customers = customerDao.displayCustomers(userDetails);
			// Reset the Application Settings
			// LoginDao login = new LoginDaoImpl();
			// userDetails.setSettings(login
			// .getCustomerApplicationSettings(userDetails));
			session.setAttribute("customersList", customers);
			logger.info("Customer information successfuly persisted.");
			return mapping.findForward(result);
		}
		return mapping.findForward(result);

	}

	/**
	 * Display the list of registered and active customer.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward display(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		List<Customer> customers = null;
		HttpSession session = request.getSession();

		CustomerForm customerForm = (CustomerForm) form;

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerDao customerDao = new CustomerDaoImpl();

		logger.info("Retrive the active customers");
		customers = customerDao.displayCustomers(userDetails);

		request.setAttribute("customersList", customers);
		customerForm.reset(mapping, request);
		return mapping.findForward("success");
	}

	/**
	 * Retrieve the selected customer information to view or edit the
	 * information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retrive(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		CustomerForm customerForm = (CustomerForm) form;
		HttpSession session = request.getSession();
		CustomerDao customerDao = new CustomerDaoImpl();

		// selected customer id
		Integer customerId = Integer.parseInt(request.getParameter("id")
				.toString());

		// Get the application settings to configure the database session.
		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		// code implements for password securityQuestions
		List<SecretQuestion> securityQnsList;
		securityQnsList = customerDao.displaySecretQuestions(appDetails);
		session.setAttribute("secretQnsList", securityQnsList);

		CustomerInfoDto customerInfoDto = customerDao.retriveCustomer(
				customerId, appDetails);
		// Customer information such as company code, name and company address.
		Customer customer = customerInfoDto.getCustomer();

		// Customer SLA Details.
		CustomerSLA customerSLA = customerInfoDto.getCustomerSLA();
		// Application settings
		CustomerApplicationSettings applicationSettings = customerInfoDto
				.getApplicationSettings();
		// Customer personal details and user information.
		CustomerContacts customerContact = null;
		// Customer have many contact, so display primary contact when view the
		// customer.
		if (customerInfoDto.getContacts() != null
				&& customerInfoDto.getContacts().size() != 0) {
			for (CustomerContacts contact : customerInfoDto.getContacts()) {
				if (contact.getIsPrimaryContact().equals((byte) 1)) {
					customerContact = contact;
				}
			}
		}

		customerForm.setCompanycode(customer.getCustCode());
		customerForm.setHiddenCustCode(customer.getCustCode());
		customerForm.setCompanyname(customer.getCustName());
		customerForm.setDunsnum(customer.getDunsNumber());
		customerForm.setTaxid(customer.getTaxId());

		customerForm.setAddress1(customer.getAddress1());
		customerForm.setCity(customer.getCity());
		customerForm.setCountry(customer.getCountry());
		customerForm.setState(customer.getState());
		customerForm.setRegion(customer.getRegion());
		customerForm.setProvince(customer.getProvince());
		customerForm.setPhone(customer.getPhone());
		customerForm.setEmail(customer.getEmailId());
		customerForm.setMobile(customer.getMobile());
		customerForm.setFax(customer.getFax());
		customerForm.setWebSite(customer.getWebsite());
		customerForm.setZipcode(customer.getZipCode());
		customerForm.setSlaStartDate(CommonUtils
				.convertDateToString(customerSLA.getStartDate()));
		customerForm.setSlaEndDate(CommonUtils.convertDateToString(customerSLA
				.getEndDate()));
		customerForm.setLicencedUsers(customerSLA.getLicencedUsers());
		customerForm.setLicencedVendors(customerSLA.getLicencedVendors());

		// set the DB settings
		customerForm.setDatabaseIp(customer.getDatabaseIp());
		customerForm.setDatabaseName(customer.getDatabaseName());
		customerForm.setUserName(customer.getUserName());
		customerForm.setDbpassword(customer.getDbpassword());
		customerForm.setApplicationUrl(customer.getApplicationUrl());
		// If primary contact is exist then show the primary contact information
		// to user.
		if (customerContact != null) {

			customerForm.setContactId(customerContact.getId());
			customerForm.setFirstName(customerContact.getFirstName());
			customerForm.setLastName(customerContact.getLastName());
			customerForm.setDesignation(customerContact.getDesignation());
			customerForm.setContactPhone(customerContact.getPhoneNumber());
			customerForm.setContactMobile(customerContact.getMobile());
			customerForm.setContactFax(customerContact.getFax());
			customerForm.setContanctEmail(customerContact.getEmailId());
			customerForm.setHiddenEmailId(customerContact.getEmailId());

			customerForm.setLoginAllowed(CommonUtils
					.getBooleanValue(customerContact.getIsAllowedLogin()));

			customerForm.setLoginDisplayName(customerContact
					.getLoginDisplayName());
			// customerForm.setLoginId(customerContact.getLoginId());
			customerForm.setHiddenLoginId(customerContact.getLoginId());
			customerForm.setLoginpassword(customerContact.getPassword());
			customerForm.setConfirmPassword(customerContact.getPassword());
			customerForm.setUserSecQn(customerContact.getSecretQuestionId()
					.getId());

			customerForm.setUserSecQnAns(customerContact.getSecQueAns());
		}
		customerForm.setLogoImagePath(applicationSettings.getLogoPath());
		/*
		 * customerForm.setBackground("#" +
		 * applicationSettings.getBodyBackgroundColor());
		 * customerForm.setTextColor("#" +
		 * applicationSettings.getBodyTextColor()); customerForm.setMenuBar("#"
		 * + applicationSettings.getMenuBarColor());
		 * customerForm.setMenuButton("#" +
		 * applicationSettings.getMenuButtonColor()); customerForm
		 * .setRollOver("#" + applicationSettings.getMenuSelectColor());
		 * customerForm.setHeading("#" +
		 * applicationSettings.getHeaderPrimaryColor());
		 * 
		 * session.setAttribute("backgroundFromDB",
		 * applicationSettings.getBodyBackgroundColor());
		 * session.setAttribute("secondaryFromDB",
		 * applicationSettings.getBodyTextColor());
		 * session.setAttribute("headingFromDB",
		 * applicationSettings.getHeaderPrimaryColor());
		 * session.setAttribute("rolloverFromDB",
		 * applicationSettings.getMenuSelectColor());
		 * session.setAttribute("menubarFromDB",
		 * applicationSettings.getMenuBarColor());
		 * session.setAttribute("menubuttonFromDB",
		 * applicationSettings.getMenuButtonColor());
		 * 
		 * session.setAttribute("background", "51C0F0");
		 * session.setAttribute("secondary", "DCEDFD");
		 * session.setAttribute("heading", "004C75");
		 * session.setAttribute("rollover", "CEEBFF");
		 * session.setAttribute("menubar", "0075C4");
		 * session.setAttribute("menubutton", "0090F5");
		 */

		session.setAttribute("customerInfo", customerInfoDto);

		// populate countries list
		CommonDao commonDao = new CommonDaoImpl();
		List<Country> countries = commonDao.getCountries(appDetails);
		session.setAttribute("countryList", countries);
		// forward to customer edit page.
		return mapping.findForward("edit");
	}

	/**
	 * Add new contact information to customer.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward addContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		/** object for access the customer services */
		CustomerDao customerDao = new CustomerDaoImpl();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerContactForm contactForm = (CustomerContactForm) form;

		String result = customerDao.addContact(
				(Customer) session.getAttribute("customer"), contactForm,
				(Integer) session.getAttribute("userId"), userDetails);

		CustomerInfoDto customerInfoDto = customerDao.retriveCustomer(
				((Customer) session.getAttribute("customer")).getId(),
				userDetails);
		session.setAttribute("customerInfo", customerInfoDto);

		return mapping.findForward(result);
	}

	/**
	 * Show the selected contact information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		/** object for access the customer services */
		CustomerDao customerDao = new CustomerDaoImpl();
		CustomerContactForm contactForm = (CustomerContactForm) form;

		/** contact holds the customer contact information. */
		CustomerContacts contact = customerDao.retriveContact(
				Integer.parseInt(request.getParameter("custId").toString()),
				userDetails);

		contactForm.setId(contact.getId());
		contactForm.setFirstName(contact.getFirstName());
		contactForm.setFirstName(contact.getFirstName());
		contactForm.setLastName(contact.getLastName());
		contactForm.setDesignation(contact.getDesignation());
		contactForm.setContanctEmail(contact.getEmailId());
		contactForm.setContactMobile(contact.getMobile());
		contactForm.setContactPhone(contact.getPhoneNumber());
		contactForm.setLoginpassword(contact.getPassword());
		contactForm.setContactFax(contact.getFax());
		if (contact.getSecretQuestionId() != null) {
			contactForm.setUserSecQn(contact.getSecretQuestionId().getId());
		} else {
			contactForm.setUserSecQn(0);
		}

		contactForm.setUserSecQnAns(contact.getSecQueAns());
		if (contact.getIsAllowedLogin().equals((byte) 1)) {
			contactForm.setLoginAllowed(true);
		} else {
			contactForm.setLoginAllowed(false);
		}

		contactForm.setLoginDisplayName(contact.getLoginDisplayName());
		contactForm.setLoginId(contact.getLoginId());
		contactForm.setLoginpassword(contact.getPassword());

		// session.setAttribute("selectedContact", contact);
		return mapping.findForward("viewcontact");
	}

	/**
	 * Delete the selected contact information into database.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		/** object for access the customer services */
		CustomerDao customerDao = new CustomerDaoImpl();
		HttpSession session = request.getSession();

		/**
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		String result = customerDao.deleteContact(
				Integer.parseInt(request.getParameter("custId").toString()),
				userDetails);
		CustomerInfoDto customerInfoDto = customerDao.retriveCustomer(
				((Customer) session.getAttribute("customer")).getId(),
				userDetails);
		session.setAttribute("customerInfo", customerInfoDto);

		return mapping.findForward(result);
	}

	/**
	 * Merge the contact information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward updateContact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		/**
		 * Application details such as database settings, application
		 * personalize settings of customer.
		 */
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerDao customerDao = new CustomerDaoImpl();

		CustomerContactForm contactForm = (CustomerContactForm) form;

		String result = customerDao.updateContact(
				(Customer) session.getAttribute("customer1"), contactForm,
				(Integer) session.getAttribute("userId"), userDetails);

		CustomerInfoDto customerInfoDto = customerDao.retriveCustomer(
				userDetails.getCurrentCustomer().getCustomerId().getId(),
				userDetails);
		session.setAttribute("customerInfo", customerInfoDto);

		return mapping.findForward(result);
	}

	/**
	 * Method for invoking the delete the customer information and contact.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward deleteCustomer(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CustomerDao customerDao = new CustomerDaoImpl();

		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer customerId = Integer.parseInt(request.getParameter("id")
				.toString());
		String result = customerDao.deleteCustomer(customerId, userDetails);
		List<Customer> customers = null;
		customers = customerDao.displayCustomers(userDetails);
		request.setAttribute("customersList", customers);

		// If any problem with deletion then forward to back.
		if (result.equalsIgnoreCase("failure")) {
			ActionErrors errors = new ActionErrors();
			errors.add("referencecustomer", new ActionMessage("reference"));
			saveErrors(request, errors);
			return mapping.getInputForward();
		}

		return mapping.findForward(result);
	}

	/**
	 * Method for invoking the new contact page to add the contact information.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward contact(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return mapping.findForward("addContact");
	}

	/**
	 * Method for invoking retrieve the customer information from customer
	 * database for customer modification..
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward retriveCustomerInfoFromCustomerDB(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		CustomerForm customerForm = (CustomerForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto appDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		// code implements for password securityQuestions

		List<SecretQuestion> securityQnsList;
		CustomerDao customerDao = new CustomerDaoImpl();
		securityQnsList = customerDao.displaySecretQuestions(appDetails);
		session.setAttribute("secretQnsList", securityQnsList);

		CustomerInfoDto customerInfoDto = customerDao
				.retriveCustomerInfoFromCustomerDB(appDetails);
		Customer customer = customerInfoDto.getCustomer();

		com.fg.vms.customer.model.CustomerApplicationSettings applicationSettings = customerInfoDto
				.getSettings();
		com.fg.vms.customer.model.CustomerApplicationSettings settings = (com.fg.vms.customer.model.CustomerApplicationSettings) session
				.getAttribute("isDivisionStatus");
		// Set isDivision status
		customerForm.setIsDivision(settings.getIsDivision());

		CustomerContacts customerContact = null;
		if (customerInfoDto.getContacts() != null
				&& customerInfoDto.getContacts().size() != 0) {
			for (CustomerContacts contact : customerInfoDto.getContacts()) {
				if (contact.getIsPrimaryContact().equals((byte) 1)) {
					customerContact = contact;
				}
			}
		}

		customerForm.setId(customer.getId());
		customerForm.setCompanycode(customer.getCustCode());
		customerForm.setHiddenCustCode(customer.getCustCode());
		customerForm.setCompanyname(customer.getCustName());
		customerForm.setDunsnum(customer.getDunsNumber());
		customerForm.setHiddenDunsNumber(customer.getDunsNumber());
		customerForm.setHiddenTaxId(customer.getTaxId());
		customerForm.setTaxid(customer.getTaxId());

		customerForm.setAddress1(customer.getAddress1());
		customerForm.setAddress2(customer.getAddress2());
		customerForm.setAddress3(customer.getAddress3());
		customerForm.setCity(customer.getCity());
		customerForm.setCountry(customer.getCountry());
		customerForm.setState(customer.getState());
		customerForm.setRegion(customer.getRegion());
		customerForm.setProvince(customer.getProvince());
		customerForm.setPhone(customer.getPhone());
		customerForm.setEmail(customer.getEmailId());
		customerForm.setMobile(customer.getMobile());
		customerForm.setFax(customer.getFax());
		customerForm.setWebSite(customer.getWebsite());
		customerForm.setZipcode(customer.getZipCode());
		if (customerContact != null) {

			customerForm.setContactId(customerContact.getId());
			customerForm.setFirstName(customerContact.getFirstName());
			customerForm.setLastName(customerContact.getLastName());
			customerForm.setDesignation(customerContact.getDesignation());
			customerForm.setContactPhone(customerContact.getPhoneNumber());
			customerForm.setContactMobile(customerContact.getMobile());
			customerForm.setContactFax(customerContact.getFax());
			customerForm.setContanctEmail(customerContact.getEmailId());
			customerForm.setHiddenEmailId(customerContact.getEmailId());

			customerForm.setLoginAllowed(CommonUtils
					.getBooleanValue(customerContact.getIsAllowedLogin()));

			customerForm.setLoginDisplayName(customerContact
					.getLoginDisplayName());
			// customerForm.setLoginId(customerContact.getLoginId());
			customerForm.setHiddenLoginId(customerContact.getLoginId());
			customerForm.setLoginpassword(customerContact.getPassword());
			customerForm.setConfirmPassword(customerContact.getPassword());
			if (customerContact.getSecretQuestionId() != null)
				customerForm.setUserSecQn(customerContact.getSecretQuestionId()
						.getId());

			customerForm.setUserSecQnAns(customerContact.getSecQueAns());
		}
		customerForm.setLogoImagePath(applicationSettings.getLogoPath());
		customerForm.setMenuColor(applicationSettings.getMenuSelectColor());
		customerForm.setSessionTimeout(applicationSettings.getSessionTimeout());
		customerForm.setTermsCondition(applicationSettings.getTermsCondition());
		customerForm.setPrivacyTerms(applicationSettings.getPrivacyTerms());
		customerForm.setPrimeTrainingUrl(applicationSettings.getPrimeTrainingUrl());
		customerForm.setNonPrimeTrainingUrl(applicationSettings.getNonPrimeTrainingUrl());
		/*
		 * customerForm.setBackground("#" +
		 * applicationSettings.getBodyBackgroundColor());
		 * customerForm.setTextColor("#" +
		 * applicationSettings.getBodyTextColor()); customerForm.setMenuBar("#"
		 * + applicationSettings.getMenuBarColor());
		 * customerForm.setMenuButton("#" +
		 * applicationSettings.getMenuButtonColor()); customerForm
		 * .setRollOver("#" + applicationSettings.getMenuSelectColor());
		 * customerForm.setHeading("#" +
		 * applicationSettings.getHeaderPrimaryColor()); if
		 * (applicationSettings.getSpend_data_upload_frequency() != null) {
		 * customerForm.setSpendDataUploadFrequency(applicationSettings
		 * .getSpend_data_upload_frequency().toString()); }
		 * customerForm.setFiscalStartDate(CommonUtils
		 * .convertDateToString(applicationSettings.getFiscalStartDate()));
		 * customerForm.setFiscalEndDate(CommonUtils
		 * .convertDateToString(applicationSettings.getFiscalEndDate()));
		 */

		/*
		 * session.setAttribute("backgroundFromDB",
		 * applicationSettings.getBodyBackgroundColor());
		 * session.setAttribute("secondaryFromDB",
		 * applicationSettings.getBodyTextColor());
		 * session.setAttribute("headingFromDB",
		 * applicationSettings.getHeaderPrimaryColor());
		 * session.setAttribute("rolloverFromDB",
		 * applicationSettings.getMenuSelectColor());
		 * session.setAttribute("menubarFromDB",
		 * applicationSettings.getMenuBarColor());
		 * session.setAttribute("menubuttonFromDB",
		 * applicationSettings.getMenuButtonColor());
		 * 
		 * session.setAttribute("background", "51C0F0");
		 * session.setAttribute("secondary", "DCEDFD");
		 * session.setAttribute("heading", "004C75");
		 * session.setAttribute("rollover", "CEEBFF");
		 * session.setAttribute("menubar", "0075C4");
		 * session.setAttribute("menubutton", "0090F5");
		 */

		session.setAttribute("customerInfo", customerInfoDto);
		// populate countries list
		CommonDao commonDao = new CommonDaoImpl();
		List<Country> countries = commonDao.getCountries(appDetails);
		session.setAttribute("countryList", countries);

		return mapping.findForward("edit");
	}

	/**
	 * Merge the customer information by customer.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward updateCustomerInfoByCustomer(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		log.info(" =================== create method is calling =========");
		CustomerForm customerForm = (CustomerForm) form;

		FormFile uploadImage = null;
		uploadImage = customerForm.getCompanyLogo();

		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");

		if (customerForm.getLogoImagePath() == null) {

			if (uploadImage.getFileSize() == 0) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.size"));
				saveErrors(request, errors);
				return mapping.getInputForward();

			} else if (!uploadImage.getContentType().contains("image")) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.content"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
			customerForm.setLogoImagePath(customerForm.getCompanycode() + "/"
					+ uploadImage.getFileName());
		}
		String filePath;

		// Save uploaded image into physical path (Its executed on both update
		// and save)
		if (uploadImage != null && uploadImage.getFileSize() != 0) {
			if (!uploadImage.getContentType().contains("image")) {

				ActionErrors errors = new ActionErrors();
				errors.add("companyLogo", new ActionMessage(
						"err.company.logo.content"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
			filePath = FileUploadProcess.createUploadImageFilePath(
					Constants.getString(Constants.UPLOAD_LOGO_PATH),
					customerForm.getCompanycode());
			// Chech the folder is available
			if (FileUploadProcess.checkFolderAvailable(
					Constants.getString(Constants.UPLOAD_LOGO_PATH),
					customerForm.getCompanycode())) {
				CommonUtils.saveImage(uploadImage, filePath);

			} else {
				FileUploadProcess.createFolder(
						Constants.getString(Constants.UPLOAD_LOGO_PATH),
						customerForm.getCompanycode());
				CommonUtils.saveImage(uploadImage, filePath);
			}
			// construct the logo path and set it to customerForm
			customerForm.setLogoImagePath(customerForm.getCompanycode() + "/"
					+ uploadImage.getFileName());
			logger.info("File Path :::" + filePath);
		}

		// CommonUtils.saveImage(uploadImage, filePath);
		CustomerDao customerDao = new CustomerDaoImpl();
		customerForm.setApplicationServer(request.getServerName());
		String result = customerDao.updateCustomerInfoByCustomer(customerForm,
				userId, userDetails);

		if (result.equalsIgnoreCase("available")) {
			ActionErrors errors = new ActionErrors();
			errors.add("loginId", new ActionMessage("userLogin.unique"));
			saveErrors(request, errors);
			logger.info("Login user name should be unique.");
			return mapping.getInputForward();
		}
		if (result.equals("failure")) {

			return mapping.findForward("failure");

		} else if (result.equals("success")) {

			customerForm.reset(mapping, request);
			ActionMessages messages = new ActionMessages();
			ActionMessage msg = new ActionMessage("customer.save.ok");
			messages.add("customer", msg);
			saveMessages(request, messages);
			List<Customer> customers = null;
			customers = customerDao.displayCustomers(userDetails);
			LoginDao login = new LoginDaoImpl();
			userDetails.setSettings(login
					.getCustomerApplicationSettings(userDetails));
			session.setAttribute("customersList", customers);

		}
		
		com.fg.vms.customer.model.CustomerApplicationSettings settings = null;
		LoginDao login = new LoginDaoImpl();
		settings = login.getCustomerApplicationSettings(userDetails);
		userDetails.setSettings(settings);

		session.setAttribute("isDivisionStatus", settings);
		return mapping.findForward(result);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward checkCustomerDivisionStatus(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Method to view Manage Division page");
		Integer userId = null;
		CustomerForm customerForm = (CustomerForm) form;		
		HttpSession session = request.getSession();

		userId = (Integer) session.getAttribute("userId");
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		CustomerDivisionDao customerDivisionDaoImpl = new CustomerDivisionDaoImpl();		
		String isDivisionPresent = null;
		//List<Users> customerDivisionStatus = null;

		Long divisionStatus = customerDivisionDaoImpl
				.checkDivisionStatus(userDetails);
		if (divisionStatus != null && divisionStatus > 0) {
			isDivisionPresent = "Y";
//			customerDivisionStatus = customerDivisionDaoImpl
//					.checkCustomerDivisionStatus(userDetails, userId);	
//			if (customerDivisionStatus.get(0) != null
//					&& customerDivisionStatus.size() > 0) {
//				isDivisionPresent = "Y";
//			} else {
//				customerForm.setIsDivision((byte) 0);
//				isDivisionPresent = "N";
//			}
		} else {
			isDivisionPresent = "N";
		}
		request.setAttribute("isDivisionPresent", isDivisionPresent);
		return mapping.findForward("editinfo");
	}

}
