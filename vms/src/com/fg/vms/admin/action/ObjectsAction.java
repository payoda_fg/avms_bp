/*
 * ObjectsAction.java 
 */
package com.fg.vms.admin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.impl.ObjectsDaoImpl;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.admin.pojo.ObjectsForm;
import com.fg.vms.util.Constants;

/**
 * Represents the object configuration (eg.. Creating new objects, Modifying the
 * object details, Remove/Deactivate the object details.)
 * 
 * This class is used to store the page, session, application level objects.
 * Hence by enabling these objects can give privileges to the various types of
 * users.
 * 
 * @author vinoth
 * 
 */

public class ObjectsAction extends DispatchAction {

    /** Logger of the system. */
    private Logger logger = Constants.logger;

    /**
     * Creating new objects in VMS application
     * 
     * <p>
     * This method is used to Creating the new objects into database.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward insertObject(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	logger.info(" Method starts creating new object..");

	HttpSession session = request.getSession();
	Integer userId = (Integer) session.getAttribute("userId"); // Get the
								   // current
								   // user id
								   // from
								   // session

	/*
	 * It holds the user details..(such as current customer database details
	 * and application settings)
	 */
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	ObjectsForm configurationForm = (ObjectsForm) form;
	ObjectsDaoImpl configurationDaoImpl = new ObjectsDaoImpl();

	/* Creating new objects and saves it into database */
	String result = configurationDaoImpl.createObject(configurationForm,
		userId, userDetails);

	if (result.equals("unique")) {

	    /* For check unique object name. */
	    ActionErrors errors = new ActionErrors();
	    errors.add("objectName", new ActionMessage("objectName.unique"));
	    saveErrors(request, errors);
	    return mapping.getInputForward();
	}

	if (result.equals("success")) {
	    List<VMSObjects> objects = null;

	    /* For show the list of objects. */
	    objects = configurationDaoImpl.listObjects(userDetails);
	    session.setAttribute("objects", objects);

	    /* For show the successful object insert message. */
	    ActionMessages messages = new ActionMessages();
	    ActionMessage msg = new ActionMessage("object.ok");
	    messages.add("object", msg);
	    saveMessages(request, messages);
	    configurationForm.reset(mapping, request);
	}

	logger.info(" Forward to show the list of objects page ");
	return mapping.findForward("configsuccess");
    }

    /**
     * Show the list of objects when user selects the insert object menu.
     * 
     * <p>
     * This method is used to show the list of active objects available in
     * database
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward view(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	logger.info(" Method starts to view the list of available active objects..");

	List<VMSObjects> objects = null;
	HttpSession session = request.getSession();

	/*
	 * It holds the user details..(such as current customer database details
	 * and application settings)
	 */
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	ObjectsDaoImpl configurationDaoImpl = new ObjectsDaoImpl();

	/* For show the list of objects. */
	objects = configurationDaoImpl.listObjects(userDetails);
	session.setAttribute("objects", objects);

	logger.info(" Forward to show the list of objects page ");
	return mapping.findForward("viewsuccess");
    }

    /**
     * Deactivate/Remove the available objects.
     * 
     * <p>
     * This method is used to remove the object from database.
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward deleteobject(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	logger.info(" Method starts to delete the list of available active objects..");

	ObjectsDaoImpl configurationDaoImpl = new ObjectsDaoImpl();
	HttpSession session = request.getSession();

	/*
	 * It holds the user details..(such as current customer database details
	 * and application settings)
	 */
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");

	/* Deleting the selected object. */
	String result = configurationDaoImpl.deleteObject(
		Integer.parseInt(request.getParameter("id")), userDetails);

	if (result.equals("success")) {
	    List<VMSObjects> objects = null;

	    /* For show the list of objects. */
	    objects = configurationDaoImpl.listObjects(userDetails);
	    session.setAttribute("objects", objects);
	}

	logger.info(" Forward to show the list of objects page ");
	return mapping.findForward("deletesuccess");
    }

    /**
     * Retrieve the selected object details
     * 
     * <p>
     * This method is used to retrieve/view the selected object details
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward retrive(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	logger.info(" Method starts retrieving the seleccted object details..");

	ObjectsForm configurationForm = (ObjectsForm) form;
	ObjectsDaoImpl configurationDaoImpl = new ObjectsDaoImpl();
	HttpSession session = request.getSession();

	/*
	 * It holds the user details..(such as current customer database details
	 * and application settings)
	 */
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	Integer id = Integer.parseInt(request.getParameter("id").toString()); // Get
									      // the
									      // user
									      // id
									      // from
									      // request

	/* Retrieving the selected object. */
	VMSObjects configuration = configurationDaoImpl.retriveObject(id,
		userDetails);

	/* Set the values to respective fields. */
	if (configuration != null) {
	    configurationForm.setId(configuration.getId());
	    configurationForm.setObjectName(configuration.getObjectName());
	    configurationForm.setObjectType(configuration.getObjectType());
	    configurationForm.setObjectDescription(configuration
		    .getObjectDescription());
	    configurationForm.setIsActive(configuration.getIsActive()
		    .toString());
	}

	List<VMSObjects> objects = null;

	/* For show the list of objects. */
	objects = configurationDaoImpl.listObjects(userDetails);
	session.setAttribute("objects", objects);

	logger.info(" Forward to show the list of objects page ");
	return mapping.findForward("reteivesuccess");
    }

    /**
     * Update the selected object details
     * 
     * <p>
     * This method is used to modify/update the selected object details
     * 
     * @param mapping
     *            - The ActionMapping used to select this instance
     * @param form
     *            - The ActionForm bean for request
     * @param request
     *            - The HTTP request to process
     * @param response
     *            - The HTTP response that will be created
     * @return
     * @throws Exception
     *             - if the application business logic throws an exception
     */
    public ActionForward updateObject(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	logger.info(" Method starts update the retrieved object details..");

	HttpSession session = request.getSession();
	Integer currentUserId = (Integer) session.getAttribute("userId"); // Get
									  // the
									  // user
									  // id
									  // from
									  // request

	/*
	 * Application details such as database settings, application
	 * personalize settings of customer.
	 */
	UserDetailsDto userDetails = (UserDetailsDto) session
		.getAttribute("userDetails");
	ObjectsDaoImpl configurationDaoImpl = new ObjectsDaoImpl();
	ObjectsForm configurationForm = (ObjectsForm) form;

	/* Updating the object details. */
	String result = configurationDaoImpl.updateObject(configurationForm,
		configurationForm.getId(), currentUserId, userDetails);

	if (result.equals("success")) {
	    List<VMSObjects> objects = null;

	    /* For show the list of objects. */
	    objects = configurationDaoImpl.listObjects(userDetails);
	    request.setAttribute("objects", objects);
	    session.removeAttribute("listobjects");
	    configurationForm.reset(mapping, request);
	}

	logger.info(" Forward to show the list of objects page ");
	return mapping.findForward("updatesuccess");
    }
}
