package com.fg.vms.admin.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.fg.vms.admin.dao.ObjectsDao;
import com.fg.vms.admin.dao.RolePrvilegesDao;
import com.fg.vms.admin.dao.RolesDao;
import com.fg.vms.admin.dao.impl.ObjectsDaoImpl;
import com.fg.vms.admin.dao.impl.RolePrivilegesDaoImpl;
import com.fg.vms.admin.dao.impl.UserRolesDaoImpl;
import com.fg.vms.admin.dto.RolesAndObjects;
import com.fg.vms.admin.dto.UserDetailsDto;
import com.fg.vms.admin.model.RolePrivileges;
import com.fg.vms.admin.model.Users;
import com.fg.vms.admin.model.VMSObjects;
import com.fg.vms.admin.pojo.PrivilegeForm;
import com.fg.vms.admin.pojo.UserRolesForm;
import com.fg.vms.util.CommonUtils;

import fr.improve.struts.taglib.layout.datagrid.Datagrid;

/**
 * Assign privileges to role.
 * 
 * @author vinoth karthi
 * 
 */

public class RolePrivilegesAction extends DispatchAction {
	private static final String SUCCESS = "success";

	/**
	 * Forward to privileges page
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward rolePrivileges(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		PrivilegeForm privilegeForm = (PrivilegeForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");

		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		List<UserRolesForm> userRolesForms = userRolesDaoImpl
				.viewRoles(userDetails);

		session.setAttribute("userRoles", userRolesForms);
		//session.setAttribute("roleName", 0);
		List<VMSObjects> objects = null;
		ObjectsDao configurationDaoImpl = new ObjectsDaoImpl();
		objects = configurationDaoImpl.listObjects(userDetails);

		// Get an object list.
		List<?> aList = packageRolesPrivileges(objects, null);

		// Create a datagrid.
		Datagrid datagrid = Datagrid.getInstance();

		// Set the bean class for new objects. We suppose RolesAndObjects is the
		// class of the object in the List aList.
		datagrid.setDataClass(RolesAndObjects.class);

		// Set the data
		datagrid.setData(aList);

		// Initialize the form
		privilegeForm.setDatagrid(datagrid);
		privilegeForm.setRoleName("0");
		privilegeForm.setApplicationcategoryname("0");
		// forward to the privileges page.
		return mapping.findForward(SUCCESS);
	}

	/**
	 * Save the privileges by role.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward persistPrivilegesByRole(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		PrivilegeForm privilegeForm = (PrivilegeForm) form;

		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		Integer userId = (Integer) session.getAttribute("userId");
		Users currentUser = (Users) session.getAttribute("currentUser");
		request.setAttribute("roleName", privilegeForm.getRoleName());

		RolePrvilegesDao rolePrvilegesDao = new RolePrivilegesDaoImpl();

		// Get the datagrid.
		Datagrid datagrid = privilegeForm.getDatagrid();

		// Deal with new subjects.
		Collection<?> privilegesAddedOrModified = datagrid.getModifiedData();
		Iterator<?> iterate = privilegesAddedOrModified.iterator();
		while (iterate.hasNext()) {
			RolesAndObjects roleAndObject = (RolesAndObjects) iterate.next();
			if (!roleAndObject.getObjectName().equalsIgnoreCase(
					"Role Privileges")) {
				rolePrvilegesDao.persistPrivilegesByRole(roleAndObject,
						privilegeForm.getRoleName(), userId, userDetails);
			}
		}

		List<RolePrivileges> privileges;
		privileges = rolePrvilegesDao.retrievePrivilegesByRole(currentUser
				.getUserRoleId().getId(), userDetails);
		//To hide/show menu based on sub-menus
		setMenusInHomePage(privileges,session);
		session.setAttribute("privileges", privileges);
		// forward to the privileges page
		return mapping.findForward(SUCCESS);
	}

	/**
	 * Retrieve the list of object privileges by role.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */
	public ActionForward viewPrivilegesByRole(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		PrivilegeForm privilegeForm = (PrivilegeForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		List<UserRolesForm> userRolesForms = userRolesDaoImpl
				.viewRoles(userDetails);

		// get the selected role id
		Integer roleId = Integer.parseInt(request.getParameter("roleId")
				.toString());
		session.setAttribute("userRoles", userRolesForms);

		request.setAttribute("roleName", roleId.toString());
		List<VMSObjects> objects = null;
		ObjectsDao configurationDaoImpl = new ObjectsDaoImpl();

		objects = configurationDaoImpl.listObjects(userDetails);
		RolePrvilegesDao rolePrvilegesDaoImpl = new RolePrivilegesDaoImpl();
		privilegeForm.setRoleName(roleId.toString());
		// Get an object list.
		List<?> aList = packageRolesPrivileges(objects,
				rolePrvilegesDaoImpl.retrievePrivilegesByRole(roleId,
						userDetails));

		// Create a datagrid.
		Datagrid datagrid = Datagrid.getInstance();

		// Set the bean class for new objects. We suppose RolesAndObjects is the
		// class of the object in the List aList.
		datagrid.setDataClass(RolesAndObjects.class);

		// Set the data
		datagrid.setData(aList);

		// Initialize the form
		privilegeForm.setDatagrid(datagrid);
		// forward to the privileges page.
		return mapping.findForward(SUCCESS);
	}

	/**
	 * Convert VMSObjects and RolePrivileges to RoleAndPrivileges object
	 * 
	 * @param objects
	 * @param privilegesByRole
	 * @return an list of RoleAndPrivileges objects.
	 */
	public List<RolesAndObjects> packageRolesPrivileges(
			List<VMSObjects> objects, List<RolePrivileges> privilegesByRole) {

		List<RolesAndObjects> rolesAndObjects = new ArrayList<RolesAndObjects>();

		// Store what are the VMSObjects present in RolePrivileges.
		List<VMSObjects> objectsInPrivilegs = new ArrayList<VMSObjects>();

		// Make list of RolesAndObjects objects based on objects and
		// privilegesByRole.
		if (privilegesByRole != null) {
			for (RolePrivileges privileges : privilegesByRole) {
				for (VMSObjects object : objects) {
					// Find objects present in privilegesByRole
					if (privileges.getObjectId().getId().equals(object.getId())) {
						objectsInPrivilegs.add(object);
						break;
					}
				}

			}
			// Set the list of privilegesByRole value to rolesAndObjects.
			for (RolePrivileges privileges : privilegesByRole) {
				RolesAndObjects roleAndObject = new RolesAndObjects();
				roleAndObject.setAdd(CommonUtils.getBooleanValue(privileges
						.getAdd()));
				roleAndObject.setDelete(CommonUtils.getBooleanValue(privileges
						.getDelete()));
				roleAndObject.setEnable(CommonUtils.getBooleanValue(privileges
						.getEnable()));
				roleAndObject.setModify(CommonUtils.getBooleanValue(privileges
						.getModify()));
				roleAndObject.setObjectName(privileges.getObjectId()
						.getObjectName());
				roleAndObject.setView(CommonUtils.getBooleanValue(privileges
						.getView()));
				roleAndObject.setVisible(CommonUtils.getBooleanValue(privileges
						.getVisible()));
				roleAndObject.setObjectDesc(privileges.getObjectId().getObjectDescription());
				roleAndObject.setId(privileges.getObjectId().getId());
				rolesAndObjects.add(roleAndObject);

			}
			objects.removeAll(objectsInPrivilegs);
			for (VMSObjects object : objects) {
				RolesAndObjects roleAndObject = new RolesAndObjects();
				roleAndObject.setObjectName(object.getObjectName());
				roleAndObject.setObjectDesc(object.getObjectDescription());
				roleAndObject.setId(object.getId());
				rolesAndObjects.add(roleAndObject);
			}
		}
		// Didn't assign privileges to role.
		if (privilegesByRole == null) {
			for (VMSObjects object : objects) {
				RolesAndObjects roleAndObject = new RolesAndObjects();
				roleAndObject.setObjectName(object.getObjectName());
				roleAndObject.setObjectDesc(object.getObjectDescription());
				roleAndObject.setId(object.getId());
				rolesAndObjects.add(roleAndObject);

			}
		}
		return rolesAndObjects;
	}
	/**
	 * Method which set menu headers(Vendor,report etc) in homepage.
	 */
	
	private void setMenusInHomePage(List<RolePrivileges> privList,HttpSession session)
	{
		boolean haveVendorSection=false;
		boolean haveReportSection=false;
		boolean haveAdminSection=false;
		
		if(privList !=null)
		{
		for(RolePrivileges priv:privList)
		{
			if(priv.getObjectId().getApplicationCategory() != null){
			if(priv.getObjectId().getApplicationCategory().equalsIgnoreCase("V") && priv.getVisible()==(byte)1)
			{
				haveVendorSection=true;
			}
			if(priv.getObjectId().getApplicationCategory().equalsIgnoreCase("R") && priv.getVisible()==(byte)1)
			{
				if(priv.getObjectId().getObjectName().equalsIgnoreCase("Dashboard View") 
						|| priv.getObjectId().getObjectName().equalsIgnoreCase("Display Dashboard")) {
					//Nothing to do...
				}
				else {
					haveReportSection = true;
				}
			}
			if(priv.getObjectId().getApplicationCategory().equalsIgnoreCase("A") && priv.getVisible()==(byte)1)
			{
				haveAdminSection=true;
			}
			
			}
		 }
		}
		
		session.setAttribute("haveVendorSection", haveVendorSection);
		session.setAttribute("haveReportSection", haveReportSection);
		session.setAttribute("haveAdminSection", haveAdminSection);
		
	}	
	
	/**
	 * Retrieve the list of object privileges by both Role Id and Application Category are > 0.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */	
	public ActionForward viewPrivilegesByApplication(ActionMapping mapping,ActionForm form, 
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		PrivilegeForm privilegeForm = (PrivilegeForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		// get the selected application category id		
		Integer applicationid = Integer.parseInt(request.getParameter("applicationid").toString());
				
		//get the selected role Id				
		Integer roleId=Integer.parseInt(privilegeForm.getRoleName());
			
		//session.setAttribute("userRoles", userRolesForms);
		request.setAttribute("applicationcategoryname", applicationid.toString());
		List<VMSObjects> objects = null;
		ObjectsDao configurationDaoImpl = new ObjectsDaoImpl();
		
		RolePrvilegesDao rolePrvilegesDaoImpl = new RolePrivilegesDaoImpl();
		
		privilegeForm.setApplicationcategoryname(applicationid.toString());
		
		// Check  application category id value
		// If Application Category Id = 0 then Display All Objectnames of the VMS_Objects(table);
		if(applicationid==0)
			 objects=configurationDaoImpl.listObjects(userDetails);  
		// If  >0 then Display that Application Category Id related Objectnames of the VMS_Objects(table);	
		else if(applicationid>0 && roleId==0)
		     objects = configurationDaoImpl.listObjectsByApplicationCategory(userDetails,privilegeForm);  
		 // If Application Category Id>0 & roleId>0  then Display both Application Category Id, Role id related Objectnames  of the VMS_Objects( table) and set to Datagrid		
		else if(roleId>0 && applicationid>0)                                                               
		{
			objects=configurationDaoImpl.listObjectsByApplicationCategory(userDetails, privilegeForm);
			List<?> aList = packageRolesPrivileges(objects,
					rolePrvilegesDaoImpl.retrievePrivilegesByRoleAndApplication(roleId,applicationid,userDetails));
			Datagrid datagrid = Datagrid.getInstance();
			datagrid.setDataClass(RolesAndObjects.class);
			datagrid.setData(aList);
			privilegeForm.setDatagrid(datagrid);
		}
		
		if(applicationid==0 || roleId==0)
		{
			List<?> aList = packageRolesPrivileges(objects, null);
			Datagrid datagrid = Datagrid.getInstance();
			datagrid.setDataClass(RolesAndObjects.class);
			datagrid.setData(aList);
			privilegeForm.setDatagrid(datagrid);
		}
		 
		return mapping.findForward(SUCCESS);
	}	

	/**
	 * Retrieve the list of objects privileges by Both ApplicationCategory id and Role id are > 0.
	 * 
	 * @param mapping
	 *            - The ActionMapping used to select this instance
	 * @param form
	 *            - The ActionForm bean for request
	 * @param request
	 *            - The HTTP request to process
	 * @param response
	 *            - The HTTP response that will be created
	 * @return
	 * @throws Exception
	 *             - if the application business logic throws an exception
	 */	
	public ActionForward viewRolePrivilegesByRole(ActionMapping mapping,ActionForm form, 
			HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		PrivilegeForm privilegeForm = (PrivilegeForm) form;
		HttpSession session = request.getSession();

		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		
		RolesDao userRolesDaoImpl = new UserRolesDaoImpl();
		List<UserRolesForm> userRolesForms = userRolesDaoImpl
				.viewRoles(userDetails);

		// get the selected role id
		Integer roleId = Integer.parseInt(request.getParameter("roleId")
				.toString());
		
		// get the selected Application Category id		
		Integer applicationcategoryid=Integer.parseInt(privilegeForm.getApplicationcategoryname());
		
		session.setAttribute("userRoles", userRolesForms);

		request.setAttribute("roleName", roleId.toString());
		
		List<VMSObjects> objects = null;
		ObjectsDao configurationDaoImpl = new ObjectsDaoImpl();
		
		RolePrvilegesDao rolePrvilegesDaoImpl = new RolePrivilegesDaoImpl();
		privilegeForm.setRoleName(roleId.toString());
		
		// Check  the application category id value		
		//if((roleId==0 && applicationcategoryid==0)||(roleId>0 && applicationcategoryid==0))		
		// If Application Category Id = 0 then Display All Objectnames of the VMS_Objects(table);
		
		if(applicationcategoryid==0)
			objects=configurationDaoImpl.listObjects(userDetails);
		else if(roleId==0 && applicationcategoryid>0)
			objects=configurationDaoImpl.listObjectsByApplicationCategory(userDetails, privilegeForm);
		else if(roleId>0 && applicationcategoryid>0)
		{
			objects=configurationDaoImpl.listObjectsByApplicationCategory(userDetails, privilegeForm);
			List<?> aList = packageRolesPrivileges(objects,
					rolePrvilegesDaoImpl.retrievePrivilegesByRoleAndApplication(roleId,applicationcategoryid,userDetails));
			Datagrid datagrid = Datagrid.getInstance();
			datagrid.setDataClass(RolesAndObjects.class);
			datagrid.setData(aList);
			privilegeForm.setDatagrid(datagrid);
		}
		if(applicationcategoryid==0 || roleId==0)
		{
			List<?> aList = packageRolesPrivileges(objects, null);
			Datagrid datagrid = Datagrid.getInstance();
			datagrid.setDataClass(RolesAndObjects.class);
			datagrid.setData(aList);
			privilegeForm.setDatagrid(datagrid);
		}
		return mapping.findForward(SUCCESS);
	}
}