<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ page isErrorPage="true"%>
<%@ page import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href="images/favicon.ico" rel="SHORTCUT ICON">
		<title>Supplier Portal</title>
		<style>
			body {
				background: #f9fee8;
				margin: 0;
				padding: 16%;
				text-align: center;
				font-family: Verdana, sans-serif;
				font-size: 14px;
				/*color: #666666;*/
				color: #009900;
			}
			
			#url{
				color: #009900;
			}
		</style>
	</head>
	
	<body>
		<div class="error_page">
			<h2>We're Sorry...</h2>
			<p>JavaScript must be enabled in order for you to use the Supplier Diversity Application.</p>
			<p>However, it seems JavaScript is either disabled or not supported by your Browser.</p>
			<p>To use, Enable JavaScript by changing your Browser Options, then Try Again.</p>
			<html:link forward="/mainPage" styleId="url">Back To Login </html:link>
		</div>
	</body>
</html>