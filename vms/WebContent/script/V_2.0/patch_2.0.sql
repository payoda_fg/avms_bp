--- Added 1 Column on the "customer_vendorselfregistrarinfo" Table ---
--- Author : Asarudeen A Date : 04-06-2014 ---
ALTER TABLE `customer_vendorselfregistrarinfo` ADD COLUMN `COUNT` INT(10) NOT NULL DEFAULT '0' AFTER `CUSTOMERDIVISIONID`;
------------------------------------------------------------------------------------------------------------------------------

--- Author : Asarudeen A Date : 13-06-2014 ---
CREATE TABLE `avms_users` (
 `ID` INT(10) NOT NULL AUTO_INCREMENT,
 `EMAILID` VARCHAR(255) NOT NULL,
 `USERTYPE` CHAR(1) NOT NULL,
 `FIRSTNAME` VARCHAR(255) NULL DEFAULT NULL,
 `LASTNAME` VARCHAR(255) NULL DEFAULT NULL,
 `USERID` INT(10) NOT NULL,
 PRIMARY KEY (`ID`),
 UNIQUE INDEX `EMAILID` (`EMAILID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

insert into avms_users (emailid,firstname,lastname,usertype,userid) select USEREMAILID,firstname,lastname,'C',id from users;
insert into avms_users (emailid,firstname,lastname,usertype,userid) select emailid,firstname,lastname,'V',id from customer_vendor_users ;


-- --------------------------------------------------------
-- Host:                         172.16.0.246
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for trigger avmsbp_15.tr_customer_vendor_users_insert
DROP TRIGGER IF EXISTS `tr_customer_vendor_users_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tr_customer_vendor_users_insert` BEFORE INSERT ON `customer_vendor_users` FOR EACH ROW BEGIN
 insert into avms_users (EMAILID,USERTYPE,FIRSTNAME,LASTNAME,USERID)
  values(NEW.EMAILID,'V',NEW.FIRSTNAME,NEW.LASTNAME,NEW.ID);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


-- --------------------------------------------------------
-- Host:                         172.16.0.246
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for trigger avmsbp_15.tr_users_insert
DROP TRIGGER IF EXISTS `tr_users_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tr_users_insert` AFTER INSERT ON `users` FOR EACH ROW BEGIN
 insert into avms_users (EMAILID,USERTYPE,FIRSTNAME,LASTNAME,USERID)
  values(NEW.USEREMAILID,'C',NEW.FIRSTNAME,NEW.LASTNAME,NEW.ID);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 18-06-2014 ---
ALTER TABLE `customer_vendor_users` ADD COLUMN `GENDER` CHAR(1) AFTER `ISACTIVE`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 04-07-2014 ---
# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.6.16
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-07-04 16:50:02
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table avms_test.email_type
DROP TABLE IF EXISTS `email_type`;
CREATE TABLE IF NOT EXISTS `email_type` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  `CREATEDBY` int(11) DEFAULT NULL,
  `CREATEDON` timestamp NULL DEFAULT NULL,
  `MODIFIEDBY` int(11) DEFAULT NULL,
  `MODIFIEDON` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

# Dumping data for table avms_test.email_type: ~12 rows (approximately)
/*!40000 ALTER TABLE `email_type` DISABLE KEYS */;
INSERT INTO `email_type` (`ID`, `DESCRIPTION`, `CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`) VALUES
	(1, 'New Self Registeration Mail Content', 6, '2014-07-01 20:48:34', NULL, NULL),
	(2, 'Send log in credentials to new vendor.', 6, '2014-07-01 20:58:49', NULL, NULL),
	(3, 'No login Credentials', 6, '2014-07-01 20:58:47', NULL, NULL),
	(4, ' Registration Completion.', 6, '2014-07-01 20:58:45', NULL, NULL),
	(5, 'Vendor Registration Alert(to admin)', 6, '2014-07-01 20:58:43', NULL, NULL),
	(6, 'Self Registration-Prime Vendor', 6, '2014-07-01 20:58:41', NULL, NULL),
	(7, 'For Forget password request:', 6, '2014-07-01 20:58:40', NULL, NULL),
	(8, 'Report not submitted', 6, '2014-07-01 20:58:38', NULL, NULL),
	(9, 'Assessment Completion', 123, '2014-07-01 20:58:37', NULL, NULL),
	(10, 'Assign Supplier Assessment', 123, '2014-07-01 20:58:35', NULL, NULL),
	(11, 'Tier 2 Expenditure Report', 6, '2014-07-01 20:58:34', NULL, NULL),
	(12, 'No login Credentials Alaska', 6, '2014-07-02 16:46:22', NULL, NULL);
/*!40000 ALTER TABLE `email_type` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


ALTER TABLE `customer_emailtemplate`  ADD COLUMN `EMAILTEMPLATESUBJECT` VARCHAR(255) NOT NULL AFTER `ID`;

ALTER TABLE `customer_emailtemplate`
	ADD COLUMN `TEMPLATECODE` INT(10) NULL DEFAULT NULL AFTER `EMAILTEMPLATESUBJECT`,
	ADD CONSTRAINT `FK_customer_emailtemplate_email_type` FOREIGN KEY (`TEMPLATECODE`) REFERENCES `email_type` (`ID`);
	
CREATE TABLE `customer_emailtemplateparameters` (
	`ID` INT(10) NOT NULL AUTO_INCREMENT,
	`EMAILTEMPLATEID` INT(11) NOT NULL,
	`PARAMETERNAME` VARCHAR(50) NOT NULL,
	`ISACTIVE` TINYINT(4) NOT NULL DEFAULT '1',
	`CREATEDON` DATETIME NOT NULL,
	`CREATEDBY` INT(11) NOT NULL,
	`MODIFIEDBY` INT(11) NULL DEFAULT NULL,
	`MODIFIEDON` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`ID`),
	INDEX `FK_customer_emailtemplate_parameter_customer_parameter` (`EMAILTEMPLATEID`),
	CONSTRAINT `FK_customer_emailtemplate__customer_emailtemplate_parameter` FOREIGN KEY (`EMAILTEMPLATEID`) REFERENCES `customer_emailtemplate` (`ID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT
AUTO_INCREMENT=0;

CREATE TABLE `customer_emailtemplate_division` (
	`ID` INT(10) NOT NULL AUTO_INCREMENT,
	`EMAILTEMPLATEID` INT(11) NOT NULL,
	`CUSTOMERDIVISIONID` INT(10) NOT NULL,
	`ISACTIVE` TINYINT(4) NOT NULL DEFAULT '1',
	`CREATEDON` DATETIME NOT NULL,
	`CREATEDBY` INT(11) NOT NULL,
	`MODIFIEDBY` INT(11) NULL DEFAULT NULL,
	`MODIFIEDON` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`ID`),
	INDEX `FK_customer_emailtemplate_division_customer_division` (`EMAILTEMPLATEID`),
	INDEX `FK_customer_emailtemplate_division_customer_emailtemplate` (`CUSTOMERDIVISIONID`),
	CONSTRAINT `FK_customer_emailtemplate_division_customer_certificatetype` FOREIGN KEY (`EMAILTEMPLATEID`) REFERENCES `customer_emailtemplate` (`ID`),
	CONSTRAINT `FK_customer_emailtemplate_division_customer_division` FOREIGN KEY (`CUSTOMERDIVISIONID`) REFERENCES `customer_division` (`ID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT
AUTO_INCREMENT=0;

------------------------------------------------------------------------------------------------------------------------------
--- Added 1 Column on the "email_type" Table ---
--- Author : Kaleswararao A Date : 09-07-2014 ---

ALTER TABLE `email_type` ADD COLUMN `TEMPLATETYPE` CHAR(1) AFTER `MODIFIEDON`;


UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=1;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=2;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=3;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=4;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=5;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=6;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=7;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=8;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=9;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=10;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=11;
UPDATE `email_type` SET `TEMPLATETYPE`='S' WHERE  `ID`=12;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 22-07-2014 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-07-22 16:23:01', 1, '2014-07-22 16:23:04', 'Vendor Commodities Not Saved', 'Vendor Commodities Not Saved', 'menu', 'R');
	
UPDATE `statusmaster` SET `statusname`='Vetted by Supplier Diversity' WHERE  `id`='A';

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 25-07-2014 ---
CREATE TABLE `customer_savesearchfilter` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`SEARCHNAME` VARCHAR(255) NOT NULL,
	`SEARCHDATE` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`USERID` INT(11) NOT NULL,
	`SEARCHTYPE` CHAR(1) NOT NULL COMMENT '\'(V)endor status\', \'Vendor (C)riteria\', \'(P)rime Vendor Search\',\'custom vendor (R)eport\'',
	`CRITERIA` VARCHAR(10000) NULL,
	`SEARCHQUERY` VARCHAR(10000) NOT NULL,
	PRIMARY KEY (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Mahan Date : 28-07-2014 ---
CREATE TABLE `customer_marketsector` (
 `ID` INT(10) NOT NULL AUTO_INCREMENT,
 `SECTORCODE` VARCHAR(50) NULL,
 `SECTORDESCRIPTION` VARCHAR(255) NOT NULL,
 `ISACTIVE` TINYINT NOT NULL,
 `CREATEDBY` INT NOT NULL,
 `CREATEDON` DATETIME NOT NULL,
 `MODIFIEDBY` INT NULL,
 `MODIFIEDON` DATETIME NULL,
 PRIMARY KEY (`ID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

ALTER TABLE `customer_commoditycategory`
 ADD COLUMN `MARKETSECTORID` INT NULL DEFAULT NULL AFTER `MODIFIEDON`;
 
ALTER TABLE `customer_commoditycategory`
 ADD CONSTRAINT `FK_customer_commoditycategory_customer_marketsector` FOREIGN KEY (`MARKETSECTORID`) REFERENCES `customer_marketsector` (`ID`);
 
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 01-08-2014 --- 
ALTER TABLE `customer_applicationsettings`
	ADD COLUMN `PRIVACY_TERMS` VARCHAR(2000) NULL DEFAULT NULL AFTER `TERMS_CONDITION`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 02-08-2014 --- 
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-08-02 14:43:56', 1, '2014-08-02 14:43:58', 'Display Dashboard', 'Display Dashboard', 'menu', 'R');
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 04-08-2014 ---
CREATE TABLE `customer_vendorkeywords` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`KEYWORD` VARCHAR(100) NULL DEFAULT NULL,
	`VENDORID` INT(11) NOT NULL,
	`CREATEDBY` INT(11) NOT NULL,
	`CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`MODIFIEDBY` INT(11) NULL DEFAULT NULL,
	`MODIFIEDON` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `FK__customer_vendormaster` FOREIGN KEY (`VENDORID`) REFERENCES `customer_vendormaster` (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

UPDATE `customer_applicationsettings` 
	SET `TERMS_CONDITION`='I, #NAME, #DESIGNATION, hereby certify that I have read and understood all the terms and conditions set forth in this registration and all the information I have furnished in this form are true and accurate to the best of my knowledge and belief. I understand that any false or misleading information may result in the permanent removal of my registration profile or entry into the BP Supplier Diversity Registration Portal.  I hereby certify I have the authority to make these representations.' WHERE  `ID`=1;
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 05-08-2014 ---
ALTER TABLE `customer_vendorkeywords`
 ADD FULLTEXT INDEX `fk_customer_vendorkeywords` (`KEYWORD`);
 
---Added in FullText Search @ Line No:27---
union  
  select customer_vendormaster.id from customer_vendormaster,customer_vendorkeywords
   where customer_vendorkeywords.vendorid =customer_vendormaster.id and
   MATCH (customer_vendorkeywords.KEYWORD) AGAINST (search_text IN BOOLEAN MODE)
   
ALTER TABLE `country`
	ADD COLUMN `isdefault` INT(4) NOT NULL DEFAULT '0' AFTER `provincestate`;
	
UPDATE `country` SET `isdefault`=1 WHERE  `Id`=225;
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A Date : 07-08-2014 ---
ALTER TABLE `customer_applicationsettings`
	CHANGE COLUMN `TERMS_CONDITION` `TERMS_CONDITION` TEXT(50000) NULL DEFAULT NULL AFTER `SESSIONTIMEOUT`,
	CHANGE COLUMN `PRIVACY_TERMS` `PRIVACY_TERMS` TEXT(50000) NULL DEFAULT NULL AFTER `TERMS_CONDITION`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A  Date : 08-08-2014 Setted Market Subsector IsActive = 0---
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=38;

------------------------------------------------------------------------------------------------------------------------------
--- Author :Asarudeen A   Date : 12-08-2014 ---
ALTER TABLE `customer_vendorothercertificate`
	ADD COLUMN `ISPICS` INT(3) NOT NULL DEFAULT '0' AFTER `MODIFIEDON`;
------------------------------------------------------------------------------------------------------------------------------
--- Author :Asarudeen A   Date : 14-08-2014 ---
ALTER TABLE `customer_savesearchfilter`
	ADD COLUMN `CUSTOMERDIVISIONID` INT(11) NULL AFTER `SEARCHQUERY`;
ALTER TABLE `customer_savesearchfilter`
	ADD CONSTRAINT `FK_customer_savesearchfilter_customer_division` FOREIGN KEY (`CUSTOMERDIVISIONID`) REFERENCES `customer_division` (`ID`);
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A   Date : 18-08-2014 ---
UPDATE `customer_businessgroup` SET `BUSINESSGROUPNAME`='Lower 48' WHERE  `ID`=41; --- Change North America Gas to Lower 48 ---

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A   Date : 21-08-2014 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-08-19 20:29:02', 1, '2014-08-19 20:29:05', 'Certificate Expiration Notification Email', 'Certificate Expiration Notification Email', 'menu', 'R');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A    Date : 25-08-2014 ---
ALTER TABLE `certificateexpirationnotification`
	CHANGE COLUMN `EMAILID` `EMAILID` VARCHAR(255) NULL DEFAULT NULL AFTER `VENDORID`,
	CHANGE COLUMN `CERTIFICATENUMBER` `CERTIFICATENUMBER` VARCHAR(255) NULL DEFAULT NULL AFTER `EMAILID`,
	CHANGE COLUMN `AGENCYNAME` `AGENCYNAME` VARCHAR(255) NULL DEFAULT NULL AFTER `CERTIFICATENUMBER`,
	CHANGE COLUMN `CERTIFICATENAME` `CERTIFICATENAME` VARCHAR(255) NULL DEFAULT NULL AFTER `AGENCYNAME`,
	CHANGE COLUMN `VENDORCERTIFICATEID` `VENDORCERTIFICATEID` INT(255) NULL DEFAULT NULL AFTER `CERTIFICATENAME`,
	CHANGE COLUMN `STATUS` `STATUS` VARCHAR(255) NULL DEFAULT NULL COMMENT 'YES/NO submitted or not' AFTER `EMAILDATE`;
	
------------------------------------------------------------------------------------------------------------------------------
--- Author :SHEFEEK A     Date : 29-08-2014 ---
DROP TABLE IF EXISTS `avms_custom_report_field_selection`;
CREATE TABLE IF NOT EXISTS `avms_custom_report_field_selection` (
  `id` int(10) NOT NULL,
  `Category` varchar(100) DEFAULT NULL,
  `inputtablename` varchar(100) DEFAULT NULL,
  `inputcolumnname` varchar(100) DEFAULT NULL,
  `columntitle` varchar(100) DEFAULT NULL,
  `outputtablename` varchar(100) DEFAULT NULL,
  `outputsql` varchar(2000) DEFAULT NULL,
  `wherecondition` varchar(2000) DEFAULT NULL,
  `jointype` varchar(100) DEFAULT NULL,
  `tableorder` int(11) DEFAULT NULL,
  `columnorder` int(11) DEFAULT NULL,
  `wherecondition2` varchar(2000) DEFAULT NULL,
  `wherecondition3` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `avms_custom_report_field_selection`;
INSERT INTO `avms_custom_report_field_selection` (`id`, `Category`, `inputtablename`, `inputcolumnname`, `columntitle`, `outputtablename`, `outputsql`, `wherecondition`, `jointype`, `tableorder`, `columnorder`, `wherecondition2`, `wherecondition3`) VALUES
	(1, 'Vendor Sales', 'customer_vendor_sales', 'YEARNUMBER', 'Year Number', 'customer_vendor_sales', 'customer_vendor_sales.YEARNUMBER', 'Left Join\r\n  customer_vendor_sales On customer_vendor_sales.VENDORID =\r\n    customer_vendormaster.ID', NULL, 5, 1, NULL, NULL),
	(2, 'Vendor Sales', 'customer_vendor_sales', 'YEARSALES', 'Year Sales', 'customer_vendor_sales', 'customer_vendor_sales.YEARSALES', 'Left Join\r\n  customer_vendor_sales On customer_vendor_sales.VENDORID =\r\n    customer_vendormaster.ID', NULL, 5, 2, NULL, NULL),
	(3, 'Vendor Contact', 'customer_vendor_users', 'DESIGNATION', 'Designation', 'customer_vendor_users', 'customer_vendor_users.DESIGNATION', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 3, NULL, NULL),
	(4, 'Vendor Contact', 'customer_vendor_users', 'EMAILID', 'Email Id', 'customer_vendor_users', 'customer_vendor_users.EMAILID', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 4, NULL, NULL),
	(5, 'Vendor Contact', 'customer_vendor_users', 'FAX', 'Fax', 'customer_vendor_users', 'customer_vendor_users.FAX', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 5, NULL, NULL),
	(6, 'Vendor Contact', 'customer_vendor_users', 'FIRSTNAME', 'First Name', 'customer_vendor_users', 'customer_vendor_users.FIRSTNAME', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 1, NULL, NULL),
	(7, 'Vendor Contact', 'customer_vendor_users', 'LASTNAME', 'Last Name', 'customer_vendor_users', 'customer_vendor_users.LASTNAME', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 2, NULL, NULL),
	(8, 'Vendor Contact', 'customer_vendor_users', 'MOBILE', 'Mobile', 'customer_vendor_users', 'customer_vendor_users.MOBILE', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 3, NULL, NULL),
	(9, 'Vendor Contact', 'customer_vendor_users', 'PHONENUMBER', 'Phone Number', 'customer_vendor_users', 'customer_vendor_users.PHONENUMBER', 'Left Join  customer_vendor_users On customer_vendor_users.VENDORID = customer_vendormaster.ID', NULL, 3, 4, NULL, NULL),
	(10, 'Vendor Address ', 'customer_vendoraddressmaster', 'ADDRESSTYPE', 'Address Type', 'customer_vendoraddressmaster', '(case \nwhen customer_vendoraddressmaster.ADDRESSTYPE =\'p\' then \'Physical Address\'\nwhen customer_vendoraddressmaster.ADDRESSTYPE =\'m\' then \'Mail Address\'\nwhen customer_vendoraddressmaster.ADDRESSTYPE =\'r\' then \'Remit Address\'\nEnd)AddressType\n', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 1, NULL, NULL),
	(11, 'Vendor Address ', 'customer_vendoraddressmaster', 'ADDRESS', 'Address', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.ADDRESS', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 2, NULL, NULL),
	(12, 'Vendor Address ', 'customer_vendoraddressmaster', 'CITY', 'City', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.CITY', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 3, NULL, NULL),
	(13, 'Vendor Address ', 'customer_vendoraddressmaster', 'COUNTRY', 'Country', 'customer_vendoraddressmaster', 'country.countryname', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', 'Left Outer', 2, 4, 'Inner Join  country On customer_vendoraddressmaster.COUNTRY = country.Id', NULL),
	(14, 'Vendor Address ', 'customer_vendoraddressmaster', 'FAX', 'Fax', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.FAX', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 9, NULL, NULL),
	(15, 'Vendor Address ', 'customer_vendoraddressmaster', 'MOBILE', 'Mobile', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.MOBILE', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 10, NULL, NULL),
	(16, 'Vendor Address ', 'customer_vendoraddressmaster', 'PHONE', 'Phone', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.PHONE', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 11, NULL, NULL),
	(17, 'Vendor Address ', 'customer_vendoraddressmaster', 'PROVINCE', 'Province', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.PROVINCE', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 5, NULL, NULL),
	(18, 'Vendor Address ', 'customer_vendoraddressmaster', 'REGION', 'Region', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.REGION', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 6, NULL, NULL),
	(19, 'Vendor Address ', 'customer_vendoraddressmaster', 'STATE', 'State', 'customer_vendoraddressmaster', 'state.statename', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', 'Left Outer', 2, 7, ' Inner Join state On customer_vendoraddressmaster.STATE = state.ID', NULL),
	(20, 'Vendor Address ', 'customer_vendoraddressmaster', 'ZIPCODE', 'Zip Code', 'customer_vendoraddressmaster', 'customer_vendoraddressmaster.ZIPCODE', 'Left Join customer_vendoraddressmaster On customer_vendoraddressmaster.VENDORID = customer_vendormaster.ID', NULL, 2, 8, NULL, NULL),
	(21, 'Vendor Certificate', 'customer_vendorcertificate', 'DIVERSER_QUALITY', 'Diverser/Quality', 'customer_vendorcertificate', '(case \nwhen customer_vendorcertificate.DIVERSER_QUALITY =1 then \'Diverse Certificate\'\nwhen customer_vendorcertificate.DIVERSER_QUALITY =2 then \'Quality Certificate\'\nEnd)Diverse_quality', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', NULL, 6, 1, NULL, NULL),
	(22, 'Vendor Certificate', 'customer_vendorcertificate', 'CERTIFICATETYPE', 'Certificate Type', 'customer_vendorcertificate', 'customer_certificatetype.description', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', 'Left Outer', 6, 2, 'Inner Join customer_certificatetype On customer_vendorcertificate.CERTIFICATETYPE = customer_certificatetype.ID', NULL),
	(23, 'Vendor Certificate', 'customer_vendorcertificate', 'CERTIFICATENUMBER', 'Certificate Number', 'customer_vendorcertificate', 'customer_vendorcertificate.CERTIFICATENUMBER', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', NULL, 6, 3, NULL, NULL),
	(24, 'Vendor Certificate', 'customer_vendorcertificate', 'EFFECTIVEDATE', 'Certificate Effective Date', 'customer_vendorcertificate', 'DATE_FORMAT(customer_vendorcertificate.EFFECTIVEDATE, \'%d/%m/%Y\')\r\n', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', NULL, 6, 4, NULL, NULL),
	(25, 'Vendor Certificate', 'customer_vendorcertificate', 'EXPIRYDATE', 'Certificate Expiry Date', 'customer_vendorcertificate', 'DATE_FORMAT(customer_vendorcertificate.EXPIRYDATE, \'%d/%m/%Y\')', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', NULL, 6, 5, NULL, NULL),
	(26, 'Vendor Certificate', 'customer_vendorcertificate', 'FILENAME', 'Certificate File Name', 'customer_vendorcertificate', 'customer_vendorcertificate.FILENAME', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', NULL, 6, 6, NULL, NULL),
	(27, 'Vendor Certificate', 'customer_vendorcertificate', 'ETHNICITY_ID', 'Ethnicity ', 'customer_vendorcertificate', 'certificateEthnicity.ethnicity', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', 'Left Outer', 6, 7, ' Left Join ethnicity as certificateEthnicity On customer_vendorcertificate.ETHNICITY_ID = certificateEthnicity.ID', NULL),
	(28, 'Vendor Certificate', 'customer_vendorcertificate', 'CERTAGENCYID', 'Agency Name', 'customer_vendorcertificate', 'customer_certificateagencymaster.CERTAGENCYNAME', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', 'Left Outer', 6, 8, 'Inner Join customer_certificateagencymaster On customer_vendorcertificate.CERTAGENCYID =  customer_certificateagencymaster.ID', NULL),
	(29, 'Vendor Certificate', 'customer_vendorcertificate', 'CERTMASTERID', 'Certificate Name', 'customer_vendorcertificate', 'vendorcertificate.CERTIFICATENAME', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', 'Left Outer', 6, 9, 'Inner Join  customer_certificatemaster  as vendorcertificate On customer_vendorcertificate.CERTMASTERID = vendorcertificate.ID ', NULL),
	(30, 'Vendor Commodity', 'customer_commodity', 'COMMODITYID', 'Commodity Name', 'customer_vendorcommodity', 'customer_commodity.COMMODITYDESCRIPTION', 'Left Join customer_vendorcommodity On customer_vendorcommodity.VENDORID = customer_vendormaster.ID', 'Left Outer', 8, 1, 'Inner Join  customer_commodity On customer_vendorcommodity.COMMODITYID = customer_commodity.ID', NULL),
	(31, 'Diverse Classification', 'customer_vendordiverseclassifcation', 'DIVERSE_QUALITY', 'Diverser/Quality', 'customer_vendordiverseclassifcation', '(case \nwhen customer_vendorcertificate.DIVERSER_QUALITY =1 then \'Diverse Certificate\'\nwhen customer_vendorcertificate.DIVERSER_QUALITY =2 then \'Quality Certificate\'\nEnd)Diverse_quality', 'Left Join customer_vendorcertificate On customer_vendorcertificate.VENDORID = customer_vendormaster.ID', NULL, 7, 1, NULL, NULL),
	(32, 'Diverse Classification', 'customer_vendordiverseclassifcation', 'CERTMASTERID', 'Certificate Name', 'customer_certificatemaster', 'diverseclassification.CERTIFICATENAME', 'Left Join customer_vendordiverseclassifcation as vendordiverseclassifcation on vendordiverseclassifcation.VENDORID=customer_vendormaster.ID', 'Left Outer', 7, 2, 'Inner Join customer_certificatemaster as diverseclassification On vendordiverseclassifcation.CERTMASTERID = diverseclassification.ID\r\n', NULL),
	(33, 'Company Information', 'customer_vendormaster', 'MOBILE', 'Mobile ', 'customer_vendormaster', 'customer_vendormaster.MOBILE', NULL, NULL, 1, 12, NULL, NULL),
	(34, 'Company Information', 'customer_vendormaster', 'PHONE', 'Phone', 'customer_vendormaster', 'customer_vendormaster.PHONE', NULL, NULL, 1, 11, NULL, NULL),
	(35, 'Company Information', 'customer_vendormaster', 'FAX', 'Fax', 'customer_vendormaster', 'customer_vendormaster.FAX', NULL, NULL, 1, 10, NULL, NULL),
	(36, 'Company Information', 'customer_vendormaster', 'COMPANY_TYPE', 'Legal Structure', 'customer_vendormaster', 'customer_legal_structure.name', 'Left Join\r\n  customer_legal_structure On customer_vendormaster.COMPANY_TYPE =\r\n    customer_legal_structure.Id', 'Left Outer', 1, 9, NULL, NULL),
	(37, 'Company Information', 'customer_vendormaster', 'DIVSERSUPPLIER', 'Diverse Supplier', 'customer_vendormaster', '(case \nwhen customer_vendormaster.DIVSERSUPPLIER =1 then \'Yes\'\nElse \'No\'\nEnd)DiverseSupplier', NULL, NULL, 1, 8, NULL, NULL),
	(38, 'Company Information', 'customer_vendormaster', 'DUNSNUMBER', 'DUNS Number', 'customer_vendormaster', 'customer_vendormaster.DUNSNUMBER', NULL, NULL, 1, 7, NULL, NULL),
	(39, 'Company Information', 'customer_vendormaster', 'EMAILID', 'Email Id', 'customer_vendormaster', 'customer_vendormaster.EMAILID', NULL, NULL, 1, 6, NULL, NULL),
	(40, 'Company Information', 'customer_vendormaster', 'NUMBEROFEMPLOYEES', 'Number of Employees', 'customer_vendormaster', 'customer_vendormaster.NUMBEROFEMPLOYEES', NULL, NULL, 1, 5, NULL, NULL),
	(41, 'Company Information', 'customer_vendormaster', 'PRIMENONPRIMEVENDOR', 'Prime Or Non Prime ', 'customer_vendormaster', '(case \nwhen customer_vendormaster.PRIMENONPRIMEVENDOR =1 then \'Prime\'\nwhen customer_vendormaster.PRIMENONPRIMEVENDOR =2 then \'Non Prime\'\nEnd)PrimeNonPrime', NULL, NULL, 1, 4, NULL, NULL),
	(42, 'Company Information', 'customer_vendormaster', 'TAXID', 'Tax ID', 'customer_vendormaster', 'customer_vendormaster.TAXID', NULL, NULL, 1, 3, NULL, NULL),
	(43, 'Company Information', 'customer_vendormaster', 'VENDORCODE', 'Vendor Code', 'customer_vendormaster', 'customer_vendormaster.VENDORCODE', NULL, NULL, 1, 2, NULL, NULL),
	(44, 'Company Information', 'customer_vendormaster', 'VENDORNAME', 'Vendor Name', 'customer_vendormaster', 'customer_vendormaster.VENDORNAME', NULL, NULL, 1, 1, NULL, NULL),
	(45, 'Company Information', 'customer_vendormaster', 'WEBSITE', 'Website', 'customer_vendormaster', 'customer_vendormaster.WEBSITE', NULL, NULL, 1, 13, NULL, NULL),
	(46, 'Company Information', 'customer_vendormaster', 'YEAROFESTABLISHMENT', 'Year Of Establishment', 'customer_vendormaster', 'customer_vendormaster.YEAROFESTABLISHMENT', NULL, NULL, 1, 14, NULL, NULL),
	(47, 'Company Information', 'customer_vendormaster', 'ETHNICITY_ID', 'Ethnicity ', 'customer_vendormaster', 'ethnicity.ethnicity', 'Left Join\r\n  ethnicity On customer_vendormaster.ETHNICITY_ID = ethnicity.id', 'Left Outer', 1, 15, NULL, NULL),
	(48, 'Company Information', 'customer_vendormaster', 'DIVERSENOTES', 'Diverse Notes', 'customer_vendormaster', 'customer_vendormaster.DIVERSENOTES', NULL, NULL, 1, 16, NULL, NULL),
	(49, 'Company Information', 'customer_vendormaster', 'VENDORNOTES', 'Vendor Notes', 'customer_vendormaster', 'customer_vendormaster.VENDORNOTES', NULL, NULL, 1, 17, NULL, NULL),
	(50, 'Company Information', 'customer_vendormaster', 'VENDORSTATUS', 'Vendor Status', 'customer_vendormaster', '(CASE WHEN customer_vendormaster.VENDORSTATUS =\'N\' THEN \n\'New Registration\' WHEN customer_vendormaster.VENDORSTATUS =\'A\' THEN\n \'Vetted by Supplier Diversity\' WHEN customer_vendormaster.VENDORSTATUS =\'B\' THEN \'BP Supplier\' End)VENDORSTATUS', NULL, NULL, 1, 18, NULL, NULL),
	(51, 'Company Information', 'customer_vendormaster', 'VENDORDESCRIPTION', 'Vendor Description', 'customer_vendormaster', 'customer_vendormaster.VENDORDESCRIPTION', NULL, NULL, 1, 19, NULL, NULL),
	(52, 'Company Information', 'customer_vendormaster', 'STATEINCORPORATION', 'State Of Incorporation', 'customer_vendormaster', 'customer_vendormaster.STATEINCORPORATION', NULL, NULL, 1, 20, NULL, NULL),
	(53, 'Company Information', 'customer_vendormaster', 'STATESALESTAXID', 'State Sales Tax ID', 'customer_vendormaster', 'customer_vendormaster.STATESALESTAXID', NULL, NULL, 1, 21, NULL, NULL),
	(54, 'Company Information', 'customer_vendormaster', 'COMPANYOWNERSHIP', 'Company Ownership', 'customer_vendormaster', '(case \nwhen customer_vendormaster.COMPANYOWNERSHIP =1 then \'Publicly Traded\'\nwhen customer_vendormaster.COMPANYOWNERSHIP =2 then \'Privately Owned\'\nEnd)CompanyOwnership', NULL, NULL, 1, 22, NULL, NULL),
	(55, 'Company Information', 'customer_vendormaster', 'BUSINESSTYPE', 'Business Type', 'customer_vendormaster', 'customer_businesstype.TypeName', 'Left Join\r\n  customer_businesstype On customer_vendormaster.BUSINESSTYPE =\r\n    customer_businesstype.Id', 'Left Outer', 1, 23, NULL, NULL),
	(56, 'Company Information', 'customer_vendormaster', 'COMPANYINFORMATION', 'Company Information', 'customer_vendormaster', 'customer_vendormaster.COMPANYINFORMATION', NULL, NULL, 1, 24, NULL, NULL),
	(57, 'Vendor NAICS', 'customer_vendornaics', 'CAPABILITIES', 'Capabilites', 'customer_vendornaics', 'customer_vendornaics.CAPABILITIES', 'Left Join customer_vendornaics On customer_vendornaics.VENDORID = customer_vendormaster.ID', NULL, 8, 2, NULL, NULL),
	(58, 'Vendor NAICS', 'customer_vendornaics', 'NAICSID', 'NAICS', 'customer_vendornaics', 'concat(customer_naicscode.NAICSCODE,\' - \',customer_naicscode.NAICSDESCRIPTION)', 'Left Join customer_vendornaics On customer_vendornaics.VENDORID = customer_vendormaster.ID', 'Left Outer', 8, 3, 'Inner Join customer_naicscode On customer_vendornaics.NAICSID = customer_naicscode.NAICSID', NULL),
	(59, 'Other Certificates', 'customer_vendorothercertificate', 'EXPIRYDATE', 'Expiry Date', 'customer_vendorothercertificate', 'DATE_FORMAT(customer_vendorothercertificate.EXPIRYDATE, \'%d/%m/%Y\')', 'Left Join  customer_vendorothercertificate On customer_vendorothercertificate.VENDORID = customer_vendormaster.ID', NULL, 9, 1, NULL, NULL),
	(60, 'Other Certificates', 'customer_vendorothercertificate', 'CERTIFICATIONTYPE', 'Certification Type', 'customer_vendorothercertificate', 'othercertificate.CERTIFICATENAME', 'Left Join  customer_vendorothercertificate On customer_vendorothercertificate.VENDORID = customer_vendormaster.ID', 'left outer', 9, 2, 'Inner Join  customer_certificatemaster as othercertificate  On customer_vendorothercertificate.CERTIFICATIONTYPE = othercertificate.ID', NULL),
	(61, 'Other Certificates', 'customer_vendorothercertificate', 'FILENAME', 'File Name', 'customer_vendorothercertificate', 'customer_vendorothercertificate.FILENAME', 'Left Join  customer_vendorothercertificate On customer_vendorothercertificate.VENDORID = customer_vendormaster.ID', NULL, 9, 3, NULL, NULL),
	(63, 'Vendor Owner', 'customer_vendorowner', 'OWNERNAME', 'Owner Name', 'customer_vendorowner', 'customer_vendorowner.OWNERNAME', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 1, NULL, NULL),
	(64, 'Vendor Owner', 'customer_vendorowner', 'TITLE', 'Title', 'customer_vendorowner', 'customer_vendorowner.TITLE', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 2, NULL, NULL),
	(65, 'Vendor Owner', 'customer_vendorowner', 'EMAIL', 'Email', 'customer_vendorowner', 'customer_vendorowner.EMAIL', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 3, NULL, NULL),
	(66, 'Vendor Owner', 'customer_vendorowner', 'PHONE', 'Phone', 'customer_vendorowner', 'customer_vendorowner.PHONE', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 4, NULL, NULL),
	(67, 'Vendor Owner', 'customer_vendorowner', 'EXTENSION', 'Extension', 'customer_vendorowner', 'customer_vendorowner.EXTENSION', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 5, NULL, NULL),
	(68, 'Vendor Owner', 'customer_vendorowner', 'MOBILE', 'Mobile', 'customer_vendorowner', 'customer_vendorowner.MOBILE', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 6, NULL, NULL),
	(69, 'Vendor Owner', 'customer_vendorowner', 'GENDER', 'Gender', 'customer_vendorowner', 'case\r\n WHEN customer_vendorowner.GENDER=\'M\' then \'Male\'\r\n WHEN customer_vendorowner.GENDER=\'F\' then \'Female\'\r\nend', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 7, NULL, NULL),
	(70, 'Vendor Owner', 'customer_vendorowner', 'OWNERSETHINICITY', 'Owner Ethnicity ', 'customer_vendorowner', 'ownerEthnicity.ethnicity', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', 'Left Outer', 4, 8, 'Left Join ethnicity ownerEthnicity  On customer_vendorowner.OWNERSETHINICITY = ownerEthnicity.ID', NULL),
	(71, 'Vendor Owner', 'customer_vendorowner', 'OWNERSHIPPERCENTAGE', 'Ownership Percentage', 'customer_vendorowner', 'customer_vendorowner.OWNERSHIPPERCENTAGE', 'Left Join\r\n  customer_vendorowner On customer_vendorowner.VENDORID =\r\n    customer_vendormaster.ID', NULL, 4, 9, NULL, NULL),
	(72, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEADDRESS', 'Ref Address', 'customer_vendorreference', 'customer_vendorreference.REFERENCEADDRESS', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 1, NULL, NULL),
	(73, 'Vendor Reference', 'customer_vendorreference', 'REFERENCECITY', 'Ref City', 'customer_vendorreference', 'customer_vendorreference.REFERENCECITY', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 2, NULL, NULL),
	(74, 'Vendor Reference', 'customer_vendorreference', 'REFERENCECOUNTRY', 'Ref Country', 'customer_vendorreference', 'country.countryname', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', 'Left Outer', 12, 3, 'Left Join  country On customer_vendorreference.REFERENCECOUNTRY = country.Id', NULL),
	(75, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEEMAILID', 'Ref Email ID', 'customer_vendorreference', 'customer_vendorreference.REFERENCEEMAILID', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 4, NULL, NULL),
	(76, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEMOBILE', 'Ref Mobile', 'customer_vendorreference', 'customer_vendorreference.REFERENCEMOBILE', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 5, NULL, NULL),
	(77, 'Vendor Reference', 'customer_vendorreference', 'REFERENCENAME', 'Ref Name', 'customer_vendorreference', 'customer_vendorreference.REFERENCENAME', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 6, NULL, NULL),
	(78, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEPHONE', 'Ref Phone', 'customer_vendorreference', 'customer_vendorreference.REFERENCEPHONE', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 7, NULL, NULL),
	(79, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEEXTENSION', 'Ref Extension', 'customer_vendorreference', 'customer_vendorreference.REFERENCEEXTENSION', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 8, NULL, NULL),
	(80, 'Vendor Reference', 'customer_vendorreference', 'REFERENCESTATE', 'Ref State', 'state', 'state.statename', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', 'Left Outer', 12, 9, ' Left Join state On customer_vendorreference.REFERENCESTATE = state.ID', NULL),
	(81, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEZIP', 'Ref Zip', 'customer_vendorreference', 'customer_vendorreference.REFERENCEZIP', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 10, NULL, NULL),
	(82, 'Vendor Reference', 'customer_vendorreference', 'REFERENCEPROVINCE', 'Ref Province', 'customer_vendorreference', 'customer_vendorreference.REFERENCEPROVINCE', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 11, NULL, NULL),
	(83, 'Vendor Reference', 'customer_vendorreference', 'REFERENCECOMPANYNAME', 'Ref Company Name', 'customer_vendorreference', 'customer_vendorreference.REFERENCECOMPANYNAME', 'Left Join\r\n  customer_vendorreference On customer_vendorreference.VENDORID =\r\n    customer_vendormaster.ID', NULL, 12, 12, NULL, NULL),
	(84, 'Vendor service area', 'customer_vendorservicearea', 'SERVICEAREAID', 'Service Area ID', 'customer_vendorservicearea', 'customer_servicearea.SERVICEAREANAME', 'Left Join  customer_vendorservicearea On customer_vendorservicearea.VENDORID =   customer_vendormaster.ID\r\n', 'Left Outer', 10, 1, ' Inner Join  customer_servicearea On customer_vendorservicearea.SERVICEAREAID =    customer_servicearea.ID', NULL),
	(85, 'Vendor Business ', 'vendor_businessarea', 'BUSINESSAREAID', 'Business Area ID', 'customer_businessarea', 'customer_businessarea.AREANAME', 'Left Join vendor_businessarea On vendor_businessarea.VENDORID = customer_vendormaster.ID', 'Left Outer', 11, 1, 'Inner Join  customer_businessarea On vendor_businessarea.BUSINESSAREAID =  customer_businessarea.ID', NULL);

------------------------------------------------------------------------------------------------------------------------------
--- Author :SHEFEEK A     Date : 29-08-2014 ---
ALTER TABLE `customer_savesearchfilter`
	CHANGE COLUMN `SEARCHTYPE` `SEARCHTYPE` CHAR(2) NOT NULL COMMENT '\'(V)endor status\', \'Vendor (C)riteria\', \'(P)rime Vendor Search\',\'custom vendor (R)eport\',\'Vendor Criteria Dynamic(CD)\',\'Prime Vendor Search Dynamic(VD)\'' AFTER `USERID`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A  Date : 08-09-2014 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-06 13:24:17', 1, '2014-09-06 13:24:20', 'Registered Vendors', 'Registered Vendors', 'menu', 'R');

UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=16; (Spend Report)
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=17; (Diversity Report)
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=49; (Tier 2 Reporting Ethnicity Breakdown)	
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A    Date : 24-09-2014 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:11:17', 1, '2014-09-24 15:11:20', 'Diversity Analysis Dashboard', 'Diversity Analysis Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:14:18', 1, '2014-09-24 15:14:19', 'Vendor Status Breakdown Dashboard', 'Vendor Status Breakdown Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:15:17', 1, '2014-09-24 15:15:19', 'Supplier Count By State Dashboard', 'Supplier Count By State Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:16:06', 1, '2014-09-24 15:16:07', 'BP Vendor Count By State Dashboard', 'BP Vendor Count By State Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:16:53', 1, '2014-09-24 15:16:54', 'Spend By Agency Dashboard', 'Spend By Agency Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:17:46', 1, '2014-09-24 15:17:47', 'Prime Supplier Report by Tier 2 Total Spend Dashboard', 'Prime Supplier Report by Tier 2 Total Spend Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:18:36', 1, '2014-09-24 15:18:38', 'Prime Supplier Report by Tier 2 Direct Spend Dashboard', 'Prime Supplier Report by Tier 2 Direct Spend Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:19:24', 1, '2014-09-24 15:19:25', 'Prime Supplier Report by Tier 2 Indirect Spend Dashboard', 'Prime Supplier Report by Tier 2 Indirect Spend Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:20:38', 1, '2014-09-24 15:20:40', 'Tier 2 Vendor Name Report Dashboard', 'Tier 2 Vendor Name Report Dashboard', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-09-24 15:21:21', 1, '2014-09-24 15:21:22', 'Tier 2 Reporting Summary Dashboard', 'Tier 2 Reporting Summary Dashboard', 'menu', 'D');	

UPDATE `vms_objects` SET `APPLICATIONCATEGORY`='D' WHERE  `ID`=45;

UPDATE `vms_objects` SET `APPLICATIONCATEGORY`='D' WHERE  `ID`=55;	
------------------------------------------------------------------------------------------------------------------------------
--- Author : SHEFEEK A  Date : 25-09-2014 ---
UPDATE `avms_custom_report_field_selection` SET `outputsql`='(CASE WHEN customer_vendormaster.VENDORSTATUS =\'N\' THEN \n\'New Registration\' WHEN customer_vendormaster.VENDORSTATUS =\'A\' THEN\n \'Vetted by Supplier Diversity\' WHEN customer_vendormaster.VENDORSTATUS =\'B\' THEN \'BP Supplier\' \nWHEN customer_vendormaster.VENDORSTATUS =\'P\' THEN \'Pending Review\' End)VENDORSTATUS' WHERE  `id`=50;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 30-09-2014 ---
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=37;	---(Dashboard Settings)---

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 01-10-2014 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:40:42', 1, '2014-10-01 15:40:46', 'Contact Information', 'Contact Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:45:06', 1, '2014-10-01 15:45:08', 'Company Information', 'Company Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:46:23', 1, '2014-10-01 15:46:24', 'Company Address Information', 'Company Address Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:47:20', 1, '2014-10-01 15:47:21', 'Company Ownership Information', 'Company Ownership Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:48:02', 1, '2014-10-01 15:48:04', 'Diverse Classification Information', 'Diverse Classification Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:48:58', 1, '2014-10-01 15:49:00', 'Service Area Information', 'Service Area Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:49:32', 1, '2014-10-01 15:49:33', 'Business Area Information', 'Business Area Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:52:11', 1, '2014-10-01 15:52:12', 'Business Biography and Safety Information', 'Business Biography and Safety Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:55:56', 1, '2014-10-01 15:55:58', 'References Information', 'References Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:56:44', 1, '2014-10-01 15:56:45', 'Quality/Other Certifications Information', 'Quality/Other Certifications Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:58:04', 1, '2014-10-01 15:58:06', 'Documents Upload Information', 'Documents Upload Information', 'wizard', 'P');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-01 15:59:55', 1, '2014-10-01 15:59:57', 'Vendor Notes and Meeting Information', 'Vendor Notes and Meeting Information', 'wizard', 'P');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 02-10-2014 ---
INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Contact Information', 'Contact Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Company Information', 'Company Information', 'wizard');	

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Company Address Information', 'Company Address Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Company Ownership Information', 'Company Ownership Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Diverse Classification Information', 'Diverse Classification Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Service Area Information', 'Service Area Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Business Area Information', 'Business Area Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Business Biography and Safety Information', 'Business Biography and Safety Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'References Information', 'References Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Quality/Other Certifications Information', 'Quality/Other Certifications Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Documents Upload Information', 'Documents Upload Information', 'wizard');

INSERT INTO `vendor_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`) 
	VALUES (1, '2014-10-02 15:43:31', 'Vendor Notes and Meeting Information', 'Vendor Notes and Meeting Information', 'wizard');

--- We need to modify CREATEDBY ID, MODIFIEDBY ID, OBJECTID, VENDORUSERROLEID ---	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 9, 1);
	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 10, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 11, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 12, 1);	
	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 13, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 14, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 15, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 16, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 17, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 18, 1);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 19, 1);
	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 20, 1);

--- We need to modify CREATEDBY ID, MODIFIEDBY ID, OBJECTID, VENDORUSERROLEID ---	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 9, 2);
	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 10, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 11, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 12, 2);	
	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 13, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 14, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 15, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 16, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 17, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 18, 2);

INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 19, 2);
	
INSERT INTO `customer_vendor_rolesprivileges` (`ISADD`, `CREATEDBY`, `CREATEDON`, `ISDELETE`, `MODIFIEDBY`, `MODIFIEDON`, `ISMODIFY`, `ISVIEW`, `ISVISIBLE`, `OBJECTID`, `VENDORUSERROLEID`) 
	VALUES (1, 3777, '2014-10-02 20:30:28', 1, 3777, '2014-10-02 20:30:38', 1, 1, 1, 20, 2);	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 07-10-2014 ---
CREATE TABLE `stakeholders_registration` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`EMAILID` VARCHAR(255) NOT NULL,
	`FIRSTNAME` VARCHAR(255) NOT NULL,
	`LASTNAME` VARCHAR(255) NULL DEFAULT NULL,
	`MANAGERNAME` VARCHAR(255) NOT NULL,
	`REASON` LONGTEXT NOT NULL,
	`CREATEDBY` INT(11) NOT NULL,
	`CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`ISAPPROVED` VARCHAR(255) NOT NULL DEFAULT 'Not Approved' COMMENT 'Not Approved, Approved, Unapproved',
	`APPROVEDBY` INT(11) NULL DEFAULT NULL,
	`APPROVEDON` DATETIME NULL DEFAULT NULL,
	`CUSTOMERDIVISIONID` INT(11) NULL DEFAULT NULL,
	UNIQUE INDEX `EMAILID` (`EMAILID`),
	PRIMARY KEY (`ID`),
	CONSTRAINT `FK__customer_division` FOREIGN KEY (`CUSTOMERDIVISIONID`) REFERENCES `customer_division` (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

ALTER TABLE `stakeholders_registration`
	CHANGE COLUMN `CREATEDBY` `CREATEDBY` INT(11) NOT NULL DEFAULT '0' AFTER `REASON`;

ALTER TABLE `customer_vendorbusinessbiographysafety`
	CHANGE COLUMN `ANSWER` `ANSWER` VARCHAR(60000) NULL DEFAULT NULL AFTER `ANSWERTYPE`;	

INSERT INTO `customer_division` (`ID`, `DIVISIONNAME`, `DIVISIONSHORTNAME`, `CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`) 
	VALUES (3, 'Both', 'BP Companies', 1, '2014-10-09 14:47:15', 139, '2014-10-09 14:47:17');

---1.Create Folders[P66, FDOT, BP] under log folder---
---2.Create Folders[P66, FDOT, BP] under ExcelTemplate folder---	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 10-10-2014 ---
UPDATE `vms_objects` SET `APPLICATIONCATEGORY`='P' WHERE  `ID`=41; ---View Modified Date---

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-10 17:59:00', 1, '2014-10-10 17:59:03', 'Update Status', 'Update Status', 'button', 'P');
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 14-10-2014 ---	
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-14 19:33:18', 1, '2014-10-14 19:33:20', 'Credentialing', 'Credentialing', 'menu', 'A');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 16-10-2014 ---	
UPDATE `statusmaster` SET `statusname`='Reviewed by Supplier Diversity', 
	`statusdescription`='Reviewed by Supplier Diversity is the 3rd status. This means the vendor has been vetted and is active. However, BP has not spent any money with them yet' 
	WHERE  `id`='A';

UPDATE `avms_custom_report_field_selection` 
	SET `outputsql`='(CASE WHEN customer_vendormaster.VENDORSTATUS =\'N\' THEN \n\'New Registration\' WHEN customer_vendormaster.VENDORSTATUS =\'A\' THEN\n \'Reviewed by Supplier Diversity\' WHEN customer_vendormaster.VENDORSTATUS =\'B\' THEN \'BP Supplier\' \nWHEN customer_vendormaster.VENDORSTATUS =\'P\' THEN \'Pending Review\' End)VENDORSTATUS' 
	WHERE  `id`=50;

--for Fulltext Search--
CASE vendor.VENDORSTATUS 
	WHEN 'A' THEN 'Reviewed by Supplier Diversity' 
	WHEN 'B' THEN 'BP Vendor' WHEN 'N' THEN 'New Registration' 
	WHEN 'P' THEN 'Pending Review' END AS VENDORSTATUS
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 27-10-2014 ---		
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-27 19:10:50', 1, '2014-10-27 19:10:59', 'New Registrations By Month Dashboard', 'New Registrations By Month Dashboard', 'menu', 'D');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 31-10-2014 ---		
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `MODIFIEDBY`, `MODIFIEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-10-31 15:48:57', 1, '2014-10-31 15:48:58', 'Keyword Search', 'Keyword Search', 'menu', 'V');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 04-11-2014 ---			
ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `INTERNATIONAL_MODE` TINYINT(4) NULL DEFAULT '0' COMMENT 'Yes = 1 and No = 0' AFTER `DOCUMENT_SIZE`;
	
------------------------------------------------------------------------------------------------------------------------------
--- Author :  Mahan    Date : 13-11-2014 ---

	CREATE TABLE `customer_segmentmaster` (
 `ID` INT(11) NOT NULL AUTO_INCREMENT,
 `SEGMENTNAME` VARCHAR(255) NOT NULL,
 `ISACTIVE` TINYINT(4) NOT NULL DEFAULT '1',
 `CREATEDBY` INT(11) NOT NULL,
 `CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `MODIFIEDBY` INT(11) NULL DEFAULT NULL,
 `MODIFIEDON` DATETIME NULL DEFAULT NULL,
 PRIMARY KEY (`ID`),
 UNIQUE INDEX `SEGMENTNAME` (`SEGMENTNAME`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

INSERT INTO `customer_segmentmaster` (`SEGMENTNAME`, `CREATEDBY`, `CREATEDON`) VALUES ('Alternative Energy', 123, '2014-11-10 22:48:44');
INSERT INTO `customer_segmentmaster` (`SEGMENTNAME`, `CREATEDBY`, `CREATEDON`) VALUES ('Functions', 123, '2014-11-10 22:49:53');
INSERT INTO `customer_segmentmaster` (`SEGMENTNAME`, `CREATEDBY`, `CREATEDON`) VALUES ('Refining and Marketing', 123, '2014-11-10 22:50:26');
INSERT INTO `customer_segmentmaster` (`SEGMENTNAME`, `CREATEDBY`, `CREATEDON`) VALUES ('Upstream', 123, '2014-11-10 22:50:52');
INSERT INTO `customer_segmentmaster` (`SEGMENTNAME`, `CREATEDBY`, `CREATEDON`) VALUES ('Indirect Procurement', 123, '2014-11-10 22:51:21');

ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `BP_SEGMENT` TINYINT(4) NULL DEFAULT '0' COMMENT 'Yes = 1 and No = 0' AFTER `INTERNATIONAL_MODE`;
	
ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `STATUS` CHAR(1) NULL DEFAULT NULL AFTER `BP_SEGMENT`,
 	ADD CONSTRAINT `FK_customer_vendorwfconfig_statusmaster` FOREIGN KEY (`STATUS`) REFERENCES `statusmaster` (`id`);
 	
ALTER TABLE `customer_vendormaster`
	ADD COLUMN `BPSEGMENT` INT(11) NULL DEFAULT NULL AFTER `VENDORSTATUS`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 17-11-2014 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2014-11-17 19:56:43', 'BP Segment', 'BP Segment', 'wizard', 'P');

CREATE TABLE `customer_vendornotes` (
 `ID` INT(11) NOT NULL AUTO_INCREMENT,
 `VENDORID` INT(11) NULL DEFAULT NULL,
 `NOTES` VARCHAR(20000) NULL DEFAULT NULL, 
 `CREATEDBY` INT(11) NOT NULL DEFAULT '0',
 `CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
 PRIMARY KEY (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

ALTER TABLE `customer_vendornotes`
 ADD CONSTRAINT `FK_customer_vendornotes_customer_vendormaster` FOREIGN KEY (`VENDORID`) REFERENCES `customer_vendormaster` (`ID`);	

INSERT INTO customer_vendornotes (VENDORID, NOTES, CREATEDBY, CREATEDON) SELECT v.ID, v.VENDORNOTES, v.CREATEDBY, v.CREATEDON FROM customer_vendormaster v WHERE v.VENDORNOTES IS NOT NULL AND v.VENDORNOTES != '';
UPDATE customer_vendornotes v SET v.CREATEDBY = 139 WHERE v.CREATEDBY = -1;
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 26-11-2014 ---
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=15; ---Approve Vendors---
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=13; ---Vendor Approve---
UPDATE `vms_objects` SET `ISACTIVE`=0 WHERE  `ID`=7; ---NAICS Category Master---

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 28-11-2014 ---
---In full text Search line no:16---
,
(SELECT cvn.NOTES from customer_vendornotes cvn where cvn.VENDORID = vendor.ID 
ORDER BY cvn.CREATEDON DESC LIMIT 1) AS VENDORNOTES

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 17-12-2014 ---
INSERT INTO `email_type` (`ID` ,`DESCRIPTION`, `CREATEDBY`, `CREATEDON`, `TEMPLATETYPE`) 
	VALUES (23, 'Customer User Account Registration - Admin', 6, '2014-12-17 19:27:35', 'S');
	
INSERT INTO `email_type` (`ID` ,`DESCRIPTION`, `CREATEDBY`, `CREATEDON`, `TEMPLATETYPE`) 
	VALUES (24 ,'Customer User Registration Credentials', 6, '2014-12-17 19:27:35', 'S');

INSERT INTO `email_type` (`ID` ,`DESCRIPTION`, `CREATEDBY`, `CREATEDON`, `TEMPLATETYPE`) 
	VALUES (25 ,'Customer User Registration Unapproved', 6, '2014-12-17 19:27:35', 'S');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 18-12-2014 ---
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='View User Log', `OBJECTNAME`='View User Log' WHERE  `ID`=53;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 22-12-2014 ---
ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `CERTIFICATE_EXPIRY_EMAIL_SUMMARY_ALERT` TINYINT(4) NULL DEFAULT NULL AFTER `STATUS`;	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 29-12-2014 ---
INSERT INTO `email_type` (`ID` ,`DESCRIPTION`, `CREATEDBY`, `CREATEDON`, `TEMPLATETYPE`) 
	VALUES (26 ,'Approved Customer User Account Creation Alert', 6, '2014-12-17 19:27:35', 'S');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 08-01-2015 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-01-08 19:50:45', 'Vendors By Industry', 'Vendors By Industry', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-01-08 19:52:51', 'BP Vendors By Industry', 'BP Vendors By Industry', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-01-09 16:05:12', 'Vendors By Industry Report', 'Vendors By Industry Report', 'menu', 'R');	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 29-01-2015 ---	
ALTER TABLE `reportdue`
	CHANGE COLUMN `ISSUBMITTED` `ISSUBMITTED` VARCHAR(50) NULL DEFAULT NULL AFTER `SUBMITTEDON`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 30-01-2015 ---
---For Both the DB (P66_live & Master)---	
ALTER TABLE `customer_applicationsettings`
	ADD COLUMN `PRIME_TRAINING_URL` VARCHAR(255) NULL DEFAULT NULL AFTER `PRIVACY_TERMS`,
	ADD COLUMN `NONPRIME_TRAINING_URL` VARCHAR(255) NULL DEFAULT NULL AFTER `PRIME_TRAINING_URL`;	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 02-03-2015 ---
Modified "export_selected_vendor_sp" in Database
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 13-03-2015 ---
---Vendor---
UPDATE `vms_objects` SET `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Create Vendor';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Search Vendor All Statuses', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'View Vendors';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Email Vendor - General' WHERE OBJECTNAME = 'Mail Notifications';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Review Assessments', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Assessment Review';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='View Assessment Emails', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Assessment Email Details';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Assessment Scorecard', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Assessment Score Card';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Email Vendor - Assessment', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Assessment Email Notification';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Search for a Vendor' WHERE OBJECTNAME = 'Search Vendor';

---Report---
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Registered Vendors Report' WHERE OBJECTNAME = 'Registered Vendors';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Vendors By Industry' WHERE OBJECTNAME = 'Vendors By Industry Report';

---Administration---
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Manage Roles', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Roles';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Manage Users', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'User';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='NAICS Code', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'NAICS Master';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Manage Classifications', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'View Certificate';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Manage Certifying Agencies', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Certifying Agency';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Edit Customer Info' WHERE OBJECTNAME = 'Edit Information';
UPDATE `vms_objects` SET `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Performance Assessment Template';
UPDATE `vms_objects` SET `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Email Distribution';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Workflow Configuration', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Workflow';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Tier2 Reporting Period', `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Reporting Period';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Country and State' WHERE OBJECTNAME = 'Country';
UPDATE `vms_objects` SET `OBJECTTYPE`='menu' WHERE OBJECTNAME = 'Manage Division';

---Dashboard---
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Diversity Analysis' WHERE OBJECTNAME = 'Diversity Analysis Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='New Registrations By Month' WHERE OBJECTNAME = 'New Registrations By Month Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='BP Vendor Count By State' WHERE OBJECTNAME = 'BP Vendor Count By State Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Prime Supplier Report by Tier 2 Direct Spend' WHERE OBJECTNAME = 'Prime Supplier Report by Tier 2 Direct Spend Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Prime Supplier Report by Tier 2 Indirect Spend' WHERE OBJECTNAME = 'Prime Supplier Report by Tier 2 Indirect Spend Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Prime Supplier Report by Tier 2 Total Spend' WHERE OBJECTNAME = 'Prime Supplier Report by Tier 2 Total Spend Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Prime Supplier Report T2 Spend by Agency' WHERE OBJECTNAME = 'Spend By Agency Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Supplier Count By State' WHERE OBJECTNAME = 'Supplier Count By State Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Tier 2 Reporting Summary' WHERE OBJECTNAME = 'Tier 2 Reporting Summary Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Tier 2 Vendor Name Report' WHERE OBJECTNAME = 'Tier 2 Vendor Name Report Dashboard';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Vendor Status Breakdown' WHERE OBJECTNAME = 'Vendor Status Breakdown Dashboard';

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 17-03-2015 ---
UPDATE customer_vendormaster c SET c.COMPANYINFORMATION = REPLACE(c.COMPANYINFORMATION,'&lt;','<'), c.VENDORDESCRIPTION = REPLACE(c.VENDORDESCRIPTION,'&lt;','<');
UPDATE customer_vendormaster c SET c.COMPANYINFORMATION = REPLACE(c.COMPANYINFORMATION,'&gt;','>'), c.VENDORDESCRIPTION = REPLACE(c.VENDORDESCRIPTION,'&gt;','>');
UPDATE customer_vendormaster c SET c.COMPANYINFORMATION = REPLACE(c.COMPANYINFORMATION,'&amp;','&'), c.VENDORDESCRIPTION = REPLACE(c.VENDORDESCRIPTION,'&amp;','&');
UPDATE customer_vendormaster c SET c.COMPANYINFORMATION = REPLACE(c.COMPANYINFORMATION,'&quot;','"'), c.VENDORDESCRIPTION = REPLACE(c.VENDORDESCRIPTION,'&quot;','"');

ALTER TABLE `customer_vendormaster`
	CHANGE COLUMN `VENDORDESCRIPTION` `VENDORDESCRIPTION` VARCHAR(5000) NULL DEFAULT NULL AFTER `STATUSMODIFIEDON`,
	CHANGE COLUMN `COMPANYINFORMATION` `COMPANYINFORMATION` VARCHAR(5000) NULL DEFAULT NULL AFTER `ISPARTIALLYSUBMITTED`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 25-03-2015 ---	
ALTER TABLE `certificateexpirationnotification`
	CHANGE COLUMN `SUBMITTEDBY` `SUBMITTEDBY` INT(11) NULL DEFAULT NULL AFTER `SUBMITTEDON`;
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : SHEFEEK A     Date : 27-03-2015 ---		
ALTER TABLE `customer_division`
	ADD COLUMN `ISGLOBAL` TINYINT NULL DEFAULT NULL AFTER `REGISTRATIONQUESTION`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 30-03-2015 ---
INSERT INTO `customer_division` (`DIVISIONNAME`, `DIVISIONSHORTNAME`, `CREATEDBY`, `CREATEDON`, `ISGLOBAL`) 
	VALUES ('BP Global', 'BP Global', 1, '2015-03-30 22:32:26', 1);

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 03-04-2015 ---
ALTER TABLE `customer_division`
	CHANGE COLUMN `ISGLOBAL` `ISGLOBAL` TINYINT(4) NULL DEFAULT NULL COMMENT 'It Should be 1 for BP Global Only not for Others' AFTER `REGISTRATIONQUESTION`;

ALTER TABLE `customer_division`
	ADD UNIQUE INDEX `ISGLOBAL` (`ISGLOBAL`);	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 06-04-2015 ---	
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Vendors By Business Type', `OBJECTNAME`='Vendors By Business Type Report' WHERE  `OBJECTNAME`='Vendors By Industry Report';

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 07-04-2015 ---
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='Vendors By Business Type', `OBJECTNAME`='Vendors By Business Type' WHERE  `OBJECTNAME`='Vendors By Industry';
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='BP Vendors By Business Type', `OBJECTNAME`='BP Vendors By Business Type' WHERE  `OBJECTNAME`='BP Vendors By Industry';

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 08-04-2015 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (11, '2015-04-08 13:14:22', 'Vendors By NAICS Description', 'Vendors By NAICS Description Report', 'menu', 'R');
	
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-04-08 18:29:14', 'Vendors By BP Market Sector', 'Vendors By BP Market Sector Report', 'menu', 'R');

ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `REPORT_NOT_SUBMITTED_REMINDER_DATE` INT(10) NULL DEFAULT '0' AFTER `CERTIFICATE_EXPIRY_EMAIL_SUMMARY_ALERT`;
	
------------------------------------------------------------------------------------------------------------------------------
--- Author : SHEFEEK A     Date : 02-04-2015 ---	
CREATE TABLE `customer_userdivision` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`CUSTOMERDIVISIONID` INT(11) NULL DEFAULT NULL,
	`USERID` INT(11) NULL DEFAULT NULL,
	`ISACTIVE` TINYINT(4) NOT NULL DEFAULT '0',
	`CREATEDBY` INT(11) NOT NULL,
	`CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`MODIFIEDBY` INT(11) NULL DEFAULT NULL,
	`MODIFIEDON` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`ID`),
	INDEX `FK_cutomer_userdivision_customer_division` (`CUSTOMERDIVISIONID`),
	INDEX `FK_cutomer_userdivision_users` (`USERID`),
	CONSTRAINT `FK_cutomer_userdivision_customer_division` FOREIGN KEY (`CUSTOMERDIVISIONID`) REFERENCES `customer_division` (`ID`),
	CONSTRAINT `FK_cutomer_userdivision_users` FOREIGN KEY (`USERID`) REFERENCES `users` (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 10-04-2015 ---
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-04-09 18:31:49', 'Vendors By NAICS Description', 'Vendors By NAICS Description', 'menu', 'D');

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-04-09 18:32:36', 'Vendors By BP Market Sector', 'Vendors By BP Market Sector', 'menu', 'D');

INSERT INTO customer_userdivision (CUSTOMERDIVISIONID, USERID, ISACTIVE, CREATEDBY) 
	SELECT u.CUSTOMERDIVISIONID, u.ID, 1, 1 FROM users u WHERE u.CUSTOMERDIVISIONID IS NOT NULL;

------------------------------------------------------------------------------------------------------------------------------
--- Author : SHEFEEK A     Date : 16-04-2015 ---
--- Check the id for email template and email template parameter ---
INSERT INTO `customer_emailtemplateparameters` (`ID`, `EMAILTEMPLATEID`, `PARAMETERNAME`, `CREATEDON`, `CREATEDBY`) 
	VALUES (47, 12, '#startDate', '2015-04-16 23:04:38', 123);

INSERT INTO `customer_emailtemplateparameters` (`ID`, `EMAILTEMPLATEID`, `PARAMETERNAME`, `CREATEDON`, `CREATEDBY`) 
	VALUES (48, 12, '#endDate', '2015-04-16 23:05:46', 123);

------------------------------------------------------------------------------------------------------------------------------
--- Author : SHEFEEK A     Date : 20-04-2015 ---
--- Remove the parameter divisionid and recreate using data type varchar in Full Text Search Stored Procedures---
change `divisionid` as  VARCHAR(50)

//Change Division id condition
case when divisionid='' THEN 0=0
   when divisionid <> '' THEN FIND_IN_SET(vendor.CUSTOMERDIVISIONID, divisionid) END
      
ALTER TABLE `customer_division`
	DROP INDEX `ISGLOBAL`;

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 28-04-2015 ---	
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='BP Vendors By BP Market Sector', `OBJECTNAME`='BP Vendors By BP Market Sector' WHERE `OBJECTNAME`='Vendors By BP Market Sector';

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A     Date : 05-05-2015 ---	
ALTER TABLE `customer_certificatetype`
	ADD COLUMN `ISCERTIFICATEEXPIRYALERTREQUIRED` TINYINT(4) NOT NULL DEFAULT '0' AFTER `SHORTDESCRIPTION`;

INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-05-05 22:02:57', 'Inactive Vendor Search', 'Inactive Vendor Search', 'menu', 'V');	

------------------------------------------------------------------------------------------------------------------------------
--- Author :SHEFEEK A      Date : 08-05-2015 ---
CREATE TABLE `indirect_procurement_marketsectors` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`MARKETSECTORID` INT(11) NOT NULL,
	`REPORTNAME` VARCHAR(1000) NOT NULL,
	`CREATEDBY` INT(11) NOT NULL,
	`CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`ID`),
	CONSTRAINT `FK__customer_marketsector` FOREIGN KEY (`MARKETSECTORID`) REFERENCES `customer_marketsector` (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

--- Author :Asarudeen A      Date : 08-05-2015 ---
INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (10, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');
	
INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (16, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');

INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (17, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');		
	
INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (28, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');
	
INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (29, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');
	
INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (36, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');
	
INSERT INTO `indirect_procurement_marketsectors` (`MARKETSECTORID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (37, 'Indirect Procurement Commodities', 102, '2015-05-08 20:40:57');
	
INSERT INTO `vms_objects` (`CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) 
	VALUES (1, '2015-05-08 22:33:38', 'Indirect Procurement Commodities Report', 'Indirect Procurement Commodities Report', 'menu', 'R');	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A      Date : 12-05-2015 ---
ALTER TABLE `statusmaster`
	ADD COLUMN `iscertificateexpiryalertreq` TINYINT(4) NOT NULL DEFAULT '0' AFTER `createdon`;
	
UPDATE `statusmaster` SET `iscertificateexpiryalertreq`=1 WHERE  `id`='B';

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A   Date : 14-05-2015 ---
CREATE TABLE `indirect_procurement_certificatetypes` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`CERTIFICATETYPEID` INT(11) NOT NULL,
	`REPORTNAME` VARCHAR(1000) NOT NULL,
	`CREATEDBY` INT(11) NOT NULL,
	`CREATEDON` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`ID`),
	CONSTRAINT `FK__customer_certificatetype` FOREIGN KEY (`CERTIFICATETYPEID`) REFERENCES `customer_certificatetype` (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

SELECT * FROM customer_certificatetype c where c.DESCRIPTION IN ('NMSDC','WBENC');

INSERT INTO `indirect_procurement_certificatetypes` (`CERTIFICATETYPEID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (1, 'Indirect Procurement Commodities', 102, '2015-05-14 20:24:35');

INSERT INTO `indirect_procurement_certificatetypes` (`CERTIFICATETYPEID`, `REPORTNAME`, `CREATEDBY`, `CREATEDON`) 
	VALUES (3, 'Indirect Procurement Commodities', 102, '2015-05-14 20:24:35');

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A   Date : 20-05-2015 ---
UPDATE `vms_objects` SET `OBJECTDESCRIPTION`='BP Market Sector Search Report', `OBJECTNAME`='BP Market Sector Search Report' 
	WHERE  `OBJECTNAME`='Indirect Procurement Commodities Report';
	
UPDATE `indirect_procurement_marketsectors` SET `REPORTNAME`='BP Market Sector Search' 
	WHERE  `REPORTNAME`='Indirect Procurement Commodities';	

UPDATE `indirect_procurement_certificatetypes` SET `REPORTNAME`='BP Market Sector Search' 
	WHERE  `REPORTNAME`='Indirect Procurement Commodities';	

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A   Date : 06-08-2015 ---
INSERT INTO `customer_emailtemplateparameters` (`EMAILTEMPLATEID`, `PARAMETERNAME`, `CREATEDON`, `CREATEDBY`) 
	VALUES (13, '#reportingPeriod', '2015-08-06 16:50:57', 123);

---Need to Change Email Template of "Report Submitted"---
-- New Tier 2 Expenditure Report has been created #vendorName on #date for the reporting period: #reportingPeriod. --

------------------------------------------------------------------------------------------------------------------------------
--- Author : Asarudeen A   Date : 20-08-2015 ---
---Need to Change the REFERENCEID value for 'export_selected_vendor_sp' REFERENCEID=2 & REFERENCEID=3---
---Need to Add IFNULL for all the Columns in the REFERENCE Section of 'export_selected_vendor_sp'---

------------------------------------------------------------------------------------------------------------------------------
--- Author :Bhaskar      Date : 15-08-2016 ---
INSERT INTO `bpavmsystem`.`vms_objects` (`ID`, `CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) VALUES (94, 1, '2016-08-03 15:52:21', 'Vendors By BP Market Subsector Report', 'Vendors By BP Market Subsector Report', 'menu', 'R');
INSERT INTO `bpavmsystem`.`vms_objects` (`ID`, `CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) VALUES (95, 1, '2016-08-03 15:54:14', 'Vendors By BP Commodity Group Report', 'Vendors By BP Commodity Group Report', 'menu', 'R');

INSERT INTO `avms_bp`.`vms_objects` (`ID`, `CREATEDBY`, `CREATEDON`, `OBJECTDESCRIPTION`, `OBJECTNAME`, `OBJECTTYPE`, `APPLICATIONCATEGORY`) VALUES (96, 1, '2016-08-31 16:23:01', 'Prime Supplier by Tier 2 Diversity By Ethnicity', 'Prime Supplier by Tier 2 Diversity By Ethnicity', 'menu', 'R');

------------------------------------------------------------------------------------------------------------------------------
--- Author :Bhaskar      Date : 25-10-2016 ---
ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `CC_MAIL_ID` varchar(50) NULL DEFAULT NULL AFTER `REPORT_NOT_SUBMITTED_REMINDER_DATE`;
	
ALTER TABLE `customer_vendorwfconfig`
	ADD COLUMN `SUPPORT_MAIL_ID` varchar(50) NULL DEFAULT NULL AFTER `CC_MAIL_ID`;

	
	