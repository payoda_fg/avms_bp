function generateSearchResults(path, fileName) {
	var pageHead = "VENDORS";
	var pathArray = window.location.href.split('/');
	var protocol = pathArray[0];
	var host = pathArray[2];
	var parameter = pathArray[3];
	var url = protocol + '//' + host + "/" + parameter;
	var source = document.getElementById('searchvendor');
	var copy = source.cloneNode(true);

	if (fileName != null && fileName == 'SearchVendors') {
		$(copy).find("td:first-child").remove();
		$(copy).find("th:first-child").remove();
		$(copy).find("td:last-child").remove();
		$(copy).find("th:last-child").remove();
	} else if (fileName != null && fileName == 'Vendors') {
		$(copy).find("td:first-child").remove();
		$(copy).find("th:first-child").remove();
	}
	var tableValue = copy.innerHTML;
	var html = "<html><head>"
			+ "<style script=&quot;css/text&quot;>"
			+ "table.tableList_1 th {text-align:center; "
			+ "vertical-align: middle; padding:5px;}"
			+ "table.tableList_1 td {text-align: left; vertical-align: top; padding:5px;}";
	if (fileName != null
			&& (fileName == 'Vendors' || fileName == 'SearchVendors')) {
		html += "@page { size: A3 landscape;}";
	}
	html += "</style>"
			+ "</head>"
			+ "<body style=&quot;page:land;&quot;>"
			+ "<div class=&quot;logo&quot;>"
			+ "<img src=&quot;"
			+ url
			+ "/temp/"
			+ path
			+ "&quot; style=&quot;float:left;"
			+ "position:absolute;top:0px;left:0px;&quot; "
			+ "height=&quot;60px&quot; width=&quot;60px&quot; alt=&quot;Logo&quot; />"
			+ "</div><br/>"
			+ "<div class=&quot;pageHead_1&quot; style=&quot;text-align:center;padding: 5%;font-family:Verdana;&quot;><b>"
			+ pageHead
			+ "</b></div> <table align=&quot;center&quot; style=&quot;font-family:Verdana;&quot;>";
	html += tableValue + "</table></body></html>";
	html = html.replace(/'/g, '&apos;');

	var form = "<form name='pdfexportform' action='generategrid' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='" + fileName
			+ "'>";
	form = form + "<input type='hidden' name='imagePath' value='" + path + "'>";
	form = form + "<input type='hidden' name='pdfBuffer' value='" + html + "'>";
	form = form + "</form><script>document.pdfexportform.submit();</sc"
			+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();
}

/**
 * Export data in excel format
 */
function exportExcel(tableID, fileName) {

	var table = document.getElementById("searchvendor");
	var html = "";
	var copy = table.cloneNode(true);
	if (fileName != null && fileName == 'SearchVendors') {
		$(copy).find("td:last-child").remove();
		$(copy).find("th:last-child").remove();
	} else if (fileName != null && fileName == 'Vendors') {
		$(copy).find("td:last-child").remove();
		$(copy).find("th:last-child").remove();
	}
	for ( var i = 0, row; row = copy.rows[i]; i++) {
		// iterate through rows
		// rows would be accessed using the "row" variable assigned in the for
		// loop
		for ( var j = 0, col; col = row.cells[j]; j++) {
			// iterate through columns
			// columns would be accessed using the "col" variable assigned in
			// the for loop
			html = html + row.cells[j].innerHTML + "\t"; // output each Row
															// as
		}
		html = html + "\n";
	}
	html = html + "\n";
	// alert(html);
	html = html.replace(/'/g, '&apos;');
	var form = "<form name='csvexportform' action='exportData.do?method=excelExport' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='" + fileName
			+ "'>";
	form = form + "<input type='hidden' name='csvBuffer' value='" + html + "'>";
	form = form + "</form><script>document.csvexportform.submit();</sc"
			+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();

}

/**
 * Export data in excel format
 */
function exportSearchResultExcel(tableID, fileName) {

	var table = document.getElementById("searchvendor");
	var html = "";
	var copy = table.cloneNode(true);
	if (fileName != null && fileName == 'VendorStatusSearch') {
		$(copy).find("td:first-child").remove();
		$(copy).find("th:first-child").remove();
		$(copy).find("td:nth-child(1)").remove();
		$(copy).find("th:nth-child(1)").remove();
		
		if($(copy).find("th:nth-child(4)").text() == "Email this Vendor")
		{
			$(copy).find("td:nth-child(4)").remove();
			$(copy).find("th:nth-child(4)").remove();
		}
		
		/*$(copy).find("td:last-child").remove();
		$(copy).find("th:last-child").remove();*/
	} else if (fileName != null && fileName == 'VendorCriteriaSearch') {
		$(copy).find("td:first-child").remove();
		$(copy).find("th:first-child").remove();
		$(copy).find("td:nth-child(1)").remove();
		$(copy).find("th:nth-child(1)").remove();
	} else if (fileName != null && fileName == 'PrimeVendorSearch') {
		$(copy).find("td:first-child").remove();
		$(copy).find("th:first-child").remove();
	} else if (fileName != null && fileName == 'InactiveVendorSearch') {
		//Nothing to Remove.
	}
	
	for ( var i = 0, row; row = copy.rows[i]; i++) {
		// iterate through rows
		// rows would be accessed using the "row" variable assigned in the for
		// loop
		for ( var j = 0, col; col = row.cells[j]; j++) {
			// iterate through columns
			// columns would be accessed using the "col" variable assigned in
			// the for loop
			// output each Row as
			if (i == 0) {
				html = html + row.cells[j].innerHTML + "\t";
			} else if (j == 0) {
				html = html + row.cells[j].childNodes[0].innerHTML + "\t";
			} else {
				html = html + row.cells[j].innerHTML + "\t";
			}
		}
		html = html + "\n";
	}
	html = html + "\n";
	// alert(html);
	html = html.replace(/'/g, '&apos;');
	var form = "<form name='csvexportform' action='exportData.do?method=excelExport' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='" + fileName
			+ "'>";
	form = form + "<input type='hidden' name='csvBuffer' value='" + html + "'>";
	form = form + "</form><script>document.csvexportform.submit();</sc"
			+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();

}

/**
 * Export helper function.
 */
function exportSearchResultHelper(table, name, path) {
	var exportType = $("input[name=export]:checked").val();

	if (exportType == 1) {
		exportSearchResultExcel(table, name);
	} else if (exportType == 2) {
		//$("#searchvendor").table2CSV();
		if (name == "VendorStatusSearch") {
			window.location = "viewVendorsStatus.do?method=generateVendorStatusSearchResultCSVFile";
		} else if (name == "VendorCriteriaSearch") {
			window.location = "viewVendors.do?method=generateVendorCriteriaSearchResultCSVFile";
		} else if (name == "PrimeVendorSearch") {
			window.location = "viewVendors.do?method=generatePrimeVendorSearchResultCSVFile";
		} else if (name == "InactiveVendorSearch") {
			window.location = "viewVendors.do?method=generateInactiveVendorSearchResultCSVFile";
		}
	} else if (exportType == 3) {
		// generateSearchResults(path, name);
		if (name == "VendorStatusSearch") {
			window.location = "viewVendorsStatus.do?method=getVendorStatusReport";
		} else if (name == "VendorCriteriaSearch") {
			window.location = "viewVendors.do?method=getVendorReport";
		} else if (name == "PrimeVendorSearch") {
			window.location = "viewVendors.do?method=getPrimeVendorReport";
		} else if (name == "InactiveVendorSearch") {
			window.location = "viewVendors.do?method=generateInactiveVendorSearchResultPDFFile";
		}
	} else {
		alert('Please Choose Export Type');
		return false;
	}

	$('#dialog').dialog('close');
}

function exportVendorDetailsHelper()
{
	var exportType = $("input[name=export]:checked").val();
	var vendorId = $('#vendorID_hdn').val();
	
	if (exportType == 1) {
		window.location = "generateVendorReport.do?method=generateVendorReportXLS&id=" + vendorId;
	} else if (exportType == 3) {
		window.location = "generateVendorReport.do?method=generateVendorReport&id=" + vendorId;		
	} else {
		alert('Please Choose Export Type');
		return false;
	}
	$('#exportPDFXLS').dialog('close');
}


function exportTier2VndorsReport()
{
	var exportType = $("input[name=export]:checked").val();
	var tier2ReportMasterId = $('#tier2ReportMasterID').val();
	
	if (exportType == 1) {
		window.location = "generateVendorReport.do?method=generateTier2VendorsSpendReportXLS&id=" + tier2ReportMasterId;
	} else if (exportType == 3) {
		window.location = "generateVendorReport.do?method=generateTier2VendorsSpendReport&id=" + tier2ReportMasterId;		
	} else {
		alert('Please Choose Export Type');
		return false;
	}
	$('#exportPDFXLS').dialog('close');
}


function exportSupplierDiversityReport()
{
	var exportType = $("input[name=export]:checked").val();
	var supplierDiversityId = $('#supplierDiversityId').val();
	
	if (exportType == 1) {
		window.location = "generateVendorReport.do?method=generateSupplierDiversityXLS&id=" + supplierDiversityId;
	} else if (exportType == 3) {
		window.location = "generateVendorReport.do?method=generateSupplierDiversity&id=" + supplierDiversityId;		
	} else {
		alert('Please Choose Export Type');
		return false;
	}
	$('#exportPDFXLS').dialog('close');
}

