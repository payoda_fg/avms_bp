/**
 * Export View User Log In Excel File. 
 */
function exportViewUserLogInExcel(tableID, fileName) 
{
	var table = document.getElementById(tableID);
	var html = "";
	var copy = table.cloneNode(true);
	
	$(copy).find("td:last-child").remove();
	$(copy).find("th:last-child").remove();
	
	for ( var i = 0, row; row = copy.rows[i]; i++) 
	{
		// iterate through rows would be accessed using the "row" variable assigned in the for loop
		for ( var j = 0, col; col = row.cells[j]; j++) 
		{
			// iterate through columns would be accessed using the "col" variable assigned in the for loop
			if(fileName == "UserLogReport")
			{
				if(i> 0 && j==4) { //Date Column
					html = html + " " + row.cells[j].innerHTML + " " + "\t"; // output each Row as
				} else {//For Other Columns
					html = html + row.cells[j].innerHTML + "\t"; // output each Row as
				}
			}
			else
			{
				html = html + row.cells[j].innerHTML + "\t"; // output each Row as
			}
		}
		html = html + "\n";
	}
	html = html + "\n";
	
	html = html.replace(/'/g, '&apos;');
	var form = "<form name='csvexportform' action='exportData.do?method=excelExport' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='" + fileName + "'>";
	form = form + "<input type='hidden' name='csvBuffer' value='" + html + "'>";
	form = form + "</form><script>document.csvexportform.submit();</sc"	+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();
}