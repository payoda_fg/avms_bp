/**
 * Validation Functions for VMS application
 * 
 * @author vinoth
 */

/**
 * Function for confirm delete action
 */
function confirm_delete() {

	input_box = confirm("Are you sure you want to delete this Record?");
	if (input_box == true) {
		// Output when OK is clicked
		return true;
	} else {
		// Output when Cancel is clicked
		// alert("Delete cancelled");
		return false;
	}
}

/**
 * Function for get the sub categories
 * 
 */
function getCategory() {
	var catName = document.getElementById('naicCategory').value;
	window.location = "viewnaicsubcategory.do?parameter=viewSubCategoriesById&categoryId="
			+ catName;
}

/**
 * Function for enable/disable the div to enter the values in NAICS sub-category
 */
function toggleAlert() {
	toggleDisabled(document.getElementById("table-holder"));
}
function toggleDisabled(el) {
	try {
		el.disabled = el.disabled ? false : true;
	} catch (E) {
	}
	if (el.childNodes && el.childNodes.length > 0) {
		for ( var x = 0; x < el.childNodes.length; x++) {
			toggleDisabled(el.childNodes[x]);
		}
	}
}

/**
 * Function to validate the End date cannot be less than start date
 */
function checkDate(fromDate, toDate) {
	var from = new Date($('#' + fromDate).val());
	var to = new Date($('#' + toDate).val());

	if (from > to) {
		alert(" End date cannot be less than start date. .");
		// $('#' + toDate).focus();
		return false;
	}
	return true;
}

/**
 * Function for enable the controls in Item Master page
 */
function enable() {
	document.getElementById('itemDescription').disabled = false;
	document.getElementById('itemCode').disabled = false;
	document.getElementById('itemCategory').disabled = false;
	document.getElementById('itemManufacturer').disabled = false;
	document.getElementById('itemUom').disabled = false;
	document.getElementById('isActive').disabled = false;
}

/**
 * Function for enable the control in subCategory page
 */
function enableSubCategoryDesc() {
	var catName = document.getElementById('naicCategory').value;
	if (catName != 0 && catName != null) {
		document.getElementById('naicSubCategoryDesc').disabled = false;
		document.getElementById('isActive').disabled = false;
		document.getElementById('Save').disabled = false;
		document.getElementById('Cancel').disabled = false;
	} else {
		alert('Choose the category to enable textbox');
	}
}

/**
 * Function to validate year of data same as start date and end date
 */
function checkDateSpend(fromDate, toDate) {
	var from = new Date($('#' + fromDate).val());
	var to = new Date($('#' + toDate).val());
	var year = $('#yearOfData').val();

	var yr1 = from.getFullYear();
	var yr2 = to.getFullYear();

	if (!((year == yr1) && (year == yr2))) {
		alert('The year should be same as the year entered in the textbox.');
		return false;

	}
	if (new Date(from) >= new Date(to)) {
		alert("From date must be less then to date.");
		return false;
	}
	return true;
}

/**
 * Function to validate entered year could not exceed current year
 */

function currentYearOfDataValidation() {

	// To get the current year
	var d = new Date();
	var curr_year = d.getFullYear();

	// To get the entered year
	var year_entered = $('#yearOfData').val();
	if (year_entered.toString().match(/^[-]?\d*\.?\d*$/)) {
		if (year_entered > curr_year) {
			alert('Year cannot exceed current year');
			// $('#yearOfData').val('');
			$('#yearOfData').focus();
			return false;
		} else {
			return true;
		}
	} else {
		alert('Year should be a number');
		// $('#yearOfData').val('');
		$('#yearOfData').focus();
		return false;
	}
}

/**
 * Function to validate entered year could not exceed current year
 */

function currentYearValidation(element) {
	// To get the current year
	var d = new Date();
	var curr_year = d.getFullYear();

	// To get the entered year
	var year_entered = $('#' + element).val();
	if (year_entered != "") {

		if (year_entered.toString().match(/^[-]?\d*\.?\d*$/)) {
			if (year_entered > curr_year) {
				alert('Year cannot exceed current year');
				$('#' + element).val('');
				$('#' + element).focus();
				return false;
			} else {
				return true;
			}
		} else {
			alert('Year should be a number');
			$('#' + element).val('');
			$('#' + element).focus();
			return false;
		}
	}
}

/**
 * Convert Money value into number in anonymous vendor / vendor form
 */

function moneyFormatToUS(from, to) {

	// To get the entered annual turnover
	var annual_Turnover = $('#'+from).val();
	
	if (annual_Turnover != "") {
		// check entered values are in currency format
		if (annual_Turnover.toString().match(
				/^\$?(\d{1,3},?(\d{3},?)*\d{3}(.\d{0,3})?|\d{1,3}(.\d{2})?)$/)) {
			// Convert into number (double)
			var strVal = annual_Turnover.replace(/[^0-9.]+/g, "");

			// Put it in a hidden text box
			$('#'+to).val(strVal);
		} else {
			alert('Entered money is not in valid format');
			$('#'+from).val('');
			$('#'+from).focus();
		}
	}
}

/**
 * For Search vendor in mail notification page
 */

function searchVendor() {
	var to = $('#to').val();
	var cc = $('#cc').val();
	var bcc = $('#bcc').val();
	// window.location =
	// "searchvendor.do?method=showMailNotificationPage&emailAddresss="+emailAddresss+"&cc="+
	// cc +"&bcc=" + bcc;
	$("#var1").val(to);
	$("#var2").val(cc);
	$("#var3").val(bcc);
	$("#form").submit();
}

function dueAlert() {
	var date = $('#date').val();
	var content = "";
	if (date != "") {
		content = "Response due on or before " + date;
	}
	$('#mailContentArea').val(content);
}

function getSubject() {
	var templateName = $("#templateId option:selected").text();
	if (templateName != "-----Select-----") {
		$('#subject').val(templateName);
	} else {
		$('#subject').val('');
	}
}

function dateValidation(element) {

	var date = new Date($('#' + element).val());
	var today = new Date();

	if (date < today) {
		alert(" Selected date must be equal or greater than current date");
		$('#' + element).val('');
		$('#' + element).focus();
		return false;
	}
	return true;
}

function validateNumber(ele) {
	if (isNaN(ele.value)) {
		alert('Enter only numbers');
		$("#"+ele.id).val('');
		$("#"+ele.id).focus();
	}

}

function validateCertificate() {
	
	jQuery.fn.exists = function() {
		return this.length > 0;
	};
	if (divCert1) {
		var certificateType1 = $('#divCertType1').val();
		var certifyingAgency1 = $('#divCertAgen1').val();
		var certType1 = $("#certType1").val();
		var certificationNo1 = $("#certificationNo1").val();
		var effectiveDate1 = $("#effDate1").val();
		var expairyDate1 = $('#expDate1').val();
		var uploadedFile1 = $('#certfile1').val();
		var downloadFile1 = 0;

		if ($('#downloadFile1').exists()) {
			downloadFile1 = 1;
			if (certificateType1 != 0
					&& (certifyingAgency1 == "" || certType1 == ""
							|| certificationNo1 == "" || effectiveDate1 == ""
							|| expairyDate1 == "" || downloadFile1 == 0)) {
				alert('Row No. 1 : Other particulars are mandatory if Classification is selected');
				return false;
			}

		} else {
			if (certificateType1 != 0
					&& (certifyingAgency1 == "" || certificationNo1 == ""
							|| effectiveDate1 == "" || expairyDate1 == "" || uploadedFile1 == "")) {
				alert('Row No. 1 : Other particulars are mandatory if Classification is selected');
				return false;
			}
		}
	}
	else
	{
		if($('#divCertType1').val() != "" && $('#expDate1').val() == "")
		{
			alert('Row No. 1 : Expiry date are mandatory if Classification is selected');
			return false;
		}
	}
	
	if (divCert2) {
		var certificateType2 = $('#divCertType2').val();
		var certifyingAgency2 = $('#divCertAgen2').val();
		var certType2 = $("#certType2").val();
		var certificationNo2 = $('#certificationNo2').val();
		var effectiveDate2 = $("#effDate2").val();
		var expairyDate2 = $('#expDate2').val();
		var uploadedFile2 = $('#certfile2').val();
		var downloadFile2 = 0;

		if ($('#downloadFile2').exists()) {
			downloadFile2 = 1;
			if (certificateType2 != 0
					&& (certifyingAgency2 == "" || certType2 == ""
							|| certificationNo2 == "" || effectiveDate2 == ""
							|| expairyDate2 == "" || downloadFile2 == 0)) {
				alert('Row No. 2 : Other particulars are mandatory if Classification is selected');
				return false;
			}
		} else {
			if (certificateType2 != 0
					&& (certifyingAgency2 == "" || certType2 == ""
							|| certificationNo2 == "" || effectiveDate2 == ""
							|| expairyDate2 == "" || uploadedFile2 == "")) {
				alert('Row No. 2 : Other particulars are mandatory if Classification is selected');
				return false;
			}
		}
	}
	else
	{
		if($('#divCertType2').val() != "" && $('#expDate2').val() == "")
		{
			alert('Row No. 2 : Expiry date are mandatory if Classification is selected');
			return false;
		}
	}
	
	if (divCert3) {
		var certificateType3 = $('#divCertType3').val();
		var certifyingAgency3 = $('#divCertAgen3').val();
		var certType3 = $("#certType3").val();
		var certificationNo3 = $('#certificationNo3').val();
		var effectiveDate3 = $("#effDate3").val();
		var expairyDate3 = $('#expDate3').val();
		var uploadedFile3 = $('#certfile3').val();
		var downloadFile3 = 0;

		if ($('#downloadFile3').exists()) {
			downloadFile3 = 1;
			if (certificateType3 != 0
					&& (certifyingAgency3 == "" || certType3 == ""
							|| certificationNo3 == "" || effectiveDate3 == ""
							|| expairyDate3 == "" || downloadFile3 == 0)) {
				alert('Row No. 3 : Other particulars are mandatory if Classification is selected');
				return false;
			}
		} else {
			if (certificateType3 != 0
					&& (certifyingAgency3 == "" || certType3 == ""
							|| certificationNo3 == "" || effectiveDate3 == ""
							|| expairyDate3 == "" || uploadedFile3 == "")) {
				alert('Row No. 3 : Other particulars are mandatory if Classification is selected');
				return false;
			}
		}
	}
	else
	{
		if($('#divCertType3').val() != "" && $('#expDate3').val() == "")
		{
			alert('Row No. 3 : Expiry date are mandatory if Classification is selected');
			return false;
		}
	}
	
	return true;
}

function validateNaics() {
	
	if (validateCertificate()) {
		return true;
	} else {
		return false;
	}
}

/* Validate Email with domain name */
function email_validate(emailId) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(([a-zA-Z]+\.)+[a-zA-Z]{2,})$/;
	if (emailId != "") {
		if (!re.test(emailId)) {
			alert("Please enter correct email address");
			$('#emailAddress').val('');
			$('#emailAddress').focus();
			return false;
		}
	}
}

/*
 * Validate report year should be between 1900 and 3000
 */
function validateReportYear(year) {

	if (year != "" && year != null) {

		if (isNaN(year)) {
			alert('Year should be valid number format');
			$('#reportYear').val('');
			$('#reportYear').focus();
			return false;
		}

		if (year <= 1900 || year >= 3000) {
			alert('Year should be in range of 1900 and 3000');
			$('#reportYear').val('');
			$('#reportYear').focus();
			return false;
		}
	}

}

/*
 * Function to validate certificate in unknown vendor registration.
 */
function validateAnonymousVendorCertificate() {

	var certificateType1 = $('#divCertType1').val();
	var certifyingAgency1 = $('#divCertAgen1').val();
	var certificationNo1 = $("#certificationNo1").val();
	var effectiveDate1 = $("#effDate1").val();
	var expairyDate1 = $('#expDate1').val();
	var uploadedFile1 = $('#certfile1').val();
	var downloadFile1 = 0;

	jQuery.fn.exists = function() {
		return this.length > 0;
	};

	// if ($("#chkDiverse").is(':checked')) {
	// document.getElementById("diverseClassificationTab").style.display =
	// "block";
	// }

	if ($('#downloadFile1').exists()) {
		downloadFile1 = 1;
		if (certificateType1 == 0 && certifyingAgency1 == ""
				&& certificationNo1 == "" && effectiveDate1 == ""
				&& expairyDate1 == "" && downloadFile1 == 0) {
			alert('Corresponding certifying agency, certification number, effective date, expairy date and upload certificate in diverse classification is mandatory');
			return false;
		} else if (certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 == "" && effectiveDate1 == ""
				&& expairyDate1 == "" && downloadFile1 == 0) {
			alert('Corresponding certification number, effective date, expairy date and upload certificate in diverse classification is mandatory');
			return false;
		} else if (certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 != "" && effectiveDate1 == ""
				&& expairyDate1 == "" && downloadFile1 == 0) {
			alert('Corresponding effective date, expairy date, upload certificate in diverse classification is mandatory');
			return false;
		} else if (certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 != "" && effectiveDate1 != ""
				&& expairyDate1 == "" && downloadFile1 == 0) {
				alert('Corresponding expairy date and upload certificate in diverse classification is mandatory');
				return false;
		} else if (certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 != "" && effectiveDate1 != ""
				&& expairyDate1 != "" && downloadFile1 == 0) {
				alert('Corresponding upload certificate in diverse classification tab is mandatory');
				return false;
		}

	} else if ($("#chkDiverse").is(':checked')) {
		if (certificateType1 == 0 && certifyingAgency1 == ""
				&& certificationNo1 == "" && effectiveDate1 == ""
				&& expairyDate1 == "" && uploadedFile1 == "") {
			alert('Corresponding certifying agency, certification number, effective date, expairy date and upload certificate in diverse classification is mandatory');
			return false;
		} else if (certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 == "" && effectiveDate1 == ""
				&& expairyDate1 == "" && uploadedFile1 == "") {
			alert('Corresponding certification number, effective date, expairy date and upload certificate in diverse classification is mandatory');
			return false;
		} else if (certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 != "" && effectiveDate1 == ""
				&& expairyDate1 == "" && uploadedFile1 == "") {
			alert('Corresponding effective date, expairy date, upload certificate in diverse classification is mandatory');
			return false;
		} else if(certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 != "" && effectiveDate1 != ""
					&& expairyDate1 == "" && uploadedFile1 == ""){
			alert('Corresponding expairy date, upload certificate in diverse classification is mandatory');
			return false;
		} else if(certificateType1 != 0 && certifyingAgency1 != 0
				&& certificationNo1 != "" && effectiveDate1 != ""
					&& expairyDate1 != "" && uploadedFile1 == ""){
			alert('Corresponding upload certificate in diverse classification is mandatory');
			return false;
		}
	}
	return true;
}

function validateVendorOtherCertificate() {
	var certificateType1 = $('#otherCertType1').val();
	var expairyDate1 = $('#othexpDate1').val();
	var uploadedFile1 = $('#otherCert1').val();
	var downloadFile1 = 0;

	jQuery.fn.exists = function() {
		return this.length > 0;
	};

	if ($('#downloadFile1').exists()) {
		downloadFile1 = 1;
		if (certificateType1 != -1 && expairyDate1 == "" && downloadFile1 == 0) {
			alert('Corresponding expairy date and upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType1 != -1 && expairyDate1 != ""
				&& downloadFile1 == 0) {
			alert('Corresponding upload certificate in Quality/other certification tab is mandatory');
			return false;
		}
	} else {
		if (certificateType1 != -1 && expairyDate1 == "" && uploadedFile1 == "") {
			alert('Corresponding expairy date and upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType1 != -1 && expairyDate1 != ""
				&& uploadedFile1 == "") {
			alert('Corresponding upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType1 == -1 && expairyDate1 == ""
			&& uploadedFile1 != ""){
			alert('Corresponding certificate type and expairy date in Quality/other certification tab is mandatory');
			return false;
		}
	}

	var certificateType2 = $('#otherCertType2').val();
	var expairyDate2 = $('#othexpDate2').val();
	var uploadedFile2 = $('#otherCert2').val();
	var downloadFile2 = 0;

	if ($('#downloadFile2').exists()) {
		downloadFile2 = 1;
		if (certificateType2 != -1 && expairyDate2 == "" && downloadFile2 == 0) {
			alert('Corresponding expairy date and upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType2 != -1 && expairyDate2 != ""
				&& downloadFile2 == 0) {
			alert('Corresponding upload certificate in Quality/other certification tab is mandatory');
			return false;
		}
	} else {
		if (certificateType2 != -1 && expairyDate2 == "" && uploadedFile2 == "") {
			alert('Corresponding expairy date and upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType2 != -1 && expairyDate2 != ""
				&& uploadedFile2 == "") {
			alert('Corresponding upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType2 == -1 && expairyDate2 == ""
			&& uploadedFile2 != ""){
			alert('Corresponding certificate type and expairy date in Quality/other certification tab is mandatory');
			return false;
		}
	}
	
	var certificateType3 = $('#otherCertType3').val();
	var expairyDate3 = $('#othexpDate3').val();
	var uploadedFile3 = $('#otherCert3').val();
	var downloadFile3 = 0;

	if ($('#downloadFile3').exists()) {
		downloadFile3 = 1;
		if (certificateType3 != -1 && expairyDate3 == "" && downloadFile3 == 0) {
			alert('Corresponding expairy date and upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType3 != -1 && expairyDate3 != ""
				&& downloadFile3 == 0) {
			alert('Corresponding upload certificate in Quality/other certification tab is mandatory');
			return false;
		}
	} else {
		if (certificateType3 != -1 && expairyDate3 == "" && uploadedFile3 == "") {
			alert('Corresponding expairy date and upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType3 != -1 && expairyDate3 != ""
				&& uploadedFile3 == "") {
			alert('Corresponding upload certificate in Quality/other certification tab is mandatory');
			return false;
		} else if (certificateType3 == -1 && expairyDate3 == ""
			&& uploadedFile3 != ""){
			alert('Corresponding certificate type and expairy date in Quality/other certification tab is mandatory');
			return false;
		}
	}

	return true;
}

function validateSelfVendorOtherCertificate() {
	var numberOfRows =  $('#otherCertificate tr').length - 1;
	var i;
	for(i=1;i<=numberOfRows;i++){
	
		/*Getting Values from Orginal Ids*/
		var certificateType1 = $('#otherCertType' + i).val();
		var expairyDate1 = $('#othexpDate' + i).val();
		var uploadedFile1 = $('#otherCert' + i).val();
		var downloadFile1 = 0;
		jQuery.fn.exists = function() {
			return this.length > 0;
		};
	
		if ($('#downloadOtherFile' + i).exists()) {
			downloadFile1 = 1;
			if(certificateType1 == -1 && expairyDate1 == "")
			{
				alert('Row '+i+': Corresponding certificate type and expiry date in Quality/Other certification is mandatory');
				return false;
			}
			else if(certificateType1 == -1 && expairyDate1 !="")
			{
				alert('Row '+i+': Corresponding certificate type in Quality/Other certification is mandatory');
				return false;
			}
			else if(certificateType1 != -1 && expairyDate1 =="")
			{
				alert('Row '+i+': Corresponding expiry date in Quality/Other certification is mandatory');
				return false;
			}
			else if (certificateType1 != -1 && expairyDate1 == "" && downloadFile1 == 0) 
			{
				alert('Row '+i+': Corresponding expiry date and upload certificate in Quality/Other certification is mandatory');
				return false;
			} else if (certificateType1 != -1 && expairyDate1 != "" && downloadFile1 == 0) 
			{
				alert('Row '+i+': Corresponding upload certificate in Quality/Other certification is mandatory');
				return false;
			}
		} else{
			if(certificateType1 == -1 && expairyDate1 == "" && uploadedFile1 != "")
			{
				alert('Row '+i+': Corresponding certificate type and expiry date in Quality/Other certification is mandatory');
				return false;
			}
			else if(certificateType1 == -1 && expairyDate1 != "" && uploadedFile1 == "")
			{
				alert('Row '+i+': Corresponding certificate type and upload certificate in Quality/Other certification is mandatory');
				return false;
			}
			else if(certificateType1 == -1 && expairyDate1 != "" && uploadedFile1 != "")
			{
				alert('Row '+i+': Corresponding certificate type in Quality/Other certification is mandatory');
				return false;
			}
			else if (certificateType1 != -1 && expairyDate1 == "" && uploadedFile1 == "") {
				alert('Row '+i+': Corresponding expiry date and upload certificate in Quality/Other certification is mandatory');
				return false;
			}
			else if(certificateType1 != -1 && expairyDate1 == "" && uploadedFile1 != "")
			{
				alert('Row '+i+': Corresponding expiry date in Quality/Other certification is mandatory');
				return false;
			}
			else if (certificateType1 != -1 && expairyDate1 != "" && uploadedFile1 == "") 
			{
				alert('Row '+i+': Corresponding upload certificate in Quality/Other certification is mandatory');
				return false;
			} 
		}
	}
	return true;
}

function validateSelfVendorDocumentsUpload() {
	var vendorDocNameLength = document.getElementsByName("vendorDocName").length;
	var documentFileSizeLength = document.getElementsByName("documentFilesize").length;	
		
	if(vendorDocNameLength > 0)
	{		
		for(var i = 0; i < vendorDocNameLength; i++) 
		{			
			if(document.getElementsByName("vendorDocName")[i].value != "" && document.getElementsByName("vendorDocDesc")[i].value == "" 
				&& $('#vendorDoc' + i).val() == "") 
			{
				if(documentFileSizeLength > 0)
				{
					if(documentFileSizeLength <= i) 
					{
						alert('Corresponding vendor document description and document file in document upload is mandatory');
						return false;
					} else {
						alert('Corresponding vendor document description in document upload is mandatory');
						return false;
					}					
				}
				else
				{
					alert('Corresponding vendor document description and document file in document upload is mandatory');
					return false;
				}				
			} 
			else if(document.getElementsByName("vendorDocDesc")[i].value != "" && document.getElementsByName("vendorDocName")[i].value == "" 
				&& $('#vendorDoc' + i).val() == "") 
			{
				if(documentFileSizeLength > 0)
				{
					if(documentFileSizeLength <= i) 
					{
						alert('Corresponding vendor document Name and document file in document upload is mandatory');
						return false;
					} else {
						alert('Corresponding vendor document Name in document upload is mandatory');
						return false;
					}					
				}
				else
				{
					alert('Corresponding vendor document Name and document file in document upload is mandatory');
					return false;
				}				
			} 
			else if($('#vendorDoc' + i).val() != "" && document.getElementsByName("vendorDocName")[i].value == "" 
				&& document.getElementsByName("vendorDocDesc")[i].value == "") 
			{
				alert('Corresponding vendor document Name and vendor document description in document upload is mandatory');
				return false;
			}
			else if(document.getElementsByName("vendorDocName")[i].value != "" && document.getElementsByName("vendorDocDesc")[i].value != "" 
				&& $('#vendorDoc' + i).val() == "") 
			{				
				if(documentFileSizeLength > 0)
				{
					if(documentFileSizeLength <= i) 
					{
						alert('Corresponding document file in document upload is mandatory');
						return false;
					}
				}
				else
				{
					alert('Corresponding document file in document upload is mandatory');
					return false;
				}				
			}
			else if(document.getElementsByName("vendorDocName")[i].value != "" && document.getElementsByName("vendorDocDesc")[i].value == "" 
				&& $('#vendorDoc' + i).val() != "") 
			{
				alert('Corresponding vendor document description in document upload is mandatory');
				return false;
			}
			else if(document.getElementsByName("vendorDocName")[i].value == "" && document.getElementsByName("vendorDocDesc")[i].value != "" 
				&& $('#vendorDoc' + i).val() != "") 
			{
				alert('Corresponding vendor document name in document upload is mandatory');
				return false;
			}
		}
	}	
	return true;
}
//function validateCertExpDate() {
//	$("input[id*=expDate]").each(function() {
//		var expDate = $(this).val();
//		var certType1 = $('#divCertType1').val();
//		var certAgency1 = $('#divCertAgen1').val();
//
//		var certType2 = $('#divCertType2').val();
//		var certAgency2 = $('#divCertAgen2').val();
//
//		var certType3 = $('#divCertType3').val();
//		var certAgency3 = $('#divCertAgen3').val();
//
//		if (certType1 != "" && certAgency1 != "" && expDate == "") {
//			alert("Please Enter the Certificate Expairy Date1");
//			return false;
//		}
//		if (certType2 != "" && certAgency2 != "" && expDate == "") {
//			alert("Please Enter the Certificate Expairy Date2");
//			return false;
//		}
//		if (certType3 != "" && certAgency3 != "" && expDate == "") {
//			alert("Please Enter the Certificate Expairy Date3");
//			return false;
//		}
//		return true;
//	});
//}