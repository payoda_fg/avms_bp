
function initialize() {
	if (document.getElementById("byOption1").checked) {

		document.getElementById("by_supplier").style.display = "block";
		document.getElementById("by_quarter").style.display = "none";
		document.getElementById("by_year").style.display = "none";
	}
	if (document.getElementById("byOption2").checked) {
		document.getElementById("by_quarter").style.display = "block";
		document.getElementById("by_supplier").style.display = "none";
		document.getElementById("by_year").style.display = "none";
	}
	if (document.getElementById("byOption3").checked) {
		document.getElementById("by_quarter").style.display = "none";
		document.getElementById("by_supplier").style.display = "none";
		document.getElementById("by_year").style.display = "block";
	}
	if (document.getElementById("byOption4").checked) {
		document.getElementById("by_quarter").style.display = "none";
		document.getElementById("by_supplier").style.display = "none";
		document.getElementById("by_year").style.display = "none";
	}
}
function displaySupplier() {

	document.getElementById("by_supplier").style.display = "block";
	document.getElementById("by_quarter").style.display = "none";
	document.getElementById("by_year").style.display = "none";

}
function displayQuarter() {
	document.getElementById("by_quarter").style.display = "block";
	document.getElementById("by_supplier").style.display = "none";
	document.getElementById("by_year").style.display = "none";

}
function displayYear() {
	document.getElementById("by_quarter").style.display = "none";
	document.getElementById("by_supplier").style.display = "none";
	document.getElementById("by_year").style.display = "block";

}

function displayDateToYear() {
	document.getElementById("by_quarter").style.display = "none";
	document.getElementById("by_supplier").style.display = "none";
	document.getElementById("by_year").style.display = "none";

}

function datePicker_Report() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#startDate').removeClass('text-label');
				$('#endDate').removeClass('text-label');
			}
		});
		$("#startDate").datepicker();
		$("#endDate").datepicker();
	});
}

