/**
 * Add one row to RFI Documents.
 * 
 * @param tableID
 */
function fn_addRow(tableID) {

	rowcnt = document.getElementById("filelastrowcount").value;
	rowcnt = parseInt(rowcnt) + 1;
	tab = document.getElementById(tableID);
	row = tab.insertRow(tab.rows.length);
	row.id = rowcnt;

	cell = row.insertCell(0);
	cell.innerHTML = "<input name='chkfile_"
			+ rowcnt
			+ "' type='checkbox' value='' size='15' /><input type='hidden' name='ridfile_"
			+ rowcnt + "' id='ridfile_" + rowcnt + "' value='" + rowcnt + "'/>";

	cell1 = row.insertCell(1);
	cell1.innerHTML = "<input type='text' class='text-box' name='titles' value='' size='15' title='' />";

	cell2 = row.insertCell(2);
	cell2.innerHTML = "<input type='file' name='docs' size='20' />";

	document.getElementById("filelastrowcount").value = rowcnt;
}

/**
 * Remove the selected row from the RFI Documents
 * 
 * @param tableID
 */
function fn_removeRow(tableID) {
	try {

		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var flag = false;
		for ( var i = 0; i < rowCount; i++) {

			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			flag = false;
			if (null != chkbox && true == chkbox.checked) {
				table.deleteRow(i);
				rowCount--;
				i--;
				flag = true;
			}
			if (flag) {
				document.getElementById("filelastrowcount").value = table.rows.length;
			}
		}
	} catch (e) {
		alert(e);
	}
}

/**
 * Add one row to the Compliance Details.
 * 
 * @param id
 * @param type
 */
function addRow67678(id, type) {

	rowcnt = document.getElementById("lastrowcount").value;
	rowcnt1 = parseInt(document.getElementById("lastrowcount").value);
	rowcnt = parseInt(rowcnt) + 1;

	tab = document.getElementById(id);
	// var tbody = document.getElementById(id).getElementsByTagName("tbody")[0];
	row = tab.insertRow(tab.rows.length);
	row.id = rowcnt;
	cell = row.insertCell(0);
	cell.innerHTML = "<input name='chk_" + rowcnt
			+ "' type='checkbox' value='' size='15' />";

	cell1 = row.insertCell(1);
	cell1.innerHTML = rowcnt + "<input type='hidden' name='rid_" + rowcnt
			+ "' id='rid_" + rowcnt + "' value='" + rowcnt + "'/>";

	cell2 = row.insertCell(2);
	cell2.innerHTML = "<input type='hidden' name='attributeCount' value='3' id='attributecount_"
			+ rowcnt
			+ "' /><textarea rows='3' cols='25' id='qn_"
			+ rowcnt
			+ "' name='complianceItems' value=''/> ";

	cell3 = row.insertCell(3);
	cell3.innerHTML = "<p class='leftalign'><label><input type='radio' name='target["
			+ rowcnt1
			+ "]'"
			+ "value='V' />Vendor</label><br /><label><input type='radio' name='target["
			+ rowcnt1 + "]' value='I' />Internal</label><br /></p>";

	cell4 = row.insertCell(4);
	var tabAtt = "tabatt_" + rowcnt;

	if (type == "RFP") {
		var temp = "<div class='create-smllform'><div class='Comp-attri-titles'><p style='font-weight:normal'>Attributes<span style='padding-left:50px'>Weightage</span></p></div><div id='Comp-attri'>";
		temp += "<table id='tabatt_" + rowcnt + "' ><tr>";

		temp += "<td><input name='chkatt_" + rowcnt
				+ "_1' type='checkbox' value='1' size='15' /></td>";
		temp += "<td><input type='text' name='name='attribute' class='text-box'  /></td>";
		temp += "<td><input type='text' name='weightages' class='text-box'  /></td>";
		temp += "</tr><tr><td><input name='chkatt_" + rowcnt
				+ "_2' type='checkbox' value='2' size='15' /></td>";
		temp += "<td><input type='text' name='attribute' class='text-box'  /></td>";
		temp += "<td><input type='text' name='weightages' class='text-box'  /></td>";
		temp += "</tr><tr><td><input name='chkatt_" + rowcnt
				+ "_3' type='checkbox' value='3' size='15' /></td>";
		temp += "<td><input type='text' name='attribute' class='text-box'  /></td>";
		temp += "<td><input type='text' name='weightages' class='text-box'  /></td>";
		temp += "</tr></table>";
	} else {
		var temp = "<div class='create-smllform-RFI'><div class='Comp-attri-titles'><p style='font-weight:normal'>Attributes</p></div><div id='Comp-attri'>";
		temp += "<table id='tabatt_" + rowcnt + "' ><tr>";
		temp += "<td><input name='chkatt_" + rowcnt
				+ "_1' type='checkbox' value='1' size='15' /></td>";
		temp += "<td><input type='text' name='attribute' class='text-box'  /></td>";
		temp += "</tr><tr><td><input name='chkatt_" + rowcnt
				+ "_2' type='checkbox' value='2' size='15' /></td>";
		temp += "<td><input type='text' name='attribute' class='text-box'  /></td>";
		temp += "</tr><tr><td><input name='chkatt_" + rowcnt
				+ "_3' type='checkbox' value='3' size='15' /></td>";
		temp += "<td><input type='text' name='attribute' class='text-box'   /></td>";
		temp += "</tr></table>";
	}
	temp += "</div><div id='Comp-attri-icon'>";
	temp += '<img src="'
			
			+ 'images/icon-Plus.gif" width="16" height="16" onClick="fn_addAttRow(\''
			+ tabAtt + '\', \'' + rowcnt + '\',\'' + type + '\')" />';
	temp += '<img src='
			
			+ 'images/icon-minus.gif width="16" height="16" onClick="fn_removeAttRow(\''
			+ tabAtt + '\', \'' + rowcnt + '\')" />';
	temp += "</div></div>";
	cell4.innerHTML = temp;

	cell5 = row.insertCell(5);
	cell5.innerHTML = "<p class='leftalign'><label><input type='radio' name='mandatory["
			+ rowcnt1
			+ "]'"
			+ " value='M' />Mandatory</label><br /><label><input type='radio' name='mandatory["
			+ rowcnt1 + "]' " + "value='O' />Optional</label><br /></p>";

	cell6 = row.insertCell(6);
	cell6.innerHTML = "<textarea rows='3' cols='15' id='notes_" + rowcnt
			+ "' name='notes' /> ";

	document.getElementById("lastrowcount").value = rowcnt;
}

/**
 * Delete the selected RFI Compliance details row from the Compliance Table.
 * 
 * @param tableID
 */
function deleteRow(tableID) {

	try {

		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;

		var flag = false;
		for ( var i = 0; i < rowCount; i++) {
			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			flag = false;
			if (null != chkbox && true == chkbox.checked) {
				table.deleteRow(i);
				rowCount--;
				i--;
				flag = true;
			}
			if (flag) {
				document.getElementById("lastrowcount").value = table.rows.length;
			}
		}
	} catch (e) {
		alert(e);
	}
}
/**
 * Add the row to Atribute table
 * 
 * @param tableID
 * @param tabRowId
 * @param type
 */
function fn_addAttRow(tableID, tabRowId, type) {

	tab = document.getElementById(tableID);
	rowcnt = tab.rows.length;

	row = tab.insertRow(tab.rows.length);
	row.id = rowcnt;

	cell = row.insertCell(0);
	cell.innerHTML = "<input name='chkatt_" + tabRowId + "_" + rowcnt
			+ "' type='checkbox'  />";

	cell1 = row.insertCell(1);
	cell1.innerHTML = "<input type='text' name='attribute' class='text-box'   />";
	document.getElementById("attributecount_" + tabRowId).value = parseInt(rowcnt) + 1;
	if (type == "RFP") {
		cell2 = row.insertCell(2);
		cell2.innerHTML = "<input type='text' name='weightage[" + rowcnt
				+ "]' class='text-box'   />";
	}
}

/**
 * Remove the selected attribute row from the Attribute table
 * 
 * @param tableID
 */
function fn_removeAttRow(tableID, tabRowId) {
	try {

		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		for ( var i = 0; i < rowCount; i++) {

			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			if (null != chkbox && true == chkbox.checked) {
				table.deleteRow(i);
				rowCount--;
				i--;
			}
			document.getElementById("attributecount_" + tabRowId).value = rowCount;
		}
	} catch (e) {
		alert(e);
	}
}

function fn_addNaicsRow(tableID) {

	rowcnt = document.getElementById("lastrowcount").value;
	rowcnt = parseInt(rowcnt) + 1;
	tab = document.getElementById(tableID);
	row = tab.insertRow(tab.rows.length);
	row.id = rowcnt;

	cell = row.insertCell(0);
	cell.innerHTML = "<input type='text' name='naicsCode' id='naicsCode_"
			+ rowcnt + "' size='10'/>" + "<img src='"  
			+ "images/magnifier.gif'onClick=javascript:fn_findNaic('"
			+  "/NaicsCode.do?method=naicsCodes&rowcount=" + rowcnt
			+ "');>";

	cell1 = row.insertCell(1);
	cell1.innerHTML = "<textarea  name='naicsDesc' id='naicsDesc_" + rowcnt
			+ "' value=''  />";

	cell2 = row.insertCell(2);
	cell2.innerHTML = "<input type='text' name='isicCode' id='isicCode_"
			+ rowcnt + "' size='10'	title='' />";
	cell2 = row.insertCell(3);
	cell2.innerHTML = "<textarea name='isicDesc' id='isicDesc_" + rowcnt
			+ "' />";
	cell2 = row.insertCell(4);
	cell2.innerHTML = "<input type='radio' name='primary' id='primary_"
			+ rowcnt + "' value='0'/>";

	document.getElementById("lastrowcount").value = rowcnt;
}



function showAllVendorsByCategory(str) {

	var categoryId = document.getElementById('naicsCategoryId').value;
	var url = "getallvendors.jsp";
	url += "?subcategoryId=" + str + "&categoryId=" + categoryId;

	if (window.XMLHttpRequest) {
		// Non-IE browsers
		req = new XMLHttpRequest();
		req.onreadystatechange = stateChangeVendor;
		try {
			req.open("GET", url, true);
		} catch (e) {
			alert(e);
		}
		req.send(null);
	} else if (window.ActiveXObject) { // IE
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			req.onreadystatechange = stateChangeVendor;
			req.open("GET", url, true);
			req.send();
		}
	}

}

function stateChangeVendor() {
	if (req.readyState == 4) { // Complete
		if (req.status == 200) { // OK response
			document.getElementById("vendors").innerHTML = req.responseText;
		} else {
			alert("Problem: " + req.statusText);
		}
	}

}


function ajaxFnSubCategory(str, divId, name,event) {
	var url = "subcategory.jsp";
	url += "?categoryId=" + encodeURIComponent(str) + "&name="
			+ encodeURIComponent(name)+"&event="+encodeURIComponent(event);
	$.ajax({
		url : url,
		success : function(data) {
			document.getElementById(divId).innerHTML = data;
		}
	});
	
}


