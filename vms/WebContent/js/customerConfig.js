// set the default colors to personalize profile when the checkbox is not checked

function fn_default() {
    if (document.getElementById("defaultSet").checked == true) {
	document.getElementById("Background").value = bgcolor;
	document.getElementById("Background").style.backgroundColor = "#"
		+ bgcolor;
	document.getElementById("SecondaryColor").value = secondary;
	document.getElementById("SecondaryColor").style.backgroundColor = "#"
		+ secondary;
	document.getElementById("Heading").value = heading;
	document.getElementById("Heading").style.backgroundColor = "#"
		+ heading;
	document.getElementById("RollOver").value = rollover;
	document.getElementById("RollOver").style.backgroundColor = "#"
		+ rollover;
	document.getElementById("MenuBar").value = menubar;
	document.getElementById("MenuBar").style.backgroundColor = "#"
		+ menubar;
	document.getElementById("MenuButton").value = menubutton;
	document.getElementById("MenuButton").style.backgroundColor = "#"
		+ menubutton;

	document.getElementById("Background").disabled = "true";
	document.getElementById("SecondaryColor").disabled = "true";
	document.getElementById("Heading").disabled = "true";
	document.getElementById("RollOver").disabled = "true";
	document.getElementById("MenuBar").disabled = "true";
	document.getElementById("MenuButton").disabled = "true";
    } else {
	document.getElementById("Background").disabled = "";
	document.getElementById("SecondaryColor").disabled = "";
	document.getElementById("Heading").disabled = "";
	document.getElementById("RollOver").disabled = "";
	document.getElementById("MenuBar").disabled = "";
	document.getElementById("MenuButton").disabled = "";
    }
}

function fn_defaultForEditProfile() {
    if (document.getElementById("defaultSet").checked == true) {
	document.getElementById("Background").value = bgcolor;
	document.getElementById("Background").style.backgroundColor = "#"
		+ bgcolor;
	document.getElementById("SecondaryColor").value = secondary;
	document.getElementById("SecondaryColor").style.backgroundColor = "#"
		+ secondary;
	document.getElementById("Heading").value = heading;
	document.getElementById("Heading").style.backgroundColor = "#"
		+ heading;
	document.getElementById("RollOver").value = rollover;
	document.getElementById("RollOver").style.backgroundColor = "#"
		+ rollover;
	document.getElementById("MenuBar").value = menubar;
	document.getElementById("MenuBar").style.backgroundColor = "#"
		+ menubar;
	document.getElementById("MenuButton").value = menubutton;
	document.getElementById("MenuButton").style.backgroundColor = "#"
		+ menubutton;

	document.getElementById("Background").disabled = "true";
	document.getElementById("SecondaryColor").disabled = "true";
	document.getElementById("Heading").disabled = "true";
	document.getElementById("RollOver").disabled = "true";
	document.getElementById("MenuBar").disabled = "true";
	document.getElementById("MenuButton").disabled = "true";
    } else {
	document.getElementById("Background").value = bgcolorFromDB;
	document.getElementById("Background").style.backgroundColor = "#"
		+ bgcolorFromDB;
	document.getElementById("SecondaryColor").value = secondaryFromDB;
	document.getElementById("SecondaryColor").style.backgroundColor = "#"
		+ secondaryFromDB;
	document.getElementById("Heading").value = headingFromDB;
	document.getElementById("Heading").style.backgroundColor = "#"
		+ headingFromDB;
	document.getElementById("RollOver").value = rolloverFromDB;
	document.getElementById("RollOver").style.backgroundColor = "#"
		+ rolloverFromDB;
	document.getElementById("MenuBar").value = menubarFromDB;
	document.getElementById("MenuBar").style.backgroundColor = "#"
		+ menubarFromDB;
	document.getElementById("MenuButton").value = menubuttonFromDB;
	document.getElementById("MenuButton").style.backgroundColor = "#"
		+ menubuttonFromDB;

	document.getElementById("Background").disabled = "";
	document.getElementById("SecondaryColor").disabled = "";
	document.getElementById("Heading").disabled = "";
	document.getElementById("RollOver").disabled = "";
	document.getElementById("MenuBar").disabled = "";
	document.getElementById("MenuButton").disabled = "";
    }
}

// Ajax request Duns number validation

function ajaxfn(obj, numType) {
    var ajDunsNo = obj.value;
    var xmlHttp;
    try {
	// Firefox, Opera 8.0+, Safari
	xmlHttp = new XMLHttpRequest();
    } catch (e) {
	// Internet Explorer
	try {
	    xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
	    try {
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch (e) {
		alert("Your browser does not support AJAX!");
		return false;
	    }
	}
    }
    xmlHttp.onreadystatechange = function() {
	if (xmlHttp.readyState == 4) {
	    if (xmlHttp.status == 200) {
		var retVal = xmlHttp.responseText;
		if (retVal == "false") {
		    if ("D" == numType) {
			alert("Duplicate DUNS Number [" + obj.value + "] !");
		    } else if ("T" == numType) {
			alert("Duplicate Tax ID Number [" + obj.value + "] !");
		    }
		    obj.value = "";
		    obj.focus();
		    return false;
		}
	    }
	}
    }
    ajDunsNo = TextEncoding(ajDunsNo);
    xmlHttp.open("POST", "DuplicateDunsNumChk?search=ajaxreq&dunsNo="
	    + ajDunsNo + "&type=" + numType, true);
    xmlHttp.send(null);
}

function TextEncoding(inputString) {
    var encodedInputString = "";
    if (inputString != "") {
	encodedInputString = escape(inputString);
	var len = encodedInputString.length;
	var finalEncodedStr = "";
	for (i = 0; i < len; i++) {
	    var ch = encodedInputString.charAt(i);
	    if ("+" == ch) {
		finalEncodedStr = finalEncodedStr + "%2B";
	    } else if ("/" == ch) {
		finalEncodedStr = finalEncodedStr + "%2F";
	    } else {
		finalEncodedStr = finalEncodedStr + ch;
	    }
	}
	encodedInputString = finalEncodedStr;

    }
    return encodedInputString;
}

// form reset javascript code

/*
 * function fn_reset() { for ( var n = 0; n < document.forms.length; n++) {
 * 
 * for ( var i = 0; i < document.forms[n].elements.length; i++) { if
 * (document.forms[n].elements[i].name == 'Background' ||
 * document.forms[n].elements[i].name == 'Heading' ||
 * document.forms[n].elements[i].name == 'SecondaryColor' ||
 * document.forms[n].elements[i].name == 'RollOver' ||
 * document.forms[n].elements[i].name == 'MenuBar' ||
 * document.forms[n].elements[i].name == 'MenuButton' ||
 * document.forms[n].elements[i].name == 'DropDown' ||
 * document.forms[n].elements[i].name == 'FormText' ||
 * document.forms[n].elements[i].name == 'MenuText') {
 * document.forms[n].elements[i].style.backgroundColor = '#FFFFFF';
 * document.forms[n].elements[i].value = ''; } else {
 * document.forms[n].elements[i].value = ''; } } } }
 */

/*
 * // form save javascript code
 * 
 * function fn_save() {
 * 
 * if (true) {
 * 
 * fn_readOnlyAll(true); var btnSave = document.getElementById('save');
 * btnSave.value = " Confirm "; btnSave.onclick = function() { fn_confirm(); }; }
 * else { fn_readOnlyAll(false); } } // form confirm javascript code
 * 
 * function fn_confirm() { var conSave = confirm('Do you want to proceed?'); if
 * (conSave) {
 * 
 * document.custConfig.action = "/CustomerAction?actionId=createCustomer";
 * 
 * var defaultval = ""; if (document.getElementById("defaultSet").checked ==
 * true) { defaultval = "Yes"; } else { defaultval = "No"; }
 * document.custConfig.defaultSetting.value = defaultval;
 * document.custConfig.submit(); } else { fn_readOnlyAll(false); var btnSave =
 * document.getElementById('save'); btnSave.value = " Save "; btnSave.onclick =
 * function() { fn_save(); }; } }
 */

/*
 * function fn_readOnlyAll(flagRead) {
 * 
 * var controlIndex; var element; var numberOfControls =
 * document.custConfig.length; var str = new String(); var i = 1; var flag; var
 * chkVal;
 * 
 * for (controlIndex = 0; controlIndex < numberOfControls; controlIndex++) {
 * flag = false; element = document.custConfig[controlIndex]; if (element.type ==
 * "text") { element.readOnly = flagRead; } else if (element.type == "checkbox") {
 * element.disabled = flagRead; } } }
 */

function fn_diverseSup() {

    if (document.getElementById("chkDiverse").checked == true) {
	$("#diverseCertTab").show();
    } else {
	$("#diverseCertTab").hide();
    }
}

function fn_findNaic(action) {
    popWin = window
	    .open(
		    action,
		    'FindNAICCode',
		    'toolbar=no,status=no,width=800,height=395, scrollbars=no, resizable=no, left=130, top=90');
}

function fn_copyTemplate(action) {

    var templateName = document.getElementById('templateName').value;
    var templateNameFromTable = null;
    var templateFlag = false;
    
    if (templateName == "") {
	alert('Please enter the template name');
	document.getElementById('templateName').focus();
    } else {
    $('#mytable tr').each(function() {
        templateNameFromTable = $(this).find(".templatename").html(); 
        if(templateNameFromTable == document.getElementById('templateName').value){
        	templateFlag = true;
        }
     });
    
    if(!templateFlag){
    	popWin = window
		.open(
			action + "&templateName=" + templateName,
			'Template',
			'toolbar=no,status=no,width=800,height=395, scrollbars=no, resizable=no, left=130, top=90');
	var timer = setInterval(function() {
	    if (popWin.closed) {
		clearInterval(timer);
		window.location = "questions.do?method=showPage";
	    }
	}, 1000);
    }else{
    	alert('Cannot use the existing template name');
    }
    
    }
}

function fn_moveTemplate(action) {

    var templateName = document.getElementById('templateName').value;
    var templateNameFromTable = null;
    var templateFlag = false;
    
    if (templateName == "") {
	alert('Please enter the template name');
	document.getElementById('templateName').focus();
    } else {
    $('#mytable tr').each(function() {
        templateNameFromTable = $(this).find(".templatename").html(); 
        if(templateNameFromTable == document.getElementById('templateName').value){
        	templateFlag = true;
        }
     });
    
    if(!templateFlag){
    	popWin = window
		.open(
			action + "&templateName=" + templateName,
			'Template',
			'toolbar=no,status=no,width=800,height=395, scrollbars=no, resizable=no, left=130, top=90');
	var timer = setInterval(function() {
	    if (popWin.closed) {
		clearInterval(timer);
		window.location = "questions.do?method=showPage";
	    }
	}, 1000);
    }else{
    	alert('Need to use the existing template name');
    }
    
    }
}

function isValidNaic(naiccode) {

    var isValid = false;
    for ( var k = 0; k < naicsCodes.length; k++) {
	if (naiccode == naicsCodes[k]) {

	    document.getElementById("naicsDesc").innerHTML = "<textarea rows='3' cols='30'>"
		    + naicsDesc[k] + "</textarea> ";
	    document.frm_supReg.isicCode.value = isicsCode[k];
	    document.getElementById("isicDesc").innerHTML = "<textarea rows='3' cols='30'>"
		    + isicsDesc[k] + "</textarea> ";
	    isValid = true;
	    break;
	}
    }
    return isValid;
}

function fn_isValidNaic(naiccode) {

    var isValid = false;
    if (!isEmpty(naiccode)) {
	for ( var k = 0; k < naicsCodes.length; k++) {
	    if (naiccode == naicsCodes[k]) {

		document.getElementById("naicsDesc1").innerHTML = "<textarea rows='3' cols='34'>"
			+ naicsDesc1[k] + "</textarea> ";
		document.getElementById("isicCode").value = isicsCode[k];
		document.getElementById("isicDesc1").innerHTML = "<textarea rows='3' cols='34'>"
			+ isicsDesc1[k] + "</textarea> ";
		$("#naicDescValTr").show();
		$("#naicDescValTr").show();
		isValid = true;
		break;
	    }
	}
    }
    return isValid;
}

function fn_isValidNaic1(naiccode) {
    var isValid = false;
    if (!isEmpty(naiccode)) {
	for ( var k = 0; k < naicsCodes.length; k++) {
	    if (naiccode == naicsCodes[k]) {

		document.getElementById("naicsDesc2").innerHTML = "<textarea rows='3' cols='34'>"
			+ naicsDesc2[k] + "</textarea> ";
		document.getElementById("isicCode1").value = isicsCode[k];
		document.getElementById("isicDesc2").innerHTML = "<textarea rows='3' cols='34'>"
			+ isicsDesc2[k] + "</textarea> ";
		$("#naicDescLabTr1").show();
		$("#naicDescValTr1").show();
		isValid = true;
		break;
	    }
	}
    }
    return isValid;
}

function fn_isValidNaic2(naiccode) {
    // alert('naiccode'+naiccode);
    var isValid = false;
    if (!isEmpty(naiccode)) {
	for ( var k = 0; k < naicsCodes.length; k++) {
	    if (naiccode == naicsCodes[k]) {

		document.getElementById("naicsDesc3").innerHTML = "<textarea rows='3' cols='34'>"
			+ naicsDesc3[k] + "</textarea> ";
		document.getElementById("isicCode2").value = isicsCode[k];
		document.getElementById("isicDesc3").innerHTML = "<textarea rows='3' cols='34'>"
			+ isicsDesc[k] + "</textarea> ";
		$("#naicDescLabTr2").show();
		$("#naicDescValTr2").show();
		isValid = true;
		break;
	    }
	}
    }
    return isValid;
}

function fn_trHide() {
    $("#naicDescLabTr").hide();
    $(".naicDescValTr").hide();
    $(".naicDescValTr1").hide();
    $(".naicDescValTr2").hide();
}

function fn_trShow(obj) {
    var isValid = false;
    var naicCo = trim(obj.value);
    if (!isEmpty(naicCo)) {
	if (isValidNaic(naicCo)) {

	    $("#naicDescLabTr").show();
	    $("#naicDescValTr").show();
	    isValid = true;
	} else {
	    alert("Please enter the valid NAICS code!");
	    obj.value = "";
	    obj.focus();
	    $("#naicDescLabTr").hide();
	    $("#naicDescValTr").hide();
	}
    } else {
	alert('Please enter the NAICS code!');
	obj.value = "";
	obj.focus();
    }
    return isValid;
}

// password validation.
function validatePassword(userPassword, confirm_password) {

    var password = document.getElementById(userPassword).value;
    var confirmPassword = document.getElementById(confirm_password).value;

    if (password.length == 0 && confirmPassword.length == 0) {
	return true;
    }
    if (password != "" && password == confirmPassword) {
	if (password.length < 4) {
	    alert("Password must contain at least four characters!");
	    document.getElementById(userPassword).focus();
	    return false;
	}

	var re = /^[\w]+$/;
	if (!re.test(password)) {
	    alert("Password only alphanumeric!");
	    document.getElementById(userPassword).focus();
	    return false;
	}
	return true;
    } else {
	alert(" Password and confirm password are not match!");
	document.getElementById(userPassword).focus();
	return false;
    }

}
