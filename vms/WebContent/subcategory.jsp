<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.fg.vms.customer.model.NAICSubCategory"%>
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.customer.dao.impl.NAICSubCategoryImpl"%>
<%@page contentType="text/html; charset=ISO-8859-1"%>

<%
    String categoryId = request.getParameter("categoryId");
    String subCategoryName = request.getParameter("name");
    String event = "";
    if (request.getParameter("event") != null) {
		event = request.getParameter("event");
    }
    String buffer = "<select name='" + subCategoryName
		    + "' id='naicsSubCategoryID' "
		    + "style='width:136px;height:21px;font-size:11px;' ";
    if (request.getParameter("event") != null) {
		buffer += event;
    }

    buffer += " ><option value='-1'>-----Select-----</option>";
    NAICSubCategoryImpl naicSubCategoryImpl = new NAICSubCategoryImpl();
    List<NAICSubCategory> subCategories;
    // get the selected category id

    UserDetailsDto appDetails = (UserDetailsDto) session
		    .getAttribute("userDetails");
    subCategories = naicSubCategoryImpl.retrieveSubCategoryById(
		    Integer.parseInt(categoryId.toString()), appDetails);
    for (NAICSubCategory sub : subCategories) {
		buffer = buffer + "<option value='" + sub.getId() + "'>"
			+ sub.getNaicSubCategoryDesc() + "</option>";
    }
    buffer = buffer + "</select>";

    response.getWriter().println(buffer);
    response.setHeader("Cache-Control", "no-cache");
%>