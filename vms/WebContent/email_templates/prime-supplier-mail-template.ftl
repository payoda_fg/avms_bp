<html>
<body style='color:#083B69;'>
Dear Supplier,<br/><br/>

Please find your AVMS credentials below to include the following: <br/><br/>

1. Email ID confirmation <br/>
2. Temporary Password <br/><br/>

	EmailId : ${emailID}<br/>

	Password : ${password}<br/><br/>

Your temporary password will expire 7 days from today's date.  <br/>

You may use the below URL to log into your AVMS account.  <br/>

<a href="${serverName}/viewanonymousvendors.do?parameter=emailLogin&prime=yes"> Click here</a> <br/><br/>

To log in and register, use the following steps:  <br/>

<ol>
<li>
 Go to the AVMS registration portal using the above link. </li>
<li> Enter your Email ID and temporary password. </li>
<li> System will take you to the Reset Password section. </li>
<li> Enter your company information. </li>
<li> Finalize the registration process by completing all required fields. </li>
<li> Confirm that your information is complete and accurate. </li>
<li> Submit your form to be reviewed by BP. </li>
</ol>
If you have any questions, you may email <a href="mailto:support@avmsystem.com">
support@avmsystem.com</a> <br/><br/>

Thank you. <br/><br/>

Warmest Regards, <br/>
AVMS Support Team.
</body>
</html>