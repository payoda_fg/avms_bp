<%@page import="com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl"%>
<%@page import="com.fg.vms.customer.dao.VendorDocumentsDao"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.customer.model.VendorCertificate"%>
<%@page import="com.fg.vms.customer.dao.impl.VendorDaoImpl"%>
<%@page import="com.fg.vms.customer.dao.VendorDao"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		Integer id = Integer
				.parseInt(request.getParameter("id").toString());
		VendorDocumentsDao documentsDao=new VendorDocumentsDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		VendorCertificate certificate = documentsDao.downloadCertificate(id,
				userDetails);
		try {
			response.setHeader("Content-Disposition", "inline;filename=\""
					+ certificate.getFilename() + "\"");
			OutputStream out1 = response.getOutputStream();
			response.setContentType("application/octet-stream");
			response.setContentLength((int) certificate.getContent()
					.length());
			IOUtils.copy(certificate.getContent().getBinaryStream(), out1);

			out1.flush();
			out1.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	%>
</body>
</html>