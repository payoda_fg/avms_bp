<%@page import="com.fg.vms.customer.dao.impl.VendorDocumentsDaoImpl"%>
<%@page import="com.fg.vms.customer.dao.VendorDocumentsDao"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page import="java.io.*"%>
<%@page import="com.fg.vms.util.Constants"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		Integer id = Integer
				.parseInt(request.getParameter("id").toString());
		VendorDocumentsDao documentsDao = new VendorDocumentsDaoImpl();
		UserDetailsDto userDetails = (UserDetailsDto) session
				.getAttribute("userDetails");
		String companyCode = userDetails.getCustomer().getCustCode();
		VendorDocuments documents = documentsDao.downloadDocument(id,
				userDetails);
		OutputStream out1 = null;
		try {

			BufferedInputStream filein = null;
			BufferedOutputStream output = null;
			File file = new File(
					Constants.getString(Constants.UPLOAD_DOCUMENT_PATH)
							+ File.separator + companyCode + File.separator
							+ documents.getVendorId().getId()
							+ File.separator
							+ documents.getDocumentFilename()); // path of file
			// 			out.println(Constants.getString(Constants.UPLOAD_DOCUMENT_PATH)
			// 					+  File.separator + documents.getVendorId().getId()
			// 					+  File.separator 
			// 					+ documents.getDocumentFilename());
			if (file.exists()) {

				filein = new BufferedInputStream(new FileInputStream(file));
				response.setHeader(
						"Content-Disposition",
						"inline;filename=\""
								+ documents.getDocumentFilename() + "\"");
				out1 = response.getOutputStream();
				response.setContentType("application/octet-stream");
				response.setContentLength(documents.getDocumentFilesize()
						.intValue());
				int len = 0;
				byte[] buf = new byte[104900];
				while ((len = filein.read(buf)) > 0) {
					out1.write(buf, 0, len);
				}
				out1.flush();
				out1.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	%>
</body>
</html>