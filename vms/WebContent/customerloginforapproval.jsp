<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script type="text/javascript">
<!--
/**
 *  Redirection for supplier registration page 
 */
function supplierRegistration() {
	window.location = "viewanonymousvendors.do?parameter=viewAnonymousVendors";
}

//-->
</script>
<div id="Login-Container-boder">
	<h2>LOGIN</h2>
	<div id="Login-Container-form">
		<html:form
			action="/customerloginapproval.do?parameter=authenticateCustomerforApproval"
			styleClass="AVMS">
			<div class="login-box">
				<ul>
					<li>
						<div>
							<label class="desc">Login ID</label>
							<div>
								<html:text property="userLogin" name="customerloginapprovalForm"></html:text>
							</div>
						</div>
					</li>
					<li style="height: 75px;">
						<div>
							<label class="desc">Password</label>
							<div>
								<html:password property="userPassword"
									name="customerloginapprovalForm"></html:password>
								<br>
								<p>
									<span style="color: red; font-style: italic;"><html:errors
											property="login"></html:errors> </span>
								</p>
							</div>
						</div>
					</li>
					<li style="text-align: center; padding-top: 33px;"><html:submit
							value=" " styleClass="login-btn"></html:submit></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="register-here">
				<p class="box">New Vendors</p>
				<br>
				<li style="text-align: center"><input class="reg-btn"
					type="button" name="signin" onclick="supplierRegistration()"></li>
			</div>

		</html:form>
	</div>
</div>