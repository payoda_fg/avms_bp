<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.customer.model.VendorMaster"%>
<%@page import="com.fg.vms.customer.dao.impl.CommonDaoImpl"%>
<%@page import="com.fg.vms.customer.dao.CommonDao"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html; charset=UTF-8"%>

<%
	CommonDao common = new CommonDaoImpl();
	Integer categoryId = Integer.parseInt(request.getParameter(
			"categoryId").toString());
	Integer naicsSubCategoryId = Integer.parseInt(request.getParameter(
			"subcategoryId").toString());

	UserDetailsDto appDetails = (UserDetailsDto) session
			.getAttribute("userDetails");
	List<VendorMaster> listOfVendors = common.getAllVendorsByCategory(
			categoryId, naicsSubCategoryId,appDetails);

	String buffer = "<select name='allVendors' multiple='true' id='allvendors' style='width:210px;'>";

	for (VendorMaster vendor : listOfVendors) {
		buffer = buffer + "<option value='" + vendor.getId() + "'>"
				+ vendor.getVendorName() + "</option>";
	}
	buffer = buffer + "</select>";
	response.setHeader("Cache-Control", "no-cache");
	response.getWriter().println(buffer);
%>