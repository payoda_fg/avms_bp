/**
 * 
 */
function AJAXInteraction(url, callback, object, type) {

	var req = init();
	req.onreadystatechange = processRequest;

	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	function processRequest() {
		// readyState of 4 signifies request is complete
		if (req.readyState == 4) {
			// status of 200 signifies sucessful HTTP call
			if (req.status == 200) {
				if (callback)
					callback(req.responseXML, object, type);
			}
		}
	}

	this.doGet = function() {
		// make a HTTP GET request to the URL asynchronously
		req.open("GET", url, true);
		req.send(null);
	};

}
/**
 * Ajax validation for user name , email id.. when create the new record.
 * 
 * @param object
 * @param type
 */
function ajaxFn(object, type) {
	if (object.value != "") {
		var url = "validationServlet.ajax?object="
				+ encodeURIComponent(object.value) + "&type="
				+ encodeURIComponent(type);
		var ajax = new AJAXInteraction(url, validateCallback, object, type);
		ajax.doGet();
	}

}

/**
 * Ajax validation for user name , email id.. when update the record.
 * 
 * @param object
 * @param type
 */
function ajaxEdFn(object, existValue, type) {

	if (object.value != "") {
		if ((document.getElementById(existValue).value.toLowerCase()) != (object.value
				.toLowerCase())) {
			var url = "validationServlet.ajax?object="
					+ encodeURIComponent(object.value) + "&type="
					+ encodeURIComponent(type);
			var ajax = new AJAXInteraction(url, validateCallback, object, type);
			ajax.doGet();
		}
	}

}
function validateCallback(responseXML, object, type) {
	var msg = responseXML.getElementsByTagName("valid")[0].firstChild.nodeValue;

	if (msg == "false") {
		if (type == "U" || type == "VU" || type == "CU") {
			alert("This LoginId is already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "E" || type == "VE" || type == "CE" || type == "VT2E") {
			alert("This Email ID already exist.");
			object.focus();
			$("#contanctEmail").val('');
			if(type != "VT2E") {
				location.reload();
			}
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "C" || type == "CC") {
			alert("This company code is already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "R" || type == "VR") {
			alert("This role is already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "DB") {
			alert("This database already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "URL") {
			alert("This URL already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "D") {
			alert("This Duns Number already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "T") {
			alert("This TaxId already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "NC") {
			alert("This NAICS Code already exist.");
			object.focus();
			var submitBtn = document.getElementById("submit");
			submitBtn.disabled = true;
		} else if (type == "P") {
			alert("Your Prime Vendor license counts exceeds SLA. ");
			object.focus();
			$('.selectedPrimeVendor').prop('checked', false); 
		}
	} else {
		var submitBtn = document.getElementById("submit");
		submitBtn.disabled = false;
	}
}

function deleteVendor(id) {
	input_box = confirm("Are you sure you want to delete this Record?");
	if (input_box == true) {
		var url = "validationServlet.ajax?object=" + encodeURIComponent(id)
				+ "&type=" + encodeURIComponent("delete");
		var ajax = new AJAXInteraction(url, deleteCallback, id, 'delete');
		ajax.doGet();
		return true;
	} else {
		// Output when Cancel is clicked
		alert("Delete cancelled");
		return false;
	}

}

function deleteCallback(responseXML, object, type) {
	var msg = responseXML.getElementsByTagName("valid")[0].firstChild.nodeValue;

	if (msg == "false") {
		alert("Cannot Delete Vendor. Its have Reference!");
	} else {
		alert("Vendor deleted!");
	}

}
