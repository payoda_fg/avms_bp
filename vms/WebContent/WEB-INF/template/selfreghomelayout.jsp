<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html class="fixed no-overflowscrolling sidebar-left-collapsed">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="Author" content="" />
<meta name="keyphrase" content="" />
<meta name="Title" content="" />
<meta name="classification" content="" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="subject" content="" />
<meta name="page-type" content="" />
<meta name="audience" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="robots" content="index,follow" />
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0" />
<script type="text/javascript" language="javascript" >
	document.createElement('header');
	document.createElement('nav');
	document.createElement('slider');
	document.createElement('aside');
	document.createElement('figure');
	document.createElement('article');
	document.createElement('section');
	document.createElement('footer');
</script>
<layout:skin includeScript="true" />

<!--<link href="css/bp_style.css" rel="stylesheet" type="text/css" />
<link href="css/media-queries.css" rel="stylesheet" type="text/css" />-->
<script src="js/modernizr-2.6.1.min.js"></script>

<style>
<!--
#ajaxloader {
	position: absolute;
	left: 50%;
	top: 50%;
	display: none;
	z-index: 99999;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}

.ajax-loader {
	margin-left: -32px; /* -1 * image width / 2 */
	margin-top: -32px; /* -1 * image height / 2 */
	display: block;
}

.placeholder {
	color: #aaa;
}
-->
</style>

<title><tiles:getAsString name="title" /></title>
<link href="images/favicon.ico" rel="SHORTCUT ICON">

<noscript>	
	<meta HTTP-EQUIV="REFRESH" content="0; url=noscript_page.jsp"> 
	<style>
		.page-margin-wrapper{
			display:none;
		}
	</style>
</noscript>

</head>
<body>

	<div class="page-margin-wrapper">
		<div>
			<tiles:insert name="header" attribute="header" />
		</div>
		<div class="clear"></div>
		<div class="clear"></div>
		<div class="page-wrapper">
			<div id="content-area">
				<div class="innepage-content" style="width: 100%;">
					<div id="ajaxloader" style="display: none;">
						<img src="images/ajax-loader1.gif" alt="Ajax Loading Image"
							class="ajax-loader" />
					</div>
					<tiles:insert name="body" attribute="body" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div>
			<tiles:insert name="footer" attribute="footer" />
		</div>
		<div class="clear"></div>
	</div>
</body>
<script type="text/javascript">
	/* add placeholder attr to all text field  */
	$('input[type="text"]').each(function() {
		//$(this).addClass('text-box');
		if ($(this).attr('alt') == 'Optional') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', 'Optional');
			}
		} else if ($(this).attr('alt') == 'Please click to select date') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', 'Please click to select date');
			}
		}
	});
	
	setTimeout(function() {
		$('#successMsg').fadeOut('slow');
		$('#failureMsg').fadeOut('slow');
	}, 5000);
</script>
<!-- Set focus on first form field -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(':input:text:enabled:visible:first').focus();
		/* jquery place holder */
		$(function() {
			$('input').placeholder();
		});
		
		jQuery(document).on('click', '.toggletrigger', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer').slideToggle();
		});
		jQuery(document).on('click', '.toggletrigger1', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer1').slideToggle();
		});
		jQuery(document).on('click', '.toggletrigger2', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer2').slideToggle();
		});

		jQuery(document).bind('click', function(e) {
			// event.preventDefault();   
			$('.togglecontainer').slideUp();
			$('.togglecontainer1').slideUp();
			$('.togglecontainer2').slideUp();
		});
	});
</script>
</html>