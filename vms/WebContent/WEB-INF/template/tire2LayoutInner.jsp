<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- include custom error page in this jsp code --%>
<html class="fixed no-overflowscrolling sidebar-left-collapsed">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="Author" content="" />
<meta name="keyphrase" content="" />
<meta name="Title" content="" />
<meta name="classification" content="" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="subject" content="" />
<meta name="page-type" content="" />
<meta name="audience" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="robots" content="index,follow" />
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0" />

<layout:skin includeScript="true" />
<style>
.placeholder {
	color: #aaa;
}
</style>
<!--<link href="css/bp_style.css" rel="stylesheet" type="text/css" />
 <link href='css/fontFace.css' rel='stylesheet' type='text/css'> -->
<link href="css/media-queries.css" rel="stylesheet" type="text/css">
<script src="js/modernizr-2.6.1.min.js"></script>

<script type="text/javascript" language="javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('slider');
	document.createElement('aside');
	document.createElement('figure');
	document.createElement('article');
	document.createElement('section');
	document.createElement('footer');
</script>

<script type="text/javaScript"
	src="<layout:resource type="cfg" name="swap.js"/>"></script>
<%
	UserDetailsDto applicationSettings = (UserDetailsDto) session
			.getAttribute("userDetails");
	CustomerApplicationSettings settings = applicationSettings
			.getSettings();
	String userType = applicationSettings.getUserType();
%>
<title><tiles:getAsString name="title" />
</title>
<link href="images/favicon.ico" rel="SHORTCUT ICON">

<noscript>	
	<meta HTTP-EQUIV="REFRESH" content="0; url=noscript_page.jsp"> 
	<style>
		.page-margin-wrapper{
			display:none;
		}
	</style>
</noscript>

</head>
<body>

	<div class="page-margin-wrapper">
		<div>
			<tiles:insert name="header" attribute="header" />
		</div>

		<div class="clear"></div>

		<div class="clear"></div>
		<div class="page-wrapper">
			<div id="content-area">
				<div>
					<tiles:insert name="menu" attribute="menu" />
				</div>

				<div class="innepage-content">
					<tiles:insert name="body" attribute="body" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div>
			<tiles:insert name="footer" attribute="footer" />
		</div>
		<div class="clear"></div>
	</div>
</body>
<script type="text/javascript">
	/* add placeholder attr to all text field  */
	$('input[type="text"]').each(function() {
		//$(this).addClass('text-box');
		if ($(this).attr('alt') == 'Optional') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', $(this).attr('alt'));
			}
		} else if ($(this).attr('alt') == 'Please click to select date') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', $(this).attr('alt'));
			}
		}
	});

	setTimeout(function() {
		$('#successMsg').fadeOut('slow');
		$('#failureMsg').fadeOut('slow');
	}, 5000);
</script>

<!-- Set focus on first form field -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(':input:enabled:visible:first').focus();

	});
	/* jquery place holder */
	$(function() {
		$('input, textarea').placeholder();
	});
</script>

</html>