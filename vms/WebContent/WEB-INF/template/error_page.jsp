<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isErrorPage="true"%>
<%@ page import="java.io.*"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error Page</title>
<style>
body {
	background: #f9fee8;
	margin: 0;
	padding: 20px;
	text-align: center;
	font-family: Verdana, sans-serif;
	font-size: 14px;
	color: #666666;
}

a {
	color: #9caa6d;
	text-decoration: none;
}

a:hover {
	color: green;
	text-decoration: underline;
}
</style>
</head>
<body style="padding: 20%;">
	<div class="error_page">
		<h1>We're sorry...</h1>
		<p>The page which you are looking for have some errors.</p>
	</div>
</body>
</html>