<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- include custom error page in this jsp code --%>
<html class="fixed no-overflowscrolling sidebar-left-collapsed">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="Author" content="" />
<meta name="keyphrase" content="" />
<meta name="Title" content="" />
<meta name="classification" content="" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="subject" content="" />
<meta name="page-type" content="" />
<meta name="audience" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="robots" content="index,follow" />
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0" />

<layout:skin includeScript="true" />
<style>
.placeholder {
	color: #aaa;
}
</style>
<!--<link href="css/bp_style.css" rel="stylesheet" type="text/css" />
 <link href='css/fontFace.css' rel='stylesheet' type='text/css'> -->
<link href="css/media-queries.css" rel="stylesheet" type="text/css">
<script src="js/modernizr-2.6.1.min.js"></script>

<script type="text/javascript" language="javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('slider');
	document.createElement('aside');
	document.createElement('figure');
	document.createElement('article');
	document.createElement('section');
	document.createElement('footer');
</script>

<script type="text/javaScript"
	src="<layout:resource type="cfg" name="swap.js"/>"></script>
<!-- <script type="text/javascript" src="js/customerConfig.js"></script> -->
<title><tiles:getAsString name="title" />
</title>
<link href="images/favicon.ico" rel="SHORTCUT ICON">
<style>
<!--
#ajaxloader {
	position: absolute;
	left: 50%;
	top: 50%;
	display: none;
	z-index: 99999;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}
.ajax-loader {
	margin-left: -32px; /* -1 * image width / 2 */
	margin-top: -32px; /* -1 * image height / 2 */
	display: block;
}
-->
</style>

<noscript>	
	<meta HTTP-EQUIV="REFRESH" content="0; url=noscript_page.jsp"> 
	<style>
		.page-margin-wrapper{
			display:none;
		}
	</style>
</noscript>

</head>
<body>
	<div class="page-margin-wrapper">
		<div>
			<tiles:insert name="header" attribute="header" />
		</div>
		<div class="clear"></div>
		<div class="clear"></div>
		
			
				<tiles:insert name="menu" attribute="menu" />
				<div class="innepage-content">
					<div id="ajaxloader" style="">
						<img src="images/ajax-loader1.gif" alt="Ajax Loading Image"
							class="ajax-loader" />
					</div>
					<tiles:insert name="body" attribute="body" />
				</div>
		<div class="clear"></div>
		<div>
			<tiles:insert name="footer" attribute="footer" />
		</div>
		<div class="clear"></div>
	</div>
</body>
<script type="text/javascript">
	/* add placeholder attr to all text field  */
	$('input[type="text"]').each(function() {
		//$(this).addClass('text-box');
		if ($(this).attr('alt') == 'Optional') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', $(this).attr('alt'));
			}
		} else if ($(this).attr('alt') == 'Please click to select date') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', $(this).attr('alt'));
			}
		}
	});

	setTimeout(function() {
		$('#successMsg').fadeOut('slow');
		$('#failureMsg').fadeOut('slow');
	}, 5000);
</script>

<!-- Set focus on first form field -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(':input:enabled:visible:first').focus();
	});
	/* jquery place holder */
	$(function() {
		$('input').placeholder();
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('body').on('click', '.toggletrigger', function(e) {
			e.stopPropagation();
			jQuery(this).next('.togglecontainer').slideToggle();
		})
		jQuery('body').on('click', '.toggletrigger1', function(e) {
			e.stopPropagation();
			jQuery(this).next('.togglecontainer1').slideToggle();
		})
		jQuery('body').on('click', '.toggletrigger2', function(e) {
			e.stopPropagation();
			jQuery(this).next('.togglecontainer2').slideToggle();
		})
		jQuery('body').on('click', '.toggletrigger3', function(e) {
			e.stopPropagation();
			jQuery(this).next('.togglecontainer3').slideToggle();
		})
		jQuery('body').on('click', '.toggletrigger4', function(e) {
			e.stopPropagation();
			jQuery(this).next('.togglecontainer4').slideToggle();
		})
		jQuery('body').bind('click', function(e) {
			// event.preventDefault();   
			jQuery('.togglecontainer').slideUp();
			jQuery('.togglecontainer1').slideUp();
		});
	});
</script>

</html>