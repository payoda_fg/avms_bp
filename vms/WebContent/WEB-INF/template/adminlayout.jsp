<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- include custom error page in this jsp code --%>
<%@ page errorPage="error_page.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta http-equiv="cache-control" content="no-cache"/>
<title><tiles:getAsString name="title" /></title>
<link href="images/favicon.ico" rel="SHORTCUT ICON">

<noscript>	
	<meta HTTP-EQUIV="REFRESH" content="0; url=noscript_page.jsp"> 
	<style>
		#wrapper{
			display:none;
		}
	</style>
</noscript>

</head>
<body>

	<div id="wrapper">
		<div id="header">
			<tiles:insert name="header" attribute="header" />
		</div>
		<div class="clear"></div>
		<div id="Container">
			<div id="form-container">
				<div id="form-container-in">
					<div id="Login-Container">
						<tiles:insert name="bodyleft" attribute="bodyleft" />
						<tiles:insert name="bodyright" attribute="bodyright" />

					</div>
				</div>
			</div>

		</div>
		<div class="clear"></div>
		<div id="footer">
			<tiles:insert name="footer" attribute="footer" />
		</div>

		<div class="clear"></div>
	</div>
</body>
<!-- Set focus on first form field -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(':input:enabled:visible:first').focus();

	});
</script>
</html>