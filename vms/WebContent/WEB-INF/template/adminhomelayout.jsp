<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- include custom error page in this jsp code --%>
<html class="fixed no-overflowscrolling sidebar-left-collapsed">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="Author" content="" />
<meta name="keyphrase" content="" />
<meta name="Title" content="" />
<meta name="classification" content="" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="subject" content="" />
<meta name="page-type" content="" />
<meta name="audience" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="robots" content="index,follow" />
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0" />

<layout:skin includeScript="true" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/media-queries.css" rel="stylesheet" type="text/css">
<script src="js/modernizr-2.6.1.min.js"></script>
<script type="text/javascript" language="javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('slider');
	document.createElement('aside');
	document.createElement('figure');
	document.createElement('article');
	document.createElement('section');
	document.createElement('footer');
</script>

<script type="text/javaScript"
	src="<layout:resource type="cfg" name="swap.js"/>"></script>
<%
	UserDetailsDto applicationSettings = (UserDetailsDto) session
			.getAttribute("userDetails");
	CustomerApplicationSettings settings = applicationSettings
			.getSettings();
	String userType = applicationSettings.getUserType();
%>


<script type="text/javascript" src="js/customerConfig.js"></script>
<title><tiles:getAsString name="title" /></title>
<link href="images/favicon.ico" rel="SHORTCUT ICON">

<noscript>	
	<meta HTTP-EQUIV="REFRESH" content="0; url=noscript_page.jsp"> 
	<style>
		.page-margin-wrapper{
			display:none;
		}
	</style>
</noscript>

</head>
<body>

	<div class="page-margin-wrapper">
		<div>
			<tiles:insert name="header" attribute="header" />
		</div>
		<div class="clear"></div>
		<%-- <div class="bluetabs-bg">
			<tiles:insert name="menu" attribute="menu" />
		</div> --%>
		<div class="clear"></div>
		<div>
			<tiles:insert name="body" attribute="body" />
		</div>
		<div class="clear"></div>
		<div>
			<tiles:insert name="footer" attribute="footer" />
		</div>
		<div class="clear"></div>
	</div>
</body>
<SCRIPT>
	$('input[type="text"]').each(function() {

		$(this).addClass('textBox');

		if ($(this).attr('alt') == 'Optional') {
			if ($(this).attr('value') == '') {
				this.value = $(this).attr('alt');
				$(this).addClass('text-label');
			}
		} else if ($(this).attr('alt') == 'Please click to select date') {
			if ($(this).attr('value') == '') {
				$(this).attr('placeholder', $(this).attr('alt'));
			}
		}

		$(this).focus(function() {
			if (this.value == $(this).attr('alt')) {
				this.value = '';
				$(this).removeClass('text-label');
			}
		});

		$(this).blur(function() {
			if (this.value == '') {
				this.value = $(this).attr('alt');
				$(this).addClass('text-label');
			}
		});

		//$('.bluetabs-bg ul li a').hover(function(){ $(this).css({"position":"relative","z-index":"200"}); });
		//$('.bluetabs-bg a').mouseout(function(){ $(this).focus(); });

	});

	$('input[type="text"]').focus(function() {
		$(this).addClass("focus");
	});

	$('input[type="text"]').blur(function() {
		$(this).removeClass("focus");
	});

	$('input[type="password"]').each(function() {
		$(this).addClass('textBox');
	});

	$('input[type="password"]').focus(function() {
		$(this).addClass("focus");
	});

	$('input[type="password"]').blur(function() {
		$(this).removeClass("focus");
	});

	setTimeout(function() {
		$('#successMsg').fadeOut('slow');
		$('#failureMsg').fadeOut('slow');
	}, 5000);
</SCRIPT>
<%-- <script type="text/javascript">
<%if (settings != null && !userType.equalsIgnoreCase("FG")) {%>
var primary='#<%=settings.getHeaderPrimaryColor()%> ';
var menubar=' #<%=settings.getMenuBarColor()%> ';
var menuselect='#<%=settings.getMenuSelectColor()%>';
var text='#<%=settings.getBodyTextColor()%>';
var background='#<%=settings.getBodyBackgroundColor()%>';
var menubutton='#<%=settings.getMenuButtonColor()%>
	';

	jQuery(document).ready(function($) {
		$('#header').css('background', primary);
		$('body').css('background', background);
		$('body').css('color', text);
		$('#box2 TABLE').css('color', text);
		$('div .ui-widget-content').removeClass('ui-widget-content').css({
			color : text,
			border : '1px solid #A6C9E2'
		});
		$('.bluetabs').css('background', menubar);
		$('.bluetabs LI A').css('background', menubutton);

		$(':input:enabled:visible:first').focus();

	});
<%}%>
	
</script> --%>
<!-- Set focus on first form field -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(':input:text:enabled:visible:first').focus();
	});
</script>
</html>