<%@page import="com.fg.vms.customer.model.Template"%>
<%@page import="java.util.*"%>
<%
	ArrayList<Template> templates = null;
	if (session.getAttribute("reviewtemplates") != null) {

		templates = (ArrayList<Template>) session
				.getAttribute("reviewtemplates");
	}
	if (templates != null) {
		int i = 1;
		out.println("<option value='" + 0 + "'>-- Select --</option>");
		for (Template template : templates) {

			out.println("<option value='" + template.getId() + "'>"
					+ template.getTemplateName() + "</option>");
			i++;
		}

	}
%>