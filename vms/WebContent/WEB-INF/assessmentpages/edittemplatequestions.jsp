<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="height: 95%; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp; Add
				Template Questions
			</h2>
		</div>

		<div id="form-wrapper">
			<div id="successMsg">
				<html:messages id="msg" property="template" message="true">
					<span><bean:write name="msg" /></span>
				</html:messages>
			</div>
			<html:form
				action="/updateTemplateQuestions?parameter=updateTemplateQuestions"
				styleId="formID">
				<div id="table-holder" style="width: 99.5%;">
					<html:javascript formName="templateQuestionForm" />
					<table cellspacing="20">

						<tr>
							<html:hidden property="templateId" />
							<td>Question Order</td>
							<td><html:text property="questionOrder"
									name="templateQuestionForm" alt="" /> <span class="error">
									<html:errors property="questionOrder"></html:errors>
							</span></td>
							<td>
						</tr>
						<tr>
							<td>Assessment Question</td>
							<td><html:text property="questionDescription"
									name="templateQuestionForm" alt=""
									style="width:500px;height:50px;" /> <span class="error">
									<html:errors property="questionDescription"></html:errors>
							</span></td>

						</tr>
						<tr>
							<td>Answer Data Type</td>
							<td><html:select property="answerDatatype"
									name="templateQuestionForm">
									<html:option value="-1">----- Select -----</html:option>
									<html:option value="Boolean">Boolean</html:option>
									<html:option value="VarChar">VarChar</html:option>
									<html:option value="Char">Char</html:option>
									<html:option value="Number">Number</html:option>
									<html:option value="Date">Date</html:option>
								</html:select></td>
						</tr>

						<tr>
							<td>Question Weightage</td>
							<td><html:text property="answerWeightage"
									name="templateQuestionForm" alt="" /> <span class="error">
									<html:errors property="answerWeightage"></html:errors>
							</span></td>
						</tr>
						<tr>
							<td><html:submit value="Update" styleClass="customerbtTxt"
									styleId="submit"></html:submit></td>
						</tr>
					</table>
				</div>

			</html:form>
		</div>

		<div id="box2">
			<table width="100%">
				<logic:present name="templateQuestionsList">
					<bean:size id="size" name="templateQuestionsList" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<th>Question Order</th>
								<th>Assessment Questions</th>
								<th>Answer Data Type</th>
								<th>Question Weightage</th>
								<th>Actions</th>
							</tr>
						</thead>
						<logic:iterate name="templateQuestionsList" id="templateQuestions">
							<tbody>
								<tr>
									<td><bean:write name="templateQuestions"
											property="questionOrder" /></td>
									<td><bean:write name="templateQuestions"
											property="questionDescription" /></td>
									<td><bean:write name="templateQuestions"
											property="answerDatatype" /></td>
									<td><bean:write name="templateQuestions"
											property="answerWeightage" /></td>
									<bean:define id="templateQuestionId" name="templateQuestions"
										property="id"></bean:define>
									<td><html:link action="#">Edit</html:link> &nbsp;&nbsp; <html:link
											action="/deleteTemplateQuestion.do?parameter=deleteTemplateQuestion"
											paramId="id" paramName="templateQuestionId"
											onclick="return confirm_delete();">Delete</html:link>
								</tr>
							</tbody>
						</logic:iterate>

					</logic:greaterThan>
				</logic:present>


			</table>

		</div>
	</div>
</div>
