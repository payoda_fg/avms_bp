<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<!-- <script src="js/common/dhtmlxgrid_pgn.js" type="text/javascript"
	charset="utf-8"></script> -->
<script src="js/common/connector.js" type="text/javascript"
	charset="utf-8"></script>

<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
<script
	src="js/dhtmlxChart/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js"></script>

<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='stylesheet' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>

<div class="page-title">Assessment Score Details</div>

<div class="form-box">
	<div class='xGridContainer'>
		<div id="box" style="background-color: white; height: 350px;"></div>
		<div
			style="display: none; color: red; position: absolute; top: 350px; left: 500px;"
			id="xId1_0">No records matched your search criteria...</div>
	</div>

	<script>
		mygrid = new dhtmlXGridObject('box');
		mygrid.setImagePath("js/common/imgs/");
		mygrid
				.setHeader("Question Order,Question Description, Answer Description, Reviewer Remarks, Weightage Awarded");
		mygrid.setInitWidths("100,*,100,*,100");
		mygrid.setColTypes("ro,ro,ro,ro,ro");
		mygrid.setColSorting("int,str,str,str,int");

		mygrid.init();
		mygrid.setSkin("light");
		mygrid.loadXML("grid_AssessmentScoreDetail.scoredetail");
		var dp = new dataProcessor("grid_AssessmentScoreDetail.scoredetail");
		mygrid.attachEvent("onXLE", function() {
			if (!mygrid.getRowsNum())
				$("div#xId1_0").show();
			else
				$("div#xId1_0").hide();
		});
		mygrid.attachEvent("onEditCell", function(state, rowId, cellIndex) {
			if (state == 0 && cellIndex == 2)
				return false;
			return true;
		});
		dp.init(mygrid);
	</script>
</div>

