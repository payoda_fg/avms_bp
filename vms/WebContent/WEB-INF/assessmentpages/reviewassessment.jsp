<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>
<!-- <script src="js/common/dhtmlxgrid_pgn.js" type="text/javascript"
	charset="utf-8"></script> -->
<script src="js/common/connector.js" type="text/javascript"
	charset="utf-8"></script>
<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='stylesheet' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>
<!-- <script type="text/javascript" src=jquery/js/jquery.validate.min.js"></script> -->
<script type="text/javascript">
	function templateOfVendor() {
		window.location = "assessmentreview.do?method=reviewTemplate";
	}
	$(document).ready(function() {

		$("#vendorId").change(function() {
			var id = $(this).val();
			var dataString = 'vendorSelected=' + id;

			$.ajax({
				type : "POST",
				url : "assessmentreview.do?method=reviewTemplate",
				data : dataString,
				cache : false,
				success : function(html) {

					$("#templateId").html(html);
					$("#templateId").trigger("chosen:updated");
				}
			});
		});
	});
</script>
<section role="main" class="content-body card-margin pt-4">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">Templates to be reviewed</h2>
			</header>
<div class="form-box card-body">
	<html:form action="/assessmentreview.do?method=reviewTemplate"
		styleId="assessmentAnswerForm">		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<logic:present name="selectedVendorForReview">
						<bean:define id="selectedVendor" name="selectedVendorForReview"></bean:define>
						<html:select property="vendorId" styleId="vendorId"
							value="<%=selectedVendor.toString()%>" styleClass="chosen-select form-control">
							<html:option value="0">--Select--</html:option>
							<logic:present property="templatevendors"
								name="assessmentAnswerForm">
								<logic:iterate id="vendor" name="assessmentAnswerForm"
									property="templatevendors">
									<bean:define id="id" name="vendor" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="vendor" property="vendorName"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:present>
						</html:select>
					</logic:present>
					<logic:notPresent name="selectedVendorForReview">
						<html:select property="vendorId" styleId="vendorId"
							styleClass="chosen-select form-control">
							<html:option value="0">--Select--</html:option>
							<logic:present property="templatevendors"
								name="assessmentAnswerForm">
								<logic:iterate id="vendor" name="assessmentAnswerForm"
									property="templatevendors">
									<bean:define id="id" name="vendor" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="vendor" property="vendorName"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:present>
						</html:select>
					</logic:notPresent>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">IsReviewed</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:radio property="isreviewed" value="1"
						name="assessmentAnswerForm" styleId="isreviewed">&nbsp;Yes&nbsp;</html:radio>
					<html:radio property="isreviewed" value="0"
						name="assessmentAnswerForm" styleId="isreviewed">&nbsp;No&nbsp;</html:radio>
					<html:radio property="isreviewed" value="2"
						name="assessmentAnswerForm" styleId="isreviewed">&nbsp;All&nbsp;</html:radio>
				</div>
			</div>
		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Template</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<logic:present name="templatenameforreview">
						<bean:define id="selectedtemplate" name="templatenameforreview"></bean:define>
						<html:select property="templateId" name="assessmentAnswerForm"
							styleId="templateId" onchange="getSubject()"
							styleClass="chosen-select form-control">
							<logic:present name="reviewtemplates">
								<bean:size id="size" name="reviewtemplates"></bean:size>
								<logic:iterate name="reviewtemplates" id="templateList">
									<bean:define id="templateId" name="templateList" property="id"></bean:define>
									<html:option value="<%=templateId.toString()%>">
										<bean:write name="templateList" property="templateName"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:present>
						</html:select>
					</logic:present>
					<logic:notPresent name="templatenameforreview">
						<html:select property="templateId" name="assessmentAnswerForm"
							styleId="templateId" onchange="getSubject()"
							styleClass="chosen-select form-control">

							<logic:present name="reviewtemplates">
								<bean:size id="size" name="reviewtemplates"></bean:size>
								<logic:iterate name="reviewtemplates" id="templateList">
									<bean:define id="templateId" name="templateList" property="id"></bean:define>
									<html:option value="<%=templateId.toString()%>">
										<bean:write name="templateList" property="templateName"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:present>
						</html:select>
					</logic:notPresent>
				</div>
			</div>
			<div style="display: none; color: red;" id="xId1_0">No records
				matched your search criteria...</div>
		
		<footer class="mt-2 card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
					<html:submit value="Search" styleClass="btn btn-primary" styleId="submit"></html:submit>
					<html:reset value="Reset" styleClass="btn btn-default"
					onclick="templateOfVendor()"></html:reset>
				</div>
			</div>
		</footer>
	</html:form>
</div>

<div class="clear"></div>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Assessment Review" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div id="box"
				style="background-color: white; height: 250px; width: 100%;"></div>

		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Assessment Review" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box text-center">
				<h4>You have no rights to view template list</h4>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:present name="selectedVendorForReview">
	<logic:present name="isReviewedeforreview">
		<logic:notEqual value="0" name="selectedVendorForReview">
			<script>
				mygrid = new dhtmlXGridObject('box');
				mygrid.setImagePath("js/common/imgs/");
				//mygrid.setColAlign("center,center,center");
				mygrid
						.setHeader("Question, Answer Description,Default Weightage, Reviewed, Remarks, Weightage Awarded");
				mygrid.setInitWidths("320,160,120,120,130,120");
				mygrid.setColTypes("ro,ro,ro,ch,edtxt,ed");
				mygrid.sortRows(0, "str", "asc");
				mygrid.setColSorting("str,str,int,int,str,int");
				mygrid.init();
				mygrid.setSkin("light");
				mygrid.loadXML("grid_ReviewTemplates.drid3");
				var dp = new dataProcessor("grid_ReviewTemplates.drid3");
				mygrid.attachEvent("onXLE", function() {
					if (!mygrid.getRowsNum())
						$("div#xId1_0").show();
					else
						$("div#xId1_0").hide();
				})
				dp.init(mygrid);
			</script>
		</logic:notEqual>
	</logic:present>
</logic:present>
</div>
</div>
</section>