<%@page import="com.fg.vms.customer.model.Template"%>
<%@page import="com.fg.vms.customer.model.TemplateQuestions"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>
<script src="js/common/connector.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<!--  <link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='stylesheet' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>-->
	
<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />

<script type="text/javascript">
	function goToQuestionsPage() {
		window.location = "questions.do?method=showPage";
	}
	
	$(document).ready(function() {

		$("#answerDatatype").select2({width: "90%"});

	});
</script>
<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-9 mx-auto">	
		<div id="successMsg">
			<html:messages id="msg" property="template" message="true">
				<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
			</html:messages>
		</div>
		<section class="card">
			<header class="card-header">
				<h2 class="card-title pull-left">Add Template Questions :
				<bean:write name="template" property="templateName" />
			</h2>
		</header>
		<div class="form-box card-body">
			<html:form
		action="/createtemplateQuestions?method=createtemplateQuestions"
		styleId="formID">
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Template Questions" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="add">
					<html:javascript formName="templateQuestionForm" />

					<html:hidden property="templateId" />
						<div class="row-wrapper form-group row">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Question Order</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text property="questionOrder" styleClass="text-box form-control"
									name="templateQuestionForm" alt="" styleId="questionOrder" />
							</div>
							<span class="error"> <html:errors property="questionOrder"></html:errors>
							</span>
						</div>
						<div class="row-wrapper form-group row">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Assessment Question</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text property="questionDescription" styleId="questionDescription"
									name="templateQuestionForm" alt="" styleClass="text-box form-control" />
							</div>
							<span class="error"> <html:errors
									property="questionDescription"></html:errors>
							</span>
						</div>

						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Answer Data Type</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="answerDatatype"
									name="templateQuestionForm" styleId="answerDatatype" styleClass="form-control">
									<html:option value="-1">-- Select --</html:option>
									<html:option value="Yes/No">Yes/No</html:option>
									<html:option value="Memo">Memo</html:option>
									<html:option value="Text">Text</html:option>
									<html:option value="Value">Value</html:option>
									<html:option value="Date">Date</html:option>
								</html:select>
							</div>
						</div>

						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Question Weightage</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text property="answerWeightage" styleClass="text-box form-control"
									name="templateQuestionForm" alt="" styleId="answerWeightage"/>
							</div>
							<span class="error"> 
								<html:errors property="answerWeightage"></html:errors>
							</span>
						</div>
						<footer class="mt-2">
							<div class="row justify-content-end">
								<div class="col-sm-9 wrapper-btn">
									<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit1"></html:submit>
									<html:button value="Back" property="" onclick="goToQuestionsPage()" styleClass="btn" styleId="submit"></html:button>
								</div>
							</div>
						</footer>
				</logic:match>
			</logic:match>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Template Questions" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="add">
					<div style="text-align: center;">
						<h3>You have no rights to add template questions</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>
	</html:form>
	</div>
	</section>
	
<section class="card">
	<header class="card-header">
		<h2 class="card-title pull-left">Template Question List</h2>
	</header>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Template Questions" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box card-body">
			<div id="box" style="min-height: 150px; overflow: hidden;"></div>
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="Template Questions" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="view">
							<logic:iterate id="privilege" name="privileges">
								<logic:match value="Template Questions" name="privilege"
									property="objectId.objectName">
									<logic:match value="1" name="privilege" property="delete">
										<footer class="mt-2">
							<div class="row justify-content-end">
								<div class="wrapper-btn">
											<input type="button" name="delete" value="Delete Questions"
												class="btn btn-primary" onclick="mygrid.deleteSelectedRows()">
										</div>
									</div>
									</footer>
									</logic:match>
								</logic:match>
							</logic:iterate>
							<logic:iterate id="privilege" name="privileges">
								<logic:match value="Template Questions" name="privilege"
									property="objectId.objectName">
									<logic:match value="0" name="privilege" property="delete">
										<div style="text-align: center;">
											<h3>You have no rights to delete template questions</h3>
										</div>
									</logic:match>
								</logic:match>
							</logic:iterate>
						</logic:match>
					</logic:match>
				</logic:iterate>

			</div>
			<script>
				mygrid = new dhtmlXGridObject('box');
				mygrid.setImagePath("js/common/imgs/");
				mygrid
						.setHeader("Question Order, Question Description, Answer DataType, Answer Weightage");
				mygrid.setInitWidths("180,180,180,170");
				mygrid.setColTypes("edtxt,ed,ed,ed");
				mygrid.setColSorting("int,str,str,int");
				mygrid.init();
				mygrid.setSkin("light");
				mygrid.loadXML("grid_TemplateQuestions.drid1");
				var dp = new dataProcessor("grid_TemplateQuestions.drid1");
				dp.init(mygrid);
			</script>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Template Questions" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view template questions</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</section>
</div>
</div>
</section>
<script>
		
	jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
        return this.optional(element) || /^[0-9]+$/.test(value);
    },"Please enter only numbers.");
	
	 $(document).ready(function() {
		  
		$("#formID").validate({
			rules : {
				questionOrder : {
					required : true,
					onlyNumbers : true
				},
				questionDescription : {
					required : true
				},
				answerDatatype : {
					required : true
				},
				answerWeightage : {
					required : true,
					onlyNumbers : true
				}
			}
		});
	});
</script>