<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>


<html:html>
<head>
<script src="jquery/js/jquery.min.js"></script>
<link href="css/style.css" type="text/css" rel="stylesheet"></link>

<script type="text/javascript">
	function questions() {
		//var url = sel[sel.selectedIndex].value;
		var templateId = document.getElementById("templateId").value;
		window.location = "copyTemplate.do?method=templateQuestions&templateId="
				+ templateId;

	}

	function close_win() {
		this.opener.document.getElementById("templateName").value = "";
		window.opener.location.reload(true);
		close();
	}

	$(document).ready(function() {
		// add multiple select / deselect functionality
		$("#selectall").click(function() {
			$('.case').attr('checked', this.checked);
		});
	});

</script>

</head>
<div id="form-container" style="height: 100%; width: 95%;">

	<table cellpadding="10px" cellspacing="10px">
		<tr>
			<td><b>Select Template</b></td>
			<bean:define id="selectedTemplate" name="selectedTemplate"></bean:define>
			<td><html:select property="template" name="templateForm"
					style="width:120px;font-size:11px;" onchange="questions();"
					styleId="templateId" value="<%=selectedTemplate.toString() %>">
					<html:option value="0">-----Select-----</html:option>
					<logic:present name="templateNameList">
						<bean:size id="size" name="templateNameList"></bean:size>
						<logic:iterate name="templateNameList" id="templateList">
							<bean:define id="templateId" name="templateList" property="id"></bean:define>
							<html:option value="<%=templateId.toString() %>">
								<bean:write name="templateList" property="templateName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:present>
				</html:select></td>
		</tr>
	</table>

	<html:form action="/copyTemplate.do?method=saveCopyTemplate">
		<div id="box2">
			<table width="100%" border="0" cellspacing="10" cellpadding="10"
				align="center">
				<logic:present name="templateQuestions">
					<bean:size id="size" name="templateQuestions" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" /> Action</th>
								<th>Question Order</th>
								<th>Assessment Questions</th>
								<th>Answer Data Type</th>
								<th>Question Weightage</th>
							</tr>
						</thead>
						<logic:iterate name="templateQuestions" id="templateQuestion">
							<bean:define id="templateQuestionId" name="templateQuestion"
								property="id"></bean:define>
							<bean:define id="questionDescriptions" name="templateQuestion"
								property="questionDescription"></bean:define>
							<%
								if (questionDescriptions.toString() != null) {
							%>
							<tbody>
								<tr>
								
									<td align="center"><html:checkbox property="questions"
											name="templateForm" styleClass="case"
											value="<%=templateQuestionId.toString()%>"></html:checkbox></td>

									<td align="center"><bean:write name="templateQuestion"
											property="questionOrder" /></td>

									<td align="center"><bean:write name="templateQuestion"
											property="questionDescription" /></td>


									<td align="center"><bean:write name="templateQuestion"
											property="answerDatatype" /></td>

									<td align="center"><bean:write name="templateQuestion"
											property="answerWeightage" /></td>
								</tr>
							</tbody>
							<%
								}
							%>
						</logic:iterate>
						<tr>
							<td colspan="5" align="center"><html:submit value="Submit"
									styleClass="customerbtTxt" styleId="submit"></html:submit> <html:button
									styleClass="customerbtTxt" property="" value="Close"
									onclick="close_win();" /></td>
						</tr>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
				  No Questions
				</logic:equal>

				</logic:present>

			</table>
		</div>


	</html:form>
	<div id="successMsg">
		<html:messages id="msg" property="template" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>

	</div>
</div>
</html:html>