<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/common/connector.js" type="text/javascript"
	charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgridcell.js"></script>
<script
	src="js/dhtmlxChart/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='stylesheet' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>
<script type="text/javascript">
<!--
	function getScoreDetails() {
		var getTemplateForScore = $('#templateId').val();

		if (getTemplateForScore != "undefined" && getTemplateForScore != "") {
			window.location = "assessmentscore.do?method=viewscore&getTemplateForScore="
					+ getTemplateForScore;
		}
	}
//-->
</script>
<section role="main" class="content-body card-margin pt-5 mt-5 pb-4" style="min-height:400px;">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">Assessment Score Card</h2>
			</header>
<div class="form-box card-body">
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Capability Assessment Template</label>
		<div class="col-sm-7 ctrl-col-full-wrapper">
			<logic:present name="getTemplateForScore">
				<bean:define id="selectedtemplate" name="getTemplateForScore"></bean:define>
				<html:select property="templateId" name="assessmentAnswerForm"
					styleId="templateId" value="<%=selectedtemplate.toString()%>"
					onchange="getScoreDetails()" styleClass="chosen-select form-control">
					<html:option value="0">--Select--</html:option>
					<logic:present name="assessmentemaildetailtemplate">
						<%-- <bean:size id="size" name="assessmentemaildetailtemplate"></bean:size> --%>
						<logic:iterate name="assessmentemaildetailtemplate"
							id="templateList">
							<bean:define id="templateId" name="templateList" property="id"></bean:define>
							<html:option value="<%=templateId.toString()%>">
								<bean:write name="templateList" property="templateName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:present>
				</html:select>
			</logic:present>
		</div>
	</div>
	
	<div class="wrapper-half">
		<div style="display: none; color: red;" id="xId1_0">No records
			matched your search criteria...</div>
	</div>
</div>
<div class="clear"></div>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Assessment Score Card" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box card-body">
				<div id="box"
					style="background-color: white; height: 250px; width: 100%;"></div>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Assessment Score Card" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box text-center">
				<h4>You have no rights to view assessment score details</h4>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script>
	mygrid = new dhtmlXGridObject('box');
	mygrid.setImagePath("js/common/imgs/");
	mygrid
			.setHeader("Vendor Name, Date of Assessment,Avg Weightage Awarded, View Details");
	mygrid.setInitWidths("*,*,*,*");
	mygrid.setColTypes("link,ro,ro,link");
	mygrid.setColSorting("str,date,int,str");

	mygrid.init();
	mygrid.setSkin("light");
	mygrid.loadXML("grid_AssessmentScoreCard.score");
	var dp = new dataProcessor("grid_AssessmentScoreCard.score");
	mygrid.attachEvent("onXLE", function() {
		if (!mygrid.getRowsNum())
			$("div#xId1_0").show();
		else
			$("div#xId1_0").hide();
	});
	mygrid.attachEvent("onEditCell", function(state, rowId, cellIndex) {
		if (state == 0 && cellIndex == 2)
			return false;
		return true;
	});
	dp.init(mygrid);
</script>