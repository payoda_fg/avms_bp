<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/customerConfig.js"></script>

<script type="text/javascript">
// $('.bluetabs-bg a').hover(function(){ $("#templateName").blur(); });
// $('.bluetabs-bg a').mouseout(function(){ $("#templateName").focus(); });
function getActiveStatus(templateid,statusvalue){
	
	if(templateid != 0 && statusvalue != null && templateid != "undefined"){
		window.location = "questions.do?method=savestatus&templateid="+templateid+"&statusvalue="+statusvalue;
	}
}

$(document).ready(function(){
	jQuery.validator.addMethod("alpha", function(value, element) { 
        return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
  },"No Special Characters Allowed.");
	
	$("#formID").validate({
		rules : {
			templateName : {
				required : true,
				alpha : true
			},
		},
		/*submitHandler : function(form) {
			$("#ajaxloader").css('display','block');
			$("#ajaxloader").show();
			form.submit();
		}*/
	});
});

</script>
<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-12 mx-auto">	

	<div id="successMsg">
		<html:messages id="msg" property="statusupdate" message="true">
			<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
		</html:messages>
		<html:messages id="msg" property="template" message="true">
			<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
		</html:messages>
		<html:messages id="msg" property="templateReferenceDelete"
			message="true">
			<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
		</html:messages>
	</div>
	<section class="card">
		<header class="card-header">
			<h2 class="card-title pull-left">Add Template</h2>
		</header>

			<div class="form-box card-body">
				<html:form action="/createtemplate?parameter=create" styleId="formID">
					<html:javascript formName="templateForm" />
					<logic:iterate id="privilege" name="privileges">
						<logic:match value="Performance Assessment Template" name="privilege"
							property="objectId.objectName">
							<logic:match value="1" name="privilege" property="add">			
								<div class="row-wrapper form-group row">
									<label class="label-col-wrapper control-label text-sm-right col-sm-3">Template Name</label>
										<div class="col-sm-6 ctrl-col-wrapper">
											<html:text property="templateName" name="templateForm" alt=""
												styleId="templateName" styleClass="text-box form-control" />
										</div>
										<span class="error"> <html:errors property="templateName"></html:errors>
										</span>
									</div>
			
								<footer class="mt-2">
									<div class="row justify-content-end">
										<div class="col-sm-9 wrapper-btn">
											<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit1"></html:submit>
											<html:reset value="Reset" styleClass="btn btn-default"></html:reset>
												<!-- <input type="button" class="btn" value="Copy Existing Questions" -->
												<!-- onClick="javascript:fn_copyTemplate('copyTemplate.do?method=showCopyTemplate');" /> -->
										</div>
									</div>
								</footer>
							</logic:match>
						</logic:match>
					</logic:iterate>
					<logic:iterate id="privilege" name="privileges">
						<logic:match value="Performance Assessment Template" name="privilege"
							property="objectId.objectName">
							<logic:match value="0" name="privilege" property="add">
								<div style="padding: 5%; text-align: center;">
									<h3>You have no rights to add new template</h3>
								</div>
							</logic:match>
						</logic:match>
					</logic:iterate>
			
				</html:form>
			</div>
</section>
<section class="card">
	<header class="card-header">
		<h2 class="card-title pull-left">
			List of Templates</h2>
	</header>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Performance Assessment Template" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">
		
					<div class="form-box card-body">
						<table class="main-table table table-bordered table-striped mb-0" id="mytable">
							<logic:present name="templateNameList">
								<bean:size id="size" name="templateNameList" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<td>Template Name</td>
											<td>Active Status</td>
											<td>Actions</td>
										</tr>
									</thead>
									<tbody>
										<logic:iterate name="templateNameList" id="templateList"
											indexId="index">
											<tr>
												<td class="templatename"><bean:write name="templateList"
														property="templateName" /></td>
												<bean:define id="templateId" name="templateList" property="id"></bean:define>
		
												<td align="center"><logic:equal value="1"
														name="templateList" property="isActive">
														<input type="radio" name="isActive_<%=index%>" value="1"
															checked="checked"
															onclick="getActiveStatus(<bean:write name="templateList" property="id" />,this.value)" />
														&nbsp;Yes&nbsp;
													<input type="radio" name="isActive_<%=index%>" value="0"
															onclick="getActiveStatus(<bean:write name="templateList" property="id" />,this.value)" />
														&nbsp;No&nbsp;
												</logic:equal> 
												<logic:equal value="0" name="templateList" property="isActive">
														<input type="radio" name="isActive_<%=index%>" value="1"
															onclick="getActiveStatus(<bean:write name="templateList" property="id" />,this.value)" />
														&nbsp;Yes&nbsp;
													<input type="radio" name="isActive_<%=index%>" value="0"
															checked="checked"
															onclick="getActiveStatus(<bean:write name="templateList" property="id" />,this.value)" />
														&nbsp;No&nbsp;
												</logic:equal></td>
		
												<td><logic:iterate id="privilege" name="privileges">
														<logic:match value="Performance Assessment Template"
															name="privilege" property="objectId.objectName">
															<logic:match value="1" name="privilege" property="modify">
																<html:link
																	action="/retriveTemplate.do?parameter=retriveTemplate"
																	paramId="id" paramName="templateId">Edit</html:link>
															</logic:match>
														</logic:match>
													</logic:iterate> &nbsp;&nbsp; <logic:iterate id="privilege" name="privileges">
														<logic:match value="Performance Assessment Template"
															name="privilege" property="objectId.objectName">
															<logic:match value="1" name="privilege" property="delete">
																<html:link action="/deleteTemplate.do?parameter=delete"
																	paramId="id" paramName="templateId"
																	onclick="return confirm_delete();">Delete</html:link>
															</logic:match>
														</logic:match>
													</logic:iterate> &nbsp;&nbsp; <logic:iterate id="privilege" name="privileges">
														<logic:equal value="Template Questions" name="privilege"
															property="objectId.objectName">
															<logic:equal value="1" name="privilege" property="visible">
																<html:link
																	action="/templateQuestion.do?method=showTemplateQuestionpage"
																	paramId="id" paramName="templateId">Question</html:link>
															</logic:equal>
														</logic:equal>
													</logic:iterate></td>
											</tr>
										</logic:iterate>
									</tbody>
								</logic:greaterThan>
							</logic:present>
						</table>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Performance Assessment Template" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view template</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</section>

</div>
</div>
</section>

