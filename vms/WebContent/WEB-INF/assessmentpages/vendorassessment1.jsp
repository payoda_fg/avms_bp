<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<%
	Integer count = 0;
%>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript">
	$(function() {
		$(".date").datepicker({
			nextText : "",
			prevText : "",
			changeMonth : true,
			changeYear : true
		});
	});
</script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript">
	datePickerAssessment();
	var imgPath = "jquery/images/calendar.gif";
</script>
<bean:define id="currentUserId" name="userId"></bean:define>
<div class="page-title">Capability Assessment</div>
<div class="form-box" style="width: 95%; padding: 2%;">
	<div id="successMsg">
		<html:messages id="msg" property="assessment" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
				<html:form action="/saveAssmentAns?method=saveAssessmentAnswers">
					<html:javascript formName="assessmentAnswerForm" />
					<table class="main-table" >
						<logic:present name="assessmentQuestions">
							<bean:size id="size" name="assessmentQuestions" />
							<logic:greaterThan value="0" name="size">
								<logic:iterate id="assessmentQuestion"
									name="assessmentQuestions">
									<bean:define id="temp" name="assessmentQuestion" property="key"></bean:define>
									<bean:define id="tempQuestions" name="assessmentQuestion"
										property="value"></bean:define>
									<tr>
										<td
											style="font-weight: bolder; font-size: 14px; color: red; text-transform: capitalize; left: 150px;">
											<bean:write name="temp" property="templateName" />
										</td>
										<td><logic:iterate id="templateQuestions1"
												name="tempQuestions" indexId="ctr">
												<tr>
													<td><bean:write name="templateQuestions1"
															property="questions" /></td>
													<td><bean:define id="templateid"
															name="templateQuestions1" property="templateId"></bean:define>
														<html:hidden property="templateId"
															value="<%=templateid.toString()%>" /> <logic:equal
															value="Yes/No" property="dataType"
															name="templateQuestions1">
															<html:radio
																property='<%="labelValue["
															+ count + "].value"%>'
																value="1">Yes</html:radio>
															<html:radio
																property='<%="labelValue["
															+ count + "].value"%>'
																value="0">No</html:radio>
															<span class="error"><html:errors
																	property='<%="labelValue["
															+ count + "].value"%>'></html:errors></span>
															<bean:define id="templateQuestionId"
																name="templateQuestions1" property="templateQuestionId"></bean:define>
															<bean:define id="dataType" name="templateQuestions1"
																property="dataType"></bean:define>
															<html:hidden property="templateQuestionId"
																value="<%=templateQuestionId
														.toString()%>" />
														</logic:equal> <logic:equal value="Memo" property="dataType"
															name="templateQuestions1">
															<html:text
																property='<%="labelValue["
															+ count + "].value"%>'
																alt="" styleClass="text-box"></html:text>
															<span class="error"><html:errors
																	property='<%="labelValue["
															+ count + "].value"%>'></html:errors></span>
															<bean:define id="templateQuestionId"
																name="templateQuestions1" property="templateQuestionId"></bean:define>
															<html:hidden property="templateQuestionId"
																value="<%=templateQuestionId
														.toString()%>" />
														</logic:equal> <logic:equal value="Text" property="dataType"
															name="templateQuestions1">
															<html:text
																property='<%="labelValue["
															+ count + "].value"%>'
																alt="" styleClass="text-box"></html:text>
															<span class="error"><html:errors
																	property='<%="labelValue["
															+ count + "].value"%>'></html:errors></span>
															<bean:define id="templateQuestionId"
																name="templateQuestions1" property="templateQuestionId"></bean:define>
															<html:hidden property="templateQuestionId"
																value="<%=templateQuestionId
														.toString()%>" />
														</logic:equal> <logic:equal value="Value" property="dataType"
															name="templateQuestions1">
															<html:text
																property='<%="labelValue["
															+ count + "].value"%>'
																alt="" onblur="return validateNumber()"
																styleId="numericAnswer" styleClass="text-box"></html:text>
															<span class="error"><html:errors
																	property='<%="labelValue["
															+ count + "].value"%>'></html:errors></span>
															<bean:define id="templateQuestionId"
																name="templateQuestions1" property="templateQuestionId"></bean:define>
															<html:hidden property="templateQuestionId"
																value="<%=templateQuestionId
														.toString()%>" />
														</logic:equal> <logic:equal value="Date" property="dataType"
															name="templateQuestions1">
															<html:text styleClass="datePicker date text-box"
																property='<%="labelValue["
															+ count + "].value"%>'
																alt="Please click to select date"></html:text>
															<span class="error"><html:errors
																	property='<%="labelValue["
															+ count + "].value"%>'></html:errors></span>
															<bean:define id="templateQuestionId"
																name="templateQuestions1" property="templateQuestionId"></bean:define>
															<html:hidden property="templateQuestionId"
																value="<%=templateQuestionId
														.toString()%>" />
														</logic:equal></td>
												</tr>
												<%
													count++;
												%>
											</logic:iterate></td>
									</tr>
								</logic:iterate>
							</logic:greaterThan>
							<logic:equal value="0" name="size">
								<span style="color: red;">No Questions</span>
							</logic:equal>
						</logic:present>
					</table>
					<logic:present name="assessmentQuestions">
						<bean:size id="size" name="assessmentQuestions" />
						<logic:greaterThan value="0" name="size">
							<div class="btn-wrapper">
								<html:submit value="Submit" styleClass="btn"
									styleId="submit" />
								<html:reset value="Cancel" styleClass="btn" />
							</div>
						</logic:greaterThan>
					</logic:present>
				</html:form>
</div>
