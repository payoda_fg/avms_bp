<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript">
<!--
	/**
	 *  Redirection for supplier registration page 
	 */
	function supplierRegistration() {
		window.location = "viewanonymousvendors.do?parameter=viewAnonymousVendors";
	}
//-->
</script>

<div style="position: absolute; left: 250px; top: 160px;">
	<bean:define id="countFailureAttempts" name="count"></bean:define>
	<%
		if (Integer.parseInt(countFailureAttempts.toString()) == 2) {
	%>
	<span style="color: orange; font-size: 12px;"> You have entered
		wrong password two times.If you wrongly entered next time your account
		will be deactivated. </span>
	<%
		}
	%>
	<%
		if (Integer.parseInt(countFailureAttempts.toString()) >= 3) {
	%>
	<span style="color: red; font-size: 12px;"> You have entered
		wrong password three times. Your account deactivated. </span>

	<%
		}
	%>
</div>
<div id="Login-Container-boder">
	<h2>LOGIN</h2>
	<div id="Login-Container-form">
		<html:form action="/login.do?method=authenticate" styleClass="AVMS">

			<div class="login-box">
				<ul>
					<li>
						<div>
							<label class="desc">Login ID</label>
							<div>
								<html:text property="userEmailId" name="loginForm" styleClass="text-box"></html:text>
							</div>
						</div>
					</li>
					<li style="height: 75px;">
						<div>
							<label class="desc">Password</label>
							<div>
								<html:password property="userPassword" name="loginForm" styleClass="text-box"></html:password>
								<br>
								<p>
									<html:link forward="/forgot">Forgot your Password?</html:link>
									<br>
								</p>

								<p>
									<span style="color: red; font-style: italic;"><html:errors
											property="login"></html:errors> </span>
								</p>
							</div>
						</div>
					</li>
					<li style="text-align: center; padding-top: 33px;"><html:submit
							value=" " styleClass="login-btn"></html:submit></li>
				</ul>
				<div class="clear"></div>
			</div>

		</html:form>
	</div>
</div>