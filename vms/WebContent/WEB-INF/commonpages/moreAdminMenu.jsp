
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<!--Menupart -->
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<!-- <div class="inner-menu"> -->
<section class="body">
	<header class="page-header">
		<h2>Administration</h2>
	</header>
		<div class="inner-wrapper btn-orange">
			<aside id="sidebar-left" class="sidebar-left">			
			    <div class="sidebar-header">
			         <div class="sidebar-title">Navigation</div>
			        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
			        </div>
			    </div>
		    <div class="nano">
		    <div class="nano-content">
		    	<nav id="menu" class="nav-main" role="navigation">
		    		<ul class="nav nav-main">
		    			<li class="nav-active">
							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Roles" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<html:link action="/viewuserrole.do?parameter=viewUserRoles"
											styleClass="icon-12 nav-link" styleId="menuselect0">
											<i class="icon-img"><img src="bpimages/roles.png" />
											</i>											
											<span class="icon-text">Manage Roles</span>
										</html:link>
									</logic:equal>
								</logic:equal>
							</logic:iterate>
						</li>
				<li>		
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="User" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/viewuser.do?parameter=viewUsers"
								styleClass="icon-13 nav-link" styleId="menuselect1">
								<i class="icon-img"><img src="bpimages/users.png" />
								</i>								
								<span class="icon-text">Manage Users</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Role Privileges" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/roleprivileges.do?method=rolePrivileges"
								styleClass="icon-17 nav-link" styleId="menuselect2">
								<i class="icon-img"><img
									src="bpimages/roleprivileges.png" /> </i>
									
								<span class="icon-text">Role Privileges</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="View Certificate" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/viewcertificate.do?parameter=viewCertificate"
								styleClass="icon-16" styleId="menuselect5">
								<i class="icon-img"><img
									src="bpimages/manageclassifications.png" /> </i>									
								<span class="icon-text">Manage Classifications</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Certifying Agency" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/viewagencies.do?method=view"
								styleClass="icon-23" styleId="menuselect6">
								<i class="icon-img"><img
									src="bpimages/manage-certifying-agency.png" /></i>
									
								<span class="icon-text">Manage Certifying Agencies</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Email Distribution" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/emaildistribution.do?method=viewEmailDistribution"
								styleClass="icon-2 ">
								<i class="icon-img"><img
									src="bpimages/emaildistribution.png" /> </i>									
								<span class="icon-text">Email Distribution</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Workflow" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/workflowconfig.do?method=viewWorkflowConfig"
								styleClass="icon-14 ">
								<i class="icon-img"><img
									src="bpimages/workflowconfiguration.png" /> </i>									
								<span class="icon-text">Workflow Configuration</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="NAICS Master" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/naicscode.do?method=viewNaicsCode"
								styleClass="icon-15" styleId="menuselect8">
								<i class="icon-img"><img src="bpimages/naicscode.png" />
								</i>								
								<span class="icon-text">NAICS Code</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Reporting Period" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/reportingperiod.do?method=showPage"
								styleClass="icon-27">
								<i class="icon-img"><img
									src="bpimages/tier2-reporting-period.png" /> </i>
									
								<span class="icon-text">Tier2 Reporting Period
								</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Performance Assessment Template"
						name="privilege" property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/questions.do?method=showPage"
								styleClass="icon-25">
								<i class="icon-img"><img
									src="bpimages/performance-assessment-template.png" /> </i>
									
								<span class="icon-text">Performance Assessment Template
								</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Ethnicity" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/ethnicity.do?method=view"
								styleClass="icon-29">
								<i class="icon-img"><img src="bpimages/ethnicity.png" />
								</i>
								
								<span class="icon-text">Ethnicity</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Tier 2 Email Notification" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/proactiveMonitoring.do?method=proactiveMonitoring"
								styleClass="icon-26">
								<i class="icon-img"><img
									src="bpimages/tier2emailnotification.png" /> </i>
									
								<span class="icon-text">Tier 2 Email Notification </span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Edit Information" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/retrivecustomer?method=retriveCustomerInfoFromCustomerDB"
								styleClass="icon-24">
								<i class="icon-img"><img
									src="bpimages/edit-customer-info.png" /> </i>
									
								<span class="icon-text">Edit Customer Info</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				<!--  Dashboard move to RolePrivileges, -->
				<%-- <logic:iterate id="privilege" name="privileges">
					<logic:equal value="Dashboard Settings" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/dashboardsettings?method=viewDashboardSettings"
								styleClass="dashboard-settings">
								<span class="icon-text">Dashboard Settings</span>
								<span class="icon-img"><img
									src="bpimages/dashboard-settings.png" /> </span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate> --%>
				<%-- <logic:iterate id="privilege" name="privileges">
					<logic:equal value="Market Subsector" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/commoditycategory?method=viewCommodityCategory"
								styleClass="commodity-category">
								<span class="icon-text">Market Subsector</span>
								<span class="icon-img"><img
									src="bpimages/commodity-category.png" /> </span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate> --%>
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Commodity Group" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/commoditycategory?method=viewCommodity"
								styleClass="commodity">
								<i class="icon-img"><img src="bpimages/commodity.png" />
								</i>
								
								<span class="icon-text">Commodity Group</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Business Area" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/geographicalsettings?method=showGeographicalSettingsPage"
								styleClass="geographical-settings">
								<i class="icon-img"><img
									src="bpimages/geographical-settings.png" /> </i>
									
								<span class="icon-text">Business Area</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Email Template" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/emailtemplate?parameter=showEmailTemplatePage"
								styleClass="email-template">
								<i class="icon-img"><img
									src="bpimages/email-template.png" /> </i>
									
								<span class="icon-text">Email Template</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Certification Type" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/certificationType?parameter=showCertificatePage"
								styleClass="certification-type">
								<i class="icon-img"><img
									src="bpimages/certification-type.png" /> </i>									
								<span class="icon-text">Certification Type</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Country" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/countrystate?method=showCountryStateSettingsPage"
								styleClass="country">
								<i class="icon-img"><img
									src="bpimages/country.png" /> </i>
									
								<span class="icon-text">Country and State</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<%-- <logic:equal value="1" name="isDivisionStatus" property="isDivision"> --%>
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Manage Division" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<html:link
									action="/customerDivision?method=showMangeDivisionPage"
									styleClass="manage-division">
									<i class="icon-img"><img
										src="bpimages/manage_division.png" /> </i>
										
									<span class="icon-text">Manage Division</span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
				</li>
				<%-- </logic:equal> --%>
				
				<li>
				<!-- For New Menu-UserloginLog under Admin -->
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="View User Log" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/userLogTrack?parameter=viewUserLoginDetails"
								styleClass="view-log">
								<i class="icon-img"><img
									src="bpimages/view-log.png" /> </i>
									
								<span class="icon-text">View User Log</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				
				<li>
				<!--  Credentialing -->
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Credentialing" name="privilege" property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/customeruserregistration?method=viewNotApprovedStakeholders" styleClass="credentialing">
								
								<i class="icon-img"><img src="bpimages/credentialing.png" /></i>
								<span class="icon-text">Credentialing</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				</li>
				   </ul>
		            </nav>
				
	            <hr class="separator" />
				</div>
				</div>
			</aside>
		</div>
		<div class="clear"></div>
</section>
<!-- </div> -->
<!--<div class="inner-menu-list-orange">

	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Roles" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/viewuserrole.do?parameter=viewUserRoles"
					styleId="menuselect0">
					<img src="images/new-icon/icon-12.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="User" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/viewuser.do?parameter=viewUsers"
					styleId="menuselect1">
					<img src="images/new-icon/icon-13.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Role Privileges" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/roleprivileges.do?method=rolePrivileges"
					styleId="menuselect2">
					<img src="images/new-icon/icon-13.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>

	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="View Certificate" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/viewcertificate.do?parameter=viewCertificate"
					styleId="menuselect5">
					<img src="images/new-icon/icon-16.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Certifying Agency" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/viewagencies.do?method=view"
					styleId="menuselect6">
					<img src="images/new-icon/icon-17.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Certifying Agency" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link
					action="/emaildistribution.do?method=viewEmailDistribution">
					<img src="images/new-icon/icon-2.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Certifying Agency" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/workflowconfig.do?method=viewWorkflowConfig">
					<img src="images/new-icon/icon-14.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="NAICS Master" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/naicscode.do?method=viewNaicsCode"
					styleId="menuselect8">
					<img src="images/new-icon/icon-17.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>

	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Performance Assessment Template" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/reportingperiod.do?method=showPage">
					<img src="images/new-icon/tier2-reporting-period.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Performance Assessment Template" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/questions.do?method=showPage">
					<img src="images/new-icon/performance-assessment-template.png"
						alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<html:link action="/proactiveMonitoring.do?method=proactiveMonitoring">
		<img src="images/new-icon/proactive-monitoring-and-alerting.png"
			alt="" />
	</html:link>
	<html:link
		action="/retrivecustomer?method=retriveCustomerInfoFromCustomerDB">
		<img src="images/new-icon/edit-customer-info_hover.png" alt="" />
	</html:link>
	<html:link action="/dashboardsettings?method=viewDashboardSettings">
		<img src="images/new-icon/dashboard-settings.png" alt="" />
	</html:link>
	<html:link action="/commoditycategory?method=viewCommodityCategory">
		<img src="images/new-icon/commodity-category.png" alt="" />
	</html:link>
	<html:link action="/commoditycategory?method=viewCommodity">
		<img src="images/new-icon/commodity.png" alt="" />
	</html:link>
	<html:link
		action="/geographicalsettings?method=showGeographicalSettingsPage">
		<img src="images/new-icon/geographical-settings.png" alt="" />
	</html:link>
	<%-- <html:link
		action="/geographicalsettings?method=showGeographicalSettingsPage">
		<img src="images/new-icon/geographical-settings.png" alt="" />
	</html:link> --%>
</div> -->