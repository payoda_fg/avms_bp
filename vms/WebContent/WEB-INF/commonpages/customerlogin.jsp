<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Web Fonts  -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
	rel="stylesheet" type="text/css" />

<!-- Vendor CSS -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/animate.css" type="text/css">

<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
<!-- Porto Theme css -->
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />

<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/modals.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>

<script type="text/javascript">
	function DisplayPrivacyTerms() {
		$("#dialog1").css({
			"display" : "block"
		});

		$("#dialog1").dialog({
			width : 800,
			modal : true,
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}

	/**
	 *  Redirection for supplier registration page 
	 */
	function supplierRegistration(value) {
		if (value === "p") {
			window.location = "viewanonymousvendors.do?parameter=manageAnonymousVendors&prime=yes";
		} else {
			window.location = "viewanonymousvendors.do?parameter=manageAnonymousVendors";
		}
	}

	function sendEmail() {

		var emailId = $('#emailId').val();
		var securityQuestion = $('#securityQuestion').val();
		var securityAnswer = $('#securityAnswer').val();

		if (emailId != '' && securityQuestion != '' && securityAnswer != '') {

			$("#ajaxloader").css('display', 'block');
			$("#ajaxloader").show();

			$.ajax({
				url : "forgot.do?method=sendNewPassword&userEmailId=" + emailId
						+ "&securityQuestion=" + securityQuestion
						+ "&securityAnswer=" + securityAnswer + '&random='
						+ Math.random(),
				async : false,
				dataType : "json",
				success : function(data) {
					$('#result').show('fast');
					var result = data.msg;
					$('#result').html(result).css('color', 'red');
					$("#ajaxloader").hide();
					$('#failureMsg').hide('fast');
					$('#emailId').val('');
					$('#securityQuestion').val('');
					$('#securityAnswer').val('');
				}
			});
			return false;
		} else {
			$('#failureMsg').show('fast');

		}
		setTimeout(function() {
			$('#failureMsg').fadeOut('slow');
			$('#result').fadeOut('slow');
		}, 3000);
	}

	/*function popup() {

		$('#failureMsg').hide();
		$('#result').hide();
		$('#emailId').val('');
		$('#securityQuestion').val('');
		$('#securityAnswer').val('');

		//This Script is used for load pop up div to forgot password screen
		$("#dialog").css({
			"display" : "block"
		});
		$("#dialog").dialog({
			minWidth : 650,
			modal : true
		});
		$("#securityQuestion").select2({
			width : "89%"
		});
	}*/

	/*function showBenefits() {

		$("#benefits").css({
			'display' : 'block',
			'text-indent' : '1.5em',
			'font-size' : 'inherit'
		});
		/$("#benefits").dialog({
			width : 600,
			height : 300,
			modal : true
		});
	}*/

	/*jQuery.validator.addMethod("password",function(value, element) {
	return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");*/
	$(document).ready(function() {
		$("#loginForm").validate({
			rules : {
				userEmailId : {
					required : true,
					email : true
				},
				userPassword : {
					required : true,
				}
			},
		});
		$('.modal-benefits, .modal-with-form, .modal-with-zoom-anim').magnificPopup({
			type: 'inline',
			preloader: false,
			modal: true
		});
		
	});

	setTimeout(function() {
		$('#successMsg').fadeOut('slow');
		$('#failureMsg').fadeOut('slow');
	}, 5000);
</script>
<style>
<!--
#ajaxloader {
	position: absolute;
	display: none;
	z-index: 99999;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}
-->
</style>
<div id="forgotpwdmodal" class="modal-block modal-block-primary mfp-hide">
<section class="card">
	<header class="card-header">
		<h2 class="card-title">Forgot Password</h2>
	</header>	
	<div class="card-body">
	<form>
	<div id="failureMsg" style="display: none;">
		<p style="color: red;">EmailId, Secret Question and Answer are
			Mandatory</p>
	</div>
	<div id="result" style="display: none;"></div>
	<div id="ajaxloader" style="">
		<img src="images/ajax-loader1.gif" alt="Ajax Loading Image"
			class="ajax-loader" />
	</div>
	<div class="form-row">
		<div class="form-group col-md-12">
			<label for="emailId">Email ID</label>
			<input type="text" name="userEmailId" id="emailId" class="text-box form-control">
		</div>
	</div>	
	
		<div class="form-row">
			<div class="form-group col-md-12">
				<label for="securityQuestion">Select Security Question</label>
			
				<select name="securityQuestion" class="form-control" id="securityQuestion">
					<option value="">----Select----</option>
					<logic:present name="secretQuestionsList">
						<bean:size id="size" name="secretQuestionsList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="secretQn" name="secretQuestionsList">
								<bean:define id="id" name="secretQn" property="id"></bean:define>
								<option value="<%=id.toString()%>">
									<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
								</option>
							</logic:iterate>
						</logic:greaterEqual>
					</logic:present>
				</select>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-12">
				<label for="securityAnswer">Security Answer</label>			
				<input type="text" name="securityAnswer" id="securityAnswer"
					class="text-box form-control">
			</div>
		</div>
	
</form>
</div>
	<footer class="card-footer">
		<div class="row">
			<div class="col-md-12 text-right">
				<input type="button" value="Send" class="btn btn-primary" onclick="sendEmail();" />
				<button class="btn btn-default modal-dismiss">Cancel</button>
			</div>
		</div>
		</footer>		
	</div>
	</section>
</div>
<body id="home-bg">
	<section class="body-sign">
		<div class="center-sign">
			<div class="logo float-left">
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails"
						property="settings.logoPath">
					</bean:define>
					<img
						src="<%=request.getContextPath()%>/images/dynamic/?file=<%=logoPath%>"
						height="70px" />
					<h1 style="text-align: center; text-transform: uppercase;">BP
						Supplier Diversity</h1>
				</logic:present>
			</div>
			<!-- 		<div id="logo"> -->
			<!-- 			<img src="images/logo.png" height="80" style="margin-top: -2%;float: left;" /> -->
			<!-- 			<h1 style="color: #009900;text-align: center;text-transform: uppercase;">BP Supplier Diversity</h1> -->
			<!-- 		</div> -->
			<div class="panel card-sign">
				<div class="card-title-sign text-right">
					<h2 class="title text-uppercase font-weight-bold m-0">
						<i class="fa fa-user mr-1"></i> Sign In
					</h2>
				</div>
				<html:form action="/login.do?method=authenticate" styleClass="AVMS"
					styleId="loginForm">
					<div class="card-body">
						<!-- 		<div align="center"><p style="background-color: white; color: blue;width: 60%"><u><b>*ATTENTION*</b></u> -->
						<!-- Please note that this system will be unavailable for any updates to existing company profiles, new Tier 2 report submissions, and new registrations, until further notice.<br>   -->
						<!-- <u><b>Existing users:</b></u> You can still login as usual to view information in a read-only format.<br>  -->
						<!-- <u><b>New users:</b></u> If you are interested in registration for potential work with BP, please send an email to us for further consideration at:<br><b><a style="color: blue;" href="#">supplierdiversity@bp.com</a></b>.<br><br><br> -->

						<!-- We apologize for the inconvenience.<br><br> -->

						<!-- Thank you.<br><br>  -->

						<!-- BP Supplier Diversity</p></div> -->
						<div class="signin-page">
							<div id="successMsg">
								<html:messages id="msg" property="resetpassword" message="true">
									<span
										style="color: green; font-weight: bolder; font-size: medium;"><bean:write
											name="msg" /></span>
								</html:messages>
							</div>
							<div class="form-group mb-3">
								<label>Email ID</label>
								<div class="input-group input-group-icon">
									<html:text property="userEmailId" name="loginForm"
										styleClass="form-control form-control-lg"
										styleId="userEmailId"></html:text>
									<span class="input-group-addon"> <span
										class="icon icon-lg"> <i class="fa fa-user"></i>
									</span>
									</span>
								</div>
							</div>
							<div class="form-group mb-3">
								<label>Password</label>
								<div class="input-group input-group-icon">
									<html:password property="userPassword" name="loginForm"
										styleClass="form-control form-control-lg"
										styleId="userPassword"></html:password>
									<span class="input-group-addon"> <span
										class="icon icon-lg"> <i class="fa fa-lock"></i>
									</span>
									</span>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="checkbox-custom checkbox-default">
										<input name="rememberMe" id="RememberMe" type="checkbox"
											value="" /> <label for="RememberMe">Remember Me</label>
									</div>
								</div>
								<div class="col-sm-6 text-right">
									<div class="mt-1">
										<a href="#forgotpwdmodal" class="modal-with-form">Forgotten your
											Password?</a>
									</div>
								</div>
							</div>
							<div class="lable">
								<span style="color: red; font-style: italic;"><html:errors
										property="login"></html:errors> </span>
							</div>

							<div class="row">
								<div class="col-sm-12 text-right">
									<input type="submit" class="btn btn-primary mt-2" value="Login">
								</div>
							</div>
						</div>
						<span class="mt-3 mb-3 line-thru text-center text-uppercase">
								<span>or</span>
							</span>
						<!--  Sign Up -->
						<div id="sing-up-box">
													
							<div class="btn-box1 text-center">
								<span>
									<span>New User?</span> 
								</span>	
									<span><a href="#" class=""
										onclick="supplierRegistration('n')" id="abutton"> Sign Up!</a></span>
									<!-- <li><a href="#" class="btn push_btm_fix"
							onclick="supplierRegistration('p')" id="abutton"> Prime Supplier Registration</a></li> -->
								
							</div>
							<div class="btn-box">
								<a href="#modalBenefits" class="btn btn-facebook  mr-1 modal-benefits"> Benefits
									of Registering</a> 
									
							<div id="modalBenefits" class="modal-block modal-block-info mfp-hide">
								<section class="card">
									<div class="card-body">
										<div class="modal-wrapper">
										<div class="modal-icon">
											<i class="fa fa-info-circle"></i>
										</div>
										<div class="modal-text">
										<p>This site will allow you to provide BP with detailed
											information about your company's capabilities. Once submitted,
											your company information will be transferred into our Supplier
											Diversity database. This database is used as a sourcing tool by
											our U.S. Procurement & Supply Chain Management (PSCM) team to
											identify diverse companies that are capable of providing
											products and services necessary to meet our supply chain needs.</p>
										<p>Inclusion in this database does not guarantee a bid
											opportunity from BP. However, by becoming a part of this
											database, your company will be exposed to our PSCM team when
											sourcing activity occurs. Therefore it is important that you
											provide a concise but thorough description of your company
											products, services, and capabilities.</p>
											</div>
											</div>
										</div>
										<footer class="card-footer">
											<div class="row">
												<div class="col-md-12 text-right">
													<button class="btn btn-info modal-dismiss">OK</button>
												</div>
											</div>
										</footer>
								</section>
							</div>
									<a href="http://support.avmsystem.com/support/solutions/folders/1000030759"
									target="_blank" class="btn btn-twitter mr-1"> FAQs</a> <a
									href="http://support.avmsystem.com" target="_blank" class="btn btn-facebook">
									Need Help</a>
							
							</div>
						</div>
						<div class="get-help">
							Having problems? <a href="http://support.avmsystem.com"
								target="_blank">Get help</a> <br /> Notice! Please read prior to
							logging in: <a href="#privacy" class="mb-1 mt-1 mr-1 modal-with-zoom-anim ws-normal">Privacy
								and Terms</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</html:form>
			</div>
		</div>
		<!-- Dialog Box for Privacy and Terms -->
		<div id="privacy" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide" title="Privacy and Terms"
			>
			<logic:present name="userDetails" property="settings.privacyTerms">
				<bean:define id="privacyTerms" name="userDetails"
					property="settings.privacyTerms" />
				<section class="card">
				<header class="card-header">
					<h2 class="card-title">Privacy and Terms</h2>
				</header>
				<div class="card-body">
				<div class="modal-wrapper">
				<div class="modal-text"><p>
					<bean:write name="privacyTerms" />
				</p></div>
				</div>
				<footer class="card-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<button class="btn btn-info modal-dismiss">Ok</button>
						</div>
					</div>
				</footer>
				</div>
				</section>
			</logic:present>
			<logic:notPresent name="userDetails" property="settings.privacyTerms">
				<p>There is no Privacy and Terms.</p>
			</logic:notPresent>
		</div>		
	</section>
</body>