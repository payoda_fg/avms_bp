<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<div id="right-part">
	<div id="banner-right">
		<img alt="Banner" src="images/image-banner.jpg">
	</div>
	<div class="clear"></div>
	<div class="login-tabs-bot">
		<div class="tab1">
			<a href="#">Benefits of Registering</a>
		</div>
		<div class="tab2">
			<a href="#">FAQ's</a>
		</div>
		<div class="tab3">
			<a href="#">Need Help?</a>
		</div>
	</div>
</div>
<div class="clear"></div>
