<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>

<!--Menupart -->

<section class="body">
	<header class="page-header">
		<h2>Vendors</h2>
	</header>
		<div class="inner-wrapper">
			<aside id="sidebar-left" class="sidebar-left">			
			    <div class="sidebar-header">
			         <div class="sidebar-title">Navigation</div>
			        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
			        </div>
			    </div>
		    <div class="nano ">
		    <div class="nano-content">
		    	<nav id="menu" class="nav-main" role="navigation">
		    		<ul class="nav nav-main">
		    			<li class="nav-parent">
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Create Vendor" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu1" class="icon-1 nav-link" id="menuselect0"
										onclick="linkPage('viewVendor.do?method=primeVendor')">
										<i class="icon-img"><img
											src="bpimages/vendor.png" /> </i> 											
										<span class="icon-text">Create Vendor</span> 
									</a>
			
								</logic:equal>
							</logic:equal>
						</logic:iterate>
						</li>
						<li class="nav-parent">
							<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Supplier Diversity Request Form" name="privilege"
					property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
					
						<c:choose>
						<c:when test="${isDisplaySDF == '1'}" >
						<a href="#" class="nav-link" id="menuselect2"
							onclick="linkPage('supplierDiversity.do?method=supplierDivercityForm')"> <i
							class="icon-img"><img
								src="bpimages/prime-vendor-search.png" /> </i><span
							class="icon-text">Supplier Diversity Request Form</span> </a>
								</c:when>
							</c:choose>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
						</li>						
						
						<li class="nav-parent">
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="View Vendors" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#" class="nav-link" id="menuselect2"
										onclick="linkPage('viewVendorsStatus.do?method=showVendorSearch&searchType=V')">
										
										 <i
										class="icon-img"><img src="bpimages/search_vendor.png" />
									</i> <span
										class="icon-text">Search Vendor All Statuses</span></a>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
						</li>

			
			<!-- <div class="togglecontainer3 submenu" id="menu3"> -->
			<li class="nav-parent">
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Search Vendor" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<a href="#" class="nav-link" id="menuselect2"
								onclick="linkPage('viewVendors.do?method=showVendorSearch&searchType=C')">
								<!-- <span class="icon-text active">Vendor Profile Criteria</span> -->
								<i class="icon-img"><img
									src="bpimages/search-for-a-vendor.png" /></i>
								<span class="icon-text">Search for a Vendor</span>
							</a>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
			</li>
			<li class="nav-parent">
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Keyword Search" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<a href="#" class="nav-link" id="menuselect2"
								onclick="linkPage('viewVendors.do?method=showVendorFullSearch')">
								<i class="icon-img"><img src="bpimages/keyword-search.png" /></i>
							<span class="icon-text">Keyword Search</span> 
							</a>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
			</li>
			<!-- </div> -->
			<li class="nav-parent">
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Prime Vendor Search" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<a href="#" class="" id="menuselect5"
									onclick="linkPage('viewVendors.do?method=showPrimeVendorSearch&searchType=P')">
									
									<i class="icon-img"><img
										src="bpimages/prime-vendor-search.png" /> </i>
									<span class="icon-text">Prime Vendor Search</span> 
								 </a>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
				</li>
				<li class="nav-parent">
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Inactive Vendor Search" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<a href="#" class=""
									onclick="linkPage('viewVendors.do?method=showVendorStatusBySearch')">
									 <i class="icon-img"><img
										src="bpimages/inactive-vendor-search.png" /> </i> 
									<span class="icon-text">Inactive Vendor Search</span>
								</a>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
				</li>
				<li class="nav-parent">
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Assessment Review" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<a href="#menu3" class="toggletrigger4 submenuheader"
									id="menuselect3"> <i class="icon-img"><img
										src="bpimages/review_assessment.png" /> </i><i class="icon-img pl-1"><img
										src="bpimages/sub-plus.png" /> </i><span class="icon-text">Review
										Assessments</span> </a>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
				</li>
				
							
				<li>
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Assessment Review" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<html:link action="/assessmentreview.do?method=reviewTemplate"
									styleClass="btn-gray sub-icon-5 sub">										
									<i class="icon-img"><img
										src="bpimages/capability_assessment.png" /> </i>
										<span class="icon-text active">Capability Assessment</span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
					</li>
					<li>
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Assessment Score Card" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<html:link
										action="/assessmentscore.do?method=viewscore&getTemplateForScore=0"
										styleClass="btn-gray">
										
										<i class="icon-img"><img
											src="bpimages/assessmentscorecard.png" /> </i>
											<span class="icon-text active">Assessment Score card</span>
									</html:link>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
					</li>
					<li>
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Assessment Email Details" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<html:link
										action="/assessmentemaildetails.do?method=viewemaildetails&getTemplate=0"
										styleClass="btn-gray">
										
										<i class="icon-img"><img
											src="bpimages/assessmentemail.png" /> </i>
											<span class="icon-text active">View Assessment Emails</span>
									</html:link>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
					</li>
					
				<li class="nav-parent">
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Inactive Vendor Search" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<a href="#" class="" id="menuselect2"
									onclick="linkPage('viewVendors.do?method=viewTier2VendorSpendReport')">
									 <i
									class="icon-img"><img
										src="bpimages/prime-vendor-search.png" /> </i>
									<span
									class="icon-text">Search Tier2 Vendors Report</span>
									 </a>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
				</li>
				<li>
						<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Mail Notifications" name="privilege"
					property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a href="#menu2" class="toggletrigger2 submenuheader nav-link"
							id="menuselect1">
							<i class="icon-img"><img src="bpimages/email.png" /> </i><i
							class="icon-img"><img src="bpimages/sub-plus.png" /> </i> 
							<span class="icon-text">Email Vendor</span>		
					</a>

					</logic:equal>
				</logic:equal>
			</logic:iterate>
			<div class="togglecontainer2 submenu" id="menu2">
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Mail Notifications" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link action="/searchvendor.do?method=searchVendor"
								styleClass="btn-gray sub-icon-1 sub ">								
								<i class="icon-img"><img src="bpimages/general.png" />
								</i>
								<span class="icon-text active">General</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="Assessment Email Notification" name="privilege"
						property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<html:link
								action="/capabalityAssessmentSearchvendor.do?method=searchAssessmentVendor"
								styleClass="btn-gray sub-icon-2 sub">
								<i class="icon-img"><img src="bpimages/assessment.png" />
								</i>								
								<span class="icon-text active">Assessment</span>
							</html:link>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
			</div>
				</li>
				</ul>
		            </nav>
				
	            <hr class="separator" />
				</div>
				</div>
			</aside>
		</div>
		<div class="clear"></div>
</section>
	<div class="clear"></div>
	<script type="text/javascript">
		var pathname = window.location.href;
		$(".submenuheader").click(function() {
			var $curr = $(this);
			$("a").removeClass("active");
			$("div").removeClass("active");
			$(this).addClass("active");
			$curr.parent().prev().addClass("active");
		});

		$(".sub").click(function() {
			$(".sub").removeClass("active");
			$(this).addClass("active");
		});

		/* $('.btn-bg').each(function() {
			var LiN = $(this).find('a').length;
			if (LiN > 8) {
				$('a', this).eq(7).nextAll().hide().addClass('toggleable');
			}
		}); */

		/* if (pathname.indexOf("showVendorSearch") >= 0) {
			$('.icon-3').addClass("active");
		} else if (pathname.indexOf("showMailNotificationPage") >= 0) {
			$('.sub-icon-3').addClass("active");
		} else if (pathname.indexOf("viewVendor") >= 0) {
			$('.icon-1').addClass("active");
		} else if (pathname.indexOf("searchvendor") >= 0) {
			$('.sub-icon-1').addClass("active");
		} else if (pathname.indexOf("capabalityAssessmentSearchvendor") >= 0) {
			$('.sub-icon-2').addClass("active");
		} else if (pathname.indexOf("listanonymousvendors") >= 0) {
			$('.sub-icon-4').addClass("active");
		} else if (pathname.indexOf("assessmentreview") >= 0) {
			$('.sub-icon-5').addClass("active");
		} else if (pathname.indexOf("assessmentscore") >= 0) {
			$('.sub-icon-6').addClass("active");
		} else if (pathname.indexOf("assessmentemaildetails") >= 0) {
			$('.sub-icon-7').addClass("active");
		} */
	</script>

