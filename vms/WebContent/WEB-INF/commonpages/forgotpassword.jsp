<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script src="jquery/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#forgetForm").validate({
			rules : {
				userEmailId : {
					required : true,
					email : true
				},
				securityQuestion : {
					required : true
				},
				securityAnswer : {
					required : true
				}
			}
		});
	
	});

	function backToLoginPage(){
		window.location = "home.do?method=loginPage";
	}
	
//-->
</script>
<style>
<!--
.main-list-box {
	background: #FFFFFF;
	border: 0 none;
	border-radius: 5px 5px 5px 5px;
	border: 1px solid #ccc;
	color: #000000 !important;
	height: 25px;
	padding: 1% 2%;
	width: 93%;
	float: left;
	line-height: 25px;
}
-->
</style>
<div id="Login-Container-boder">
	<h2>Forgot Password</h2>
	<div id="Login-Container-form">
		<html:messages id="msg" property="forgot" message="true">
			<span style="color: green; font-size: 13px;"><bean:write
					name="msg" /></span>
		</html:messages>
		<p>
			<span style="color: red; font-style: italic;"><html:errors
					property="forgot"></html:errors> </span>
		</p>
		<html:form action="/forgot?method=sendNewPassword" styleClass="AVMS" styleId="forgetForm">
			<html:javascript formName="loginForm" />
			<div class="login-box">
				<ul>
					<li style="height: 75px;">
						<div>
							<label class="desc">Login ID</label>
							<div>
								<html:text property="userLogin" name="loginForm" styleId="userLogin">
								</html:text>

							</div>
						</div>
					</li>
					<li style="height: 75px;">
						<div>
							<label class="desc">Email Id</label>
							<div>
								<html:text property="userEmailId" name="loginForm"
									styleId="userEmailId"
									onblur="return email_validate(this.value)">

								</html:text>
							</div>
						</div>
					</li>
					<li style="height: 75px;"><div>
							<label class="desc">Security Question</label>
							<div>
								<select name="securityQuestion" id="securityQuestion"
									tabindex="30" style="width: 100%;" class="main-list-box">
									<option value="">----Select----</option>
									<logic:present name="secretQuestionsList">
										<bean:size id="size" name="secretQuestionsList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="secretQn" name="secretQuestionsList">
												<bean:define id="id" name="secretQn" property="id"></bean:define>
												<option value="<%=id.toString()%>">
													<bean:write name="secretQn"
														property="secQuestionParticular"></bean:write>
												</option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</select>
							</div>
						</div></li>
					<li style="height: 75px;"><div>
							<label class="desc">Answer</label>
							<div>
								<input type="text" name="securityAnswer" id="securityAnswer">
							</div>
						</div></li>

					<li style="text-align: center; padding-top: 15px;"><html:submit
							value=" " styleClass="send-btn"></html:submit>
							<input type="button" value=" " onclick="backToLoginPage();" class="login-btn">
							</li>
				</ul>
				<div class="clear"></div>
			</div>
			<%-- <logic:present name="userType">
				<logic:notEqual value="fg" name="userType">
					<div class="register-here">
						<p class="box">New Vendors</p>
						<br>
						<li style="text-align: center"><input class="reg-btn"
							type="button" name="signin" onclick="supplierRegistration()"></li>
					</div>
				</logic:notEqual>
			</logic:present> --%>
		</html:form>
	</div>
</div>