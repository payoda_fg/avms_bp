<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				e.preventDefault();
				$('#submit').click();
			}
		}
	});
		
	function copyAddress() {
		var address1 = $("#address1").val();
		var province = $('#province').val();
		var city = $('#city').val();
		var country = $('#country').val();
		var state = $('#state').val();
		var region = $('#region').val();
		var zip = $('#zipcode').val();
		var mobile = $('#mobile').val();
		var phone = $('#phone').val();
		var fax = $('#fax').val();
		if ($("#same").is(':checked')) {
			$("#address2").val(address1);
			$('#province2').val(province);
			$('#city2').val(city);
			$('#country2').val(country);
			$("#state2").find('option').remove().end().append(
					$('#state').html());
			$('#state2').val(state);
			$('#region2').val(region);
			$('#zipcode2').val(zip);
			$('#mobile2').val(mobile);
			$('#phone2').val(phone);
			$('#fax2').val(fax);
			
			$("#address2").prop('readonly', true);
			$('#province2').prop('readonly', true);
			$('#city2').prop('readonly', true);
			$('#country2').prop('readonly', true);
			$('#state2').prop('readonly', true);
			$('#region2').prop('readonly', true);
			$('#zipcode2').prop('readonly', true);
			$('#mobile2').prop('readonly', true);
			$('#phone2').prop('readonly', true);
			$('#fax2').prop('readonly', true);
			
		} else {
			$("#address2").val('');
			$('#province2').val('');
			$('#city2').val('');
			$('#country2').val('');
			$('#state2').val('');
			$('#region2').val('');
			$('#zipcode2').val('');
			$('#mobile2').val('');
			$('#phone2').val('');
			$('#fax2').val('');
			
			$("#address2").prop('readonly', false);
			$('#province2').prop('readonly', false);
			$('#city2').prop('readonly', false);
			$('#country2').prop('readonly', false);
			$('#state2').prop('readonly', false);
			$('#region2').prop('readonly', false);
			$('#zipcode2').prop('readonly', false);
			$('#mobile2').prop('readonly', false);
			$('#phone2').prop('readonly', false);
			$('#fax2').prop('readonly', false);
		}
		$("#country2").select2();
		$("#state2").select2();
	}
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != '') {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2();
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
			$('#' + id).select2();
		}
	}
	function vendorInformationPage(){
		window.location = "selfvendornavigation.do?parameter=companyInfoNavigation";
	}
</script>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<html:form
	action="selfregaddressinfo.do?parameter=saveAddressInformation"
	method="post" styleId="addressForm" styleClass="FORM">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
				<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
					<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
					<input type="button" class="btn" value="Back" 
					 				onclick="vendorInformationPage();"> 
					<input type="submit" class="btn" value="Save" id="submit" name="submit">
					<input type="reset" class="btn" value="Reset" id="reset">
				</div>
				<div id="step-1" class="content">
					<h2 class="StepTitle">Address Information</h2>
					<div class="panelCenter_1" style="padding-top: 2%;">
						<h3>Physical Address</h3>
						<div class="form-box">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Address</div>
									<div class="ctrl-col-wrapper">
										<html:textarea styleClass="main-text-area" property="address1"
											alt="" styleId="address1" tabindex="38" />
									</div>
									<span class="error"> <html:errors property="address1"></html:errors>
									</span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">City</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="city" alt=""
											styleId="city" tabindex="39" />
									</div>
									<span class="error"> <html:errors property="city"></html:errors>
									</span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Country</div>
									<div class="ctrl-col-wrapper">
										<logic:present name="userDetails" property="workflowConfiguration">
											<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
												<html:select property="country" styleId="country" styleClass="chosen-select-width" tabindex="40"
													onchange="changestate(this,'state');">
													<html:option value="">--Select--</html:option>
													<logic:iterate id="country" name="countryList">
														<bean:define id="name" name="country" property="id"/>
														<html:option value="<%=name.toString()%>">
															<bean:write name="country" property="countryname" />
														</html:option>
													</logic:iterate>
												</html:select>
												<span class="error"> <html:errors property="country"></html:errors></span>
											</logic:equal>
											<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
												<input type="text" class="text-box" id="countryValue" readonly="readonly">
												<html:select property="country" styleId="country" styleClass="chosen-select-width" tabindex="40"
													onchange="changestate(this,'state');" style="display:none;">
													<logic:iterate id="country" name="countryList">
														<bean:define id="name" name="country" property="id"/>
														<html:option value="<%=name.toString()%>">
															<bean:write name="country" property="countryname" />
														</html:option>
													</logic:iterate>
												</html:select>
											</logic:equal>
										</logic:present>						
									</div>									
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">State</div>
									<div class="ctrl-col-wrapper">
										<html:select property="state" styleId="state"
											styleClass="chosen-select-width" tabindex="41">
											<html:option value="">--Select--</html:option>
											<logic:present name="editVendorMasterForm"
												property="phyStates1">
												<logic:iterate id="states" name="editVendorMasterForm"
													property="phyStates1">
													<bean:define id="name" name="states" property="id"></bean:define>
													<html:option value="<%=name.toString()%>">
														<bean:write name="states" property="statename" />
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select>
									</div>
									<span class="error"> <html:errors property="state"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">County/Province</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="province"
											alt="Optional" styleId="province" tabindex="42" />
									</div>
									<span class="error"> <html:errors property="province"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Region</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="region"
											alt="Optional" styleId="region" tabindex="43" />
									</div>
									<span class="error"> <html:errors property="region"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Zip Code</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="zipcode" alt=""
											styleId="zipcode" tabindex="44" />
									</div>
									<span class="error"> <html:errors property="zipcode"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Mobile</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="mobile" alt="Optional"
											styleId="mobile" tabindex="45" />
									</div>
									<span class="error"> <html:errors property="mobile"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Phone</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="phone" alt=""
											styleId="phone" tabindex="46" />
									</div>
									<span class="error"> <html:errors property="phone"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Fax</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="fax" alt=""
											styleId="fax" tabindex="47" />
									</div>
									<span class="error"> <html:errors property="fax"></html:errors></span>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
					<div class="panelCenter_1">
						<h3>Mailing Address</h3>
						<div class="form-box">
							&nbsp;&nbsp;Same as physical Address : <input type="checkbox"
								id="same" onclick="copyAddress();" style="margin-top: 1%;">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Address</div>
									<div class="ctrl-col-wrapper">
										<html:textarea styleClass="main-text-area" property="address2"
											alt="" styleId="address2" tabindex="48" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">City</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="city2" alt=""
											styleId="city2" tabindex="49" />
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Country</div>
									<div class="ctrl-col-wrapper">
										<logic:present name="userDetails" property="workflowConfiguration">
											<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
												<html:select property="country2" styleId="country2" styleClass="chosen-select-width" tabindex="50"
													onchange="changestate(this,'state2');">
													<html:option value="">--Select--</html:option>
													<bean:size id="size" name="countryList" />
													<logic:greaterEqual value="0" name="size">
														<logic:iterate id="country" name="countryList">
															<bean:define id="name" name="country" property="id"/>
															<html:option value="<%=name.toString()%>">
																<bean:write name="country" property="countryname" />
															</html:option>
														</logic:iterate>
													</logic:greaterEqual>
												</html:select>
											</logic:equal>
											<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
												<html:select property="country2" styleId="country2" styleClass="chosen-select-width" tabindex="50"
													onchange="changestate(this,'state2');" disabled="true">
													<bean:size id="size" name="countryList" />
													<logic:greaterEqual value="0" name="size">
														<logic:iterate id="country" name="countryList">
															<bean:define id="name" name="country" property="id"/>
															<html:option value="<%=name.toString()%>">
																<bean:write name="country" property="countryname" />
															</html:option>
														</logic:iterate>
													</logic:greaterEqual>
												</html:select>
											</logic:equal>
										</logic:present>							
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">State</div>
									<div class="ctrl-col-wrapper">
										<html:select property="state2" styleId="state2"
											styleClass="chosen-select-width" tabindex="51">
											<html:option value="">--Select--</html:option>
											<logic:present name="editVendorMasterForm"
												property="mailingStates2">
												<logic:iterate id="states" name="editVendorMasterForm"
													property="mailingStates2">
													<bean:define id="name" name="states" property="id"></bean:define>
													<html:option value="<%=name.toString()%>">
														<bean:write name="states" property="statename" />
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select>
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">County/Province</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="province2"
											alt="Optional" styleId="province2" tabindex="52" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Region</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="region2"
											alt="Optional" styleId="region2" tabindex="53" />
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Zip Code</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="zipcode2"
											alt="Optional" styleId="zipcode2" tabindex="54" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Mobile</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="mobile2" alt="Optional"
											styleId="mobile2" tabindex="55" />
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Phone</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="phone2" alt=""
											styleId="phone2" tabindex="56" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Fax</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="fax2" alt=""
											styleId="fax2" tabindex="57" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2"> <input
						type="submit" class="btn" value="Next" id="submit3" name="submit3"> <input
						type="button" class="btn" value="Back" onclick="vendorInformationPage();"> <input
						type="submit" class="btn" value="Save" id="submit2" name="submit2">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
</html:form>
</div>
</div>
</section>
<script type="text/javascript">
$(document).ready(function() {
	$("#menu3").removeClass().addClass("current");
	
	if($("#province").val() === 'Optional'){
		$("#province").val('');
	}
	if($("#region").val() === 'Optional'){
		$("#region").val('');
	}
	if($("#province2").val() === 'Optional'){
		$("#province2").val('');
	}
	if($("#region2").val() === 'Optional'){
		$("#region2").val('');
	}
	if($("#zipcode2").val() === 'Optional'){
		$("#zipcode2").val('');
	}
	
	$("#countryValue").val($("#country option:selected").text());
});
	$(function($) {
		$("#phone").mask("(999) 999-9999?");
		$("#phone2").mask("(999) 999-9999?");
		$("#fax").mask("(999) 999-9999?");
		$("#fax2").mask("(999) 999-9999?");
		$("#mobile").mask("(999) 999-9999?");
		$("#mobile2").mask("(999) 999-9999?");
	});
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
	}, "No Special Characters Allowed.");
	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	jQuery.validator.addMethod("allowhyphens", function(value, element) {
		return this.optional(element) || /^[0-9-]+$/.test(value);
	}, "Please enter valid numbers.");
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		$("#addressForm").validate({
				rules : {
					address1 : {maxlength : 120},
					city : {alpha : true,maxlength : 60},
					province : {maxlength : 60},
					region : {maxlength : 60},ethnicity : {range : [ 1, 1000 ]},
					country : {maxlength : 120},
					zipcode : {allowhyphens : true},
					phone : {maxlength : 16},
					zipcode1 : {allowhyphens : true, maxlength : 255},
					zipcode2 : {allowhyphens : true, maxlength : 255},
					address2 : {maxlength : 120},
					city2 : {alpha : true,maxlength : 60},
					province2 : {maxlength : 60},
					region2 : {maxlength : 60}					
				},
				submitHandler : function(form) {
					if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
						$('#addressForm').append("<input type='hidden' name='submitType' value='submit' />");
						$("#ajaxloader").css('display','block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
						alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
						$('#addressForm').append(
										"<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#saveandexit1').data('clicked')
							|| $('#saveandexit2').data('clicked')){
						$('#addressForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}
				},
				invalidHandler : function(form,	validator) {
					$("#ajaxloader").css('display','none');
					alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
					$('#submit1').data('clicked', false);
					$('#submit').data('clicked', false);
					$('#submit2').data('clicked', false);
					$('#submit3').data('clicked', false);
					$('#saveandexit1').data('clicked', false);
					$('#saveandexit2').data('clicked', false);
				}
		  });
	});
</script>