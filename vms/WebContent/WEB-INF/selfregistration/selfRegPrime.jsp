<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@page import="java.util.*"%>

<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<script>
	$(document).ready(function() {
		
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$.datepicker.setDefaults({
			changeYear : true,
			dateFormat : 'mm/dd/yy'
		});
		
		$("#contactDate").datepicker();
		
		$("#countryValue").val($("#country option:selected").text());
		
		<logic:present name="showPrivacyTerms">			
			DisplayPrivacyTerms();
		</logic:present>
	});
	
	function DisplayPrivacyTerms()
	{
		$("#PrivacyAndTerms").css(
		{
			"display" : "block"
		});		
		
		$("#PrivacyAndTerms").dialog(
		{
			width : 800,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
	}
	
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != '') {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2({
						width : "90%"
					});
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
			$('#' + id).select2({
				width : "90%"
			});
		}
	}
	function resetVendorForm() {
		window.location.reload(true);
	}
</script>
<div id="header_title">
	<h2 style="text-align: center;">Supplier Registration</h2>
</div>
<div class="clear"></div>
<div id="successMsg">
	<html:messages id="msg" property="vendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="transactionFailure" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<html:form method="post" styleClass="FORM" styleId="vendorForm"
	action="/selfprimevendor?parameter=createPrimeVendor">
	<html:javascript formName="vendorMasterForm" />
	<html:hidden property="id" styleId="vendorId" />
	<div class="panelCenter_1">
		<h3>Company</h3>
		<div class="form-box">
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<bean:message key="comp.name" />
					</div>
					<div class="ctrl-col-wrapper">
						<html:text property="vendorName" alt="" styleId="vendorName"
							styleClass="text-box" />
					</div>
					<span class="error"> <html:errors property="vendorName"></html:errors>
					</span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Email Address</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="emailId"
							styleId="emailId" alt="" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Address</div>
					<div class="ctrl-col-wrapper">
						<html:textarea styleClass="main-text-area" property="address1"
							alt="" styleId="address1" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">City</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="city" alt=""
							styleId="city" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Country</div>
					<div class="ctrl-col-wrapper">
						<logic:present name="userDetails" property="workflowConfiguration">
							<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
								<html:select property="country" styleId="country" styleClass="chosen-select" 
									style="width:90%;" onchange="changestate(this,'state');">
									<html:option value="">--Select--</html:option>
									<logic:present name="countryList">
										<html:optionsCollection name="countryList" value="id" label="countryname" />
									</logic:present>
								</html:select>
								<span class="error"> <html:errors property="country"></html:errors></span>
							</logic:equal>
							<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
								<input type="text" class="text-box" id="countryValue" readonly="readonly">
								<html:select property="country" styleId="country" styleClass="chosen-select" 
									style="width:90%; display:none;" onchange="changestate(this,'state');">
									<logic:present name="countryList">
										<html:optionsCollection name="countryList" value="id" label="countryname" />
									</logic:present>
								</html:select>
							</logic:equal>
						</logic:present>				
					</div>					
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">State</div>
					<div class="ctrl-col-wrapper">
						<html:select property="state" styleId="state"
							styleClass="chosen-select" style="width:90%;">
							<html:option value="">--Select--</html:option>
							<logic:present name="statesList">
								<html:optionsCollection name="statesList" value="id" label="statename" />
							</logic:present>
						</html:select>
					</div>
					<span class="error"> <html:errors property="state"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Zip Code</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="zipcode" alt=""
							styleId="zipcode" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Phone Number</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="phone" alt=""
							styleId="phone" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Fax Number</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="fax" alt=""
							styleId="fax" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper"></div>
					<div class="ctrl-col-wrapper"></div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="firstName" alt=""
							styleId="firstName" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Title</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="designation"
							styleId="designation" alt="" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact Phone</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="contactPhone"
							styleId="contactPhone" alt="" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact Email Address</div>
					<div class="ctrl-col-wrapper">
						<logic:present name="primeEmailId">
						<bean:define id="email" name="primeEmailId"></bean:define>
								<html:text styleClass="text-box" property="contanctEmail"
							styleId="contanctEmail" alt="" readonly="true"
							value="${ email }"/>
						</logic:present>
						<span class="error"><html:errors
							property="contanctEmail"></html:errors></span>
					</div>
				</div>			
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Gender</div>
						<div class="ctrl-col-wrapper">
							<html:radio property="gender" value="M">Male</html:radio>
							<html:radio property="gender" value="F">Female</html:radio>
							<html:radio property="gender" value="T">Transgender</html:radio>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="panelCenter_1" style="margin-top: 1%;">
		<h3>BP Contact Information</h3>
			<div class="form-box">
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Contact First Name</div>
						<div class="ctrl-col-wrapper">
							<html:text property="contactFirstName" alt=""
								styleId="contactFirstName" styleClass="text-box" />
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Contact Last Name</div>
						<div class="ctrl-col-wrapper">
							<html:text property="contactLastName" alt=""
								styleId="contactLastName" styleClass="text-box" />
						</div>
					</div>
				</div>
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Contact Date</div>
						<div class="ctrl-col-wrapper">
							<html:text property="contactDate" alt="Please click to select date"
								styleId="contactDate" styleClass="text-box" readonly="true" />
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Contact State</div>
						<div class="ctrl-col-wrapper">
							<html:select property="contactState" styleId="contactState"
								styleClass="chosen-select" style="width:90%;">
								<html:option value="">--Select--</html:option>
								<logic:present name="contactStates">
									<logic:iterate id="states" name="contactStates">
										<bean:define id="name" name="states" property="id"/>
										<html:option value="${name }">
											<bean:write name="states" property="statename" />
										</html:option>
									</logic:iterate>
								</logic:present>
							</html:select>
						</div>
					</div>
				</div>
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Contact Event</div>
						<div class="ctrl-col-wrapper">
							<html:textarea property="contactEvent" alt=""
								styleId="contactEvent" styleClass="main-text-area" />
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="clear"></div>
	<div class="btn-wrapper">
		<html:submit value="Submit" styleClass="btn" styleId="submit1"></html:submit>
		<html:reset value="Clear" styleClass="btn"
			onclick="resetVendorForm();"></html:reset>
		<%-- <html:reset value="Exit" styleClass="btn"
			onclick="history.go(-1);" ></html:reset> --%>
		<html:link action="/logout.do" styleClass="btn">Exit</html:link>
	</div>
</html:form>

<!-- Dialog Box for Privacy and Terms -->
<div id="PrivacyAndTerms" title="Privacy and Terms" style="display: none; max-height: 300px;">
	<logic:present name="userDetails" property="settings.privacyTerms">
		<bean:define id="privacyTerms" name="userDetails" property="settings.privacyTerms"/>
		<p><bean:write name="privacyTerms"/></p>
	</logic:present>
	<logic:notPresent name="userDetails" property="settings.privacyTerms">
		<p>There is no Privacy and Terms.</p>
	</logic:notPresent>
</div>
			
<script type="text/javascript">
	$(document).ready(function() {
		$("#phone").mask("(999) 999-9999?");
		$("#contactPhone").mask("(999) 999-9999?");
		$("#fax").mask("(999) 999-9999?");
	});
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
	}, "No Special Characters Allowed.");
	
	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	
	jQuery.validator.addMethod("phoneNumber", function(value, element) {
		return this.optional(element) || /^[0-9-()]+$/.test(value);
	}, "Please enter valid phone number.");
	
	jQuery.validator.addMethod("password",function(value, element) {
		return this.optional(element)
		|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	},"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	
	jQuery.validator.addMethod("allowhyphens", function(value, element) {
		return this.optional(element) || /^[0-9-]+$/.test(value);
	}, "Please enter valid numbers.");
	
	$("#vendorForm").validate({
		rules : {
			vendorName : {
				required : true,
				maxlength : 255
			},
			address1 : {
				required : true,
				maxlength : 120
			},
			city : {
				required : true,
				alpha : true,
				maxlength : 60
			},
			country : {
				required : true
			},
			state : {
				required : function(element) {if($("#country option:selected").text()=='United States') {return true;}else{return false;}},
				alpha : true,
				maxlength : 60
			},
			zipcode : {
				required : true,
				allowhyphens : true,
				maxlength : 255
			},
			phone : {
				required : true
			},
			emailId : {
				required : true,
				maxlength : 120,
				email : true
			},
			firstName : {
				required : true,
				maxlength : 255,
				alpha : true,
			},
			designation : {
				required : true,
				maxlength : 255
			},
			contactPhone : {
				required : true
			},
			contanctEmail : {
				required : true,
				email : true,
				maxlength : 120
			},
			contactFirstName:{
				maxlength : 255,
				alpha : true				
			},
			contactLastName:{
				maxlength : 255,
				alpha : true
			}
			
		},
	});
</script>