<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.placeholder.js"></script>

<script type="text/javascript">
jQuery.validator.addMethod("password",function(value, element) {
	return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

	$(document).ready(function() {
		$("#resetForm").validate({
			rules : {
				userEmailId : {
					required : true,
					email : true
				},
				securityQuestion : {
					required : true
				},
				securityAnswer : {
					required : true
				},
				oldPassword : {
					required : true
				},
				newPassword : {
					required : true,
					password : true,
				},
				confirmPassword : {
					required : true,
					equalTo : "#newPassword"
				},
			}
		});

	});

	function backToLoginPage() {
		window.location = "viewanonymousvendors.do?parameter=manageAnonymousVendors";
	}

</script>
<style>

.main-list-box {
	background: #FFFFFF;
	border: 0 none;
	border-radius: 5px 5px 5px 5px;
	border: 1px solid #ccc;
	color: #000000 !important;
	height: 25px;
	padding: 1% 2%;
	width: 93%;
	float: left;
	line-height: 25px;
}

</style>
<div id="Login-Container-boder">
	<h2>Reset Password</h2>
	<div id="Login-Container-form">
		<html:messages id="msg" property="forgot" message="true">
			<span style="color: green; font-size: 13px;"><bean:write
					name="msg" /></span>
		</html:messages>
		<p>
			<span style="color: red; font-style: italic;"><html:errors
					property="forgot"></html:errors> </span>
		</p>
		<p>You will be prompted to change your password every 90 days and cannot have been used for your last 12 passwords.</p>
		<html:form action="/resetselfpassword?method=resetSelfVendorPassword" styleId="resetForm">
			
			<div id="result" style="display: none;"></div>
			<div id="ajaxloader" style="">
				<img src="images/ajax-loader1.gif" alt="Ajax Loading Image"
					class="ajax-loader" />
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<label class="lable">Email ID</label>
					</div>
					<div class="ctrl-col-wrapper">
						<logic:present name="resetEmail">
							<bean:define id="email" name="resetEmail"></bean:define>
							<html:text property="userEmailId" styleId="userEmailId" readonly="true"
							styleClass="text-box" value="${email}"></html:text>
						</logic:present>
					</div>
					<span class="error"><html:errors property="userEmailId"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper"></div>
					<div class="ctrl-col-wrapper"></div>
				</div>
			</div>
			<div class="ctrl-col-wrapper">
						<logic:present name="divisionId">
							<bean:define id="division" name="divisionId"></bean:define>
							<html:hidden property="customerDivisionId" styleId="customerEmailId" 
							styleClass="text-box" value="${division}"></html:hidden>
						</logic:present>
					</div>
			
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<label class="lable">Select Security Question</label>
					</div>
					<div class="ctrl-col-wrapper">
						<html:select property="securityQuestion" styleId="securityQuestion"
							styleClass="chosen-select-width">
							<option value="">Select</option>
							<logic:present name="secretQuestionsList">
								<bean:size id="size" name="secretQuestionsList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="secretQn" name="secretQuestionsList">
										<bean:define id="id" name="secretQn" property="id"></bean:define>
										<option value="<%=id.toString()%>">
											<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
										</option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</html:select>
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper"></div>
					<div class="ctrl-col-wrapper"></div>
				</div>
			</div>

			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<label class="lable">Security Answer</label>
					</div>
					<div class="ctrl-col-wrapper">
						<html:text property="securityAnswer" styleId="securityAnswer"
							styleClass="text-box"></html:text>
					</div>
				</div>
			</div>
			<div class="wrapper-half">

				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<label class="lable">Old Password</label>
					</div>
					<div class="ctrl-col-wrapper">
						<html:password property="oldPassword" styleId="oldPassword"
							styleClass="text-box"></html:password>
					</div>
				</div>
				<span class="error"><html:errors property="oldPassword"></html:errors></span>
			</div>
			<div class="wrapper-half">

				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<label class="lable">New Password</label>
					</div>
					<div class="ctrl-col-wrapper">
						<html:password property="newPassword" styleId="newPassword"
							styleClass="text-box"></html:password>
					</div>
				</div>
			</div>
			<div class="wrapper-half">

				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">
						<label class="lable">Confirm New Password</label>
					</div>
					<div class="ctrl-col-wrapper">
						<html:password property="confirmPassword" styleId="confirmPassword"
							styleClass="text-box"></html:password>
					</div>
				</div>
			</div>
			<div class="btn-wrapper">
				<html:submit value="Reset Password" styleClass="btn"></html:submit>
				<input type="button" value="Cancel" class="btn" onclick="backToLoginPage();">
			</div>
		</html:form>
	</div>
</div>
<script type="text/javascript">
	chosenConfig();
	function chosenConfig() {
		var config = {
			'.chosen-select' : {
				width : "150px"
			},
			'.chosen-select-deselect' : {
				allow_single_deselect : true,
				width : "90%"
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "90%"
			}

		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
	}
</script>