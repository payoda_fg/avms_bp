<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<!-- Files used for load jqgrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if(e.which == 13)
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'search')
				{
					searchNaicsCodes();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
	
	function searchNaicsCodes()
	{
		var text = $("#search").val();
		var postdata = $("#addtree").jqGrid('getGridParam', 'postData');
		var isNumber = $.isNumeric(text);
		if(isNumber){
			$.extend(postdata, {
				filters : '',
				searchField : 'code',
				searchOper : 'eq',
				searchString : text
			});
		} else {
			$.extend(postdata, {
				filters : '',
				searchField : 'name',
				searchOper : 'bw',
				searchString : text
			});
		}
		$("#addtree").jqGrid('setGridParam', {
			search : text.length > 0,
			postData : postdata
		});
		$("#addtree").trigger("reloadGrid", [ {
			page : 1
		} ]);
	}
		
	function showOwnerInfo() {
		window.location = "selfvendornavigation.do?parameter=diverseClassificationNavigation";
	}
	
	function pickNaics(value) {
		press = value;
		pickNaicsCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.85;
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#dialog").dialog({
				autoOpen : false,
				width : dWidth,
				height : dHeight,
				modal : true,
				close : function(event, ui) {
					//close event goes here
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});
		$("#dialog").dialog("open");
	}
	
	function add() {
		var code1 = document.getElementById("naicsCode_0").value;
		var code2 = document.getElementById("naicsCode_1").value;
		var code3 = document.getElementById("naicsCode_2").value;
		if(code2=='Optional'){
			code2="";
		}
		if(code3=='Optional'){
			code3="";
		}
		if(naicsCode!=""){
			if (((code3!="" && code2!="" && code3 == code2))
					|| ((code3!="" && code1!="" && code3 == code1))
					||(code2!="" && code1!="" && code2 == code1)
					||(naicsCode!="" && naicsCode==code1)||
					(naicsCode==code2)||(naicsCode==code3)) {
				alert("Selected NAICS already Exists ");
			} else{
				if (press == 0) {
					$('#naicsCode_0').val(naicsCode);
					$('#naicsDesc_0').val(des);
					$('#naicsDesc0_0').focus();
				} else if (press == 1) {
					$('#naicsCode_1').val(naicsCode);
					$('#naicsDesc_1').val(des);
					$('#naicsDesc1_1').focus();
				} else if (press == 2) {
					$('#naicsCode_2').val(naicsCode);
					$('#naicsDesc_2').val(des);
					$('#naicsDesc2_2').focus();
				}
				naicscode = '';
				rowLevel = '';
				des = '';
				press = 0;
				$("#dialog").dialog("close");
			}
		} else {
			alert('Please Select NAICS');
		}
	}
	
	function reset() {
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#dialog").dialog("close");
	}
	
	/* $(document).ready(function() {		
		$("#search").keyup(function(e) {
			if(e.which == 13) {
			
			}
		});
	}); */// document.ready
</script>
<style>
.main-text-box {
	width: 86%;
	padding: 0%;
}
</style>

<!-- For Adding Keyword Dynamically -->
<script type="text/javascript">
	var index = 0;
</script>

<html:form action="selfregservicearea.do?parameter=saveServiceArea"
	method="post" styleId="naicsForm">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
				<html:hidden property="id" styleId="vendorId"></html:hidden>
			<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
				<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
				<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
				<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
				<input type="button" class="btn" value="Back"
					onclick="showOwnerInfo();"> 
				<input type="submit" class="btn" value="Save" id="submit" name="submit">
				<input type="reset" class="btn" value="Reset" id="reset">
			</div>
				<div id="step-1" class="content">
					<h2 class="StepTitle">Service Area</h2>
					<html:hidden property="naicslastrowcount" value="3"
						styleId="naicslastrowcount"></html:hidden>
					<html:hidden property="lastrowcount" name="editVendorMasterForm"
						styleId="lastrowcount" />
					<table width="100%" border="0" class="main-table">
						<tr>
							<td class="header">NAICS Code</td>
							<td class="header">NAICS Desc</td>
							<td class="header">Capabilities</td>
							<td class="header">NAICS Type</td>
							<td class="header">Action</td>
						</tr>
						<tr class="even">
							<td><html:hidden property="naicsID1" styleId="naicsID1" /> <html:text
									property="naicsCode_0" styleId="naicsCode_0" tabindex="86"
									size="10" alt=""
									title="Click the search image to select NAICS code"
									readonly="true" styleClass="main-text-box" /><img
								src="images/magnifier.gif" onClick="pickNaics(0);"> <span
								class="error"><html:errors property="naicsCode_0"></html:errors>
							</span></td>
							<td><html:textarea property="naicsDesc1"
									styleId="naicsDesc_0" readonly="true"
									styleClass="main-text-area" tabindex="87" /></td>
							<td><html:textarea property="capabilitie1"
									styleId="naicsDesc0_0" styleClass="main-text-area"
									tabindex="88" /></td>
							<td>Primary</td>
						</tr>
						<tr class="even">
							<td><html:hidden property="naicsID2" styleId="naicsID2" /> <html:text
									property="naicsCode_1" styleId="naicsCode_1" tabindex="90"
									size="10" alt="Optional" readonly="true"
									styleClass="main-text-box" /><img src="images/magnifier.gif"
								onClick="pickNaics(1);"> <span class="error"><html:errors
										property="naicsCode_1"></html:errors> </span></td>
							<td><html:textarea property="naicsDesc2"
									styleId="naicsDesc_1" readonly="true"
									styleClass="main-text-area" tabindex="91" /></td>
							<td><html:textarea property="capabilitie2"
									styleId="naicsDesc1_1" styleClass="main-text-area"
									tabindex="92" /></td>
							<td>Secondary</td>
							<td id="naicsDelete2"><a onclick="return clearNaics('2');"
								href="#">Delete</a></td>
						</tr>
						<tr class="even">
							<td><html:hidden property="naicsID3" styleId="naicsID3" /> <html:text
									property="naicsCode_2" styleId="naicsCode_2" tabindex="94"
									size="10" alt="Optional" readonly="true"
									styleClass="main-text-box" /><img src="images/magnifier.gif"
								onClick="pickNaics(2);"> <span class="error"><html:errors
										property="naicsCode_2"></html:errors> </span></td>
							<td><html:textarea property="naicsDesc3"
									styleId="naicsDesc_2" readonly="true"
									styleClass="main-text-area" tabindex="95" /></td>
							<td><html:textarea property="capabilitie3"
									styleId="naicsDesc2_2" styleClass="main-text-area"
									tabindex="96" /></td>
							<td>Secondary</td>
							<td id="naicsDelete3"><a onclick="return clearNaics('3');"
								href="#">Delete</a></td>
						</tr>
					</table>
					<div class="clear"></div>

					<div id="dialog" title="NAICS Code" style="display: none;">
						<div class="page-title">
							<input type="search" placeholder="Search" alt="Search"
								style="float: left; width: 100%;" class="main-text-box"
								id="search">
						</div>
						<div class="clear"></div>
						<div id="gridContainer" style="height: 350px; overflow: auto;">
							<table id="addtree" width="100%"></table>
							<div id="paddtree"></div>
						</div>
						<div class="btn-wrapper" style="margin: 0px;">
							<input type="button" value="Cancel" class="btn"
								onclick="reset();">
							<input type="button" value="Ok" class="btn" onclick="add();">
						</div>
					</div>
					<div class="clear"></div>
					
					<div class="panelCenter_1">
						<h3>Keywords</h3>
						<div class="form-box">
							<table class='main-table' width='100%' border='0' id="keywords"
								style='border: none; border-collapse: collapse; float: none;'>
								<thead>
									<tr>
										<td class='header'>Keywords</td>								
										<td class='header'>Action</td>
									</tr>
								</thead>
								<tbody>
									<logic:present name="vendorKeywords">
										<bean:size id="size" name="vendorKeywords" />
										<logic:equal value="0" name="size">
											<tr>
												<td>Nothing to display</td>
											</tr>
										</logic:equal>
									</logic:present>
									<logic:present name="vendorKeywords">
										<logic:iterate id="vendorKeyword" name="vendorKeywords" indexId="index">
											<tr id="<%=index %>" class="even">
												<td>
													<logic:present property="keyword" name="vendorKeyword">
														<bean:define id="keyword" property="keyword" name="vendorKeyword"/>
														<html:text property="vendorKeyword" value="<%=keyword.toString() %>" styleClass="main-text-box" alt=""/>
													</logic:present>
												</td>
												<td>
													<logic:present property="id" name="vendorKeyword">
														<bean:define id="id" property="id" name="vendorKeyword"/>
														<html:link action="deleteKeyword.do?method=deleteVendorKeyword" paramId="id" paramName="id" styleId="deleteFile2" styleClass="del">
															<img src='images/deleteicon.jpg' alt='Delete'/>
														</html:link>
													</logic:present>
												</td>
											</tr>
											<script type="text/javascript">
												index++;
											</script>
										</logic:iterate>
									</logic:present>
								</tbody>
							</table>
							<div class="clear"></div>
							<INPUT id="cmd1" type="button" value="Add Keyword" class="btn" onclick="addRowKeyword('keywords')" />
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
					<input type="submit" class="btn" value="Next" id="submit3" name="submit3">
					<input type="button" class="btn" value="Back"
						onclick="showOwnerInfo();"> <input type="submit"
						class="btn" value="Save" id="submit2" name="submit2">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</html:form>
<script type="text/javascript">
$(document).ready(function() {
	$("#menu6").removeClass().addClass("current");
});
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		$("#naicsForm").validate({
			rules : {
				vendorKeyword : {					
					maxlength : 100
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					//if (validateNaics()){
						$('#naicsForm').append("<input type='hidden' name='submitType' value='submit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					//}
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
					$('#naicsForm').append(
									"<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#naicsForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
	
	function naicsChecked(id, value) {
		$('#' + id).val(value);
	}
	
	function deleteRow(ele) {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$(ele).closest("tr").remove();
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}
	
	function addRowKeyword(table) 
	{
		var rowCount = $("#" + table + " tr").length;		
		rowCount -= 2;
		var html = "<tr id='"+rowCount+"' class='even'><td><input type='text' name='vendorKeyword' class='main-text-box'></td></tr>";
		$("#" + table).append(html);
	}
	
	$("#keywords tr td .del").click(function(e) 
	{
		e.preventDefault();
		var row = $(this).closest('tr');
		var rowid = row.attr('id');
		var url = $(this).attr('href');
		var ajaxUrl = url.substring(url.lastIndexOf("/") + 1, url.length);		
		
		input_box = confirm("Are You Sure You Want to Delete this Record?");
		if (input_box == true)
		{
			// Output when OK is clicked
			$.ajax(
			{
				url : ajaxUrl,
				type : "POST",
				async : false,
				success : function() 
				{					
					$('#' + rowid).remove();
					alert('Record Deleted');
				}
			});
			return false;
		} 
		else 
		{			
			return false;
		}
	});
</script>