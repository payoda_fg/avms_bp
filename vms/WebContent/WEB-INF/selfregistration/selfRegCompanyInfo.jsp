<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<style type="text/css">
	* {
 	margin-top: -3px;
   	padding: 0;
    text-decoration: none;
}
</style>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				e.preventDefault();
				$('#submit').click();
			}
		}
	});
		
	function copyContact() {
		var firstName = $("#preparerFirstName").val();
		var lastName = $('#preparerLastName').val();
		var title = $('#preparerTitle').val();
		var phone = $('#preparerPhone').val();
		var mobile = $('#preparerMobile').val();
		var fax = $('#preparerFax').val();
		if ($("#sameContact").is(':checked')) {
			$("#firstName").val(firstName);
			$('#lastName').val(lastName);
			$('#designation').val(title);
			$('#contactPhone').val(phone);
			$('#contactMobile').val(mobile);
			$('#contactFax').val(fax);
		} else {
			$("#firstName").val('');
			$('#lastName').val('');
			$('#designation').val('');
			$('#contactPhone').val('');
			$('#contactMobile').val('');
			$('#contactFax').val('');
		}
	}
	function validateSalesYear(ele) {
		// To get the current year
		var d = new Date();
		var curr_year = d.getFullYear();
		var salesYear = ele.value;
		var yearOfEstablishment = $("#yearOfEstablishment").val();
		if (salesYear != "") {
			if (salesYear<yearOfEstablishment || salesYear>curr_year) {
				alert("Please enter year between " + yearOfEstablishment
						+ " and current year( " + curr_year + " )");
				$(ele).val('');
				$(ele).focus();
				return false;
			}
		}
	}
	function vendorContactPage(){
		window.location = "selfvendornavigation.do?parameter=contactNavigation";
	}
	
	$(document).ready(function() { // on page DOM load
		
		var revenue1 = $("#annualYear1").val();
		var revenue2 = $("#annualYear2").val();
		var revenue3 = $("#annualYear3").val();
		if(revenue1 === '0'){
			$("#annualYear1").val('');
		}
		if(revenue2 === '0'){
			$("#annualYear2").val('');
		}
		if(revenue3 === '0'){
			$("#annualYear3").val('');
		}
		
		if($("#annualTurnover").val() === 'Optional'){
			$("#annualTurnover").val('');
		}
		if($("#sales2").val() === 'Optional'){
			$("#sales2").val('');
		}
		if($("#sales3").val() === 'Optional'){
			$("#sales3").val('');
		}
		
	});
</script>
<html:form
	action="selfregcompanyinfo.do?parameter=saveCompanyInformation"
	method="post" styleId="companyForm" styleClass="FORM">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">  <!-- style="height: 717px;" -->
				<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
					<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
					<input
					type="button" class="btn" value="Back" onclick="vendorContactPage();">
					<input type="submit" class="btn" value="Save" id="submit" name="submit">
					<input type="reset" class="btn" value="Reset" id="reset">
				</div>
				<div id="step-1" class="content" style="display: block;">
					<h2 class="StepTitle">Company Information</h2>
					<div class="panelCenter_1">
						<br/>
						<h3>Company</h3>
						<div class="form-box">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">
										<bean:message key="comp.name" />
									</div>
									<div class="ctrl-col-wrapper">
										<html:text property="vendorName" alt="" styleId="vendorName"
											styleClass="text-box" tabindex="23" />
									</div>
									<span class="error"> <html:errors property="vendorName"></html:errors>
									</span>
								</div>
								<logic:present name="currentUser">
									<div class="row-wrapper form-group row">
										<div class="label-col-wrapper">Status</div>
										<div class="ctrl-col-wrapper">
											<html:select property="vendorStatus" styleId="vendorStatus"
												styleClass="chosen-select-width" tabindex="24">
												<logic:present name="editVendorMasterForm"
													property="statusMasters">
													<logic:iterate id="status" name="editVendorMasterForm"
														property="statusMasters">
														<bean:define id="id" name="status" property="id"></bean:define>
														<html:option value="<%=id.toString()%>">
															<bean:write name="status" property="statusname" />
														</html:option>
													</logic:iterate>
												</logic:present>
											</html:select>
										</div>
									</div>
								</logic:present>
							</div>
							<div id="hideDivforTier2Vendor">
								<div class="wrapper-half">
									<div class="row-wrapper form-group row">
										<div class="label-col-wrapper">Duns Number</div>
										<div class="ctrl-col-wrapper">
											<html:text styleClass="text-box" property="dunsNumber"
												styleId="dunsNumber" alt="Optional" tabindex="25" />
											<span class="error"> <html:errors
													property="dunsNumber"></html:errors>
											</span>
										</div>
									</div>
									<div class="row-wrapper form-group row">
										<div class="label-col-wrapper">Federal Tax ID</div>
										<div class="ctrl-col-wrapper">
											<html:text styleClass="text-box" property="taxId" alt=""
												styleId="taxId" tabindex="26" />
										</div>
										<span class="error"> <html:errors property="taxId"></html:errors>
										</span>
									</div>
								</div>
								<div class="wrapper-half">

									<div class="row-wrapper form-group row">
										<div class="label-col-wrapper">Company Type</div>
										<div class="ctrl-col-wrapper">
											<html:select property="companytype" styleId="companytype"
												styleClass="chosen-select-width" tabindex="27">
												<html:option value="">Select One</html:option>
												<logic:present name="editVendorMasterForm"
													property="legalStructures">
													<logic:iterate id="company" name="editVendorMasterForm"
														property="legalStructures">
														<bean:define id="id" name="company" property="id"></bean:define>
														<html:option value="<%=id.toString()%>">
															<bean:write name="company" property="name" />
														</html:option>
													</logic:iterate>
												</logic:present>
											</html:select>
										</div>
									</div>
									<div class="row-wrapper form-group row">
										<div class="label-col-wrapper">Number of Employees</div>
										<div class="ctrl-col-wrapper">
											<html:text styleClass="text-box" property="numberOfEmployees"
												alt="" styleId="numberOfEmployees" tabindex="28" />
										</div>
										<span class="error"> <html:errors
												property="numberOfEmployees"></html:errors>
										</span>
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Business Type</div>
									<div class="ctrl-col-wrapper">
										<html:select property="businessType" styleId="businessType"
											styleClass="chosen-select-width" tabindex="29">
											<html:option value="">Select One</html:option>
											<logic:present name="editVendorMasterForm"
												property="businessTypes">
												<logic:iterate id="business" name="editVendorMasterForm"
													property="businessTypes">
													<bean:define id="id" name="business" property="id"></bean:define>
													<html:option value="<%=id.toString()%>">
														<bean:write name="business" property="typeName" />
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select>
									</div>
									<span class="error"> <html:errors
											property="businessType"></html:errors>
									</span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Year Established</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box"
											property="yearOfEstablishment" alt=""
											styleId="yearOfEstablishment" tabindex="30"
											onblur="return currentYearValidation('yearOfEstablishment');" />
									</div>
									<span class="error"> <html:errors
											property="yearOfEstablishment"></html:errors>
									</span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Website URL(http://)</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="website"
											alt="" styleId="website" tabindex="31" />
									</div>
									<span class="error"> <html:errors property="website"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Email</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="emailId"
											styleId="emailId" alt="" tabindex="32" />
									</div>
									<span class="error"> <html:errors property="emailId"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">State Incorporation</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="stateIncorporation"
											styleId="stateIncorporation" tabindex="33" alt="Optional" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">State Sales TaxId</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="stateSalesTaxId"
											styleId="stateSalesTaxId" tabindex="34" alt="Optional" />
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="panelCenter_1" style="padding-top: 2%;">
						<h3>Revenue (for the past three years)</h3>
						<div class="form-box">
							<table class="main-table" style="width: 100%;">
								<tr>
									<td class="header">Year</td>
									<td class="header">Annual Revenue</td>
								</tr>
								<tr class="even">
									<td><html:text styleClass="main-text-box"
											name="editVendorMasterForm" property="annualYear1"
											alt="" styleId="annualYear1" tabindex="35"
											onchange="validateSalesYear(this);" /></td>
									<td><html:hidden property="annualTurnoverId1" alt="" /> <html:text
											styleClass="main-text-box" property="annualTurnover"
											alt="" styleId="annualTurnover"
											onchange="moneyFormatToUS('annualTurnover','turnoverFormatValue');"
											tabindex="35" /> <html:hidden styleId="turnoverFormatValue"
											property="annualTurnoverFormat" alt="" /> <span
										class="error"> <html:errors property="annualTurnover"></html:errors>
									</span></td>
								</tr>
								<tr class="even">
									<td><html:text styleClass="main-text-box"
											name="editVendorMasterForm" property="annualYear2"
											alt="Optional" styleId="annualYear2" tabindex="36"
											onchange="validateSalesYear(this);" /></td>
									<td><html:hidden property="annualTurnoverId2" alt="" /> <html:text
											styleClass="main-text-box" property="sales2" tabindex="36"
											styleId="sales2" alt="Optional"
											onchange="moneyFormatToUS('sales2','salesFormat2');" /> <html:hidden
											styleId="salesFormat2" property="salesFormat2" alt="" /><span
										class="error"> <html:errors property="sales2"></html:errors>
									</span></td>
								</tr>
								<tr class="even">
									<td><html:text styleClass="main-text-box"
											name="editVendorMasterForm" property="annualYear3"
											alt="Optional" styleId="annualYear3" tabindex="37"
											onchange="validateSalesYear(this);" /></td>
									<td><html:hidden property="annualTurnoverId3" alt="" /> <html:text
											styleClass="main-text-box" property="sales3" alt="Optional"
											tabindex="37" styleId="sales3"
											onchange="moneyFormatToUS('sales3','salesFormat3');" /> <html:hidden
											styleId="salesFormat3" property="salesFormat3" alt="" /><span
										class="error"> <html:errors property="sales3"></html:errors>
									</span></td>
								</tr>
							</table>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2"> <input
						type="submit" class="btn" value="Next" id="submit3" name="submit3"> <input
						type="button" class="btn" value="Back" onclick="vendorContactPage();"> <input
						type="submit" class="btn" value="Save" id="submit2" name="submit2">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
</html:form>
<script type="text/javascript">
$(document).ready(function() {
	$("#menu2").removeClass().addClass("current");
});
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
	}, "No Special Characters Allowed.");
	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	jQuery.validator.addMethod("allowhyphens", function(value, element) {
		return this.optional(element) || /^[0-9-]+$/.test(value);
	}, "Please enter valid numbers.");
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		
		$("#companyForm").validate({
				rules : {
					
					vendorName : {
						maxlength : 255
					},
					yearOfEstablishment : {
						onlyNumbers : true,
						range : [ 1900, 3000 ]
					},
					website : {
						maxlength : 255						
					},
					dunsNumber : {
						allowhyphens : true,
						maxlength : 255
					},
					taxId : {
						allowhyphens : true,
						maxlength : 255
					},
					numberOfEmployees : {
						onlyNumbers : true,
						maxlength : 10
					},
					address1 : {
						maxlength : 120
					},
					city : {
						alpha : true,
						maxlength : 60
					},
					state : {
					},
					province : {
						maxlength : 60
					},
					region : {
						maxlength : 60
					},
					zipcode : {
						allowhyphens : true
					},
					zipcode2 : {
						allowhyphens : true
					},
					emailId : {
						email : true
					},
					annualYear1 : {
						onlyNumbers : true,
						range : [ 1900, 3000 ],
						required : true
					},
					annualTurnover : {
						required : true
					},
				annualYear2 : {
						required : function() {
							if ($('#sales2').val()!="") {
								range : [ 1900, 3000 ]
								return true;
							} else {
								return false;
							}
						},
						onlyNumbers : true,
						annualYear2 : false
					},
					sales2:{
						required : function() {
							if ($('#annualYear2').val()!="") {
								return true;
							} else {
								return false;
							}
						},
						sales2 : false
						
					},
					sales3:{
						required : function() {
							if ($('#annualYear3').val()!="") {
								return true;
							} else {
								return false;
							}
						},
						sales3 : false
						
					},
					annualYear3 : {
						
						required : function() {
							if ($('#sales3').val()!="") {
								range : [ 1900, 3000 ]
								return true;
							} else {
								return false;
							}
						},
						onlyNumbers : true,
						annualYear3 : false
					}
				},
				submitHandler : function(form) {

					if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
						$('#companyForm').append("<input type='hidden' name='submitType' value='submit' />");
						$("#ajaxloader").css('display','block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
						alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
						$('#companyForm').append("<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#saveandexit1').data('clicked')
							|| $('#saveandexit2').data('clicked')){
						$('#companyForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}
				},
				invalidHandler : function(form,	validator) {
					$("#ajaxloader").css('display','none');
					alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
					$('#submit1').data('clicked', false);
					$('#submit').data('clicked', false);
					$('#submit2').data('clicked', false);
					$('#submit3').data('clicked', false);
					$('#saveandexit1').data('clicked', false);
					$('#saveandexit2').data('clicked', false);
				}
		});
	});
</script>