<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<!-- Files used for load jqgrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<!-- This is for Auto Sizing the Text Area -->
<script type="text/javascript" src="jquery/ui/jquery.autosize.min.js"></script>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'searchCommodityText')
				{
					e.preventDefault();				
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}					
		}
	});
		
	function showServiceArea() {
		window.location = "selfvendornavigation.do?parameter=serviceAreaNavigation";
	}
	function validateBusinessArea(ele){
		
	    var businessFlag=true;
		$('#businessArea :selected').each(function(i, selected){ 
			 if ($(selected).text().match("Toledo") || $(selected).text().match("Whiting")){
				 <logic:present name="businessAreaconfig">
					<logic:iterate id="certconfig" name="businessAreaconfig">
					<logic:present name="diverseClassification">
					<bean:size id="diverse" name="diverseClassification" />
					<logic:greaterEqual value="0" name="diverse">
						<logic:iterate id="certificate" name="diverseClassification">
							<bean:define id="id" name="certificate" property="certMasterId.id"></bean:define>
							<logic:equal value="<%=id.toString()%>" name="certconfig" property="certificateId.id">
							businessFlag=false;
							</logic:equal>
						</logic:iterate>
						</logic:greaterEqual>
					</logic:present>
					</logic:iterate>
				</logic:present>
			 }
		});
		$('#businessArea :selected').each(function(i, selected){ 
			 if ($(selected).text().match("Toledo") || $(selected).text().match("Whiting")){
			    if(businessFlag){
			    	alert("Due to information you have provided, you must be a minority-owned enterprise, a women owned enterprise or a small business enterprise to register in this system.");
			    	businessFlag=false;
			    }
			 }
		});
	}
</script>
<style>
.main-text-box {
	width: 86%;
	padding: 0%;
}

.main-text-area {
	max-height : 500px
}
</style>
<html:form action="selfregbusinessarea.do?parameter=saveBusinessArea"
	method="post" styleId="serviceForm" onsubmit="return saveData();"> <!--Removed Bcoz of Vendor Commodities onsubmit="return saveData();" -->
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
			<html:hidden property="id" styleId="vendorId"></html:hidden>
			<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
				<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
				<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
				<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
				<input type="button" class="btn" value="Back"
					onclick="showServiceArea();"> 
				<input type="submit" class="btn" value="Save" id="submit" name="submit">
				<input type="reset" class="btn" value="Reset" id="reset">
			</div>
				<div id="step-1" class="content">
					<h2 class="StepTitle">Business Area</h2>
					
					<!-- We need this code, Please Don't Remove Even in New Architecture. Starts Here-->
					<h3>Vendor Commodities</h3>
					<table class='main-table' border='0' id="vendorCommodity"
						style='border: none; border-collapse: collapse; float: none;'>
						<thead>
							<tr>
								<td class='header' style="display: none;">Commodity ID</td>
								<td class='header' style="display: none;">Commodity Code</td>
								<td class='header'>Market Sector Description</td>
								<td class='header'>Market Subsector Description</td>
								<td class='header'>Commodity Description</td>
								<td class='header'>Action</td>
							</tr>
						</thead>
						<tbody>
							<logic:present name="vendorCommodities">
								<bean:size id="size" name="vendorCommodities" />
								<logic:equal value="0" name="size">
									<tr>
										<td>Nothing to display</td>
									</tr>
								</logic:equal>
							</logic:present>
							<logic:present name="vendorCommodities">
							<logic:iterate id="vendorCommodityDto" name="vendorCommodities"
									indexId="index">
									<tr id="row<%=index%>" class="even">
										<td style="display: none;"><bean:define id="id"
												property="vendorCommodityId" name="vendorCommodityDto"></bean:define><html:text
												property="commodityId" value="<%=id.toString()%>"
												styleClass="main-text-box" alt="" styleId="commodityId" /></td>
										 <td style="display: none;"><bean:define id="commodityCode"
												property="commodityCode" name="vendorCommodityDto"></bean:define>
											<html:text property="commodityCode" readonly="true"
												value="<%=commodityCode.toString()%>"
												styleClass="main-text-box" alt="" /></td> 
										<td><bean:define id="sectorDescription"
												property="sectorDescription" name="vendorCommodityDto"></bean:define>
											<html:text property="sectorDescription"
												value="<%=sectorDescription.toString() %>" styleClass="main-text-box" alt="" readonly="true"/></td>
									   <td><bean:define id="subSectorDescription"
												property="categoryDesc" name="vendorCommodityDto"></bean:define>
											<html:text property="categoryDesc"
												value="<%=subSectorDescription.toString() %>" styleClass="main-text-box" alt="" readonly="true"/></td>
										<td><bean:define id="commodityDescription"
												property="commodityDescription"
												name="vendorCommodityDto"></bean:define> <html:text
												property="commodityDescription"
												value="<%=commodityDescription.toString()%>"
												styleClass="main-text-box" alt="" readonly="true" /></td>
										<td><html:link
												action="deletecommodity.do?method=deleteCommodity"
												paramId="id" paramName="id" styleId="deleteCommodity"
												styleClass="del1">
												<img src='images/deleteicon.jpg' alt='Delete' />
											</html:link></td>
									</tr>
								</logic:iterate>
							 	<%--<logic:iterate id="vendorCommodity" name="vendorCommodities"
									indexId="index">
									<tr id="row<%=index%>" class="even">
										<td style="display: none;"><bean:define id="id"
												property="id" name="vendorCommodity"></bean:define> <html:text
												property="commodityId" value="<%=id.toString()%>"
												styleClass="main-text-box" alt="" styleId="commodityId" /></td>
										<td><bean:define id="commodityCode"
												property="commodityId.commodityCode" name="vendorCommodity"></bean:define>
											<html:text property="commodityCode" readonly="true"
												value="<%=commodityCode.toString()%>"
												styleClass="main-text-box" alt="" /></td>
										<td><bean:define id="commodityDesc"
												property="commodityId.commodityDescription"
												name="vendorCommodity"></bean:define> <html:text
												property="commodityDesc"
												value="<%=commodityDesc.toString()%>"
												styleClass="main-text-box" alt="" readonly="true" /></td>
										<td><html:link
												action="deletecommodity.do?method=deleteCommodity"
												paramId="id" paramName="id" styleId="deleteCommodity"
												styleClass="del1"><img src='images/deleteicon.jpg' alt='Delete' /></html:link></td>
									</tr>
								</logic:iterate> --%>
							</logic:present>
						</tbody>
					</table> 
					<div class="clear"></div>
					<INPUT id="cmd2" type="button" value="Add Commodity" class="btn"
						onclick="addCommodity()" tabindex="98" />
					<!-- We need this code, Please Don't Remove Even in New Architecture. Ends Here -->
						
					<div class="clear"></div>
					<div class="panelCenter_1">
						<h3>Services Description</h3>
						<div class="form-box">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Describe Services</div>
									<div class="ctrl-col-wrapper">
										<html:textarea styleClass="main-text-area"
											property="vendorDescription" alt=""
											styleId="vendorDescription" tabindex="99" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Company Information</div>
									<div class="ctrl-col-wrapper">
										<html:textarea styleClass="main-text-area"
											property="companyInformation" alt=""
											styleId="companyInformation" tabindex="100" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="panelCenter_1">
						<h3>Business Services</h3>
						<div class="form-box">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Service Area</div>
									<div class="ctrl-col-wrapper">
										<html:select property="serviceArea" styleId="serviceArea"
											 multiple="true" tabindex="101">
											<bean:size id="serviceAreaSize" name="serviceAreaList" />
											<logic:greaterEqual value="0" name="serviceAreaSize">
												<logic:iterate id="service" name="serviceAreaList">
													<bean:define id="serviceareaname" name="service"
														property="id"></bean:define>
													<html:option value="<%=serviceareaname.toString()%>">
														<bean:write name="service" property="serviceArea" />
													</html:option>
												</logic:iterate>
											</logic:greaterEqual>
										</html:select>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Business Area</div>
									<div class="ctrl-col-wrapper">
										<html:select property="businessArea" styleId="businessArea"
											style="width:90%;" 
											multiple="true" tabindex="102" onchange="validateBusinessArea(this);">
											<bean:size id="areasize" name="serviceAreas1" />
											<logic:greaterEqual value="0" name="areasize">
												<logic:iterate id="area" name="serviceAreas1">
													<bean:define id="bgroup" name="area"
														property="businessGroup"></bean:define>
													<bean:define id="barea" name="area" property="serviceArea"></bean:define>
													<optgroup label="<%=bgroup.toString()%>">
														<%
															String[] areas = barea.toString().split("\\|");
															if (areas != null && areas.length != 0) {
																for (int i = 0; i < areas.length; i++) {
																	String[] parts = areas[i].split("-");
																	String areaId = parts[0];
																	String areaName = parts[1];
														%>
														<option value="<%=areaId%>"><%=areaName%></option>
														<%
															}
														}
														%>
													</optgroup>
												</logic:iterate>
											</logic:greaterEqual>
										</html:select>
										<script>
									var arrayValue=[];
									</script>
										<logic:present name="editVendorMasterForm"
											property="businessArea">
											<logic:iterate id="selectID" name="editVendorMasterForm"
												property="businessArea">
												<script>
												arrayValue.push('<%=selectID%>');
												</script>
											</logic:iterate>
										</logic:present>
										<script>
											$('select[id=businessArea] option').each(function() {
												for ( var index = 0; index < arrayValue.length; index++)
													if ($(this).val() == arrayValue[index])
														$(this).attr('selected','selected');
											});
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					
					<!-- We need this code, Please Don't Remove Even in New Architecture. Starts Here-->
					<div id="dialog2" title="Choose Commodity" style="display: none;">
					
					<div class="wrapper-full">
							<div class="row-wrapper form-group row">
								 <div class="label-col-wrapper" style="width: 25%;">Search Vendor Commodities:</div>
								 <div class="ctrl-col-wrapper">
								 	<input type="search" class="text-box" id="searchCommodityText" placeholder="Search">
								 </div>
							</div>
							<div class="row-wrapper form-group row">
								<div class="label-col-wrapper" style="width: 25%;"></div>
<!-- 								<div class="ctrl-col-wrapper"><input type="button" value="Search" class="btn" onclick="commodityListBySearch();"></div> -->
							</div>
					
					<%--<div class="wrapper-full">
							<div class="row-wrapper form-group row">
								<div class="label-col-wrapper" style="width: 25%;">Choose BP Market Sector:</div>
								<div class="ctrl-col-wrapper">
									<select name="marketSectors" class="chosen-select" id="marketSectors" 
										onchange="changeMarketSubSectorBasedOnSector()" style="width: 60%;">
										<option value="0">-- Select --</option>	
										<logic:present name="marketsectors">
											<logic:iterate id="sectorIterator" name="marketsectors">
												<option value='<bean:write name="sectorIterator" property="id"  />'>
																<bean:write name="sectorIterator" property="sectorDescription" />
												</option>
											</logic:iterate>
										</logic:present>
									</select>
								</div>
							</div>
							<div class="row-wrapper form-group row">
								<div class="label-col-wrapper" style="width: 25%;">Choose BP Market Subsector:</div>
								<div class="ctrl-col-wrapper">
									<select name="marketSubSectors" class="chosen-select" id="marketSubSectors" 
										onchange="changeGridBasedOnSubSector()"  style="width: 60%;">
										<option value="0">-- Select --</option>
										<logic:present name="marketsubsectors">
											<logic:iterate id="subsectorIterator" name="marketsubsectors">
												<option value='<bean:write name="subsectorIterator" property="id"  />'>
																<bean:write name="subsectorIterator" property="categoryDescription" />
												</option>
											</logic:iterate>
										</logic:present>
									</select>
								</div>
							</div>
						</div> --%>
						<div class="clear"></div>					
						<table id="list2" width="100%"></table>
						<div id="pager2"></div>
						<div class="btn-wrapper" style="margin: 5px; width:55%;" id="okButtonDiv" hidden="true">
							<input type="button"
								value="Cancel" class="btn" onclick="cancelCommodity();">
							<input type="button" value="Ok" class="btn"
								onclick="addCommodityContent();"> 
						</div>
					</div>
					<div class="clear"></div>
				</div>
				</div>
				<!-- We need this code, Please Don't Remove Even in New Architecture. Ends Here -->
				
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
					<input type="submit" class="btn" value="Next" id="submit3" name="submit3">
					<input type="button" class="btn" value="Back"
						onclick="showServiceArea();"> <input type="submit"
						class="btn" value="Save" id="submit2" name="submit2">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</html:form>
<script type="text/javascript">
var commodityStatus=0;
var rowCount =0;
<logic:present name="commodityCode">	
var x = document.getElementById("vendorCommodity").rows.length;
	commodityStatus=x-1;
rowCount = commodityStatus;
</logic:present>
$(document).ready(function() {
	$('#vendorDescription').autosize();
	$('#companyInformation').autosize();
	
	$("#menu7").removeClass().addClass("current");
	
	$("#searchCommodityText").keyup(function(e) {
		if($("#searchCommodityText").val() != '')
		 commodityListBySearch();
		else{
			$("#list2").hide();
			$("#okButtonDiv").hide();
		}
	});
});
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		$("#serviceForm").validate({
			rules : {
				vendorDescription : {
					rangelength : [ 0, 5000 ]
				},
				companyInformation : {
					rangelength : [ 0, 5000 ]
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					$('#serviceForm')
							.append(
									"<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
					$('#serviceForm')
							.append(
									"<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#serviceForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
	
	<!-- We need this code, Please Don't Remove Even in New Architecture. Starts Here-->
	function cancelCommodity() {
		$("#dialog2").dialog("close");
	}
	
	$("#vendorCommodity tr td .del1").click(function(e) {
		e.preventDefault();
		var row = $(this).closest('tr');
		var rowid = row.attr('id');
		var url = $(this).attr('href');
		var ajaxUrl = url.substring(url.lastIndexOf("/") + 1, url.length);
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$.ajax({
				url : ajaxUrl,
				type : "POST",
				async : false,
				success : function() {
					$('#vendorCommodity tr#' + rowid).remove();
					alert('Record Deleted');
					commodityStatus=commodityStatus-1;
				}
			});
			return false;
		} else {
			return false;
		}
	});
	
	function addCommodity() {
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		$("#okButtonDiv").hide();
		$("#searchCommodityText").val('');
		//var wHeight = $(window).height();
		//var dHeight = wHeight * 0.8;
		
		//To Reset the Select Boxes....
		$("#marketSectors option[value = " + 0 + "]").prop('selected', true);
		$("#marketSectors").select2();
		
		$('#marketSubSectors').empty();
		$('#marketSubSectors').find('option').remove().end().append("<option value='0'>-- Select --</option>");
		$('#marketSubSectors').select2();
		
		//To Reset Grid Data
		$("#list2").jqGrid('GridUnload');
		
		$("#dialog2").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});
		$("#dialog2").dialog({
			width : dWidth,
			//height : dHeight,
			minheight : 400,
			modal : true,
			open: function(){
				document.getElementById("searchCommodityText").focus();
		    },
			close : function(event, ui) {
				//close event goes here
			},
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}
	
	var selectedIDs = [];
	function changeGridBasedOnSubSector()
	{
		var marketSubSectorId = $("#marketSubSectors").val();		
		selectedIDs = pickCommodityCode(marketSubSectorId);
	} 
	
	function addCommodityContent() {
		var grid = jQuery("#list2");
// 		var rowCount = $("#vendorServiceArea tr").length;
// 		rowCount -= 1;
		var html = "";
		var messages = [];
		messages.push("Following Commodities already selected. You cannot add it again.\n");
		if (selectedIDs.length > 0) {
			for ( var i = 0, il = selectedIDs.length; i < il; i++) {
				commodityStatus=commodityStatus+1;
				var row = grid.getLocalRow(selectedIDs[i]);
				var commodityCode = row.commodityCode1;
				var commodityDesc = row.commodityDesc;
				var sectorDesc = row.sectorDesc;
				var subSectorDesc = row.commodityCategoryId;
				var commodityExists = false;
				$("input[name='newCommodityCode']").each(function(index) { //For each of inputs
				    if (commodityCode === $(this).val()) { //if match against array
				    	messages.push(commodityDesc + "\n");
				    	commodityStatus=commodityStatus-1;
				        commodityExists = true;
				    } 
				});
				$("input[name='commodityCode']").each(function(index) { //For each of inputs
				    if (commodityCode === $(this).val()) { //if match against array
				    	messages.push(commodityDesc + "\n");
				    	commodityStatus=commodityStatus-1;
				        commodityExists = true;
				    } 
				});
				if(!commodityExists && commodityCode != ''){
					html = html	+ "<tr class='even' id='row" + rowCount + "'>";
					html = html
							+ "<td style='display:none;'><input type='text' name='commodityId' class='main-text-box' value='0'></td>";
					html = html
							+ "<td style='display:none;'><input type='text' name='newCommodityCode' class='main-text-box' readonly='readonly' value='" + commodityCode + "'></td>";
					html = html
				    		+ "<td><input type='text' name='sectorDescription' class='main-text-box' readonly='readonly' value='" + sectorDesc + "'></td>";
				    html = html
				    		+ "<td><input type='text' name='subSectorDescription' class='main-text-box' readonly='readonly' value='" + subSectorDesc + "'></td>";
					html = html
							+ "<td><input type='text' name='commodityDesc' class='main-text-box' readonly='readonly' value='" + commodityDesc + "'></td>";
					html = html
							+ "<td><a style='cursor: pointer;' onclick='deleteRow(this);' >Delete</a></td></tr>";
					rowCount++;
				}
				else if(commodityCode =='')
				{
					alert("Few recode(s) donot have Commodity Description.");
					commodityStatus = commodityStatus-1;
				}
			}
			if(messages.length > 1){
				alert(messages.join(""));
			}
			$("#vendorCommodity").append(html);
			$("#dialog2").dialog("close");
		}
		else
		{
			alert("Please select atleast one record");	
			return false;
		}
	}
	
	function deleteRow(ele) {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$(ele).closest("tr").remove();
			commodityStatus=commodityStatus-1;
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}	
	
	//validate Business area atleast one commodity.
	function saveData(){
		if(commodityStatus > 0 && commodityStatus <= 3){
			return true;
		}else if(commodityStatus > 3)
		{
			
			$("#ajaxloader").show();
			alert("You can Upload Only 3 Commodity Codes");	
			$("#ajaxloader").css('display','none');
			$('#submit1').data('clicked', false);
			$('#submit').data('clicked', false);
			$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
			$('#saveandexit1').data('clicked', false);
			$('#saveandexit2').data('clicked', false);
		 	return false;
		}
		else
			{
			$("#ajaxloader").show();
				alert("Please select atleast one commodity");	
				$("#ajaxloader").css('display','none');
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
 				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			 	return false;
		}
	}
	
	function changeMarketSubSectorBasedOnSector() 
	{
		var marketSectorId = $("#marketSectors").val();
		var optionBody = '';
		
		if(marketSectorId > 0) 
		{
			$.ajax(
			{
				url : "commoditycategory.do?method=showMarketSubSectorsBySector&selectedSector=" + marketSectorId
					+ "&sectorChangedIn=commodity",
					type : "POST",
					async : false,
					success : function(data) 
					{
						if (data.subSectorList != '') 
						{
							$('#marketSubSectors').empty();
							optionBody += "<option value='0'>-- Select --</option>";
							for ( var i in data.subSectorList) 
							{
								optionBody += "<option value="+data.subSectorList[i].id+"> " + data.subSectorList[i].subSectorName + " </option>";
							}
							$('#marketSubSectors').find('option').remove().end().append(optionBody);
							$('#marketSubSectors').select2();
						} 
						else 
						{
							$('#marketSubSectors').empty();
							optionBody += "<option value='0'>-- Select --</option>";
							$('#marketSubSectors').find('option').remove().end().append(optionBody);
							$('#marketSubSectors').select2();
						}
					}
			});
		} 
		else 
		{
			$('#marketSubSectors').empty();
			optionBody += "<option value='0'>-- Select --</option>";
			$('#marketSubSectors').find('option').remove().end().append(optionBody);
			$('#marketSubSectors').select2();
		}	
	}
	
	function commodityListBySearch()
	{
		var searchCommodityText = $("#searchCommodityText").val();
		selectedIDs = pickCommodityCode(searchCommodityText);
		$("#okButtonDiv").show();
	}
</script>