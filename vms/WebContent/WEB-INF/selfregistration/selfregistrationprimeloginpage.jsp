<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<style>
<!--
#ajaxloader {
	position: absolute;
	z-index: 99999;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}
-->
</style>
<script>
	function DisplayPrivacyTerms()
	{
		$("#dialog1").css(
		{
			"display" : "block"
		});		
		
		$("#dialog1").dialog(
		{
			width : 800,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
	}

	// jQuery.validator.addMethod("password",function(value, element) {
	// 	return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	// },	"Your password contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

	/* function show(value) {
		if (value == 1) {
			$('#contentYes').show();
			$('#contentNo').hide();
		} else {
			$('#contentNo').show();
			$('#contentYes').hide();
		}
	} */
	$(document).ready(function() {
		$('#contentYes').show();
		$("#contanctEmail").val('');
		$("#loginpassword").val('');
		$("#loginForm").validate({
			rules : {
				contanctEmail : {
					required : true,
					email : true
				},
				loginpassword : {
					required : true
				}
			}
		});
	});

	var emailobj;
	function sendLoginCredentials() {
		var emailObj = $("#emailId");
		var emailId = $("#emailId").val();
		var divisionObj = $("#divisionId");
		var divisionId = $("#divisionId").val();
// 		var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
// 		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(([a-zA-Z]+\.)+[a-zA-Z]{2,})$/;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (emailId != "" && re.test(emailId) && divisionId !="Select") {
			ajaxFn(emailObj, "VE");
			$
					.ajax({
						url : "selfregistration.do?method=sendLoginCredentials&preparerEmail="
								+ emailId
								+ "&customerDivision="
								+ divisionId
								+ "&prime=yes",
						type : "POST",
						async : true,
						beforeSend : function() {
							$("#ajaxloader").css('display', 'block');
							$("#ajaxloader").show(); //show image loading
						},
						success : function(data) {
							$("#ajaxloader").hide();
							//$("#contentYes").hide();
							alert($.trim(data));
							$("#emailId").val('');
						}
					});
		} else {
			if(divisionId !="Select")
				alert("Please Enter valid Email Address...");				
			else
				alert("Please Select Division...");
		}
	}

	function validateEmail(obj) {
		emailobj = obj;
		//ajaxFn(obj, "VE");
	}
</script>
<html:form
	action="retrieveprimereg.do?method=retrieveSavedData&prime=yes"
	styleId="loginForm">
	<div class="form-box"
		style="width: 96%; margin-left: 1%; min-height: 400px;">
		<h1>Welcome To AVMS Prime Supplier Registration System</h1>
		<br>
		<!-- <p style="margin-top: 2%;">
			Is your business presently certified by one of the previously
			mentioned organizations: <input type="radio" name="sample" value="1"
				onclick="show('1')">Yes &nbsp;&nbsp;<input type="radio" name="sample"
				value="0" onclick="show('0')">No
		</p> -->
		<div id="contentYes" style="display: none;">
			<div id="ajaxloader" style="display: none;">
				<img src="images/ajax-loader1.gif" alt="Ajax Loading Image"
					class="ajax-loader" />
			</div>
			<p style="text-decoration: underline;">You must be <b>invited</b> by Supplier Diversity to register as a <b>Prime Supplier</b>.</p>
			<p style="color: red;"><b>This registration option is only intended for invited suppliers <span style="text-decoration: underline;">who will report their spend with M/WBEs into the AVMS on a quarterly basis.</span></b>
			   <br/><span style="color: black;">If this is <b>not</b> the case for your company, please return to the previous page using the Back button below to register under Supplier Registration.</span></p>
			<logic:equal value="1" name="isDivisionStatus" property="isDivision">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Please Select Customer
						Division Under which You want to Register</div>
					<html:select property="customerDivision" styleId="divisionId"
						styleClass="chosen-select" style="width:312px">
						<html:option value="Select">Select</html:option>
						<bean:size id="size" name="customerDivisions" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="customerDivision" name="customerDivisions">
								<bean:define id="id" name="customerDivision" property="id"></bean:define>
								<html:option value="${id}">
									<bean:write name="customerDivision" property="divisionname"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</logic:equal>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Please Enter Email Address:</div>
					<div class="ctrl-col-wrapper">
						<html:text property="preparerEmail" alt="" 
							styleId="emailId" onblur="validateEmail(this);" 
								styleClass="text-box" />
					</div>
				</div>
			</div>
			<div class="wrapper-btn">
				<input type="button" class="btn" id="submit" value="Send Credentials" onclick="sendLoginCredentials();">
			</div>
		</div>
		<div class="clear"></div>

		<div id="successMsg">
			<html:messages id="emailId" property="emailId" message="true">
				<span><bean:write name="emailId" /></span>
			</html:messages>
			<html:messages id="loginfail" property="loginfail" message="true">
				<span><bean:write name="loginfail" /></span>
			</html:messages>
			<html:messages id="msg" property="resetpassword" message="true">
				<span style="color: green; font-weight: bolder; font-size: medium;">
				<bean:write	name="msg" /></span>
			</html:messages>
		</div>
		<div>
			<p>Are you returning to complete your saved profile? Please login here.</p>
			<p>Notice! Please read prior to logging in: <a href="#" onclick="DisplayPrivacyTerms();" style="color: #009900;">Privacy and Terms</a></p>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Email Id</div>
					<div class="ctrl-col-wrapper">
						<html:text property="contanctEmail" alt="" 
							styleId="contanctEmail" styleClass="text-box" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Password</div>
					<div class="ctrl-col-wrapper">
						<html:password property="loginpassword" alt=""
							styleId="loginpassword" styleClass="text-box" />
					</div>
				</div>
			</div>
			<div class="wrapper-btn">
				<html:submit styleClass="btn" styleId="submit" value="Login"></html:submit>
				<html:link action="/home.do?method=loginPage" styleClass="btn">Back</html:link>
			</div>
		</div>
	</div>
</html:form>

<!-- Dialog Box for Privacy and Terms -->
<div id="dialog1" title="Privacy and Terms" style="display: none; max-height: 300px;">
	<logic:present name="userDetails" property="settings.privacyTerms">
		<bean:define id="privacyTerms" name="userDetails" property="settings.privacyTerms"/>
		<p><bean:write name="privacyTerms"/></p>
	</logic:present>
	<logic:notPresent name="userDetails" property="settings.privacyTerms">
		<p>There is no Privacy and Terms.</p>
	</logic:notPresent>
</div>
