<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				e.preventDefault();
				$('#submit').click();
			}			
		}
	});
		
	function showDocumentsPage() {
		window.location = "selfvendornavigation.do?parameter=documentsNavigation";
	}
	
	$(document).ready(function() 
	{
		var contactName = $("#contactFirstName").val();
		if(contactName != ""){
			$("#metYes").attr('checked', true);
		} else {
			$("#metNo").attr('checked', true);
		}
		
		$.datepicker.setDefaults({
			changeYear : true,
			dateFormat : 'mm/dd/yy'
		});
		
		$("#contactDate").datepicker();
		
		if ($("input:radio[name=metBpSupplier]:checked").val() == 1) {
			$('#meetingInfo').css('display', 'block');
		} else {
			$('#meetingInfo').css('display', 'none');
		}
		
		$('#metNo').click(function() {
		   $("#meetingInfo").hide();
		});
		
		$('#metYes').click(function() {
		   $("#meetingInfo").show();
		});
	});
</script>
<style>
<!--
.swMain div.actionBar {
	z-index: 0;
}

.swMain .stepContainer .StepTitle {
	z-index: 0;
}
-->
</style>
<html:form action="selfregmeetinginfo.do?parameter=saveMeetingInfo"
	method="post" styleId="meetingForm">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
				<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div class="actionBar top_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
					<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
					<input type="button" class="btn" value="Back"
						onclick="showDocumentsPage();"> <input type="submit"
						class="btn" value="Save" id="submit" name="submit">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
				<div id="step-1" class="content" style="display: block;">
					<h2 class="StepTitle">Contact Meeting Information</h2>
					<div class="panelCenter_1">
					<div style="margin: 2% 0;">
					<label>Have you met with a BP Supplier Diversity personnel recently?</label>
							 <label for="metYes">Yes</label>&nbsp;<input type="radio" value="1" name="metBpSupplier" id="metYes">
							 <label for="metNo">No</label>&nbsp;<input type="radio" value="0" name="metBpSupplier" id="metNo">
					</div>
						<div class="form-box" id="meetingInfo" style="display: none;">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Contact First Name</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contactFirstName" alt=""
											styleId="contactFirstName" styleClass="text-box" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Contact Last Name</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contactLastName" alt=""
											styleId="contactLastName" styleClass="text-box" />
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Contact Date</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contactDate" alt="Please click to select date" styleId="contactDate"
											styleClass="text-box" readonly="true" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Contact State</div>
									<div class="ctrl-col-wrapper">
										<html:select property="contactState" styleId="contactState"
											styleClass="chosen-select-width">
											<html:option value="">- Select -</html:option>
											<logic:present name="contactStates">
												<logic:iterate id="states" name="contactStates">
													<bean:define id="name" name="states" property="id"></bean:define>
													<html:option value="<%=name.toString()%>">
														<bean:write name="states" property="statename" />
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select>
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Contact Event</div>
									<div class="ctrl-col-wrapper">
										<html:textarea property="contactEvent" alt=""
											styleId="contactEvent" styleClass="main-text-area" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
					<input type="submit" class="btn" value="Next" id="submit3" name="submit3">
					<input type="button" class="btn" value="Back"
						onclick="showDocumentsPage();"> <input type="submit"
						class="btn" value="Save" id="submit2" name="submit2">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</html:form>
<script type="text/javascript">
$(document).ready(function() {
	$("#menu12").removeClass().addClass("current");
});
	$('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		$("#meetingForm").validate({
			rules : {
				contactFirstName : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactLastName : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactState : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactDate : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactEvent : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					$('#meetingForm').append("<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
					$('#meetingForm').append("<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#meetingForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
</script>