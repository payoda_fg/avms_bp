<%@page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.form.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">

	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'picsCertificateSubmit')
				{
					e.preventDefault();
					$('#picsCertificateSubmit').click();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}					
		}
	});

	function showdiverseClassification() {
		window.location = "selfvendornavigation.do?parameter=businessAreaNavigation";
	}
	
	$(document).ready(function() {
		
		if ($("input:radio[name=expOilGas]:checked").val() == 1) {
			$('#oilGasExp').css('display', 'block');
		} else {
			$('#oilGasExp').css('display', 'none');
		}
		if ($("input:radio[name=isBpSupplier]:checked").val() == 1) {
			$('#bpsupplier').css('display', 'block');
		} else {
			$('#bpsupplier').css('display', 'none');
		}
		if ($("input:radio[name=isCurrentBpSupplier]:checked").val() == 1) {
			$('#bpFranchise').css('display', 'block');
		} else {
			$('#bpFranchise').css('display', 'none');
		}
		if ($("input:radio[name=isFortune]:checked").val() == 1) {
			$('#fortune').css('display', 'block');
		} else {
			$('#fortune').css('display', 'none');
		}
		$('input[name=expOilGas]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#oilGasExp').css('display', 'block');
			} else {
				$('#oilGasExp').css('display', 'none');
			}
		});
		$('input[name=isBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpsupplier').css('display', 'block');
			} else {
				$('#bpsupplier').css('display', 'none');
			}
		});
		$('input[name=isCurrentBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpFranchise').css('display', 'block');
			} else {
				$('#bpFranchise').css('display', 'none');
			}
		});
		$('input[name=isFortune]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#fortune').css('display', 'block');
			} else {
				$('#fortune').css('display', 'none');
			}
		});
		
	});
	
	function setBusinessValue(ele, value) {
		ele.value = value;
		
		//To Display a Message, When Is your company Avetta certified = Yes
		if(ele.id == 'yesPicsCertified') {			
			$("#PICSCertified").css(
			{
				"display" : "block"
			});		
			
			$("#PICSCertified").dialog(
			{
				width : 700,
				modal : true,
				closeOnEscape: false,
				open: function() {
	                $(this).parent().find(".ui-dialog-titlebar-close").hide();
	            },
				show : 
				{
					effect : "scale",
					duration : 1000
				},
				hide : 
				{
					effect : "scale",
					duration : 1000
				}
			});
		}
	}
	
	function closeDialog() {		
		$("#noPicsCertified").prop("checked", true);
		$("#PICSCertified").dialog("close");
	}
	
	$('body').on('focus', ".picsExpireDate", function() {
		$(this).datepicker();
	});
	
	function validatePicsCertificate() 
	{
		var picsCertificate = $('#picsCertificate').val();
		var expireDate = $('#picsExpireDate').val();
				
		if(picsCertificate == "") {
			alert('Pics Certificate is Mandatory.');
		} else if(expireDate == "") {
			alert('Expire Date is Mandatory.');
		} else if(picsCertificate != "" && expireDate != "") {
			var options = { 
            	url     : "selfregbiography.do?parameter=savePicsCertificate",
	            type    : "POST",
	            dataType: 'json',
	            success:function( data ) {
               		if(data.result == "success") {
						alert("PICS Certificate Saved Successfully.");	
					} else {
						alert("PICS Certificate Not Saved, Due to Error.");
					}					
					$('#picsCertificate').val('');
					$('#picsExpireDate').val('');
					$("#PICSCertified").dialog("close");
                },
            }; 
	        $('#picsCertificateForm').ajaxSubmit(options);
	        return false;
		}
		return false;	
	}

	
	var lastDate = new Date();
	lastDate.setDate(lastDate.getDate());

	$.datepicker.setDefaults({
		changeYear : true,
		defaultDate: lastDate,
		dateFormat : 'mm/dd/yy'
			
	});
	$(document).ready(function() {

	$("#workstartdate").datepicker();
	});
</script>

<div class="clear"></div>
<div id="PICSCertified" title="PICS Certification" style="display: none;">
	<p>Please be Sure to Upload a Copy of Your Avetta Certification.</p>
	<html:form styleId="picsCertificateForm" enctype="multipart/form-data">
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">1.Avetta Certificate</div>
				<div class="ctrl-col-wrapper">					
					<html:file property="picsCertificate" styleId="picsCertificate" styleClass="upload"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">2.Expire Date</div>
				<div class="ctrl-col-wrapper">
					<html:text property="picsExpireDate" styleId="picsExpireDate" readonly="true"
						styleClass="picsExpireDate main-text-box" alt="Please click to select date" style="width:45%;" />
				</div>
			</div>
		</div>
		<div class="wrapper-btn" style="width:55%; margin: 20px 0 0 6%;">
			<html:reset value="Cancel" styleClass="btn" onclick="closeDialog();"/>
			<html:submit value="Upload" styleClass="btn" styleId="picsCertificateSubmit" onclick="return validatePicsCertificate();"/>	
		</div>
	</html:form>
</div>
<div class="clear"></div>

<html:form action="selfregbiography.do?parameter=saveBusinessBiography"
	method="post" styleId="biographyForm">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
			<html:hidden property="id" styleId="vendorId"></html:hidden>
			<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
				<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
				<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
				<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
				<input type="button" class="btn" value="Back" id="previous1" onclick="showdiverseClassification();">
				<input type="submit" class="btn" value="Save" id="submit" name="submit">
				<input type="reset" class="btn" value="Reset" id="reset">
			</div>
				<div id="step-1" class="content">
					<h2 class="StepTitle">Business Biography and Safety</h2>
					<h3>Biography</h3>
						<div class="form-box">
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Do you have current experience
										in the Oil &amp; Gas Industry?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="expOilGas" value="bussines[0]" idName="editVendorMasterForm" 
											tabindex="225" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="expOilGas" value="bussines[1]"
											tabindex="226" idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
									</div>
								</div>
							</div>
									<div class="wrapper-full" id="oilGasExp" style="display: none;">
									
											<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Is your work primarily Onshore or Offshore?</div>
											<div class="ctrl-col-wrapper">
											<html:select property="workPlace" styleId="workPlace"
																	styleClass="chosen-select-width" tabindex="64">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="Onshore">Onshore</html:option>
																	<html:option value="OffShore">OffShore</html:option>
																</html:select>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Please list your clients below</div>
											<div class="ctrl-col-wrapper">
												<html:textarea property="listClients" rows="5" cols="30"
													tabindex="227" styleClass="main-text-area" styleId="listClients"></html:textarea>
											</div>
										</div>
									</div>
					
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Are you a BP Supplier?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isBpSupplier" value="bussines[2]" tabindex="228"
											idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isBpSupplier" value="bussines[3]" tabindex="229"
											idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full" id="bpsupplier" style="display: none;">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">BP Contact Name:</div>
									<div class="ctrl-col-wrapper">
										<html:text property="bpContact" styleId="bpContact"
											styleClass="text-box" tabindex="230"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">BP Division:</div>
									<div class="ctrl-col-wrapper">
										<html:text property="bpDivision" styleId="bpDivision"
											styleClass="text-box" tabindex="231"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">BP Contact Phone:</div>
									<div class="ctrl-col-wrapper">
										<html:text property="bpContactPhone" alt="" styleClass="text-box"
											styleId="bpContactPhone" style="width:45%;" tabindex="232" />
										<div class="label-col-wrapper" style="width: 10%;">Ext</div>
										<html:text property="bpContactPhoneExt" styleId="bpContactPhoneExt" tabindex="233"
											size="3" styleClass="text-box" alt="Optional" style="width:22%;"></html:text>
									</div>
								</div>
								
											<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Are you a 1st Tier or 2nd Tier supplier?</div>
											<div class="ctrl-col-wrapper"> 
											<html:select property="typeBpSupplier" styleId="typeBpSupplier"
																	styleClass="chosen-select-width" tabindex="64">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="1st Tier">1st Tier</html:option>
																	<html:option value="2nd Tier">2nd Tier</html:option>
																</html:select>	
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Please describe the specific work (products or services) you are providing to BP.</div>
											<div class="ctrl-col-wrapper"> 
											<html:textarea property="specificwork" rows="5" cols="30"
													tabindex="227" styleClass="main-text-area" styleId="specificwork"></html:textarea>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Start date of the work </div>
											<div class="ctrl-col-wrapper"> 
											<html:text property="workstartdate"
												alt="Please click to select date"
												styleId="workstartdate" styleClass="text-box"
												readonly="false" />
										</div>
											</div>
											<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Contract value ($)</div>
											<div class="ctrl-col-wrapper"> 
											<html:text property="contractValue"
												alt="enter-contractValue"
												styleId="contractValue" styleClass="text-box" />
										</div>
											</div>
												<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Cities/States/BP site locations where this work for BP takes place?</div>
											<div class="ctrl-col-wrapper">
											<html:textarea property="specificwork" rows="5" cols="30"
													tabindex="227" styleClass="main-text-area" styleId="specificwork"></html:textarea>
										</div>
											</div>
							</div>
						<%-- 	<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Are you a current 1st or 2nd
										Tier Supplier to BP or a BP Franchise?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isCurrentBpSupplier" value="bussines[4]" tabindex="234"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isCurrentBpSupplier" value="bussines[5]" tabindex="235"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full" id="bpFranchise" style="display: none;">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Please describe the scope of
										services. Include contract value, locations serviced, and other
										pertinent information:</div>
									<div class="ctrl-col-wrapper">
										<html:textarea property="currentBpSupplierDesc" rows="5" cols="30"
											styleClass="main-text-area" tabindex="236" styleId="currentBpSupplierDesc"></html:textarea>
									</div>
								</div>
							</div> --%>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Are you currently doing
										business with any Fortune 500 companies?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isFortune" value="bussines[6]" tabindex="237"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)" >Yes</html:radio>
										<html:radio property="isFortune" value="bussines[7]" tabindex="238"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full" id="fortune" style="display: none;">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Please list your Fortune 500
										clients below:</div>
									<div class="ctrl-col-wrapper">
										<html:textarea property="fortuneList" rows="5" cols="30"
											styleClass="main-text-area" tabindex="239" styleId="fortuneList"></html:textarea>
									</div>
								</div>
							</div>
					
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Does your company utilize union
										represented workforce?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isUnionWorkforce" value="bussines[8]" tabindex="240"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isUnionWorkforce" value="bussines[9]" tabindex="241"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
									</div>
								</div>
							</div>
					
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">If yes, does your company also
										utilize non-union represented workforce?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isNonUnionWorkforce" value="bussines[10]" tabindex="242"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isNonUnionWorkforce" value="bussines[11]" tabindex="243"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
									</div>
								</div>
							</div>
						</div>
						<h3>Safety</h3>
						<div class="form-box">
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Is your company Avetta certified?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isPicsCertified" value="bussines[12]" tabindex="244"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)" styleId="yesPicsCertified">Yes</html:radio>
										<html:radio property="isPicsCertified" value="bussines[13]" tabindex="245"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)" styleId="noPicsCertified">No</html:radio>
										<html:radio property="isPicsCertified" value="bussines[14]" tabindex="246"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)" styleId="naPicsCertified">NA</html:radio>
									</div>
								</div>
							</div>
										<div class="wrapper-full">
										<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Is your company ISNetworld certified?</div>
											<div class="ctrl-col-wrapper">
												<html:radio property="isNetworldCertified" value="bussines[36]" tabindex="246"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
												<html:radio property="isNetworldCertified" value="bussines[37]" tabindex="247"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
												<html:radio property="isNetworldCertified" value="bussines[38]" tabindex="248"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
											</div>
										</div>
									</div>							
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Is your company ISO 9000
										certified?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isIso9000Certified" value="bussines[15]" tabindex="246"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isIso9000Certified" value="bussines[16]" tabindex="247"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										<html:radio property="isIso9000Certified" value="bussines[17]" tabindex="248"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Is your company ISO 14001
										certified?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isIso14000Certified" value="bussines[18]" tabindex="249"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isIso14000Certified" value="bussines[19]" tabindex="250"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										<html:radio property="isIso14000Certified" value="bussines[20]" tabindex="251"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Have you received any OSHA
										citations in the last three years?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isOshaRecd" value="bussines[21]" tabindex="252"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isOshaRecd" value="bussines[22]" tabindex="253"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										<html:radio property="isOshaRecd" value="bussines[23]" tabindex="254"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Does your company have an
										Occupational Safety &amp; Health Administration (OSHA) 3-yr average of
										2.0 or less?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isOshaAvg" value="bussines[24]" tabindex="255"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isOshaAvg" value="bussines[25]" tabindex="256"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										, please specify
										<html:text property="isOshaAvgTxt" styleClass="text-box" tabindex="257"></html:text>
										<html:radio property="isOshaAvg" value="bussines[26]" tabindex="258"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Does your company have an
										Experience Modification Rate (EMR) of 1.0 or less?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isEmr" value="bussines[27]" tabindex="259"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isEmr" value="bussines[28]" tabindex="260"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										, please specify
										<html:text property="isEmrTxt" styleClass="text-box" tabindex="261"></html:text>
										<html:radio property="isEmr" value="bussines[29]" tabindex="262"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Will your company agree to BP's
										4 hour onsite safety training and have a 10-hr OSHA card for all
										contractor personnel working at the refinery?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isOshaAgree" value="bussines[30]" tabindex="263"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isOshaAgree" value="bussines[31]" tabindex="264"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										<html:radio property="isOshaAgree" value="bussines[32]" tabindex="265"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Is your company in compliance
										with the Northwest Indiana Business Roundtable substance abuse
										policy?</div>
									<div class="ctrl-col-wrapper">
										<html:radio property="isNib" value="bussines[33]" tabindex="266"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
										<html:radio property="isNib" value="bussines[34]" tabindex="267"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
										<html:radio property="isNib" value="bussines[35]" tabindex="268"
										idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Please provide a snapshot of
										major jobs / projects completed:</div>
									<div class="ctrl-col-wrapper">
										<html:textarea property="jobsDesc" rows="5" cols="30"
											styleClass="main-text-area" tabindex="269" styleId="jobsDesc"></html:textarea>
									</div>
								</div>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Customer / Location</div>
									<div class="ctrl-col-wrapper">
										<html:text property="custLocation" styleId="custLocation"
											styleClass="text-box" tabindex="270"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Customer contact</div>
									<div class="ctrl-col-wrapper">
										<html:text property="custContact" styleId="custContact"
											styleClass="text-box" tabindex="271"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Telephone</div>
									<div class="ctrl-col-wrapper">
										<html:text property="telephone" styleId="telephone"
											styleClass="text-box" tabindex="272"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Type of work</div>
									<div class="ctrl-col-wrapper">
										<html:text property="workType" styleId="workType"
											styleClass="text-box" tabindex="273"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Size $</div>
									<div class="ctrl-col-wrapper">
										<html:text property="size" styleId="size" tabindex="274"
										 styleClass="text-box" onblur="moneyFormatToUS('formattedValue','size');" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="actionBar bottom_actionbar_fix">
						<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
						<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
						<input type="submit" class="btn" value="Next" id="submit3" name="submit3">
						<input type="button" class="btn" value="Back" id="previous1" onclick="showdiverseClassification();">
						<input type="submit" class="btn" value="Save" id="submit2" name="submit2">
						<input type="reset" class="btn" value="Reset" id="reset">
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
</html:form>

<style>
.hasPlaceholder {
	color: #777;
}
</style>

<script type="text/javascript">

jQuery(function() {
	jQuery.support.placeholder = false;
	test = document.createElement('input');
	if('placeholder' in test) jQuery.support.placeholder = true;
});

$(document).ready(function() {
	if(!$.support.placeholder) {
		if($('#bpContactPhoneExt').val() == '' || $('#bpContactPhoneExt').val() == $('#bpContactPhoneExt').attr('alt')){
			$('#bpContactPhoneExt').val($('#bpContactPhoneExt').attr('alt')).addClass('hasPlaceholder');
		}
		$('#bpContactPhoneExt').focus(function () {
			if ($(this).val() == $(this).attr('alt')) {
				$(this).val('').removeClass('hasPlaceholder');
			} 
		}).blur(function () {
			if ( ($(this).val() == '' || $(this).val() == $(this).attr('alt'))) {
				$(this).val($(this).attr('alt')).addClass('hasPlaceholder');
			}
		});
		$('form').submit(function () {
			$(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
		});
	}
	$("#menu8").removeClass().addClass("current");
});
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function($) {
		  $("#telephone").mask("(999) 999-9999?");
		  $("#bpContactPhone").mask("(999) 999-9999?");
	});
	
	jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
        return this.optional(element) || /^[0-9]+$/.test(value);
    },"Please enter only numbers.");
	
	$(function() {
		$("#biographyForm").validate({
			rules : {
				bpContactPhoneExt : {
					onlyNumbers : true
				},
				contractValue : {
					onlyNumbers : true
					},
				listClients	: {
					rangelength : [ 0, 60000 ]
				},
				currentBpSupplierDesc : {
					rangelength : [ 0, 60000 ]
				},
				fortuneList : {
					rangelength : [ 0, 60000 ]
				},
				jobsDesc : {
					rangelength : [ 0, 60000 ]
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					$('#biographyForm').append(
									"<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
					$('#biographyForm').append(
									"<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#biographyForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});	
</script>