<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<style>
<!--
#businessArea{
    display: none;
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index: 999;
    top: 0;
    left: 0;
}
-->
</style>
<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				e.preventDefault();
				$('#submit').click();
			}
		}
	});
		
	$(document).ready(function() {
		$("#previous").prop('disabled', true);
		$("#previous").css('background','none repeat scroll 0 0 #848484');
		$("#previous1").prop('disabled', true);
		$("#previous1").css('background','none repeat scroll 0 0 #848484');
		
		<logic:present name="showPrivacyTerms">
			DisplayPrivacyTerms();
		</logic:present>
	});
	
	function DisplayPrivacyTerms()
	{
		$("#PrivacyAndTerms").css(
		{
			"display" : "block"
		});		
		
		$("#PrivacyAndTerms").dialog(
		{
			width : 800,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
	}
	
	function copyContact() {
		var firstName = $("#firstName").val();
		var lastName = $('#lastName').val();
		var title = $('#designation').val();
		var phone = $('#contactPhone').val();
		var mobile = $('#contactMobile').val();
		var fax = $('#contactFax').val();
		var email = $('#contanctEmail').val();
		if ($("#sameContact").is(':checked')) {
		
			$("#preparerFirstName").val(firstName);
			$('#preparerLastName').val(lastName);
			$('#preparerTitle').val(title);
			$('#preparerPhone').val(phone);
			$('#preparerMobile').val(mobile);
			$('#preparerFax').val(fax);
			$('#preparerEmail').val(email);
			
			$("#preparerFirstName").prop('readonly', true);
			$('#preparerLastName').prop('readonly', true);
			$('#preparerTitle').prop('readonly', true);
			$('#preparerPhone').prop('readonly', true);
			$('#preparerMobile').prop('readonly', true);
			$('#preparerFax').prop('readonly', true);
			$('#preparerEmail').prop('readonly', true);
			
		} else {
			
			$("#preparerFirstName").val('');
			$('#preparerLastName').val('');
			$('#preparerTitle').val('');
			$('#preparerPhone').val('');
			$('#preparerMobile').val('');
			$('#preparerFax').val('');
			$('#preparerEmail').val('');
			
			$("#preparerFirstName").prop('readonly', false);
			$('#preparerLastName').prop('readonly', false);
			$('#preparerTitle').prop('readonly', false);
			$('#preparerPhone').prop('readonly', false);
			$('#preparerMobile').prop('readonly', false);
			$('#preparerFax').prop('readonly', false);
			$('#preparerEmail').prop('readonly', false);
		}
	}
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null) {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2();
				}
			});
		}
	}
</script>
<html:form
	action="selfregcontactinfo.do?parameter=selfRegistrationPage"
	method="post" styleId="contactForm" styleClass="FORM">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
				
			<div class="stepContainer" >
			<html:hidden property="id" styleId="vendorId"></html:hidden>
			<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
				<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
				<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
				<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
				<input type="button" class="btn" value="Back" id="previous">
				<input type="submit" class="btn" value="Save" id="submit" name="submit">
				<input type="reset" class="btn" value="Reset" id="reset">
			</div>
				<div id="step-1" class="content" style="display: block;">
					<h2 class="StepTitle">Contact Information</h2>
					<div class="panelCenter_1" id="loginDisplay">
						<h3>Preparer's Contact Information (person completing this
							form)</h3>
						<div class="form-box">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">First Name</div>
									<div class="ctrl-col-wrapper">
										<html:text property="firstName" alt="" styleId="firstName"
											styleClass="text-box" tabindex="10" />
									</div>
									<span class="error"><html:errors property="firstName"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Last Name</div>
									<div class="ctrl-col-wrapper">
										<html:text property="lastName" alt="" styleId="lastName"
											styleClass="text-box" tabindex="12" />
									</div>
									<span class="error"><html:errors property="lastName"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Title</div>
									<div class="ctrl-col-wrapper">
										<html:text property="designation" alt="" styleId="designation"
											tabindex="13" styleClass="text-box" />
									</div>
									<span class="error"><html:errors property="designation"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Email ID</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contanctEmail" alt=""
											styleId="contanctEmail" onchange="ajaxFn(this,'VE');"
											tabindex="14" readonly="true" styleClass="text-box" />
									</div>
									<span class="error"><html:errors
											property="contanctEmail"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Password</div>
									<div class="ctrl-col-wrapper">
										<html:password property="loginpassword"
											styleId="loginpassword" tabindex="15" styleClass="text-box" />
									</div>
									<span class="error"><html:errors
											property="loginpassword"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Confirm Password</div>
									<div class="ctrl-col-wrapper">
										<html:password property="confirmPassword"
											styleId="confirmPassword" tabindex="16" styleClass="text-box" />
									</div>
									<span class="error"><html:errors
											property="confirmPassword"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Secret Question</div>
									<div class="ctrl-col-wrapper">
										<html:select property="userSecQn" name="editVendorMasterForm"
											styleClass="chosen-select-width" styleId="userSecQn"
											tabindex="17">
											<html:option value="">--Select--</html:option>
											<bean:size id="size" name="secretQnsList" />
											<logic:greaterEqual value="0" name="size">
												<logic:iterate id="secretQn" name="secretQnsList">
													<bean:define id="id" name="secretQn" property="id"></bean:define>
													<html:option value="<%=id.toString()%>">
														<bean:write name="secretQn"
															property="secQuestionParticular"></bean:write>
													</html:option>
												</logic:iterate>
											</logic:greaterEqual>
										</html:select>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Secret Question Answer</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="userSecQnAns"
											styleId="userSecQnAns" alt="" tabindex="18" />
									</div>
									<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Phone</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contactPhone" alt=""
											styleId="contactPhone" styleClass="text-box" tabindex="19" />
									</div>
									<span class="error"><html:errors property="contactPhone"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Mobile</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contactMobile" alt="Optional"
											styleId="contactMobile" styleClass="text-box" tabindex="20" />
									</div>
									<span class="error"><html:errors
											property="contactMobile"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">FAX</div>
									<div class="ctrl-col-wrapper">
										<html:text property="contactFax" alt="" styleId="contactFax"
											styleClass="text-box" tabindex="21" />
									</div>
									<span class="error"><html:errors property="contactFax"></html:errors></span>
								</div>

							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div style="padding-top: 0;">
						 <input type="checkbox" id="sameContact" onclick="copyContact();" style="margin-top: 1%;">
						&nbsp;<label for="sameContact"
							style="font-size: 100%; line-height: 1.3;">Check here if
							the Business Contact is the same as the Preparer</label>
					</div>
					<div class="clear"></div>
					<div class="panelCenter_1" id="businessContact" style="padding-top: 1%;">
						<h3>Business Contact Information</h3>
						<div class="form-box">
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">First Name</div>
									<div class="ctrl-col-wrapper">
										<html:text property="preparerFirstName" alt=""
											styleId="preparerFirstName" styleClass="text-box"
											tabindex="2" />
									</div>
									<span class="error"><html:errors property="firstName"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Last Name</div>
									<div class="ctrl-col-wrapper">
										<html:text property="preparerLastName" alt=""
											styleId="preparerLastName" styleClass="text-box" tabindex="3" />
									</div>
									<span class="error"><html:errors property="lastName"></html:errors></span>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Title</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="preparerTitle"
											styleId="preparerTitle" alt="" tabindex="4" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Phone</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="preparerPhone"
											styleId="preparerPhone" alt="" tabindex="5" />
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Mobile</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="preparerMobile"
											styleId="preparerMobile" alt="Optional" tabindex="6" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">FAX</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="preparerFax"
											styleId="preparerFax" alt="" tabindex="7" />
									</div>
								</div>
							</div>
							<div class="wrapper-half">
								<div class="row-wrapper form-group row">
									<div class="label-col-wrapper">Email ID</div>
									<div class="ctrl-col-wrapper">
										<html:text styleClass="text-box" property="preparerEmail"
											styleId="preparerEmail" alt="" onchange="ajaxFn(this,'VE');"
											tabindex="8" />
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
					<input type="submit" class="btn" value="Next" id="submit3" name="submit3">
					<input type="button" class="btn" value="Back" id="previous1">
					<input type="submit" class="btn" value="Save" id="submit2" name="submit2">
					<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</html:form>

<!-- Dialog Box for Privacy and Terms -->
<div id="PrivacyAndTerms" title="Privacy and Terms" style="display: none; max-height: 300px;">
	<logic:present name="userDetails" property="settings.privacyTerms">
		<bean:define id="privacyTerms" name="userDetails" property="settings.privacyTerms"/>
		<p><bean:write name="privacyTerms"/></p>
	</logic:present>
	<logic:notPresent name="userDetails" property="settings.privacyTerms">
		<p>There is no Privacy and Terms.</p>
	</logic:notPresent>
</div>

<script type="text/javascript">
<!--
$(document).ready(function() {
	$("#menu1").removeClass().addClass("current");
});
$(function($) {
	  $("#contactPhone").mask("(999) 999-9999?");
	  $("#contactFax").mask("(999) 999-9999?");
	  $("#contactMobile").mask("(999) 999-9999?");
	  $("#preparerPhone").mask("(999) 999-9999?");
	  $("#preparerFax").mask("(999) 999-9999?");
	  $("#preparerMobile").mask("(999) 999-9999?");
});
$(function() {
	 jQuery.validator.addMethod("password",function(value, element) {
		return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	$('#submit1').click(function(){
		  $(this).data('clicked', true);
	});
 	$('#submit').click(function(){
		  $(this).data('clicked', true);
	});
 	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
	}, "No Special Characters Allowed.");
	$("#contactForm").validate({
		rules : {
			firstName : {alpha : true, maxlength : 255},
			lastName : {alpha : true, maxlength : 255},
			designation : {maxlength : 255},
			preparerFirstName : {alpha : true,maxlength : 255 },
			preparerLastName : {alpha : true, maxlength : 255},
			preparerTitle : {maxlength : 255},
			contanctEmail : {required : true,email : true},loginpassword : {required : true,password:true},
			confirmPassword : {equalTo : "#loginpassword"},
			userSecQn : {required : true},userSecQnAns : {required : true},
			preparerEmail : {email : true}
		},
		submitHandler : function(form) {
			if($('#preparerEmail').val() === $('#contanctEmail').val()){
				if($('#preparerFirstName').val() != $('#firstName').val() || $('#preparerLastName').val() != $('#lastName').val() 
					|| $('#preparerTitle').val() != $('#designation').val() || $('#preparerPhone').val() != $('#contactPhone').val()){
					alert('Business contact EmailID and Preparer EmailID are same but other particulars are different, so changes in business contact will be not be saved.');
				}
			}
			if($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
				$('#contactForm').append("<input type='hidden' name='submitType' value='submit' />");
				$("#ajaxloader").css('display','block');
				$("#ajaxloader").show();
				form.submit();
			} else if($('#submit').data('clicked') || $('#submit2').data('clicked')){
				alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
				$('#contactForm').append("<input type='hidden' name='submitType' value='save' />");
				$("#ajaxloader").css('display','block');
				$("#ajaxloader").show();
				form.submit();
			} else if ($('#saveandexit1').data('clicked')
					|| $('#saveandexit2').data('clicked')){
				$('#contactForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show();
				form.submit();
			}
		},
		invalidHandler : function(form,	validator) {
			$("#ajaxloader").css('display','none');
			alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
			$('#submit1').data('clicked', false);
			$('#submit').data('clicked', false);
			$('#submit2').data('clicked', false);
			$('#submit3').data('clicked', false);
			$('#saveandexit1').data('clicked', false);
			$('#saveandexit2').data('clicked', false);
		}
	});
});
//-->
</script>