<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<!-- <script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script> -->
<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@page import="java.util.*"%>
<%	
//For Restriction on Number of Files to Upload
int uploadRestriction=0;
if (session.getAttribute("uploadRestriction") != null) 
{
	uploadRestriction =(Integer)session.getAttribute("uploadRestriction");
}
//For Calculate Already Uploaded File Size & Count
double documentSize=0;
int documentCount=0;
if (session.getAttribute("vendorDocuments") != null) 
{
	List<VendorDocuments> documents=(List<VendorDocuments>)session.getAttribute("vendorDocuments");
	 
	for(VendorDocuments vendorDocuments:documents)
	{
		if(vendorDocuments.getDocumentFilesize()!=null)
		{
			documentSize=documentSize+vendorDocuments.getDocumentFilesize();
 		}
		documentCount=documents.size();
	}
}
//For Restriction on Total File Size of Uploaded Files
int documentMaxSize=0;
if(session.getAttribute("documentMaxSize") != null)
{
	documentMaxSize = (Integer) session.getAttribute("documentMaxSize");	
}
%>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				e.preventDefault();
				$('#submit').click();
			}
		}
	});
		
	var index = 0;

	function showOtherCertificates() {
		window.location = "selfvendornavigation.do?parameter=otherCertificateNavigation";
	}
</script>
<html:form action="selfregdocuments.do?parameter=saveDocuments"
	method="post" styleId="documentsForm" enctype="multipart/form-data">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
				<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div class="actionBar top_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1" onclick="return checkFileSize();">
					<input type="submit" class="btn" value="Next" id="submit1" name="submit1" onclick="return checkFileSize();">
					<input type="button" class="btn" value="Back"
						onclick="showOtherCertificates();"> <input type="submit"
						class="btn" value="Save" id="submit" name="submit" onclick="return checkFileSize();">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
				<div id="step-1" class="content" style="display: block;">
					<h2 class="StepTitle">Vendor Documents</h2>
					<p style="background-color: burlywood;"><b>To make your profile more robust, it is highly recommended that you upload any PowerPoint Presentations, Capability Statements, Brochures, etc. in this area.</b></p>
					<table class='main-table' width='100%' border='0' id="vendorDocs"
						style='border: none; border-collapse: collapse; float: none;'>
						<thead>
							<tr>
								<td class='header'>Document Name</td>
								<td class='header'>Document Description</td>								
								<td class='header'>File</td>
								<td class='header'>Size</td>
								<td class='header'>Action</td>
							</tr>
						</thead>
						<tbody>
							<logic:present name="vendorDocuments">
								<bean:size id="size" name="vendorDocuments" />
								<logic:equal value="0" name="size">
									<tr>
										<td>Nothing to display</td>
									</tr>
								</logic:equal>
							</logic:present>
							<logic:present name="vendorDocuments">
								<logic:iterate id="vendorDoc" name="vendorDocuments" indexId="index">
									<tr id="<%=index %>" class="even">
										<td><logic:present property="documentName" name="vendorDoc">
												<bean:define id="docName" property="documentName"
													name="vendorDoc"></bean:define>
												<html:text property="vendorDocName"
													value="<%=docName.toString() %>" styleClass="main-text-box"
													alt="" />
											</logic:present></td>
										<td><logic:present property="documentDescription"
												name="vendorDoc">
												<bean:define id="docDesc" property="documentDescription"
													name="vendorDoc"></bean:define>
												<html:textarea property="vendorDocDesc"
													styleClass="main-text-area" value="<%=docDesc.toString() %>" />
											</logic:present></td>
										<td>
											<div class="fileUpload btn btn-primary">
												<span>Browse</span>
												<html:file property='<%="vendorDoc[" + index+"]"%>'
														styleId='<%="vendorDoc"+index%>' styleClass="file1"></html:file>
												<script>
													$("#vendorDoc" + <%=index%>).on("change", function() {
														document.getElementById("docFileName" + <%=index%>).innerHTML = this.value;
													});
												</script>
											</div> 
												<span id='<%="docFileName" + index%>'></span>
									    </td>										
										<td><logic:present property="documentFilesize"
												name="vendorDoc">
												<bean:define id="docSize" property="documentFilesize"
													name="vendorDoc"></bean:define>
												<html:text property="documentFilesize"
													value="<%=docSize.toString() %>" styleClass="main-text-box"
													alt="" readonly="true" />
											</logic:present></td>										
										<td><logic:present property="id" name="vendorDoc">
												<bean:define id="id" property="id" name="vendorDoc"></bean:define>
												<html:link href="downloadHelper.jsp" paramId="id" paramName="id"
													styleId="downloadFile2" styleClass="downloadFile" target="_blank">Download</html:link>
											/ <html:link action="deletedoc.do?method=deleteVendorDoc"
													paramId="id" paramName="id" styleId="deleteFile2"
													styleClass="del">Delete</html:link>
											</logic:present></td>
									</tr>
									<script type="text/javascript">
										index++;
									</script>
								</logic:iterate>
							</logic:present>
						</tbody>
					</table>
					<div class="clear"></div>
					<INPUT id="cmd1" type="button" value="Add Documents" class="btn"
						onclick="addRowDocs('vendorDocs')" />
					<div class="clear"></div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2" onclick="return checkFileSize();">
					<input type="submit" class="btn" value="Next" id="submit3" name="submit3" onclick="return checkFileSize();">
					<input type="button" class="btn" value="Back"
						onclick="showOtherCertificates();"> <input type="submit"
						class="btn" value="Save" id="submit2" name="submit2" onclick="return checkFileSize();">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</html:form>
<script type="text/javascript">
	$(document).ready(function() {
		$("#menu11").removeClass().addClass("current");
	});
	$('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		$("#documentsForm").validate({
			rules : {
				
			},
			submitHandler : function(form) {
				if (validateSelfVendorDocumentsUpload()) {
					if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
						$('#documentsForm').append(
										"<input type='hidden' name='submitType' value='submit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
						alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
						$('#documentsForm').append(
										"<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#saveandexit1').data('clicked')
							|| $('#saveandexit2').data('clicked')){
						$('#documentsForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}	
				}				
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});

	function addRowDocs(table) 
	{
		var noOfFilesToBeUploaded = <%=uploadRestriction%>;		
		var documentSize = <%= documentSize %>;
		var documentCount = <%= documentCount %>;	
		var documentMaxSizeMB = <%=documentMaxSize%>;
		
		//console.log("doc count : "+documentSize);
		//console.log("files to be uploaded : "+noOfFilesToBeUploaded);
		
		//For Convert Max Size from MB to Bytes.
		var documentMaxSize = documentMaxSizeMB * 1024 * 1024;
		
		if (documentCount < noOfFilesToBeUploaded) 
		{
			if (documentSize <= documentMaxSize) 
			{
				//For Documents Already There
				if(documentCount != 0)
				{
					var rowCount = $("#" + table + " tr").length;
					rowCount -= 1;
					if (rowCount < noOfFilesToBeUploaded) 
					{
						var html = "<tr id='"+rowCount+"' class='even'><td><input type='text' name='vendorDocName' class='main-text-box'></td>";
						html = html	+ "<td><textarea name='vendorDocDesc' class='main-text-area' ></textarea></td>";
						html = html	+ "<td> <div class='fileUpload btn btn-primary'> "
									+ "<span>Browse</span>"
									+" <input type='file' name='vendorDoc["
									+ index + "]' id='vendorDoc" + index++
									+ "'></div><span id='docFileName"+rowCount+"'></span></td></tr>";
						$("#" + table).append(html);
						$("#vendorDoc"+rowCount).on("change",function() {
							document.getElementById("docFileName"+rowCount).innerHTML = this.value;
						});
					} 
					else 
					{
						alert("Only " + noOfFilesToBeUploaded + " files You can upload");
					}	
				}	
				else//For No Documents
				{
					var rowCount = $("#" + table + " tr").length;					
					rowCount -= 2;					
					if (rowCount < noOfFilesToBeUploaded) 
					{
						var html = "<tr id='"+rowCount+"' class='even'><td><input type='text' name='vendorDocName' class='main-text-box'></td>";
						html = html	+ "<td><textarea name='vendorDocDesc' class='main-text-area' ></textarea></td>";
						html = html	+ "<td> <div class='fileUpload btn btn-primary'> "
									+ "<span>Browse</span>"
									+" <input type='file' name='vendorDoc["
									+ index + "]' id='vendorDoc" + index++
									+ "'></div><span id='docFileName"+rowCount+"'></span></td></tr>";
						$("#" + table).append(html);
						$("#vendorDoc"+rowCount).on("change",function() {
							document.getElementById("docFileName"+rowCount).innerHTML = this.value;
						});
					} 
					else 
					{
						alert("Only " + noOfFilesToBeUploaded + " files You can upload");
					}
				}
			} 
			else 
			{
				alert("You have Already Exceeded the Size of " + documentMaxSizeMB + " MB.");
			}
		} 
		else 
		{
			alert("You have already uploaded " + documentCount 
					+ " files you can upload remaining "
					+ (noOfFilesToBeUploaded - documentCount) + " files");
			return false;
		}
	}

	$("#vendorDocs tr td .del").click(function(e) 
	{
		e.preventDefault();
		var row = $(this).closest('tr');
		var rowid = row.attr('id');
		var url = $(this).attr('href');
		var ajaxUrl = url.substring(url.lastIndexOf("/") + 1, url.length);
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) 
		{
			// Output when OK is clicked
			$.ajax({
				url : ajaxUrl,
				type : "POST",
				async : false,
				success : function() 
				{
					$('#' + rowid).remove();
					var count = 0;
					for ( var i = 0; i < index; i++) 
					{
						if (i != rowid) 
						{
							document.getElementById('vendorDoc'
									+ i).name = 'vendorDoc['
									+ count + ']';
							document.getElementById('vendorDoc'
									+ i).id = 'vendorDoc'
									+ count++;
						}
					}
					index--;
					alert('Record Deleted');
				}
			});
			return false;
		} 
		else 
		{
			// Output when Cancel is clicked
			return false;
		}
	});
	
	function checkFileSize() 
	{		
		var fileMaxSizeMB = <%=documentMaxSize%>;
		var documentSize = <%= documentSize %>;
		//For Convert Max Size from MB to Bytes.
		var fileMaxSize = fileMaxSizeMB * 1024 * 1024;		
		
		for(var i=0;i<index;i++)
		{
			var value=document.getElementById('vendorDoc' + i).value;
			
			if(value != "")	
			{				
				var fileSize=0;
				
				fileSize = document.getElementById('vendorDoc' + i).files[0].size;
		        
				documentSize=documentSize + fileSize;
				
				if(fileSize>fileMaxSize)
				{
					alert("Your File Size Must be Below " + fileMaxSizeMB + " MB.");
					document.getElementById('vendorDoc' + i).value='';
					$("#docFileName" + i).html("");
					return false;
				}
				else if(documentSize>fileMaxSize)
				{
					alert("You have Exceeded the Total Size of " + fileMaxSizeMB + " MB.");
					document.getElementById('vendorDoc' + i).value='';
					$("#docFileName" + i).html("");
					return false;
				}
			}
		}
		return true;		
	}
</script>