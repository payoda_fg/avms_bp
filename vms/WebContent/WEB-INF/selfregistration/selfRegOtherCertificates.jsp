<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.fg.vms.util.CommonUtils"%>
<%@page import="com.fg.vms.customer.model.VendorOtherCertificate"%>
<%@ page import="com.fg.vms.customer.model.CustomerVendornetworldCertificate"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				e.preventDefault();
				$('#submit').click();
			}			
		}
	});
		
	function showReferences() {
		window.location = "selfvendornavigation.do?parameter=referencesNavigation";
	}
	$(document).ready(function() {
		if(document.getElementById("otherCert1") != null){
			document.getElementById("otherCert1").onchange = function() {
				document.getElementById("otherFile1").innerHTML = this.value;
			};
		}
		$("#otherCert1").on("change",function() {
			document.getElementById("otherFile1").innerHTML = this.value;
		});

		if(document.getElementById("otherCert2") != null){
			document.getElementById("otherCert2").onchange = function() {
				document.getElementById("otherFile2").innerHTML = this.value;
			};
		}
		
		$("#otherCert2").on("change",function() {
			document.getElementById("otherFile2").innerHTML = this.value;
		});
		
		if($("#otherCertType1").val() === "-1")
			$("#othexpDate1").val('');

		$("#yesnet").hide();
		$("#yesnetworld").click(function() {
			
			$('#nonetworld').attr('checked',false);
		    $("#yesnet").show();
		  });
		$("#nonetworld").click(function() {
			
			$('#yesnetworld').attr('checked',false);
		    $("#yesnet").hide();
		  });
		  
	});
	
	var imgPath = "jquery/images/calendar.gif";
	datePickerOtherExp();
	function checkDiverseType(element) {
		var rowCount = $('#otherCertificate tr').length - 1;
		for ( var i = 1; i <= rowCount; i++) {
			var id = "otherCertType" + i;
			if (id == element) {
			} else {
				var v1 = $('#' + id).val();
				var v2 = $('#' + element).val();
				if (v1 == v2) {
					alert("Certificate should be unique");
					document.getElementById(element).selectedIndex = 0;
				}
			}
		}
	}
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = 6;//table.rows[1].cells.length;
		for ( var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[1].cells[i].innerHTML;
			if (i == 2) {
				newcell.innerHTML = '<div class="fileUpload btn btn-primary"><span>Browse</span>'
						+ '	<input name="otherCert['+(rowCount-1)+']" type="file" id="otherCert'
						+ rowCount
						+ '" class="upload"/> </div> <span id="otherFile'+rowCount+'"></span>';
				$("#otherCert" + rowCount).on("change",	function() {
									document.getElementById("otherFile"
											+ rowCount).innerHTML = this.value;
								});
			}else if(i==0) {
				newcell.innerHTML = '<select name="otherCertType1" id="otherCertType'+ rowCount+'" class="chosen-select fields" >'
				+ $('#otherCertType1').html() + '</select>';
				newcell.childNodes[0].selectedIndex = 0;
				//newcell.childNodes[0].id = "otherCertType" + rowCount;
				$('#otherCertType'+rowCount).select2();
			}else if(i==5){
				newcell.innerHTML = '<a onclick="return clearOtherCertificate(\''+rowCount+'\',\'0\')" href="#">Delete</a>';
			}else if(i==4){
				newcell.innerHTML = '';
			}else if(i==1){
				newcell.innerHTML = " <input class='othexpDate1 main-text-box ' readony='readonly' id='othexpDate"+rowCount+"' alt='Please click to select date' name='otherExpiryDate' style='width:45%;'/>";
				newcell.childNodes[0].value = "";
			}else if(i==3){
				newcell.innerHTML = '';
			}
		}
	}
	$('body').on('focus', ".othexpDate1", function() {
		$(this).datepicker();
	});
</script>
<style>
<!--
.swMain div.actionBar {
	z-index: 0;
}
.swMain .stepContainer .StepTitle {
	z-index: 0;
}
-->
</style>
<html:form action="selfothercertificate.do?parameter=saveOtherCertificates"
	method="post" styleId="otherCertForm" enctype="multipart/form-data">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="netmsg" property="networld" message="true">
					<span><bean:write name="netmsg" /></span>
				</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="netmsg" property="transactionFailure" message="true">
					<span><bean:write name="netmsg" /></span>
				</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
				<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div class="actionBar top_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
					<input type="submit" class="btn" value="Next" id="submit1" name="submit1">
					<input type="button" class="btn" value="Back"
						onclick="showReferences();"> <input type="submit"
						class="btn" value="Save" id="submit" name="submit">
						<input type="reset" class="btn" value="Reset" id="reset">
				</div>
				<div id="step-1" class="content">
					<h2 class="StepTitle">Quality/Other Certifications</h2>
					<table width="100%" border="0" class="main-table" id="otherCertificate">
						<tr>
							<td class="header">Certification Type</td>
							<td class="header">Expire Date</td>
							<td class="header">Certificate</td>
							<td class="header">Is this Avetta certification?</td>
							<td class="header"></td>
							<td class="header">Action</td>
						</tr>
					
						<logic:present name="otherCertificates">
							<bean:size id="size" name="otherCertificates" />
							<logic:equal value="0" name="size">
								<tr>
									<td><html:select styleId="otherCertType1" tabindex="127"
											property="otherCertType1" onchange="checkDiverseType(this.id);"
											styleClass="chosen-select">
											<html:option value="-1">----- Select -----</html:option>
											<logic:present name="qualityCertificates">
												<logic:iterate id="qualityCertificate" name="qualityCertificates">
													<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
													<html:option value="${id}">
														<bean:write name="qualityCertificate"
															property="certificateName"></bean:write>
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select></td>
									<td><html:text property="otherExpiryDate" styleId="othexpDate1" readonly="true"
											styleClass="othexpDate1 main-text-box" alt="Please click to select date" tabindex="128" style="width:45%;" /></td>
									<td>
										<div class="fileUpload btn btn-primary">
											<span>Browse</span>
											<html:file property="otherCert[0]" styleId="otherCert1" tabindex="129"
												styleClass="upload">
											</html:file>
										</div> <span id="otherFile1"></span>
									</td>
									<td align="center">
										<html:checkbox property="isPics" styleId="isPics" disabled="true" value="1"/>
									</td>									
									<td></td>
									<td><a onclick="return clearOtherCertificate('1','0');" href="#">Delete</a></td>
								</tr>
								</logic:equal>
								</logic:present>
								<logic:notPresent name="otherCertificates">
								<tr>
									<td><html:select styleId="otherCertType1" tabindex="127"
											property="otherCertType1" onchange="checkDiverseType(this.id);"
											styleClass="chosen-select">
											<html:option value="-1">----- Select -----</html:option>
											<logic:present name="qualityCertificates">
												<logic:iterate id="qualityCertificate" name="qualityCertificates">
													<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
													<html:option value="${id}">
														<bean:write name="qualityCertificate"
															property="certificateName"></bean:write>
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select></td>
									<td><html:text property="otherExpiryDate" styleId="othexpDate1" readonly="true"
											styleClass="othexpDate1 main-text-box" alt="" tabindex="128" style="width:45%;" /></td>
									<td>
										<div class="fileUpload btn btn-primary">
											<span>Browse</span>
											<html:file property="otherCert[0]" styleId="otherCert1" tabindex="129"
												styleClass="upload">
											</html:file>
										</div> <span id="otherFile1"></span>
									</td>
									<td></td>									
									<td></td>
									<td><a onclick="return clearOtherCertificate('1','0');" href="#">Delete</a></td>
								</tr>
								</logic:notPresent>
						<logic:present name="otherCertificates">
							<logic:iterate id="otherCert" name="otherCertificates" indexId="index">
								<tr>
									<%
										int i = index + 1;
												String certType = "-1";
									%>
									<logic:greaterThan value="0" name="otherCert"
										property="certificationType">
										<bean:define id="type" name="otherCert"
											property="certificationType"></bean:define>
										<%
											certType = type.toString();
										%>
									</logic:greaterThan>
									<td><html:select onchange="checkDiverseType(this.id);"
											styleId='<%="otherCertType" + i%>' property="otherCertType1"
											styleClass="chosen-select" value="<%=certType%>">
											<html:option value="-1">----- Select -----</html:option>
											<logic:present name="qualityCertificates">
												<logic:iterate id="qualityCertificate" name="qualityCertificates">
													<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
													<html:option value="<%=id.toString()%>">
														<bean:write name="qualityCertificate"
															property="certificateName"></bean:write>
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select></td>
									<%
										String date = CommonUtils
														.convertDateToString(((VendorOtherCertificate) otherCert)
																.getExpiryDate());
									%>
									<td><html:text property="otherExpiryDate" styleId='<%="othexpDate" + i%>'
											value="<%=date.toString()%>" readonly="true"
											styleClass="othexpDate1 main-text-box" alt="" style="width:45%;" /></td>
									<td>
										<div class="fileUpload btn btn-primary">
											<span>Browse</span>
											<html:file property='<%="otherCert[" + index+"]"%>' styleId='<%="otherCert" + i%>'
												tabindex="130" styleClass="upload">
											</html:file>
											<script>
											$("#otherCert" + <%=i%>).on("change",
												function() {
													document.getElementById("otherFile"
															+ <%=i%>).innerHTML = this.value;
												});
											</script>
										</div> <span id='<%="otherFile" + i%>'></span>
									</td>
									<td align="center">
										<logic:present name="otherCert" property="isPics">
											<html:checkbox property="isPics" name="otherCert" styleId='isPics' disabled="true" value="1"/>
										</logic:present>										
									</td>									
									<td><logic:present name="otherCert" property="filename">
											<bean:define id="id" property="id" name="otherCert"></bean:define>
											<html:link href="download_othercertificate.jsp" paramId="id"
												 paramName="id" styleClass="downloadFile" styleId='<%="downloadOtherFile" + i%>' target="_blank">Download</html:link>
										</logic:present></td><bean:define id="oID"  property="id" name="otherCert"/>
										<td><a onclick="return clearOtherCertificate('<%=i%>','<%=oID %>');" href="#">Delete</a></td>
								</tr>
							</logic:iterate>
						</logic:present>
					</table>
					<INPUT id="cmd1" type="button" value="Add Quality Certificate"
						class="btn" onclick="addRow('otherCertificate')" />
					<div class="clear"></div>
				</div>
				
											<div class="content">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<div id="networld" class="content">
								<h2 class="StepTitle">Networld certificate</h2>	
								<table width="100%" border="0" class="main-table" id="isnetwotldcerti">
									<tr>
										<td class="header">Discription</td>
										<td class="header">Yes/No</td>	
										<td class="header">Action</td>									
<!-- 										<td class="header">Uploaded Certificate</td> -->
										<td class="header">Uploaded Certificate</td>
										
									</tr>
									
								 <c:set var ="hasnetworld" scope = "session" value ='<%= session.getAttribute("hasnetworldcsrtificate") %>'/>
								<c:choose>
								<c:when test="${hasnetworld == true}">
								 <c:set var ="networldcert" scope = "session" value ='<%= session.getAttribute("networldcertificate") %>'/>
								 <td>
								<h3 style="color: #009900">Do yo want to modify your company ISNetworld certification? </h3>
								</td>
								<td>
								<input name="hasNetWorldCert" type="radio" value="true" id="yesnetworld" onclick="yesNet()"><b>Yes</b>
								<input type="radio" id="nonetworld" onclick="noNet()"><b>No</b>
								</td>
								<td>
								<div id="yesnet">
								<div class="fileUpload btn btn-primary" style="float: left;">
								<span>Modify</span>
								<html:file property="networldCert" styleId="otherCert2"  styleClass="upload"></html:file>
								</div> 
								<span id="otherFile2"></span>	
								</div>	
								</td>
								<td>
								<h4 style="color: #009900"><b>${networldcert}</b></h4>	
								</td>
<!-- 								<td> -->
<!-- 								<a onclick="" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a> -->
<!-- 								</td> -->
								</c:when>
								<c:otherwise>
								<td>
								<h3 style="color: #009900">Does your company have ISNetworld certification? </h3>															
								</td>
								<td>
								<input name="hasNetWorldCert" type="radio" value="true" id="yesnetworld" onclick="yesNet()"><b>Yes</b>
								<input name="hasNetWorldCert" type="radio" value="false" id="nonetworld" onclick="noNet()"><b>No</b>
								</td>
								<td>
								<div id="yesnet">
								<div class="fileUpload btn btn-primary" style="float: left;">
								<span>Browse</span>
								<html:file property="networldCert" styleId="otherCert2"  styleClass="upload"></html:file>
								</div> 
								<span id="otherFile2"></span>	
								</div>	
								</td>
								<td></td>
								</c:otherwise>
								</c:choose>
								</table>
						</div>	
					</div>
				
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
					<input type="submit" class="btn" value="Next" id="submit3" name="submit3">
					<input type="button" class="btn" value="Back"
						onclick="showReferences();"> <input type="submit"
						class="btn" value="Save" id="submit2" name="submit2">
					<input type="reset" class="btn" value="Reset" id="reset">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</html:form>
<script type="text/javascript">
$(document).ready(function() {
	$("#menu10").removeClass().addClass("current");
});
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	$(function() {
		$("#otherCertForm").validate({
			rules : {
				
			},
			submitHandler : function(form) {
				if (validateSelfVendorOtherCertificate()) {
					if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
							$('#otherCertForm').append(
											"<input type='hidden' name='submitType' value='submit' />");
							$("#ajaxloader").css('display', 'block');
							$("#ajaxloader").show();
							form.submit();
					} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
						alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
						$('#otherCertForm').append(
										"<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#saveandexit1').data('clicked')
							|| $('#saveandexit2').data('clicked')){
						$('#otherCertForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}
				} else {
					$("#ajaxloader").hide();
					$('#submit1').data('clicked', false);
					$('#submit').data('clicked', false);
					$('#submit2').data('clicked', false);
					$('#submit3').data('clicked', false);
					$('#saveandexit1').data('clicked', false);
					$('#saveandexit2').data('clicked', false);
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
	
</script>