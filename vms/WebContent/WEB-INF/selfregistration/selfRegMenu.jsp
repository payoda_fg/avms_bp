<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script type="text/javascript">
<!--
	$(document).ready(function() {

		$('body').on('click', '.toggletrigger', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer').slideToggle();
		});
		$('body').on('click', '.toggletrigger1', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer1').slideToggle();
		});
		$('body').on('click', '.toggletrigger2', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer2').slideToggle();
		});

		$('body').bind('click', function(e) {
			// event.preventDefault();   
			$('.togglecontainer').slideUp();
			$('.togglecontainer1').slideUp();
			$('.togglecontainer2').slideUp();
		});
		var status = 0;
		<logic:present name="menuStatus">
			<logic:iterate id="menu" name="menuStatus">
			
				<logic:equal value="1" name="menu" property="tab">
					<logic:equal value="2" name="menu" property="status">
						$("#menu1").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu1").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
						$("#menu1").removeClass().addClass("error");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="2" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu2").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu2").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu2").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
						$("#menu2").removeClass().addClass("error");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="3" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu3").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu3").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu3").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
						$("#menu3").removeClass().addClass("error");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="4" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu4").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu4").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu4").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
						$("#menu4").removeClass().addClass("error");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="5" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu5").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu5").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu5").removeClass().addClass("selected");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="6" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu6").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu6").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu6").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
						$("#menu6").removeClass().addClass("error");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="7" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu7").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu7").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu7").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
					$("#menu7").removeClass().addClass("error");
				</logic:equal>
				</logic:equal>
				
				<logic:equal value="8" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu8").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu8").removeClass().addClass("done");
						status = status + 5;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu8").removeClass().addClass("selected");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="9" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu9").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu9").removeClass().addClass("done");
						status = status + 10;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu9").removeClass().addClass("selected");
					</logic:equal>
					<logic:equal value="3" name="menu" property="status">
						$("#menu9").removeClass().addClass("error");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="10" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu10").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu10").removeClass().addClass("done");
						status = status + 5;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu10").removeClass().addClass("selected");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="11" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu11").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu11").removeClass().addClass("done");
						status = status + 5;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu11").removeClass().addClass("selected");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="12" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu12").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu12").removeClass().addClass("done");
						status = status + 5;
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu12").removeClass().addClass("selected");
					</logic:equal>
				</logic:equal>
				
				<logic:equal value="13" name="menu" property="tab">
					<logic:equal value="0" name="menu" property="status">
						$("#menu13").removeClass().addClass("disabled");
						$('.disabled').bind('click', false);
					</logic:equal>
					<logic:equal value="1" name="menu" property="status">
						$("#menu13").removeClass().addClass("done");
					</logic:equal>
					<logic:equal value="2" name="menu" property="status">
						$("#menu13").removeClass().addClass("selected");
					</logic:equal>
				</logic:equal>
			</logic:iterate>
		</logic:present>
		
		/* $('#progressbar').animate(
			    {width:''+status+'%'}, 
			    {
			        duration:2000,
			        step: function(now, fx) {
			            var data= Math.round(now);
			            $(this).html(data + '% Completed');
			        }
			    }        
			); */
	});
	
	/* $(function(){
	    $("#reset").click(function(){
	    	$(".chosen-select-width").select2('data', null); // clear out values selected
	    });
	}); */
//-->
</script>
<style>
<!--
.form-box {
	border: 1px solid #009900;
}
.btn {
	float: right;
}
#progressbar { 
	height:20px; 
	background: #f9c667; /* Old browsers */
	background: -moz-linear-gradient(top, #f9c667 0%, #f79621 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9c667), color-stop(100%,#f79621)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #f9c667 0%,#f79621 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #f9c667 0%,#f79621 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #f9c667 0%,#f79621 100%); /* IE10+ */
	background: linear-gradient(to bottom, #f9c667 0%,#f79621 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 ); /* IE6-9 */
	width:0; 
	float: left;
	text-align:center; 
	/*color: #000;*/
	padding: 0;
	clear:both;
	margin: 0 0 1% 0;
	behavior: url(PIE.htc);
	-webkit-border-radius: 15px;
	-moz-border-radius: 15px;
	border-radius: 15px;
	position: relative;
}
.clearfix{
clear: both;
}
-->
</style>
<div id="header_title">
	<header class="card-header">
		<h2 class="card-title text-left">Supplier Registration</h2>
		</header>
</div>
<!-- <div id="progressbar"></div> -->
<div class="clearfix"></div>
<ul class="anchor">
	<li><html:link
			action="/selfvendornavigation.do?parameter=contactNavigation"
			styleId="menu1" styleClass="done">
			<label class="stepNumber"><img src="images/contact_info.png"></label>
			<span class="stepDesc"><small>Contact Information</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=companyInfoNavigation"
			styleId="menu2">
			<label class="stepNumber"><img src="images/company_info.png"></label>
			<span class="stepDesc"><small> Company Information</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=companyAddressNavigation"
			styleId="menu3">
			<label class="stepNumber"><img src="images/address.png"></label>
			<span class="stepDesc"><small>Company Address Info</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=companyOwnerNavigation"
			styleId="menu4">
			<label class="stepNumber"><img src="images/ownership.png"></label>
			<span class="stepDesc"><small>Company Ownership</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=diverseClassificationNavigation"
			styleId="menu5">
			<label class="stepNumber"><img
				src="images/classification.png"></label>
			<span class="stepDesc"><small> Diverse Classifications
			</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=serviceAreaNavigation"
			styleId="menu6">
			<label class="stepNumber"><img src="images/service_icon.png"></label>
			<span class="stepDesc"><small> Service Area</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=businessAreaNavigation"
			styleId="menu7">
			<label class="stepNumber"><img src="images/business.png"></label>
			<span class="stepDesc"><small> Business Area</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=biographyNavigation"
			styleId="menu8">
			<label class="stepNumber"><img src="images/biography.png"></label>
			<span class="stepDesc"><small>Business Biography and
					Safety </small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=referencesNavigation"
			styleId="menu9">
			<label class="stepNumber"><img src="images/reference.png"></label>
			<span class="stepDesc"><small>Reference</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=otherCertificateNavigation"
			styleId="menu10">
			<label class="stepNumber"><img src="images/quality.png"></label>
			<span class="stepDesc"><small>Quality and Other
					Certifications</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=documentsNavigation"
			styleId="menu11">
			<label class="stepNumber"><img src="images/upload.png"></label>
			<span class="stepDesc"><small>Documents Upload</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=contactMeetingNavigation"
			styleId="menu12">
			<label class="stepNumber"><img src="images/submit.png"></label>
			<span class="stepDesc"><small>Contact Meeting
					Information</small> </span>
		</html:link></li>
	<li><html:link
			action="/selfvendornavigation.do?parameter=submitNavigation"
			styleId="menu13">
			<label class="stepNumber"><img src="images/meeting_info.png"></label>
			<span class="stepDesc"><small>Submit</small> </span>
		</html:link></li>
</ul>
