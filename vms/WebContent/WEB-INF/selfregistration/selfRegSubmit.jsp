<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript">
	function showMeetingPage() {
		window.location = "selfvendornavigation.do?parameter=contactMeetingNavigation";
	}
	$(document).ready(function() {
		
		$("#submit1").css('background','none repeat scroll 0 0 #848484');
		$("#submit1").prop('disabled',true);
		
		$("#submit").css('background','none repeat scroll 0 0 #848484');
		$("#submit").prop('disabled',true);
		
		$('#authentication').click(function(){
			if ($(this).is(":checked")){
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
				$("#submit").css('background','none repeat scroll 0 0 #009900');
				$("#submit").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
				$("#submit").css('background','none repeat scroll 0 0 #848484');
				$("#submit").prop('disabled',true);
			}
		});
	});
</script>
<html:form action="selfregsubmit.do?parameter=submitSelfRegistration"
	method="post" styleId="submitForm">
	<div id="successMsg">
		<html:messages id="msg" property="vendor" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
		<html:messages id="msg" property="transactionFailure" message="true">
			<span><bean:write name="msg" /></span>
		</html:messages>
	</div>
	<div id="content-area">
		<div id="wizard" class="swMain">
			<jsp:include page="selfRegMenu.jsp"></jsp:include>
			<div class="stepContainer">
				<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
				<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
				<input type="submit" class="btn" value="Submit" id="submit1" name="submit1">
				<input type="button" class="btn" value="Back"
					onclick="showMeetingPage();">
			</div>
				<div id="step-1" class="content">
					<h2 class="StepTitle">Privacy and Terms</h2>
					<logic:present name="userDetails" property="settings.privacyTerms">
						<bean:define id="privacyTerms" name="userDetails" property="settings.privacyTerms"/>
						<p><bean:write name="privacyTerms"/></p>
					</logic:present>
					<br/>
					
					<h2 class="StepTitle">Submit Registration</h2>
					<logic:present name="editVendorMasterForm" property="vendorErrors">
						<div style="color: red;">
							<h3>You have missed few mandatory fields, Please do the following and come back.</h3><br>
						</div>
						<logic:iterate id="errors" name="editVendorMasterForm" property="vendorErrors">
							<span class="error"><bean:write name="errors"/></span><br>
						</logic:iterate>
					</logic:present>
					<div id="auth" style="padding-top: 5%;">
						<input type="checkbox" id="authentication" tabindex="112" >
						&nbsp;
							<bean:define id="TC" property="termsCondition" name="editVendorMasterForm"/>
							<label for="authentication">
							<b>
								<bean:write name="TC"/>
							</b>
							</label>
					</div>
				</div>
				<div class="actionBar bottom_actionbar_fix">
					<html:link action="/selflogout.do?method=selfRegLogout" styleClass="btn">Exit</html:link>
					<input type="submit" class="btn" value="Submit" id="submit" name="submit">
					<input type="button" class="btn" value="Back"
						onclick="showMeetingPage();">
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</html:form>
<script type="text/javascript">
$(document).ready(function() {
	$("#menu13").removeClass().addClass("current");
});
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function() {
		$("#submitForm").validate({
			rules : {
				authentication : {
					required : true
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit').data('clicked')) {
					$('#submitForm').append(
									"<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} 
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
			}
		});
	});
</script>