<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
.header .toggle-sidebar-left {
	display: none;
}
</style>
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}

	$(document)
			.ready(
					function() {
						var orangeMenuCount = $("#orange").find("img").length;
						var yellowMenuCount = $("#yellow").find("img").length;
						var greenMenuCount = $("#green").find("img").length;
						$("#orange").find("img").each(
								function(index) {
									if (index > 3) {
										$(this).parent().parent().css(
												'display', 'none');
									}
								});
						$("#yellow")
								.find("img")
								.each(
										function(index) {
											var isTilePresent = false;
											<logic:iterate id="privilege" name="privileges">
											<logic:equal value="Mail Notifications" name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
											<logic:iterate id="privilege" name="privileges">
											<logic:equal value="Assessment Email Notification" name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
											if (index > 3) {
												$(this).parent().parent().css(
														'display', 'none');
											}
											isTilePresent = true;
											</logic:equal>
											<logic:equal value="0" name="privilege" property="visible">
											if (index > 5) {
												$(this).parent().parent().css(
														'display', 'none');
											}
											isTilePresent = true;
											</logic:equal>
											</logic:equal>
											</logic:iterate>
											</logic:equal>
											</logic:equal>
											</logic:iterate>
											if (!isTilePresent) {
												<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Assessment Email Notification" name="privilege" property="objectId.objectName">
												<logic:equal value="1" name="privilege" property="visible">
												if (index > 4) {
													$(this).parent().parent()
															.css('display',
																	'none');
												}
												</logic:equal>
												<logic:equal value="0" name="privilege" property="visible">
												if (index > 3) {
													$(this).parent().parent()
															.css('display',
																	'none');
												}
												</logic:equal>
												</logic:equal>
												</logic:iterate>
											}
										});
						$("#green").find("img").each(
								function(index) {
									if (index > 3) {
										$(this).parent().parent().css(
												'display', 'none');
									}
								});
						if (orangeMenuCount < 5) {
							$("#orangeMore").css('display', 'none');
						}
						if (greenMenuCount < 5) {
							$("#greenID").css('display', 'none');
						}

						var isMorePresent = false;
						<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Mail Notifications" name="privilege" property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
						<logic:iterate id="privilege" name="privileges">
						<logic:equal value="Assessment Email Notification" name="privilege" property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
						if (yellowMenuCount < 8) {
							$("#yellowMore").css('display', 'none');
						}
						isMorePresent = true;
						</logic:equal>
						<logic:equal value="0" name="privilege" property="visible">
						if (yellowMenuCount < 7) {
							$("#yellowMore").css('display', 'none');
						}
						isMorePresent = true;
						</logic:equal>
						</logic:equal>
						</logic:iterate>
						</logic:equal>
						</logic:equal>
						</logic:iterate>
						if (!isMorePresent) {
							<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Assessment Email Notification" name="privilege" property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
							if (yellowMenuCount < 6) {
								$("#yellowMore").css('display', 'none');
							}
							isMorePresent = true;
							</logic:equal>
							<logic:equal value="0" name="privilege" property="visible">
							if (yellowMenuCount < 5) {
								$("#yellowMore").css('display', 'none');
							}
							isMorePresent = true;
							</logic:equal>
							</logic:equal>
							</logic:iterate>
							if (!isMorePresent) {
								<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Mail Notifications" name="privilege" property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
								if (yellowMenuCount < 5) {
									$("#yellowMore").css('display', 'none');
								}
								</logic:equal>
								</logic:equal>
								</logic:iterate>
							}
						}

						//Display White Space Text/Verbiage for Empty Space When There is Only 1 Menu in the Home Page.
						if (yellowMenuCount > 0 && greenMenuCount == 0
								&& orangeMenuCount == 0) {
							$("#white-space-text").show();
						} else if (yellowMenuCount == 0 && greenMenuCount == 0
								&& orangeMenuCount == 0) {
							$("#white-space-text").show();
							$("#white-space-text").css("maxWidth", "100%");
						}
					});
//-->
</script>

<html:messages id="msg" property="rfip" message="true">
	<script type="text/javascript">
		alert("Successfully saved  ");
	</script>
</html:messages>
<article>
	<!--Content Area Pane Start Here-->
	<section role="main" class="content-body">
	<div class="page-wrapper">		
		<header class="page-header">
			<h2>Home</h2>
		</header>
		
			<div id="content-area">
				<div class="home-widgets">
					<div class="row">
						<div class="col-lg-4 col-xl-4">
							<logic:equal value="true" name="haveVendorSection">
								<div class="font-weight-semibold text-dark text-uppercase mb-3 mt-3">Vendors</div>
							</logic:equal>
							<div class="card card-featured-left card-featured-tertiary mb-4">
								<div class="card-body" id="yellow">
									<div class="widget-summary">
										<div class="widget-summary-col">
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Create Vendor" name="privilege"
													property="objectId.objectName">
													<div class="widget-summary">
														<div class="widget-summary-col widget-summary-col-icon">
															<div class="summary-icon">
																<img src="bpimages/vendor.png" />
															</div>
														</div>
														<div class="widget-summary-col">
															<div class="summary">
																<a href="#menu1" class="icon-1" id="menuselect0"
																	onclick="linkPage('viewVendor.do?method=primeVendor')">
																	<span class="card-title">Create Vendor</span>
																</a>
															</div>
														</div>
													</div>
													<div class="summary-footer"></div>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Supplier Diversity Request Form"
								name="privilege" property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">

									<c:choose>
										<c:when test="${isDisplaySDF == '1'}">
											<a href="#" class="" id="menuselect2"
												onclick="linkPage('supplierDiversity.do?method=supplierDivercityForm')"> <i
												class="icon-img"><img
													src="bpimages/prime-vendor-search.png" /> </i> 
												<span
												class="icon-text">Supplier Diversity Request Form</span>
													</a>
										</c:when>
									</c:choose>
								</logic:equal>
													<div class="summary-footer"></div>
												</logic:equal>
											</logic:iterate>
											
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="View Vendors" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<div class="widget-summary">
															<div class="widget-summary-col widget-summary-col-icon">
																<div class="summary-icon">
																	<img src="bpimages/search_vendor.png" />
																</div>
															</div>
															<div class="widget-summary-col">
																<div class="summary">
																	<a href="#" class="" id="menuselect2"
																		onclick="linkPage('viewVendorsStatus.do?method=showVendorSearch&searchType=V')">
																		<span class="card-title">Search Vendor All
																			Statuses</span>
																	</a>
																</div>
															</div>
														</div>
													</logic:equal>
													<div class="summary-footer"></div>
												</logic:equal>
											</logic:iterate>
											<%-- <logic:iterate id="privilege" name="privileges">
							<logic:equal value="Search Vendor" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu3" class="toggletrigger3 submenuheader"
										id="menuselect3"><span class="icon-text">Search for a Vendor</span> 
										<span class="icon-img"><img
											src="bpimages/search_vendor.png" /> </span> <span class="icon-img"><img
											src="bpimages/sub-plus.png" /> </span></a>
								</logic:equal>
							</logic:equal>
						</logic:iterate> --%>
											<!-- <div class="togglecontainer3 submenu" id="menu3"> -->
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Search Vendor" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<div class="widget-summary">
															<div class="widget-summary-col widget-summary-col-icon">
																<div class="summary-icon">
																	<img src="bpimages/search-for-a-vendor.png" />
																</div>
															</div>
															<div class="widget-summary-col">
																<div class="summary">
																	<a href="#" class="" id="menuselect2"
																		onclick="linkPage('viewVendors.do?method=showVendorSearch&searchType=C')">
																		<!-- <span class="icon-text active">Vendor Profile Criteria</span> -->
																		<span class="card-title">Search for a Vendor</span>
																	</a>
																</div>
															</div>
														</div>
													</logic:equal>
													<div class="summary-footer"></div>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Keyword Search" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a href="#" class="" id="menuselect2"
															onclick="linkPage('viewVendors.do?method=showVendorFullSearch')">
															<span class="icon-text">Keyword Search</span> <span
															class="icon-img"><img
																src="bpimages/keyword-search.png" /></span>
														</a>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<!-- </div> -->
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Prime Vendor Search" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a href="#" class="" id="menuselect2"
															onclick="linkPage('viewVendors.do?method=showPrimeVendorSearch&searchType=P')"><span
															class="icon-text">Prime Vendor Search</span> <span
															class="icon-img"><img
																src="bpimages/prime-vendor-search.png" /> </span> </a>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Inactive Vendor Search" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a href="#" class=""
															onclick="linkPage('viewVendors.do?method=showVendorStatusBySearch')"><span
															class="icon-text">Inactive Vendor Search</span> <span
															class="icon-img"><img
																src="bpimages/inactive-vendor-search.png" /> </span> </a>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Assessment Review" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a href="#menu4" class="toggletrigger4 submenuheader"
															id="menuselect4"><span class="icon-text">Review
																Assessments</span> <span class="icon-img"><img
																src="bpimages/review_assessment.png" /> </span> <span
															class="icon-img"><img src="bpimages/sub-plus.png" />
														</span> </a>
													</logic:equal>
												</logic:equal>
											</logic:iterate>

											<div class="togglecontainer4 submenu" id="menu4">
												<logic:iterate id="privilege" name="privileges">
													<logic:equal value="Assessment Review" name="privilege"
														property="objectId.objectName">
														<logic:equal value="1" name="privilege" property="visible">
															<html:link
																action="/assessmentreview.do?method=reviewTemplate"
																styleClass="btn-gray sub-icon-5 sub">
																<span class="icon-text active">Capability
																	Assessment</span>
																<span class="icon-img"><img
																	src="bpimages/capability_assessment.png" /> </span>
															</html:link>
														</logic:equal>
													</logic:equal>
												</logic:iterate>
												<logic:iterate id="privilege" name="privileges">
													<logic:equal value="Assessment Score Card" name="privilege"
														property="objectId.objectName">
														<logic:equal value="1" name="privilege" property="visible">
															<html:link
																action="/assessmentscore.do?method=viewscore&getTemplateForScore=0"
																styleClass="btn-gray">
																<span class="icon-text active">Assessment
																	Scorecard</span>
																<span class="icon-img"><img
																	src="bpimages/assessmentscorecard.png" /> </span>
															</html:link>
														</logic:equal>
													</logic:equal>
												</logic:iterate>
												<logic:iterate id="privilege" name="privileges">
													<logic:equal value="Assessment Email Details"
														name="privilege" property="objectId.objectName">
														<logic:equal value="1" name="privilege" property="visible">
															<html:link
																action="/assessmentemaildetails.do?method=viewemaildetails&getTemplate=0"
																styleClass="btn-gray">
																<span class="icon-text active">View Assessment
																	Emails</span>
																<span class="icon-img"><img
																	src="bpimages/assessmentemail.png" /> </span>
															</html:link>
														</logic:equal>
													</logic:equal>
												</logic:iterate>
											</div>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Inactive Vendor Search" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a href="#" class="" id="menuselect2"
															onclick="linkPage('viewVendors.do?method=viewTier2VendorSpendReport')"><span
															class="icon-text">Search Tier2 Vendors Report</span> <span
															class="icon-img"><img
																src="bpimages/prime-vendor-search.png" /> </span> </a>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Mail Notifications" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu2" class="toggletrigger2 submenuheader"
										id="menuselect1"> <i class="icon-img"><img
											src="bpimages/email.png" /> </i> <i class="icon-img"><img
											src="bpimages/sub-plus.png" /> </i>
											<span class="icon-text">Email
											Vendor</span></a>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
						<div class="togglecontainer2 submenu" id="menu2">
							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Mail Notifications" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<html:link action="/searchvendor.do?method=searchVendor"
											styleClass="btn-gray sub-icon-1 sub ">
											<i class="icon-img"><img src="bpimages/general.png" />
											</i>											
											<span class="icon-text active">General</span>
										</html:link>
									</logic:equal>
								</logic:equal>
							</logic:iterate>
							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Assessment Email Notification"
									name="privilege" property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<html:link
											action="/capabalityAssessmentSearchvendor.do?method=searchAssessmentVendor"
											styleClass="btn-gray sub-icon-2 sub ">
											<i class="icon-img"><img
												src="bpimages/assessment.png" /> </i>												
											<span class="icon-text active">Assessment</span>
										</html:link>
									</logic:equal>
								</logic:equal>
							</logic:iterate>
						</div>
									<%-- 	<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Inactive Vendor Search" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a href="#" class="" id="menuselect2"
															onclick="linkPage('supplierDiversity.do?method=supplierDivercityForm')"><span
															class="icon-text">Supplier Diversity Request Form</span>
															<span class="icon-img"><img
																src="bpimages/prime-vendor-search.png" /> </span> </a>
													</logic:equal>
												</logic:equal>
											</logic:iterate> --%>

											<div class="widget-summary text-center home-widget-more">
												<html:link action="/moreVendor.do?method=moreVendor"
													styleClass="more-btn" styleId="yellowMore">More...</html:link>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-xl-4">
								<logic:equal value="true" name="haveReportSection">
									<div class="font-weight-semibold text-dark text-uppercase mb-3 mt-3">Reports</div>
								</logic:equal>
								<div class="card card-featured-left card-featured-secondary mb-4">
								
								<div class="card-body" id="green">
									<div class="widget-summary">
									<div class="widget-summary-col">
									<logic:iterate id="privilege" name="privileges">
										<logic:equal
											value="Prime Supplier Report by Tier 2 Total Spend"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-secondary">
														<img src="bpimages/supplierbytotalspend.png" />
													</div>
													</div>
													<div class="widget-summary-col">
														<div class="summary">	
															<a href="#" onclick="linkPage('totalSalesReport.do?method=viewTotalSalesReport')">
																<span class="card-title">Prime Supplier Report by Tier 2 Total Spend</span>  
															</a>
														</div>
													</div>
												</div>																							
												<div class="summary-footer"></div>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal
											value="Prime Supplier Report by Tier 2 Direct Spend"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
											<div class="widget-summary">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-secondary">
										<img src="bpimages/supplierbydirectspend.png" />
									</div>
								</div>
								<div class="widget-summary-col">
										<div class="summary">	
												<a href="#" class="icon-9"
													onclick="linkPage('tier2Report.do?method=viewDirectSpendReport')"><span
													class="card-title">Prime Supplier Report by Tier 2
														Direct Spend </span> </a>
										</div>
									 </div>
									</div>
									<div class="summary-footer"></div>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Prime Supplier Report by Tier 2 Indirect Spend"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<div class="widget-summary">
													<div class="widget-summary-col widget-summary-col-icon">
														<div class="summary-icon bg-secondary">
															<img src="bpimages/supplierbyindirectspend.png" /> 
														</div>
													</div>
												<div class="widget-summary-col">
													<div class="summary">
														<a href="#" class="icon-10" onclick="linkPage('tier2Report.do?method=viewIndirectSpendReport')">
															<span class="card-title">Prime Supplier Report by
																Tier 2 Indirect Spend</span> 
														</a>
													</div>
												</div>
										</div>
										<div class="summary-footer"></div>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									
									<logic:iterate id="privilege" name="privileges">
										<logic:equal
											value="Prime Supplier by Tier 2 Diversity Category"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
											<div class="widget-summary">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon bg-secondary">
												<img src="bpimages/supplierbydiversitystatus.png" /> </div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
												<a href="#" class="icon-7"
													onclick="linkPage('diversityReport.do?method=viewDiversityReport')"><span
													class="card-title">Prime Supplier by Tier 2 Diversity
														Category</span>  </a>
													</div>
												</div>
											</div>
											<div class="summary-footer"></div>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Tier 2 Reporting Summary" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-8"
													onclick="linkPage('ethnicityReport.do?method=viewEthnicityBreakdownReport')"><span
													class="icon-text">Tier 2 Reporting Summary</span> <span
													class="icon-img"><img src="bpimages/ethnicity.png" />
												</span> </a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>

									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Spend Data Dashboard Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-11"
													onclick="linkPage('spendReport.do?method=viewSpendDashboard')">
													<span class="icon-text">Spend Data Dashboard Report</span>
													<span class="icon-img"><img
														src="bpimages/spenddatadashbord.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Supplier Custom Search Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('searchReport.do?method=searchVendorReport')">
													<span class="icon-text">Supplier Custom Search
														Report</span> <span class="icon-img"><img
														src="bpimages/tier2spend.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Diversity Analysis" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('diversityAnalysisReport.do?method=viewDiversityAnalysisReport')">
													<span class="icon-text">Diversity Analysis</span> <span
													class="icon-img"><img
														src="bpimages/diversity-analysis.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendor Status Breakdown" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorStatusBreakdownReport.do?method=viewVendorStatusBreakdownReport')">
													<span class="icon-text">Vendor Status Breakdown</span> <span
													class="icon-img"><img
														src="bpimages/vendor-status-breakdown.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Supplier Count By State" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('supplierCountByStateReport.do?method=viewSupplierCountByStateReport')">
													<span class="icon-text">Supplier Count By State</span> <span
													class="icon-img"><img
														src="bpimages/supplier-count-by-state.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="BP Vendor Count By State" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('bpVendorCountByStateReport.do?method=viewBPVendorCountByStateReport')">
													<span class="icon-text">BP Vendor Count By State</span> <span
													class="icon-img"><img
														src="bpimages/bp-vendor-count-by-state.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Spend By Agency" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('spendByAgencyReport.do?method=viewSpendByAgencyReport')">
													<span class="icon-text">Spend By Agency</span> <span
													class="icon-img"><img
														src="bpimages/spend-by-agency.png" /> </span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendor Commodities Not Saved"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorCommoditiesNotSavedReport.do?method=viewVendorCommoditiesNotSavedReport')">
													<span class="icon-text">Vendor Commodities Not Saved</span>
													<span class="icon-img"><img
														src="bpimages/vendor-commodities-not-saved.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Certificate Expiration Notification Email"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('certificateExpirationNotificationEmailReport.do?method=viewCertificateExpirationNotificationEmailReport')">
													<span class="icon-text">Certificate Expiration
														Notification Email</span> <span class="icon-img"><img
														src="bpimages/certificate-expiration-notification-email.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Registered Vendors" name="privilege"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('registeredVendorsReport.do?method=viewRegisteredVendorsReport')">
													<span class="icon-text">Registered Vendors Report</span> <span
													class="icon-img"><img
														src="bpimages/registered-vendors.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendors By Business Type Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorsByIndustryReport.do?method=viewVendorsByIndustryReport')">
													<span class="icon-text">Vendors By Business Type</span> <span
													class="icon-img"><img
														src="bpimages/vendors-by-industry.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendors By NAICS Description Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorsByNaicsReport.do?method=viewVendorsByNaicsReport')">
													<span class="icon-text">Vendors By NAICS Description</span>
													<span class="icon-img"><img
														src="bpimages/vendors-by-naics-description.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendors By BP Market Sector Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorsByBPMarketSectorReport.do?method=viewVendorsByBPMarketSectorReport')">
													<span class="icon-text">Vendors By BP Market Sector</span>
													<span class="icon-img"><img
														src="bpimages/vendors-by-bp-market-sector.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="BP Market Sector Search Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('indirectProcurementCommoditiesReport.do?method=viewIndirectProcurementCommoditiesReport')">
													<span class="icon-text">BP Market Sector Search</span> <span
										class="icon-img"><img
											src="bpimages/indirect-procurement-commodities.png" /></span>
									</a>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="BP Market Sector Search Report"
								name="privilege" property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#" class="icon-3"
										onclick="linkPage('indirectProcurementCommoditiesReport.do?method=viewIndirectProcurementCommoditiesReport')">
										<span class="icon-text">Indirect Procurement Suppliers</span> <span
										class="icon-img"><img
											src="bpimages/indirect-procurement-commodities.png" /></span>
									</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendors By BP Market Subsector Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorsByBPMarketSubsectorReport.do?method=viewVendorsByBPMarketSubsectorReport')">
													<span class="icon-text">Vendors By BP Market
														Subsector</span> <span class="icon-img"><img
														src="bpimages/vendors-by-bp-market-sector.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal value="Vendors By BP Commodity Group Report"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-3"
													onclick="linkPage('vendorsByBPCommodityGroupReport.do?method=viewVendorsByBPCommodityGroupReport')">
													<span class="icon-text">Vendors By BP Commodity
														Group</span> <span class="icon-img"><img
														src="bpimages/vendors-by-bp-market-sector.png" /></span>
												</a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
									<logic:iterate id="privilege" name="privileges">
										<logic:equal
											value="Prime Supplier by Tier 2 Diversity By Ethnicity"
											name="privilege" property="objectId.objectName">
											<logic:equal value="1" name="privilege" property="visible">
												<a href="#" class="icon-7"
													onclick="linkPage('diversityReport.do?method=viewDiversityReport&type=ethnicity')"><span
													class="icon-text">Prime Supplier by Tier 2 Diversity
														By Ethnicity</span> <span class="icon-img"><img
														src="bpimages/supplierbydiversitystatus.png" /> </span> </a>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
								<div class="widget-summary text-center home-widget-more">
									<html:link action="/moreVendor.do?method=moreReport"
									styleClass="more-btn" styleId="greenID">More...</html:link>
								</div>
							</div>
						</div>
					</div>
								
							</div>
						</div>
			

						<div class="col-lg-4 col-xl-4">
							<logic:equal value="true" name="haveAdminSection">
								<div class="font-weight-semibold text-dark text-uppercase mb-3 mt-3">Administration</div>
							</logic:equal>
							<div class="card card-featured-left card-featured-primary mb-4">
								<div class="card-body" id="orange">
									<div class="widget-summary">
										<div class="widget-summary-col">
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Roles" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<div class="widget-summary">
															<div class="widget-summary-col widget-summary-col-icon">
																<div class="summary-icon bg-primary">
																	<img src="bpimages/roles.png">
																</div>
															</div>
															<div class="widget-summary-col">
																<div class="summary">
																	<html:link
																		action="/viewuserrole.do?parameter=viewUserRoles"
																		styleClass="icon-12" styleId="menuselect0">
																		<span class="card-title">Manage Roles</span>
																	</html:link>
																</div>
															</div>
														</div>
														<div class="summary-footer"></div>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="User" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<div class="widget-summary">
															<div class="widget-summary-col widget-summary-col-icon">
																<div class="summary-icon bg-primary">
																	<img src="bpimages/users.png" />
																</div>
															</div>
															<div class="widget-summary-col">
																<div class="summary">
																	<html:link action="/viewuser.do?parameter=viewUsers"
																		styleClass="icon-13" styleId="menuselect1">
																		<span class="card-title">Manage Users</span>

																	</html:link>
																</div>
															</div>
														</div>
														<div class="summary-footer"></div>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Role Privileges" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<div class="widget-summary">
															<div class="widget-summary-col widget-summary-col-icon">
																<div class="summary-icon bg-primary">
																	<img src="bpimages/roleprivileges.png" />
																</div>
															</div>
															<div class="widget-summary-col">
																<div class="summary">
																	<html:link
																		action="/roleprivileges.do?method=rolePrivileges"
																		styleClass="icon-17" styleId="menuselect2">
																		<span class="card-title">Role Privileges</span>
																	</html:link>
																</div>
															</div>
														</div>
														<div class="summary-footer"></div>
													</logic:equal>
												</logic:equal>
											</logic:iterate>


											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="View Certificate" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<div class="widget-summary">
															<div class="widget-summary-col widget-summary-col-icon">
																<div class="summary-icon bg-primary">
																	<img src="bpimages/manageclassifications.png" />
																</div>
															</div>
															<div class="widget-summary-col">
																<div class="summary">
																	<html:link
																		action="/viewcertificate.do?parameter=viewCertificate"
																		styleClass="icon-16" styleId="menuselect5">
																		<span class="card-title">Manage Classifications
																		</span>
																	</html:link>
																</div>
															</div>
														</div>
														<div class="summary-footer"></div>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Certifying Agency" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link action="/viewagencies.do?method=view"
															styleClass="icon-23" styleId="menuselect6">
															<span class="icon-text">Manage Certifying<br>Agencies
															</span>
															<span class="icon-img"><img
																src="bpimages/manage-certifying-agency.png" /></span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Email Distribution" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/emaildistribution.do?method=viewEmailDistribution"
															styleClass="icon-2 ">
															<span class="icon-text">Email Distribution</span>
															<span class="icon-img"><img
																src="bpimages/emaildistribution.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Workflow" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/workflowconfig.do?method=viewWorkflowConfig"
															styleClass="icon-14 ">
															<span class="icon-text">Workflow Configuration</span>
															<span class="icon-img"><img
																src="bpimages/workflowconfiguration.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="NAICS Master" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link action="/naicscode.do?method=viewNaicsCode"
															styleClass="icon-15" styleId="menuselect8">
															<span class="icon-text">NAICS Code</span>
															<span class="icon-img"><img
																src="bpimages/naicscode.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>

											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Reporting Period" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link action="/reportingperiod.do?method=showPage"
															styleClass="icon-27">
															<span class="icon-text">Tier2 Reporting <br>
																Period
															</span>
															<span class="icon-img"><img
																src="bpimages/tier2-reporting-period.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Performance Assessment Template"
													name="privilege" property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link action="/questions.do?method=showPage"
															styleClass="icon-25">
															<span class="icon-text">Performance <br>
																Assessment <br> Template
															</span>
															<span class="icon-img"><img
																src="bpimages/performance-assessment-template.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Ethnicity" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link action="/ethnicity.do?method=view"
															styleClass="icon-29">
															<span class="icon-text">Ethnicity</span>
															<span class="icon-img"><img
																src="bpimages/ethnicity.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Tier 2 Email Notification"
													name="privilege" property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/proactiveMonitoring.do?method=proactiveMonitoring"
															styleClass="icon-26">
															<span class="icon-text">Tier 2 Email<br>
																Notification
															</span>
															<span class="icon-img"><img
																src="bpimages/tier2emailnotification.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Edit Information" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/retrivecustomer?method=retriveCustomerInfoFromCustomerDB"
															styleClass="icon-24">
															<span class="icon-text">Edit Customer Info</span>
															<span class="icon-img"><img
																src="bpimages/edit-customer-info.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<!--  Dashboard move to RolePrivileges, -->
											<%-- <logic:iterate id="privilege" name="privileges">
							<logic:equal value="Dashboard Settings" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<html:link
										action="/dashboardsettings?method=viewDashboardSettings"
										styleClass="dashboard-settings">
										<span class="icon-text">Dashboard Settings</span>
										<span class="icon-img"><img
											src="bpimages/dashboard-settings.png" /> </span>
									</html:link>
								</logic:equal>
							</logic:equal>
						</logic:iterate> --%>
											<%-- <logic:iterate id="privilege" name="privileges">
							<logic:equal value="Market Subsector" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<html:link
										action="/commoditycategory?method=viewCommodityCategory"
										styleClass="commodity-category">
										<span class="icon-text">Market Subsector</span>
										<span class="icon-img"><img
											src="bpimages/commodity-category.png" /> </span>
									</html:link>
								</logic:equal>
							</logic:equal>
						</logic:iterate> --%>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Commodity Group" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/commoditycategory?method=viewCommodity"
															styleClass="commodity">
															<span class="icon-text">Commodity Group</span>
															<span class="icon-img"><img
																src="bpimages/commodity.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Business Area" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/geographicalsettings?method=showGeographicalSettingsPage"
															styleClass="geographical-settings">
															<span class="icon-text">Business Area</span>
															<span class="icon-img"><img
																src="bpimages/geographical-settings.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Email Template" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/emailtemplate?parameter=showEmailTemplatePage"
															styleClass="email-template">
															<span class="icon-text">Email Template</span>
															<span class="icon-img"><img
																src="bpimages/email-template.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Certification Type" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/viewcertificationType?parameter=showCertificatePage"
															styleClass="certification-type">
															<span class="icon-text">Certification Type</span>
															<span class="icon-img"><img
																src="bpimages/certification-type.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Country" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/countrystate?method=showCountryStateSettingsPage"
															styleClass="country">
															<span class="icon-text">Country and State</span>
															<span class="icon-img"><img
																src="bpimages/country.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>

											<logic:equal value="1" name="isDivisionStatus"
												property="isDivision">
												<logic:iterate id="privilege" name="privileges">
													<logic:equal value="Manage Division" name="privilege"
														property="objectId.objectName">
														<logic:equal value="1" name="privilege" property="visible">
															<html:link
																action="/customerDivision?method=showMangeDivisionPage"
																styleClass="manage-division">
																<span class="icon-text">Manage Division</span>
																<span class="icon-img"><img
																	src="bpimages/manage_division.png" /> </span>
															</html:link>
														</logic:equal>
													</logic:equal>
												</logic:iterate>
											</logic:equal>

											<!-- For New Menu-UserloginLog under Admin -->
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="View User Log" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/userLogTrack?parameter=viewUserLoginDetails"
															styleClass="view-log">
															<span class="icon-text">View User Log</span>
															<span class="icon-img"><img
																src="bpimages/view-log.png" /> </span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
<div class="widget-summary text-center home-widget-more">
												<html:link action="/moreVendor.do?method=moreAdmin" styleClass="more-btn" styleId="orangeMore">More...</html:link>
											</div>
											<!--  Credentialing -->
											<logic:iterate id="privilege" name="privileges">
												<logic:equal value="Credentialing" name="privilege"
													property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<html:link
															action="/customeruserregistration?method=viewNotApprovedStakeholders"
															styleClass="credentialing">
															<span class="icon-text">Credentialing</span>
															<span class="icon-img"><img
																src="bpimages/credentialing.png" /></span>
														</html:link>
													</logic:equal>
												</logic:equal>
											</logic:iterate>
											
											<%-- <html:link
					action="/geographicalsettings?method=showGeographicalSettingsPage"
					styleClass="geographical-settings">
					<span class="icon-text">Geographical Settings</span>
					<span class="icon-img"><img
						src="bpimages/geographical-settings.png" /> </span>
				</html:link> --%>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
					<div id="white-space-text">
						<p class="question">What is the AVMS?</p>
						<p class="answer">The AVMS (Advanced Vendor Management System)
							was launched by the BP Supplier Diversity team in 2014 and is a
							centralized database containing all certified diverse suppliers
							who have registered with BP.</p>
						<p class="question">Who uses AVMS?</p>
						<p class="answer">The AVMS is predominantly used by the supply
							chain and procurement teams within BP as a direct "linkage" path
							to identify diverse suppliers for supply chain opportunities.</p>
						<p class="question">What core information does the AVMS tool
							provide?</p>
						<ul class="answerUl" style="list-style-type: none;">
							<li>Business profiles of active and potential diverse-owned
								vendors:</li>
							<li>
								<ul class="answerUl">
									<li>Divided into 12 sections such as Company Ownership,
										Diverse Classifications, Service Area, References.</li>
									<li>Vendor diverse certification details such as agency,
										certificate type, effective/expiration dates.</li>
									<li>Capability statements/brochures/presentations.</li>
								</ul>
							</li>
						</ul>
						<p class="question">Key Features:</p>
						<ul class="answerUl" style="padding-left: 3em;">
							<li>Search by various types of data such as Company Name,
								Commodity, NAICS Code, Keyword, Business area.</li>
							<li>Save customizable search results and export
								functionality.</li>
							<li>Internet accessible via smartphone or tablet.</li>
						</ul>
					</div>
				</div>
			</div>
	
		<!--Content Area Pane End Here-->
	
	</div>
	</section>
</article>


<script>
	$(".submenuheader").click(function() {
		var $curr = $(this);
		$("a").removeClass("active");
		$("div").removeClass("active");
		$(this).addClass("active");
		$curr.parent().prev().addClass("active");
	});

	$(".sub").click(function() {
		$(".sub").removeClass("active");
		$(this).addClass("active");
	});

	$('.btn-bg').each(function() {
		var LiN = $(this).find('a').length;
		if (LiN > 4) {
			$('a', this).eq(3).nextAll().hide().addClass('toggleable');
		}
	});
</script>
