<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@page import="java.util.*"%>
<%	
int uploadRestriction=0;
if (session.getAttribute("uploadRestriction") != null) {
	uploadRestriction =(Integer)session.getAttribute("uploadRestriction");
}

 double documentSize=0;
 int documentCount=0;
 if (session.getAttribute("vendorDocuments") != null) {
	 List<VendorDocuments> documents=(List<VendorDocuments>)session.getAttribute("vendorDocuments");
	 
	 for(VendorDocuments vendorDocuments:documents){
		 if(vendorDocuments.getDocumentFilesize()!=null){
			 documentSize=documentSize+vendorDocuments.getDocumentFilesize();
	 		}
		 documentCount=documents.size();
	 }
	}
 %>
<script type="text/javascript">
	var index = 0;
</script>

<h3>Vendor Documents</h3>
<table class='main-table' width='100%' border='0' id="vendorDocs"
	style='border: none; border-collapse: collapse; float: none;'>
	<thead>
		<tr>
			<td class='header'>Document Name</td>
			<td class='header'>Document Description</td>
			<td class='header'>Size</td>
			<td class='header'>File</td>
			<td class='header'>Action</td>
		</tr>
	</thead>
	<tbody>
		<logic:present name="vendorDocuments">
			<bean:size id="size" name="vendorDocuments" />
			<logic:equal value="0" name="size">
				<tr>
					<td>Nothing to display</td>
				</tr>
			</logic:equal>
		</logic:present>
		<logic:present name="vendorDocuments">
			<logic:iterate id="vendorDoc" name="vendorDocuments" indexId="index">
				<tr id="<%=index %>" class="even">
					<td><logic:present property="documentName" name="vendorDoc">
							<bean:define id="docName" property="documentName"
								name="vendorDoc"></bean:define>
							<html:text property="vendorDocName"
								value="<%=docName.toString() %>" styleClass="main-text-box"
								alt="" />
						</logic:present></td>
					<td><logic:present property="documentDescription"
							name="vendorDoc">
							<bean:define id="docDesc" property="documentDescription"
								name="vendorDoc"></bean:define>
							<html:textarea property="vendorDocDesc"
								styleClass="main-text-area" value="<%=docDesc.toString() %>" />
						</logic:present></td>

					<td><logic:present property="documentFilesize"
							name="vendorDoc">
							<bean:define id="docSize" property="documentFilesize"
								name="vendorDoc"></bean:define>
							<html:text property="documentFilesize"
								value="<%=docSize.toString() %>" styleClass="main-text-box"
								alt="" readonly="true" />
						</logic:present></td>

					<td><html:file property='<%="vendorDoc[" + index+"]"%>'
							styleId='<%="vendorDoc"+index%>' styleClass="file1"></html:file></td>
					<td><logic:present property="id" name="vendorDoc">
							<bean:define id="id" property="id" name="vendorDoc"></bean:define>
							<html:link href="downloadHelper.jsp" paramId="id" paramName="id"
								styleId="downloadFile2" styleClass="downloadFile">Download</html:link>
						/ <html:link action="deletedoc.do?method=deleteVendorDoc"
								paramId="id" paramName="id" styleId="deleteFile2"
								styleClass="del">Delete</html:link>
						</logic:present></td>
				</tr>
				<script type="text/javascript">
					index++;
				</script>
			</logic:iterate>

		</logic:present>
	</tbody>
</table>
<div class="clear"></div>
<INPUT id="cmd1" type="button" value="Add Documents" class="btn"
	onclick="addRowDocs('vendorDocs')" />
<div class="clear"></div>
<script type="text/javascript">
<!--
	function addRowDocs(table) {
		var noOfFilesToBeUploaded = 6;
		var documentSize = <%= documentSize %>;
		var documentCount = <%= documentCount %>;

		// 	console.log("doc count : "+documentSize);
		// 	console.log("files to be uploaded : "+noOfFilesToBeUploaded);

		if (documentCount < noOfFilesToBeUploaded) {

			if (documentSize < 10485760) {
				var rowCount = $("#" + table + " tr").length;
				rowCount -= 1;
				if (rowCount < noOfFilesToBeUploaded) {
					var html = "<tr id='"+rowCount+"' class='even'><td><input type='text' name='vendorDocName' class='main-text-box'></td>";
					html = html
							+ "<td><textarea name='vendorDocDesc' class='main-text-area' ></textarea></td>";
					html = html
							+ "<td><input type='file' name='vendorDoc["
							+ index + "]' id='vendorDoc" + index++
							+ "'></td></tr>";
					$("#" + table).append(html);
				} else {
					alert("Only " + noOfFilesToBeUploaded
							+ " files You can upload");
				}
			} else {
				alert("you have exceed the size of 10 MB");
			}

		} else {
			alert("You have already uploaded " + documentCount
					+ " files you can upload remaining "
					+ (noOfFilesToBeUploaded - documentCount) + " files");
			return false;
		}
	}

	$("#vendorDocs tr td .del")
			.click(
					function(e) {
						e.preventDefault();
						var row = $(this).closest('tr');
						var rowid = row.attr('id');
						var url = $(this).attr('href');
						var ajaxUrl = url.substring(url.lastIndexOf("/") + 1,
								url.length);
						input_box = confirm("Are you sure you want to delete this Record?");
						if (input_box == true) {
							// Output when OK is clicked
							$.ajax({
								url : ajaxUrl,
								type : "POST",
								async : false,
								success : function() {
									$('#' + rowid).remove();
									var count = 0;
									for ( var i = 0; i < index; i++) {
										if (i != rowid) {

											document.getElementById('vendorDoc'
													+ i).name = 'vendorDoc['
													+ count + ']';
											document.getElementById('vendorDoc'
													+ i).id = 'vendorDoc'
													+ count++;
										}
									}
									index--;
									alert('Record Deleted');
								}
							});

							return false;
						} else {
							// Output when Cancel is clicked
							return false;
						}
					});
//-->
</script>