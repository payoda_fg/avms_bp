<%@taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>

<div class="panelCenter_1">
	<h3>Personalize your profile</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Select company logo</div>
				<div class="ctrl-col-wrapper">
					<html:file property="companyLogo" alt="" styleClass="text-box"
						styleId="companyLogo" tabindex="38"/>
				</div>
				<span class="error"><html:errors property="companyLogo"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
