<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<div class="clear"></div>
<div class="panelCenter_1">
	<h3>Reference 1</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceName1" alt="" styleClass="text-box"
						styleId="referenceName1" tabindex="174" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="referenceAddress1"
						styleId="referenceAddress1" alt="" styleClass="main-text-area"
						tabindex="175" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referencePhone1" alt="" tabindex="176"
						styleClass="text-box" styleId="referencePhone1"
						style="width:47%;display:inline-block;" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ext</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension1" styleId="extension1" size="3"
						styleClass="text-box" alt="Optional"
						style="width:22%;display:inline-block;" tabindex="177" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMailId1" alt="" styleClass="text-box"
						styleId="referenceMailId1" tabindex="178" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMobile1" alt="Optional"
						styleClass="text-box" styleId="referenceMobile1" tabindex="179" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceCity1" alt="" styleClass="text-box"
						styleId="referenceCity1" tabindex="180" />
				</div>
				<span class="error"> <html:errors property="referenceCity1"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceZip1" alt="" styleClass="text-box"
						styleId="referenceZip11" tabindex="181" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceCountry1"
						styleId="referenceCountry1" styleClass="chosen-select-width"
						tabindex="182" onchange="changestate(this,'referenceState1');">
						<html:option value="">-- Select --</html:option>
						<logic:present name="countryList">
						<html:optionsCollection name="countryList" value="id" label="countryname"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceState1" styleId="referenceState1"
						styleClass="chosen-select-width" tabindex="183">
						<html:option value="">- Select -</html:option>
					</html:select>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Reference 2</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceName2" alt="Optional"
						styleClass="text-box" styleId="referenceName2" tabindex="184" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="referenceAddress2"
						styleId="referenceAddress2" alt="Optional"
						styleClass="main-text-area" tabindex="185" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referencePhone2" alt=""
						styleClass="text-box" styleId="referencePhone2" style="width:45%;"
						tabindex="186" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ext</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension2" styleId="extension2" size="3"
						styleClass="text-box" alt="Optional" style="width:22%;"
						tabindex="187"></html:text>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMailId2" alt="Optional"
						styleClass="text-box" styleId="referenceMailId2" tabindex="188" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMobile2" alt="Optional"
						styleClass="text-box" styleId="referenceMobile2" tabindex="189" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceCity2" alt="Optional"
						styleClass="text-box" styleId="referenceCity2" tabindex="190" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceZip2" alt="Optional"
						styleClass="text-box" styleId="referenceZip2" tabindex="191" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceCountry2"
						styleId="referenceCountry2" styleClass="chosen-select-width"
						tabindex="192" onchange="changestate(this,'referenceState2');">
						<html:option value="">- Select -</html:option>
						<logic:present name="countryList">
						<html:optionsCollection name="countryList" value="id" label="countryname"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceState2" styleId="referenceState2"
						styleClass="chosen-select-width" tabindex="193">
						<html:option value="">- Select -</html:option>
					</html:select>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Reference 3</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceName3" alt="Optional"
						styleClass="text-box" styleId="referenceName3" tabindex="194" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="referenceAddress3"
						styleId="referenceAddress3" alt="Optional"
						styleClass="main-text-area" tabindex="195" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referencePhone3" alt="" tabindex="196"
						styleClass="text-box" styleId="referencePhone3" style="width:45%;" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ext</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension3" styleId="extension3" size="3"
						styleClass="text-box" alt="Optional" style="width:22%;"
						tabindex="197" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMailId3" alt="Optional"
						styleClass="text-box" styleId="referenceMailId3" tabindex="198" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMobile3" alt="Optional"
						styleClass="text-box" styleId="referenceMobile3" tabindex="199" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceCity3" alt="Optional"
						styleClass="text-box" styleId="referenceCity3" tabindex="200" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceZip3" alt="Optional"
						styleClass="text-box" styleId="referenceZip3" tabindex="201" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceCountry3"
						styleId="referenceCountry3" styleClass="chosen-select-width"
						tabindex="202" onchange="changestate(this,'referenceState3');">
						<html:option value="">-Select-</html:option>
						<logic:present name="countryList">
						<html:optionsCollection name="countryList" value="id" label="countryname"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceState3" styleId="referenceState3"
						styleClass="chosen-select-width" tabindex="203">
						<html:option value="">- Select -</html:option>
					</html:select>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
