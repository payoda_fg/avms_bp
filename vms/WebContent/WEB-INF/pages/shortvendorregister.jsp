<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script>
$(document).ready(function() { 
	//$(".chosen-select").select2();
	$("#countryValue").val($("#country option:selected").text());
});

function closePopup() {
// 	$('#vendorName').val('');
// 	$('#address1').val('');
// 	$('#city').val('');
// 	$('#state').val('');
// 	$('#zipcode').val('');
// 	$('#firstName').val('');
// 	$('#lastName').val('');
// 	$('#contactPhone').val('');
// 	$('#contanctEmail').val('');
// 	$('input [name=certificates]').val('');
// 	var widget = $("#widgetId").val();
// 	$('#'+widget+' option[value=""]').attr('selected', true);
// 	$("#"+widget).select2();
	$('#dialog2').dialog('close');
}

function changestate(ele,id) {
	var country=ele.value;
	if (country != null && country != '') {
		$.ajax({
			url : 'state.do?method=getState&id='+country+'&random='
					+ Math.random(),
			type : "POST",
			async : false,
			success : function(data) {
				$("#"+id).find('option').remove().end().append(data);
				$('#'+id).select2();
			}
		});
	} else {
		$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
		$('#' + id).select2();
	}
}
</script>
<div class="panelCenter_1">
	<h3>Tier2 Vendor</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<bean:message key="comp.name" />
				</div>
				<div class="ctrl-col-wrapper">
					<input type="text" name="vendorName" alt="" id="vendorName"
						class="text-box" tabindex="2" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="address1" alt=""
						id="address1" tabindex="3" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="city" alt=""
						id="city" tabindex="4" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<logic:present name="userDetails" property="workflowConfiguration">
						<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
							<select name="country" id="country" onchange="changestate(this,'state');" tabindex="5" class="chosen-select">
								<option value="">--Select--</option>
								<bean:size id="size" name="countryList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="country" name="countryList">
										<bean:define id="name" name="country" property="id"/>
										<option value="<%=name.toString()%>"><bean:write name="country" property="countryname" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</select>
							<span class="error"><html:errors property="country"></html:errors></span>
						</logic:equal>
						<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
							<input type="text" class="text-box" id="countryValue" readonly="readonly">
							<select name="country" id="country" onchange="changestate(this,'state');" tabindex="5" style="display:none;">
								<bean:size id="size" name="countryList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="country" name="countryList">
										<bean:define id="name" name="country" property="id"/>
										<option value="<%=name.toString()%>"><bean:write name="country" property="countryname" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</select>
						</logic:equal>
					</logic:present>					
				</div>				
			</div>
		</div>
		<div class="wrapper-half">
		<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">					
					<select  class="chosen-select" name="state" id="state" tabindex="6" >
						<option value="">--Select--</option>
						<logic:present name="statesList">
							<bean:size id="size2" name="statesList"/>
							<logic:greaterEqual value="0" name="size2">
								<logic:iterate id="state" name="statesList">
									<bean:define id="name" name="state" property="id"/>
									<option value="<%=name.toString()%>"><bean:write name="state" property="statename" /></option>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>						
					</select>					
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip Code</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="zipcode" alt=""
						id="zipcode" tabindex="6" />
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="panelCenter_1">
	<h3>Business Contact Information</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="firstName" alt=""
						id="firstName" tabindex="7" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="lastName"
						id="lastName" alt="" tabindex="9" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="contactPhone"
						id="contactPhone" alt="" tabindex="10" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" name="contanctEmail"
						id="contanctEmail" alt="" onchange="ajaxFn(this,'VT2E');"
						tabindex="11" />
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
Diverse Supplier
<input type="checkbox" name="deverseSupplier" id="chkDiverse"
	onclick="fn_diverseSup()" tabindex="12" />
<div id="diverseClassificationTab" class="panelCenter_1"
	style="display: none;">
	<h3>Diversity Information</h3>
	<div class="form-box" style="padding-left: 1%; width: 98%;">
		<p style="font-weight: bold;">Diversity/small Business Classifications - check all that apply</p>
		<table style="border-collapse: collapse; border: none;" cellpadding="10">
			<bean:size id="size1" name="certificateTypes" />
			<logic:greaterEqual value="0" name="size1">
				<logic:iterate id="certificate" name="certificateTypes" >
					<bean:define id="id" name="certificate" property="id"></bean:define>
					<tr></tr>
					<tr>
						<td><input type="checkbox" name="certificates"
								value="${id}" class="cert">
						&nbsp;<bean:write name="certificate" property="certificateName"></bean:write>
						</td>
					</tr>
				</logic:iterate>
			</logic:greaterEqual>
		</table>
	</div>
</div>
<div class="clear"></div>