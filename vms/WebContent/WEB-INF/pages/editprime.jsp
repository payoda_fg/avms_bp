<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@page import="java.util.*"%>

<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css" />
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<script>
	function clearfields() {
		// 		window.location = "retrievesupplier.do?method=editPrimeVendor";
	}
	$(document).ready(function() {

		if($("#state option:selected").text() != "- Select -"){
			$("#stateValue").val($("#state option:selected").text());
		}
		
		$("select.chosen-select").select2({
			width : "90%"
		});
		$('.panelCenter_1').panel({
			collapsible : false
		});
// 		$.datepicker.setDefaults({
// 			changeYear : true,
// 			dateFormat : 'mm/dd/yy'
// 		});
// 		$("#contactDate").datepicker();
		$("#countryValue").val($("#country option:selected").text());
	});
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != '') {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2({
						width : "90%"
					});
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
			$('#' + id).select2({
				width : "90%"
			});
		}
	}
	function resetVendorForm() {
		window.location.reload(true);
	}
</script>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">
					<img src="images/icon-registration.png">Update Vendor</h2>
				</header>
			
<div class="clear"></div>
<div id="successMsg">
	<html:messages id="msg" property="vendorupdate" message="true">
		<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
	</html:messages>
	<html:messages id="msg" property="transactionFailure" message="true">
		<div class="alert alert-danger nomargin">><bean:write name="msg" /></div>
	</html:messages>
</div>
<logic:present name="privileges">
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Create Vendor" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="add">

				<html:form enctype="multipart/form-data" styleClass="FORM"
					styleId="editVendorMasterForm"
					action="/updatePrimeVendor?method=updatePrimeVendor">
					<html:javascript formName="vendorMasterForm" />
					
					<div>
						<header class="card-header">
							<h2 class="card-title pull-left"></h2>
						</header>
						<div class="form-box card-body">
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Approved</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<input type="checkbox" name="isApproved" value="1"
											disabled="disabled" checked="checked">
									</div>
								</div>
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">
										<bean:message key="comp.name" />
									</label>
									<div>
										<html:hidden property="id" name="editVendorMasterForm" />
									</div>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text property="vendorName" alt="" styleId="vendorName"
											name="editVendorMasterForm" styleClass="text-box form-control" />
									</div>
									<span class="error"> <html:errors property="vendorName"></html:errors>
									</span>
								</div>
								
							<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">
									Email Address</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="emailId"
											name="editVendorMasterForm" styleId="emailId" alt="" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Address</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:textarea styleClass="main-text-area form-control" property="address1"
											name="editVendorMasterForm" alt="" styleId="address1" />
									</div>
								</div>
							
						
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="city" alt=""
											name="editVendorMasterForm" styleId="city" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<logic:present name="userDetails" property="workflowConfiguration">
											<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
												<html:select property="country" styleId="country" name="editVendorMasterForm" onchange="changestate(this,'state');" styleClass="chosen-select">
													<html:option value="">--Select--</html:option>
													<logic:present name="countryList">
														<html:optionsCollection name="countryList" value="id" label="countryname" />
													</logic:present>
												</html:select>
												<span class="error"> <html:errors property="country"></html:errors> </span>											
											</logic:equal>
											<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
												<input type="text" class="text-box" id="countryValue" readonly="readonly"/>		
												<html:select property="country" styleId="country" style="display:none;" name="editVendorMasterForm" onchange="changestate(this,'state');">
													<logic:present name="countryList">
														<html:optionsCollection name="countryList" value="id" label="countryname" />
													</logic:present>
												</html:select>
											</logic:equal>
										</logic:present>
									</div>									
								</div>
							
							
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
									<div class="col-sm-9 ctrl-col-wrapper">										
										<html:select property="state" styleId="state"
											name="editVendorMasterForm" styleClass="chosen-select-width form-control">
											<html:option value="">--Select--</html:option>
											<logic:present name="editVendorMasterForm" property="state">
												<logic:iterate id="states" name="editVendorMasterForm"
													property="phyStates1">
													<bean:define id="name" name="states" property="id"></bean:define>
													<html:option value="<%=name.toString()%>">
														<bean:write name="states" property="statename" />
													</html:option>
												</logic:iterate>
											</logic:present>
										</html:select>									
									</div>
									<span class="error"> <html:errors property="state"></html:errors></span>
								</div>

								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Zip Code</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="zipcode" alt=""
											name="editVendorMasterForm" styleId="zipcode" />
									</div>
								</div>

							
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone Number</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="phone" alt=""
											name="editVendorMasterForm" styleId="phone" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Fax Number</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="fax" alt=""
											name="editVendorMasterForm" styleId="fax" />
									</div>
								</div>
							
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact Name</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="firstName" alt=""
											name="editVendorMasterForm" styleId="firstName" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="designation"
											name="editVendorMasterForm" styleId="designation" alt="" />
									</div>
								</div>
							
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact Phone</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="contactPhone"
											name="editVendorMasterForm" styleId="contactPhone" alt="" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact Email Address</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="contanctEmail"
											name="editVendorMasterForm" styleId="contanctEmail" alt=""
											onchange="ajaxFn(this,'VE');" />
									</div>
									<span class="error"><html:errors
											property="contanctEmail"></html:errors></span>
								</div>
						
								<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor Status</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:checkbox property="vendorStatus" styleId="vendorStatus" styleClass="styled" value="I">&nbsp;Inactive&nbsp;</html:checkbox>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:radio property="gender" value="M">&nbsp;Male&nbsp;</html:radio>
										<html:radio property="gender" value="F">&nbsp;Female&nbsp;</html:radio>
										<html:radio property="gender" value="T">&nbsp;Transgender&nbsp;</html:radio>
									</div>
								</div>
							
							<div class="clear"></div>
						</div>
					</div>
						<div class="clear"></div>
						<div class="">
							<header class="card-header">
								<h2 class="card-title pull-left">BP Contact Information</h2>
							</header>
							<div class="form-box card-body">
							<div class="row-wrapper form-group row">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">
										Contact First Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="contactFirstName" alt=""
												name="editVendorMasterForm" styleId="contactFirstName"
												readonly="true" styleClass="text-box form-control" />
										</div>
									</div>
									<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact Last Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="contactLastName" alt=""
												name="editVendorMasterForm" styleId="contactLastName"
												readonly="true" styleClass="text-box form-control" />
										</div>
									</div>								
							
									<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact Date</label>
										<div class="cols-m-9 ctrl-col-wrapper">
											<html:text property="contactDate"
												alt="Please click to select date" styleId="contactDate"
												name="editVendorMasterForm"
												styleClass="text-box form-control" readonly="true"/>										
										</div>
									</div>
									<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact State</label>
										<div class="col-sm-9 ctrl-col-wrapper">
										<input type="text" class="text-box" id="stateValue" readonly="readonly">
											<html:select property="contactState" styleId="contactState"
												name="editVendorMasterForm" styleClass="chosen-select-width form-control"  style="display:none;">
												<html:option value="">- Select -</html:option>
												<logic:present name="editVendorMasterForm" property="state">
													<logic:iterate id="states" name="editVendorMasterForm"
														property="phyStates1">
														<bean:define id="name" name="states" property="id"></bean:define>
														<html:option value="<%=name.toString()%>">
															<bean:write name="states" property="statename" />
														</html:option>
													</logic:iterate>
												</logic:present>
											</html:select>
										</div>
									</div>								
								
									<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contact Event</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:textarea property="contactEvent" alt="" readonly="true"
												name="editVendorMasterForm" styleId="contactEvent"
												styleClass="main-text-area form-control" />
										</div>
									</div>								
							</div>
						</div>
					
					<div class="clear"></div>

					<footer class="mt-2">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<html:submit value="Update" styleClass="btn btn-primary" styleId="submit1"></html:submit>
						<html:reset value="Cancel" styleClass="btn btn-default"
							onclick="linkPage('viewVendors.do?method=showPrimeVendorSearch')"></html:reset>
						<html:link action="/viewVendorsStatus.do?method=getPreviousSearchResults&valueFrom=wizard" styleClass="btn btn-default">Back to Results</html:link>
						</div>
					</div>
					</footer>
				</html:form>
			</logic:equal>
			
			<logic:equal value="0" name="privilege" property="add">
				<div class="form-box">
					<h3>You have no rights to add new vendor</h3>
				</div>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
</logic:present>
</div>
</div>
</section>


<script type="text/javascript">
	$(document).ready(function() {
		$("#phone").mask("(999) 999-9999?");
		$("#contactPhone").mask("(999) 999-9999?");
		$("#fax").mask("(999) 999-9999?");
	});
	
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
	}, "No Special Characters Allowed.");
	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	jQuery.validator.addMethod("phoneNumber", function(value, element) {
		return this.optional(element) || /^[0-9-()]+$/.test(value);
	}, "Please enter valid phone number.");
	jQuery.validator
			.addMethod(
					"password",
					function(value, element) {
						return this.optional(element)
								|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/)
										.test(value);
					},
					"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	jQuery.validator.addMethod("allowhyphens", function(value, element) {
		return this.optional(element) || /^[0-9-]+$/.test(value);
	}, "Please enter valid numbers.");
	$("#editVendorMasterForm")
			.validate(
					{
						rules : {
							vendorName : {
								required : true,
								maxlength : 255
							},
							address1 : {
								required : true,
								maxlength : 120
							},
							city : {
								required : true,
								alpha : true,
								maxlength : 60
							},
							country : {
								required : true
							},
							state : {
								required : function(element) {
									if ($("#country option:selected").text() == 'United States') {
										return true;
									} else {
										return false;
									}
								},
								alpha : true,
								maxlength : 60
							},
							zipcode : {
								required : true,
								allowhyphens : true,
								maxlength : 255
							},
							phone : {
								required : true
							},
							emailId : {
								required : true,
								maxlength : 120,
								email : true
							},
							firstName : {
								required : true,
								maxlength : 255,
								alpha : true
							},
							designation : {
								required : true,
								maxlength : 255
							},
							contactPhone : {
								required : true
							},
							contanctEmail : {
								required : true,
								email : true,
								maxlength : 120
							}
						},
					});
</script>