<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html class="fixed has-top-menu dashboard-page no-overflowscrolling">
<head>

<!--  <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css" />-->
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.dashboard.js"></script>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<!-- Vendor CSS -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />

<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<!-- Porto Theme css -->
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />
<style type="text/css">
.header .toggle-sidebar-left {
	display: none;
}
</style>

</head>
<body>
	<logic:present name="currentQuarter">
		<c:set var="quarter" value="${currentQuarter}" />
	</logic:present>
	<section role="main" class="content-body dashboard-wrapper">
		<header class="page-header">
			<h2>Dashboard</h2>
			<div class="right-wrapper text-right pr-4">
				<ol class="breadcrumbs">
					<li><a href="/demo/customerhome.do?method=customerHomePage"> <i class="fa fa-home"></i>
					</a></li>
					<li class="active"><span>Dashboard</span></li>
				</ol>
			</div>
		</header>
		<div>
		 <div class="grid-wrapper">
			<div class="row">
				<div class="col-lg-6">
					<section class="card">
						<header class="card-header">
							<h2 class="card-title">Diversity Analysis </h2>
						</header>

						<div class="card-body">
							<div class="responsiveGrid">
								<table class="table table-bordered table-striped mb-0"
									id="gridtable1">
									<tbody>
										<tr>
											<td />
										</tr>
									</tbody>
								</table>
								<div id="pager1"></div>
							</div>
						<div class="grid-img">
							<jsp:include page="../report/jspdemo/diversity.jsp"></jsp:include>
						</div>
						</div>
					</section>
				</div>

				<div class="col-lg-6">
					<section class="card">
						<header class="card-header">
							<h2 class="card-title">Vendor Status Breakdown</h2>
						</header>
						<div class="card-body">
							<div id="grid_container10">
								<div class="responsiveGrid">
									<table class="table table-bordered table-striped mb-0"
										id="gridtable9">
										<tbody>
											<tr>
												<td />
											</tr>
										</tbody>
									</table>
									<div id="pager9"></div>
								</div>
							</div>
						<div class="grid-img"><jsp:include page="../report/jspdemo/statusbreakdown.jsp"></jsp:include></div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="grid-wrapper">
			<div class="form-box" id="newRegistrationsByMonth">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">New Registrations By Month</h2>
							</header>
							<div class="card-body">
								<div id="grid_container12">
									<div class="responsiveGrid">
										<table class="table table-bordered table-striped mb-0"
											id="gridtable12">
											<tbody>
												<tr>
													<td />
												</tr>
											</tbody>
										</table>
										<div id="pager12"></div>
									</div>
								</div>
							<div class="grid-img"><jsp:include page="../report/jspdemo/newRegistrationsByMonth.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-wrapper">
			<div class="form-box" id="vendorsByIndustry">
			   <div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">Vendors By Business Type</h2>
								</header>
								<div class="card-body">
									<div id="grid_container13">
										<div class="responsiveGrid">
											<table id="gridtable13" class="table table-bordered table-striped mb-0">
												<tbody>
													<tr>
														<td />
													</tr>
												</tbody>
											</table>
											<div id="pager13"></div>
										</div>
									</div>
									<div class="grid-img"><jsp:include page="../report/jspdemo/vendorsByIndustry.jsp"></jsp:include></div>
								</div>
							</section>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-wrapper">
			<div class="form-box" id="bpVendorsByIndustry">
			<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
							<h2 class="card-title">BP Vendors By Business Type</h2>
							</header>
							<div class="card-body">
								<div id="grid_container14">
									<div class="responsiveGrid">
										<table id="gridtable14" class="table table-bordered table-striped mb-0">
											<tbody>
												<tr>
													<td />
												</tr>
											</tbody>
										</table>
										<div id="pager14"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/bpVendorsByIndustry.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>			
			</div>
		</div>
		<div class="grid-wrapper">
			<div class="form-box" id="vendorsByNaicsDescription">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
							<h2 class="card-title">Vendors By NAICS Description</h2>
							</header>
							<div class="card-body">
								<div id="grid_container15">
									<div class="responsiveGrid">
										<table id="gridtable15" class="table table-bordered table-striped mb-0">
											<tbody>
											<tr>
												<td />
											</tr>
											</tbody>
										</table>
										<div id="pager15"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/vendorsByNaicsDescription.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-wrapper">
			<div class="form-box" id="vendorsByBPMarketSector">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">BP Vendors By BP Market Sector</h2>
								</header>
								<div class="card-body">
									<div id="grid_container16">
										<div class="responsiveGrid">
											<table id="gridtable16" class="table table-bordered table-striped mb-0">
												<tbody>
												<tr>
													<td />
												</tr>
												</tbody>
											</table>
											<div id="pager16"></div>
										</div>
									</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/vendorsByBPMarketSector.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<!--  This data not available in live while integration -->
		<!-- <div class="grid-wrapper">
			<div class="form-box" id="ethnicity">
			<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">Tier2 Reporting Ethnicity Breakdown</h2>
							</header>
							<div class="card-body">
								<div id="grid_container1">
									<table id="gridtable" class="table table-bordered table-striped mb-0">
										<tbody>
										<tr>
											<td />
										</tr>
										</tbody>
									</table>
									<div id="pager"></div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/ethnicity.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div> -->
		<div class="grid-wrapper">
			<div class="form-box" id="supplierCount">
			  <div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">Supplier Count by State</h2>
							</header>
							<div class="card-body">
								<div id="grid_container3">
									<div class="responsiveGrid">
										<table id="gridtable2" class="table table-bordered table-striped mb-0">
										<tbody>
											<tr>
												<td />
											</tr>
										</tbody>
										</table>
										<div id="pager2"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/supplierstate.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>

		<div class="grid-wrapper">
			<div class="form-box" id="bpVendorsCount">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">BP Vendor Count by State</h2>
							</header>
							<div class="card-body">
								<div id="grid_container11">
									<div class="responsiveGrid">
										<table id="gridtable11" class="table table-bordered table-striped mb-0">
											<tbody>
												<tr>
													<td />
												</tr>
											</tbody>
										</table>
										<div id="pager11"></div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		
		<!-- Removed Bcoz Client Don't Want Chart for This Part -->
		<%-- <jsp:include page="../report/jspdemo/bpvendorcountstate.jsp"></jsp:include> --%>
	</div>
		<div class="grid-wrapper">
			<div class="form-box" id="agencySpend">
			<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">Prime Supplier Report T2 Spend by Agency</h2>
							</header>
							<div class="card-body">
								<div id="grid_container4">
									<div class="responsiveGrid">
										<table id="gridtable3" class="table table-bordered table-striped mb-0">
										<tbody>
											<tr>
												<td />
											</tr>
										</tbody>
										</table>
										<div id="pager3"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/agencyspend.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-wrapper">
			<div class="form-box" id="totalSpend">
			<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">
									Prime Supplier Report by Tier 2 Total Spend for [
									<c:out value="${quarter}" />
									]
								</h2>
							</header>
							<div class="card-body">
								<div id="grid_container5">
									<div class="responsiveGrid">
										<table id="gridtable4" class="table table-bordered table-striped mb-0">
										<tbody>
											<tr>
												<td />
											</tr>
										</tbody>
										</table>
										<div id="pager4"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/totalspend.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		
		<div class="grid-wrapper">
			<div class="form-box" id="directSpend">
			<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">
									Prime Supplier Report by Tier 2 Direct Spend for [
									<c:out value="${quarter}" />
									]
								</h2>
							</header>
							<div class="card-body">
								<div id="grid_container6">
									<div class="responsiveGrid">
										<table id="gridtable5" class="table table-bordered table-striped mb-0">
											<tbody>
											<tr>
												<td />
											</tr>
											</tbody>
										</table>
										<div id="pager5"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/directspend.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	
		<div class="grid-wrapper">
			<div class="form-box" id="indirectSpend">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">
									Prime Supplier Report by Tier 2 Indirect Spend for [
									<c:out value="${quarter}" />
									]
								</h2>
							</header>
							<div class="card-body">
								<div id="grid_container7">
									<div class="responsiveGrid">
										<table id="gridtable6" class="table table-bordered table-striped mb-0">
											<tbody>
											<tr>
												<td />
											</tr>
											</tbody>
										</table>
										<div id="pager6"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/indirectspend.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		
		<div class="grid-wrapper">
			<div class="form-box" id="diversitySpend">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">
									Tier 2 Vendor Name Report for [
									<c:out value="${quarter}" />
									]
								</h2>
							</header>
							<div class="card-body">
								<div id="grid_container8">
									<div class="responsiveGrid">
										<table class="table table-bordered table-striped mb-0" id="gridtable7">
											<tbody>
												<tr>
													<td />
												</tr>
											</tbody>
										</table>
										<div id="pager7"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/diversityreport.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	
		<div class="grid-wrapper">
			<div class="form-box" id="ethnicityAnalysis">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">
									Tier 2 Reporting Summary for [
									<c:out value="${quarter}" />
									]
								</h2>
							</header>
							<div class="card-body">
								<div id="grid_container9">
									<div class="responsiveGrid">
										<table id="gridtable8" class="table table-bordered table-striped mb-0">
											<tbody>
											<tr>
												<td />
											</tr>
											</tbody>
										</table>
										<div id="pager8"></div>
									</div>
								</div>
								<div class="grid-img"><jsp:include page="../report/jspdemo/ethnicityanalysis.jsp"></jsp:include></div>
							</div>
						</section>
					</div>
				</div>
			</div>		
		</div>
		</div>					
	</section>
</body>
</html>

<logic:present name="diversityview" scope="session">
	<logic:equal value="1" name="diversityview" scope="session">
		<script>
			$(document).ready(function() {
				getDiversityCharts();
				$('#diversity').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="ethnicitydashboard" scope="session">
	<logic:equal value="1" name="ethnicitydashboard" scope="session">
		<script>
			$(document).ready(function() {
				getDashboardReports();
				$("#ethnicity").show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="supplierdashboard" scope="session">
	<logic:equal value="1" name="supplierdashboard" scope="session">
		<script>
			$(document).ready(function() {
				getSupplierStateReport();
				$('#supplierCount').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="agencydashboardGV" scope="session">
	<logic:equal value="1" name="agencydashboardGV" scope="session">
		<script>
			$(document).ready(function() {
				getAgencyReport();
				$('#agencySpend').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="totalSpendDashboard" scope="session">
	<logic:equal value="1" name="totalSpendDashboard" scope="session">
		<script>
			$(document).ready(function() {
				var currentYear = (new Date).getFullYear();
				getTotalSalesDBReport(currentYear);
				$('#totalSpend').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="directDashboard" scope="session">
	<logic:equal value="1" name="directDashboard" scope="session">
		<script>
			$(document).ready(function() {
				var currentYear = (new Date).getFullYear();
				getDirectSpendDBReport(currentYear);
				$('#directSpend').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="indirectDashboard" scope="session">
	<logic:equal value="1" name="indirectDashboard" scope="session">
		<script>
			$(document).ready(function() {
				var currentYear = (new Date).getFullYear();
				getIndirectSpendDBReport(currentYear);
				$('#indirectSpend').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="diversitydashboard" scope="session">
	<logic:equal value="1" name="diversitydashboard" scope="session">
		<script>
			$(document).ready(function() {
				var currentYear = (new Date).getFullYear();
				getDiversityDBReport(currentYear);
				$('#diversitySpend').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="ethnicityAnalysis" scope="session">
	<logic:equal value="1" name="ethnicityAnalysis" scope="session">
		<script>
			$(document).ready(function() {
				var currentYear = (new Date).getFullYear();
				getEthnicityAnalysisDBReport(currentYear);
				$('#ethnicityAnalysis').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="ststusdashboardvalue" scope="session">
	<logic:equal value="1" name="ststusdashboardvalue" scope="session">
		<script>
			$(document).ready(function() {
				getVendorStatusReport();
				$('#vendorStatus').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="bpVendorsCountDashboard" scope="session">
	<logic:equal value="1" name="bpVendorsCountDashboard" scope="session">
		<script>
			$(document).ready(function() {
				getBPVendorStateReport();
				$('#bpVendorsCount').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="newRegistrationsByMonthDashboard" scope="session">
	<logic:equal value="1" name="newRegistrationsByMonthDashboard"
		scope="session">
		<script>
			$(document).ready(function() {
				getNewRegistrationsByMonthReport();
				$('#newRegistrationsByMonth').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="vendorsByIndustryDashboard" scope="session">
	<logic:equal value="1" name="vendorsByIndustryDashboard"
		scope="session">
		<script>
			$(document).ready(function() {
				getVendorsByIndustryDashboardReport();
				$('#vendorsByIndustry').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="bpVendorsByIndustryDashboard" scope="session">
	<logic:equal value="1" name="bpVendorsByIndustryDashboard"
		scope="session">
		<script>
			$(document).ready(function() {
				getBPVendorsByIndustryDashboardReport();
				$('#bpVendorsByIndustry').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="vendorsByNaicsDescriptionDashboard" scope="session">
	<logic:equal value="1" name="vendorsByNaicsDescriptionDashboard"
		scope="session">
		<script>
			$(document).ready(function() {
				getVendorsByNaicsDescriptionDashboardReport();
				$('#vendorsByNaicsDescription').show();
			});
		</script>
	</logic:equal>
</logic:present>
<logic:present name="vendorsByBPMarketSectorDashboard" scope="session">
	<logic:equal value="1" name="vendorsByBPMarketSectorDashboard"
		scope="session">
		<script>
			$(document).ready(function() {
				getVendorsByBPMarketSectorDashboardReport();
				$('#vendorsByBPMarketSector').show();
			});
		</script>
	</logic:equal>
</logic:present>