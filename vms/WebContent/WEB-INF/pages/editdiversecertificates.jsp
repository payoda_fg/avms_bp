<%@page import="java.util.Date"%>
<%@page import="com.fg.vms.util.CommonUtils"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<logic:present name="diverseClassification">
	<bean:size id="diverse" name="diverseClassification" />
	<logic:greaterEqual value="0" name="diverse">
		<logic:iterate id="certificate" name="diverseClassification">
			<bean:define id="id" name="certificate" property="certMasterId.id"></bean:define>
			<input type="checkbox" value="<%=id.toString()%>" name="diverse"
				style="display: none;">
		</logic:iterate>
	</logic:greaterEqual>
</logic:present>
<script type="text/javascript" src="jquery/js/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		document.getElementById("certfile3").onchange = function() {
			document.getElementById("uploadFile3").innerHTML = this.value;
		};
		document.getElementById("certfile2").onchange = function() {
			document.getElementById("uploadFile2").innerHTML = this.value;
		};
		document.getElementById("certfile1").onchange = function() {
			document.getElementById("uploadFile1").innerHTML = this.value;
		};
	});
	function fn_diverseSup() {

		if (document.getElementById("chkDiverse").checked == true) {
			document.getElementById("diverseClassificationTab").style.display = "block";
			document.getElementById("diverseCertTab").style.display = "block";
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
			document.getElementById("diverseCertTab").style.display = "none";
		}
	}
	var imgPath = "jquery/images/calendar.gif";
	datePickerExp();
	$(document).ready(function() {
		//$('#chkDiverse').attr('checked', 'checked');
		if ($("#chkDiverse").is(':checked')) {
			document.getElementById("diverseClassificationTab").style.display = "block";
			document.getElementById("diverseCertTab").style.display = "block";
			// Check the list of certificates.
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
			document.getElementById("diverseCertTab").style.display = "none";
		}
		var certificates = [];
		$("input[name='diverse']").each(function() {
			certificates.push($(this).val());
		});
		$("input[name='certificates']").each(
				function() {
					var attrib = $(this).attr("value");
					$.each(certificates, function(i, val) {
						if (attrib == val) {
							$("input[value='" + val + "']")
									.prop('checked', true);
						}
					});
		});
		enableEthnicity(document.getElementById("divCertType1"),"ethnicity1",'1');
		enableEthnicity(document.getElementById("divCertType2"),"ethnicity2",'2');
		enableEthnicity(document.getElementById("divCertType3"),"ethnicity3",'3');
	});
	function checkDiverseClassificationType(type, value) {
		var divCertType1 = document.getElementById("divCertType1").value;
		var divCertType2 = document.getElementById("divCertType2").value;
		var divCertType3 = document.getElementById("divCertType3").value;
		if (((divCertType3!="" && divCertType2!="" && divCertType3 == divCertType2))
				|| ((divCertType3!="" && divCertType1!="" && divCertType3 == divCertType1))
				||(divCertType2!="" && divCertType1!="" && divCertType2 == divCertType1)) {
			alert("Classification already exists");
			document.getElementById(type).selectedIndex = 0;
			$("#"+type).select2('open');
			document.getElementById(type).focus();
		} else {
			updateCertificateData(type, value);
		}
	}
	var divCert1=false;
	var divCert2=false;
	var divCert3=false;
	function enableEthnicity(ele, id,row) {
		
		divCert1=false;
		divCert2=false;
		divCert3=false;
		
		var status = ele.value;
		var flag=false;
		<logic:present  name="certificateTypes">
		<bean:size id="size" name="certificateTypes" />
		<logic:greaterEqual value="0" name="size">
		<logic:iterate id="certificateType" name="certificateTypes">
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="isEthinicity" />'){
				flag=true;
			}
		}
		if(row=='1'){
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="certificateUpload" />'){
				divCert1=true;
			}
		}}
		if(row=='2'){
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="certificateUpload" />'){
				divCert2=true;
			}
		}}
		if(row=='3'){
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="certificateUpload" />'){
				divCert3=true;
			}
		}}
		</logic:iterate>
		if(flag){
			$("#"+id).prop('disabled', false);
			$("#"+id).select2();
		}else{
			$("#"+id).prop('disabled', true);
			$("#"+id).val(0);
			$("#"+id).select2();
		}
		</logic:greaterEqual>
		</logic:present>
	}
	function updateCertificateData(id, value) {
		if(value!=''){
		$.ajax({
			url : "ajaxclassification.do?method=getClassificationData&" + id
					+ "=" + value,
			type : "POST",
			async : false,
			success : function(data) {
				var split = data.split('|');
				var agency = split[0];
				var certificateType = split[1];
				if (id == "divCertType1") {
					$('#divCertAgen1').find('option').remove().end().append('<option value="">--Select--</option>').append(
							agency);
					$('#certType1').find('option').remove().end().append('<option value="">--Select--</option>').append(
							certificateType);
					$("#certType1").select2();
					$("#divCertAgen1").select2();
				} else if (id == "divCertType2") {
					$('#divCertAgen2').find('option').remove().end().append('<option value="">--Select--</option>').append(
							agency);
					$('#certType2').find('option').remove().end().append('<option value="">--Select--</option>').append(
							certificateType);
					$("#certType2").select2();
					$("#divCertAgen2").select2();
				} else if (id == "divCertType3") {
					$('#divCertAgen3').find('option').remove().end().append('<option value="">--Select--</option>').append(
							agency);
					$('#certType3').find('option').remove().end().append('<option value="">--Select--</option>').append(
							certificateType);
					$("#certType3").select2();
					$("#divCertAgen3").select2();
				}
			}
		});
		}
	}
</script>
<style>
/* .file1 {
	cursor: pointer;
	font-size: 23px;
	margin: 0;
	position: absolute;
	width:25%;
} */
</style>
<html:hidden property="filelastrowcount" value="3"
	styleId="filelastrowcount" />

<div class="clear"></div>
Diverse Supplier
<html:checkbox property="deverseSupplier" styleId="chkDiverse"
	onclick="fn_diverseSup()" tabindex="104" />
<div id="diverseClassificationTab" class="panelCenter_1"
	style="margin-bottom: 18%; display: none;">
	<h3>Diversity Information</h3>
	<div class="form-box" style="padding: 1%; width: 98%;">
		<p style="font-weight: bold;">Diversity/small Business
			Classifications - check all that apply</p>
		<bean:size id="size1" name="certificateTypes" />
		<div class="coulmn-1-row">
			<logic:greaterEqual value="0" name="size1">
				<logic:iterate id="certificate" name="certificateTypes">
					<bean:define id="id" name="certificate" property="id"></bean:define>
					<div class="coulmn-2">
						<html:checkbox property="certificates" value="<%=id.toString()%>"
							tabindex="105">
							<bean:write name="certificate" property="certificateName"></bean:write>
						</html:checkbox>
					</div>
				</logic:iterate>
			</logic:greaterEqual>
		</div>
	</div>
</div>
<div class="clear"></div>
<div id="diverseCertTab" style="display: none; padding-top: 2%;"
	class="panelCenter_1">
	<h3>Upload Diverse Certificates </h3>

	<table width="100%" border="0" class="main-table">
		<tr>
			<td class="header">Classification</td>
			<td class="header">Agency</td>
			<td class="header">Certification Type</td>
			<td class="header">Certification #</td>
			<td class="header">Effective Date</td>
			<td class="header">Expire Date</td>
			<td class="header">Certificate</td>
		</tr>
		<tr class="even" id="TemplateRow1">
			<td><html:hidden property="vendorCertID1" styleId="vendorCertID1"/> <html:select
					property="divCertType1" styleClass="chosen-select"
					styleId="divCertType1" tabindex="106"
					onchange="checkDiverseClassificationType('divCertType1',this.value);enableEthnicity(this,'ethnicity1','1');">
					<html:option value="">--Select--</html:option>
					<logic:notEmpty name="certificateTypes">
						<logic:iterate id="certificate" name="certificateTypes">
							<bean:define id="id" name="certificate" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certificate" property="certificateName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select> <span class="error"> <html:errors property="divCertType"></html:errors>
			</span></td>
			<td><html:select property="divCertAgen1" tabindex="107"
					styleClass="chosen-select" styleId="divCertAgen1">
					<html:option value="">--Select--</html:option>
					<logic:notEmpty property="certAgencies1" name="editVendorMasterForm">
						<logic:iterate id="certAgency" property="certAgencies1" name="editVendorMasterForm">
							<bean:define id="id" name="certAgency" property="id"></bean:define>
							<html:option value="${id}">
								<bean:write name="certAgency" property="agencyName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select> <span class="error"> <html:errors property="divCertAgen"></html:errors>
			</span></td>
			<td><html:select property="certType1" styleId="certType1"
					styleClass="chosen-select" tabindex="108">
					<html:option value="">---- Select ----</html:option>
					<logic:notEmpty property="certificateTypes1" name="editVendorMasterForm">
						<logic:iterate id="certType" property="certificateTypes1" name="editVendorMasterForm">
							<bean:define id="id" name="certType" property="id"></bean:define>
							<html:option value="${id}">
								<bean:write name="certType" property="certificateTypeDesc"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:text property="certificationNo1" tabindex="109"
					styleId="certificationNo1" alt="" styleClass="main-text-box" /></td>
			<td><html:text property="effDate1" styleId="effDate1" alt="Please click to select date"
					styleClass="main-text-box" tabindex="110" /></td>
			<td><html:text property="expDate1" styleId="expDate1" alt="Please click to select date"
					styleClass="main-text-box" tabindex="111" /> <span class="error">
					<html:errors property="expDate"></html:errors>
			</span></td>
			<td>
				<div class="fileUpload btn btn-primary">
					<span>Browse</span>
					<html:file property="certFile1" styleId="certfile1" tabindex="112"
						styleClass="upload">
					</html:file>
				</div> <span id="uploadFile1"></span> <span class="error"> <html:errors
						property="certFile"></html:errors>
			</span>
			</td>

		</tr>
		<tr class="even">
			<td colspan="5">
				<div class="row-wrapper" style="width: 50%">
					<div class="label-col-wrapper">Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ethnicity1" styleId="ethnicity1"
							styleClass="chosen-select-width" tabindex="117"
							style="height: 2%;width:80%;" >
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="ethnicities" label="ethnicity"
								value="id" />
						</html:select>
					</div>
				</div>
			</td>
			<td><logic:present property="certFile1Id"
					name="editVendorMasterForm">
					<logic:notEqual value="0" property="certFile1Id"
						name="editVendorMasterForm">
						<bean:define id="id" property="certFile1Id"
							name="editVendorMasterForm"></bean:define>
							<html:hidden property="certFile1Id"/>
						<html:link href="download.jsp" paramId="id" paramName="id"
							styleId="downloadFile1" styleClass="downloadFile">Download</html:link>
					</logic:notEqual>
				</logic:present></td>
				<td><a onclick="return clearCertificate('1');" href="#">Delete</a></td>
		</tr>
		<tr class="even" id="TemplateRow1">
			<td><html:hidden property="vendorCertID2" styleId="vendorCertID2"/> <html:select
					property="divCertType2" tabindex="113" styleClass="chosen-select"
					styleId="divCertType2"
					onchange="checkDiverseClassificationType('divCertType2',this.value);enableEthnicity(this,'ethnicity2','2');">
					<html:option value="">--Select--</html:option>
					<logic:notEmpty name="certificateTypes">
						<logic:iterate id="certificate" name="certificateTypes">
							<bean:define id="id" name="certificate" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certificate" property="certificateName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:select property="divCertAgen2" tabindex="114"
					styleClass="chosen-select" styleId="divCertAgen2"
					onchange="checkDiverseAgent()">
					<html:option value="">--Select--</html:option>
					<logic:notEmpty property="certAgencies2" name="editVendorMasterForm">
						<logic:iterate id="certAgency" property="certAgencies2" name="editVendorMasterForm">
							<bean:define id="id" name="certAgency" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certAgency" property="agencyName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:select property="certType2" styleId="certType2"
					styleClass="chosen-select" tabindex="115">
					<html:option value="">---- Select ----</html:option>
					<logic:notEmpty property="certificateTypes2" name="editVendorMasterForm">
						<logic:iterate id="certType" property="certificateTypes2" name="editVendorMasterForm">
							<bean:define id="id" name="certType" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certType" property="certificateTypeDesc"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:text property="certificationNo2" tabindex="116"
					styleId="certificationNo2" alt="" styleClass="main-text-box" /></td>
			<td><html:text property="effDate2" styleId="effDate2" alt="Please click to select date"
					styleClass="main-text-box" tabindex="94" /></td>
			<td><html:text property="expDate2" styleId="expDate2" alt="Please click to select date"
					styleClass="main-text-box" tabindex="95" /></td>
			<td>
				<div class="fileUpload btn btn-primary">
					<span>Browse</span>
					<html:file property="certFile2" styleId="certfile2" tabindex="117"
						styleClass="upload">
					</html:file>
				</div> <span id="uploadFile2"></span>
			</td>
		</tr>
		<tr class="even">
			<td colspan="5">
				<div class="row-wrapper" style="width: 50%">
					<div class="label-col-wrapper">Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ethnicity2" styleId="ethnicity2"
							styleClass="chosen-select-width" tabindex="117"
							style="height: 2%;width:80%;" >
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="ethnicities" label="ethnicity"
								value="id" />
						</html:select>
					</div>
				</div>
			</td>
			<td><logic:present property="certFile2Id"
					name="editVendorMasterForm">
					<logic:notEqual value="0" property="certFile2Id"
						name="editVendorMasterForm">
						<bean:define id="id" property="certFile2Id"
							name="editVendorMasterForm"></bean:define>
							<html:hidden property="certFile2Id"/>
						<html:link href="download.jsp" paramId="id" paramName="id"
							styleId="downloadFile2" styleClass="downloadFile">Download</html:link>
					</logic:notEqual>
				</logic:present>
			</td>
			<td><a onclick="return clearCertificate('2');" href="#">Delete</a></td>
		</tr>
		<tr class="even" id="TemplateRow1">
			<td><html:hidden property="vendorCertID3" styleId="vendorCertID3" /> <html:select
					property="divCertType3" tabindex="118" styleClass="chosen-select"
					styleId="divCertType3"
					onchange="checkDiverseClassificationType('divCertType3',this.value);enableEthnicity(this,'ethnicity3','3');">
					<html:option value="">--Select--</html:option>
					<logic:notEmpty name="certificateTypes">
						<logic:iterate id="certificate" name="certificateTypes">
							<bean:define id="id" name="certificate" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certificate" property="certificateName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:select property="divCertAgen3" tabindex="119"
					styleClass="chosen-select" styleId="divCertAgen3"
					onchange="checkDiverseAgent()">
					<html:option value="">--Select--</html:option>
					<logic:notEmpty property="certAgencies3" name="editVendorMasterForm">
						<logic:iterate id="certAgency" property="certAgencies3" name="editVendorMasterForm">
							<bean:define id="id" name="certAgency" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certAgency" property="agencyName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:select property="certType3" styleId="certType3"
					styleClass="chosen-select" tabindex="120">
					<html:option value="">---- Select ----</html:option>
					<logic:notEmpty property="certificateTypes3" name="editVendorMasterForm">
						<logic:iterate id="certType" property="certificateTypes3" name="editVendorMasterForm">
							<bean:define id="id" name="certType" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certType" property="certificateTypeDesc"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:notEmpty>
				</html:select></td>
			<td><html:text property="certificationNo3" tabindex="121"
					styleId="certificationNo3" alt="" styleClass="main-text-box" /></td>
			<td><html:text property="effDate3" styleId="effDate3" alt="Please click to select date"
					styleClass="main-text-box" tabindex="122" /></td>
			<td><html:text property="expDate3" styleId="expDate3" alt="Please click to select date"
					styleClass="main-text-box" tabindex="123" /></td>
			<td>
				<div class="fileUpload btn btn-primary">
					<span>Browse</span>
					<html:file property="certFile3" styleId="certfile3" tabindex="124"
						styleClass="upload">
					</html:file>
				</div> <span id="uploadFile3" style="overflow: hidden;"></span>
			</td>
		</tr>
		<tr class="even">
			<td colspan="5">
				<div class="row-wrapper" style="width: 50%">
					<div class="label-col-wrapper">Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ethnicity3" styleId="ethnicity3"
							styleClass="chosen-select-width" tabindex="117"
							style="height: 2%;width:80%;" >
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="ethnicities" label="ethnicity"
								value="id" />
						</html:select>
					</div>
				</div>
			</td>
			<td><logic:present property="certFile3Id"
					name="editVendorMasterForm">
					<logic:notEqual value="0" property="certFile3Id"
						name="editVendorMasterForm">
						<bean:define id="id" property="certFile3Id"
							name="editVendorMasterForm"></bean:define>
							<html:hidden property="certFile3Id"/>
						<html:link href="download.jsp" paramId="id" paramName="id"
							styleId="downloadFile3" styleClass="downloadFile">Download</html:link>
					</logic:notEqual>
				</logic:present></td>
				<td><a onclick="return clearCertificate('3');" href="#">Delete</a></td>
		</tr>
	</table>
</div>
<div class="clear"></div>
<style>
#moreInfo td {
	padding: 5px;
}
</style>
<div class="clear"></div>
