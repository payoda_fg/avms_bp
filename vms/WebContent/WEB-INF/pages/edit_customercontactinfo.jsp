<%@page import="com.fg.vms.admin.model.Customer"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="com.fg.vms.admin.dto.CustomerInfoDto"%>

<%
	CustomerInfoDto customerInfo = (CustomerInfoDto) session
			.getAttribute("customerInfo");
	session.setAttribute("contacts", customerInfo.getContacts());
	session.setAttribute("customer", customerInfo.getCustomer());
%>

<div class="toggleFilter">
	<h2 id="show2" title="Click here to hide/show the form"
		style="cursor: pointer; margin: 10px 0px 10px 0px; width: 100%;">
		<img id="showx" src="images/arrow-down.png" />&nbsp;&nbsp;Contacts
	</h2>
</div>
<div id="table-holder2" style="padding: 5px 0px 10px 0px;">

	<html:link action="/contact.do?method=contact"
		styleClass="linkToButton">Add Contact</html:link>

	<table id="box2" style="border-collapse: collapse;">
		<bean:size id="size" name="contacts" />
		<logic:greaterThan value="0" name="size">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Phone</th>
					<th>Mobile</th>
					<th>Fax</th>
					<th>Email</th>
					<th>User Name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<logic:iterate name="contacts" id="contacts" indexId="index">
				<tbody>
					<tr>

						<td><bean:write name="contacts" property="firstName" /></td>
						<td><bean:write name="contacts" property="phoneNumber" /></td>
						<td><bean:write name="contacts" property="mobile" /></td>
						<td><bean:write name="contacts" property="fax" /></td>
						<td><bean:write name="contacts" property="emailId" /></td>
						<td><bean:write name="contacts" property="loginId" /></td>
						<bean:define id="custId" name="contacts" property="id"></bean:define>


						<td><html:link action="/viewcontact.do?method=viewContact"
								paramId="custId" paramName="custId">Edit</html:link>||<html:link
								action="/viewcontact.do?method=delete" paramId="custId"
								paramName="custId" onclick="return confirm_delete();">Delete</html:link></td>

					</tr>
				</tbody>
			</logic:iterate>
		</logic:greaterThan>
		<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
	</table>
</div>