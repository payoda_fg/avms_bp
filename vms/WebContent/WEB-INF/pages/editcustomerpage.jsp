<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<style>
.ui-autocomplete {
	max-height: 150px;
	max-width: 500px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: auto;
	/* add padding to account for vertical scrollbar */
	padding-right: 20px;
	white-space: nowrap;
}

/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
* html .ui-autocomplete {
	height: 150px;
	width: 500px;
	overflow-y: auto;
	overflow-x: auto;
	padding-right: 20px;
	white-space: nowrap;
}

.myDivSty {
	visibility: hidden;
	display: none;
}
</style>

<div id="form-container">
	<div id="form-container-in">

		<h1>
			<img src="images/Edit-Customer.gif" />Customer Configuration
		</h1>
		<!--  <FORM NAME="custConfig" METHOD="POST" class="AVMS" >-->

		<html:form enctype="multipart/form-data" styleClass="AVMS"
			action="/createCustomer?actionId=create">
			<html:javascript formName="customerForm" />

			<h2>
				Personalize your profile<br /> <br />
			</h2>

			<script type="text/javascript">
                    var bgcolor='<%=request.getAttribute("background")%>';
                    var secondary='<%=request.getAttribute("secondary")%>';
                    var heading='<%=request.getAttribute("heading")%>';
                    var rollover='<%=request.getAttribute("rollover")%>';
                    var menubar='<%=request.getAttribute("menubar")%>';
                    var menubutton='<%=request.getAttribute("menubutton")%>';
			</script>

			<table align="center" style="padding-top: 1px; width: 100%;"
				id="main_table" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="padding-left: 10px; vertical-align: top;">

						<table border="0" cellpadding="0" cellspacing="5" width="100%">

							<tr align="left" valign="middle">
								<td align="left" valign="middle"><label class="desc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select
										for default color theme</label></td>
								<td><html:checkbox property="defaultSet"
										styleId="defaultSet" onclick="fn_default()"></html:checkbox></td>
							</tr>

							<tr>
								<td colspan="2"><h2>Header</h2></td>
							</tr>
							<tr height="30">

								<td style="padding-left: 20px;"><label class="desc">Select
										company logo </label></td>
								<td colspan="2"><html:file property="companyLogo"
										styleId="companyLogo"></html:file>
									<div style="color: red">
										<font size="1px"><html:errors property="companyLogo"></html:errors></font>
									</div></td>
								<td style="padding-left: 30px;"><bean:write name=""
										property="companyLogo"></bean:write>
							</tr>
							<tr height="30">
								<td style="padding-left: 20px;"><label class="desc">Primary</label></td>
								<td><html:text property="backGround"
										styleClass="color config-color" styleId="Background"
										value="<bean:write name='background'/>" /></td>
							</tr>

							<tr>
								<td colspan="2"><h2>Body</h2></td>
							</tr>


							<tr height="30">
								<td style="padding-left: 20px;"><label class="desc">BackGround</label></td>
								<td><html:text property="secondaryColor"
										styleClass="color config-color" styleId="SecondaryColor"
										value="<bean:write name='secondary'/>"></html:text></td>
							</tr>
							<tr height="30">
								<td style="padding-left: 20px;"><label class="desc">Text</label></td>
								<td><html:text property="heading" styleId="Heading"
										styleClass="color config-color"
										value="<bean:write name='heading'/>"></html:text></td>
							</tr>

							<tr>
								<td colspan="2"><h2>Menu</h2></td>
							</tr>
							<tr height="30">
								<td style="padding-left: 20px;"><label class="desc">Menu
										Select</label></td>
								<td><html:text property="rollOver" styleId="RollOver"
										styleClass="color config-color"
										value="<bean:write name='rollover'/>"></html:text></td>
							</tr>
							<tr height="30">
								<td style="padding-left: 20px;"><label class="desc">Menu
										Bar</label></td>
								<td><html:text property="menuBar" styleId="MenuBar"
										styleClass="color config-color"
										value="<bean:write name='menubar'/>"></html:text></td>
							</tr>
							<tr height="30">
								<td style="padding-left: 20px;"><label class="desc">Menu
										Button</label></td>
								<td><html:text property="menuButton" styleId="MenuButton"
										styleClass="color config-color"
										value="<bean:write name='menubutton'/>"></html:text></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<h2>Customer Information</h2>
			<ul>

				<li>

					<div class="height left">
						<label class="desc">Company Name<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="companyname"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="companyname"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Company Code<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="companycode"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="companycode"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">DUNS Number<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="dunsnum"
								name="customerForm" onchange="ajaxfn(this, 'D')" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="dunsnum"></html:errors></font>
						</div>
					</div>
				</li>
				<li>
					<div class="height left">
						<label class="desc">Tax ID Number <span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="taxid"
								name="customerForm" onchange="ajaxfn(this, 'T')" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="taxid"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Address 1<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="address1"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="address1"></html:errors></font>
						</div>
					</div>


					<div class="height left">
						<label class="desc">Address 2</label>
						<div>
							<html:text styleClass="text-box" property="address2"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="address2"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">Address 3</label>
						<div>
							<html:text styleClass="text-box" property="address3"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="address3"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">City<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="city"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="city"></html:errors></font>
						</div>
					</div>


					<div class="height left">
						<label class="desc">State</label>
						<div>
							<html:text styleClass="text-box" property="state"
								name="customerForm" size="25" alt=""></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="state"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">Province</label>
						<div>
							<html:text styleClass="text-box" property="province"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="province"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Zip Code</label>
						<div>
							<html:text styleClass="text-box" property="zipcode"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="zipcode"></html:errors></font>
						</div>
					</div>


					<div class="height left">
						<label class="desc">Region</label>
						<div>
							<html:text styleClass="text-box" property="region"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="region"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">Country<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="country"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="country"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Mobile</label>
						<div>
							<html:text styleClass="text-box" property="mobile"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="mobile"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Phone<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="phone"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="phone"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">FAX</label>
						<div>
							<html:text styleClass="text-box" property="fax"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="fax"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Website URL(http://)</label>
						<div>
							<html:text styleClass="text-box" property="webSite"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="webSite"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Email</label>
						<div>
							<html:text styleClass="text-box" property="email"
								name="customerForm" size="25" styleId="emailAddress"
								onblur="return email_validate(this.value)"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="email"></html:errors></font>
						</div>
					</div>

				</li>

			</ul>

			<h2>Primary Contact Information</h2>
			<ul>
				<li>
					<div class="height left">
						<label class="desc">First Name<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="firstName"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="firstName"></html:errors></font>
						</div>
					</div>


					<div class="height left">
						<label class="desc">Last Name</label>
						<div>
							<html:text styleClass="text-box" property="lastName"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="lastName"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Title</label>
						<div>
							<html:text styleClass="text-box" property="title"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="title"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">Phone<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="contactPhone"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="contanctPhone"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Mobile<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="contactMobile"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="contactMobile"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">FAX</label>
						<div>
							<html:text styleClass="text-box" property="contactFax"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="contactFax"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">Email<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="contanctEmail"
								name="customerForm" size="25" styleId="emailAddress"
								onblur="return email_validate(this.value)"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="contanctEmail"></html:errors></font>
						</div>
					</div>
				</li>
			</ul>

			<h2>User Information</h2>
			<ul>
				<li>
					<div class="height left">
						<label class="desc">Name<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="userName"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userName"></html:errors></font>
						</div>
					</div>


					<div class="height left">
						<label class="desc">Phone<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="userPhone"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userPhone"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Email<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="userEmail"
								name="customerForm" size="25" styleId="emailAddress"
								onblur="return email_validate(this.value)"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userEmail"></html:errors></font>
						</div>
					</div>
				</li>

				<li>
					<div class="height left">
						<label class="desc">Login ID:<span class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="userLoginId"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userLoginId"></html:errors></font>
						</div>
					</div>


					<div class="height left">
						<label class="desc">Password<span class="req">*</span></label>
						<div>
							<html:password styleClass="text-box" property="userPassword"
								name="customerForm" size="25"></html:password>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userPassword"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Confirm Password<span class="req">*</span></label>
						<div>
							<html:password styleClass="text-box" property="userConfPassword"
								name="customerForm" size="25"></html:password>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userConfPassword"></html:errors></font>
						</div>
					</div>
				</li>


				<li>
					<div class="height left">
						<label class="desc">Select Qustion<span class="req">*</span></label>
						<div>
							<html:select property="userSecQn" name="customerForm"
								onfocus="window.dropdown_menu_hack(this)"
								styleClass="field select medium2">
								<html:option value="">-- Select --</html:option>
								<html:option value="qn1">What is your father's first name</html:option>
								<html:option value="qn2">What is your favorite color</html:option>
								<html:option value="qn3">What is your mother's first name</html:option>
								<html:option value="qn4">What is your mother's maiden name</html:option>

							</html:select>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userSecQn"></html:errors></font>
						</div>
					</div>

					<div class="height left">
						<label class="desc">Secret Question's Answer:<span
							class="req">*</span></label>
						<div>
							<html:text styleClass="text-box" property="userSecQnAns"
								name="customerForm" size="25"></html:text>
						</div>
						<div style="color: red">
							<font size="1px"><html:errors property="userSecQnAns"></html:errors></font>
						</div>
					</div>
				</li>
			</ul>

			<div class="button-holder">
				<ul>
					<li class="sub"><html:submit styleClass="btTxt submit"
							value="Submit"></html:submit> <html:reset
							styleClass="btTxt submit" value="Reset"></html:reset></li>
				</ul>
			</div>

		</html:form>

	</div>
</div>