<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.dto.RetriveVendorInfoDto"%>
<%
	RetriveVendorInfoDto vendorInfo = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");
	/*  session.setAttribute("vendorContacts",
		    vendorInfo.getVendorContacts()); */
	session.setAttribute("vendorMaster", vendorInfo.getVendorMaster());
%>
<div class="page-title">
	<img src="images/user-group.gif" />&nbsp;&nbsp;Vendor Contacts
	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="modify">
					<html:link action="/subVendorContact.do?method=subVendorContact"
						styleClass="btn" style="float:right;">Add Contact</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
	</logic:present>
</div>
<table id="box2" class="main-table" width="100%" border="0">
	<logic:notEmpty name="vendorInfoDto" property="vendorContacts">
		<thead>
			<tr>
				<td class="header">First Name</td>
				<td class="header">Phone</td>
				<td class="header">Mobile</td>
				<td class="header">Fax</td>
				<td class="header">Email</td>
				<td class="header">Business Contact</td>
				<td class="header">Actions</td>
			</tr>
		</thead>
		<logic:iterate id="vendorContact" name="vendorInfoDto"
			property="vendorContacts" indexId="index">
			<tbody>
				<tr>
					<td><bean:write name="vendorContact" property="firstName" /></td>
					<td><bean:write name="vendorContact" property="phoneNumber" /></td>
					<td><bean:write name="vendorContact" property="mobile" /></td>
					<td><bean:write name="vendorContact" property="fax" /></td>
					<td><bean:write name="vendorContact" property="emailId" /></td>
					<td align="center">
						<logic:equal value="1"  name="vendorContact" property="isBusinessContact">Yes</logic:equal>
						<logic:equal value="0"  name="vendorContact" property="isBusinessContact">No</logic:equal>
					</td>
					<bean:define id="vendorId" name="vendorContact" property="id"></bean:define>
					<td><html:link
							action="/viewsubvendorcontact.do?method=viewSubVendorContact"
							paramId="vendorId" paramName="vendorId">View</html:link> <!--|  <html:link
							action="/viewsubvendorcontact.do?method=deleteContact" paramId="vendorId"
							paramName="vendorId" onclick="return confirm_delete();">Delete</html:link>--></td>
				</tr>
			</tbody>
		</logic:iterate>
	</logic:notEmpty>
	<logic:empty name="vendorInfoDto" property="vendorContacts">
	No such Records
	</logic:empty>
</table>