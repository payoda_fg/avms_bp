<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script>
	jQuery(document).ready(function() {
		document.getElementById("certfile3").onchange = function() {
			document.getElementById("uploadFile3").innerHTML = this.value;
		};
		document.getElementById("certfile2").onchange = function() {
			document.getElementById("uploadFile2").innerHTML = this.value;
		};
		document.getElementById("certfile1").onchange = function() {
			document.getElementById("uploadFile1").innerHTML = this.value;
		};
	});
	var imgPath = "jquery/images/calendar.gif";
	datePickerExp();
	function fn_diverseSup() {
		if (document.getElementById("chkDiverse").checked == true) {
			document.getElementById("diverseClassificationTab").style.display = "block";
			document.getElementById("diverseCertTab").style.display = "block";
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
			document.getElementById("diverseCertTab").style.display = "none";
		}
	}
	$(document).ready(function() {
		//$('#chkDiverse').attr('checked', 'checked');
		if ($("#chkDiverse").is(':checked')) {
			document.getElementById("diverseClassificationTab").style.display = "block";
			document.getElementById("diverseCertTab").style.display = "block";
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
			document.getElementById("diverseCertTab").style.display = "none";
		}
	});
	function checkDiverseClassificationType(type, value) {
		var divCertType1 = document.getElementById("divCertType1").value;
		var divCertType2 = document.getElementById("divCertType2").value;
		var divCertType3 = document.getElementById("divCertType3").value;
		/* if(value != ''){ */
			if (divCertType2 == divCertType1) {
				alert("Classification already exists");
				document.getElementById("divCertType2").value = "";
				document.getElementById("divCertType2").focus();
			} else if ((divCertType3 == divCertType2)
					|| (divCertType3 == divCertType1)) {
				alert("Classification already exists");
				document.getElementById("divCertType3").value = "";
				document.getElementById("divCertType3").focus();
			} else {
				updateCertificateData(type, value);
			}
		/* } else {
			if(type == 'divCertType2'){
				$("#certType2").val('');
				$("#divCertAgen2").val('');
			} else if(type == 'divCertType3'){
				$("#certType3").val('');
				$("#divCertAgen3").val('');
			}
		} */
	}
	$(function() {
		var count1 = 3;
		$('#addDiverse').click(function() {
			count1 = count1 + $('#TemplateRow1').length;
			document.getElementById("filelastrowcount").value = count1;
			var $newRow = $('#TemplateRow1').clone(true);
			$newRow.find('#divCertType3').val(
					$("#TemplateRow").find('#divCertType3').val());
			$newRow.find('#divCertAgen3').val(
					$("#TemplateRow").find('#divCertAgen3').val());
			$newRow.find('#expDate3').val(
					$("#TemplateRow").find('#expDate3').val());
			$newRow.find('#certfile3').val(
					$("#TemplateRow").find('#certfile3').val());
			$newRow.find('*').andSelf().removeAttr('id');
			$('#BoxTable tr:last').before($newRow);
			return false;
		});
	});
	function updateCertificateData(id, value) {
		$.ajax({
			url : "ajaxclassification.do?method=getClassificationData&" + id
					+ "=" + value,
			type : "POST",
			async : false,
			success : function(data) {
				var split = data.split('|');
				var agency = split[0];
				var certificateType = split[1];
				if (id == "divCertType1") {
					$('#divCertAgen1').find('option').remove().end().append(
							agency);
					$('#certType1').find('option').remove().end().append(
							certificateType);
					$("#certType1").select2();
					$("#divCertAgen1").select2();
				} else if (id == "divCertType2") {
					$('#divCertAgen2').find('option').remove().end().append(
							agency);
					$('#certType2').find('option').remove().end().append(
							certificateType);
					$("#certType2").select2();
					$("#divCertAgen2").select2();
				} else if (id == "divCertType3") {
					$('#divCertAgen3').find('option').remove().end().append(
							agency);
					$('#certType3').find('option').remove().end().append(
							certificateType);
					$("#certType3").select2();
					$("#divCertAgen3").select2();
				}
			}
		});
	}
	function enableEthnicity(ele,id) {
		var status = ele.value;
		var flag=false;
		<logic:present  name="certificateTypes">
		<bean:size id="size" name="certificateTypes" />
		<logic:greaterEqual value="0" name="size">
		<logic:iterate id="certificateType" name="certificateTypes">
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="isEthinicity" />'){
				flag=true;
				
			}
		}
		</logic:iterate>
		if(flag){
			$("#"+id).prop('disabled', false);
			$("#"+id).select2();
		}else{
			$("#"+id).prop('disabled', true);
			$("#"+id).val(0);
			$("#"+id).select2();
		}
		</logic:greaterEqual>
		</logic:present>
	}
</script>
<html:hidden property="filelastrowcount" value="3"
	styleId="filelastrowcount" />
<div class="clear"></div>
Diverse Supplier
<html:checkbox property="deverseSupplier" styleId="chkDiverse"
	onclick="fn_diverseSup()" tabindex="94" />
<div id="diverseClassificationTab" class="panelCenter_1"
	style="display: none;">
	<h3>Diversity Information</h3>
	<div class="form-box" style="padding: 1%; width: 98%;">
		<p style="font-weight: bold;">Diversity/small Business
			Classifications - check all that apply</p>
		<bean:size id="size1" name="certificateTypes" />
		<div class="coulmn-1-row">
			<logic:greaterEqual value="0" name="size1">
				<logic:iterate id="certificate" name="certificateTypes">
					<bean:define id="id" name="certificate" property="id"></bean:define>
					<div class="coulmn-2">
						<html:checkbox property="certificates" value="<%=id.toString()%>"
							tabindex="95"> 
						&nbsp;<bean:write name="certificate" property="certificateName"></bean:write>
						</html:checkbox>
					</div>
				</logic:iterate>
			</logic:greaterEqual>
		</div>
	</div>
</div>
<div class="clear"></div>
<div id="diverseCertTab" class="panelCenter_1"
	style="display: none; padding-top: 2%;">
	<h3>Upload Diverse Certificates </h3>
	<!-- 	<div class="form-box"> -->
	<table class="main-table">
		<tr>
			<td class="header">Classification</td>
			<td class="header">Agency</td>
			<td class="header">Certification Type</td>
			<td class="header">Certification #</td>
			<td class="header">Effective Date</td>
			<td class="header">Expire Date</td>
			<td class="header">Certificate</td>
		</tr>
		<tr class="even" id="TemplateRow1">
			<td><html:select property="divCertType1" tabindex="96"
					styleClass="chosen-select" styleId="divCertType1"
					onchange="updateCertificateData('divCertType1',this.value);enableEthnicity(this,'ethnicity1');">
					<html:option value="">--Select--</html:option>
					<logic:present name="certificateTypes">
					<html:optionsCollection name="certificateTypes" label="certificateName" value="id"/></logic:present>
				</html:select> <span class="error"> <html:errors property="divCertType"></html:errors>
			</span></td>
			<td><html:select property="divCertAgen1" tabindex="97"
					styleClass="chosen-select" styleId="divCertAgen1"></html:select></td>
			<td><html:select property="certType1" styleId="certType1"
					styleClass="chosen-select" tabindex="98"></html:select></td>
			<td><html:text property="certificationNo1" tabindex="99"
					styleId="certificationNo1" alt="" styleClass="main-text-box" /></td>
			<td><html:text property="effDate1" styleId="effDate1" alt="Please click to select date"
					styleClass="main-text-box" tabindex="100" /></td>
			<td><html:text property="expDate1" styleId="expDate1" alt="Please click to select date"
					styleClass="main-text-box" tabindex="101" /> <span class="error">
					<html:errors property="expDate"></html:errors>
			</span></td>
			<td><div class="fileUpload btn btn-primary">
					<span>Browse</span>
					<html:file property="certFile1" styleId="certfile1" tabindex="102"
						styleClass="upload">
					</html:file>
				</div> <span id="uploadFile1"></span> <span class="error"> <html:errors
						property="certFile"></html:errors>
			</span></td>

		</tr>
		<tr class="even">
			<td colspan="7">
				<div class="row-wrapper" style="width: 50%">
					<div class="label-col-wrapper">Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ethnicity1" styleId="ethnicity1"
							styleClass="chosen-select-width" tabindex="117"
							style="height: 2%;width:80%;" disabled="true">
							<html:option value="">--Select--</html:option>
							<logic:present name="ethnicities">
							<html:optionsCollection name="ethnicities" label="ethnicity"
								value="id" /></logic:present>
						</html:select>
					</div>
				</div>
			</td>
		</tr>
		<tr class="even" id="TemplateRow1">
			<td><html:select property="divCertType2" tabindex="103"
					styleClass="chosen-select" styleId="divCertType2"
					onchange="checkDiverseClassificationType('divCertType2',this.value);enableEthnicity(this,'ethnicity2');">
					<html:option value="">--Select--</html:option>
					<logic:present name="certificateTypes">
					<html:optionsCollection name="certificateTypes" label="certificateName" value="id"/></logic:present>
				</html:select></td>
			<td><html:select property="divCertAgen2" tabindex="104"
					styleClass="chosen-select" styleId="divCertAgen2"
					onchange="checkDiverseAgent()">
				</html:select></td>
			<td><html:select property="certType2" styleId="certType2"
					styleClass="chosen-select" tabindex="105">
				</html:select></td>
			<td><html:text property="certificationNo2" tabindex="106"
					styleId="certificationNo2" alt="" styleClass="main-text-box" /></td>
			<td><html:text property="effDate2" styleId="effDate2" alt="Please click to select date"
					styleClass="main-text-box" tabindex="107" /></td>
			<td><html:text property="expDate2" styleId="expDate2" alt="Please click to select date"
					styleClass="main-text-box" tabindex="108" /></td>
			<td><div class="fileUpload btn btn-primary">
					<span>Browse</span>
					<html:file property="certFile2" styleId="certfile2" tabindex="109"
						styleClass="upload">
					</html:file>
				</div> <span id="uploadFile2"></span></td>
		</tr>
		<tr class="even">
			<td colspan="7">
				<div class="row-wrapper" style="width: 50%">
					<div class="label-col-wrapper">Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ethnicity2" styleId="ethnicity2"
							styleClass="chosen-select-width" tabindex="117"
							style="height: 2%;width:80%;" disabled="true">
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="ethnicities" label="ethnicity"
								value="id" />
						</html:select>
					</div>
				</div>
			</td>
		</tr>
		<tr class="even" id="TemplateRow1">
			<td><html:select property="divCertType3" tabindex="110"
					styleClass="chosen-select" styleId="divCertType3"
					onchange="checkDiverseClassificationType('divCertType3',this.value);enableEthnicity(this,'ethnicity3');">
					<html:option value="">--Select--</html:option>
					<logic:present name="certificateTypes">
					<html:optionsCollection name="certificateTypes" label="certificateName" value="id"/></logic:present>
				</html:select></td>
			<td><html:select property="divCertAgen3" tabindex="111"
					styleClass="chosen-select" styleId="divCertAgen3"
					onchange="checkDiverseAgent()">
				</html:select></td>
			<td><html:select property="certType3" styleId="certType3"
					styleClass="chosen-select" tabindex="112">
				</html:select></td>
			<td><html:text property="certificationNo3" tabindex="113"
					styleId="certificationNo3" alt="" styleClass="main-text-box" /></td>
			<td><html:text property="effDate3" styleId="effDate3" alt="Please click to select date"
					styleClass="main-text-box" tabindex="114" /></td>
			<td><html:text property="expDate3" styleId="expDate3" alt="Please click to select date"
					styleClass="main-text-box" tabindex="115" /></td>
			<td>
				<div class="fileUpload btn btn-primary">
					<span>Browse</span>
					<html:file property="certFile3" styleId="certfile3" tabindex="116"
						styleClass="upload">
					</html:file>
				</div> <span id="uploadFile3" style="overflow: hidden;"></span>
			</td>
		</tr>
		<tr class="even">
			<td colspan="7">
				<div class="row-wrapper" style="width: 50%">
					<div class="label-col-wrapper">Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ethnicity3" styleId="ethnicity3"
							styleClass="chosen-select-width" tabindex="117"
							style="height: 2%;" disabled="true">
							<html:option value="">--Select--</html:option>
							<html:optionsCollection name="ethnicities" label="ethnicity"
								value="id" />
						</html:select>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>
<div class="clear"></div>
<style>
#moreInfo {
	border-collapse: separate;
}
</style>
<div class="clear"></div>