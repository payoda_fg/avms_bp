<%@page import="com.fg.vms.admin.model.VMSObjects"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript">
<!--
//Function to reset the form
function clearfields() {
	window.location = "viewconfig.do?parameter=view";
}
//-->
</script>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Update
				Object
			</h2>
		</div>
		<div id="table-holder" style="margin: 0 1.5%;">
			<html:form action="/updateobject?parameter=updateObject">
				<html:hidden property="id" name="configurationForm" />
				<table style="padding-left: 30%;" cellspacing="10" cellpadding="5">
					<tr>
						<td>Object Name</td>
						<td><html:text property="objectName" name="configurationForm"
								alt=""></html:text><span class="error"><html:errors
									property="objectName"></html:errors></span></td>
					</tr>
					<tr>
						<td>Object Type</td>
						<td><html:text property="objectType" name="configurationForm"
								alt=""></html:text><span class="error"><html:errors
									property="objectType"></html:errors></span></td>
					</tr>
					<tr>
						<td>Object Description</td>
						<td><html:textarea property="objectDescription"
								name="configurationForm" alt=""></html:textarea><span
							class="error"><html:errors property="objectDescription"></html:errors></span></td>
					</tr>
					<tr>
						<td>IsActive</td>
						<td>Yes&nbsp;<html:radio property="isActive" value="1"
								name="configurationForm"></html:radio>&nbsp;&nbsp; No&nbsp;<html:radio
								property="isActive" value="0" name="configurationForm"></html:radio>
							<span class="error"><html:errors property="isActive"></html:errors></span></td>
					</tr>
				</table>
				<logic:iterate id="privilege" name="privileges">

					<logic:match value="Objects" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="add">
							<div style="padding: 0 0 1% 38%;">
								<html:submit value="Update" styleClass="btTxt submit"
									styleId="submit"></html:submit>
								<html:reset value="Cancel" styleClass="btTxt submit"
									onclick="clearfields()"></html:reset>
							</div>
						</logic:match>
					</logic:match>

				</logic:iterate>

			</html:form>
		</div>
		<logic:iterate id="privilege" name="privileges">

			<logic:match value="Objects" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">
					<h2>
						<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Available
						Objects
					</h2>
					<div id="box2" style="max-height: 150px; overflow: auto;"
						onload="clearForms()">
						<table width="100%">
							<bean:size id="size" name="objects" />
							<logic:greaterThan value="0" name="size">
								<thead>
									<tr>
										<th>Object Name</th>
										<th>Object Type</th>
										<th>Object Description</th>
										<th>IsActive</th>
										<th>Actions</th>
									</tr>
								</thead>
								<logic:iterate name="objects" id="objectlist">
									<tbody>
										<tr>
											<td><bean:write name="objectlist" property="objectName" /></td>
											<td><bean:write name="objectlist" property="objectType" /></td>
											<td><bean:write name="objectlist"
													property="objectDescription" /></td>
											<td>Yes</td>
											<bean:define id="objectId" name="objectlist" property="id"></bean:define>
											<td><logic:iterate id="privilege" name="privileges">

													<logic:match value="Objects" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="modify">

															<html:link action="/retriveobject.do?parameter=retrive"
																paramId="id" paramName="objectId">Edit</html:link>
														</logic:match>
													</logic:match>

												</logic:iterate> <logic:iterate id="privilege" name="privileges">

													<logic:match value="Objects" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="delete">
												| <html:link
																action="/deleteobject.do?parameter=deleteobject"
																paramId="id" paramName="objectId"
																onclick="return confirm_delete();">Delete</html:link>
														</logic:match>
													</logic:match>

												</logic:iterate></td>
										</tr>
									</tbody>
								</logic:iterate>
							</logic:greaterThan>
							<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
						</table>
					</div>
				</logic:match>
			</logic:match>

		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:match value="Objects" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view object</h3>
						</div>
				</logic:match>
			</logic:match>

		</logic:iterate>

	</div>
</div>