<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%@page import="com.fg.vms.util.CommonUtils"%>
<%@page import="com.fg.vms.customer.model.VendorOtherCertificate"%>

<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script>
	var imgPath = "jquery/images/calendar.gif";
	datePickerOtherExp();
	function checkDiverseType(element) {
		var rowCount = $('#otherCertificate tr').length - 1;
		for ( var i = 1; i <= rowCount; i++) {
			var id = "otherCertType" + i;
			if (id == element) {
			} else {
				var v1 = $('#' + id).val();
				var v2 = $('#' + element).val();
				if (v1 == v2) {
					alert("Certificate should be unique");
					document.getElementById(element).selectedIndex = 0;
				}
			}
		}
	}
	$(document).ready(function() {
	$("#otherCert1").on("change",function() {document.getElementById("otherFile1").innerHTML = this.value;}); });
</script>
<h3>Quality / Other Certificate</h3>
<table width="100%" border="0" class="main-table" id="otherCertificate">
	<tr>
		<td class="header">Certification Type</td>
		<td class="header">Expire Date</td>
		<td class="header">Certificate</td>
		<td class="header"></td>
		<td class="header">Action</td>
	</tr>

	<logic:present name="otherCertificates">
		<bean:size id="size" name="otherCertificates" />
		<logic:equal value="0" name="size">
			<tr>
				<td><html:select styleId="otherCertType1" tabindex="127"
						property="otherCertType1" onchange="checkDiverseType(this.id);"
						styleClass="chosen-select">
						<html:option value="-1">----- Select -----</html:option>
						<logic:present name="qualityCertificates">
							<logic:iterate id="qualityCertificate" name="qualityCertificates">
								<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
								<html:option value="${id}">
									<bean:write name="qualityCertificate"
										property="certificateName"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select></td>
				<td><html:text property="otherExpiryDate" styleId="othexpDate1"
						styleClass="othexpDate1 main-text-box" alt="Please click to select datePlease click to select date" tabindex="128" style="width:45%;" /></td>
				<td>
					<div class="fileUpload btn btn-primary">
						<span>Browse</span>
						<html:file property="otherCert[0]" styleId="otherCert1" tabindex="129"
							styleClass="upload">
						</html:file>
					</div> <span id="otherFile1"></span>
				</td>
				<td></td>
				<td><a onclick="return clearOtherCertificate('1','0');" href="#">Delete</a></td>
			</tr>
			</logic:equal>
			</logic:present>
			<logic:notPresent name="otherCertificates">
			<tr>
				<td><html:select styleId="otherCertType1" tabindex="127"
						property="otherCertType1" onchange="checkDiverseType(this.id);"
						styleClass="chosen-select">
						<html:option value="-1">----- Select -----</html:option>
						<logic:present name="qualityCertificates">
							<logic:iterate id="qualityCertificate" name="qualityCertificates">
								<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
								<html:option value="${id}">
									<bean:write name="qualityCertificate"
										property="certificateName"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select></td>
				<td><html:text property="otherExpiryDate" styleId="othexpDate1"
						styleClass="othexpDate1 main-text-box" alt="Please click to select date" tabindex="128" style="width:45%;" /></td>
				<td>
					<div class="fileUpload btn btn-primary">
						<span>Browse</span>
						<html:file property="otherCert[0]" styleId="otherCert1" tabindex="129"
							styleClass="upload">
						</html:file>
					</div> <span id="otherFile1"></span>
				</td>
				<td></td>
				<td><a onclick="return clearOtherCertificate('1','0');" href="#">Delete</a></td>
			</tr>
			</logic:notPresent>
	<logic:present name="otherCertificates">
		<logic:iterate id="otherCert" name="otherCertificates" indexId="index">
			<tr>
				<%
					int i = index + 1;
							String certType = "-1";
				%>
				<logic:greaterThan value="0" name="otherCert"
					property="certificationType">
					<bean:define id="type" name="otherCert"
						property="certificationType"></bean:define>
					<%
						certType = type.toString();
					%>
				</logic:greaterThan>
				<td><html:select onchange="checkDiverseType(this.id);"
						styleId='<%="otherCertType" + i%>' property="otherCertType1"
						styleClass="chosen-select" value="<%=certType%>">
						<html:option value="-1">----- Select -----</html:option>
						<logic:present name="qualityCertificates">
							<logic:iterate id="qualityCertificate" name="qualityCertificates">
								<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
								<html:option value="<%=id.toString()%>">
									<bean:write name="qualityCertificate"
										property="certificateName"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select></td>
				<%
					String date = CommonUtils
									.convertDateToString(((VendorOtherCertificate) otherCert)
											.getExpiryDate());
				%>
				<td><html:text property="otherExpiryDate" styleId='<%="othexpDate" + i%>'
						value="<%=date.toString()%>"
						styleClass="othexpDate1 main-text-box" alt="Please click to select date" style="width:45%;" /></td>
				<td>
					<div class="fileUpload btn btn-primary">
						<span>Browse</span>
						<html:file property='<%="otherCert[" + index+"]"%>' styleId='<%="otherCert" + i%>'
							tabindex="130" styleClass="upload">
						</html:file>
						<script>
						$("#otherCert" + <%=i%>)
						.on(
								"change",
								function() {
									document.getElementById("otherFile"
											+ <%=i%>).innerHTML = this.value;
								});
						</script>
					</div> <span id='<%="otherFile" + i%>'></span>
				</td>
				<td><logic:present name="otherCert" property="filename">
						<bean:define id="id" property="id" name="otherCert"></bean:define>
						<html:link href="download_othercertificate.jsp" paramId="id"
							 paramName="id" styleClass="downloadFile" styleId='<%="downloadOtherFile" + i%>'>Download</html:link>
					</logic:present></td><bean:define id="oID"  property="id" name="otherCert"/>
					<td><a onclick="return clearOtherCertificate('<%=i%>','<%=oID %>');" href="#">Delete</a></td>
			</tr>
		</logic:iterate>
	</logic:present>
</table>
<INPUT id="cmd1" type="button" value="Add Quality Certificate"
	class="btn" onclick="addRow('otherCertificate')" />
<div class="clear"></div>
<div class="clear"></div>
<script type="text/javascript">
<!--
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = 5;//table.rows[1].cells.length;
		for ( var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[1].cells[i].innerHTML;
			if (i == 2) {
				newcell.innerHTML = '<div class="fileUpload btn btn-primary"><span>Browse</span>'
						+ '	<input name="otherCert['+(rowCount-1)+']" type="file" id="otherCert'
						+ rowCount
						+ '" class="upload"/> </div> <span id="otherFile'+rowCount+'"></span>';
				$("#otherCert" + rowCount).on("change",	function() {
									document.getElementById("otherFile"
											+ rowCount).innerHTML = this.value;
								});
			}else if(i==0) {
				newcell.innerHTML = '<select name="otherCertType1" id="otherCertType'+ rowCount+'" class="chosen-select fields" >'
				+ $('#otherCertType1').html() + '</select>';
				newcell.childNodes[0].selectedIndex = 0;
				//newcell.childNodes[0].id = "otherCertType" + rowCount;
				$('#otherCertType'+rowCount).select2();
			}else if(i==4){
				newcell.innerHTML = '<a onclick="return clearOtherCertificate(\''+rowCount+'\',\'0\')" href="#">Delete</a>';
			}else if(i==3){
				newcell.innerHTML = '';
			}else if(i==1){
				newcell.innerHTML = " <input class='othexpDate1 main-text-box ' alt='Please click to select date' id='othexpDate"+rowCount+"' name='otherExpiryDate' style='width:45%;'/>";
				newcell.childNodes[0].value = "";
			}
		}
	}
	$('body').on('focus', ".othexpDate1", function() {
		$(this).datepicker();
	});
//-->
</script>