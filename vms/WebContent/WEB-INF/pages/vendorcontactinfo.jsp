<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script>
	function allowed_to_login() {

		if (document.getElementById("loginAllowed").checked == true) {
			document.getElementById("loginDisplay").style.visibility = "visible";
		} else {
			document.getElementById("loginDisplay").style.visibility = "hidden";
		}
	}
</script>
<div id="successMsg">
	<html:messages id="msg" property="vendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="transactionFailure" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="panelCenter_1">
	<h3>Contact Information</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="firstName" alt=""
						styleId="firstName" tabindex="2" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="lastName"
						styleId="lastName" alt="" tabindex="3" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Title</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="designation"
						styleId="designation" alt="" tabindex="4" />
				</div>
				<span class="error"><html:errors property="designation"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contactPhone"
						styleId="contactPhone" alt="" tabindex="5" />
				</div>
				<span class="error"><html:errors property="contactPhone"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contactMobile"
						styleId="contactMobile" alt="Optional" tabindex="6" />
				</div>
				<span class="error"><html:errors property="contactMobile"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">FAX</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contactFax"
						styleId="contactFax" alt="" tabindex="7" />
				</div>
				<span class="error"><html:errors property="contactFax"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email ID</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contanctEmail"
						styleId="contanctEmail" alt="" onchange="ajaxFn(this,'VE');"
						tabindex="8" />
				</div>
				<span class="error"><html:errors property="contanctEmail"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="loginpassword" styleId="loginpassword"
						tabindex="9" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="loginpassword"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Confirm Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="confirmPassword" styleId="confirmPassword"
						tabindex="10" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="confirmPassword"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question</div>
				<div class="ctrl-col-wrapper">
					<html:select property="userSecQn" name="vendorMasterForm"
						styleClass="chosen-select-width" styleId="userSecQn" tabindex="11">
						<html:option value="">--Select--</html:option>
						<html:optionsCollection name="secretQnsList" value="id"
							label="secQuestionParticular" />
					</html:select>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question Answer</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="userSecQnAns"
						styleId="userSecQnAns" alt="" tabindex="12" />
				</div>
				<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
