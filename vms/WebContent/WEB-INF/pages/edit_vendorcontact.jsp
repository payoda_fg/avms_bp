<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.dto.RetriveVendorInfoDto"%>
<%
	RetriveVendorInfoDto vendorInfo = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");
	session.setAttribute("vendorContacts",
			vendorInfo.getVendorContacts());
	session.setAttribute("vendorMaster", vendorInfo.getVendorMaster());
%>
<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Contact Information" name="privilege" property="objectId.objectName">
		<header class="card-header">
				<h2 class="card-title pull-left">
			<img src="images/user-group.gif" alt="" /> Vendor Contacts</h2>
			<logic:equal value="1" name="privilege" property="modify">	
				<logic:equal value="1" name="privilege" property="add">
					<html:link action="/vendorContact.do?method=vendorcontact"
						styleClass="btn" style="float:right">Add Contact</html:link>
				</logic:equal>
			</logic:equal>			
		</header>
		
		<div class="clear"></div>

		<table class="main-table table table-bordered table-striped mb-0" width="100%" border="0">
			<bean:size id="size" name="vendorContacts" />
			<logic:greaterThan value="0" name="size">
				<thead>
					<tr>
						<td class="">First Name</td>
						<td class="">Last Name</td>
						<td class="">Gender</td>
						<td class="">Title</td>
					    <td class="">Phone Number</td>
						<td class="">Fax</td>
						<td class="">Email</td>
						<td class="">Business Contact</td>
						<td class="">Actions</td>
					</tr>
				</thead>
				
				<tbody>
					<logic:iterate id="vendorContacts" name="vendorContacts" indexId="index">
						<logic:equal value="1" name="vendorContacts" property="isActive">
							<tr>
								<td><bean:write name="vendorContacts" property="firstName" /></td>
								<td><bean:write name="vendorContacts" property="lastName" /></td>
								<td><logic:equal value="M" name="vendorContacts"
									property="gender">Male</logic:equal> <logic:equal value="F"
									name="vendorContacts" property="gender">Female</logic:equal> <logic:equal
									value="T" name="vendorContacts" property="gender">Transgender</logic:equal>
								</td>
								<td><bean:write name="vendorContacts" property="designation" /></td>
								<td><bean:write name="vendorContacts" property="phoneNumber" /></td>
								<td><bean:write name="vendorContacts" property="fax" /></td>
								<td><bean:write name="vendorContacts" property="emailId" /></td>
								<td align="center">
									<logic:equal value="1"  name="vendorContacts" property="isBusinessContact">Yes</logic:equal>
									<logic:equal value="0"  name="vendorContacts" property="isBusinessContact">No</logic:equal>
								</td>
								<bean:define id="vendorId" name="vendorContacts" property="id"></bean:define>
								<td>
									<html:link action="/viewvendorcontact.do?method=viewContact"
										paramId="vendorId" paramName="vendorId">View </html:link>							
									<logic:equal value="0" name="vendorContacts" property="isPreparer">
										<logic:equal value="1" name="privilege" property="modify">
											<logic:equal value="1" name="privilege" property="delete">
												<html:link action="/viewvendorcontact.do?method=deleteContact"
													paramId="vendorId" paramName="vendorId" onclick="return confirm_delete();">| 
													<img src='images/deleteicon.jpg' alt='Delete' />
												</html:link>
											</logic:equal>
										</logic:equal>
									</logic:equal>
									<logic:equal value="1" name="vendorContacts" property="isPreparer">
										<span style="color: #009900;font-weight: bold;">Primary</span>
									</logic:equal>
								</td>
							</tr>
						</logic:equal>
					</logic:iterate>
				</tbody>
			</logic:greaterThan>
			<logic:equal value="0" name="size">
				No such Records
			</logic:equal>
		</table>
		<div class="clear"></div>
	</logic:equal>
</logic:iterate>