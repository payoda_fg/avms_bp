<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script>
	$(document).ready(function() {
		
		if ($("input:radio[name=expOilGas]:checked").val() == 1) {
			$('#oilGasExp').css('display', 'block');
		} else {
			$('#oilGasExp').css('display', 'none');
		}
		if ($("input:radio[name=isBpSupplier]:checked").val() == 1) {
			$('#bpsupplier').css('display', 'block');
		} else {
			$('#bpsupplier').css('display', 'none');
		}
		if ($("input:radio[name=isCurrentBpSupplier]:checked").val() == 1) {
			$('#bpFranchise').css('display', 'block');
		} else {
			$('#bpFranchise').css('display', 'none');
		}
		if ($("input:radio[name=isFortune]:checked").val() == 1) {
			$('#fortune').css('display', 'block');
		} else {
			$('#fortune').css('display', 'none');
		}
		$('input[name=expOilGas]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#oilGasExp').css('display', 'block');
			} else {
				$('#oilGasExp').css('display', 'none');
			}
		});
		$('input[name=isBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpsupplier').css('display', 'block');
			} else {
				$('#bpsupplier').css('display', 'none');
			}
		});
		$('input[name=isCurrentBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpFranchise').css('display', 'block');
			} else {
				$('#bpFranchise').css('display', 'none');
			}
		});
		$('input[name=isFortune]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#fortune').css('display', 'block');
			} else {
				$('#fortune').css('display', 'none');
			}
		});
	});
	function setBusinessValue(ele, value) {
		ele.value = value;
		//if (id != null && id != '') {
		//	$('#'+id).val('');
		//}
	}
</script>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Business Biography</h3>
	<div class="form-box">
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Do you have current experience
					in the Oil &amp; Gas Industry?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="expOilGas" value="bussines[0]" idName="editVendorMasterForm" 
						tabindex="225" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="expOilGas" value="bussines[1]"
						tabindex="226" idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="oilGasExp" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please list your clients below</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="listClients" rows="5" cols="30"
						tabindex="227" styleClass="main-text-area"></html:textarea>
				</div>
			</div>
		</div>

		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Are you a BP Supplier?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isBpSupplier" value="bussines[2]" tabindex="228"
						idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isBpSupplier" value="bussines[3]" tabindex="229"
						idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="bpsupplier" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">BP Contact Name:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="bpContact" styleId="bpContact"
						styleClass="text-box" tabindex="230"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">BP Division:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="bpDivision" styleId="bpDivision"
						styleClass="text-box" tabindex="231"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">BP Contact Phone:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="bpContactPhone" alt="" styleClass="text-box"
						styleId="bpContactPhone" style="width:45%;" tabindex="232" />
					<div class="label-col-wrapper" style="width: 10%;">Ext</div>
					<html:text property="bpContactPhoneExt" styleId="bpContactPhoneExt" tabindex="233"
						size="3" styleClass="text-box" alt="Optional" style="width:22%;"></html:text>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Are you a current 1st or 2nd
					Tier Supplier to BP or a BP Franchise?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isCurrentBpSupplier" value="bussines[4]" tabindex="234"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isCurrentBpSupplier" value="bussines[5]" tabindex="235"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="bpFranchise" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please describe the scope of
					services. Include contract value, locations serviced, and other
					pertinent information:</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="currentBpSupplierDesc" rows="5" cols="30"
						styleClass="main-text-area" tabindex="236"></html:textarea>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Are you currently doing
					business with any Fortune 500 companies?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isFortune" value="bussines[6]" tabindex="237"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)" >Yes</html:radio>
					<html:radio property="isFortune" value="bussines[7]" tabindex="238"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="fortune" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please list your Fortune 500
					clients below:</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="fortuneList" rows="5" cols="30"
						styleClass="main-text-area" tabindex="239"></html:textarea>
				</div>
			</div>
		</div>

		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Does your company utilize union
					represented workforce?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isUnionWorkforce" value="bussines[8]" tabindex="240"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isUnionWorkforce" value="bussines[9]" tabindex="241"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
				</div>
			</div>
		</div>

		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">If yes, does your company also
					utilize non-union represented workforce?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isNonUnionWorkforce" value="bussines[10]" tabindex="242"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isNonUnionWorkforce" value="bussines[11]" tabindex="243"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Safety</h3>
	<div class="form-box">
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company PICS certified?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isPicsCertified" value="bussines[12]" tabindex="244"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isPicsCertified" value="bussines[13]" tabindex="245"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					<html:radio property="isPicsCertified" value="bussines[14]" tabindex="246"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company ISO 9000
					certified?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isIso9000Certified" value="bussines[15]" tabindex="246"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isIso9000Certified" value="bussines[16]" tabindex="247"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					<html:radio property="isIso9000Certified" value="bussines[17]" tabindex="248"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company ISO 14001
					certified?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isIso14000Certified" value="bussines[18]" tabindex="249"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isIso14000Certified" value="bussines[19]" tabindex="250"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					<html:radio property="isIso14000Certified" value="bussines[20]" tabindex="251"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Have you received any OSHA
					citations in the last three years?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isOshaRecd" value="bussines[21]" tabindex="252"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isOshaRecd" value="bussines[22]" tabindex="253"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					<html:radio property="isOshaRecd" value="bussines[23]" tabindex="254"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Does your company have an
					Occupational Safety &amp; Health Administration (OSHA) 3-yr average of
					2.0 or less?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isOshaAvg" value="bussines[24]" tabindex="255"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isOshaAvg" value="bussines[25]" tabindex="256"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					, please specify
					<html:text property="isOshaAvgTxt" styleClass="text-box" tabindex="257"></html:text>
					<html:radio property="isOshaAvg" value="bussines[26]" tabindex="258"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Does your company have an
					Experience Modification Rate (EMR) of 1.0 or less?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isEmr" value="bussines[27]" tabindex="259"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isEmr" value="bussines[28]" tabindex="260"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					, please specify
					<html:text property="isEmrTxt" styleClass="text-box" tabindex="261"></html:text>
					<html:radio property="isEmr" value="bussines[29]" tabindex="262"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Will your company agree to BP's
					4 hour onsite safety training and have a 10-hr OSHA card for all
					contractor personnel working at the refinery?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isOshaAgree" value="bussines[30]" tabindex="263"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isOshaAgree" value="bussines[31]" tabindex="264"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					<html:radio property="isOshaAgree" value="bussines[32]" tabindex="265"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company in compliance
					with the Northwest Indiana Business Roundtable substance abuse
					policy?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isNib" value="bussines[33]" tabindex="266"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
					<html:radio property="isNib" value="bussines[34]" tabindex="267"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
					<html:radio property="isNib" value="bussines[35]" tabindex="268"
					idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please provide a snapshot of
					major jobs / projects completed:</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="jobsDesc" rows="5" cols="30"
						styleClass="main-text-area" tabindex="269"></html:textarea>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Customer / Location</div>
				<div class="ctrl-col-wrapper">
					<html:text property="custLocation" styleId="custLocation"
						styleClass="text-box" tabindex="270"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Customer contact</div>
				<div class="ctrl-col-wrapper">
					<html:text property="custContact" styleId="custContact"
						styleClass="text-box" tabindex="271"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Telephone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="telephone" styleId="telephone"
						styleClass="text-box" tabindex="272"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Type of work</div>
				<div class="ctrl-col-wrapper">
					<html:text property="workType" styleId="workType"
						styleClass="text-box" tabindex="273"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Size $</div>
				<div class="ctrl-col-wrapper">
					<html:text property="size" styleId="size" tabindex="274"
					 styleClass="text-box" onblur="moneyFormatToUS('formattedValue','size');" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>