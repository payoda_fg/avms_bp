<%@page import="com.fg.vms.admin.model.Customer"%>
<%@page import="com.fg.vms.admin.dto.CustomerInfoDto"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%
	CustomerInfoDto customerInfoDto = (CustomerInfoDto) session
			.getAttribute("customerInfo");
	Customer customer = customerInfoDto.getCustomer();
%>
<!-- For JQuery Panel -->
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript">
	function allowed_to_login() {

		if (document.getElementById("loginAllowed").checked == true) {
			document.getElementById("loginId").style.visibility = 'visible';
		} else {
			document.getElementById("loginId").style.visibility = 'hidden';
		}

	}
</script>

<div id="form-container" style="height: 100%; width: 95%;"
	onload="allowed_to_login();">
	<div id="form-container-in" style="height: 95%; width: 100%;">
		<html:form action="/addContact?method=addContact" styleClass="AVMS">
			<%
				session.setAttribute("customer", customer);
			%>

			<html:javascript formName="contactForm" />


			<div class="panelCenter_1"
				style="width: 95%; padding: 10px 0 0 20px; font-size: 11px;">
				<h3>Customer Contact Information</h3>
				<table cellpadding="10" cellspacing="5" style="padding-left: 10%;">

					<tr>
						<td>First Name</td>
						<td><html:text property="firstName" alt="" /><span
							class="error"><html:errors property="firstName"></html:errors></span></td>
						<td>Last Name</td>
						<td><html:text property="lastName" alt="Optional" /><span
							class="error"><html:errors property="lastName"></html:errors></span></td>
					</tr>
					<tr>
						<td>Title</td>
						<td><html:text property="designation" alt="Optional" /><span
							class="error"><html:errors property="designation"></html:errors></span></td>
						<td>Phone</td>
						<td><html:text property="contactPhone" alt="" /><span
							class="error"><html:errors property="contactPhone"></html:errors></span></td>
					</tr>
					<tr>
						<td>Mobile</td>
						<td><html:text property="contactMobile" alt="" /><span
							class="error"><html:errors property="contactMobile"></html:errors></span></td>
						<td>FAX</td>
						<td><html:text property="contactFax" alt="Optional" /><span
							class="error"><html:errors property="contactFax"></html:errors></span></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><html:text property="contanctEmail" alt=""
								styleId="emailAddress"
								onblur="return email_validate(this.value)" /><span
							class="error"><html:errors property="contanctEmail"></html:errors></span></td>
						<td>Allowed to Login</td>
						<td><html:checkbox property="loginAllowed"
								styleId="loginAllowed" onclick="allowed_to_login()" value="true" /></td>
					</tr>


				</table>
			</div>
			<div id="loginId"
				style="visibility: hidden; width: 95%; padding: 10px 0 0 20px; font-size: 11px;"
				class="panelCenter_1">
				<h3>User Information</h3>
				<table cellpadding="10" cellspacing="5" style="padding-left: 10%;">

					<tr>
						<td>DisplayName</td>
						<td><html:text property="loginDisplayName" alt="Optional"
								styleId="displayName" /><span class="error"><html:errors
									property="loginDisplayName"></html:errors></span></td>
						<td>loginId</td>
						<td><html:text property="loginId" styleId="loginId"
								alt="Optional" /><span class="error"><html:errors
									property="loginId"></html:errors></span></td>
					</tr>

					<tr>
						<td>Password</td>
						<td><html:password property="loginpassword"
								styleId="loginPwd" /><span class="error"><html:errors
									property="loginpassword"></html:errors></span></td>
						<td>Confirm Password</td>
						<td><html:password property="confirmPassword"
								styleId="loginPwd" /><span class="error"><html:errors
									property="confirmPassword"></html:errors></span></td>
					</tr>
					<tr>
						<td>Secret Question</td>
						<td><html:select property="userSecQn" name="contactForm">
								<html:option value="">----Select---</html:option>
								<bean:size id="size" name="secretQnsList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="secretQn" name="secretQnsList">
										<bean:define id="id" name="secretQn" property="id"></bean:define>
										<html:option value="<%=id.toString() %>">
											<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
										</html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select></td>
						<td>Secret Question Answer</td>
						<td><html:text property="userSecQnAns" alt="Optional" /><span
							class="error"><html:errors property="userSecQnAns"></html:errors></span></td>
					</tr>


				</table>
			</div>
			<div align="center" style="padding: 10px 0 10px 0">
				<html:submit value="Submit" styleClass="btTxt submit"
					styleId="submit"></html:submit>
				<html:reset value="Clear" styleClass="btTxt submit"></html:reset>

			</div>

		</html:form>
		<script type="text/javascript">
			allowed_to_login();
			$(function() {
				$("#tabs").tabs();
			});
		</script>
	</div>
</div>
