<%@page import="com.fg.vms.admin.dto.CustomerInfoDto"%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<div class="panelCenter_1">
	<h3>Personalize your profile</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Select company logo</div>
				<div class="ctrl-col-wrapper">
					<html:file property="companyLogo" alt="" styleClass="text-box"
						styleId="companyLogo" tabindex="38"/>
					<bean:define id="logoPath" property="logoImagePath"
						name="editCustomerForm"></bean:define>
					<img width="100" alt="Company Logo" height="100"
						src="<%=request.getContextPath() %>/images/dynamic/?file=<%=logoPath%>">
				</div>
				<span class="error"><html:errors property="companyLogo"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
