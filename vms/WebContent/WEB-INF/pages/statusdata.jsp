<%@page import="com.fg.vms.customer.model.StatusMaster"%>
<%@page import="com.fg.vms.customer.model.State"%>
<%@page import="java.util.*,java.util.ArrayList"%>

<%
	List<StatusMaster> status = null;
	if (session.getAttribute("status") != null) {
		status = (List<StatusMaster>) session.getAttribute("status");
	}
	StringBuffer buffer = new StringBuffer(
			"<option value=''>- Select -</option>");
	if (status != null && !status.isEmpty()) {
		for (StatusMaster s : status) {
			buffer.append("<option value='" + s.getId() + "'>"
					+ s.getStatusname() + "</option>");
		}
	}

	response.setHeader("Cache-Control", "no-cache");
	response.getWriter().println(buffer);
%>
