
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html class="fixed sidebar-left-collapsed">
<%@page import="com.fg.vms.admin.model.Users"%>

<!--Menupart -->

<div id="bluemenu" class="bluetabs">
	<ul>
		<li><html:link action="customerhome.do?method=customerHomePage">Home</html:link></li>
		<li><a href="#" rel="dropmenu1_b">Manage Vendors</a></li>
		<li><a href="#" rel="dropmenu2_b">Dashboards</a></li>
		<li><a href="#" rel="dropmenu3_b">Query</a></li>
		<li><a href="#" rel="dropmenu6_b">Sourcing</a></li>
		<li><a href="#" rel="dropmenu4_b">Tier 2 Reporting</a></li>
		<li><a href="#" rel="dropmenu5_b">Administration</a></li>
	</ul>
</div>



<!--AdminTab -->
<div id="dropmenu5_b" class="dropmenudiv_b" style="width: auto;">

	<logic:present name="privileges">

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Roles" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewuserrole.do?parameter=viewUserRoles">Roles</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="User" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewuser.do?parameter=viewUsers">Users</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Role Privileges" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/roleprivileges.do?method=rolePrivileges">Role Privileges</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Customer" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/displayCustomer.do?method=display">Customers</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Objects" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewconfig.do?parameter=view">Object Entry</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="NAICS Category Master" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/naicsCategoryView.do?method=view">NAICS Category</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="NAICS Sub-category" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/viewnaicsubcategory.do?parameter=viewSubCategoriesById&categoryId=0">NAICS Sub-category</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="NAICS Master" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/viewnaicsmaster.do?parameter=viewNAICSMasterPage">NAICS Code</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="View Certificate" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewcertificate.do?parameter=viewCertificate">View Certificate</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Certifying Agency" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewagencies.do?method=view">Certifying Agency</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="editinformation" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/retrivecustomer?method=retriveCustomerInfoFromCustomerDB">Edit Customer Info</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Performance Assessment Template" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/questions.do?method=showPage">Performance Assessment Template</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

	</logic:present>

	<!--  Vendor user menu -->


	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Roles" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="visible">
					<html:link action="/viewvendorrole.do?parameter=viewVendorRoles">Roles</html:link>
				</logic:match>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="User" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewvendor.do?parameter=viewVendors">Users</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Privileges" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/vendorroleprivileges.do?method=rolePrivileges">Role Privileges</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>


		<bean:define id="vendorId" name="vendorUser" property="vendorId.id"></bean:define>
		<html:link action="/retrieveprimesupplier.do?method=showSupplierContactDetails&requestString=prime"
			paramId="id" paramName="vendorId"> Edit Profile</html:link>


	</logic:present>


</div>


<div id="dropmenu1_b" class="dropmenudiv_b" style="width: auto;">
	<logic:present name="privileges">
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewVendor.do?method=view">Create Vendor</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="View Vendors" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewVendors.do?method=showVendorSearch">View Vendors</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Approve Vendors" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/listanonymousvendors.do?parameter=listVendors">Anonymous Vendor Activation</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Vendor Approve" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="visible">
					<html:link
						action="/approvevendorpage.do?method=showMailNotificationPage">Vendor Approval</html:link>
				</logic:match>
			</logic:match>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Mail Notifications" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/searchvendor.do?method=searchVendor">General Email Notification</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Assessment Email Notification" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/capabalityAssessmentSearchvendor.do?method=searchAssessmentVendor">Assessment Email Notification</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Assessment Email Details" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/assessmentemaildetails.do?method=viewemaildetails&getTemplate=0">Assessment Email Details</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Assessment Review" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/assessmentreview.do?method=reviewTemplate">Capability Assessment Review</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Assessment Score Card" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/assessmentscore.do?method=viewscore&getTemplateForScore=0">Assessment Score Card</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

	</logic:present>

	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewVendor.do?method=view&tire2vendor=yes">Create Vendor</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="View Vendors" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewSubVendors.do?method=showSubVendorSearch">View Vendors</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Capability Assessment" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/capabilityAssessment.do?method=showCapabilityAssessmentPage">Capability Assessment</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

	</logic:present>
	<!--<html:link action="/viewItem.do?parameter=viewItems">Create Items</html:link>-->
	<!--<html:link action="/viewItem.do?parameter=viewItems">Create Items</html:link>
	-->

</div>


<div id="dropmenu6_b" class="dropmenudiv_b" style="width: auto;">
	<logic:present name="privileges">
		<logic:iterate id="privilege" name="privileges">

			<logic:equal value="RFI Dashboard" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/rfiinformation.do?method=showRFIDashboard">RFI Dashboard</html:link>
				</logic:equal>
			</logic:equal>

		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:equal value="Dashboard RFP" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/rfiinformation.do?method=showRFPDashboard">RFP Dashboard</html:link>
				</logic:equal>
			</logic:equal>

		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">

			<logic:equal value="Create RFI" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/rfiinformation.do?method=showRFIPage">Create RFI</html:link>
				</logic:equal>
			</logic:equal>

		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:equal value="RFP Create" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/rfiinformation.do?method=showRFPPage">Create RFP</html:link>
				</logic:equal>
			</logic:equal>

		</logic:iterate>
	</logic:present>

</div>
<div id="dropmenu4_b" class="dropmenudiv_b" style="width: auto;">
	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Spend Upload" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/spendupload.do?method=showUploadPage">Spend Upload</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Spend Upload" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="view">
					<html:link
						action="/viewspenddata.do?method=retrieveUploadedSpendData">Spend Dashboard</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

	</logic:present>
</div>


<div id="dropmenu2_b" class="dropmenudiv_b" style="width: auto;">
	<logic:present name="privileges">

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Spend Report" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/t2supplierSpendReport.do?method=showT2SpendReport">Spend Report</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Diversity Report" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/t2supplierSpendReport.do?method=diversitySpendReport">Spend Report By Diversity</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Indirect Spend Report" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/indirectSpendReport.do?method=indirectSpendReportPage">Indirect Spend Report</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
	</logic:present>

</div>
<script type="text/javascript">
	//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
	tabdropdown.init("bluemenu");
</script>
</html>