<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	function clearFields() {
		window.location = "viewSubVendors.do?method=showSubVendorSearch";
	}
</script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<style>
<!--
div.wrapper {
    overflow:hidden;
    overflow-y: scroll;
    height: 100px; // change this to desired height
}
-->
</style>
<div class="page-title">
	<img src="images/VendorSearch.gif" />&nbsp;&nbsp;Search Vendor
</div>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:equal value="View Vendors" name="privilege"
		property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
			<div class="form-box">

				<html:form action="/viewSubVendors.do?method=subVendorSearch">
					<html:javascript formName="searchVendorForm" />
					<%@include file="../customerpages/search.jsp"%>
				</html:form>
			</div>

			<div class="page-title">
				<img id="show" src="images/icon-registration.png" />&nbsp;Following
				are your Vendors

			</div>
			
			<table width="100%" border="0" class="main-table">
				<logic:present property="vendorsList" name="searchVendorForm">
					<bean:size id="size" property="vendorsList" name="searchVendorForm" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="header">Vendor Name</td>
								<td class="header">Country</td>
								<td class="header">NAICS</td>
								<td class="header">Duns Number</td>
								<td class="header">Region</td>
								<td class="header">State</td>
								<td class="header">City</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate property="vendorsList" name="searchVendorForm"
								id="list">
								<tr>
									<td><font class="fontbn2"> <bean:define
												id="vendorId" name="list" property="id"></bean:define> <html:link
												action="/retriveVendor?method=retriveT2Vendor" paramId="id"
												paramName="vendorId">
												<bean:write name="list" property="vendorName" />
											</html:link>
									</font></td>
									<td><bean:write name="list" property="countryName" /></td>
									<td><bean:write name="list" property="naicsCode" /></td>
									<td><bean:write name="list" property="duns" /></td>
									<td><bean:write name="list" property="region" /></td>
									<td><bean:write name="list" property="stateName" /></td>
									<td><bean:write name="list" property="city" /></td>
								</tr>

							</logic:iterate>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
						<tr>
							<td>No such Records</td>
						</tr>
					</logic:equal>
				</logic:present>
				</tbody>
			</table>
			<div class="clear"></div>

		</logic:equal>
	</logic:equal>
</logic:iterate>

<logic:iterate id="privilege" name="rolePrivileges">
	<logic:equal value="View Vendors" name="privilege"
		property="objectId.objectName">
		<logic:equal value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have no rights to view vendor</h3>
			</div>
		</logic:equal>
	</logic:equal>
</logic:iterate>



