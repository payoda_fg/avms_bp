<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script type="text/javascript">
<!--
    var imgPath = "jquery/images/calendar.gif";
	$(document).ready(function() {
		$.datepicker.setDefaults({
// 			showOn : 'both',
// 			buttonImageOnly : true,
// 			buttonImage : imgPath,
			changeYear : true,
			dateFormat : 'mm/dd/yy'

		});
		$("#expirationDate").datepicker();

	});
//-->
</script>
<div class="clear"></div>
<div class="panelCenter_1">
	<h3>Vendor Insurance</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Insurance</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="insurance" styleId="insurance"
						styleClass="text-box" alt="Optional" tabindex="179"/>
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Limit</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="limit" styleId="limit" styleClass="text-box"
						alt="Optional" tabindex="180"/>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Provider</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="provider" styleId="provider"
						styleClass="text-box" alt="Optional" tabindex="181" />
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Carrier Name</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="carrierName" styleId="carrierName"
						styleClass="text-box" alt="Optional" tabindex="182" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Expiration Date</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="expirationDate" styleId="expirationDate"
						styleClass="text-box" alt="Please click to select date" tabindex="183" />
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Policy Number</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="policyNumber" styleId="policyNumber"
						styleClass="text-box" alt="Optional" tabindex="184" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Additional Insured</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="additionalInsured" styleId="additionalInsured"
						styleClass="text-box" alt="Optional" tabindex="185" />
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Agent</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="agent" styleId="agent" styleClass="text-box"
						alt="Optional" tabindex="186" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Phone</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="phoneInsurance" styleId="phoneInsurance"
						styleClass="text-box" alt="Optional" tabindex="187" />
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Extension</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extInsurance" styleId="extInsurance"
						styleClass="text-box" alt="Optional" style="width: 25%;" tabindex="188" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Fax</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="faxInsurance" styleId="faxInsurance"
						styleClass="text-box" alt="Optional" tabindex="189" />
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<label class="lable">Status</label>
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="status" styleId="status" styleClass="text-box"
						alt="Optional" tabindex="190" />
				</div>
			</div>
		</div>

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<html:file property="insuranceFile" styleId="insuranceFile" tabindex="191">Browse
					</html:file>
				</div>

			</div>
			<logic:present name="insuranceId">
				<div class="row-wrapper form-group row">
					<div class="ctrl-col-wrapper">
						<html:link href="insuranceDocDownloader.jsp" paramId="insuranceId"
							paramName="insuranceId" styleId="downloadFile2"
							styleClass="downloadFile" style="cursor:pointer;color:blue;" tabindex="192">Download</html:link>
					</div>
				</div>
			</logic:present>
		</div>
	</div>
	<div class="clear"></div>
</div>
