<%@page import="com.fg.vms.admin.dao.impl.LoginDaoImpl"%>
<%@page import="com.fg.vms.admin.dao.LoginDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.admin.model.Users"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>



<!-- <link href="css/style.css" type="text/css" rel="stylesheet"></link> -->
<link href="css/form-style.css" type="text/css" rel="stylesheet"></link>
<link href="css/tableStyle.css" type="text/css" rel="stylesheet"></link>
<link href="css/tabber.css" type="text/css" rel="stylesheet"></link>
<link href="css/tableStyle-RFQ.css" type="text/css" rel="stylesheet"></link>
<link href="css/Color_custom.css" type="text/css" rel="stylesheet"></link>
<link href="css/bluetabs.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />

<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/dropdowntabs.js"></script>
<script type="text/javascript" src="js/dropdown_menu_hack.js"></script>
<script type="text/javascript" src="js/tabber.js"></script>
<script type="text/javascript" src="js/jscolor.js"></script>
<script type="text/javascript" src="js/customerConfig.js"></script>


<script src="jquery/js/jquery-1.7.2.min.js"></script>
<script src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>
<script src="jquery/ui/jquery.ui.autocomplete.js"></script>

<!-- Added for fixed table header -->

<!-- <link href="css/jquery.tablescroll.css" type="text/css" rel="stylesheet"></link> -->
<script type="text/javascript" src="jquery/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript">
	ddaccordion.init({
		headerclass : "submenuheader", //Shared CSS class name of headers group
		contentclass : "submenu", //Shared CSS class name of contents group
		revealtype : "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
		mouseoverdelay : 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
		collapseprev : true, //Collapse previous content (so only one open at any time)? true/false 
		defaultexpanded : [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
		onemustopen : false, //Specify whether at least one header should be open always (so never all headers closed)
		animatedefault : false, //Should contents open by default be animated into view?
		persiststate : true, //persist state of opened contents within browser session?
		toggleclass : [ "", "active" ], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
		//Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
		animatespeed : "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
		oninit : function(headers, expandedindices) { //custom code to run when headers have initalized

		},
		onopenclose : function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
			//do nothing

		}
	});
</script>



<header>
	<!--Header Pane Start Here-->
	<div class="page-wrapper">
		<header class="header">
		<div class="logo-container">
			<logic:present name="userDetails">
				<bean:define id="logoPath" name="userDetails"
					property="settings.logoPath"></bean:define>
				<a href="#"><img src="<%=request.getContextPath() %>/images/dynamic/?file=<%=logoPath%>" height="80px" /></a>
			</logic:present>
		</div>
		</header>
		<section class="welcome_bg">
			<aside class="welcome-text">
				<b>Welcome:</b> <span><logic:present name="currentUser">
						<bean:define id="loginId" name="currentUser" property="firstName"></bean:define>
						<bean:write name="currentUser" property="userName" />
					</logic:present></span>
			</aside>

			<aside class="top-links">

				<html:link action="adminHome.do?method=adminHomePage">Home</html:link>
				|<a href="#">FAQ</a>|<a href="#">Contact Us</a> <!-- | <a href="#">Browse
					Cards</a> -->|
				<html:link action="/logout.do">Log out</html:link>
			</aside>
		</section>
	</div>
	<!--Header Pane End Here-->
</header>

<!--Product Logo -->
<%-- <div id="product-logo">
	<img src="images/AVMS-logo.png" alt="VMS logo" />
</div>

<div id="header-rightpart">

	<div id="client-logo">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails"
				property="settings.logoPath"></bean:define>
			<img src="/logo/<%=logoPath%>" />
		</logic:present>

	</div>
</div>
<div id="customer-bar">
	<p>
		<span class="customer"></span>
		<logic:present name="currentUser">
			<bean:define id="loginId" name="currentUser" property="userLogin"></bean:define>
			<bean:write name="currentUser" property="userName" />
		</logic:present>
		<logic:present name="vendorUser">
			<bean:define id="loginId" name="vendorUser" property="loginId"></bean:define>
			<bean:write name="vendorUser" property="loginDisplayName" />
		</logic:present>

		| Welcome |
		<html:link action="/logout.do">LogOut</html:link>

	</p>
</div> --%>



