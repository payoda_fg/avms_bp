
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>

<SCRIPT type="text/javascript">

//     Methods for Both for Roles,Application Categories. And send request to proper methods of Action Class

    function viewApplicationCategoryPriviliges(){
    	var applicationcategoryid=document.getElementById('applicationcategoryname').value;
   		window.location = "roleprivileges.do?method=viewPrivilegesByApplication&applicationid="+ applicationcategoryid;        
    }
    
    function viewRolePrivilegesByRole()
	{
		var roleId = document.getElementById('roleName').value;		
		window.location = "roleprivileges.do?method=viewRolePrivilegesByRole&roleId="+ roleId;		
	}
    
    function Validate()
    {
    	var roleId = document.getElementById('roleName').value;
		var applicationcategoryid=document.getElementById('applicationcategoryname').value;
		
		if(applicationcategoryid == 0)
   		{
   			alert("Please Select Application Category.");
   			return false;
   		}
		else if(roleId == 0)
		{
			alert("Please Select User Role.");
			return false;
		}
		else
		{
			return true;
		}
    }
    
	function viewPriviliges() {
		var roleId = document.getElementById('roleName').value;
		window.location = "roleprivileges.do?method=viewPrivilegesByRole&roleId="
				+ roleId;
	}
	$(function() {
		var roleName = $('#roleName').val();
		var applicationcategoryname=$('#applicationcategoryname').val();  // Getting the value of the application category id
		/*  add multiple select / deselect functionality */
		if (roleName != 0 && applicationcategoryname !=0) {
			$("#selectall").click(function() {
				var isChecked = $('.main-table input:checkbox').is(':checked');
				if (!isChecked) {//Check whether any of the checkboxes is checked or not
					$('.main-table input:checkbox').attr('checked', 'checked');
					$('#selectall').val('DeSelect All');
				} else {
					$('.main-table input:checkbox').attr('checked', false);
					$('#selectall').val('Select All');
				}
			});
		}
	});

	/* Function to check the checkbox status and update button value on page load */
	$(document).ready(function() {
		var isChecked = $('.main-table input:checkbox').is(':checked');
		if (!isChecked) {//Check whether any of the checkboxes is checked or not
			$('#selectall').val('Select All');
		} else {
			$('#selectall').val('DeSelect All');
		}
	});
</SCRIPT>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<section role="main" class="content-body card-margin">
<div class="row">
	<div class="mx-auto">
			<section class="card">
				<header class="card-header">
					<h2 class="card-title">Available Role Privileges</h2>
				</header>
<div class="form-box card-body">
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Role Privileges" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
				<html:form
					action="/roleprivileges.do?method=persistPrivilegesByRole"
					styleClass="AVMS" onsubmit="Validate()">
						<div class="form-group row row-wrapper">
								<label class="col-sm-12 col-lg-3 control-label text-sm-right label-col-wrapper">Application Category</label>
							<div class="col-sm-12 col-lg-6 ctrl-col-wrapper">
								<html:select styleId="applicationcategoryname"
									styleClass="chosen-select form-control" property="applicationcategoryname"
									onchange="viewApplicationCategoryPriviliges()">
									<html:option value="0">--Select--</html:option>
									<html:option value="1">Vendor</html:option>
									<html:option value="2">Administration</html:option>
									<html:option value="3">Report</html:option>
									<html:option value="4">Dashboard</html:option>
									<html:option value="5">Vendor Profile</html:option>
								</html:select>
							</div>
						</div>
					
						<div class="form-group row row-wrapper">
								<label class="col-sm-12 col-lg-3 control-label text-sm-right label-col-wrapper">Role Name</label>
							<div class="col-sm-12 col-lg-6 ctrl-col-wrapper">
								<html:select styleId="roleName" property="roleName"
									onchange="viewRolePrivilegesByRole()" styleClass="chosen-select form-control">
									<bean:size id="size" name="userRoles" />
									<logic:greaterEqual value="0" name="size">
										<html:option value="0">--Select--</html:option>
										<logic:iterate id="role" name="userRoles">
											<bean:define id="id" name="role" property="id"></bean:define>
											<bean:define id="roleName" name="role" property="roleName"></bean:define>
											<html:option value="${id}">${roleName}</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>

						<div class="form-group row row-wrapper">
							<div class="col-sm-12 ctrl-col-wrapper text-center">
								<input type="button" value="Select All" class="btn btn-primary"
									id="selectall" />
							</div>
						</div>
					<div class="grid-wrapper">
									<div id="grid_container">
									<layout:datagrid property="datagrid" model="datagrid"
										styleClass="headerRole table table-bordered table-striped mb-0">
										<layout:datagridText property="objectName" title="Object Name" />
										<layout:datagridText property="objectDesc" title="Tile Name" />
										<layout:datagridCheckbox property="add" title="Add" />
										<layout:datagridCheckbox property="delete" title="Delete" />
										<layout:datagridCheckbox property="modify" title="Modify" />
										<layout:datagridCheckbox property="view" title="View" />
										<layout:datagridCheckbox property="visible" title="Visible" />			
									</layout:datagrid>
						
					</div>
				</div>
				
					<footer class="mr-2">
						<div class="justify-content-end">
							<div class="wrapper-btn text-sm-right">
						<logic:iterate id="privilege1" name="privileges">
							<logic:match value="Role Privileges" name="privilege1"
								property="objectId.objectName">
								<logic:match value="1" name="privilege1" property="add">
									<layout:submit value="Submit" styleClass="btn btn-primary" styleId="submit"></layout:submit>
								</logic:match>
							</logic:match>
						</logic:iterate>
						</div>						
					</div>
					</footer>
				</html:form>
			</logic:match>
		</logic:match>
	</logic:iterate>

	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Role Privileges" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div>
					<h3>You have no rights to view role privileges</h3>
				</div>
			</logic:match>
		</logic:match>

	</logic:iterate>
	</div>
	</section>
	</div>
</div>
</section>
<script>
	$('#datagridJsId').addClass('main-table');
	var config = {
		'.chosen-select' : {
			width : "90%"
		}
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	}
</script>
