<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<script type="text/javascript">
	var imgPath = "jquery/images/calendar.gif";
	datePicker();
</script>

<div class="panelCenter_1">
	<h3>SLA Details</h3>

	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">SLA Start Date</div>
				<div class="ctrl-col-wrapper">
					<html:text property="slaStartDate" styleId="slaStartDate" alt="Please click to select date"
						styleClass="text-box" tabindex="33"></html:text>
				</div>
				<span class="error"><html:errors property="slaStartDate"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">SLA End Date</div>
				<div class="ctrl-col-wrapper">
					<html:text property="slaEndDate" styleId="slaEndDate" alt="Please click to select date"
						onchange="checkDate('slaStartDate','slaEndDate')"
						styleClass="text-box" tabindex="34" />
				</div>
				<span class="error"><html:errors property="slaEndDate"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Number of Users Licenses</div>
				<div class="ctrl-col-wrapper">
					<html:text property="licencedUsers" alt="" size="8"
						styleId="licencedUsers" styleClass="text-box" tabindex="35" />
				</div>
				<span class="error"><html:errors property="licencedUsers"></html:errors></span>
			</div>


			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Number of Vendor Licenses</div>
				<div class="ctrl-col-wrapper">
					<html:text property="licencedVendors" alt="" size="8"
						styleId="licencedVendors" styleClass="text-box" tabindex="36" />
				</div>
				<span class="error"><html:errors property="licencedVendors"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
