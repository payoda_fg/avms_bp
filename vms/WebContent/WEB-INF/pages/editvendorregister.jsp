<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript">
<!--
	function copyAddress() {
		var address1 = $("#address1").val();
		var province = $('#province').val();
		var city = $('#city').val();
		var country = $('#country').val();
		var state = $('#state').val();
		var region = $('#region').val();
		var zip = $('#zipcode').val();
		var mobile = $('#mobile').val();
		var phone = $('#phone').val();
		var fax = $('#fax').val();
		if ($("#same").is(':checked')) {
			$("#address2").val(address1);
			$('#province2').val(province);
			$('#city2').val(city);
			$('#country2').val(country);
			$("#state2").find('option').remove().end().append($('#state').html());
			$('#state2').val(state);
			$('#region2').val(region);
			$('#zipcode2').val(zip);
			$('#mobile2').val(mobile);
			$('#phone2').val(phone);
			$('#fax2').val(fax);
			
			$("#address2").prop('readonly', true);
			$('#province2').prop('readonly', true);
			$('#city2').prop('readonly', true);
			$('#country2').prop('readonly', true);
			$('#state2').prop('readonly', true);
			$('#region2').prop('readonly', true);
			$('#zipcode2').prop('readonly', true);
			$('#mobile2').prop('readonly', true);
			$('#phone2').prop('readonly', true);
			$('#fax2').prop('readonly', true);
			
		} else {
			$("#address2").val('');
			$('#province2').val('');
			$('#city2').val('');
			$('#country2').val('');
			$('#state2').val('');
			$('#region2').val('');
			$('#zipcode2').val('');
			$('#mobile2').val('');
			$('#phone2').val('');
			$('#fax2').val('');
			
			$("#address2").prop('readonly', false);
			$('#province2').prop('readonly', false);
			$('#city2').prop('readonly', false);
			$('#country2').prop('readonly', false);
			$('#state2').prop('readonly', false);
			$('#region2').prop('readonly', false);
			$('#zipcode2').prop('readonly', false);
			$('#mobile2').prop('readonly', false);
			$('#phone2').prop('readonly', false);
			$('#fax2').prop('readonly', false);
		}
		$("#country2").select2();
		$("#state2").select2();
	}
function changestate(ele,id) {
	var country=ele.value;
	if (country != null) {
		$.ajax({
			url : 'state.do?method=getState&id='+country+'&random='
					+ Math.random(),
			type : "POST",
			async : false,
			success : function(data) {
				$("#"+id).find('option').remove().end().append(data);
				$('#'+id).select2();
			}
		});
	}
}
function companyOwnershipChecked(id, value) {
	$('#' + id).val(value);
}
function validateSalesYear(ele){
	// To get the current year
	var d = new Date();
	var curr_year = d.getFullYear();
	var salesYear = ele.value;
	var yearOfEstablishment = $("#yearOfEstablishment").val();
	if(salesYear != ""){
		if(salesYear<yearOfEstablishment || salesYear>curr_year){
		  alert("Please enter year between " + yearOfEstablishment + " and current year( " +curr_year+ " )");
		  $(ele).val('');
		  $(ele).focus();
		  return false;
		}
	}
}
//-->
</script>

<div class="panelCenter_1">
	<h3>Company</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<bean:message key="comp.name" />
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="vendorName" alt="" styleId="vendorName"
						styleClass="text-box" tabindex="23"/>
				</div>
				<span class="error"> <html:errors property="vendorName"></html:errors>
				</span>
			</div>
			<logic:present name="currentUser">
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Status</div>
				<div class="ctrl-col-wrapper">
					<html:select property="vendorStatus" styleId="vendorStatus"
							styleClass="chosen-select-width" tabindex="24" >
							<logic:present name="editVendorMasterForm" property="statusMasters">
								<logic:iterate id="status" name="editVendorMasterForm" property="statusMasters">
									<bean:define id="id" name="status" property="id"></bean:define>
									<html:option value="<%=id.toString() %>">
										<bean:write name="status" property="statusname"/>
									</html:option>
								</logic:iterate>
							</logic:present>
					</html:select>
				</div>
			</div>
			</logic:present>
		</div>
		<div id="hideDivforTier2Vendor">
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Duns Number</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="dunsNumber"
							styleId="dunsNumber" alt="Optional" tabindex="25" />
						<span class="error"> <html:errors property="dunsNumber"></html:errors>
						</span>
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Federal Tax ID</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="taxId" alt=""
							styleId="taxId" tabindex="26" />
					</div>
					<span class="error"> <html:errors property="taxId"></html:errors>
					</span>
				</div>
			</div>
			<div class="wrapper-half">
				
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Company Type</div>
					<div class="ctrl-col-wrapper">
						<html:select property="companytype" styleId="companytype"
							styleClass="chosen-select-width" tabindex="27">
							<html:option value="">Select One</html:option>
							<logic:present name="editVendorMasterForm" property="legalStructures">
							<logic:iterate id="company" name="editVendorMasterForm" property="legalStructures">
							<bean:define id="id" name="company" property="id"></bean:define>
							<html:option value="<%=id.toString() %>"><bean:write name="company" property="name"/></html:option>
							</logic:iterate>
							</logic:present>
						</html:select>
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Number of Employees</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="numberOfEmployees"
							alt="" styleId="numberOfEmployees" tabindex="28" />
					</div>
					<span class="error"> <html:errors
							property="numberOfEmployees"></html:errors>
					</span>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Business Type</div>
					<div class="ctrl-col-wrapper">
						<html:select property="businessType" styleId="businessType"
							styleClass="chosen-select-width" tabindex="29" >
						<html:option value="">Select One</html:option>
						<logic:present name="editVendorMasterForm" property="businessTypes">
						<logic:iterate id="business" name="editVendorMasterForm" property="businessTypes">
						<bean:define id="id" name="business" property="id"></bean:define>
						<html:option value="<%=id.toString() %>"><bean:write name="business" property="typeName"/></html:option>
						</logic:iterate>
						</logic:present>
					</html:select>
					</div>
					<span class="error"> <html:errors property="businessType"></html:errors>
					</span>
				</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Year Established</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="yearOfEstablishment"
						alt="" styleId="yearOfEstablishment" tabindex="30"
						onblur="return currentYearValidation('yearOfEstablishment');" />
				</div>
				<span class="error"> <html:errors
						property="yearOfEstablishment"></html:errors>
				</span>
			</div>
		</div>
			<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Website URL(http://)</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="website" alt="Optional"
						styleId="website" tabindex="31" />
				</div>
				<span class="error"> <html:errors property="website"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="emailId"
						styleId="emailId" alt="" tabindex="32" />
				</div>
				<span class="error"> <html:errors property="emailId"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State Incorporation</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="stateIncorporation"
						styleId="stateIncorporation" tabindex="33" alt="Optional" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State Sales TaxId</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="stateSalesTaxId"
						styleId="stateSalesTaxId" tabindex="34"  alt="Optional" />
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

<div class="panelCenter_1" style="padding-top: 2%;">
	<h3>Revenue (for the past three years)</h3>
	<div class="form-box">
	<table class="main-table" style="width: 100%;">
		<tr>
			<td class="header">Year</td>
			<td class="header">Annual Revenue</td>
		</tr>
		<tr class="even">
			<td>
			<html:text styleClass="main-text-box" name="editVendorMasterForm" property="annualYear1" alt="Optional" 
			                     styleId="annualYear1" tabindex="35" onchange="validateSalesYear(this);"/></td>
			<td>
				<html:hidden  property="annualTurnoverId1" alt="" />
						<html:text styleClass="main-text-box" property="annualTurnover" alt="Optional"
							styleId="annualTurnover" onchange="moneyFormatToUS('annualTurnover','turnoverFormatValue');" tabindex="35" />
						<html:hidden styleId="turnoverFormatValue"
							property="annualTurnoverFormat" alt="" />
					<span class="error"> <html:errors property="annualTurnover"></html:errors>
					</span>
			</td>
		</tr>
		<tr class="even">
			<td><html:text styleClass="main-text-box" name="editVendorMasterForm" property="annualYear2" alt="Optional" 
			                        styleId="annualYear2" tabindex="36" onchange="validateSalesYear(this);"/></td>
			<td>
				<html:hidden  property="annualTurnoverId2" alt="" />
					<html:text styleClass="main-text-box" property="sales2" tabindex="36"
						styleId="sales2"  alt="Optional"  onchange="moneyFormatToUS('sales2','salesFormat2');"/>
						<html:hidden styleId="salesFormat2"
							property="salesFormat2" alt="" />
			</td>
		</tr>
		<tr class="even">
			<td><html:text styleClass="main-text-box" name="editVendorMasterForm" property="annualYear3" alt="Optional" 
			                        styleId="annualYear3" tabindex="37" onchange="validateSalesYear(this);"/></td>
			<td>
				<html:hidden  property="annualTurnoverId3" alt="" />
					<html:text styleClass="main-text-box" property="sales3" alt="Optional" tabindex="37"
						styleId="sales3" onchange="moneyFormatToUS('sales3','salesFormat3');"  />
					<html:hidden styleId="salesFormat3"
						property="salesFormat3" alt="" />
			</td>
		</tr>
	</table>
	</div>
	<div class="clear"></div>
</div>
<div class="panelCenter_1" style="padding-top: 2%;">
	<h3>Physical Address</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea styleClass="main-text-area" property="address1" alt=""
						styleId="address1" tabindex="38" />
				</div>
				<span class="error"> <html:errors property="address1"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="city" alt=""
						styleId="city" tabindex="39" />
				</div>
				<span class="error"> <html:errors property="city"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
		<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="country" styleId="country"
						styleClass="chosen-select-width" tabindex="40" onchange="changestate(this,'state');">
						<html:option value="">--Select--</html:option>
						<logic:iterate id="country" name="countryList">
							<bean:define id="name" name="country" property="id"></bean:define>
								<html:option value="<%=name.toString()%>"><bean:write name="country" property="countryname" /></html:option>
						</logic:iterate>
					</html:select>
				</div>
				<span class="error"> <html:errors property="country"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
				<html:select property="state" styleId="state"
						styleClass="chosen-select-width" tabindex="41">
						<html:option value="">- Select -</html:option>
						<logic:present name="editVendorMasterForm" property="phyStates1">
						<logic:iterate id="states" name="editVendorMasterForm" property="phyStates1">
							<bean:define id="name" name="states" property="id"></bean:define>
								<html:option value="<%=name.toString()%>"><bean:write name="states" property="statename" /></html:option>
						</logic:iterate>
						</logic:present>
				</html:select>
					
				</div>
				<span class="error"> <html:errors property="state"></html:errors></span>
			</div>
			
		</div>
		<div class="wrapper-half">
		<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">County/Province</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="province" alt="Optional"
						styleId="province" tabindex="42" />
				</div>
				<span class="error"> <html:errors property="province"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Region</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="region" alt="Optional"
						styleId="region" tabindex="43" />
				</div>
				<span class="error"> <html:errors property="region"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip Code</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="zipcode" alt=""
						styleId="zipcode" tabindex="44" />
				</div>
				<span class="error"> <html:errors property="zipcode"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="mobile" alt=""
						styleId="mobile" tabindex="45" />
				</div>
				<span class="error"> <html:errors property="mobile"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="phone" alt=""
						styleId="phone" tabindex="46" />
				</div>
				<span class="error"> <html:errors property="phone"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Fax</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="fax" alt=""
						styleId="fax" tabindex="47" />
				</div>
				<span class="error"> <html:errors property="fax"></html:errors></span>
			</div></div></div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="padding-top: 2%;">
	<h3>Mailing Address</h3>
	<div class="form-box">
	&nbsp;&nbsp;Same as physical Address : <input type="checkbox" id="same" onclick="copyAddress();" style="margin-top: 1%;">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea styleClass="main-text-area" property="address2"
						alt="" styleId="address2" tabindex="48" />
				</div></div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="city2" alt=""
						styleId="city2" tabindex="49" />
				</div></div></div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="country2" styleId="country2"
						styleClass="chosen-select-width" tabindex="50" onchange="changestate(this,'state2');">
						<html:option value="">-----Select-----</html:option>
						<bean:size id="size" name="countryList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countryList">
								<bean:define id="name" name="country" property="id"></bean:define>
								<html:option value="<%=name.toString()%>"><bean:write name="country" property="countryname" /></html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div></div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
				<html:select property="state2" styleId="state2"
						styleClass="chosen-select-width" tabindex="51">
						<html:option value="">- Select -</html:option>
						<logic:present name="editVendorMasterForm" property="mailingStates2">
						<logic:iterate id="states" name="editVendorMasterForm" property="mailingStates2">
							<bean:define id="name" name="states" property="id"></bean:define>
								<html:option value="<%=name.toString()%>"><bean:write name="states" property="statename" /></html:option>
						</logic:iterate>
						</logic:present>
				</html:select>
				</div></div>
			</div>
		<div class="wrapper-half">
		<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">County/Province</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="province2" alt="Optional"
						styleId="province2" tabindex="52" />
				</div></div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Region</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="region2" alt="Optional"
						styleId="region2" tabindex="53" />
				</div>
			</div>
			</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip Code</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="zipcode2" alt="Optional"
						styleId="zipcode2" tabindex="54" />
				</div></div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="mobile2" alt=""
						styleId="mobile2" tabindex="55" />
				</div></div></div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="phone2" alt=""
						styleId="phone2" tabindex="56" />
				</div></div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Fax</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="fax2" alt=""
						styleId="fax2" tabindex="57" />
				</div></div></div></div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="padding-top: 1%;">
	<h3>Company Ownership</h3>
	 <div class="form-box">
	<h4 style="padding: 1%;">
			<html:radio property="companyOwnership"
					value="ownerPublic"  idName="editVendorMasterForm" styleId="ownerPublic" onclick="companyOwnershipChecked('ownerPublic','1')">
					Publicly Traded </html:radio>
				<html:radio property="companyOwnership" 
					value="ownerPrivate" idName="editVendorMasterForm" styleId="ownerPrivate" onclick="companyOwnershipChecked('ownerPrivate','2')">Privately Owned </html:radio>
		</h4>
	<div id="ownerInfo"> 
	 <div class="panelCenter_1" style="padding-top: 2%;">
   		 <h3>Owner 1</h3>
   		 <div class="form-box">
   			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerName1" alt=""
							styleId="ownerName1" tabindex="58" />
							<html:hidden  property="ownerId1" alt="" />
					</div></div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Title</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerTitle1" alt=""
							styleId="ownerTitle1" tabindex="59" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">E-mail </div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerEmail1" alt=""
							styleId="ownerEmail1" tabindex="60" />
					</div></div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Phone</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerPhone1" alt=""
							styleId="ownerPhone1" tabindex="61" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Extension</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerExt1" alt="Optional"
							styleId="ownerExt1"  style="width:26%;" tabindex="62"/>
					</div></div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerMobile1" alt=""
							styleId="ownerMobile1" tabindex="63" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Gender</div>
				<div class="ctrl-col-wrapper">
					<html:select property="ownerGender1" styleId="ownerGender1"
						styleClass="chosen-select-width" tabindex="64" >
						<html:option value="">-- Select --</html:option>
						<html:option value="M">Male</html:option>
						<html:option value="F">Female</html:option>
					</html:select>
					</div></div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Owner Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ownerEthnicity1" styleId="ownerEthnicity1"
									styleClass="chosen-select-width"  style="height: 2%;" tabindex="65">
								<html:option value="">-----Select-----</html:option>
								<bean:size id="size" name="ethnicities" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="ethni" name="ethnicities">
										<bean:define id="name" name="ethni" property="id"></bean:define>
										<html:option value="<%=name.toString()%>"><bean:write name="ethni" property="ethnicity" /></html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select>
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">% Ownership</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="ownerOwnership1" alt=""
								styleId="ownerOwnership1" tabindex="66" />
					</div></div></div></div>
			<div class="clear"></div>
			</div>
  	<div class="panelCenter_1" style="padding-top: 2%;">
   		 <h3>Owner 2</h3>
   		  <div class="form-box">
   			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerName2" alt="Optional"
							styleId="ownerName2" tabindex="67" />
							<html:hidden  property="ownerId2" alt="" />
					</div></div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Title</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerTitle2" alt="Optional"
							styleId="ownerTitle2" tabindex="68" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">E-mail </div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerEmail2" alt="Optional"
							styleId="ownerName2" tabindex="69" />
					</div></div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Phone</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerPhone2" alt=""
							styleId="ownerPhone2" tabindex="70" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Extension</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box " property="ownerExt2" alt="Optional"
							styleId="ownerExt2" style="width:26%;" tabindex="71" />
					</div></div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerMobile2" alt=""
							styleId="ownerMobile2" tabindex="72" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Gender</div>
				<div class="ctrl-col-wrapper">
					<html:select property="ownerGender2" styleId="ownerGender2"
						styleClass="chosen-select-width" tabindex="73" >
						<html:option value="">-- Select --</html:option>
						<html:option value="M">Male</html:option>
						<html:option value="F">Female</html:option>
					</html:select>
					</div></div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Owner Ethnicity</div>
					<div class="ctrl-col-wrapper">
						<html:select property="ownerEthnicity2" styleId="ownerEthnicity2"
								styleClass="chosen-select-width"  style="height: 2%;" tabindex="74">
							<html:option value="">-----Select-----</html:option>
							<bean:size id="size" name="ethnicities" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="ethni" name="ethnicities">
									<bean:define id="name" name="ethni" property="id"></bean:define>
									<html:option value="<%=name.toString()%>"><bean:write name="ethni" property="ethnicity" /></html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</html:select>
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">% Ownership</div>
					<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="ownerOwnership2" alt="Optional"
							styleId="ownerOwnership2" tabindex="75" />
					</div></div></div></div>
			<div class="clear"></div>
			</div>
  	<div class="panelCenter_1" style="padding-top: 2%;">
   		 <h3>Owner 3</h3>
   		  <div class="form-box">
   			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerName3" alt="Optional"
							styleId="ownerName3" tabindex="76" />
							<html:hidden  property="ownerId3" alt="" />
					</div></div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Title</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerTitle3" alt="Optional"
							styleId="ownerTitle3" tabindex="77" />
					</div></div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">E-mail </div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerEmail3" alt="Optional"
							styleId="ownerName3" tabindex="78" />
					</div></div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Phone</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerPhone3" alt=""
							styleId="ownerPhone3" tabindex="79" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Extension</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerExt3" alt="Optional"
							styleId="ownerExt3" style="width:26%;" tabindex="80" />
					</div></div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerMobile3" alt=""
							styleId="ownerMobile3" tabindex="81" />
					</div></div></div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Gender</div>
				<div class="ctrl-col-wrapper">
					<html:select property="ownerGender3" styleId="ownerGender3"
						styleClass="chosen-select-width" tabindex="82">
						<html:option value="">-- Select --</html:option>
						<html:option value="M">Male</html:option>
						<html:option value="F">Female</html:option>
					</html:select>
					</div></div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Owner Ethnicity</div>
					<div class="ctrl-col-wrapper">
							<html:select property="ownerEthnicity3" styleId="ownerEthnicity3"
									styleClass="chosen-select-width"  style="height: 2%;" tabindex="83">
								<html:option value="">-----Select-----</html:option>
								<bean:size id="size" name="ethnicities" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="ethni" name="ethnicities">
										<bean:define id="name" name="ethni" property="id"></bean:define>
										<html:option value="<%=name.toString()%>"><bean:write name="ethni" property="ethnicity" /></html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select>
					</div></div>
			</div>
			<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">% Ownership</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="text-box" property="ownerOwnership3" alt="Optional"
							styleId="ownerOwnership3" tabindex="84" />
					</div></div>
			</div>
  	</div> <div class="clear"></div></div>
</div></div></div>
<div class="clear"></div>