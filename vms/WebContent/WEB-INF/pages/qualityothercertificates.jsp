<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script>
	jQuery(document).ready(function() {
		document.getElementById("otherCert1").onchange = function() {
			document.getElementById("otherFile1").innerHTML = this.value;
		};

		$('input[name=expOilGas]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#oilGasExp').css('display', 'block');
			} else {
				$('#oilGasExp').val('');
				$('#oilGasExp').css('display', 'none');
			}
		});

		$('input[name=isBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpsupplier').css('display', 'block');
			} else {
				$('#bpsupplier').val('');
				$('#bpsupplier').css('display', 'none');
			}
		});

		$('input[name=isCurrentBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpFranchise').css('display', 'block');
			} else {
				$('#bpFranchise').val('');
				$('#bpFranchise').css('display', 'none');
			}
		});

		$('input[name=isFortune]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#fortune').css('display', 'block');
			} else {
				$('#fortune').val('');
				$('#fortune').css('display', 'none');
			}
		});

	});

	var imgPath = "jquery/images/calendar.gif";
	datePickerOtherExp();

	function checkDiverseType(element) {
		var rowCount = $('#dataTable tr').length - 1;
		for ( var i = 1; i <= rowCount; i++) {
			var id = "otherCertType" + i;
			if (id == element) {
			} else {
				var v1 = $('#' + id).val();
				var v2 = $('#' + element).val();
				if (v1 == v2) {
					alert("Certificate should be unique");
					document.getElementById(element).selectedIndex = 0;
				}
			}
		}
	}
</script>
<h3>Quality / Other Certificate</h3>
<table width="100%" border="0" class="main-table" id="dataTable">
	<tr>
		<td class="header">Certification Type</td>
		<td class="header">Expire Date</td>
		<td class="header">Certificate</td>

	</tr>
	<tr class="even">
		<td><html:select property="otherCertType1" tabindex="119"
				styleClass="chosen-select" styleId="otherCertType1"
				onchange="checkDiverseType(this.id);">
				<html:option value="-1">-- Select --</html:option>
				<html:optionsCollection name="qualityCertificates" label="certificateName" value="id"/>
			</html:select></td>
		<td><html:text property="otherExpiryDate"
				styleId="otherExpiryDate" styleClass="main-text-box othexpDate1"
				alt="Please click to select date" tabindex="120" /></td>
		<td>
			<div class="fileUpload btn btn-primary">
				<span>Browse</span>
				<html:file property="otherCert[0]" styleId="otherCert1" tabindex="121"
					styleClass="upload">
				</html:file>
			</div> <span id="otherFile1"></span>
		</td>

	</tr>
</table>
<INPUT id="cmd1" type="button" value="Add Quality Certificate"
	class="btn" onclick="addRow('dataTable')" />
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Business Biography</h3>
	<div class="form-box">
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Do you have current experience
					in the Oil &amp; Gas Industry?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="expOilGas" value="1" tabindex="122">Yes</html:radio>
					<html:radio property="expOilGas" value="0" tabindex="123">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="oilGasExp" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please list your clients below</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="listClients" rows="5" cols="30"
						styleClass="main-text-area" tabindex="124"></html:textarea>
				</div>
			</div>
		</div>

		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Are you a BP Supplier?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isBpSupplier" value="1" tabindex="125">Yes</html:radio>
					<html:radio property="isBpSupplier" value="0" tabindex="126">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="bpsupplier" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">BP Contact Name:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="bpContact" styleId="bpContact"
						styleClass="text-box" tabindex="127"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">BP Division:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="bpDivision" styleId="bpDivision"
						styleClass="text-box" tabindex="128"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">BP Contact Phone:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="bpContactPhone" alt="" styleClass="text-box"
						styleId="bpContactPhone" style="width:45%;" tabindex="129"/>
					<div class="label-col-wrapper" style="width: 10%;">Ext</div>
					<html:text property="bpContactPhoneExt" styleId="bpContactPhoneExt"
						size="3" styleClass="text-box" alt="Optional" style="width:22%;" tabindex="130"></html:text>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Are you a current 1st or 2nd
					Tier Supplier to BP or a BP Franchise?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isCurrentBpSupplier" value="1" tabindex="131">Yes</html:radio>
					<html:radio property="isCurrentBpSupplier" value="0" tabindex="132">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="bpFranchise" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please describe the scope of
					services. Include contract value, locations serviced, and other
					pertinent information:</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="currentBpSupplierDesc" rows="5" cols="30"
						styleClass="main-text-area" tabindex="133"></html:textarea>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Are you currently doing
					business with any Fortune 500 companies?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isFortune" value="1" tabindex="134">Yes</html:radio>
					<html:radio property="isFortune" value="0" tabindex="135">No</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full" id="fortune" style="display: none;">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please list your Fortune 500
					clients below:</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="fortuneList" rows="5" cols="30"
						styleClass="main-text-area" tabindex="136"></html:textarea>
				</div>
			</div>
		</div>

		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Does your company utilize union
					represented workforce?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isUnionWorkforce" value="1" tabindex="137">Yes</html:radio>
					<html:radio property="isUnionWorkforce" value="0" tabindex="138">No</html:radio>
				</div>
			</div>
		</div>

		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">If yes, does your company also
					utilize non-union represented workforce?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isNonUnionWorkforce" value="1" tabindex="139">Yes</html:radio>
					<html:radio property="isNonUnionWorkforce" value="0" tabindex="140">No</html:radio>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Safety</h3>
	<div class="form-box">
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company PICS certified?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isPicsCertified" value="1" tabindex="141">Yes</html:radio>
					<html:radio property="isPicsCertified" value="0" tabindex="142">No</html:radio>
					<html:radio property="isPicsCertified" value="2" tabindex="143">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company ISO 9000
					certified?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isIso9000Certified" value="1" tabindex="144">Yes</html:radio>
					<html:radio property="isIso9000Certified" value="0" tabindex="145">No</html:radio>
					<html:radio property="isIso9000Certified" value="2" tabindex="146">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company ISO 14001
					certified?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isIso14000Certified" value="1" tabindex="147">Yes</html:radio>
					<html:radio property="isIso14000Certified" value="0" tabindex="148">No</html:radio>
					<html:radio property="isIso14000Certified" value="2" tabindex="149">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Have you received any OSHA
					citations in the last three years?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isOshaRecd" value="1" tabindex="150">Yes</html:radio>
					<html:radio property="isOshaRecd" value="0" tabindex="151">No</html:radio>
					<html:radio property="isOshaRecd" value="2" tabindex="152">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Does your company have an
					Occupational Safety &amp; Health Administration (OSHA) 3-yr average of
					2.0 or less?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isOshaAvg" value="1" tabindex="153">Yes</html:radio>
					<html:radio property="isOshaAvg" value="0" tabindex="154">No</html:radio>
					, please specify
					<html:text property="isOshaAvgTxt" styleClass="text-box" tabindex="159"></html:text>
					<html:radio property="isOshaAvg" value="2" tabindex="155">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Does your company have an
					Experience Modification Rate (EMR) of 1.0 or less?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isEmr" value="1" tabindex="156">Yes</html:radio>
					<html:radio property="isEmr" value="0" tabindex="157">No</html:radio>
					, please specify
					<html:text property="isEmrTxt" styleClass="text-box" tabindex="158"></html:text>
					<html:radio property="isEmr" value="2" tabindex="159">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Will your company agree to BP's
					4 hour onsite safety training and have a 10-hr OSHA card for all
					contractor personnel working at the refinery?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isOshaAgree" value="1" tabindex="160">Yes</html:radio>
					<html:radio property="isOshaAgree" value="0" tabindex="161">No</html:radio>
					<html:radio property="isOshaAgree" value="2" tabindex="162">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Is your company in compliance
					with the Northwest Indiana Business Roundtable substance abuse
					policy?</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="isNib" value="1" tabindex="163">Yes</html:radio>
					<html:radio property="isNib" value="0" tabindex="164">No</html:radio>
					<html:radio property="isNib" value="2" tabindex="165">NA</html:radio>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Please provide a snapshot of
					major jobs / projects completed:</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="jobsDesc" rows="5" cols="30"
						styleClass="main-text-area" tabindex="166"></html:textarea>
				</div>
			</div>
		</div>
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Customer / Location</div>
				<div class="ctrl-col-wrapper">
					<html:text property="custLocation" styleId="custLocation"
						styleClass="text-box" tabindex="167"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Customer contact</div>
				<div class="ctrl-col-wrapper">
					<html:text property="custContact" styleId="custContact"
						styleClass="text-box" tabindex="168" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Telephone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="telephone" styleId="telephone"
						styleClass="text-box" tabindex="169"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Type of work</div>
				<div class="ctrl-col-wrapper">
					<html:text property="workType" styleId="workType"
						styleClass="text-box" tabindex="170"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Size $</div>
				<div class="ctrl-col-wrapper">
					<html:text property="size" styleId="size" styleClass="text-box" tabindex="171"/>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<script type="text/javascript">
<!--
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[1].cells.length;
		for ( var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[1].cells[i].innerHTML;
			//alert(newcell.childNodes);
			if (i == 2) {
				newcell.innerHTML = '<div class="fileUpload btn btn-primary"><span>Browse</span>'
						+ '	<input name="otherCert['+(rowCount-1)+']" type="file" id="otherCert'
						+ rowCount
						+ '" class="upload"/> </div> <span id="otherFile'+rowCount+'"></span>';

				$("#otherCert" + rowCount)
						.on(
								"change",
								function() {
									document.getElementById("otherFile"
											+ rowCount).innerHTML = this.value;
								});
			}
			else if(i==0){
					newcell.innerHTML = '<select name="otherCertType1" id="otherCertType'+ rowCount+'" class="chosen-select fields" >'
									+ $('#otherCertType1').html() + '</select>';
				newcell.childNodes[0].selectedIndex = 0;
				//newcell.childNodes[0].id = "otherCertType" + rowCount;
				 $("#otherCertType" + rowCount).select2();
			}
			switch (newcell.childNodes[0].type) {
			case "text":
				newcell.innerHTML = " <input class='othexpDate1 main-text-box ' alt='Please click to select date' name='otherExpiryDate'/>";
				newcell.childNodes[0].value = "";
				 $(document).on('focus', ".othexpDate1", function() {
					$(this).datepicker();
				});
				break;
			}
		}
	}
//-->
</script>
