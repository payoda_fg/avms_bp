<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!-- For JQuery Panel -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

		$('.panelCenter_1').panel({
			collapsible : false
		});
		$(function($) {
			 $("#contactMobile").mask("(999) 999-9999?");
		});
		var ipAddressPattern = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

		jQuery.validator.addMethod("noSpace", function(value,
				element) {
			return value.indexOf(" ") < 0 && value != "";
		}, "Please do not use space");

		jQuery.validator.addMethod("ipAddress", function(value,
				element) {
			return ipAddressPattern.test(value);
		}, "Please enter valid IP address");
		jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
		},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #083B69');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});

	});

	function clearField() {

		window.location.reload(true);
	}
</script>

<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<html:form enctype="multipart/form-data" styleClass="FORM"
	action="/createCustomer?method=create" styleId="customerForm">
	<html:javascript formName="customerForm" />

	<div id="demoTab">
		<ul class="resp-tabs-list">
			<li><a href="#tabs-1" style="color: #fff;" tabindex="1">Customer
					Registration</a></li>
			<li><a href="#tabs-2" style="color: #fff;" tabindex="18">Contact
					Information</a></li>
			<li><a href="#tabs-3" style="color: #fff;" tabindex="32">SLA</a></li>
			<li><a href="#tabs-4" style="color: #fff;" tabindex="37">Personalize
					your profile</a></li>
			<li><a href="#tabs-5" style="color: #fff;" tabindex="39">Database
					Settings</a></li>
		</ul>

		<div class="resp-tabs-container">
			<div id="tabs-1">
				<%@include file="customerregister.jsp"%>
			</div>
			<div id="tabs-2">
				<%@include file="customercontactinfo.jsp"%>
			</div>
			<div id="tabs-3">
				<%@include file="customerslapage.jsp"%>
			</div>
			<div id="tabs-4">
				<%@include file="personalizedprofile.jsp"%>
			</div>
			<div id="tabs-5">
				<%@include file="databaseSettings.jsp"%>
			</div>
		</div>
	</div>
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Customer" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">
				<div class="wrapper-btn">
					<html:submit value="Submit" styleClass="btn" 
					 styleId="submit1"></html:submit>
					<html:reset value="Clear" styleClass="btn" onclick="clearField();"></html:reset>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>

</html:form>


<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>
	
<script type="text/javascript">
	$(function() {
		
		$('#demoTab').easyResponsiveTabs();
		
		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z-0-9 ]+$/.test(value);
		}, "No Special Characters Allowed.");
		
		jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
	          return this.optional(element) || /^[0-9]+$/.test(value);
	    },"Please enter only numbers.");
		
		jQuery.validator.addMethod("login", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9]+$/.test(value)
		}, "No Special Characters Allowed.");
		
		jQuery.validator.addMethod("phoneNumber", function(value, element) { 
	        return this.optional(element) || /^[0-9-]+$/.test(value);
	  	},"Please enter valid phone number.");

		$("#customerForm").validate({
				rules : {
					companyname : {
						/* alpha : true, */
						required : true
					},
					companycode : {
						required : true,
						maxlength : 16
					},
					dunsnum : {
						required : true,
						maxlength : 9,
						onlyNumbers : true
					},
					taxid : {
						maxlength : 14,
						onlyNumbers : true
					},
					address1 : {
						required : true,
						maxlength : 125
					},
					city : {
						required : true,
						alpha : true,
						maxlength : 60
					},
					state : {
						required : true,
						alpha : true,
						maxlength : 60
					},
					province : {
						maxlength : 60
					},
					region : {
						maxlength : 60
					},
					country : {
						required : true,
						maxlength : 120
					},
					zipcode : {
						maxlength : 10,
						onlyNumbers : true
					},
					mobile : {
						maxlength : 20,
						phoneNumber : true
					},
					phone : {
						required : true,
						maxlength : 20,
						phoneNumber : true
					},
					fax : {
						maxlength : 16,
						onlyNumbers : true
					},
					webSite : {
						url : true
					},
					email : {
						required : true,
						maxlength : 120,
						email : true
					},
					slaStartDate : {
						required : true
					},
					slaEndDate : {
						required : true
					},
					licencedUsers : {
						required : true,
						onlyNumbers : true
					},
					licencedVendors : {
						required : true,
						onlyNumbers : true
					},
					firstName : {
						required : true,
						login : true,
						rangelength : [ 3, 60 ]
					},
					lastName : {
						rangelength : [ 3, 60 ]
					},
					designation : {
						alpha : true,
						rangelength : [ 1, 60 ]
					},
					contactPhone : {
						required : true,
						maxlength : 20,
						phoneNumber : true
					},
					contactMobile : {
						required : true,
					},
					contactFax : {
						required : true,
						maxlength : 16,
						onlyNumbers : true
					},
					contanctEmail : {
						required : true,
						email : true,
						maxlength : 120
					},
					loginDisplayName : {
						required : true,
						rangelength : [ 3, 60 ]
					},
					loginId : {
						required : true,
						noSpace : true,
						maxlength : 120
					},
					loginpassword : {
						required : true,
						password:true
					},
					confirmPassword : {
						required : true,
						equalTo : "#loginpassword"
					},
					userSecQn : {
						required : true
					},
					userSecQnAns : {
						required : true,
						rangelength : [ 3, 15 ]
					},
					companyLogo : {
						required : true,
						accept : "jpg|bmp|gif|tiff|exif|png|webp"
					},
					databaseName : {
						required : true,
						noSpace : true,
						maxlength : 120
					},
					databaseIp : {
						required : true,
						ipAddress : true
					},
					userName : {
						required : true,
						maxlength : 120
					},
					dbpassword : {
						required : true,
						maxlength : 120
					},
					applicationUrl : {
						required : true,
						noSpace : true
					},
				},

				ignore : "ui-tabs-hide",
				submitHandler : function(form) {
					if (checkDate('slaStartDate', 'slaEndDate')) {
						$("#ajaxloader").css('display','block');
						$("#ajaxloader").show();
						form.submit();
					}
				},

				invalidHandler : function(form, validator) {

					var errors = validator.numberOfInvalids();
					if (errors) {
						var invalidPanels = $(validator.invalidElements())
								.closest(".resp-tab-content", form);

						if (invalidPanels.size() > 0) {
							$.each($.unique(invalidPanels.get()),function() {

								if ( $(window).width() > 650 ) {
									$("a[href='#"+ this.id+ "']").parent()
									.not(".resp-accordion").addClass("error-tab-validation")
									.show("pulsate",{times : 3});
								} else {
								   $("a[href='#"+ this.id+ "']").parent()
									.addClass("error-tab-validation")
										.show("pulsate",{times : 3});
								}
							});
							
							/* $('input[type="text"]').each(function() {
								if ($(this).attr('alt') == 'Optional'
										&& $(this).attr('value') == '') {
									this.value = 'Optional';
								}
							}); */
						}
					}
				},
				unhighlight : function(element, errorClass,validClass) {

					$(element).removeClass(errorClass);
					$(element.form).find("label[for=" + element.id + "]")
							.removeClass(errorClass);
					var $panel = $(element).closest(
							".resp-tab-content", element.form);
					if ($panel.size() > 0) {
						if ($panel.find("." + errorClass + ":visible")
								.size() > 0) {
							if ($(window).width() > 650) {
								$("a[href='#"+ $panel[0].id+ "']")
										.parent()
										.removeClass("error-tab-validation");
							} else {
								$("a[href='#"+ $panel[0].id+ "']")
										.parent()
										.removeClass("error-tab-validation");
							}
						}
					}
				}
			});
	});
</script>
