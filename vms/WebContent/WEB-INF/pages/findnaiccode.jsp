<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>

<html:html>
<head>
<title>Find NAIC / ISIC Code</title>
<%
	String rowcount = session.getAttribute("rowcount").toString();
%>
<link href="css/style.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript">
	var rowcount='<%=rowcount%>';


	function close_win() {
	    close();
	}

	function fn_category() {
	    var categoryId = document.getElementById("naicsCategoryId").value;
	    window.location = "NaicsCode.do?method=naicsSubCategoriesById&categoryId="
		    + categoryId;
	}

	function fn_sub_category() {
	    var subCategoryId = document.getElementById("subCategoryId").value;
	    window.location = "NaicsCode.do?method=naicsMastersById&subCategoryId="
		    + subCategoryId;
	}

	function fn_setParentValue() {

	    var fullVal = document.getElementById('naicsMasterCode').value;
	    var mySplitResult = fullVal.split("-");
		var flag=true;
		
	    if (mySplitResult != null && mySplitResult.length > 0) {
		if (rowcount == 1) {
		    var naicesCode0 = this.opener.document
			    .getElementById("naicsCode_0").value;
		    var naicesCode2 = this.opener.document
			    .getElementById("naicsCode_2").value;
		    var naicesCode1 = mySplitResult[1];
		    if (naicesCode1 == naicesCode0 || naicesCode1 == naicesCode2) {
			this.opener.document.getElementById("naicsCode_" + rowcount).value = "";
			this.opener.document.getElementById("naicsDesc_" + rowcount).value = "";
			alert("NaicsCode is existed");
			flag=false;
			this.opener.document.getElementById("naicsCode_" + rowcount)
				.focus();
		    } else {
			this.opener.document.getElementById("naicsCode_" + rowcount).value = mySplitResult[1];
			this.opener.document.getElementById("naicsDesc_" + rowcount).value = mySplitResult[2];
			this.opener.document.getElementById("naicsCode_" + rowcount)
				.focus();
			this.opener.document.getElementById("naicsCode_" + rowcount).className = "textBox";
		    }
		} else if (rowcount == 2) {
		    var naicesCode0 = this.opener.document
			    .getElementById("naicsCode_0").value;
		    var naicesCode1 = this.opener.document
			    .getElementById("naicsCode_1").value;
		    var naicesCode2 = mySplitResult[1];
		    if ((naicesCode1 == naicesCode2) || (naicesCode2 == naicesCode0)) {
			this.opener.document.getElementById("naicsCode_" + rowcount).value = "";
			this.opener.document.getElementById("naicsDesc_" + rowcount).value = "";
			alert("NaicsCode is existed");
			flag=false;
			this.opener.document.getElementById("naicsCode_" + rowcount)
				.focus();
		    } else {
			this.opener.document.getElementById("naicsCode_" + rowcount).value = mySplitResult[1];
			this.opener.document.getElementById("naicsDesc_" + rowcount).value = mySplitResult[2];
			this.opener.document.getElementById("naicsCode_" + rowcount)
				.focus();
			this.opener.document.getElementById("naicsCode_" + rowcount).className = "textBox";
		    }
		} else if (rowcount == 0) {
		    var naicesCode1 = this.opener.document
			    .getElementById("naicsCode_1").value;
		    var naicesCode2 = this.opener.document
			    .getElementById("naicsCode_2").value;
		    var naicesCode0 = mySplitResult[1];
		    if ((naicesCode0 == naicesCode1) || (naicesCode0 == naicesCode2)) {
			this.opener.document.getElementById("naicsCode_" + rowcount).value = "";
			this.opener.document.getElementById("naicsDesc_" + rowcount).value = "";
			alert("NaicsCode is existed");
			flag=false;
			this.opener.document.getElementById("naicsCode_" + rowcount)
				.focus();
		    } else {
			this.opener.document.getElementById("naicsCode_" + rowcount).value = mySplitResult[1];
			this.opener.document.getElementById("naicsDesc_" + rowcount).value = mySplitResult[2];
			this.opener.document.getElementById("naicsCode_" + rowcount)
				.focus();
			this.opener.document.getElementById("naicsCode_" + rowcount).className = "textBox";
		    }
		}
	    }
	    if(flag){
		close_win();
		    }
	    
	}
</script>
</head>

<body bgcolor="#FFFFFF" style="font-family:Verdana; ">
	<html:form styleId="frm_findNaicCode" method="post"
		action="/NaicsAndIsic.do?method=viewNaicsCodes">
		<html:javascript formName="findNaicsCode" />
		<div class="RFP-Info-Box-Left">
			<h2 align="center">&nbsp;&nbsp;Find NAIC / ISIC Code</h2>
			<div class="Box-inside-cont">
				<table width="100%" border="0" cellpadding="3" cellspacing="2">
					<tr>
						<td><b>NAICS Category</b>&nbsp;</td>
						<bean:define id="selectedCateory" name="categoryId"></bean:define>
						<td><bean:define id="category" name="categoryName"></bean:define>
							<html:select property="naicsCategoryId"
								value="<%=selectedCateory.toString() %>"
								styleId="naicsCategoryId" style="width:210px;"
								name="findNaicsCode" onchange="fn_category()">
								<html:option value="">----Select---</html:option>
								<bean:size id="size" name="categoryName" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="category" name="categoryName">
										<bean:define id="id" name="category" property="id"></bean:define>
										<bean:define id="naicsCategoryDesc" name="category"
											property="naicsCategoryDesc"></bean:define>
										<html:option value="<%=id.toString() %>">
											<bean:write name="category" property="naicsCategoryDesc" />
										</html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select></td>
					</tr>
					<tr>
						<td><b>NAICS SubCategory</b>&nbsp;</td>
						<%
							String subCatId = "";
									if (session.getAttribute("subCategoryId") != null) {
										subCatId = session.getAttribute("subCategoryId")
												.toString();
									}
						%>
						<td><html:select property="naicsSubCategoryId"
								value="<%=subCatId%>" name="findNaicsCode"
								styleId="subCategoryId" style="width:210px;"
								onchange="fn_sub_category()">

								<html:option value="0" key="select">-----Select-----</html:option>
								<%
									if (session.getAttribute("subcategories") != null) {
								%>
								<bean:define id="subCategory" name="subcategories"></bean:define>
								<bean:size id="size" name="subcategories" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="subCategory" name="subcategories">
										<bean:define id="id" name="subCategory" property="id"></bean:define>
										<bean:define id="naicSubCategoryDesc" name="subCategory"
											property="naicSubCategoryDesc"></bean:define>
										<html:option value="<%=id.toString() %>">
											<bean:write name="subCategory" property="naicSubCategoryDesc"></bean:write>
										</html:option>
									</logic:iterate>
								</logic:greaterEqual>
								<%
									}
								%>
							</html:select></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>

					<tr>
						<td><b>NAICS / ISIC Code:</b></td>
						<td colspan="2"><html:select property="naicMasterCode"
								name="findNaicsCode" styleId="naicsMasterCode"
								styleClass="seltbox" size="10"
								style="width: 450px; border: 1px solid #c4c4c4">
								<%
									if (session.getAttribute("naicsMasters") != null) {
								%>
								<bean:size id="size" name="naicsMasters" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="naicsMst" name="naicsMasters">
										<bean:define id="id" name="naicsMst" property="id"></bean:define>
										<bean:define id="naicscode" name="naicsMst"
											property="naicsCode" />
										<bean:define id="naicsDesc" name="naicsMst"
											property="naicsDescription" />
										<bean:define id="isicDesc" name="naicsMst"
											property="isicDescription" />
										<bean:define id="isiccode" name="naicsMst" property="isicCode" />
										<html:option
											value="<%=id.toString()+'-'+naicscode.toString()+'-'+naicsDesc.toString()+'-'+isiccode.toString()+'-'+isicDesc.toString()%>">
											<bean:write name="naicsMst" property="naicsCode" />-
											<bean:write name="naicsMst" property="naicsDescription" />-
											<bean:write name="naicsMst" property="isicCode" />-
											<bean:write name="naicsMst" property="isicDescription" />
										</html:option>
									</logic:iterate>
								</logic:greaterEqual>
								<%
									}
								%>
							</html:select></td>
					</tr>
				</table>

				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="" align="center"><html:button
								styleClass="btTxt2 submit" property="" value="Submit"
								onclick="fn_setParentValue();" /> <html:button
								styleClass="btTxt2 submit" property="" value="Cancel"
								onclick="close_win();" /></td>
					</tr>
				</table>
			</div>
		</div>
	</html:form>

</body>
</html:html>