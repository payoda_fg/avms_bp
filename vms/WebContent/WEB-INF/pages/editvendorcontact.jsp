<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="com.fg.vms.customer.dto.RetriveVendorInfoDto,com.fg.vms.customer.model.VendorMaster"%>
<%
	RetriveVendorInfoDto vendorInfo1 = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");

	session.setAttribute("vendorMaster1", vendorInfo1.getVendorMaster());
%>

<!-- For JQuery Panel -->
<script class="jsbin" type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />

<script>
	$(document).ready(function() {
		<logic:iterate id="privilege1" name="privileges">
			<logic:equal value="Contact Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#formBox :input').attr('readonly', true);
					$('#formBox :radio, :checkbox').attr('disabled', true);
					$('#userSecQn').attr('disabled', true);
					$('#userSecQn').select2();
					$('#roleId').attr('disabled', true);
					$('#roleId').select2();
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		$('.panelCenter_1').panel({
			collapsible : false
		});
		
		if ($("#isPreparer").is(':checked')) {
			document.getElementById("preparer").style.display = "block";
		}else{
			document.getElementById("preparer").style.display = "none";
		}
		
		$("#userSecQn").select2({width: "90%"});
		$("#roleId").select2({width: "90%"});
	});
	
	function allowed_to_login() {
		if (document.getElementById("loginAllowed").checked == true) {
			document.getElementById("loginDisplay").style.visibility = "visible";
			$('#loginAllowed').val("on");
		} else {
			document.getElementById("loginDisplay").style.visibility = "hidden";
			document.getElementById("loginAllowed").value = "";
		}
	}
</script>

<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Contact Information" name="privilege" property="objectId.objectName">
		<div class="clear"></div>
		<div class="page-title" onload="allowed_to_login();">
			<img src="images/edit_user.png" alt="" />Edit Contact
		</div>
		<div class="clear"></div>

		<html:form action="/updateVendorContact?method=updateContact" styleClass="AVMS" styleId="vendorContactForm">
			<html:javascript formName="editVendorContactForm" />
			<div class="panelCenter_1">
				<h3>Update Contact Information</h3>
				<div class="form-box" id="formBox">
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">First Name</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="firstName" alt="" styleId="firstName" />
							</div>
							<span class="error"><html:errors property="firstName"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Last Name</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="lastName" alt="" styleId="lastName" />
							</div>
							<span class="error"><html:errors property="lastName"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Title</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="designation" alt="" styleId="designation" />
							</div>
							<span class="error"><html:errors property="designation"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Phone</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="contactPhone" alt="" styleId="contactPhone" />
							</div>
							<span class="error"><html:errors property="contactPhone"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Mobile</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="contactMobile" alt="Optional" styleId="contactMobile" />
							</div>
							<span class="error"><html:errors property="contactMobile"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">FAX</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="contactFax" alt="" styleId="contactFax" />
							</div>
							<span class="error"><html:errors property="contactFax"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Role</div>
							<div class="ctrl-col-wrapper">
								<html:select property="roleId" styleId="roleId">
									<html:option value="">--Select--</html:option>
									<logic:present name="vendorRoles">
										<html:optionsCollection name="vendorRoles" value="id" label="roleName" />
									</logic:present>														
								</html:select>
							</div>					
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Email ID</div>
							<div class="ctrl-col-wrapper">
								<html:hidden property="hiddenEmailId" alt="" styleId="hiddenEmailId" />
								<html:text property="contanctEmail" alt="" styleId="emailAddress"
									onchange="ajaxEdFn(this,'hiddenEmailId','VE');" styleClass="main-text-box" />
							</div>
							<span class="error"><html:errors property="contanctEmail"></html:errors></span>
						</div>						
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Password</div>
							<div class="ctrl-col-wrapper">
								<html:password property="loginpassword" styleClass="main-text-box" styleId="loginpassword" alt=""  />
							</div>
							<span class="error"><html:errors property="loginpassword"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Confirm Password</div>
							<div class="ctrl-col-wrapper">
								<html:password property="confirmPassword" styleClass="main-text-box" styleId="confirmPassword" alt="" />
							</div>
							<span class="error"><html:errors property="confirmPassword"></html:errors></span>
						</div>						
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Secret Question</div>
							<div class="ctrl-col-wrapper">
								<html:select property="userSecQn" styleId="userSecQn">
									<html:option value="">--Select--</html:option>
									<bean:size id="size" name="secretQnsList" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="secretQn" name="secretQnsList">
											<bean:define id="id" name="secretQn" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
							<span class="error"><html:errors property="userSecQn"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Secret Question Answer</div>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="main-text-box" property="userSecQnAns" alt="" styleId="userSecQnAns" />
							</div>
							<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
						</div>						
					</div>	
					<div class="wrapper-half">
						<div class="row-wrapper" id="gender">
							<div class="label-col-wrapper">Gender</div>
							<div class="ctrl-col-wrapper">
								<html:radio property="gender" value="M">Male</html:radio>
								<html:radio property="gender" value="F">Female</html:radio>
								<html:radio property="gender" value="T">Transgender</html:radio>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Primary Contact</div>
							<div class="ctrl-col-wrapper">
								<html:checkbox property="primaryContact" name="editVendorContactForm" styleId="primaryContact" />
							</div>
						</div>						
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Allowed to Login</div>
							<div class="ctrl-col-wrapper">
								<html:checkbox property="loginAllowed" styleId="loginAllowed" onclick="allowed_to_login()" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Business Contact</div>
							<div class="ctrl-col-wrapper">
								<html:checkbox property="isBusinessContact" styleId="isBusinessContact" />
							</div>
						</div>						
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper" id="preparer">
							<div class="label-col-wrapper">Preparer</div>
							<div class="ctrl-col-wrapper">
								<html:checkbox property="isPreparer" styleId="isPreparer" disabled="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="panelCenter_1" id="loginDisplay" style="visibility: hidden;padding-top: 2%;">
			</div>
			<div class="clear"></div>
			<div class="btn-wrapper">
				<logic:equal value="1" name="privilege" property="modify">
					<html:submit value="Update" styleClass="btn" styleId="submit1"></html:submit>
				</logic:equal>
				<html:reset value="Cancel" styleClass="btn" onclick="history.go(-1)"></html:reset>
			</div>
		</html:form>
	</logic:equal>
</logic:iterate>

<script>
	allowed_to_login();
	$(function($) {
		  $("#contactPhone").mask("(999) 999-9999?");
		  $("#contactFax").mask("(999) 999-9999?");
		  $("#contactMobile").mask("(999) 999-9999?");
	});
	$(document).ready(function() {
		jQuery.validator.addMethod("onlyNumbers", function(value, element) {
			return this.optional(element) || /^[0-9]+$/.test(value)
		}, "Please enter only numbers.");

		jQuery.validator.addMethod("phoneNumber", function(value, element) { 
	        return this.optional(element) || /^[0-9-]+$/.test(value);
	  	},"Please enter valid phone number.");
		
		jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
		},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
		
		$("#vendorContactForm").validate({

			rules : {
				firstName : {
					required : true,
					maxlength : 255
				},
				lastName : {
					required : true,
					maxlength : 255
				},
				designation : {
					required : true,
					maxlength : 255
				},
				contactPhone : {
					required : true
				},
				roleId : {
					required : true
				},
				contanctEmail : {
					required : true,
					email : true
				},
				loginpassword : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					password : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				confirmPassword : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					equalTo : "#loginpassword"
				},
				userSecQn : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				userSecQnAns : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		});
	});

	var config = {
		'.chosen-select' : {
			width : "90%"
		}
	};
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	}
</script>

