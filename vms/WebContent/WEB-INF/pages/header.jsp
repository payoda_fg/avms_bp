<%@page import="com.fg.vms.admin.dao.impl.LoginDaoImpl"%>
<%@page import="com.fg.vms.admin.dao.LoginDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.admin.model.Users"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html class="fixed sidebar-left-collapsed">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Porto Theme css -->
<!-- Web Fonts  -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
	rel="stylesheet" type="text/css" />

<!-- Vendor CSS -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" />
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css" />
<!--  <link href="jquery/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />-->
<link href="css/tabber.css" type="text/css" rel="stylesheet"></link>

<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/validation.js"></script>

<script src="jquery/js/jquery-1.7.2.min.js"></script>
<script src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>
<script src="jquery/js/jquery.placeholder.js"></script>
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
	
<script src="js/nanoscroller.js"></script>
<script src="js/theme.js"></script>
<script src="jquery/ui/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script src="js/theme.init.js"></script>
 <script type="text/javascript" src="js/tabber.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var current = location.pathname;
	    $('.nav li a').each(function(){
	        var $this = $(this);
	        // if the current path is like this link, make it active
	        if($this.attr('href').indexOf(current) !== -1){
	            $this.parent().addClass('active');
	        }
	    })
	});				
</script>


<script type="text/javascript">
	ddaccordion.init({
		headerclass : "submenuheader", //Shared CSS class name of headers group
		contentclass : "submenu", //Shared CSS class name of contents group
		revealtype : "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
		mouseoverdelay : 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
		collapseprev : true, //Collapse previous content (so only one open at any time)? true/false 
		defaultexpanded : [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
		onemustopen : false, //Specify whether at least one header should be open always (so never all headers closed)
		animatedefault : false, //Should contents open by default be animated into view?
		persiststate : true, //persist state of opened contents within browser session?
		toggleclass : [ "", "active" ], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
		//Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
		animatespeed : "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
		oninit : function(headers, expandedindices) { //custom code to run when headers have initalized
			/* alert(headers) */
			/* $(".submenuheader").click(function() {
				var $curr = $(this);
				$("a").removeClass("active");
				$("div").removeClass("active");
				$(this).addClass("active");
				$curr.parent().prev().addClass("active");
			}); */
			/* $('.box-title').addClass("active"); */
			/* $('#menuselect' + expandedindices).addClass("active"); */
		},
		onopenclose : function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
			//do nothing
			// create vendor menu option.
			/*  if (index == 0 && isuseractivated && state == 'block') {
				window.location = "viewVendor.do?method=view";
			}
			// search vendor menu option.
			if (index == 2 && isuseractivated && state == 'block') {
				window.location = "viewVendors.do?method=showVendorSearch";
			}
			alert(header + "  " + index + "  " + state + "  "
					+ isuseractivated) */
		}
	});
</script>
<script>
$(document).ready(function (){
	$(".sidepanelMenu").nanoScroller();
});	

</script>
<style>
/*.chosen-select {
	width: 90%;
}

.chosen-select-width {
	width: 90%;
}

.chosen-select-deselect: {
	width: 90%;
}

.chosen-select-no-single {
	width: 90%;
}*/
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var current = location.pathname;
	    $('.nav li a').each(function(){
	        var $this = $(this);
	        // if the current path is like this link, make it active
	        if($this.attr('href').indexOf(current) !== -1){
	            $this.parent().addClass('active');
	        }
	    })
	});
			
		/*$(".chosen-select").select2();
		$(".chosen-select-width").select2();
		$(".chosen-select-deselect").select2();
		$(".chosen-select-no-single").select2();*/
</script>
<header>
	<!--Header Pane Start Here-->
	<div class="page-wrapper">
		<header class="header header-nav-menu header-nav-stripe">
			<div class="logo-container float-left">
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails"
						property="settings.logoPath"></bean:define>
					<!-- <img
						src="<%=request.getContextPath()%>/images/dynamic/?file=<%=logoPath%>"
						height="56px;" /> -->
						<img
						src="<%=request.getContextPath()%>/images/logo.png"	/>
				</logic:present>
				<div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</div>
			<div class="header-right">
				<div class="notifications">
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#headerNavbar" aria-controls="headerNavbar" aria-expanded="false" aria-label="Toggle navigation">
					    <span class="fa fa-bars"></span>
					</button>
				</div>
				<div id="userbox" class="userbox">
					<a href="#" data-toggle="dropdown"> 
						<div class="profile-info" data-lock-name="AVMS Support">
							<span class="name"> <logic:present name="currentUser">
									<bean:define id="loginId" name="currentUser"
										property="userEmailId"></bean:define>
									<bean:write name="currentUser" property="userName" />
									
								</logic:present>
							</span>
						</div>
						<i class="fa custom-caret"></i>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdownMenu">
						<ul class="list-unstyled mb-2">
							<li class="divider"></li>
							<li><logic:present name="userId">
									<bean:define id="currentUserId" name="userId"></bean:define>
									<a role="menuitem" tabindex="-1"> <html:link
											action="/viewuser.do?parameter=retrive&var=userEdit"
											paramId="id" paramName="currentUserId">
											<i class="fa fa-user"></i> User Profile</html:link>
									</a>
								</logic:present></li>
							<li><html:link action="/logout.do">
									<i class="fa fa-power-off"></i> Sign out</html:link></li>
						</ul>
					</div>
				</div>
				<div id="userbox1" class="pl-1 userbox mr-5">
					<a href="#" data-toggle="dropdown"> 
						<div class="profile-info" data-lock-name="Help">
							<span class="name"> 									
									Help								
							</span>
						</div>
						<i class="fa custom-caret"></i>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdownMenu">
						<ul class="list-unstyled mb-2">
							<li class="divider"></li>
							<li><a href="http://support.avmsystem.com" target="_blank">Support</a></li>
								
								<li><a href="<%=request.getContextPath()%>/support/?file=${custCode}/help/Customer_User_Manual_v2.0.pdf" target="_blank"><i class="fa fa-user"></i>User Manual Documentation</a></li>
							</ul>
						</div>
					</div>
					<div class="header-nav collapse navbar-collapse" id="headerNavbar">
						<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 header-nav-main-square">
							<nav>
								<ul class="nav nav-pills" id="mainNav">
									<li>					
						<html:link action="customerhome.do?method=customerHomePage" styleClass="nav-link">Home</html:link>
						</li>
						
						<logic:iterate id="privilege" name="privileges">
							<logic:match value="Dashboard View" name="privilege"
								property="objectId.objectName">
								<logic:match value="1" name="privilege" property="view">
									<li><html:link styleClass="nav-link" action="reportdashboard.do?method=viewDashboard">
					Dashboard</html:link> </li>
			</logic:match>
							</logic:match>

						</logic:iterate>
						<li><a href="http://support.avmsystem.com" target="_blank">Contact
							Us</a> </li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
	</div>
</header>
</html>

					 <!-- <div class="dropdown-menu">
					<ul class="list-unstyled mb-2">
						<li><a role="menuitem" tabindex="-1" href="http://support.avmsystem.com" target="_blank">Support</a></li>
						<logic:present name="userDetails">
						<bean:define id="custCode" name="userDetails"
							property="customer.custCode"></bean:define></logic:present>
						<li>
						<a role="menuitem" tabindex="-1" href="<%=request.getContextPath()%>/support/?file=${custCode}/help/Customer_User_Manual_v2.0.pdf" target="_blank"><i class="fa fa-user"></i>User Manual Documentation
						</a></li>
					</ul>
				</div>-->
