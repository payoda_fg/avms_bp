<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments" %>
<%@page import="java.util.*" %>
 
<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css" />
<link href="select2/select2.css" rel="stylesheet"/>
<script src="select2/select2.js"></script>
<style>
	.chosen-select{width: 150px;} 
	.chosen-select-width{width : 90%}
	.chosen-select-deselect:{width : 90%}
</style>	
<script>
	$(document).ready(function() { $(".chosen-select").select2();
	$(".chosen-select-width").select2(); $("#businessArea").select2(); });
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$('#demoTab').easyResponsiveTabs();
		$("#contactInformation").click(function() {
			validateCertificate();
		});
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});
	    if ($("#same").is(':checked')) {
	    	alert('checked');
	    }
	});
	function deleteRow(val){
			$(val).remove();
	}
	function resetVendorForm() {
		window.location.reload(true);
	}
	function deleteRow(rowId){
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$("#" + table + " tr#" + rowId).remove();
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}
</script>
<div class="page-title">
	<img src="images/icon-registration.png">Vendor Registration
</div>
	<logic:present name="privileges">
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="add">
					<html:form method="post" enctype="multipart/form-data"
						styleClass="FORM" styleId="vendorForm"
						action="/createVendor?method=create">
						<html:javascript formName="vendorMasterForm" />
						<div id="demoTab" >
							<ul class="resp-tabs-list">
								<li><a href="#tabs-1" id="contactInformation" style="color: #fff;" tabindex="1">Contact
										Information</a></li>
								<li><a href="#tabs-2" style="color: #fff;" tabindex="13">Company Information</a></li>
								<li><a href="#tabs-3" style="color: #fff;" tabindex="75">Services Description</a></li>
								<li><a href="#tabs-4" style="color: #fff;" tabindex="93">Diverse Classification</a></li>
								<li><a href="#tabs-5" style="color: #fff;" tabindex="118">Quality Certifications</a></li>
								<li><a href="#tabs-6" style="color: #fff;" tabindex="172">Documents</a></li>
								<li><a href="#tabs-7" style="color: #fff;" tabindex="173">References</a></li>
								<li><a href="#tabs-8" style="color: #fff;" tabindex="204">Notes</a></li>
							</ul>
							<div class="resp-tabs-container">
								<div id="tabs-1">
									<%@include file="vendorcontactinfo.jsp"%>
								</div>
								<div id="tabs-2">
									<%@include file="vendorregister.jsp"%>
								</div>
								<div id="tabs-3">
									<%@include file="naicscodes.jsp"%>
								</div>
								<div id="tabs-4">
									<%@include file="diversecertificates.jsp"%>
								</div>
								<div id="tabs-5">
									<%@include file="qualityothercertificates.jsp"%>
								</div>
								<div id="tabs-6">
									<%@include file="vendorDocuments.jsp"%>
								</div>
								<div id="tabs-7">
									<%@include file="vendorReferences.jsp"%>
								</div>
								<div id="tabs-8">
									<%@include file="vendor_notes.jsp"%>
								</div>
							</div>
						</div>
						<div id="auth" style="padding-top: 1%;">
							<input type="checkbox" id="authentication" tabindex="112" >
							&nbsp;<label for="authentication"><b>Under 15 U.S C. 645(d), any person who misrepresents its size status shall 
							(1) be punished by a fine, imprisonment, or both; (2) be subject to administrative remedies; and 
							(3) be ineligible for participation in programs conducted under the authority of the Small Business Act.<br/>
							By choosing to submit this form, you certify that the information you have provided above is true and accurate</b></label>
						</div>
						<div class="btn-wrapper">
							<html:submit value="Submit" styleClass="btn" tabindex="225" styleId="submit1"
								disabled="true" style="background: none repeat scroll 0 0 #848484;"></html:submit>
							<html:reset value="Clear" styleClass="btn" tabindex="226"
								onclick="resetVendorForm();"></html:reset>
						<!-- 		<input type="button" id="btnPrevious" value="Previous" style = "display:none" class="btn"/>
								<input type="button" id="btnNext" value="Next" class="btn" />  -->
						</div>
					</html:form>

					<script type="text/javascript">
					$(function($) {
						  $("#phone").mask("(999) 999-9999?");
						  $("#contactPhone").mask("(999) 999-9999?");
						  $("#phone2").mask("(999) 999-9999?");
						  $("#bpContactPhone").mask("(999) 999-9999?");
						  $("#telephone").mask("(999) 999-9999?");
						  $("#referencePhone1").mask("(999) 999-9999?");
						  $("#referencePhone2").mask("(999) 999-9999?");
						  $("#referencePhone3").mask("(999) 999-9999?");
						  $("#ownerPhone1").mask("(999) 999-9999?");
						  $("#ownerPhone2").mask("(999) 999-9999?");
						  $("#ownerPhone3").mask("(999) 999-9999?");
						  $("#fax").mask("(999) 999-9999?");
						  $("#fax2").mask("(999) 999-9999?");
						  $("#contactFax").mask("(999) 999-9999?");
						  $("#contactMobile").mask("(999) 999-9999?");
					});
				    jQuery.validator.addMethod("alpha", function(value, element) { 
				          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
				    },"No Special Characters Allowed.");
				    jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
				          return this.optional(element) || /^[0-9]+$/.test(value);
				    },"Please enter only numbers.");
					jQuery.validator.addMethod("phoneNumber", function(value, element) { 
				        return this.optional(element) || /^[0-9-()]+$/.test(value);
				  	},"Please enter valid phone number.");
					jQuery.validator.addMethod("password",function(value, element) {
						return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
					},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
				    jQuery.validator.addMethod("allowhyphens", function(value, element) { 
				          return this.optional(element) || /^[0-9-]+$/.test(value);
				    },"Please enter valid numbers.");
						$(function() {
							$("#vendorForm").validate({
								rules : {
									vendorName : {
										required : true
									},
									dunsNumber : {
										allowhyphens : true
									},
									taxId : {
										required : true,
										allowhyphens : true
									},
									businessType:{required : true},
									companytype : {
										required : true
									},
									numberOfEmployees : {
										required : true,
										onlyNumbers : true
									},
									/* annualTurnover : {
										required : true
									},
									sales2: {
										required : true
									},sales3: {
										required : true
									}, */
									yearOfEstablishment : {
										required : true,
										range : [ 1900, 3000 ]
									},
									ethnicity : {
										range : [ 1, 1000 ]
									},
									address1 : {
										required : true,
										maxlength : 120
									},
									city : {
										required : true,
										alpha : true,
										maxlength : 60
									},
									state : {
										required : true,
										alpha : true,
										maxlength : 60
									},
									province : {
										maxlength : 60
									},
									region : {
										maxlength : 60
									},
									country : {
										required : true,
										maxlength : 120
									},
									zipcode : {
										required : true,
										allowhyphens : true
									},
									zipcode2 : {
										allowhyphens : true
									},
									referenceZip2 : {
										allowhyphens : true
									},
									referenceZip3 : {
										allowhyphens : true
									},
									mobile : {
										maxlength : 16 ,
										onlyNumbers : true 
									},
									mobile2 : {
										maxlength : 16 ,
										onlyNumbers : true 
									},
									referenceMobile1 : {
										maxlength : 16 ,
										onlyNumbers : true 
									},
									referenceMobile2 : {
										maxlength : 16 ,
										onlyNumbers : true 
									},
									referenceMobile3 : {
										maxlength : 16 ,
										onlyNumbers : true 
									},
									phone : {
										required : true
									},
									emailId : {
										required : true,
										maxlength : 120,
										email : true
									},
									referenceMailId2 : {
										maxlength : 120,
										email : true
									},	
									referenceMailId3 : {
										maxlength : 120,
										email : true
									},	
									naicsCode_0 : {
										required : true
									},
									firstName : {
										required : true ,
										alpha : true 
									},
									lastName : {
										required : true ,
										alpha : true 
									},
									designation : {
										required : true
									},
									contactPhone : {
										required : true
									},
									contanctEmail : {
										required : true,
										email : true,
										maxlength : 120
									},
									loginpassword : {
										required : true,
										password:true
									},
									confirmPassword : {
										required : true,
										equalTo : "#loginpassword"
									},
									userSecQn : {
										required : true
									},
									userSecQnAns : {
										required : true,
										rangelength : [ 3, 15 ]
									},
									extension1 : {
										onlyNumbers : true
									},
									extension2 : {
										onlyNumbers : true
									},
									extension3 : {
										onlyNumbers : true
									},
									bpContactPhoneExt : {
										onlyNumbers : true
									},
									vendorDescription : {
										rangelength : [ 0, 2000 ]
									},
									companyInformation : {
										rangelength : [ 0, 2000 ]
									},
									ownerName1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										}
									},
									ownerTitle1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										}
									},
									ownerEmail1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										},email : true
									},
									ownerPhone1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										}
									},
									ownerExt1 : {
										onlyNumbers : true
									},
									ownerExt2 : {
										onlyNumbers : true
									},
									ownerExt3 : {
										onlyNumbers : true
									},
									ownerMobile1 : {
										phoneNumber : true
									},
									ownerGender1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										},
									},
									ownerOwnership1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										},range : [ 0, 100 ]
									},
									ownerEthnicity1 : {
										required : function(element) {
											if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
												return true;
											} else {
												return false;
											}
										},
									},
									ownerMobile2 : {
										phoneNumber : true
									},
									ownerEmail2 : {
										email : true,
									},
									ownerOwnership2 : {
										range : [ 0, 100 ]
									},
									ownerMobile3 : {
										phoneNumber : true
									},
									ownerEmail3 : {
										email : true,
									},
									ownerOwnership3 : {
										range : [ 0, 100 ]
									},
									/*serviceArea : {
										required : true
									},
									businessArea : {
										required : true
									},*/
									referenceName1:{
										required : true
									},
									referenceAddress1:{
										required : true
									},
									referencePhone1:{
										required : true
									},
									referenceMailId1:{required : true, email : true},
									referenceCity1:{required : true},
									referenceZip1:{required : true, allowhyphens : true},
									referenceState1:{required : true},
									referenceCountry1:{required : true}
								},

								ignore : "ui-tabs-hide",
								submitHandler : function(form) {
									if (validateNaics() && validateCertificate()) {
										if (validateVendorOtherCertificate()) {
											$("#ajaxloader").css('display','block');
											$("#ajaxloader").show();
											form.submit();
										}
									}
								},

								invalidHandler : function(form,
										validator) {
									var errors = validator
											.numberOfInvalids();
									if (errors) {
										var invalidPanels = $(validator.invalidElements())
												.closest(".resp-tab-content",form);
										
										if (invalidPanels.size() > 0) {
											$.each($.unique(invalidPanels.get()), function() {
																
												if ( $(window).width() > 650 ) {
														$("a[href='#"+ this.id+ "']").parent()
														.not(".resp-accordion").addClass("error-tab-validation")
														.show("pulsate",{times : 3});
												} else {
													   $("a[href='#"+ this.id+ "']").parent()
														.addClass("error-tab-validation")
															.show("pulsate",{times : 3});
														}
												});
											
// 											if ( $.browser.msie ) {
// 												$('input[type="text"]').each(function() {
// 													if ($(this).attr('alt') == 'Optional'
// 														&& $(this).attr('placeholder') == 'Optional') {
// 														this.value = 'Optional';
// 													}
// 												});
// 											}
										}
									}
									$('input, textarea').placeholder();
								},
								unhighlight : function(element,
										errorClass, validClass) {
									$(element).removeClass(errorClass);
									$(element.form).find("label[for="+ element.id+ "]")
											.removeClass(errorClass);
									var $panel = $(element).closest(".resp-tab-content",element.form);

									if ($panel.size() > 0) {
										
										if ($panel.find(".error:visible").size() > 0) {

											if ($(window).width() > 650) {
												$("a[href='#"+ $panel[0].id+ "']")
														.parent()
														.removeClass("error-tab-validation");
											} else {
												$("a[href='#"+ $panel[0].id+ "']")
														.parent()
														.removeClass("error-tab-validation");
											}
										}
									}
								}
							});
						});
						
						var QueryString = function () {
							  // This function is anonymous, is executed immediately and 
							  // the return value is assigned to QueryString!
						  var query_string = {};
						  var query = window.location.search.substring(1);
						  var vars = query.split("&");
						  for (var i=0;i<vars.length;i++) {
						    var pair = vars[i].split("=");
						    	// If first entry with this name
						    if (typeof query_string[pair[0]] === "undefined") {
						      query_string[pair[0]] = pair[1];
						    	// If second entry with this name
						    } else if (typeof query_string[pair[0]] === "string") {
						      var arr = [ query_string[pair[0]], pair[1] ];
						      query_string[pair[0]] = arr;
						    	// If third or later entry with this name
						    } else {
						      query_string[pair[0]].push(pair[1]);
						    }
						  } 
						    return query_string;
						} ();
						
						if(QueryString.tire2vendor != "yes"){
							//alert(QueryString.tire2vendor);
							$("#hideDivforTier2Vendor").css('display','block');
						}
						
					</script>
				</logic:equal>
				<logic:equal value="0" name="privilege" property="add">
					<div class="form-box">
						<h3>You have no rights to add new vendor</h3>
					</div>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
	</logic:present>
