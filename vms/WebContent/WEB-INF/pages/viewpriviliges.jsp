<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>

<SCRIPT type="text/javascript">
	function myconfirmBox(roleName) {
		window.location = "roleprivileges.do?method=viewPrivilegesByRole";
	}
</SCRIPT>


<layout:form action="/roleprivileges.do?method=saveObject"
	styleClass="PANEL">


	<layout:select property="roleName" key="Role Name">



		<bean:size id="size" name="userRoles" />
		<logic:greaterEqual value="0" name="size">
			<logic:iterate id="role" name="userRoles">
				<bean:define id="id" name="role" property="id"></bean:define>
				<bean:define id="roleName" name="role" property="roleName"></bean:define>
				<layout:option value="<%=id.toString() %>"
					key="<%=roleName.toString() %>">

				</layout:option>

			</logic:iterate>
		</logic:greaterEqual>
	</layout:select>
	<layout:datagrid property="datagrid" styleClass="DATAGRID"
		model="datagrid">
		<layout:datagridText property="objectName" title="Object Name" />
		<layout:datagridCheckbox property="add" title="Add" />
		<layout:datagridCheckbox property="delete" title="Delete" />
		<layout:datagridCheckbox property="modify" title="modify" />
		<layout:datagridCheckbox property="view" title="View" />
		<layout:datagridCheckbox property="visible" title="Visible" />
		<layout:datagridCheckbox property="enable" title="Enable" />
	</layout:datagrid>

	<layout:submit value="Submit" styleClass="btTxt submit"></layout:submit>


</layout:form>

