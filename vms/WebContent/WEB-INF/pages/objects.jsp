<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<script type="text/javascript"
	src="jquery/js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#objectForm").validate({
			rules : {
				objectName : {
					required : true
				},
				objectType : {
					required : true
				},
				objectDescription : {
					required : true
				}
			}
		});

		$('#objectTable').tableScroll({height:150});
	});

	// Function to reset the form
	function clearfields() {
		window.location = "viewconfig.do?parameter=view";
	}
//-->
</script>
<script type="text/javascript"
	src="jquery/js/jqueryUtils.js"></script>

<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">

		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Object
				Entry
			</h2>
		</div>
		<div id="successMsg">
			<html:messages id="msg" property="object" message="true">
				<span style="color: green;"><bean:write name="msg" /></span>
			</html:messages>
		</div>
		<div id="table-holder" style="margin: 0 1.5%;">
			<html:form action="/config?parameter=insertObject"
				styleId="objectForm">
				<html:javascript formName="configurationForm" />
				<table style="margin: 0 0 0 28%;">
					<tr>
						<td><p>
								<label for="objectName">Object Name</label>
								<html:text property="objectName" name="configurationForm"
									alt="" styleId="objectName"></html:text>
								<span class="error"><html:errors property="objectName"></html:errors></span>
							</p></td>
					</tr>
					<tr>
						<td><p>
								<label for="objectType">Object Type</label>
								<html:text property="objectType" name="configurationForm"
									alt="" styleId="objectType"></html:text>
								<span class="error"><html:errors property="objectType"></html:errors></span>
							</p></td>
					</tr>
					<tr>
						<td><p>
								<label for="objectDescription">Object Description</label>
								<html:textarea property="objectDescription" style="width:50%;"
									name="configurationForm" alt="" styleId="objectDescription"></html:textarea>
								<span class="error"><html:errors
										property="objectDescription"></html:errors></span>
							</p></td>
					</tr>
					<tr>
						<td><p>
								<label for="isActive">IsActive</label>
								<html:radio property="isActive" value="1"
									name="configurationForm">&nbsp;Yes&nbsp;</html:radio>
								<html:radio property="isActive" value="0"
									name="configurationForm" disabled="true">&nbsp;No&nbsp;</html:radio>
								<span style="color: red;"><html:errors
										property="isActive"></html:errors></span>
							</p></td>
					</tr>
				</table>
				<div style="padding: 0 0 1% 35%;">

					<logic:iterate id="privilege" name="privileges">
						<logic:match value="Objects" name="privilege"
							property="objectId.objectName">
							<logic:match value="1" name="privilege" property="add">
								<html:submit value="Submit" styleClass="btTxt submit"
									styleId="submit"></html:submit>
								<html:reset value="Clear" styleClass="btTxt submit"
									onclick="clearfields();"></html:reset>
							</logic:match>
						</logic:match>
					</logic:iterate>

				</div>
			</html:form>
		</div>
		<logic:iterate id="privilege" name="privileges">

			<logic:match value="Objects" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">
					<h2>
						<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Available
						Objects
					</h2>
					<div id="box" style="margin-left: 1.5%;">
						<table width="96%" id="objectTable">
							<bean:size id="size" name="objects" />
							<logic:greaterThan value="0" name="size">
								<thead>
									<tr>
										<th>Object Name</th>
										<th>Object Type</th>
										<th>Object Description</th>
										<th>IsActive</th>
										<th>Actions</th>
									</tr>
								</thead>
								<logic:iterate name="objects" id="objectlist">
									<tbody>
										<tr>
											<td><bean:write name="objectlist" property="objectName" /></td>
											<td><bean:write name="objectlist" property="objectType" /></td>
											<td style="width: 200px;"><bean:write name="objectlist"
													property="objectDescription" /></td>
											<td align="center">Yes</td>
											<bean:define id="objectId" name="objectlist" property="id"></bean:define>
											<td><logic:iterate id="privilege" name="privileges">

													<logic:match value="Objects" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="modify">

															<html:link action="/retriveobject.do?parameter=retrive"
																paramId="id" paramName="objectId">Edit</html:link>
														</logic:match>
													</logic:match>

												</logic:iterate> <logic:iterate id="privilege" name="privileges">

													<logic:match value="Objects" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="delete">
												| <html:link
																action="/deleteobject.do?parameter=deleteobject"
																paramId="id" paramName="objectId"
																onclick="return confirm_delete();">Delete</html:link>
														</logic:match>
													</logic:match>

												</logic:iterate></td>
										</tr>
									</tbody>
								</logic:iterate>
							</logic:greaterThan>
							<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
						</table>
					</div>
				</logic:match>
			</logic:match>

		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:match value="Objects" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3> You have no rights to view objects</font>
					</h3>
				</logic:match>
			</logic:match>

		</logic:iterate>

	</div>
</div>
