<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script>
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null) {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2();
				}
			});
		}
	}
</script>
<div class="clear"></div>
<div class="panelCenter_1">
	<h3>Reference 1</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceName1" alt="" styleClass="text-box"
						styleId="referenceName1" tabindex="132" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="referenceAddress1"
						styleId="referenceAddress1" alt="" styleClass="main-text-area"
						tabindex="133" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referencePhone1" alt="" tabindex="134"
						styleClass="text-box" styleId="referencePhone1"
						style="width:47%;display:inline-block;" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ext</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension1" styleId="extension1" size="3"
						styleClass="text-box" alt="Optional"
						style="width:22%;display:inline-block;" tabindex="135" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMailId1" alt="" styleClass="text-box"
						styleId="referenceMailId1" tabindex="136" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMobile1" alt=""
						styleClass="text-box" styleId="referenceMobile1" tabindex="137" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceCity1" alt="" styleClass="text-box"
						styleId="referenceCity1" tabindex="138" />
				</div>
				<span class="error"> <html:errors property="referenceCity1"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceZip1" alt="" styleClass="text-box"
						styleId="referenceZip1" tabindex="139" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceCountry1"
						styleId="referenceCountry1" styleClass="chosen-select-width"
						tabindex="140" onchange="changestate(this,'referenceState1');">
						<html:option value="">- Select -</html:option>
						<bean:size id="size" name="countryList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countryList">
								<bean:define id="name" name="country" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="countryname" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceState1" styleId="referenceState1"
						styleClass="chosen-select-width" tabindex="140">
						<html:option value="">- Select -</html:option>
						<logic:present name="editVendorMasterForm" property="refStates1">
							<logic:iterate id="states" name="editVendorMasterForm"
								property="refStates1">
								<bean:define id="name" name="states" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="states" property="statename" />
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">County/Province</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="referenceProvince1"
						alt="" styleId="referenceProvince1" tabindex="140" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Reference 2</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceName2" alt="Optional"
						styleClass="text-box" styleId="referenceName2" tabindex="141" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="referenceAddress2"
						styleId="referenceAddress2" alt="Optional"
						styleClass="main-text-area" tabindex="142" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referencePhone2" alt=""
						styleClass="text-box" styleId="referencePhone2" style="width:45%;"
						tabindex="143" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ext</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension2" styleId="extension2" size="3"
						styleClass="text-box" alt="Optional" style="width:22%;"
						tabindex="144" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMailId2" alt="Optional"
						styleClass="text-box" styleId="referenceMailId2" tabindex="145" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMobile2" alt=""
						styleClass="text-box" styleId="referenceMobile2" tabindex="146" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceCity2" alt="Optional"
						styleClass="text-box" styleId="referenceCity2" tabindex="147" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceZip2" alt="Optional"
						styleClass="text-box" styleId="referenceZip2" tabindex="148" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceCountry2"
						styleId="referenceCountry2" styleClass="chosen-select-width"
						tabindex="149" onchange="changestate(this,'referenceState2');">
						<html:option value="">- Select -</html:option>
						<bean:size id="size" name="countryList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countryList">
								<bean:define id="name" name="country" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="countryname" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceState2" styleId="referenceState2"
						styleClass="chosen-select-width" tabindex="150">
						<html:option value="">- Select -</html:option>
						<logic:present name="editVendorMasterForm" property="refStates2">
							<logic:iterate id="states" name="editVendorMasterForm"
								property="refStates2">
								<bean:define id="name" name="states" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="states" property="statename" />
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">County/Province</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="referenceProvince2"
						alt="Optional" styleId="referenceProvince2" tabindex="150" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 2%;">
	<h3>Reference 3</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceName3" alt="Optional"
						styleClass="text-box" styleId="referenceName3" tabindex="151" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="referenceAddress3"
						styleId="referenceAddress3" alt="Optional"
						styleClass="main-text-area" tabindex="152" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referencePhone3" alt="" tabindex="153"
						styleClass="text-box" styleId="referencePhone3" style="width:45%;" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ext</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension3" styleId="extension3" size="3"
						styleClass="text-box" alt="Optional" style="width:22%;"
						tabindex="154" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMailId3" alt="Optional"
						styleClass="text-box" styleId="referenceMailId3" tabindex="155" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceMobile3" alt=""
						styleClass="text-box" styleId="referenceMobile3" tabindex="156" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceCity3" alt="Optional"
						styleClass="text-box" styleId="referenceCity3" tabindex="157" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip</div>
				<div class="ctrl-col-wrapper">
					<html:text property="referenceZip3" alt="Optional"
						styleClass="text-box" styleId="referenceZip3" tabindex="158" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceCountry3"
						styleId="referenceCountry3" styleClass="chosen-select-width"
						tabindex="159" onchange="changestate(this,'referenceState3');">
						<html:option value="">-Select-</html:option>
						<bean:size id="size" name="countryList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countryList">
								<bean:define id="name" name="country" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="countryname" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:select property="referenceState3" styleId="referenceState3"
						styleClass="chosen-select-width" tabindex="160">
						<html:option value="">- Select -</html:option>
						<logic:present name="editVendorMasterForm" property="refStates3">
							<logic:iterate id="states" name="editVendorMasterForm"
								property="refStates3">
								<bean:define id="name" name="states" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="states" property="statename" />
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">County/Province</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="referenceProvince3"
						alt="Optional" styleId="referenceProvince3" tabindex="160" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
