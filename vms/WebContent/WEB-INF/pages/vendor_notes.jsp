<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#stateValue").val($("#contactState option:selected").text());
	}); 
//-->
</script>
<div class="panelCenter_1">
	<h3>Notes</h3>
	<div class="wrapper-full" id="bpsupplier">
		<div class="row-wrapper form-group row">
			<div class="ctrl-col-wrapper" style="padding: 1%; width: 90%">
				<html:textarea property="vendorNotes" styleClass="main-text-area"
					cols="100" tabindex="205" style="height:90px;padding:0;">
				</html:textarea>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 1%;">
	<h3>&nbsp;BP contact information</h3>
	<div id="slider1"></div>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Contact First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactFirstName" alt=""
						styleId="contactFirstName" styleClass="text-box" readonly="true" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Contact Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactLastName" alt=""
						styleId="contactLastName" styleClass="text-box" readonly="true" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Contact Date</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactDate" alt="" styleId="contactDate"
						styleClass="text-box" readonly="true" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Contact State</div>
				<div class="ctrl-col-wrapper">
					<input type="text" class="text-box" id="stateValue" readonly="readonly">
					<html:select property="contactState" styleId="contactState"
						styleClass="chosen-select-width" style="display:none;">
						<html:option value="">- Select -</html:option>
						<logic:present name="contactStates">
							<logic:iterate id="states" name="contactStates">
								<bean:define id="name" name="states" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="states" property="statename" />
								</html:option>
							</logic:iterate>
						</logic:present>
					</html:select>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Contact Event</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="contactEvent" alt=""
						styleId="contactEvent" styleClass="main-text-area" readonly="true" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
