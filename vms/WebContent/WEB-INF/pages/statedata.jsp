<%@page import="com.fg.vms.customer.model.State"%>
<%@page import="java.util.*,java.util.ArrayList"%>

<%
	List<State> states = null;
	if (session.getAttribute("states") != null) {
		states = (List<State>) session.getAttribute("states");
	}
	StringBuffer buffer = new StringBuffer(
			"<option value=''>&nbsp;&nbsp;</option>");
	if (states != null && !states.isEmpty()) {
		for (State state : states) {
			buffer.append("<option value='" + state.getId() + "'>"
					+ state.getStatename() + "</option>");
		}
	}

	response.setHeader("Cache-Control", "no-cache");
	response.getWriter().println(buffer);
%>
