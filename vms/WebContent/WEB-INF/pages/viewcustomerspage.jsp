<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<div id="successMsg" style="color: red;">
	<span><html:errors property="referencecustomer" /> </span>
</div>
<div class="page-title">

	<div style="float: left;">
		<img src="images/Vendor-Approval.gif" alt="" />Following are your
		customers
	</div>
	<logic:iterate id="privilege" name="privileges">

		<logic:match value="Customer" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">
				<html:link forward="/createCustomer"
					style="color:#fff;text-decoration:none;float: right;"
					styleClass="linkToButton">Create Customer</html:link>
			</logic:match>
		</logic:match>

	</logic:iterate>
</div>

<div class="form-box">
	<logic:iterate id="privilege" name="privileges">

		<logic:match value="Customer" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">

				<table class="main-table">
					<bean:size id="size" name="customersList" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="header">Company Name</td>
								<td class="header">DUNS Number</td>
								<td class="header">Country</td>
								<td class="header">Region</td>
								<td class="header">State</td>
								<td class="header">City</td>
								<td class="header">Action</td>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="6" class="rounded-foot-left"><em>&nbsp;</em></td>
								<td class="rounded-foot-right">&nbsp;</td>
							</tr>
						</tfoot>
						<logic:iterate id="customer" name="customersList">
							<tr>
								<td align="left"><bean:define id="customerId"
										name="customer" property="id"></bean:define> <logic:iterate
										id="privilege" name="privileges">
										<logic:match value="Customer" name="privilege"
											property="objectId.objectName">
											<logic:match value="1" name="privilege" property="modify">
												<html:link action="/retriveCustConfiguration?method=retrive"
													paramId="id" paramName="customerId">
													<bean:write name="customer" property="custName" />
												</html:link>
											</logic:match>
											<logic:match value="0" name="privilege" property="modify">

												<bean:write name="customer" property="custName" />

											</logic:match>
										</logic:match>
									</logic:iterate></td>

								<td align="left"><bean:write name="customer"
										property="dunsNumber" /></td>

								<td align="left"><bean:write name="customer"
										property="country" /></td>

								<td align="left"><bean:write name="customer"
										property="region" /></td>

								<td align="left"><bean:write name="customer"
										property="state" /></td>

								<td align="left"><bean:write name="customer"
										property="city" /></td>
								<td><logic:iterate id="privilege" name="privileges">
										<logic:match value="Customer" name="privilege"
											property="objectId.objectName">
											<logic:match value="1" name="privilege" property="delete">
												<html:link action="/deletecustomer?method=deleteCustomer"
													paramId="id" paramName="customerId"
													onclick="return confirm_delete();">Delete</html:link>
											</logic:match>
										</logic:match>

									</logic:iterate></td>

							</tr>
						</logic:iterate>

					</logic:greaterThan>

					<logic:equal value="0" name="size">
						<div style="text-align: center;">
							<h3>No customers records to display</h3>
						</div>
					</logic:equal>

				</table>

				<div class="clear"></div>
				<!-- </div> -->
			</logic:match>
		</logic:match>

	</logic:iterate>

</div>

<logic:iterate id="privilege" name="privileges">

	<logic:match value="Customer" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view customers</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>

