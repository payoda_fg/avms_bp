<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<html:messages id="msg" property="rfip" message="true">

	<script type="text/javascript">
		alert("Successfully saved  ");
	</script>
</html:messages>
<article>
	<!--Content Area Pane Start Here-->
	<div class="page-wrapper">
		<div id="content-area">

			<div class="item-box-three">
				<div class="box-width">
					<div class="box-title">Administration</div>
					<div class="btn-bg">
						<logic:present name="privileges">
							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Roles" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<a href="#menu1" class="icon-12 "
											onclick="linkPage('viewfgrole.do?parameter=viewUserRoles')">Manage
											Roles</a>
									</logic:equal>
								</logic:equal>
							</logic:iterate>
							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="User" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<a href="#menu2" class="icon-13 "
											onclick="linkPage('viewfguser.do?parameter=viewUsers')">Manage
											Users</a>
										<%-- <html:link action="/viewfguser.do?parameter=viewUsers"
								styleClass="icon-13" styleId="menuselect1">Manage Users</html:link> --%>
									</logic:equal>
								</logic:equal>
							</logic:iterate>

							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Role Privileges" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<a href="#menu3" class="icon-17"
											onclick="linkPage('fgroleprivileges.do?method=rolePrivileges')">Role
											Privileges</a>


									</logic:equal>
								</logic:equal>
							</logic:iterate>
							<logic:iterate id="privilege" name="privileges">
								<logic:equal value="Customer" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<a href="#menu4" class="icon-14"
											onclick="linkPage('displayCustomer.do?method=display')">Manage
											Customer<br>Data
										</a>
									</logic:equal>
								</logic:equal>
							</logic:iterate>

						</logic:present>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Content Area Pane End Here-->
</article>


<script>
	$(".submenuheader").click(function() {
		var $curr = $(this);
		$("a").removeClass("active");
		$("div").removeClass("active");
		$(this).addClass("active");
		$curr.parent().prev().addClass("active");
	});

	$(".sub").click(function() {
		$(".sub").removeClass("active");
		$(this).addClass("active");
	});
</script>
