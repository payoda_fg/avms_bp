<%@page import="com.fg.vms.admin.model.CustomerContacts"%>
<%@page import="com.fg.vms.admin.model.Customer"%>
<%@page import="com.fg.vms.admin.dto.CustomerInfoDto"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%
	CustomerInfoDto customerInfoDto = (CustomerInfoDto) session
			.getAttribute("customerInfo");
	Customer customer = customerInfoDto.getCustomer();
%>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>

<div>
	<header class="card-header pb-5">
		<h2 class="card-title pull-left">Customer Contact Information</h2>
	</header>

	<div class="form-box card-body">
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="firstName" alt=""
						styleId="firstName" tabindex="19" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="lastName"
						styleId="lastName" alt="Optional" tabindex="20" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>
			</div>			
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="designation"
						styleId="designation" alt="Optional" tabindex="21" />
				</div>
				<span class="error"><html:errors property="designation"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="contactPhone"
						styleId="contactPhone" alt="" tabindex="22" />
				</div>
				<span class="error"><html:errors property="contactPhone"></html:errors></span>
			</div>		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="contactMobile"
						styleId="contactMobile" alt="" tabindex="23" />
				</div>
				<span class="error"><html:errors property="contactMobile"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">FAX</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="contactFax"
						styleId="contactFax" alt="" tabindex="24" />
				</div>
				<span class="error"><html:errors property="contactFax"></html:errors></span>
			</div>
		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:hidden property="hiddenEmailId" alt=""
						styleId="hiddenEmailId" />
					<html:text property="contanctEmail" alt="" styleId="contanctEmail"
						onchange="ajaxEdFn(this,'hiddenEmailId','CE');"
						styleClass="text-box form-control" tabindex="25" />
					<html:hidden property="hiddenLoginId" />
				</div>
				<span class="error"><html:errors property="contanctEmail"></html:errors></span>
			</div>
		
	</div>
	<div class="clear"></div>
</div>

<div id="loginId">
	<header class="card-header pb-5">
		<h2 class="card-title pull-left">User Information</h2>
	</header>

	<div class="form-box card-body">
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Display Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="loginDisplayName" alt="Optional"
						styleId="loginDisplayName" tabindex="26" styleClass="text-box form-control" />
				</div>
				<span class="error"><html:errors property="loginDisplayName"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Login Id</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:hidden property="hiddenLoginId" alt=""
						styleId="hiddenLoginId" />
					<html:text property="loginId" styleId="loginId" alt=""
						styleClass="text-box"
						onchange="ajaxEdFn(this,'hiddenLoginId','CU');" tabindex="27" />
				</div>
				<span class="error"><html:errors property="loginId"></html:errors></span>
			</div>			
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Password</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:password property="loginpassword" styleId="loginpassword"
						tabindex="28" styleClass="text-box form-control" />
				</div>
				<span class="error"><html:errors property="loginpassword"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Confirm Password</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:password property="confirmPassword" styleId="confirmPassword"
						tabindex="29" styleClass="text-box form-control" />
				</div>
				<span class="error"><html:errors property="confirmPassword"></html:errors></span>
			</div>			
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:select property="userSecQn" styleId="userSecQn"
						styleClass="list-box" tabindex="30">
						<html:option value="">----Select---</html:option>
						<bean:size id="size" name="secretQnsList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="secretQn" name="secretQnsList">
								<bean:define id="id" name="secretQn" property="id"></bean:define>
								<html:option value="<%=id.toString()%>">
									<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question Answer</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="userSecQnAns" alt="Optional"
						styleId="userSecQnAns" styleClass="text-box form-control" tabindex="31" />
				</div>
				<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
			</div>
	</div>
	<div class="clear"></div>
</div>
