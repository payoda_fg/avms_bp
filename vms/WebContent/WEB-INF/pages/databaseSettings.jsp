<%@page import="com.fg.vms.util.Constants"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<div class="panelCenter_1">
	<h3>Database Settings</h3>

	<div class="form-box">
		<div class="wrapper-full">
			<div class="row-wrapper form-group row">
				<div class="label-col-full-wrapper">Database Name</div>
				<div class="ctrl-col-full-wrapper">
					<html:text property="databaseName" alt=""
						styleClass="main-text-box" styleId="databaseName"
						onchange="ajaxFn(this,'DB');" tabindex="40" />
				</div>
				<span class="error"><html:errors property="databaseName"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-full-wrapper">Database IP/Host</div>
				<div class="ctrl-col-full-wrapper">
					<html:text property="databaseIp" alt="" styleId="databaseIp"
						styleClass="main-text-box" tabindex="41" />
				</div>
				<span class="error"><html:errors property="databaseIp"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-full-wrapper">Database UserName</div>
				<div class="ctrl-col-full-wrapper">
					<html:text property="userName" alt="" styleId="userName"
						styleClass="main-text-box" tabindex="42" />
				</div>
				<span class="error"><html:errors property="userName"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-full-wrapper">Database Password</div>
				<div class="ctrl-col-full-wrapper">
					<html:password property="dbpassword" alt="" styleId="dbpassword"
						styleClass="main-text-box" tabindex="43" style="width:85%;" />
				</div>
				<span class="error"><html:errors property="dbpassword"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-full-wrapper">Application URL (http://)</div>
				<div class="ctrl-col-full-wrapper">
					<html:text property="applicationUrl" alt=""
						styleClass="main-text-box" styleId="applicationUrl"
						onchange="ajaxFn(this,'URL');" tabindex="44" />
					<%
						out.println("."
								+ Constants.getString(Constants.DOMAIN_NAME).toString()
										.replace("www.", "") + ")");
					%>
				</div>
				<span class="error"><html:errors property="applicationUrl"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>

</div>
