<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<html:messages id="msg" property="customer" message="true">

	<script type="text/javascript">
		alert("Successfully saved customer profile ");
	</script>
</html:messages>

<div class="panelCenter_1">
	<h3>Company</h3>

	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Company Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="companyname" alt="" styleId="companyname"
						styleClass="text-box" tabindex="2" />
				</div>
				<span class="error"><html:errors property="companyname"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Company Code</div>
				<div class="ctrl-col-wrapper">
					<html:text property="companycode" alt="" styleId="companycode"
						onchange="ajaxFn(this, 'CC');" styleClass="text-box" tabindex="3" />
				</div>
				<span class="error"><html:errors property="companycode"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Duns Number</div>
				<div class="ctrl-col-wrapper">
					<html:text property="dunsnum" alt="" styleClass="text-box"
						onchange="ajaxFn(this, 'D')" styleId="dunsnum" tabindex="4" />
				</div>
				<span class="error"><html:errors property="dunsnum"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Tax ID</div>
				<div class="ctrl-col-wrapper">
					<html:text property="taxid" alt="Optional" styleClass="text-box"
						onchange="ajaxFn(this, 'T')" styleId="taxid" tabindex="5" />
				</div>
				<span class="error"><html:errors property="taxid"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>

</div>
<div class="panelCenter_1">
	<h3>Address</h3>

	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="address1" alt=""
						styleId="address1" tabindex="6" />
				</div>
				<span class="error"><html:errors property="address1"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="city" alt=""
						styleId="city" tabindex="7" />
				</div>

				<span class="error"> <html:errors property="city"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="state" alt=""
						styleId="state" tabindex="8" />
				</div>
				<span class="error"><html:errors property="state"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Province</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="province" alt="Optional"
						styleId="province" tabindex="9" />
				</div>
				<span class="error"> <html:errors property="province"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Region</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="region" alt="Optional"
						styleId="region" tabindex="10" />
				</div>
				<span class="error"> <html:errors property="region"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="country" styleId="country"
						styleClass="list-box" tabindex="11">
						<html:option value="">-- Select --</html:option>
						<bean:size id="size" name="countryList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countryList">
								<bean:define id="name" name="country" property="name"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="name" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
				<span class="error"> <html:errors property="country"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip Code</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="zipcode" alt="Optional"
						styleId="zipcode" tabindex="12" />
				</div>
				<span class="error"> <html:errors property="zipcode"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="mobile" alt="Optional"
						styleId="mobile" tabindex="13" />
				</div>
				<span class="error"> <html:errors property="mobile"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="phone" alt=""
						styleId="phone" tabindex="14" />
				</div>
				<span class="error"> <html:errors property="phone"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Fax</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="fax" alt="Optional"
						styleId="fax" tabindex="15" />
				</div>
				<span class="error"> <html:errors property="fax"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Website URL(http://)</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="webSite" alt="Optional"
						styleId="webSite" tabindex="16" />
				</div>
				<span class="error"><html:errors property="webSite"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="email" alt=""
						styleId="email" tabindex="17" />
				</div>
				<span class="error"><html:errors property="email"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
