<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@page import="java.util.*"%>

<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css" />
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<script>
	$(document).ready(function() {
		/*$("select.chosen-select").select2({
			width : "90%"
		});*/
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$.datepicker.setDefaults({
			changeYear : true,
			dateFormat : 'mm/dd/yy'
		});
		$("#contactDate").datepicker();
		
		$("#countryValue").val($("#country option:selected").text());
	});
	var 	emailObj= null;
	var emailId =null;
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != '') {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2({
						width : "90%"
					});
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
			$('#' + id).select2({
				width : "90%"
			});
		}
	}
	function resetVendorForm() {
		window.location = "viewVendor.do?method=primeVendor";
	}	
</script>
<section role="main" class="content-body card-margin mt-3">
	<div class="row">
		<div class="col-lg-9 mx-auto">	
			<header class="card-header">
				<h2 class="card-title">
				<img src="images/icon-registration.png">Create Vendor</h2>
			</header>
			<div class="clear"></div>
		<div id="successMsg">
			<html:messages id="msg" property="vendor" message="true">
				<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
			</html:messages>
			<html:messages id="msg" property="transactionFailure" message="true">
				<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
			</html:messages>
		</div>
<logic:present name="privileges">
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Create Vendor" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="add">
				<html:form method="post" enctype="multipart/form-data"
					styleClass="FORM" styleId="vendorForm"
					action="/createPrimeVendor?method=createPrimeVendor">
					<html:javascript formName="vendorMasterForm" />
					<section class="card">
						<header class="card-header">
							<h2 class="card-title">Company</h2>
							</header>
						<div class="form-box card-body">							
								<div class="row-wrapper form-group row">	
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Approved</label>
										<div class="col-sm-9 ctrl-col-wrapper">
										<div class="checkbox-custom chekbox-primary pt-2">
											<input type="checkbox" name="isApproved" value="1"
												disabled="disabled" checked="checked">
											<label for=""></label>
										</div>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">
										<bean:message key="comp.name" />
									</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text property="vendorName" alt="" styleId="vendorName"
											styleClass="text-box form-control" />
									</div>
									<span class="error"> <html:errors property="vendorName"></html:errors>
									</span>
								</div>
							
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Email Address</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="emailId"
											styleId="emailId" alt="" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Address</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:textarea styleClass="main-text-area" property="address1"
											alt="" styleId="address1" rows="7" cols="40" />
									</div>
								</div>
						
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">City</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="city" alt=""
											styleId="city" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Country</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<logic:present name="userDetails" property="workflowConfiguration">
											<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
												<html:select property="country" styleId="country" styleClass="chosen-select form-control" onchange="changestate(this,'state');">
													<html:option value="">--Select--</html:option>
													<logic:present name="countryList">
														<html:optionsCollection name="countryList" value="id" label="countryname" />
													</logic:present>
												</html:select>
												<span class="error"> <html:errors property="country"></html:errors></span>
											</logic:equal>
											<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
												<input type="text" class="form-control" id="countryValue" readonly="readonly">
												<html:select property="country" styleId="country" styleClass="chosen-select form-control"
													onchange="changestate(this,'state');" style="display:none;">
													<logic:present name="countryList">
														<html:optionsCollection name="countryList" value="id" label="countryname" />
													</logic:present>
												</html:select>											
											</logic:equal>
										</logic:present>
									</div>
								</div>
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">State</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:select property="state" styleId="state" styleClass="chosen-select form-control">
											<html:option value="">--Select--</html:option>
											<logic:present name="statesList">
												<html:optionsCollection name="statesList" value="id" label="statename" />
											</logic:present>
										</html:select>
									</div>
									<span class="error"> <html:errors property="state"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Zip Code</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="zipcode" alt=""
											styleId="zipcode" />
									</div>
								</div>
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Phone Number</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="phone" alt=""
											styleId="phone" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Fax Number</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="fax" alt=""
											styleId="fax" />
									</div>
								</div>
								
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact Name</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="firstName" alt=""
											styleId="firstName" />
									</div>
								</div>
								<div class="row-wrapper form-group row" id="gender">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Gender</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<div class="radio-custom radio-primary">
										<html:radio property="gender" value="M">&nbsp;Male&nbsp;</html:radio>
										<label for=""></label>
										</div>
										<div class="radio-custom radio-primary">
										<html:radio property="gender" value="F">&nbsp;Female&nbsp;</html:radio>
										<label for=""></label>
										</div>
										<div class="radio-custom radio-primary">
										<html:radio property="gender" value="T">&nbsp;Transgender&nbsp;</html:radio>
										<label for=""></label>
										</div>
									</div>
								</div>
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Title</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="designation"
											styleId="designation" alt="" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact Phone</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="contactPhone"
											styleId="contactPhone" alt="" />
									</div>
								</div>
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact Email Address</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="contanctEmail"
											styleId="contanctEmail" alt="" onchange="ajaxFn(this,'VE');" />
									</div>
									<span class="error"><html:errors
											property="contanctEmail"></html:errors></span>
								</div>
								<div class="row-wrapper form-group row">
									<logic:equal value="1" name="isDivisionStatus" property="isDivision">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Customer Division</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:select property="customerDivision" styleId="customerDivision" styleClass="chosen-select form-control">
												<html:option value="">--Select--</html:option>
												<logic:present name="customerDivisionList">
													<html:optionsCollection name="customerDivisionList" value="id" label="divisionname" />
												</logic:present>
											</html:select>
										</div>
										<span class="error"> <html:errors property="customerDivision"></html:errors></span>
									</logic:equal>
								</div>				
							<div class="clear"></div>
						</div>
					</section>
						<div class="clear"></div>
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">BP Contact Information</h2>
							</header>
							<div class="form-box card-body">
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact First Name</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text property="contactFirstName" alt=""
											styleId="contactFirstName" styleClass="text-box form-control" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact Last Name</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text property="contactLastName" alt=""
											styleId="contactLastName" styleClass="text-box form-control" />
									</div>
								</div>
								
									<div class="row-wrapper form-group row">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact Date</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="contactDate"
												alt="Please click to select date" styleId="contactDate"
												styleClass="text-box form-control" readonly="true" />
										</div>
									</div>
									<div class="row-wrapper form-group row">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact State</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:select property="contactState" styleId="contactState"
												styleClass="chosen-select form-control">
												<html:option value="">- Select -</html:option>
												<logic:present name="contactStates">
													<logic:iterate id="states" name="contactStates">
														<bean:define id="name" name="states" property="id"></bean:define>
														<html:option value="${name }">
															<bean:write name="states" property="statename" />
														</html:option>
													</logic:iterate>
												</logic:present>
											</html:select>
										</div>
									</div>
								
								
									<div class="row-wrapper form-group row">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Contact Event</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:textarea property="contactEvent" alt=""
												styleId="contactEvent" styleClass="main-text-area form-control" />
										</div>
									</div>
						<div class="clear"></div>

						<footer class="mt-2 card-footer">
							<div class="row justify-content-end">
								<div class="col-sm-9 wrapper-btn">
							<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit1"></html:submit>
							<html:reset value="Clear" styleClass="btn btn-default"
								onclick="resetVendorForm();"></html:reset>							
						</div>
						</div>
						</footer>
						</div>
					</section>
					
				</html:form>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="add">
				<div class="form-box">
					<h3>You have no rights to add new vendor</h3>
				</div>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
</logic:present>
</div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		$("#phone").mask("(999) 999-9999?");
		$("#contactPhone").mask("(999) 999-9999?");
		$("#fax").mask("(999) 999-9999?");
	});
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
	}, "No Special Characters Allowed.");
	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	jQuery.validator.addMethod("phoneNumber", function(value, element) {
		return this.optional(element) || /^[0-9-()]+$/.test(value);
	}, "Please enter valid phone number.");
	jQuery.validator
			.addMethod(
					"password",
					function(value, element) {
						return this.optional(element)
								|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/)
										.test(value);
					},
					"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	jQuery.validator.addMethod("allowhyphens", function(value, element) {
		return this.optional(element) || /^[0-9-]+$/.test(value);
	}, "Please enter valid numbers.");
	$("#vendorForm")
			.validate(
					{
						rules : {
							vendorName : {
								required : true,
								maxlength : 255
							},
							address1 : {
								required : true,
								maxlength : 120
							},
							city : {
								required : true,
								alpha : true,
								maxlength : 60
							},
							country : {
								required : true
							},
							state : {
								required : function(element) {
									if ($("#country option:selected").text() == 'United States') {
										return true;
									} else {
										return false;
									}
								},
								alpha : true,
								maxlength : 60
							},
							zipcode : {
								required : true,
								allowhyphens : true,
								maxlength : 255
							},
							phone : {
								required : true
							},
							emailId : {
								required : true,
								maxlength : 120,
								email : true
							},
							firstName : {
								required : true,
								maxlength : 255,
								alpha : true
							},
							designation : {
								required : true,
								maxlength : 255
							},
							contactPhone : {
								required : true
							},
							contanctEmail : {
								required : true,
								email : true,
								maxlength : 120
							},
							customerDivision : {
								required : true
							},
							contactFirstName:{
								maxlength : 255,
								alpha : true
							},
							contactLastName:{
								maxlength : 255,
								alpha : true
							}
						},
					});
</script>
