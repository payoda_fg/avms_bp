<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>


<script>
	$(function($) {
		$("#phoneNumber").mask("(999) 999-9999?");
	});
	jQuery.validator
			.addMethod(
					"password",
					function(value, element) {
						return this.optional(element)
								|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/)
										.test(value);
					},
					"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	
	jQuery.validator.addMethod("alphaNumSC", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9\.\_\-\s]+$/.test(value);
	}, "Please Enter Valid Values.");

	$(document).ready(function() {
		$("#formID").validate({
			rules : {
				userName : {
					required : true,
					minlength : 4,
					maxlength : 255
					//alphaNumSC : true
				},
				userPassword : {
					required : true,
					password : true
				},
				confirmPassword : {
					required : true,
					equalTo : "#userPassword"
				},
				userEmailId : {
					required : true,
					email : true
				},
				roleId : {
					required : true
				},
				secretQuestionId : {
					required : true
				},
				secQueAns : {
					required : true
				},
				firstName : {
					required : false,
					minlength : 2,
					maxlength : 255,
					alphaNumSC : true
				},
				lastName : {
					required : false,
					minlength : 2,
					maxlength : 255,
					alphaNumSC : true
				},
				title : {
					required : false,
					minlength : 2,
					maxlength : 255
					//alphaNumSC : true
				},
				department : {
					required : false,
					minlength : 2,
					maxlength : 255
					//alphaNumSC : true
				},
				division : {
					required : false,
					minlength : 2,
					maxlength : 255
					//alphaNumSC : true
				},
				extension : {
					required : false,
					onlyNumbers : true
				},
				phoneNumber : {
					required : true
				},
				timeZone : {
					required : false
				}
				<logic:equal value="1" name="isDivisionStatus"
					property="isDivision">,
				customerDivisionId : {
					required : true
				} </logic:equal>
			},
			ignore : ":hidden:not(select)"
		});
		
		//For multiselect 
		<logic:notPresent name="isGlobalDivisionId">
			$("#customerDivision").multiselect({
				selectedText : "# of # selected"
			});
		</logic:notPresent>
		<logic:present name="isGlobalDivisionId">
			$("#customerDivision").multiselect(
			{
				selectedText : "# of # selected",
				click: function(e, ui)
				{
					if(ui.value == ${isGlobalDivisionId} && ui.checked)//If BP Global Checked
					{
						$("#customerDivision").multiselect("widget").find("input:checkbox").each(function()
						{
							if($(this).val() != ${isGlobalDivisionId})
							{
								$(this).attr('disabled','disabled');
							}
						});
					}
					else if(ui.value == ${isGlobalDivisionId} && !ui.checked)//If BP Global Unchecked
					{
						$("#customerDivision").multiselect("widget").find("input:checkbox").each(function()
						{
							$(this).removeAttr('disabled');
						});
					}
					else if(ui.value != ${isGlobalDivisionId} && ui.checked)//If Others Checked
					{
						$("#customerDivision").multiselect("widget").find("input:checkbox").each(function()
						{
							if($(this).val() == ${isGlobalDivisionId})
							{
								$(this).attr('disabled','disabled');
							}
						});
					}
					else if(ui.value != ${isGlobalDivisionId} && !ui.checked)//If Others Unchecked
					{
						var isSelected = false;
						var bpGlobal;
						$("#customerDivision").multiselect("widget").find("input:checkbox").each(function()
						{
							if($(this).val() == ${isGlobalDivisionId})
							{
								bpGlobal=this;
							}
							if($(this).val() != ${isGlobalDivisionId} && $(this).is(":checked"))
							{
								isSelected = true;
							} 
						});
						if(!isSelected)
						{
							$(bpGlobal).removeAttr('disabled');
						}
					}
				},
				checkAll: function()
				{
					$("#customerDivision").multiselect("widget").find("input:checkbox").each(function()
					{
						if($(this).val() == ${isGlobalDivisionId})
						{
							$(this).removeAttr('checked');
							$(this).attr('disabled','disabled');
						}
						else
						{
							$(this).removeAttr('disabled');
							$(this).attr('checked','checked');
						}
					});
				},
				uncheckAll: function()
				{
					$("#customerDivision").multiselect("widget").find("input:checkbox").each(function()
					{
						$(this).removeAttr('checked');
						$(this).removeAttr('disabled');
					});
				}
			});
		</logic:present>
	});
 
	// Function to reset the form
	function clearfields() {
		window.location = "viewuser.do?parameter=viewUsers";
	}
</script>
<bean:define id="currentUserId" name="userId"></bean:define>


<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<section class="card">
				<div id="successMsg">
					<html:messages id="msg" property="user" message="true">
						<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
					</html:messages>
					<html:messages id="msg" property="deleteUser" message="true">
						<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
					</html:messages>
					<html:messages id="msg" property="updateUser" message="true">
						<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
					</html:messages>
				</div>
				<header class="card-header">
					<h2 class="card-title">
						<img src="images/userRegistration.png" alt="" />User Registration
					</h2>
				</header>

				<!--Row End Here-->
				<div class="form-box card-body">
					<html:form action="/createUser?parameter=create" styleId="formID">
						<html:javascript formName="userForm" />
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="User" name="privilege"
								property="objectId.objectName">
								<logic:match value="1" name="privilege" property="add">

									<div class="form-group row row-wrapper">
										<label
											class="col-sm-3 control-label text-sm-right label-col-wrapper">First
											Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="firstName" name="userForm"
												alt="Optional" styleId="firstName"
												styleClass="text-box form-control" />
										</div>
									</div>
									<div class="form-group row row-wrapper">
										<label
											class="col-sm-3 control-label text-sm-right label-col-wrapper">Last
											Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="lastName" name="userForm" alt="Optional"
												styleId="lastName" styleClass="text-box form-control" />
										</div>
									</div>

									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Title</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="title" name="userForm" alt="Optional"
													styleId="title" styleClass="text-box form-control" />
											</div>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Department</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="department" name="userForm"
													alt="Optional" styleId="department"
													styleClass="text-box form-control" />
											</div>
										</div>
									</div>
									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Division</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="division" name="userForm"
													alt="Optional" styleId="division"
													styleClass="text-box form-control" />
											</div>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">User
												Name</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="userName" name="userForm" alt=""
													styleId="userName" styleClass="text-box form-control" />
											</div>
											<span class="error"><html:errors property="userName"></html:errors></span>
										</div>
									</div>
									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Phone
												Number</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="phoneNumber" name="userForm" alt=""
													styleId="phoneNumber" styleClass="text-box form-control" />
											</div>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Extension</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="extension" name="userForm"
													styleId="extension" alt="Optional"
													styleClass="text-box form-control" />
											</div>
										</div>
									</div>
									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Time
												Zone</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:select property="timeZone" name="userForm"
													styleClass="chosen-select form-control" styleId="timeZone">
													<html:option value="">----Select---</html:option>
													<logic:present name="zoneMasters">
														<bean:size id="size" name="zoneMasters" />
														<logic:greaterEqual value="0" name="size">
															<logic:iterate id="timezone" name="zoneMasters">
																<bean:define id="id" name="timezone" property="id"></bean:define>
																<option value="${id}">
																	<bean:write name="timezone" property="zoneDesc"></bean:write>
																</option>
															</logic:iterate>
														</logic:greaterEqual>
													</logic:present>
												</html:select>
											</div>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Email
												ID</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="userEmailId" name="userForm" alt=""
													styleId="userEmailId" onchange="ajaxFn(this,'U');"
													styleClass="text-box form-control" />
											</div>
											<span class="error"><html:errors
													property="userEmailId"></html:errors></span>
										</div>
									</div>
									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">User
												Password</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:password property="userPassword" name="userForm"
													styleId="userPassword" styleClass="text-box form-control" />
											</div>
											<span class="error"><html:errors
													property="userPassword"></html:errors></span>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Confirm
												Password</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:password property="confirmPassword" name="userForm"
													styleId="confirmPassword"
													styleClass="text-box form-control" />
											</div>
											<span class="error"><html:errors
													property="confirmPassword"></html:errors></span>
										</div>
									</div>
									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">User
												Role</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:select property="roleId" name="userForm"
													styleId="roleId" styleClass="chosen-select form-control">
													<html:option value="">----Select---</html:option>
													<bean:size id="size" name="userRoles" />
													<logic:greaterEqual value="0" name="size">
														<logic:iterate id="role" name="userRoles">
															<bean:define id="id" name="role" property="id"></bean:define>
															<html:option value="${id}">
																<bean:write name="role" property="roleName"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:greaterEqual>
												</html:select>
												<span class="error"><html:errors property="roleId"></html:errors></span>
											</div>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Secret
												Question</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:select property="secretQuestionId" name="userForm"
													styleClass="chosen-select form-control"
													styleId="secretQuestionId">
													<html:option value="">----Select---</html:option>
													<logic:present name="secretQuestionsList">
														<bean:size id="size" name="secretQuestionsList" />
														<logic:greaterEqual value="0" name="size">
															<logic:iterate id="secretQn" name="secretQuestionsList">
																<bean:define id="id" name="secretQn" property="id"></bean:define>
																<option value="${id}">
																	<bean:write name="secretQn"
																		property="secQuestionParticular"></bean:write>
																</option>
															</logic:iterate>
														</logic:greaterEqual>
													</logic:present>
												</html:select>

												<span class="error"><html:errors
														property="secretQuestionId"></html:errors> </span>
											</div>
										</div>
									</div>
									<div class="wrapper-half">
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Secret
												Question Answer</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="secQueAns" name="userForm"
													styleClass="text-box form-control" styleId="secQueAns" />
												<span class="error"><html:errors property="secQueAns"></html:errors>
												</span>
											</div>
										</div>
										<div class="form-group row row-wrapper">
											<logic:equal value="1" name="isDivisionStatus"
												property="isDivision">
												<label
													class="col-sm-3 control-label text-sm-right label-col-wrapper">Customer
													Division</label>
												<div class="col-sm-9 ctrl-col-wrapper">
													<html:select property="customerDivisionId"
														styleClass="form-control" name="userForm" multiple="true"
														styleId="customerDivision">
														<bean:size id="size" name="customerDivisions" />
														<logic:greaterEqual value="0" name="size">
															<logic:iterate id="customerDivision"
																name="customerDivisions">
																<bean:define id="id" name="customerDivision"
																	property="id"></bean:define>
																<html:option value="${id}">
																	<bean:write name="customerDivision"
																		property="divisionname"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:greaterEqual>
													</html:select>
												</div>
												<span class="error"><html:errors
														property="customerDivisionId"></html:errors> </span>
											</logic:equal>
										</div>
									</div>

									<footer class="card-footer mt-4">
										<div class="row justify-content-end">
											<div class="col-sm-9 wrapper-btn">
												<html:submit value="Submit" styleClass="btn btn-primary"
													styleId="submit"></html:submit>
												<html:reset value="Clear" styleClass="btn btn-default"
													onclick="clearfields();"></html:reset>
											</div>
										</div>
									</footer>
								</logic:match>
							</logic:equal>
						</logic:iterate>
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="User" name="privilege"
								property="objectId.objectName">
								<logic:match value="0" name="privilege" property="add">
									<h3>You have no rights to add users</h3>
								</logic:match>
							</logic:equal>
						</logic:iterate>
					</html:form>
				</div>
			</section>
		</div>	
	</div>



	<!--Row End Here-->
	<div class="grid-wrapper">
		<div class="form-box">
			<div class="row">
				<div class="col-lg-12 mx-auto">
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="User" name="privilege"
							property="objectId.objectName">
							<logic:match value="1" name="privilege" property="view">
								<header class="card-header">
									<h2 class="card-title">
										<img src="images/user-group.gif" alt="" />Registered Users
									</h2>
								</header>
								<div class="card-body" id="grid_container">
									<table
										class="main-table table table-bordered table-striped mb-0">
										<bean:size id="size" name="users" />
										<logic:greaterThan value="0" name="size">
											<thead>
												<tr>
													<th>User Name</th>
													<th>User Email Id</th>
													<th>User Role</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<logic:iterate name="users" id="userlist">
													<tr>
														<td><bean:write name="userlist" property="userName" /></td>
														<td><bean:write name="userlist"
																property="userEmailId" /></td>
														<td><bean:write name="userlist"
																property="userRoleId.roleName" /></td>
														<bean:define id="userId" name="userlist" property="id"></bean:define>
														<td><logic:iterate id="privilege1" name="privileges">
																<logic:equal value="User" name="privilege1"
																	property="objectId.objectName">
																	<logic:match value="1" name="privilege1"
																		property="modify">
																		<html:link action="/viewuser.do?parameter=retrive"
																			paramId="id" paramName="userId">Edit</html:link>
																	</logic:match>
																</logic:equal>
															</logic:iterate>|<logic:iterate id="privilege2" name="privileges">
																<logic:equal value="User" name="privilege2"
																	property="objectId.objectName">
																	<logic:match value="1" name="privilege2"
																		property="delete">
																		<logic:notEqual value="${currentUserId}"
																			name="userlist" property="id">
																			<html:link action="/viewuser.do?parameter=delete"
																				paramId="id" paramName="userId"
																				onclick="return confirm_delete();">Delete</html:link>
																		</logic:notEqual>
																		<logic:equal value="${currentUserId}" name="userlist"
																			property="id">
																			<span style="color: red; font-weight: bold;">Current
																				User</span>
																		</logic:equal>
																	</logic:match>
																</logic:equal>
															</logic:iterate></td>
													</tr>
												</logic:iterate>
											</tbody>
										</logic:greaterThan>
										<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
									</table>
								</div>
							</logic:match>
						</logic:equal>
					</logic:iterate>
					<logic:iterate id="privilege" name="privileges">
						<logic:equal value="User" name="privilege"
							property="objectId.objectName">
							<logic:match value="0" name="privilege" property="view">
								<div class="form-box">
									<h3>You have no rights to view users</h3>
								</div>
							</logic:match>
						</logic:equal>

					</logic:iterate>
				</div>
			</div>
			
		</div>
	</div>
</section>