<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>

<script type="text/javascript" src="jquery/js/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>
<link href="select2/select2.css" rel="stylesheet"/>
<script src="select2/select2.js"></script>
<style>
	.chosen-select-width{width : 90%}
</style>
<script type="text/javascript">
$(document).ready(function() {$(".chosen-select-width").select2();  });
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$("#tabs").easyResponsiveTabs();
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});
		$("#manualFlag").css("display","block");
	});
	function cancle() {
		window.location = "viewSubVendors.do?method=showSubVendorSearch";
	}
	function beforeSubmit(){
		 $('#vendorForm').append("<input type='hidden' name='moreInfo' value='editsubvendor' />");
		 return true;
	}

	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement
				: null);
		if (evt != null
				&& node != null
				&& ((evt.keyCode == 13) && ((node.type == "text")
						|| (node.type == "radio") || (node.type == "textarea") || (node.type == "checkbox")))) {
			if ($('#authentication').is(":checked")) {
				return true;
			} else if (node.id == 'search') {
			} else {
				alert("Please certify that the information you have provided is true and accurate. ");
				return false;
			}
		}
	}

	document.onkeypress = stopRKey;
</script>
<html:form enctype="multipart/form-data"
						action="/tier2MoreInfo?method=saveMoreInfo" styleId="vendorForm">
<div class="page-title">
	<img src="images/edit-cust.gif" />&nbsp;Vendor More Information
	<html:submit value="Back" styleClass="btn" style="float:right;"	styleId="submit" onclick="return beforeSubmit();"></html:submit>
	</div>
<div class="form-box">
	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="modify">
					<div class="clear"></div>
						<html:javascript formName="editVendorMasterForm" />
						<html:hidden property="id" styleId="vendorId"></html:hidden>
						<div id="tabs">
							<ul class="resp-tabs-list">
								<li><a href="#tabs-1" style="color: #fff;" tabindex="193">References</a></li>
								<li><a href="#tabs-2" style="color: #fff;" tabindex="224">Business Biography</a></li> 
								<li><a href="#tabs-3" style="color: #fff;" tabindex="275">Documents</a></li> 
							</ul>
							<div class="resp-tabs-container">
								<div id="tabs-1">
								<%@include file="editVendorReferences.jsp"%>
								</div>
								<div id="tabs-2">
									<%@include file="biographySafety.jsp"%>
								</div> 
								<div id="tabs-3">
									<%@include file="editVendorDocuments.jsp"%>
								</div> 
							</div>
						</div>
						<div class="clear"></div>
					<div id="auth" style="padding-top: 1%;">
						<input type="checkbox" id="authentication" tabindex="112" >
						&nbsp;<label for="authentication"><b>Under 15 U.S C. 645(d), any person who misrepresents its size status shall 
						(1) be punished by a fine, imprisonment, or both; (2) be subject to administrative remedies; and 
						(3) be ineligible for participation in programs conducted under the authority of the Small Business Act.<br/>
						By choosing to submit this form, you certify that the information you have provided above is true and accurate</b></label>
					</div>
						<div class="btn-wrapper">
							<html:submit value="Update" styleClass="btn" styleId="submit1" tabindex="277" 
								disabled="true" style="background: none repeat scroll 0 0 #848484;"></html:submit>
							<html:reset value="Cancel" styleClass="btn" tabindex="278" onclick="cancle();"></html:reset>
						</div>
					
					<script type="text/javascript">
						$(function($) {
							  $("#telephone").mask("(999) 999-9999?");
							  $("#bpContactPhone").mask("(999) 999-9999?");
							  $("#referencePhone1").mask("(999) 999-9999?");
							  $("#referencePhone2").mask("(999) 999-9999?");
							  $("#referencePhone3").mask("(999) 999-9999?");
							  $("#referenceMobile1").mask("(999) 999-9999?");
							  $("#referenceMobile2").mask("(999) 999-9999?");
							  $("#referenceMobile3").mask("(999) 999-9999?");
						});
					
						$(function() {
							
							jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
						          return this.optional(element) || /^[0-9]+$/.test(value)
						    },"Please enter only numbers.");
							 
							jQuery.validator.addMethod("allowhyphens", function(value, element) { 
						          return this.optional(element) || /^[0-9-]+$/.test(value);
						    },"Please enter valid numbers.");
							
							$("#vendorForm").validate({
										rules : {
											referenceName1:{required : true},
											referenceAddress1:{required : true},
											referencePhone1:{required : true,maxlength : 16},
											referenceMailId1:{required : true},
											referenceCity1:{required : true},
											referenceZip1:{required : true, allowhyphens : true},
											referenceState1:{required : function(element) {if($("#referenceCountry1 option:selected").text()=='United States') {return true;}else{return false;}}},
											referenceState2:{required : function(element) {if($("#referenceCountry2 option:selected").text()=='United States') {return true;}else{return false;}}},
											referenceState3:{required : function(element) {if($("#referenceCountry3 option:selected").text()=='United States') {return true;}else{return false;}}},
											referenceCountry1:{required : true},
											referenceZip2 : {allowhyphens : true},
											referenceZip3 : {allowhyphens : true},
											referenceMailId2 : {maxlength : 120,email : true},	
											referenceMailId3 : {
												maxlength : 120,
												email : true
											},	
											extension1 : {
												onlyNumbers : true
											},
											extension2 : {
												onlyNumbers : true
											},
											extension3 : {
												onlyNumbers : true
											},
											bpContactPhoneExt : {
												onlyNumbers : true
											}		
										},
										ignore : "ui-tabs-hide",
										submitHandler : function(form) {
											if (validateNaics()) {
												$("#ajaxloader").css('display','block');
												$("#ajaxloader").show();
												form.submit();
											}
										},
										invalidHandler : function(form,
												validator) {
											$("#ajaxloader").css('display','none');
											var errors = validator
													.numberOfInvalids();
											if (errors) {
												var invalidPanels = $(validator.invalidElements())
														.closest(".resp-tab-content",form);
												if (invalidPanels.size() > 0) {
													$.each($.unique(invalidPanels.get()), function() {
																		
														if ( $(window).width() > 650 ) {
																$("a[href='#"+ this.id+ "']").parent()
																.not(".resp-accordion").addClass("error-tab-validation")
																.show("pulsate",{times : 3});
														} else {
															   $("a[href='#"+ this.id+ "']").parent()
																.addClass("error-tab-validation")
																	.show("pulsate",{times : 3});
																}
														});
												}
											}
										},
										unhighlight : function(element,
												errorClass, validClass) {
											$(element).removeClass(errorClass);
											$(element.form).find("label[for="+ element.id+ "]")
													.removeClass(errorClass);
											var $panel = $(element).closest(".resp-tab-content",element.form);
											if ($panel.size() > 0) {
												// console.log($panel.find(".error:visible").size());
												if ($panel.find(".error:visible").size() > 0) {
													if ($(window).width() > 650) {
														$("a[href='#"+ $panel[0].id+ "']")
																.parent()
																.removeClass("error-tab-validation");
													} else {
														$("a[href='#"+ $panel[0].id+ "']")
																.parent()
																.removeClass("error-tab-validation");
													}
												}
											}
										}
									});
						});
					</script>
					<div class="clear"></div>
				</logic:equal>
				<logic:equal value="0" name="privilege" property="modify">
					<div class="form-box">
						<h3>You have no rights to modify vendor information</h3>
					</div>
				</logic:equal>
			</logic:equal>
		</logic:iterate>
	</logic:present>
</div>
</html:form>