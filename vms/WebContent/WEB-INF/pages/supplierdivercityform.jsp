<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/additional-methods.js"></script>
<!-- <script type="text/javascript" src="tinymce/tinymce.min.js"></script> -->

<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<!-- For Tooltip -->
<script type="text/javascript" src="jquery/ui/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/tooltipster.css" />

<!-- <script type="text/javascript" src="ajax/ajaxvalidation.js"></script> -->
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<script type="text/javascript" src="jquery/js/additional-methods.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<link href="css/form.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script> 

<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>

<script type="text/javascript" src="jquery/js/jqueryUtils.js">
	
</script>
<style type="text/css">
/* Sortable style starts here */
/* Modified for Sorting Header*/
.main-table td,th {
	border: 1px solid #e9eaea;
	padding: 5px;
}

.main-table th.header {
	background: #009900;
	color: #fff;
	cursor: pointer;
}

/* Sorting Table Header */
table.sortable th:not (.sorttable_nosort ):not (.sorttable_sorted ):not
	 (.sorttable_sorted_reverse ):after {
	content: " \25B4\25BE"
}
/* Sortable style ends here */
.ajax-loader{
display: none;
}

.select2-container-multi .select2-choices {
    height: 29px !important;
    overflow-y: scroll;
}

</style>

<style>
#documentSize {
	width: 30%;
}

<!-- /* textarea.content { */
/* 	width: 500px; */
/* 	height: 130px; */
/* } */

/* textarea.emailId { */
/* 	width: 370px; */
/* 	height: 60px; */
/* } */

/* .row-wrapper .label-col-full-wrapper { */
/* 	float: left; */
/* 	line-height: 25px; */
/* 	padding: 0 0 0 2%; */
/* 	width: 12%; */
/* } */
.row-wrapper .ctrl-col-full-wrapper {
	padding: 0 0 0 0%;
}

.row-wrapper .ctrl-col-full-wrapper #message {
	padding: 0 0 0 50%;
}

-->
</style>

<script>

function printPDFXLS(id) {			
	$('#supplierDiversityId').val(id);
	$("#exportPDFXLS").css({
		"display" : "block"
	});
	$("#exportPDFXLS").dialog({
		minWidth : 300,
		modal : true
	});
}

// function exportSupplierDiversityReport()
// {
// 	var exportType = $("input[name=export]:checked").val();
// 	var supplierDiversityId = $('#supplierDiversityId').val();
	
// 	if (exportType == 1) {
// 		window.location = "generateVendorReport.do?method=generateSupplierDiversityXLS&id=" + supplierDiversityId;
// 	} else if (exportType == 3) {
// 		window.location = "generateVendorReport.do?method=generateSupplierDiversity&id=" + supplierDiversityId;		
// 	} else {
// 		alert('Please Choose Export Type');
// 		return false;
// 	}
// 	$('#exportPDFXLS').dialog('close');
// }

function createSupplierDiversity() {	

	tinymce.init({
		selector : "textarea#scopOfWork",
		theme : "modern",
		statusbar : false,
		height : 300,
		width : 930,
		plugins : [
				"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
		toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect",
		toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
		menubar : false,
		/* file_browser_callback: AVMSFileBrowser, */
		toolbar_items_size : 'medium'

	});
	$("#dialog").css({
		"display" : "block"
	});
	$("#dialog").dialog({
		minWidth : 975,
		modal : true,
		close: function () { 
	    	window.location = "supplierDiversity.do?method=supplierDivercityForm";
	    	
	     }
    
	});

}

// tinymce.init({
// 			selector : "textarea#scopOfWork",
// 			theme : "modern",
// 			statusbar : false,
// 			height : 300,
// 			width : 930,
// 			plugins : [
// 					"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
// 					"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
// 					"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
// 			toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
// 			toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
// 			menubar : false,
// 			/* file_browser_callback: AVMSFileBrowser, */
// 			toolbar_items_size : 'medium'

// 		});
<%-- function AVMSFileBrowser(field_name, url, type, win) {
	
	   var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;

	   tinyMCE.activeEditor.windowManager.open({
	     file: avmsFileman,
	     title: 'Email Template',
	     width: 400, 
	     height: 400,
	     resizable: "yes",
	     plugins: "media",
	     inline: "yes",
	     close_previous: "no"  
	  }, {     window: win,     input: field_name    });
	  return false; 
	} --%>

	$(document).ready(function() {
		if(${form}){
			createSupplierDiversity();
		}
		$('.panelCenter_1').panel({
			header: "h4",
			active : false,
			collapsible : true
		});

		var lastDate = new Date();
		lastDate.setDate(lastDate.getDate());
		
		$.datepicker.setDefaults({
			changeYear : true,
			defaultDate: lastDate,
			dateFormat : 'mm/dd/yy'
				
		});
		
		$("#requestedOn").datepicker();
		$("#sourceEventStartDate").datepicker();
		$("#estimateWorkStartDate").datepicker();
		$("#lastResponseDate").datepicker();

		$('#savesettings').click(function() {
			// Validation for follow workflow
			var oBtnsfollow = document.getElementsByName('followWF');
			var isfollowChecked = false;
			for (i = 0; i < oBtnsfollow.length; i++) {
				if (oBtnsfollow[i].checked) {
					isfollowChecked = true;
					i = oBtnsfollow.length;
				}
			}
			if (!isfollowChecked) {
				alert('Please select a Follow Workflow type');
			}
			return isfollowChecked;
		});

		if ($("input[@name='followWF']:checked").val() == 1) {
			//alert('here');
			$('#table').css('display', 'block');
		} else {
			$('#distributionEmail').prop('checked', false);
			$('#assessmentRequired').prop('checked', false);
			$('#assessmentCompleted').prop('checked', false);
			$('#approvalEmail').prop('checked', false);
			$('#certificateEmail').prop('checked', false);
			$('#certificateEmailSummaryAlert').prop('checked', false);
		}

		$('#clear').click(function() {
			$('#dialog').dialog('close');
			return false;
		});

		//For International Mode Hide and Show Starts Here
		if ($("input:radio[name=internationalMode]:checked").val() == 0) {
			$('#defaultCountryDiv').css('display', 'block');
		} else {
			$('#defaultCountryDiv').css('display', 'none');
		}

		$('input[name=internationalMode]:radio').click(function() {
			if ($(this).val() == 0) {
				$('#defaultCountryDiv').css('display', 'block');
			} else {
				$('#defaultCountryDiv').css('display', 'none');
			}
		});
		//For International Mode Hide and Show Ends Here

		//For BP Segment Hide and Show Starts Here
		if ($("input:radio[name=bpSegment]:checked").val() == 1) {
			$('#statusRowDiv').css('display', 'block');
		} else {
			$('#statusRowDiv').css('display', 'none');
		}

		$('input[name=bpSegment]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#statusRowDiv').css('display', 'block');
			} else {
				$('#statusRowDiv').css('display', 'none');
			}
		});
		//For BP Segment Hide and Show Ends Here
	});

	function showhideoptions(value) {
		if (value == 1) {
			$('#table').css('display', 'block');
		} else {
			$('#table').css('display', 'none');
		}
	}

// 	function validateForm() {
// 		var fields = $("input[name='checkbox']").serializeArray();
// 		var fields2 = $("input[name='checkbox2']").serializeArray();
// 		var listname = $('#emailDistributionListName').val();
// 		if (!listname && listname.length <= 0) {
// 			alert('Please enter email distribution name');
// 			// cancel submit
// 			return false;
// 		}
// 		if (fields.length == 0 && fields2.length == 0) {
// 			alert('Please select atleast one user');
// 			// cancel submit
// 			return false;
// 		}
// 	}

// 	function validation() {
// 		var defaultCountry = $('#defaultCountry').val();
// 		var internationalMode = $("input:radio[name=internationalMode]:checked")
// 				.val();
// 		if (defaultCountry == 0 && internationalMode == 0) {
// 			alert("Please select default country");
// 			return false;
// 		}

// 		var status = $('#status').val();
// 		var bpSegment = $("input:radio[name=bpSegment]:checked").val();
// 		if (status == 0 && bpSegment == 1) {
// 			alert("Please select status");
// 			return false;
// 		}

// 		$("#workflowForm").submit();
// 	}


	$(document).ready(function(){
		$('#macthfound').hide();
		$('#macthnotfound').hide();
		//$('#commodityCode option:selected').remove();
		//$("#commodityCode").val(-1);
		//$("#incumbentVendorId").val(-1);
		
		
		// commodity category & Sector start
		$("#commoditySubmitId").click(function(){
			$('#macthfound').hide();
			$('#macthnotfound').hide();
			    var commoditySearchValue  = $("#commoditySearchValue").val();
			   // alert(commoditySearchValue);
				$.ajax({
				url : "<%=request.getContextPath()%>/supplierDiversity.do?method=commoditySearchList&commoditySearchValue="+ commoditySearchValue,
				type : "POST",
				data : $("#reportform").serialize(),
				async : true,
				beforeSend : function(){
					$("#ajaxloader").show();  
				},
				success : function(res) {
					$("#commodityCode").empty();
					$("#ajaxloader").hide();
			
				var data = JSON.parse(res);
				
					 $.each(data, function(i, obj)
						  {

						 $('#commodityCode').append($('<option>', {   value: obj.commodityId, text : obj.commodityDesc   }));

						});
					 if (data.length >= 1)	{
						 $('#macthfound').show();
						 }	
					 else{
						 $('#macthnotfound').show();
						 }	
					 $("#commodityCode").trigger("click");
				
				},
				 error: function (xhr, ajaxOptions, thrownError) {
		        alert(xhr.status);
		        alert(thrownError);
				 }
				});
			  });

		// commodity category & Sector end

	    // Incumbent diverse supplier names start
	<%-- 	 $("#commodityCode").on('change',function(){

			    var commodityCode  = $("#commodityCode").val();

				$.ajax({
					url : "<%=request.getContextPath()%>/supplierDiversity.do?method=commoditySearchList&commodityCode="+ commodityCode,
					type : "POST",
					data : $("#reportform").serialize(),
					async : true,
					beforeSend : function(){
						$("#ajaxloader").show();  
					},
					success : function(res) {
						$("#incumbentVendorId").empty();
						$("#ajaxloader").hide();
				
					var data = JSON.parse(res);
					
						 $.each(data, function(i, obj)
							  {

							 $('#incumbentVendorId').append($('<option>', {   value: obj.vendorId, text : obj.VendorName   }));

							});
					
					},
					 error: function (xhr, ajaxOptions, thrownError) {
			        alert(xhr.status);
			        alert(thrownError);
			     	 } 
			     	 
					});

			      }); --%>

		 //Incumbent diverse supplier end
	

	});

	$(document).ready(function() {
	$("#suplierDiversityForm")
	.validate(
			{
				rules : {
					business : {
						required : true,
						maxlength : 255
					},
					requestedOn : {
						required : true
						
					},
					oppurtunityEventName : {
						required : true,
						maxlength : 255
					},
					sourceEvent : {
						required : true
						
					},
					sourceEventStartDate : {
						required : true
					},
					estimateSpend : {
						required : true,
						 number: true
					},
					commodityCode : {
						required : true
					},
					geography : {
						required : true
					},
					miniSupplierRequirement : {
						required : true
					}
				},
			});
	});
	
</script>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<div id="successMsg">
				<html:messages id="msg" property="successMsg" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>
			<div id="failureMsg">
					<html:messages id="msg" property="errorMsg" message="true">
						<div class="alert alert-danger nomargin"><bean:write name="msg" /> </div>
					</html:messages>
				</div>

			
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Supplier Diversity Request Form" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<logic:match value="Supplier Diversity Request Form" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="add">
				<header class="card-header">
					<h2 class="card-title pull-left">
					Supplier Diversity List</h2>	
					<div class="pull-right">	
						<input type="button" value="Supplier Diversity Form" class="btn btn-primary"
						 onclick="createSupplierDiversity();">
						</div>
				 </header>
				</logic:match>
			</logic:match>
			<div class="form-box card-body">			
				<table id="searchvendor" border="0"
					class="sortable main-table table table-bordered table-striped mb-0">
					<thead>
						<tr>
							<th class="sorttable_nosort" style="padding:0 20px !important;">Print</th>
							<th class="">Requestor Name</th>
							<th class="">Business/Site/Location</th>
							<th class="">Date of Request</th>
							<th class="">Opportunity/Event Name</th>
							<th class="">Sourcing Event</th>
							<th class="">Sourcing Event Start Date</th>
							<th class="">Estimated Spend ($)</th>
							<th class="">Estimated Work Start Date</th>
							<th class="sorttable_nosort ">Action</th>
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="supplierDiversityDtoList">
							<logic:iterate name="supplierDiversityDtoList"
								id="supplierDiversityList">
								<tr align="center">
									<bean:define id="supplierDiversityId"
										name="supplierDiversityList" property="id" />
									<td><a href="#" alt="Export" class=""
										title="Print Supplier Diversity Profile"
										onclick="printPDFXLS('${supplierDiversityId}')"><span>Print</span></a></td>
									<td><bean:write name="supplierDiversityList"
											property="firstName" /></td>
									<td><bean:write name="supplierDiversityList"
											property="business" /></td>
									<td class="sorttable_customkey='MMDDYYYY'"><bean:write
											name="supplierDiversityList" property="requestedOn" /></td>
									<td><bean:write name="supplierDiversityList"
											property="oppurtunityEventName" /></td>
									<td><bean:write name="supplierDiversityList"
											property="sourceEvent" /></td>
									<td class="sorttable_customkey='MMDDYYYY'"><bean:write
											name="supplierDiversityList" property="sourceEventStartDate" /></td>
									<td><bean:write name="supplierDiversityList"
											property="estimateSpend" /></td>
									<td class="sorttable_customkey='MMDDYYYY'"><bean:write
											name="supplierDiversityList" property="estimateWorkStartDate" /></td>
									<td><logic:match value="Supplier Diversity Request Form"
											name="privilege" property="objectId.objectName">
											<logic:match value="1" name="privilege" property="modify">

												<html:link
													action="/supplierDiversity.do?method=updateSupplierDivercity"
													paramId="id" paramName="supplierDiversityId"
													styleId="editId">Edit</html:link>
													<logic:match value="Supplier Diversity Request Form"
											name="privilege" property="objectId.objectName">
											<logic:match value="1" name="privilege" property="delete">
														/
														</logic:match>
														</logic:match>
											</logic:match>
											</logic:match> 
											<logic:match value="Supplier Diversity Request Form"
											name="privilege" property="objectId.objectName">
											<logic:match value="1" name="privilege" property="delete">
												<html:link
													action="/supplierDiversity.do?method=deleteSupplierDivercityForm"
													paramId="id" paramName="supplierDiversityId" onclick="return confirm_delete();">Delete</html:link>
											</logic:match>
											</logic:match>
										</td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
						<logic:empty name="supplierDiversityList">
							<tr>
								<td colspan='13'>No records are found...</td>
							</tr>
						</logic:empty>
					</tbody>
				</table>
			</div>

			<div id="exportPDFXLS" style="display: none;"
				title="Choose Export Type">
				<input type="hidden" id="supplierDiversityId" />
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;"><input type="radio" name="export"
							value="1" id="excel"></td>
						<td><label for="excel"><img id="excelExport"
								src="images/excel_export.png" />Excel</label></td>
						<td style="padding: 1%;"><input type="radio" name="export"
							value="3" id="pdf"></td>
						<td><label for="pdf"><img id="pdfExport"
								src="images/pdf_export.png" />PDF</label></td>
					</tr>
				</table>

				<footer class="mt-2 card-footer">
						<div class="row justify-content-end text-sm-right">
							<div class="wrapper-btn text-center">
								<logic:present name="userDetails">
									<bean:define id="logoPath" name="userDetails"
										property="settings.logoPath"></bean:define>
									<input type="button" value="Export" class="exportBtn btn btn-primary"
										onclick="exportSupplierDiversityReport();">
								</logic:present>
							</div>
						</div>
					</footer>
			</div>

			<div id="dialog" style="display: none;" class="form-box mt-5" >
				<html:form
					action="/supplierDiversity?method=createSupplierDiversity"
					styleId="suplierDiversityForm" styleClass="form-control" method="post"
					enctype="multipart/form-data">
					<%-- 					<html:hidden property="workflowConfigId"></html:hidden> --%>
					<!-- Change it to radio button if required. -->
					<%-- 					<html:hidden property="naicsCodeAM"></html:hidden> --%>
					<%-- 					<html:hidden property="naicsCodeAM"></html:hidden> --%>
					<%-- 					<html:javascript formName="workflowConfigurationForm" /> --%>
					<div class="panelCenter_1" style="margin-top: 1%;">
						<h4>Supplier Diversity Request</h4>
						<div class="form-box pt-3"
							style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
							<div style="padding-left: 1%;">
								
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:hidden property="id" name="supplierDiversityForm"
												alt="Optional" styleId="id" styleClass="text-box" />
											<html:text property="firstName" name="supplierDiversityForm" 
												styleId="firstName" styleClass="text-box form-control" readonly="true"/>
										</div>
									</div>
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="lastName" name="supplierDiversityForm"
												 styleId="lastName" styleClass="text-box form-control" readonly="true"/>
										</div>
									</div>						

								
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Business/Site/Location</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="business" name="supplierDiversityForm"
												alt="Enter Value" styleId="business" styleClass="text-box form-control" />
										</div>
										<span class="error"><html:errors property="business"></html:errors></span>
									</div>
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Date of Request</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="requestedOn"
												alt="Please click to select date" styleId="requestedOn"
												styleClass="text-box form-control" readonly="true"/>
										</div>
										<span class="error"><html:errors property="requestedOn"></html:errors></span>
									</div>
								

									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Opportunity/Event Name</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="oppurtunityEventName"
												name="supplierDiversityForm" alt="Required"
												styleId="oppurtunityEventName" styleClass="text-box form-control" />
										</div>
										<span class="error"><html:errors property="oppurtunityEventName"></html:errors></span>
									</div>
									
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Sourcing Event</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:select property="sourceEvent" styleId="sourceEvent"
												styleClass="chosen-select-deselect form-control">
												<html:option value="">-----Select Event-----</html:option>
												<html:option value="RFI">RFI</html:option>
												<html:option value="RFP">RFP</html:option>
												<html:option value="Other">Other</html:option>
												<%-- <logic:present name="marketSectorsList">
													<bean:size id="size" name="marketSectorsList" />
													<logic:greaterEqual value="0" name="size">
														<logic:iterate id="marketSector" name="marketSectorsList">
															<bean:define id="sectorId" name="marketSector"
																property="id" />
															<option value="<%=sectorId.toString()%>"><bean:write
																	name="marketSector" property="sectorDescription" /></option>
														</logic:iterate>
													</logic:greaterEqual>
												</logic:present> --%>
											</html:select>
										</div>
										
									</div>
									<span class="error"><html:errors property="sourceEvent"></html:errors></span>
								

									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Sourcing Event Start Date</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="sourceEventStartDate"
												alt="Please click to select date"
												styleId="sourceEventStartDate" styleClass="text-box form-control"
												readonly="true" />
										</div>
										<span class="error"><html:errors property="sourceEventStartDate"></html:errors></span>
									</div>
									
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Estimated Spend ($)</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="estimateSpend"
												name="supplierDiversityForm" alt="enter-estimateSpend"
												styleId="estimateSpend" styleClass="text-box form-control" />
										</div>
									</div>
									<span class="error"><html:errors property="estimateSpend"></html:errors></span>
								
								
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Estimated Work Start Date</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="estimateWorkStartDate"
												alt="Please click to select date"
												styleId="estimateWorkStartDate" styleClass="text-box form-control"
												readonly="true" />
										</div>
									</div>
									<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Response Date</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="lastResponseDate"
												alt="Please click to select date" styleId="lastResponseDate"
												styleClass="text-box form-control" readonly="true" />
										</div>
									</div>
								
							</div>
						</div>
					</div>
						<div class="clear"></div>
					
						<div class="panelCenter_1 pt-5" style="margin-top: 1%; clear:both;">
							<h4>Category/Market Sector(s) and Sub-Sector(s)</h4>
							<div class="form-box card-body"
								style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
								<div class="coulmn-1-row">
									<div class="row-wrapper" style="margin-left: 1%;">
										<!-- <div class="label-col-wrapper">Commodity</div> -->
										<div class="ctrl-col-wrapper">
											<div class="pt-2">
												<input type="text" name="commoditySearchValue"
													id="commoditySearchValue"
													class="col-sm-4 form-control mr-1">
													
												<input type="button" value="Search" id="commoditySubmitId"
													class="btn btn-primary col-sm-2 mr-1" >

												<select name="commodityCode" id="commodityCode"
													class="chosen-select-deselect form-control col-sm-4"
													multiple="multiple">

													<c:choose>
														<c:when test="${form == 'true'}">

															<logic:notEmpty name="supplierDiversityForm"
																property="commodityDataList">

																<logic:iterate id="commodityDataList"
																	name="supplierDiversityForm"
																	property="commodityDataList">
																	<option
																		value="<bean:write name="commodityDataList" property="key"/>"
																		selected="selected">
																		<bean:write name="commodityDataList" property="value" /></option>
																</logic:iterate>
															</logic:notEmpty>

														</c:when>
													</c:choose>



												</select>
												<div id="macthfound">
												<h4 style="color: green; margin: 9px 0 10px 67px;"><b>Click on above box to see matches </b></h4>
												</div>
												<div id="macthnotfound">
												<h4 style="color: red; margin: 9px 0 10px 67px;"><b>No matches found </b></h4>
												</div>
												<%-- 		<html:select property="commodityCode" styleId="commodityCode"
												styleClass="chosen-select-deselect" style="width: 300px;"
												multiple="multiple">
												<html:option value="0">Bhaskar</html:option>
												<html:option value="1">Bhaskar</html:option>
												<html:option value="2">Bhaskar</html:option> --%>
												<%-- <logic:present name="marketSectorsList">
													<bean:size id="size" name="marketSectorsList" />
													<logic:greaterEqual value="0" name="size">
														<logic:iterate id="marketSector" name="marketSectorsList">
															<bean:define id="sectorId" name="marketSector"
																property="id" />
															<option value="<%=sectorId.toString()%>"><bean:write
																	name="marketSector" property="sectorDescription" /></option>
														</logic:iterate>
													</logic:greaterEqual>
												</logic:present> 
											</html:select> --%>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="panelCenter_1 pt-5" style="margin-top: 1%;clear:both;">
							<h4>Opportunity/Project/Event Scope of Work</h4>
							<div class="form-box pt-2"
								style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
								<div class="coulmn-1-row">
									<div class="row-wrapper" style="margin-left: 1%;">
										<!-- <div class="label-col-wrapper">Scop Of Work</div> -->
										<div class="ctrl-col-full-wrapper">
											<html:textarea property="scopOfWork" styleId="scopOfWork"
												styleClass="main-text-area form-control" rows="6"
												></html:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="panelCenter_1 pt-5" style="margin-top: 1%;clear:both;">
							<h4>Geography</h4>
							<div class="form-box"
								style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
								<div class="coulmn-1-row">
									<div class="row-wrapper" style="margin-left: 1%;">
										<!-- <div class="label-col-wrapper">Scop Of Work</div> -->
										<div class="ctrl-col-full-wrapper">
											<html:textarea property="geography" styleId="geography"
												styleClass="main-text-area form-control" rows="6"
												></html:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="panelCenter_1 pt-5" style="margin-top: 1%;clear:both;">
							<h4>Minimum Supplier Requirements (including any necessary
								certifications)</h4>
							<div class="form-box"
								style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
								<div class="coulmn-1-row">
									<div class="row-wrapper" style="margin-left: 1%;">
										<!-- <div class="label-col-wrapper">Minimum Supplier
											Requirement</div> -->
										<div class="ctrl-col-full-wrapper">
											<html:textarea property="miniSupplierRequirement"
												styleId="miniSupplierRequirement"
												styleClass="main-text-area form-control" rows="6"
												></html:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="panelCenter_1 pt-5" style="margin-top: 1%;clear:both;">
							<h4>Contract Insight (Expiration? New work?)</h4>
							<div class="form-box"
								style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
								<div class="coulmn-1-row">
									<div class="row-wrapper" style="margin-left: 1%;">
										<!-- <div class="label-col-wrapper">Contract Insight</div> -->
										<div class="ctrl-col-full-wrapper">
											<html:textarea property="contractInsight"
												styleId="contractInsight" styleClass="main-text-area form-control"
												rows="6"></html:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="panelCenter_1 pt-5" style="margin-top: 1%;clear:both;">
							<h4>Incumbent diverse supplier names (if applicable)</h4>
							<div class="form-box"
								style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
								<div class="coulmn-1-row">
									<div class="row-wrapper" style="margin-left: 1%">
									<div class="ctrl-col-full-wrapper">
											<html:textarea property="incumbentVendorId"
												styleId="incumbentVendorId" styleClass="main-text-area"
												rows="7" style="width:159%;height:45%;"></html:textarea>
										</div>
										<!-- <div class="label-col-wrapper">Incumbent Vedorid</div> -->
										<div class="ctrl-col-wrapper">
											<select name="incumbentVendorId" id="incumbentVendorId"
												class="chosen-select-deselect form-control" multiple="multiple"
												>

												<c:choose>
													<c:when test="${form == 'true'}">

														<logic:notEmpty name="supplierDiversityForm"
															property="incumbentDataList">

															<logic:iterate id="incumbentDataList"
																name="supplierDiversityForm"
																property="incumbentDataList">
																<option
																	value="<bean:write name="incumbentDataList" property="key"/>"
																	selected="selected">
																	<bean:write name="incumbentDataList" property="value" /></option>
															</logic:iterate>
														</logic:notEmpty>

													</c:when>
												</c:choose>

											</select>

											 	<html:select property="incumbentVendorId"
												styleId="incumbentVendorId"
												styleClass="chosen-select-deselect" style="width: 300px;"
												multiple="multiple">
												<html:option value="0">Bhaskar</html:option>
												<html:option value="1">Bhaskar</html:option>
												<html:option value="2">Bhaskar</html:option> 
											 <logic:present name="marketSectorsList">
													<bean:size id="size" name="marketSectorsList" />
													<logic:greaterEqual value="0" name="size">
														<logic:iterate id="marketSector" name="marketSectorsList">
															<bean:define id="sectorId" name="marketSector"
																property="id" />
															<option value="<%=sectorId.toString()%>"><bean:write
																	name="marketSector" property="sectorDescription" /></option>
														</logic:iterate>
													</logic:greaterEqual>
												</logic:present> 
											</html:select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="clear"></div>
					<footer class="mt-2 card-footer pt-2" style="clear:both;">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
						<c:choose>
							<c:when test="${form == 'true'}">
								<html:submit value="Update" styleClass="btn btn-primary" styleId="submit"></html:submit>
							</c:when>
							<c:otherwise>
								<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit"></html:submit>
							</c:otherwise>
						</c:choose>
						<html:submit value="Close" styleClass="btn" styleId="close" onclick="myWindow.close();"></html:submit>
						<%-- <html:reset value="Cancel" styleClass="btn" styleId="submit"
							onclick="resetForm();"></html:reset> --%>
					</div>
					</div>
					</footer>
				</html:form>
				</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Supplier Diversity Request Form" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view Supplier Diversity Request Form</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#workflowForm").validate({
			rules : {
				daysUpload : {
					number : true,
					required : true,
					range : [ 1, 30 ]
				},
				reportDueDays : {
					number : true,
					required : true,
					range : [ 1, 30 ]
				},
				daysExpiry : {
					number : true,
					required : true,
					range : [ 1, 30 ]
				},
				/* daysInactive : {
					number : true
				}, */
				documentUpload : {
					number : true,
					required : true
				},
				documentSize : {
					number : true,
					required : true
				},
				reportNotSubmittedReminderDate : {
					number : true,
					required : true,
					range : [ 1, 30 ]
				}
			}
		/*,
					submitHandler : function(form) {
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}*/
		});
	});
//-->

</script>
