<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%@page import="com.fg.vms.customer.model.NaicsMaster"%>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<style>
.main-text-box {
	width: 86%;
	padding: 0%;
}
.ui-state-default .ui-icon{
	
}
</style>
<script>
	function pickNaics(value) {
		press = value;
		pickNaicsCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.8;
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});

			$("#dialog").dialog({
				autoOpen : false,
				width : dWidth,
				height : dHeight,
				modal : true,
				close : function(event, ui) {
					//close event goes here
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});

		$("#dialog").dialog("open");
	}

	var selectedIDs = [];

	function addCommodity() {
		selectedIDs = pickCommodityCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.8;
		$("#dialog2").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});

		$("#dialog2").dialog({
			width : dWidth,
			height : dHeight,
			modal : true,
			close : function(event, ui) {
				//close event goes here
			},
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}

	function add() {
		if (press == 0) {
			$('#naicsCode_0').val(naicsCode);
			$('#naicsDesc_0').val(des);
			$('#naicsDesc0_0').focus();
		} else if (press == 1) {
			$('#naicsCode_1').val(naicsCode);
			$('#naicsDesc_1').val(des);
			$('#naicsDesc1_1').focus();
		} else if (press == 2) {
			$('#naicsCode_2').val(naicsCode);
			$('#naicsDesc_2').val(des);
			$('#naicsDesc2_2').focus();
		}
		$('#search').val("");
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#dialog").dialog("close");
	}

	function reset() {
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#dialog").dialog("close");
	}

	function cancel() {
		$("#dialog1").dialog("close");
	}

	function deleteRow(ele) {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$(ele).closest("tr").remove();
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}
	function clearNaics(value){
		if(value == '1'){
			$("#naicsCode_0").val('');
			$("#naicsDesc_0").val('');
			$("#naicsDesc0_0").val('');
			$("#primary_0").prop('checked', false);
		} else if(value == '2'){
			$("#naicsCode_1").val('');
			$("#naicsDesc_1").val('');
			$("#naicsDesc1_1").val('');
			$("#primary_1").prop('checked', false);
		} else {
			$("#naicsCode_2").val('');
			$("#naicsDesc_2").val('');
			$("#naicsDesc2_2").val('');
			$("#primary_2").prop('checked', false);
		}
	}
	function cancelCommodity() {
		$("#dialog2").dialog("close");
	}

	function createCommodity() {
		var grid = jQuery("#list2");
		var table = document.getElementById("commodityTable");
		var rowCount = table.rows.length;
		if (selectedIDs.length > 0) {
			for ( var i = 0, il = selectedIDs.length; i < il; i++) {
				var row = grid.getLocalRow(selectedIDs[i]);
				var commodityCode = row.commodityCode
				var commodityDesc = row.commodityDesc;
				var row = table.insertRow(rowCount);
				row.id = "row"+i;
				var newcell0 = row.insertCell(0);
				newcell0.innerHTML = "<td><input type='text' name='commodityCode' id='geographicState"+ i +"' class='main-text-box' readonly='readonly' value='" + commodityCode + "'></td>";
				var newcell1 = row.insertCell(1);
				newcell1.innerHTML = "<td><input type='text' name='commodityDesc' id='commodityDesc"+ i +"' class='main-text-box' readonly='readonly' value='" + commodityDesc + "'></td>";
				var newcell2 = row.insertCell(2);
				newcell2.innerHTML = "<td><a style='cursor: pointer;' onclick='deleteRow(this);' >Delete</a></td>";
			}
			$("#dialog2").dialog("close");
		} else {
			alert("Choose any commodity");
		}
	}

	$(document).ready(function() {

		$("#search").keyup(function(e) {
			 if(e.which == 13) {
				 var text = $("#search").val();
					var postdata = $("#addtree").jqGrid('getGridParam', 'postData');
					var isNumber = $.isNumeric(text);
					if(isNumber){
						$.extend(postdata, {
							filters : '',
							searchField : 'code',
							searchOper : 'eq',
							searchString : text
						});
					} else {
						$.extend(postdata, {
							filters : '',
							searchField : 'name',
							searchOper : 'bw',
							searchString : text
						});
					}
					$("#addtree").jqGrid('setGridParam', {
						search : text.length > 0,
						postData : postdata
					});
					$("#addtree").trigger("reloadGrid", [ {
						page : 1
					} ]);
		    }
		});
		$("#geographicalServiceArea").multiselect({
			selectedText : "# of # selected"
		});

	});// document.ready
</script>

<html:hidden property="naicslastrowcount" value="3"
	styleId="naicslastrowcount"></html:hidden>
<html:hidden property="lastrowcount" value="2" styleId="lastrowcount" />

<table width="100%" border="0" class="main-table">
	<tr>
		<td class="header">NAICS Code</td>
		<td class="header">NAICS Desc</td>
		<td class="header">Capabilities</td>
		<td class="header">Primary</td>
	</tr>
	<tr class="even">
		<td><html:text property="naicsCode_0" styleId="naicsCode_0"
				tabindex="76" size="10" title="Click the search image to select NAICS code" 
				alt="" readonly="true" styleClass="main-text-box" />
				 <img src="images/magnifier.gif"
			onClick="pickNaics(0);"> <span class="error"><html:errors
					property="naicsCode_0"></html:errors></span></td>
		<td><html:textarea property="naicsDesc1" styleId="naicsDesc_0"
				readonly="true" styleClass="main-text-area" tabindex="77" /></td>
		<td><html:textarea property="capabilitie1" styleId="naicsDesc0_0"
				styleClass="main-text-area" tabindex="78" /></td>
		<td><html:radio property="primary" styleId="primary_0" value="1"
				tabindex="79"></html:radio></td>
		<td><a onclick="clearNaics('1');" href="#">Clear</a></td>
	</tr>
	<tr class="even">
		<td><html:text property="naicsCode_1" styleId="naicsCode_1"
				tabindex="80" size="10" alt="Optional" readonly="true"
				styleClass="main-text-box" /><img src="images/magnifier.gif"
			onClick="pickNaics(1);"> <span class="error"><html:errors
					property="naicsCode_1"></html:errors></span></td>
		<td><html:textarea property="naicsDesc2" styleId="naicsDesc_1"
				readonly="true" styleClass="main-text-area" tabindex="81" /></td>
		<td><html:textarea property="capabilitie2" styleId="naicsDesc1_1"
				styleClass="main-text-area" tabindex="82" /></td>
		<td><html:radio property="primary" styleId="primary_1" value="2"
				tabindex="83"></html:radio></td>
		<td><a onclick="clearNaics('2');" href="#">Clear</a></td>
	</tr>
	<tr class="even">
		<td><html:text property="naicsCode_2" styleId="naicsCode_2"
				tabindex="84" size="10" alt="Optional" readonly="true"
				styleClass="main-text-box" /><img src="images/magnifier.gif"
			onClick="pickNaics(2);"> <span class="error"><html:errors
					property="naicsCode_2"></html:errors></span></td>
		<td><html:textarea property="naicsDesc3" styleId="naicsDesc_2"
				readonly="true" styleClass="main-text-area" tabindex="85" /></td>
		<td><html:textarea property="capabilitie3" styleId="naicsDesc2_2"
				styleClass="main-text-area" tabindex="86" /></td>
		<td><html:radio property="primary" styleId="primary_2" value="3"
				tabindex="87"></html:radio></td>
		<td><a onclick="clearNaics('3');" href="#">Clear</a></td>
	</tr>
</table>
<div class="clear"></div>
<div id="dialog" title="NAICS Code" style="display: none;">
	<div class="page-title">
		<input type="search" placeholder="Search" alt="Search"
			style="float: left; width: 100%;" id="search" class="main-text-box">
	</div>
	<div class="clear"></div>
	<div id="gridContainer" style="height: 350px;overflow: auto;">
		<table id="addtree" width="100%"></table>
		<div id="paddtree"></div>
	</div>
	<div class="btn-wrapper">
		<input type="button" value="Ok" class="btn" onclick="add();">
		<input type="button" value="Cancel" class="btn" onclick="reset();">
	</div>
</div>
<div class="clear"></div>
<input type="button" value="Add Commodity" class="btn" tabindex="88"
	style="float: right;margin-top: 5px;" onclick="addCommodity();">
<table id='commodityTable' class='main-table' width='100%' border='0' style='border:none;border-collapse:collapse;float: none;'>
	<thead>
		<td class='header'>Commodity Code</td>
		<td class='header'>Commodity</td>
		<td class='header'>Action</td>
	</thead>
	<tbody>
	
	</tbody>
</table>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 4%;">
	<h3>Services Description</h3>
<div class="form-box">
	<div class="wrapper-half">
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Describe Services</div>
			<div class="ctrl-col-wrapper">
				<html:textarea styleClass="main-text-area"
					property="vendorDescription" alt="" styleId="vendorDescription"
					tabindex="89" />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Company Information</div>
			<div class="ctrl-col-wrapper">
				<html:textarea styleClass="main-text-area"
					property="companyInformation" alt="" styleId="companyInformation"
					tabindex="90" />
			</div>
		</div>
	</div>
</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 4%;">
	<h3>Business Services</h3>
<div class="form-box">
	<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Service Area</div>
				<div class="ctrl-col-wrapper">
					<html:select property="serviceArea" styleId="serviceArea"
						styleClass="chosen-select-width" tabindex="91">
						<html:option value="">--Select--</html:option>
						<html:optionsCollection name="serviceAreaList" label="serviceArea" value="id"/>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Business Area</div>
				<div class="ctrl-col-wrapper">
					<html:select property="businessArea" styleId="businessArea"
						styleClass="chosen-select-deselect" multiple="multiple" style="width: 90%;" tabindex="92">
						<bean:size id="areasize" name="serviceAreas" />
						<logic:greaterEqual value="0" name="areasize">
							<logic:iterate id="area" name="serviceAreas">
								<bean:define id="bgroup" name="area" property="businessGroup"></bean:define>
								<bean:define id="barea" name="area" property="serviceArea"></bean:define>
								<optgroup label="<%=bgroup.toString()%>">
									<%
										String[] areas = barea.toString().split("\\|");

													if (areas != null && areas.length != 0) {
														for (int i = 0; i < areas.length; i++) {
															String[] parts = areas[i].split("-");
															String areaId = parts[0];
															String areaName = parts[1];
									%>
									<option value="<%=areaId%>"><%=areaName%></option>
									<%
										}
									}
									%>
								</optgroup>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
		</div>
</div>
</div>
<div class="clear"></div>
<div id="dialog2" title="Choose Commodity" style="display: none;">
	<table id="list2" width="100%"></table>
	<div id="pager2"></div>
	<div class="btn-wrapper">
		<input type="button" value="Ok" class="btn"
			onclick="createCommodity();"> <input type="button"
			value="Cancel" class="btn" onclick="cancelCommodity();">
	</div>
</div>
<div class="clear"></div>