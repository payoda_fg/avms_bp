<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page
	import="com.fg.vms.customer.dto.RetriveVendorInfoDto,com.fg.vms.customer.model.VendorMaster"%>
<%
	RetriveVendorInfoDto vendorInfo1 = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");

	session.setAttribute("vendorMaster1", vendorInfo1.getVendorMaster());
%>
<!-- For JQuery Panel -->
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<script>
<!--
	$(document).ready(function() {
		
		$("#contactPhone").mask("(999) 999-9999?");
		$("#contactFax").mask("(999) 999-9999?");
		
		
		$('.panelCenter_1').panel({
			collapsible : false
		});
		
		if ($("#isPreparer").is(':checked')) {
			document.getElementById("preparer").style.display = "block";
		}else{
			document.getElementById("preparer").style.display = "none";
		}
	});
	//-->
	function allowed_to_login() {
		if (document.getElementById("loginAllowed").checked == true) {
			$('#loginAllowed').val("on");
		} else {
			document.getElementById("loginAllowed").value = "";
		}
	}
</script>

<div class="clear"></div>
<div class="page-title" onload="allowed_to_login();">
	<img src="images/edit_user.png" alt="" />Edit Contact
</div>
<div class="clear"></div>
<html:form
	action="/updateSubVendorContact?method=updateSubVendorContact"
	styleClass="AVMS" styleId="vendorContactForm">
	<html:javascript formName="editVendorContactForm" />
	<div class="panelCenter_1">
		<h3>Update Contact Information</h3>
		<div class="form-box">
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">First Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="firstName" alt=""
							styleId="firstName" />
					</div>
					<span class="error"><html:errors property="firstName"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Last Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="lastName" alt=""
							styleId="lastName" />
					</div>
					<span class="error"><html:errors property="lastName"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Title</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="designation"
							alt="" styleId="designation" />
					</div>
					<span class="error"><html:errors property="designation"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Phone</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contactPhone"
							alt="" styleId="contactPhone" />
					</div>
					<span class="error"><html:errors property="contactPhone"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Mobile</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contactMobile"
							alt="Optional" styleId="contactMobile" />
					</div>
					<span class="error"><html:errors property="contactMobile"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">FAX</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contactFax"
							alt="" styleId="contactFax" />
					</div>
					<span class="error"><html:errors property="contactFax"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Email ID</div>
					<div class="ctrl-col-wrapper">
						<html:hidden property="hiddenEmailId" alt=""
							styleId="hiddenEmailId" />
						<html:text property="contanctEmail" alt="" styleId="emailAddress"
							onchange="ajaxEdFn(this,'hiddenEmailId','VE');"
							styleClass="main-text-box" />

					</div>
					<span class="error"><html:errors property="contanctEmail"></html:errors></span>
				</div>
					<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Password</div>
					<div class="ctrl-col-wrapper">
						<html:password property="loginpassword" styleClass="main-text-box"
							styleId="loginpassword" alt=""  />
					</div>
					<span class="error"><html:errors property="loginpassword"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Confirm Password</div>
					<div class="ctrl-col-wrapper">
						<html:password property="confirmPassword" 
							styleClass="main-text-box" styleId="confirmPassword" alt="" />
					</div>
					<span class="error"><html:errors property="confirmPassword"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Secret Question</div>
					<div class="ctrl-col-wrapper">
						<html:select property="userSecQn" styleId="userSecQn"
							styleClass="main-list-box">
							<html:option value="">--Select--</html:option>
							<bean:size id="size" name="secretQnsList" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="secretQn" name="secretQnsList">
									<bean:define id="id" name="secretQn" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</html:select>
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Secret Question Answer</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="userSecQnAns"
							alt="" styleId="userSecQnAns" />
					</div>
					<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Primary Contact</div>
					<div class="ctrl-col-wrapper">
						<html:checkbox property="primaryContact"
							name="editVendorContactForm" styleId="primaryContact" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Allowed to Login</div>
					<div class="ctrl-col-wrapper">
						<html:checkbox property="loginAllowed" styleId="loginAllowed"
							onclick="allowed_to_login()" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Business Contact</div>
					<div class="ctrl-col-wrapper">
						<html:checkbox property="isBusinessContact" styleId="isBusinessContact" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper" id="preparer">
					<div class="label-col-wrapper">Preparer</div>
					<div class="ctrl-col-wrapper">
						<html:checkbox property="isPreparer" styleId="isPreparer" disabled="true" />
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="btn-wrapper">
		<html:submit value="Update" styleClass="btn" styleId="submit"></html:submit>
		<html:reset value="Cancel" styleClass="btn" onclick="history.go(-1)"></html:reset>
	</div>
</html:form>
<script>
$(function($) {
	  $("#contactMobile").mask("(999) 999-9999?");
});
	allowed_to_login();
	$(function() {
		
			jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
		          return this.optional(element) || /^[0-9]+$/.test(value);
		    },"Please enter only numbers.");
			
			jQuery.validator.addMethod("phoneNumber", function(value, element) { 
		        return this.optional(element) || /^[0-9-]+$/.test(value);
		  	},"Please enter valid phone number.");
			 
 			jQuery.validator.addMethod("password",function(value, element) {
				return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
			},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
		
				$("#vendorContactForm").validate({
					rules : {
						firstName : {
							required : true
						},
						lastName : {
							required : true
						},
						designation : {
							required : true
						},
						contactPhone : {
							required : true
						},
						contanctEmail : {
							required : true,
							email : true
						},
						loginpassword : {
							required : true
						},
						confirmPassword : {
							required : true,
							equalTo : "#loginpassword"
						},
						userSecQnAns : {
							required : true
						}
					},
					ignore:"ui-tabs-hide",
				    submitHandler: function(form) {
						  $("#ajaxloader").css('display','block');
						  $("#ajaxloader").show();
						  form.submit();
				    }
			});
	});
</script>

