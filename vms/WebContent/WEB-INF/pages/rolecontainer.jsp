<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!--  theme css starts -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />
<!--  theme css ends-->
<!-- Added for jquery validation -->
<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script type="text/javascript"  src="js/bootstrap.js"></script>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
		}, "No Special Characters Allowed.");

		$("#formId").validate({
			rules : {
				roleName : {
					required : true,
					minlength : 2,
					maxlength : 255,
					alpha : true
				},
				roleDesc : {
					required : true,
					maxlength : 255
				}
			}
		});
	});

	function backToUserrole() {
		window.location = "viewuserrole.do?parameter=viewUserRoles";
	}
</script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>


<section role="main" class="content-body card-margin">
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Roles" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">

				<div class="row">
					<div class="col-lg-6 mx-auto">
						<div class="form-box">
							<section class="card">
							<div id="successMsg">
								<span style="color: red;"><html:errors property="referencerole" />
								</span>
								<html:messages id="msg" property="userrole" message="true">
								<div class="alert alert-info nomargin">
									<bean:write name="msg" />
								</div>
								</html:messages>
							</div>
								<header class="card-header">
									<h2 class="card-title">Add User Roles</h2>
								</header>

								<div class="form-box card-body">
									<html:form action="/userrole.do?parameter=addUserRole"
										styleId="formId">
										<html:javascript formName="userRoleForm" />
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Role
												Name</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="roleName" alt="" styleId="roleName"
													onchange="ajaxFn(this,'R');"
													styleClass="text-box form-control" />
											</div>
											<span class="error"><html:errors property="roleName" /></span>
										</div>
										<div class="form-group row row-wrapper">
											<label
												class="col-sm-3 control-label text-sm-right label-col-wrapper">Role
												Description</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:textarea property="roleDesc" alt="" styleId="roleDesc"
													styleClass="main-text-area form-control" rows="10"
													cols="30" />
											</div>
										</div>
										<footer class="card-footer mt-4">
											<div class="row justify-content-end">
												<div class="col-sm-9 wrapper-btn">
													<html:submit value="Submit" styleClass="btn btn-primary"
														styleId="submit1"></html:submit>
													<html:reset value="Cancel" styleClass="btn btn-default"
														onclick="backToUserrole();"></html:reset>
												</div>
											</div>
										</footer>
									</html:form>
								</div>
							</section>
						</div>
					</div>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>

	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Roles" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to add new role</h3>
				</div>
			</logic:match>
		</logic:match>

	</logic:iterate>

	<div class="grid-wrapper">
		<div class="form-box">
			<div class="row">
				<div class="col-lg-6 mx-auto">
					<logic:iterate id="privilege" name="privileges">
						<logic:match value="Roles" name="privilege"
							property="objectId.objectName">
							<logic:match value="1" name="privilege" property="view">
								<header class="card-header">
									<h2 class="card-title">User Roles</h2>
								</header>
								
									<div class="card-body">
										<div id="grid_container">
											<table class="main-table table table-bordered table-striped mb-0"
												id="userroleTable">
												<bean:size id="size" name="userRoles" />
												<logic:greaterThan value="0" name="size">
													<thead>
														<tr>
															<th>Role Name</th>
															<th>Active</th>
															<th>Action</th>
														</tr>
													</thead>
													<logic:iterate name="userRoles" id="userRole">
														<tbody>
															<tr>
																<td><bean:write name="userRole" property="roleName" /></td>
																<td align="center"><html:checkbox
																		property="isActive" name="userRole" disabled="true"
																		value="isActive">
																	</html:checkbox></td>
																<bean:define id="id" name="userRole" property="id"></bean:define>
																<td align="center"><logic:iterate id="privilege1"
																		name="privileges">
																		<logic:match value="Roles" name="privilege1"
																			property="objectId.objectName">
																			<logic:match value="1" name="privilege1"
																				property="modify">
																				<html:link property="text" paramName="id"
																					paramId="id"
																					action="/viewuserrole.do?parameter=retrive">Edit </html:link>
																			</logic:match>
																		</logic:match>

																	</logic:iterate> <logic:iterate id="privilege2" name="privileges">

																		<logic:match value="Roles" name="privilege2"
																			property="objectId.objectName">
																			<logic:match value="1" name="privilege2"
																				property="delete">
			| <html:link action="/deleteuserrole?parameter=delete" paramId="id"
																					paramName="id" onclick="return confirm_delete();">Delete</html:link>
																			</logic:match>
																		</logic:match>
																	</logic:iterate></td>
															</tr>
														</tbody>
													</logic:iterate>
												</logic:greaterThan>
												<logic:equal value="0" name="size">
No such Records
</logic:equal>
											</table>
										</div>
									</div>
							</logic:match>
						</logic:match>

					</logic:iterate>
				</div>
			</div>
		</div>
	</div>
</section>
<logic:iterate id="privilege" name="privileges">

	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view roles</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<div id="sample1" align="center" style="padding: 2% 0 0 30%;"></div>
