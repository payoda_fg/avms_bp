<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script>
	function pickNaics(value) {
		press = value;
		pickNaicsCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.8;
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#dialog").dialog({
				autoOpen : false,
				width : dWidth,
				height : dHeight,
				modal : true,
				close : function(event, ui) {
					//close event goes here
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});
		$("#dialog").dialog("open");
	}
	function add() {
		var code1 = document.getElementById("naicsCode_0").value;
		var code2 = document.getElementById("naicsCode_1").value;
		var code3 = document.getElementById("naicsCode_2").value;
		if(code2=='Optional'){
			code2="";
		}
		if(code3=='Optional'){
			code3="";
		}
		if(naicsCode!=""){
			if (((code3!="" && code2!="" && code3 == code2))
					|| ((code3!="" && code1!="" && code3 == code1))
					||(code2!="" && code1!="" && code2 == code1)
					||(naicsCode!="" && naicsCode==code1)||
					(naicsCode==code2)||(naicsCode==code3)) {
				alert("Selected NAICS already Exists ");
			} else{
				if (press == 0) {
					$('#naicsCode_0').val(naicsCode);
					$('#naicsDesc_0').val(des);
					$('#naicsDesc0_0').focus();
				} else if (press == 1) {
					$('#naicsCode_1').val(naicsCode);
					$('#naicsDesc_1').val(des);
					$('#naicsDesc1_1').focus();
				} else if (press == 2) {
					$('#naicsCode_2').val(naicsCode);
					$('#naicsDesc_2').val(des);
					$('#naicsDesc2_2').focus();
				}
				naicscode = '';
				rowLevel = '';
				des = '';
				press = 0;
				$("#dialog").dialog("close");
			}
		}else{
			alert('Please Select NAICS');
		}
	}
	function reset() {
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#dialog").dialog("close");
	}
	$(document).ready(function() {

		$("#search").keyup(function(e) {
			if(e.which == 13) {
			var text = $("#search").val();
			var postdata = $("#addtree").jqGrid('getGridParam', 'postData');
			var isNumber = $.isNumeric(text);
			if(isNumber){
				$.extend(postdata, {
					filters : '',
					searchField : 'code',
					searchOper : 'eq',
					searchString : text
				});
			} else {
				$.extend(postdata, {
					filters : '',
					searchField : 'name',
					searchOper : 'bw',
					searchString : text
				});
			}
			$("#addtree").jqGrid('setGridParam', {
				search : text.length > 0,
				postData : postdata
			});
			$("#addtree").trigger("reloadGrid", [ {
				page : 1
			} ]);
		}
		});
		
		
	});// document.ready
</script>
<style>
.main-text-box {
	width: 86%;
	padding: 0%;
}
</style>
<html:hidden property="naicslastrowcount" value="3"
	styleId="naicslastrowcount"></html:hidden>
<html:hidden property="lastrowcount" name="editVendorMasterForm"
	styleId="lastrowcount" />
<table width="100%" border="0" class="main-table">
	<tr>
		<td class="header">NAICS Code</td>
		<td class="header">NAICS Desc</td>
		<td class="header">Capabilities</td>
		<td class="header">Primary</td>
		<td class="header">Action</td>
	</tr>
	<tr class="even">
		<td><html:hidden property="naicsID1" styleId="naicsID1"/> <html:text
				property="naicsCode_0" styleId="naicsCode_0" tabindex="86" size="10"
				alt="" title="Click the search image to select NAICS code" readonly="true" 
				styleClass="main-text-box" /><img
			src="images/magnifier.gif" onClick="pickNaics(0);"> <span
			class="error"><html:errors property="naicsCode_0"></html:errors>
		</span></td>
		<td><html:textarea property="naicsDesc1" styleId="naicsDesc_0"
				readonly="true" styleClass="main-text-area" tabindex="87" /></td>
		<td><html:textarea property="capabilitie1" styleId="naicsDesc0_0"
				styleClass="main-text-area" tabindex="88" /></td>
		<td>Primary</td>
		<!-- <td id="naicsDelete1">
			<a onclick="return clearNaics('1');" href="#">Delete</a>
		</td> -->
	</tr>
	<tr class="even">
		<td><html:hidden property="naicsID2" styleId="naicsID2"/> <html:text
				property="naicsCode_1" styleId="naicsCode_1" tabindex="90" size="10"
				alt="Optional" readonly="true" styleClass="main-text-box" /><img
			src="images/magnifier.gif" onClick="pickNaics(1);"> <span
			class="error"><html:errors property="naicsCode_1"></html:errors>
		</span></td>
		<td><html:textarea property="naicsDesc2" styleId="naicsDesc_1"
				readonly="true" styleClass="main-text-area" tabindex="91" /></td>
		<td><html:textarea property="capabilitie2" styleId="naicsDesc1_1"
				styleClass="main-text-area" tabindex="92" /></td>
		<td>Secondary</td>
		<td id="naicsDelete2">
			<a onclick="return clearNaics('2');" href="#">Delete</a>
		</td>
	</tr>
	<tr class="even">
		<td><html:hidden property="naicsID3" styleId="naicsID3"/> <html:text
				property="naicsCode_2" styleId="naicsCode_2" tabindex="94" size="10"
				alt="Optional" readonly="true" styleClass="main-text-box" /><img
			src="images/magnifier.gif" onClick="pickNaics(2);"> <span
			class="error"><html:errors property="naicsCode_2"></html:errors>
		</span></td>
		<td><html:textarea property="naicsDesc3" styleId="naicsDesc_2"
				readonly="true" styleClass="main-text-area" tabindex="95" /></td>
		<td><html:textarea property="capabilitie3" styleId="naicsDesc2_2"
				styleClass="main-text-area" tabindex="96" /></td>
		<td>Secondary</td>
		<td id="naicsDelete3">
			<a onclick="return clearNaics('3');" href="#">Delete</a>
		</td>
	</tr>
</table>
<div class="clear"></div>

<div id="dialog" title="NAICS Code" style="display: none;">
	<div class="page-title">
		<input type="search" placeholder="Search" alt="Search"
			style="float: left; width: 100%;" class="main-text-box" id="search">
	</div>
	<div class="clear"></div>
	<div id="gridContainer" style="height: 350px;overflow: auto;">
		<table id="addtree" width="100%"></table>
		<div id="paddtree"></div>
	</div>
	<div class="btn-wrapper">
		<input type="button" value="Ok" class="btn" onclick="add();">
		<input type="button" value="Cancel" class="btn" onclick="reset();">
	</div>
</div>
<div class="clear"></div>
<h3>Vendor Commodities</h3>
<table class='main-table' border='0' id="vendorCommodity"
	style='border: none; border-collapse: collapse; float: none;'>
	<thead>
		<tr>
			<td class='header' style="display: none;">Commodity ID</td>
			<td class='header'>Commodity Code</td>
			<td class='header'>Commodity Description</td>
			<td class='header'>Action</td>
		</tr>
	</thead>
	<tbody>
		<logic:present name="vendorCommodities">
			<bean:size id="size" name="vendorCommodities" />
			<logic:equal value="0" name="size">
				<tr>
					<td>Nothing to display</td>
				</tr>
			</logic:equal>
		</logic:present>
		<logic:present name="vendorCommodities">
			<logic:iterate id="vendorCommodity" name="vendorCommodities"
				indexId="index">
				<tr id="row<%=index%>" class="even">
					<td style="display: none;"><bean:define id="id" property="id" name="vendorCommodity"></bean:define>
						<html:text property="commodityId" value="<%=id.toString()%>"
							styleClass="main-text-box" alt="" styleId="commodityId" /></td>
					<td><bean:define id="commodityCode"
							property="commodityId.commodityCode" name="vendorCommodity"></bean:define>
						<html:text property="commodityCode" readonly="true"
							value="<%=commodityCode.toString()%>" styleClass="main-text-box"
							alt="" /></td>
					<td><bean:define id="commodityDesc"
							property="commodityId.commodityDescription"
							name="vendorCommodity"></bean:define> <html:text
							property="commodityDesc" value="<%=commodityDesc.toString()%>"
							styleClass="main-text-box" alt="" readonly="true" /></td>
					<td><html:link
							action="deletecommodity.do?method=deleteCommodity" paramId="id"
							paramName="id" styleId="deleteCommodity" styleClass="del1">Delete</html:link>
					</td>
				</tr>
			</logic:iterate>
		</logic:present>
	</tbody>
</table>
<div class="clear"></div>
<INPUT id="cmd2" type="button" value="Add Commodity" class="btn"
	onclick="addCommodity()" tabindex="98" />
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 4%;">
	<h3>Services Description</h3>
<div class="form-box">
	<div class="wrapper-half">
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Describe Services</div>
			<div class="ctrl-col-wrapper">
				<html:textarea styleClass="main-text-area" property="vendorDescription" alt=""
					styleId="vendorDescription" tabindex="99" />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Company Information</div>
			<div class="ctrl-col-wrapper">
				<html:textarea styleClass="main-text-area" property="companyInformation" alt=""
					styleId="companyInformation" tabindex="100" />
			</div>
		</div>
	</div>
</div>
</div>
<div class="clear"></div>
<div class="panelCenter_1" style="margin-top: 4%;">
	<h3>Business Services</h3>
<div class="form-box">	
<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Service Area</div>
				<div class="ctrl-col-wrapper">
					<html:select property="serviceArea" styleId="serviceArea"
						 multiple="true" tabindex="101">
						<bean:size id="serviceAreaSize" name="serviceAreaList" />
						<logic:greaterEqual value="0" name="serviceAreaSize">
							<logic:iterate id="service" name="serviceAreaList">
								<bean:define id="serviceareaname" name="service" property="id"></bean:define>
								<html:option value="<%=serviceareaname.toString()%>">
									<bean:write name="service" property="serviceArea" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Business Area</div>
				<div class="ctrl-col-wrapper">
					<html:select property="businessArea" styleId="businessArea" style="width:90%;"
						 multiple="true" tabindex="102" >
						<bean:size id="areasize" name="serviceAreas1" />
						<logic:greaterEqual value="0" name="areasize">
							<logic:iterate id="area" name="serviceAreas1">
								<bean:define id="bgroup" name="area" property="businessGroup"></bean:define>
								<bean:define id="barea" name="area" property="serviceArea"></bean:define>
								<optgroup label="<%=bgroup.toString()%>">
									<%
										String[] areas = barea.toString().split("\\|");

										if (areas != null && areas.length != 0) {
											for (int i = 0; i < areas.length; i++) {
												String[] parts = areas[i].split("-");
												String areaId = parts[0];
												String areaName = parts[1];
									%>
									
									<option value="<%=areaId%>" ><%=areaName%></option>
									<%
										}
									}
									%>
								</optgroup>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
					<script>
					var arrayValue=[];
					</script>
					<logic:present  name="editVendorMasterForm"  property="businessArea">
					<logic:iterate id="selectID" name="editVendorMasterForm"  property="businessArea">
									<script>
									arrayValue.push('<%=selectID%>')
									</script>
					</logic:iterate>
					</logic:present>
					<script>
					$('select[id=businessArea] option').each(function() {
						
						for(var index=0;index<arrayValue.length;index++)
						    if ($(this).val()== arrayValue[index]) $(this).attr('selected', 'selected');
						
						});			
					</script>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div id="dialog2" title="Choose Commodity" style="display: none;">
	<table id="list2" width="100%"></table>
	<div id="pager2"></div>
	<div class="btn-wrapper">
		<input type="button" value="Ok" class="btn"
			onclick="addCommodityContent();"> <input type="button"
			value="Cancel" class="btn" onclick="cancelCommodity();">
	</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	function naicsChecked(id, value) {
		$('#' + id).val(value);
	}
	function cancelCommodity() {
		$("#dialog2").dialog("close");
	}
	$("#vendorCommodity tr td .del1").click(function(e) {
		e.preventDefault();
		var row = $(this).closest('tr');
		var rowid = row.attr('id');
		var url = $(this).attr('href');
		var ajaxUrl = url.substring(url.lastIndexOf("/") + 1, url.length);
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$.ajax({
				url : ajaxUrl,
				type : "POST",
				async : false,
				success : function() {
					$('#vendorCommodity tr#' + rowid).remove();
					alert('Record Deleted');
				}
			});
			return false;
		} else {
			return false;
		}
	});
	var selectedIDs = [];
	function addCommodity() {
		selectedIDs = pickCommodityCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.8;
		$("#dialog2").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});
		$("#dialog2").dialog({
			width : dWidth,
			height : dHeight,
			modal : true,
			close : function(event, ui) {
				//close event goes here
			},
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}
	function addCommodityContent() {
		var grid = jQuery("#list2");
		var rowCount = $("#vendorServiceArea tr").length;
		rowCount -= 1;
		var html = "";
		var messages = [];
		messages.push("Following Commodities already selected. You cannot add it again.\n");
		if (selectedIDs.length > 0) {
			for ( var i = 0, il = selectedIDs.length; i < il; i++) {
				var row = grid.getLocalRow(selectedIDs[i]);
				var commodityCode = row.commodityCode
				var commodityDesc = row.commodityDesc;
				var commodityExists = false;
				$("input[name='newCommodityCode']").each(function(index) { //For each of inputs
				    if (commodityCode === $(this).val()) { //if match against array
				    	messages.push(commodityDesc + "\n");
				        commodityExists = true;
				    } 
				});
				$("input[name='commodityCode']").each(function(index) { //For each of inputs
				    if (commodityCode === $(this).val()) { //if match against array
				    	messages.push(commodityDesc + "\n");
				        commodityExists = true;
				    } 
				});
				if(!commodityExists){
					html = html	+ "<tr class='even' id='row" + rowCount + "'>";
					html = html
							+ "<td style='display:none;'><input type='text' name='commodityId' class='main-text-box' value='0'></td>";
					html = html
							+ "<td><input type='text' name='newCommodityCode' class='main-text-box' readonly='readonly' value='" + commodityCode + "'></td>";
					html = html
							+ "<td><input type='text' name='commodityDesc' class='main-text-box' readonly='readonly' value='" + commodityDesc + "'></td>";
					html = html
							+ "<td><a style='cursor: pointer;' onclick='deleteRow(this);' >Delete</a></td></tr>";
					rowCount++;
				}
			}
			if(messages.length > 1){
				alert(messages.join(""));
			}
			$("#vendorCommodity").append(html);
			$("#dialog2").dialog("close");
		}
	}
	function deleteRow(ele) {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$(ele).closest("tr").remove();
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}
</script>