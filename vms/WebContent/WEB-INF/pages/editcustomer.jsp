<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- For JQuery Panel -->

<script type="text/javascript">
<!--
	$(document).ready(function() {
		
		$('.panelCenter_1').panel({
			collapsible : false
		});

	});

	function backToCustomer() {
		window.location = "displayCustomer.do?method=display";
	}
//-->
</script>
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>

<div class="page-title">
	<img src="images/Edit-Customer.gif" />Edit Customer
</div>
<html:form enctype="multipart/form-data" styleClass="FORM"
	action="/editcustomer.do?method=update" styleId="editcustomerform">
	<html:javascript formName="editCustomerForm" />

	<div id="demoTab">
		<ul class="resp-tabs-list">
			<li><a href="#tabs-1" style="color: #fff;" tabindex="1">Customer
					Information</a></li>
			<li><a href="#tabs-2" style="color: #fff;" tabindex="18">Contact
					Information</a></li>
			<li><a href="#tabs-3" style="color: #fff;" tabindex="32">SLA</a></li>
			<li><a href="#tabs-4" style="color: #fff;" tabindex="37">Personalize
					your profile</a></li>
			<li><a href="#tabs-5" style="color: #fff;" tabindex="39">Database
					Settings</a></li>
		</ul>

		<div class="resp-tabs-container">
			<div id="tabs-1">
				<%@include file="editcustomerregister.jsp"%>
			</div>
			<div id="tabs-2">
				<%@include file="editcontact.jsp"%>
			</div>
			<bean:define id="adminRole" name="adminRoleName"></bean:define>
			<logic:notEqual value="<%=adminRole.toString()%>" name="currentUser"
				property="userRoleId.roleName">
				<div id="tabs-3">
					<%@include file="noneditablecustomerslapage.jsp"%>
				</div>
			</logic:notEqual>
			<logic:equal value="<%=adminRole.toString()%>" name="currentUser"
				property="userRoleId.roleName">
				<div id="tabs-3">
					<%@include file="editcustomerslapage.jsp"%>
				</div>
			</logic:equal>
			<div id="tabs-4">
				<%@include file="editpersonalizedprofile.jsp"%>
			</div>
			<logic:notEqual value="<%=adminRole.toString()%>" name="currentUser"
				property="userRoleId.roleName">
				<div id="tabs-5">
					<%@include file="noneditdatabasesettings.jsp"%>
				</div>
			</logic:notEqual>
			<logic:equal value="<%=adminRole.toString()%>" name="currentUser"
				property="userRoleId.roleName">
				<div id="tabs-5">
					<%@include file="editdatabasesettings.jsp"%>
				</div>
			</logic:equal>
		</div>
	</div>

	<footer class="mt-2 card-footer">
	<div class="row justify-content-end">
		<div class="col-sm-9 wrapper-btn">
			<html:submit value="Update" styleClass="btn" styleId="submit1"></html:submit>
			<html:reset value="Cancel" styleClass="btn"
				onclick="backToCustomer();"></html:reset>
			</div>
		</div>
	</footer>

</html:form>
<script type="text/javascript">

	$(function($) {
		  $("#phone").mask("(999) 999-9999?");
		  $("#contactMobile").mask("(999) 999-9999?");
	});

	$(function() {
		$('#demoTab').easyResponsiveTabs();
		
		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
		}, "No Special Characters Allowed.");
		
		jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
	          return this.optional(element) || /^[0-9]+$/.test(value);
	    },"Please enter only numbers.");
		
		jQuery.validator.addMethod("login", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9]+$/.test(value)
		}, "No Special Characters Allowed.");
		
		jQuery.validator.addMethod("phoneNumber", function(value, element) { 
	        return this.optional(element) || /^[0-9-]+$/.test(value);
	  	},"Please enter valid phone number.");

		jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
		},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
		
		$("#editcustomerform").validate({
			rules : {
				companyname : {
					/* alpha : true, */
					required : true
				},
				companycode : {
					required : true,
					maxlength : 16
				},
				dunsnum : {
					required : true,
					maxlength : 9,
					onlyNumbers : true
				},
				taxid : {
					maxlength : 14,
					onlyNumbers : true
				},
				address1 : {
					required : true,
					maxlength : 125
				},
				city : {
					alpha : true,
					required : true,
					maxlength : 60
				},
				state : {
					required : true,
					alpha : true,
					maxlength : 60
				},
				province : {
					alpha : true,
					maxlength : 60
				},
				region : {
					alpha : true,
					maxlength : 60
				},
				country : {
					required : true,
					maxlength : 120
				},
				zipcode : {
					maxlength : 10,
					onlyNumbers : true
				},
				mobile : {
					maxlength : 20,
					phoneNumber : true
				},
				phone : {
					required : true/* ,
					maxlength : 20,
					phoneNumber : true */
				},
				fax : {
					maxlength : 16,
					onlyNumbers : true
				},
				webSite : {
					url : true
				},
				email : {
					required : true,
					maxlength : 120,
					email : true
				},
				firstName : {
					required : true,
					rangelength : [ 3, 15 ]
				},
				lastName : {
					rangelength : [ 3, 60 ]
				},
				designation : {
					rangelength : [ 1, 60 ]
				},
				contactPhone : {
					required : true,
					maxlength : 20,
					phoneNumber : true
				},
				contactMobile : {
					required : true,
				},
				contactFax : {
					required : true,
					maxlength : 16,
					onlyNumbers : true
				},
				contanctEmail : {
					required : true,
					email : true,
					maxlength : 120
				},
				loginDisplayName : {
					required : true,
					rangelength : [ 3, 15 ]
				},
				loginId : {
					required : true,
					login : true,
					rangelength : [ 3, 15 ]
				},
				loginpassword : {
					required : true,
					password:true
				},
				confirmPassword : {
					required : true,
					equalTo : "#loginpassword"
				},
				userSecQn : {
					required : true
				},
				userSecQnAns : {
					required : true,
					rangelength : [ 3, 15 ]
				},
				slaStartDate : {
					required : true
				},
				slaEndDate : {
					required : true
				},
				companyLogo : {
					accept : "jpg|bmp|gif|tiff|exif|png|webp"
				},
				licencedUsers : {
					required : true,
					onlyNumbers : true
				},
				licencedVendors : {
					required : true,
					onlyNumbers : true
				},
			},

			ignore : "ui-tabs-hide",
			submitHandler : function(form) {
				if (checkDate('slaStartDate', 'slaEndDate')) {
					$("#ajaxloader").css('display','block');
					$("#ajaxloader").show();
					form.submit();
				}
			},

			invalidHandler : function(form, validator) {

				var errors = validator.numberOfInvalids();
				if (errors) {
					var invalidPanels = $(
							validator.invalidElements())
							.closest(".resp-tab-content", form);

					if (invalidPanels.size() > 0) {
						$.each($.unique(invalidPanels.get()),function() {

							if ( $(window).width() > 650 ) {
								$("a[href='#"+ this.id+ "']").parent()
								.not(".resp-accordion").addClass("error-tab-validation")
								.show("pulsate",{times : 3});
							} else {
							   $("a[href='#"+ this.id+ "']").parent()
								.addClass("error-tab-validation")
									.show("pulsate",{times : 3});
							}
						});
						$('input[type="text"]').each(function() {
							if ($(this).attr('alt') == 'Optional'
									&& $(this).attr('value') == '') {
								this.value = 'Optional';
							}
						});
					}
				}
			},
			unhighlight : function(element, errorClass,
					validClass) {

				$(element).removeClass(errorClass);
				$(element.form).find(
						"label[for=" + element.id + "]")
						.removeClass(errorClass);
				var $panel = $(element).closest(
						".resp-tab-content", element.form);
				if ($panel.size() > 0) {
					if ($panel.find(
							"." + errorClass + ":visible")
							.size() > 0) {
						if ($(window).width() > 650) {
							$("a[href='#"+ $panel[0].id+ "']")
									.parent()
									.removeClass("error-tab-validation");
						} else {
							$("a[href='#"+ $panel[0].id+ "']")
									.parent()
									.removeClass("error-tab-validation");
						}
					}
				}
			}
		});
	});
</script>
