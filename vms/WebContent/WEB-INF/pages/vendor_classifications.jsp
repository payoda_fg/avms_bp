<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<script>
	function setPrimeValue() {
		$('#prime').val('1');
	}
	function setNonPrimeValue() {
		$('#nonprime').val('0');
	}
	
	$(document).ready(function() {
		if($("input[name='primeNonPrimeVendor']:checked")){
			$("#nonprime").attr('disabled','true');
		}
		
		$("input[name='primeNonPrimeVendor']").click(function() {
		    if ($(this).val() === '1') {
		      input_box = confirm("Do you want change from Non-Prime Vendor to Prime Vendor? Prime vendor can not be changed as Non-Prime Vendor");
				if (input_box == true) {
					// Output when OK is clicked
					return true;
				} else {
					// Output when Cancel is clicked
					// alert("Delete cancelled");
					return false;
				}
		    } 
		    
		});
	});
</script>

<div class="panelCenter_1">
	<h3>Vendor Classification</h3>
	<table cellpadding="10" cellspacing="5" class="main-table" border="0"
			 style="border-collapse: separate;">
		<tr></tr>
		<tr>
			<td>Prime Vendor <html:radio property="primeNonPrimeVendor"
					value="prime" idName="editVendorMasterForm" tabindex="113"
					styleId="prime" onclick="setPrimeValue()"></html:radio> Non-Prime
				Vendor <html:radio property="primeNonPrimeVendor" value="nonprime"
					idName="editVendorMasterForm" tabindex="114" styleId="nonprime"
					onclick="setNonPrimeValue()"></html:radio></td>
		</tr>
	</table>
</div>
<div class="clear"></div>