<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<div class="panelCenter_1">
	<h3>Customer Contact Information</h3>

	<div class="form-box">

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="firstName" alt=""
						styleId="firstName" tabindex="19" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="lastName"
						styleId="lastName" alt="Optional" tabindex="20" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>

			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Title</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="designation"
						styleId="designation" alt="Optional" tabindex="21" />
				</div>
				<span class="error"><html:errors property="designation"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contactPhone"
						styleId="contactPhone" alt="" tabindex="22" />
				</div>
				<span class="error"><html:errors property="contactPhone"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contactMobile"
						styleId="contactMobile" alt="" tabindex="23" />
				</div>
				<span class="error"><html:errors property="contactMobile"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">FAX</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contactFax"
						styleId="contactFax" alt="" tabindex="24" />
				</div>
				<span class="error"><html:errors property="contactFax"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="contanctEmail"
						styleId="contanctEmail" alt="" onchange="ajaxFn(this,'CE');"
						tabindex="25" />
				</div>
				<span class="error"><html:errors property="contanctEmail"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div id="loginId" class="panelCenter_1">
	<h3>User Information</h3>

	<div class="form-box">

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Display Name</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="loginDisplayName"
						styleId="loginDisplayName" alt="" tabindex="26" />
				</div>
				<span class="error"><html:errors property="loginDisplayName"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Login Id</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="loginId"
						styleId="loginId" alt="" onchange="ajaxFn(this,'CU');"
						tabindex="27" />
				</div>
				<span class="error"><html:errors property="loginId"></html:errors></span>

			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="loginpassword" styleId="loginpassword"
						tabindex="28" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="loginpassword"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Confirm Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="confirmPassword" styleId="confirmPassword"
						tabindex="29" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="confirmPassword"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question</div>
				<div class="ctrl-col-wrapper">
					<html:select property="userSecQn" name="customerForm"
						styleClass="list-box" styleId="userSecQn" tabindex="30">
						<html:option value="">----Select---</html:option>
						<bean:size id="size" name="secretQnsList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="secretQn" name="secretQnsList">
								<bean:define id="id" name="secretQn" property="id"></bean:define>
								<html:option value="<%=id.toString()%>">
									<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question Answer</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="userSecQnAns"
						styleId="userSecQnAns" alt="" tabindex="31" />
				</div>
				<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
