<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<h3>Vendor Documents</h3>
<INPUT type="button" value="New" class="btn"
	onclick="addDocRow('vendorDocs')" />
<%	
int uploadRestriction=0;
if (session.getAttribute("uploadRestriction") != null) {
	uploadRestriction =(Integer)session.getAttribute("uploadRestriction");
}
 %>

<table id='vendorDocs' class='main-table' width='100%' border='0'
	style='border: none; border-collapse: collapse; float: none;'>
	<thead>
		<tr>
			<td class='header'>Document Name</td>
			<td class='header'>Document Description</td>
			<td class='header'>File</td>
			<td class='header'>Size</td>
		<tr>
	</thead>
	<tbody>

	</tbody>
</table>
<div class="clear"></div>
<script type="text/javascript">
	function addDocRow(tableID) {
		var noOfFilesToBeUploaded=<%= uploadRestriction %>;
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		if ((rowCount - 2) <= noOfFilesToBeUploaded) {
			var row = table.insertRow(rowCount);
			var newcell0 = row.insertCell(0);
			newcell0.innerHTML = "<td><input type='text' name='vendorDocName' id='vendorDocName"+rowCount+"' class='main-text-box'></td>";
			var newcell1 = row.insertCell(1);
			newcell1.innerHTML = "<td><textarea name='vendorDocDesc' id='vendorDocDesc"+rowCount+"' class='main-text-area' ></textarea></td>";
			var newcell2 = row.insertCell(2);
			newcell2.innerHTML = "<td><input type='text' readonly='readonly' id='vendorDocFileName"+rowCount+"'  class='main-text-box'></td>";
			var newcell3 = row.insertCell(3);
			newcell3.innerHTML = "<td><input type='text' readonly='readonly' id='vendorDocFileSize"+rowCount+"' class='main-text-box'></td>";
			var newcell4 = row.insertCell(4);
			newcell4.innerHTML = '<div class="fileUpload btn btn-primary"><span>Browse</span>'
					+ '	<input name="vendorDoc['
					+ (rowCount - 2)
					+ ']" type="file" id="vendorDoc'
					+ rowCount
					+ '" class="upload"/> </div>';

			$("#vendorDoc" + rowCount)
					.on(
							"change",
							function() {
								var fileSize = this.files[0].size;
								if (fileSize > 10485760) {
									alert("Sorry you have exceeded the file size of "
											+ Math
													.round((10485760 / 1024) / 1024)
											+ " MB");
									return false;
								}
								document.getElementById("vendorDocFileName"
										+ rowCount).value = this.value;
								document.getElementById("vendorDocFileSize"
										+ rowCount).value = fileSize + " KB";
							});
		} else {
			alert("Only " + noOfFilesToBeUploaded + " files can be uploaded");
			return false;
		}

	}
</script>