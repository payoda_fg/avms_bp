<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<script type="text/javascript">
	function clearFields() {
		window.location = "viewVendors.do?method=showVendorSearch";
	}

	$(document).ready(function() {

		$("#diverseCertificateNames").multiselect({
			selectedText : "# of # selected"
		});

	});
</script>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>

<section role="main" class="content-body card-margin mt-5">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<header class="card-header">
				<h2 class="card-title pull-left">
	<img id="show1" src="images/VendorSearch.gif" /> Search Vendor</h2>
</header>


<logic:iterate id="privilege" name="privileges">

	<logic:match value="View Vendors" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">

			<div class="form-box card-body">

				<html:form action="/viewVendors.do?method=vendorSearch">
					<html:javascript formName="searchVendorForm" />
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor Name</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text styleClass="text-box form-control" property="vendorName"
									alt="Optional" size="23" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">NAICS Code</label>
							<div class="ctrl-col-wrapper">
								<html:text styleClass="text-box form-control" property="naicCode"
									alt="Optional" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Naics Description</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:textarea property="naicsDesc" styleClass="main-text-area"
									rows="3" cols="29" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text property="city" alt="Optional" />
							</div>
						</div>					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text styleClass="text-box form-control" property="state" alt="" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Province</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text styleClass="text-box form-control" property="province"
									alt="Optional" size="23" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="country" styleId="countryId"
									styleClass="list-box">
									<html:option value="0" key="select">-----Select-----</html:option>
									<logic:present name="searchVendorForm" property="countries">
										<bean:size id="size" property="countries"
											name="searchVendorForm" />
										<logic:greaterEqual value="0" name="size">

											<logic:iterate id="country" property="countries"
												name="searchVendorForm">
												<bean:define id="name" name="country" property="name"></bean:define>
												<html:option value="<%=name.toString()%>">
													<bean:write name="country" property="name" />
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</html:select>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Region</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:text styleClass="text-box" property="region"
									alt="Optional" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Diverse Supplier</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:radio property="diverse" value="1">&nbsp;Diverse&nbsp;</html:radio>
								<html:radio property="diverse" value="0">&nbsp;Non Diverse&nbsp;</html:radio>
							</div>

						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Diverse Classification</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="diverseCertificateNames" multiple="true"
									styleId="diverseCertificateNames" name="searchVendorForm">
									<logic:present name="certificateTypes">
										<bean:size id="size" name="certificateTypes" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="certificate" name="certificateTypes">
												<bean:define id="id" name="certificate" property="id"></bean:define>
												<html:option value="<%=id.toString()%>">
													<bean:write name="certificate" property="certificateName"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</html:select>
							</div>
						</div>
					
					<div class="clear"></div>
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<html:submit value="Search" styleClass="btn btn-primary" styleId="submit"></html:submit>
						<html:reset value="Clear" styleClass="btn btn-default" styleId="submit"
							onclick="clearFields()"></html:reset>
					</div>
					</div>
					</footer>

				</html:form>
			</div>
			<div class="clear"></div>
			<header class="card-header">
				<h2 class="card-title pull-left">
				<img src="images/icon-registration.png" />&nbsp;&nbsp;Following are
				your Vendors</h2>
			</header>
			<logic:present property="vendorsList" name="searchVendorForm">

				<div class="form-box card-body">
					<table 
						border="0" class="main-table table table-bordered table-striped mb-0" id="vendorData">
						<bean:size id="size" property="vendorsList"
							name="searchVendorForm" />
						<logic:greaterThan value="0" name="size">
							<tr>

								<td class="">Vendor Name</td>
								<td class="">Company Code</td>
								<td class="">DUNS Number</td>
								<td class="">Country</td>
								<td class="">NAICS Code</td>
								<td class="">Region</td>
								<td class="">State</td>
								<td class="">City</td>
								<td class="">Action</td>
							</tr>
							<logic:iterate property="vendorsList" name="searchVendorForm"
								id="list">
								<bean:define id="vendor" name="list" property="vendor"></bean:define>
								<bean:define id="naics" name="list" property="naics"></bean:define>
								<tr>
									<td nowrap><font class="fontbn2"> <bean:define
												id="vendorId" name="vendor" property="id"></bean:define> <html:link
												action="/retriveVendor?method=retriveVendor" paramId="id"
												paramName="vendorId">
												<bean:write name="vendor" property="vendorName" />
											</html:link>
									</font></td>
									<td><bean:write name="vendor" property="vendorCode" /></td>
									<td><bean:write name="vendor" property="dunsNumber" /></td>
									<td><bean:write name="vendor" property="country" /></td>
									<td><bean:write name="naics" property="naicsCode" /></td>
									<td><bean:write name="vendor" property="region" /></td>
									<td><bean:write name="vendor" property="state" /></td>
									<td><bean:write name="vendor" property="city" /></td>
									<td><a onclick="return deleteVendor('<%=vendorId%>');"
										style="cursor: pointer;">Delete</a></td>
								</tr>

							</logic:iterate>
						</logic:greaterThan>
						<logic:equal value="0" name="size">
							<tr>
							</tr>
							<tr>
								<td>No such Records</td>
							</tr>
						</logic:equal>
					</table>


				</div>

			</logic:present>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">

	<logic:match value="View Vendors" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have no rights to view vendor</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
</div>
</div>
</section>
