<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>
<%@page import="com.fg.vms.customer.model.NaicsMaster"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<script type="text/javascript">
<!--
	function backToNaicsCode() {
		window.location = "viewnaicsmaster.do?parameter=viewNAICSMasterPage";
	}

	$(document).ready(function() {

		$("#formID").validate({
			rules : {
				naicsCategoryId : {
					required : true
				},
				subCategory : {
					required : true
				},
				naicsCode : {
					required : true
				},
				naicsDescription : {
					required : true
				},
				isicCode : {
					required : true
				},
				isicDescription : {
					required : true
				}
			}
		});
	});
//-->
</script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">
		<logic:iterate id="privilege" name="privileges">

			<logic:match value="NAICS Master" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="modify">
					<div class="toggleFilter">
						<h2 id="show" title="Click here to hide/show the form"
							style="cursor: pointer;">
							<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Update
							NAICS

						</h2>
					</div>
					<div id="table-holder"
						style="margin: 1% 1.5%; float: left; width: 96.5%;">
						<html:form
							action="/updatenaicsmaster.do?parameter=updatenaicsmaster"
							styleId="formID">
							<html:javascript formName="naicsMasterForm" />
							<%
								String categoryName = session.getAttribute(
														"categoryName").toString();
							%>
							<div style="float: left;">
								<html:hidden property="id" name="naicsMasterForm" />
								<table style="margin-left: 50px;" cellspacing="10"
									cellpadding="5">
									<tr>
										<td>NAICS Category</td>
										<td><bean:define id="category" name="categoryName"></bean:define>
											<html:select property="naicsCategoryId"
												name="naicsMasterForm" styleId="naicsCategoryId"
												style="width:140px;font-size:11px;">
												<bean:size id="size" name="categoryName" />
												<logic:greaterEqual value="0" name="size">
													<html:option value="0" key="select">-----Select-----</html:option>
													<logic:iterate id="categories" name="categoryName">
														<bean:define id="id" name="categories" property="id"></bean:define>
														<bean:define id="naicsCategoryDesc" name="categories"
															property="naicsCategoryDesc"></bean:define>
														<html:option value="<%=id.toString() %>">
															<bean:write name="categories"
																property="naicsCategoryDesc" />
														</html:option>
													</logic:iterate>
												</logic:greaterEqual>
											</html:select></td>
										<td><span class="error"><html:errors
													property="naicsCategoryId"></html:errors></span></td>
									</tr>
									<tr>
										<td>NAICS Sub-Category</td>
										<td><bean:define id="category" name="naicsub"></bean:define>
											<html:select property="subCategory" name="naicsMasterForm"
												styleId="naicsSubCategoryId"
												style="width:140px;font-size:11px;">
												<bean:size id="size" name="naicsub" />
												<logic:greaterEqual value="0" name="size">
													<html:option value="0" key="select">-----Select-----</html:option>
													<logic:iterate id="categories" name="naicsub">
														<bean:define id="id" name="categories" property="id"></bean:define>
														<bean:define id="naicSubCategoryDesc" name="categories"
															property="naicSubCategoryDesc"></bean:define>
														<html:option value="<%=id.toString() %>">
															<bean:write name="categories"
																property="naicSubCategoryDesc" />
														</html:option>
													</logic:iterate>
												</logic:greaterEqual>
											</html:select></td>
										<td><span class="error"><html:errors
													property="subCategory"></html:errors></span></td>
									</tr>
									<tr>
										<td>NAICS Code</td>
										<html:hidden property="hiddenNaicsCode" alt=""
											styleId="hiddenNaicsCode" />
										<td><html:text property="naicsCode"
												name="naicsMasterForm" alt="" styleId="naicsCode"
												onchange="ajaxEdFn(this,'hiddenNaicsCode','NC')" /></td>
										<td><span class="error"><html:errors
													property="naicsCode"></html:errors></span></td>
									</tr>
									<tr>
										<td>NAICS Description</td>
										<td><html:text property="naicsDescription"
												name="naicsMasterForm" alt="" /></td>
										<td><span class="error"><html:errors
													property="naicsDescription"></html:errors></span></td>
									</tr>
								</table>
							</div>
							<div style="float: left;">
								<table style="margin-left: 45%;" cellspacing="10"
									cellpadding="5">
									<tr>
										<td>ISIC Code</td>
										<td><html:text property="isicCode" name="naicsMasterForm"
												alt="" /></td>
										<td><span class="error"><html:errors
													property="isicCode"></html:errors></span></td>
									</tr>
									<tr>
										<td>ISIC Description</td>
										<td><html:text property="isicDescription"
												name="naicsMasterForm" alt="" /></td>
										<td><span class="error"><html:errors
													property="isicDescription"></html:errors></span></td>
									</tr>
									<tr>
										<td>IsActive&nbsp;</td>
										<td><html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
											<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
										</td>
									</tr>
								</table>

								<logic:iterate id="privilege" name="privileges">

									<logic:match value="NAICS Master" name="privilege"
										property="objectId.objectName">
										<logic:match value="1" name="privilege" property="add">
											<div style="padding: 30px 0 10px 0;">
												<html:submit value="Update" styleClass="customerbtTxt"
													styleId="submit"></html:submit>
												<html:reset value="Cancel" styleClass="customerbtTxt"
													onclick="backToNaicsCode();"></html:reset>
											</div>
										</logic:match>
									</logic:match>

								</logic:iterate>

							</div>

						</html:form>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:match value="NAICS Master" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="modify">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to edit NAICS code</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>

	</div>
</div>