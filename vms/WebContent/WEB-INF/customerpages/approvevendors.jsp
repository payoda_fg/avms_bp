<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.fg.vms.customer.model.VendorMaster"%>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>

<script type="text/javascript">
	function searchData() {
		var category = $('#naicsCategoryId').val();
		var vendorName = $('#vendorName').val();
		var naicCode = $('#naicCode').val();
		var naicsDesc = $('#naicsDesc').val();
		var subCategory = $('#subCategory').val();
		var province = $('#province').val();
		var city = $('#city').val();
		var country = $('#countryId').val();
		var state = $('#state').val();
		var region = $('#region').val();
		var diverse = $('input:radio[name=diverse]:checked').val();
		var diverseCertificateNames = [];
		$('#diverseCertificateNames > :selected').each(function() {
			diverseCertificateNames.push($(this).val());
		});
		if(diverseCertificateNames == ""){
			diverseCertificateNames = 0;
		}
		
		$.ajax({
			url : "searchvendorpage.do?method=vendorApprovalSearch&diverseCertificateNames="+diverseCertificateNames,
			type : 'POST',
			data : {
				category : category,
				vendorName : vendorName,
				naicCode : naicCode,
				naicsDesc : naicsDesc,
				subCategory : subCategory,
				province : province,
				city : city,
				country : country,
				state : state,
				region : region,
				diverse : diverse
			},
			async : false,
			success : function(data) {
				$("#gridsearchvendor").jqGrid('GridUnload');
				gridSearchData(data);
			}
		});
	}

	function reportDownload() {
		window.location = "naicreportdownload.do?method=downloadReport";
	}
	
	$(document).ready(function() {

		$("#diverseCertificateNames").multiselect({
			selectedText : "# of # selected"
		});
		
	});
</script>
<style>
<!-- /* 
.ui-jqgrid .ui-jqgrid-btable {
	table-layout: fixed;
	cursor: pointer;
}

.ui-jqgrid .ui-jqgrid-bdiv {
	position: relative;
	margin: 0em;
	padding: 0;
	overflow: auto;
	overflow-x: hidden;
	overflow-y: auto;
	text-align: left;
}
*/
-->
</style>
<div class="page-title">
	<img id="show1" src="images/VendorSearch.gif" />Search Vendor
</div>

<logic:iterate id="privilege" name="privileges">

	<logic:match value="Vendor Approve" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div id="successMsg">
				<html:messages id="msg" property="mail" message="true">
					<span style="color: green;"><bean:write name="msg" /></span>
				</html:messages>
			</div>
			<div id="failureMsg">
				<html:messages id="msg" property="mailfailed" message="true">
					<span style="color: red;"><bean:write name="msg" /></span>
				</html:messages>
			</div>


			<div class="form-box">
				<form id="searchForm">
					<input type="hidden" id="vendorid" />

					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Vendor Name</div>
							<div class="ctrl-col-wrapper">
								<input type="text" name="vendorName" id="vendorName"
									alt="Optional" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">NAICS Code</div>
							<div class="ctrl-col-wrapper">
								<input type="text" name="naicCode" id="naicCode" alt="Optional"
									size="23" />
							</div>

						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Naics Description</div>
							<div class="ctrl-col-wrapper">
								<input name="naicsDesc" class="main-text-area" id="naicsDesc" 
									style="width: 85%;"/>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">City</div>
							<div class="ctrl-col-wrapper">
								<input type="text" name="city" id="city" alt="Optional" />
							</div>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">State</div>
							<div class="ctrl-col-wrapper">
								<input type="text" name="state" id="state" alt="Optional" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Region</div>
							<div class="ctrl-col-wrapper">
								<input type="text" name="region" id="region" alt="Optional" />
							</div>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Province</div>
							<div class="ctrl-col-wrapper">
								<input type="text" name="province" id="province" alt="Optional" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Country</div>
							<div class="ctrl-col-wrapper">
								<select name="country" id="countryId" class="list-box">
									<option value="0">--Select--</option>
									<logic:present name="searchVendorForm" property="countries">
										<bean:size id="size" property="countries"
											name="searchVendorForm" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="country" property="countries"
												name="searchVendorForm">
												<bean:define id="name" name="country" property="name"></bean:define>
												<option value="<%=name.toString()%>">
													<bean:write name="country" property="name" />
												</option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</select>
							</div>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Diverse Supplier</div>
							<div class="ctrl-col-wrapper">
								<input type="radio" name="diverse" value="1">&nbsp;Diverse&nbsp;
								<input type="radio" name="diverse" value="0">&nbsp;Non
								Diverse&nbsp;
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Diverse Classification</div>
							<div class="ctrl-col-wrapper">
								<html:select property="diverseCertificateNames" multiple="true"
									styleId="diverseCertificateNames" name="searchVendorForm">
									<logic:present name="certificateTypes">
										<bean:size id="size" name="certificateTypes" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="certificate" name="certificateTypes">
												<bean:define id="id" name="certificate" property="id"></bean:define>
												<html:option value="<%=id.toString()%>">
													<bean:write name="certificate" property="certificateName"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</html:select>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="wrapper-btn">
						<input type="button" value="Search" class="btn" id="submit"
							onclick="searchData();"> <input type="reset"
							value="Clear" class="btn" onclick="clearFields();">
					</div>
				</form>
			</div>
			<div class="clear"></div>
			<div class="page-title">
				<img id="show" src="images/Vendor-Approval.gif" />Vendors to be
				approved
			</div>
			<div class="form-box">
				<table id="gridsearchvendor">

				</table>
				<div id="pager"></div>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">

	<logic:match value="Vendor Approve" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div>
				<h3>You have no rights to view vendors</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
<div id="dialog" style="display: none;" title="Approve Vendor">
	<table cellpadding="10" cellspacing="10">
		<tr></tr>
		<tr>
			<td>Prime/NonPrime:</td>
			<td><input type="radio" name="prime" id="primevendor"
				class="selectedPrimeVendor" value="1" onclick="ajaxFn(this,'P');" />&nbsp;Prime&nbsp;
				<input type="radio" class="selectedPrimeVendor" name="prime"
				value="0" />&nbsp;Non-Prime&nbsp;</td>
		</tr>
		<tr></tr>
		<tr>
			<td align="right">Approve:</td>
			<td><input type="radio" class="selectedApprovalStatus"
				name="approvalStatus" value="1" />&nbsp;Yes&nbsp; <input
				type="radio" class="selectedApprovalStatus" name="approvalStatus"
				name="approval" value="0" />&nbsp;No&nbsp;</td>
		</tr>
		<tr></tr>
		<tr>
			<td align="right">Comments:</td>
			<td><textarea cols="50" rows="5" name="isApprovedDesc"
					id="isApprovedDesc"></textarea></td>
		</tr>
	</table>
	<div class="btn-wrapper">
		<input type="button" class="btn" onclick="getSelectedIds();"
			value="Submit"> <input type="reset" class="btn" value="Reset"
			onclick="resetFields();">
	</div>
	<div id="ajaxloader" style="display: none;">
		<img src="images/ajax-loader.gif" alt="Ajax Loading Image" />
	</div>
</div>


