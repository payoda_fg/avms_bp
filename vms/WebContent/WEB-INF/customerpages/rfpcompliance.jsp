<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>

<div class="Phase2-forms-Container">

	<table id="gradient-style" summary="Meeting Results">
		<thead>
			<tr>
				<th scope="col"><input type="checkbox" name="chk" /></th>
				<th scope="col">S.No</th>
				<th scope="col">Compliance Item</th>
				<th scope="col">Target</th>
				<th scope="col">Compliance Attributes</th>
				<th scope="col">Mandatory</th>
				<th scope="col">Notes</th>
			</tr>
		</thead>
		<tr>
			<td><input type="checkbox" name="chk_1" /></td>
			<td>1<input type='hidden' name='rid_1' id='rid_1' value='1' /><input
				type="hidden" name="attributeCount" value="3" id="attributecount_1" /></td>
			<td><textarea name="complianceItems" cols="25" rows="3"></textarea></td>
			<td><p class="leftalign">
					<label> <input type="radio" name="target[0]" value="V" />
						Vendor
					</label> <br /> <label> <input type="radio" name="target[0]"
						value="I" /> Internal
					</label> <br />
				</p></td>
			<td class="nospace">


				<div class="create-smllform-RFI">
					<div class="Comp-attri-titles">
						<p style="font-weight: normal">Attributes</p>

					</div>
					<div id="Comp-attri">
						<table id="tabatt_1">

							<tr>
								<td><input name='chkatt_1_1' type='checkbox' value='1'
									size='15' /></td>
								<td><input type="text" name="attribute" size="15" value="" /></td>
								<td><input type="text" name="weightages" size="5" value="" /></td>
							</tr>
							<tr>
								<td><input name='chkatt_1_2' type='checkbox' value='2'
									size='15' /></td>
								<td><input type="text" name="attribute" size="15" value="" /></td>
								<td><input type="text" name="weightages" size="5" value="" /></td>
							</tr>
							<tr>
								<td><input name='chkatt_1_3' type='checkbox' value='3'
									size='15' /></td>
								<td><input type="text" name="attribute" size="15" value="" /></td>
								<td><input type="text" name="weightages" size="5" value="" /></td>
							</tr>

						</table>
					</div>
					<div id="Comp-attri-icon">
						<img src="images/icon-Plus.gif" width="16" height="16"
							onClick="fn_addAttRow('tabatt_1', '1', 'RFP')" /> <img
							src="images/icon-minus.gif" width="16" height="16"
							onClick="fn_removeAttRow('tabatt_1', '1')" />
					</div>
				</div>
			</td>

			<td>
				<p class="leftalign">
					<label> <input type="radio" name="mandatory[0]" value="M" />
						Mandatory
					</label><br /> <label> <input type="radio" name="mandatory[0]"
						value="O" /> Optional
					</label> <br />
				</p>
			</td>
			<td><textarea name="notes" cols="15" rows="3"></textarea></td>
		</tr>

	</table>

	<div id="footer-btn">
		<input name="Button" class="customerbtTxt" type="button"
			value="Add Question" onClick="addRow('gradient-style', 'RFI');" /> <input
			name="Button" class="customerbtTxt" type="button"
			value="Delete Qusetion" onClick="deleteRow('gradient-style');" />

	</div>

</div>
<div class="clear"></div>