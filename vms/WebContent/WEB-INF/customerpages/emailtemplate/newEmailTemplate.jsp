<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<style>
<!--
.ui-multiselect ui-widget ui-state-default ui-corner-all {
	width: 250px;
}
-->
</style>
<style>
	.spancolor {
		color: green;
	}
</style>

<script>
	jQuery.validator.addMethod("selectAtleastOne", function(value, element) {
		return this.optional(element) || ! /^[0]+$/.test(value);
	}, "Please Select Any Template Name.");
	
	$(document).ready(function() {
		$("#formID").validate({
			rules : {
				emailNewTemplateName : {
					required : true
				},
				emailTemplateMessage : {
					required : true
				},
				subject : {
					required :true					
				}
			}
		});

		//For multiselect 
		$("#emailtemplateDivision").multiselect({
			selectedText : "# of # selected"
		});
	});

	function clearfields() {
		window.location = "emailtemplate.do?parameter=showEmailTemplatePage";
	}

	tinymce.init({
		mode : "exact",
		/* elements : "emailTemplateMessage", */
		selector : "textarea#emailTemplateMessage",
		theme : "modern",
		statusbar : false,
		height : 300,
		width : 650,
		plugins : [
				"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
		toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor ",
		menubar : false,
		file_browser_callback: AVMSFileBrowser,
		toolbar_items_size : 'medium'
	});
	
	function AVMSFileBrowser(field_name, url, type, win) {
		
		   var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;
		  
		   tinyMCE.activeEditor.windowManager.open({
		     file: avmsFileman,
		     title: 'Email Template',
		     width: 600, 
		     height: 500,
		     resizable: "yes",
		     plugins: "media",
		     inline: "yes",
		     close_previous: "no"  
		  }, {     window: win,     input: field_name    });
		  return false; 
		}
</script>

<div id="successMsg">
	<html:messages id="msg" property="template" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="transactionFailure" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="divisionExist" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="parameterExist" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="templateExists" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Email Template" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="add">
			<div class="page-title">
				<img src="images/arrow-down.png" alt="" />Email Templates
			</div>
			<!--Row End Here-->
			<div class="form-box">
				<html:form action="/emailtemplate?parameter=saveNewTemplates" styleId="formID">
					<html:hidden property="templateId" />
					<html:hidden property="templateCode"/>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Email Template</div>
							<div class="ctrl-col-wrapper">
								<html:text property="emailNewTemplateName" size="100" alt="" styleClass="main-text-box" styleId="emailNewTemplateName"></html:text>
							</div>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Subject:</div>
							<div class="ctrl-col-wrapper">
								<html:text property="subject" size="100" alt="" styleClass="main-text-box" styleId="subject"></html:text>
							</div>
							<span class="error"><html:errors property="subject" /></span>
						</div>
					</div>
					<logic:equal value="1" name="isDivisionStatus" property="isDivision">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Applicable Divisions</div>
							<div class="ctrl-col-wrapper">
								<html:select property="emailtemplateDivision" multiple="true" name="emailTemplateForm" styleId="emailtemplateDivision">
									<bean:size id="size" name="customerDivisions" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="customerDivision" name="customerDivisions">
											<bean:define id="id" name="customerDivision" property="id"></bean:define>
											<html:option value="${id}">
												<bean:write name="customerDivision" property="divisionname"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
							<span class="error">
								<html:errors property="customerDivision"></html:errors> 
							</span>
						</div>
					</logic:equal>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Email Template Message</div>
							<div class="ctrl-col-wrapper">
								<html:textarea property="emailTemplateMessage" alt="" styleId="emailTemplateMessage" styleClass="main-text-area" />
							</div>
						</div>
					</div>
					<div class="wrapper-btn">
						<logic:notPresent name="emailTemplateForm" property="templateId">
							<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
						</logic:notPresent>
						<logic:present name="emailTemplateForm" property="templateId">
							<html:submit value="Update" styleClass="btn" styleId="submit"></html:submit>
						</logic:present>
							<html:reset value="Cancel" styleClass="btn" onclick="clearfields();"></html:reset>
							<html:reset value="Clear" styleClass="btn"></html:reset>
					</div>
				</html:form>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>