<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<style>
	.row-wrapper .label-col-full-wrapper
	{
		float: left;
		line-height: 25px;
		padding: 0 0 0 2%;
		width: 12%;
	}
	
	.row-wrapper .ctrl-col-full-wrapper
	{
		padding: 0 0 0 0%;
	}	
</style>

<script type="text/javascript">
	tinymce.init(
	{
		selector : "textarea#message",
		theme : "modern",
		statusbar : false,
		height : 250,
		width : 600,
		plugins : [
				"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
		toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
		menubar : false,
		file_browser_callback: AVMSFileBrowser,
		toolbar_items_size : 'medium'
	});
	
	function AVMSFileBrowser(field_name, url, type, win)
	{	
		var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;
	
		tinyMCE.activeEditor.windowManager.open(
		{
			file: avmsFileman,
			title: 'Email Template',
			width: 600, 
			height: 500,
			resizable: "yes",
			plugins: "media",
			inline: "yes",
			close_previous: "no"
		},
		{     window: win,     input: field_name    });
		
		return false; 
	}
	
	function backToSearchResultPage()
	{
		window.location = "viewVendorsStatus.do?method=getPreviousSearchResults&valueFrom=wizard";
	}
	
	function validateEmailHistoryForm()
	{
		var toValue = $("#to").val();
		var ccValue = $("#cc").val();
		var subjectValue = $("#subject").val();

		if(toValue == '' && ccValue == '')
		{
			alert("Enter atleast one email id in TO/CC.");
			return false;
		}
		else if(subjectValue == 'undefined' || subjectValue == '')
		{
			alert("Enter Subject Message.");
			return false;
		}
		
		return true;
	}
	
	function getVendorEmailIds()
	{
		$(function()
		{
			$("#vendorsEmailIdListDialog").css(
			{
				'display' : 'block',
				'font-size' : 'inherit'
			});
			
			$("#vendorsEmailIdListDialog").dialog(
			{
				height : '400',
				modal : true,
				resizable : false,
				autoResize : false,
				show :
				{
					effect : "scale",
					duration : 1000
				},
				hide :
				{
					effect : "scale",
					duration : 1000
				}
			});
		});
	}
	
	function setSelectedVendorEmailIds()
	{
		var vendorEmailIdsList = [];
		
		$('input[name=listVendorEmailId]').each(function()
		{
			if ($(this).is(":checked"))
				vendorEmailIdsList.push($(this).val());
		});
		
		if(vendorEmailIdsList.length != 0 && vendorEmailIdsList != "")
		{
			for (index = 0; index < vendorEmailIdsList.length; index++)
			{
				if($("#to").val() != "" && $("#to").val() != 'undefined')
				{
					$("#to").val($("#to").val() + (vendorEmailIdsList[index] + ";"));
				}				
				else
				{
					$("#to").val(vendorEmailIdsList[index] + ";");
				}				
			}
			
			$('input[name=listVendorEmailId]').each(function()
			{
				$(this).attr("checked", false);
			});
			
			$('#vendorsEmailIdListDialog').dialog('close');
		}		
	}
</script>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-6 mx-auto">	
			<section class="card">
				<header class="card-header">
					<h2 class="card-title">
					<img id="show1" src="images/icon-mail-note.gif" />Email Notification</h2>
				</header>


	<html:form action="/mailNotification?parameter=sendEmailThisVendor&requestFrom=searchPage" enctype="multipart/form-data" styleId="mailForm">
		<div id="successMsg">
			<html:messages id="msg" property="mail" message="true">
				<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
			</html:messages>
		</div>
		<div id="failureMsg">
			<html:messages id="msg" property="mailfailed" message="true">
				<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
			</html:messages>
		</div>
		<div class="form-box card-body">
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">To:</label>
				<div class="ctrl-col-full-wrapper">			
					
					<html:textarea property="to" styleClass="main-text-area" style="width:93%;" cols="70" rows="3" styleId="to"/>
				</div>
				<table>
					<tr>
						<td>
							<img style="cursor: pointer;" src="images/emaildistributionlist1.jpg" onclick="getVendorEmailIds();">
							<span style="color: #009900;"> 
								<a style="cursor: pointer;" onclick="getVendorEmailIds();">Vendor Email List</a>
							</span>
						</td>
					</tr>
				</table>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">CC:</label>
				<div class="col-sm-9 ctrl-col-full-wrapper">					
					<html:textarea property="cc" styleClass="main-text-area form-control" style="width:93%;" cols="70" rows="3" styleId="cc"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Attachment :</label>
				<div class="col-sm-9 ctrl-col-full-wrapper">
					<html:file property="attachment" styleId="attachment"/>
				</div>				
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Subject:</label>
				<div class="col-sm-9 ctrl-col-full-wrapper">
					<html:text property="subject" size="100" alt="" styleClass="main-text-box form-control" styleId="subject"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mail Content:</label>
				<div class="col-sm-9 ctrl-col-full-wrapper">
					<html:textarea property="message" styleId="message" styleClass="main-text-area form-control" rows="7" style="width:93%;height:45%;"/>
				</div>
			</div>
		</div>
			
		<footer class="mt-2 card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
					<html:submit value="Send Email" styleClass="btn" styleId="submit1" onclick="return validateEmailHistoryForm();"></html:submit>
					<html:reset value="Cancel" styleClass="btn" styleId="submit" onclick="backToSearchResultPage();"></html:reset>
				</div>
			</div>
		</footer>
	</html:form>	
</section>
<div id="vendorsEmailIdListDialog" title="Choose Vendor Email Id" style="display: none;">
	<logic:notEmpty name="nonPrimaryVendorUserList">
		<table id="allVendorUserListTable" class="main-table">
			<thead>
				<tr>
					<td class="header">Vendor Email Id</td>
					<td class="header">Select</td>
				</tr>
			</thead>
			<tbody>
				<logic:iterate name="nonPrimaryVendorUserList" id="allVendorUserListIterate">
					<tr>
						<td><bean:write name="allVendorUserListIterate" property="emailId" /></td>
						<td>
							<input type="checkbox" name="listVendorEmailId" value="<bean:write name="allVendorUserListIterate" property="emailId"/>">
						</td>
					</tr>
				</logic:iterate>
			</tbody>
		</table>
		<div class="btn-wrapper">
			<input type="button" value="Add" class="btn" onclick="setSelectedVendorEmailIds();">
		</div>
	</logic:notEmpty>	
	<logic:empty name="nonPrimaryVendorUserList">
		<p>There is no Vendor Email Id's Available...</p>
	</logic:empty>
</div>

</div>
</div>
</section>