<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<style>
	.row-wrapper .label-col-full-wrapper
	{
		float: left;
		line-height: 25px;
		padding: 0 0 0 2%;
		width: 12%;
	}
	
	.row-wrapper .ctrl-col-full-wrapper
	{
		padding: 0 0 0 0%;
	}
	
	.btnNew
	{
		float: left;
		background: #009900;
		height: 28px;
		padding: 0 10px;
		line-height: 28px;
		color: #fff;
		border: 0 none;
		cursor: pointer;
		position: relative;
		/*behavior: url(PIE.htc);*/
		-webkit-border-radius: 6px;
		-moz-border-radius: 6px;
		border-radius: 6px;
		display: block;
		font-family: Verdana, Geneva, sans-serif;
		margin: 0 5px 5px 0;
		text-transform: none;
	}
</style>

<script type="text/javascript">
	tinymce.init(
	{
		selector : "textarea#message",
		theme : "modern",
		statusbar : false,
		height : 250,
		width : 600,
		plugins : [
				"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
		toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
		menubar : false,
		file_browser_callback: AVMSFileBrowser,
		toolbar_items_size : 'medium'
	});
	
	function AVMSFileBrowser(field_name, url, type, win)
	{	
		var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;
	
		tinyMCE.activeEditor.windowManager.open(
		{
			file: avmsFileman,
			title: 'Email Template',
			width: 600, 
			height: 500,
			resizable: "yes",
			plugins: "media",
			inline: "yes",
			close_previous: "no"
		},
		{     window: win,     input: field_name    });
		
		return false; 
	}
	
	function backToMeetingInformationPage()
	{
		window.location = "vendornavigation.do?parameter=contactMeetingNavigation";
	}
	
	function validateEmailHistoryForm()
	{
		var toValue = $("#to").val();
		var ccValue = $("#cc").val();
		var subjectValue = $("#subject").val();

		if(toValue == '' && ccValue == '')
		{
			alert("Enter atleast one email id in TO/CC.");
			return false;
		}
		else if(subjectValue == 'undefined' || subjectValue == '')
		{
			alert("Enter Subject Message.");
			return false;
		}
		
		return true;
	}
	
	function getVendorEmailIds()
	{
		$(function()
		{
			$("#vendorsEmailIdListDialog").css(
			{
				'display' : 'block',
				'font-size' : 'inherit'
			});
			
			$("#vendorsEmailIdListDialog").dialog(
			{
				height : '400',
				modal : true,
				resizable : false,
				autoResize : false,
				show :
				{
					effect : "scale",
					duration : 1000
				},
				hide :
				{
					effect : "scale",
					duration : 1000
				}
			});
		});
	}
	
	function setSelectedVendorEmailIds()
	{
		var vendorEmailIdsList = [];
		
		$('input[name=listVendorEmailId]').each(function()
		{
			if ($(this).is(":checked"))
				vendorEmailIdsList.push($(this).val());
		});
		
		if(vendorEmailIdsList.length != 0 && vendorEmailIdsList != "")
		{
			for (index = 0; index < vendorEmailIdsList.length; index++)
			{
				if($("#to").val() != "" && $("#to").val() != 'undefined')
				{
					$("#to").val($("#to").val() + (vendorEmailIdsList[index] + ";"));
				}				
				else
				{
					$("#to").val(vendorEmailIdsList[index] + ";");
				}
			}
			
			$('input[name=listVendorEmailId]').each(function()
			{
				$(this).attr("checked", false);
			});
			
			$('#vendorsEmailIdListDialog').dialog('close');
		}		
	}
</script>

<div id="content-area">
	<div id="wizard" class="swMain">
		<jsp:include page="/WEB-INF/vendorpages/new vendor wizard pages/vendorWizardMenu.jsp"></jsp:include>
		<div class="stepContainer">
			<div id="step-1" class="content" style="display: block;">
				<h2 class="StepTitle">Email this Vendor</h2>
				<div class="panelCenter_1" style="padding-top: 1%;">
					<h3>Email Notification</h3>
					<div class="form-box">
						<html:form action="/mailNotification?parameter=sendEmailThisVendor&requestFrom=vendorProfilePage" enctype="multipart/form-data" styleId="mailForm">
							<div id="successMsg">
								<html:messages id="msg" property="mail" message="true">
									<span style="color: green;"><bean:write name="msg" /></span>
								</html:messages>
							</div>
							<div id="failureMsg">
								<html:messages id="msg" property="mailfailed" message="true">
									<span style="color: red;"><bean:write name="msg" /></span>
								</html:messages>
							</div>
							<div class="wrapper-full">
								<div class="row-wrapper form-group row">
									<div class="label-col-full-wrapper">To:</div>
									<div class="ctrl-col-full-wrapper">			
										<span style="font-weight: bolder;">Enter email addresses separate by semi ';'</span>
										<html:textarea property="to" styleClass="main-text-area" style="width:93%;" cols="70" rows="3" styleId="to"/>
									</div>
									<table>
										<tr>
											<td>
												<img style="cursor: pointer;" src="images/emaildistributionlist1.jpg" onclick="getVendorEmailIds();">
												<span style="color: #009900;"> 
													<a style="cursor: pointer;" onclick="getVendorEmailIds();">Vendor Email List</a>
												</span>
											</td>
										</tr>
									</table>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-full-wrapper">CC:</div>
									<div class="ctrl-col-full-wrapper">					
										<html:textarea property="cc" styleClass="main-text-area" style="width:93%;" cols="70" rows="3" styleId="cc"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-full-wrapper">Attachment :</div>
									<div class="ctrl-col-full-wrapper">
										<html:file property="attachment" styleId="attachment"/>
									</div>				
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-full-wrapper">Subject:</div>
									<div class="ctrl-col-full-wrapper">
										<html:text property="subject" size="100" alt="" styleClass="main-text-box" styleId="subject"/>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<div class="label-col-full-wrapper">Mail Content:</div>
									<div class="ctrl-col-full-wrapper">
										<html:textarea property="message" styleId="message" styleClass="main-text-area" rows="7" style="width:93%;height:45%;"/>
									</div>
								</div>
							</div>
							<div class="btn-wrapper">
								<html:submit value="Send Email" styleClass="btnNew" styleId="submit1" onclick="return validateEmailHistoryForm();"></html:submit>
								<html:reset value="Cancel" styleClass="btnNew" styleId="submit" onclick="backToMeetingInformationPage();"></html:reset>
							</div>
						</html:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="vendorsEmailIdListDialog" title="Choose Vendor Email Id" style="display: none;">
	<logic:notEmpty name="nonPrimaryVendorUserList">
		<table id="allVendorUserListTable" class="main-table">
			<thead>
				<tr>
					<td class="header">Vendor Email Id</td>
					<td class="header">Select</td>
				</tr>
			</thead>
			<tbody>
				<logic:iterate name="nonPrimaryVendorUserList" id="allVendorUserListIterate">
					<tr>
						<td><bean:write name="allVendorUserListIterate" property="emailId" /></td>
						<td>
							<input type="checkbox" name="listVendorEmailId" value="<bean:write name="allVendorUserListIterate" property="emailId"/>">
						</td>
					</tr>
				</logic:iterate>
			</tbody>
		</table>
		<div class="btn-wrapper">
			<input type="button" value="Add" class="btnNew" onclick="setSelectedVendorEmailIds();">
		</div>
	</logic:notEmpty>	
	<logic:empty name="nonPrimaryVendorUserList">
		<p>There is no Vendor Email Id's Available...</p>
	</logic:empty>
</div>