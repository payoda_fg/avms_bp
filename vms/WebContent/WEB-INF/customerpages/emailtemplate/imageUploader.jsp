<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script src="jquery/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/jquery.form.js"></script>
<script type="text/javascript" src="css/bp_style.css"></script>
<style>
<!--
.ui-multiselect ui-widget ui-state-default ui-corner-all {
	width: 250px;
}
-->

p.one {
    border-style: solid;
    border-width: thin;
} 
div.box{;
    padding: 1px;
    border: 1px solid #C65858 ;
    float: left;
    margin-bottom: 10px;
    padding-top: 10px;
}
div.btn{
  margin-top: 2px;
}

div.text{
    position:absolute;
    left:2px;
    width:30px;
}

div.img {
    margin: 2px;
    padding: 1px;
    border: 1px solid #009900;
    height: auto;
    width: auto;
    float: left;
    text-align: center;
    position: relative;
}	

div.img img {
    display: inline;
/*     margin: 5px; */
    border: 1px solid #ffffff;
}

div.img a:hover img {
    border: 1px solid #0000ff;
}

div.desc {
  text-align: center;
  font-weight: normal;
  width: 120px;
  margin: 50px;
  padding: 100px;
}
}
</style>
<style>
	.spancolor {
		color: green;
	}
</style>
<script>
$(document).ready(function() { 
	//Don't Remove this Bcoz its Needed for IE < 9 Versions to Take indexOf().
	if(!Array.prototype.indexOf) {
	    Array.prototype.indexOf = function(obj, start) {
	         for (var i = (start || 0), j = this.length; i < j; i++) {
	             if (this[i] === obj) { return i; }
	         }
	         return -1;
	    }
	}
});

$('#${field_name}', window.parent.document).val('');
var globalURL='<%=session.getAttribute("imageUrl")%>';

// Uploading New Image to server.
  function uploadImageFile()
  {
	  var imageFile=$('#uploadImage').val();
	  var imageTextBoxValue = $.trim($('#textboxid').val());
	  if(imageTextBoxValue!='' && imageFile!=''){
		  var options = { 
	            	url     : "emailtemplate.do?parameter=uploadEmailTemplateImage",
		            type    : "POST",
		            dataType: 'json',
		            success:function(data) {
	               		if(data.result == "success") {	
	               			$('#${field_name}', window.parent.document).val(data.globalimageurl+""+data.newImagePath);
							$('#textboxid').val('');
							$('#uploadImage').val('');
							alert("' "+imageTextBoxValue+" ' Saved Successfully.");
							top.tinymce.activeEditor.windowManager.close();
						} else {
							alert("'"+imageTextBoxValue+"' Not Saved, Due to Error.");
						}
	                }
	            };
		  $('#imageUploadForm').ajaxSubmit(options);
	   /* $.ajax({
			url : "emailtemplate.do?parameter=uploadEmailTemplateImage",
			type : "POST",
			async : false,
			beforeSend : function() {
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				alert("Hello World");
				$("#ajaxloader").hide();
				$('#${field_name}', window.parent.document).val('${emailImageUpload}');
				top.tinymce.activeEditor.windowManager.close();
			}
		}); */ 
	  }
	  else
	  {
		  alert("Browse one image file");
		  return false;
	  }
  }
		
	//Validating, uploading file is image or not?
	function uploadImageFileValidation(selectedFile)
	{	
		//Validation for File type
		var validExts = new Array(".png", ".jpg", ".gif" , ".jpeg");
	    var fileExt = selectedFile.value;
	    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
	    
	    if (validExts.indexOf(fileExt) < 0) 
	    {
	    	alert("You can import image files only [.png, .jpg , .jpeg & .gif].");
	      	$("#uploadImage").val('');
	      	$('#textboxid').val('');
	      	return false;
	    }
	    else
	    {
	    	$('#textboxid').val(selectedFile.value);
	    }			
	}

var checkBoxValue = new Array(40) ;
var globalInt=0;

// Image check and unchecking opertions
function onSelected(checkBox)
{
    navigator.appName + navigator.appCodeName;
	// If select add to array
	if(document.getElementById(checkBox.id).checked)
    {
	  checkBoxValue[globalInt]=checkBox.value;
	  globalInt=globalInt+1;
    }
	// If deselect delete data from array
	else
	{
		if(navigator.appName!="Microsoft Internet Explorer")
		  removeArrayValue(checkBoxValue,checkBox.value);
		else
		  checkBoxValue.remove(checkBox.value);
 		globalInt=globalInt-1;
	}
}


//Image delete opertion
function deleteSelectedImageFile()
{
	var thisImageSelectedAny_Where="no";
	if(globalInt>0){
		var i=0;
		var deletedImages="";
		for(i=0;i<globalInt;i++)
		{
				deletedImages=checkBoxValue[i]+"--"+deletedImages;
		}
		if(thisImageSelectedAny_Where == "no"){
			input_box = confirm("Are you sure you want to delete this Record?");
			if(input_box== true)
			{
			  $.ajax({
					url : "emailtemplate.do?parameter=deleteUploadedEmailTemplateImage&deletedImages="+deletedImages,
					type : "POST",
					async : false,
					success : function(data) {
						if(data.feedBack == "success"){
							alert("Some files are not deleted");
						}
						else{
						  alert("File(s) deleted successfully");
						  $('#${field_name}', window.parent.document).val('');
						}
						top.tinymce.activeEditor.windowManager.close();
					}
			  });
			}
		}
		else
		{
			alert("One of the image is selected in previous window");
			return false;
		}
	}
	else
	{
		alert("Select atleast one file");
		return false;
	}
}

// Insert selected image file to template..
function getPathSelectedImageFile()
{
	if(globalInt==1)
	{
		$('#${field_name}', window.parent.document).val(checkBoxValue[globalInt-1]);	
		top.tinymce.activeEditor.windowManager.close();
	}
	else
	{
		alert("Please select one image file");
		return false;
	}
}

// Removing Once check and then Unchecked Image Data
function removeArrayValue(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

Array.prototype.remove = function(value) {
	var idx = this.indexOf(value);
	if (idx != -1) {
	    return this.splice(idx, 1); // The second parameter is the number of elements to remove.
	}
	return false;
	}
</script>
<html:form styleId="imageUploadForm" enctype="multipart/form-data">
	<table>
		<tr>
			<bean:define id="imagePathId" property="imageFilePath"
				value="emailtemplateimages"></bean:define>
			<td>Select Image file to import:</td>
			<td><html:file property="importImageFile" styleId="uploadImage"
					onchange="uploadImageFileValidation(this);"></html:file></td>

		</tr>
		<tr>
			<td>Selected File Path:</td>
			<td><input type="text" disabled="disabled" id="textboxid"></td>
		</tr>
	</table>
	<div class="clear"></div>
	<div class="clear"></div>
	<p align="center"><input type="button" onclick="uploadImageFile()" value="Upload New Image" class="btn" /></p>
</html:form>
<p class="one">
<div class="page-title"><h3><b>Available Images</b></h3></div></p>
<div class="clear"></div>

<div class="form-box">
	<div class="box">
		<%-- <logic:iterate name="listUploadedFiles" id="listUploadedFiles"
			indexId="indexNum">
			<div class="img">
				<img src=<bean:write name="listUploadedFiles"/> height="100"
					width="100"><input type="checkbox" id='${indexNum}'
					onchange="onSelected(this)"
					value=<bean:write name="listUploadedFiles" /> /></img>
			</div>
		</logic:iterate> --%>
		<logic:iterate id="map" name="listUploadedImagesNames" indexId="indexNum">		
			<div class="img"><%-- <div class="text"><bean:write name="map" property="key" /></div> --%>
				<img src="<bean:write name='map' property='value' />" height="100" width="100">
					<input type="checkbox" id='${indexNum}' onchange="onSelected(this)" value="<bean:write name='map' property='value' />" />
				</img>
			</div>
		</logic:iterate>
	</div>
</div>
<div class="clear"></div>
	<div class="clear"></div>
<h3 align="center"><input type="button" onclick="getPathSelectedImageFile();" value="Insert" />
<input type="button" onclick="deleteSelectedImageFile();" value="Delete" />
