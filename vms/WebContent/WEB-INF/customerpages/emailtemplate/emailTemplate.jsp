<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" />

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>

<style>
	.ui-multiselect ui-widget ui-state-default ui-corner-all {
		width: 250px;
	}
	
	.spancolor {
		color: green;
	}
</style>

<script>
	jQuery.validator.addMethod("selectAtleastOne", function(value, element) {
		return this.optional(element) || ! /^[0]+$/.test(value);
	}, "Please Select Any Template Name.");
	
	$(document).ready(function() 
	{
		$("#formID").validate(
		{
			rules : 
			{
				emailTemplateName : {
					selectAtleastOne : true
				},
				emailTemplateMessage : {
					required : true
				},
				subject : {
					required :true					
				}
			}
		});

		//For multiselect 
		$("#emailtemplateDivision").multiselect({
			selectedText : "# of # selected"
		});
	});

	function clearfields() {
		window.location = "emailtemplate.do?parameter=showEmailTemplatePage";
	}
	
	tinymce.init(
	{
		mode : "exact",
		/* elements : "emailTemplateMessage", */
		selector : "textarea#emailTemplateMessage",
		theme : "modern",
		statusbar : false,
		height : 300,
		width : 650,
		plugins : [
				"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
		toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
		toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
		menubar : false,
 		file_browser_callback: AVMSFileBrowser,
		toolbar_items_size : 'medium'
	});
	
	function newTemplate()
	{
		window.location="emailtemplate.do?parameter=newTemplatePage";
	}
	
	function AVMSFileBrowser(field_name, url, type, win) 
	{
		var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;
				  
		tinyMCE.activeEditor.windowManager.open(
		{
			file: avmsFileman,
		    title: 'Email Template',
		    width: 600, 
		    height: 500,
		    resizable: "yes",
		    plugins: "media",
		    inline: "yes",
		    close_previous: "no"  
	  	}, {     window: win,     input: field_name    });
		  
		return false; 
	}
</script>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<div id="successMsg">
				<html:messages id="msg" property="template" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="divisionExist" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="parameterExist" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="templateExists" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Email Template" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="add">
			<section class="card">
				<header class="card-header">
					<h2 class="card-title pull-left">Email Templates</h2>
						<input type="button" value="New Email Template" class="btn btn-primary pull-right"  onclick="newTemplate()"></input>
				</header>
		
			
			<!--Row End Here-->
			<div class="form-box card-body">
				<html:form action="/emailtemplate?parameter=saveTemplates" styleId="formID">
					<html:hidden property="templateId" />
					<html:hidden property="templateCode"/>
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template</label>
							<div class="col-sm-6 ctrl-col-wrapper">
								<html:select styleId="emailTemplateName" styleClass="chosen-select form-control" property="emailTemplateName">
									<html:option value="0">-- Select Template Name --</html:option>
									<bean:size id="size" name="emailTemplateNames" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="emailTemplateNames" name="emailTemplateNames">
											<bean:define id="id" name="emailTemplateNames" property="id"></bean:define>
											<html:option value="${id}">
												<bean:write name="emailTemplateNames" property="description"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>
					
					
					<!-- while editing we are avoid the user to edit parameters -->
					<logic:notPresent name="emailTemplateForm" property="templateId">
						
							<div class="row-wrapper form-group row">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template Parameter(s)</label>
								<div class="col-sm-9 ctrl-col-wrapper">
									<span class="spancolor">In the Email content all the parameter should be preceded by # symbol (for example #emailid)</span>
									<html:textarea property="emailTemplateParameters" alt="Optional"
										styleId="emailTemplateParameters" styleClass="main-text-area form-control" rows="10" cols="45"/>
								</div>
							</div>
						
					</logic:notPresent>
					
					<!-- while editing we are avoid the user to edit parameters -->
					<logic:present name="emailTemplateForm" property="templateId">
						
							<div class="form-group row row-wrapper">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template Parameter(s)</label>
								<div class="col-sm-6 ctrl-col-wrapper">
									<span class="spancolor">In the Email content all the parameter should be preceded by # symbol (for example #emailid)</span>
									<html:textarea property="emailTemplateParameters" alt="Optional"
										styleId="emailTemplateParameters" styleClass="main-text-area form-control" rows="10" cols="45" readonly="true"/>
								</div>
							</div>
						
					</logic:present>
					
					
						<div class="form-group row row-wrapper">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Subject:</label>
							<div class="col-sm-6 ctrl-col-wrapper">
								<html:text property="subject" size="100" alt="" styleClass="main-text-box form-control" styleId="subject"></html:text>
							</div>
							<span class="error"><html:errors property="subject" /></span>
						</div>
				
					
					<logic:equal value="1" name="isDivisionStatus" property="isDivision">
						<div class="form-group row row-wrapper">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Applicable Divisions</label>
							<div class="col-sm-6 ctrl-col-wrapper">
								<html:select property="emailtemplateDivision" multiple="true" styleClass="form-control" name="emailTemplateForm" styleId="emailtemplateDivision">
									<bean:size id="size" name="customerDivisions" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="customerDivision" name="customerDivisions">
											<bean:define id="id" name="customerDivision" property="id"></bean:define>
												<html:option value="${id}"><bean:write name="customerDivision" property="divisionname"/></html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
							<span class="error"><html:errors property="customerDivision"></html:errors></span>
						</div>
					</logic:equal>
					
				
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template Message</label>
						<div class="col-sm-9 ctrl-col-wrapper">
							<html:textarea property="emailTemplateMessage" alt="" styleId="emailTemplateMessage" styleClass="main-text-area form-control" />
						</div>
					</div>
				
					
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
								<logic:present name="emailTemplateForm" property="templateId">
									<html:submit value="Update" styleClass="btn" styleId="submit"></html:submit>
								</logic:present>
								<logic:notPresent name="emailTemplateForm" property="templateId">
									<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit"></html:submit>
								</logic:notPresent>
								<html:reset value="Clear" styleClass="btn btn-default" onclick="clearfields();"></html:reset>
							</div>
						</div>
					</footer>
				</html:form>
			</div>
		</section>
		</logic:match>

		<!--    For Email Template Editing (When Email Template did not the Add privilege from Administrator ) -->
		<logic:match value="0" name="privilege" property="add">
			<logic:notPresent name="emailTemplateForm" property="templateId">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to add new Email Template</h3>
				</div>
			</logic:notPresent>
			<logic:present name="emailTemplateForm" property="templateId">
				<logic:match value="1" name="privilege" property="modify">
					<section class="card">
						<header class="card-header">
							<h2 class="card-title pull-left">Email Templates</h2>
						</header>	
					
					<!--Row End Here-->
					<div class="form-box card-body">
						<html:form action="/emailtemplate?parameter=saveTemplates" styleId="formID">
							<html:hidden property="templateId" />						
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template</label>
									<div class="colsm-6 ctrl-col-wrapper">
										<html:text property="emailTemplateName" alt="" styleId="emailTemplateName" styleClass="text-box form-control" />
									</div>
								</div>							
							
							<!-- while editing we are avoid the user to edit parameters -->
							<logic:notPresent name="emailTemplateForm" property="templateId">
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template Parameter(s)</label>
									<div class="col-sm-6 ctrl-col-wrapper">
										<span class="spancolor">In the Email content all the parameter should be preceded by # symbol (for example #emailid)</span>
										<html:textarea property="emailTemplateParameters"
											 alt="Optional"
											styleId="emailTemplateParameters" styleClass="main-text-area form-control"
											rows="10" cols="45"/>
									</div>
								</div>
								
							</logic:notPresent>
							<logic:present name="emailTemplateForm" property="templateId">
								
									<div class="row-wrapper form-group row">
										<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template Parameter(s)</label>
										<div class="col-sm-6 ctrl-col-wrapper">
											<span class="spancolor">In the Email content all the parameter should be preceded by # symbol (for example #emailid)</span>
											<html:textarea property="emailTemplateParameters"
												alt="Optional"
												styleId="emailTemplateParameters" styleClass="main-text-area form-control"
												rows="10" cols="45" readonly="false"/>
										</div>
									</div>
								
							</logic:present>
							
								<div class="row-wrapper form-group row">
										<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Subject:</label>
										<div class="col-sm-6 ctrl-col-wrapper">
											<html:text property="subject" size="100" alt=""
												styleClass="main-text-box" styleId="subject"></html:text>
										</div>
										<span class="error"><html:errors property="subject" /></span>
								</div>
							
							<logic:equal value="1" name="isDivisionStatus" property="isDivision">
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Applicable Divisions</label>
									<div class="col-sm-6 ctrl-col-wrapper">
										<html:select property="emailtemplateDivision" multiple="true"
											name="emailTemplateForm" styleClass="form-control" styleId="emailtemplateDivision">
											<bean:size id="size" name="customerDivisions" />
											<logic:greaterEqual value="0" name="size">
												<logic:iterate id="customerDivision" name="customerDivisions">
													<bean:define id="id" name="customerDivision" property="id"></bean:define>
													<html:option value="${id}"><bean:write name="customerDivision" property="divisionname"/></html:option>
												</logic:iterate>
											</logic:greaterEqual>
										</html:select>
									</div>
									<span class="error"><html:errors property="customerDivision"></html:errors> </span>
								</div>
							</logic:equal>
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Template Message</label>
									<div class="col-sm-6 ctrl-col-wrapper">
										<html:textarea property="emailTemplateMessage" alt=""
											styleId="emailTemplateMessage" styleClass="main-text-area form-control" />
									</div>
								</div>
							
							<footer class="mt-2 card-footer">
								<div class="row justify-content-end">
									<div class="wrapper-btn">
										<html:submit value="Update" styleClass="btn btn-primary" styleId="submit"></html:submit>
										<html:reset value="Clear" styleClass="btn btn-default" onclick="clearfields();"></html:reset>
									</div>
								</div>
							</footer>
						</html:form>
					</div>
					</section>
				</logic:match>
			</logic:present>
		</logic:match>
		<!--Row End Here-->

		<logic:match value="1" name="privilege" property="view">
			<header class="card-header">
				<h2 class="card-title pull-left">
				Available Templates</h2>
			</header>
			<div class="form-box card-body">
				<table class="main-table table table-bordered table-striped mb-0">
					<bean:size id="size" name="emailTemplatesDivisions" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td>Email Template Name</td>
								<td>Email Template Subject</td>
								<td>Applicable Divisions</td>
								<td>Actions</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="emailTemplatesDivisions" id="templatelist">
				            	<tr>
									<td><bean:write name="templatelist" property="emailTemplateName" /></td>
									<td><bean:write name="templatelist" property="emailTemplateSubject" /></td>
									<td><bean:write name="templatelist" property="customerDivision"/></td>
									<bean:define id="templateId" name="templatelist" property="emailTemplateId"></bean:define>
									<td align="center">
										<logic:iterate id="privilege1" name="privileges">
											<logic:match value="Email Template" name="privilege1" property="objectId.objectName">
												<logic:match value="1" name="privilege1" property="modify">
											 		<logic:equal value="S" property="templateType" name="templatelist">
														<html:link action="/emailtemplate.do?parameter=editTemplate"
																paramId="id" paramName="templateId" styleId="editTemplate">Edit</html:link>
													</logic:equal>
													<logic:equal value="G" property="templateType" name="templatelist">
														<html:link action="/emailtemplate.do?parameter=editNewTemplate"
																paramId="id" paramName="templateId" styleId="editTemplate">Edit</html:link>
													</logic:equal>					
												</logic:match>
												<logic:match value="1" name="privilege1" property="delete">
													<logic:equal value="G" property="templateType" name="templatelist">												   
														<html:link action="/emailtemplate.do?parameter=deleteTemplate"
																paramId="id" paramName="templateId"
																onclick="return confirm_delete();">Delete</html:link>														
													</logic:equal>
												</logic:match>
											</logic:match>
										</logic:iterate>
									</td>										
								</tr>
							</logic:iterate>
						</tbody>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
	                	No such records
	                </logic:equal>
				</table>
			</div>
		</logic:match>
	
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view email templates</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>