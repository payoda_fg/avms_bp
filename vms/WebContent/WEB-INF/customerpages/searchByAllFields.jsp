<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fg.vms.util.SearchFields"%>
<%
	pageContext.setAttribute("searchByOptions", SearchFields.values());
%>
<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />

<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<script type="text/javascript" src="jquery/ui/jquery.ui.autocomplete.js"></script>
<script type="text/javascript"
	src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>

<!-- Added for populate jqgrid. -->
<link rel="stylesheet" type="text/css"
	href="jquery/css/ui.multiselect.css" />
	<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
	<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />



<script type="text/javascript">
$(document).ready(function(){

	var value1;
	if("${searchVendorForm.companyName}"){
	$('#companyName').val("${searchVendorForm.companyName}");
	}
	
	if("${searchVendorForm.keyWord}"){
	$('#keyWord').val("${searchVendorForm.keyWord}");
	}
	
	if("${searchVendorForm.bpMarketSectorId}"){
	var bpMarketSectorId='${searchVendorForm.bpMarketSectorIdAsJsonString}';
	var bpMarketSectorIdAsJson=JSON.parse(bpMarketSectorId);
	$("#bpMarketSectorId").val(bpMarketSectorIdAsJson).trigger("chosen:updated");
	}
	if("${searchVendorForm.bpMarketSubSectorId}"){
		var bpMarketSubSectorId='${searchVendorForm.bpMarketSubSectorIdAsJsonString}';
		var bpMarketSubSectorIdAsJson=JSON.parse(bpMarketSubSectorId);
		$("#bpMarketSubSectorId").val(bpMarketSubSectorIdAsJson).trigger("chosen:updated");
		}
	if("${searchVendorForm.primeVendorStatus}"){
		var primeVendorStatus='${searchVendorForm.primeVendorStatusAsJsonString}';
		var primeVendorStatusAsJson=JSON.parse(primeVendorStatus);
		$("#primeVendorStatus").val(primeVendorStatusAsJson).trigger("chosen:updated");
		}
	if("${searchVendorForm.vendorCommodity}"){
	var vendorCommodity='${searchVendorForm.vendorCommodityAsJsonString}';
	var vendorCommodityAsJson=JSON.parse(vendorCommodity);
	$("#vendorCommodity").val(vendorCommodityAsJson).trigger("chosen:updated");
	}

	if("${searchVendorForm.businessAreaName}"){
	var businessAreaName='${searchVendorForm.businessAreaNameAsJsonString}';
	var businessAreaNameAsJson=JSON.parse(businessAreaName);
	$("#businessAreaName").val(businessAreaNameAsJson).trigger("chosen:updated");
	}

	if("${searchVendorForm.certificationType}"){
	var certificationType='${searchVendorForm.certificationTypeAsJsonString}';
	var certificationTypeAsJson=JSON.parse(certificationType);
	$("#certificationType").val(certificationTypeAsJson).trigger("chosen:updated");
	}

	if("${searchVendorForm.diverseCertificateNames}"){
	var diverseCertificateNames='${searchVendorForm.diverseCertificateNamesAsJsonString}';
	var diverseCertificateNamesAsJson=JSON.parse(diverseCertificateNames);
	$("#diverseCertificateNames").val(diverseCertificateNamesAsJson).trigger("chosen:updated");
	}

	if("${searchVendorForm.firstName}"){
	$('#firstName').val("${searchVendorForm.firstName}");
	}

	if("${searchVendorForm.lastName}"){
	$('#lastName').val("${searchVendorForm.lastName}");
	}

	if("${searchVendorForm.capabilities}"){
	$('#capabilities').val("${searchVendorForm.capabilities}");
	}

	if("${searchVendorForm.oilAndGas}"){
	$('#oilandgas').val(["${searchVendorForm.oilAndGas}"]).trigger("chosen:updated");
	}
	if("${searchVendorForm.onshoreOrOffshore}"){
		$('#onshoreoroffshore').val(["${searchVendorForm.onshoreOrOffshore}"]).trigger("chosen:updated");
		}
	if("${searchVendorForm.bpSegmentIds}"){
		var bpSegmentIds='${searchVendorForm.bpSegmentIdsAsJsonString}';
		var bpSegmentIdsAsJson=JSON.parse(bpSegmentIds);
		$("#bpSegmentIds").val(bpSegmentIdsAsJson).trigger("chosen:updated");
		}

	if("${searchVendorForm.dunsNumber}"){
		var tableID='searchTable';
		test(tableID,'DUNSNUMBER', 'dunsNumberId');
		$('#dunsNumberId').val("${searchVendorForm.dunsNumber}");
		}

	if("${searchVendorForm.taxId}"){
		var tableID='searchTable';
		test(tableID,'TAXID', 'taxId');
		$('#taxId').val("${searchVendorForm.taxId}");
		} 
	if("${searchVendorForm.noOfEmployee}"){
		var tableID='searchTable';
		test(tableID,'NUMBEROFEMPLOYEES', 'noOfEmployee');
		$('#noOfEmployee').val("${searchVendorForm.noOfEmployee}");
		}

	if("${searchVendorForm.companyType}"){
		var tableID='searchTable';
		test(tableID,'COMPANYTYPE', 'companyType');
		$('#companyType').val("${searchVendorForm.companyType}");
		} 

	if("${searchVendorForm.address}"){
		var tableID='searchTable';
		test(tableID,'ADDRESS', 'address');
		$('#address').val("${searchVendorForm.address}");
		} 

	if("${searchVendorForm.province}"){
		var tableID='searchTable';
		test(tableID,'PROVINCE', 'province');
		$('#province').val("${searchVendorForm.province}");
		}

	if("${searchVendorForm.region}"){
		var tableID='searchTable';
		test(tableID,'REGION', 'region');
		$('#region').val("${searchVendorForm.region}");
		}

	if("${searchVendorForm.zipCode}"){
		var tableID='searchTable';
		test(tableID,'ZIPCODE', 'zipCode');
		$('#zipCode').val("${searchVendorForm.zipCode}");
		} 

	if("${searchVendorForm.mobile}"){
		var tableID='searchTable';
		test(tableID,'MOBILE', 'mobile');
		$('#mobile').val("${searchVendorForm.mobile}");
		} 

	if("${searchVendorForm.phone}"){
		var tableID='searchTable';
		test(tableID,'PHONE', 'phone');
		$('#phone').val("${searchVendorForm.phone}");
		} 

	if("${searchVendorForm.fax}"){
		var tableID='searchTable';
		test(tableID,'FAX', 'fax');
		$('#fax').val("${searchVendorForm.fax}");
		} 

	if("${searchVendorForm.webSiteUrl}"){
		var tableID='searchTable';
		test(tableID,'WEBSITEURL', 'webSiteUrl');
		$('#webSiteUrl').val("${searchVendorForm.webSiteUrl}");
		} 

	if("${searchVendorForm.email}"){
		var tableID='searchTable';
		test(tableID,'EMAIL', 'email');
		$('#email').val("${searchVendorForm.email}");
		} 

	if("${searchVendorForm.minRevenueRange}"){
		var tableID='searchTable';
		test(tableID,'REVENUERANGE', null);
		$('#minRevenueRange').val("${searchVendorForm.minRevenueRange}");
		$('#maxRevenueRange').val("${searchVendorForm.maxRevenueRange}");
		} 

	if("${searchVendorForm.yearRelationId}"){
		var tableID='searchTable';
		test(tableID,'YEAROFESTABLISHMENT', null);
		$('#yearRelationId').val(["${searchVendorForm.yearRelationId}"]).trigger("chosen:updated");
		$('#establishedYear').val("${searchVendorForm.year}");
		}

	if("${searchVendorForm.rfiRfpNote}"){
		var tableID='searchTable';
		test(tableID,'RFIRFPNOTE', 'rfiRfpNoteId');
		$('#rfiRfpNoteId').val("${searchVendorForm.rfiRfpNote}");
		}

	if("${searchVendorForm.naicsDesc}"){
		var tableID='searchTable';
		test(tableID,'NAICSDESC', null);
		$('#naicsDescription').val("${searchVendorForm.naicsDesc}");
		}

	if("${searchVendorForm.naicCode}"){
		var tableID='searchTable';
		test(tableID,'NAICSCODE', null);
		$('#naicsCode').val("${searchVendorForm.naicCode}");
		}

	if("${searchVendorForm.certificateAgencyIds}"){
		var certificateAgencyIds='${searchVendorForm.certificateAgencyIdsAsJsonString}';
		var certificateAgencyAsJson=JSON.parse(certificateAgencyIds);
		var tableID='searchTable';
		test(tableID,'CERTIFYINGAGENCY', null);
		$("#certifyAgencies").val(certificateAgencyAsJson).trigger("chosen:updated");
		}

	if("${searchVendorForm.certificationNumber}"){
		var tableID='searchTable';
		test(tableID,'CERTIFICATION', 'certificationNumber');
		$('#certificationNumber').val("${searchVendorForm.certificationNumber}");
		}

	if("${searchVendorForm.effectiveDate}"){
		var tableID='searchTable';
		test(tableID,'EFFECTIVEDATE', 'effectiveDate');
		$('#effectiveDate').val("${searchVendorForm.effectiveDate}");
		}

	if("${searchVendorForm.expireDate}"){
		var tableID='searchTable';
		test(tableID,'EXPIREDATE', 'expireDate');
		$('#expireDate').val("${searchVendorForm.expireDate}");
		}

	if("${searchVendorForm.expireDate}"){
		var tableID='searchTable';
		test(tableID,'EXPIREDATE', 'expireDate');
		$('#expireDate').val("${searchVendorForm.expireDate}");
		}

	if("${searchVendorForm.diversClassificationNotes}"){
		var tableID='searchTable';
		test(tableID,'DIVERSITYCLASSIFICATIONNOTES', 'diversClassificationNotes');
		$('#diversClassificationNotes').val("${searchVendorForm.diversClassificationNotes}");
		}

	if("${searchVendorForm.title}"){
		var tableID='searchTable';
		test(tableID,'DESIGNATION', 'titleId');
		$('#titleId').val("${searchVendorForm.title}");
		}

	if("${searchVendorForm.contactPhone}"){
		var tableID='searchTable';
		test(tableID,'CONTACTPHONE', 'contactPhone');
		$('#contactPhone').val("${searchVendorForm.contactPhone}");
		}

	if("${searchVendorForm.contactMobile}"){
		var tableID='searchTable';
		test(tableID,'CONTACTMOBILE', 'contactMobile');
		$('#contactMobile').val("${searchVendorForm.contactMobile}");
		}

	if("${searchVendorForm.contactFax}"){
		var tableID='searchTable';
		test(tableID,'CONTACTFAX', 'contactFax');
		$('#contactFax').val("${searchVendorForm.contactFax}");
		}

	if("${searchVendorForm.contactEmail}"){
		var tableID='searchTable';
		test(tableID,'CONTACTEMAIL', 'contactEmail');
		$('#contactEmail').val("${searchVendorForm.contactEmail}");
		}

	if("${searchVendorForm.contactFirstName}"){
		var tableID='searchTable';
		test(tableID,'CONTACTFIRSTNAME', 'contactFirstName');
		$('#contactFirstName').val("${searchVendorForm.contactFirstName}");
		}

	if("${searchVendorForm.contactLastName}"){
		var tableID='searchTable';
		test(tableID,'CONTACTLASTNAME', 'contactLastName');
		$('#contactLastName').val("${searchVendorForm.contactLastName}");
		}

	if("${searchVendorForm.contactDate}"){
		var tableID='searchTable';
		test(tableID,'CONTACTDATE', null);
		$('#contactDate').val("${searchVendorForm.contactDate}");
		}

	if("${searchVendorForm.contactState}"){
		var tableID='searchTable';
		test(tableID,'CONTACTSTATE', null);
		$('#contactStateId').val(["${searchVendorForm.contactState}"]).trigger("chosen:updated");
		}

	if("${searchVendorForm.contactEvent}"){
		var tableID='searchTable';
		test(tableID,'CONTACTEVENT', 'contactEvent');
		$('#contactEvent').val("${searchVendorForm.contactEvent}");
		}
		
	$.validator.addMethod("greaterThan",

			function (value, element, param) {
			  var $min = $(param);
			  if (this.settings.onfocusout) {
			    $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
			      $(element).valid();
			    });
			  }
			  return parseInt(value) > parseInt($min.val());
			}, "Max must be greater than min");

			$('#searchform').validate({
			  rules: {
				  maxRevenueRange: {
			      greaterThan: '#minRevenueRange'
			    }
			  }
			});
	});
	//Gets Country (United States) and Sets Related States in State Button...
	$(document).ready(function() 
	{
		<logic:present name="userDetails" property="workflowConfiguration">
			<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
				$.ajax(
				{
					url : 'state.do?method=getStateByDefaultCountry&random=' + Math.random(),
					type : "POST",
					async : true,
					success : function(data) 
					{
						$("#state").find('option').remove().end().append(data);
						$('#state').trigger("chosen:updated");
					}
				});
			</logic:equal>
		</logic:present>
		
		callAutoCompleteForNaicsCode();
		callAutoCompleteForNaicsDesc();
	});
	
	$(document).keypress(function(e) {
		if (e.which == 13) {
			$(e.target).blur();
			e.preventDefault();
			return searchData();
		}
	});

	function initDatePicker(){
		$(".datePicker").datepicker({
			changeYear : true,
			dateFormat : 'mm/dd/yy'
		});
	}
	
	function recallPreviousSearch(){
		
			$("#dialog1").css({
				"display" : "block"
			});
			$("#dialog1").dialog({
				minWidth : 900,
				modal : true
			});
	}
	
	function previousSearch(searchId,searchType) {
	if(searchType =='V' || searchType == 'VP' ){
		$.ajax({
			url : "viewVendors.do?method=searchByPreviousFilterData&searchPageType="
					+"searchPrimeVendorProgress&searchId="+searchId,
			type : 'POST',
			data : $("#searchform").serialize(),
			async : false,
			dataType : "json",
			beforeSend : function() {
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				$("#ajaxloader").hide();
				window.location = "viewVendors.do?method=searchByStatusRestult";
			}
		});	
	}
	else
		searchByPreviousSavedColumns(searchId);
	}
	
	function confirm_delete1() {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			return true;
		} else {
			return false;
		}
	}
	function deleteSearchFilter(searchId) {
		var result=confirm_delete1();
	if(result){
			$.ajax({
			url : "viewVendors.do?method=deleteSearchFilter&searchType=V"
					+"&searchId="+searchId,
			type : 'POST',
			async : false,
			dataType : "json",
			beforeSend : function() {
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#dialog1").dialog("close");
				if(data.result=="success")
				{
					alert("Search Filter Successfully Deleted");
					window.location.reload(true);
				}
				else{
						alert("Sorry Transaction Failed");
					}
			}
		});		
	}
	else
		return false;
	}
	function searchData() {
		$.ajax({
			url : "viewVendors.do?method=vendorSearch&searchPageType=searchPrimeVendorProgress",
			type : 'POST',
			data : $("#searchform").serialize(),
			async : false,
			dataType : "json",
			beforeSend : function() {
				$("#ajaxloader").show(); //show image loading
			},
			success : function(data) {
				$("#ajaxloader").hide();
				window.location = "viewVendors.do?method=searchByStatusRestult";
			}
		});				 
	}
	
	function customizeColumns() {

		$.ajax({
			url : "viewVendors.do?method=vendorSearch&searchPageType=searchPrimeVendorProgress&type=customize" ,
			data : $("#searchform").serialize(),
			type : "POST",
			async : true,
			success : function(data) {
				window.location = "viewVendors.do?method=showDynamicColumnsSelectPage";
				
				}
		});
	}
	function assignValue(value, id) {
		$('#' + id).val(value.value);
	}
	
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != '') {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : true,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).trigger("chosen:updated");
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">&nbsp;&nbsp;</option>');
			$('#' + id).trigger("chosen:updated");
		}
	}

	function getVendorStatus(status) {
		if (status != '' && status == 'P') {
			$("#primeRow").css('visibility', 'visible');
		} else {
			$("#primeRow").css('visibility', 'hidden');
		}
	}
	function dynamicExport() {

		$("#dialog2").css({
			"display" : "block"
		});
		$("#dialog2").dialog({
			minWidth : 300,
			modal : true
		});

	}
	function backToSearch() {
		window.location = "viewVendorsStatus.do?method=showVendorSearch&searchType=V";
	}
</script>

<style>
.main-list-box {
	padding: 0% 1%;
}

.main-table1 td.header {
	background: none repeat scroll 0 0 #009900;
	color: #FFFFFF;
}

.main-table1 td {
	border: 1px solid #E9EAEA;
	padding: 5px;
}

tr:nth-child(2n+1) {
	background: none repeat scroll 0 0;
}

#searchTable td {
	border: 0px solid #E9EAEA;
	text-align: left !important;
}

.chosen-disabled {
	opacity: 1 !important;
}
#myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 50%;
  font-size: 12px;
  padding: 8px 10px 6px 12px;
  border:1px solid #09893c;
  margin-bottom: 10px;
}
</style>

<section role="main" class="content-body card-margin mt-5">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">
					<img id="show1" src="images/VendorSearch.gif" /> Search Vendor All Statuses </h2>
					<input type="button" value="Recall Previous Search"
				class="btn btn-primary pull-right" onclick="recallPreviousSearch();">
			</header>
			
<logic:iterate id="privilege" name="privileges">
	<logic:match value="View Vendors" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div id="select" style="display: block;" class="form-box">
				<html:form action="/viewVendors.do?method=vendorSearch"
					styleId="searchform">
				<div class="form-box card-body">
					<table class="main-table1 table table-bordered table-striped mb-0" id="searchTable">
						<tr>
							<td style="font-weight:bold; color: #000; text-align: center !important;">Fields</td>
							<td style="font-weight:bold; color: #000; text-align: center !important;">Values</td>
							<!-- <td style="float: right;"><input type="button" value="Search" class="btn"
				onclick="searchData();"></td> -->
						</tr>

						<tr>
							<td>Vendor Status</td>
							<td><input type="hidden" value="PRIME" name="vendorType">
								<select name="primeVendorStatus" id="primeVendorStatus"
								class="chosen-select fields form-control"
								onchange="showStatusDesc(this.value)">
									<option value="0">All Status</option>
									<logic:present name="vendorStatusList">
										<bean:size id="size" name="vendorStatusList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="primeVendorStatus" name="vendorStatusList">
												<bean:define id="name" name="primeVendorStatus"
													property="id"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="primeVendorStatus" property="statusname" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><div id="statusDescArea"
									style="display: none;">
									<textarea name="statusdesc" id="statusdesc" readonly="readonly"
										style="font-weight: bolder; height: 60px; color: #009900;"></textarea>
								</div></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields1"
								onchange="changeInputType(2,this);" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" selected="selected"
											disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE">NAICS Code</option>
										<option value="NAICSDESC">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME">First Name</option>
										<option value="CONTACTLASTNAME">Last Name</option>
										<option value="CONTACTDATE">Contact Date</option>
										<option value="CONTACTSTATE">Contact State</option>
										<option value="CONTACTEVENT">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE">Oil and
											Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								id="companyName" name="searchFieldsValue" style="width:96%;"/></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields1" value="LEGALCOMPANYNAME" /></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields2"
								onchange="changeInputType(3,this);" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" selected="selected">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box" style="width:96%;" id="keyWord"
								name="searchFieldsValue" /></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields2" value="KEYWORD" /></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields3"
								onchange="changeInputType(4,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" selected="selected">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="bpMarketSectorHiddenId" />
								<select name="bpMarketSectorId" class="chosen-select-deselect form-control"
								id="bpMarketSectorId" multiple="multiple"
								data-placeholder=" "
								onchange="assignValue(this,'bpMarketSectorHiddenId');">
									<option value=""></option>
									<logic:present name="marketSectorsList">
										<bean:size id="size" name="marketSectorsList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="marketSector" name="marketSectorsList">
												<bean:define id="sectorId" name="marketSector" property="id" />
												<option value="<%=sectorId.toString()%>"><bean:write
														name="marketSector" property="sectorDescription" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields3" value="BPMARKETSECTOR" /></td>
						</tr>
						<tr>
                        	<td><select name="searchFields" id="searchFields33"
								onchange="changeInputType(16,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
											<option value="BPMARKETSUBSECTOR" selected="selected">BP
											Market Sub Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
<!-- 							<td><select name="searchFields" id="searchFields15"
								onchange="changeInputType(16,this)" class="chosen-select fields"
								disabled="disabled">
								<option value="BPMARKETSUBSECTOR" selected="selected">BP Market Sub Sector</option>
								</select>
								</td> -->
								<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="bpMarketSubSectorHiddenId" />
								<select name="bpMarketSubSectorId" id="bpMarketSubSectorId"
								class="chosen-select-deselect" style="width: 350px;"
								multiple="multiple" data-placeholder=" "
								onchange="assignValue(this,'bpMarketSubSectorHiddenId');">
									<option value=""></option>
									<logic:present name="marketSubSectorsList">
										<bean:size id="size" name="marketSubSectorsList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="marketSubSector" name="marketSubSectorsList">
												<bean:define id="subsectorId" name="marketSubSector" property="id" />
												<option value="<%=subsectorId.toString()%>"><bean:write
														name="marketSubSector" property="categoryDescription" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
								<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields33" value="BPMARKETSUBSECTOR" /></td>
						</tr>
						<%-- <tr>
							<td><select name="searchFields" id="searchFields4"
								onchange="changeInputType(5,this)" class="chosen-select fields">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<option value="COMPANYCODE" disabled="disabled">Company Code</option>
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE"  disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option>
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" selected="selected" disabled="disabled">Commodity Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE" disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="commodityDescription1" />
								<select name="commodityDescription"
								class="chosen-select-deselect" style="width: 350px;"
								multiple="multiple"
								data-placeholder="Choose a commodity description... "
								onchange="assignValue(this,'commodityDescription1');">
									<option value=""></option>
									<logic:present name="commodityDescription">
										<bean:size id="size" name="commodityDescription" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="commodity" name="commodityDescription">
												<bean:define id="category" name="commodity"
													property="commodityCategory"></bean:define>
												<bean:define id="description" name="commodity"
													property="commodityDescription"></bean:define>
												<optgroup label="<%=category.toString()%>">

													<%
														String[] descriptions = description
																							.toString().split("\\|");
																					if (descriptions != null
																							&& descriptions.length != 0) {
																						for (int i = 0; i < descriptions.length; i++) {
																							String[] parts = descriptions[i]
																									.split(":");
																							String descriptionId = parts[0];
																							String descriptionName = parts[1];
													%>
													<option value="<%=descriptionId%>">
														<%=descriptionName%>
													</option>
													<%
														}
																					}
													%>

												</optgroup>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
						</tr>
 --%>
						<tr>
							<td><select name="searchFields" id="searchFields4"
								onchange="changeInputType(5,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" selected="selected">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box "
								name="searchFieldsValue" value="" id="vendorCommodity1" /> <select
								name="vendorCommodity" id="vendorCommodity"
								class="chosen-select-deselect" 
								multiple="multiple" data-placeholder=" "
								onchange="assignValue(this,'vendorCommodity1');">
									<option value=""></option>
									<logic:present name="vendorCommodity">
										<bean:size id="size" name="vendorCommodity" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="commodity" name="vendorCommodity">
												<bean:define id="subSectorDesc" name="commodity"
													property="subSectorDescription" />
												<bean:define id="commodityDesc" name="commodity"
													property="commodityDescription" />
												<optgroup label="<%=subSectorDesc.toString()%>">
													<%
														String[] descriptions = commodityDesc
																							.toString().split("\\|");
																					if (descriptions != null
																							&& descriptions.length != 0) {
																						for (int i = 0; i < descriptions.length; i++) {
																							String[] parts = descriptions[i]
																									.split(":");
																							String commodityDescId = parts[0];
																							String commodityDescName = parts[1];
													%>
													<option value="<%=commodityDescId%>"><%=commodityDescName%></option>
													<%
														}
																					}
													%>
												</optgroup>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields4" value="VENDORCOMMODITY" /></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields5"
								onchange="changeInputType(6,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" selected="selected"
											disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>

							<td><input type="hidden" class="main-text-box form-control"
								name="searchFieldsValue" value="" id="businessArea1" /> 
								<select
								name="businessAreaName" id="businessAreaName"
								class="chosen-select-deselect" multiple="true" tabindex="102" data-placeholder=" "
								onchange="assignValue(this,'businessArea1');">
									<option value=""></option>
									<logic:present name="businessAreas">
										<bean:size id="size" name="businessAreas" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="businessArea" name="businessAreas">
												<bean:define id="bGroup" name="businessArea"
													property="businessGroup"></bean:define>
												<bean:define id="barea" name="businessArea"
													property="serviceArea"></bean:define>
												<optgroup label="<%=bGroup.toString()%>">

													<%
														String[] areas = barea.toString()
																							.split("\\|");
																					if (areas != null && areas.length != 0) {
																						for (int i = 0; i < areas.length; i++) {
																							String[] parts = areas[i]
																									.split("-");
																							String areaId = parts[0];
																							String areaName = parts[1];
													%>
													<option value="<%=areaId%>">
														<%=areaName%>
													</option>
													<%
														}
																					}
													%>

												</optgroup>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields5" value="BUSINESSAREA" /></td>
						</tr>

						<tr>
							<logic:present name="userDetails"
								property="workflowConfiguration">
								<logic:equal value="1" name="userDetails"
									property="workflowConfiguration.internationalMode">
									<td><select name="searchFields" id="searchFields6"
										onchange="changeInputType(7,this)"
										class="chosen-select fields form-control" disabled="disabled">
											<option value=""></option>
											<optgroup label="Company Information">
												<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
													Company Name</option>
												<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
												<option value="DUNSNUMBER">Duns Number</option>
												<option value="TAXID">Tax ID</option>
												<option value="NUMBEROFEMPLOYEES">Number of
													Employees</option>
												<option value="COMPANYTYPE">Company Type</option>
												<!-- <option value="YEARESTABLISHED">Year Established</option> -->
												<option value="ADDRESS">Address</option>
												<option value="CITY">City</option>
												<option value="STATE" disabled="disabled">State</option>
												<option value="PROVINCE">Province</option>
												<option value="REGION">Region</option>
												<option value="COUNTRY" selected="selected">Country</option>
												<option value="ZIPCODE">Zip Code</option>
												<option value="MOBILE">Mobile</option>
												<option value="PHONE">Phone</option>
												<option value="FAX">Fax</option>
												<option value="WEBSITEURL">Website URL</option>
												<option value="EMAIL">Email</option>
												<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
													Classification</option>
												<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
													Type</option>
												<option value="BPSEGMENT" disabled="disabled">BP
													Segment</option>
												<option value="REVENUERANGE">Revenue Range</option>
												<option value="YEAROFESTABLISHMENT">Year Of
													Establishment</option>
												<option value="RFIRFPNOTE">RFI/RFP Note</option>
											</optgroup>
											<!-- <optgroup label="NAICS">
												<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
												<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
											</optgroup> -->
											<optgroup label="Diverse Classification">
												<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
												<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
													Classification</option>
												<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
													Agency</option>
												<option value="CERTIFICATION">Certification #</option>
												<option value="EFFECTIVEDATE">Effective Date</option>
												<option value="EXPIREDATE">Expire Date</option>
												<option value="DIVERSESUPPLIER" disabled="disabled">Diverse
													Supplier</option>
												<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
													Classification Notes</option>
											</optgroup>
											<optgroup label="Contact Information">
												<option value="FIRSTNAME" disabled="disabled">Contact
													First Name</option>
												<option value="LASTNAME" disabled="disabled">Contact
													Last Name</option>
												<option value="DESIGNATION">Title</option>
												<option value="CONTACTPHONE">Contact Phone</option>
												<option value="CONTACTMOBILE">Contact Mobile</option>
												<option value="CONTACTFAX">Contact Fax</option>
												<option value="CONTACTEMAIL">Contact Email</option>
											</optgroup>
											<optgroup label="Business Area">
												<option value="BUSINESSAREA" disabled="disabled">Business
													Area</option>
												<option value="VENDORCOMMODITY" disabled="disabled">BP
													Commodity</option>
												<option value="BPMARKETSECTOR" disabled="disabled">BP
													Market Sector</option>
											</optgroup>
											<!-- <optgroup label="Commodity Description">
												<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
											</optgroup> -->
											<optgroup label="Service Area">
												<option value="KEYWORD" disabled="disabled">Keyword</option>
											</optgroup>
											<optgroup label="Contact Meeting Information">
												<option value="CONTACTFIRSTNAME" disabled="disabled">First
													Name</option>
												<option value="CONTACTLASTNAME" disabled="disabled">Last
													Name</option>
												<option value="CONTACTDATE" disabled="disabled">Contact
													Date</option>
												<option value="CONTACTSTATE" disabled="disabled">Contact
													State</option>
												<option value="CONTACTEVENT" disabled="disabled">Contact
													Event</option>
											</optgroup>
											<optgroup label="Business Biography and Safety">
												<option value="OILANDGASINDUSTRYEXPERIENCE"
													disabled="disabled">Oil and Gas Industry
													Experience</option>
											</optgroup>
									</select></td>

									<td><input type="hidden" class="main-text-box"
										name="searchFieldsValue" value="" id='selectCountries1' /><select
										name="selectCountries" class="chosen-select form-control"
										data-placeholder=" "
										onchange="changestate(this,'state');assignValue(this,'selectCountries1');">
											<option value=""></option>
											<logic:present name="countries">
												<bean:size id="size" name="countries" />
												<logic:greaterEqual value="0" name="size">
													<logic:iterate id="country" name="countries">
														<bean:define id="name" name="country" property="id"></bean:define>
														<option value="<%=name.toString()%>"><bean:write
																name="country" property="countryname" /></option>
													</logic:iterate>
												</logic:greaterEqual>
											</logic:present>
											<!-- <option value="1"> United States </option> -->
									</select></td>
									<td><input type="hidden" name="searchFields"
										id="hiddenSearchFields6" value="COUNTRY" /></td>
								</logic:equal>
							</logic:present>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields7"
								onchange="changeInputType(8,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" selected="selected" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><select name="searchFieldsValue" class="chosen-select form-control"
								id="state">
									<option value="">&nbsp;&nbsp;</option>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields7" value="STATE" /></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields8"
								onchange="changeInputType(9,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" selected="selected"
											disabled="disabled">Certification Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="certificationType1" /> <select
								name="certificationType" id="certificationType"
								class="chosen-select-deselect form-control" 
								multiple="multiple" data-placeholder=" "
								onchange="assignValue(this,'certificationType1');">
									<option value=""></option>
									<logic:present name="certificationTypes">
										<bean:size id="size" name="certificationTypes" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="certificationType"
												name="certificationTypes">
												<bean:define id="name" name="certificationType"
													property="certficateTypeId"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="certificationType"
														property="certficateTypeDescription" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields8" value="CERTIFICATIONTYPE" /></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields9"
								onchange="changeInputType(10,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" selected="selected"
											disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="diverseCertificate1" /> <select
								name="diverseCertificateNames" id="diverseCertificateNames"
								class="chosen-select-deselect form-control" 
								multiple="multiple" data-placeholder=" "
								onchange="assignValue(this,'diverseCertificate1');">
									<option value=""></option>
									<logic:present name="certificateTypes">
										<bean:size id="size" name="certificateTypes" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="certificate" name="certificateTypes">
												<bean:define id="id" name="certificate" property="id"></bean:define>
												<option value="<%=id.toString()%>"><bean:write
														name="certificate" property="certificateName"></bean:write></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields9" value="DIVERSECLASSIFICATION" /></td>
						</tr>

						<!-- <tr>
							<td><select name="searchFields" id="searchFields10"
								onchange="changeInputType(11,this)" class="chosen-select fields">
								<option value=""></option>
								<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<option value="COMPANYCODE" disabled="disabled">Company Code</option>
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<option value="DIVERSESUPPLIER" selected="selected" disabled="disabled">Diverse Supplier</option>
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA"  disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE" disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><select name="searchFieldsValue"
								class="chosen-select-deselect" style="width: 250px;"
								data-placeholder="Choose a diverse supplier...">
									<option value=""></option>
									<option value="1">Diverse</option>
									<option value="0">Non Diverse</option>
							</select></td>
						</tr> -->

						<tr>
							<td><select name="searchFields" id="searchFields10"
								onchange="changeInputType(11,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" selected="selected"
											disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box" style="width:96%;"
								name="searchFieldsValue" id="firstName" /></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields10" value="FIRSTNAME" /></td>
						</tr>

						<tr>
							<td><select name="searchFields" id="searchFields11"
								onchange="changeInputType(12,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" selected="selected"
											disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box" style="width:96%;"
								name="searchFieldsValue" id="lastName" /></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields11" value="LASTNAME" /></td>
						</tr>

						<!-- <tr>
							<td><select name="searchFields" id="searchFields12"
								onchange="changeInputType(13,this)" class="chosen-select fields">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<option value="COMPANYCODE" selected="selected" disabled="disabled">Company Code</option>
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option>
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE" disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" /></td>
						</tr> -->

						<tr>
							<td><select name="searchFields" id="searchFields12"
								onchange="changeInputType(13,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" selected="selected">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box" style="width:96%;"
								name="searchFieldsValue" id="capabilities" /></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields12" value="CAPABILITIES" /></td>
						</tr>

						<%-- <tr>
							<td><select name="searchFields" id="searchFields13"
								onchange="changeInputType(14,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" selected="selected" disabled="disabled">NAICS Code</option>
										<!-- <option value="NAICSDESC" disabled="disabled">NAICS Desc</option> -->
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE" disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsCode1" />
							<input  type="text" class="main-text-box"  id="naicsCode"  name="naicsCode"
								placeholder=" "
								onchange="assignValue(this,'naicsCode1');" /></td>
								<td><input type="hidden"name="searchFields" id="hiddenSearchFields13" value="NAICSCODE" /></td>
							
							<!-- <td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsCode1" /> <select 
								name="naicsCode" class="chosen-select-deselect"
								style="width: 350px;" multiple="multiple"
								data-placeholder="Choose a naics code..."
								onchange="assignValue(this,'naicsCode1');">
									<option value=""></option>
									<logic:present name="naicsCodeList">
										<bean:size id="size" name="naicsCodeList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="naicsCodes" name="naicsCodeList">
												<bean:define id="name" name="naicsCodes"
													property="naicsCode"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="naicsCodes" property="naicsCode" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td> -->
						</tr> --%>

						<%-- <tr>
							<td><select name="searchFields" id="searchFields13"
								onchange="changeInputType(14,this)" class="chosen-select fields">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" selected="selected" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE" disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsDescription1" /> 
							<input type="text" class="main-text-box" id="naicsDescription" name="naicsDescription"
								placeholder="Choose a naics Description..."
								onchange="assignValue(this,'naicsDescription1');" /></td>
							
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsDescription1" /> <select
								name="naicsDescription" class="chosen-select-deselect"
								style="width: 350px;" multiple="multiple"
								data-placeholder="Choose a naics Description..."
								onchange="assignValue(this,'naicsDescription1');">
									<option value=""></option>
									<logic:present name="naicsCodeList">
										<bean:size id="size" name="naicsCodeList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="naicsCodes" name="naicsCodeList">
												<bean:define id="name" name="naicsCodes"
													property="naicsDescription"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="naicsCodes" property="naicsDescription" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
						</tr> --%>

						<tr>
							<td><select name="searchFields" id="searchFields13"
								onchange="changeInputType(14,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE"
											selected="selected">Oil and Gas Industry Experience</option>
									</optgroup>
							</select></td>
							<td><select name="searchFieldsValue" id="oilandgas"
								class="chosen-select-deselect form-control" 
								data-placeholder=" ">
									<option value=""></option>
									<option value="1">Yes</option>
									<option value="0">No</option>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields13" value="OILANDGASINDUSTRYEXPERIENCE" /></td>
						</tr>
						<tr>
							<td><select name="searchFields" id="searchFields16"
								onchange="changeInputType(17,this)" class="chosen-select fields form-control"
								disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<!-- <option value="YEARESTABLISHED">Year Established</option> -->
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
											Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
											Type</option>
										<option value="BPSEGMENT" disabled="disabled">BP
											Segment</option>
										<option value="REVENUERANGE">Revenue Range</option>
										<option value="YEAROFESTABLISHMENT">Year Of
											Establishment</option>
										<option value="RFIRFPNOTE">RFI/RFP Note</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
											Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact
											First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact
											Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business
											Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP
											Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP
											Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label="Contact Meeting Information">
										<option value="CONTACTFIRSTNAME" disabled="disabled">First
											Name</option>
										<option value="CONTACTLASTNAME" disabled="disabled">Last
											Name</option>
										<option value="CONTACTDATE" disabled="disabled">Contact
											Date</option>
										<option value="CONTACTSTATE" disabled="disabled">Contact
											State</option>
										<option value="CONTACTEVENT" disabled="disabled">Contact
											Event</option>
									</optgroup>
									<optgroup label="Vendor Business Biography and Safety">
										<option value="ONSHOREOROFFSHORE"
											selected="selected">Primarily  OnShore or OffShore  </option>
									</optgroup>
							</select></td>
							<td><select name="searchFieldsValue" id="onshoreoroffshore"
								class="chosen-select-deselect form-control" data-placeholder=" ">
									<option value=""></option>
									<option value="both">Both</option>
									<option value="Onshore">Onshore</option>
									<option value="Offshore">Offshore</option>
							</select></td>
							<td><input type="hidden" name="searchFields"
								id="hiddenSearchFields13" value="PRIMARILYONSHOREOROFFSHORE" /></td>
						</tr>
						<tr>
							<logic:present name="userDetails"
								property="workflowConfiguration">
								<logic:equal value="1" name="userDetails"
									property="workflowConfiguration.bpSegment">
									<logic:iterate id="privilege1" name="privileges">
										<logic:equal value="BP Segment" name="privilege1"
											property="objectId.objectName">
											<logic:equal value="1" name="privilege1" property="view">
												<td><select name="searchFields" id="searchFields14"
													onchange="changeInputType(15,this)"
													class="chosen-select fields form-control" disabled="disabled">
														<option value=""></option>
														<optgroup label="Company Information">
															<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
																Company Name</option>
															<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
															<option value="DUNSNUMBER">Duns Number</option>
															<option value="TAXID">Tax ID</option>
															<option value="NUMBEROFEMPLOYEES">Number of
																Employees</option>
															<option value="COMPANYTYPE">Company Type</option>
															<!-- <option value="YEARESTABLISHED">Year Established</option> -->
															<option value="ADDRESS">Address</option>
															<option value="CITY">City</option>
															<option value="STATE" disabled="disabled">State</option>
															<option value="PROVINCE">Province</option>
															<option value="REGION">Region</option>
															<option value="COUNTRY" disabled="disabled">Country</option>
															<option value="ZIPCODE">Zip Code</option>
															<option value="MOBILE">Mobile</option>
															<option value="PHONE">Phone</option>
															<option value="FAX">Fax</option>
															<option value="WEBSITEURL">Website URL</option>
															<option value="EMAIL">Email</option>
															<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor
																Classification</option>
															<option value="CERTIFICATIONTYPE" disabled="disabled">Certification
																Type</option>
															<option value="BPSEGMENT" selected="selected">BP
																Segment</option>
															<option value="REVENUERANGE">Revenue Range</option>
															<option value="YEAROFESTABLISHMENT">Year Of
																Establishment</option>
															<option value="RFIRFPNOTE">RFI/RFP Note</option>
														</optgroup>
														<!-- <optgroup label="NAICS">
															<option value="NAICSCODE"  disabled="disabled">NAICS Code</option>
															<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
														</optgroup> -->
														<optgroup label="Diverse Classification">
															<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
															<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
																Classification</option>
															<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying
																Agency</option>
															<option value="CERTIFICATION">Certification #</option>
															<option value="EFFECTIVEDATE">Effective Date</option>
															<option value="EXPIREDATE">Expire Date</option>
															<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
															<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
																Classification Notes</option>
														</optgroup>
														<optgroup label="Contact Information">
															<option value="FIRSTNAME" disabled="disabled">Contact
																First Name</option>
															<option value="LASTNAME" disabled="disabled">Contact
																Last Name</option>
															<option value="DESIGNATION">Title</option>
															<option value="CONTACTPHONE">Contact Phone</option>
															<option value="CONTACTMOBILE">Contact Mobile</option>
															<option value="CONTACTFAX">Contact Fax</option>
															<option value="CONTACTEMAIL">Contact Email</option>
														</optgroup>
														<optgroup label="Business Area">
															<option value="BUSINESSAREA" disabled="disabled">Business
																Area</option>
															<option value="VENDORCOMMODITY" disabled="disabled">BP
																Commodity</option>
															<option value="BPMARKETSECTOR" disabled="disabled">BP
																Market Sector</option>
														</optgroup>
														<!-- <optgroup label="Commodity Description">
															<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
														</optgroup> -->
														<optgroup label="Service Area">
															<option value="KEYWORD" disabled="disabled">Keyword</option>
														</optgroup>
														<optgroup label="Contact Meeting Information">
															<option value="CONTACTFIRSTNAME" disabled="disabled">First
																Name</option>
															<option value="CONTACTLASTNAME" disabled="disabled">Last
																Name</option>
															<option value="CONTACTDATE" disabled="disabled">Contact
																Date</option>
															<option value="CONTACTSTATE" disabled="disabled">Contact
																State</option>
															<option value="CONTACTEVENT" disabled="disabled">Contact
																Event</option>
														</optgroup>
														<optgroup label="Business Biography and Safety">
															<option value="OILANDGASINDUSTRYEXPERIENCE"
																disabled="disabled">Oil and Gas Industry
																Experience</option>
														</optgroup>
												</select></td>
												<td><input type="hidden" class="main-text-box"
													name="searchFieldsValue" value="" id="bpSegmentHiddenId" />
													<select name="bpSegmentIds" id="bpSegmentIds" class="form-control chosen-select-deselect"
													multiple="multiple"
													data-placeholder=" "
													onchange="assignValue(this,'bpSegmentHiddenId');">
														<option value=""></option>
														<logic:present name="bpSegmentSearchList">
															<bean:size id="size" name="bpSegmentSearchList" />
															<logic:greaterEqual value="0" name="size">
																<logic:iterate id="bpSegmentSearchListId"
																	name="bpSegmentSearchList">
																	<bean:define id="name" name="bpSegmentSearchListId"
																		property="id"></bean:define>
																	<option value="<%=name
																	.toString()%>"><bean:write
																			name="bpSegmentSearchListId" property="segmentName" /></option>
																</logic:iterate>
															</logic:greaterEqual>
														</logic:present>
												</select></td>
												<td><input type="hidden" name="searchFields"
													id="hiddenSearchFields14" value="BPSEGMENT" /></td>
											</logic:equal>
										</logic:equal>
									</logic:iterate>
								</logic:equal>
							</logic:present>
						</tr>
					</table>

					<div style="display: none;">
						<select id="diverseCertificateNames">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="certificateTypes">
								<bean:size id="size" name="certificateTypes" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="certificate" name="certificateTypes">
										<bean:define id="id" name="certificate" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="certificate" property="certificateName"></bean:write></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select name="country" id="country">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="countries">
								<bean:size id="size" name="countries" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="country" name="countries">
										<bean:define id="name" name="country" property="id"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="country" property="countryname" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="certifyingAgencies">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="certifyingAgencies">
								<bean:size id="size" name="certifyingAgencies" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="agency" name="certifyingAgencies">
										<bean:define id="id" name="agency" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="agency" property="agencyName" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="businessAreaName">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="businessAreas">
								<bean:size id="size" name="businessAreas" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="businessArea" name="businessAreas">
										<bean:define id="bGroup" name="businessArea"
											property="businessGroup"></bean:define>
										<bean:define id="barea" name="businessArea"
											property="serviceArea"></bean:define>
										<optgroup label="<%=bGroup.toString()%>">

											<%
												String[] areas = barea.toString()
																					.split("\\|");
																			if (areas != null && areas.length != 0) {
																				for (int i = 0; i < areas.length; i++) {
																					String[] parts = areas[i]
																							.split("-");
																					String areaId = parts[0];
																					String areaName = parts[1];
											%>
											<option value="<%=areaId%>">
												<%=areaName%>
											</option>
											<%
												}
																			}
											%>
										</optgroup>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select><select id="commodityDescription">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="commodityDescription">
								<bean:size id="size" name="commodityDescription" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="commodity" name="commodityDescription">
										<bean:define id="category" name="commodity"
											property="commodityCategory"></bean:define>
										<bean:define id="description" name="commodity"
											property="commodityDescription"></bean:define>
										<optgroup label="<%=category.toString()%>">
											<%
												String[] descriptions = description
																					.toString().split("\\|");
																			if (descriptions != null
																					&& descriptions.length != 0) {
																				for (int i = 0; i < descriptions.length; i++) {
																					String[] parts = descriptions[i]
																							.split(":");
																					String descriptionId = parts[0];
																					String descriptionName = parts[1];
											%>
											<option value="<%=descriptionId%>">
												<%=descriptionName%>
											</option>
											<%
												}
																			}
											%>

										</optgroup>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <input type="text" class="main-text-box form-control" id="naicsCode"
							name="naicsCode" placeholder=" "
							onchange="assignValue(this,'naicsCode1');" /> <input type="text"
							class="main-text-box" id="naicsDescription"
							name="naicsDescription" placeholder=" "
							onchange="assignValue(this,'naicsDescription1');" />
						<%-- <select id="naicsCode">
							<option value=""></option>
							<logic:present name="naicsCodeList">
								<bean:size id="size" name="naicsCodeList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="naicsCodes" name="naicsCodeList">
										<bean:define id="name" name="naicsCodes" property="naicsCode"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="naicsCodes" property="naicsCode" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select><select id="naicsDescription">
							<option value=""></option>
							<logic:present name="naicsCodeList">
								<bean:size id="size" name="naicsCodeList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="naicsCodes" name="naicsCodeList">
										<bean:define id="name" name="naicsCodes"
											property="naicsDescription"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="naicsCodes" property="naicsDescription" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> --%>
						<select id="vendorCommodity">
							<option value=""></option>
							<logic:present name="vendorCommodity">
								<bean:size id="size" name="vendorCommodity" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="commodity" name="vendorCommodity">
										<bean:define id="subSectorDesc" name="commodity"
											property="subSectorDescription" />
										<bean:define id="commodityDesc" name="commodity"
											property="commodityDescription" />
										<optgroup label="<%=subSectorDesc.toString()%>">
											<%
												String[] descriptions = commodityDesc
																					.toString().split("\\|");
																			if (descriptions != null
																					&& descriptions.length != 0) {
																				for (int i = 0; i < descriptions.length; i++) {
																					String[] parts = descriptions[i]
																							.split(":");
																					String commodityDescId = parts[0];
																					String commodityDescName = parts[1];
											%>
											<option value="<%=commodityDescId%>"><%=commodityDescName%></option>
											<%
												}
																			}
											%>
										</optgroup>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="certificationType">
							<option value=""></option>
							<logic:present name="certificationTypes">
								<bean:size id="size" name="certificationTypes" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="certificationType" name="certificationTypes">
										<bean:define id="name" name="certificationType"
											property="certficateTypeId"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="certificationType"
												property="certficateTypeDescription" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select><select id="vendorclassification1">
							<option value=""></option>
							<option value="1">Prime</option>
							<option value="0">Non-Prime</option>
						</select><select id="diversesupplier1">
							<option value=""></option>
							<option value="1">Diverse</option>
							<option value="0">Non Diverse</option>
						</select> <select id="contactStates">
							<option value=""></option>
							<logic:present name="contactStates">
								<bean:size id="size" name="contactStates" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="contactState" name="contactStates">
										<bean:define id="name" name="contactState" property="id" />
										<option value="<%=name.toString()%>"><bean:write
												name="contactState" property="statename" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="bpSegmentIds">
							<option value=""></option>
							<logic:present name="bpSegmentSearchList">
								<bean:size id="size1" name="bpSegmentSearchList" />
								<logic:greaterEqual value="0" name="size1">
									<logic:iterate id="bpSegmentSearchListId"
										name="bpSegmentSearchList">
										<bean:define id="name" name="bpSegmentSearchListId"
											property="id" />
										<option value="<%=name.toString()%>"><bean:write
												name="bpSegmentSearchListId" property="segmentName" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="oilAndGasIndustryExperience">
							<option value=""></option>
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select> <select id="bpMarketSectorId">
							<option value=""></option>
							<logic:present name="marketSectorsList">
								<bean:size id="size" name="marketSectorsList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="marketSector" name="marketSectorsList">
										<bean:define id="sectorId" name="marketSector" property="id" />
										<option value="<%=sectorId.toString()%>"><bean:write
												name="marketSector" property="sectorDescription" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> </select> <select id="yearRelationId">
							<option value=""></option>
							<option value=">">></option>
							<option value="=">=</option>
							<option value="<"><</option>
							<option value=">">>=</option>
							<option value="<"><=</option>
						</select> <input type="text" id="establishedYear" />
					</div>
					<div class="clear"></div>
					<INPUT id="cmd1" type="button" value="Other Search Criteria"
						class="btn btn-primary btn-other-search mt-2" onclick="addRow('searchTable')" />
					<div class="clear"></div>
					<footer class="card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
								<input type="button" value="Search" class="btn btn-tertiary"
								onclick="searchData();"> <input type="button"
								value="Customize Columns" class="btn btn-primary"
								onclick="customizeColumns();">
							</div>
						</div>
					</footer>
					</div>
				</html:form>
				
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>


<div id="dialog1" title="Choose Previous Search Filters"
	style="display: none;">
	<logic:present name="previousSearchList">
	<input type="text" id="myInput" onkeyup="myFunctionSearch()" placeholder="Search for names.." title="Type in a name">
		<table class="main-table table table-bordered table-striped mb-0" id="table1">
			<bean:size id="size" name="previousSearchList" />
			<logic:greaterThan value="0" name="size">
				<thead>
					<tr>
						<td class="" onclick="sortTable(0)">Search Name</td>
						<td class="" onclick="sortTable(1)">Search Date</td>
						<td class="">Criteria</td>
						<td class="">Actions</td>
					</tr>
				</thead>
				<fmt:setLocale value="en_US" />
				<logic:iterate name="previousSearchList" id="previousSearch">
					<bean:define id="previousSearchId" name="previousSearch"
						property="id"></bean:define>
					<bean:define id="searchType" name="previousSearch"
						property="searchType"></bean:define>
					<tbody>
						<tr>
							<td><html:link href="Javascript:void ( 0 ) ;"
									onclick="previousSearch(${previousSearchId},'${searchType}');">
									<bean:write name="previousSearch" property="searchName" />
								</html:link></td>
							<td><bean:write name="previousSearch" property="searchDate"
									format="yyyy-MM-dd hh:mma" /></td>
							<td><bean:write name="previousSearch" property="criteria" /></td>

							<td><html:link href="Javascript:void ( 0 ) ;"
									onclick="deleteSearchFilter(${previousSearchId});"> Delete </html:link>/<a
								href="viewVendorsStatus.do?method=editSearchFilter&searchType=V&searchId=${previousSearchId}">Edit</a>
							</td>
						</tr>
					</tbody>
				</logic:iterate>
			</logic:greaterThan>
			<logic:equal value="0" name="size">
					No such Records
			</logic:equal>
		</table>
	</logic:present>
	<logic:notPresent name="previousSearchList">
		<p>There is no record(s) available.</p>
	</logic:notPresent>
</div>
<div id="dynamicresultHeading" class="page-title" style="display: none;">
	<img src="images/icon-registration.png" />&nbsp;&nbsp;Following are
	your Vendors
	<div id="dynamicExport" style="display: none; float: right;">
		<input type="button" class="btn" value="Export" id="export"
			onclick="dynamicExport();">
	</div>
</div>
<div id="grid_container" style="width: 100%; overflow: auto;">
	<table id="gridtable">
		<tr>
			<td />
		</tr>
	</table>
	<div id="pager"></div>
</div>

<div id="backToSearch" class="wrapper-btn" style="display: none;">
	<input type="button" class="btn" value="Back To Search"
		id="buttonClass" onclick="backToSearch();">
</div>

<div id="dialog2" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="1" id="excel" /></td>
			<td><label for="excel"><img id="excelExport"
					src="images/excel_export.png" />Excel</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="2" id="csv" /> <input type="hidden" name="fileName"
				id="fileName" value="SupplierReport" /></td>
			<td><label for="csv"><img id="csvExport"
					src="images/csv_export.png" />CSV</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="3" id="pdf" /></td>
			<td><label for="pdf"><img id="pdfExport"
					src="images/pdf_export.png" />PDF</label></td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails"
				property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="exportBtn btn-primary btn"
				onclick="exportHelperForDynamic('gridtable','<%=logoPath%>');">
		</logic:present>
	</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
	function addRow(tableID) {

		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[1].cells.length;
		for ( var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[2].cells[i].innerHTML;

			switch (newcell.childNodes[1].type) {
			case "text":
				newcell.innerHTML = " <input class='main-text-box ' name='searchFieldsValue'/>";
				newcell.childNodes[0].value = "";
				break;
			case "select-one":
				newcell.innerHTML = '<select name="searchFields" id="searchFields'
						+ rowCount
						+ '" class="chosen-select fields" onchange="changeInputType('
						+ rowCount
						+ ',this)" >'
						+ $('#searchFields1').html()
						+ '</select>';
				newcell.childNodes[0].selectedIndex = 0;
				$("#searchFields" + rowCount).chosen({
					width : "96%"
				});
				var values = $(".chosen-select").map(function() {
					return this.value;
				}).get();

				$('select[id=searchFields' + rowCount + '] option')
						.each(
								function() {
									for ( var index = 0; index < values.length; index++)
										if ($(this).val() == values[index])
											$(this)
													.attr('disabled',
															'disabled');
								});
				break;
			}
			$("#searchFields" + rowCount).trigger("chosen:updated");
			//$("#searchFields"+rowCount).trigger("liszt:updated");
		}

	}

	function test(tableID, param, paramId){
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[1].cells.length;
		for ( var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			if(i != 2){
			newcell.innerHTML = table.rows[2].cells[i].innerHTML;
			}
			if(i == 2){
				var table = document.getElementById('searchTable');
				var value=$("#searchFields" + rowCount);
				var v=$("#searchFields" + rowCount).val(param).trigger("chosen:updated");
				if (param == 'DIVERSESUPPLIER') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="diversesupplier1" name="searchFieldsValue" value="">'
							+'<select name="searchFieldsValue" style="width:265px;" class="chosen-single chosen-default"' +
						 	'data-placeholder=" "  onchange="assignValue(this,\'diversesupplier1\');"   >'
							+$('#diversesupplier1').html()+'</select>';
//				 			chosenConfig();
							$("#diversesupplier1").next().chosen();
							$("#diversesupplier1").next().trigger("chosen:updated");


				} else if (param == 'VENDORCLASSIFICATION') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="vendorclassification1" name="searchFieldsValue" value="">'
					+'<select name="searchFieldsValue" style="width:265px;" class="chosen-single chosen-default"'+
				 	'data-placeholder=" "  onchange="assignValue(this,\'vendorclassification1\');"  >'
				 	+$('#vendorclassification1').html()+'</select>';
//		 			chosenConfig();
					$("#vendorclassification1").next().chosen();
					$("#vendorclassification1").next().trigger("chosen:updated");
				} else if (param == 'COUNTRY') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="selectCountries1" name="searchFieldsValue" value="">'
							+ '<select name="selectCountries" style="width:265px;" data-placeholder=" " '
							//+ 'multiple class="chosen-select-deselect" onchange="assignValue(this,\'selectCountries1\');"> '
							+ 'class="chosen-select" onchange="changestate(this,\'state\');assignValue(this,\'selectCountries1\');"> '
							//+ ' class="chosen-select-deselect" disabled="disabled"> <option value="1"> United States</option> '
							+ $('#country').html() + '</select>';
		 			//chosenConfig();			
					$("#selectCountries1").next().chosen();
					$("#selectCountries1").next().trigger("chosen:updated");
				} else if (param == 'DIVERSECLASSIFICATION') {

					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="diverseCertificate1"	name="searchFieldsValue" value=""/>'
							+ '<select name="diverseCertificateNames" style="width:265px;" multiple="multiple" class="chosen-select-deselect"'
							+ ' data-placeholder=" " onchange="assignValue(this,\'diverseCertificate1\');">'
							+ $('#diverseCertificateNames').html() + '</select>';
//		 			chosenConfig();
					$("#diverseCertificate1").next().chosen();
					$("#diverseCertificate1").next().trigger("chosen:updated");

				} else if (param == 'CERTIFYINGAGENCY') {

					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="certifyAgencies1"/>'
							+ '<select name="certifyAgencies" id="certifyAgencies" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'certifyAgencies1\');">'
							+ $('#certifyingAgencies').html() + '</select>';
//		 			chosenConfig();
					$("#certifyAgencies1").next().chosen();
					$("#certifyAgencies1").next().trigger("chosen:updated");

				} else if (param == 'BUSINESSAREA') {

					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="businessArea1"/>'
							+ '<select name="businessAreaName" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'businessArea1\');">'
							+ $('#businessAreaName').html() + '</select>';
//		 			chosenConfig();
					$("#businessArea1").next().chosen();
					$("#businessArea1").next().trigger("chosen:updated");

				} else if (param == 'COMMODITYDESCRIPTION') {

					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="commodityDescription1"/>'
							+ '<select name="commodityDescription" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'commodityDescription1\');">'
							+ $('#commodityDescription').html() + '</select>';
//		 			chosenConfig();
					$("#commodityDescription1").next().chosen();
					$("#commodityDescription1").next().trigger("chosen:updated");

				} else if (param == 'NAICSCODE') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsCode1"/>'
						+ '<input type="text" id="naicsCode" class="main-text-box" name="naicsCode" placeholder=" " onchange="assignValue(this,\'naicsCode1\');" />'
						+ $('#naicsCode').html();
					callAutoCompleteForNaicsCode();

				} 
				else if (param == 'BPSEGMENT') {				
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpSegmentId"/>'
							+ '<select name="bpSegmentIds" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'bpSegmentId\');">'
							+ $('#bpSegmentIds').html() + '</select>';
//		 			chosenConfig();
					$("#bpSegmentId").next().chosen();
					$("#bpSegmentId").next().trigger("chosen:updated");
				}

				else if (param == 'REVENUERANGE') {				
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="revenueRangeId"/><input a type="text" class="number main-text-box" style="width: 100px;" placeholder="Min Range"	name="minRevenueRange" value="" id="minRevenueRange"/>'
						+'<input type="text" class="number main-text-box" style="width: 100px;margin-left: 36px;" placeholder="Max Range"	name="maxRevenueRange" value="" id="maxRevenueRange"/>';
//		 			$("#revenueRangeId").next().chosen();
					$("#revenueRangeId").trigger("chosen:updated");
				}

				else if (param == 'YEAROFESTABLISHMENT') {				
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="yearId"/>'
					+'<input type="text" class="main-text-box" placeholder="Year" style="width:100px;margin-left: 152px;" name="establishedYear" value="" id="establishedYear"/>'
					+'<select name="yearRelationId" id="yearRelationId" class="select2-container select2-container-multi chosen-select-deselect" style="width:100px;margin-left: -265px;">'
					+ $('#yearRelationId').html()+'</select>';
						
// 					$("#yearRelationId").next().chosen();
// 					$("#yearRelationId").next().trigger("chosen:updated");
				}

				else if (param == 'NAICSDESC') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsDescription1"/>'
						+ '<input type="text" id="naicsDescription" class="main-text-box" name="naicsDescription" placeholder=" " onchange="assignValue(this,\'naicsDescription1\');" />'
						+ $('#naicsDescription').html();
					callAutoCompleteForNaicsDesc();

				} else if (param == 'CERTIFICATIONTYPE') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="certificatioType1"/>'
							+ '<select name="certificationType" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'certificatioType1\');">'
							+ $('#certificationType').html() + '</select>';
//		 			chosenConfig();
					$("#certificatioType1").next().chosen();
					$("#certificatioType1").next().trigger("chosen:updated");
				} else if (param == 'VENDORCOMMODITY') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="vendorCommodity6"/>'
						+ '<select name="vendorCommodity" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
						+ 'data-placeholder=" " onchange="assignValue(this,\'vendorCommodity6\');">'
						+ $('#vendorCommodity').html() + '</select>';
//					chosenConfig();
					$("#vendorCommodity6").next().chosen();
					$("#vendorCommodity6").next().trigger("chosen:updated");
				} else if (param == 'CONTACTSTATE') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="contactStates" name="searchFieldsValue" value="">'
						+'<select name="contactState" id="contactStateId" style="width:265px;" class="chosen-single chosen-default"'+
						'data-placeholder=" "  onchange="assignValue(this,\'contactStates\');"  >'
						+$('#contactStates').html()+'</select>';
						$("#contactStates").next().chosen();
						$("#contactStates").next().trigger("chosen:updated");
				} else if (param == 'CONTACTDATE') {
					table.rows[rowCount].cells[1].innerHTML = "<input name='searchFieldsValue' class='main-text-box datePicker' id='contactDate' alt='Please click to select date' readonly='readonly'/>";
					initDatePicker();
				} else if (param == 'OILANDGASINDUSTRYEXPERIENCE') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="oilAndGasIndustryExperience1" name="searchFieldsValue" value="">'
					+'<select name="oilAndGasIndustryExperience" class="chosen-select-deselect" style="width: 265px;"'+
				 	'data-placeholder=" "  onchange="assignValue(this,\'oilAndGasIndustryExperience1\');"  >'
				 	+$('#oilAndGasIndustryExperience').html()+'</select>';
					//chosenConfig();
					$("#oilAndGasIndustryExperience1").next().chosen();
					$("#oilAndGasIndustryExperience1").next().trigger("chosen:updated");
				} else if (param == 'PRIMARILYONSHOREOROFFSHORE') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="primarilyOnshoreOrOffshore1" name="searchFieldsValue" value="">'
						+'<select name="primarilyOnshoreOrOffshore" class="chosen-select-deselect" style="width: 265px;"'+
					 	'data-placeholder=" "  onchange="assignValue(this,\'primarilyOnshoreOrOffshore1\');"  >'
					 	+$('#primarilyOnshoreOrOffshore').html()+'</select>';
						//chosenConfig();
						$("#primarilyOnshoreOrOffshore1").next().chosen();
						$("#primarilyOnshoreOrOffshore1").next().trigger("chosen:updated");
					} else if (param == 'BPMARKETSECTOR') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpMarketSectorDynamicId"/>'
						+ '<select name="bpMarketSectorId" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
						+ 'data-placeholder=" " onchange="assignValue(this,\'bpMarketSectorDynamicId\');">'
						+ $('#bpMarketSectorId').html() + '</select>';
					//chosenConfig();
					$("#bpMarketSectorDynamicId").next().chosen();
					$("#bpMarketSectorDynamicId").next().trigger("chosen:updated");
				} else if (param == 'BPMARKETSUBSECTOR') {
					table.rows[rowCount].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpMarketSubSectorDynamicId"/>'
						+ '<select name="bpMarketSubSubSectorId" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
						+ 'data-placeholder=" " onchange="assignValue(this,\'bpMarketSubSectorDynamicId\');">'
						+ $('#bpMarketSubSectorId').html() + '</select>';
					//chosenConfig();
					$("#bpMarketSubSectorDynamicId").next().chosen();
					$("#bpMarketSubSectorDynamicId").next().trigger("chosen:updated");
				}else {
					/* $("#searchFields" + rowCount).val(param); */
					table.rows[rowCount].cells[1].innerHTML = "<input class='main-text-box ' name='searchFieldsValue' id='"+paramId+"'/>";
				}
				}
		
			if(i == 0){
			switch (newcell.childNodes[1].type) {
			case "text":
				newcell.innerHTML = " <input class='main-text-box ' name='searchFieldsValue'/>";
				newcell.childNodes[0].value = "";
				break;
			case "select-one":
				newcell.innerHTML = '<select name="searchFields" id="searchFields'
						+ rowCount
						+ '" class="chosen-select fields" onchange="changeInputType('
						+ rowCount
						+ ',this)" >'
						+ $('#searchFields1').html()
						+ '</select>';
				newcell.childNodes[0].selectedIndex = 0;
				$("#searchFields" + rowCount).chosen({
					width : "96%"
				});
				var values = $(".chosen-select").map(function() {
					return this.value;
				}).get();
				$('select[id=searchFields' + rowCount + '] option')
						.each(
								function() {
									for ( var index = 0; index < values.length; index++)
										if ($(this).val() == values[index])
											$(this)
													.attr('disabled',
															'disabled');
								});
				break;
			}
			}
			
		}

		
	}
	/**
	 * used to change the input type when select the field
	 */
	function changeInputType(rowNo, value) {

		$('select[id=' + value.id + '] option').each(function() {
			$(this).removeAttr('disabled');
		});
		$("#" + value.id).trigger("chosen:updated");
		var values = $(".fields").map(function() {
			return this.value;
		}).get()
		$('select[id=' + value.id + '] option').each(function() {
			for ( var index = 0; index < values.length; index++)
				if ($(this).val() == values[index])
					$(this).attr('disabled', 'disabled');

		});
		$("#" + value.id).trigger("chosen:updated");
		var table = document.getElementById('searchTable');
		if (value.value == 'DIVERSESUPPLIER') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="diversesupplier1" name="searchFieldsValue" value="">'
					+'<select name="searchFieldsValue" style="width:265px;" class="chosen-single chosen-default"'+
					'data-placeholder=" "  onchange="assignValue(this,\'diversesupplier1\');"  >'
					+$('#diversesupplier1').html()+'</select>';
//		 			chosenConfig();
					$("#diversesupplier1").next().chosen();
					$("#diversesupplier1").next().trigger("chosen:updated");


		} else if (value.value == 'VENDORCLASSIFICATION') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="vendorclassification1" name="searchFieldsValue" value="">'
			+'<select name="searchFieldsValue" style="width:265px;" class="chosen-single chosen-default"'+
		 	'data-placeholder=" " onchange="assignValue(this,\'vendorclassification1\');"  >'
		 	+$('#vendorclassification1').html()+'</select>';
// 			chosenConfig();
			$("#vendorclassification1").next().chosen();
			$("#vendorclassification1").next().trigger("chosen:updated");
		} else if (value.value == 'COUNTRY') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="selectCountries1" name="searchFieldsValue" value="">'
					+ '<select name="selectCountries" style="width:265px;" data-placeholder=" " '
					//+ 'multiple class="chosen-select-deselect" onchange="assignValue(this,\'selectCountries1\');"> '
					+ 'class="chosen-select" onchange="changestate(this,\'state\');assignValue(this,\'selectCountries1\');"> '
					//+ ' class="chosen-select-deselect" disabled="disabled"> <option value="1"> United States</option> '
					+ $('#country').html() + '</select>';
			//chosenConfig();			
			$("#selectCountries1").next().chosen();
			$("#selectCountries1").next().trigger("chosen:updated");
		} else if (value.value == 'DIVERSECLASSIFICATION') {

			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="diverseCertificate1"	name="searchFieldsValue" value=""/>'
					+ '<select name="diverseCertificateNames" style="width:265px;" multiple="multiple" class="chosen-select-deselect"'
					+ ' data-placeholder=" " onchange="assignValue(this,\'diverseCertificate1\');">'
					+ $('#diverseCertificateNames').html() + '</select>';
// 			chosenConfig();
			$("#diverseCertificate1").next().chosen();
			$("#diverseCertificate1").next().trigger("chosen:updated");

		} else if (value.value == 'CERTIFYINGAGENCY') {

			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="certifyAgencies1"/>'
					+ '<select name="certifyAgencies" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'certifyAgencies1\');">'
					+ $('#certifyingAgencies').html() + '</select>';
// 			chosenConfig();
			$("#certifyAgencies1").next().chosen();
			$("#certifyAgencies1").next().trigger("chosen:updated");

		} else if (value.value == 'BUSINESSAREA') {

			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="businessArea1"/>'
					+ '<select name="businessAreaName" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'businessArea1\');">'
					+ $('#businessAreaName').html() + '</select>';
// 			chosenConfig();
			$("#businessArea1").next().chosen();
			$("#businessArea1").next().trigger("chosen:updated");

		} else if (value.value == 'COMMODITYDESCRIPTION') {

			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="commodityDescription1"/>'
					+ '<select name="commodityDescription" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'commodityDescription1\');">'
					+ $('#commodityDescription').html() + '</select>';
// 			chosenConfig();
			$("#commodityDescription1").next().chosen();
			$("#commodityDescription1").next().trigger("chosen:updated");

		} else if (value.value == 'NAICSCODE') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsCode1"/>'
				+ '<input type="text" id="naicsCode" class="main-text-box" name="naicsCode" placeholder=" " onchange="assignValue(this,\'naicsCode1\');" />'
				+ $('#naicsCode').html();
			callAutoCompleteForNaicsCode();
			
			/* table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsCode1"/>'
					+ '<select name="naicsCode" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'naicsCode1\');">'
					+ $('#naicsCode').html() + '</select>';
 			//chosenConfig();
			$("#naicsCode1").next().chosen();
			$("#naicsCode1").next().trigger("chosen:updated"); */			
		} 
		
		else if (value.value == 'BPSEGMENT') {				
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpSegmentId"/>'
					+ '<select name="bpSegmentIds" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'bpSegmentId\');">'
					+ $('#bpSegmentIds').html() + '</select>';
// 			chosenConfig();
			$("#bpSegmentId").next().chosen();
			$("#bpSegmentId").next().trigger("chosen:updated");
		}

		else if (value.value == 'REVENUERANGE') {				
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="revenueRangeId"/><input a type="text" class="number main-text-box" style="width: 100px;" placeholder="Min Range"	name="minRevenueRange" value="" id="minRevenueRange"/>'
				+'<input type="text" class="number main-text-box" style="width: 100px;margin-left: 36px;" placeholder="Max Range"	name="maxRevenueRange" value="" id="maxRevenueRange"/>'
		}

		else if (value.value == 'YEAROFESTABLISHMENT') {				
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="yearId"/>'
			+'<input type="text" class="main-text-box" placeholder="Year" style="width:100px;margin-left: 152px;" name="establishedYear" value="" id="establishedYear"/>'
			+'<select name="yearRelationId" class="select2-container select2-container-multi chosen-select-deselect" style="width:100px;margin-left: -265px;">'
			+ $('#yearRelationId').html()+'</select>';
				
			$("#yearRelationId").next().chosen();
			$("#yearRelationId").next().trigger("chosen:updated");
		}
		
		else if (value.value == 'NAICSDESC') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsDescription1"/>'
				+ '<input type="text" id="naicsDescription" class="main-text-box" name="naicsDescription" placeholder=" " onchange="assignValue(this,\'naicsDescription1\');" />'
				+ $('#naicsDescription').html();
			callAutoCompleteForNaicsDesc();
			
			/* table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsDescription1"/>'
					+ '<select name="naicsDescription" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'naicsDescription1\');">'
					+ $('#naicsDescription').html() + '</select>';
 			//chosenConfig();
			$("#naicsDescription1").next().chosen();
			$("#naicsDescription1").next().trigger("chosen:updated"); */
		} else if (value.value == 'CERTIFICATIONTYPE') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="certificatioType1"/>'
					+ '<select name="certificationType" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'certificatioType1\');">'
					+ $('#certificationType').html() + '</select>';
// 			chosenConfig();
			$("#certificatioType1").next().chosen();
			$("#certificatioType1").next().trigger("chosen:updated");
		} else if (value.value == 'VENDORCOMMODITY') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="vendorCommodity6"/>'
				+ '<select name="vendorCommodity" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
				+ 'data-placeholder=" " onchange="assignValue(this,\'vendorCommodity6\');">'
				+ $('#vendorCommodity').html() + '</select>';
//			chosenConfig();
			$("#vendorCommodity6").next().chosen();
			$("#vendorCommodity6").next().trigger("chosen:updated");
		} else if (value.value == 'CONTACTSTATE') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="contactStates" name="searchFieldsValue" value="">'
				+'<select name="contactState" style="width:265px;" class="chosen-single chosen-default"'+
				'data-placeholder=" "  onchange="assignValue(this,\'contactStates\');"  >'
				+$('#contactStates').html()+'</select>';
				$("#contactStates").next().chosen();
				$("#contactStates").next().trigger("chosen:updated");
		} else if (value.value == 'CONTACTDATE') {
			table.rows[rowNo].cells[1].innerHTML = "<input name='searchFieldsValue' class='main-text-box datePicker' id='contactDate' alt='Please click to select date' readonly='readonly'/>";
			initDatePicker();
		} else if (value.value == 'OILANDGASINDUSTRYEXPERIENCE') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="oilAndGasIndustryExperience1" name="searchFieldsValue" value="">'
				+'<select name="oilAndGasIndustryExperience" class="chosen-select-deselect" style="width: 265px;"'+
			 	'data-placeholder=" "  onchange="assignValue(this,\'oilAndGasIndustryExperience1\');"  >'
			 	+$('#oilAndGasIndustryExperience').html()+'</select>';
				//chosenConfig();
				$("#oilAndGasIndustryExperience1").next().chosen();
				$("#oilAndGasIndustryExperience1").next().trigger("chosen:updated");
		}  else if (value.value == 'PRIMARILYONSHOREOROFFSHORE') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="primarilyOnshoreOrOffshore1" name="searchFieldsValue" value="">'
				+'<select name="primarilyOnshoreOrOffshore" class="chosen-select-deselect" style="width: 265px;"'+
			 	'data-placeholder=" "  onchange="assignValue(this,\'primarilyOnshoreOrOffshore1\');"  >'
			 	+$('#primarilyOnshoreOrOffshore').html()+'</select>';
				//chosenConfig();
				$("#primarilyOnshoreOrOffshore1").next().chosen();
				$("#primarilyOnshoreOrOffshore1").next().trigger("chosen:updated");
			}else if (value.value == 'BPMARKETSECTOR') {
			table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpMarketSectorDynamicId"/>'
				+ '<select name="bpMarketSectorId" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
				+ 'data-placeholder=" " onchange="assignValue(this,\'bpMarketSectorDynamicId\');">'
				+ $('#bpMarketSectorId').html() + '</select>';
			//chosenConfig();
			$("#bpMarketSectorDynamicId").next().chosen();
			$("#bpMarketSectorDynamicId").next().trigger("chosen:updated");
			} else if (value.value == 'BPMARKETSUBSECTOR') {
				table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpMarketSubSectorDynamicId"/>'
					+ '<select name="bpMarketSubSectorId" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
					+ 'data-placeholder=" " onchange="assignValue(this,\'bpMarketSubSectorDynamicId\');">'
					+ $('#bpMarketSubSectorId').html() + '</select>';
				//chosenConfig();
				$("#bpMarketSubSectorDynamicId").next().chosen();
				$("#bpMarketSubSectorDynamicId").next().trigger("chosen:updated");
			}else {
			table.rows[rowNo].cells[1].innerHTML = "<input class='main-text-box ' name='searchFieldsValue'/>";
		}
	}
</script>

<script type="text/javascript">
	chosenConfig();
	function chosenConfig() {
		 var config = {
			'.chosen-select' : {
				width : "96%"
			},
			'.chosen-select-deselect' : {
				allow_single_deselect : true,
				width : "96%"
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "95%"
			}
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
	}
</script>
<script>
	function showStatusDesc(statusId) {
		var status = statusId;
		var statusdesc = "";
		<logic:present name="vendorStatusList">
		<bean:size id="size" name="vendorStatusList"/>
		<logic:greaterEqual value="0" name="size">
		<logic:iterate id="vendorStatus" name="vendorStatusList">
		if (status == '<bean:write name="vendorStatus" property="id" />') {
			statusdesc = '<bean:write name="vendorStatus" property="statusdescription" />';
		}
		</logic:iterate>
		</logic:greaterEqual>
		</logic:present>
		if (statusdesc != "") {
			document.getElementById('statusDescArea').style.display = "block";
			document.getElementById("statusdesc").value = statusdesc;

		} else {
			document.getElementById('statusDescArea').style.display = "hidden";
		}
	}
</script>
<script>
function callAutoCompleteForNaicsCode()
{
$(function() {
	var items=[];	
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}
$( "#naicsCode" )
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
	}
})
.autocomplete({
source: function( request, response ) {
	var searchCode = extractLast( request.term );
	 $.ajax({
	        url: "viewVendorsStatus.do?method=searchNaicsCode&term="+searchCode,
	        dataType: "json",
	        type: "GET",
	        success: function (data) {
	        	while(items.length>0){
	        		items.pop();
	        	}
	        	for(i=0;i < data.length;i++ )
					{					
					var naicsStr='';
				 naicsStr=naicsStr+data[i];
						items.push(naicsStr);
					}
	            response(items);
	        }
	    });	
	},
	search: function() {
	var term = extractLast( this.value );
	if ( term.length < 2 ) {
			return false;
		}
	},
	focus: function() {
		return false;
	},
	select: function( event, ui ) {
	var terms = split( this.value );
			terms.pop();
		terms.push( ui.item.value );
		terms.push( "" );
	this.value = terms.join( "," );
		return false;
	}
	});
});
}
</script>
<script>
function callAutoCompleteForNaicsDesc()
{
$(function() {
	var items=[];
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}
$( "#naicsDescription" )	
	.bind( "keydown", function( event ) {
	if ( event.keyCode === $.ui.keyCode.TAB &&
	$( this ).data( "ui-autocomplete" ).menu.active ) {
	event.preventDefault();
	}
})
.autocomplete({
source: function( request, response ) {
	var searchCode = extractLast( request.term );
	 $.ajax({
	        url: "viewVendorsStatus.do?method=searchNaicsDescription&term="+searchCode,
	        dataType: "text",
	        type: "GET",
	        success: function (data) {
	        	while(items.length>0){
	        		items.pop();
	        	}
	        	var naicsData=data.split(',');
					for(i=0;i <naicsData.length;i++ )
					{
						var naicsDesc='';
						naicsDesc=naicsData[i];
						 if(i==0)
							 naicsDesc= naicsDesc.replace('[',' ');
						 if(i==naicsData.length-1)
							 naicsDesc= naicsDesc.replace(']',' ');
						if(naicsDesc.charAt(0) === ' '){
							naicsDesc = naicsDesc.substr(1);
						}
						
						items.push(naicsDesc);
					}
	            response(items);
	        }
	    });	
	},
	search: function() {
		var term = extractLast( this.value );
		if ( term.length < 2 ) {
		return false;
		}
	},
	focus: function() {
		return false;
	},
	select: function( event, ui ) {
		var terms = split( this.value );
		terms.pop();
	terms.push( ui.item.value );
		terms.push( "" );
		this.value = terms.join( "," );
	return false;
	}
	});
});
}
function sortTable(n) {
	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("table1");
	  switching = true;
	  //Set the sorting direction to ascending:
	  dir = "asc"; 
	  /*Make a loop that will continue until
	  no switching has been done:*/
	  while (switching) {
	    //start by saying: no switching is done:
	    switching = false;
	    rows = table.getElementsByTagName("TR");
	    /*Loop through all table rows (except the
	    first, which contains table headers):*/
	    for (i = 1; i < (rows.length - 1); i++) {
	      //start by saying there should be no switching:
	      shouldSwitch = false;
	      /*Get the two elements you want to compare,
	      one from current row and one from the next:*/
	      x = rows[i].getElementsByTagName("TD")[n];
	      y = rows[i + 1].getElementsByTagName("TD")[n];
	      /*check if the two rows should switch place,
	      based on the direction, asc or desc:*/
	      if (dir == "asc") {
	        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
	          //if so, mark as a switch and break the loop:
	          shouldSwitch= true;
	          break;
	        }
	      } else if (dir == "desc") {
	        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
	          //if so, mark as a switch and break the loop:
	          shouldSwitch= true;
	          break;
	        }
	      }
	    }
	    if (shouldSwitch) {
	      /*If a switch has been marked, make the switch
	      and mark that a switch has been done:*/
	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	      switching = true;
	      //Each time a switch is done, increase this count by 1:
	      switchcount ++;      
	    } else {
	      /*If no switching has been done AND the direction is "asc",
	      set the direction to "desc" and run the while loop again.*/
	      if (switchcount == 0 && dir == "asc") {
	        dir = "desc";
	        switching = true;
	      }
	    }
	  }
	}

	function myFunctionSearch() {
		  var input, filter, table, tr, td, i;
		  input = document.getElementById("myInput");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("table1");
		  tr = table.getElementsByTagName("tr");
		  for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[0];
		    if (td) {
		      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
		        tr[i].style.display = "";
		      } else {
		        tr[i].style.display = "none";
		      }
		    }       
		  }
		}
</script>

