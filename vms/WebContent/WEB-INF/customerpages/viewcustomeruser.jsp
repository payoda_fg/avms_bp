<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<style>
<!--
#ajaxloader {
	position: absolute;
	z-index: 99999;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}
-->
</style>

<script>
	$(document).ready(function() {
		$("#contanctEmail").val('');
		$("#loginpassword").val('');
		
		$("#loginForm").validate({
			rules : {
				/* userEmailId : {
					required : true,
					email : true
				},
				userPassword : {
					required : true
				}, */
				emailId : {
					email : true,
					required : true
				},
				stackHolderFirstName : {
					rangelength : [ 0, 255 ],
					required : true
				},
				stackHolderLastName : {
					rangelength : [ 0, 255 ]
				},
				stackHolderManagerName : {
					rangelength : [ 0, 255 ],
					required : true
				},
				stackHolderReason : {
					rangelength : [ 0, 60000],
					required : true
				}
			}
		});
		
		$("#loginForm1").validate({
			rules : {
				userEmailId : {
					required : true,
					email : true
				},
				userPassword : {
					required : true
				}
			}
		});
	});

	var emailobj;
	var status = "true";
	function sendLoginCredentials() 
	{
		var emailId = $.trim($('#emailId').val());
		var divisionId = $("#divisionId").val();		
		var firstName = $.trim($('#stackHolderFirstName').val());
		var lastName = $.trim($('#stackHolderLastName').val());
		var managerName = $.trim($('#stackHolderManagerName').val());
		var reason = $.trim($('#stackHolderReason').val());
		status = "true";
		
		if(divisionId != 'Select') 
		{
			validation();
			if (status == "true") 
			{
				$.ajax(
				{
					url : "customeruserregistration.do?method=sendStakeHolderLoginCredentials&emailId="+ emailId + "&customerDivision=" + divisionId
					          + "&stackHolderFirstName=" + firstName +"&stackHolderLastName=" + lastName +"&stackHolderManagerName=" + managerName
					          + "&stackHolderReason=" + reason,
					type : "POST",
					async : true,
					beforeSend : function() {
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show(); //show image loading
					},
					success : function(data) {
						$("#ajaxloader").hide();						
						alert($.trim(data));
						$("#emailId").val('');
						$("#stackHolderFirstName").val('');
						$("#stackHolderLastName").val('');
						$("#stackHolderManagerName").val('');
						$("#stackHolderReason").val('');
						$("#divisionId option[value='Select']").prop('selected', true);
						$("#divisionId").select2();
					}
				});
			}
		}
		else 
		{
			alert("Please Select Division...");
		}
	}
	
	function validation()
	{		
		var emailId = $.trim($('#emailId').val());	
		var firstName = $.trim($('#stackHolderFirstName').val());
		var managerName = $.trim($('#stackHolderManagerName').val());
		var reason = $.trim($('#stackHolderReason').val());
		
		var domain = emailId.substring(emailId.indexOf('@'));
		domain = domain.toUpperCase();
	
		if(emailId == "")
		{
			alert("Please Enter valid Email Address.");
			status = "false";
		}
		else if(domain != "@BP.COM")
		{
			alert("Email Domain should be bp.com");
			status = "false";
		} 
		else if(firstName == "")
		{
			alert("Please Enter first name");
			status = "false";
		}
		else if(managerName == "")
		{
			alert("Please Enter manager name");
			status = "false";
		}
		else if(reason == "")
		{
			alert("Please Enter Reason");
			status = "false";
		}
	}
</script>

<div class="page-title">
	<h1 style="color: #009900;text-align: center;">Stakeholders Registration</h1>
</div>

<div class="form-box" style="width: 96%; margin-left: 1%; min-height: 300px;">
	<html:form  styleId="loginForm">
		<logic:present name="isDivisionStatus">		
			<logic:equal value="1" name="isDivisionStatus" property="isDivision">
				<div class="wrapper-half">
					<div class="row-wrapper" style="width: 100%">
						<div class="label-col-wrapper">Please Select Division Under which You want to Register</div>
						<html:select property="customerDivision" styleId="divisionId" styleClass="chosen-select" style="width:312px">
							<html:option value="Select">Select</html:option>
							<bean:size id="size" name="customerDivisions"></bean:size>
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="customerDivision" name="customerDivisions">
									<bean:define id="id" name="customerDivision" property="id"></bean:define>
										<html:option value="${id}">
											<bean:write name="customerDivision" property="divisionname"></bean:write>
										</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</html:select>
					</div>
				</div>
			</logic:equal>
		</logic:present>
	
		<div class="wrapper-half">
			<div class="row-wrapper" style="width: 100%">
				<div class="label-col-wrapper">Please Enter Email Address:</div>
				<div class="ctrl-col-wrapper" style="width: 30%">
					<h5><font color="red">*<input type="text" name="emailId" id="emailId" class="text-box"></font></h5>
				</div>
				<h5>Note: Email Domain should be bp.com (example@bp.com)</h5>
			</div>
		</div>
		
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name:</div>
				<div class="ctrl-col-wrapper" >
					<h5><font color="red">*<input type="text" name="stackHolderFirstName" id="stackHolderFirstName" class="text-box"></font></h5>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name:</div>
				<div class="ctrl-col-wrapper">
					<input type="text" name="stackHolderLastName" id="stackHolderLastName" class="text-box" alt="Optional">
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Manager Name:</div>
				<div class="ctrl-col-wrapper">
					<h5><font color="red">*<input type="text" name="stackHolderManagerName" id="stackHolderManagerName" class="text-box"></font></h5>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Reason:</div>
				<div class="ctrl-col-wrapper">
					<h5><font color="red">*<html:textarea property="stackHolderReason" rows="5" cols="30" styleClass="main-text-area" styleId="stackHolderReason"></html:textarea></font></h5>
				</div>
			</div>
		</div>
	
		<div class="wrapper-btn">
			<input type="button" class="btn" id="submit" value="Send for Approval" onclick="sendLoginCredentials();">
		</div>

		<div class="clear"></div>
		
		<div id="successMsg">
			<html:messages id="emailId" property="emailId" message="true">
				<span><bean:write name="emailId" /></span>
			</html:messages>
			<html:messages id="loginfail" property="loginfail" message="true">
				<span><bean:write name="loginfail" /></span>
			</html:messages>
		</div>
	</html:form>

	<html:form action="/customeruserregistration?method=viewCustomerUser" styleId="loginForm1">
		<p>Please login here.</p>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email Id</div>
				<div class="ctrl-col-wrapper">
					<html:text property="userEmailId" alt="" styleId="userEmailId"
						styleClass="text-box" />
				</div>
			</div>
		</div>
		
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="userPassword" alt=""
						styleId="userPassword" styleClass="text-box" />
				</div>
			</div>
		</div>
		
		<div class="lable">
			<span style="color: red; font-style: italic;"><html:errors property="login"></html:errors> </span>
		</div>
		
		<div class="wrapper-btn">
			<html:submit styleClass="btn" styleId="submit" value="Login"></html:submit>
		</div>
	</html:form>
</div>