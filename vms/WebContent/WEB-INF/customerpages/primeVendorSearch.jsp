<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fg.vms.util.SearchFields"%>

<%
	pageContext.setAttribute("searchByOptions", SearchFields.values());
%>

<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>

<script type="text/javascript">
	//Gets Country (United States) and Sets Related States in State Button...
	$(document).ready(function() 
	{
		<logic:present name="userDetails" property="workflowConfiguration">
			<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
				$.ajax(
				{
					url : 'state.do?method=getStateByDefaultCountry&random=' + Math.random(),
					type : "POST",
					async : true,
					success : function(data) 
					{
						$("#state").find('option').remove().end().append(data);
						$('#state').trigger("chosen:updated");
					}
				});
			</logic:equal>
		</logic:present>
	});

	$(document).keypress(function(e) {
		if (e.which == 13) {
			e.preventDefault();
			return searchData();
		}
	});
	
	function searchData() 
	{
		$.ajax(
		{
			url : "viewVendorsStatus.do?method=primeVendorSearch&searchPageType=primeVendorSearch",
			type : 'POST',
			data : $("#searchform").serialize(),
			async : false,
			dataType : "json",
			beforeSend : function(){
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show(); //show image loading
			}, 
			complete : function(data){
				$("#ajaxloader").hide();				
				window.location = "viewVendorsStatus.do?method=searchByPrimeVendorRestult";
			}
		});
	}

	function assignValue(value, id) 
	{
		$('#' + id).val(value.value);
	}
	
	function changestate(ele, id) 
	{
		var country = ele.value;
		if (country != null) 
		{
			$.ajax(
			{
				url : 'state.do?method=getState&id=' + country + '&random=' + Math.random(),
				type : "POST",
				async : false,
				success : function(data) 
				{
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2({width : "100%"});
				}
			});
		}
	}
	
	function recallPreviousSearch()
	{		
		$("#dialog1").css({
			"display" : "block"
		});
		$("#dialog1").dialog({
			minWidth : 900,
			modal : true
		});
	}
	
	function previousSearch(searchId) 
	{
		$.ajax(
		{
			url : "viewVendors.do?method=searchByPreviousFilterData&searchPageType=primeVendorSearch&searchId="+searchId,
			type : 'POST',
			data : $("#searchform").serialize(),
			async : false,
			dataType : "json",
			beforeSend : function() {
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				$("#ajaxloader").hide();
				window.location = "viewVendorsStatus.do?method=searchByPrimeVendorRestult";
			}
		});				 
	}
	
	function confirm_delete1() {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			return true;
		} else {
			return false;
		}
	}
	
	function deleteSearchFilter(searchId) {
		var result=confirm_delete1();
	if(result){
			$.ajax({
			url : "viewVendors.do?method=deleteSearchFilter&searchType=P"
					+"&searchId="+searchId,
			type : 'POST',
			async : false,
			dataType : "json",
			beforeSend : function() {
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#dialog1").dialog("close");
				if(data.result=="success")
				{
					alert("Search Filter Successfully Deleted");
					window.location.reload(true);
				}
				else{
						alert("Sorry Transaction Failed");
					}
			}
		});		
	}
	else
		return false;
	}
</script>

<style type="text/css">
	/*.main-list-box {
		padding: 0% 1%;
	}
	
	.main-table1 td.header {
		background: none repeat scroll 0 0 #009900;
		color: #FFFFFF;
	}
	
	.main-table1 td {
		border: 1px solid #E9EAEA;
		padding: 5px;
	}
	
	tr:nth-child(2n+1) {
		background: none repeat scroll 0 0;
	}
	
	#searchTable td {
		border: 0px solid #E9EAEA;
	}*/
</style>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">
<header class="card-header">
				<h2 class="card-title pull-left">
	<img id="show1" src="images/VendorSearch.gif" />Prime Vendor Search </h2>
	<input type="button" value="Recall Previous Search"  class="btn btn-primary pull-right" onclick="recallPreviousSearch();">
</header>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Prime Vendor Search" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box card-body">
				<html:form action="/viewVendorsStatus.do?method=primeVendorSearch" styleId="searchform">
					<table width="100%" border="0" class="main-table1 table table-bordered table-striped mb-0" id="searchTable">
						<tr align="center">
							<td class="" >Fields <input type="hidden" value="NONPRIME" name="vendorType"></td>
							<td class="" >Values</td>
							<td/>							
						</tr>
						<tr>
							<td>
								<input type="hidden" value="PRIME" name="vendorType"/>
								<input type="hidden" value="NONPRIME" name="vendorType"/> 
								<input type="hidden" value="LEGALCOMPANYNAME" name="searchFields"/>Company Name
							</td>
							<td>
								<input type="text" class="main-text-box" style="width:96%;" name="searchFieldsValue" />
							</td>							
						</tr>
						<tr>
							<td>
								<input type="hidden" value="FIRSTNAME" name="searchFields" />Contact First Name
							</td>
							<td>
								<input type="text" class="main-text-box" style="width:96%;" name="searchFieldsValue" />
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" value="LASTNAME" name="searchFields" />Contact Last Name
							</td>
							<td>
								<input type="text" class="main-text-box" style="width:96%;" name="searchFieldsValue" />
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" value="CONTACTEMAIL" name="searchFields" />Contact Email
							</td>
							<td>
								<input type="text" class="main-text-box" style="width:96%;" name="searchFieldsValue" onchange="ajaxFn(this,'U');" />
							</td>
						</tr>					
						<tr>
							<logic:present name="userDetails" property="workflowConfiguration">
								<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
									<td>
										<input type="hidden" value="COUNTRY" name="searchFields" />Country
									</td>
									<td>
										<input type="hidden" class="main-text-box" name="searchFieldsValue" value="" id="selectCountries1" /> 
										<select name="selectCountries" class="chosen-select-deselect form-control"
											data-placeholder=" " onchange="changestate(this,'state');assignValue(this,'selectCountries1');">
											<option value=""></option>
											<logic:present name="countries">
												<bean:size id="size" name="countries" />
												<logic:greaterEqual value="0" name="size">
													<logic:iterate id="country" name="countries">
														<bean:define id="name" name="country" property="id"></bean:define>
		 												<option value="<%=name.toString()%>">
															<bean:write name="country" property="countryname" />
														</option>
													</logic:iterate>
												</logic:greaterEqual>
											</logic:present>
											<!-- <option value="1"> United States </option> -->
										</select>
									</td>
								</logic:equal>
							</logic:present>
						</tr>							
						<tr>
							<td><input type="hidden" value="STATE" name="searchFields" />State</td>
							<td>
								<select name="searchFieldsValue" class="chosen-select form-control" id="state">
									<option value="">&nbsp;&nbsp;</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><input type="hidden" value="CITY" name="searchFields" />City</td>
							<td><input type="text" class="main-text-box" style="width:96%;" name="searchFieldsValue" /></td>
						</tr>
					</table>
					
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
							<input type="button" value="Search" class="btn btn-primary" id="search" onclick="searchData();">
						</div>
						</div>
					</footer>
				</html:form>
			</div>
			
			<div id="ajaxloader" style="display: none;">
				<img src="images/ajax-loader.gif" alt="Ajax Loading Image" />
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Prime Vendor Search" name="privilege" property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box card-body">
				<h3 align="center">You Have No Rights to Search the Prime Vendor</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<!-- Dialog Box for Choose Previous Search Filters -->
<div id="dialog1" title="Choose Previous Search Filters" style="display: none;">
	<logic:present name="previousSearchList">
		<table class="main-table table table-bordered table-striped mb-0">
			<bean:size id="size" name="previousSearchList" />
			<logic:greaterThan value="0" name="size">
				<thead>
					<tr>
						<td class="">Search Name</td>
						<td class="">Search Date</td>
						<td class="">Search Type</td>
						<td class="">Criteria</td>
						<td class="">Actions</td>
					</tr>
				</thead>
				<fmt:setLocale value="en_US"/>
				<logic:iterate name="previousSearchList" id="previousSearch">
					<bean:define id="previousSearchId" name="previousSearch" property="id"></bean:define>
					<tbody>
						<tr>
							<td>
								<html:link href="Javascript:void ( 0 ) ;" onclick="previousSearch(${previousSearchId});">
									<bean:write name="previousSearch" property="searchName" />
								</html:link>
							</td>
							<td><bean:write name="previousSearch" property="searchDate"  format="yyyy-MM-dd hh:mma"/></td>
							<td><bean:write name="previousSearch" property="searchType" /></td>
							<td><bean:write name="previousSearch" property="criteria" /></td>							
							<td>
								<html:link href="Javascript:void ( 0 ) ;" onclick="deleteSearchFilter(${previousSearchId});">Delete</html:link>
							</td>
						</tr>
					</tbody>
				</logic:iterate>
			</logic:greaterThan>
			<logic:equal value="0" name="size">
				No such Records
			</logic:equal>
		</table>
	</logic:present>
	<logic:notPresent name="previousSearchList">
		<p>There is no record(s) available.</p>
	</logic:notPresent>
</div>
</div>
</div>
</section>