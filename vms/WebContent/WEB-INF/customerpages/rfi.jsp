<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="js/listboxmove.js"></script>
<script type="text/javascript">
	var imgPath = "jquery/images/calendar.gif";
	datePickerRFI();
</script>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 20px;">
		<html:form action="/createrfiinformation?method=createRFIInfo"
			method="post" enctype="multipart/form-data" styleId="formID">
			<html:javascript formName="rfiInformation" />
			<html:hidden property="rfType" value="I" />
			<div class="toggleFilter">
				<h2 id="show" title="Click here to hide/show the form"
					style="cursor: pointer;">
					<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;RFI
					Information
				</h2>
				<%@include file="rfinformation.jsp"%>
			</div>

			<div class="toggleFilter">
				<h2 id="show2" title="Click here to hide/show the form"
					style="cursor: pointer;">
					<img id="showx" src="images/arrow-down.png" />&nbsp;&nbsp;Select
					Vendor
				</h2>
			</div>

			<%@include file="rfivendor.jsp"%>

			<div class="toggleFilter">
				<h2 id="show3" title="Click here to hide/show the form"
					style="cursor: pointer;">
					<img id="showy" src="images/arrow-down.png" />&nbsp;&nbsp;RFI
					Documents
				</h2>
			</div>

			<div id="table-holder3">
				<%@include file="rfidocuments.jsp"%>
			</div>

			<div class="toggleFilter">
				<h2 id="show4" title="Click here to hide/show the form"
					style="cursor: pointer;">
					<img id="showz" src="images/arrow-down.png" />&nbsp;&nbsp;RFI
					Compliance
				</h2>
			</div>

			<div id="table-holder4">
				<%@include file="compliance.jsp"%>


			</div>
			<div class="save-btn">
				<html:submit value="Submit" styleId="submit"
					styleClass="customerbtTxt" style="position: absolute; left: 500px;" />
			</div>
		</html:form>
	</div>
</div>
