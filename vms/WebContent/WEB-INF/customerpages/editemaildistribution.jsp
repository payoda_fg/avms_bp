<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<!--<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />   -->
<%
	Integer count = 0;
	Integer count1 = 0;
	Integer count2 = 0;
%>
<script>
	$(document).ready(function() {
		$("#emailDistributionForm").validate({
			rules : {
				emailDistributionListName : {
					required : true
				}
			}
		});
	});
	// Function to reset the form
	function clearfields() {
		window.location = "emaildistribution.do?method=viewEmailDistribution";
	}
	function enable_cb1(index) {
		if ($("#checkbox" + index).is(':checked')) {
			$("#checkboxIsTo" + index).removeAttr("disabled");
			$("#checkboxIsCC" + index).removeAttr("disabled");
			$("#checkboxIsBCC" + index).removeAttr("disabled");
		} else {
			$("#checkboxIsTo" + index).attr("disabled", true);
			$("#checkboxIsCC" + index).attr("disabled", true);
			$("#checkboxIsBCC" + index).attr("disabled", true);
		}
	}
	function enable_cb2(index) {
		if ($("#checkboxv" + index).is(':checked')) {
			$("#vendorCheckboxIsTo" + index).removeAttr("disabled");
			$("#vendorCheckboxIsCC" + index).removeAttr("disabled");
			$("#vendorCheckboxIsBCC" + index).removeAttr("disabled");
		} else {
			$("#vendorCheckboxIsTo" + index).attr("disabled", true);
			$("#vendorCheckboxIsCC" + index).attr("disabled", true);
			$("#vendorCheckboxIsBCC" + index).attr("disabled", true);
		}
	}
	function validateForm() {
		var fields1 = $("input[name='checkbox1']").serializeArray();
		var fields3 = $("input[name='checkbox3']").serializeArray();
		var checkboxIsTo = $("input[name='checkboxIsTo']").serializeArray();
		var checkboxIsCC = $("input[name='checkboxIsCC']").serializeArray();
		var checkboxIsBCC = $("input[name='checkboxIsBCC']").serializeArray();
		var vendorCheckboxIsTo = $("input[name='vendorCheckboxIsTo']")
				.serializeArray();
		var vendorCheckboxIsCC = $("input[name='vendorCheckboxIsCC']")
				.serializeArray();
		var vendorCheckboxIsBCC = $("input[name='vendorCheckboxIsBCC']")
				.serializeArray();
		if (fields1.length == 0 && fields3.length == 0) {
			alert('Please select atleast one user');
			$("input[name='checkbox1']").prop('checked', true);
			return false;
		}
		if (checkboxIsTo.length == 0 && checkboxIsCC.length == 0
				&& checkboxIsBCC.length == 0 && vendorCheckboxIsTo.length == 0
				&& vendorCheckboxIsCC.length == 0
				&& vendorCheckboxIsBCC.length == 0) {
			alert('Please select atleast one recipient');
			// cancel submit
			return false;
		}
	}
	function enableSelectAll(name) {
		if (name == 'checkbox1' || name == 'checkbox') {
			if ($("#cselectall").is(':checked')) {
				$("input[name='checkbox1']").attr("checked", true);
				$("input[name='checkbox']").attr("checked", true);
				$("#custtoselectall").removeAttr("disabled");
				$("#custccselectall").removeAttr("disabled");
				$("#custbccselectall").removeAttr("disabled");
				$("input[name='checkboxIsTo']").removeAttr("disabled");
				$("input[name='checkboxIsCC']").removeAttr("disabled");
				$("input[name='checkboxIsBCC']").removeAttr("disabled");
			} else {
				$("input[name='checkbox1']").attr("checked", false);
				$("input[name='checkbox']").attr("checked", false);
				$("#custtoselectall").attr("disabled", true);
				$("#custccselectall").attr("disabled", true);
				$("#custbccselectall").attr("disabled", true);
				$("input[name='checkboxIsTo']").attr("disabled", true);
				$("input[name='checkboxIsCC']").attr("disabled", true);
				$("input[name='checkboxIsBCC']").attr("disabled", true);
			}
		} else if (name == 'checkbox3' || name == 'checkbox2') {
			if ($("#vselectall").is(':checked')) {
				$("input[name='checkbox3']").attr("checked", true);
				$("input[name='checkbox2']").attr("checked", true);
				$("#ventoselectall").removeAttr("disabled");
				$("#venccselectall").removeAttr("disabled");
				$("#venbccselectall").removeAttr("disabled");
				$("input[name='vendorCheckboxIsTo']").removeAttr("disabled");
				$("input[name='vendorCheckboxIsCC']").removeAttr("disabled");
				$("input[name='vendorCheckboxIsBCC']").removeAttr("disabled");
			} else {
				$("input[name='checkbox3']").attr("checked", false);
				$("input[name='checkbox2']").attr("checked", false);
				$("#ventoselectall").attr("disabled", true);
				$("#venccselectall").attr("disabled", true);
				$("#venbccselectall").attr("disabled", true);
				$("input[name='vendorCheckboxIsTo']").attr("disabled", true);
				$("input[name='vendorCheckboxIsCC']").attr("disabled", true);
				$("input[name='vendorCheckboxIsBCC']").attr("disabled", true);
			}
		}
	}
	function enableCustomerSelectAll(type) {
		if(type == 'to'){
			if ($("#custtoselectall").is(':checked')) {
				$("input[name='checkboxIsTo']").attr("checked", true);
			} else {
				$("input[name='checkboxIsTo']").attr("checked", false);
			}
		}else if(type == 'cc'){
			if ($("#custccselectall").is(':checked')) {
				$("input[name='checkboxIsCC']").attr("checked", true);
			} else {
				$("input[name='checkboxIsCC']").attr("checked", false);
			}
		} else {
			if ($("#custbccselectall").is(':checked')) {
				$("input[name='checkboxIsBCC']").attr("checked", true);
			} else {
				$("input[name='checkboxIsBCC']").attr("checked", false);
			}
		}
	}
	function enableVendorSelectAll(type) {
		if(type == 'to'){
			if ($("#ventoselectall").is(':checked')) {
				$("input[name='vendorCheckboxIsTo']").attr("checked", true);
			} else {
				$("input[name='vendorCheckboxIsTo']").attr("checked", false);
			}
		}else if(type == 'cc'){
			if ($("#venccselectall").is(':checked')) {
				$("input[name='vendorCheckboxIsCC']").attr("checked", true);
			} else {
				$("input[name='vendorCheckboxIsCC']").attr("checked", false);
			}
		} else {
			if ($("#venbccselectall").is(':checked')) {
				$("input[name='vendorCheckboxIsBCC']").attr("checked", true);
			} else {
				$("input[name='vendorCheckboxIsBCC']").attr("checked", false);
			}
		}
	}
	function addUser() {
		// Ajax function to load customer user records
		$.ajax({
			url : "emaildistribution.do?method=getCustomerContacts&random="
					+ Math.random(),
			type : "POST",
			async : true,
			beforeSend : function() {
				$("#ajaxloader").show();
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#gridtable1").jqGrid('GridUnload');
				gridCustomerContact(data);
			}
		});
		$("#customer").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});
		$("#customer").dialog({
			width : 600
		});
	}
	function addVendor() {
		// Ajax function to load vendor user records
		$.ajax({
			url : "emaildistribution.do?method=getVendorContacts&random="
					+ Math.random(),
			type : "POST",
			async : true,
			beforeSend : function() {
				$("#ajaxloader").show();
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#gridtable").jqGrid('GridUnload');
				gridVendorContact(data);
			}
		});
		$("#vendor").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});
		$("#vendor").dialog({
			width : 900
		});
	}
	var idsOfSelectedRows1 = [];
	function gridCustomerContact(myGridData) {
		var newdata1 = jQuery.parseJSON(myGridData);
		$('#gridtable1').jqGrid({
			data : newdata1,
			datatype : 'local',
			colNames : [ 'Select', 'Name', 'Email ID' /* , 'TO', 'CC', 'BCC' */],
			colModel : [ {
				name : 'id',
				index : 'id',
				editable : true,
				hidden : true
			}, {
				name : 'name',
				index : 'name',
				width : 250,
				align : 'left'
			}, {
				name : 'emailid',
				index : 'emailid',
				width : 250,
				align : 'left'
			} ],
			rowNum : 10,
			rowList : [ 10, 20, 50, 100 ],
			pager : '#pager1',
			viewrecords : true,
			width : 'auto',
			emptyrecords : 'No data available',
			onSelectRow : function(id, isSelected) {
				var p = this.p, item = p.data[p._index[id]], i = $
						.inArray(id, idsOfSelectedRows1);
				item.cb = isSelected;
				if (!isSelected && i >= 0) {
					idsOfSelectedRows1.splice(i, 1); // remove id from the list
				} else if (i < 0) {
					idsOfSelectedRows1.push(id);
				}
			},
			onSelectAll : function(id, isSelected) {
				for ( var j = 0; j < id.length; j++) {
					//console.log(status);
					var p = this.p, item = p.data[p._index[id[j]]], i = $
							.inArray(id[j], idsOfSelectedRows1);
					item.cb = isSelected;
					if (!isSelected && i >= 0) {
						idsOfSelectedRows1.splice(i, 1); // remove id from the list
					} else if (i < 0) {
						idsOfSelectedRows1.push(id[j]);
					}
				}
			},
			loadComplete : function() {
				var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
				for (i = 0,
						selCount = idsOfSelectedRows1.length; i < selCount; i++) {
					rowid = idsOfSelectedRows1[i];
					item = data[index[rowid]];
					if ('cb' in item && item.cb) {
						$this.jqGrid('setSelection', rowid,
								false);
					}
				}
			},
			multiselect : true,
			cmTemplate : {
				title : false
			},
			height : 'auto'
		});
		return idsOfSelectedRows1;
	}
	var idsOfSelectedRows = [];
	function gridVendorContact(myGridData) {
		var newdata1 = jQuery.parseJSON(myGridData);
		$('#gridtable').jqGrid({
				data : newdata1,
				datatype : 'local',
				colNames : [ 'Select', 'Name', 'Email ID',
						'Vendor Name' , 'Vendor Type','Vendor Status' /*, 'CC', 'BCC' */],
				colModel : [ {
					name : 'id',
					index : 'id',
					editable : true,
					hidden : true
				}, {
					name : 'name',
					index : 'name',
					align : 'left',
					width : 100
				}, {
					name : 'emailid',
					index : 'emailid',
					align : 'left',
					width : 250
				}, {
					name : 'vendorname',
					index : 'vendorname',
					align : 'left',
					width : 250
				}, {
					name : 'prime',
					index : 'prime',
					align : 'left',
					width : 90
				}, {
					name : 'vendorstatus',
					index : 'vendorstatus',
					align : 'left',
					width : 120
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50, 100, 200, 500, 1000 ],
				pager : '#pager',
				viewrecords : true,
				emptyrecords : 'No data available',
				onSelectRow : function(id, isSelected) {
					var p = this.p, item = p.data[p._index[id]], i = $
							.inArray(id, idsOfSelectedRows);
					item.cb = isSelected;
					if (!isSelected && i >= 0) {
						idsOfSelectedRows.splice(i, 1); // remove id from the list
					} else if (i < 0) {
						idsOfSelectedRows.push(id);
					}
				},
				onSelectAll : function(id, isSelected) {
					for ( var j = 0; j < id.length; j++) {
						//console.log(status);
						var p = this.p, item = p.data[p._index[id[j]]], i = $
								.inArray(id[j], idsOfSelectedRows);
						item.cb = isSelected;
						if (!isSelected && i >= 0) {
							idsOfSelectedRows.splice(i, 1); // remove id from the list
						} else if (i < 0) {
							idsOfSelectedRows.push(id[j]);
						}
					}
				},
				loadComplete : function() {
					var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
					for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
						rowid = idsOfSelectedRows[i];
						item = data[index[rowid]];
						if ('cb' in item && item.cb) {
							$this.jqGrid('setSelection', rowid,
									false);
						}
					}
				},
				multiselect : true,
				cmTemplate : {
					title : false
				},
				height : 'auto'
			}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			refresh : false,
			search : false
		},
		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{});
		return idsOfSelectedRows;
	}
	function createCustomerTable() {
		var grid = jQuery("#gridtable1");
		var table = document.getElementById("dynamicCustomerTable");
		var messages = [];
		messages.push("The Following EmailID's already selected. You cannot add it again.\n");
		if (idsOfSelectedRows1.length > 0) {
			for ( var i = 0, il = idsOfSelectedRows1.length; i < il; i++) {
				var rowCount = table.rows.length;
				var row = grid.getLocalRow(idsOfSelectedRows1[i]);
				var userId = row.id;
				var userName = row.name;
				var userEmailId = row.emailid;
				var emailExists = false;
				$("label[name='newCustomerEmailID']").each(function(index) { //For each of inputs
				    if (userEmailId === $(this).text()) { //if match against array
				    	messages.push(userEmailId + "\n");
				        emailExists = true;
				    } 
				});
				$("label[name='existingUserEmailID']").each(function(index) { //For each of inputs
				    if (userEmailId === $(this).text()) { //if match against array
				    	messages.push(userEmailId + "\n");
				        emailExists = true;
				    } 
				});
				if(!emailExists){
					var row = table.insertRow(rowCount);
					var newcell0 = row.insertCell(0);
					newcell0.innerHTML = "<td><input type='checkbox' name='checkbox' checked='checked' id='checkbox"
							+ rowCount
							+ "' value='"
							+ userId
							+ "' onclick='enable_cb1(" + rowCount + ")'></td>";
					var newcell1 = row.insertCell(1);
					newcell1.innerHTML = "<td><label>" + userName + "</label><td>";
					var newcell2 = row.insertCell(2);
					newcell2.innerHTML = "<td><label name='newCustomerEmailID'>" + userEmailId
							+ "</label><td>";
					var newcell3 = row.insertCell(3);
					newcell3.innerHTML = "<td><input type='checkbox' name='checkboxIsTo' value='" + userId + "' id='checkboxIsTo" + rowCount + "'></td>";
					var newcell4 = row.insertCell(4);
					newcell4.innerHTML = "<td><input type='checkbox' name='checkboxIsCC' value='" + userId + "' id='checkboxIsCC" + rowCount + "' ></td>";
					var newcell5 = row.insertCell(5);
					newcell5.innerHTML = "<td><input type='checkbox' name='checkboxIsBCC' value='" + userId + "' id='checkboxIsBCC" + rowCount + "' ></td>";
				}
			}
			idsOfSelectedRows1 = [];
			if(messages.length > 1){
				alert(messages.join(""));
			}
			$("#customerTable").css('display', 'block');
			$("#customer").dialog("close");
		} else {
			alert("Choose atleast one user");
		}
	}

	function createVendorTable() {
		var grid = jQuery("#gridtable");
		var table = document.getElementById("dynamicVendorTable");
		var messages = [];
		messages.push("The Following EmailID's already selected. You cannot add it again.\n");
		if (idsOfSelectedRows.length > 0) {
			for ( var i = 0, il = idsOfSelectedRows.length; i < il; i++) {
				var rowCount = table.rows.length;
				var row = grid.getLocalRow(idsOfSelectedRows[i]);
				var vendorId = row.id;
				var vendorName = row.name;
				var vendorEmailId = row.emailid;
				var vname = row.vendorname;
				var primeNonprime = row.prime;
				var vendorstatus=row.vendorstatus;
				var emailExists = false;
				$("label[name='newVendorEmailID']").each(function(index) { //For each of inputs
				    if (vendorEmailId === $(this).text()) { //if match against array
				    	messages.push(vendorEmailId + "\n");
				        emailExists = true;
				    } 
				});
				$("label[name='existingVendorEmailID']").each(function(index) { //For each of inputs
				    if (vendorEmailId === $(this).text()) { //if match against array
				    	messages.push(vendorEmailId + "\n");
				        emailExists = true;
				    } 
				});
				if(!emailExists){
					var row = table.insertRow(rowCount);
					var newcell0 = row.insertCell(0);
					newcell0.innerHTML = "<td><input type='checkbox' name='checkbox2' checked='checked' id='checkboxv"
							+ rowCount
							+ "' value='"
							+ vendorId
							+ "' onclick='enable_cb2(" + rowCount + ")'></td>";
					var newcell1 = row.insertCell(1);
					newcell1.innerHTML = "<td><label>" + vendorName
							+ "</label><td>";
					var newcell2 = row.insertCell(2);
					newcell2.innerHTML = "<td><label name='newVendorEmailID'>" + vendorEmailId
							+ "</label><td>";
					var newcell3 = row.insertCell(3);
					newcell3.innerHTML = "<td><label>" + vname + "</label><td>";
					var newcell4 = row.insertCell(4);
					newcell4.innerHTML = "<td><label>" + primeNonprime + "</label><td>";
					var newcell5 = row.insertCell(5);
					newcell5.innerHTML = "<td><label>" + vendorstatus + "</label><td>";
					var newcell6 = row.insertCell(6);
					newcell6.innerHTML = "<td><input type='checkbox' name='vendorCheckboxIsTo' value='" + vendorId + "' id='vendorCheckboxIsTo" + rowCount + "' ></td>";
					var newcell7 = row.insertCell(7);
					newcell7.innerHTML = "<td><input type='checkbox' name='vendorCheckboxIsCC' value='" + vendorId + "' id='vendorCheckboxIsCC" + rowCount + "' ></td>";
					var newcell8 = row.insertCell(8);
					newcell8.innerHTML = "<td><input type='checkbox' name='vendorCheckboxIsBCC' value='" + vendorId + "' id='vendorCheckboxIsBCC" + rowCount + "' ></td>";
				}
			}
			idsOfSelectedRows = [];
			if(messages.length > 1){
				alert(messages.join(""));
			}
			$("#vendorTable").css('display', 'block');
			$("#vendor").dialog("close");
		} else {
			alert("Choose atleast one vendor");
		}
	}
	
	function cancelCustomer() {
		$("#customer").dialog("close");
	}
	
	function cancelVendor() {
		$("#vendor").dialog("close");
	}
</script>

<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-12 mx-auto">	
		<div id="successMsg">
			<html:messages id="msg" property="successMsg" message="true">
				<div class="alert alert-info nomargin">
					<bean:write name="msg" />
				</div>
			</html:messages>
		</div>
		<div id="failureMsg">
			<html:messages id="msg" property="errorMsg" message="true">
				<div class="alert alert-info nomargin">
					<bean:write name="msg" />
				</div>
			</html:messages>
		</div>
		
<div class="form-box card-body">
	<header class="card-header">
		<h2 class="card-title pull-left">Email Distribution</h2>
	</header>
	<html:form
		action="/updateemaildistributionlist?method=updateEmailDistributionList"
		styleId="emailDistributionForm" onsubmit="return validateForm();">
		<html:hidden property="emailDistributionMasterId"
			styleId="emailDistributionMasterId" />
					<div class="row">
					
						<div class="col-lg-12 mx-auto">
							<div class="form-box card-body" id="emailForm">	
								
								<div class="form-group row row-wrapper">`
									<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Email Distribution List</label>
								<div class="col-sm-9 ctrl-col-wrapper col-lg-6">
									<html:text property="emailDistributionListName" alt=""
										styleClass="text-box form-control" styleId="emailDistributionListName" />
								</div>
							</div>			
							<footer class="card-footer mt-4">
								<div class="row justify-content-end">
									<div class="col-sm-9 wrapper-btn">
								<html:submit value="Update" styleClass="btn btn-primary" styleId="submit"></html:submit>
								<html:reset value="Cancel" styleClass="btn btn-default" onclick="clearfields();"
									styleId="clear"></html:reset>
								</div>
							</div>
						</footer>
						<div class="clear"></div>
					</div>
	<div class="grid-wrapper">
		<div class="form-box">
			<div class="col-lg-12 mx-auto">
			<div id="customer" style="display: none; width:100% !important;">
				<div id="grid_container1" class="card-body customer-email-table vendor-table">
					<table id="gridtable1"  style="width:100% !important;" class="table table-bordered table-striped mb-0">
						<tr>
							<td />
						</tr>
					</table>
					<div id="pager1"></div>
				</div>
				<footer class="card-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<button class="exportBtn btn btn-primary" onclick="createCustomerTable();">Ok</button>
				 			<input type="button" value="Cancel" class="exportBtn btn btn-default" onclick="cancelCustomer();">
				 		</div>
				 	</div>
				 </footer>
			</div>
			<div id="vendor" style="display: none; width:100% !important;">
				<div id="grid_container" class="card-body vendor-table">
					<table id="gridtable" style="width:100% !important;" title="Vendors" class="main-table table table-bordered ui-jqgrid-btable table-striped mb-0">
						<tr>
							<td />
						</tr>
					</table>
					<div id="pager"></div>
				</div>
				<footer class="card-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<input type="button" value="Ok" class="btn btn-primary exportBtn"
								onclick="createVendorTable();">
							<input type="button" value="Cancel" class="btn btn-default exportBtn" onclick="cancelVendor();">
						</div>
					</div>
				</footer>
			</div>
		<div id="customerContainer">
			<header class="card-header">
				<h2 class="card-title pull-left">Customer</h2>
				<div id="customerBtn" class="wrapper-btn pull-right">
					<input type="button" value="Add Customer User" class="btn btn-primary"
						onclick="addUser();">
				</div>
			</header>
			<div id="grid_container">
			<table class="main-table table table-bordered table-striped mb-0" id="dynamicCustomerTable">
				<logic:present name="disUserList">
					<thead>
						<tr>
							<td >Select <br>
							<input type="checkbox" id="cselectall"
								onclick="enableSelectAll('checkbox');"></td>
							<td>User Name</td>
							<td>EmailId</td>
							<td>O<br>
							<input type="checkbox" id="custtoselectall"
								onclick="enableCustomerSelectAll('to');"></td>
							<td>CC<br>
							<input type="checkbox" id="custccselectall"
								onclick="enableCustomerSelectAll('cc');"></td>
							<td>BCC<br>
							<input type="checkbox" id="custbccselectall" class="btn-primary"
								onclick="enableCustomerSelectAll('bcc');"></td>
						</tr>
					</thead>
					<tbody>
						<logic:iterate name="disUserList" id="detail">
							<tr>
								<td><input type="checkbox" name="checkbox1"
									value="<bean:write name="detail" property="emailDistributionListDetailId" />"
									checked="checked" onclick="enable_cb1('<%=count %>');" id="checkbox<%=count %>" /></td>
								<td><bean:write name="detail" property="firstName" /></td>
								<td><label name="existingUserEmailID"><bean:write name="detail" property="emailId" /></label></td>
								<td><logic:equal value="0" property="checkboxIsTo"
										name="detail">
										<input type="checkbox" name="checkboxIsTo" id="checkboxIsTo<%=count %>"
											value="<bean:write name="detail" property="id" />" />
									</logic:equal> <logic:notEqual value="0" property="checkboxIsTo"
										name="detail">
										<input type="checkbox" name="checkboxIsTo" id="checkboxIsTo<%=count %>"
											value="<bean:write name="detail" property="id" />"
											checked="checked" />
									</</logic:notEqual></td>

								<td><logic:equal value="0" property="checkboxIsCC"
										name="detail">
										<input type="checkbox" name="checkboxIsCC" id="checkboxIsCC<%=count %>"
											value="<bean:write name="detail" property="id" />" />
									</logic:equal> <logic:notEqual value="0" property="checkboxIsCC"
										name="detail">
										<input type="checkbox" name="checkboxIsCC" id="checkboxIsCC<%=count %>"
											value="<bean:write name="detail" property="id" />"
											checked="checked" />
									</</logic:notEqual></td>

								<td><logic:equal value="0" property="checkboxIsBCC"
										name="detail">
										<input type="checkbox" name="checkboxIsBCC" id="checkboxIsBCC<%=count %>"
											value="<bean:write name="detail" property="id" />" />
									</logic:equal> <logic:notEqual value="0" property="checkboxIsBCC"
										name="detail">
										<input type="checkbox" name="checkboxIsBCC" id="checkboxIsBCC<%=count %>"
											value="<bean:write name="detail" property="id" />"
											checked="checked" />
									</</logic:notEqual></td>
							</tr>
							<%count++; %>
						</logic:iterate>
				</logic:present>
				</tbody>
			</table>
		</div>
	</div>
		<div id="vendorContainer" class="mt-2">
			<header class="card-header">
				<h2 class="card-title pull-left">Vendor</h2>
				<div id="vendorBtn" class="wrapper-btn pull-right">
					<input type="button" value="Add Vendor User" class="btn btn-primary"
						onclick="addVendor();">
				</div>
			</header>
			<div id="tableId">
				<table class="rt cf main-table table table-bordered table-striped ui-jqgrid-btable mb-0" id="dynamicVendorTable">
					<logic:present name="distributionList">
						<thead class="cf">
							<tr>
								<th>Select<br><input type="checkbox" id="vselectall"
									onclick="enableSelectAll('checkbox2');"></th>
								<th>User Name</th>
								<th>EmailId</th>
								<th>Vendor</th>
								<th>Vendor Type</th>
								<th>Status</th>
								<th>TO<br>
								<input type="checkbox" id="ventoselectall"
									onclick="enableVendorSelectAll('to');"></th>
								<th>CC<br>
								<input type="checkbox" id="venccselectall"
									onclick="enableVendorSelectAll('cc');"></th>
								<th>BCC<br>
								<input type="checkbox" id="venbccselectall"
									onclick="enableVendorSelectAll('bcc');"></th>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="distributionList" id="detail">
								<tr>
									<td><input type="checkbox" name="checkbox3"
										value="<bean:write name="detail" property="emailDistributionListDetailId" />"
										checked="checked" onclick="enable_cb2('<%=count1 %>');" id="checkboxv<%=count1 %>" /></td>
									<td><bean:write name="detail" property="firstName" /></td>
									<td><label name="existingVendorEmailID"><bean:write name="detail" property="emailId" /></label></td>
									<td><bean:write name="detail" property="vendorName" /></td>
									<td><logic:equal value="0" property="primrNonPrime"
											name="detail">
											Non-Prime
										</logic:equal> 
										<logic:notEqual value="0" property="primrNonPrime"
											name="detail">
											Prime
										</logic:notEqual>
									</td>
									<td><bean:write name="detail" property="vendorStatus" /></td>
									<td><logic:equal value="0" property="vendorCheckboxIsTo"
											name="detail">
											<input type="checkbox" name="vendorCheckboxIsTo"
												id="vendorCheckboxIsTo<%=count1 %>"
												value="<bean:write name="detail" property="id" />" />
										</logic:equal> <logic:notEqual value="0" property="vendorCheckboxIsTo"
											name="detail">
											<input type="checkbox" name="vendorCheckboxIsTo"
												id="vendorCheckboxIsTo<%=count1 %>"
												value="<bean:write name="detail" property="id" />"
												checked="checked" />
										</logic:notEqual></td>
	
									<td><logic:equal value="0" property="vendorCheckboxIsCC"
											name="detail">
											<input type="checkbox" name="vendorCheckboxIsCC"
												id="vendorCheckboxIsCC<%=count1 %>"
												value="<bean:write name="detail" property="id" />" />
										</logic:equal> <logic:notEqual value="0" property="vendorCheckboxIsCC"
											name="detail">
											<input type="checkbox" name="vendorCheckboxIsCC"
												id="vendorCheckboxIsCC<%=count1 %>"
												value="<bean:write name="detail" property="id" />"
												checked="checked" />
										</logic:notEqual></td>
	
									<td><logic:equal value="0" property="vendorCheckboxIsBCC"
											name="detail">
											<input type="checkbox" name="vendorCheckboxIsBCC"
												id="vendorCheckboxIsBCC<%=count1 %>"
												value="<bean:write name="detail" property="id" />" />
										</logic:equal> <logic:notEqual value="0" property="vendorCheckboxIsBCC"
											name="detail">
											<input type="checkbox" name="vendorCheckboxIsBCC"
												id="vendorCheckboxIsBCC<%=count1 %>"
												value="<bean:write name="detail" property="id" />"
												checked="checked" />
										</logic:notEqual></td>
								</tr>
								<%count1++; %>
							</logic:iterate>
					</logic:present>
					</tbody>
				</table>
			</div>
		</div>
		
		<div id="grid_container" class="mt-2">
		<table id="emailDistributionList" class="table main-table card-body table-bordered table-striped mb-0">
			<thead>
				<tr>
					<td>Email Distribution List</td>
					<td>Action</td>
				</tr>
			</thead>
			<logic:iterate name="distributionModels" id="emailDistributionList">
				<tbody>
					<tr>
						<td><bean:write name="emailDistributionList"
								property="emailDistributionListName" /></td>
						<td><logic:iterate id="privilege" name="privileges">
								<logic:match value="Email Distribution" name="privilege"
									property="objectId.objectName">
									<logic:match value="1" name="privilege" property="modify">
										<bean:define id="mailId" name="emailDistributionList"
											property="emailDistributionMasterId"></bean:define>
										<html:link
											action="/editemaildistribution.do?method=editEmailDistribution"
											paramId="emailDistributionMasterId" paramName="mailId">Edit</html:link>
							| 
										</logic:match>
								</logic:match>
							</logic:iterate> <logic:iterate id="privilege" name="privileges">
								<logic:match value="Email Distribution" name="privilege"
									property="objectId.objectName">
									<logic:match value="1" name="privilege" property="delete">
										<html:link
											action="/deleteemaildistribution.do?method=deleteEmailDistribution"
											paramId="emailDistributionMasterId" paramName="mailId"
											onclick="return confirm_delete();">Delete</html:link>
									</logic:match>
								</logic:match>
							</logic:iterate></td>
					</tr>
				</tbody>
			</logic:iterate>
		</table>
		</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</html:form>
	</div>
	</div>
	</div>
</section>

