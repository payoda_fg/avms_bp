<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />

<script type="text/javascript">
var currentUserId=<%=(Integer) session.getAttribute("userId")%>;
var currentDivision;
function clearfields() {
	window.location = "customerDivision.do?method=showMangeDivisionPage";
}
function cancelCustomer() {
	$("#customer").dialog("close");
	
}
function cancelDivisionUser() {
	$("#divisionUser").dialog("close");
}

	$(document).ready(function() {
		placingReadOnly();
		
	jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
		}, "No Special Characters Allowed.");
		$("#formID").validate({
			rules : {
				divisionName : {
					required : true,
					maxlength:255,
					alpha : true
				},
				divisionShortName : {
					minlength:2,
					maxlength:50,
					alpha : true
				},
				isActive : {
					required : true
				}/* ,
				checkList : {
					required : true
				},
				certificationAgencies : {
					required : true
				},
				registrationRequest : {
					required : true
				},
				registrationQuestion : {
					required : true
				} */
			},
		});
	});

	function assignToUsers(){
		var userIds=idsOfSelectedRows1;
		var divisionId=currentDivision;		
		if(idsOfSelectedRows1.length > 0){
		$.ajax({
			url : "customerDivision.do?method=assignToUsers&userIds="+userIds+
					"&divisionId="+divisionId,
					type:"POST",
					beforeSend : function(){
						$("#ajaxloader").show(); 
					},success : function(data) {
						$("#ajaxloader").hide();
						$("#customer").dialog("close");
						alert("Customer Division Successfully Assigned to Selected Users ");
						idsOfSelectedRows1 = [];
						
					}
		});
		}
		else
			alert("Select atleast one user");
	}
	
	function removeDivisionFromUsers(){
		var userIds=idsOfSelectedRows2;
		var divisionId=currentDivision;	
		var i=0;
		var flag=0;
		if(idsOfSelectedRows2.length > 0){
			for(i=0;i<idsOfSelectedRows2.length;i++)
			{
				if(currentUserId==idsOfSelectedRows2[i])
				{
					flag=1;
					break;
				}	
			}
			if(flag==1)
			{
				alert("Cannot Remove Division for Logged in User, IsDivision is Active.");
			}
			else{
				 	$.ajax({
						url : "customerDivision.do?method=removeDivisionFromUsers&userIds="+userIds+
								"&divisionId="+divisionId,
								type:"POST",
								beforeSend : function(){
									$("#ajaxloader").show(); 
								},success : function(data) {
										if(data==1)
										{
											$("#ajaxloader").hide();
											$("#divisionUser").dialog("close");
											alert("Customer Division Successfully Removed From Selected Users ");
											idsOfSelectedRows2 = [];
										}
										else if(data==0){	
											$("#ajaxloader").hide();
											$("#divisionUser").dialog("close");
											alert("Sorry Cannot Delete Division, IsDivision is Active ");
											clearfields();
										}
								}		
						}); 
			}
		}
		else
			alert("Select atleast one user");
	}
	
function assignDivisionToUsers(divisionId){
	currentDivision=divisionId;
		// Ajax function to load customer user records
		$.ajax({
				url : "customerDivision.do?method=getCustomerContacts&random="
						+ Math.random(),
				type : "POST",
				async : true,
				beforeSend : function(){
						$("#ajaxloader").show(); 
				},success : function(data) {				
						$("#ajaxloader").hide();
						// 	populateUsers(data);
						$("#gridtable1").jqGrid('GridUnload');
						gridCustomerContact(data);
			}
		});
			$("#customer").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#customer").dialog({
				width : 660,
				height : 330
			});
	}
 
function populateUsersFromDivision(divisionId){
	currentDivision=divisionId;
		// Ajax function to load customer user records
		$.ajax({
				url : "customerDivision.do?method=getUsersDivision&divisionId="
					+ divisionId,
					type : "POST",
				async : true,
				beforeSend : function(){
						$("#ajaxloader").show(); 
				},success : function(data) {	
						$("#ajaxloader").hide();
						$("#gridtable2").jqGrid('GridUnload');
						griddivisionContact(data);
				
			}
		});
			$("#divisionUser").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#divisionUser").dialog({
				width : 650,
				height : 330
			});
	}

	 var idsOfSelectedRows1 = [];
		function gridCustomerContact(myGridData){
		var newdata1 = jQuery.parseJSON(myGridData);
		$('#gridtable1').jqGrid({
			data : newdata1,
			datatype : 'local',
			colNames : [ 'Select','Id', 'Name', 'Email ID','Current Division' ],
			colModel : [ 
			 {
				name : 'divisionId',
				index : 'divisionId',
				editable: true,
				hidden: true
			 },
			 {
					name : 'id',
					index : 'id',
					editable: true,
					hidden: true
				 },
				 {
				name : 'name',
				index : 'name',
				width : 200,
				align : 'left'
			}, {
				name : 'emailid',
				index : 'emailid',
				width : 250,
				align : 'left'
			},{
				name : 'customerDivisionId',
				index : 'customerDivisionId',
				width : 250,
				align : 'left'
			}],
			rowNum : 10,
			rowList : [ 10, 20, 50, 100 ],
			pager : '#pager1',
			viewrecords : true,
			width : 'auto',
			emptyrecords : 'No data available', 
			onSelectRow: function (id, isSelected) {
	            var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows1);
	            item.cb = isSelected;
	            if (!isSelected && i >= 0) {
	                idsOfSelectedRows1.splice(i,1); // remove id from the list
	            } else if (i < 0) {
	                idsOfSelectedRows1.push(id);
	            }
	        },
	        onSelectAll:function(id,isSelected){
	        	
	            for(var j=0;j<id.length;j++){
	            	//console.log(status);
	            	var p = this.p, item = p.data[p._index[id[j]]], i = $.inArray(id[j], idsOfSelectedRows1);
		            item.cb = isSelected;
		            if (!isSelected && i >= 0) {
		                idsOfSelectedRows1.splice(i,1); // remove id from the list
		            } else if (i < 0) {
		                idsOfSelectedRows1.push(id[j]);
		            }
	            }
	        },
	        loadComplete: function () {
	            var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
	            for (i = 0, selCount = idsOfSelectedRows1.length; i < selCount; i++) {
	                rowid = idsOfSelectedRows1[i];
	                item = data[index[rowid]];
	                if ('cb' in item && item.cb) {
	                    $this.jqGrid('setSelection', rowid, false);
	                }
	            }
	        },  
			multiselect : true,
			cmTemplate: { title: false },
			height : 'auto'
		});
	    return idsOfSelectedRows1;
	}
		
		  var idsOfSelectedRows2 = [];
			function griddivisionContact(divisionData){
			var newdata2 = jQuery.parseJSON(divisionData);
			$('#gridtable2').jqGrid({
				data : newdata2,
				datatype : 'local',
				colNames : [ 'Select','Id', 'Name', 'Email ID','Current Division' ],
				colModel : [ 
				 {
					name : 'divisionId',
					index : 'divisionId',
					editable: true,
					hidden: true
				 },
				 {
						name : 'id',
						index : 'id',
						editable: true,
						hidden: true
					 },
					 {
					name : 'name',
					index : 'name',
					width : 200,
					align : 'left'
				}, {
					name : 'emailid',
					index : 'emailid',
					width : 250,
					align : 'left'
				},{
					name : 'customerDivisionId',
					index : 'customerDivisionId',
					width : 250,
					align : 'left'
				}],
				rowNum : 10,
				rowList : [ 10, 20, 50, 100 ],
				pager : '#pager1',
				viewrecords : true,
				width : 'auto',
				emptyrecords : 'No data available', 
				onSelectRow: function (id, isSelected) {
		            var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows2);
		            item.cb = isSelected;
		            if (!isSelected && i >= 0) {
		                idsOfSelectedRows2.splice(i,1); // remove id from the list
		            } else if (i < 0) {
		                idsOfSelectedRows2.push(id);
		            }
		        },
		        onSelectAll:function(id,isSelected){
		        	
		            for(var j=0;j<id.length;j++){
		            	//console.log(status);
		            	var p = this.p, item = p.data[p._index[id[j]]], i = $.inArray(id[j], idsOfSelectedRows2);
			            item.cb = isSelected;
			            if (!isSelected && i >= 0) {
			                idsOfSelectedRows2.splice(i,1); // remove id from the list
			            } else if (i < 0) {
			                idsOfSelectedRows2.push(id[j]);
			            }
		            }
		        },
		        loadComplete: function () {
		            var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
		            for (i = 0, selCount = idsOfSelectedRows2.length; i < selCount; i++) {
		                rowid = idsOfSelectedRows2[i];
		                item = data[index[rowid]];
		                if ('cb' in item && item.cb) {
		                    $this.jqGrid('setSelection', rowid, false);
		                }
		            }
		        },  
				multiselect : true,
				cmTemplate: { title: false },
				height : 'auto'
			});
		    return idsOfSelectedRows2;
		}		
			
function placingReadOnly()
{
	<%String readOnlyStatus = (String) session.getAttribute("divisionReadyOnlyStatus");%>
	var status = "<%=readOnlyStatus%>";
	if(status == "yes")
	{
		document.getElementById("divisionName").readOnly = true;
		document.getElementById("divisionShortName").readOnly = true;
		document.getElementById("checkList").readOnly = true;
		document.getElementById("certificationAgencies").readOnly = true;
		document.getElementById("registrationRequest").readOnly = true;
		document.getElementById("registrationQuestion").readOnly = true;
		document.getElementById("isActive1").disabled= true;
		document.getElementById("isActive2").disabled= true;
		document.getElementById("isGlobal1").disabled= true;
		document.getElementById("isGlobal2").disabled= true;
	}
}

function checkIsGlobalStatus(divisionId){
	
	$.ajax({
		url : "customerDivision.do?method=checkIsGlobalDivisionAvailable&divisionId="+divisionId,
				type:"POST",
				beforeSend : function(){
					$("#ajaxloader").show(); 
				},success : function(data) {
					$("#ajaxloader").hide();
					if(data.globalDivision == 1)
					{
						alert("IsGlobal Division Already Available.");
						$("#isGlobal1").prop("checked", false)
						$("#isGlobal2").prop("checked", true)
					}
				}
	});
}
</script>

<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<div id="successMsg">
				<html:messages id="msg" property="successMsg" message="true">
					<div class="alert alert-info nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-danger nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
				<html:messages id="msg" property="divisionReference" message="true">
					<div class="alert alert-info nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
				<html:messages id="msg" property="updateMsg" message="true">
					<div class="alert alert-info nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
				<html:messages id="msg" property="updateReference" message="true">
					<div class="alert alert-info nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
			</div>

			<!--   Adding the New Managing Division -->
			<logic:iterate id="privi" name="privileges">
				<logic:match value="Manage Division" name="privi"
					property="objectId.objectName">
					<logic:match value="1" name="privi" property="add">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title pull-left">Manage Divisions</h2>
							</header>
							<div class="form-box card-body">
								<html:form action="customerDivision.do?method=addDivision"
									styleClass="FORM" styleId="formID">
									<html:javascript formName="certifyingAgencyForm" />
									<logic:iterate id="privilege" name="privileges">
										<logic:match value="Certifying Agency" name="privilege"
											property="objectId.objectName">
											<logic:match value="1" name="privilege" property="add">
												<html:hidden property="id" styleClass="text-box"
													name="customerDivisionForm" alt="" styleId="id"></html:hidden>

												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Division
														Name</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:text property="divisionName"
															styleClass="text-box form-control"
															name="customerDivisionForm" alt="" styleId="divisionName"></html:text>
													</div>
													<span class="error"><html:errors
															property="divisionName"></html:errors></span>
												</div>
												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Division
														Short Name</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:text property="divisionShortName"
															styleClass="text-box form-control"
															name="customerDivisionForm" alt="Optional"
															styleId="divisionShortName"></html:text>
													</div>
												</div>

												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Check
														List</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:textarea property="checkList"
															name="customerDivisionForm" alt="Optional"
															styleId="checkList"
															styleClass="main-text-area form-control" rows="10"
															cols="50" />
													</div>
												</div>
												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Certification
														Agencies</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:textarea property="certificationAgencies"
															name="customerDivisionForm"
															styleId="certificationAgencies"
															styleClass="main-text-area form-control" rows="10"
															cols="30" />
													</div>
												</div>


												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Registration
														Request</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:textarea property="registrationRequest"
															name="customerDivisionForm" alt="Optional"
															styleId="registrationRequest"
															styleClass="main-text-area form-control" rows="10"
															cols="30" />
													</div>
												</div>
												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Registration
														Question</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:textarea property="registrationQuestion"
															name="customerDivisionForm"
															styleId="registrationQuestion"
															styleClass="main-text-area form-control" rows="10"
															cols="30" />
													</div>
												</div>


												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Is
														Active</label>
													<div class="col-sm-9 ctrl-col-wrapper" id="status">
														<html:radio property="isActive" value="1"
															name="customerDivisionForm" styleId="isActive1">&nbsp;Yes&nbsp;</html:radio>
														<html:radio property="isActive" value="0"
															name="customerDivisionForm" styleId="isActive2">&nbsp;No&nbsp;</html:radio>
													</div>
												</div>
												<div class="row-wrapper form-group row">
													<label
														class="col-sm-3 label-col-wrapper control-label text-sm-right">Is
														Global</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<html:radio property="isGlobal" value="1"
															name="customerDivisionForm" styleId="isGlobal1"
															onclick="checkIsGlobalStatus('${customerDivisionForm.id}')">&nbsp;Yes&nbsp;</html:radio>
														<html:radio property="isGlobal" value="0"
															name="customerDivisionForm" styleId="isGlobal2">&nbsp;No&nbsp;</html:radio>
													</div>
												</div>
												<footer class="mt-2 card-footer">
													<div class="row justify-content-end">
														<div class="col-sm-9 wrapper-btn">
															<logic:present name="customerDivisionForm" property="id">
																<html:submit value="Update" styleClass="btn btn-primary"
																	styleId="submit1"></html:submit>
															</logic:present>
															<logic:notPresent name="customerDivisionForm"
																property="id">
																<html:submit value="Submit" styleClass="btn btn-primary"
																	styleId="submit1"></html:submit>
															</logic:notPresent>
															<html:reset value="Cancel" styleClass="btn btn-default"
																onclick="clearfields();"></html:reset>
														</div>
													</div>
												</footer>
											</logic:match>
										</logic:match>
									</logic:iterate>
								</html:form>
							</div>
						</section>
					</logic:match>
				</logic:match>
			</logic:iterate>
			<!--    For Manage Division Role Editing  ( When Manage Division role did not have the Add privilege from Administrator) -->
			<logic:iterate id="privi" name="privileges">
				<logic:match value="Manage Division" name="privi"
					property="objectId.objectName">
					<logic:match value="0" name="privi" property="add">
						<logic:notPresent name="customerDivisionForm" property="id">
							<div>

								<h3 class="card-title">You have no right to add new Manage
									Division</h3>
							</div>
						</logic:notPresent>
						<logic:present name="customerDivisionForm" property="id">
							<logic:match value="1" name="privi" property="modify">
								<section class="card">
									<header class="card-header">
										<h2 class="card-title pull-left">Manage Divisions</h2>
									</header>
									<div class="form-box card-body">
										<html:form action="customerDivision.do?method=addDivision"
											styleClass="FORM" styleId="formID">
											<html:javascript formName="certifyingAgencyForm" />
											<logic:iterate id="privilege" name="privileges">
												<logic:match value="Certifying Agency" name="privilege"
													property="objectId.objectName">
													<logic:match value="1" name="privilege" property="add">
														<html:hidden property="id" styleClass="text-box"
															name="customerDivisionForm" alt="" styleId="id"></html:hidden>


														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Division
																Name</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:text property="divisionName" styleClass="text-box form-control"
																	name="customerDivisionForm" alt=""
																	styleId="divisionName"></html:text>
															</div>
															<span class="error"><html:errors
																	property="divisionName"></html:errors></span>
														</div>
														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Division
																Short Name</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:text property="divisionShortName"
																	styleClass="text-box form-control" name="customerDivisionForm"
																	alt="Optional" styleId="divisionShortName"></html:text>
															</div>
														</div>


														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Check
																List</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:textarea property="checkList"
																	name="customerDivisionForm" 
																	alt="Optional" styleId="checkList"
																	styleClass="main-text-area form-control" rows="10" cols="50" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Certification
																Agencies</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:textarea property="certificationAgencies"
																	name="customerDivisionForm"
																	styleId="certificationAgencies"
																	styleClass="main-text-area form-control" rows="10" cols="30" />
															</div>
														</div>

														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Registration
																Request</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:textarea property="registrationRequest"
																	name="customerDivisionForm" alt="Optional" styleId="registrationRequest"
																	styleClass="main-text-area form-control" rows="10" cols="30" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Registration
																Question</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:textarea property="registrationQuestion"
																	 name="customerDivisionForm"
																	styleId="registrationQuestion"
																	styleClass="main-text-area form-control" rows="10" cols="30" />
															</div>
														</div>

														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Is
																Active</label>
															<div class="col-sm-9 ctrl-col-wrapper" id="status">
																<html:radio property="isActive" value="1"
																	name="customerDivisionForm" styleId="isActive">&nbsp;Yes&nbsp;</html:radio>
																<html:radio property="isActive" value="0"
																	name="customerDivisionForm" styleId="isActive">&nbsp;No&nbsp;</html:radio>
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Is
																Global</label>
															<div class="col-sm-9 ctrl-col-wrapper">
																<html:radio property="isGlobal" value="1"
																	name="customerDivisionForm" styleId="isGlobal">&nbsp;Yes&nbsp;</html:radio>
																<html:radio property="isGlobal" value="0"
																	name="customerDivisionForm" styleId="isGlobal">&nbsp;No&nbsp;</html:radio>
															</div>
														</div>
														<footer class="mt-2 card-footer">
															<div class="row justify-content-end">
																<div class="col-sm-9 wrapper-btn">
																	<html:submit value="Update" styleClass="btn btn-update btn-primary"
																		styleId="submit1"></html:submit>
																	<html:reset value="Cancel" styleClass="btn btn-default btn-default"
																		onclick="clearfields();"></html:reset>
																</div>
															</div>
														</footer>
													</logic:match>
												</logic:match>
											</logic:iterate>
										</html:form>
									</div>
								</section>
							</logic:match>
						</logic:present>
					</logic:match>
				</logic:match>
			</logic:iterate>

			<div id="customer" style="display: none;">
				<div id="grid_container1"
					class="form-box card-body">
					<div class="responsiveGrid">
					<table id="gridtable1" class="main-table table table-bordered table-striped mb-0">
						<tr>
							<td />
						</tr>
					</table>
					<div id="pager1"></div>
					</div>
				</div>
				<footer class="mt-2 card-footer">
					<div class="row justify-content-end">
						<div class="col-sm-9 wrapper-btn">
							<input type="button" value="Add" class="exportBtn btn-primary"
						onclick="assignToUsers();"> <input type="button"
						value="Cancel" class="exportBtn btn-default" onclick="cancelCustomer();">
						</div>
					</div>
				</footer>
			</div>

			<div id="divisionUser" style="display: none;">
				<div id="grid_container1"
					class="form-box card-body">
					<div class="responsiveGrid">
					<table id="gridtable2" class="main-table table table-bordered table-striped mb-0">
						<tr>
							<td />
						</tr>
					</table>
					<div id="pager1"></div>
					</div>
				</div>
				<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<input type="button" value="Remove" class="btn-primary exportBtn"
							onclick="removeDivisionFromUsers();"> 
							<input type="button"
							value="Cancel" class="btn-default exportBtn" onclick="cancelDivisionUser();">
						</div>
					</div>
				</footer>
			</div>

			<!--   View the Manage Divisions -->
			<logic:iterate id="privilege" name="privileges">
				<logic:match value="Manage Division" name="privilege"
					property="objectId.objectName">
					<logic:match value="1" name="privilege" property="view">
						<header class="card-header">
							<h2 class="card-title pull-left">Available Divisions</h2>
						</header>

						<div class="form-box card-body">
							<table border="0" class="main-table table table-bordered table-striped mb-0 table-no-more">
								<bean:size id="size" name="customerDivisions" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<td>Division Name</td>
											<td>Short Name</td>
											<td>Is Global</td>
											<td>Is Active</td>
											<td>Action</td>
										</tr>
									</thead>
									<tbody>
										<logic:iterate name="customerDivisions" id="divisionList">
											<tr>
												<td data-title="Division Name"><bean:write name="divisionList"
														property="divisionname" /></td>
												<td data-title="Short Name"><bean:write name="divisionList"
														property="divisionshortname" /></td>
												<td data-title="Is Global" align="center"><html:checkbox property="isGlobal"
														name="divisionList" disabled="true" value="1"></html:checkbox></td>
												<td data-title="Is Active" align="center"><html:checkbox property="isactive"
														name="divisionList" disabled="true" value="1"></html:checkbox></td>
												<bean:define id="divisionId" name="divisionList"
													property="id"></bean:define>
												<td data-title="Action" align="center"><logic:iterate id="privilege1"
														name="privileges">
														<logic:match value="Manage Division" name="privilege1"
															property="objectId.objectName">
															<logic:match value="1" name="privilege1"
																property="modify">
																<html:link
																	action="/customerDivision.do?method=editDivision"
																	paramId="id" paramName="divisionId"> Edit </html:link>
															</logic:match>
														</logic:match>
													</logic:iterate> &nbsp;&nbsp; <logic:iterate id="privilege1"
														name="privileges">
														<logic:match value="Manage Division" name="privilege1"
															property="objectId.objectName">
															<logic:match value="1" name="divisionList"
																property="isactive">
																<%-- 	<logic:match value="1" name="privilege1" property="delete">
													<html:link
														action="/customerDivision.do?method=deleteDivision"
														paramId="id" paramName="divisionId"
														onclick="return confirm_delete();"> Delete </html:link>
												</logic:match> --%>
															</logic:match>
															<logic:match value="0" name="divisionList"
																property="isactive">
																<!-- <span style="cursor:not-allowed; color: #ABF1A8">Delete</span> -->
																<!-- 													<label style="color: #ABF1A8">Delete</label> -->
															</logic:match>
														</logic:match>
													</logic:iterate> <logic:equal value="1" name="divisionList"
														property="isactive">
									&nbsp;&nbsp;	<a href="Javascript:void ( 0 ) ;"
															onclick="assignDivisionToUsers(${divisionList.id});">
															Assign to Users </a>
									&nbsp;&nbsp;	<a href="Javascript:void ( 0 ) ;"
															onclick="populateUsersFromDivision(${divisionList.id});">
															Remove Users </a>
													</logic:equal></td>

											</tr>
										</logic:iterate>
									</tbody>
								</logic:greaterThan>
								<logic:equal value="0" name="size">
	                   No such Records
	                </logic:equal>
							</table>
						</div>
					</logic:match>
				</logic:match>
			</logic:iterate>

			<logic:iterate id="privilege" name="privileges">
				<logic:match value="Manage Division" name="privilege"
					property="objectId.objectName">
					<logic:match value="0" name="privilege" property="view">
						<div style="padding: 5%; text-align: center;">
							<h3>You have no rights to view Manage Divisions</h3>
						</div>
					</logic:match>
				</logic:match>
			</logic:iterate>

		</div>
	</div>
</section>