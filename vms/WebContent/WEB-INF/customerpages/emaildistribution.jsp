<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<!-- <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />-->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" /> 
	
<%
	Integer count = 0;
	Integer count1 = 0;
	Integer count2 = 0;
%>
<script>

	$(document).ready(function() {
		$("#emailDistributionForm").validate({
			rules : {
				emailDistributionListName : {
					required : true
				}
			}
		});

	});

	function enable_cb1(index) {
	  if ($("#checkbox"+index).is(':checked')){
		  $("#checkboxIsTo"+ index).removeAttr("disabled");
		  $("#checkboxIsCC"+ index).removeAttr("disabled");
		  $("#checkboxIsBCC"+ index).removeAttr("disabled");
	  } else {
		  $("#checkboxIsTo"+ index).attr("disabled", true);
		  $("#checkboxIsCC"+ index).attr("disabled", true);
		  $("#checkboxIsBCC"+ index).attr("disabled", true);
	  }
	}

	function enable_cb2(index) {
	  if ($("#checkboxv"+index).is(':checked')){
		  $("#vendorCheckboxIsTo"+ index).removeAttr("disabled");
		  $("#vendorCheckboxIsCC"+ index).removeAttr("disabled");
		  $("#vendorCheckboxIsBCC"+ index).removeAttr("disabled");
	  } else {
		  $("#vendorCheckboxIsTo"+ index).attr("disabled", true);
		  $("#vendorCheckboxIsCC"+ index).attr("disabled", true);
		  $("#vendorCheckboxIsBCC"+ index).attr("disabled", true);
	  }
	}
	// Function to reset the form
	function clearfields() {
		window.location = "emaildistribution.do?method=viewEmailDistribution";
	}
	//var selectedCustomerIds = [];
	
	function addUser(){
		
		// Ajax function to load customer user records
		$.ajax({
			url : "emaildistribution.do?method=getCustomerContacts&random="
					+ Math.random(),
			type : "POST",
			async : true,
			beforeSend : function(){
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#gridtable1").jqGrid('GridUnload');
				gridCustomerContact(data);
			}
		});
		
		$("#customer").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});
		$("#customer").dialog({
			width : 600,
			height : 350
		});
	}
	
	function addVendor(){
		// Ajax function to load vendor user records
		
		$.ajax({
			url : "emaildistribution.do?method=getVendorContacts&random="
					+ Math.random(),
			type : "POST",
			async : true,
			beforeSend : function(){
				$("#ajaxloader").show(); 
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#gridtable").jqGrid('GridUnload');
				gridVendorContact(data);
			}
		});
		
		$("#vendor").css({
			'display' : 'block',
			'font-size' : 'inherit'
		});
		$("#vendor").dialog({
			width : 900,
			height : 350
		});
	}
	
	function enableForm() {
		$("#emailDistributionListName").removeAttr("disabled", "disabled");
		$("#submit").removeAttr("disabled", "disabled");
		$("#clear").removeAttr("disabled", "disabled");
		$("#emailList1").css("display", "block");
		$("#emailList2").css("display", "block");
		$("#emailForm").css("display", "block");
		$("#emailDistributionListTable").css("display", "none");
		$("#newdistribution").css("display", "none");
		$("#emailDistributionListName").val('');
		$("#customerTable").css("display", "block");
		$("#vendorTable").css("display", "block");
		
	}
	
	var idsOfSelectedRows1 = [];
	function gridCustomerContact(myGridData){
		
		var newdata1 = jQuery.parseJSON(myGridData);
		$('#gridtable1').jqGrid({
			data : newdata1,
			datatype : 'local',
			colNames : [ 'Select', 'Name', 'Email ID' /* , 'TO', 'CC', 'BCC' */ ],
			colModel : [ 
			 {
				name : 'id',
				index : 'id',
				editable: true,
				hidden: true
			 },
			 {
				name : 'name',
				index : 'name'
			}, {
				name : 'emailid',
				index : 'emailid'
			}],
			rowNum : 10,
			rowList : [ 10, 20, 50, 100 ],
			pager : '#pager1',
			viewrecords : true,
			width : 'auto',
			emptyrecords : 'No data available', 
			onSelectRow: function (id, isSelected) {
	            var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows1);
	            item.cb = isSelected;
	            if (!isSelected && i >= 0) {
	                idsOfSelectedRows1.splice(i,1); // remove id from the list
	            } else if (i < 0) {
	                idsOfSelectedRows1.push(id);
	            }
	        },
	        onSelectAll:function(id,isSelected){
	        	
	            for(var j=0;j<id.length;j++){
	            	//console.log(status);
	            	var p = this.p, item = p.data[p._index[id[j]]], i = $.inArray(id[j], idsOfSelectedRows1);
		            item.cb = isSelected;
		            if (!isSelected && i >= 0) {
		                idsOfSelectedRows1.splice(i,1); // remove id from the list
		            } else if (i < 0) {
		                idsOfSelectedRows1.push(id[j]);
		            }
	            }
	        },
	        loadComplete: function () {
	            var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
	            for (i = 0, selCount = idsOfSelectedRows1.length; i < selCount; i++) {
	                rowid = idsOfSelectedRows1[i];
	                item = data[index[rowid]];
	                if ('cb' in item && item.cb) {
	                    $this.jqGrid('setSelection', rowid, false);
	                }
	            }
	        },  
			multiselect : true,
			cmTemplate: { title: false },
			height : 'auto'
		});

	    return idsOfSelectedRows1;
	}
	var idsOfSelectedRows = [];
	function gridVendorContact(myGridData){
		var newdata1 = jQuery.parseJSON(myGridData);
		$('#gridtable').jqGrid({
				data : newdata1,
				datatype : 'local',
				colNames : [ 'Select', 'Name', 'Email ID', 'Vendor Name' , 'Vendor Type','Vendor Status' /*, 'CC', 'BCC' */ ],
				colModel : [ 
				{
					name : 'id',
					index : 'id',
					editable: true,
					hidden: true
				}, {
					name : 'name',
					index : 'name'
				}, {
					name : 'emailid',
					index : 'emailid'
				}, {
					name : 'vendorname',
					index : 'vendorname'
				}, {
					name : 'prime',
					index : 'prime'
				}, {
					name : 'vendorstatus',
					index : 'vendorstatus'
				}],
				rowNum : 10,
				rowList : [ 10, 20, 50, 100, 200, 500, 1000 ],
				pager : '#pager',
				viewrecords : true,
				emptyrecords : 'No data available',
				onSelectRow: function (id, isSelected) {
		            var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
		            item.cb = isSelected;
		            if (!isSelected && i >= 0) {
		                idsOfSelectedRows.splice(i,1); // remove id from the list
		            } else if (i < 0) {
		                idsOfSelectedRows.push(id);
		            }
		        },
		        onSelectAll:function(id,isSelected){
		        	
		            for(var j=0;j<id.length;j++){
		            	//console.log(status);
		            	var p = this.p, item = p.data[p._index[id[j]]], i = $.inArray(id[j], idsOfSelectedRows);
			            item.cb = isSelected;
			            if (!isSelected && i >= 0) {
			                idsOfSelectedRows.splice(i,1); // remove id from the list
			            } else if (i < 0) {
			                idsOfSelectedRows.push(id[j]);
			            }
		            }
		        },
		        loadComplete: function () {
		            var p = this.p, data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
		            for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
		                rowid = idsOfSelectedRows[i];
		                item = data[index[rowid]];
		                if ('cb' in item && item.cb) {
		                    $this.jqGrid('setSelection', rowid, false);
		                }
		            }
		        }, 
				multiselect : true,
				cmTemplate: { title: false },
				height : 'auto'
			}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			refresh : false,
			search : false
		},

		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{});
		
	    return idsOfSelectedRows;
	}

	function validateForm() {
		var fields = $("input[name='checkbox']").serializeArray();
		var fields2 = $("input[name='checkbox2']").serializeArray();
		var checkboxIsTo=$("input[name='checkboxIsTo']").serializeArray();
		var checkboxIsCC=$("input[name='checkboxIsCC']").serializeArray();
		var checkboxIsBCC=$("input[name='checkboxIsBCC']").serializeArray();

		var vendorCheckboxIsTo=$("input[name='vendorCheckboxIsTo']").serializeArray();
		var vendorCheckboxIsCC=$("input[name='vendorCheckboxIsCC']").serializeArray();
		var vendorCheckboxIsBCC=$("input[name='vendorCheckboxIsBCC']").serializeArray();
		
		if (fields.length == 0 && fields2.length == 0) {
			alert('Please select atleast one user');
			// cancel submit
			return false;
		}

		if (checkboxIsTo.length == 0 && checkboxIsCC.length == 0
				&& checkboxIsBCC.length == 0 && vendorCheckboxIsTo.length == 0
				&& vendorCheckboxIsCC.length == 0
				&& vendorCheckboxIsBCC.length == 0) {
			alert('Please select atleast one recipient');
			// cancel submit
			return false;
		}
	}
	
	function createCustomerTable(){
		var grid = jQuery("#gridtable1");
		var table = document.getElementById("dynamicCustomerTable");
		var messages = [];
		messages.push("Following EmailID's already selected. You cannot add it again.\n");
		if (idsOfSelectedRows1.length > 0) {
			for ( var i = 0, il = idsOfSelectedRows1.length; i < il; i++) {
				var rowCount = table.rows.length;
				var row = grid.getLocalRow(idsOfSelectedRows1[i]);
				var userId = row.id;
				var userName = row.name;
				var userEmailId = row.emailid;
				var emailExists = false;
				$("label[name='customerEmailID']").each(function(index) { //For each of inputs
				    if (userEmailId === $(this).text()) { //if match against array
				    	messages.push(userEmailId + "\n");
				        emailExists = true;
				    } 
				});
				if(!emailExists){
					var row = table.insertRow(rowCount);
					var newcell0 = row.insertCell(0);
					newcell0.innerHTML = "<td><input type='checkbox' name='checkbox' checked='checked' id='checkbox" + rowCount + "' value='" + userId + "' onclick='enable_cb1(" + rowCount + ")'></td>";
					var newcell1 = row.insertCell(1);
					newcell1.innerHTML = "<td><label>" + userName + "</label><td>";
					var newcell2 = row.insertCell(2);
					newcell2.innerHTML = "<td><label name='customerEmailID'>" + userEmailId + "</label><td>";
					var newcell3 = row.insertCell(3);
					newcell3.innerHTML = "<td><input type='checkbox' name='checkboxIsTo' value='" + userId + "' id='checkboxIsTo" + rowCount + "'></td>";
					var newcell4 = row.insertCell(4);
					newcell4.innerHTML = "<td><input type='checkbox' name='checkboxIsCC' value='" + userId + "' id='checkboxIsCC" + rowCount + "' ></td>";
					var newcell5 = row.insertCell(5);
					newcell5.innerHTML = "<td><input type='checkbox' name='checkboxIsBCC' value='" + userId + "' id='checkboxIsBCC" + rowCount + "' ></td>";
				}
			}
			idsOfSelectedRows1 = [];
			$("#customerTable").css('display','block');
			if(messages.length > 1){
				alert(messages.join(""));
			}
			$("#customer").dialog("close");
		} else {
			alert("Choose atleast one user");
		}
	}
	
	function createVendorTable(){
		var grid = jQuery("#gridtable");
		var table = document.getElementById("dynamicVendorTable");
		var messages = [];
		messages.push("Following EmailID's already selected. You cannot add it again.\n");
		if (idsOfSelectedRows.length > 0) {
			for ( var i = 0, il = idsOfSelectedRows.length; i < il; i++) {
				var rowCount = table.rows.length;
				var row = grid.getLocalRow(idsOfSelectedRows[i]);
				var vendorId = row.id;
				var vendorName = row.name;
				var vendorEmailId = row.emailid;
				var primeNonprime = row.prime;
				var vname = row.vendorname;
				var vendorstatus=row.vendorstatus;
				var emailExists = false;
				$("label[name='vendorEmailID']").each(function(index) { //For each of inputs
				    if (vendorEmailId === $(this).text()) { //if match against array
				    	messages.push(vendorEmailId + "\n");
				        emailExists = true;
				    } 
				});
				if(!emailExists){
					var row = table.insertRow(rowCount);
					var newcell0 = row.insertCell(0);
					newcell0.innerHTML = "<td><input type='checkbox' name='checkbox2' checked='checked' id='checkboxv" + rowCount + "' value='" + vendorId + "' onclick='enable_cb2(" + rowCount + ")'></td>";
					var newcell1 = row.insertCell(1);
					newcell1.innerHTML = "<td><label>" + vendorName + "</label><td>";
					var newcell2 = row.insertCell(2);
					newcell2.innerHTML = "<td><label name='vendorEmailID'>" + vendorEmailId + "</label><td>";
					var newcell3 = row.insertCell(3);
					newcell3.innerHTML = "<td><label>" + vname + "</label><td>";
					var newcell4 = row.insertCell(4);
					newcell4.innerHTML = "<td><label>" + primeNonprime + "</label><td>";
					var newcell5 = row.insertCell(5);
					newcell5.innerHTML = "<td><label>" + vendorstatus + "</label><td>";
					var newcell6 = row.insertCell(6);
					newcell6.innerHTML = "<td><input type='checkbox' name='vendorCheckboxIsTo' value='" + vendorId + "' id='vendorCheckboxIsTo" + rowCount + "' ></td>";
					var newcell7 = row.insertCell(7);
					newcell7.innerHTML = "<td><input type='checkbox' name='vendorCheckboxIsCC' value='" + vendorId + "' id='vendorCheckboxIsCC" + rowCount + "' ></td>";
					var newcell8 = row.insertCell(8);
					newcell8.innerHTML = "<td><input type='checkbox' name='vendorCheckboxIsBCC' value='" + vendorId + "' id='vendorCheckboxIsBCC" + rowCount + "' ></td>";
				}
			}
			idsOfSelectedRows = [];
			if(messages.length > 1){
				alert(messages.join(""));
			}
			$("#vendorTable").css('display','block');
			$("#vendor").dialog("close");
		} else {
			alert("Choose atleast one vendor");
		}
	}
	
	function enableSelectAll(name){
		if(name == 'checkbox'){
			if ($("#cselectall").is(':checked')){
				$("input[name='checkbox']").attr("checked", true);
				$("#custtoselectall").removeAttr("disabled");
				$("#custccselectall").removeAttr("disabled");
				$("#custbccselectall").removeAttr("disabled");
				$("input[name='checkboxIsTo']").removeAttr("disabled");
				$("input[name='checkboxIsCC']").removeAttr("disabled");
				$("input[name='checkboxIsBCC']").removeAttr("disabled");
			} else {
				$("input[name='checkbox']").attr("checked", false);
				$("#custtoselectall").attr("disabled", true);
				$("#custccselectall").attr("disabled", true);
				$("#custbccselectall").attr("disabled", true);
				$("input[name='checkboxIsTo']").attr("disabled", true);
				$("input[name='checkboxIsCC']").attr("disabled", true);
				$("input[name='checkboxIsBCC']").attr("disabled", true);
			}
		}else if(name == 'checkbox2'){
			if ($("#vselectall").is(':checked')){
				$("input[name='checkbox2']").attr("checked", true);
				$("#ventoselectall").removeAttr("disabled");
				$("#venccselectall").removeAttr("disabled");
				$("#venbccselectall").removeAttr("disabled");
				$("input[name='vendorCheckboxIsTo']").removeAttr("disabled");
				$("input[name='vendorCheckboxIsCC']").removeAttr("disabled");
				$("input[name='vendorCheckboxIsBCC']").removeAttr("disabled");
			} else {
				$("input[name='checkbox2']").attr("checked", false);
				$("#ventoselectall").attr("disabled", true);
				$("#venccselectall").attr("disabled", true);
				$("#venbccselectall").attr("disabled", true);
				$("input[name='vendorCheckboxIsTo']").attr("disabled", true);
				$("input[name='vendorCheckboxIsCC']").attr("disabled", true);
				$("input[name='vendorCheckboxIsBCC']").attr("disabled", true);
			}
		}
	}
	
	function enableCustomerSelectAll(type) {
		if(type == 'to'){
			if ($("#custtoselectall").is(':checked')) {
				$("input[name='checkboxIsTo']").attr("checked", true);
			} else {
				$("input[name='checkboxIsTo']").attr("checked", false);
			}
		}else if(type == 'cc'){
			if ($("#custccselectall").is(':checked')) {
				$("input[name='checkboxIsCC']").attr("checked", true);
			} else {
				$("input[name='checkboxIsCC']").attr("checked", false);
			}
		} else {
			if ($("#custbccselectall").is(':checked')) {
				$("input[name='checkboxIsBCC']").attr("checked", true);
			} else {
				$("input[name='checkboxIsBCC']").attr("checked", false);
			}
		}
	}

	function enableVendorSelectAll(type) {
		if(type == 'to'){
			if ($("#ventoselectall").is(':checked')) {
				$("input[name='vendorCheckboxIsTo']").attr("checked", true);
			} else {
				$("input[name='vendorCheckboxIsTo']").attr("checked", false);
			}
		}else if(type == 'cc'){
			if ($("#venccselectall").is(':checked')) {
				$("input[name='vendorCheckboxIsCC']").attr("checked", true);
			} else {
				$("input[name='vendorCheckboxIsCC']").attr("checked", false);
			}
		} else {
			if ($("#venbccselectall").is(':checked')) {
				$("input[name='vendorCheckboxIsBCC']").attr("checked", true);
			} else {
				$("input[name='vendorCheckboxIsBCC']").attr("checked", false);
			}
		}
	}
	
	function cancelCustomer() { 
		$("#customer").hide();
	}
	
	function cancelVendor() {
		$("#vendor").hide();
	}

</script>
<style type="text/css">
/*th.ui-th-column div {
	white-space: normal !important;
	height: auto !important;
	padding: 2px;
}
.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
	height: auto;
	vertical-align: text-top;
	padding-top: 2px;
}*/
</style>

<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-9 mx-auto">	
		<div id="successMsg">
			<html:messages id="msg" property="successMsg" message="true">
				<div class="alert alert-info nomargin">
					<bean:write name="msg" />
				</div>
			</html:messages>
		</div>
		<div id="failureMsg">
			<html:messages id="msg" property="errorMsg" message="true">
				<div class="alert alert-info nomargin">
					<bean:write name="msg" />
				</div>
			</html:messages>
		</div>

			<section class="mt-2">
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="Email Distribution" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="add">
							<header class="card-header">
								<h2 class="card-title pull-left">Email Distribution</h2>									
								<div class="wrapper-btn pull-right text-center">
									<input type="button" class="btn btn-primary" onclick="enableForm();"
										value="Add New Email Distribution" id="newdistribution" />
								</div>
							</header>
						</logic:match>
					</logic:match>
				</logic:iterate>
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="Email Distribution" name="privilege"
						property="objectId.objectName">
						<logic:match value="0" name="privilege" property="add">
							<div style="text-align: center;">
								<h3>You have no rights to add Email Distribution</h3>
							</div>
						</logic:match>
					</logic:match>
				</logic:iterate>
			</section>
	</div>
</div>

	<html:form
		action="/emaildistributionlist?method=saveEmailDistributionList"
		styleId="emailDistributionForm" onsubmit="return validateForm();">
		<div class="row">
		<div class="col-lg-9 mx-auto">
			<div class="form-box card-body">	
				<div class="form-group row row-wrapper">
					<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Email Distribution List</label>
					<div class="col-sm-9 ctrl-col-wrapper col-lg-6">
						<html:text property="emailDistributionListName" alt=""
							styleClass="text-box form-control" styleId="emailDistributionListName" />
					</div>
				</div>
				<footer class="card-footer mt-4">
					<div class="row justify-content-end">
						<div class="col-sm-9 wrapper-btn">
							<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit"
								disabled="true"></html:submit>
							<html:reset value="Cancel" styleClass="btn btn-default" onclick="clearfields();"
								styleId="clear" disabled="true"></html:reset>
						</div>		
					</div>
				</footer>
				<div class="clear"></div>
			</div>
<div class="grid-wrapper">
	<div class="form-box">
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Email Distribution" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">
				
			<div class="mx-auto">
				<div id="customer" style="display: none; width:100% !important;">
					<div id="grid_container1" class="card-body customer-email-table vendor-table">
						<table id="gridtable1" style="width:100% !important;" class="table table-bordered table-striped mb-0">
							<tr>
								<td  />
							</tr>
								</table>
								<div id="pager1"></div>
							</div>
							<footer class="card-footer">
								<div class="row">
									<div class="col-md-12 text-right">
										<button class="btn exportBtn btn-primary" onclick="createCustomerTable();">Ok</button>
									 	<input type="button" value="Cancel" class="btn exportBtn btn-default"  onclick="cancelCustomer();">
									</div>
								</div>
							</footer>
						</div>
						
						<div id="vendor" style="display: none; width:100% !important;">
							<div id="grid_container" class="card-body vendor-table">
								<table id="gridtable" style="width:100% !important;" title="Vendors" class="main-table table table-bordered ui-jqgrid-btable table-striped mb-0">
									<tr>
										<td />
									</tr>
								</table>
								<div id="pager"></div>
							</div>
							<footer class="card-footer">
								<div class="row">
									<div class="col-md-12 text-right">
										<input type="button" value="Ok" class="btn exportBtn btn-primary" onclick="createVendorTable();">
										 <input type="button" value="Cancel" class="btn exportBtn btn-default" onclick="cancelVendor();">
								 	</div>								 
								</div>
							</footer>
						</div>
						<div id="customerTable" class="card-body" style="display: none;">
						<header class="card-header">
								<h2 class="card-title pull-left" id="customerLabel">Customer</h2>
						
						<div id="customerBtn" class="wrapper-btn pull-right">
							<input type="button" value="Add Customer User" class="btn btn-primary" onclick="addUser();">
						</div>
						</header>
						<div id="grid_container" class="card-body">
							<table id='dynamicCustomerTable' class="main-table table table-bordered table-striped mb-0">
								<thead>
								<tr>
									<th>Select<br><input type="checkbox" id="cselectall" onclick="enableSelectAll('checkbox');"></th>
									<th>Name</th>
									<th>Email ID</th>
									<th>TO<br><input type="checkbox" id="custtoselectall" onclick="enableCustomerSelectAll('to');"></th>
									<th>CC<br><input type="checkbox" id="custccselectall" onclick="enableCustomerSelectAll('cc');"></th>
									<th>BCC<br><input type="checkbox" id="custbccselectall" onclick="enableCustomerSelectAll('bcc');"></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						<div id="vendorTable" class="card-body mt-2" style="display: none;">
							<header class="card-header">
								<h2 class="card-title pull-left">Vendor</h2>
								<div id="vendorBtn" class="wrapper-btn pull-right">
									<input type="button" value="Add Vendor User" class="btn btn-primary" onclick="addVendor();">
								</div>
							</header>
							<div id="grid_container" class="card-body">
							<table id='dynamicVendorTable' class="main-table table table-bordered table-striped ui-jqgrid-btable mb-0">
								<thead>
								<tr>
									<th>Select<br><input type="checkbox" id="vselectall" onclick="enableSelectAll('checkbox2');"></th>
									<th>Name</th>
									<th>Email ID</th>
									<th>Vendor Name</th>
									<th>Vendor Type</th>
									<th>Status</th>
									<th>TO<br><input type="checkbox" id="ventoselectall" onclick="enableVendorSelectAll('to');"></th>
									<th>CC<br><input type="checkbox" id="venccselectall" onclick="enableVendorSelectAll('cc');"></th>
									<th>BCC<br><input type="checkbox" id="venbccselectall" onclick="enableVendorSelectAll('bcc');"></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							</div>
						</div>
					</div>
						</logic:match>
					</logic:match>
				</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Email Distribution" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">

					<logic:notEmpty name="distributionModels">
					<div id="grid_container">
						<table id="emailDistributionListTable" class="card-body main-table table table-bordered table-striped mb-0">
							<thead>
								<tr>
									<th>Email Distribution List</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<logic:iterate name="distributionModels"
									id="emailDistributionList">
									<tr>
										<td><bean:write name="emailDistributionList"
												property="emailDistributionListName" />
										</td>
										<td><bean:define id="mailId" name="emailDistributionList"
												property="emailDistributionMasterId"></bean:define> <logic:match
												value="1" name="privilege" property="modify">
												<html:link
													action="/editemaildistribution.do?method=editEmailDistribution"
													paramId="emailDistributionMasterId" paramName="mailId">Edit</html:link>
								                  |
										</logic:match> <logic:match value="1" name="privilege" property="delete">
												<html:link
													action="/deleteemaildistribution.do?method=deleteEmailDistribution"
													paramId="emailDistributionMasterId" paramName="mailId"
													onclick="return confirm_delete();">Delete</html:link>
											</logic:match></td>
									</tr>

								</logic:iterate>
							</tbody>
						</table>
						</div>
					</logic:notEmpty>
				</logic:match>
			</logic:match>
		</logic:iterate>
		<logic:empty name="distributionModels">
			<p>There is no Email Distribution List available..</p>
		</logic:empty>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Email Distribution" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
					<div style="text-align: center;">
						<h3>You have no rights to view Email Distribution</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>
		</div>
	</div>
	</div>
	</div>
	</html:form>

</section>
