<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link rel="stylesheet" type="text/css"
	href="jquery/css/ui.multiselect.css" />
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>


<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />
<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>
 <link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<!--  <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" /> -->
	
<script>

$(document).ready(function() 
{
	$(document).keypress(function(e) {
		if (e.which == 13) {
			$(e.target).blur();
			return saveDynamicColumns();
		}
	});
});

	function showSaveFilter(){
		$("#dialog2").css({
			"display" : "block"
		});
		$("#dialog2").dialog({
			minWidth : 400,
			modal : true
		});
		$("#searchName").focus();
		
	 	$(document).keypress(function(e) {
			if (e.which == 13 && $("#dialog2").is(':visible')) {
				$(e.target).blur();
				return saveFilter();
			}
		}); 	
	}
	function backToStatusSearch() {
		window.location = "viewVendorsStatus.do?method=showVendorSearch&searchType=V";
	}
	function backToCriteriaSearch() {
		window.location = "viewVendors.do?method=showVendorSearch&searchType=C";
	}
	$(function() {
		$("#accordion").accordion({
			 heightStyle: "content"
		});
	});
	
	
	function clearFields(){
		window.location.reload(true);
	}
	
	function saveDynamicColumns(){
		var chkArray = [];
		var selected;
		
		$(".chk:checked").each(function() {
			chkArray.push($(this).val());
		});
		selected = chkArray.join(',');
		if(selected.length > 1){
		$.ajax({
			url : "dynamicReport.do?method=saveCustomColumnsForSearch&columnIds="+selected,
			type : "POST",
			async : false,
			success : function(data) {
				$('#dynamicColumnSelect').hide();
				$('#result').css('display', 'block');
				$("#gridtable").jqGrid('GridUnload');
				gridSearchVendorData(data);
			}
		});
			
		}else{
			alert("Please select atleast one checkbox");	
		}  
	}
	
	function gridSearchVendorData(myGridData) {
		
		var dynamicData = myGridData.split("|");
		var dynamicColNames = jQuery.parseJSON(dynamicData[0]);
		var dynamicColModel = jQuery.parseJSON(dynamicData[1]);
		var dynamicColData = dynamicData[2];

		var newdata1 = jQuery.parseJSON(dynamicColData);

		$('#gridtable').jqGrid({
			
					data : newdata1,
					datatype : 'local',
					colNames : dynamicColNames,
					colModel : dynamicColModel,
					pager : '#pager',
					shrinkToFit : false,
					viewrecords : true,
					autowidth : true,
					emptyrecords : 'No data available',
					gridview: true,
		            pgbuttons:false,
		            pgtext:'',
		            width: ($.browser.webkit?466:497),
		            loadonce: true,
		            rowNum: 10000,
					cmTemplate: { title: false },
					height: 250
				}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		},
		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{
			drag : true,
			closeOnEscape : true
		}
		);

		$("#gridtable").jqGrid('navButtonAdd', '#pager', {
			caption : "Change Order",
			title : "Reorder Columns",
			onClickButton : function() {
				$("#gridtable").jqGrid('columnChooser', {
					title : "Select and reorder columns",
					width : 700,
					height : 350,
					done : function(perm) {
						if (perm) {
							this.jqGrid("remapColumns", perm, true);
						}
					}
				});
			}
		});
	}
	
function saveFilter() {
		
	var actionUrl="dynamicReport.do?method=saveSearchedData&searchType=VD";
	<logic:equal value="vendorcriteria" name="searchtype">
			actionUrl="dynamicReport.do?method=saveSearchedData&searchType=CD";
	</logic:equal>
		
		var searchName=$("#searchName").val();
		if (searchName != '' && searchName != 'undefined') 
		{
			$.ajax({
				url : actionUrl+"&searchName="+searchName,
				type : "POST",
				async : false,
				dataType : "json",
				beforeSend : function() {
						$("#ajaxloader").show();
				},
				success : function(data) {
					$("#ajaxloader").hide();
					if((data.result)=="success"){
						$('#searchName').val('');
						$("#dialog2").dialog("close");
						alert("Search criteria successfully saved ");
						window.location.reload(true);	
					}
					else if((data.result)=="unique"){
						alert("Search Name allready available ");
					}
					else
						alert("Search criteria failed to save ");
				}
			});
		}
		else{
			alert("Please Enter a Name for search");
		}
	}
	
</script>
<section role="main" class="content-body">
	<div class="row">
		<div class="col-lg-12">
<div id="dynamicColumnSelect" class="card">
<header class="card-header">
	<h2 class="card-title">SELECT COLUMNS FROM HERE..</h2>
</header>

	<form>
		
		<div class="accordion customize-column" id="accordion">
					
			<logic:present name="searchFeildsList">
				<bean:size id="size" name="searchFeildsList" />
				<logic:greaterEqual value="0" name="size">
				<% int count = 0; %>
						<logic:iterate id="searchFeildsList" name="searchFeildsList">
						<% String accId = "collapse" + count; %>
						<div class="card card-default" style="height:auto !important;">
							<div class="card-header">
								<h4 class="card-title m-0">
									<a class="accordion-toggle" data-toggle="collapse"
										data-parent="#accordion" href="#<%=accId %>"> 
										<bean:write
											name="searchFeildsList" property="category"></bean:write>
									</a>
								</h4>
							</div>

										<div id="<%=accId %>" class="collapse">
							<bean:define id="columnTitle" name="searchFeildsList"
								property="columnTitle"></bean:define>

							<bean:size id="size" name="columnTitle" />
							<logic:greaterEqual value="0" name="size">
								<div class="form-group card-body">
								<table>
								
									<tr>
									
										<%	int j=0; %>
									<logic:iterate id="columnDetails" name="columnTitle">
											<%
								String[] columnValue = columnDetails.toString().split("\\,");
															if (columnValue != null
																		&& columnValue.length != 0) {
																	for (int i = 0; i < columnValue.length; i++) {
																		String[] parts = columnValue[i]
																				.split(":");
																		String coulumnId = parts[0];
																		String columnName = parts[1];
																		if(j==5){
																			j=0;
																			%>
																			
									
									</tr>
									
									<tr>
									</tr>
									<tr>
									</tr>
									<tr>
									</tr>
									<tr>
									
										<%
														}
														j++;
						%>

										<td align="left">
										<td class="checkbox-custom chekbox-primary"><input
											id="" name="for[]" type="checkbox"
											value="<%=coulumnId%>" class="chk"> <label
											for=""> <%=columnName%></label></td>

										<%
							}
								}
						%>
									</logic:iterate>	
									</tr>
								</table>
							</div>
							</logic:greaterEqual>
										</div>
										</div>
										<%count++; %>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
					
			</div>

		<div class="clear"></div>
			<footer>
				<div class="row justify-content-end">
					<div class="wrapper-btn">
			<input type="button" class="btn btn-primary" value="Show Report" id="buttonClass" onclick="saveDynamicColumns();"> 
			<input type="button" class="btn" value="Clear" id="buttonClass" onclick="clearFields();">
		<logic:equal value="vendorStatus" name="searchtype">			
			<input type="button" class="btn btn-tertiary" value="Back To Search" id="buttonClass" onclick="backToStatusSearch();"> 
		</logic:equal>
		<logic:equal value="vendorcriteria" name="searchtype">
			<input type="button" class="btn btn-default" value="Back To Search" id="buttonClass" onclick="backToCriteriaSearch();">
		</logic:equal>
		</div>
		</div>
		</footer>
	</form>
</div>


<div id="result" style="display: none;">
	<header class="card-header">
			<h2 class="card-title">
		<img src="images/icon-registration.png" />&nbsp;&nbsp;Following are
		your Vendors</h2>
		<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
					<div id="export" class="pull-right">
						<input type="button" class="btn btn-tertiary" value="Export" id="export"
							onclick="exportGrid();">
					</div>
					
					<input type="button" value="Save Search" class="btn pull-right btn-primary mr-1"  onclick="showSaveFilter();">
				</div>
			</div>			
	</header>

	<div class="grid-wrapper view-reports-table pt-0">
				<div class="form-box">
					<div class="row">
						<div class="col-lg-12">
							<section class="vendor-search-table">
								<div class="card-body">
									<div id="grid_container" style="width:100% !important;">
										<table id="gridtable" style="width:100% !important;" class="table table-bordered table-striped mb-0">
											<tbody>
												<tr>
													<td />
												</tr>
											</tbody>
										</table>
										<div id="pager"></div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>

			</div>
	<div id="dialog" style="display: none;" title="Choose Export Type">
		<p>Please Choose the Export Type</p>
		<table style="width: 100%;">
			<tr>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="1" id="excel" /></td>
				<td><label for="excel"><img id="excelExport"
						src="images/excel_export.png" />Excel</label></td>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="2" id="csv" /> <input type="hidden" name="fileName"
					id="fileName" value="SupplierReport" /></td>
				<td><label for="csv"><img id="csvExport"
						src="images/csv_export.png" />CSV</label></td>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="3" id="pdf" /></td>
				<td><label for="pdf"><img id="pdfExport"
						src="images/pdf_export.png" />PDF</label></td>
			</tr>
		</table>
		<div class="text-center wrapper-btn">
			<logic:present name="userDetails">
				<bean:define id="logoPath" name="userDetails"
					property="settings.logoPath"></bean:define>
				<input type="button" value="Export" class="btn btn-primary exportBtn"
					onclick="exportHelperForDynamic('gridtable','<%=logoPath%>');">
			</logic:present>
		</div>
	</div>
	<footer class="">
				<div class="row justify-content-end">
					<div class="wrapper-btn">
		<logic:equal value="vendorStatus" name="searchtype">			
			<input type="button" class="btn btn-primary" value="Back To Search" id="buttonClass" onclick="backToStatusSearch();"> 
		</logic:equal>
		<logic:equal value="vendorcriteria" name="searchtype">
			<input type="button" class="btn btn-tertiary" value="Back To Search" id="buttonClass" onclick="backToCriteriaSearch();">
		</logic:equal>

		<input type="button" class="btn btn-default" value="Back" id="buttonClass" onclick="clearFields();">
	</div>
	</div>
	</footer>
</div>
<div id="dialog2" class="form-box" style="display: none;"
	title="Save Filter">

	<div class="wrapper-full">
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper" style="width: 35%;">Search Name
				:</div>
			<div class="ctrl-col-wrapper">
				<input type="text" id="searchName" name="searchName"
					class="text-box" />
			</div>
		</div>
	</div>
	<div class="btn-wrapper text-center">
		<input type="button" class="exportBtn btn btn-primary" onclick="saveFilter();"
			value="Submit" id="statusButton" />
	</div>
</div>
<div id="ajaxloader" style="display: none;">
	<img src="images/ajax-loader.gif" alt="Ajax Loading Image" />
</div>
</div>
</div>
</section>