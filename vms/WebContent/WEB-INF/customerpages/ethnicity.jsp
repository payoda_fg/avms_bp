<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

 <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
 <link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
	
	
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>


<style>
<!--
/*#paddtree_center{
	padding: 0 0 0 30%;
}
#editmodaddtree{
	height: 150px !important;
}*/
-->
</style>
<div class="clear"></div>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">	
			<section class="card">
				<header class="card-header">
					<h2 class="card-title pull-left">Ethnicity</h2>
				</header>
			<div class="clear"></div>
			<div id="form-box" class="form-box card-body ethnicity-table">
				<table class="main-table table table-bordered table-striped mb-0" id="addtree">
				
					<tr>
				        <td />
				    </tr>
				  
				</table>
				<div id="paddtree"></div>
			</div>
		</section>
	</div>
</div>
</section>
<script type="text/javascript">
	$.ajax({
		url : 'ethnicity.do?method=listEthnicity' + '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) {
			$("#addtree").jqGrid('GridUnload');
			ethnicityData(data);
		}
	});
	
	function checkUnity() {		
		$.ajax(
		{
			url : 'ethnicity.do?method=getJSONData' + '&random=' + Math.random(),
			type : "POST",
			async : false,
			success : function(data) {
				var ce = data.isUnique;				
				if(ce==1){
					alert("Ethnicity Name Should be Unique.");
					window.location = "ethnicity.do?method=view";					
				}
				else {
					window.location = "ethnicity.do?method=view";
				}
			}
		});
	}
	
	function ethnicityData(myGridData) {
		var newdata = jQuery.parseJSON(myGridData);
		jQuery("#addtree").jqGrid(
				{
					data : newdata,
					datatype : "local",
					colNames : [ "id", "Ethnicity" ],
					colModel : [ {
						name : 'id',
						index : 'id',
						hidden : true,
						editable : true
					}, {
						name : 'Ethnicity',
						index : 'Ethnicity',
						align : "left",
						editable : true,
						editrules : {
							required : true,
							custom : true,
							custom_func : maxLength
						}
					} ],

					height : '350',
					pager : "#paddtree",
					rowNum : 15,
					rowList : [ 10, 20, 50 ],
					loaded : true,
					loadonce: true,
					cmTemplate: { title: false },
					editurl : 'ethnicity.do?method=updateEthnicity'
							+ '&random=' + Math.random(),
					/* caption : "Ethnicity", */
					hidegrid : false,
					onSelectRow : function(id) {
						var rowData = jQuery(this).getRowData(id);
						currentId = rowData['id'];
					}
				});
		function maxLength(value, name) {			
			if(value.length > 250)
				return [false, "Please Enter Lessthan 250 Characters."];
			else
				return [true, ""];
		}
		
		jQuery("#addtree").jqGrid('navGrid', "#paddtree", {
			add : true,
			edit : true,
			del : true,
			search : false

		}, {
			recreateForm : true,
			height : 100,
			width : 500,
			reloadAfterSubmit : true,
			afterSubmit: function () {
				window.location.reload(true);
            },
			closeAfterEdit : true,
			afterSubmit : function(response, postdata)
			{
				checkUnity();
			}

		}, {
			recreateForm : true,
			height : 200,
			width : 500,
			reloadAfterSubmit : true,
			afterSubmit: function () {
				window.location.reload(true);
            },
			closeAfterAdd : true,
			afterSubmit : function(response, postdata)
			{
				checkUnity();
			},
		}, {}, {});
		
		

		$(window).bind('resize', function() {
			$("#addtree").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize');
	}
	$(document).ready(function() {
		<logic:iterate id="privilege" name="privileges">
			<logic:equal value="Ethnicity" name="privilege"
				property="objectId.objectName">
				<logic:equal value="0" name="privilege" property="add">
					$("#add_addtree").css("display", "none");
				</logic:equal>
			</logic:equal>
			<logic:equal value="Ethnicity" name="privilege"
				property="objectId.objectName">
				<logic:equal value="0" name="privilege" property="delete">
					$("#del_addtree").css("display", "none");
				</logic:equal>
			</logic:equal>
			<logic:equal value="Ethnicity" name="privilege"
				property="objectId.objectName">
				<logic:equal value="0" name="privilege" property="modify">
					$("#edit_addtree").css("display", "none");
				</logic:equal>
			</logic:equal>
		</logic:iterate>
	});
	
</script>


