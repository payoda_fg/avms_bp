<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.fg.vms.customer.model.NaicsCategory"%>
<%@page import="com.fg.vms.customer.model.NAICSubCategory"%>
<style>
<!--
textarea.catecoryDescription {
	width: 300px;
	height: 50px;
}
-->
</style>
<script type="text/javascript">
<!--
	function backToSubcategory() {
		window.location = "viewnaicsubcategory.do?parameter=viewSubCategoriesById&categoryId=0";
	}
	//-->
	$(document).ready(function() {

		$('#subCategoryTable').tableScroll({
			height : 150
		});
	});
</script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<%
	NaicsCategory selectCategory = (NaicsCategory) session
			.getAttribute("categoryId2");
	String cateValue = "0";
	String selectCategoryName = "";
	if (selectCategory != null) {
		cateValue = selectCategory.getId().toString();
		selectCategoryName = selectCategory.getNaicsCategoryDesc();
	}
%>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Update
				NAICS Sub-Category Entry
			</h2>
		</div>
		<div id="table-holder" style="margin: 0 1.5%;">
			<html:form
				action="/updatesubcategory.do?parameter=updateNAICSubCategory">
				<html:javascript formName="naicSubCategoryForm" />
				<html:hidden property="id" name="naicSubCategoryForm" />
				<table style="margin-left: 30%;" cellspacing="10" cellpadding="5">
					<tr>
						<td>NAIC Sub-Category Description</td>
						<td><html:textarea property="naicSubCategoryDesc" styleClass="catecoryDescription"
								name="naicSubCategoryForm"></html:textarea><span class="error"><html:errors
									property="naicSubCategoryDesc"></html:errors></span></td>
					</tr>
					<tr>
						<td>IsActive&nbsp;</td>
						<td><html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
							<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
						</td>
					</tr>
				</table>

				<logic:iterate id="privilege" name="privileges">
					<logic:match value="NAICS Sub-category" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="add">
							<div style="padding: 0 0 1% 44%;">
								<html:submit value="Update" styleClass="customerbtTxt"
									styleId="submit"></html:submit>
								<html:reset value="Cancel" styleClass="customerbtTxt"
									onclick="backToSubcategory()"></html:reset>
							</div>
						</logic:match>
					</logic:match>
				</logic:iterate>
			</html:form>
		</div>

		<logic:iterate id="privilege" name="privileges">
			<logic:match value="NAICS Sub-category" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">

					<h2>
						<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Available
						NAICS Sub-Categories&nbsp;&nbsp;<span style="color: green;"><%=selectCategoryName%></span>
					</h2>
					<div id="box" style="margin-left: 1.5%;">
						<table width="96%" id="subCategoryTable">
							<bean:size id="size" name="naicsub1" />
							<logic:greaterThan value="0" name="size">
								<thead>
									<tr>
										<th>NAICS Sub-Category Description</th>
										<th>IsActive</th>
										<th>Actions</th>
									</tr>
								</thead>
								<logic:iterate name="naicsub1" id="naicsublist">
									<tbody>
										<tr>
											<td><bean:write name="naicsublist"
													property="naicSubCategoryDesc" /></td>
											<td>Yes</td>
											<bean:define id="naicsubCategoryId" name="naicsublist"
												property="id"></bean:define>
											<td><logic:iterate id="privilege" name="privileges">
													<logic:match value="NAICS Sub-category" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="modify">
															<html:link paramId="id" paramName="naicsubCategoryId"
																action="/retrivesubcategory.do?parameter=retriveNAICSubCategory">Edit</html:link>
														</logic:match>
													</logic:match>

												</logic:iterate> <logic:iterate id="privilege" name="privileges">

													<logic:match value="NAICS Sub-category" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="delete">
															<logic:notEqual
																value="<%=naicsubCategoryId.toString() %>" property="id"
																name="naicSubCategoryForm">
																| <html:link
																	action="/deletesubcategory.do?parameter=deleteNAICSubCategory"
																	paramId="id" paramName="naicsubCategoryId"
																	onclick="return confirm_delete();">Delete</html:link>
															</logic:notEqual>
															<logic:equal value="<%=naicsubCategoryId.toString() %>"
																property="id" name="naicSubCategoryForm">
																<span style="color: green; font-weight: bold;">|
																	Sub-Category in Edit </span>
															</logic:equal>

														</logic:match>
													</logic:match>
												</logic:iterate></td>
										</tr>
									</tbody>
								</logic:iterate>
							</logic:greaterThan>
							<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
						</table>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:match value="NAICS Sub-category" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
				<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view NAICS sub-category</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>

	</div>
</div>
