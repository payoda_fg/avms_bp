<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>

<style type="text/css">
	/*#diverseCertificateNames
	{
		max-width: 250px;
	}*/
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("#countryId").select2({
			width : "90%"
		});
		$("#state").select2({
			width : "90%"
		});
		$("#diverseCertificateNames").multiselect({
			selectedText : "# of # selected"
		});
		
		$("#countryValue").val($("#countryId option:selected").text());
	});
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != 0) {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2({
						width : "90%"
					});
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
			$('#' + id).select2({
				width : "90%"
			});
		}
	}
</script>



<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
	<div class="form-box">	
	<section class="card-body">
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor Name</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text styleClass="text-box form-control" property="vendorName" alt="Optional"
				size="23" />
		</div>
	</div>
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">NAICS Code</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text styleClass="text-box form-control" property="naicCode" alt="Optional" />
		</div>

	</div>


	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Naics Description</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:textarea property="naicsDesc" styleClass="main-text-area form-control"
				rows="3" cols="50"  />
		</div>
	</div>
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<logic:present name="userDetails" property="workflowConfiguration">
				<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
					<html:select property="country" styleClass="form-control" styleId="countryId" onchange="changestate(this,'state');">
						<html:option value="0" key="select">--Select--</html:option>
						<logic:present name="searchVendorForm" property="countries">
							<bean:size id="size" property="countries" name="searchVendorForm" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="country" property="countries" name="searchVendorForm">
									<bean:define id="name" name="country" property="id"/>
									<html:option value="<%=name.toString()%>">
										<bean:write name="country" property="countryname" />
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>
					</html:select>
				</logic:equal>
				<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
					<input type="text" class="text-box form-control" id="countryValue" readonly="readonly">
					<html:select property="country" styleId="countryId" onchange="changestate(this,'state');" style="display:none;">
						<logic:present name="searchVendorForm" property="countries">
							<bean:size id="size" property="countries" name="searchVendorForm" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="country" property="countries" name="searchVendorForm">
									<bean:define id="name" name="country" property="id"/>
									<html:option value="<%=name.toString()%>">
										<bean:write name="country" property="countryname" />
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>
					</html:select>
				</logic:equal>
			</logic:present>		
		</div>
	</div>

	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:select property="state" styleId="state" styleClass="form-control" tabindex="31">
			<html:option value="">--Select--</html:option>
				<logic:present name="searchVendorForm" property="states">
					<logic:iterate id="states" name="searchVendorForm"
						property="states">
						<bean:define id="name" name="states" property="id"></bean:define>
						<html:option value="<%=name.toString()%>"><bean:write name="states" property="statename" /></html:option>
					</logic:iterate>
				</logic:present>
			</html:select>
		</div>
	</div>
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text property="city" alt="Optional" styleClass="text-box form-control" />
		</div>
	</div>


	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Region</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text styleClass="text-box form-control" property="region" alt="Optional" />
		</div>
	</div>
	
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Province</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text styleClass="text-box form-control" property="province" alt="Optional"
				size="23" />
		</div>
	</div>


	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Supplier Type</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<div class="radio-custom radio-primary">
				<html:radio property="diverse" value="1">&nbsp;Diverse&nbsp;</html:radio>
				<label></label>
			</div>
			<div class="radio-custom radio-primary">
				<html:radio property="diverse" value="0">&nbsp;Non Diverse&nbsp;</html:radio>
				<label></label>
			</div>
		</div>
	</div>
	
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Diverse Classification</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:select property="diverseCertificateNames" multiple="true"
				styleId="diverseCertificateNames" name="searchVendorForm">
				<logic:present name="certificateTypes">
					<bean:size id="size" name="certificateTypes" />
					<logic:greaterEqual value="0" name="size">
						<logic:iterate id="certificate" name="certificateTypes">
							<bean:define id="id" name="certificate" property="id"></bean:define>
							<html:option value="<%=id.toString()%>">
								<bean:write name="certificate" property="certificateName"></bean:write>
							</html:option>
						</logic:iterate>
					</logic:greaterEqual>
				</logic:present>
			</html:select>
		</div>
	</div>

<div class="clear"></div>
<footer class="mt-2 card-footer">
	<div class="row justify-content-end">
		<div class="col-sm-9 wrapper-btn">
	<html:submit value="Search" styleClass="btn btn-primary" styleId="submit"></html:submit>
	<html:reset value="Clear" styleClass="btn btn-default" onclick="clearFields()"></html:reset>
	</div>
	</div>
	</footer>
	</section>
	</div>
</div>
</div>
</section>
