<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.fg.vms.customer.model.VendorMaster"%>

<SCRIPT>
	$(document).ready(
			function() {

				$('input:checkbox[name="selectedEmailAddress"][value=""]')
						.attr('disabled', true);
			});

	$(function() {

		// add multiple select / deselect functionality
		$("#selectall").click(function() {
			$('.case').attr('checked', this.checked);
		});

		// if all checkbox are selected, check the selectall checkbox
		// and viceversa
		$(".case").click(function() {

			if ($(".case").length == $(".case:checked").length) {
				$("#selectall").attr("checked", "checked");
			} else {
				$("#selectall").removeAttr("checked");
			}
		});
	});
</SCRIPT>

<script type="text/javascript">
	function clearFields() {
		window.location = "searchvendor.do?method=showMailNotificationPage";
	}
	function reportDownload() {
		window.location = "naicreportdownload.do?method=downloadReport";
	}
</script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<section role="main" class="content-body card-margin mt-5">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">
					<img src="images/VendorSearch.gif" />&nbsp;&nbsp;Search Vendor</h2>
			</header>
<div class="form-box card-body">

	<html:form action="/searchvendor.do?method=vendorAssessmentNameSearch">
		<html:javascript formName="searchVendorForm" />
		<div>
			<%@include file="search.jsp"%>
		</div>
	</html:form>
</div>
<div class="clear"></div>
</div>
</div>
</section>

<section role="main" class="content-body card-margin mt-4">
	<div class="row">
		<div class="col-lg-9 mx-auto">
<logic:present property="vendorsList" name="searchVendorForm">
	<header class="card-header">
		<h2 class="card-title pull-left">
		<img id="show" src="images/icon-registration.png" />&nbsp;&nbsp;Vendors</h2>
	</header>
	<div class="form-box card-body">
		<html:form action="/searchvendor.do?method=viewAssessmentMailId">
		<div style="max-height: 305px; overflow: auto;">
			<table class="main-table table table-bordered table-striped mb-0">
				<logic:present property="vendorsList" name="searchVendorForm">
					<bean:size id="size" property="vendorsList" name="searchVendorForm" />
					<logic:greaterThan value="0" name="size">
						<tr>
							<td>Select All <input type="checkbox"
								id="selectall" /></td>
							<td>Vendor Name</td>
							<td>Country</td>
							<td>NAICS</td>
							<td>Duns Number</td>
							<td>Registered Date</td>
							<td>Diverse</td>
							<td>Prime</td>
							<td>Approved</td>
						</tr>
						<logic:iterate property="vendorsList" name="searchVendorForm"
							id="list">
							<bean:define id="emailId" name="list" property="emailId"></bean:define>
							<tr>
								<td>
									<div id="checkboxes">
										<input type="checkbox" name="selectedEmailAddress"
											value="<%=emailId.toString()%>" class="case" id="chk_box" />
									</div>
								</td>
								<td><bean:write name="list" property="vendorName" /></td>
								<td><bean:write name="list" property="countryName" /></td>
								<td><bean:write name="list" property="naicsCode" /></td>
								<td><bean:write name="list" property="duns" /></td>
								<td><bean:write name="list" property="createdon"
										format="yyyy-MM-dd" /></td>
								<td><logic:equal value="1" name="list"
										property="divsersupplier">
												Yes
											</logic:equal> <logic:equal value="0" name="list"
										property="divsersupplier">
												No
											</logic:equal></td>
								<td><logic:equal value="1" name="list"
										property="primenonprimevendor">
												Prime
											</logic:equal> <logic:equal value="0" name="list"
										property="primenonprimevendor">
												Non-Prime
											</logic:equal></td>
								<td><logic:equal value="1" name="list"
										property="isapproved">
												Yes
											</logic:equal> <logic:equal value="0" name="list" property="isapproved">
												No
											</logic:equal></td>
							</tr>

						</logic:iterate>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
						<tr>
							<td>No such Records</td>
						</tr>
					</logic:equal>
				</logic:present>
			</table>
			</div>
			<div class="clear"></div>
			<logic:present property="vendorsList" name="searchVendorForm">
				<bean:size id="size" property="vendorsList" name="searchVendorForm" />
				<logic:greaterThan value="0" name="size">
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
								<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
								<html:reset value="Reset" styleClass="btn"></html:reset>
							</div>
						</div>
					</footer>
				</logic:greaterThan>
			</logic:present>				
		</html:form>
		</div>
</logic:present>
</div>

</div>
</section>




