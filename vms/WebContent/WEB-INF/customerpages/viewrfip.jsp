
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>

<div id="table-holder"
	style="margin: 0 1.5%; padding: 20px 0 10px 90px;">
	<table cellpadding="10px" cellspacing="10px">
		<tr>
			<td align="left" valign="top">RFI Number</td>
			<td align="left" valign="top" width="345"><html:text
					property="rfiNumber" alt="" name="rfiInformation" readonly="true" /><span
				class="error"><html:errors property="rfiNumber"></html:errors></span></td>
			<td align="left" valign="top">Contact Person</td>
			<td align="left" valign="top"><html:text
					property="contactPerson" alt="Optional" name="rfiInformation"
					readonly="true" /></td>
		</tr>
		<tr>
			<td align="left" valign="top">RFI Start Date</td>
			<td align="left" valign="top" width=""><html:text
					property="rfiStartDate" styleId="rfiStartDate" alt="Please click to select date"
					name="rfiInformation" readonly="true" /><span class="error"><html:errors
						property="rfiStartDate"></html:errors></span></td>

			<td align="left" valign="top">RFI End Date</td>
			<td align="left" valign="top" width=""><html:text
					property="rfiEndDate" styleId="rfiEndDate" alt="Please click to select date"
					name="rfiInformation" readonly="true" /><span class="error">
					<html:errors property="rfiEndDate"></html:errors>
			</span></td>
		</tr>
		<tr>
			<td align="left" valign="top">Email Id</td>
			<td align="left" valign="top"><html:text property="emailId"
					alt="Optional" name="rfiInformation" readonly="true"
					styleId="emailAddress" onblur="return email_validate(this.value)" /></td>
			<td align="left" valign="top">Phone Number</td>
			<td align="left" valign="top"><html:text property="phoneNumber"
					alt="Optional" name="rfiInformation" readonly="true" /></td>
		</tr>
		<tr>
			<td align="left" valign="top">RFI Description</td>
			<td align="left" valign="top"><html:textarea
					property="rfiDescription" name="rfiInformation" style="width:75%;"
					readonly="true" /></td>
			<td align="left" valign="top">Currency</td>
			<td align="left" valign="top"><html:text property="currency"
					readonly="true" name="rfiInformation">

				</html:text></td>
		</tr>
	</table>
</div>