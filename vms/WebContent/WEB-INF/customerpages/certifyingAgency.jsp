<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<script type="text/javascript">
<!--
	$(document).ready(function() {		
		$(function($) {
			  $("#phone").mask("(999) 999-9999?");
			  $("#fax").mask("(999) 999-9999?");
		});
		
		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
		}, "No Special Characters Allowed.");
		
		$("#agencyForm").validate({
			rules : {
				agencyName : {
					required : true,
					alpha : true,
					maxlength : 255
				},
				agencyShortName : {
					required : true,
					alpha : true,
					maxlength : 255
				},
				phone : {
					maxlength : 20
				}
			}
		});
	});
	
	function backToAgency() {
		window.location = "viewagencies.do?method=view";
	}
//-->
</script>


<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-8 mx-auto">
		<section class="card">
		<div id="successMsg">
			<html:messages id="msg" property="agency" message="true">
				<div class="alert alert-info nomargin">				
					<bean:write name="msg" />
				</div>
			</html:messages>
			<html:messages id="msg" property="agencyUpdated" message="true">
				<div class="alert alert-info nomargin">
					<bean:write name="msg" />
				</div>
			</html:messages>
			<html:messages id="msg" property="agencyDeleted" message="true">
				<div class="alert alert-info nomargin">
					<bean:write name="msg" />
				</div>
			</html:messages>
			<html:messages id="msg" property="agencyReferency" message="true">
				<div class="alert alert-info nomargin">
				<bean:write name="msg" />
				</div>
			</html:messages>
		</div>
		
		<header class="card-header">
			<h2 class="card-title">Certifying Agency Entry</h2>
		</header>
		
			<div class="form-box card-body">	
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="Certifying Agency" name="privilege" property="objectId.objectName">
						<logic:match value="1" name="privilege" property="add">
							<html:form action="/certifyingAgency.do?method=insertagency" styleClass="FORM" styleId="agencyForm">
								<html:hidden property="id" />
								<html:javascript formName="certifyingAgencyForm" />
									<div class="form-group row row-wrapper">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Certifying Agency</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="agencyName" styleClass="text-box form-control"
													name="certifyingAgencyForm" alt="" styleId="agencyName"></html:text>
											</div>
											<span class="error"><html:errors property="agencyName"></html:errors></span>
										</div>
									<div class="form-group row row-wrapper">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Certifying Agency ShortName</label>
										<div class="col-sm-9 ctrl-col-wrapper">
											<html:text property="agencyShortName" styleClass="text-box form-control"
												name="certifyingAgencyForm" alt="" styleId="agencyShortName"></html:text>
										</div>
										<span class="error"><html:errors property="agencyShortName"></html:errors></span>
									</div>
								
									<div class="form-group row row-wrapper">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Phone Number</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="phone" styleClass="text-box form-control"
													name="certifyingAgencyForm" alt="" styleId="phone"></html:text>
											</div>
										</div>
										<div class="form-group row row-wrapper">
										<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Fax</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:text property="fax" styleClass="text-box form-control"
													name="certifyingAgencyForm" alt="" styleId="fax"></html:text>
											</div>
										</div>
								
										<div class="form-group row row-wrapper">
											<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Address</label>
											<div class="col-sm-9 ctrl-col-wrapper">
												<html:textarea property="address" styleClass="main-text-area form-control"
													name="certifyingAgencyForm" alt="" styleId="address"></html:textarea>
											</div>
										</div>
										
										<footer class="card-footer mt-4">
											<div class="row justify-content-end">
												<div class="col-sm-9 wrapper-btn">
													<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit1"></html:submit>
													<html:reset value="Clear" styleClass="btn btn-default" onclick="backToAgency();"></html:reset>
												</div>
											</div>
										</footer>
							</html:form>
						</logic:match>
					</logic:match>
				</logic:iterate>
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="Certifying Agency" name="privilege" property="objectId.objectName">
						<logic:match value="0" name="privilege" property="add">
							<div style="padding: 5%; text-align: center;">
								<h3>You have no rights to add certifying agency</h3>
							</div>
						</logic:match>
					</logic:match>
				</logic:iterate>	
			</div>
		</section>
	</div>
</div>


<div class="grid-wrapper">
		<div class="form-box">
			<div class="row">
				<div class="col-lg-12 mx-auto">
			<logic:iterate id="privilege" name="privileges">
				<logic:match value="Certifying Agency" name="privilege" property="objectId.objectName">
					<logic:match value="1" name="privilege" property="view">
					<header class="card-header">
						<h2 class="card-title">Certifying Agency List</h2>
					</header>
					<div id="grid_container" class="card-body">
						<table class="main-table table table-bordered table-striped mb-0">
							<logic:present name="certifyingAgencies">
								<bean:size id="size" name="certifyingAgencies" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<th>Cerfifying Agency</th>
											<th>Certifying Agency Short Name</th>
											<th>Phone</th>
											<th>Fax</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<logic:iterate name="certifyingAgencies" id="certifyingAgenciesList">
											<tr class="even">
												<td><bean:write name="certifyingAgenciesList" property="agencyName" /></td>
												<td><bean:write name="certifyingAgenciesList" property="agencyShortName" /></td>
												<td><bean:write name="certifyingAgenciesList" property="phone" /></td>
												<td><bean:write name="certifyingAgenciesList" property="fax" /></td>
												<bean:define id="id" name="certifyingAgenciesList" property="id"></bean:define>
												<td align="center">
													<logic:match value="1" name="privilege" property="modify">
														<html:link action="/viewagencies.do?method=editCertifyAgency" paramId="id" paramName="id">Edit</html:link>
														<logic:match value="1" name="privilege" property="delete">
															|
														</logic:match>										        
												    </logic:match>
								                   <logic:match value="1" name="privilege" property="delete">
										           		<html:link action="/viewagencies.do?method=deleteCertifyAgency" paramId="id" paramName="id" onclick="return confirm_delete();">
									               			Delete
										           	 	</html:link>
											       </logic:match>
								               </td>
											</tr>
										</logic:iterate>
									</tbody>
								</logic:greaterThan>
							</logic:present>
						</table>
						</div>
					</logic:match>
				</logic:match>
			</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Certifying Agency" name="privilege" property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no rights to view certifying agency</h3>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>
</div>
</div>
</div>
</div>
</section>
