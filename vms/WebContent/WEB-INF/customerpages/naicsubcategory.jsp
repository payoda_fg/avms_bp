<%@page import="com.fg.vms.customer.model.NaicsCategory"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<style>
<!--
textarea.catecoryDescription {
	width: 300px;
	height: 50px;
}
-->
</style>
<script type="text/javascript">
<!--
	function eraseFields() {
		window.location = "viewnaicsubcategory.do?parameter=viewNAICSubCategory";
	}
	$(document).ready(function() {

		$('#subCategoryTable').tableScroll({
			height : 150
		});
	});
//-->
</script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/validation.js"></script>

<div id="form-container" style="height: 100%; width: 95%;"
	onload="getCategory();">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">

		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;NAICS
				Sub-Category Entry
			</h2>
		</div>

		<%
			NaicsCategory selectCategory = (NaicsCategory) session
					.getAttribute("categoryId2");
			String cateValue = "0";
			String selectCategoryName = "";
			if (selectCategory != null) {
				cateValue = selectCategory.getId().toString();
				selectCategoryName = selectCategory.getNaicsCategoryDesc();
			}
		%>
		<div id="successMsg">
			<html:messages id="msg" property="master" message="true">
				<span><bean:write name="msg" /></span>
			</html:messages>
			<html:messages id="msg" property="referenceSubCategory"
				message="true">
				<span style="color: red;"><bean:write name="msg" /></span>
			</html:messages>
		</div>
		<html:form
			action="/naicsubcategory.do?parameter=insertNAICSubCategory">
			<html:javascript formName="naicSubCategoryForm" />

			<div id="content" style="padding: 0 0 10px 20px;"
				onload="getCategory()">

				Category
				<bean:define id="category" name="categoryName1"></bean:define>
				<html:select property="naicCategory" onchange="getCategory()"
					value="<%=cateValue %>" styleId="naicCategory"
					style="font-size:11px;width:140px;">
					<bean:size id="size" name="categoryName1" />
					<logic:greaterEqual value="0" name="size">
						<html:option value="0" key="select">-----Select-----</html:option>
						<logic:iterate id="categories" name="categoryName1">
							<bean:define id="id" name="categories" property="id"></bean:define>
							<bean:define id="naicsCategoryDesc" name="categories"
								property="naicsCategoryDesc"></bean:define>
							<html:option value="<%=id.toString() %>"
								key="<%=naicsCategoryDesc.toString() %>">
								<bean:write name="categories" property="naicsCategoryDesc" />
							</html:option>
						</logic:iterate>
					</logic:greaterEqual>
				</html:select>
				<html:button property="Add" value="Add" styleClass="customerbtTxt"
				onclick="enableSubCategoryDesc()"
				style="float:right;margin:0%;" />
			</div>

			<div id="table-holder" style="margin: 1% 1.2%; padding: 0 0 0 30%;">
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="NAICS Sub-category" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="add">
							<table cellspacing="10" cellpadding="5">
								<tr>
									<td>NAICS Sub-Category Description</td>
									<td><html:textarea property="naicSubCategoryDesc"
											name="naicSubCategoryForm" disabled="true"
											styleClass="catecoryDescription"
											styleId="naicSubCategoryDesc"></html:textarea><span
										class="error"><html:errors
												property="naicSubCategoryDesc"></html:errors></span></td>
								</tr>
								<tr>
									<td>IsActive&nbsp;</td>
									<td><html:radio property="isActive" value="1"
											disabled="true" styleId="isActive">&nbsp;Yes&nbsp;</html:radio>
										<html:radio property="isActive" value="0" disabled="true"
											styleId="isActive">&nbsp;No&nbsp;</html:radio></td>
								</tr>

							</table>

							<div style="padding: 0 0 1% 20%;">
								<html:submit value="Submit" styleClass="customerbtTxt"
									styleId="Save" onclick=""></html:submit>
								<html:reset value="Clear" styleClass="customerbtTxt"
									styleId="Cancel" onclick="eraseFields();"></html:reset>
							</div>
						</logic:match>
					</logic:match>
				</logic:iterate>
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="NAICS Sub-category" name="privilege"
						property="objectId.objectName">
						<logic:match value="0" name="privilege" property="add">
							<div style="padding: 5%; text-align: center;">
								<h3>You have no rights to add sub-category</h3>
							</div>
						</logic:match>
					</logic:match>
				</logic:iterate>

			</div>

		</html:form>



		<logic:iterate id="privilege" name="privileges">

			<logic:match value="NAICS Sub-category" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">

					<h2>
						<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Available
						NAICS Sub-Categories&nbsp;&nbsp;<span style="color: green;"><%=selectCategoryName%></span>
					</h2>
					<div id="box" style="margin-left: 1.5%;">
						<table width="96%" id="subCategoryTable">
							<bean:size id="size" name="naicsub1" />
							<logic:greaterThan value="0" name="size">
								<thead>
									<tr>
										<th>NAICS Sub-Category Description</th>
										<th>IsActive</th>
										<th>Actions</th>
									</tr>
								</thead>
								<logic:iterate name="naicsub1" id="naicsublist">
									<tbody>
										<tr>
											<td><bean:write name="naicsublist"
													property="naicSubCategoryDesc" /></td>
											<td>Yes</td>
											<bean:define id="naicsubCategoryId" name="naicsublist"
												property="id"></bean:define>
											<td><logic:iterate id="privilege" name="privileges">
													<logic:match value="NAICS Sub-category" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="modify">
															<logic:iterate id="privilege" name="privileges">

																<logic:match value="NAICS Sub-category" name="privilege"
																	property="objectId.objectName">
																	<logic:match value="1" name="privilege"
																		property="modify">
																		<html:link paramId="id" paramName="naicsubCategoryId"
																			action="/retrivesubcategory.do?parameter=retriveNAICSubCategory">Edit</html:link>
																	</logic:match>
																</logic:match>

															</logic:iterate>
														</logic:match>
													</logic:match>
												</logic:iterate> <logic:iterate id="privilege" name="privileges">
													<logic:match value="NAICS Sub-category" name="privilege"
														property="objectId.objectName">
														<logic:match value="1" name="privilege" property="delete">
															<logic:iterate id="privilege" name="privileges">

																<logic:match value="NAICS Sub-category" name="privilege"
																	property="objectId.objectName">
																	<logic:match value="1" name="privilege"
																		property="delete">
															| <html:link
																			action="/deletesubcategory.do?parameter=deleteNAICSubCategory"
																			paramId="id" paramName="naicsubCategoryId"
																			onclick="return confirm_delete();">Delete</html:link>
																	</logic:match>
																</logic:match>
															</logic:iterate>
														</logic:match>
													</logic:match>
												</logic:iterate></td>
										</tr>
									</tbody>
								</logic:iterate>
							</logic:greaterThan>
							<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
						</table>
					</div>
				</logic:match>
			</logic:match>

		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:match value="NAICS Sub-category" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view sub-category</h3>
					</div>
				</logic:match>
			</logic:match>

		</logic:iterate>
	</div>
</div>
