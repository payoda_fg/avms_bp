<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
			$("#reportingperiodform").validate({
				rules : {
					fiscalStartDate : {
						required : true
					},
					fiscalEndDate : {
						required : true
					}
				}
			});

			$.datepicker.setDefaults({
				changeYear : true,
				dateFormat : 'mm/dd/yy',
				onClose : function(dateText, inst) {
					$('#fiscalStartDate').removeClass('text-label');
					$('#fiscalEndDate').removeClass('text-label');
				}
			});
			$("#fiscalStartDate").datepicker(
					{
						onSelect : function(selected) {
							$("#fiscalEndDate").datepicker("option",
									"minDate", selected);
						}
					});
			$("#fiscalEndDate").datepicker(
					{
						onSelect : function(selected) {
							$("#fiscalStartDate").datepicker("option",
									"maxDate", selected);
						}
					});

		});	

	// Function to reset the form
	function clearfields() {
		window.location = "reportingperiod.do?method=showPage";
	}
	
	//Function to Validate Start Date, End Date, and Periods.
	function validate()
	{
		var period=$('#frequencyPeriod').val();
		var sdate=$('#fiscalStartDate').val();
		var edate=$('#fiscalEndDate').val();			    
		var startdate = new Date(sdate);
	    var enddate = new Date(edate);
		var numOfMonth = (enddate.getYear() - startdate.getYear()) * 12 + enddate.getMonth() - startdate.getMonth() + 1 ;		
		
		if(period == "M" && numOfMonth==1)
		{
			return true;
		}
		else if(period == "M" && numOfMonth!=1)
		{
			alert("Number of Month Should Be 1, for Monthly");
			return false;
		}
		if(period == "Q" && numOfMonth==3)
		{
			return true;
		}
		else if(period == "Q" && numOfMonth!=3)
		{
			alert("Number of Month Should Be 3, for Quarterly");
			return false;
		}
		if(period == "A" && numOfMonth==12)
		{
			return true;
		}
		else if(period == "A" && numOfMonth!=12)
		{
			alert("Number of Month Should Be 12, for Yearly");
			return false;
		}
	}	
</script>
<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-9 mx-auto">	
		<div id="successMsg">
			<html:messages id="msg" property="successMsg" message="true">
				<div class="alert alert-info nomargin"><bean:write name="msg" />
				</div>
			</html:messages>
		</div>
		<div id="failureMsg">
			<html:messages id="msg" property="errorMsg" message="true">
				<div class="alert alert-danger nomargin"><bean:write name="msg" />
				</div>>
			</html:messages>
		</div>
		<section class="card">
			<header class="card-header">
				<h2 class="card-title pull-left">Tier2 Reporting Period</h2>
			</header>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Reporting Period" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">
					<div class="form-box card-body">		
						<html:form action="/savereport?method=saveReportingPeriod"
							styleId="reportingperiodform">
							<html:hidden property="reportingPeriodId" />
								<div class="row-wrapper form-group row">
									<label class="label-col-wrapper control-label text-sm-right col-sm-3">Frequency Period</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<%-- 								<html:text property="frequencyPeriod" alt=""
											styleClass="text-box" disabled="true" value="Quarter" 
											name="tier2RepotingForm" /> --%>
										<%
											String select = "selected='selected'";
										%>
										<html:select property="frequencyPeriod" name="tier2RepotingForm"
											styleClass="chosen-select-no-single form-control" styleId="frequencyPeriod">
											<html:option value="M">Monthly</html:option>
											<html:option value="Q">Quarterly</html:option>
											<html:option value="A">Annually</html:option>
										</html:select>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="label-col-wrapper control-label text-sm-right col-sm-3">Start Date</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text property="fiscalStartDate" styleClass="text-box form-control" readonly="true"
											 styleId="fiscalStartDate" alt="Please click to select date"
											name="tier2RepotingForm" />
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label class="label-col-wrapper control-label text-sm-right col-sm-3">End Date</label>
									<div class="col-sm-9 ctrl-col-wrapper">
										<html:text property="fiscalEndDate" alt="Please click to select date" styleClass="text-box form-control"
										styleId="fiscalEndDate" readonly="true"
											name="tier2RepotingForm" />
									</div>
									<span class="error"><html:errors
											property="certificateShortName"></html:errors>
									</span>
								</div>
							<logic:iterate id="privilege1" name="privileges">
								<logic:match value="Reporting Period" name="privilege1"
									property="objectId.objectName">
									<logic:match value="1" name="privilege1" property="add">
										<footer class="card-footer mt-2">
											<div class="row justify-content-end">
												<div class="col-sm-9 wrapper-btn">
													<html:submit value="Submit" styleClass="btn btn-primary" onclick="return validate();"></html:submit>
													<html:reset value="Cancel" styleClass="btn btn-default"
												onclick="clearfields();"></html:reset>
												</div>										
											</div>
										</footer>
									</logic:match>
								</logic:match>
							</logic:iterate>
							<logic:iterate id="privilege1" name="privileges">
								<logic:match value="Reporting Period" name="privilege1"
									property="objectId.objectName">
									<logic:match value="0" name="privilege1" property="add">
										<div style="text-align: center;">
											<h3>You have no rights to add reporting period</h3>
										</div>
									</logic:match>
								</logic:match>
							</logic:iterate>
						</html:form>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="Reporting Period" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
					<div style="text-align: center;">
						<h3>You have no rights to view reporting period</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>
		</section>
	</div>
</div>
</section>
