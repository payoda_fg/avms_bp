<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script>
	function backtoShowLog() {		
		window.location = "userLogTrack.do?parameter=viewUserLoginDetails";
	}
</script>

<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-12 mx-auto">	
			<section class="card">
				<header class="card-header">
					<h2 class="card-title pull-left">User Login Times</h2>
					<html:reset value="Cancel" styleClass="btn btn-primary pull-right" onclick="backtoShowLog();"
					></html:reset>
				</header>

				<div class="form-box card-body">
					<div>
						<table class="main-table table table-bordered table-striped mb-0">
							<thead>
								<tr>
									<td>Login Times</td>
									<td>User Name</td>
								</tr>
							</thead>
							<tbody>
								<logic:iterate name="loginTimes" id="loginTimesList">
									<tr>
										<td><bean:write name="loginTimesList" property="userLogon"/></td>
										<td><bean:write name="loginTimesList" property="username"/></td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</div>
				
				<!-- 	<div class="wrapper-btn"></div> -->
				</div>
			</section>
		</div>
	</div>
</section>