<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript">
<!--
	function clearfields() {
		window.location = "naicsCategoryView.do?method=view";
	}

	$(document).ready(function() {
		$('#naicsMasterTable').tableScroll({
			height : 150
		});

		$("#naicsCategoryForm").validate({
			rules : {
				naicsCategoryDesc : {
					required : true
				},
				webServiceURL : {
					required : true,
					url : true
				},
				webServiceParameters : {
					required : true
				},
				isWebService : {
					required : true
				}
			}
		});
	});
//-->
</script>
<style>
<!--
.textBox {
	width: 250px;
	font-size: 11px;
	height: 25px;
}
-->
</style>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 2%;">

		<logic:iterate id="privilege" name="privileges">
			<logic:match value="NAICS Category Master" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="modify">
					<div class="toggleFilter">
						<h2 id="show" title="Click here to hide/show the form"
							style="cursor: pointer;">
							<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Add
							NAICS Category
						</h2>
					</div>
					<div id="successMsg">
						<html:messages id="msg" property="category" message="true">
							<span style="color: green;"><bean:write name="msg" /></span>
						</html:messages>
					</div>
					<div id="successMsg">
						<html:messages id="msg" property="categoryreference"
							message="true">
							<span style="color: red;"><bean:write name="msg" /></span>
						</html:messages>
					</div>

					<html:form
						action="/updatenaicscategory.do?parameter=updateCategory"
						styleId="naicsCategoryForm">
						<html:javascript formName="naicsCategoryForm" />
						<html:hidden property="id" name="naicsCategoryForm" />
						<div id="table-holder" style="width: 96.5%; margin: 0 0 0 1.5%;">
							<table style="margin-left: 25%;" cellspacing="10" cellpadding="5">
								<tr>
									<td>NAICS Category</td>
									<td><html:text property="naicsCategoryDesc" alt=""
											name="naicsCategoryForm" styleId="naicsCategoryDesc" /><span
										class="error"><html:errors property="naicsCategoryDesc"></html:errors></span></td>
								</tr>
								<tr>
									<td>WebService URL (http://)</td>
									<td><html:text property="webServiceURL" alt=""
											name="naicsCategoryForm" styleId="webServiceURL" /><span
										class="error"><html:errors property="webServiceURL"></html:errors></span></td>
								</tr>
								<tr>
									<td>WebService Parameters</td>
									<td><html:text property="webServiceParameters" alt=""
											name="naicsCategoryForm" styleId="webServiceParameters" /><span
										class="error"><html:errors
												property="webServiceParameters"></html:errors></span></td>
								</tr>
								<tr>
									<td>WebService</td>
									<td><html:radio property="isWebService" value="1"
											styleId="isWebService" name="naicsCategoryForm">&nbsp;Yes&nbsp;</html:radio>
										<html:radio property="isWebService" value="0"
											styleId="isWebService" name="naicsCategoryForm">&nbsp;No&nbsp;</html:radio></td>
								</tr>
								<tr>
									<td>Active&nbsp;</td>
									<td><html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
										<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
									</td>
								</tr>
							</table>
							<div style="margin: 0 0 1% 33%;">
								<logic:iterate id="privilege" name="privileges">
									<logic:match value="NAICS Category Master" name="privilege"
										property="objectId.objectName">
										<logic:match value="1" name="privilege" property="modify">
											<html:submit value="Update" styleClass="customerbtTxt"
												styleId="submit"></html:submit>
											<html:reset value="Cancel" styleClass="customerbtTxt"
												onclick="clearfields();"></html:reset>
										</logic:match>
									</logic:match>
								</logic:iterate>
							</div>
						</div>
					</html:form>

					<logic:iterate id="privilege" name="privileges">

						<logic:match value="User" name="privilege"
							property="objectId.objectName">
							<logic:match value="1" name="privilege" property="view">
								<h2 id="show">
									<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;NAICS
									Category List
								</h2>
								<div id="box" style="margin-left: 1.5%;">
									<table width="96%" id="naicsMasterTable">
										<bean:size id="size" name="categorymaster" />
										<logic:greaterThan value="0" name="size">
											<thead>
												<tr>
													<th>NAICS Category</th>
													<th>WebService URL</th>
													<th>WebService Parameters</th>
													<th>WebService</th>
													<th>IsActive</th>
													<th>Actions</th>
												</tr>
											</thead>
											<logic:iterate name="categorymaster" id="categorylist">
												<tbody>
													<tr>
														<td style="width: 200px;"><bean:write
																name="categorylist" property="naicsCategoryDesc" /></td>
														<td style="width: 150px;"><bean:write
																name="categorylist" property="webserivceURL" /></td>
														<td style="width: 200px;"><bean:write
																name="categorylist" property="webserviceParameters" /></td>
														<td><logic:equal value="1" name="categorylist"
																property="isWebService">
										Yes
									</logic:equal> <logic:equal value="0" name="categorylist"
																property="isWebService">
										No
									</logic:equal></td>
														<td><logic:equal value="1" name="categorylist"
																property="isActive">
										Yes
									</logic:equal> <logic:equal value="0" name="categorylist" property="isActive">
										No
									</logic:equal></td>
														<bean:define id="objectId" name="categorylist"
															property="id"></bean:define>
														<td><logic:iterate id="privilege" name="privileges">
																<logic:match value="NAICS Category Master"
																	name="privilege" property="objectId.objectName">
																	<logic:match value="1" name="privilege"
																		property="modify">
																		<html:link
																			action="/retrievecategory.do?parameter=retrive"
																			paramId="id" paramName="objectId">Edit</html:link>
																	</logic:match>
																</logic:match>
															</logic:iterate> | <logic:iterate id="privilege" name="privileges">
																<logic:match value="NAICS Category Master"
																	name="privilege" property="objectId.objectName">
																	<logic:match value="1" name="privilege"
																		property="delete">
																		<logic:notEqual value="<%=objectId.toString() %>"
																			property="id" name="naicsCategoryForm">
																			<html:link
																				action="/deletecategory.do?parameter=deletecategory"
																				paramId="id" paramName="objectId"
																				onclick="return confirm_delete();">Delete</html:link>
																		</logic:notEqual>
																		<logic:equal value="<%=objectId.toString() %>"
																			property="id" name="naicsCategoryForm">
																			<span style="color: green; font-weight: bold;">Category
																				in Edit</span>
																		</logic:equal>
																	</logic:match>
																</logic:match>
															</logic:iterate></td>
													</tr>
												</tbody>
											</logic:iterate>
										</logic:greaterThan>
										<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
									</table>
								</div>
							</logic:match>
						</logic:match>
					</logic:iterate>

					<logic:iterate id="privilege" name="privileges">
						<logic:match value="NAICS Category Master" name="privilege"
							property="objectId.objectName">
							<logic:match value="0" name="privilege" property="view">
								<div style="padding: 5%; text-align: center;">
									<h3>You have no rights to view NAICS category master</h3>
								</div>
							</logic:match>
						</logic:match>
					</logic:iterate>

				</logic:match>
			</logic:match>
		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">
			<logic:match value="NAICS Category Master" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="modify">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to edit NAICS category master</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>
	</div>
</div>
