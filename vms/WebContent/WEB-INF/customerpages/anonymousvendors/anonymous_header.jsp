<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html class="fixed sidebar-left-collapsed">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />


<!-- Porto Theme css -->
<!-- Web Fonts  -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
	rel="stylesheet" type="text/css" />

<!-- Vendor CSS -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" />
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css" />
<!--  <link href="jquery/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />-->
<link href="css/tabber.css" type="text/css" rel="stylesheet"></link>
<link href="select2/select2.css" rel="stylesheet" />

<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/validation.js"></script>

<script src="jquery/js/jquery-1.7.2.min.js"></script>
<script src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>
<script src="jquery/js/jquery.placeholder.js"></script>
<script src="select2/select2.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>	
<script src="js/nanoscroller.js"></script>
<script src="js/theme.js"></script>
<script src="jquery/ui/jquery.ui.autocomplete.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script src="js/theme.init.js"></script>
 <script type="text/javascript" src="js/tabber.js"></script>

<script type="text/javascript">
	ddaccordion.init({
		headerclass : "submenuheader", //Shared CSS class name of headers group
		contentclass : "submenu", //Shared CSS class name of contents group
		revealtype : "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
		mouseoverdelay : 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
		collapseprev : true, //Collapse previous content (so only one open at any time)? true/false 
		defaultexpanded : [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
		onemustopen : false, //Specify whether at least one header should be open always (so never all headers closed)
		animatedefault : false, //Should contents open by default be animated into view?
		persiststate : true, //persist state of opened contents within browser session?
		toggleclass : [ "", "active" ], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
		//Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
		animatespeed : "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
		oninit : function(headers, expandedindices) { //custom code to run when headers have initalized
			/* alert(headers) */
			/* $(".submenuheader").click(function() {
				var $curr = $(this);
				$("a").removeClass("active");
				$("div").removeClass("active");
				$(this).addClass("active");
				$curr.parent().prev().addClass("active");
			}); */
			/* $('.box-title').addClass("active"); */
			/* $('#menuselect' + expandedindices).addClass("active"); */
		},
		onopenclose : function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
			//do nothing
			// create vendor menu option.
			/*  if (index == 0 && isuseractivated && state == 'block') {
				window.location = "viewVendor.do?method=view";
			}
			// search vendor menu option.
			if (index == 2 && isuseractivated && state == 'block') {
				window.location = "viewVendors.do?method=showVendorSearch";
			}
			alert(header + "  " + index + "  " + state + "  "
					+ isuseractivated) */
		}
	});
	
</script>


<header class="header">
<div class="logo-container">
	<div class=""> <logic:present name="userDetails">
		<bean:define id="logoPath" name="userDetails"
			property="settings.logoPath"></bean:define>
		<html:link action="customerhome.do?method=customerHomePage"><img
			src="images/logo.png"
			height="56px" /></html:link>
	</logic:present> </div>
	<!-- <aside class="top-links"> <span class="customer"></span> Guest
	| Welcome | </aside> -->
</div>
</header>
<!--Header Pane End Here-->
<style>
/*.chosen-select {
	width: 150px;
}

.chosen-select-width {
	width: 90%
}

.chosen-select-deselect: {
	width: 90%
}*/
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".chosen-select").select2();
		$("#serviceArea").select2({placeholder:"Click to select",width: "90%"});
		$("#businessArea").select2({placeholder:"Click to select",width: "90%"});
		$(".chosen-select-width").select2();
		$(".chosen-select-deselect").select2();
	});
	/*$('.panelCenter_1').panel({
		collapsible : false
	});*/
</script>
</html>