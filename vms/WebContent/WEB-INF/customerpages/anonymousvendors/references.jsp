<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<div class="clear"></div>
<div class="panelCenter_1" style="margin-bottom: 25%;">
	<h3>Reference 1</h3>
	<div class="wrapper-half">
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Name</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceName1" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceName1"  />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Address</div>
			<div class="ctrl-col-wrapper">
				<html:textarea property="referenceAddress1"
					styleId="referenceAddress1" alt="Optional"
					name="anonymousVendorForm" styleClass="main-text-area"
					 />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Phone</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referencePhone1" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referencePhone1"  />
			</div>
			<span class="error"> <html:errors property="referencePhone1"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Email</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceMailId1" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceMailId1"  />
			</div>
			<span class="error"> <html:errors property="referenceMailId1"></html:errors></span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Mobile</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceMobile1" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceMobile1"  />
			</div>
			<span class="error"> <html:errors property="referenceMobile1"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">City</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceCity1" alt="Optional"
					styleClass="text-box" styleId="referenceCity1"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceCity1"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Zip</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceZip1" alt="Optional"
					styleClass="text-box" styleId="referenceZip1"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceZip1"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">State</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceState1" alt="Optional"
					styleId="referenceState1" styleClass="text-box"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceState1"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Country</div>
			<div class="ctrl-col-wrapper">
				<html:select property="referenceCountry1"
					styleId="referenceCountry1" styleClass="main-list-box"
					name="anonymousVendorForm" style="height: 30px;">
					<html:option value="0" key="select">-----Select-----</html:option>
					<logic:present name="countries">
						<bean:size id="size" name="countries" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countries">
								<bean:define id="name" name="country" property="name"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="name" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</logic:present>
				</html:select>
			</div>
		</div>
	</div>
</div>

<div class="panelCenter_1" style="margin-bottom: 25%;">
	<h3>Reference 2</h3>
	<div class="wrapper-half">
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Name</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceName2" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceName2"  />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Address</div>
			<div class="ctrl-col-wrapper">
				<html:textarea property="referenceAddress2"
					styleId="referenceAddress2" alt="Optional"
					name="anonymousVendorForm" styleClass="main-text-area"
					 />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Phone</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referencePhone2" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referencePhone2"  />
			</div>
			<span class="error"> <html:errors property="referencePhone2"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Email</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceMailId2" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceMailId2"  />
			</div>
			<span class="error"> <html:errors property="referenceMailId2"></html:errors></span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Mobile</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceMobile2" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceMobile2"  />
			</div>
			<span class="error"> <html:errors property="referenceMobile2"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">City</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceCity2" alt="Optional"
					styleClass="text-box" styleId="referenceCity2"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceCity2"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Zip</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceZip2" alt="Optional"
					styleClass="text-box" styleId="referenceZip2"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceZip2"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">State</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceState2" alt="Optional"
					styleId="referenceState2" styleClass="text-box"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceState2"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Country</div>
			<div class="ctrl-col-wrapper">
				<html:select property="referenceCountry2"
					styleId="referenceCountry2" styleClass="main-list-box"
					name="anonymousVendorForm" style="height: 30px;">
					<html:option value="0" key="select">-----Select-----</html:option>
					<logic:present name="countries">
						<bean:size id="size" name="countries" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countries">
								<bean:define id="name" name="country" property="name"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="name" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</logic:present>
				</html:select>
			</div>
		</div>
	</div>
</div>
<div class="panelCenter_1" style="margin-bottom: 25%;">
	<h3>Reference 3</h3>
	<div class="wrapper-half">
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Name</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceName3" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceName3"  />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Address</div>
			<div class="ctrl-col-wrapper">
				<html:textarea property="referenceAddress3"
					styleId="referenceAddress3" alt="Optional"
					name="anonymousVendorForm" styleClass="main-text-area"
					 />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Phone</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referencePhone3" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referencePhone3"  />
			</div>
			<span class="error"> <html:errors property="referencePhone3"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Email</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceMailId3" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceMailId3"  />
			</div>
			<span class="error"> <html:errors property="referenceMailId3"></html:errors></span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Mobile</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceMobile3" alt="Optional"
					styleClass="text-box" name="anonymousVendorForm"
					styleId="referenceMobile3"  />
			</div>
			<span class="error"> <html:errors property="referenceMobile3"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">City</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceCity3" alt="Optional"
					styleClass="text-box" styleId="referenceCity3"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceCity3"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Zip</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceZip3" alt="Optional"
					styleClass="text-box" styleId="referenceZip3"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceZip3"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">State</div>
			<div class="ctrl-col-wrapper">
				<html:text property="referenceState3" alt="Optional"
					styleId="referenceState3" styleClass="text-box"
					name="anonymousVendorForm"  />
			</div>
			<span class="error"> <html:errors property="referenceState3"></html:errors>
			</span>
		</div>
		<div class="row-wrapper form-group row">
			<div class="label-col-wrapper">Country</div>
			<div class="ctrl-col-wrapper">
				<html:select property="referenceCountry3"
					styleId="referenceCountry3" styleClass="main-list-box"
					name="anonymousVendorForm" style="height: 30px;">
					<html:option value="0" key="select">-----Select-----</html:option>
					<logic:present name="countries">
						<bean:size id="size" name="countries" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="country" name="countries">
								<bean:define id="name" name="country" property="name"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="country" property="name" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</logic:present>
				</html:select>
			</div>
		</div>
	</div>
</div>
