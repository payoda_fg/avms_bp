<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<html class="fixed sidebar-left-collapsed">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="Author" content="" />
<meta name="keyphrase" content="" />
<meta name="Title" content="" />
<meta name="classification" content="" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="subject" content="" />
<meta name="page-type" content="" />
<meta name="audience" content="all" />
<meta name="revisit-after" content="7 days" />
<meta name="robots" content="index,follow" />
<meta http-equiv="expires" content="0" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/style1.css" rel="stylesheet" type="text/css" />
<link href='css/fontFace.css' rel='stylesheet' type='text/css'>
<script type="text/javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('slider');
	document.createElement('aside');
	document.createElement('figure');
	document.createElement('article');
	document.createElement('section');
	document.createElement('footer');
	
</script>

<link href="css/form-style.css" type="text/css" rel="stylesheet"></link>
<link href="css/tableStyle.css" type="text/css" rel="stylesheet"></link>
<link href="css/tabber.css" type="text/css" rel="stylesheet"></link>
<link href="css/tableStyle-RFQ.css" type="text/css" rel="stylesheet"></link>
<link href="css/Color_custom.css" type="text/css" rel="stylesheet"></link>
<link href="css/bluetabs.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript">
<!--
	$(document).ready(function() {
		
		alert('inside page');

		$('.panelCenter_1').panel({
			collapsible : false
		});
	});

	function backHome() {
		window.location = "home.do?method=loginPage";
	}
//-->
</script>
</head>
<body>

	<div class="page-margin-wrapper">
		<div>
			<header>
				<div class="page-wrapper">
					<aside class="logo">
						<logic:present name="userDetails">
							<bean:define id="logoPath" name="userDetails"
								property="settings.logoPath"></bean:define>
							<a href="#"><img src="<%=request.getContextPath() %>/images/dynamic/?file=<%=logoPath%>" height="80px" /></a>
						</logic:present>
					</aside>
					<!-- <aside class="top-links"> <span class="customer"></span> Guest
	| Welcome | </aside> -->
				</div>
			</header>
		</div>
		<div class="clear"></div>
		<%-- <div class="bluetabs-bg">
			<tiles:insert name="menu" attribute="menu" />
		</div> --%>
		<div class="clear"></div>
		<div>
			<div id="successMsg">
				<h4>Vendor Registration Successful.</h4>
			</div>
			<div style="margin: 20% 0% 10% 0%; text-align: center;">
				<html:reset styleClass="btTxt submit" value="Back To Login Page"
					onclick="backHome();" />
			</div>
		</div>
		<div class="clear"></div>
		<div>
			<footer>
				<!--Footer Start Here-->
				<div class="page-wrapper text-sm-right">
					<div class="copy">� 2013, All Rights Reserved</div>
					<div class="logo">Powered by</div>
				</div>
				<!--Footer End Here-->
			</footer>
		</div>
		<div class="clear"></div>
	</div>
</body>

</html>