<%@page import="com.fg.vms.admin.model.Users"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript">
<!--
	function backList() {
		window.location = "approval.do?method=loginforapproval";
	}

	$(document).ready(function() {

		$('.panelCenter_1').panel({
			collapsible : false
		});
	});
//-->
</script>
<div id="form-container">
	<div id="form-container-in">
		<div id="successMsg">
			<html:messages id="msg" property="mailtoadmin" message="true">
				<span style="color: green;"><bean:write name="msg" /></span>
			</html:messages>
		</div>
		<html:form action="/retrieveapproval.do?parameter=approveVendorByURL"
			enctype="multipart/form-data">
			<html:javascript formName="anonymousVendorForm" />

			<html:hidden property="currentVendorId" name="anonymousVendorForm" />

			<h1>Supplier Registration</h1>
			<div class="toggleFilter">
				<h2 id="show" title="Click here to hide/show the form"
					class="slidableHeader">
					<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Customer
					Information
				</h2>
			</div>

			<jsp:include page="rfiinformation.jsp" />

			<div class="toggleFilter">
				<h2 id="show2" title="Click here to hide/show the form"
					class="slidableHeader">
					<img id="showx" src="images/arrow-down.png" />&nbsp;&nbsp;NAICS
					Details
				</h2>
			</div>

			<jsp:include page="naics.jsp" />

			<div class="toggleFilter">
				<h2 id="show3" title="Click here to hide/show the form"
					class="slidableHeader">
					<img id="showy" src="images/arrow-down.png" />&nbsp;&nbsp;Diverse
					/ Supplier Certifications
				</h2>
			</div>

			<jsp:include page="diversecertificates.jsp" />

			<div class="toggleFilter">
				<h2 id="show4" title="Click here to hide/show the form"
					class="slidableHeader">
					<img id="showz" src="images/arrow-down.png" />&nbsp;&nbsp;Contact
					Information
				</h2>
			</div>

			<jsp:include page="contactinformation.jsp" />

			<div class="toggleFilter">
				<h2 id="show5" title="Click here to hide/show the form"
					class="slidableHeader">
					<img id="showi" src="images/arrow-down.png" />&nbsp;&nbsp;References
				</h2>
			</div>

			<jsp:include page="references.jsp" />

			<div class="save-btn" style="padding-left: 45%;">
				<input type="submit" name="button2" id="submit" value="Approve"
					class="customerbtTxt" /> <input type="reset" value="Back"
					onclick="backList();" class="customerbtTxt" />
			</div>
		</html:form>
	</div>
</div>
