<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<div id="successMsg">
	<html:messages id="msg" property="vendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="clear"></div>
<div class="panelCenter_1">
	<h3>Supplier</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">
					<bean:message key="comp.name" />
				</div>
				<div class="ctrl-col-wrapper">
					<html:text property="vendorName" alt="" name="anonymousVendorForm"
						styleId="vendorName" styleClass="text-box" tabindex="1" />
				</div>
				<span class="error"><html:errors property="vendorName"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Year Established</div>
				<div class="ctrl-col-wrapper">

					<html:text property="yearOfEstablishment" alt=""
						styleClass="text-box" name="anonymousVendorForm"
						styleId="yearOfEstablishment" tabindex="2"
						onblur="return currentYearValidation('yearOfEstablishment');" />
				</div>
				<span class="error"> <html:errors
						property="yearOfEstablishment"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Duns Number</div>
				<div class="ctrl-col-wrapper">
					<html:text property="dunsNumber" styleId="dunsNumber"
						styleClass="text-box" onchange="ajaxFn(this, 'D')" alt=""
						name="anonymousVendorForm" tabindex="3" />
				</div>
				<span class="error"> <html:errors property="dunsNumber"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Tax ID</div>
				<div class="ctrl-col-wrapper">
					<html:text property="taxId" onchange="ajaxFn(this, 'T')" alt=""
						tabindex="4" styleId="taxId" name="anonymousVendorForm"
						styleClass="text-box" />
				</div>
				<span class="error"> <html:errors property="taxId"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Company Type</div>
				<div class="ctrl-col-wrapper">
					<html:select property="companytype" styleId="companytype" style="height: 30px;"
						styleClass="list-box" name="anonymousVendorForm" tabindex="5">
						<html:option value="">----- Select -----</html:option>
						<html:option value="CO">Corporation</html:option>
						<html:option value="PA">Partnership</html:option>
						<html:option value="SP">Sole Proprietorship</html:option>
						<html:option value="FR">Franchise</html:option>
						<html:option value="LL">Limited Liability</html:option>
					</html:select>
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Number of Employees</div>
				<div class="ctrl-col-wrapper">
					<html:text property="numberOfEmployees" alt="" tabindex="6"
						styleClass="text-box" styleId="numberOfEmployees"
						name="anonymousVendorForm" />
				</div>
				<span class="error"> <html:errors
						property="numberOfEmployees"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Annual Revenue (USD)</div>
				<div class="ctrl-col-wrapper">
					<html:text property="annualTurnover" alt="" styleClass="text-box"
						styleId="annualTurnover" name="anonymousVendorForm"
						onblur="moneyFormatToUS();" tabindex="7" />

					<html:hidden styleId="turnoverFormatValue"
						property="annualTurnoverFormat" name="anonymousVendorForm" alt="" />
				</div>
				<span class="error"> <html:errors property="annualTurnover"></html:errors>
				</span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

<div class="panelCenter_1">
	<h3>Address</h3>

	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Address</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="address1" alt="" styleId="address1"
						name="anonymousVendorForm" tabindex="8" styleClass="main-text-area" />
				</div>
				<span class="error"> <html:errors property="address1"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">City</div>
				<div class="ctrl-col-wrapper">
					<html:text property="city" alt="" styleId="city" tabindex="9"
						name="anonymousVendorForm" styleClass="text-box" />
				</div>
				<span class="error"> <html:errors property="city"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">State</div>
				<div class="ctrl-col-wrapper">
					<html:text property="state" alt="" styleId="state"
						name="anonymousVendorForm" styleClass="text-box" tabindex="10" />
				</div>
				<span class="error"> <html:errors property="state"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Province</div>
				<div class="ctrl-col-wrapper">
					<html:text property="province" alt="Optional" styleId="province"
						name="anonymousVendorForm" styleClass="text-box" tabindex="11" />
				</div>
				<span class="error"> <html:errors property="province"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Region</div>
				<div class="ctrl-col-wrapper">
					<html:text property="region" alt="Optional" styleId="region"
						name="anonymousVendorForm" styleClass="text-box" tabindex="12" />
				</div>
				<span class="error"> <html:errors property="region"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Country</div>
				<div class="ctrl-col-wrapper">
					<html:select property="country" styleId="country"
						styleClass="list-box" tabindex="13" style="height: 30px;">
						<html:option value="">-- Select --</html:option>
						<logic:present name="countries">
							<bean:size id="size" name="countries" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="country" name="countries">
									<bean:define id="name" name="country" property="name"></bean:define>
									<html:option value="<%=name.toString()%>">
										<bean:write name="country" property="name" />
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>
					</html:select>
				</div>
				<span class="error"> <html:errors property="country"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Zip Code</div>
				<div class="ctrl-col-wrapper">
					<html:text property="zipcode" alt="" styleId="zipcode"
						name="anonymousVendorForm" styleClass="text-box" tabindex="14" />
				</div>
				<span class="error"> <html:errors property="zipcode"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="mobile" alt="Optional" styleId="mobile"
						name="anonymousVendorForm" styleClass="text-box" tabindex="15" />
				</div>
				<span class="error"> <html:errors property="mobile"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="phone" alt="" styleId="phone" tabindex="16"
						name="anonymousVendorForm" styleClass="text-box" />
				</div>
				<span class="error"> <html:errors property="phone"></html:errors>
				</span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Fax</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="fax" alt="Optional"
						styleId="fax" name="anonymousVendorForm" tabindex="17" />
				</div>
				<span class="error"> <html:errors property="fax"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Website URL(http://)</div>
				<div class="ctrl-col-wrapper">
					<html:text property="website" alt="Optional" styleId="website"
						name="anonymousVendorForm" styleClass="text-box" tabindex="18" />
				</div>
				<span class="error"> <html:errors property="website"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="emailId" tabindex="19"
						styleId="emailId" alt="Optional" name="anonymousVendorForm" />
				</div>

				<span class="error"> <html:errors property="emailId"></html:errors></span>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
