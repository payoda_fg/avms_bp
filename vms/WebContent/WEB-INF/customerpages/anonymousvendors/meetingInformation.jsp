<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script> 
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<html:form action="/meetinginfo?method=saveMeetingInfo"
	styleId="meetingForm">
	<div class="panelCenter_1">
		<h3>Contact Meeting Information</h3>
		<div class="form-box">
			&nbsp;&nbsp;Have you met with a BP Supplier Diversity personnel recently?
			 <input type="checkbox" id="metBpSupplier"
				style="margin-top: 1%;">
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact First Name</div>
					<div class="ctrl-col-wrapper">
						<html:text property="contactFirstName" alt=""
							styleId="contactFirstName" styleClass="text-box" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact Last Name</div>
					<div class="ctrl-col-wrapper">
						<html:text property="contactLastName" alt=""
							styleId="contactLastName" styleClass="text-box" />
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact Date</div>
					<div class="ctrl-col-wrapper">
						<html:text property="contactDate" alt="Please click to select date" styleId="contactDate"
							styleClass="text-box" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact State</div>
					<div class="ctrl-col-wrapper">
						<html:select property="contactState" styleId="contactState"
							styleClass="chosen-select-width">
							<html:option value="">- Select -</html:option>
							<logic:present name="contactStates">
								<logic:iterate id="states" name="contactStates">
									<bean:define id="name" name="states" property="id"></bean:define>
									<html:option value="<%=name.toString()%>">
										<bean:write name="states" property="statename" />
									</html:option>
								</logic:iterate>
							</logic:present>
						</html:select>
					</div>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Contact Event</div>
					<div class="ctrl-col-wrapper">
						<html:textarea property="contactEvent" alt=""
							styleId="contactEvent" styleClass="main-text-area" />
					</div>
				</div>
			</div>
		</div>
		<div class="btn-wrapper" style="margin-left: 45%;">
			<html:submit value="Submit" styleClass="btn" styleId="submit1" />
			<html:link action="/home.do?method=loginPage" styleClass="btn">Log off</html:link>
		</div>
	</div>
</html:form>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		
		$('.panelCenter_1').panel({
			collapsible : false
		});
		
		var imgPath = "jquery/images/calendar.gif";
		$.datepicker.setDefaults({
// 			showOn : 'both',
// 			buttonImageOnly : true,
// 			buttonImage : imgPath,
			changeYear : true,
			/* minDate : 0, */
			dateFormat : 'mm/dd/yy'
			});
		$("#contactDate").datepicker(); 

		$("#meetingForm").validate({
			rules : {
				contactFirstName : {
					required : function(element) {
						if ($("#metBpSupplier").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactLastName : {
					required : function(element) {
						if ($("#metBpSupplier").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactState : {
					required : function(element) {
						if ($("#metBpSupplier").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactDate : {
					required : function(element) {
						if ($("#metBpSupplier").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactEvent : {
					required : function(element) {
						if ($("#metBpSupplier").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				}
			},
		});
	});
//-->
</script>
<script type="text/javascript">
	chosenConfig();
	function chosenConfig() {
		var config = {
			'.chosen-select' : {
				width : "90%"
			},
			'.chosen-select-deselect' : {
				allow_single_deselect : true,
				width : "90%"
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "90%"
			}

		};
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
	}
</script>
