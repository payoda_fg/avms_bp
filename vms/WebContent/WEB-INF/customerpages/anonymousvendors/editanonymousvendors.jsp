<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script class="jsbin" type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script> 
<link  href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/rfiinformation.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery/js/jquery.ui.dialog.js"></script>
<link href="select2/select2.css" rel="stylesheet"/>
<script src="select2/select2.js"></script>
<style>
	.chosen-select{width: 150px;} 
	.chosen-select-width{width : 90%}
	.chosen-select-deselect:{width : 90%}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#tabs").easyResponsiveTabs();
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});
		if(document.getElementById("otherCert1") != null){
			document.getElementById("otherCert1").onchange = function() {
				document.getElementById("otherFile1").innerHTML = this.value;
			};
		}
		$(".chosen-select").select2();
		$(".chosen-select-width").select2(); 
		$("#businessArea").select2();
	});
	function cancle() {
		window.location = "viewSubVendors.do?method=subVendorSearch";
	}

	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement
				: null);
		if (evt != null
				&& node != null
				&& ((evt.keyCode == 13) && ((node.type == "text")
						|| (node.type == "radio") || (node.type == "textarea") || (node.type == "checkbox")))) {
			if ($('#authentication').is(":checked")) {
				return true;
			} else if (node.id == 'search') {
			} else {
				alert("Please certify that the information you have provided is true and accurate. ");
				return false;
			}
		}
	}
	document.onkeypress = stopRKey;
</script>
<style>
fieldset {
	padding: 0;
	border: 0;
	margin-top: 25px;
}
</style>
<html:form enctype="multipart/form-data" styleClass="FORM"
			action="/selfregistration?method=selfRegistration" styleId="vendorForm" >
<div class="page-title"><img src="images/edit-cust.gif"/>&nbsp;&nbsp;&nbsp;Update
			Supplier Information
	<html:submit value="More" styleClass="btn" style="float:right;" styleId="submit2"></html:submit>
</div> 
<div class="form-box">
			<html:javascript formName="editVendorMasterForm" />
			<html:hidden property="id" styleId="vendorId"></html:hidden>
				<div id="tabs">
					<ul class="resp-tabs-list">
						<li><a href="#tabs-1" style="color: #fff;" tabindex="1">Contact Information</a></li>
						<li><a href="#tabs-2" style="color: #fff;" tabindex="22">Company Information</a></li>
						<li><a href="#tabs-3" style="color: #fff;" tabindex="85">Services Description</a></li>
						<li><a href="#tabs-4" style="color: #fff;" tabindex="103">Diverse Classifications</a></li>
						<li><a href="#tabs-5" style="color: #fff;" tabindex="126">Quality/Other Certifications</a></li>
					</ul>
					<div class="resp-tabs-container">
						<div id="tabs-1">
							<%@include file="contactinformation.jsp"%>
						</div> 
						<div id="tabs-2">
							<%@include file="/WEB-INF/pages/editvendorregister.jsp"%>
						</div>
						<div id="tabs-3">
							<%@include file="/WEB-INF/pages/editnaicscodes.jsp"%>
						</div>
						<div id="tabs-4">
							<%@include file="/WEB-INF/pages/editdiversecertificates.jsp"%>
						</div>
						<div id="tabs-5">
							<%@include file="/WEB-INF/pages/editqualityothercertificates.jsp"%>
						</div>
					</div>
				</div>
			<div id="auth" style="padding-top: 1%;">
				<input type="checkbox" id="authentication" tabindex="112" >
				&nbsp;<label for="authentication"><b>Under 15 U.S C. 645(d), any person who misrepresents its size status shall 
							(1) be punished by a fine, imprisonment, or both; (2) be subject to administrative remedies; and 
							(3) be ineligible for participation in programs conducted under the authority of the Small Business Act.<br/>
							By choosing to submit this form, you certify that the information you have provided above is true and accurate</b></label>
			</div>
			<div class="btn-wrapper">
				<html:submit value="Update" styleClass="btn" tabindex="113"
					styleId="submit"></html:submit>
				<html:submit value="Submit" styleClass="btn" tabindex="114"
					styleId="submit1" disabled="true" style="background: none repeat scroll 0 0 #848484;"></html:submit>
				<html:link action="/logout.do" tabindex="115" styleClass="btn">Log off</html:link>
			</div>
		<script type="text/javascript">
					$(function($) {
						$("#phone").mask("(999) 999-9999?");
						  $("#contactPhone").mask("(999) 999-9999?");
						  $("#contactMobile").mask("(999) 999-9999?");
						  $("#phone2").mask("(999) 999-9999?");
						  $("#preparerPhone").mask("(999) 999-9999?");
						  $("#bpContactPhone").mask("(999) 999-9999?");
						  $("#ownerPhone1").mask("(999) 999-9999?");
						  $("#ownerPhone2").mask("(999) 999-9999?");
						  $("#ownerPhone3").mask("(999) 999-9999?");
						  
						  $("#fax").mask("(999) 999-9999?");
						  $("#fax2").mask("(999) 999-9999?");
						  $("#contactFax").mask("(999) 999-9999?");
						  $("#preparerFax").mask("(999) 999-9999?");
						  $("#mobile").mask("(999) 999-9999?");
						  $("#mobile2").mask("(999) 999-9999?");
						  $("#ownerMobile1").mask("(999) 999-9999?");
						  $("#ownerMobile2").mask("(999) 999-9999?");
						  $("#ownerMobile3").mask("(999) 999-9999?");
						  $("#preparerMobile").mask("(999) 999-9999?");
					});
				    jQuery.validator.addMethod("alpha", function(value, element) { 
				          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
				    },"No Special Characters Allowed.");
				   	jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
				          return this.optional(element) || /^[0-9]+$/.test(value);
				    },"Please enter only numbers.");
					jQuery.validator.addMethod("phoneNumber", function(value, element) { 
				        return this.optional(element) || /^[0-9-()]+$/.test(value);
				  	},"Please enter valid phone number."); 
					jQuery.validator.addMethod("allowhyphens", function(value, element) { 
				          return this.optional(element) || /^[0-9-]+$/.test(value);
				    },"Please enter valid numbers.");
				    jQuery.validator.addMethod("password",function(value, element) {
						return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
					},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
				    jQuery('#submit1').click(function(){
						  $(this).data('clicked', true);
					});
				    jQuery('#submit2').click(function(){
						  $(this).data('clicked', true);
					});
				    jQuery('#submit').click(function(){
						  $(this).data('clicked', true);
					});
						$(function() {
							$("#vendorForm").validate({
								rules : {
									vendorName : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									/* annualTurnover : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}}, */
									yearOfEstablishment : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},range : [ 1900, 3000 ]},
									dunsNumber : {allowhyphens : true},
									taxId : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},allowhyphens : true},
									businessType:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									companytype : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									numberOfEmployees : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},onlyNumbers : true},
									/* sales2: {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									sales3: {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}}, */
									address1 : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},maxlength : 120},
									city : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},alpha : true,maxlength : 60},
									state : {required : function(element) {if($('#submit1').data('clicked')) {if($("#country option:selected").text()=='United States'){return true;}else{return false;}} else {return false;}},alpha : true,maxlength : 60},
									province : {required:function(element) {if($('#submit1').data('clicked')) { if($("#country option:selected").text()!='United States' && $('#country').val()!='' && $('#state').val()=='') {return true;}else{return false;}} else {return false;}}, maxlength : 60},
									state2 : {required : function(element) {if($('#submit1').data('clicked')) {if($("#country2 option:selected").text()=='United States') {return true;}else{return false;}}else{return false;}},alpha : true,maxlength : 60},
									province2 : {required : function(element) {if($('#submit1').data('clicked')) {if($("#country2 option:selected").text()!='United States' && $('#country2').val()!='' && $('#state2').val()=='') {return true;}else{return false;}} else {return false;}},maxlength : 60},
									region : {maxlength : 60},ethnicity : {range : [ 1, 1000 ]},
									userSecQnAns : {required : true},
									country : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}, maxlength : 120},
									zipcode : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}, allowhyphens : true},
									phone : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},maxlength : 16},
									zipcode2 : {allowhyphens : true},
									emailId : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},maxlength : 120,email : true},
									ownerName1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}}},
									ownerTitle1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}}},
									ownerEmail1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}},email : true},
									ownerPhone1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}}},
									ownerExt1 : {onlyNumbers : true},ownerExt2 : {onlyNumbers : true},ownerExt3 : {onlyNumbers : true},
									ownerGender1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}}},
									ownerOwnership1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}},range : [ 0, 100 ]},
									ownerEthnicity1 : {required : function(element) {if ($("input:radio[name=companyOwnership]:checked").val() == 2) {if($('#submit1').data('clicked')) {return true;} else {return false;}return false;} else {return false;}}},
									ownerEmail2 : {email : true,},ownerOwnership2 : {range : [ 0, 100 ]},
									ownerEmail3 : {email : true,},ownerOwnership3 : {range : [ 0, 100 ]},/*serviceArea : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									businessArea : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},*/
									ownerName1EmailId : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},maxlength : 120,email : true},
									naicsCode_0 : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									firstName : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									lastName : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									designation : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},rangelength : [ 1, 60 ]},
									contactPhone : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},maxlength : 16},
									contanctEmail : {required : true,email : true,maxlength : 120},
									loginpassword : {required : true,password:true},
									confirmPassword : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},equalTo : "#loginpassword"},
									userSecQn : {required : true},vendorDescription : {	rangelength : [ 0, 2000 ]},companyInformation : {rangelength : [ 0, 2000 ]},
									preparerFirstName : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									preparerLastName : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									preparerTitle : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},rangelength : [ 1, 60 ]},
									preparerPhone : {required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}},maxlength : 16},
									preparerEmail : {required : true,email : true,maxlength : 120},
									minorityPercent : {range : [ 0, 100 ]},womenPercent : {range : [ 0, 100 ]}
								},
								ignore : "ui-tabs-hide",
								submitHandler : function(form) {
									if($('#submit1').data('clicked')) {
										$('#vendorForm').append("<input type='hidden' name='submitType' value='submit' />");
										if (validateNaics() && validateCertificate()) {
											if (validateVendorOtherCertificate()) {
												$("#ajaxloader").css('display','block');
												$("#ajaxloader").show();
												form.submit();
											}else{
												$("#ajaxloader").css('display','none');
											}
										}else{
											$("#ajaxloader").css('display','none');
										}
										return true;
									} else if($('#submit').data('clicked')){
										alert('You have saved only partial information. Submission completes only after entering the entire required fields and submitting the forms.');
										$('#vendorForm').append("<input type='hidden' name='submitType' value='save' />");
										$("#ajaxloader").css('display','block');
										$("#ajaxloader").show();
										form.submit();
										return false;
										
									}else{
										alert('You have saved only partial information. Submission completes only after entering all the required field and submitting the same');
										$('#vendorForm').append("<input type='hidden' name='submitType' value='save' />");
										$('#vendorForm').append("<input type='hidden' name='moreInfo' value='more' />");
										$("#ajaxloader").css('display','block');
										$("#ajaxloader").show();
										form.submit();
										return false;
									}
								},
								invalidHandler : function(form,	validator) {
									$("#ajaxloader").css('display','none');
									alert("Please fill all the required information before Clicking Submit button. The tab in red color contains required fileds. The required fields are blank and it will not contain a word 'Optional'");
									var errors = validator.numberOfInvalids();
									if (errors) {
										var invalidPanels = $(validator.invalidElements())
												.closest(".resp-tab-content",form);
										if (invalidPanels.size() > 0) {
											$.each($.unique(invalidPanels.get()), function() {
												if ( $(window).width() > 650 ) {
														$("a[href='#"+ this.id+ "']").parent()
														.not(".resp-accordion").addClass("error-tab-validation")
														.show("pulsate",{times : 3});
												} else {
													   $("a[href='#"+ this.id+ "']").parent()
														.addClass("error-tab-validation")
															.show("pulsate",{times : 3});
														}});
// 											if ( $.browser.msie ) {
// 												$('input[type="text"]').each(function() {
// 													if ($(this).attr('alt') == 'Optional'
// 														&& $(this).attr('placeholder') == 'Optional') {
// 														this.value = 'Optional';
// 													}});}
										}}$('input').placeholder();
										
										$('#submit1').data('clicked', false);
										$('#submit').data('clicked', false);
										$('#submit2').data('clicked', false);
										$('#authentication').prop('checked',false);
										$("#submit1").css('background','none repeat scroll 0 0 #848484');
										$("#submit1").prop('disabled',true);
								},
								unhighlight : function(element,
										errorClass, validClass) {
									$(element).removeClass(errorClass);
									$(element.form).find("label[for="+ element.id+ "]").removeClass(errorClass);
									var $panel = $(element).closest(".resp-tab-content",element.form);
									if ($panel.size() > 0) {
										if ($panel.find(".error:visible").size() > 0) {
											if ($(window).width() > 650) {
												$("a[href='#"+ $panel[0].id+ "']").parent().removeClass("error-tab-validation");
											} else {
												$("a[href='#"+ $panel[0].id+ "']").parent().removeClass("error-tab-validation");
										}}}}});});
						var QueryString = function () {
						  var query_string = {};
						  var query = window.location.search.substring(1);
						  var vars = query.split("&");
						  for (var i=0;i<vars.length;i++) {
						    var pair = vars[i].split("=");
						    if (typeof query_string[pair[0]] === "undefined") {
						      query_string[pair[0]] = pair[1];
						    } else if (typeof query_string[pair[0]] === "string") {
						      var arr = [ query_string[pair[0]], pair[1] ];
						      query_string[pair[0]] = arr;
						    } else {
						      query_string[pair[0]].push(pair[1]);
						    }} return query_string;
						} ();
						if(QueryString.parameter != "viewAnonymousVendors"){
							$("#hideDivforTier2Vendor").css('display','block');
						}
					</script>
			</div>
	</html:form>
