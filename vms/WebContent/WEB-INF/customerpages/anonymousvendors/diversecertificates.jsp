<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<script>
	$(document).ready(function() {
		
		$('#chkDiverse').attr('checked','checked');
		
		if ($("#chkDiverse").is(':checked')) {
			document.getElementById("diverseClassificationTab").style.display = "block";
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
		}
	});
	var imgPath = "jquery/images/calendar.gif";
	datePickerExp();

	function fn_diverseSup() {
		if (document.getElementById("chkDiverse").checked == true) {
			document.getElementById("diverseClassificationTab").style.display = "block";

		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
		}
	}
</script>

<div class="wrapper-half">
	<div class="row-wrapper form-group row">
		<div class="label-col-wrapper"></div>
		<div class="ctrl-col-wrapper">
			<html:checkbox property="diverseSupplier" name="anonymousVendorForm"
				styleId="chkDiverse" onclick="fn_diverseSup()" tabindex="23">&nbsp;Diverse</html:checkbox>
		</div>
		<span class="error"><html:errors property="firstName"></html:errors>
		</span>
	</div>
</div>
<div class="clear"></div>
<style>
<!--
.main-list-box123 {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
	border-radius: 5px 5px 5px 5px;
	color: #000000 !important;
	float: left;
	height: 25px;
	line-height: 25px;
	/*padding: 1% 2%;*/
	width: 30%;
}
-->
</style>

<div id="diverseClassificationTab" class="panelCenter_1"
	style="display: none;">

	<h3>Diverse / Supplier Certifications</h3>
	<div class="form-box">
		<p style="margin-left: 3px;">Diversity/small Business
			Classifications - Select from list</p>
		<html:select property="diverseCertificateName"
			styleClass="main-list-box123" styleId="diverseCertificateName"
			name="anonymousVendorForm" style="margin-left: 5px;">
			<html:option value="">-- Select --</html:option>
			<logic:present name="certificateTypes">
				<bean:size id="size" name="certificateTypes" />
				<logic:greaterEqual value="0" name="size">
					<logic:iterate id="certificate" name="certificateTypes">
						<bean:define id="id" name="certificate" property="id"></bean:define>
						<html:option value="<%=id.toString()%>">
							<bean:write name="certificate" property="certificateName"></bean:write>
						</html:option>
					</logic:iterate>
				</logic:greaterEqual>
			</logic:present>
		</html:select>
		<table width="100%" class="main-table"
			style="margin-top: 2%; margin-bottom: 2%;">
			<tr>
				<td class="header">Classification type</td>
				<td class="header">Certifying Agency</td>
				<td class="header">Certification Number(#)</td>
				<td class="header">Effective Date</td>
				<td class="header">Expire Date</td>
				<td class="header">Upload Certificate</td>
				<logic:present property="certId" name="anonymousVendorForm">
					<logic:notEqual value="0" property="certId"
						name="anonymousVendorForm">
						<bean:define id="id" property="certId" name="anonymousVendorForm"></bean:define>
						<td class="header">Download Certificate</td>
					</logic:notEqual>
				</logic:present>
			</tr>
			<tr>
				<td><html:select property="divCertType"
						styleClass="main-list-box" styleId="divCertType1"
						name="anonymousVendorForm">
						<html:option value="">-- Select --</html:option>
						<logic:present name="certificateTypes">
							<bean:size id="size" name="certificateTypes" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="certificate1" name="certificateTypes">
									<bean:define id="id" name="certificate1" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="certificate1" property="certificateName"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>
					</html:select></td>
				<td><html:select property="divCertAgen"
						styleClass="main-list-box" styleId="divCertAgen1"
						name="anonymousVendorForm">
						<html:option value="">-- Select --</html:option>
						<logic:present name="certAgencyList">
							<bean:size id="size" name="certAgencyList" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="certAgency" name="certAgencyList">
									<bean:define id="id" name="certAgency" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="certAgency" property="agencyName"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>
					</html:select></td>
				<td><html:text property="certificationNo1"
						name="anonymousVendorForm" styleId="certificationNo1" alt=""
						styleClass="main-text-box" /></td>
				<td><html:text property="effDate1" styleId="effDate1" alt="Please click to select date"
						styleClass="main-text-box" name="anonymousVendorForm"
						style="width: 78%;" /></td>
				<td><html:text styleClass="main-text-box" property="expDate"
						styleId="expDate1" alt="Please click to select date" name="anonymousVendorForm"
						style="width: 76%;"></html:text></td>
				<td><html:file property="certFile" styleId="certfile1"
						name="anonymousVendorForm"></html:file></td>
				<logic:present property="certId" name="anonymousVendorForm">
					<logic:notEqual value="0" property="certId"
						name="anonymousVendorForm">
						<bean:define id="id" property="certId" name="anonymousVendorForm"></bean:define>
						<td><html:link href="download_anonymous_certificate.jsp"
								paramId="id" paramName="id" styleId="downloadFile1"
								styleClass="downloadFile">Download</html:link></td>
					</logic:notEqual>
				</logic:present>
			</tr>
		</table>

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Ethnicity</div>
				<div class="ctrl-col-wrapper">
					<html:select property="ethnicity" styleId="ethnicity"
						styleClass="main-list-box" style="height: 30px;">
						<html:option value="">-----Select-----</html:option>
						<bean:size id="size" name="ethnicities" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="ethni" name="ethnicities">
								<bean:define id="name" name="ethni" property="id"></bean:define>
								<html:option value="<%=name.toString()%>">
									<bean:write name="ethni" property="ethnicity" />
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>

				</div>
				<span class="error"> <html:errors property="ethnicity"></html:errors>
				</span>
			</div>
		</div>

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">% of Minority Ownership</div>
				<div class="ctrl-col-wrapper">
					<html:text property="minorityPercent" size="3" alt=""
						styleClass="main-text-box" styleId="minorityPercent"></html:text>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">% of Women Ownership</div>
				<div class="ctrl-col-wrapper">
					<html:text property="womenPercent" size="3" alt=""
						styleClass="main-text-box" styleId="womenPercent"></html:text>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Diversity Classification Notes</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="classificationNotes" cols="30" rows="5"
						styleClass="main-text-area"></html:textarea>
				</div>
			</div>
		</div>
	</div>
</div>

