<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%@page import="com.fg.vms.customer.model.NaicsMaster"%>

<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<!-- <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" /> -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<script>
	function pickNaics(value) {
		press = value;
		pickNaicsCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.8;
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#dialog").dialog({
				autoOpen : false,
				width : dWidth,
				height : dHeight,
				modal : true,
				resizable : true,
				autoResize : true,
				close : function(event, ui) {
					//close event goes here
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});

		$("#dialog").dialog("open");
	}
	function add() {
		if (press == 0) {
			$('#naicsCode_0').val(naicsCode);
			$('#naicsDesc_0').val(des);
		} else if (press == 1) {
			$('#naicsCode_1').val(naicsCode);
			$('#naicsDesc_1').val(des);
		} else if (press == 2) {
			$('#naicsCode_2').val(naicsCode);
			$('#naicsDesc_2').val(des);
		}
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#dialog").dialog("close");
	}
	function reset() {
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#dialog").dialog("close");
	}
	$(document).ready(function() {

		$("#search").keyup(function() {
	        var text = $("#search").val();
	        var postdata = $("#addtree").jqGrid('getGridParam','postData');
	        $.extend(postdata,{filters:'',searchField: 'name', searchOper: 'bw', searchString: text});
	        $("#addtree").jqGrid('setGridParam', { search: text.length>0, postData: postdata });
	        $("#addtree").trigger("reloadGrid",[{page:1}]);
	    });
	});// document.ready
</script>


<html:hidden property="naicslastrowcount" value="3"
	styleId="naicslastrowcount"></html:hidden>
<html:hidden property="lastrowcount" value="2" styleId="lastrowcount" />

<div class="clear"></div>

<div class="panelCenter_1">
	<h3>NAICS Information</h3>


	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">NAICS Code:</div>
				<div class="ctrl-col-wrapper">
					<html:text property="naicsCode_1" styleId="naicsCode_1"
						styleClass="text-box" alt="" name="anonymousVendorForm"
						readonly="true" tabindex="20" />
					<img src="images/magnifier.gif" onClick="pickNaics(1);">
				</div>
				<span class="error"><html:errors property="naicsCode"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">NAICS Desc</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="naicsDesc" styleId="naicsDesc_1"
						styleClass="main-text-area" name="anonymousVendorForm"
						readonly="true" tabindex="21"></html:textarea>
				</div>
			</div>

			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Capabilities</div>
				<div class="ctrl-col-wrapper">
					<html:textarea property="capabilities" name="anonymousVendorForm"
						tabindex="22" styleClass="main-text-area"></html:textarea>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog" title="NAICS Code" style="display: none;">
		<div class="page-title">
		<input type="search" placeholder="Search" alt="Search"
			style="float: left;width: 97%;" id="search" class="main-text-box">
	</div>
	<div class="clear"></div>
	<table id="addtree"></table>
	<div id="paddtree"></div>
	<div class="wrapper-btn">
		<input type="button" value="Ok" class="btn" onclick="add();">
		<input type="button" value="Cancel" class="btn" onclick="reset();">
	</div>
</div>