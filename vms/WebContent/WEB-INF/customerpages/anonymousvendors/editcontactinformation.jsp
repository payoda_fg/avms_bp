<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<div class="clear"></div>
<div class="panelCenter_1">
	<h3>Business Contact Information</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="firstName" alt="" styleId="firstName"
						name="anonymousVendorForm" onfocus="validateAnonymousVendorCertificate()" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="lastName" alt="Optional" styleId="lastName"
						name="anonymousVendorForm" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>
			</div>
		</div>

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Title</div>
				<div class="ctrl-col-wrapper">
					<html:text property="designation" alt="Optional"
						styleId="designation" name="anonymousVendorForm" />
				</div>
				<span class="error"><html:errors property="designation"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactPhone" alt="" styleId="contactPhone"
						name="anonymousVendorForm" />
				</div>
				<span class="error"><html:errors property="contactPhone"></html:errors></span>
			</div>
		</div>

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactMobile" alt="" styleId="contactMobile"
						name="anonymousVendorForm" />
				</div>
				<span class="error"><html:errors property="contactMobile"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">FAX</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactFax" alt="Optional"
						styleId="contactFax" name="anonymousVendorForm" />
				</div>
				<span class="error"><html:errors property="contactFax"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email</div>
				<div class="ctrl-col-wrapper">
					<html:hidden property="hiddenEmailId" alt=""
						name="anonymousVendorForm" styleId="hiddenEmailId" />
					<html:text property="contanctEmail" alt=""
						name="anonymousVendorForm" styleId="contanctEmail"
						onchange="ajaxEdFn(this,'hiddenEmailId','VE');" />
				</div>
				<span class="error"><html:errors property="contanctEmail"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">User name</div>
				<div class="ctrl-col-wrapper">
					<input type="hidden" id="hiddenLoginId" value="" />
					<html:text property="username" alt="" name="anonymousVendorForm"
						styleId="username" onchange="ajaxEdFn(this,'hiddenLoginId','VU');" />
				</div>
				<span class="error"><html:errors property="username"></html:errors></span>
			</div>
		</div>

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="password" alt=""
						name="anonymousVendorForm" styleId="password"
						styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="password"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Confirm password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="confirmPassword" alt=""
						name="anonymousVendorForm" styleId="confirmPassword"
						styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="confirmPassword"></html:errors></span>
			</div>
		</div>
	</div>
</div>