<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="jquery/js/ui.panel.min.js"></script> 
	<link  href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/rfiinformation.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css"
	rel="stylesheet" />
<script type="text/javascript"
	src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="jquery/js/jquery.ui.dialog.js"></script>
<link href="select2/select2.css" rel="stylesheet"/>
<script src="select2/select2.js"></script>
<style>
	.chosen-select{width: 150px;} 
	.chosen-select-width{width : 90%}
	.chosen-select-deselect:{width : 90%}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".chosen-select").select2();
		$(".chosen-select-width").select2(); 
		$("#businessArea").select2();
		$("#tabs").easyResponsiveTabs();
		$('.panelCenter_1').panel({
			collapsible : false
		});
	});
	function cancle() {
		window.location = "viewSubVendors.do?method=subVendorSearch";
	}

	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement
				: null);
		if (evt != null
				&& node != null
				&& ((evt.keyCode == 13) && ((node.type == "text")
						|| (node.type == "radio") || (node.type == "textarea") || (node.type == "checkbox")))) {
			if ($('#authentication').is(":checked")) {
				return true;
			} else if (node.id == 'search') {
			} else {
				alert("Please certify that the information you have provided is true and accurate. ");
				return false;
			}
		}
	}

	document.onkeypress = stopRKey;
</script>
<style>
fieldset {
	padding: 0;
	border: 0;
	margin-top: 25px;
}
</style>
<div class="page-title"><img src="images/edit-cust.gif"/>&nbsp;&nbsp;&nbsp;Update
			Supplier Information
	<bean:define id="id1" property="id" name="editVendorMasterForm"></bean:define>
	<html:link action="/retrieveselfreg.do?method=retrieveSavedData" paramId="id" 
			paramName="id1" style="float:right;" styleClass="btn">Back</html:link> 
</div> 
<div class="form-box">
		<html:form enctype="multipart/form-data" styleClass="FORM"
			action="/selfregistration?method=saveMoreInfoSelfRegistration" styleId="vendorForm">
			<html:javascript formName="editVendorMasterForm" />
			<html:hidden property="id" styleId="vendorId"></html:hidden>
			<html:hidden property="moreInfo" styleId="moreInfo" value="moreInfo"></html:hidden>
			<html:hidden property="isPartiallySubmitted" styleId="isPartiallySubmitted" value="Yes"></html:hidden>
				<div id="tabs">
					<ul class="resp-tabs-list">
						<li><a href="#tabs-1" style="color: #fff;" tabindex="131">References</a></li>
						<li><a href="#tabs-2" style="color: #fff;" tabindex="224">Business Biography</a></li> 
						<li><a href="#tabs-3" style="color: #fff;" tabindex="275">Documents</a></li>
					</ul>
					<div class="resp-tabs-container">
						<div id="tabs-1">
							<%@include file="/WEB-INF/pages/editVendorReferences.jsp"%>
						</div>
						<div id="tabs-2">
							<%@include file="/WEB-INF/pages/biographySafety.jsp"%>
						</div> 
						<div id="tabs-3">
							<%@include file="/WEB-INF/pages/editVendorDocuments.jsp"%>
						</div> 
					</div>
				</div>
			<p style="color: #009900;"><b>Click Update Button to return to Main form</b></p>
			<div class="btn-wrapper" style="margin-left: 45%;">
				<html:submit value="Update" styleClass="btn" tabindex="277"
					styleId="submit"></html:submit>
				<html:link action="/logout.do" tabindex="278" styleClass="btn">Log off</html:link>
			</div>
		</html:form>
		<script type="text/javascript">
					$(function($) {
						  $("#telephone").mask("(999) 999-9999?");
						  $("#bpContactPhone").mask("(999) 999-9999?");
						  $("#referenceMobile1").mask("(999) 999-9999?");
						  $("#referenceMobile2").mask("(999) 999-9999?");
						  $("#referenceMobile3").mask("(999) 999-9999?");
						  $("#referencePhone1").mask("(999) 999-9999?");
						  $("#referencePhone2").mask("(999) 999-9999?");
						  $("#referencePhone3").mask("(999) 999-9999?");
					});
					jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
				          return this.optional(element) || /^[0-9]+$/.test(value);
				    },"Please enter only numbers.");
					jQuery.validator.addMethod("allowhyphens", function(value, element) { 
				          return this.optional(element) || /^[0-9-]+$/.test(value);
				    },"Please enter valid numbers.");
						$(function() {
							$("#vendorForm").validate({
								rules : {
									bpContactPhoneExt : {
										onlyNumbers : true
									},
									referenceName1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									referenceAddress1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									referencePhone1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									referenceMailId1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}, email : true},
									referenceCity1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									referenceZip1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}, allowhyphens : true},
									referenceState1:{required : function(element) {if($('#submit1').data('clicked')) {if($("#referenceCountry1 option:selected").text()=='United States') {return true;}else{return false;}} else {return false;}}},
									referenceState2:{required : function(element) {if($('#submit1').data('clicked')) {if($("#referenceCountry2 option:selected").text()=='United States') {return true;}else{return false;}} else {return false;}}},
									referenceState3:{required : function(element) {if($('#submit1').data('clicked')) {if($("#referenceCountry3 option:selected").text()=='United States') {return true;}else{return false;}} else {return false;}}},
									referenceCountry1:{required : function(element) {if($('#submit1').data('clicked')) {return true;} else {return false;}}},
									extension1: {onlyNumbers : true},referenceMailId2:{email : true},
									referenceZip2:{allowhyphens : true},	extension2: {onlyNumbers : true},referenceMailId3:{email : true},
									referenceZip3:{allowhyphens : true},	extension3: {onlyNumbers : true}
								},
								ignore : "ui-tabs-hide",
								submitHandler : function(form) {
									if (validateNaics() && validateCertificate()) {
										if (validateVendorOtherCertificate()) {
											$("#ajaxloader").css('display','block');
											$("#ajaxloader").show();
											form.submit();
										}
									}
									return true;
								},
								invalidHandler : function(form,
										validator) {
									$("#ajaxloader").css('display','none');
									var errors = validator.numberOfInvalids();
									if (errors) {
										var invalidPanels = $(validator.invalidElements())
												.closest(".resp-tab-content",form);
										if (invalidPanels.size() > 0) {
											$.each($.unique(invalidPanels.get()), function() {
												if ( $(window).width() > 650 ) {
														$("a[href='#"+ this.id+ "']").parent()
														.not(".resp-accordion").addClass("error-tab-validation")
														.show("pulsate",{times : 3});
												} else {
													   $("a[href='#"+ this.id+ "']").parent()
														.addClass("error-tab-validation")
															.show("pulsate",{times : 3});
														}});
											/* if ( $.browser.msie ) {
												$('input[type="text"]').each(function() {
													if ($(this).attr('alt') == 'Optional'
														&& $(this).attr('placeholder') == 'Optional') {
														this.value = 'Optional';
													}
												});
											} */
										}}$('input, textarea').placeholder();
								},
								unhighlight : function(element,
										errorClass, validClass) {
									$(element).removeClass(errorClass);
									$(element.form).find("label[for="+ element.id+ "]").removeClass(errorClass);
									var $panel = $(element).closest(".resp-tab-content",element.form);
									if ($panel.size() > 0) {
										if ($panel.find(".error:visible").size() > 0) {
											if ($(window).width() > 650) {
												$("a[href='#"+ $panel[0].id+ "']").parent().removeClass("error-tab-validation");
											} else {
												$("a[href='#"+ $panel[0].id+ "']").parent().removeClass("error-tab-validation");
										}}}
								}
							});
						});
					</script>
</div>
