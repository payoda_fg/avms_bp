<%@page import="com.fg.vms.customer.model.VendorDocuments"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments" %>
<%@page import="java.util.*" %>

<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>
<script>
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$('#demoTab').easyResponsiveTabs();
		$("#contactInformation").click(function() {
			validateCertificate();
		});
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});
	    if ($("#same").is(':checked')) {
	    	alert('checked');
	    }
	});
	function resetVendorForm() {
		window.location.reload(true);
	}
	
</script>
<div class="page-title" >
	<img src="images/icon-registration.png">Vendor Registration
	<html:link action="/viewanonymousvendors.do?parameter=manageAnonymousVendors" style="float:right;" styleClass="btn">Back</html:link>
</div>
					<html:form enctype="multipart/form-data" styleId="vendorForm"
							action="/selfregistration?method=selfRegistration" >
						<html:javascript formName="vendorMasterForm" />
						<html:hidden property="id" />
						<html:hidden  property="ownerId1" alt="" />
						<html:hidden  property="ownerId2" alt="" />
						<html:hidden  property="ownerId3" alt="" />
						<div id="demoTab" >
							<ul class="resp-tabs-list">
								<li><a href="#tabs-1" id="contactInformation" style="color: #fff;" tabindex="108">Contact
										Information</a></li>
								<li><a href="#tabs-2" style="color: #fff;" tabindex="1">Company Information</a></li>
								<li><a href="#tabs-3" style="color: #fff;" tabindex="65">Services Description</a></li>
								<li><a href="#tabs-4" style="color: #fff;" tabindex="81">Diverse Classification</a></li>
								<li><a href="#tabs-5" style="color: #fff;" tabindex="122">Quality / Other Certifications</a></li>
								<li><a href="#tabs-6" style="color: #fff;" tabindex="177">Documents</a></li>
								<li><a href="#tabs-7" style="color: #fff;" tabindex="193">References</a></li>
							</ul>

							<div class="resp-tabs-container">
								<div id="tabs-1">
									<%@include file="contactinformation.jsp"%>
								</div>
								<div id="tabs-2">
									<%@include file="/WEB-INF/pages/vendorregister.jsp"%>
								</div>
								<div id="tabs-3">
									<%@include file="/WEB-INF/pages/naicscodes.jsp"%>
								</div>
								<div id="tabs-4">
									<%@include file="/WEB-INF/pages/diversecertificates.jsp"%>
								</div>
								<div id="tabs-5">
									<%@include file="/WEB-INF/pages/qualityothercertificates.jsp"%>
								</div>
								<div id="tabs-6">
									<%@include file="/WEB-INF/pages/vendorDocuments.jsp"%>
								</div>
								<div id="tabs-7">
									<%@include file="/WEB-INF/pages/vendorReferences.jsp"%>
								</div>
							</div>
						</div>
						<div id="auth" style="padding-top: 1%;">
							<input type="checkbox" id="authentication" tabindex="224" >
							&nbsp;<label for="authentication"><b>I CERTIFY THAT THE INFORMATION PROVIDED IN THIS FORM IS COMPLETE AND ACCURATE</b></label>
						</div>
						<div class="btn-wrapper">
							<input type="submit" name="button3" id="save" value="Save"
													class="btn" tabindex="225"/>
							<html:submit value="Submit" styleClass="btn" tabindex="226"
								styleId="submit1" disabled="true" style="background: none repeat scroll 0 0 #848484;"></html:submit>
							<html:reset value="Clear" styleClass="btn" tabindex="227"
								onclick="resetVendorForm();"></html:reset>
						</div>
					</html:form>
					<script type="text/javascript">
					
					$(function($) {
						$("#phone").mask("(999) 999-9999?");
						  $("#contactPhone").mask("(999) 999-9999?");
						  $("#phone2").mask("(999) 999-9999?");
						  $("#preparerPhone").mask("(999) 999-9999?");
						  $("#bpContactPhone").mask("(999) 999-9999?");
						  $("#telephone").mask("(999) 999-9999?");
						  $("#referencePhone1").mask("(999) 999-9999?");
						  $("#referencePhone2").mask("(999) 999-9999?");
						  $("#referencePhone3").mask("(999) 999-9999?");
						  $("#ownerPhone1").mask("(999) 999-9999?");
						  $("#ownerPhone2").mask("(999) 999-9999?");
						  $("#ownerPhone3").mask("(999) 999-9999?");
					});
					
				    jQuery.validator.addMethod("alpha", function(value, element) { 
				          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
				    },"No Special Characters Allowed.");
				    
				    jQuery.validator.addMethod("vendor", function(value, element) { 
				          return this.optional(element) || /^[A-Za-z0-9-. ]+$/.test(value)
				    },"No Special Characters Allowed.");
					
					 jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
				          return this.optional(element) || /^[0-9]+$/.test(value)
				    },"Please enter only numbers.");
					 
					jQuery.validator.addMethod("phoneNumber", function(value, element) { 
				        return this.optional(element) || /^[0-9-()]+$/.test(value);
				  	},"Please enter valid phone number."); 
				    
				    jQuery.validator.addMethod("login", function(value, element) {
						return this.optional(element) || /^[A-Za-z0-9]+$/.test(value)
					}, "No Special Characters Allowed.");
				  	jQuery.validator.addMethod("password",function(value, element) {
						return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
					},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
				    jQuery('#submit1').click(function(){
						  $(this).data('clicked', true);
					});
				    
						$(function() {
							$("#vendorForm").validate({
								rules : {
									vendorName : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									annualTurnover : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									sales2 : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									sales3 : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									yearOfEstablishment : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										range : [ 1900, 3000 ]
									},
									ethnicity : {
										range : [ 1, 1000 ]
									},
									address1 : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 120
									},
									city : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										alpha : true,
										maxlength : 60
									},
									state : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										alpha : true,
										maxlength : 60
									},
									province : {
										maxlength : 60
									},
									region : {
										maxlength : 60
									},
									country : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 120
									},
									zipcode : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 60
									},
									mobile : {
										maxlength : 16
									},
									phone : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 16
									},
									fax : {
										maxlength : 16
									},
									emailId : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 120,
										email : true
									},
									ownerName1EmailId : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 120,
										email : true
									},
									naicsCode_0 : {
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									vendorDescription : {
										rangelength : [ 0, 2000 ]
									},
									companyInformation : {
										rangelength : [ 0, 2000 ]
									},
									firstName : {
										required : true,
										login : true
									},
									lastName : {
										required : true,
										login : true
									},
									designation : {
										required : true,
										alpha : true,
										rangelength : [ 1, 60 ]
									},
									contactPhone : {
										required : true,
										maxlength : 16
									},
									contactFax : {
										maxlength : 16,
										onlyNumbers : true
									},
									contanctEmail : {
										required : true,
										email : true,
										maxlength : 120
									},
									loginId : {
										required : true,
										login : true,
										rangelength : [ 4, 15 ]
									},
									loginpassword : {
										required : true,
										password:true
									},
									confirmPassword : {
										required : true,
										equalTo : "#loginpassword"
									},
									userSecQn : {
										required : true
									},
									userSecQnAns : {
										required : true
									},
									preparerFirstName : {
										required : true,
										login : true
									},
									preparerLastName : {
										required : true,
										login : true
									},
									preparerTitle : {
										required : true,
										alpha : true,
										rangelength : [ 1, 60 ]
									},
									preparerPhone : {
										required : true,
										maxlength : 16
									},
									preparerMobile : {
										maxlength : 16,
										phoneNumber : true
									},
									preparerFax : {
										maxlength : 16,
										onlyNumbers : true
									},
									preparerEmail : {
										required : true,
										email : true,
										maxlength : 120
									},
									
									referenceName1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referenceAddress1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referencePhone1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referenceMailId1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referenceMobile1:{maxlength : 16},
									referenceCity1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referenceZip1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referenceState1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									referenceCountry1:{
										required : function(element) {
											if($('#submit1').data('clicked')) {
												return true;
											} else {
												return false;
											}
										}
									}
								},

								ignore : "ui-tabs-hide",
								submitHandler : function(form) {
									if($('#submit1').data('clicked')) {
										$('#vendorForm').append("<input type='hidden' name='submitType' value='submit' />");
										if (validateNaics() && validateCertificate()) {
											if (validateVendorOtherCertificate()) {
												$("#ajaxloader").css('display','block');
												$("#ajaxloader").show();
												form.submit();
											}
										}
										return true;
									} else {
										alert('You have saved only partial information. Submission completes only after entering all the required field and submitting the same');
										$('#vendorForm').append("<input type='hidden' name='submitType' value='save' />");
										$("#ajaxloader").css('display','block');
										$("#ajaxloader").show();
										form.submit();
										return true;
									}
								},

								invalidHandler : function(form,
										validator) {
									
									$("#ajaxloader").css('display','none');
									alert("Please fill all the required information before Clicking Submit button. The tab in red color contains required fileds. The required fields are blank and it will not contain a word 'Optional'");
									var errors = validator
											.numberOfInvalids();
									if (errors) {
										var invalidPanels = $(validator.invalidElements())
												.closest(".resp-tab-content",form);
										
										if (invalidPanels.size() > 0) {
											$.each($.unique(invalidPanels.get()), function() {
																
												if ( $(window).width() > 650 ) {
														$("a[href='#"+ this.id+ "']").parent()
														.not(".resp-accordion").addClass("error-tab-validation")
														.show("pulsate",{times : 3});
												} else {
													   $("a[href='#"+ this.id+ "']").parent()
														.addClass("error-tab-validation")
															.show("pulsate",{times : 3});
														}
												});
											
											/*  $('input[type="text"]').each(function() {
												if ($(this).attr('alt') == 'Optional') {
													this.placeholder = 'Optional';
												}
											});  */
											if ( $.browser.msie ) {
												//alert('IE');
												$('input[type="text"]').each(function() {
													if ($(this).attr('alt') == 'Optional'
														&& $(this).attr('placeholder') == 'Optional') {
														this.value = 'Optional';
													}
												});
											}
										}
									}
									$('input, textarea').placeholder();
									
									$('#submit1').data('clicked', false);
									$('#authentication').prop('checked',false);
									$("#submit1").css('background','none repeat scroll 0 0 #848484');
									$("#submit1").prop('disabled',true);
								},
								unhighlight : function(element,
										errorClass, validClass) {
									$(element).removeClass(errorClass);
									$(element.form).find("label[for="+ element.id+ "]")
											.removeClass(errorClass);
									var $panel = $(element).closest(".resp-tab-content",element.form);

									if ($panel.size() > 0) {
										
										if ($panel.find(".error:visible").size() > 0) {

											if ($(window).width() > 650) {
												$("a[href='#"+ $panel[0].id+ "']")
														.parent()
														.removeClass("error-tab-validation");
											} else {
												$("a[href='#"+ $panel[0].id+ "']")
														.parent()
														.removeClass("error-tab-validation");
											}
										}
									}
								}
							});
						});
						
						var QueryString = function () {
							  // This function is anonymous, is executed immediately and 
							  // the return value is assigned to QueryString!
						  var query_string = {};
						  var query = window.location.search.substring(1);
						  var vars = query.split("&");
						  for (var i=0;i<vars.length;i++) {
						    var pair = vars[i].split("=");
						    	// If first entry with this name
						    if (typeof query_string[pair[0]] === "undefined") {
						      query_string[pair[0]] = pair[1];
						    	// If second entry with this name
						    } else if (typeof query_string[pair[0]] === "string") {
						      var arr = [ query_string[pair[0]], pair[1] ];
						      query_string[pair[0]] = arr;
						    	// If third or later entry with this name
						    } else {
						      query_string[pair[0]].push(pair[1]);
						    }
						  } 
						    return query_string;
						} ();
						
						if(QueryString.parameter != "viewAnonymousVendors"){
							//alert(QueryString.tire2vendor);
							$("#hideDivforTier2Vendor").css('display','block');
						}
						
					</script>
				
<script type="text/javascript">
	chosenConfig();
	function chosenConfig() {
		var config = {
			'.chosen-select' : {
				width : "90%"
			},
			'.chosen-select-deselect' : {
				allow_single_deselect : true,
				width : "90%"
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "90%"
			}

		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
	}
</script>
