<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<script type="text/javascript">
<!--
	function copyContact() {
		var firstName = $("#preparerFirstName").val();
		var lastName = $('#preparerLastName').val();
		var title = $('#preparerTitle').val();
		var phone = $('#preparerPhone').val();
		var mobile = $('#preparerMobile').val();
		var fax = $('#preparerFax').val();
		if ($("#sameContact").is(':checked')) {
			$("#firstName").val(firstName);
			$('#lastName').val(lastName);
			$('#designation').val(title);
			$('#contactPhone').val(phone);
			$('#contactMobile').val(mobile);
			$('#contactFax').val(fax);
		} else {
			$("#firstName").val('');
			$('#lastName').val('');
			$('#designation').val('');
			$('#contactPhone').val('');
			$('#contactMobile').val('');
			$('#contactFax').val('');
		}
	}
//-->
</script>
<div class="clear"></div>
<div id="successMsg">
	<html:messages id="msg" property="vendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="submitVendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="transactionFailure" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="panelCenter_1">
	<h3>Business Contact Information</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="preparerFirstName" alt=""
						styleId="preparerFirstName" styleClass="text-box" tabindex="2"
						onfocus="validateCertificate()" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="preparerLastName" alt=""
						styleId="preparerLastName" styleClass="text-box" tabindex="3" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Title</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="preparerTitle"
						styleId="preparerTitle" alt="" tabindex="4" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="preparerPhone"
						styleId="preparerPhone" alt="" tabindex="5" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="preparerMobile"
						styleId="preparerMobile" alt="" tabindex="6" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">FAX</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="preparerFax"
						styleId="preparerFax" alt="" tabindex="7" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email ID</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="preparerEmail"
						styleId="preparerEmail" alt="" onchange="ajaxFn(this,'VE');"
						tabindex="8" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<div style="padding-top: 2%;">
	<input type="checkbox" id="sameContact" tabindex="9"
		onchange="copyContact();">&nbsp;<label for="sameContact"
		style="font-size: 100%; line-height: 1.3;">Check here if the
		Business Contact is the same as the Preparer</label>
</div>
<div class="clear"></div>
<div class="panelCenter_1" id="loginDisplay" style="padding-top: 1%;">
	<h3>Preparer's Contact Information (person completing this form)</h3>
	<div class="form-box">
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="firstName" alt="" styleId="firstName"
						styleClass="text-box" tabindex="10"
						onfocus="validateCertificate()" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="lastName" alt="" styleId="lastName"
						styleClass="text-box" tabindex="12" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Title</div>
				<div class="ctrl-col-wrapper">
					<html:text property="designation" alt="" styleId="designation"
						tabindex="13" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="designation"></html:errors></span>
			</div>
				<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email ID</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contanctEmail" alt="" styleId="contanctEmail"
						onchange="ajaxFn(this,'VE');" tabindex="14"
						 readonly="true" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="contanctEmail"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="loginpassword" styleId="loginpassword"
						tabindex="15" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="loginpassword"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Confirm Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="confirmPassword" styleId="confirmPassword"
						tabindex="16" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="confirmPassword"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question</div>
				<div class="ctrl-col-wrapper">
					<html:select property="userSecQn" name="editVendorMasterForm"
						styleClass="chosen-select-width" styleId="userSecQn"
						tabindex="17">
						<html:option value="">--Select--</html:option>
						<bean:size id="size" name="secretQnsList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="secretQn" name="secretQnsList">
								<bean:define id="id" name="secretQn" property="id"></bean:define>
								<html:option value="<%=id.toString()%>">
									<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question Answer</div>
				<div class="ctrl-col-wrapper">
					<html:text styleClass="text-box" property="userSecQnAns"
						styleId="userSecQnAns" alt="" tabindex="18" />
				</div>
				<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactPhone" alt="" styleId="contactPhone"
						styleClass="text-box" tabindex="19" />
				</div>
				<span class="error"><html:errors property="contactPhone"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Mobile</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactMobile" alt=""
						styleId="contactMobile" styleClass="text-box" tabindex="20" />
				</div>
				<span class="error"><html:errors property="contactMobile"></html:errors></span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">FAX</div>
				<div class="ctrl-col-wrapper">
					<html:text property="contactFax" alt=""
						styleId="contactFax" styleClass="text-box" tabindex="21" />
				</div>
				<span class="error"><html:errors property="contactFax"></html:errors></span>
			</div>
		
		</div>
		<div class="wrapper-half">
			</div></div>
<div class="clear"></div>
</div>