<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/animate.css" type="text/css">

<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
<!-- Porto Theme css -->
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />

<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/modals.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<style>
#ajaxloader {
	position: absolute;
	z-index: 99999;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}
</style>
<script>
	function DisplayPrivacyTerms()
	{
		$("#dialog1").css(
		{
			"display" : "block"
		});		
		
		$("#dialog1").dialog(
		{
			width : 800,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
	}
	$(document).ready(function() {
		$('.modal-with-zoom-anim').magnificPopup({
			type: 'inline',
			preloader: false,
			modal: true
		});
		
		
	});

	// jQuery.validator.addMethod("password",function(value, element) {
	// 	return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	// },	"Your password contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

	function show(value) {
		if (value == 1) {
			$('#contentYes').show();
			$('#contentNo').hide();
			$("#contentTwo").hide();
			$("#cetificationAgenciesDescription").hide();
		} else {
			$('#contentNo').show();
			$('#contentYes').hide();
			$("#contentTwo").hide();
		}
	}
	$(document).ready(function() {
		
		
		<logic:equal value="1" name="isDivisionStatus" property="isDivision">
		$('#contentChoose').attr("style", "display:none");
		$('#cetificationAgencies').attr("style", "display:none");
		$("#cetificationAgenciesDescription").hide();
		$("#contentChoose1").hide();
		</logic:equal>

		$("#contanctEmail").val('');
		$("#loginpassword").val('');
		$("#loginForm").validate({
			rules : {
				contanctEmail : {
					required : true,
					email : true
				},
				loginpassword : {
					required : true
				}
			}
		});
	});
	
	var emailobj;
	function sendLoginCredentials() {
		var emailObj = $("#emailId");
		var emailId = $("#emailId").val();
		var divisionObj = $("#divisionId");
		var divisionId = $("#divisionId").val();
// 		var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
// 		var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
// 		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(([a-zA-Z]+\.)+[a-zA-Z]{2,})$/;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (emailId != "" && re.test(emailId) && divisionId != "Select") {
			ajaxFn(emailObj, "VE");
			$
					.ajax({
						url : "selfregistration.do?method=sendLoginCredentials&preparerEmail="
								+ emailId + "&customerDivision=" + divisionId,
						type : "POST",
						async : true,
						beforeSend : function() {
							$("#ajaxloader").css('display', 'block');
							$("#ajaxloader").show(); //show image loading
						},
						success : function(data) {
							$("#ajaxloader").hide();
							alert($.trim(data));
							$("#emailId").val('');
						}
					});
		} else {
			if (divisionId != "Select")
				alert("Please Enter valid Email Address...");
			else
				alert("Please Select Division...");
		}
	}

	function validateEmail(obj) {
		emailobj = obj;
		//ajaxFn(obj, "VE");
	}
	
	function showCheckList()
	{
		var divisionObj = $("#divisionId");
		var divisionId = $("#divisionId").val();
		
		if (divisionId != "Select")
		{
			$.ajax(
			{
				url : "selfregistration.do?method=getIsGlobalDivision&customerDivision=" + divisionId,
				type : "POST",
				async : true,
				dataType : "json",
				success : function(data)
				{				
					if(data.isDivisionGlobal == 0)
					{
						$.ajax(
						{
							url : "selfregistration.do?method=populateCheckList&customerDivision=" + divisionId,
							type : "POST",
							async : true,
							dataType : "json",
							success : function(data)
							{
								$.each(data, function(i, item)
								{
									if (!(undefined == item.checkList))
									{
										$("#contentOne").html(item.checkList);
									}
									if (!(undefined == item.registrationRequest))
									{
										$("#registrationRequest").html(item.registrationRequest);
									}
									if (!(undefined == item.registrationQuestion))
									{
										$("#registrationQuestion").html(item.registrationQuestion);
									}
								});
							}
						});
						
						$("#contentOne").show();
						$("#contentYes").hide();
						$("#cetificationAgencies").hide();
						$("#cetificationAgenciesDescription").hide();
						$("#contentChoose1").show();
						$("#contentOneRequest").show();
			
						$("#loginDetails").show();
						$("#contentTwo").hide();
						$("#backButton").hide();
						$("#radioYes").prop('checked', false);
						$("#radioNo").prop('checked', false);
					}
					else
					{
						$("#contentOne").hide();
						$("#cetificationAgencies").hide();
						$("#cetificationAgenciesDescription").hide();
						$("#contentChoose").hide();
						$("#contentChoose1").hide();
						$("#contentOneRequest").hide();
						$("#contentYes").show();
					}
				}
			});
		}
		else
		{
			$("#contentOne").hide();
			$("#cetificationAgencies").hide();
			$("#cetificationAgenciesDescription").hide();
			$("#contentChoose").hide();
			$("#contentChoose1").hide();
			$("#contentOneRequest").hide();
			$("#backButton").show();
		}
	}
	
	function showCertificatonAgency() {
		$('#contentNo').show();
		$('#contentYes').hide();
		<logic:equal value="1" name="isDivisionStatus" property="isDivision">
		var divisionObj = $("#divisionId");
		var divisionId = $("#divisionId").val();
		$
				.ajax({
					url : "selfregistration.do?method=populateCertificationAgencies&customerDivision="
							+ divisionId,
					type : "POST",
					async : true,
					success : function(data) {
						// 						alert(data);
						$("#cetificationAgencies").show();
						$("#cetificationAgenciesDescription").show();
						$("#contentTwo").html(data);
						$("#contentTwo").show();
						$('#contentYes').hide();
					}

				});
		</logic:equal>
	}
</script>
<section role="main" class="content-body card-margin signupPage" style="padding-top: 60px;">
<div class="row">
<div class="col-lg-12">
<html:form action="retrieveselfreg.do?method=retrieveSavedData&prime=no"
	styleId="loginForm" styleClass="form-horizontal">
	<section class="card" style="border: 1px solid #0088cc;">
		<header class="card-header" style="background: #0088cc;">
			<h2 class="card-title pb-3" style="color:#fff;">Welcome To AVMS Vendor Registration System</h2>
			<p class="pt-1" style="color:#fff;">To begin a NEW registration,please select the appropriate Customer Division (BP ALASKA if your company is based in Alaska and only services Alaska, or BP AMERICA if your company is based in any of the other U.S. territories).</p>
		</header>
		<div class="form-box card-body">
		<logic:equal value="1" name="isDivisionStatus" property="isDivision">			
				<div class="row-wrapper form-group row">
					<!-- 	<p>Your company must be based in the United States of America
						and be certified by one or more of the following entities in order
						to continue with the registration process.</p> -->
					<div class="form-group row">
						<label class="col-sm-3 control-label text-sm-right pl-4 pt-2">
						</label>
					<div class="col-sm-9">
					<html:select property="customerDivision" styleId="divisionId"
						styleClass="chosen-select form-control" style="width:312px"
						onclick="showCheckList()">
						<option value="Select" selected="selected">Select</option>
						<bean:size id="size" name="customerDivisions" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="customerDivision" name="customerDivisions">
								<bean:define id="id" name="customerDivision" property="id"></bean:define>
								<html:option value="${id}">
									<bean:write name="customerDivision" property="divisionname"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
					</div>
					</div>
				</div>
			
		</logic:equal>
		<logic:equal value="0" name="isDivisionStatus" property="isDivision">
			<div class="row-wrapper form-group row">
				<h3>Registration Checklist</h3>
				<p>Your company must be based in the United States of America
					and be certified by one or more of the following entities in order
					to continue with the registration process.</p>
				<ol style="margin-left: 5%;">
					<li>An Affiliate of the National Minority Supplier Development
						Council (NMSDC)</li>
					<li>An Affiliate of the Women's Business Enterprise National
						Council (WBENC)</li>
					<li>An Affiliate of the National Gay and Lesbian Chamber of
						Commerce (NGLCC)</li>
					<li>The U.S. Federal Government (SBA-Small Business
						Administration)</li>
					<li>A U.S. State or Local Government Agency (considered on a
						case-by-case basis)</li>
				</ol>
			</div>
		</logic:equal>
		<logic:equal value="1" name="isDivisionStatus" property="isDivision">
			<div id="contentOneRequest">
				<p id="registrationRequest"></p>
			</div>
			<div id="checkList" style="width: 600px;">
				<ol style="margin-left: 3%; clear: left;" id="contentOne">
				</ol>
			</div>
		</logic:equal>
		<div id="contentChoose">
			<p style="margin-top: 2%;">
				Is your business presently certified by one of the previously
				mentioned organizations:
				 <div class="radio-custom radio-primary"><input type="radio" name="sample" value="1"
					onclick="show('1')">Yes 
					<label for=""></label>
					</div>
					<div class="radio-custom radio-primary">
					<input type="radio"
					name="sample" value="0" onclick="showCertificatonAgency()">No
					<label for=""></label>
					</div>
			
		</div>
		<logic:equal value="1" name="isDivisionStatus" property="isDivision">
			<div id="contentChoose1">
				<br /> <span id="registrationQuestion" style="margin-top: 2%;"></span>
				<span class="radio-custom radio-primary"> <input type="radio" id="radioYes" name="sample"
					value="1" onclick="show('1')">Yes <label for=""></label></span>
					<span class="radio-custom radio-primary"> <input
					type="radio" id="radioNo" name="sample" value="0"
					onclick="showCertificatonAgency()">No<label for=""></label></span>
				</span> <br /> <br />
			</div>
		</logic:equal>
		<div id="contentYes" style="display: none;">
			<div id="ajaxloader" style="display: none;">
				<img src="images/ajax-loader1.gif" alt="Ajax Loading Image"
					class="ajax-loader" />
			</div>
			
			
			<div class="row-wrapper form-group row pl-3">
					<div class="label-col-wrapper">
					<p>To register in our database, you will need the following information:</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;General Company Information including Company Name, Former Company</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Name(s) Used, Internet Address, Federal Tax Id, Dun and Bradstreet Number.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Parent Company Name and Federal Tax Id.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Executive and Primary Contact Information.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Business/Financial Information including Year Established, Gross Annual Sales for the last 3 years, number of employees and Legal Structure (Last 3 years Sales figures).</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Product/Services Information including Specific Commodities.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;NAICS and SIC Codes, Product/Service Description.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Geographical Service Area.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Diversity Information including Ethnicity, Gender, Diversity Category Certification Details, % of Minority and Women Ownership.</p>
					<p><img src="images/arrow-down.png" style="background-color: #009900"/>&nbsp;&nbsp;Documentation: Upload of Supplier Documents including Diversity Certifications and other documents.</p>
					</div>
				</div>
			

			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-left">Please Enter Email Address</label>
					<div class="ctrl-col-wrapper">
						<html:text property="preparerEmail" alt="" styleId="emailId"
							onblur="validateEmail(this);" styleClass="text-box form-control" />
					</div>
				</div>
			

			<div class="row justify-content-end mb-5">
					<div class="col-sm-9 wrapper-btn">
				<input type="button" class="btn btn-primary" id="sendCredential"
					value="Send Credentials" onclick="sendLoginCredentials();">
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<logic:equal value="1" name="isDivisionStatus" property="isDivision">
			<div class="wrapper-half" id="cetificationAgencies">
				<ul style="margin-left: 3%; width: 600px" id="contentTwo"></ul>
				<!-- <p style="margin-top: 2%;">Once certified with one of the
					agencies above, please come back and register with BP Supplier
					Diversity.</p> -->
			</div>
		</logic:equal>

		<logic:equal value="1" name="isDivisionStatus" property="isDivision">
			<div class="wrapper-half" id="cetificationAgenciesDescription">
				<p style="margin-top: 2%;">Once certified with one of the
					agencies above, please come back and register with BP Supplier
					Diversity.</p>
			</div>
		</logic:equal>

		<logic:equal value="0" name="isDivisionStatus" property="isDivision">
			<div id="contentNo" style="display: none;">
				<ul style="margin-left: 3%;">
					<li><a target="_blank" href="http://www.nmsdc.org">National
							Minority Supplier Development Council</a> (NMSDC)*</li>
					<li><a target="_blank" href="http://www.wbenc.org">Women's
							Business Enterprise National Council</a> (WBENC)*</li>
					<li>Local City and State Governments (Considered on a case by
						case basis)</li>
				</ul>
				<p style="margin-top: 2%;">Once certified with one of the
					agencies above, please come back and register with BP Supplier
					Diversity.</p>
			</div>
		</logic:equal>
		<div id="successMsg">
			<html:messages id="emailId" property="emailId" message="true">
				<span><bean:write name="emailId" /></span>
			</html:messages>
			<html:messages id="loginfail" property="loginfail" message="true">
				<span><bean:write name="loginfail" /></span>
			</html:messages>
			<html:messages id="msg" property="resetpassword" message="true">
				<span style="color: green; font-weight: bolder; font-size: medium;">
					<bean:write name="msg" />
				</span>
			</html:messages>
		</div>
		<div id="loginDetails">		
			<div class="form-group row">
					<label class="col-sm-1 text-sm-left label-col-wrapper control-label">Email Id</label>
					<div class="ctrl-col-full-wrapper">						
						<html:text property="contanctEmail" alt="" styleId="contanctEmail"
							styleClass="text-box form-control"  />
					</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-1 text-sm-left label-col-wrapper control-label">Password</label>
					<div class="ctrl-col-full-wrapper">
						<html:password property="loginpassword" alt=""
							styleId="loginpassword" styleClass="text-box form-control" />
					</div>
			</div>
			<footer class="card-footer">
				<div class="row justify-content-end">
					<div class="col-sm-9 pl-0">
						<html:submit styleClass="btn btn-primary" styleId="submit" value="Login"></html:submit>
						<html:link action="/home.do?method=loginPage" styleClass="btn btn-default ml-2">Back</html:link>
					</div>				
				</div>
			</footer>
			
			<div class="col-sm-9 mt-4">
			<div>Are you returning to complete your saved profile? Please login here.</div>			
			<p>Notice! Please read prior to logging in: <a href="#privacy2" class="mb-1 mt-1 mr-1 modal-with-zoom-anim ws-normal">Privacy and Terms</a></p>
			</div>
		</div>
		<%-- <div id="backButton" class="wrapper-btn">
			<html:link action="/home.do?method=loginPage" styleClass="btn">Back</html:link>
		</div> --%>
	</div>
	</section>
</html:form>
</div>
</div>
</section>
<!-- Dialog Box for Privacy and Terms -->
<div id="privacy2" title="Privacy and Terms" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
	<logic:present name="userDetails" property="settings.privacyTerms">
		<bean:define id="privacyTerms" name="userDetails" property="settings.privacyTerms"/>
		<section class="card">
				<header class="card-header">
					<h2 class="card-title">Privacy and Terms</h2>
				</header>
				<div class="card-body">
				<div class="modal-wrapper">
				<div class="modal-text"><p>
					<bean:write name="privacyTerms" />
				</p></div>
				</div>
				<footer class="card-footer">
					<div class="row">
						<div class="col-md-12 text-right">
							<button class="btn btn-info modal-dismiss">Ok</button>
						</div>
					</div>
				</footer>
				</div>
				</section>
	</logic:present>
	<logic:notPresent name="userDetails" property="settings.privacyTerms">
		<p>There is no Privacy and Terms.</p>
	</logic:notPresent>
</div>

