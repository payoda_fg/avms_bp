<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.AnonymousVendor"%>

<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript">
<!--
	/* $(function() {

	// add multiple select / deselect functionality
	$("#selectall").click(function() {
	$('.case').attr('checked', this.checked);
	});

	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$(".case").click(function() {

	if ($(".case").length == $(".case:checked").length) {
	$("#selectall").attr("checked", "checked");
	} else {
	$("#selectall").removeAttr("checked");
	}

	});
	});

	function getSelectedIds() {
	var selectedId = $('input[type=checkbox]:checked').val();
	if (selectedId != null && selectedId != 0) {
	window.location = "approvevendorfromlist.do?parameter=approveVendor&selectedId="
	+ selectedId;
	} else {
	alert('Choose any vendor to approve');
	}
	} */
//-->
</script>
<div id="successMsg">
	<html:messages id="msg" property="successMsg" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div id="failureMsg">
	<html:messages id="msg" property="failureMsg" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="page-title">
	<img id="show" src="images/Vendor-Approval.gif" />&nbsp;&nbsp;Anonymous
	Vendor List
</div>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Approve Vendors" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">

			<div id="form-box">
				<table width="100%" class="main-table">
					<logic:present name="anonymousVendors">
						<bean:size id="size" name="anonymousVendors" />
						<logic:greaterThan value="0" name="size">
							<thead>
								<tr>
									<td class="header">Vendor Name</td>
									<td class="header">Vendor Address</td>
									<td class="header">Vendor EmailId</td>
									<td class="header">Contact Name</td>
									<td class="header">Activate Vendor</td>
								</tr>
							</thead>
							<logic:iterate name="anonymousVendors" id="vendorlist">
								<tbody>
									<tr>
										<td><bean:define id="vendorId" name="vendorlist"
												property="id"></bean:define> <html:link
												action="/editanonymousvendor.do?parameter=retrive"
												paramId="id" paramName="vendorId">
												<bean:write name="vendorlist" property="vendorName" />
											</html:link></td>
										<td><bean:write name="vendorlist" property="address1" /></td>
										<td><bean:write name="vendorlist" property="emailId" /></td>
										<td><bean:write name="vendorlist" property="firstName" /></td>
										<td align="center"><html:link
												action="/editanonymousvendor.do?parameter=retrive"
												paramId="id" paramName="vendorId" styleClass="linkToButton"
												style="font-weight: bold;text-decoration: none;">
												<bean:write name="vendorlist" property="vendorName" />
											</html:link></td>
									</tr>
								</tbody>
							</logic:iterate>
						</logic:greaterThan>
						<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
					</logic:present>
				</table>
			</div>
			<%-- <div style="padding: 1% 0 0 45%;">
						<html:submit value="Activate" styleId="submit"
							styleClass="customerbtTxt" onclick="getSelectedIds()"></html:submit>
					</div> --%>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">

	<logic:match value="Approve Vendors" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view vendors</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
</div>
</div>