<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="form-container">
	<div id="form-container-in">
		<p style="text-align: center; margin-top: 12%">
			Thank you for register your profile. Please click <a href="home.do?method=loginPage"
				style="text-decoration: underline; color: #009900">here</a> to
			return to the Login page.
		</p>
	</div>
</div>