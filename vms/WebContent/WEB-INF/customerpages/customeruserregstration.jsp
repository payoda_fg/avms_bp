<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script>
	$(function($) {
		$("#phoneNumber").mask("(999) 999-9999?");
	});
	$(document).ready(function() {
		$(".chosen-select").select2({
			width : "90%"
		});
	});
	jQuery.validator
			.addMethod(
					"password",
					function(value, element) {
						return this.optional(element)
								|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/)
										.test(value);
					},
					"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");

	function validate() {		
		$("#formID").validate({
			rules : {
				userName : {
					required : true,
					minlength : 4
				},
				userPassword : {
					required : true,
					password : true
				},
				confirmPassword : {
					required : true,
					equalTo : "#userPassword"
				},
				userEmailId : {
					required : true,
					email : true
				},
				roleId : {
					required : true
				},
				secretQuestionId : {
					required : true
				},
				secQueAns : {
					required : true
				},
				firstName : {
					required : true
				},
				lastName : {
					required : true
				},
				extension : {
					onlyNumbers : true
				},phoneNumber:{required : true}
			},
			ignore : ":hidden:not(select)"
		});
	}

	function clearfields() {
		window.location = "viewcustomeruserregistration.do";
	}
</script>
<div class="page-title">
	<h3 style="color: #009900;text-align: center;">User Registration</h3>
</div>
<div class="form-box">
	<html:messages id="msg" property="user" message="true">
		<bean:write name="msg" />
	</html:messages>

	<html:form action="/savecustomeruser?method=saveCustomerUser"
		styleId="formID">
		<html:javascript formName="userForm" />
		<html:hidden property="id" name="userForm" styleId="selectedUserId" />

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">First Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="firstName" name="userForm" styleId="firstName"
						styleClass="text-box" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Last Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="lastName" name="userForm" styleId="lastName"
						styleClass="text-box" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Title</div>
				<div class="ctrl-col-wrapper">
					<html:text property="title" name="userForm" styleId="title"
						styleClass="text-box" alt="Optional" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Department</div>
				<div class="ctrl-col-wrapper">
					<html:text property="department" name="userForm"
						styleId="department" styleClass="text-box" alt="Optional" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Division</div>
				<div class="ctrl-col-wrapper">
					<html:text property="division" name="userForm" styleId="division"
						styleClass="text-box" alt="Optional" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">User Name</div>
				<div class="ctrl-col-wrapper">
					<html:text property="userName" name="userForm" alt=""
						styleId="userName" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="userName"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Phone Number</div>
				<div class="ctrl-col-wrapper">
					<html:text property="phoneNumber" name="userForm"
						styleId="phoneNumber" styleClass="text-box" />
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Extension</div>
				<div class="ctrl-col-wrapper">
					<html:text property="extension" name="userForm" style="width:25%;"
						styleId="extension" styleClass="text-box" alt="Optional" />
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email ID</div>
				<div class="ctrl-col-wrapper">
					<html:text property="userEmailId" name="userForm" alt=""
						styleId="userEmailId" onchange="ajaxFn(this,'VE');"
						styleClass="text-box" readonly="true" />
				</div>
				<span class="error"><html:errors property="userEmailId"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">User Role</div>
				<div class="ctrl-col-wrapper">
					<html:hidden property="roleId" name="userForm" />
					<html:select property="stackHolder" name="userForm"
						styleId="roleId" styleClass="chosen-select" disabled="true">
						<html:option value="">--Select--</html:option>
						<bean:size id="size" name="userRoles" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="role" name="userRoles">
								<bean:define id="id" name="role" property="id"></bean:define>
								<html:option value="${id}">
									<bean:write name="role" property="roleName"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
					<span class="error"><html:errors property="roleId"></html:errors>
					</span>
				</div>
			</div>
		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">User Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="userPassword" name="userForm"
						styleId="userPassword" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="userPassword"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Confirm Password</div>
				<div class="ctrl-col-wrapper">
					<html:password property="confirmPassword" name="userForm"
						styleId="confirmPassword" styleClass="text-box" />
				</div>
				<span class="error"><html:errors property="confirmPassword"></html:errors>
				</span>
			</div>
		</div>
		<div class="wrapper-half">			
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question</div>
				<div class="ctrl-col-wrapper">
					<html:select property="secretQuestionId" name="userForm"
						styleId="userForm" styleClass="chosen-select" style="width: 312px;">
						<html:option value="">--Select--</html:option>
						<bean:size id="size" name="secretQuestionsList" />
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="secretQn" name="secretQuestionsList">
								<bean:define id="id" name="secretQn" property="id"></bean:define>
								<html:option value="${id}">
									<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
				<span class="error"><html:errors property="secretQuestionId"></html:errors>
				</span>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Secret Question Answer</div>
				<div class="ctrl-col-wrapper">
					<html:text property="secQueAns" name="userForm"
						styleClass="text-box" />
					<span class="error"><html:errors property="secQueAns"></html:errors>
					</span>
				</div>
			</div>
		</div>		
		<div class="wrapper-btn">
			<html:submit value="Submit" styleClass="btn" styleId="submit" onclick="validate();"></html:submit>
			<html:reset value="Cancel" styleClass="btn" onclick="clearfields();"></html:reset>
		</div>
	</html:form>
</div>

