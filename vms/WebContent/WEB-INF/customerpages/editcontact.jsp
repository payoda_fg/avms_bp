<%@page import="com.fg.vms.admin.model.CustomerContacts"%>
<%@page import="com.fg.vms.admin.model.Customer"%>
<%@page import="com.fg.vms.admin.dto.CustomerInfoDto"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%
	CustomerInfoDto customerInfoDto = (CustomerInfoDto) session
			.getAttribute("customerInfo");
	Customer customer = customerInfoDto.getCustomer();
%>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript">
	function allowed_to_login() {

		if (document.getElementById("loginAllowed").checked == true) {
			document.getElementById("loginId").style.visibility = 'visible';
		} else {
			document.getElementById("loginId").style.visibility = 'hidden';
		}
	}
</script>

<div>
	<header class="card-header pb-5">
		<h2 class="card-title pull-left">Customer Contact Information</h2>
	</header>

	<div class="form-box card-body">
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="firstName" alt=""
						styleId="firstName" tabindex="19" />
				</div>
				<span class="error"><html:errors property="firstName"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="lastName"
						styleId="lastName" alt="Optional" tabindex="20" />
				</div>
				<span class="error"><html:errors property="lastName"></html:errors></span>
			</div>			
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="designation"
						styleId="designation" alt="Optional" tabindex="21" />
				</div>
				<span class="error"><html:errors property="designation"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="contactPhone"
						styleId="contactPhone" alt="" tabindex="22" />
				</div>
				<span class="error"><html:errors property="contactPhone"></html:errors></span>
			</div>		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="contactMobile"
						styleId="contactMobile" alt="" tabindex="23" />
				</div>
				<span class="error"><html:errors property="contactMobile"></html:errors></span>
			</div>

			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">FAX</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text styleClass="text-box form-control" property="contactFax"
						styleId="contactFax" alt="" tabindex="24" />
				</div>
				<span class="error"><html:errors property="contactFax"></html:errors></span>
			</div>
		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:hidden property="hiddenEmailId" alt=""
						styleId="hiddenEmailId" />
					<html:text property="contanctEmail" alt="" styleId="contanctEmail"
						onchange="ajaxEdFn(this,'hiddenEmailId','CE');"
						styleClass="text-box form-control" tabindex="25" />
					<html:hidden property="hiddenLoginId" />
				</div>
				<span class="error"><html:errors property="contanctEmail"></html:errors></span>
			</div>
		
	</div>
	<div class="clear"></div>

</div>


<script type="text/javascript">
	//allowed_to_login();
	$(function() {
		$("#tabs").tabs();
	});
</script>
<script type="text/javascript">
	var config = {
		'.chosen-select' : {
			width : "90%"
		}
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	}
</script>

