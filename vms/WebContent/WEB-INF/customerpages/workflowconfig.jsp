<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="js/custom-form-elements.js"></script>

<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
	<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<link href="css/form.css" type="text/css" rel="stylesheet"></link> -->

<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>

<style>
	/*#documentSize
	{
		width:30%;
	}*/
</style>

<script>
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$('#savesettings').click(function() {
			// Validation for follow workflow
			var oBtnsfollow = document.getElementsByName('followWF');
			var isfollowChecked = false;
			for (i = 0; i < oBtnsfollow.length; i++) {
				if (oBtnsfollow[i].checked) {
					isfollowChecked = true;
					i = oBtnsfollow.length;
				}
			}
			if (!isfollowChecked) {
				alert('Please select a Follow Workflow type');
			}
			return isfollowChecked;
		});

		if ($("input[@name='followWF']:checked").val() == 1) {
			//alert('here');
			$('#table').css('display', 'block');
		} else {
			$('#distributionEmail').prop('checked', false);
			$('#assessmentRequired').prop('checked', false);
			$('#assessmentCompleted').prop('checked', false);
			$('#approvalEmail').prop('checked', false);
			$('#certificateEmail').prop('checked', false);
			$('#certificateEmailSummaryAlert').prop('checked', false);
			$('#profileUpdateAlert').prop('checked', false);
		}

		$('#clear').click(function() {
			$('#dialog').dialog('close');
			return false;
		});
		
		//For International Mode Hide and Show Starts Here
		if ($("input:radio[name=internationalMode]:checked").val() == 0) {
			$('#defaultCountryDiv').css('display', 'block');
		} else {
			$('#defaultCountryDiv').css('display', 'none');
		}
		
		$('input[name=internationalMode]:radio').click(function() {
			if ($(this).val() == 0) {
				$('#defaultCountryDiv').css('display', 'block');
			} else {
				$('#defaultCountryDiv').css('display', 'none');
			}
		});
		//For International Mode Hide and Show Ends Here
		
		//For BP Segment Hide and Show Starts Here
		$('#sendSdfEmailYes').click(function() {
			$('#sdrMail').show();
			
		});
		$('#sendSdfEmailNo').click(function() {
			$('#sdrMail').hide();
			
		});

		$("#workflowForm").validate({
			rules : {
				sdfToMailId : {
					email : true,
					required : true
					
				},
				sdfCCMailId : {
					email : true
		
				}
			}
		});
		
	});
		if ($("input:radio[name=bpSegment]:checked").val() == 1) {
			$('#statusRowDiv').css('display', 'block');
		} else {
			$('#statusRowDiv').css('display', 'none');
		}
		
		$('input[name=bpSegment]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#statusRowDiv').css('display', 'block');
			} else {
				$('#statusRowDiv').css('display', 'none');
			}
		});
		//For BP Segment Hide and Show Ends Here
	});

	function showhideoptions(value) {
		if (value == 1) {
			$('#table').css('display', 'block');
		} else {
			$('#table').css('display', 'none');
		}
	}

	function viewEmailDistributionPage() {
		/*$('#emailDistributionListName').val('');
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#dialog").dialog({
				autoOpen : false,
				height : '400',
				width : '600',
				modal : true,
				resizable : true,
				autoResize : true,
				close : function(event, ui) {
					//alert('closed');
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});

		$("#dialog").dialog("open");*/
		window.location="emaildistribution.do?method=viewEmailDistribution";
	}
	function validateForm() {
		var fields = $("input[name='checkbox']").serializeArray();
		var fields2 = $("input[name='checkbox2']").serializeArray();
		var listname = $('#emailDistributionListName').val();
		if (!listname && listname.length <= 0) {
			alert('Please enter email distribution name');
			// cancel submit
			return false;
		}
		if (fields.length == 0 && fields2.length == 0) {
			alert('Please select atleast one user');
			// cancel submit
			return false;
		}
	}
		
	function validation()
	{		
		var defaultCountry = $('#defaultCountry').val();
		var internationalMode = $("input:radio[name=internationalMode]:checked").val();
		if(defaultCountry == 0 && internationalMode == 0) 
		{
			alert("Please select default country");
			return false;
		}
		
		var status = $('#status').val();
		var bpSegment = $("input:radio[name=bpSegment]:checked").val();		
		if(status == 0 && bpSegment == 1)
		{
			alert("Please select status");
			return false;
		}
		
		$("#workflowForm").submit();
	}
</script>

<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-12 mx-auto">	
		<div id="successMsg">
			<html:messages id="msg" property="successMsg" message="true">
				<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
			</html:messages>
		</div>

		<header class="card-header">
			<h2 class="card-title pull-left">Workflow Configuration</h2>
		</header>
		<section class="workflow-wrapper">
				<logic:iterate id="privilege" name="privileges">
					<logic:match value="Workflow" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="view">
							<html:form action="/workflowconfiglist?method=saveWFconfigList"
								styleId="workflowForm">
								<html:hidden property="workflowConfigId"></html:hidden>
								<!-- Change it to radio button if required. -->
								<html:hidden property="naicsCodeAM"></html:hidden>
								<html:hidden property="naicsCodeAM"></html:hidden>
								<html:javascript formName="workflowConfigurationForm" />							
								<div class="panelCenter_1" >
									<div class="card-body">
										<div class="form-group row">
											<label class="pl-1 control-label text-sm-right">Follow Workflow :</label>
											<div class="col-sm-9">
												<div class="radio-custom radio-primary">
													<html:radio property="followWF" value="1"
													onchange="showhideoptions(this.value);">Yes</html:radio>
													<label for=""></label>
												</div>
											
												<div class="radio-custom radio-primary">
													<html:radio property="followWF" value="0"
														onchange="showhideoptions(this.value);">No</html:radio>
													<label for=""></label>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-9">
												<div class="checkbox-custom chekbox-primary">
													<html:checkbox property="distributionEmail"
														styleId="distributionEmail" styleClass="styled" value="1">Send email to distribution when vendor created</html:checkbox>
														
													<label for="distributionEmail"></label>
													<html:hidden property="distributionEmailPercent" alt="Optional"
														styleId="distributionEmailPercent"></html:hidden>
												</div>
											</div>
										</div>
										<div class="form-group row">
											  <div class="col-sm-9">
												<div class="checkbox-custom chekbox-primary">
													<html:checkbox property="assessmentRequired"
														styleId="assessmentRequired" styleClass="styled" value="1">Make Capability Statement available when vendor created</html:checkbox>
													<label for="assessmentRequired"></label>
													<html:hidden property="assessmentRequiredPercent" alt="Optional"
														styleId="assessmentRequiredPercent"></html:hidden>
												</div>												
											</div>
										</div>
									<div class="form-group row">
										<div class="col-sm-9">
											<div class="checkbox-custom chekbox-primary">
												<html:checkbox property="assessmentCompleted"
													styleId="assessmentCompleted" styleClass="styled" value="1">Send email to distribution when Capability Assessment completed</html:checkbox>
												<label for="assessmentCompleted"></label>
												<html:hidden property="assessmentCompletedPercent" alt="Optional"
												styleId="assessmentCompletedPercent"></html:hidden>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-9">
											<div class="checkbox-custom chekbox-primary">
												<html:checkbox property="approvalEmail" styleId="approvalEmail"
													styleClass="styled" value="1">Send email to vendor once approved</html:checkbox>
												<label for="assessmentCompleted"></label>
												<html:hidden property="approvalEmailPercent" alt="Optional"
													styleId="approvalEmailPercent"></html:hidden>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-9">
											<div class="checkbox-custom chekbox-primary">
												<html:checkbox property="certificateEmail"
													styleId="certificateEmail" styleClass="styled" value="1">Send email alert to vendor to remind Certificate Expiration</html:checkbox>
												<label for="assessmentCompleted"></label>
											</div>
										</div>
									</div>
																		
									<div class="form-group row">
										<div class="col-sm-9">
											<div class="checkbox-custom chekbox-primary">
												<html:checkbox property="certificateEmailSummaryAlert"
													styleId="certificateEmailSummaryAlert" styleClass="styled" value="1">Send email to Admin Group about Certificate Expiration Notification Monthly Report</html:checkbox>
												<label for="assessmentCompleted"></label>
											</div>	
										</div>	
									</div>
									
										<div class="form-box card-body">
											<div class="form-group row row-wrapper">
												<label class="control-label text-sm-right label-col-wrapper">Email Distribution List :</label>
													<div class="col-sm-9 ctrl-col-wrapper">
													<html:select property="emailDistributionMasterId"
														styleClass="chosen-select-no-single form-control col-sm-7">
														<html:option value="0">-- Select --</html:option>
														<bean:size id="size" name="distributionModels" />
														<logic:greaterEqual value="0" name="size">
															<logic:iterate id="emailDistributionMaster"
																name="distributionModels">
																<bean:define id="id" name="emailDistributionMaster"
																	property="emailDistributionMasterId"></bean:define>
																<html:option value="<%=id.toString()%>">
																	<bean:write name="emailDistributionMaster"
																		property="emailDistributionListName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:greaterEqual>
													</html:select>
													<input name="" type="button" class="btn btn-primary ml-2" value="Add"
														onclick="viewEmailDistributionPage();" />
												</div>
											</div>
										</div>
										</div> <!--  card body -->		
									</div>
									<div class="clear"></div>
									<div class="panelCenter_1 mt-2">
									
										<header class="card-header">
											<h2 class="card-title pull-left">Workflow for Tier 2 Report </h2>
										</header>
										<div class="form-box card-body">
											<div class="form-group row row-wrapper">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Email Distribution List :</label>
												<div class="col-sm-9 ctrl-col-wrapper">
													<html:select property="tier2ReportEmailDist"
														styleClass="chosen-select-no-single form-control col-sm-7">
														<html:option value="0">-- Select --</html:option>
														<bean:size id="size" name="distributionModels" />
														<logic:greaterEqual value="0" name="size">
															<logic:iterate id="emailDistributionMaster"
																name="distributionModels">
																<bean:define id="id" name="emailDistributionMaster"
																	property="emailDistributionMasterId"></bean:define>
																<html:option value="<%=id.toString()%>">
																	<bean:write name="emailDistributionMaster"
																		property="emailDistributionListName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:greaterEqual>
													</html:select>
													<input name="" type="button" class="btn btn-primary ml-2" value="Add"
														onclick="viewEmailDistributionPage();"/>
												</div>
											</div>
										</div>
									</div>
									<div class="clear"></div>
									<div class="panelCenter_1 mt-2">
										<header class="card-header">
											<h2 class="card-title pull-left">Admin Group </h2>
										</header>
										<div class="form-box card-body">
											<div class="form-group row row-wrapper">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Email Distribution List :</label>
												<div class="col-sm-9 ctrl-col-wrapper">
													<html:select property="adminGroup"
														styleClass="chosen-select-no-single form-control col-sm-7">
														<html:option value="0">-- Select --</html:option>
														<bean:size id="size" name="distributionModels" />
														<logic:greaterEqual value="0" name="size">
															<logic:iterate id="emailDistributionMaster"
																name="distributionModels">
																<bean:define id="id" name="emailDistributionMaster"
																	property="emailDistributionMasterId"></bean:define>
																<html:option value="<%=id.toString()%>">
																	<bean:write name="emailDistributionMaster"
																		property="emailDistributionListName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:greaterEqual>
													</html:select>
													<input name="" type="button" class="btn btn-primary ml-2" value="Add"
														onclick="viewEmailDistributionPage();" />
												</div>
											</div>
										</div>
									</div>
									<div class="clear"></div>
									<div class="panelCenter_1 mt-2">
										<header class="card-header">
											<h2 class="card-title pull-left">Prior Days for Notifications</h2>
										</header>
										<div class="form-box card-body">
											<div class="form-group row row-wrapper">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Tier 2 Spend Report Upload Reminder:</label>
													<div class="col-sm-6 ctrl-col-wrapper">
														<html:text property="daysUpload" alt="Optional"
															styleClass="text-box form-control" styleId="daysUpload"></html:text>
													</div>
												</div>
												<div class="form-group row row-wrapper">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Tier 2 Spend Report Due:</label>
													<div class="col-sm-6 ctrl-col-wrapper">
														<html:text property="reportDueDays" alt="Optional"
															styleClass="text-box form-control" styleId="reportDueDays"></html:text>
													</div>
												</div>
												<div class="form-group row row-wrapper">
													<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Tier 2 Spend Report Not Submitted Reminder:</label>
													<div class="col-sm-6 ctrl-col-wrapper">
														<html:text property="reportNotSubmittedReminderDate" alt="Optional"
															styleClass="text-box form-control" styleId="reportNotSubmittedReminderDate"></html:text>
													</div>
												</div>								
												<div class="form-group row row-wrapper">
													<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Certificate Expiration Reminder:</label>
													<div class="col-sm-6 ctrl-col-wrapper">
														<html:text property="daysExpiry" alt="Optional"
															styleClass="text-box form-control" styleId="daysExpiry"></html:text>
													</div>
												</div>
											
												<div class="form-group row row-wrapper">
													<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Maximum Number of Document Upload:</label>
													<div class="col-sm-6 ctrl-col-wrapper">
														<html:text property="documentUpload" alt="Optional"
															styleClass="text-box form-control" styleId="documentUpload"></html:text>
													</div>
												</div>
												<div class="form-group row row-wrapper">
													<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Maximum Size of Documents:</label>
													<div class="col-sm-6 ctrl-col-wrapper">
														<html:text property="documentSize" alt="Optional"
															styleClass="text-box form-control" styleId="documentSize"></html:text>
														<span>&nbsp;(in MB)</span>
													</div>
												</div>
										<div class="clear"></div>	
										</div>
									</div>
									<div class="clear"></div>
									<div class="panelCenter_1 mt-2">
										<header class="card-header">
											<h2 class="card-title pull-left">Country Configuration</h2>
										</header>
										<div class="form-box card-body">
											<div class="form-group row row-wrapper col-sm-6">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">International Mode:</label>
												<div class="col-sm-9 ctrl-col-wrapper">
													<div class="radio-custom radio-primary">
														<html:radio property="internationalMode" value="1" styleId="internationalMode">&nbsp;Yes&nbsp;</html:radio>
														<label for="internationalMode"></label>
													</div>
													<div class="radio-custom radio-primary">
														<html:radio property="internationalMode" value="0" styleId="internationalMode">&nbsp;No&nbsp;</html:radio>
														<label for="internationalMode"></label>
													</div>
												</div>
											</div>								
											<div class="form-group row row-wrapper" style="display: none;" id="defaultCountryDiv">
												<div class="col-sm-1  label-col-wrapper">Default Country:</div>
												<label class="control-label text-sm-right label-col-wrapper">
													<html:select property="defaultCountry" styleId="defaultCountry" styleClass="chosen-select col-sm-12 form-control">
														<html:option value="0">--Select--</html:option>
														<logic:present name="countryList">
															<html:optionsCollection name="countryList" value="id" label="countryname" />
														</logic:present>
													</html:select>
												</label>
											</div>
										</div>
									</div>
									<div class="clear"></div>
									<div class="panelCenter_1 mt-2">
										<header class="card-header">
											<h2 class="card-title pull-left">BP Segment Configuration</h2>
										</header>
										<div class="form-box card-body">
											<div class="form-group row row-wrapper">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">BP Segment:</label>
												<div class="col-sm-9 ctrl-col-wrapper">
													<div class="radio-custom radio-primary">
														<html:radio property="bpSegment" value="1" styleId="bpSegment">&nbsp;Yes&nbsp;</html:radio>
														<label for="bpSegment"></label>
													</div>
													<div class="radio-custom radio-primary">
														<html:radio property="bpSegment" value="0" styleId="bpSegment">&nbsp;No&nbsp;</html:radio>
														<label for="bpSegment"></label>
													</div>
												</div>
											</div>
											<div class="form-group row row-wrapper" id="statusRowDiv">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Status:</label>
												<div class="col-sm-6 ctrl-col-wrapper">
													<html:select property="status" styleClass="chosen-select form-control" styleId="status">
														<html:option value="0">-- Select --</html:option>
														<logic:present name="workFlowStatus">
															<html:optionsCollection name="workFlowStatus" value="id" label="statusname" />
														</logic:present>
													</html:select>
												</div>
											</div>
										</div>
									</div>
									<div class="clear"></div>
									
									<div class="panelCenter_1 mt-2">
									<header class="card-header">
										<h2 class="card-title pull-left">Email Configuration</h2>
									</header>
									<div class="form-box card-body">
										<div class="form-group row row-wrapper">
											<label class="col-sm-3 control-label text-sm-right label-col-wrapper">CC:</label>											
											<div class="col-sm-6 ctrl-col-wrapper">
												<html:select property="ccMailId" styleId="ccMailId" styleClass="chosen-select form-control">
													<html:option value="0">--Select--</html:option>
													<logic:present name="users">
														<html:optionsCollection name="users" value="userEmailId" label="userEmailId" />
													</logic:present>
												</html:select>
											</div>
										</div>
										<div class="form-group row row-wrapper" id="userMailDiv">
											<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Support:</label>
											<div class="col-sm-6 ctrl-col-wrapper">
												<html:select property="supportMailId" styleId="supportMailId" styleClass="chosen-select form-control">
													<html:option value="0">--Select--</html:option>
													<logic:present name="users">
														<html:optionsCollection name="users" value="userEmailId" label="userEmailId" />
													</logic:present>
												</html:select>
											</div>
										</div>
									</div>
								</div>
									<div class="clear"></div>
									<div class="clear"></div>
									<div class="panelCenter_1 mt-2">
										<header class="card-header">
											<h2 class="card-title pull-left">Supplier Diversity Request Form</h2>
										</header>
										<div class="form-box card-body">
											<div class="form-group row row-wrapper">
												<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Display Supplier Diversity Request Form ?</label>
													<div class="col-sm-9 ctrl-col-wrapper">
														<div class="radio-custom radio-primary">
															<html:radio property="isDisplaySDF" value="1" styleId="isDisplaySDF">&nbsp;Yes&nbsp;</html:radio>															
															<label for="isDisplaySDF"></label>
														</div>
														<div class="radio-custom radio-primary">
															<html:radio property="isDisplaySDF" value="0" styleId="isDisplaySDF">&nbsp;No&nbsp;</html:radio>																
															<label for="isDisplaySDF"></label>
														</div>
													</div>
											</div>
										</div>
									</div>
									<div class="clear"></div>
								<logic:iterate id="privilege" name="privileges">
									<logic:match value="Workflow" name="privilege"
										property="objectId.objectName">
										<logic:match value="1" name="privilege" property="add">
											<footer class="mt-4">
												<div class="row justify-content-end pull-right">
													<div class="col-sm-12 wrapper-btn">
												<%-- <html:submit value="Save Configuration" styleClass="btn"
													styleId="savesettings" onclick="return validation();"></html:submit> --%>
													<input type = "button"  value = "Save Configuration" class="btn btn-primary" id = "savesettings" onclick="return validation();"/>
												</div>
											</div>
											</footer>
										</logic:match>
									</logic:match>
								</logic:iterate>
								<logic:iterate id="privilege" name="privileges">
									<logic:match value="Workflow" name="privilege"
										property="objectId.objectName">
										<logic:match value="0" name="privilege" property="add">
											<div style="text-align: center;">
												<h3>You have no rights to add Workflow</h3>
											</div>
										</logic:match>
									</logic:match>
								</logic:iterate>
							</html:form>
					</logic:match>
				</logic:match>
			</logic:iterate>
	
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Workflow" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view Workflow</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</section>
</div>
</div>
<div id="dialog" title="Add Email Distribution" style="display: none;">
	<html:form
		action="/emaildistributionlist?method=saveEmailDistributionListFromWorkflowConfig"
		styleId="emailDistributionForm" onsubmit="return validateForm();">
		<div class="wrapper-half" id="emailForm">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper">Email Distribution List</div>
				<div class="ctrl-col-wrapper">
					<html:text property="emailDistributionListName" alt=""
						styleClass="text-box" styleId="emailDistributionListName" />
				</div>
			</div>
			<div class="wrapper-btn">
				<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
				<html:reset value="Close" styleClass="btn" styleId="clear"></html:reset>
			</div>
		</div>

		<div class="form-box" id="customer" style="margin-top: 2px;">
			<logic:present name="users">
				<h1 align="center">Customer</h1>
				<table class="main-table">
					<thead>
						<tr>
							<td class="header">Select</td>
							<td class="header">User Name</td>
							<td class="header">EmailId</td>
						</tr>
					</thead>
					<logic:iterate name="users" id="userlist">
						<tbody>
							<tr>
								<td><input type="checkbox" name="checkbox"
									value="<bean:write name="userlist" property="id" />" /></td>
								<td><bean:write name="userlist" property="userName" /></td>
								<td><bean:write name="userlist" property="userEmailId" /></td>
							</tr>
						</tbody>
					</logic:iterate>
				</table>
			</logic:present>
		</div>
		<div class="form-box" id="vendor">
			<logic:present name="vendorContacts">
				<h1 align="center">Vendor</h1>
				<table id="rt1" class="rt cf " style="width: 100%;">
					<thead class="cf">
						<tr>
							<th>Select</th>
							<th>User Name</th>
							<th>EmailId</th>
							<th>Vendor</th>
						</tr>
					</thead>
					<tbody>
						<logic:iterate name="vendorContacts" id="vendorlist">
							<tr>
								<td><input type="checkbox" name="checkbox2"
									value="<bean:write name="vendorlist" property="id" />" /></td>
								<td><bean:write name="vendorlist" property="firstName" /></td>
								<td><bean:write name="vendorlist" property="emailId" /></td>
								<td><bean:write name="vendorlist" property="vendorName" /></td>
							</tr>
						</logic:iterate>
					</tbody>
				</table>
			</logic:present>
		</div>
	</html:form>
</div>
</section>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#workflowForm").validate({
			rules : {
				daysUpload : {
					number : true,
					required : true,
					range: [1, 30]
				},
				reportDueDays : {
					number : true,
					required : true,
					range: [1, 30]
				},
				daysExpiry : {
					number : true,
					required : true,
					range: [1, 30]
				},
				/* daysInactive : {
					number : true
				}, */
				documentUpload : {
					number : true,
					required : true
				},
				documentSize : {
					number : true,
					required : true
				},
				reportNotSubmittedReminderDate : {
					number : true,
					required : true,
					range: [1, 30]
				}
			}/*,
			submitHandler : function(form) {
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show();
				form.submit();
			}*/
		});
	});
//-->
</script>
