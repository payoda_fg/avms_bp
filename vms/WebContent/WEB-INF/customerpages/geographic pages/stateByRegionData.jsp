<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<GeographicalStateRegionDto> regionDtos = null;
	if (session.getAttribute("stateRegionDtos") != null) {
		regionDtos = (List<GeographicalStateRegionDto>) session
				.getAttribute("stateRegionDtos");
	}
	GeographicalStateRegionDto regionDto = null;

	if (regionDtos != null) {
		for (int index = 0; index < regionDtos.size(); index++) {
			regionDto = regionDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("region", regionDto.getRegion());
			cellobj.put("state", regionDto.getState());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
