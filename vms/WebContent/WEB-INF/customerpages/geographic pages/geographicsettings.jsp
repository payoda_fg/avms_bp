<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<!-- Multi select -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<style>
<!--
#editmodaddtree {
	height: 150px !important;
}
-->
</style>
<script type="text/javascript">
	$(document).ready(function() {

		$.ajax({
			url : 'geographicalsettings.do?method=listAreaByGroup'
					+ '&random=' + Math.random(),
			type : "POST",
			async : false,
			success : function(data) {
				fillGeographyGrid(data);
			}
		});

	});
	function region() {
		$("#region").val('');
		$("#regionId").val('');
		$("#dialog1").css({
			"display" : "block"
		});
		
		$("input[name='multiselect_states']").each(function() {
			$(this).prop('checked', false);
		});
		
		$("#dialog1").dialog({
			width : 500,
			modal : true,
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
		
		$("#region").focus();
	}

	function state() {
		$("#stateId").val('');
		$("#state").val('');
		
		$("input[name='multiselect_regions']").each(function() {
			$(this).prop('checked', false);
		});
		
		$("#dialog2").css({
			"display" : "block"
		});
		$("#dialog2").dialog({
			width : 500,
			modal : true,
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}

	function saveGeoRegion(isActive) 
	{		
		var region = $('#region').val();		
		var regionId = $('#regionId').val();
		
		if (region != '' && region != 'undefined') 
		{
			$.ajax(
			{
				url : "geographicalsettings.do?method=saveBusinessGroup&region="
						+ region + "&regionId=" + regionId + "&isActive=" + isActive,
				type : "POST",
				async : false,
				success : function(data) 
				{
					alert("Business Group saved successfully");
					$('#region').val('');
					$("#dialog1").dialog("close");
					window.location.reload(true);
				}
			});
		} 
		else 
		{
			alert('Please Enter Business Group Name.');
			return false;
		}
	}

	function saveServiceArea(isActive) 
	{
		var bgroups = [];
		$('#regions > :selected').each(function() {
			bgroups.push($(this).val());
		});

		var area = $('#state').val();		
		var areaId = $('#stateId').val();
		
// 		var re = /^[a-zA-Z0-9\s]+$/;

// 		var re = /^[ A-Za-z0-9_@./#&+-]*$/;

		var re = /[^,;a-zA-Z0-9_-]|[,;]$/g;
		
 	   if (area != '' && area != 'undefined') 
 	   {
			if (re.test(area)) 
			{
				if (bgroups != 0 && area != '' && area != 'undefined' ) 
				{
					$.ajax(
					{
						url : "geographicalsettings.do?method=saveServiceArea&state="
								+ area + "&regions=" + bgroups
								+ "&stateId=" + areaId + "&isActive=" + isActive,
						type : "POST",
						async : false,
						success : function(data) {
							alert("Service Area saved successfully");
							window.location.reload(true);
						}
					});
				} 
				else 
				{
					alert("Please choose Business Group");
					return false;
				}
			} 
			else 
			{
				alert("Enter only valid characters");
			}
		} 
 	   	else 
 	   	{
			alert('Please Enter Service Area.');
			return false;
		}
	}
	
	function editBgroup(id){
		
		var region = "";
		var regionId = "";
		
		if (id != '' && id != 0) {
			$.ajax({
				url : "geographicalsettings.do?method=retrieveBgroup&id="
						+ id,
				type : "POST",
				async : false,
				success : function(data) {
					var parts = data.split("-"),
				    i, l;
					for (i = 0, l = parts.length; i < l; i += 3) {
						if(region == ""){
							region = region+$.trim(parts[i]);
						}
						if(regionId == ""){
							regionId = regionId+$.trim(parts[i+1]);
						}
					}
				}
			});
			$("#region").val(region);
			$("#regionId").val(regionId);
		} 
	}
	
	function deleteBgroup(id){
		
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			// Output when OK is clicked
			if (id != '' && id != 0) {
				$.ajax({
					url : "geographicalsettings.do?method=deleteGroup&id="
							+ id,
					type : "POST",
					async : false,
					success : function(data) {
						if(data.isDeletable=='Yes')
							alert("Business Group Deactivated");
						else
						    alert("Cannot Deleted, It has Reference");
						window.location.reload(true);
					}
				});
			} 
		} else {
			// Output when Cancel is clicked
			// alert("Delete cancelled");
			return false;
		}
	}
	
	function editArea(id){
		
		var area = "";
		var areaId = "";
		var bgroup = "";
		
		if (id != '' && id != 0) {
			$.ajax({
				url : "geographicalsettings.do?method=retrieveArea&id="
						+ id,
				type : "POST",
				async : false,
				success : function(data) {
					var parts = data.split("-"),
				    i, l;
					for (i = 0, l = parts.length; i < l; i += 3) {
						if(area == ""){
							area = area+$.trim(parts[i]);
						}
						if(areaId == "" && $.trim(parts[i+1]) != 0){
							areaId = areaId+$.trim(parts[i+1]);
						}
						if($.trim(parts[i+2]) != 0){
							bgroup = bgroup+$.trim(parts[i+2]);
						}
					}
				}
			});
			$("#state").val(area);
			$("#stateId").val(areaId);
			$("#regions option[value=" + bgroup + "]").attr('selected', 'selected');
			$("#regions").select2();
		} 
	}
	
	function deleteArea(id){
		
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			// Output when OK is clicked
			if (id != '' && id != 0) {
				$.ajax({
					url : "geographicalsettings.do?method=deleteArea&id="
							+ id,
					type : "POST",
					async : false,
					success : function(data) {
						alert("Service Area deleted successfully.");
						window.location.reload(true);
					}
				});
			} 
			window.location.reload(true);
		} else {
			// Output when Cancel is clicked
			// alert("Delete cancelled");
			return false;
		}
	}
	
	function checkServiceAreaUnity()
	{		
		var serviceAreaHidId = $("#stateId").val();
		var regionId = $("#regions").val();
		var stateId = $.trim($('#state').val());
		
		if((regionId !='' && regionId !=0) && (stateId !='' && stateId !=0 && stateId!=null))
		{
		    $.ajax({
				url : "geographicalsettings.do?method=checkServerAreaUnity&id="
						+ regionId+"&name="+stateId+"&serviceAreaHidId="+serviceAreaHidId,
				type : "POST",
				async : false,
				success : function(data) {				
					var msg=data.isDuplicate;
					var isActive=data.isActiveAtDuplicate;
					if(msg=='no')
					{
						saveServiceArea(isActive);
					}
					else
						alert("Service Area Should be Unique in Each Business Group.");
				}
			});
		}
		else
			alert("Please Enter Service Area and Business Group.");
	}
	
	function checkBusinessGroupUnity()
	{
		var regionId = $('#regionId').val();
		var region = $('#region').val();	
// 		var re = /^[a-zA-Z0-9\s]+$/;
		var re = /[^,;a-zA-Z0-9_-]|[,;]$/g;
		
		if(region !='' && region !=0) 
		{
		    if (re.test(region)) 
	   		{	
		 		$.ajax(
 				{
					url : "geographicalsettings.do?method=checkBusinessGroupUnity&name="
				             +region+"&regionId="+regionId,
					type : "POST",
					async : false,
					success : function(data) 
					{
						var msg1=data.isBgDuplicate;
						var isActive=data.isActiveAtDuplicate;
						if(msg1=='no')
						{
							saveGeoRegion(isActive);
						}
						else
							alert("Business Group should be unique.");
					}
				});
	    	}
	      	else
	      	{
	    	  alert("Enter only valid characters");
	      	}
		}
	    else
	    	alert("Please Enter Business Group.");
	}
</script>
<div class="clear"></div>
<section role="main" class="content-body card-margin admin-module">
<div class="row">
	<div class="col-lg-12 mx-auto">	
<section class="card">
	<header class="card-header">
		<div class="mx-auto text-center">
		<input type="button" value="Business Group" onclick="region();" class="btn btn-primary"> 
		<input type="button" value="Service Area"
		onclick="state();" class="btn btn-primary">
		</div>
		
</header>
<div class="clear"></div>
<div id="form-box" class="form-box card-body">
	<div class="responsiveGrid">
	<table id="gridtable" class="table table-bordered table-striped mb-0">
	</table>
	<div id="pager"></div>
	</div>
</div>
<div id="dialog1" title="Add Business Group" style="display: none;">
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">
				<input type="hidden" id="regionId">
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper" style="width: 25%;">Business
							Group :</div>
						<div class="ctrl-col-wrapper">
							<html:text property="region" alt="" name="geographicform"
								styleId="region" styleClass="text-box" />
						</div>
					</div>
				</div>
				<div class="wrapper-btn">
					<input type="button" id="regionBtn" value="Submit" class="btn"
						onclick="checkBusinessGroupUnity();" />
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to add New Business Group</h3>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
				<div class="form-box"
					style="margin-top: 2%; height: 200px; overflow: auto;">
					<logic:present name="group1">
						<table class="main-table">
							<thead>
								<tr>
									<td class="header">Business Group</td>
									<td class="header">Actions</td>
								</tr>
							</thead>
							<tbody>
								<logic:iterate id="grp1" name="group1">
									<tr class="even">
										<td><bean:write name="grp1" property="businessGroupName" /></td>
										<bean:define id="regionId" name="grp1" property="id"></bean:define>
										<td align="center"><logic:match value="1" name="privilege"
												property="modify">
												<a href="#"
													onclick="editBgroup('<%=regionId.toString()%>');">Edit</a>
											</logic:match> | <logic:match value="1" name="privilege" property="delete">
												<a href="#"
													onclick="deleteBgroup('<%=regionId.toString()%>');">Delete</a>
											</logic:match></td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:present>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to view Business Groups</h3>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
</div>
<div id="dialog2" title="Add Service Area" style="display: none;">
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">
				<input type="hidden" id="stateId">
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper" style="width: 25%;">Service
							Area :</div>
						<div class="ctrl-col-wrapper">
							<html:text property="state" alt="" name="geographicform"
								styleId="state" styleClass="text-box" />
						</div>
					</div>
				</div>
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper" style="width: 25%;">Choose
							Business Group :</div>
						<div class="ctrl-col-wrapper">
							<html:select property="regions" styleClass="chosen-select"
								name="geographicform" styleId="regions">
								<option value='0'>-- Select --</option>
								<logic:present name="geographicform" property="groups">
									<logic:iterate id="group" name="geographicform"
										property="groups">
										<option value='<bean:write name="group" property="id"  />'><bean:write
												name="group" property="businessGroupName" /></option>
									</logic:iterate>
								</logic:present>
							</html:select>
						</div>
					</div>
				</div>
				<div class="wrapper-btn">
					<input type="button" id="stateBtn" value="Submit" class="btn"
						onclick="checkServiceAreaUnity();" />
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to add New Service Area</h3>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
				<div class="form-box"
					style="margin-top: 2%; height: 200px; overflow: auto;">
					<logic:present name="areasList1">
						<table class="main-table">
							<thead>
								<tr>
									<td class="header" align="center">Service Area</td>
									<td class="header" align="center">Actions</td>
								</tr>
							</thead>
							<tbody>
								<logic:iterate id="area" name="areasList1">
									<tr class="even">
										<td align="center"><bean:write name="area"
												property="areaName" /></td>
										<bean:define id="areaId" name="area" property="id"></bean:define>
										<td align="center"><logic:match value="1"
												name="privilege" property="modify">
												<a href="#" onclick="editArea('<%=areaId.toString()%>');">Edit</a>
											</logic:match> | <logic:match value="1" name="privilege" property="delete">
												<a href="#" onclick="deleteArea('<%=areaId.toString()%>');">Delete</a>
											</logic:match></td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:present>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Business Area" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no rights to view the Service Areas</h3>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
</div>
</section>
</div>
</div>
</section>

<script type="text/javascript">

	function fillGeographyGrid(myGridData){
		
		var newdata1 = jQuery.parseJSON(myGridData);

		$('#gridtable').jqGrid(
				{

					data : newdata1,
					datatype : 'local',
					colNames : [ 'Business Group', 'Service Area'],
					colModel : [ {
						name : 'region',
						index : 'region',
						/* width : 150, */
						align : 'center'
					}, {
						name : 'state',
						index : 'state',
						/* width : 500, */
						align : 'center'
					} ],
					rowNum : 10,
					rowList : [ 10, 20, 50 ],
					pager : '#pager',
					shrinkToFit : false,
					autowidth : true,
					viewrecords : true,
					emptyrecords : 'No data available..',
					grouping : true,
					groupingView : {
						groupField : [ 'region' ],
						groupColumnShow : [ false ],
						groupText : [ '<b>{0} - {1} Item(s)</b>' ],
						groupCollapse : false,
						groupOrder : [ 'asc' ],
						groupSummary : [ true ],
						showSummaryOnHide : true,
						groupDataSorted : true
					},
					cmTemplate: { title: false },
					height : 'auto'
				}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		},

		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{});

		$(window).bind('resize', function() {
			$("#gridtable").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize'); 
	}	
	
	var config = {
			'.chosen-select' : {
				width : "90%"
			}
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
</script>

