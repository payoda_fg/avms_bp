<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>

<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 20px;">
		<html:form action="/rfiinformation?method=showRFIDashboard">
		<div class="toggleFilter">
				<h2 id="show" title="Click here to hide/show the form"
					style="cursor: pointer;">
					<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;RFI
					Information
				</h2>
				<%@include file="viewrfip.jsp"%>
			</div>
			

		</html:form>


	</div>
</div>