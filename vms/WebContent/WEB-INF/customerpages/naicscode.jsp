<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
 <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" /> 

<script type="text/javascript">

$(document).ready(function() {
	$("#search").keyup(function(e) {
		 if(e.which == 13) {
	        var text = $("#search").val();
	        var postdata = grid.jqGrid('getGridParam','postData');
	        var isNumber = $.isNumeric(text);
			if(isNumber){
				$.extend(postdata, {
					filters : '',
					searchField : 'code',
					searchOper : 'eq',
					searchString : text
				});
			} else {
				$.extend(postdata, {
					filters : '',
					searchField : 'name',
					searchOper : 'bw',
					searchString : text
				});
			}
	        grid.jqGrid('setGridParam', { search: text.length>0, postData: postdata });
	        grid.trigger("reloadGrid",[{page:1}]);
		 }
    });
});// document.ready

	var prevoiusValue = '';
	var rowLevel = '';
	var parentId = 0;
	var operation = '';
	var interval;
	var currentId = '';
	var des;
	var leaf;
	/* disble the parent Id  when add the naics code*/
	function disableSelect() {
		if ($('#parentID').length > 0) {
			$("#parentID").prop('disabled', true);
			if ($.trim(currentId) != '0') {
				$('#parentID').append($('<option>', {
					value : currentId,
					text : prevoiusValue + " - " + des
				}));
				$('#parentID option[value=' + currentId + ']').attr('selected',
						true);
				$("#parentID").prop('disabled', true);
				$("#parentID").css('width', '70%');
				$("#parentID").css('margin-bottom', '5px');
				clearInterval(interval);
			} else {
				$("#parentID").prop('disabled', true);
				$("#parentID").css('width', '70%');
				$("#parentID").css('margin-bottom', '5px');
				clearInterval(interval);
			}
		}
	}
	function disableSelectOnEdit() {
		if ($('#parentID').length > 0) {
			if ($.trim(leaf) == 'false' || $.trim(rowLevel) == '0') {
				$("#parentID").prop('disabled', true);
				$('#parentID option[value=' + parentId + ']')
						.attr('selected', true);
				$("#parentID").prop('disabled', true);
				clearInterval(interval);
			}
			$("#parentID").css('width', '70%');
			$("#parentID").css('margin-bottom', '5px');
		}
	}
	function check_naicsCode(value, colname) {
		var result = null;
		var unique = false;
		if (operation == 'edit') {
			if ($.trim(rowLevel) == '0') {
				if (!($.trim(value).length == '3')) {
					result = [ false,
							"NAICS code should be 3 digits for Level 1." ];
				} else {
					unique = true;
				}
			} else if ($.trim(rowLevel) == '1') {
				if (!($.trim(value).length == '4')) {
					result = [ false,
							"NAICS code should be 4 digits for Level 2." ];
				} else {
					unique = true;
				}
			} else if ($.trim(rowLevel) == '2') {
				if (!($.trim(value).length == '5')) {
					result = [ false,
							"NAICS code should be 5 digits for Level 3." ];
				} else {
					unique = true;
				}
			} else if ($.trim(rowLevel) == '3') {
				if (!($.trim(value).length == '6')) {
					result = [ false,
							"NAICS code should be 6 digits for Level 4." ];
				} else {
					unique = true;
				}
			}
		} else {
			if ($.trim(rowLevel) == '') {
				if (!($.trim(value).length == '3')) {
					result = [ false,
							"NAICS code should be 3 digits for Level 1." ];
				} else {
					unique = true;
				}
			} else if ($.trim(rowLevel) == '0') {
				if (!($.trim(value).length == '4')) {
					result = [ false,
							"NAICS code should be 4 digits for Level 2." ];
				} else {
					unique = true;
				}
			} else if ($.trim(rowLevel) == '1') {
				if (!($.trim(value).length == '5')) {
					result = [ false,
							"NAICS code should be 5 digits for Level 3." ];
				} else {
					unique = true;
				}
			} else if ($.trim(rowLevel) == '2') {
				if (!($.trim(value).length == '6')) {
					result = [ false,
							"NAICS code should be 6 digits for Level 4." ];
				} else {
					unique = true;
				}
			}
		}
		if (unique) {
			if (prevoiusValue != value) {
				$.ajax({
					url : 'naicscode.do?method=validateNaicsCode' + '&random='
							+ Math.random(),
					data : {
						'NAICSCODE' : value

					},
					type : 'POST',
					async : false,
					datatype : 'text',
					success : function(data) {
						if ($.trim(data) == 'success') {
							result = [ true, "" ];
						} else {
							result = [ false, "NAICS code should be unique." ];
						}
					}
				});
			} else {
				result = [ true, "" ];
			}
		}
		return result;
	}

	function check_parent(value, colname) {
		var result = null;
		if (operation == 'edit') {
			if ($.trim(rowLevel) == '0') {
				if ($.trim(parentId) != $.trim(value)) {
					result = [ false, "We can't modify the parent category." ];
				} else {
					result = [ true, "" ];
				}
			} else {
				result = [ true, "" ];
			}
		} else {
			result = [ true, "" ];
		}
		return result;
	}
//
</script>
<style>
/*.main-text-box {
	width: 30%;
}*/
</style>
<div class="clear"></div>
<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-12 mx-auto">	
		<div class="card">
			<header class="card-header">
				<h2 class="card-title pull-left col-lg-5">NAICS Code Standard Configurations</h2>
				<div class="pull-right col-lg-7">
					<input type="text" placeholder="Search" alt="Search" class="main-text-box form-control" id="search" />
				</div>
			</header>
			<div class="clear"></div>
			<div id="form-box" class="form-box card-body naics-table">		
				<table id="addtree" class="filterable table table-bordered table-striped mb-0">
					<tr>
						<td></td>
					</tr>
				</table>
					<div id="paddtree"></div>
			</div>
		</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	var grid = $("#addtree");
	jQuery("#addtree").jqGrid({
		url : 'naicscode.do?method=listNaicsCode' + '&random='
				+ Math.random(),
		treedatatype : "xml",
		treeGridModel : 'adjacency',
		mtype : "POST",
		colNames : [ "id", "NAICS Description", "NAICS Code",
				"Parent" ],
		colModel : [
				{
					name : 'id',
					index : 'id',
					 width : 1, 
					hidden : true,
					key : true,
					editable : true
				},
				{
					name : 'NAICSDESCRIPTION',
					index : 'NAICSDESCRIPTION',
					width : 600, 
					align : "left",
					editable : true,
					editrules : {
						required : true
					}

				},
				{
					name : 'NAICSCODE',
					index : 'NAICSCODE',
					width : 120,
					editable : true,
					editrules : {
						required : true,
						number : true,
						custom : true,
						custom_func : check_naicsCode
					}

				},
				{
					name : 'parentID',
					index : 'parentID',
					 width : 1, 
					hidden : true,
					editable : true,
					edittype : "select",
					editoptions : {
						dataUrl : 'naicscode.do?method=parentNaicsCode'
								+ '&random=' + Math.random()
					},
					editrules : {
						edithidden : true,
						required : true,
						custom : true,
						custom_func : check_parent
					}
				} ],
		height : '350',
		pager : "#paddtree",
		/* expanded : true, */
		loaded : true,
		treeGrid : true,
		shrinkToFit : true,
		autowidth : true,
		ExpandColumn : 'NAICSDESCRIPTION',
		editurl : 'naicscode.do?method=editNaicsCode'
				+ '&random=' + Math.random(),
		caption : "NAICS code",
		hidegrid : false,
		cmTemplate: { title: false },
		onSelectRow : function(id) {
			var rowData = jQuery(this).getRowData(id);

			prevoiusValue = rowData['NAICSCODE'];
			rowLevel = rowData.level;
			leaf = rowData.isLeaf;
			parentId = rowData['parentID'];
			currentId = rowData['id'];
			des = rowData['NAICSDESCRIPTION'];
		},
		gridComplete : function() {
			rowLevel = '';
			parentId = 0;
			prevoiusValue = '';
			des = 'Parent';
			currentId = 0;
			$("#addtree").trigger("reloadGrid");
			$("#addtree").jqGrid('resetSelection');
		},

	});
	jQuery("#addtree").jqGrid('navGrid', "#paddtree", {
		add : true,
		edit : true,
		del : true,
		search : false,
		beforeRefresh : function() {
			rowLevel = '';
			parentId = 0;
			des = 'Parent';
			prevoiusValue = '0';
			currentId = 0;

		}
	}, {
		recreateForm : true,
		height : 200,
		width : 500,
		reloadAfterSubmit : true,
		closeAfterEdit : true,
		beforeShowForm : function(formid) {
			operation = 'edit';
		},
		afterShowForm : function(formid) {
			interval = setInterval(disableSelectOnEdit, 1000);
		}
	}, {
		recreateForm : true,
		height : 200,
		width : 500,
		reloadAfterSubmit : true,
		closeAfterAdd : true,
		beforeShowForm : function(formid) {

			operation = 'add';
		},
		afterShowForm : function(formid) {
			/* disble the parent Id */
			if ($.trim(rowLevel) == '3') {
				$(".ui-icon-closethick").trigger('click');
			}
			interval = setInterval(disableSelect, 1000);
		}
	}, {}, {});

	$(window).bind('resize', function() {
		$("#addtree").setGridWidth($('#form-box').width() - 30, true);
	}).trigger('resize');
</script>


