<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en1.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min1.js"></script>

<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<!-- This is for Search Option of JqGrid -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid1.css" />
<!-- This is for Other Things of JqGrid -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<style>
<!--
#editmodaddtree {
	height: 150px !important;
}
-->
</style>
<script>
	var whichButtonClicked = "";
	function marketSector() {
		$("#sectorCode").val('');
		$("#sectorDescription").val('');
		$("#sectorId").val('');

		$("#sector").css({
			"display" : "block"
		});

		$("#sector").dialog({
			width : 500,
			modal : true,
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}
	function marketSubSector() {
		whichButtonClicked = "marketSubSector";

		// Reset the Hidden Ids.....
		$("#subsectorId").val('');

		// Reset the Text Fields......
		$("#subsectorCode").val('');
		$("#subsectorDescription").val('');

		// Reset the Select Boxe...
		$("#sectorSelect option[value=" + 0 + "]").prop('selected', true);
		$("#sectorSelect").select2();

		// Reset the Table <tBody> Area....
		/*  var  tBody = '<tr>';
		 tBody += '<td align="center" colspan="2"> No Market Subsector Found...</td>';
		 tBody += '</tr>';
		 $("#subSectorTableBody").html(tBody); */

		// Dialog Box of Market SubSector
		$("#subsector").css({
			"display" : "block"
		});

		$("#subsector").dialog({
			width : 500,
			modal : true,
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}
	function commodityGroup() {
		whichButtonClicked = "commodityGroup";

		// Reset the Hidden Ids.....
		$("#commodityId").val('');

		// Reset the Text Fields......
		$("#commodityDescription").val('');
		$("#commodityCode").val('');

		// Reset the Select Boxes....
		$("#sectorSelectInCommodity option[value=" + 0 + "]").prop('selected',
				true);
		$("#sectorSelectInCommodity").select2();

		$("#subSectorSelect option[value=" + 0 + "]").prop('selected', true);
		$("#subSectorSelect").select2();

		// Reset the Table <tBody> Area....
		var tBody = '<tr>';
		tBody += '<td align=center colspan=2> No Commodities Found...</td>';
		tBody += '</tr>';
		$('#commodityTableBody').html(tBody);

		// Dialog Box of Commodity
		$("#commodity").css({
			"display" : "block"
		});

		$("#commodity").dialog({
			width : 500,
			modal : true,
			show : {
				effect : "scale",
				duration : 1000
			},
			hide : {
				effect : "scale",
				duration : 1000
			}
		});
	}

	function editSector(sectorId) {
		if (sectorId != '' && sectorId > 0) {
			$.ajax({
				url : "commoditycategory.do?method=editMarketSector&sectorId="
						+ sectorId,
				type : "POST",
				async : false,
				success : function(data) {
					var obj = $.parseJSON(data);
					$("#sectorCode").val(obj.sectorCode);
					$("#sectorDescription").val(obj.sectorDesc);
					$("#sectorId").val(obj.sectorId);
				}
			});
		}
	}

	var hasReference = "no";
	function deleteSector(sectorId) {
		input_box = confirm("Are you sure you want to delete?");
		if (input_box == true) {
			if (sectorId != '' && sectorId > 0) {
				$
						.ajax({
							url : "commoditycategory.do?method=deleteMarketSector&sectorId="
									+ sectorId,
							type : "POST",
							async : false,
							success : function(data) {
								var obj = $.parseJSON(data);
								if (obj.SectorStatus == "Deleted")
									alert("Market Sector Deleted Successfully");
								else if (obj.SectorStatus == "Reference") {
									alert("Cannot Deleted, Its has Reference");
									hasReference = "yes";
								} else
									alert("Market Sector Not Deleted");

								if (hasReference == "no") {
									$('#sectorCode').val('');
									$("#sectorDescription").val('');
									$("#sectorId").val('');
									$("#sector").dialog("close");
									//loadCommodity Group();
									window.location.reload(true);
								}
							}
						});
			}
		} else
			return false;
	}
	function editSubSector(subsectorId) {
		var sectorId = 0;
		if (subsectorId != '' && subsectorId > 0) {
			$
					.ajax({
						url : "commoditycategory.do?method=editSubSector&commCategoryId="
								+ subsectorId,
						type : "POST",
						async : false,
						success : function(data) {
							var obj = $.parseJSON(data);
							if (obj.sectorId > 0)
								sectorId = obj.sectorId;
							$("#subsectorCode").val(obj.subsectorCode);
							$("#subsectorDescription").val(obj.subsectorDesc);
							$("#subsectorId").val(obj.subsectorId);
							if (sectorId != 0) {
								$(
										"#sectorSelect option[value="
												+ sectorId + "]").prop(
										'selected', true);
								$("#sectorSelect").select2();
							} else {
								$("#sectorSelect option[value=" + 0 + "]")
										.prop('selected', true);
								$("#sectorSelect").select2();
							}
						}
					});
		}
	}
	function deleteSubSector(subsectorId) {
		hasReference = "no";
		input_box = confirm("Are you sure you want to delete?");
		if (input_box == true) {
			if (subsectorId != '' && subsectorId > 0) {
				$
						.ajax({
							url : "commoditycategory.do?method=deleteSubSector&commCategoryId="
									+ subsectorId,
							type : "POST",
							async : false,
							success : function(data) {
								var obj = $.parseJSON(data);
								if (obj.SectorStatus == "Deleted")
									alert("Market Subsector Deleted Successfully");
								else if (obj.SectorStatus == "Reference") {
									alert("Cannot Deleted, Its has Reference");
									hasReference = "yes";
								} else
									alert("Market Subsector Not Deleted");
								if (hasReference == "no") {
									$('#subsectorCode').val('');
									$("#subsectorDescription").val('');
									$("#subsectorId").val('');
									$("#subsector").dialog("close");
									//loadCommodity Group();
									window.location.reload(true);
								}
							}
						});
			}
		} else
			return false;
	}
	function editCommodity(commodityId) {
		var sectorId = 0;
		var subsectorId = 0;
		if (commodityId != '' && commodityId > 0) {
			$.ajax({
				url : "commoditycategory.do?method=editCommodity&commodityId="
						+ commodityId,
				type : "POST",
				async : false,
				success : function(data) {
					var obj = $.parseJSON(data);
					if (obj.sectorId > 0)
						sectorId = obj.sectorId;
					if (obj.subsectorId > 0)
						subsectorId = obj.subsectorId;
					$("#commodityCode").val(obj.commodityCode);
					$("#commodityDescription").val(obj.commodityDesc);
					$("#commodityId").val(obj.commodityId);
					if (sectorId != 0) {
						$(
								"#sectorSelectInCommodity option[value="
										+ sectorId + "]")
								.prop('selected', true);
						$("#sectorSelectInCommodity").select2();
					} else {
						$("#sectorSelectInCommodity option[value=" + 0 + "]")
								.prop('selected', true);
						$("#sectorSelectInCommodity").select2();
					}
					if (subsectorId != 0) {
						$("#subSectorSelect option[value=" + sectorId + "]")
								.prop('selected', true);
						$("#subSectorSelect").select2();
					} else {
						$("#subSectorSelect option[value=" + 0 + "]").prop(
								'selected', true);
						$("#subSectorSelect").select2();
					}
				}
			});
		}
	}
	function deleteCommodity(commodityId) {
		hasReference = "no";
		input_box = confirm("Are you sure you want to delete?");
		if (input_box == true) {
			if (commodityId != '' && commodityId > 0) {
				$
						.ajax({
							url : "commoditycategory.do?method=deleteCommodity&commodityId="
									+ commodityId,
							type : "POST",
							async : false,
							success : function(data) {
								var obj = $.parseJSON(data);
								if (obj.SectorStatus == "Deleted")
									alert("Commodity Group Deleted Successfully");
								else if (obj.SectorStatus == "Reference") {
									alert("Cannot Deleted, Its has Reference");
									hasReference = "yes";
								} else
									alert("Commodity Group Not Deleted");

								if (hasReference = "no") {
									$('#commodityCode').val('');
									$("#commodityDescription").val('');
									$("#commodityId").val('');
									$("#commodity").dialog("close");
									//loadCommodity Group();
									window.location.reload(true);
								}
							}
						});
			}
		} else
			return false;
	}

	var isUnique = "no";
	function saveSector() {
		isUnique = "no";
		var sectorCode = $.trim($('#sectorCode').val());
		var sectorDesc = $.trim($('#sectorDescription').val());
		var sectorId = $.trim($('#sectorId').val());

		if ((sectorCode != '')) {
			if ((sectorDesc != '' && sectorDesc != null)) {
				var re = /^[0-9\s]+$/;
				if (re.test(sectorCode)) {
					$
							.ajax({
								url : "commoditycategory.do?method=saveMarketSector&sectorCode="
										+ sectorCode
										+ "&sectorDescription="
										+ sectorDesc + "&sectorId=" + sectorId,
								type : "POST",
								async : false,
								success : function(data) {
									var obj = $.parseJSON(data);
									if (obj.SectorStatus == "Success")
										alert("Market Sector Saved Successfully");
									else if (obj.SectorStatus == "Failure")
										alert("Market Sector Not Saved ");
									else if (obj.SectorStatus == "Updated")
										alert("Market Sector Updated Successfully");
									else {
										alert("Sector Code Must be Unique");
										isUnique = "yes";
									}
									if (isUnique == "no") {
										$('#sectorCode').val('');
										$("#sectorDescription").val('');
										$("#sectorId").val('');
										$("#sector").dialog("close");
										//loadCommodity Group();
										window.location.reload(true);
									}
								}
							});
				} else {
					alert('Enter Sector Code Numbers Only.');
					return false;
				}
			} else {
				alert("Please Enter Sector Description");
				return false;
			}
		} else {
			alert("Please Enter Sector Code");
			return false;
		}
	}
	function saveSubSector() {
		isUnique = "no";
		var subsectorCode = $.trim($('#subsectorCode').val());
		var subsectorDesc = $.trim($('#subsectorDescription').val());
		var sectorId = $("#sectorSelect").val();
		var commCategoryId = $("#subsectorId").val();

		if (sectorId > 0) {
			if ((subsectorCode != '')) {
				if ((subsectorDesc != '' && subsectorDesc != null)) {
					var re = /^[0-9\s]+$/;
					if (re.test(subsectorCode)) {
						$
								.ajax({
									url : "commoditycategory.do?method=saveSubSector&categoryCode="
											+ subsectorCode
											+ "&categoryDesc="
											+ subsectorDesc
											+ "&sectorId="
											+ sectorId
											+ "&commCategoryId="
											+ commCategoryId,
									type : "POST",
									async : false,
									success : function(data) {
										var obj = $.parseJSON(data);
										if (obj.SectorStatus == "Success")
											alert("Market Subsector Saved Successfully");
										else if (obj.SectorStatus == "Failure")
											alert("Market Subsector Not Saved ");
										else if (obj.SectorStatus == "Updated")
											alert("Market Subsector Updated Successfully");
										else {
											alert("Subsector Code Must be Unique");
											isUnique = "yes";
										}
										if (isUnique == "no") {
											$('#subsectorCode').val('');
											$("#subsectorDescription").val('');
											$("#subsectorId").val('');
											$("#subsector").dialog("close");
											//loadCommodity Group();
											window.location.reload(true);
										}
									}
								});
					} else {
						alert('Enter SubSector Code Numbers Only.');
						return false;
					}
				} else {
					alert("Please Enter SubSector Description");
					return false;
				}
			} else {
				alert("Please Enter SubSector Code");
				return false;
			}
		} else {
			alert("Select atleast one Sector ");
			return false;
		}
	}

	function saveCommodity() {
		isUnique = "no";
		var commodityCode = $.trim($('#commodityCode').val());
		var commodityDesc = $.trim($('#commodityDescription').val());
		var sectorId = $("#sectorSelectInCommodity").val();
		var subSectorId = $("#subSectorSelect").val();
		var commodityId = $('#commodityId').val();

		if (sectorId > 0) {
			if (subSectorId > 0) {
				if ((commodityCode != '')) {
					if ((commodityDesc != '' && commodityDesc != null)) {
						var re = /^[0-9\s]+$/;
						if (re.test(commodityCode)) {
							$
									.ajax({
										url : "commoditycategory.do?method=saveCommodity&commodityCode="
												+ commodityCode
												+ "&commodityDescription="
												+ commodityDesc
												+ "&commCategoryId="
												+ subSectorId
												+ "&sectorId="
												+ sectorId
												+ "&commodityId="
												+ commodityId,
										type : "POST",
										async : false,
										success : function(data) {
											var obj = $.parseJSON(data);
											if (obj.SectorStatus == "Success")
												alert("Commodity Group Saved Successfully");
											else if (obj.SectorStatus == "Failure")
												alert("Commodity Group Not Saved ");
											else if (obj.SectorStatus == "Updated")
												alert("Commodity Group Updated Successfully");
											else {
												alert("Commodity Code Must be Unique");
												isUnique = "yes";
											}
											if (isUnique == "no") {
												$('#commodityCode').val('');
												$("#commodityDescription").val(
														'');
												$("#commodityId").val('');
												$("#commodity").dialog("close");
												//loadCommodity Group();
												window.location.reload(true);
											}
										}
									});
						} else {
							alert('Enter Commodity Code Numbers Only.');
							return false;
						}
					} else {
						alert("Please Enter Commodity Description");
						return false;
					}
				} else {
					alert("Please Enter Commodity Code");
					return false;
				}
			} else {
				alert("Select atleast one SubSector");
			}
		} else {
			alert("Select atleast one Sector");
			return false;
		}
	}

	var flage = 0;

	function changeSector() {
		var sectorId = $("#sectorSelect").val();
		var sectorIdInCommodity = $("#sectorSelectInCommodity").val();
		var sectorChangedIn = "subsector";
		if (whichButtonClicked == "marketSubSector")
			sectorChangedIn = "subsector";
		else
			sectorChangedIn = "commodity";

		if (sectorChangedIn == "subsector") {
			if (sectorId != 0) {
				$
						.ajax({

							url : "commoditycategory.do?method=showMarketSubSectorsBySector&selectedSector="
									+ sectorId
									+ "&sectorChangedIn="
									+ sectorChangedIn,
							type : "POST",
							async : false,
							success : function(data) {
								/* $("#subsectorCode").val('');
								$("#subsectorDescription").val(''); */
								var tBody = '';
								var edit = data.editprivilege;
								var deleted = data.deleteprivilege;
								if (data.subSectorList == '') {
									tBody += '<tr>';
									tBody += '<td align=center colspan=2> No Market Subsector Found...</td>';
									tBody += '</tr>';
								} else {
									for ( var i in data.subSectorList) {
										tBody += '<tr>';
										tBody += '<td align=center>'
												+ data.subSectorList[i].subSectorName
												+ '</td>';
										tBody += '<td align=center>';
										if (edit == 'yes') {
											tBody += '<a href="#" onclick="editSubSector('
													+ data.subSectorList[i].id
													+ ');">Edit</a>';
											if (deleted == 'yes')
												tBody += ' | ';
										}
										if (deleted == 'yes') {
											tBody += '<a href="#" onclick="deleteSubSector('
													+ data.subSectorList[i].id
													+ ');">Delete</a>';
										}
										tBody += '</td>';
										tBody += '</tr>';
									}
								}
								$('#subSectorTableBody').html(tBody);
							}
						});
			} else {
				tBody = '<tr>';
				tBody += '<td align=center colspan=2> No Market Subsector Found...</td>';
				tBody += '</tr>';
				$('#subSectorTableBody').html(tBody);
			}
		} else if (sectorChangedIn = "commodity") {
			var optionBody = '';
			if (sectorIdInCommodity > 0) {
				$
						.ajax({
							url : "commoditycategory.do?method=showMarketSubSectorsBySector&selectedSector="
									+ sectorIdInCommodity
									+ "&sectorChangedIn="
									+ sectorChangedIn,
							type : "POST",
							async : false,
							success : function(data) {
								/* $("#subsectorCode").val('');
								$("#subsectorDescription").val(''); */
								if (data.subSectorList != '') {
									flage = 1;
									$('#subSectorSelect').empty();
									optionBody += "<option value='0'>-- Select --</option>";
									for ( var i in data.subSectorList) {
										/* $("#"+ id).find('option').remove().end().append(data.cityList[i].name);
										$('#'+ id).select2(); */
										optionBody += "<option value="+data.subSectorList[i].id+"> "
												+ data.subSectorList[i].subSectorName
												+ " </option>";
										//	 							$('#city').html(optionBody);
									}
									$('#subSectorSelect').find('option')
											.remove().end().append(optionBody);
									$('#subSectorSelect').select2();
								} else if (flage == 1) {
									$('#subSectorSelect').empty();
									optionBody += "<option value='0'>-- Select --</option>";
									$('#subSectorSelect').find('option')
											.remove().end().append(optionBody);
									$('#subSectorSelect').select2();
									//	 						optionBody +="<option value='0'>- Select -</option>";
									//	 						$('#city').html(optionBody);
								} else {
									$('#subSectorSelect').empty();
									optionBody += "<option value='0'>-- Select --</option>";
									$('#subSectorSelect').find('option')
											.remove().end().append(optionBody);
									$('#subSectorSelect').select2();
								}
							}
						});
			} else {
				$('#subSectorSelect').empty();
				optionBody += "<option value='0'>-- Select --</option>";
				$('#subSectorSelect').find('option').remove().end().append(
						optionBody);
				$('#subSectorSelect').select2();
			}
			var tBody = '<tr>';
			tBody += '<td align=center colspan=2> No Commodities Found...</td>';
			tBody += '</tr>';
			$('#commodityTableBody').html(tBody);
		}
	}
	function changeSubSector() {
		var subsectorId = $("#subSectorSelect").val();
		if (subsectorId != 0) {
			$
					.ajax({
						url : "commoditycategory.do?method=showCommodityBySubSector&selectedSubSector="
								+ subsectorId,
						type : "POST",
						async : false,
						success : function(data) {
							/* $("#subsectorCode").val('');
							$("#subsectorDescription").val(''); */
							var tBody = '';
							var edit = data.editprivilege;
							var deleted = data.deleteprivilege;
							if (data.commodityList == '') {
								tBody += '<tr>';
								tBody += '<td align=center colspan=2> No Commodities Found...</td>';
								tBody += '</tr>';
							} else {
								for ( var i in data.commodityList) {
									tBody += '<tr>';
									tBody += '<td align=center>'
											+ data.commodityList[i].commodityName
											+ '</td>';
									tBody += '<td align=center>';
									if (edit == 'yes') {
										tBody += '<a href="#" onclick="editCommodity('
												+ data.commodityList[i].id
												+ ');">Edit</a>';
										if (deleted == 'yes')
											tBody += ' | ';
									}
									if (deleted == 'yes') {
										tBody += '<a href="#" onclick="deleteCommodity('
												+ data.commodityList[i].id
												+ ');">Delete</a>';
									}
									tBody += '</td>';
									tBody += '</tr>';
								}
							}
							$('#commodityTableBody').html(tBody);
						}
					});
		} else {
			tBody = '<tr>';
			tBody += '<td align=center colspan=2> No Commodities Found...</td>';
			tBody += '</tr>';
			$('#commodityTableBody').html(tBody);
		}
	}
</script>

<!-- Market Sector Section -->
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Commodity Group" name="privilege"
		property="objectId.objectName">
		<div id="sector" title="Add Market Sector" style="display: none;">
			<%-- 	<logic:iterate id="privilege" name="privileges"> --%>
			<%-- 		<logic:match value="Commodity Group" name="privilege" property="objectId.objectName"> --%>
			<logic:match value="1" name="privilege" property="add">
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<input type="hidden" id="sectorId">
						<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Market Sector Code :</label>
						<div class="col-sm-9 ctrl-col-wrapper">
							<html:text property="sectorCode" alt="" name="commodityform"
								styleId="sectorCode" styleClass="text-box form-control" />
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Market Sector Description :</label>
						<div class="col-sm-9 ctrl-col-wrapper">
							<html:text property="sectorDescription" alt=""
								name="commodityform" styleId="sectorDescription"
								styleClass="text-box form-control" />
						</div>
					</div>
				</div>
				<footer class="mt-2 ">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
					<input type="button" id="sectorBtn" value="Submit" class="btn btn-primary"
						onclick="saveSector()" />
				</div>
			</div>
			</footer>
			</logic:match>
			<%-- 		</logic:match> --%>
			<%-- 	</logic:iterate> --%>
			<%-- 	<logic:iterate id="privilege" name="privileges"> --%>
			<%-- 		<logic:match value="Commodity Group" name="privilege" --%>
			<%-- 			property="objectId.objectName"> --%>
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no rights to add New Market Sector</h3>
				</div>
			</logic:match>
			<%-- 		</logic:match> --%>
			<%-- 	</logic:iterate> --%>
			<div class="form-box card-body"
				style="height: 400px; overflow: auto;">
				<%-- 		<logic:iterate id="privilege" name="privileges"> --%>
				<%-- 			<logic:match value="Commodity Group" name="privilege" --%>
				<%-- 				property="objectId.objectName"> --%>
				<logic:match value="1" name="privilege" property="view">
					<table class="main-table main-table table table-bordered table-striped mb-0" id="sectorform">
						<thead>
							<tr>
								<td class="" align="center">Sector Description</td>
								<td class="" align="center">Action</td>
							</tr>
						</thead>
						<tbody id="sectorTableBody">
							<logic:present name="marketsectors">
								<logic:iterate id="sector" name="marketsectors">
									<tr class="even">
										<td align="center"><bean:write name="sector"
												property="sectorDescription" /></td>
										<bean:define id="sectorId" name="sector" property="id"></bean:define>
										<td align="center"><logic:match value="1"
												name="privilege" property="modify">
												<a href="#"
													onclick="editSector('<%=sectorId.toString()%>');">Edit</a>
												<logic:match value="1" name="privilege" property="delete"> |</logic:match>
											</logic:match> <logic:match value="1" name="privilege" property="delete">
												<a href="#"
													onclick="deleteSector('<%=sectorId.toString()%>');">Delete</a>
											</logic:match></td>
									</tr>
								</logic:iterate>
							</logic:present>
							<logic:notPresent name="marketsectors">
								<tr>
									<td align="center" colspan="2">No Market Sector Found...</td>
								</tr>
							</logic:notPresent>
						</tbody>
					</table>
				</logic:match>
				<%-- 			</logic:match> --%>
				<%-- 		</logic:iterate> --%>
				<%-- 		<logic:iterate id="privilege" name="privileges"> --%>
				<%-- 			<logic:match value="Commodity Group" name="privilege" --%>
				<%-- 				property="objectId.objectName"> --%>
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view Market Sectors</h3>
					</div>
				</logic:match>
				<%-- 			</logic:match> --%>
				<%-- 		</logic:iterate> --%>
			</div>
		</div>
	</logic:match>
</logic:iterate>

<!-- Market Sub Sector Section -->
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Commodity Group" name="privilege"
		property="objectId.objectName">
		<div id="subsector" title="Add Market Subsector"
			style="display: none;">
			<%-- 	<logic:iterate id="privilege" name="privileges"> --%>
			<%-- 		<logic:match value="Commodity Group" name="privilege" --%>
			<%-- 			property="objectId.objectName"> --%>
			<logic:match value="1" name="privilege" property="add">
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<input type="hidden" id="subsectorId">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Choose
							Market Sector :</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:select property="marketSectors" styleClass="chosen-select form-control"
								name="commodityform" styleId="sectorSelect"
								onchange="changeSector()">
								<html:option value="0">-- Select --</html:option>
								<logic:present name="marketsectors">
									<logic:iterate id="sectorIterator" name="marketsectors">
										<option
											value='<bean:write name="sectorIterator" property="id"  />'><bean:write
												name="sectorIterator" property="sectorDescription" /></option>
									</logic:iterate>
								</logic:present>
							</html:select>
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Market
							Subsector Code :</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:text property="categoryCode" alt="" name="commodityform"
								styleId="subsectorCode" styleClass="text-box form-control" />
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Market
							Subsector Description :</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:text property="categoryDesc" alt="" name="commodityform"
								styleId="subsectorDescription" styleClass="text-box form-control" />
						</div>
					</div>
				</div>
				<footer class="mt-2">
						<div class="row justify-content-end">
							<div class="col-sm-8 wrapper-btn">
					<input type="button" id="subsectorBtn" value="Submit" class="btn btn-primary"
						onclick="saveSubSector()" />
				</div>
				</div>
				</footer>
			</logic:match>
			<%-- 		</logic:match> --%>
			<%-- 	</logic:iterate> --%>
			<%-- 	<logic:iterate id="privilege" name="privileges"> --%>
			<%-- 		<logic:match value="Commodity Group" name="privilege" --%>
			<%-- 			property="objectId.objectName"> --%>
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no rights to add New Market Subsector</h3>
				</div>
			</logic:match>
			<%-- 		</logic:match> --%>
			<%-- 	</logic:iterate> --%>
			<div class="form-box card-body"
				style="height: 400px; overflow: auto;">
				<%-- 		<logic:iterate id="privilege" name="privileges"> --%>
				<%-- 			<logic:match value="Commodity Group" name="privilege" --%>
				<%-- 				property="objectId.objectName"> --%>
				<logic:match value="1" name="privilege" property="view">
					<table class="main-table main-table table table-bordered table-striped mb-0" id="subsectorform">
						<thead>
							<tr>
								<td class="" align="center">Subsector Description</td>
								<td class="" align="center">Action</td>
							</tr>
						</thead>
						<tbody id="subSectorTableBody">
							<logic:present name="marketsubsectors">
								<logic:iterate id="subsector" name="marketsubsectors">
									<tr class="even">
										<td align="center"><bean:write name="subsector"
												property="categoryDescription" /></td>
										<bean:define id="subsectorId" name="subsector" property="id"></bean:define>
										<td align="center"><a href="#"
											onclick="editSubSector('<%=subsectorId.toString()%>');">Edit</a>
											| <a href="#"
											onclick="deleteSubSector('<%=subsectorId.toString()%>');">Delete</a>
										</td>
									</tr>
								</logic:iterate>
							</logic:present>
							<logic:notPresent name="marketsubsectors">
								<tr>
									<td align="center" colspan="2">No Market Subsector
										Found...</td>
								</tr>
							</logic:notPresent>
						</tbody>
					</table>
				</logic:match>
				<%-- 			</logic:match> --%>
				<%-- 		</logic:iterate> --%>
				<%-- 		<logic:iterate id="privilege" name="privileges"> --%>
				<%-- 			<logic:match value="Commodity Group" name="privilege" --%>
				<%-- 				property="objectId.objectName"> --%>
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view Market Subsectors</h3>
					</div>
				</logic:match>
				<%-- 			</logic:match> --%>
				<%-- 		</logic:iterate> --%>
			</div>
		</div>
	</logic:match>
</logic:iterate>

<!-- Commodity Group Section -->
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Commodity Group" name="privilege"
		property="objectId.objectName">
		<div id="commodity" title="Add Commodity Group" style="display: none">
			<%-- 	<logic:iterate id="privilege" name="privileges"> --%>
			<%-- 		<logic:match value="Commodity Group" name="privilege" --%>
			<%-- 			property="objectId.objectName"> --%>
			<logic:match value="1" name="privilege" property="add">
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<input type="hidden" id="commodityId">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Choose
							Market Sector:</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:select property="marketSectors" styleClass="chosen-select form-control"
								name="commodityform" styleId="sectorSelectInCommodity"
								onchange="changeSector()">
								<html:option value="0">-- Select --</html:option>
								<logic:present name="marketsectors">
									<logic:iterate id="sectorIterator" name="marketsectors">
										<option
											value='<bean:write name="sectorIterator" property="id"  />'><bean:write
												name="sectorIterator" property="sectorDescription" /></option>
									</logic:iterate>
								</logic:present>
							</html:select>
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Choose
							Market Subsector:</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:select property="marketSubSectors"
								styleClass="chosen-select form-control" name="commodityform"
								styleId="subSectorSelect" onchange="changeSubSector()">
								<html:option value="0">-- Select --</html:option>
								<logic:present name="marketsubsectors">
									<logic:iterate id="subsectorIterator" name="marketsubsectors">
										<option
											value='<bean:write name="subsectorIterator" property="id"  />'><bean:write
												name="subsectorIterator" property="categoryDescription" /></option>
									</logic:iterate>
								</logic:present>
							</html:select>
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Commodity
							Code:</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:text property="commodityCode" alt="" name="commodityform"
								styleId="commodityCode" styleClass="text-box form-control" />
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Commodity
							Description:</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:text property="commodityDescription" alt=""
								name="commodityform" styleId="commodityDescription"
								styleClass="text-box form-control" />
						</div>
					</div>
				</div>
				<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-8 wrapper-btn">
							<input type="button" id="commodityBtn" value="Submit" class="btn btn-primary"
						onclick="saveCommodity()" />
					</div>
				</div>
			</footer>
			</logic:match>
			<%-- 		</logic:match> --%>
			<%-- 	</logic:iterate> --%>
			<%-- 	<logic:iterate id="privilege" name="privileges"> --%>
			<%-- 		<logic:match value="Commodity Group" name="privilege" --%>
			<%-- 			property="objectId.objectName"> --%>
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no rights to add New Commodity Group</h3>
				</div>
			</logic:match>
			<%-- 		</logic:match> --%>
			<%-- 	</logic:iterate> --%>
			<div class="form-box card-body"
				style="height:400px; overflow: auto;">
				<%-- 		<logic:iterate id="privilege" name="privileges"> --%>
				<%-- 			<logic:match value="Commodity Group" name="privilege" --%>
				<%-- 				property="objectId.objectName"> --%>
				<logic:match value="1" name="privilege" property="view">
					<table class="main-table" id="commodityform">
						<thead>
							<tr>
								<td class="" align="center">Commodity Description</td>
								<td class="" align="center">Action</td>
							</tr>
						</thead>
						<tbody id="commodityTableBody">
							<logic:present name="commoditygroups">
								<logic:iterate id="commodity" name="commoditygroups">
									<tr class="even">
										<td style="width: 350px"><bean:write name="commodity"
												property="commodityDescription" /></td>
										<bean:define id="commodityId" name="commodity" property="id"></bean:define>
										<td align="center"><a href="#"
											onclick="editCommodity('<%=commodityId.toString()%>');">Edit</a>
											| <a href="#"
											onclick="deleteCommodity('<%=commodityId.toString()%>');">Delete</a>
										</td>
									</tr>
								</logic:iterate>
							</logic:present>
							<logic:notPresent name="commoditygroups">
								<tr>
									<td align="center" colspan="2">No Commodities Found...</td>
								</tr>
							</logic:notPresent>
						</tbody>
					</table>
				</logic:match>
				<%-- 			</logic:match> --%>
				<%-- 		</logic:iterate> --%>
				<%-- 		<logic:iterate id="privilege" name="privileges"> --%>
				<%-- 			<logic:match value="Commodity Group" name="privilege" --%>
				<%-- 				property="objectId.objectName"> --%>
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view Commodity Groups</h3>
					</div>
				</logic:match>
				<%-- 			</logic:match> --%>
				<%-- 		</logic:iterate> --%>
			</div>
		</div>
	</logic:match>
</logic:iterate>

<div class="clear"></div>
<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-12 mx-auto">	
<section class="card">
			<header class="card-header">
				<h2 class="card-title pull-left">Commodity Group </h2>
				<input 
		type="button" value="Commodity Group" class="btn btn-primary pull-right ml-1"
		onclick="commodityGroup()"></input> <input
		type="button" value="Market Subsector" class="btn btn-primary pull-right ml-1"
		 onclick="marketSubSector()"></input> <input
		type="button" value="Market Sector" class="btn btn-primary pull-right"
		onclick="marketSector()"></input>
</header>
<!-- Will be deleted after 1 month -->
<!-- <input type="text" -->
<!-- 		placeholder="Search" alt="Search" style="float: left;  -->
<!-- 		margin-bottom :2%; width: 93%;" class="main-text-box" id="search" -->
<!-- 		onkeyup="filter(this, 'sf', 3)" onblur="if (this.value == '') this.value = '';"> -->
<div class="clear"></div>

<div id="form-box" class="form-box card-body commodity-group-table">
	<table id="addtree" class="filterable table table-bordered table-striped mb-0">
	</table>
	<div id="paddtree"></div>
</div>
</section>
</div>
</div>
</section>
<script type="text/javascript">
	/* $.ajax({
		url : 'commoditycategory.do?method=listCommodity' + '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) {
			$("#addtree").jqGrid('GridUnload');
	// 			commodityGrid(data);
			marketSectorCommodityGrid(data);
		}
	}); */

	$.ajax({
		url : 'commoditycategory.do?method=listCommodityWithSectors'
				+ '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) {
			$("#addtree").jqGrid('GridUnload');
			marketSectorCommodityGrid(data);
		}
	});

	function checkUnity() {
		$
				.ajax({
					url : 'commoditycategory.do?method=getJSONData'
							+ '&random=' + Math.random(),
					type : "POST",
					async : false,
					success : function(data) {
						var cmdty = data.isUnique;
						if (cmdty == 1) {
							alert("Commodity Group Code Should be Unique.");
							window.location = "commoditycategory.do?method=viewCommodity";
						} else {
							window.location = "commoditycategory.do?method=viewCommodity";
						}
					}
				});
	}

	function commodityGrid(myGridData) {
		var newdata = jQuery.parseJSON(myGridData);
		jQuery("#addtree")
				.jqGrid(
						{
							data : newdata,
							datatype : "local",

							colNames : [ "id", "Commodity Group Code",
									"Commodity Group Description",
									"Market Subsector Category" ],
							colModel : [
									{
										name : 'id',
										index : 'id',
										hidden : true,
										editable : true,
									},
									{
										name : 'commodityCode',
										index : 'commodityCode',
										align : "center",
										sorttype : 'integer',
										searchoptions : {
											sopt : [ 'cn', 'eq', 'ne', 'le',
													'lt', 'gt', 'ge' ]
										},
										editable : true,
										editrules : {
											required : true,
											number : true
										}
									},
									{
										name : 'commodityDesc',
										index : 'commodityDesc',
										align : "center",
										sorttype : 'string',
										searchoptions : {
											sopt : [ 'cn', 'eq', 'bw', 'bn',
													'nc', 'ew', 'en' ]
										},
										editable : true,
										editrules : {
											required : true
										}
									},
									{
										name : 'commodityCategoryId',
										index : 'commodityCategoryId',
										align : "center",
										sorttype : 'string',
										searchoptions : {
											sopt : [ 'cn', 'eq', 'bw', 'bn',
													'nc', 'ew', 'en' ]
										},
										//hidden : true,
										editable : true,
										edittype : "select",
										editoptions : {
											dataUrl : 'commoditycategory.do?method=parentCategory'
													+ '&random='
													+ Math.random()
										},
										editrules : {
											edithidden : true,
											required : true
										//custom : true
										},
									} ],
							height : 'auto',
							pager : "#paddtree",
							rowNum : 10,
							rowList : [ 10, 20, 50 ],
							loaded : true,
							loadonce : true,
							ignoreCase : true,
							cmTemplate : {
								title : false
							},
							editurl : 'commoditycategory.do?method=updateCommodity'
									+ '&random=' + Math.random(),
							hidegrid : false
						});

		jQuery("#addtree").jqGrid('filterToolbar', {
			stringResult : true,
			searchOperators : true,
			searchOnEnter : false
		});

		jQuery("#addtree").jqGrid('navGrid', "#paddtree", {
			add : true,
			edit : true,
			del : true,
			search : false
		}, {
			recreateForm : true,
			height : 100,
			width : 500,
			reloadAfterSubmit : true,
			afterSubmit : function() {
				window.location.reload(true);
			},
			closeAfterEdit : true,
			afterSubmit : function(response, postdata) {
				checkUnity();
			}
		}, {
			recreateForm : true,
			height : 200,
			width : 500,
			reloadAfterSubmit : true,
			afterSubmit : function() {
				window.location.reload(true);
			},
			closeAfterAdd : true,
			afterSubmit : function(response, postdata) {
				checkUnity();
			}
		}, {}, {});

		$(window).bind('resize', function() {
			$("#addtree").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize');
	}
	function marketSectorCommodityGrid(myGridData) {
		var newdata = jQuery.parseJSON(myGridData);
		$("#addtree").jqGrid(
				{
					data : newdata,
					datatype : 'local',
					height : 'auto',
					colNames : [ 'sectorId', 'Market Sector', 'subSectorId',
							'Market Subsector', 'commodityId',
							'Commodity Group' ],
					colModel : [ {
						name : 'sectorId',
						index : 'sectorId',
						hidden : true,
						align : 'center'
					}, {
						name : 'sectordescription',
						index : 'sectordescription',
						align : 'center'
					}, {
						name : 'subSectorId',
						index : 'subSectorId',
						hidden : true,
						align : 'center'
					}, {
						name : 'subSectorDescription',
						index : 'subSectorDescription',
						align : 'center'
					}, {
						name : 'commodityId',
						index : 'commodityId',
						hidden : true,
						align : 'center'
					}, {
						name : 'commodityDescription',
						index : 'commodityDescription',
						align : 'center'
					} ],
					rowNum : 5,
					rowList : [ 5, 10, 20, 50 ],
					pager : '#paddtree',
					shrinkToFit : false,
					// 					autowidth : true,
					viewrecords : true,
					caption : "Market Sectors & Market Sub-Sectors",
					emptyrecords : 'No Data Available..',
					grouping : true,
					groupingView : {
						groupField : [ 'sectordescription',
								'subSectorDescription' ],
						groupColumnShow : [ false, false ],
						groupText : [ '<b>{0}</b>' ],
						groupCollapse : false,
						groupOrder : [ 'asc', 'asc' ],
						groupSummary : [ true, true ],
						showSummaryOnHide : true,
						groupDataSorted : true
					},
				}).jqGrid('navGrid', '#paddtree', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		}, {}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{});
		$(window).bind('resize', function() {
			$("#addtree").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize');
	}
</script>

<script type="text/javascript">
	// function filter (term, _id, cellNr){
	// 	var suche = term.value.toLowerCase();
	// 	var table = document.getElementById('addtree');
	// 	var ele;
	// 	for (var r = 0; r < table.rows.length; r++){
	// 		ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g,"");
	// 		if (ele.toLowerCase().indexOf(suche)>=0 )
	// 			table.rows[r].style.display = '';
	// 		else table.rows[r].style.display = 'none';
	// 	}
	// }
</script>
