<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.model.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();
	List<CustomerCommodityCategory> commodityCategories = null;
	if (session.getAttribute("customerCommodityCategories") != null) {
		commodityCategories = (List<CustomerCommodityCategory>) session
				.getAttribute("customerCommodityCategories");
	}
	CustomerCommodityCategory commodity = null;
	if (commodityCategories != null) {
		for (int index = 0; index < commodityCategories.size(); index++) {
			commodity = commodityCategories.get(index);
			cellobj = new JSONObject();
			cellobj.put("id", commodity.getId());
			cellobj.put("categoryCode", commodity.getCategoryCode());
			cellobj.put("categoryDesc",
					commodity.getCategoryDescription());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
