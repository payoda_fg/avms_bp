<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<style>
<!--
#paddtree_center{
	padding: 0 0 0 30%;
}
#editmodaddtree{
	height: 150px !important;
}
-->
</style>
<div class="clear"></div>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Market Subsector
</div>
<div class="clear"></div>
<div id="form-box">

	<table id="addtree">
		<tr>
	        <td />
	    </tr>
	</table>
	<div id="paddtree"></div>
</div>
<script type="text/javascript">
	$.ajax({
		url : 'commoditycategory.do?method=listCommodityCategory' + '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) {
			$("#addtree").jqGrid('GridUnload');
			sss(data);
		}
	});
	
	function checkUnity()
	{		
		$.ajax(
		{
			url : 'commoditycategory.do?method=getJSONData' + '&random=' + Math.random(),
			type : "POST",
			async : false,
			success : function(data) 
			{
				var cc = data.isUnique;				
				if(cc==1)
				{
					alert("Market Subsector Code Should be Unique.");
					window.location = "commoditycategory.do?method=viewCommodityCategory";					
				}
				else 
				{
					window.location = "commoditycategory.do?method=viewCommodityCategory";		
				}
			}
		});
	}
	
	function sss(myGridData) {
		var newdata = jQuery.parseJSON(myGridData);
		jQuery("#addtree").jqGrid(
				{

					data : newdata,
					datatype : "local",

					colNames : [ "id", "Market Subsector Description", "Market Subsector Code" ],
					colModel : [ {
						name : 'id',
						index : 'id',
						hidden : true,
						editable : true
					}, {
						name : 'categoryDesc',
						index : 'categoryDesc',
						align : "center",
						editable : true,
						editrules : {
							required : true
						}

					}, {
						name : 'categoryCode',
						index : 'categoryCode',
						align : "center",
						editable : true,
						editrules : {
							required : true,
							number : true
						}

					}  ],

					height : '350',
					pager : "#paddtree",
					rowNum : 10,
					rowList : [ 10, 20, 50 ],
					loaded : true,
					loadonce: true,
					cmTemplate: { title: false },
					editurl : 'commoditycategory.do?method=updateCommodityCategory'
							+ '&random=' + Math.random(),
					hidegrid : false
				});
		jQuery("#addtree").jqGrid('navGrid', "#paddtree", {
			add : true,
			edit : true,
			del : true,
			search : false

		}, {
			recreateForm : true,
			height : 100,
			width : 500,
			reloadAfterSubmit : true,
			afterSubmit: function () {
				window.location.reload(true);
            },
			closeAfterEdit : true,
			afterSubmit : function(response, postdata)
			{
				checkUnity();
			}
		}, {
			recreateForm : true,
			height : 200,
			width : 500,
			reloadAfterSubmit : true,
			afterSubmit: function () {
				window.location.reload(true);
            },
            afterSubmit : function(response, postdata)
			{
				checkUnity();
			},
			closeAfterAdd : true

		}, {}, {});

		$(window).bind('resize', function() {
			$("#addtree").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize');
	}
</script>


