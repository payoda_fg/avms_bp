<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.model.*,java.util.ArrayList,com.fg.vms.customer.dto.CommodityDto"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray commodityCellArray=new JSONArray();
	JSONObject commodityCellObj=null;
	List<CommodityDto> sectorCommodities=null;
	CommodityDto commodityDto=new CommodityDto();
	
	// Getting the Market Sector values with related Commodities...
	if(session.getAttribute("commoditysectors") != null)
	{
		sectorCommodities=(List<CommodityDto>)session.getAttribute("commoditysectors");
	}
	if(sectorCommodities!=null){
		for(int index=0; index<sectorCommodities.size(); index++)
		{
			commodityDto=sectorCommodities.get(index);
			commodityCellObj = new JSONObject();
			commodityCellObj.put("sectorId",commodityDto.getSectorId());
			commodityCellObj.put("sectordescription",commodityDto.getSectorDescription());
			commodityCellObj.put("subSectorId", commodityDto.getCommCategoryId());
			commodityCellObj.put("subSectorDescription", commodityDto.getCategoryDesc());
			commodityCellObj.put("commodityId", commodityDto.getCommodityId());
			commodityCellObj.put("commodityDescription", commodityDto.getCommodityDescription());
			commodityCellArray.add(commodityCellObj);
		}
		out.println(commodityCellArray);
	}
%>
