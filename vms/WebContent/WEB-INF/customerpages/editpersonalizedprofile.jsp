<%@page import="com.fg.vms.admin.dto.CustomerInfoDto"%>
<%@taglib uri="http://struts.application-servers.com/layout"
	prefix="layout"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<style>

/* Height and Width of Terms & Condition Text Area */
#termsCondition
{
	height: 80px;
	width: 600px;
}

#privacyTerms
{
	height: 80px;
	width: 600px;
}

</style>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/jscolor.js"></script>
<script type="text/javascript">
	var imgPath = "jquery/images/calendar.gif";
	datePickerFiscal();

	function realodPage() {
		window.location = "retrivecustomer.do?method=retriveCustomerInfoFromCustomerDB";
	}
	function checkDivisionStatus() {
		var mydata = '';
		$.ajax({
			url : "retrivecustomer.do?method=checkCustomerDivisionStatus",
			type : "POST",
			async : true,
			beforeSend : function() {
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show(); //show image loading
			},
			success : function(data) {
				mydata = data[0];

				$("#ajaxloader").hide();

				if (mydata == 'N') {
					alert("Please Create Division or There is no Active Division");
					realodPage();
				}
			}
		});

	}
</script>
<div id="main_table">
	<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Select company logo</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:file property="companyLogo" name="editcustomer"
				styleId="companyLogo" styleClass="textBox form-control" tabindex="33" />
			<bean:define id="logoPath" property="logoImagePath"
				name="editcustomer"></bean:define>
		</div>
		<span class="error"><html:errors property="companyLogo"></html:errors></span>
		<!-- <img
			width="100" height="100"
			src="<%=request.getContextPath()%>/images/dynamic/?file=<%=logoPath%>"> -->
			<img
			width="100" height="100" src="/demo/images/logo.png"/>
	</div>
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Select Menu color</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text property="menuColor" styleClass="color form-control" tabindex="34" />
		</div>
	</div>
</div>

	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is Division</label>
		<div class="col-sm-9 ctrl-col-wrapper" id="status">
			<html:radio property="isDivision" value="1" name="editcustomer"
				onclick="checkDivisionStatus();" styleId="isActive" tabindex="35">&nbsp;Yes&nbsp;</html:radio>
			<html:radio property="isDivision" value="0" name="editcustomer"
				styleId="isActive" tabindex="35">&nbsp;No&nbsp;</html:radio>
		</div>
	</div>
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Session Timeout</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text property="sessionTimeout" alt="" name="editcustomer"
				styleId="sessionTimeout" styleClass="text-box form-control" tabindex="36"
				 />
			<span>&nbsp;(in Minutes)</span>
		</div>
		<span class="error"><html:errors property="sessionTimeout"></html:errors></span>
	</div>

	
	<!-- In This Div We Should Have Only 1 Field Bcoz this is Big in Size-->
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Terms and Condition</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:textarea property="termsCondition" styleClass="main-text-area form-control"
				name="editcustomer" alt="" styleId="termsCondition" tabindex="37"></html:textarea>
		</div>
		<span class="error"><html:errors property="termsCondition"></html:errors></span>
	</div>

	
	<!-- In This Div We Should Have Only 1 Field Bcoz this is Big in Size-->
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Privacy and Terms</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:textarea property="privacyTerms" styleClass="main-text-area"
				name="editcustomer" alt="" styleId="privacyTerms" tabindex="38"></html:textarea>
		</div>
		<span class="error"><html:errors property="privacyTerms"></html:errors></span>
	</div>

	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Prime Training Url</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text property="primeTrainingUrl" styleClass="text-box form-control"
				name="editcustomer" alt="" styleId="primeTrainingUrl" tabindex="39"/>
		</div>
		<span class="error"><html:errors property="primeTrainingUrl"></html:errors></span>
	</div>
	<div class="row-wrapper form-group row">
		<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Diverse Supplier Training Url</label>
		<div class="col-sm-9 ctrl-col-wrapper">
			<html:text property="nonPrimeTrainingUrl" styleClass="text-box form-control"
				name="editcustomer" alt="" styleId="nonPrimeTrainingUrl" tabindex="40"/>
		</div>
		<span class="error"><html:errors property="nonPrimeTrainingUrl"></html:errors></span>
	</div>

<div class="clear"></div>