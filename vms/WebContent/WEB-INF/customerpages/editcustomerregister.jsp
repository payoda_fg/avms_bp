<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<div>
	<header class="card-header">
				<h2 class="card-title pull-left">Company</h2></header>
	<div class="form-box card-body">
		<html:messages id="msg" property="customer" message="true">

			<script type="text/javascript">
				alert("Successfully saved customer profile ");
			</script>
		</html:messages>
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Company Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="companyname" name="editcustomer"
						styleId="companyname" alt="" styleClass="text-box form-control" tabindex="2" />
				</div>
				<span class="error"><html:errors property="companyname"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<html:hidden property="hiddenCustCode" alt=""
					styleId="hiddenCustCode" />
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Company Code</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="companycode" name="editcustomer" alt=""
						styleId="companycode" styleClass="text-box form-control" tabindex="3" readonly="true"
						onblur="ajaxEdFn(this,'hiddenCustCode','CC');" />
				</div>
				<span class="error"><html:errors property="companycode"></html:errors></span>
			</div>		
		
			<div class="row-wrapper form-group row">
				<html:hidden property="hiddenDunsNumber" alt=""
					styleId="hiddenDunsNumber" />
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Duns Number</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="dunsnum" name="editcustomer" alt=""
						styleClass="text-box form-control" styleId="dunsnum"
						onchange="ajaxEdFn(this,'hiddenDunsNumber', 'D')" tabindex="4" />
				</div>
				<span class="error"><html:errors property="dunsnum"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<html:hidden property="hiddenTaxId" alt="" styleId="hiddenTaxId" />
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Tax ID</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="taxid" name="editcustomer" alt="Optional"
						tabindex="5" styleId="taxid" styleClass="text-box form-control"
						onchange="ajaxEdFn(this,'hiddenTaxId', 'T')" />
				</div>
				<span class="error"><html:errors property="taxid"></html:errors></span>
			</div>
		
	</div>
	<div class="clear"></div>
</div>

<div>
	<header class="card-header">
				<h2 class="card-title pull-left">Address</h2></header>

	<div class="form-box card-body">		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Address</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="address1" name="editcustomer" alt=""
						styleId="address1" styleClass="text-box form-control" tabindex="6" />
				</div>
				<span class="error"><html:errors property="address1"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="city" alt="" name="editcustomer"
						styleId="city" styleClass="text-box form-control" tabindex="7" />
				</div>
				<span class="error"><html:errors property="city"></html:errors></span>
			</div>		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="state" alt="" name="editcustomer"
						styleId="state" styleClass="text-box form-control" tabindex="8" />
				</div>
				<span class="error"><html:errors property="state"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Province</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="province" alt="Optional" name="editcustomer"
						styleId="province" styleClass="text-box form-control" tabindex="9" />
				</div>
				<span class="error"><html:errors property="province"></html:errors></span>
			</div>
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Region</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="region" alt="Optional" name="editcustomer"
						styleId="region" styleClass="text-box form-control" tabindex="10" />
				</div>
				<span class="error"><html:errors property="region"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<logic:present name="userDetails" property="workflowConfiguration">
						<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
							<html:select property="country" styleId="country" styleClass="form-control chosen-select" tabindex="11">
								<html:option value="" key="select">--Select--</html:option>
								<bean:size id="size" name="countryList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="country" name="countryList">
										<bean:define id="name" name="country" property="name"></bean:define>
										<html:option value="<%=name.toString()%>">
											<bean:write name="country" property="name" />
										</html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select>				
						</logic:equal>
						<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
							<html:text property="country" styleClass="text-box" readonly="true"/>
						</logic:equal>
					</logic:present>
				</div>
				<span class="error"> <html:errors property="country"></html:errors>
				</span>
			</div>
		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="mobile" alt="Optional" styleClass="text-box form-control"
						name="editcustomer" styleId="mobile" tabindex="12" />
				</div>
				<span class="error"><html:errors property="mobile"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="phone" alt="" name="editcustomer"
						styleId="phone" styleClass="text-box form-control" tabindex="13" />
				</div>
				<span class="error"><html:errors property="phone"></html:errors></span>
			</div>		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Fax</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="fax" alt="" styleClass="text-box form-control"
						name="editcustomer" styleId="fax" tabindex="14" />
				</div>
				<span class="error"><html:errors property="fax"></html:errors></span>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Website URL(http://)</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="webSite" alt="Optional" name="editcustomer"
						styleId="webSite" styleClass="text-box form-control" tabindex="15" />
				</div>
				<span class="error"><html:errors property="webSite"></html:errors></span>
			</div>		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Zip Code</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="zipcode" alt="" name="editcustomer"
						styleId="zipcode" styleClass="text-box form-control" tabindex="16" />
				</div>
				<span class="error"><html:errors property="zipcode"></html:errors></span>
			</div>
			
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="email" alt="Optional" name="editcustomer"
						styleId="email" styleClass="text-box form-control" tabindex="17" />
				</div>
				<span class="error"><html:errors property="email"></html:errors></span>
			</div>
		
	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
	var config = {
		'.chosen-select' : {
			width : "90%"
		}
		
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	}
</script>





