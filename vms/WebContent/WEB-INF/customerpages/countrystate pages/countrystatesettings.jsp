<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
	
<!-- Multi select -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>

<script type="text/javascript">
	$(document).ready(function() 
	{	
		loadCountryState();
	});	
	
	function loadCountryState()
	{
		$.ajax(
		{
			url : 'countrystate.do?method=listStateByCountry&random=' + Math.random(),
			type : "POST",
			async : false,
			success : function(data) 
			{
				$("#gridtable").jqGrid('GridUnload');
				fillCountryStateGrid(data);
			}
		});
	}

	function country() 
	{		
		$("#countryId").val('');
		$("#countryName").val('');		
		$("#provinceState option[value='S']").prop('selected', true);
		$("#provinceState").select2();
		
		$("#dialog1").css(
		{
			"display" : "block"
		});		
		
		$("#dialog1").dialog(
		{
			width : 500,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
		
		$("#countryName").focus();
	}

	function state() 
	{			
		$("#stateId").val('');
		$("#stateName").val('');
		$("#countriesGroup option[value='0']").prop('selected', true);
		$("#countriesGroup").select2();
		var tBody = '<tr>';
	   	tBody += '<td align="center" colspan="2">No State Found...</td>';
		tBody += '</tr>';
		$("#stateTableBody").html(tBody);
		
		$("#dialog2").css(
		{
			"display" : "block"
		});
		
		$("#dialog2").dialog(
		{
			width : 500,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
		$("#stateName").focus();
	}
	
	function saveCountry() 
	{	
		var countryName = $('#countryName').val();
		
		var provinceState = $('#provinceState').val();
		
		var countryId = $('#countryId').val();
		
		var re = /^[a-zA-Z0-9\s]+$/;
	
		if (countryName != '' && countryName != 'undefined') 
		{		
			if (re.test(countryName)) 
			{			
				$.ajax(
				{
					url : "countrystate.do?method=saveCountry&countryName="+ countryName + "&countryId=" + countryId + "&provinceState=" + provinceState,
					type : "POST",
					async : false,
					success : function(data) 
					{
						alert("Country saved successfully");
						$('#countryName').val('');
						$("#dialog1").dialog("close");
						//loadCountryState();
						window.location.reload(true);					
					}
				});
			}
		 	else 
			{
				alert('Please enter valid characters.');
				return false;
			}
		}
		else 
		{
			alert('Please enter country name.');
			return false;
		}
	}
	
	function editCountry(id)
	{	
		var provinceState = "";
		var countryName = "";
		var countryId = "";
		
		if (id != '' && id != 0) 
		{
			$.ajax(
			{
				url : "countrystate.do?method=retrieveCountry&id="+ id,
				type : "POST",
				async : false,
				success : function(data) 
				{					
					var parts = data.split("-"),
				    i, l;
					for (i = 0, l = parts.length; i < l; i += 3) 
					{						
						if(countryName == "")
						{
							countryName = countryName+$.trim(parts[i]);
						}
						if(countryId == "")
						{
							countryId = countryId+$.trim(parts[i+1]);
						}
						if(provinceState == "")
						{
							provinceState = provinceState+$.trim(parts[i+2]);							
						}
					}
				}
			});			
			$("#countryName").val(countryName);
			$("#countryId").val(countryId);			
			$("#provinceState option[value=" + provinceState + "]").attr('selected', 'selected');
			$("#provinceState").select2();
		} 
	}
	
	function deleteCountry(id)
	{	
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) 
		{		
			// Output when OK is clicked
			if (id != '' && id != 0) 
			{
				$.ajax(
				{
					url : "countrystate.do?method=deleteCountryCheck&id="+ id,
					type : "POST",
					async : false,
					success : function(data) 
					{						
						if(data=="delete")
						{
							$.ajax(
							{
								url : "countrystate.do?method=deleteCountry&id="+ id,
								type : "POST",
								async : false,
								success : function(data) 
								{
									alert("Country deleted successfully.");									
									$("#dialog1").dialog("close");
									//loadCountryState();
									window.location.reload(true);
								}
							});
						}
						if(data=="notDelete")
						{
							alert("Cannot delete country. It has references.");
							window.location.reload(true);
						}						
					}
				});
			} 
		} 
		else 
		{
			// Output when Cancel is clicked
			// alert("Delete cancelled");
			return false;
		}
	}
	
	function saveState() 
	{		
		var countries = [];
		$('#countriesGroup > :selected').each(function() 
		{
			countries.push($(this).val());
		});

		var stateName = $('#stateName').val();		
		var stateId = $('#stateId').val();	
		
		var re = /^[a-zA-Z0-9\s]+$/;

		if (stateName != '' && stateName != 'undefined') 
		{		
			if(re.test(stateName))
			{
				if(countries!=0 && stateName != '' && stateName != 'undefined' )
				{
					$.ajax(
					{
						url : "countrystate.do?method=saveState&stateName="+ stateName + "&countriesGroup=" + countries + "&stateId=" + stateId,
						type : "POST",
						async : false,
						success : function(data) 
						{
							alert("State saved successfully");
							$('#stateName').val('');
							$("#dialog2").dialog("close");
							//loadCountryState();
							window.location.reload(true);
						}
					});
				}
				else
				{
					alert('Please choose country name.');
					return false;
				}
			}		
			else
			{
				alert('Please enter valid characters.');
				return false;
			}
		}
		else 
		{
			alert('Please enter state name.');
			return false;
		}
	}
	
	function editState(id)
	{		
		var stateName = "";
		var stateId = "";
		var country = "";
		
		if (id != '' && id != 0) 
		{
			$.ajax(
			{
				url : "countrystate.do?method=retrieveState&id="+ id,
				type : "POST",
				async : false,
				success : function(data) 
				{
					var parts = data.split("-"),
				    i, l;
					for (i = 0, l = parts.length; i < l; i += 3) 
					{
						if(stateName == "")
						{
							stateName = stateName+$.trim(parts[i]);
						}
						if(stateId == "" && $.trim(parts[i+1]) != 0)
						{
							stateId = stateId+$.trim(parts[i+1]);
						}
						if($.trim(parts[i+2]) != 0)
						{
							country = country+$.trim(parts[i+2]);
						}
					}
				}
			});			
			$("#stateName").val(stateName);
			$("#stateId").val(stateId);
			$("#countriesGroup option[value=" + country + "]").attr('selected', 'selected');
			$("#countriesGroup").select2();
		} 
	}
	
	function deleteState(id)
	{			
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) 
		{
			// Output when OK is clicked
			if (id != '' && id != 0) 
			{
				$.ajax(
				{
					url : "countrystate.do?method=deleteStateCheck&id="+ id,
					type : "POST",
					async : false,
					success : function(data) 
					{						
						if(data=="delete")
						{
							$.ajax(
							{
								url : "countrystate.do?method=deleteState&id="+ id,
								type : "POST",
								async : false,
								success : function(data) 
								{
									alert("State deleted successfully.");									
									$("#dialog2").dialog("close");
									//loadCountryState();
									window.location.reload(true);
								}
							});
						}
						if(data=="notDelete")
						{
							alert("Cannot delete state. It has references.");
							window.location.reload(true);
						}						
					}
				});				
			}			
		} 
		else 
		{
			// Output when Cancel is clicked
			// alert("Delete cancelled");
			return false;
		}
	}
	
	function fillCountryStateGrid(myGridData)
	{		
		var newdata1 = jQuery.parseJSON(myGridData);

		$('#gridtable').jqGrid(
		{
			data : newdata1,
			datatype : 'local',
			colNames : [ 'Country', 'Country and State'],
			colModel : [
			            {
						name : 'country',
						index : 'country',
						/* width : 150, */
						align : 'center'
						}, 
						{
						name : 'state',
						index : 'state',
						/* width : 500, */
						align : 'center'
						}
					   ],
			rowNum : 10,
			rowList : [ 10, 20, 50 ],
			pager : '#pager',
			shrinkToFit : false,
			autowidth : true,
			viewrecords : true,
			emptyrecords : 'No Data Available..',
			grouping : true,
			groupingView : {
							groupField : [ 'country' ],
							groupColumnShow : [ false ],
							groupText : [ '<b>{0}</b>' ],
							groupCollapse : false,
							groupOrder : [ 'asc' ],
							groupSummary : [ true ],
							showSummaryOnHide : true,
							groupDataSorted : true
						   },
			cmTemplate: { title: false },
			height : 'auto'
		}).jqGrid('navGrid', '#pager', 
				{
					add : false,
					edit : false,
					del : false,
					search : false,
					refresh : false
				},
				{}, /* edit options */
				{}, /* add options */
				{}, /* del options */
				{});

		$(window).bind('resize', function() 
		{
			$("#gridtable").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize'); 
	}
</script>
<script type="text/javascript">
	function changeCountry() {
		var selectedcountry = $('#countriesGroup').val();
		if (selectedcountry != 0) {
			$
					.ajax({
						url : "countrystate.do?method=showCountryStateSettingByCountry&selectedcountry="
								+ selectedcountry,
						type : "POST",
						async : false,
						success : function(data) {
 							$("#stateName").val('');
							var tBody = '';
							var edit = data.editprivilege;
							var deleted = data.deleteprivilege;
							if (data.stateList=='') {
								tBody += '<tr>';
								tBody += '<td align=center colspan=2> No State Found...</td>';
								tBody += '</tr>';
							} else {
								for ( var i in data.stateList) {
									tBody += '<tr>';
									tBody += '<td align=center>'
											+ data.stateList[i].name + '</td>';
									tBody += '<td align=center>';
									if (edit == 'yes') {
										tBody += '<a href="#" onclick="editState('
												+ data.stateList[i].id
												+ ');">Edit</a>';
										if (deleted == 'yes')
											tBody += ' | ';
									}
									if (deleted == 'yes') {
										tBody += '<a href="#" onclick="deleteState('
												+ data.stateList[i].id
												+ ');">Delete</a>';
									}
									tBody += '</td>';
									tBody += '</tr>';
								}
							}
							$('#stateTableBody').html(tBody); 
						}
					});
		} else {
			alert('Please select country');
			return false;
		}
	}
</script>

<div class="clear"></div>

<section role="main" class="content-body card-margin admin-module mt-3">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<header>				
				<logic:present name="userDetails" property="workflowConfiguration">
					<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
						<input type="button" value="Country" onclick="country();" class="btn pull-right btn-primary">
					</logic:equal>
				</logic:present>		 
				<input type="button" value="State" onclick="state();" class="btn btn-primary pull-right">
			</header>

<div class="clear"></div>

<section class="card">	
<div id="form-box" class="card-body">
	<div class="responsiveGrid">
	<table id="gridtable" class="table table-bordered table-striped mb-0" >
	</table>
	<div id="pager"></div>
	</div>
</div>

<!-- For Dialog Box Country  -->
<div id="dialog1" title="Add Country" style="display: none;">
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Country" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="add">
	<input type="hidden" id="countryId" >
	<div class="wrapper-full">
		<div class="row-wrapper form-group row">
			<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country :</label>
			<div class="col-sm-9 ctrl-col-wrapper">
				<html:text property="countryName" alt="" name="countrystateform"
					styleId="countryName" styleClass="text-box form-control" />
			</div>
		</div>
	</div>
	<div class="wrapper-full">
		<div class="row-wrapper form-group row">
			<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Choose Province/State :</label>
			<div class="col-sm-9 ctrl-col-wrapper">			
				<html:select property="provinceState" styleClass="chosen-select" name="countrystateform" styleId="provinceState">					
					<html:option value="S">State</html:option>
					<html:option value="P">Province</html:option>
				</html:select>				
			</div>
		</div>
	</div>
	<footer class="mt-2">
		<div class="row justify-content-end">
			<div class="col-sm-8 wrapper-btn">
				<input type="button" id="countryBtn" value="Submit" class="btn btn-default"
			onclick="saveCountry();" />
		</div>
		</div>
	</footer>
	</logic:match>
	</logic:match>
	</logic:iterate>
	
	<logic:iterate id="privilege" name="privileges">
	<logic:match value="Country" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="add">
			<div style="text-align: center;">
				<h3>You have no right to add new country</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
	
<!-- ---Country -------- -->
	<logic:iterate id="privilege" name="privileges">

	<logic:match value="Country" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
	<div class="form-box card-body">
		<logic:present name="country1">
			<table class="main-table table table-bordered table-striped mb-0">
				<thead>
					<tr>
						<td>Country</td>
						<td>Actions</td>
					</tr>
				</thead>
				<tbody>
					<logic:iterate id="country11" name="country1">
						<tr class="even">
							<td><bean:write name="country11" property="countryname" /></td>
							<bean:define id="countryId" name="country11" property="id"></bean:define>
							<td align="center">
								<logic:match value="1" name="privilege" property="modify">
									<a href="#"
										onclick="editCountry('<%=countryId.toString()%>');">Edit</a>
									<logic:match value="1" name="privilege" property="delete">
								        | 
								     </logic:match>
								</logic:match>
								<logic:match value="1" name="privilege" property="delete">
									<a href="#"
									onclick="deleteCountry('<%=countryId.toString()%>');">Delete</a>
								</logic:match>
							</td>
						</tr>
					</logic:iterate>
				</tbody>
			</table>
		</logic:present>
	</div>
	</logic:match>
</logic:match>
</logic:iterate>
	<logic:iterate id="privilege" name="privileges">

	<logic:match value="Country" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view countries</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>

<!-- For Dialog Box State  -->
<div id="dialog2" title="Add State" style="display: none;"  >
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Country" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">
				<input type="hidden" id="stateId">
				
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">Choose
							Country :</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:select property="countriesGroup" styleClass="chosen-select form-control"
								name="countrystateform" styleId="countriesGroup"
								onchange="changeCountry()">
								<html:option value="0">-- Select --</html:option>
								<logic:present name="countrystateform" property="countries">
									<logic:iterate id="country" name="countrystateform"
										property="countries">
										<option value='<bean:write name="country" property="id"  />'><bean:write
												name="country" property="countryname" /></option>
									</logic:iterate>
								</logic:present>
							</html:select>
						</div>
					</div>
				
					<div class="row-wrapper form-group row">
						<label class="col-sm-4 label-col-wrapper control-label text-sm-right">State :</label>
						<div class="col-sm-8 ctrl-col-wrapper">
							<html:text property="stateName" alt="" name="countrystateform"
								styleId="stateName" styleClass="text-box form-control" />
						</div>
					</div>
				<footer class="mt-2">
						<div class="row justify-content-end">
							<div class="col-sm-8 wrapper-btn">
								<input type="button" id="stateBtn" value="Submit" class="btn btn-default"
							onclick="saveState();" />
						</div>
					</div>
				</footer>
			</logic:match>
		</logic:match>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Country" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="add">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to add new state</h3>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>
	<!-- 	state ---------- -->
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Country" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
				<div class="form-box card-body" style="overflow: auto;">
					<table class="main-table table table-bordered table-striped mb-0" id="statesByCountry">
						<thead>
							<tr>
								<td>State</td>
								<td>Actions</td>
							</tr>
						</thead>
						<tbody id="stateTableBody">
							<logic:present name="data">
								<logic:iterate id="state" name="data">
									<tr class="even">
										<td align="center"><bean:write name="state"
												property="statename" /></td>
										<bean:define id="stateId" name="state" property="id"></bean:define>
										<td align="center"><logic:match value="1"
												name="privilege" property="modify">
												<a href="#" onclick="editState('<%=stateId.toString()%>');">Edit</a>
												<logic:match value="1" name="privilege" property="delete">
										                           | 
										        </logic:match>
											</logic:match> <logic:match value="1" name="privilege" property="delete">
												<a href="#"
													onclick="deleteState('<%=stateId.toString()%>');">Delete</a>
											</logic:match></td>
									</tr>
								</logic:iterate>
							</logic:present>
							<logic:notPresent name="data">
								<tr>
									<td align="center" colspan="2">No state found...</td>
								</tr>
							</logic:notPresent>
						</tbody>
					</table>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Country" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no rights to view states</h3>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>
</div>
</section>
</div>
</div>
</section>