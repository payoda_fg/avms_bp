<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>

<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<script type="text/javascript">
function backToSearch()
{
	window.location = "viewVendors.do?method=showVendorStatusBySearch";
}
</script>

<style type="text/css">
/* Sortable style starts here */
/* Modified for Sorting Header*/ 
.main-table td,th
{
	border: 1px solid #e9eaea;
	padding: 5px;
}

.main-table th.header
{
	background: #009900;
	color: #fff;
	cursor: pointer;
}

/* Sorting Table Header */
table.sortable th:not(.sorttable_nosort):not(.sorttable_sorted):not(.sorttable_sorted_reverse):after
{ 
    content: " \25B4\25BE" 
}
/* Sortable style ends here */	
</style>
<div class="clear"></div>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">			
			<header class="card-header">
				<h2 class="card-title pull-left">
				<img src="images/icon-registration.png" />Following are Your Vendors</h2>
				
				<input type="button" value="Back" class="btn btn-primary pull-right" onclick="backToSearch();" />
				
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails" property="settings.logoPath"/>
					<input type="button" value="Export" class="btn btn-primary pull-right mr-1" onclick="exportGrid();">
				</logic:present>
				<div id="searchSummary" align="center" style="color: #009900;">
					<logic:present name="vendorSearchSummary">
						<bean:write name="vendorSearchSummary" format="String" />
					</logic:present>
				</div>
			</header>

<div class="form-box card-body" style="overflow: auto;">
	<table id="searchvendor" width="100%" border="0" class="sortable main-table table table-bordered table-striped mb-0">
		<thead>
			<tr>
				<th class="">Vendor Name</th>
				<th class="">Vendor Status</th>
				<th class="">Business Type</th>
				<th class="">Year of Establishment</th>							
				<th class="">State</th>
				<th class="">Contact First Name</th>
				<th class="">Contact Last Name</th>
				<th class="">Contact Phone</th>
				<th class="">Contact Email</th>							
				<th class="">Diverse Classification</th>
				<th class="">Certification Type</th>						
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="searchVendorsList">
				<logic:iterate name="searchVendorsList" id="vendorList">
					<tr>
						<bean:define id="vendorId" name="vendorList" property="id"/>
						<bean:define id="status" name="vendorList" property="vendorStatus"/>
						<td><html:link action="/retrievesupplier.do?method=showSupplierContactDetails&requestString=status" paramId="id" paramName="vendorId">
								<bean:write name="vendorList" property="vendorName" />
							</html:link>
						</td>									
						<td><bean:write name="vendorList" property="vendorStatus" /></td>
						<td><bean:write name="vendorList" property="businessType" /></td>
						<td><bean:write name="vendorList" property="yearOfEstd" /></td>									
						<td><bean:write name="vendorList" property="stateName" /></td>
						<td><bean:write name="vendorList" property="firstName" /></td>
						<td><bean:write name="vendorList" property="lastName" /></td>
						<td><bean:write name="vendorList" property="phoneNumber" /></td>
						<td><bean:write name="vendorList" property="primeContactEmailId" /></td>
						<td><bean:write name="vendorList" property="diverseClassifcation" /></td>
						<td><bean:write name="vendorList" property="certificateType" /></td>							
					</tr>
				</logic:iterate>
			</logic:notEmpty>
			<logic:empty name="searchVendorsList">
				<tr>
					<td colspan='11'>No records are found...</td>
				</tr>
			</logic:empty>						
		</tbody>
	</table>
</div>

<!-- Dialog Box to Export PDF, CSV, & XLS of All Vendors Details List -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel"/>
			</td>
			<td>
				<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"/> 
				<input type="hidden" name="fileName" id="fileName" value="InactiveVendorSearch">
			</td>
			<td>
				<label for="csv"><img id="csvExport" src="images/csv_export.png" />CSV</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails" property="settings.logoPath"/>
			<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportSearchResultHelper('searchvendor','InactiveVendorSearch','<%=logoPath%>');">
		</logic:present>
	</div>
</div>
</div>
</div>
</section>