<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>

<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 20px;">

		<div id="box2" style="max-height: 150px; overflow: auto;">
			<table width="100%">
				<bean:size id="size" name="rfiInformation" property="rfInformations" />
				<logic:greaterThan value="0" name="size">
					<thead>
						<tr>
							<th>SNo</th>
							<th>Request Number</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Description</th>
						</tr>
					</thead>
					<logic:iterate id="rfi" name="rfiInformation"
						property="rfInformations" indexId="index">
						<tbody>
							<tr>
								<td><bean:write name="index" /></td>
								<bean:define id="id" name="rfi" property="id"></bean:define>
								<td><html:link action="/rfiinformation?method=retriveRFI"
										paramId="id" paramName="id">
										<bean:write name="rfi" property="rfipNumber" />
									</html:link></td>
								<td><bean:write name="rfi" property="startDate" /></td>
								<td><bean:write name="rfi" property="endDate" /></td>
								<td><bean:write name="rfi" property="description" /></td>

							</tr>
						</tbody>
					</logic:iterate>
				</logic:greaterThan>
				<logic:equal value="0" name="size">
					<h4>No such Records</h4>
				</logic:equal>
			</table>
		</div>

	</div>
</div>