<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$.datepicker.setDefaults({
			changeYear : true,
			minDate : 0,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#date').removeClass('text-label');
			}
		});
		$("#date").datepicker();
	});
	var imgPath = "jquery/images/calendar.gif";

	function searchVendor(fieldId) {
		var assessmentTemplate = $('#templateId').val();
		var finalDate = $('#date').val();
		var templateName = $("#templateId option:selected").text();
		var emailAddresss = $('#mailArea').val();
		var cc = $('#cc').val();
		var bcc = $('#bcc').val();
		//alert('Id:'+fieldId);
		window.location = "capabalityAssessmentSearchvendor.do?method=showMailNotificationPage&assessmentTemplate="
				+ assessmentTemplate
				+ "&finalDate="
				+ finalDate
				+ "&templateName="
				+ templateName
				+ "&emailAddresss="
				+ emailAddresss
				+ "&cc="
				+ cc
				+ "&bcc="
				+ bcc
				+ "&id="
				+ fieldId;
	}

	function backToMailNotification() {
		window.location = "capabalityAssessmentSearchvendor.do?method=searchAssessmentVendor";
		// 		$.fn.reset = function () 
		// 		{
		//             $(this).each (function() 
		//        		{ 
		//             	this.reset();
		//             });                               
		//         }
		$('#mailContentArea').val('');
		$('#date').val('');
	}

	tinymce.init({
				selector : "textarea#mailContentArea",
				theme : "modern",
				statusbar : false,
				height : 300,
				width : 450,
				plugins : [
						"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
						"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
						"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
				toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
				toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
				menubar : false,
				file_browser_callback: AVMSFileBrowser,
				toolbar_items_size : 'medium'
			});
	function AVMSFileBrowser(field_name, url, type, win) {
		
		   var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;
	
		   tinyMCE.activeEditor.windowManager.open({
		     file: avmsFileman,
		     title: 'Email Template',
		     width: 600, 
		     height: 500,
		     resizable: "yes",
		     plugins: "media",
		     inline: "yes",
		     close_previous: "no"  
		  }, {     window: win,     input: field_name    });
		  return false; 
		}
</script>
<style>
/*.main-text-area {
	width: 91%;
	padding: 0%;
}

.main-text-box {
	width: 87%;
}*/
</style>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<section role="main" class="content-body card-margin mt-4">
	<div class="row">
		<div class="col-lg-12 mx-auto">	
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Assessment Email Notification" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<header class="card-header">
				<h2 class="card-title">
				<img src="images/icon-mail-note.gif" />&nbsp;&nbsp;Assessment Email	Notification</h2>
			</header>
			<div class="form-box card-body">
				<html:form
					action="/assessmentmailNotification?parameter=assessmentMailNotify"
					enctype="multipart/form-data">
					<html:javascript formName="mailForm" />
					<div id="successMsg">
						<html:messages id="msg" property="mail" message="true">
							<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
						</html:messages>
					</div>
					<div id="failureMsg">
						<html:messages id="msg" property="mailfailed" message="true">
							<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
						</html:messages>
					</div>
					<div class="form-box card-body">
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Assessment Template:</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<logic:present name="templateassessment">
									<bean:define id="selectedtemplate" name="templateassessment"></bean:define>
									<html:select property="templateId" name="mailForm"
										styleId="templateId" value="<%=selectedtemplate.toString()%>"
										onchange="getSubject()" styleClass="chosen-select form-control">
										<html:option value="0">--Select--</html:option>
										<logic:present name="templateNameList">
											<bean:size id="size" name="templateNameList"></bean:size>
											<logic:iterate name="templateNameList" id="templateList">
												<bean:define id="templateId" name="templateList"
													property="id"></bean:define>
												<html:option value="<%=templateId.toString()%>">
													<bean:write name="templateList" property="templateName"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:present>
									</html:select>
								</logic:present>
								<logic:notPresent name="templateassessment">
									<html:select property="templateId" name="mailForm"
										styleClass="main-list-box form-control" styleId="templateId"
										onchange="getSubject()">
										<html:option value="0">--Select--</html:option>
										<logic:present name="templateNameList">
											<bean:size id="size" name="templateNameList"></bean:size>
											<logic:iterate name="templateNameList" id="templateList">
												<bean:define id="templateId" name="templateList"
													property="id"></bean:define>
												<html:option value="<%=templateId.toString()%>">
													<bean:write name="templateList" property="templateName"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:present>
									</html:select>
								</logic:notPresent>
							</div>
						</div>

						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Time Frame for Reply :</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<logic:present name="finalDate">
									<bean:define id="selecteddate" name="finalDate"></bean:define>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>
									<html:text styleClass="main-text-box form-control"
										property="finalDateForReply" name="mailForm"
										alt="Please click to select date" styleId="date"
										value="<%=selecteddate.toString()%>" readonly="true"
										onchange="return dateValidation('date')" ></html:text>
									</div>
									<span class="error"><html:errors
											property="finalDateForReply" /></span>
								</logic:present>
								<logic:notPresent name="finalDate">
									<html:text styleClass="main-text-box form-control"
										property="finalDateForReply" name="mailForm"
										alt="Please click to select date" styleId="date"
										onchange="return dateValidation('date')"></html:text>
									<span class="error"><html:errors
											property="finalDateForReply" /></span>
								</logic:notPresent>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">To:</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<logic:present name="emailAddress">
									<bean:define id="emailAddress" name="emailAddress"></bean:define>
									<span>Enter Email Addresses, separate by semicolon ';'</span>
									<html:textarea property="to" name="mailForm" styleId="mailArea"
										value="<%=emailAddress.toString()%>" cols="80" rows="5"
										styleClass="main-text-area form-control pull-left">
									</html:textarea>
									<img style="cursor: pointer;" src="images/search_icon.png"
										onclick="searchVendor('TO')">
								</logic:present>
								<logic:notPresent name="emailAddress">
									<bean:define id="emailAddress" name="emailAddress"></bean:define>
									<span>Enter email addresses separate by semi';'</span>
									<html:textarea property="" value="" styleId="mailArea"
										cols="70" styleClass="main-text-area"></html:textarea>
									<html:button value="Search Vendor" onclick="searchVendor('TO')"
										property=""></html:button>
								</logic:notPresent>
							</div>
							<span style="color: red;"><html:errors property="to" /> </span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">CC:</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<logic:present name="cc">
									<bean:define id="cc" name="cc"></bean:define>
									<span>Enter CC, separate by semicolon ';'</span>
									<html:textarea property="cc" styleClass="main-text-area form-control pull-left"
										name="mailForm" cols="70" styleId="cc"
										value="<%=cc.toString()%>" rows="3">
									</html:textarea>
									<img style="cursor: pointer;" src="images/search_icon.png"
										onclick="searchVendor('CC')">
								</logic:present>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">BCC:</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<logic:present name="bcc">
									<bean:define id="bcc" name="bcc"></bean:define>
									<span>Enter BCC, separate by semicolon ';'</span>
									<html:textarea property="bcc" styleClass="main-text-area form-control pull-left"
										name="mailForm" cols="70" styleId="bcc"
										value="<%=bcc.toString()%>" rows="3">
									</html:textarea>
									<img style="cursor: pointer;" src="images/search_icon.png"
										onclick="searchVendor('BCC')">
								</logic:present>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Attachment :</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">

								<html:file property="attachment" styleId="attachment"></html:file>
							</div>
							<span style="color: red;"><html:errors
									property="attachment" /></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Subject :</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<logic:present name="templateAssessmentName">
									<bean:define id="selectedtemplate"
										name="templateAssessmentName"></bean:define>
									<html:text styleClass="main-text-box form-control" property="subject"
										name="mailForm" size="60" alt="" onblur="dueAlert()"
										styleId="subject" value="<%=selectedtemplate.toString()%>"></html:text>
									<span style="color: red;"><html:errors
											property="subject" /> </span>
								</logic:present>
								<logic:notPresent name="templateAssessmentName">
									<bean:define id="selectedtemplate"
										name="templateAssessmentName"></bean:define>
									<html:text property="subject" name="mailForm" size="60" alt=""
										onblur="dueAlert()" styleClass="form-control" styleId="subject"></html:text>
									<span style="color: red;"><html:errors
											property="subject" /> </span>
								</logic:notPresent>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mail Content :</label>
							<div class="col-sm-9 ctrl-col-full-wrapper">
								<html:textarea property="message" name="mailForm" rows="7"
									styleId="mailContentArea" onfocus="dueAlert()"
									styleClass="main-text-area" style=""></html:textarea>
							</div>
							<span style="color: red;"><html:errors property="message" />
							</span>
						</div>
					</div>
					<footer class="mt-2">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<html:submit value="Send Email" styleClass="btn btn-primary" styleId="submit" onclick="return Validation();"></html:submit>
						<html:reset value="Cancel" styleClass="btn btn-default" styleId="submit"
							onclick="backToMailNotification();"></html:reset>
						</div>
					</div>
				</footer>
				</html:form>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Assessment Email Notification" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box text-center">
				<h4>You have no rights to view assessment email notification</h4>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
	var config = {
		'.chosen-select' : {
			width : "60%"
		},
	}
	for ( var selector in config) {
		$(selector).chosen(config[selector]);
	} 
	
	
	function Validation()
	{
		var toValue=$('#mailArea').val();
		var ccValue=$('#cc').val();
		var bccValue=$('#bcc').val();
		if(toValue=='' && ccValue=='' && bccValue=='')
		{
			alert("Enter atleast one email id in TO/CC/BCC.");
// 			$("#ajaxloader").hide();
			location.reload(true);
			return false;
		}
	}	
	
</script>
