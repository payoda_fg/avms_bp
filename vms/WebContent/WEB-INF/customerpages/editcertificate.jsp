<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.fg.vms.customer.model.Certificate"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript">
	
	$(document).ready(function() {

		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
		}, "No Special Characters Allowed.");

		$("#certificateFormID").validate({
			rules : {
				certificateName : {
					required : true,
					alpha : true,
					maxlength : 255
				},
				certificateShortName : {
					required : true,
					maxlength : 255
				},
				diverseQuality : {
					required : true
				},
				isEthinicity:
				{
					required : true	
				},
				certificateUpload:
				{
					required : true
				}
				<logic:equal value="1" name="isDivisionStatus" property="isDivision">,
					customerDivision : 
					{
						required : true
					} 
				</logic:equal>	
			}/*,
			submitHandler : function(form) {
				$("#ajaxloader").css('display','block');
				$("#ajaxloader").show();
				form.submit();
			}*/
		});
		
		$("#certificateAgency").multiselect({
			selectedText : "# of # selected"
		});
		
		$("#certificateType").multiselect({
			selectedText : "# of # selected"
		});
		
		$("#customerDivision").multiselect({
			selectedText : "# of # selected"
		});

	});
	function backToCertificate() {
		window.location = "viewcertificate.do?parameter=viewCertificate";
	}

</script>
<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-8 mx-auto">
		<section class="card">
			<header class="card-header">
				<h2 class="card-title">Update Classification</h2>
			</header>
			<div id="successMsg">
				<html:messages id="msg" property="certificateReference" message="true">
					<span><bean:write name="msg" /></span>
				</html:messages>
			</div>
		<div id="grid_container" class="form-box card-body">
			<html:form action="/updatecertificate.do?parameter=updatecertificate"
				styleId="certificateFormID">
				<html:javascript formName="certificateForm" />
				<html:hidden property="id" name="certificateForm" />
		
				
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Classification Name</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:text property="certificateName" name="certificateForm" alt=""
								styleClass="text-box form-control" styleId="certificateName" />
						</div>
						<span class="error"><html:errors property="certificateName"></html:errors></span>
					</div>
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Classification Short Name</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:text property="certificateShortName" name="certificateForm"
								alt="" styleClass="text-box form-control" styleId="certificateShortName" />
						</div>
						<span class="error"><html:errors property="certificateShortName"></html:errors></span>
					</div>
				
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Certificate Agency</label>
						<div class=" col-sm-7 ctrl-col-wrapper">
							<html:select property="certificateAgency" multiple="true"
								styleId="certificateAgency" styleClass="form-control">
								<bean:size id="size" name="certAgencyList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="certAgency" name="certAgencyList">
										<bean:define id="id" name="certAgency" property="id"></bean:define>
										<html:option value="${id}">&nbsp;<bean:write name="certAgency" property="agencyName"></bean:write></html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select>
						</div>
					</div>
					
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Certificate Type</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:select property="certificateType" multiple="true"
								styleId="certificateType" styleClass="form-control">
								<bean:size id="size" name="certificateTypes" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="type" name="certificateTypes">
										<bean:define id="id" name="type" property="certficateTypeId"></bean:define>
										<html:option value="${id}">&nbsp;<bean:write name="type" property="certficateTypeDescription"></bean:write></html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select>
						</div>
					</div>
				
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Classification Type</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:radio property="diverseQuality" value="1"
								name="certificateForm" styleId="diverseQuality">&nbsp;Diverse&nbsp;</html:radio>
							<html:radio property="diverseQuality" value="0"
								name="certificateForm" styleId="diverseQuality">&nbsp;Quality&nbsp;</html:radio>
						</div>
						<span class="error"><html:errors property="diverseQuality"></html:errors></span>
					</div>
		
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">IsActive</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
							<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
						</div>
					</div>
				
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Capture Ethnicity?</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:radio property="isEthinicity" value="1">&nbsp;Yes&nbsp;</html:radio>
							<html:radio property="isEthinicity" value="0" >&nbsp;No&nbsp;</html:radio>
						</div>
					</div>
					<div class="form-group row row-wrapper">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper"> Certificate upload Required?</label>
						<div class="col-sm-7 ctrl-col-wrapper">
							<html:radio property="certificateUpload" value="1">&nbsp;Yes&nbsp;</html:radio>
							<html:radio property="certificateUpload" value="0">&nbsp;No&nbsp;</html:radio>
						</div>
					</div>
					
					<div class="form-group row row-wrapper">
						<logic:equal value="1" name="isDivisionStatus" property="isDivision">
							<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Customer Division</label>
							<div class="col-sm-7 ctrl-col-wrapper">									
								<html:select styleClass="form-control" property="customerDivision" multiple="true" name="certificateForm" styleId="customerDivision">										
									<bean:size id="size" name="customerDivisions" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="customerDivision" name="customerDivisions">
												<bean:define id="id" name="customerDivision" property="id"></bean:define>
												<html:option value="${id}">
													<bean:write name="customerDivision" property="divisionname"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
								</html:select>
							</div>
							<span class="error"><html:errors
									property="customerDivision"></html:errors> </span>
						</logic:equal>
					</div>
					
				<%-- <div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper"> Is Refinery Selectable?</div>
						<div class="ctrl-col-wrapper">
							<html:radio property="isRefinerySelectable" value="1">&nbsp;Yes&nbsp;</html:radio>
							<html:radio property="isRefinerySelectable" value="0">&nbsp;No&nbsp;</html:radio>
						</div>
					</div>
				</div> --%>
				<logic:iterate id="privilege" name="privileges">		
					<logic:match value="View Certificate" name="privilege"
						property="objectId.objectName">
						<logic:match value="1" name="privilege" property="modify">
							<footer class="card-footer mt-4">
									<div class="row justify-content-end">
										<div class="col-sm-9 wrapper-btn">
											<html:submit value="Update" styleClass="btn btn-primary" styleId="submit1"></html:submit>
											<html:reset value="Cancel" styleClass="btn btn-default"
												onclick="backToCertificate();"></html:reset>
										</div>									
									</div>
								</footer>					
						</logic:match>
					</logic:match>
		
				</logic:iterate>
			</html:form>
		</div>
	</section>
</div>
</div>
 
<div class="grid-wrapper">
		<div class="form-box">
			<div class="row">
				<div class="col-lg-8 mx-auto">
<logic:iterate id="privilege" name="privileges">
	<logic:match value="View Certificate" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="page-title">
				<img src="images/arrow-down.png" alt="" />Available Classification
			</div>
			<div id="grid_container" class="card-body">
				<table class="main-table table table-bordered table-striped mb-0" id="certificateTable">
					<bean:size id="size" name="certificates" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<th>Classification Name</th>
								<th>Classification Short Name</th>
								<th>Diverse / Quality</th>
								<th>IsActive</th>
								<th>Actions</th>
							</tr>
						</thead>
						<logic:iterate name="certificates" id="certificatelist">
							<tbody>
								<tr>
									<td><bean:write name="certificatelist"
											property="certificateName" /></td>
									<td><bean:write name="certificatelist"
											property="certificateShortName" /></td>
									<td><logic:equal value="1" name="certificatelist"
											property="diverseQuality">
													Diverse
												</logic:equal> <logic:equal value="0" name="certificatelist"
											property="diverseQuality">
													Quality
												</logic:equal></td>
									<td>Yes</td>

									<bean:define id="certificateId" name="certificatelist"
										property="id"></bean:define>
									<td><logic:iterate id="privilege" name="privileges">

											<logic:match value="View Certificate" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="modify">
													<html:link paramId="id" paramName="certificateId"
														action="/retrivecertificate.do?parameter=retrivecertificate">Edit</html:link>
												</logic:match>
											</logic:match>

										 </logic:iterate> <logic:iterate id="privilege" name="privileges">
											<logic:match value="View Certificate" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="delete">
													<logic:notEqual
														value="<%=certificateId
														.toString()%>"
														property="id" name="certificateForm">
																| <html:link
															action="/deletecertificate.do?parameter=deletecertificate"
															paramId="id" paramName="certificateId"
															onclick="return confirm_delete();">Delete</html:link>
													</logic:notEqual>
													<logic:equal
														value="<%=certificateId
														.toString()%>"
														property="id" name="certificateForm">
														<span style="color: green; font-weight: bold;">|
															Classification in Edit</span>
													</logic:equal>
												</logic:match>
											</logic:match>
										</logic:iterate> 
								</tr>
							</tbody>
						</logic:iterate>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
									No such Records
							</logic:equal>
				</table>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="View Certificate" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view Classification</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</div>
</div>
</section>