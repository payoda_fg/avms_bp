<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

 <link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script> 

<!-- Added for MultiSelect Drop-down Box -->
<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />

<script type="text/javascript" src="jquery/ui/jquery.ui.autocomplete.js"></script>

<script type="text/javascript">

function searchByVendorStatus()
{
	$.ajax(
	{
		url : "viewVendors.do?method=searchByVendorStatus",
		type : 'POST',
		data : $("#searchForm").serialize(),
		async : false,
		dataType : "json",
		beforeSend : function()
		{
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data)
		{
			$("#ajaxloader").hide();
			window.location = "viewVendors.do?method=vendorStatusWiseSearchRestult&searchPageType=searchByAllStatus";
		}
	});				 
}

$( document ).ajaxError(function()
{
	$("#ajaxloader").hide();
});

$(document).ready(function()
{
	/* $("#vendorStatusList").multiselect(
	{
		selectedText : "# of # selected"
	});
	
	$("#vendorNamesList").multiselect(
	{
		selectedText : "# of # selected"
	}); */
});
</script>

<section role="main" class="content-body card-margin mt-5 pt-5">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">Inactive Vendor Search </h2>
			</header>
		
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Inactive Vendor Search" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box card-body">
				<html:form styleId="searchForm">				
					
						<div class="row-wrapper form-group row">
							<!-- <div class="label-col-wrapper" style="width: 30%;">Vendor Status:</div> -->
							<div class="ctrl-col-wrapper">
								<%-- <html:select property="allStatusList" name="searchVendorForm" multiple="true" styleId="vendorStatusList" 
									style="max-width:250px;" styleClass="chosen-select fields">
									<logic:present name="vendorStatusList">
										<bean:size id="size" name="vendorStatusList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="vendorStatus" name="vendorStatusList">
												<bean:define id="id" name="vendorStatus" property="id"></bean:define>
												<html:option value="${id}">
													<bean:write name="vendorStatus" property="statusname"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</html:select>	 --%>	
								<html:hidden value="I" property="allStatusList" name="searchVendorForm" styleId="allStatusList" style="max-width:230px;"
								styleClass="main-text-box"/>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor Name:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<%-- <html:select property="vendorNamesList" name="searchVendorForm" multiple="true" styleId="vendorNamesList" 
									style="max-width:250px;" styleClass="chosen-select fields">
									<logic:present name="vendorNamesList">
										<bean:size id="size" name="vendorNamesList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="vendorName1" name="vendorNamesList">
												<bean:define id="id" name="vendorName1" property="vendorId"/>
												<html:option value="${id}">
													<bean:write name="vendorName1" property="vendorName"/>
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</html:select> --%>		
								<html:text property="vendorNamesList" name="searchVendorForm" styleId="vendorNamesList" style="max-width:230px;"
								styleClass="main-text-box form-control"/>
							</div>
						</div>
					
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
							<input type="button" value="Search" class="btn btn-primary" id="statusSearch" onclick="searchByVendorStatus()" />					
						</div>
						</div>
					</footer>
				</html:form>
			</div>
		</logic:match>
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box">
				<h3 align="center">You have no rights to search the inactive vendor.</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script>
$(function()
{
	var items=[];
	function split( val )
	{
		return val.split( /;\s*/ );
	}
	
	function extractLast( term )
	{
		return split( term ).pop();
	}

	$("#vendorNamesList").bind( "keydown", function( event )
	{
		if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active )
		{
			event.preventDefault();
		}
	}).autocomplete(
	{
		source: function( request, response )
		{
			var searchCode = extractLast( request.term );
	 		$.ajax(
			{
	        	url: "viewVendorsStatus.do?method=searchVendorNameForAutoComplete&term="+searchCode,
	        	dataType: "text",
	        	type: "GET",
	        	success: function (data)
	        	{
	        		while(items.length>0)
	        		{
	        			items.pop();
	        		}
	        		var vendorNames=data.split(';');
					for(i=0;i <vendorNames.length;i++ )
					{
						var vendorName='';
						vendorName=vendorNames[i];
						if(i==0)
							vendorName= vendorName.replace('[',' ');
					 	if(i==vendorNames.length-1)
					 		vendorName= vendorName.replace(']',' ');
						if(vendorName.charAt(0) === ' ')
						{
							vendorName = vendorName.substr(1);
						}
						
						items.push(vendorName);
					}
	            	response(items);
	        	}
	    	});	
		},
		search: function()
		{
			var term = extractLast( this.value );
			if ( term.length < 2 )
			{
				return false;
			}
		},
		focus: function()
		{
			return false;
		},
		select: function( event, ui )
		{
			var terms = split( this.value );
			terms.pop();
			terms.push( ui.item.value );
			terms.push( "" );
			this.value = terms.join( ";" );
			return false;
		}
	});
});
</script>