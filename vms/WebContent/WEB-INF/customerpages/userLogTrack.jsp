<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<script type="text/javascript" src="js/exportExcel.js"></script>
<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<style type="text/css">
	/* Modified for Sorting Header*/ 
	/*.main-table td,th {
		border: 1px solid #e9eaea;
		padding: 5px;
	}
	
	.main-table th.header {
		background: #009900;
		color: #fff;
		cursor: pointer;
	}
	
	/* Sorting Table Header 
	table.sortable th:not(.sorttable_nosort):not(.sorttable_sorted):not(.sorttable_sorted_reverse):after { 
	    content: " \25B4\25BE" 
	}*/
</style>
<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-12 mx-auto">	
<logic:iterate id="privilege" name="privileges">
	<logic:match value="View User Log" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<section class="card">
				<header class="card-header">
					<h2 class="card-title pull-left">Users Login Details</h2>
					<input type="button" value="Export" class="btn btn-primary pull-right" onclick="exportGrid();"/>
				</header>
				
			<div class="form-box card-body">
				<html:form action="/userLogTrack.do">
					<div class="responsiveGrid">
						<table class="sortable main-table table table-bordered table-striped mb-0 table-no-more" id = "userLogTrack">
							<logic:present name="logTracks">
								<bean:size id="size" name="logTracks" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Email Id</th>
											<th>Role Name</th>
											<th>Last Logged On</th>
											<th class="sorttable_nosort ">Action</th>
										</tr>
									</thead>
									<tbody>
										<logic:iterate name="logTracks" id="logTracksList">
											<tr>
												<td data-title="First Name"><bean:write name="logTracksList" property="firstName" /></td>
												<td data-title="Last Name"><bean:write name="logTracksList" property="lastName" /></td>
												<td data-title="Email Id"><bean:write name="logTracksList" property="userEmailId" /></td>
												<td data-title="Role Name"><bean:write name="logTracksList" property="roleName" /></td>
												<td data-title="Last Logged On"><bean:write name="logTracksList" property="userLogon" /></td>
												<bean:define id="emailId" name="logTracksList" property="userEmailId"></bean:define>
												<td data-title="Action">
													<html:link action="/userLogTrack.do?parameter=viewUserLoginTimes"
														paramName="emailId" paramId="userEmailId">View User Log</html:link>
												</td>
											</tr>
										</logic:iterate>
									</tbody>
								</logic:greaterThan>
							</logic:present>
						</table>
					</div>
				</html:form>
			</div>
			</section>
		</logic:match>
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view user log</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<!-- Dialog Box to Export PDF, CSV, & XLS of All Vendors Details -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel"/>
			</td>
			<td>
				<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"/> 
				<input type="hidden" name="fileName" id="fileName" value="UserLogReport">
			</td>
			<td>
				<label for="csv"><img id="csvExport" src="images/csv_export.png" />CSV</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportViewUserLog('userLogTrack','UserLogReport');">
	</div>
</div>
</div>
</div>
</section>


<script>
	function exportViewUserLog(table, name) 
	{
		var exportType = $("input[name=export]:checked").val();
	
		if (exportType == 1) {			
			exportViewUserLogInExcel(table, name);
		} else if (exportType == 2) {
			$("#userLogTrack").table2CSV();
		} else if (exportType == 3) {			
			window.location = "userLogTrack.do?parameter=generateViewUserLogReportInPdf";			
		} else {
			alert('Please Choose Export Type');
			return false;
		}
	
		$('#dialog').dialog('close');
	}	
</script>