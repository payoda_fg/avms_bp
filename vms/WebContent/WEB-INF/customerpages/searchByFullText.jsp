<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.fg.vms.util.SearchFields"%>
<%
	pageContext.setAttribute("searchByOptions", SearchFields.values());
%>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<!-- For Tooltip -->
<script type="text/javascript" src="jquery/ui/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/tooltipster.css" />

<script type="text/javascript">
function printPDFXLS(id)
{	
	$('#vendorID_hdn').val(id);
	$("#exportPDFXLS").css({
		"display" : "block"
	});
	$("#exportPDFXLS").dialog({
		minWidth : 300,
		modal : true
	});
}

function generateSearchGrid(data) 
{
	$('#searchvendorTBody').empty();
	$('#searchvendor').show();
	
	$.each(data,function(i, item) 
	{
		var companyCode = '';
		var region = '';
		var city = '';
		var dunsNumber = '';
		var state = '';
		var companyEmailId='';
		var primeContactEmailId='';
		if (!(undefined == item.companyCode)) {
			companyCode = item.companyCode;
		}
		if (!(undefined == item.dunsNumber)) {
			dunsNumber = item.dunsNumber;
		}
		if (!(undefined == item.region)) {
			region = item.region;
		}
		if (!(undefined == item.city)) {
			city = item.city;
		}
		if (!(undefined == item.state)) {
			state = item.state;
		}
		if(!(undefined==item.companyEmailId)){
			companyEmailId=item.companyEmailId;
		}
		if(!(undefined==item.primeContactEmailId)){
			primeContactEmailId=item.primeContactEmailId;
		}
		$('#searchvendorTBody').append("<tr><td style='text-align: center;'><img src='./images/Print.png' alt='Export' onclick='printPDFXLS("
			+ item.vendorId			
			+ ")'/>"
			+ "</td>"
			+ "<td style='text-align: center;'>"
			+ (item.vendorNotes == "None" ? "None" : "<img src='images/vendor-notes.png' class='tooltip' title='"+ item.vendorNotes +"' />")						
			+ "</td>"
			+"<td><a href='retrievesupplier.do?method=showSupplierContactDetails&id="
			+ item.vendorId
			+ "'>"
			+ item.vendorName
			+ "</a></td> <td>"
			+ companyCode
			+ "</td>"
			+ "<td>"
			+ dunsNumber
			+ "</td> <td>"
			+ item.country
			+ "</td><td>"
			+ (item.naicscode != undefined ? item.naicscode	: '')
			+ "</td> <td>"
			+ region
			+ "</td><td>"
			+ state
			+ "</td> <td>"
			+ city
			+ "</td> <td>"
			+ companyEmailId
			/* + "</td> <td>"
			+ primeContactEmailId */
			+ "</td><td>"
			+ item.invited
			+ "</td>"	
			+"<td sorttable_customkey='MMDDYYYY'>"+item.createdon+"</td>"
			+ "</tr>");
	});
	$('.tooltip').tooltipster();
}

$(document).ready(function() 
{
	$('#menu3').css('display','block');
	<%
		String previousRes="no";
	 	if(session.getAttribute("previousResultStatus")!=null) {
			previousRes=(String)session.getAttribute("previousResultStatus").toString();
	 	}
		if(previousRes.equalsIgnoreCase("yes"))
		{
			session.setAttribute("previousResultStatus", "no");
	%>
			$.ajax({
				url : "viewVendorsStatus.do?method=getPreviousSearchResults&valueFrom=load",
				type : 'POST',
				data : $("#searchform").serialize(),
				async : true,
				dataType : "json",
				success : function(data) {
					generateSearchGrid(data);
				}
			});
	<%
		}
	%>

	$('.tooltip').tooltipster();
});

function searchData() 
{
	if($("#searchFullText").val() != "")
	{
		$.ajax({
			url : "searchfulltext.do?method=getVendorFullSearch",
			type : 'POST',
			data : $("#searchform").serialize(),
			async : true,
			dataType : "json",
			beforeSend : function() {
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show(); //show image loading
			},
			success : function(data) {
				generateSearchGrid(data);				
				$("#ajaxloader").hide();
			}
		});
	} 
	else 
	{
		$("#ajaxloader").hide();
		alert("Please enter value for search");
	}
	return false;
}

function assignValue(value, id) 
{
	$('#' + id).val(value.value);
}

function changestate(ele, id) 
{
	var country = ele.value;
	if (country != null) 
	{
		$.ajax({
			url : 'state.do?method=getState&id=' + country + '&random='	+ Math.random(),
			type : "POST",
			async : false,
			success : function(data) {
				$("#" + id).find('option').remove().end().append(data);
				$('#' + id).trigger("chosen:updated");
			}
		});
	}
}

function exportAllSupplierInformation() 
{
	window.location='viewVendors.do?method=exportAllSupplierInformations';
}
</script>

<style>
	.main-list-box {
		padding: 0% 1%;
	}
	
	.main-table1 td.header {
		background: none repeat scroll 0 0 #009900;
		color: #FFFFFF;
	}
	
	.main-table1 td {
		border: 1px solid #E9EAEA;
		padding: 5px;
	}
	
	tr:nth-child(2n+1) {
		background: none repeat scroll 0 0;
	}
	
	#searchTable td {
		border: 0px solid #E9EAEA;
	}
	
	/* Sortable style starts here */
	/* Modified for Sorting Header*/ 
	.main-table td,th {
		border: 1px solid #e9eaea;
		padding: 5px;
	}
	
	.main-table th.header {
		background: #009900;
		color: #fff;
		cursor: pointer;
	}
	
	/* Sorting Table Header */
	table.sortable th:not(.sorttable_nosort):not(.sorttable_sorted):not(.sorttable_sorted_reverse):after { 
	    content: " \25B4\25BE" 
	}
	/* Sortable style ends here */
</style>

<section role="main" class="content-body card-margin mt-5 pt-5">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">
				<img id="show1" src="images/VendorSearch.gif" /> Keyword</h2>
			</header>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Keyword Search" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<form id="searchform" onsubmit="return searchData();">
				<div class="form-box card-body">
					<ul style="list-style: none;">
						<li>
							<div>
								<label class="desc" for="searchFullText">Full Text Search Content: </label>
								<div>
									<input name="searchFullText" id="searchFullText" class="main-text-box form-control" style="margin-top: 2%;">
								</div>
							</div>
						</li>
						<li><input type="button" value="Search" class="btn btn-primary mt-4" onclick="searchData();"></li>
					</ul>
				</div>
			</form>

			<div class="clear"></div>
			<header class="card-header">
				<h2 class="card-title pull-left">
				<img src="images/icon-registration.png" />Following are Your Vendors</h2>
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
					<input type="button" value="Export" class="btn btn-default pull-right" onclick="exportGrid();">
				</logic:present>
				<input type="button" value="Export All Suppliers Details" class="btn btn-primary pull-right mr-1" onclick="exportAllSupplierInformation();">
			</header>
			
			<div class="form-box card-body" style="overflow: auto;">
				<table id="searchvendor" class="sortable main-table table table-bordered table-striped mb-0" style="display: none;">
					<thead>
						<tr>
							<th class="sorttable_nosort ">Print</th>
							<th class="sorttable_nosort ">Comments</th>
							<th class="">Vendor Name</th>
							<th class="">Company Code</th>
							<th class="">DUNS Number</th>
							<th class="">Country</th>
							<th class="">NAICS Code</th>
							<th class="">Region</th>
							<th class="">State</th>
							<th class="">City</th>
							<th class="">Company Email Id</th>
							<!-- <th class="header">Prime Contact Email Id</th> -->
							<th class="">Mode of Registration</th>
							<th class="">Created On</th>
						</tr>
					</thead>
					<tbody id="searchvendorTBody">
					
					</tbody>
				</table>
			</div>
			
			<!-- Dialog Box to Export PDF, CSV, & XLS of All Vendors Details -->
			<div id="dialog" style="display: none;" title="Choose Export Type">
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel">
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="2" id="csv"> 
							<input type="hidden" name="fileName" id="fileName" value="VendorCriteriaSearch">
						</td>
						<td>
							<label for="csv"><img id="csvExport" src="images/csv_export.png" />CSV</label>
						</td>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				
				<div class="wrapper-btn">
					<logic:present name="userDetails">
						<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
						<input type="button" value="Export" class="exportBtn btn-primary" onclick="exportSearchResultHelper('searchvendor','VendorCriteriaSearch','<%=logoPath%>');">
					</logic:present>
				</div>
			</div>
			
			<!-- Dialog Box to Approve Vendors -->
			<div id="dialog1" style="display: none;" title="Approve Vendor">
				<table cellpadding="10" cellspacing="10">
					<tr></tr>
					<tr>
						<td>Status</td>
						<td>
							<input type="hidden" id="vendorid" /> 
							<select name="changeVendorStatus" class="chosen-select" id="changeVendorStatus1"></select>
						</td>
					</tr>
					<tr>
						<td align="right">Comments:</td>
						<td><textarea cols="50" rows="5" name="isApprovedDesc" id="isApprovedDesc"></textarea></td>
					</tr>
				</table>
				<div class="btn-wrapper">
					<input type="button" class="btn" onclick="getSelectedIds();" value="Submit">
				</div>
				<div id="ajaxloader" style="display: none;">
					<img src="images/ajax-loader.gif" alt="Ajax Loading Image" />
				</div>
			</div>
			
			<!-- Dialog Box to Export PDF & XLS of One Vendors Complete Registration Informations -->
			<div id="exportPDFXLS" style="display: none;" title="Choose Export Type">
				<input type="hidden" id="vendorID_hdn"/>
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel">
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>						
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				
				<div class="wrapper-btn text-center">
					<logic:present name="userDetails">
						<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
						<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportVendorDetailsHelper();">
					</logic:present>
				</div>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Keyword Search" name="privilege" property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box">
				<h3 align="center">You Have No Rights to Search the Vendors</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script>
	function showStatusDesc(statusId) 
	{
		var status = statusId;
		var statusdesc = "";
		
		<logic:present  property="vendorStatusList"  name="searchVendorForm">
			<bean:size id="size" property="vendorStatusList" name="searchVendorForm" />
			<logic:greaterEqual value="0" name="size">
				<logic:iterate id="vendorStatus" property="vendorStatusList" name="searchVendorForm">
					if (status == '<bean:write name="vendorStatus" property="id" />') {
						statusdesc = '<bean:write name="vendorStatus" property="statusdescription" />';
					}
				</logic:iterate>
			</logic:greaterEqual>
		</logic:present>
		
		if (statusdesc != "") {
			document.getElementById('statusDescArea').style.display = "block";
			document.getElementById("statusdesc").value = statusdesc;
		} else {
			document.getElementById('statusDescArea').style.display = "hidden";
		}
	}
</script>