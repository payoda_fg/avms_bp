<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!-- For JQuery Panel -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

		$('.panelCenter_1').panel({
			collapsible : false
		});
	});

	function backToCustomer() {
		window.location = "retrivecustomer.do?method=retriveCustomerInfoFromCustomerDB";
	}
//-->
</script>

<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>
	
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">
					<img src="images/Edit-Customer.gif" alt="" />Edit Customer</h2>
			</header>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Edit Information" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box card-body">
				<html:form enctype="multipart/form-data" styleClass="FORM"
					action="/updatecustomer.do?method=updateCustomerInfoByCustomer"
					styleId="editcustomerform">
					<html:javascript formName="editcustomer" />
					<div id="demoTab" class="card-body">
						<ul class="resp-tabs-list">
							<li><a href="#tabs-1" style="color: #fff;" tabindex="1">Update
									Customer Information</a></li>
							<li><a href="#tabs-2" style="color: #fff;" tabindex="18">Customer
									Contact Information</a></li>
							<li><a href="#tabs-3" style="color: #fff;" tabindex="32">Personalize
									your profile</a></li>
						</ul>
						<div class="resp-tabs-container">
							<div id="tabs-1">
								<%@include file="editcustomerregister.jsp"%>
							</div>
							<div id="tabs-2">
								<%@include file="editcontact.jsp"%>
							</div>
							<div id="tabs-3">
								<%@include file="editpersonalizedprofile.jsp"%>
							</div>
						</div>
					</div>
					<logic:iterate id="privilege1" name="privileges">
						<logic:match value="Edit Information" name="privilege1"
							property="objectId.objectName">
							<logic:match value="1" name="privilege1" property="modify">
								<footer class="mt-2">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
									<html:submit value="Update" styleClass="btn btn-primary" styleId="submit1"></html:submit>
									<html:reset value="Cancel" styleClass="btn btn-default"
										onclick="backToCustomer();"></html:reset>
								</div>
								</div>
								</footer>
							</logic:match>
						</logic:match>
					</logic:iterate>
					<logic:iterate id="privilege2" name="privileges">
						<logic:match value="Edit Information" name="privilege2"
							property="objectId.objectName">
							<logic:match value="0" name="privilege2" property="modify">
								<div style="padding: 3%; text-align: center;">
									<h3>You have no rights to modify</h3>
								</div>
							</logic:match>
						</logic:match>
					</logic:iterate>
				</html:form>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Edit Information" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
	$(function($) {
		$("#phone").mask("(999) 999-9999?");
		$("#contactPhone").mask("(999) 999-9999?");
		$("#contactMobile").mask("(999) 999-9999?");
		$("#mobile").mask("(999) 999-9999?");

		$("#fax").mask("(999) 999-9999?");
		$("#contactFax").mask("(999) 999-9999?");
	});
	$(function() {
		$('#demoTab').easyResponsiveTabs();

		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
		}, "No Special Characters Allowed.");

		jQuery.validator.addMethod("onlyNumbers", function(value, element) {
			return this.optional(element) || /^[0-9]+$/.test(value);
		}, "Please enter only numbers.");
		
		jQuery.validator.addMethod("numberRange", function(value, element) {
			return this.optional(element) || value > 59;
		}, "Please Enter Minutes 60 & Above.");

		jQuery.validator.addMethod("login", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9]+$/.test(value)
		}, "No Special Characters Allowed.");

		jQuery.validator
				.addMethod(
						"password",
						function(value, element) {
							return this.optional(element)
									|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/)
											.test(value);
						},
						"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

		jQuery.validator.addMethod("allowhyphens", function(value, element) {
			return this.optional(element) || /^[0-9-]+$/.test(value);
		}, "Please enter valid numbers.");

		$("#editcustomerform")
				.validate(
						{
							rules : {
								companyname : {
									required : true
								},
								companycode : {
									required : true,
									maxlength : 16
								},
								dunsnum : {
									required : true,
									allowhyphens : true
								},
								taxid : {
									allowhyphens : true
								},
								address1 : {
									required : true,
									maxlength : 125
								},
								city : {
									required : true,
									maxlength : 60
								},
								state : {
									required : true,
									maxlength : 60
								},
								province : {
									alpha : true,
									maxlength : 60
								},
								region : {
									maxlength : 60
								},
								country : {
									required : true
								},
								zipcode : {
									required : true,
									allowhyphens : true
								},
								phone : {
									required : true
								},
								webSite : {
									url : true
								},
								email : {
									maxlength : 120,
									email : true
								},
								firstName : {
									required : true
								},
								lastName : {
									rangelength : [ 3, 60 ]
								},
								designation : {
									rangelength : [ 1, 60 ]
								},
								contactPhone : {
									required : true
								},
								contanctEmail : {
									required : true,
									email : true,
									maxlength : 120
								},
								loginpassword : {
									required : true,
									password : true
								},
								confirmPassword : {
									required : true,
									equalTo : "#loginpassword"
								},
								userSecQn : {
									required : true
								},
								userSecQnAns : {
									required : true,
									rangelength : [ 3, 15 ]
								},
								companyLogo : {
									accept : "jpg|bmp|gif|tiff|exif|png|webp"
								},
								fiscalStartDate : {
									required : true
								},
								fiscalEndDate : {
									required : true
								},
								spendDataUploadFrequency : {
									required : true
								},
								sessionTimeout : {
									required : true,
									onlyNumbers : true,
									numberRange : true
								},
								termsCondition : {
									required : true,
									maxlength : 60000
								},
								privacyTerms : {
									required : true,
									maxlength : 60000
								},
								primeTrainingUrl : {
									required : true,
									maxlength : 250
								},
								nonPrimeTrainingUrl : {
									required : true,
									maxlength : 250
								}
							},
							ignore : "ui-tabs-hide",
							submitHandler : function(form) {
								if (checkDate('slaStartDate', 'slaEndDate')) {
									$("#ajaxloader").css('display', 'block');
									$("#ajaxloader").show();
									form.submit();
								}
							},
							invalidHandler : function(form, validator) {
								var errors = validator.numberOfInvalids();
								if (errors) {
									var invalidPanels = $(
											validator.invalidElements())
											.closest(".resp-tab-content", form);

									if (invalidPanels.size() > 0) {

										$
												.each(
														$.unique(invalidPanels
																.get()),
														function() {
															if ($(window)
																	.width() > 650) {
																$(
																		"a[href='#"
																				+ this.id
																				+ "']")
																		.parent()
																		.not(
																				".resp-accordion")
																		.addClass(
																				"error-tab-validation")
																		.show(
																				"pulsate",
																				{
																					times : 3
																				});
															} else {
																$(
																		"a[href='#"
																				+ this.id
																				+ "']")
																		.parent()
																		.addClass(
																				"error-tab-validation")
																		.show(
																				"pulsate",
																				{
																					times : 3
																				});
															}
														});
										/* $('input[type="text"]').each(function() {
											if ($(this).attr(
													'alt') == 'Optional'
													&& $(this)
															.attr(
																	'value') == '') {
												this.value = 'Optional';
											}
										}); */
									}
								}
							},
							unhighlight : function(element, errorClass,
									validClass) {

								$(element).removeClass(errorClass);
								$(element.form).find(
										"label[for=" + element.id + "]")
										.removeClass(errorClass);
								var $panel = $(element).closest(
										".resp-tab-content", element.form);
								if ($panel.size() > 0) {
									if ($panel.find(
											"." + errorClass + ":visible")
											.size() > 0) {
										if ($(window).width() > 650) {
											$("a[href='#" + $panel[0].id + "']")
													.parent()
													.removeClass(
															"error-tab-validation");
										} else {
											$("a[href='#" + $panel[0].id + "']")
													.parent()
													.removeClass(
															"error-tab-validation");
										}
									}
								}
							}
						});
	});
</script>