<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>


<layout:row>
	<layout:column>
		<layout:line>
			<layout:text property="rfiNumber" key="RFI Number"></layout:text>
			<layout:space></layout:space>
			<layout:text property="contactPerson" key="Conatct Person"></layout:text>
			<layout:space></layout:space>
			<layout:text property="rfiStartDate" key="RFI Start Date"></layout:text>
			<layout:space></layout:space>

		</layout:line>
		<layout:space></layout:space>
		<layout:space></layout:space>
		<layout:line>
			<layout:text property="phoneNumber" key="Phone Number"></layout:text>
			<layout:space></layout:space>
			<layout:text property="emailId" key="E-Mail ID"></layout:text>
			<layout:space></layout:space>
			<layout:text property="rfiEndDate" key="RFI End Date"></layout:text>
			<layout:space></layout:space>
		</layout:line>
	</layout:column>
</layout:row>
<layout:space></layout:space>
<layout:space></layout:space>
<layout:row>
	<layout:column>
		<layout:select property="vendors" key="Select vendors" multiple="true"
			styleId="vendorList">

			<layout:options property="vendor"></layout:options>

		</layout:select>
		
		<layout:space></layout:space>
		<layout:text property="currency" key="Currency"></layout:text>
	</layout:column>
	<layout:column>

		<layout:textarea property="rfiDescription" key="RFI Description"></layout:textarea>
	</layout:column>
</layout:row>

<layout:space></layout:space>
