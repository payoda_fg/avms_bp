<%@page import="com.fg.vms.admin.model.Users"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%
	String username = "";
	if (session.getAttribute("currentUser") != null) {
		Users user = (Users) session.getAttribute("currentUser");
		username = user.getUserName();
	}
%>
<style>
<!--
textarea.content {
	width: 500px;
	height: 130px;
}

textarea.emailId {
	width: 370px;
	height: 60px;
}

.row-wrapper .label-col-full-wrapper {
	float: left;
	line-height: 25px;
	padding: 0 0 0 2%;
	width: 12%;
}

.row-wrapper .ctrl-col-full-wrapper {
	padding: 0 0 0 0%;
}
-->
</style>
<script type="text/javascript" src="jquery/js/jqueryUtils.js">
	
</script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/additional-methods.js"></script>
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	var selectedId = 0;
	function resetForm() {
		window.location = "searchvendor.do?method=searchVendor";
	}
	function getMailIds() {
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#dialog").dialog({
				height : 'auto',
				width: '500px',
				modal : true,
				resizable : false,
				autoResize : false,
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});
	}
	function listEmailIds() {
		var distributionIds = [];
		$('input[name=listDistribution]').each(function() {
			if ($(this).is(":checked"))
				distributionIds.push($(this).val());
		});
		
		if(distributionIds.length != 0 && distributionIds != "")
		{
			$.ajax({
				url : "listemaildistribution.do?method=getEmailIds&listDistribution="
						+ distributionIds,
				type : "POST",
				async : false,
				success : function(data) {
					var split = data.split('|');
					$("#to").append($.trim(split[0]));
					$("#cc").val($.trim(split[1]));
					$("#bcc").val($.trim(split[2]));
					$('input[name=listDistribution]').each(function() {
						$(this).attr("checked", false);
					});
					$('#dialog').dialog('close');
				}
			});	
		}
		else
		{
			alert('Please Select Atleast One Distribution List');
		}
		
	}
	$(document).ready(function() {

		$("#generalMailForm").validate({
			errorLabelContainer : ".form_errors",
			//wrapper: "li",
			groups : {
				name : "to cc bcc"
			},
			rules : {
				'to' : {
					require_from_group : [ 1, ".main-text-area" ]
				},
				'cc' : {
					require_from_group : [ 1, ".main-text-area" ]
				},
				'bcc' : {
					require_from_group : [ 1, ".main-text-area" ]
				}
			},
			submitHandler : function(form) { // submit process
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show();
				form.submit();
			}
		});

		$("input:radio[name=emailTemplateId]").click(function() {
			selectedId = $(this).val();
		});

	});

	function showEmailTemplates() {
		$(function() {
			$("#dialog1").css({
				'display' : 'block',
				'font-size' : 'inherit',
				'max-height' : 'auto',
				'overflow' : 'auto'
			});
			$("#dialog1").dialog({
				height : 'auto',
				modal : true,
				resizable : false,
				autoResize : false,
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});
	}

	function getTemplateMessage() {

		if (selectedId != "" && selectedId != 0) {
			$.ajax({
				url : "emailtemplate.do?parameter=getEmailTemplateMessage&id="
						+ selectedId,
				type : "POST",
				async : false,
				success : function(data) {
					//$("#message").val(data);
					tinyMCE.activeEditor.setContent($.trim(data));
					$('#dialog1').dialog('close');
				}
			});
		} else {
			alert("Please choose any template.");
		}
	}

	tinymce.init({
				selector : "textarea#message",
				theme : "modern",
				statusbar : false,
				height : 300,
				width : 650,
				plugins : [
						"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
						"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
						"table contextmenu directionality emoticons template textcolor paste fullpage textcolor" ],
				toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | image",
				toolbar2 : "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | inserttime preview | forecolor backcolor",
				menubar : false,
				file_browser_callback: AVMSFileBrowser,
				toolbar_items_size : 'medium'

			});
	function AVMSFileBrowser(field_name, url, type, win) {
		
		   var avmsFileman = '<%=session.getAttribute("imageUrl")%>&type='+type+'&field_name='+field_name;
	
		   tinyMCE.activeEditor.windowManager.open({
		     file: avmsFileman,
		     title: 'Email Template',
		     width: 600, 
		     height: 500,
		     resizable: "yes",
		     plugins: "media",
		     inline: "yes",
		     close_previous: "no"  
		  }, {     window: win,     input: field_name    });
		  return false; 
		}
function Validation()
{
	var toValue=$("#to").val();
	var ccValue=$("#cc").val();
	var bccValue=$("#bcc").val();
	if(toValue=='' && ccValue=='' && bccValue=='')
	{
		alert("Enter atleast one email id in TO/CC/BCC.");
		return false;
	}
}
</script>
<section role="main" class="content-body card-margin mt-5">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<header class="card-header">
	<h2 class="card-title pull-left">
	<img id="show1" src="images/icon-mail-note.gif" />&nbsp;&nbsp;Email Notification</h2>
</header>
<div>
	<p class="form_errors"></p>
</div>
<div class="form-box card-body">
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Mail Notifications" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
				<html:form action="/mailNotification?parameter=mailNotify"
					enctype="multipart/form-data" styleId="generalMailForm">
					<html:javascript formName="mailForm" />
					<div id="successMsg">
						<html:messages id="msg" property="mail" message="true">
							<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
						</html:messages>
					</div>
					<div id="failureMsg">
						<html:messages id="msg" property="mailfailed" message="true">
							<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
						</html:messages>
					</div>
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">To:</label>
							<div class="col-sm-7 ctrl-col-full-wrapper">
								<logic:present name="emailAddress">
									<bean:define id="emailAddress" name="emailAddress"></bean:define>
									<span>Enter email addresses
										separate by semicolon ';'</span>
									<html:textarea property="to" styleClass="main-text-area form-control"
										 value="<%=emailAddress.toString()%>"
										cols="40" rows="3" styleId="to">
										<html:errors property="to" />
									</html:textarea>
								</logic:present>
							</div>
							<table style="width:100%;">
								<tr>
									<td>
										<!-- <img style="cursor: pointer;" src="images/search_icon.png"
								onclick="searchVendor();"> -->
									</td>
								</tr>
								
								<tr>								
									
									<td class="pt-2">
										<div class="col-sm-3"></div>
										<img style="cursor: pointer;" src="images/search_icon.png" class="text-left pull-left" onclick="searchVendor();"/>
										<span style="color: #009900;" class="text-left pull-left pr-2"> 
											<a style="cursor: pointer;" onclick="searchVendor();">Search Vendor List</a>
										</span>
									
										<img style="cursor: pointer;" src="images/emaildistributionlist1.jpg" class="text-left pull-left" onclick="getMailIds();">
										<span style="color: #009900;" class="text-left pull-left"> 
											<a style="cursor: pointer;" onclick="getMailIds();">Email Distribution List</a>
										</span>
									</td>
								</tr>
							</table>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">CC:</label>
							<div class="col-sm-7 ctrl-col-full-wrapper">
								<logic:present name="cc">
									<bean:define id="cc" name="cc"></bean:define>
									<html:textarea property="cc" styleClass="main-text-area form-control"
										name="mailForm" cols="70" styleId="cc"
										value="<%=cc.toString()%>" rows="3">
									</html:textarea>
								</logic:present>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">BCC:</label>
							<div class="col-sm-7 ctrl-col-full-wrapper">
								<logic:present name="bcc">
									<bean:define id="bcc" name="bcc"></bean:define>
									<html:textarea property="bcc" styleClass="main-text-area form-control"
										name="mailForm" cols="70" styleId="bcc"
										value="<%=bcc.toString()%>" rows="3">
									</html:textarea>
								</logic:present>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Attachment :</label>
							<div class="col-sm-7 ctrl-col-full-wrapper">
								<html:file property="attachment" styleId="attachment" styleClass="form-control"></html:file>
							</div>
							<span style="color: red;"><html:errors
									property="attachment" /></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Subject:</label>
							<div class="col-sm-5 ctrl-col-full-wrapper">
								<html:text property="subject" name="mailForm" size="100" alt=""
									styleClass="main-text-box form-control" styleId="subject"></html:text>
							</div>
							<div class="col-sm-2">
								<input type="button" value="Select Email Template" class="btn btn-primary"
								onclick="showEmailTemplates();"> <span
								style="color: red;"><html:errors property="subject" /></span>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mail Content:</label>
							<div class="ctrl-col-full-wrapper">
								<html:textarea property="message" styleId="message"
									styleClass="main-text-area form-control" rows="7"
									></html:textarea>
							</div>
							<span style="color: red;"><html:errors property="message" />
							</span>
							<!-- <input type="button" value="Add Email Template" class="btn" onclick="showEmailTemplates();"> -->
						</div>
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<html:submit value="Send Email" styleClass="btn btn-primary" styleId="submit1" onclick="return Validation();"></html:submit>
						<html:reset value="Cancel" styleClass="btn btn-default" styleId="submit"
							onclick="resetForm();"></html:reset>
						</div>
						</div>
					</footer>
					<div id="dialog" title="Choose Email Distribution"
						style="display: none;" class="mt-5">
						<logic:iterate id="privilege1" name="privileges">
							<logic:match value="Email Distribution" name="privilege1"
								property="objectId.objectName">
								<logic:match value="1" name="privilege1" property="view">
									<logic:notEmpty name="distributionModels">
										<table id="emailDistributionListTable" class="main-table table table-bordered table-striped mb-0 mt-2">
											<thead>
												<tr>
													<td>Email Distribution List</td>
													<td>Select</td>
												</tr>
											</thead>
											<tbody>
												<logic:iterate name="distributionModels"
													id="emailDistributionList">
													<tr>
														<td><bean:write name="emailDistributionList"
																property="emailDistributionListName" /></td>
														<td><input type="checkbox" name="listDistribution"
															value="<bean:write name="emailDistributionList" property="emailDistributionMasterId" />">
														</td>
													</tr>
												</logic:iterate>
											</tbody>
										</table>
										<div class="btn-wrapper mt-2">
											<input type="button" value="Add" class="btn btn-primary"
												onclick="listEmailIds();">
										</div>
									</logic:notEmpty>
								</logic:match>
							</logic:match>
						  </logic:iterate>
						<logic:empty name="distributionModels">
							<p>There is no Email Distribution List available..</p>
						</logic:empty>
					</div>
				</html:form>
				
				<div class="clear"></div>
				<logic:present property="mailNotificationDetails" name="mailForm">
					<div class="form-box card-body">
						<table border="0" class="main-table table table-bordered table-striped mb-0"
							style="border-collapse: collapse;">
							<tr>
								<td>Sender Name</td>
								<td>Email Id</td>
								<td>Mail Sent Date</td>
								<td>Mail Subject</td>
							</tr>
							<logic:iterate property="mailNotificationDetails" name="mailForm"
								id="list">
								<tr></tr>
								<tr>
									<td><%= username %></td>
									<td><bean:write name="list" property="vendorEmailId" /></td>
									<td><bean:write name="list"
											property="vendorEmailNotificationMaster.emailDate" /></td>
									<td><bean:write name="list"
											property="vendorEmailNotificationMaster.emailSubject" /></td>
								</tr>
							</logic:iterate>
						</table>
					</div>
				</logic:present>
			</logic:match>
			
		</logic:match>
	</logic:iterate>
	
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Mail Notifications" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div class="form-box">
					<h3>You have no rights to view Email notification</h3>
				</div>
			</logic:match>
		</logic:match>
	</logic:iterate>
</div>
<div id="dialog1" style="display: none;" title="Email Template List">
	<table class="main-table">
		<logic:iterate name="emailTemplates" id="templatelist">
			<tr class="even">
				<td><bean:write name="templatelist"
						property="emailTemplateName" /></td>
				<bean:define id="emailTemplateId" name="templatelist" property="id"></bean:define>
				<td><input type="radio" name="emailTemplateId"
					value="${emailTemplateId}"></td>
			</tr>
		</logic:iterate>
	</table>
	<div class="btn-wrapper">
		<input type="button" value="Add" class="btn"
			onclick="getTemplateMessage();">
	</div>
</div>
<form style="display: hidden"
	action="searchvendor.do?method=showMailNotificationPage" method="POST"
	id="form">
	<input type="hidden" id="var1" name="to" /> <input type="hidden"
		id="var2" name="cc" /> <input type="hidden" id="var3" name="bcc" />
</form>
</div>
</div>
</section>