<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>


<div id="table-holder2"
	style="margin: 0 1.5%; padding: 20px 0 10px 90px; height: 150px;">
	<div>
		<div style="float: left;">
			NAICS Category&nbsp;&nbsp;&nbsp;
			<html:select property="category" styleId="naicsCategoryId"
				onchange="ajaxFnSubCategory(this.value,'subCategory1','subCategory','onchange=showAllVendorsByCategory(this.value)');"
				styleClass="selectBox">
				<bean:size id="size" name="rfiInformation" property="categories" />
				<logic:greaterEqual value="0" name="size">
					<html:option value="0" key="select">-----Select-----</html:option>
					<logic:iterate id="category" name="rfiInformation"
						property="categories">
						<bean:define id="id" name="category" property="id"></bean:define>
						<bean:define id="naicsCategoryDesc" name="category"
							property="naicsCategoryDesc"></bean:define>
						<html:option value="<%=id.toString() %>">
							<bean:write name="category" property="naicsCategoryDesc" />
						</html:option>
					</logic:iterate>
				</logic:greaterEqual>
			</html:select>
			NAICS Sub-Category
		</div>
		<div style="padding-left: 350px;" id="subCategory1">
			
			<html:select property="subCategory" styleId="subCategory"
				onchange="showAllVendorsByCategory(this.value);"
				styleClass="selectBox">
				<html:option value="-1" key="select">-----Select-----</html:option>

			</html:select>
		</div>
	</div>
	<div style="margin: 0 1.5%; padding: 20px 0 10px 90px;">
		<div id="vendors" style="float: left">
			<html:select property="allVendors" multiple="true"
				styleId="allvendors" style="width:210px;">

			</html:select>
		</div>
		<div
			style="float: left; padding-left: 15px; padding-right: 15px; margin-top: 10px;">
			<a href="#"
				onclick="listbox_moveacross('allvendors', 'selectedvendors')"><img
				src="images/arrow-1.png" width="16" height="16" border="0" /></a><br />
			<a href="#"
				onclick="listbox_moveacross('selectedvendors', 'allvendors')"><img
				src="images/arrow-2.png" alt="" width="16" height="16" border="0" /></a>
		</div>
		<div style="float: left">
			<html:select property="vendors" multiple="true"
				styleId="selectedvendors" style="width:210px;">

			</html:select>
		</div>
	</div>


</div>