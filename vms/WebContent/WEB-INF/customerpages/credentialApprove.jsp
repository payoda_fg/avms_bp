<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<style type="text/css">
	/* Modified for Sorting Header*/ 
	.main-table td,th {
		border: 1px solid #e9eaea;
		padding: 5px;
	}
	
	.main-table th.header {
		background: #009900;
		color: #fff;
		cursor: pointer;
	}
	
	/* Sorting Table Header */
	table.sortable th:not(.sorttable_nosort):not(.sorttable_sorted):not(.sorttable_sorted_reverse):after { 
	    content: " \25B4\25BE" 
	}
</style>

<script>
	var displayApproval = "no";
	var isApproved = "";

	$(document).ready(function() 
	{
		<%String displayApproval = (String) session.getAttribute("displayApproval");%>
		displayApproval ='<%=displayApproval%>';
		if(displayApproval == "yes")
			$("#credentialForm").show('true');
	});

	function validation()
	{
		if(isApproved != "Approved")
		{
			if(isApproved != "UnApproved")
			{
				alert("Please choose approve or unapprove.");
				return false;
			}
			else
			{
				return true;	
			}
		}
		else
		{
			return true;	
		}
	}
	
	function selectFunction(status)
	{
		isApproved = status;
	}
	
	function updateApproveOrUnapproveStakeHolder()
	{
		var stakeId = $('#stakeId').val();
	    var status = validation();
	    if(status == true)
	    {
	    	var ajaxUrl = "customeruserregistration.do?method=updateApproveOrUnapproveStakeHolder&isApprove=" + isApproved + "&stackHolder=" + stakeId;
	    	$.ajax(
   			{
 				url : ajaxUrl,
 				type : "POST",
 				async : true,
 				beforeSend : function()
 				{
 					$("#ajaxloader").css('display', 'block');
 					$("#ajaxloader").show();
 				},
				success : function(data) 
	    		{
	    			if(data.result == "approvedsuccess")
	    			{
	    				alert("Login credentials sent successfully to approved user.");
	    			}
	    			else if(data.result == "unapprovedsuccess")
	    			{
	    				alert("User Unapproved Successfully.");
	    			}
	    			else if(data.result == "templateNotFound")
	    			{
	    				alert("Email template not found please contact admin.");
	    			}
	    			else
	    			{
	    				alert("Approval process failed.");
	    			}
	    			$("#ajaxloader").hide();
	    			window.location.href = 'customeruserregistration.do?method=viewNotApprovedStakeholders'; 
    			} 
    		});
	    }
	    else
	    {
	    	return status;	
	    }
	}
	
	function cancel()
	{
		window.location = "customeruserregistration.do?method=viewNotApprovedStakeholders";
	}
</script>
<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-12 mx-auto">	
			<section class="card">
				<header class="card-header">
					<h2 class="card-title pull-left">Credentialing</h2>
				</header>

<div class="form-box card-body" style="display: none;" id="credentialForm">
	<html:form styleClass="FORM">
		<html:javascript formName="userForm" />
		<html:hidden property="stackHolder" name="userForm" styleId="stakeId"/>		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="stackHolderFirstName" styleClass="text-box form-control" name="userForm" readonly="true"></html:text>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="stackHolderLastName" styleClass="text-box form-control" name="userForm" readonly="true"></html:text>
				</div>
			</div>
		
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email Address:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="stackHolderEmailId" styleClass="text-box form-control" name="userForm" readonly="true"></html:text>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Manager Name:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="stackHolderManagerName" styleClass="text-box form-control" name="userForm" readonly="true"></html:text>
				</div>
			</div>
						
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Reason:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:textarea property="stackHolderReason" rows="5" cols="30" styleClass="main-text-area form-control" 
						styleId="stackHolderReason" readonly="true" name="userForm"></html:textarea>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is Approvable?</label>
				<div class="col-sm-9 	ctrl-col-wrapper" id="status">
					<div class="radio-custom radio-primary">
					<html:radio property="isApprove" value="Approved" name="userForm"
						styleId="isApproveId" onchange="selectFunction('Approved')">&nbsp;Approve&nbsp;</html:radio>
						<label for=""></label>
					</div>
					<div class="radio-custom radio-primary">
					<html:radio property="isApprove" value="UnApproved"
						name="userForm" styleId="isApproveId" onchange="selectFunction('UnApproved')">&nbsp;Not Approve&nbsp;</html:radio>
						<label for=""></label>
					</div>
				</div>
			</div>			
				
			<div class="row-wrapper form-group row">			
				<logic:equal value="1" name="isDivisionStatus" property="isDivision">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Customer Division:</label>
					<div class="col-sm-9 ctrl-col-wrapper">
						<html:text property="division" styleClass="text-box form-control" name="userForm" readonly="true"></html:text>
					</div>
				</logic:equal>				
			</div>
		
		<footer class="mt-2 card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
					<input type="button" value="Send Credentials" class="btn btn-primary"
					id="submit1" onclick="return updateApproveOrUnapproveStakeHolder();">
					<input type="button" value="Cancel" class="btn btn-default" onclick="cancel();">
				</div>			
			</div>
		</footer>
	</html:form>
</div>

<div class="form-box card-body">
	<div class="responsiveGrid">
	<table class="sortable main-table table table-bordered table-striped mb-0">		
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Manager Name</th>
				<th>Email Id</th>
				<th class="sorttable_nosort">Is Approved?</th>
				<th class="sorttable_nosort">Approval</th>
			</tr>
		</thead>
		<tbody>
			<bean:size id="size" name="notApprovedStakeHolders" />
			<logic:greaterThan value="0" name="size">
				<logic:iterate name="notApprovedStakeHolders" id="notApprovedList">
					<tr>
						<td><bean:write name="notApprovedList" property="firstName" /></td>
						<td><bean:write name="notApprovedList" property="lastName" /></td>
						<td><bean:write name="notApprovedList" property="managerName" /></td>
						<td><bean:write name="notApprovedList" property="emailId" /></td>
						<td><bean:write name="notApprovedList" property="isApproved" /></td>
						<bean:define id="stakeHolderId" name="notApprovedList" property="id"/>
						<td align="center">
							<html:link action="/customeruserregistration.do?method=editNotApprovedStakeHolders"
								paramId="stakeHolderId" paramName="stakeHolderId"> Approvable </html:link>
							<logic:equal value="${stakeHolderId}" property="stackHolder" name="userForm">
								| <span style="color: green; font-weight: bold;">Credential in Edit</span>
							</logic:equal>
						</td>
					</tr>
				</logic:iterate>
			</logic:greaterThan>
			<logic:equal value="0" name="size">
				<tr>
					<td colspan="6">No records are found...</td>
				</tr>
			</logic:equal>
		</tbody>		
	</table>
	</div>
</div>
</section>
</div>
</div>
</section>