<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>


<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>

<script type="text/javascript">
	function clearFields() {
		window.location = "viewnaicsmaster.do?parameter=viewNAICSMasterPage";
	}

	$(document).ready(function() {

		$("#formID").validate({
			rules : {
				naicsCategoryId : {
					required : true
				},
				subCategory : {
					required : true
				},
				naicsCode : {
					required : true,
					maxlength : 6,
					number : true
				},
				naicsDescription : {
					required : true
				},
				isicCode : {
					required : true
				},
				isicDescription : {
					required : true
				}
			}
		});

		$('#naicsCodeTable').tableScroll({
			height : 150
		});
	});
</script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 570px; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;NAICS
				Entry
			</h2>
		</div>
		<div id="successMsg">
			<html:messages id="msg" property="master" message="true">
				<span style="color: green;"><bean:write name="msg" /></span>
			</html:messages>
		</div>
		<div id="table-holder"
			style="margin: 0 1.7% 1% 1.7%; float: left; width: 96.5%; padding-bottom: 30px;">
			<logic:iterate id="privilege" name="privileges">

				<logic:match value="NAICS Master" name="privilege"
					property="objectId.objectName">
					<logic:match value="1" name="privilege" property="add">
						<html:form action="/naicmaster.do?parameter=saveNaicsMaster"
							styleId="formID">
							<html:javascript formName="naicsMasterForm" />

							<div style="float: left;">
								<table cellspacing="10" cellpadding="5"
									style="margin-left: 50px;">
									<tr>
										<td>NAICS Category</td>
										<bean:define id="selectedCateory" name="categoryId"></bean:define>
										<td><bean:define id="category" name="categoryName"></bean:define>
											<html:select property="naicsCategoryId"
												value="<%=selectedCateory.toString() %>"
												styleId="naicsCategoryId" styleClass="selectBox"
												name="naicsMasterForm"
												onchange="ajaxFnSubCategory(this.value,'subCategory1','subCategory','');">
												<bean:size id="size" name="categoryName" />
												<logic:greaterEqual value="0" name="size">
													<html:option value="0" key="select">-----Select-----</html:option>
													<logic:iterate id="categories" name="categoryName">
														<bean:define id="id" name="categories" property="id"></bean:define>
														<bean:define id="naicsCategoryDesc" name="categories"
															property="naicsCategoryDesc"></bean:define>
														<html:option value="<%=id.toString() %>">
															<bean:write name="categories"
																property="naicsCategoryDesc" />
														</html:option>
													</logic:iterate>
												</logic:greaterEqual>
											</html:select></td>
										<td><span class="error"><html:errors
													property="naicsCategoryId"></html:errors></span></td>
									</tr>
									<tr>
										<td>NAICS Sub-Category</td>

										<td><div id="subCategory1">
												<html:select property="subCategory" styleId="subCategory"
													styleClass="selectBox" name="naicsMasterForm">
													<html:option value="" key="select">-----Select-----</html:option>

												</html:select>
											</div></td>
										<td><span class="error"><html:errors
													property="naicsSubCategoryId"></html:errors></span></td>
									</tr>
									<tr>
										<td>NAICS Code</td>
										<td><html:text property="naicsCode"
												name="naicsMasterForm" alt="" styleId="naicsCode"
												onchange="ajaxFn(this,'NC')" /></td>
										<td><span class="error"><html:errors
													property="naicsCode"></html:errors></span></td>
									</tr>
									<tr>
										<td>NAICS Description</td>
										<td><html:text property="naicsDescription"
												name="naicsMasterForm" alt="" /></td>
										<td><span class="error"><html:errors
													property="naicsDescription"></html:errors></span></td>
									</tr>
								</table>
							</div>
							<div >
								<table cellspacing="10" cellpadding="5"
									style="margin-left: 45%;">
									<tr>
										<td>ISIC Code</td>
										<td><html:text property="isicCode" name="naicsMasterForm"
												alt="" /></td>
										<td><span class="error"><html:errors
													property="isicCode"></html:errors></span></td>
									</tr>
									<tr>
										<td>ISIC Description</td>
										<td><html:text property="isicDescription"
												name="naicsMasterForm" alt="" /></td>
										<td><span class="error"><html:errors
													property="isicDescription"></html:errors></span></td>
									</tr>
									<tr>
										<td>IsActive&nbsp;</td>
										<td><html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
											<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
										</td>
									</tr>
								</table>


								<div style="padding: 30px 0 0 80px;">
									<html:submit value="Submit" styleClass="customerbtTxt"
										styleId="submit"></html:submit>
									<html:reset value="Clear" styleClass="customerbtTxt"
										onclick="clearFields()"></html:reset>
								</div>
							</div>

						</html:form>
					</logic:match>
				</logic:match>
			</logic:iterate>

			<logic:iterate id="privilege" name="privileges">
				<logic:match value="NAICS Master" name="privilege"
					property="objectId.objectName">
					<logic:match value="0" name="privilege" property="add">
						<div style="padding: 5%; text-align: center;">
							<h3>You have no rights to add new NAICS code</h3>
						</div>
					</logic:match>
				</logic:match>
			</logic:iterate>

		</div>

		<logic:iterate id="privilege" name="privileges">

			<logic:match value="NAICS Master" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="view">

					<div class="toggleFilter">
						<h2 id="show2"
							style="float: left; margin: 1% 1.5%; width: 97%; cursor: pointer;">
							<img id="showx" src="images/arrow-down.png" />&nbsp;&nbsp;Search
							NAICS Records
						</h2>
					</div>

					<div id="table-holder2"
						style="float: left; padding: 10px 5px 10px 5px; margin: 1% 1.5%; width: 95.5%;">
						<html:form action="/viewnaicsmaster.do?parameter=search">
							<%
								String categoryName = session.getAttribute(
														"categoryName").toString();
							%>

							<table width="100%">
								<tr>
									<td>NAICS Category</td>
									<bean:define id="selectedCateory" name="searchCategoryId"></bean:define>
									<td><bean:define id="category" name="categoryName"></bean:define>
										<html:select property="naicsCategoryId"
											value="<%=selectedCateory.toString() %>"
											styleId="searchCategoryId" styleClass="selectBox"
											onchange="ajaxFnSubCategory(this.value,'subCategory2','naicsSubCategoryID','');">
											<bean:size id="size" name="categoryName" />
											<logic:greaterEqual value="0" name="size">
												<html:option value="0" key="select">-----Select-----</html:option>
												<logic:iterate id="categories" name="categoryName">
													<bean:define id="id" name="categories" property="id"></bean:define>
													<bean:define id="naicsCategoryDesc" name="categories"
														property="naicsCategoryDesc"></bean:define>
													<html:option value="<%=id.toString() %>">
														<bean:write name="categories" property="naicsCategoryDesc" />
													</html:option>
												</logic:iterate>
											</logic:greaterEqual>
										</html:select></td>
									<td>NAICS Sub-Category</td>

									<td><div id="subCategory2">
											<html:select property="naicsSubCategoryID"
												styleId="naicsSubCategoryID" styleClass="selectBox">
												<html:option value="0" key="select">-----Select-----</html:option>
												<%
													if (session.getAttribute("searchSubcategories") != null) {
												%>
												<bean:define id="category" name="searchSubcategories"></bean:define>
												<bean:size id="size" name="searchSubcategories" />
												<logic:greaterEqual value="0" name="size">

													<logic:iterate id="categories" name="searchSubcategories">
														<bean:define id="id" name="categories" property="id"></bean:define>
														<bean:define id="naicSubCategoryDesc" name="categories"
															property="naicSubCategoryDesc"></bean:define>
														<html:option value="<%=id.toString() %>">
															<bean:write name="categories"
																property="naicSubCategoryDesc" />
														</html:option>
													</logic:iterate>
												</logic:greaterEqual>
												<%
													}
												%>

											</html:select>
										</div></td>
									<td><html:submit value="Search" styleClass="customerbtTxt"></html:submit></td>
								</tr>
							</table>

						</html:form>

					</div>

					<div id="box"
						style="padding-top: 10px; margin-left: 1.5%; float: left;">
						<logic:present name="masters">
							<table width="96%" id="naicsCodeTable">
								<bean:size id="size" name="masters" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<th>NAICS Code</th>
											<th>NAICS Description</th>
											<th>ICIS Code</th>
											<th>ICIS Description</th>
											<th>NAICS Category</th>
											<th>NAICS Sub-Category</th>
											<th>Actions</th>
										</tr>
									</thead>
									<logic:iterate name="masters" id="naicslist">
										<tbody>
											<tr>
												<td><bean:write name="naicslist" property="naicsCode" /></td>
												<td><bean:write name="naicslist"
														property="naicsDescription" /></td>
												<td><bean:write name="naicslist" property="isicCode" /></td>
												<td width="157"><bean:write name="naicslist"
														property="isicDescription" /></td>
												<td><bean:write name="naicslist"
														property="naicsCategoryId.naicsCategoryDesc" /></td>
												<td><bean:write name="naicslist"
														property="naicsSubCategoryId.naicSubCategoryDesc" /></td>
												<bean:define id="naicsId" name="naicslist" property="id"></bean:define>
												<td><logic:iterate id="privilege" name="privileges">

														<logic:match value="NAICS Master" name="privilege"
															property="objectId.objectName">
															<logic:match value="1" name="privilege" property="modify">
																<html:link paramId="id" paramName="naicsId"
																	action="/retrivenaicsmaster.do?parameter=retrivenaicsmaster">Edit</html:link>
															</logic:match>
														</logic:match>

													</logic:iterate> <logic:iterate id="privilege" name="privileges">

														<logic:match value="NAICS Master" name="privilege"
															property="objectId.objectName">
															<logic:match value="1" name="privilege" property="delete">
																| <html:link
																	action="/deletenaicsmaster.do?parameter=deletenaicsmaster"
																	paramId="id" paramName="naicsId"
																	onclick="return confirm_delete();">Delete</html:link>
															</logic:match>
														</logic:match>
													</logic:iterate></td>
											</tr>
										</tbody>
									</logic:iterate>
								</logic:greaterThan>
								<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
							</table>
						</logic:present>
					</div>

				</logic:match>
			</logic:match>

		</logic:iterate>

		<logic:iterate id="privilege" name="privileges">

			<logic:match value="NAICS Master" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="view">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to view NAICS code</h3>
					</div>
				</logic:match>
			</logic:match>

		</logic:iterate>


	</div>
</div>
<script type="text/javascript">
	$('#naicsCodeTable').width('940px');//Setting dynamic width for table on search
	$('.tablescroll th').css('line-height', '0px');
</script>