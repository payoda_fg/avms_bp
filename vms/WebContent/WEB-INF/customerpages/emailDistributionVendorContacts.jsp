<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<EmailDistribution> distributions = null;
	if (session.getAttribute("vendorContacts") != null) {
		distributions = (List<EmailDistribution>) session
				.getAttribute("vendorContacts");
	}
	EmailDistribution distribution = null;

	if (distributions != null) {
		for (int index = 0; index < distributions.size(); index++) {
			distribution = distributions.get(index);
			cellobj = new JSONObject();
			cellobj.put("id", distribution.getId());
			cellobj.put("name", distribution.getFirstName());
			cellobj.put("emailid", distribution.getEmailId());
			cellobj.put("vendorname", distribution.getVendorName());
			
			if (null != distribution.getPrimrNonPrime()
					&& distribution.getPrimrNonPrime() == 1) {
				cellobj.put("prime", "Prime");
			} else if (null != distribution.getPrimrNonPrime()
					&& distribution.getPrimrNonPrime() == 0) {
				cellobj.put("prime", "Non Prime");
			}
			cellobj.put("vendorstatus", distribution.getVendorStatus());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
