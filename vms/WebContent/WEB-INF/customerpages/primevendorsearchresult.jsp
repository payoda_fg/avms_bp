<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<!-- For Tooltip -->
<script type="text/javascript" src="jquery/ui/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/tooltipster.css" />

<script type="text/javascript">

$(document).ready(function() {
	$('.tooltip').tooltipster();
	writeToLog(new Date());
});


function writeToLog(date) {
	$.ajax(
	{
		url : "viewVendors.do?method=printLog&date="+date,
		type : "POST",
		async : false,
		dataType : "json",
		success : function(data) {}
	});
}

function showSaveFilter() {
	$("#dialog2").css({
		"display" : "block"
	});
	$("#dialog2").dialog({
		minWidth : 400,
		modal : true
	});
	$("#searchName").focus();	
	
	$(document).keypress(function(e) {
		if (e.which == 13 && $("#dialog2").is(':visible')) {
			$(e.target).blur();
			return saveFilter();
		}
	});
}

function saveFilter() {	
	var searchName=$("#searchName").val();
	
	if (searchName != '' && searchName != 'undefined') 
	{
		$.ajax(
		{
			url : "viewVendors.do?method=saveSearchFilters&searchType=P&searchName="+searchName,
			type : "POST",
			async : false,
			dataType : "json",
			beforeSend : function() 
			{
				$("#ajaxloader").show();
			},
			success : function(data) 
			{
				$("#ajaxloader").hide();
				if((data.result)=="success")
				{
					$('#searchName').val('');
					$("#dialog2").dialog("close");
					alert("Search Criterias Successfully Saved.");
					window.location.reload(true);	
				}
				else if((data.result)=="unique")
				{
					alert("Search Name Allready Available.");
				}
				else 
				{
					alert("Search Criteria Failed to Save.");	
				}					
			}
		});
	}
	else
	{
		alert("Please Enter a Name for search");
	}
}

function backToSearch() {
	window.location = "viewVendors.do?method=showPrimeVendorSearch&searchType=P";
}

function printPDFXLS(id) {			
	$('#vendorID_hdn').val(id);
	$("#exportPDFXLS").css({
		"display" : "block"
	});
	$("#exportPDFXLS").dialog({
		minWidth : 300,
		modal : true
	});
}

function exportAllSupplierInformation() 
{
	window.location='viewVendors.do?method=exportAllSupplierInformations';
}
</script>

<style type="text/css">
/* Sortable style starts here */
/* Modified for Sorting Header*/ 
.main-table td,th {
	border: 1px solid #e9eaea;
	padding: 5px;
}

.main-table th.header {
	background: #009900;
	color: #fff;
	cursor: pointer;
}

/* Sorting Table Header */
table.sortable th:not(.sorttable_nosort):not(.sorttable_sorted):not(.sorttable_sorted_reverse):after { 
    content: " \25B4\25BE" 
}
/* Sortable style ends here */	
</style>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Prime Vendor Search" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">		
			<div class="clear"></div>
			<logic:present name="searchCriteria">					
				<bean:size id="size" name="searchCriteria" />
				<logic:greaterThan value="0" name="size">
					<header class="card-header">
						<h2 class="card-title pull-left">
						<img src="images/VendorSearch.gif" />&nbsp;&nbsp; Search Criteria</h2>
						<logic:equal value="1" name="saveCriteria">
							<input type="button" value="Save Filter" class="btn pull-right btn-primary" onclick="showSaveFilter();"/>
						</logic:equal>							
						<div class="form-box text-center" style="overflow: auto;font-weight:bold;">
							<logic:iterate name="searchCriteria" id="criteria">
								<bean:write name="criteria" format="String" />
								<span>,</span>								
							</logic:iterate>			
						</div>
					</header>
				</logic:greaterThan>
			</logic:present>
			
			<header class="card-header">
				<h2 class="card-title pull-left">
					<img src="images/icon-registration.png" />Following are Your Vendors</h2>
				
				
				<input type="button" value="Back" class="btn btn-primary pull-right" onclick="backToSearch();">				
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails" property="settings.logoPath"/>
					<input type="button" value="Export" class="btn btn-tertiary pull-right mr-1" onclick="exportGrid();">
				</logic:present>
				<input type="button" value="Export All Suppliers Details" class="btn btn-primary pull-right mr-1" onclick="exportAllSupplierInformation();">
				<div id="searchSummary" class="text-center" style="color: #009900">
					<logic:present name="vendorSearchSummary">
						<bean:write name="vendorSearchSummary" format="String" />
					</logic:present>
				</div>				
			</header>
			
			<!-- Dialog Box to Export PDF, CSV, & XLS of All Vendors Details -->
			<div id="dialog" style="display: none;" title="Choose Export Type">
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel"/>
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="2" id="csv"/> 
							<input type="hidden" name="fileName" id="fileName" value="PrimeVendorSearch">
						</td>
						<td>
							<label for="csv"><img id="csvExport" src="images/csv_export.png" />CSV</label>
						</td>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				<div class="wrapper-btn text-center">
					<logic:present name="userDetails">
						<bean:define id="logoPath" name="userDetails" property="settings.logoPath"/>
						<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportSearchResultHelper('searchvendor','PrimeVendorSearch','<%=logoPath%>');">
					</logic:present>
				</div>
			</div>
			
			<!-- Dialog Box to Export PDF & XLS of One Vendors Complete Registration Informations -->
			<div id="exportPDFXLS" style="display: none;" title="Choose Export Type">
				<input type="hidden" id="vendorID_hdn"/>
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel">
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>						
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				
				<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<logic:present name="userDetails">
							<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
							<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportVendorDetailsHelper();">
						</logic:present>
						</div>
					</div>
				</footer>
			</div>			
			
			<div class="form-box card-body vendor-search" style="overflow-x:scroll !important;">
				<table id="searchvendor" width="100%" border="0" class="sortable main-table table table-bordered table-striped mb-0">
					<thead>
						<tr>
							<th class="sorttable_nosort" style="padding:0 20px !important;">Print</th>
							<th class="">Vendor Name</th>
							<th class="">Company Code</th>
							<th class="">DUNS Number</th>
							<th class="">Country</th>
							<th class="">NAICS Code</th>
							<th class="">Region</th>
							<th class="">State</th>
							<th class="">City</th>
							<th class="">Company Email Id</th>
							<th class="">Prime Contact Email Id</th>
							<th class="">Mode of Registration</th>
							<th class="">Created On</th>
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="searchVendorsList">
							<logic:iterate name="searchVendorsList" id="vendorList">
								<tr>
									<bean:define id="vendorId" name="vendorList" property="id"/>
									<bean:define id="status" name="vendorList" property="vendorStatus"/>								
									<td>
										<a href="#" alt="Export" class="" title="Print Vendor Profile" onclick="printPDFXLS('${vendorId}')">
										<span>Print</span></a>
										<%-- <img src="./images/Print.png" alt="Export" class="tooltip" title="Print Vendor Profile" onclick="printPDFXLS('${vendorId}')"/> --%>									
									</td>
										
									<td><html:link action="/retrievesupplier.do?method=editPrimeVendor" paramId="id" paramName="vendorId">
											<bean:write name="vendorList" property="vendorName" />
										</html:link>
									</td>
									<td><bean:write name="vendorList" property="vendorCode" /></td>
									<td><bean:write name="vendorList" property="duns" /></td>
									<td><bean:write name="vendorList" property="countryName" /></td>
									<td><bean:write name="vendorList" property="naicsCode" /></td>
									<td><bean:write name="vendorList" property="region" /></td>
									<td><bean:write name="vendorList" property="stateName" /></td>
									<td><bean:write name="vendorList" property="city" /></td>
									<td><bean:write name="vendorList" property="companyEmailId" /></td>
									<td><bean:write name="vendorList" property="primeContactEmailId" /></td>
									<td><bean:write name="vendorList" property="modeOFReg" /></td>
									<td class="sorttable_customkey='MMDDYYYY'"><bean:write name="vendorList" property="created" /></td>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
						<logic:empty name="searchVendorsList">
							<tr>
								<td colspan='13'>No records are found...</td>
							</tr>
						</logic:empty>
					</tbody>
				</table>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Prime Vendor Search" name="privilege" property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box">
				<h3 align="center">You have no rights to search the prime vendor</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<!-- Dialog Box for Save Filter -->
<div id="dialog2" class="form-box" style="display: none;" title="Save Filter">
	<div class="wrapper-full">
		<div class="row-wrapper form-group row">
			<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Search Name :</label>
			<div class="col-sm-9 ctrl-col-wrapper">
				<input type="text" id="searchName" name="searchName" class="text-box form-control" />
			</div>
		</div>
	</div>
	<div class="btn-wrapper text-center">
		<input type="button" class="exportBtn btn btn-primary" onclick="saveFilter();" value="Submit" id="statusButton" />
	</div>
</div>
</div>
</div>
</section>