<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css"/>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<script type="text/javascript" src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>

<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<!-- For Tooltip -->
<script type="text/javascript" src="jquery/ui/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/tooltipster.css" />

<script type="text/javascript">

$(document).ready(function() {	
    $('.tooltip').tooltipster();
    
	writeToLog(new Date());
});

function writeToLog(date) {
	$.ajax(
	{
		url : "viewVendors.do?method=printLog&date="+date,
		type : "POST",
		async : false,
		dataType : "json",
		success : function(data) {}
	});
}

function showSaveFilter(){
	$("#dialog2").css({
		"display" : "block"
	});
	$("#dialog2").dialog({
		minWidth : 400,
		modal : true
	});
	$("#searchName").focus();
	
	$(document).keypress(function(e) {
		if (e.which == 13 && $("#dialog2").is(':visible')) {
			$(e.target).blur();
			return saveFilter();
		}
	});
}
	function saveFilter() {
		
		var permission= '${pendingReview}';
		 
		var searchType='V';
		 if(permission == 1){
			 searchType='VP';
		  }
		var searchName=$("#searchName").val();
		if (searchName != '' && searchName != 'undefined') 
		{
			$.ajax({
				url : "viewVendors.do?method=saveSearchFilters&searchType="+searchType+"&searchName="+searchName,		
				type : "POST",
				async : false,
				dataType : "json",
				beforeSend : function() {
						$("#ajaxloader").show();
				},
				success : function(data) {
					$("#ajaxloader").hide();
					if((data.result)=="success"){
						$('#searchName').val('');
						$("#dialog2").dialog("close");
						alert("Search Criterias Successfully Saved ");
						window.location.reload(true);	
					}
					else if((data.result)=="unique"){
						alert("Search Name Allready Available ");
					}
					else
						alert("Search Criteria Failed to Save ");
				}
			});
		}
		else{
			alert("Please Enter a Name for search");
		}
	}
	function printPDFXLS(id) {
		$('#vendorID_hdn').val(id);
		$("#exportPDFXLS").css({
			"display" : "block"
		});
		$("#exportPDFXLS").dialog({
			minWidth : 300,
			modal : true
		});
	}

	function backToSearch() {
		window.location = "viewVendorsStatus.do?method=showVendorSearch&searchType=V";
	}

	function getVendorStatus(status) {
		if (status != '' && status == 'P') {
			$("#primeRow").css('visibility', 'visible');
		} else {
			$("#primeRow").css('visibility', 'hidden');
		}
	}
	
	function updateStatus(vendorId, status) {
		$('#vendorid').val(vendorId);
		$("#dialog1").css({
			"display" : "block"
		});
		$("#statusButton").prop("disabled", false);
		$("#statusButton").css("background", " none repeat scroll 0 0 #009900");

		$.ajax({
			url : 'status.do?method=getStatus&id=' + status + '&random=' + Math.random(),
			type : "POST",
			async : true,
			beforeSend : function() {
				$("#ajaxloader").show(); //show image loading
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$('#changeVendorStatus1').find('option').remove().end().append(data);
				$('#changeVendorStatus1').trigger("chosen:updated");
			}
		});
		
		$("#dialog1").dialog({
			minWidth : 600,
			height : 300,
			modal : true,
			close : function(event, ui) {
				$('#isApprovedDesc').val('');
				$('#primeRow').css('visibility', 'hidden');
				$('input:radio[name=prime]').prop('checked', false);
			}
		});
	}
	
	function exportAllSupplierInformation() 
	{
		window.location='viewVendors.do?method=exportAllSupplierInformations';
	}
</script>

<style type="text/css">
/* Sortable style starts here */
/* Modified for Sorting Header*/ 
.main-table td,th {
	border: 1px solid #e9eaea;
	padding: 5px;
}

.main-table th.header {
	background: #009900;
	color: #fff;
	cursor: pointer;
}

/* Sorting Table Header */
table.sortable th:not(.sorttable_nosort):not(.sorttable_sorted):not(.sorttable_sorted_reverse):after { 
    content: " \25B4\25BE" 
}
/* Sortable style ends here */	
</style>

<section role="main" class="content-body card-margin mt-5 pt-5">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="privileges">
	<logic:match value="View Vendors" name="privilege" property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="clear"></div>
			<logic:present name="searchCriteria">
				<bean:size id="size" name="searchCriteria" />
				<logic:greaterThan value="0" name="size">
					<header class="card-header">
						<h2 class="card-title pull-left">
						<img src="images/VendorSearch.gif" />Search Criteria</h2>
							<logic:equal value="1" name="saveCriteria">
									<input type="button" value="Save Filter" class="btn"
										style="float: right" onclick="showSaveFilter();">
							</logic:equal>	
								
						<div class="form-box card-body">
							<logic:iterate name="searchCriteria" id="criteria">
								<bean:write name="criteria" format="String"/>
								<br/>
							</logic:iterate>							
						</div>
					</header>
				</logic:greaterThan>
			</logic:present>
			
			<header class="card-header">
				<h2 class="card-title pull-left">
					<img src="images/icon-registration.png" />Following are Your Vendors</h2>
				
				
				<input type="button" value="Back" class="btn btn-default pull-right" onclick="backToSearch();">				
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails" property="settings.logoPath"/>
					<input type="button" value="Export" class="btn btn-tertiary pull-right mr-1" onclick="exportGrid();">
				</logic:present>
				<input type="button" value="Export All Suppliers Details" class="btn btn-primary pull-right mr-1" onclick="exportAllSupplierInformation();">
				<div id="searchSummary" class="text-center" style="color: #009900">
					<logic:present name="vendorSearchSummary">
						<bean:write name="vendorSearchSummary" format="String" />
					</logic:present>
				</div>				
			</header>

			<!-- Dialog Box to Export PDF, CSV, & XLS of All Vendors Details -->
			<div id="dialog" style="display: none;" title="Choose Export Type">
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel"/>
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="2" id="csv"/> 
							<input type="hidden" name="fileName" id="fileName" value="VendorStatusSearch">
						</td>
						<td>
							<label for="csv"><img id="csvExport" src="images/csv_export.png" />CSV</label>
						</td>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
							<logic:present name="userDetails">
								<bean:define id="logoPath" name="userDetails" property="settings.logoPath"/>
								<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportSearchResultHelper('searchvendor','VendorStatusSearch','<%=logoPath%>');">
							</logic:present>
						</div>
					</div>
				</footer>
			</div>
			
			<!-- Dialog Box to Export PDF & XLS of One Vendors Complete Registration Informations -->
			<div id="exportPDFXLS" style="display: none;" title="Choose Export Type">
				<input type="hidden" id="vendorID_hdn"/>
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel">
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>						
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				
				<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">				
					<logic:present name="userDetails">
						<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>					
						<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportVendorDetailsHelper();">
					</logic:present>
					</div>
				</div>
			</footer>
			</div>

			<div class="form-box card-body vendor-search" style="overflow-x:scroll !important;">
				<table id="searchvendor" border="0" style="overflow-x:scroll !important;" 
				class="sortable main-table table table-bordered table-striped mb-0">
					<thead>
						<tr>
							<th class="sorttable_nosort" style="padding:0 20px !important;">Print</th>							
							<th class="sorttable_nosort">Comments</th>
							<th class="">Vendor Name</th>
							<th class="">Vendor Status</th>
							<logic:equal name="pendingReview" value="1" >
								<th class="">Status Note</th>
								<th class="">Email this Vendor</th>
								<th class="">Date of Note</th>								
								<th class="">Contact Mobile</th>
								<th class="">Contact User</th>
							</logic:equal>
							<th class="">Business Type</th>
							<th class="">Year of Establishment</th>							
							<th class="">State</th>
							<th class="">Contact First Name</th>
							<th class="">Contact Last Name</th>
							<th class="">Contact Phone</th>
							<th class="">Contact Email</th>							
							<th class="">Diverse Classification</th>
							<th class="">Certification Type</th>						
							<!-- <th class="header">Company Code</th>
							<th class="header">DUNS Number</th>
							<th class="header">Country</th>
							<th class="header">NAICS Code</th>
							<th class="header">Region</th>
							<th class="header">State</th>
							<th class="header">City</th>
							<th class="header">Mode of Registration</th>
							<th class="header">Created On</th>
							<th class="sorttable_nosort header">Action</th> -->
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="searchVendorsList">
							<logic:iterate name="searchVendorsList" id="vendorList">
								<tr>
									<bean:define id="vendorId" name="vendorList" property="id"/>
									<bean:define id="status" name="vendorList" property="vendorStatus"/>
									<td style="text-align: center;">
										<a href="#" alt="Export" title="Print Vendor Profile" class="" onclick="printPDFXLS('${vendorId}')">Print</a>									
										<%-- <img src="./images/Print.png" alt="Export" title="Print Vendor Profile" class="tooltip" onclick="printPDFXLS('${vendorId}')"/> --%>
									</td>
									<td style="text-align: center;">
										<bean:define id="vendorComments" name="vendorList" property="vendorNotes"/>
										<logic:equal value="None" name="vendorComments">
											${vendorComments}
										</logic:equal>
										<logic:notEqual value="None" name="vendorComments">
											<img src="images/vendor-notes.png" class="tooltip" title="${vendorComments}" />
										</logic:notEqual>			
									</td>									
									<td><html:link action="/retrievesupplier.do?method=showSupplierContactDetails&requestString=progress" paramId="id" paramName="vendorId">
											<bean:write name="vendorList" property="vendorName" />
										</html:link>
									</td>									
									<td><bean:write name="vendorList" property="vendorStatus" /></td>
									<logic:equal name="pendingReview" value="1"  >
										<td><bean:write name="vendorList" property="statusNote" /></td>
										<td>
											<html:link action="/mailNotification.do?parameter=showEmailThisVendorPage&requestFrom=searchPage" paramId="vendorId" paramName="vendorId">
												Email
											</html:link>											
										</td>
										<td><bean:write name="vendorList" property="statusDate" /></td>									
										<td><bean:write name="vendorList" property="contactMobile" /></td>
										<td><bean:write name="vendorList" property="contactUser" /></td>
									</logic:equal>
									<td><bean:write name="vendorList" property="businessType" /></td>
									<td><bean:write name="vendorList" property="yearOfEstd" /></td>									
									<td><bean:write name="vendorList" property="stateName" /></td>
									<td><bean:write name="vendorList" property="firstName" /></td>
									<td><bean:write name="vendorList" property="lastName" /></td>
									<td><bean:write name="vendorList" property="phoneNumber" /></td>
									<td><bean:write name="vendorList" property="primeContactEmailId" /></td>
									<td><bean:write name="vendorList" property="diverseClassifcation" /></td>
									<td><bean:write name="vendorList" property="certificateType" /></td>							
									<%-- <td><bean:write name="vendorList" property="vendorCode" /></td>
									<td><bean:write name="vendorList" property="duns" /></td>
									<td><bean:write name="vendorList" property="countryName" /></td>
									<td><bean:write name="vendorList" property="naicsCode" /></td>
									<td><bean:write name="vendorList" property="region" /></td>
									<td><bean:write name="vendorList" property="stateName" /></td>
									<td><bean:write name="vendorList" property="city" /></td>
									<td><bean:write name="vendorList" property="modeOFReg" /></td>
									<td class="sorttable_customkey='MMDDYYYY'"><bean:write name="vendorList" property="created" /></td>
									<td>
										<a onclick="return updateStatus('${vendorId}','${status}');" style='cursor: pointer;'>Update Status</a>
									</td> --%>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
						<logic:empty name="searchVendorsList">
							<tr>
								<td colspan='16'>No records are found...</td>
							</tr>
						</logic:empty>						
					</tbody>
				</table>
			</div>
			<div id="dialog1" style="display: none;" title="Approve Vendor">
				<table >
					<tr></tr>
					<tr>
						<td align="right">Status:</td>
						<td>
							<input type="hidden" id="vendorid" /> 
							<select name="changeVendorStatus" class="chosen-select" id="changeVendorStatus1" onchange="getVendorStatus(this.value);"></select>
						</td>
					</tr>
					<tr id="primeRow" style="visibility: hidden;">
						<td>Vendor Type:</td>
						<td>
							<input type="radio" name="prime" id="primevendor" class="selectedPrimeVendor" value="1" />&nbsp;Prime&nbsp; 
							<input type="radio" class="selectedPrimeVendor" name="prime" value="0" />&nbsp;Non-Prime&nbsp;
						</td>
					</tr>
					<tr>
						<td align="right">Comments:</td>
						<td>
							<textarea cols="50" rows="5" name="isApprovedDesc" id="isApprovedDesc"></textarea>
						</td>
					</tr>
				</table>
				<div class="btn-wrapper text-center">
					<input type="button" class="exportBtn btn btn-primary" onclick="getSelectedIds();" value="Submit" id="statusButton"/>
				</div>
				<div id="ajaxloader" style="display: none;">
					<img src="images/ajax-loader.gif" alt="Ajax Loading Image"/>
				</div>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="View Vendors" name="privilege" property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div class="form-box">
				<h3 align="center">You have no rights to view search vendor all statuses</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<div id="dialog2" class="form-box" style="display: none;"
	title="Save Filter">	
		<div class="row-wrapper form-group row">
			<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Search Name :</label>
			<div class="col-sm-9 ctrl-col-wrapper">
				<input type="text" id="searchName" name="searchName" 
					class="text-box" />
			</div>
		</div>
	
	<div class="btn-wrapper text-center">
		<input type="button" class="exportBtn btn btn-primary" onclick="saveFilter();"
			value="Submit" id="statusButton" />
	</div>
</div>

</div>
</div>
</section>