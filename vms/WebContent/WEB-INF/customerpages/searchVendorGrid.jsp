
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new net.sf.json.JSONObject();
	List<SearchVendorDto> vendorDtos = null;
	if (session.getAttribute("searchVendorsList") != null) {
		vendorDtos = (List<SearchVendorDto>) session
				.getAttribute("searchVendorsList");
	}
	SearchVendorDto searchVendorDto = null;
	if (vendorDtos != null) {
		for (int index = 0; index < vendorDtos.size(); index++) {
			searchVendorDto = vendorDtos.get(index);
			cellobj = new net.sf.json.JSONObject();
			cellobj.put("vendorId", searchVendorDto.getVendor().getId());
			cellobj.put("vendorName", searchVendorDto.getVendor()
					.getVendorName());
			cellobj.put("country", searchVendorDto.getVendor()
					.getCountry());
			cellobj.put("naicsCode", searchVendorDto.getNaics()
					.getNaicsCode());
			cellobj.put("dunsNumber", searchVendorDto.getVendor()
					.getDunsNumber());
			cellobj.put("createdOn", searchVendorDto.getVendor()
					.getCreatedOn());
			cellobj.put("status", "Active");
			cellobj.put("deverseSupplier", searchVendorDto.getVendor()
					.getDeverseSupplier());
			cellobj.put("action", "Approve here");
			cellobj.put("companyCode", searchVendorDto.getVendor()
					.getVendorCode());
			cellobj.put("country", searchVendorDto.getCountry2()
					.getCountryname());
			cellobj.put("naicscode", searchVendorDto.getNaics()
					.getNaicsCode());
			cellobj.put("region", searchVendorDto.getVendor()
					.getRegion());
			cellobj.put("state", searchVendorDto.getState()
					.getStatename());
			cellobj.put("city", searchVendorDto.getVendor().getCity());
			cellobj.put("status", searchVendorDto.getVendor()
					.getVendorStatus());
			if (searchVendorDto.getVendor().getIsinvited() != null
					&& searchVendorDto.getVendor().getIsinvited() == 1){
				cellobj.put("invited", "Customer");
			}else{
				cellobj.put("invited", "Self");
			}
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
