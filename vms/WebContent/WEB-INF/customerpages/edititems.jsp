<%@page import="com.fg.vms.customer.model.Items"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<%
	Items item = (Items) session.getAttribute("currentitem");
	
%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 390px; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Update
				Item
			</h2>
		</div>
		<div id="form-wrapper">
			<html:form action="/updateItem?parameter=updateItems">
				<html:javascript formName="itemForm" />
				<html:hidden property="itemId"
					value="<%=item.getItemid().toString()%>" name="editItemForm" />
				<div id="table-holder">
					<table cellspacing="10" width="500" cellpadding="5"
						style="padding-left: 22%;">
						<tr>
							<td>Item Description</td>
							<td><html:text property="itemDescription"
									name="editItemForm" value="<%=item.getItemDescription()%>"
									alt="" /></td>
							<td><span class="error"><html:errors
										property="itemDescription" /></span></td>
							<td>Item Code</td>
							<td><html:text property="itemCode" name="editItemForm"
									value="<%=item.getItemCode()%>" alt="" /><span class="error"><html:errors
										property="itemCode" /></span></td>
						</tr>
						<tr>
							<td>Item Category</td>
							<td><html:text property="itemCategory" name="editItemForm"
									value="<%=item.getItemCategory()%>" alt="" /></td>
							<td><span class="error"> <html:errors
										property="itemCategory" /></span></td>
							<td>Item Manufacturer</td>
							<td><html:text property="itemManufacturer"
									name="editItemForm" value="<%=item.getItemManufacturer()%>"
									alt="" /></td>
							<td><span class="error"> <html:errors
										property="itemManufacturer" /></span></td>
						</tr>
						<tr>
							<td>Item Uom</td>
							<td><html:text property="itemUom" name="editItemForm"
									value="<%=item.getItemUom()%>" alt="" /></td>
							<td><span class="error"><html:errors
										property="itemUom" /></span></td>
							<td>Active&nbsp;</td>
							<td><html:checkbox property="isActive" /></td>
						</tr>
					</table>
					<div style="padding: 0 0 2% 45%">
						<html:submit value="Update" styleClass="customerbtTxt"
							styleId="submit"></html:submit>
						&nbsp;&nbsp;
						<html:reset value="Cancel" styleClass="customerbtTxt"
							onclick="history.go(-1)"></html:reset>
					</div>
				</div>
			</html:form>
		</div>
		<h2>
			<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;List of
			Items
		</h2>
		<div id="box2" style="max-height: 150px; overflow: auto;">
			<table width="100%">
				<bean:size id="size" name="items" />
				<logic:greaterEqual value="0" name="size">
					<thead>
						<tr>
							<th>Item Description</th>
							<th>Item Code</th>
							<th>Item Category</th>
							<th>Item Manufacturer</th>
							<th>Item Uom</th>
							<th>Actions</th>
						</tr>
					</thead>
					<logic:iterate id="itemlist" name="items">
						<tbody>
							<tr>
								<td><bean:write name="itemlist" property="itemDescription"></bean:write></td>
								<td><bean:write name="itemlist" property="itemCode"></bean:write></td>
								<td><bean:write name="itemlist" property="itemCategory"></bean:write></td>
								<td><bean:write name="itemlist" property="itemManufacturer"></bean:write></td>
								<td><bean:write name="itemlist" property="itemUom"></bean:write></td>
								<bean:define id="ItemId" name="itemlist" property="itemid"></bean:define>
								<td><html:link
										action="/retriveitem.do?parameter=retriveitem"
										paramId="itemid" paramName="ItemId">
									Edit</html:link>|<html:link action="/deleteitem.do?parameter=deleteItem"
										paramId="itemid" paramName="ItemId"
										onclick="return confirm_delete();">Delete</html:link></td>
							</tr>
						</tbody>
					</logic:iterate>
				</logic:greaterEqual>
			</table>
		</div>

	</div>
</div>
