<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%
	List<ClassificationDto> classificationDtos = null;
	if (session.getAttribute("classifications") != null) {
		classificationDtos = (List<ClassificationDto>) session
				.getAttribute("classifications");
		String[] certificateType;
		StringBuilder builder = new StringBuilder();
		if (classificationDtos != null && !classificationDtos.isEmpty()) {
			for (ClassificationDto dto : classificationDtos) {
				if (dto.getClassificationId() == 1) {
					certificateType = dto.getCertificateTypeData()
							.split("\\|");
					for (String str : certificateType) {

						String id = str.split("-")[0];
						String value = str.split("-")[1];
						builder.append("<option value='" + id + "'>"
								+ value + "</option>");

					}
				}
				if (dto.getClassificationId() == 2) {
					builder.append("|");
					certificateType = dto.getCertificateTypeData()
							.split("\\|");
					for (String str : certificateType) {

						String id = str.split("-")[0];
						String value = str.split("-")[1];
						builder.append("<option value='" + id + "'>"
								+ value + "</option>");

					}
				}

			}
		} else {
			builder.append("<option value='0'>Agency Not Available</option>|<option value='0'>Certificate Type Not Available</option>");
		}
		out.print(builder.toString());
	}
%>