<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page
	import="java.util.*,com.fg.vms.customer.model.*,java.util.ArrayList"%>
<%
	List<EmailDistributionListDetailModel> detailModels = null;
	if (session.getAttribute("detailModels") != null) {
		detailModels = (List<EmailDistributionListDetailModel>) session
				.getAttribute("detailModels");
		StringBuilder builder = new StringBuilder();
		if (detailModels != null && detailModels.size() != 0) {
			for (EmailDistributionListDetailModel model : detailModels) {
				if (model.getIsTo() != null && model.getIsTo() != 0) {
					if (model.getUserId() != null) {
						builder.append(model.getUserId()
								.getUserEmailId());
						builder.append(";");
					}
					if (model.getVendorId() != null) {
						builder.append(model.getVendorId().getEmailId());
						builder.append(";");
					}
				}
			}
			builder.append("|");
			for (EmailDistributionListDetailModel model : detailModels) {
				if (model.getIsCC() != null && model.getIsCC() != 0) {
					if (model.getUserId() != null) {
						builder.append(model.getUserId()
								.getUserEmailId());
						builder.append(";");
					}
					if (model.getVendorId() != null) {
						builder.append(model.getVendorId().getEmailId());
						builder.append(";");
					}
				}
			}
			builder.append("|");
			for (EmailDistributionListDetailModel model : detailModels) {
				if (model.getIsBCC() != null && model.getIsBCC() != 0) {
					if (model.getUserId() != null) {
						builder.append(model.getUserId()
								.getUserEmailId());
						builder.append(";");
					}
					if (model.getVendorId() != null) {
						builder.append(model.getVendorId().getEmailId());
						builder.append(";");
					}
				}
			}
		}
		out.print(builder.toString());
	}
%>