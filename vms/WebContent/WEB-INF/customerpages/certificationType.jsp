<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<style>
<!--
.ui-multiselect ui-widget ui-state-default ui-corner-all {
	width: 250px;
}
-->
</style>
<script type="text/javascript">
	$(document).ready(function() {

		$("#formId").validate({
			rules : {
				description : {
					required : true,
					maxlength:255
				},
				shortDescription : {
					required : true,
					maxlength:50
				},
				isActive : {
					required : true
				},
				isCertificateExpiryAlertRequired : {
					required : true
				}
			}
		});
		
		//For multiselect 
		$("#certifiacateDivision").multiselect({
			selectedText : "# of # selected"
		});
	});
	function backToUserrole() {
		window.location = "certificationType.do?parameter=showCertificatePage";
	}
</script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">	
			<div id="successMsg">
				<html:messages id="msg" property="message" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="certificateReferenceDelete"
					message="true">
					<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>

<!--  Adding New Certification -->

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Certification Type" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="add">
				<section class="card">
					<header class="card-header">			
						
				<logic:present name="certificationTypeForm" property="certificateId">
					<h2 class="card-title pull-left">Update Certification Type</h2>
				</logic:present>
				<logic:notPresent name="certificationTypeForm"
					property="certificateId">
					<h2 class="card-title pull-left">Add Certification Type</h2>					
				</logic:notPresent>
				</header>
			
			<div class="form-box card-body">
				<html:form
					action="/certificationType.do?parameter=saveCertificationType"
					styleId="formId">
					<html:hidden property="certificateId" />
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Description</label>
							<div class="col-sm-6 ctrl-col-wrapper">
								<html:textarea property="description" alt=""
									styleId="description" styleClass="main-text-area form-control" rows="10" cols="30" />
							</div>
							<span class="error"><html:errors property="description" /></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Short Description</label>
							<div class="col-sm-6 ctrl-col-wrapper">
								<html:text property="shortDescription" alt=""
									styleId="shortDescription" styleClass="text-box form-control" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is Active</label>
							<div class="col-sm-6 ctrl-col-wrapper" id="status">
								<div class="radio-custom radio-primary">
									<html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
									<label for=""></label>
								</div>
								<div class="radio-custom radio-primary">
										<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
										<label for=""></label>
								</div>
							</div>
							<span class="error"><html:errors property="isActive" /></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Notify Certificate Expiry</label>
							<!-- Is Certificate Expiry Alert Required -->
							<div class="col-sm-6 ctrl-col-wrapper">
								<div class="radio-custom radio-primary">
									<html:radio property="isCertificateExpiryAlertRequired" value="1">&nbsp;Yes&nbsp;</html:radio>
									<label for=""></label>
								</div>
								<div class="radio-custom radio-primary">
									<html:radio property="isCertificateExpiryAlertRequired" value="0">&nbsp;No&nbsp;</html:radio>
									<label for=""></label>
								</div>
							</div>
							<span class="error"><html:errors property="isCertificateExpiryAlertRequired" /></span>
						</div>											
					
					
						<div class="row-wrapper form-group rows">
							<logic:equal value="1" name="isDivisionStatus" property="isDivision">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Customer Division</label>
								<div class="col-sm-6 ctrl-col-wrapper">									
									<html:select property="certifiacateDivision" multiple="true" styleClass="form-control" name="certificationTypeForm" styleId="certifiacateDivision">										
										<bean:size id="size" name="customerDivisions" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="customerDivision" name="customerDivisions">
												<bean:define id="id" name="customerDivision" property="id"></bean:define>
												<html:option value="${id}">
													<bean:write name="customerDivision" property="divisionname"></bean:write>
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</html:select>
								</div>
								<span class="error"><html:errors
										property="customerDivision"></html:errors> </span>
							</logic:equal>
						</div>
					
					<footer class="card-footer mt-2">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<logic:present name="certificationTypeForm"
							property="certificateId">
							<html:submit value="Update" styleClass="btn" styleId="submit1"></html:submit>
						</logic:present>
						<logic:notPresent name="certificationTypeForm"
							property="certificateId">
							<html:submit value="Submit" styleClass="btn btn-primary" styleId="submit1"></html:submit>
						</logic:notPresent>
						<html:reset value="Cancel" styleClass="btn btn-default"
							onclick="backToUserrole();"></html:reset>
					</div>
					</div>
					</footer>
				</html:form>
			</div>
			</section>
		</logic:match>
	</logic:match>
</logic:iterate>
<!--    For Certification Type Role Editing  ( When Certification role did not have the Add privilege from Administrator) -->
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Certification Type" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="add">
			<logic:notPresent name="certificationTypeForm"
				property="certificateId">
				<div style="padding: 5%; text-align: center;">
					<h3>You have no right to add new Certification Type</h3>
				</div>
			</logic:notPresent>
			<logic:present name="certificationTypeForm" property="certificateId">
				<logic:match value="1" name="privilege" property="modify">
					<header class="card-header">
						<h2 class="card-title pull-left">Update Certification Type</h2>
					</header>>
					<div class="form-box card-body">
						<html:form
							action="/certificationType.do?parameter=saveCertificationType"
							styleId="formId">
							<html:hidden property="certificateId" />							
							<div class="row-wrapper form-group row">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Description</label>
								<div class="col-sm-6 ctrl-col-wrapper">
									<html:textarea property="description" alt=""
										styleId="description" styleClass="main-text-area form-control" rows="10" cols="30" />
								</div>
								<span class="error"><html:errors property="description" /></span>
							</div>
							<div class="row-wrapper form-group row">
								<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Short Description</label>
								<div class="col-sm-6 ctrl-col-wrapper">
									<html:text property="shortDescription" alt=""
										styleId="shortDescription" styleClass="text-box form-control" />
								</div>
							</div>
							
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is Active</label>
									<div class="col-sm-6 ctrl-col-wrapper" id="status">
										<div class="radio-custom radio-primary">
											<html:radio property="isActive" value="1">&nbsp;Yes&nbsp;</html:radio>
											<label for=""></label>
										</div>
										<div class="radio-custom radio-primary">
											<html:radio property="isActive" value="0">&nbsp;No&nbsp;</html:radio>
											<label for=""></label>
										</div>
									</div>
									<span class="error"><html:errors property="isActive" /></span>
								</div>
								
								<div class="row-wrapper form-group row">
									<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Notify Certificate Expiry</label><!-- Is Certificate Expiry Alert Required -->
									<div class="col-sm-6 ctrl-col-wrapper">
										<div class="radio-custom radio-primary">
											<html:radio property="isCertificateExpiryAlertRequired" value="1">&nbsp;Yes&nbsp;</html:radio>
											</div>
										<div class="radio-custom radio-primary">
											<html:radio property="isCertificateExpiryAlertRequired" value="0">&nbsp;No&nbsp;</html:radio>
										</div>
									</div>
									<span class="error"><html:errors property="isCertificateExpiryAlertRequired" /></span>
								</div>
							
							
								<div class="row-wrapper form-group row">
									<logic:equal value="1" name="isDivisionStatus" property="isDivision">
										<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Customer Division</label>
										<div class="col-sm-6 ctrl-col-wrapper">									
											<html:select property="certifiacateDivision" styleClass="form-control" multiple="true" name="certificationTypeForm" styleId="certifiacateDivision">										
												<bean:size id="size" name="customerDivisions" />
												<logic:greaterEqual value="0" name="size">
													<logic:iterate id="customerDivision" name="customerDivisions">
														<bean:define id="id" name="customerDivision" property="id"></bean:define>
														<html:option value="${id}">
															<bean:write name="customerDivision" property="divisionname"></bean:write>
														</html:option>
													</logic:iterate>
												</logic:greaterEqual>
											</html:select>
										</div>
										<span class="error"><html:errors
												property="customerDivision"></html:errors> </span>
									</logic:equal>
								</div>
							
							<footer class="mt-2">
									<div class="row justify-content-end">
										<div class="col-sm-9 wrapper-btn">
								<html:submit value="Update" styleClass="btn btn-primary" styleId="submit1"></html:submit>
								<html:reset value="Cancel" styleClass="btn btn-default"
									onclick="backToUserrole();"></html:reset>
							</div>
							</div>
							</footer>
						</html:form>
					</div>
				</logic:match>
			</logic:present>
		</logic:match>
	</logic:match>
</logic:iterate>
<!--  End of the Adding Certification Part  -->


<!--     View , Delete and MOdify the Certification Types  -->

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Certification Type" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
<header class="card-header">
	<h2 class="card-title pull-left">Available Certification Types</h2>
</header>
<div class="form-box card-body">
	<table class="main-table table table-bordered table-striped mb-0">
		<logic:present name="certificationTypes">
			<bean:size id="size" name="certificationTypes" />
			<logic:greaterThan value="0" name="size">
				<thead>
					<tr>
						<td>Description</td>
						<td>Short Description</td>
						<td>Notify Certificate Expiry</td>
						<td>Active</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<logic:iterate name="certificationTypes"
						id="certificationTypesList">
						<tr class="even">
							<td><bean:write name="certificationTypesList"
									property="certificateTypeDesc" /></td>
							<td><bean:write name="certificationTypesList"
									property="shortDescription" /></td>
							<td align="center"><html:checkbox property="isCertificateExpiryAlertRequired"
									name="certificationTypesList" disabled="true" value="1">
								</html:checkbox></td>
							<td align="center"><html:checkbox property="isActive"
									name="certificationTypesList" disabled="true" value="1">
								</html:checkbox></td>

							<%-- <td><logic:equal value="1" name="certificationTypesList"
									property="isActive">
													Yes
												</logic:equal> <logic:equal value="0" name="certificationTypesList"
									property="isActive">
													No
												</logic:equal></td>--%>
							<bean:define id="certificateId" name="certificationTypesList"
								property="id"></bean:define>

							<td align="center"><logic:iterate id="privilege1"
									name="privileges">
									<logic:match value="Certification Type" name="privilege1"
										property="objectId.objectName">
										<logic:match value="1" name="privilege1" property="modify">
											<html:link
												action="/certificationType.do?parameter=editCertificationType"
												paramId="id" paramName="certificateId">Edit
							               </html:link>
							               <logic:match value="1" name="privilege1" property="delete">
										                           | 
										   </logic:match>
										</logic:match>
									</logic:match>
								</logic:iterate> <logic:iterate id="privilege1" name="privileges">
									<logic:match value="Certification Type" name="privilege1"
										property="objectId.objectName">
										<logic:match value="1" name="certificationTypesList" property="isActive">
										<logic:match value="1" name="privilege1" property="delete">
											<html:link
												action="/certificationType.do?parameter=deleteCertificationType"
												paramId="id" paramName="certificateId"
												onclick="return confirm_delete();">Delete
							                </html:link>
										</logic:match>
										</logic:match>
										<logic:match value="0" name="certificationTypesList" property="isActive">
										<span style="cursor:not-allowed; color: #ABF1A8">Delete</span>
<!-- 											<label style="color: #ABF1A8">Delete</label> -->
										</logic:match>
									</logic:match>
								</logic:iterate>
							</td>
						</tr>
					</logic:iterate>
				</tbody>
			</logic:greaterThan>
			<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
		</logic:present>
	</table>
</div>
</logic:match>
</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Certification Type" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view Certification Types</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>