
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new net.sf.json.JSONObject();
	List<SearchVendorDto> vendorDtos = null;
	String vendorSummary="";
	if (session.getAttribute("vendorSearchSummary") != null) {
		 vendorSummary=(String)session
				.getAttribute("vendorSearchSummary");
	}
	if (session.getAttribute("searchVendorsList") != null) {
		vendorDtos = (List<SearchVendorDto>) session
				.getAttribute("searchVendorsList");
	}
	SearchVendorDto searchVendorDto = null;
	if (vendorDtos != null) {
		for (int index = 0; index < vendorDtos.size(); index++) {
			searchVendorDto = vendorDtos.get(index);
			cellobj = new net.sf.json.JSONObject();
			cellobj.put("vendorId", searchVendorDto.getId());
			
			if(searchVendorDto.getVendorName() != null)
				cellobj.put("vendorName", searchVendorDto.getVendorName().replace("|", " "));
			
			if(searchVendorDto.getVendorName() != null)
				cellobj.put("vendorName", searchVendorDto.getVendorName().replace("\"", " "));
						
			if(searchVendorDto.getCountryName() != null)
				cellobj.put("country", searchVendorDto.getCountryName().replace("|", " "));
			
			if( searchVendorDto.getDuns() != null)
				cellobj.put("dunsNumber",  searchVendorDto.getDuns().replace("|", " "));
			
			if(searchVendorDto.getNaicsCode() != null)
				cellobj.put("naicscode", searchVendorDto.getNaicsCode().replace("|", " "));
				
			if(searchVendorDto.getVendorCode() != null)
				cellobj.put("companyCode", searchVendorDto.getVendorCode().replace("|", " "));
			
			if( searchVendorDto.getRegion() != null)
				cellobj.put("region",  searchVendorDto.getRegion().replace("|", " "));
			
			if(searchVendorDto.getStateName() != null)
				cellobj.put("state", searchVendorDto.getStateName().replace("|", " "));
			
			if(searchVendorDto.getCity() != null)
				cellobj.put("city", searchVendorDto.getCity().replace("|", " "));
			
			cellobj.put("status", searchVendorDto.getVendorStatus());
			
			if(searchVendorDto.getModeOFReg() != null)
				cellobj.put("invited", searchVendorDto.getModeOFReg().replace("|", " "));
			
			if(searchVendorDto.getCompanyEmailId() != null)
				cellobj.put("companyEmailId",searchVendorDto.getCompanyEmailId().replace("|", " "));
			
			if(searchVendorDto.getPrimeContactEmailId() != null)
				cellobj.put("primeContactEmailId",searchVendorDto.getPrimeContactEmailId().replace("|", " "));
			if(searchVendorDto.getCreated()!=null)
				cellobj.put("createdon",searchVendorDto.getCreated());
			
			if(searchVendorDto.getStatusNote()!=null)
				cellobj.put("statusNote",searchVendorDto.getStatusNote());
			if(searchVendorDto.getStatusDate()!=null)
				cellobj.put("statusDate",searchVendorDto.getStatusDate());
			
			if(searchVendorDto.getBusinessType() != null)
				cellobj.put("businessType",searchVendorDto.getBusinessType());			
			
			if(searchVendorDto.getYearOfEstd() != null)
				cellobj.put("yearOfEstd",searchVendorDto.getYearOfEstd());
			
			if(searchVendorDto.getFirstName() != null)
				cellobj.put("firstName",searchVendorDto.getFirstName());
			
			if(searchVendorDto.getLastName() != null)
				cellobj.put("lastName",searchVendorDto.getLastName());
			
			if(searchVendorDto.getPhoneNumber() != null)
				cellobj.put("phoneNumber",searchVendorDto.getPhoneNumber());
			
			if(searchVendorDto.getDiverseClassifcation() != null)
				cellobj.put("diverseClassifcation",searchVendorDto.getDiverseClassifcation());
			
			if(searchVendorDto.getCertificateType() != null)
				cellobj.put("certificateType",searchVendorDto.getCertificateType());
			
			if(searchVendorDto.getVendorNotes() != null)
				cellobj.put("vendorNotes",searchVendorDto.getVendorNotes());
		
			cellobj.put("vendorSummary", vendorSummary);
			cellarray.add(cellobj);
			
			
			/* cellobj.put("vendorId", searchVendorDto.getId());
			cellobj.put("vendorName", searchVendorDto);
			cellobj.put("country", searchVendorDto.getCountryName());
			cellobj.put("naicscode", searchVendorDto.getNaicsCode());
			cellobj.put("dunsNumber", searchVendorDto.getDuns());
			cellobj.put("companyCode", searchVendorDto.getVendorCode());
			cellobj.put("region", searchVendorDto.getRegion());
			cellobj.put("state", searchVendorDto.getStateName());
			cellobj.put("city", searchVendorDto.getCity());
			cellobj.put("status", searchVendorDto.getVendorStatus());
			cellobj.put("invited", searchVendorDto.getModeOFReg());
			cellobj.put("companyEmailId", searchVendorDto.getCompanyEmailId());
			cellobj.put("primeContactEmailId", searchVendorDto.getPrimeContactEmailId()); */
		
		}
		out.println(cellarray);		
	}
%>
