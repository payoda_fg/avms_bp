<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
<!--
	$(document).ready(function() {
		$.datepicker.setDefaults({
			// 			showOn : 'both',
			// 			buttonImageOnly : true,
			// 			buttonImage : "jquery/images/calendar.gif",
			changeYear : true,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#startDate').removeClass('text-label');
				$('#endDate').removeClass('text-label');
				// $('#endAfter').removeClass('text-label');
			}
		});
		$("#startDate").datepicker();
		$("#endDate").datepicker();
		// 		$("#endAfter").datepicker();
	});
      
	$(document).ready(function() {
		var test=document.getElementById("endby");
		//alert(test);

		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[0-9]+$/.test(value)
		}, "Enter Only Numbers.");

		$("#proactiveForm").validate({
			rules : {
				numberOfOccurrence : {
					alpha : true
				},
				weeklyWeekNumber : {
					alpha : true
				},
				dailyEveryDayNumber : {
					alpha : true
				},
				monthlyDayNumber : {
					alpha : true
				},
				monthlyWeekNumber : {
					alpha : true
				},
				monthlyMonthNumber : {
					alpha : true
				},
				yearlyDayNumber : {
					alpha : true
				},
				numberOfOccurrence : {
					alpha : true
				}
			}
		});
	});

	function selectRecurrence(id) {
		if (id == 'daily') {
			$('#monthlyRecu').css('display', 'none');
			$('#quarterlyRecu').css('display', 'none');
			$('#weeklyRecu').css('display', 'none');
			$('#yearlyRecu').css('display', 'none');
			$('#dailyRecu').css('display', 'block');
		} else if (id == 'weekly') {
			$('#monthlyRecu').css('display', 'none');
			$('#quarterlyRecu').css('display', 'none');
			$('#weeklyRecu').css('display', 'block');
			$('#yearlyRecu').css('display', 'none');
			$('#dailyRecu').css('display', 'none');
		} else if (id == 'monthly') {
			$('#monthlyRecu').css('display', 'block');
			$('#quarterlyRecu').css('display', 'none');
			$('#weeklyRecu').css('display', 'none');
			$('#yearlyRecu').css('display', 'none');
			$('#dailyRecu').css('display', 'none');
		} else if (id == 'quarterly') {
			$('#monthlyRecu').css('display', 'none');
			$('#quarterlyRecu').css('display', 'block');
			$('#weeklyRecu').css('display', 'none');
			$('#yearlyRecu').css('display', 'none');
			$('#dailyRecu').css('display', 'none');
		} else if (id == 'yearly') {
			$('#monthlyRecu').css('display', 'none');
			$('#quarterlyRecu').css('display', 'none');
			$('#yearlyRecu').css('display', 'block');
			$('#dailyRecu').css('display', 'none');
		}
	}
	function selectModeOfDate(id)
	{
		if(id=='endafter'){
		    $('#eafter').css('display', 'block');   
		    $('#eby').css('display','none');
		} else if(id=='endby'){
			$('#eafter').css('display', 'none');   
			$('#eby').css('display','block');
			
		} else if(id=='noenddate'){
			$('#eafter').css('display', 'none');   
			$('#eby').css('display','none');			
		}
	}
	
	function back() {		
		window.location = "proactiveMonitoring.do?method=proactiveMonitoring";
	}
//-->
</script>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">		
			<div id="successMsg">
				<html:messages id="msg" property="successMsg" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
			
				<div id="failureMsg">
					<html:messages id="msg" property="failureMsg" message="true">
						<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
					</html:messages>
				</div>
				<div id="dysFailureMsg">
					<html:messages id="msg" property="dysFailureMsg" message="true">
						<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
					</html:messages>
				</div>
			</div>
			
			<header class="card-header">
				<h2 class="card-title pull-left">Tier 2 Email Notification</h2>
			</header>

<html:form action="proactiveMonitoring.do?method=save" method="post"
	styleId="proactiveForm">
		<div class="form-box card-body">	
				<label class="control-label text-sm-right label-col-wrapper pb-1">Recurrence Pattern</label>			
				<div class="form-group row row-wrapper">
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<input id="daily" type="radio" value="0" name="reccurrencePattern"
							onclick="selectRecurrence('daily')"> 
							<label for="daily">Daily</label>
						</div>
					</div>
				</div>
				<div class="form-group row row-wrapper">
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<input id="weekly" type="radio" value="1" name="reccurrencePattern"
							onclick="selectRecurrence('weekly')"> 
							<label for="weekly">Weekly</label>
						</div>
					</div>
				</div>
				<div class="form-group row row-wrapper">
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<input id="monthly" type="radio" value="2"
							name="reccurrencePattern" onclick="selectRecurrence('monthly')">
						<label for="monthly">Monthly</label>
						</div>
					</div>
				</div>
				<div class="form-group row row-wrapper">
					<div class="col-sm-9">
						<div class="radio-custom radio-primary">
							<input id="yearly" type="radio" value="3" name="reccurrencePattern"
							onclick="selectRecurrence('yearly')"> 
							<label for="yearly">Yearly</label>
						</div>
					</div>
				</div>
			<div class="form-group">
				<div style="display: none;" id="dailyRecu">
					<div class="form-group row row-wrapper">
						<div class="col-sm-12">
							<div class="col-sm-1 radio-custom radio-primary">
								<input id="every" type="radio" checked="checked" value="0"
									name="dailyOption"> 
								<label for="every pt-1">Every</label>
							</div>
							<div class="col-sm-3 ctrl-col-wrapper">
								<html:text value="1" property="dailyEveryDayNumber"
									name="proactiveMonitoring" alt="" styleClass="text-box form-control"></html:text>
								
							</div>
							<div class="col-sm-3">
								<div class="form-control">days</div>
							</div>
						</div>
					</div>
					<div class="form-group row row-wrapper">
						<div class="col-sm-12">
							<div class=" radio-custom radio-primary">
								<input id="weekday" type="radio" value="1" name="dailyOption">
								<label for="weekday pt-1">Every Weekday</label>
						</div>
					</div>
				</div>
				</div>
			</div>
			<div class="form-group">
				<div id="weeklyRecu">
					<div class="form-group row row-wrapper">						
						<label class="control-label label-col-wrapper pb-1 pl-1">Every week(s) on</label>
						<div class="col-sm-2">
							<div class="ctrl-col-wrapper">
								<html:text property="weeklyWeekNumber" name="proactiveMonitoring"
									alt="" styleClass="text-box form-control"></html:text>
							</div>
						</div>
					</div>
					<div class="form-group row row-wrapper">
						<div class="col-sm-9">
							<div class="checkbox-custom chekbox-primary">
								<input id="SUN" type="checkbox" value="SUN" name="weeklyWeekDays"> 
							 	<label for="SUN">Sunday</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input id="MON" type="checkbox" value="MON" name="weeklyWeekDays"> 
								<label for="MON">Monday</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input id="TUE" type="checkbox" value="TUE" name="weeklyWeekDays"> 
								<label for="TUE">Tuesday</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input id="WED" type="checkbox" value="WED" name="weeklyWeekDays"> 
								<label for="WED">Wednesday</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input id="THU" type="checkbox" value="THU" name="weeklyWeekDays"> 
								<label for="THU">Thursday</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input id="FRI" type="checkbox" value="FRI" name="weeklyWeekDays"> 
								<label for="FRI">Friday</label>
							</div>
							<div class="checkbox-custom chekbox-primary">
								<input id="SAT" type="checkbox" value="SAT" name="weeklyWeekDays" > 
								<label for="SAT">Saturday</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr/>
			<div style="display: none; float:left;" id="monthlyRecu">
				<div class="row-wrapper form-group row">
					<div class="col-sm-12">
						<div class="radio-custom radio-primary col-sm-1">
							<input id="month_day" type="radio" checked="checked" value="0"
							name="monthlyOption"> 
							<label for="month_day">Day</label>
						</div>	
						<div class="col-sm-2">				
							<html:text property="monthlyDayNumber" name="proactiveMonitoring"
								alt="" styleClass="text-box form-control"></html:text>	
						</div>				
						<div class="col-sm-1">of every</div>	
						<div class="col-sm-2">					
							<html:text value="1" name="proactiveMonitoring"
									property="monthlyWeekNumber" alt="" styleClass="text-box form-control">
							</html:text>
						</div>						
						<div class="col-sm-1">month(s)</div>
					</div>
				</div>
				<%-- <div style="display: none; float: left;" id="quarterlyRecu">
				<div class="coulmn-1-row">
					<div class="coulmn-1">
							<div class="coulmn-1">Notification Date</div>
							<div class="coulmn-1">
								<html:text property="fiscalEndDate" alt="Please click to select date" styleClass="text-box"
									style="padding: 0%;" styleId="fiscalEndDate" readonly="true"
									name="tier2RepotingForm" />
									</div>
							<span class="error"><html:errors
									property="certificateShortName"></html:errors>
							</span>
							
						</div>
				</div>
			</div> --%>
				<div class="row-wrapper form-group row">
					<div class="">
						<div class="radio-custom radio-primary col-sm-1">
							<input id="month_week" type="radio" value="1"
								name="monthlyOption">
								<label for="month_week">The</label>
							</div>
						
							<div class="col-sm-3">
								<html:select name="proactiveMonitoring"
									property="monthlyWeekNumber" styleClass="chosen-select form-control">
									<option value="first">First</option>
									<option value="second">Second</option>
									<option value="third">Third</option>
									<option value="fourth">Fourth</option>
									<option value="last">Last</option>
								</html:select>
							</div>
						<div class="ctrl-col-wrapper col-sm-3">
								<html:select property="monthlyWeekDay"
									name="proactiveMonitoring" styleClass="chosen-select form-control">
									<option value="0">Sunday</option>
									<option value="1">Monday</option>
									<option value="2">Tuesday</option>
									<option value="3">Wednesday</option>
									<option value="4">Thursday</option>
									<option value="5">Friday</option>
									<option value="6">Saturday</option>
								</html:select>
							</div>
						<div class="col-sm-1">of every</div>
						<div class="col-sm-2">
							<html:text name="proactiveMonitoring" value="1"
									property="monthlyMonthNumber" styleClass="text-box form-control"></html:text>
							</div>
						<div class="ccol-sm-1">month(s)</div>
					</div>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div style="display: none; width: 100%;" id="yearlyRecu">					
					<div class="">
						<div class="radio-custom radio-primary col-sm-1">
							<input id="year_month" type="radio" checked="checked" value="0"
								name="yearlyOption"> Every
						</div>
						<div class="ctrl-col-wrapper col-sm-3">
							<html:select name="proactiveMonitoring" property="yearlyMonthNumber" styleClass="chosen-select form-control">
								<option value="0">--select--</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option selected="selected" value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</html:select>
						</div>
						<div class="col-sm-3">
							<html:text name="proactiveMonitoring" value="31"
								property="yearlyDayNumber" alt="" styleClass="text-box form-control"></html:text>
						</div>
					</div>
					<div class="col-sm-12 mt-1">
						<div class="row-wrapper form-group row">
							<div class="radio-custom radio-primary col-sm-1">
								<input id="year_selection" type="radio" value="1"
									name="yearlyOption"> The
							</div>
							<div class="ctrl-col-wrapper col-sm-3">
									<html:select name="proactiveMonitoring" 
										property="yearlyWeekNumber" styleClass="chosen-select form-control">
										<option value="0">--select--</option>
										<option value="first">First</option>
										<option value="second">Second</option>
										<option value="third">Third</option>
										<option value="fourth">Fourth</option>
										<option value="last">Last</option>
									</html:select>
								</div>
								<div class="col-sm-3">
									<html:select name="proactiveMonitoring" 
										property="yearlyWeekDayNumber" styleClass="chosen-select form-control">
										<option value="0">--select--</option>
										<option value="0">Sunday</option>
										<option value="1">Monday</option>
										<option value="2">Tuesday</option>
										<option value="3">Wednesday</option>
										<option value="4">Thursday</option>
										<option value="5">Friday</option>
										<option value="6">Saturday</option>
									</html:select>
								</div>
								<div class="col-sm-1">of every</div>
								<div class="col-sm-3">
									<html:select name="proactiveMonitoring"
										property="yearlyMonthNumber" 
										styleClass="chosen-select form-control">
										<option value="0">--select--</option>
										<option value="1">January</option>
										<option value="2">February</option>
										<option value="3">March</option>
										<option value="4">April</option>
										<option value="5">May</option>
										<option value="6">June</option>
										<option value="7">July</option>
										<option value="8">August</option>
										<option value="9">September</option>
										<option value="10">October</option>
										<option value="11">November</option>
										<option value="12">December</option>
									</html:select>
								</div>
							</div>
						</div>
					</div>
				</div>

		
		
		<div class="form-box" style="clear: both;">
			<label class="control-label text-sm-right label-col-wrapper pb-1">Range of recurrence</label>
			<div class="form-group row row-wrapper">
				<label class="pl-1 control-label text-sm-right label-col-wrapper pb-1">Start :</label>
				<div class="col-sm-5 ctrl-col-wrapper">
					<html:text name="proactiveMonitoring" property="rangeStartDate"
						alt="Please click to select date" styleId="startDate" 
						styleClass="text-box-date form-control" readonly="true"></html:text>
				</div>
			</div>
			<div class="form-group row row-wrapper">
				<div class="col-sm-9 ctrl-col-wrapper">
					<div class="radio-custom radio-primary">
						<input id="noenddate" type="radio" checked="checked"
						name="rangeOccurenceOption" value="0" onclick="selectModeOfDate('noenddate');" >
						<label for="noenddate">No end date</label>
					</div>
				</div>
			</div>
			<div class="form-group row row-wrapper">
				<div class="col-sm-9 ctrl-col-wrapper">
					<div class="radio-custom radio-primary pull-left col-sm-2 mt-1">
						<input id="endafter" type="radio" name="rangeOccurenceOption"
						value="1" onclick="selectModeOfDate('endafter');" />
						<label for="endafter">End after</label>
					</div>	
					<div id="eafter">
						<div class="col-sm-6">				
							<html:text name="proactiveMonitoring" alt="Optioanl"
								property="numberOfOccurrence" styleId="endAfter"
								styleClass="text-box form-control col-sm-3" onchange="ajaxFn(this,'R');" disabled="flag"> Occurrences </html:text>
								<div class="col-sm-3">&nbsp;Occurrences</div><div class="error"><html:errors
									property="numberOfOccurrence" /></div>
						</div>			
					</div>			
				</div>
			</div>
			
			
			<div class="form-group row row-wrapper">
				<div class="col-sm-9 ctrl-col-wrapper">
					<div class="radio-custom radio-primary pull-left col-sm-2 mt-1">
						<input id="endby" type="radio" name="rangeOccurenceOption"
						value="2" onclick="selectModeOfDate('endby');">
						<label for="endby">End by :</label>
					</div>
					<div id="eby">
						<div class="col-sm-6">	
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>	
								<input value="Please click to select date" name="proactiveMonitoring" type="date" data-plugin-datepicker="" id="endDate" class="text-box-date form-control"/>		
							</div>
						</div>
					</div>		
				</div>
			</div>
		
		<footer class="mt-2">
			<div class="row justify-content-end">
				<div class="wrapper-btn">
					<input type="submit" value="Ok" class="btn btn-primary"> <input
					type="button" value="Cancel" class="btn btn-default" onclick="back();">
				</div>
			</div>
		</footer>
	</div>
	</div>
	
</html:form>

<logic:present name="proactiveMonitoring" property="reccurrencePattern">
	<logic:notEmpty name="proactiveMonitoring"
		property="reccurrencePattern">
		<logic:equal value="0" name="proactiveMonitoring"
			property="reccurrencePattern">
			<script>
				$('#daily').attr('checked', 'checked');
				$('#monthlyRecu').css('display', 'none');
				$('#quarterlyRecu').css('display', 'none');
				$('#weeklyRecu').css('display', 'none');
				$('#yearlyRecu').css('display', 'none');
				$('#dailyRecu').css('display', 'block');
			</script>
		</logic:equal>
		<logic:equal value="1" name="proactiveMonitoring"
			property="reccurrencePattern">
			<script>
				$('#weekly').attr('checked', 'checked');
				$('#monthlyRecu').css('display', 'none');
				$('#quarterlyRecu').css('display', 'none');
				$('#weeklyRecu').css('display', 'block');
				$('#yearlyRecu').css('display', 'none');
				$('#dailyRecu').css('display', 'none');
			</script>
		</logic:equal>
			<logic:equal value="2" name="proactiveMonitoring"
			property="reccurrencePattern">
			<script>
				$('#monthly').attr('checked', 'checked');
				$('#monthlyRecu').css('display', 'block');
				$('#quarterlyRecu').css('display', 'none');
				$('#weeklyRecu').css('display', 'none');
				$('#yearlyRecu').css('display', 'none');
				$('#dailyRecu').css('display', 'none');
			</script>
		</logic:equal>
		<logic:equal value="4" name="proactiveMonitoring"
			property="reccurrencePattern">
			<script>
				$('#quarterly').attr('checked', 'checked');
				$('#quarterlyRecu').css('display', 'block');
				$('#monthlyRecu').css('display', 'none');
				$('#weeklyRecu').css('display', 'none');
				$('#yearlyRecu').css('display', 'none');
				$('#dailyRecu').css('display', 'none');
			</script>
		</logic:equal>
		<logic:equal value="3" name="proactiveMonitoring"
			property="reccurrencePattern">
			<script>
				$('#yearly').attr('checked', 'checked');
				$('#monthlyRecu').css('display', 'none');
				$('#quarterlyRecu').css('display', 'none');
				$('#weeklyRecu').css('display', 'none');
				$('#yearlyRecu').css('display', 'block');
				$('#dailyRecu').css('display', 'none');
			</script>
		</logic:equal>
	</logic:notEmpty>
</logic:present>

<logic:present name="proactiveMonitoring" property="weeklyWeekDays">
	<logic:notEmpty name="proactiveMonitoring" property="weeklyWeekDays">
		<logic:iterate id="day" name="proactiveMonitoring"
			property="weeklyWeekDays">
			<bean:define id="day1" name="day"></bean:define>
			<script>
				$('#' +
			<%=day1%>
				.id).prop('checked', true);
			</script>
		</logic:iterate>
	</logic:notEmpty>
</logic:present>

<logic:present name="proactiveMonitoring" property="dailyOption">
	<logic:notEmpty name="proactiveMonitoring" property="dailyOption">
		<logic:equal value="0" name="proactiveMonitoring"
			property="dailyOption">
			<script>
				$('#every').attr('checked', 'checked');
			</script>
		</logic:equal>
		<logic:equal value="1" name="proactiveMonitoring"
			property="dailyOption">
			<script>
				$('#weekday').attr('checked', 'checked');
			</script>
		</logic:equal>
	</logic:notEmpty>
</logic:present>

<logic:present name="proactiveMonitoring" property="monthlyOption">
	<logic:notEmpty name="proactiveMonitoring" property="monthlyOption">
		<logic:equal value="0" name="proactiveMonitoring"
			property="monthlyOption">
			<script>
				$('#month_day').attr('checked', 'checked');
			</script>
		</logic:equal>
		<logic:equal value="1" name="proactiveMonitoring"
			property="monthlyOption">
			<script>
				$('#month_week').attr('checked', 'checked');
			</script>
		</logic:equal>
	</logic:notEmpty>
</logic:present>

<logic:present name="proactiveMonitoring" property="yearlyOption">
	<logic:notEmpty name="proactiveMonitoring" property="yearlyOption">
		<logic:equal value="0" name="proactiveMonitoring"
			property="yearlyOption">
			<script>
				$('#year_month').attr('checked', 'checked');
			</script>
		</logic:equal>
		<logic:equal value="1" name="proactiveMonitoring"
			property="monthlyOption">
			<script>
				$('#year_selection').attr('checked', 'checked');
			</script>
		</logic:equal>
	</logic:notEmpty>
</logic:present>

<logic:present name="proactiveMonitoring"
	property="rangeOccurenceOption">
	<logic:notEmpty name="proactiveMonitoring"
		property="rangeOccurenceOption">
		<logic:equal value="0" name="proactiveMonitoring"
			property="rangeOccurenceOption">
			<script>
				$('#noenddate').attr('checked', 'checked');
			</script>
		</logic:equal>
		<logic:equal value="1" name="proactiveMonitoring"
			property="rangeOccurenceOption">
			<script>
				$('#endafter').attr('checked', 'checked');
				
			</script>
		</logic:equal>
		<logic:equal value="2" name="proactiveMonitoring"
			property="rangeOccurenceOption">
			<script>
				$('#endby').attr('checked', 'checked');
			</script>
		</logic:equal>
	</logic:notEmpty>
</logic:present>
</div>
</div>
</section>
<script>
var cnt=0;
function count()
{	
	cnt=cnt+1;
}
</script>