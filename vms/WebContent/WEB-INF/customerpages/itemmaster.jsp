<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.fg.vms.customer.model.Items,java.util.List"%>

<script type="text/javascript"
	src="jquery/js/jqueryUtils.js"></script>

<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 390px; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Item
				Entry
			</h2>
		</div>
		<div id="form-wrapper">
			<html:form action="/addItem?parameter=addItems">
				<div id="table-holder">
					<br />&nbsp;&nbsp;
					<html:messages id="msg" property="item" message="true">
						<span style="color: green; font-size: 10pt"><bean:write
								name="msg" /></span>
					</html:messages>
					<html:javascript formName="itemForm" />
					<div style="padding: 0.5% 0 0 90%;">
						<html:button property="Add" value="Add" styleClass="customerbtTxt"
							onclick="enable()" />
					</div>
					<table cellspacing="10" width="500" cellpadding="5" style="padding-left: 22%;">
						<tr>
							<td>Item Description</td>
							<td><html:text property="itemDescription" name="itemForm"
									disabled="true" styleId="itemDescription" alt=""/></td><td><span
								class="error"><html:errors
										property="itemDescription" /></span></td>
							<td>Item Code</td>
							<td><html:text property="itemCode" name="itemForm"
									disabled="true" styleId="itemCode" alt=""/></td><td><span
								class="error"><html:errors property="itemCode" /></span></td>
						</tr>
						<tr>
							<td>Item Category</td>
							<td><html:text property="itemCategory" name="itemForm"
									disabled="true" styleId="itemCategory" alt=""/></td><td><span
								class="error"> <html:errors property="itemCategory" /></span></td>
							<td>Item Manufacturer</td>
							<td><html:text property="itemManufacturer" name="itemForm"
									disabled="true" styleId="itemManufacturer" alt=""/></td><td><span
								class="error"> <html:errors
										property="itemManufacturer" /></span></td>
						</tr>
						<tr>
							<td>Item Uom</td>
							<td><html:text property="itemUom" name="itemForm"
									disabled="true" styleId="itemUom" alt=""/></td><td><span
								class="error"><html:errors property="itemUom"/></span></td>
							<td>Active&nbsp;</td>
							<td><html:checkbox property="isActive" disabled="true"
									styleId="isActive" /></td>
						</tr>
					</table>
					
					<div style="padding : 0 0 2% 45%">
						<html:submit value="Submit" styleClass="customerbtTxt" styleId="submit"/>
						<html:reset value="Clear" styleClass="customerbtTxt" />
					</div>
					
				</div>
			</html:form>
		</div>
		<h2><img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;List of Items</h2>
		<div id="box2" style="max-height: 150px; overflow: auto;">
			<table width="100%">
				<bean:size id="size" name="items" />
				<logic:greaterEqual value="0" name="size">
					<thead>
						<tr>
							<th>Item Description</th>
							<th>Item Code</th>
							<th>Item Category</th>
							<th>Item Manufacturer</th>
							<th>Item Uom</th>
							<th>Actions</th>
						</tr>
					</thead>
					<logic:iterate id="itemlist" name="items">
						<tbody>
							<tr>
								<td><bean:write name="itemlist" property="itemDescription"></bean:write></td>
								<td><bean:write name="itemlist" property="itemCode"></bean:write></td>
								<td><bean:write name="itemlist" property="itemCategory"></bean:write></td>
								<td><bean:write name="itemlist" property="itemManufacturer"></bean:write></td>
								<td><bean:write name="itemlist" property="itemUom"></bean:write></td>
								<bean:define id="ItemId" name="itemlist" property="itemid"></bean:define>
								<td><html:link
										action="/retriveitem.do?parameter=retriveitem"
										paramId="itemid" paramName="ItemId">
										Edit</html:link>|<html:link action="/deleteitem.do?parameter=deleteItem"
										paramId="itemid" paramName="ItemId"
										onclick="return confirm_delete()">Delete</html:link></td>
							</tr>
						</tbody>
					</logic:iterate>
				</logic:greaterEqual>
			</table>
		</div>
	</div>
</div>
