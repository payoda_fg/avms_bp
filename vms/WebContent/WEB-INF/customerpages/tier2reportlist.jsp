<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<script type="text/javascript" src="jquery/js/grid.locale-en1.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min1.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid1.css" />

<!-- This is for Other Things of JqGrid -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />

<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />

<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<script type="text/javascript"
	src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>

<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<!-- For Tooltip -->
<script type="text/javascript" src="jquery/ui/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/tooltipster.css" />

<style type="text/css">
/* Sortable style starts here */
/* Modified for Sorting Header*/
.main-table td,th {
	border: 1px solid #e9eaea;
	padding: 5px;
}

.main-table th.header {
	background: #009900;
	color: #fff;
	cursor: pointer;
}

/* Sorting Table Header */
table.sortable th:not (.sorttable_nosort ):not (.sorttable_sorted ):not
	 (.sorttable_sorted_reverse ):after {
	content: " \25B4\25BE"
}
/* Sortable style ends here */
</style>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">
				Tier2 Report History</h2>
	<!-- <input
		type="button" value="Add New Report" class="btn" style="float: right"
		onclick="linkPage('tier2Report.do?method=tire2Report')"> -->
</header>
<div class="clear"></div>
<div id="form-box" class="card-body">
	<table id="reportHistory" class="table table-bordered table-striped mb-0">
		<tr>
			<td />
		</tr>
	</table>
	<div id="paddtree"></div>
</div>

<div id="exportPDFXLS" style="display: none;" title="Choose Export Type">
	<input type="hidden" id="tier2ReportMasterID" />
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="1" id="excel"></td>
			<td><label for="excel"><img id="excelExport"
					src="images/excel_export.png" />Excel</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="3" id="pdf"></td>
			<td><label for="pdf"><img id="pdfExport"
					src="images/pdf_export.png" />PDF</label></td>
		</tr>
	</table>

	<footer class="mt-2 card-footer">
		<div class="row justify-content-end">
			<div class="wrapper-btn">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails"
				property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="exportBtn btn btn-primary"
				onclick="exportTier2VndorsReport();">
		</logic:present>
		</div>
	</div>
	</footer>
</div>

<div class="form-box card-body vendor-search" style="overflow-x:scroll !important;">
	<table id="searchvendor" width="100%" border="0"
		class="sortable main-table table table-bordered table-striped mb-0">
		<thead>
			<tr>
				<th class="sorttable_nosort" style="padding:0 20px !important;">Print</th>
				<th class="">Vendor Name</th>
				<th class="">Year</th>
				<th class="">Reporting Period</th>
				<th class="">Direct Spend</th>
				<th class="">Indirect Spend</th>
				<th class="">Total Sales</th>
				<th class="">Total Sales To Company</th>
				<th class="">Submitted</th>
				<th class="">Created On</th>
				<th class="sorttable_nosort">Action</th>
			</tr>
		</thead>
		<tbody>
			<logic:notEmpty name="tier2ReportHistory">
				<logic:iterate name="tier2ReportHistory" id="reportList">
				<bean:define id="fileName" name="reportList"
								property="fileName" /> <c:choose>
								<c:when test="${not empty fileName}">
					<tr>
						<bean:define id="tier2reportId" name="reportList"
							property="tier2reportId" />
						<bean:define id="vendorName" name="reportList"
							property="vendorName" />
						<td style='text-align: center;'><a href="#" alt="Export"
							title="Print Vendor Profile" class=""
							onclick="printPDFXLS('${tier2reportId}')"><span>Print</span></a></td>
						<td><bean:write name="reportList" property="vendorName" /></td>
						<td><bean:write name="reportList" property="year" /></td>
						<td><bean:write name="reportList" property="reportPeriod" /></td>
						<%-- <c:set var = "dExp" value = "${Math.round(reportList.directExp)}"/>
						<td><c:out value = "${dExp}"/></td> --%>
<%-- 						<td>${reportList.directExp}</td> --%>
						<fmt:formatNumber var="dExp" type="number" value="${reportList.directExp}" />
						<fmt:parseNumber var="dierectExppence" integerOnly="true" type="number" value="${dExp}" />
						<td><c:out value="${dierectExppence}" /></td>
						<%-- <td><bean:write name="reportList" property="directExp" /></td> --%>
						<fmt:formatNumber var="inExp" type="number" value="${reportList.indirectExp}" />
						<fmt:parseNumber var="indierectExppence" integerOnly="true" type="number" value="${inExp}" />
						<td><c:out value="${indierectExppence}" /></td>
						<%-- <td><bean:write name="reportList" property="indirectExp" /></td> --%>
						<fmt:formatNumber var="toalSlae1" type="number" value="${reportList.totalSales}" />
						<fmt:parseNumber var="totalSale" integerOnly="true" type="number" value="${toalSlae1}" />
						<td><c:out value="${totalSale}" /></td>
						<%-- <td><bean:write name="reportList" property="totalSales" /></td> --%>
						<fmt:formatNumber var="totalSaleTC" type="number" value="${reportList.totalSalesToCompany}" />
						<fmt:parseNumber var="toalSlaeToCompany" integerOnly="true" type="number" value="${totalSaleTC}" />
						<td><c:out value="${toalSlaeToCompany}" /></td>
						<%-- <td><bean:write name="reportList"
								property="totalSalesToCompany" /></td> --%>
						<td><bean:write name="reportList" property="isSubmitted" /></td>
						<td class="sorttable_customkey='MMDDYYYY'"><bean:write
								name="reportList" property="createdOn" /></td>
						<td><bean:define id="fileName" name="reportList"
								property="fileName" /> <c:choose>
								<c:when test="${not empty fileName}">
									<bean:define id="tier2reportId" name="reportList"
										property="tier2reportId" />
									<a href="#" onclick="downloadExcel(${tier2reportId});">Download</a>
									<%-- <html:link href="downloadExcel.jsp" paramId="tier2reportId"
								paramName="tier2reportId" styleId="downloadFile1"
								styleClass="downloadFile" target="_blank" >Download</html:link> --%>
								</c:when>
								<c:otherwise>
									<p>Direct</p>
								</c:otherwise>
							</c:choose></td>
					</tr>
					</c:when>
					</c:choose>
				</logic:iterate>
			</logic:notEmpty>
			<logic:empty name="tier2ReportHistory">
				<tr>
					<td colspan='13'>No records are found...</td>
				</tr>
			</logic:empty>
		</tbody>
	</table>
</div>
</div>
</div>
</section>
<script type="text/javascript">

function printPDFXLS(id) {			
	$('#tier2ReportMasterID').val(id);
	$("#exportPDFXLS").css({
		"display" : "block"
	});
	$("#exportPDFXLS").dialog({
		minWidth : 300,
		modal : true
	});
}

function downloadExcel(param)
{
	document.location='viewVendors.do?method=downloadExcel&id='+param+'&random=' + Math.random();
}
	var grid = $("#reportHistory");

	$(document).ready(function() {

	});
	$
			.ajax({
				url : 'viewVendorsStatus.do?method=tier2VendorSpendReportSearch&type=report'
						+ '&random=' + Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#reportHistory").jqGrid('GridUnload');
					reportHisrory(data);
				}
			});

	jQuery("#reportHistory").jqGrid('filterToolbar', {
		stringResult : true,
		searchOperators : true,
		searchOnEnter : false
	});

	function checkboxFormatter(cellvalue, options, rowObject) {
		cellvalue = cellvalue + "";
		cellvalue = cellvalue.toLowerCase();
		var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked=\"checked\""
				: "";
		return "<input type='checkbox' disabled='true' onclick=\"ajaxSave('"
				+ options.rowId + "', this);\" " + bchk + " value='"
				+ cellvalue + "' offval='no' />";
	}
</script>
