<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%@page import="com.fg.vms.util.Decrypt"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />

<script type="text/javascript">
<!--
	function backToVendorUsers() {
		window.location = "viewvendoruser.do?parameter=viewVendors";
	}
	
	$(document).ready(function() {
		
		$("#roleId").select2({width: "90%"});
		$("#secId").select2({width: "90%"});
	});
	
//-->
</script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<bean:define id="currentUserId" name="userId"></bean:define>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">
<html:messages id="msg" property="vendor" message="true">
	<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
</html:messages>
<header class="card-header">
				<h2 class="card-title pull-left">
	<img src="images/userRegistration.png" alt="" />Edit Vendor User</h2>
</header>

<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="modify">

			<div class="form-box card-body">
				<html:form action="/updatevendor?parameter=updateVendors"
					styleId="vendorForm">
					<html:hidden property="id" name="editVendorUserForm" />

					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="firstName" alt="" styleId="firstName"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="1" />
							</div>
							<span class="error"><html:errors property="firstName"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="lastName" alt="" styleId="lastName"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="2" />
							</div>
							<span class="error"><html:errors property="lastName"></html:errors></span>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="designation" alt="" styleId="designation"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="3" />
							</div>
							<span class="error"><html:errors property="designation"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone Number</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="phonenumber" alt="" styleId="phonenumber"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="4" />
							</div>
							<span class="error"><html:errors property="phonenumber"></html:errors></span>
						</div>
					
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="mobile" alt="Optional" styleId="mobile"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="5" />
							</div>
							<span class="error"><html:errors property="mobile"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">FAX</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="fax" alt="" styleId="fax"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="6" />
							</div>
							<span class="error"><html:errors property="fax"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email ID</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:hidden property="hiddenEmailId" name="editVendorUserForm"
									alt="" styleId="hiddenEmailId" />
								<html:text property="emailId" name="editVendorUserForm" alt=""
									styleId="emailId" styleClass="text-box form-control"
									onchange="ajaxEdFn(this,'hiddenEmailId','VE');" tabindex="7" />
							</div>
							<span class="error"><html:errors property="emailId"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">User Role</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:select property="roleId" 
									styleId="roleId" name="editVendorUserForm" styleClass="form-control" tabindex="8">
									<bean:size id="size" name="vendorRoles" />
									<html:option value="">----Select---</html:option>
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="role" name="vendorRoles">
											<bean:define id="id" name="role" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="role" property="roleName"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>
					</div>
					<div class="wrapper-half">
						
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Password</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:password property="password" alt="" styleId="password"
									styleClass="text-box form-control" name="editVendorUserForm" tabindex="11" />
							</div>
							<span class="error"><html:errors property="password"></html:errors></span>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Confirm Password</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:password property="confirmPassword" styleClass="text-box form-control"
									styleId="confirmPassword" name="editVendorUserForm" alt=""
									tabindex="12" />
							</div>
							<span class="error"><html:errors property="password"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question ID</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:select property="secId" 
									styleId="secId" styleClass="form-control" name="editVendorUserForm" tabindex="13">
									<bean:size id="size" name="secretIds" />
									<html:option value="">----Select---</html:option>
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="role" name="secretIds">
											<bean:define id="id" name="role" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="role" property="secQuestionParticular"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question Answer</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="secretQuestionAnswer" alt=""
									name="editVendorUserForm" styleId="secretQuestionAnswer"
									styleClass="text-box form-control" tabindex="14" />
							</div>
							<span class="error"><html:errors
									property="secretQuestionAnswer"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
							<div class="col-sm-7 ctrl-col-wrapper">
							<span class="radio-custom radio-primary">
								<html:radio property="gender" value="M">Male</html:radio>
							</span>
							<span class="radio-custom radio-primary">
								<html:radio property="gender" value="F">Female</html:radio>
							</span>
							<span class="radio-custom radio-primary">
								<html:radio property="gender" value="T">Transgender</html:radio>
								</span>
							</div>
						</div>
					</div>

					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
						<html:submit value="Update" styleClass="btn btn-primary"></html:submit>
						<html:reset value="Cancel" onclick="backToVendorUsers();"
							styleClass="btn btn-default"></html:reset>
						</div>
					</div>
					</footer>

				</html:form>
			</div>

			<logic:iterate id="privilege" name="rolePrivileges">
				<logic:match value="User" name="privilege"
					property="objectId.objectName">
					<logic:match value="1" name="privilege" property="view">
						<header class="card-header">
							<h2 class="card-title pull-left">
							<img src="images/user-group.gif" alt="" />Registered Users</h2>
						</header>
						<div class="form-box card-body" style="overflow: auto;">
							<table class="main-table table table-bordered table-striped mb-0" id="vendorUserTable">
								<bean:size id="size" name="vendorusers" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<td class="">Role</td>
											<td class="">FirstName</td>
											<td class="">LastName</td>
											<td class="">Designation</td>
											<td class="">Phone Number</td>
											<td class="">Email Id</td>
											<td class="">Actions</td>
										</tr>
									</thead>
									<tbody>
										<logic:iterate name="vendorusers" id="vendorlist">
											<tr>
												<td><bean:write name="vendorlist"
														property="vendorUserRoleId.roleName" /></td>
												<td><bean:write name="vendorlist" property="firstName" /></td>
												<td><bean:write name="vendorlist" property="lastName" /></td>
												<td><bean:write name="vendorlist"
														property="designation" /></td>
												<td><bean:write name="vendorlist"
														property="phoneNumber" /></td>
												<td><bean:write name="vendorlist" property="emailId" /></td>
												<bean:define id="vendorId" name="vendorlist" property="id"></bean:define>
												<td><logic:iterate id="privilege" name="rolePrivileges">
														<logic:match value="User" name="privilege"
															property="objectId.objectName">
															<logic:match value="1" name="privilege" property="modify">
																<html:link
																	action="/retrivevendor.do?parameter=retriveVendor"
																	paramId="id" paramName="vendorId">
																	Edit</html:link>
															</logic:match>
														</logic:match>
													</logic:iterate> | <logic:iterate id="privilege" name="rolePrivileges">
														<logic:match value="User" name="privilege"
															property="objectId.objectName">
															<logic:match value="1" name="privilege" property="delete">
																<logic:notEqual
																	value="<%=currentUserId
																	.toString()%>"
																	name="vendorlist" property="id">
																	<logic:notEqual
																		value="<%=vendorId
																		.toString()%>"
																		property="id" name="editVendorUserForm">
																		<html:link
																			action="/deletevendor.do?parameter=deleteVendor"
																			paramId="id" paramName="vendorId"
																			onclick="return confirm_delete();">
																			Delete</html:link>
																	</logic:notEqual>
																	<logic:equal
																		value="<%=vendorId
																		.toString()%>"
																		property="id" name="editVendorUserForm">
																		<span style="color: green; font-weight: bold;">User
																			in Edit</span>
																	</logic:equal>
																</logic:notEqual>
																<logic:equal
																	value="<%=currentUserId
																	.toString()%>"
																	name="vendorlist" property="id">
																	<span style="color: red; font-weight: bold;">Current
																		User</span>
																</logic:equal>
															</logic:match>
														</logic:match>
													</logic:iterate></td>
											</tr>
										</logic:iterate>
									</tbody>
								</logic:greaterThan>
								<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
							</table>
						</div>
					</logic:match>
				</logic:match>
			</logic:iterate>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="rolePrivileges">

	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="modify">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to edit user details</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
</div>
</div>
</section>
<script>
	$(document).ready(
			function() {
				
				$(function($) {
					  $("#phonenumber").mask("(999) 999-9999?");
					  $("#fax").mask("(999) 999-9999?");
					  $("#mobile").mask("(999) 999-9999?");
				});
					
				jQuery.validator.addMethod("onlyNumbers", function(value, element) {
					return this.optional(element) || /^[0-9]+$/.test(value)
				}, "Please enter only numbers.");

				jQuery.validator.addMethod("password",function(value, element) {
					return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
				},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
				
				/* $('input[type="text"]').each(function() {
					if ($(this).attr('alt') == 'Optional'
							&& $(this).attr('value') == '') {
						this.value = 'Optional';
					}
				}); */

				$("#vendorForm").validate({

					rules : {
						firstName : {
							required : true
						},
						lastName : {
							required : true
						},
						designation : {
							required : true
						},
						emailId : {
							required : true,
							email : true
						},
						password : {
							required : true,
							password:true
						},
						confirmPassword : {
							required : true,
							equalTo : "#password"
						},
						roleId : {
							required : true
						},
						secId : {
							required : true
						},
						secretQuestionAnswer : {
							required : true,
							rangelength : [ 3, 15 ]
						},
					}
				});
			});
</script>
<script type="text/javascript">
		var config = {
			'.chosen-select' : {
				width : "90%"
			},
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
</script>
