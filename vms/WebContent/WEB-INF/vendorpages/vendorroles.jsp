<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>


<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<!-- Fixed Table header -->
<script type="text/javascript">
<!--
	$(document).ready(function() {

		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
		}, "No Special Characters Allowed.");

		$("#vendorRolesForm").validate({
			rules : {
				roleName : {
					required : true,
					alpha : true,
					minlength : 4
				},
			}
		});

	});

	function backToVendorRole() {
		window.location = "viewvendorrole.do?parameter=viewVendorRoles";
	}
//-->
</script>
<div id="successMsg">
	<span style="color: red;"><html:errors property="referencerole" />
	</span>
	<html:messages id="msg" property="userrole" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Add Vendor Roles
</div>
<logic:present name="rolePrivileges">
	<logic:iterate id="privilege" name="rolePrivileges">
		<logic:equal value="Roles" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="add">

				<!-- 				<div id="sample1" align="center" style="padding: 2% 0 0 30%;"></div> -->

				<div id="sample" class="form-box">
					<html:form action="/addvendorrole.do?parameter=addVendorRole"
						styleId="vendorRolesForm">
						<html:javascript formName="vendorRoleForm" />
						<logic:iterate id="privilege" name="rolePrivileges">
							<logic:match value="Roles" name="privilege"
								property="objectId.objectName">
								<logic:match value="1" name="privilege" property="add">

									<div class="wrapper-half">
										<div class="row-wrapper form-group row">
											<div class="label-col-wrapper">Role Name</div>
											<div class="ctrl-col-wrapper">
												<html:text property="roleName" alt="" styleClass="text-box"
													styleId="roleName" onchange="ajaxFn(this,'VR');" />
											</div>
											<span class="error"><html:errors property="roleName" /></span>
										</div>
									</div>
									<div class="wrapper-btn">
										<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
										<html:reset value="Cancel" styleClass="btn"
											onclick="backToVendorRole();"></html:reset>
									</div>
								</logic:match>
							</logic:match>
						</logic:iterate>
						<logic:iterate id="privilege" name="rolePrivileges">
							<logic:match value="Roles" name="privilege"
								property="objectId.objectName">
								<logic:match value="0" name="privilege" property="add">
									<div style="padding: 5%; text-align: center;">
										<h3>You have no rights to add vendor roles</h3>
									</div>
								</logic:match>
							</logic:match>
						</logic:iterate>
					</html:form>
				</div>
			</logic:match>
			<logic:match value="0" name="privilege" property="add">
				<div style="text-align: center;">
					<h3>You have no rights to add new role</h3>
				</div>
			</logic:match>
		</logic:equal>
	</logic:iterate>
</logic:present>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Vendor Roles
</div>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box">
				<table class="main-table" id="vendorRoleTable">

					<bean:size id="size" name="vendorRoles" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="header">Role Name</td>
								<td class="header">Action</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="vendorRoles" id="vendorRole">
								<tr>
									<td><bean:write name="vendorRole" property="roleName" /></td>
									<bean:define id="id" name="vendorRole" property="id"></bean:define>
									<td align="center"><logic:iterate id="privilege"
											name="rolePrivileges">
											<logic:match value="Roles" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="modify">
													<html:link property="text"
														action="/retrivevendorrole.do?parameter=retrive"
														paramId="id" paramName="id">Edit </html:link>
												</logic:match>
											</logic:match>
										</logic:iterate> | <logic:iterate id="privilege" name="rolePrivileges">
											<logic:match value="Roles" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="delete">
													<html:link action="/deletevendorrole?parameter=delete"
														paramId="id" paramName="id"
														onclick="return confirm_delete();">Delete</html:link>
												</logic:match>
											</logic:match>
										</logic:iterate></td>
								</tr>
							</logic:iterate>
						</tbody>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view vendor roles</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>