<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<html:messages id="msg" property="rfip" message="true">

	<script type="text/javascript">
		alert("Successfully saved  ");
	</script>
</html:messages>
<article>
	<!--Content Area Pane Start Here-->
	<div class="page-wrapper">
		<div id="content-area">

			<div class="item-box-one">
				<div class="box-width">
					<div class="box-title-green">Vendor</div>

					<div class="btn-yellow">
						<bean:define id="vendorId" name="vendorUser"
							property="vendorId.id"></bean:define>
						<html:link
							action="/viewnonprimevendor.do?method=retriveNonPrimeVendor"
							styleClass="icon-14">
							<span class="icon-text">Edit Profile</span>
							<span class="icon-img"><img src="bpimages/profile.png" />
							</span>
						</html:link>

					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!--Content Area Pane End Here-->
</article>
