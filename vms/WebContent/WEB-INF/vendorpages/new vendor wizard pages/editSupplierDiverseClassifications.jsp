<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/validation.js"></script>

<logic:present name="diverseClassification">
	<bean:size id="diverse" name="diverseClassification" />
	<logic:greaterEqual value="0" name="diverse">
		<logic:iterate id="certificate" name="diverseClassification">
			<bean:define id="id" name="certificate" property="certMasterId.id"/>
			<input type="checkbox" value="<%=id.toString()%>" name="diverse" style="display: none;">
		</logic:iterate>
	</logic:greaterEqual>
</logic:present>

<logic:present name="currentUser" property="id">
	<c:set var="action" value="updatediversecertificate" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>

<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimediversecertificate" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
		
	function showBusinessArea() {
		window.location = "${back}.do?parameter=companyOwnerNavigation";
	}
	
	$(document).ready(function() {
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="Diverse Classification Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);					
					$('#step-1 :checkbox, :file').attr('disabled', true);
					$('#step-1 :input').datepicker().datepicker('disable');
				</logic:equal>
					
				<logic:equal value="1" name="privilege1" property="view">	
					document.getElementById("certfile3").onchange = function() {
						document.getElementById("uploadFile3").innerHTML = this.value;
						if($("#certfilespan3").text() == "Renewal")
						{
							$.datepicker._clearDate("#expDate3");
							$("#expDate3").focus();
						}
					};
					
					document.getElementById("certfile2").onchange = function() {
						document.getElementById("uploadFile2").innerHTML = this.value;
						if($("#certfilespan2").text() == "Renewal")
						{
							$.datepicker._clearDate("#expDate2");
							$("#expDate2").focus();
						}
					};
					
					document.getElementById("certfile1").onchange = function() {
						document.getElementById("uploadFile1").innerHTML = this.value;
						if($("#certfilespan1").text() == "Renewal")
						{
							$.datepicker._clearDate("#expDate1");
							$("#expDate1").focus();
						}						
					};
					
					//$('#chkDiverse').attr('checked', 'checked');
					if ($("#chkDiverse").is(':checked')) {
						document.getElementById("diverseClassificationTab").style.display = "block";
						document.getElementById("diverseCertTab").style.display = "block";
					} else {
						document.getElementById("diverseClassificationTab").style.display = "none";
						document.getElementById("diverseCertTab").style.display = "none";
					}
					
					var certificates = [];
					$("input[name='diverse']").each(function() {
						certificates.push($(this).val());
					});
					
					$("input[name='certificates']").each(function() {
						var attrib = $(this).attr("value");
						$.each(certificates, function(i, val) {
							if (attrib == val) {
								$("input[value='" + val + "']")
										.prop('checked', true);
							}
						});
					});
					
					enableEthnicity(document.getElementById("divCertType1"),"ethnicity1");
					enableEthnicity(document.getElementById("divCertType2"),"ethnicity2");
					enableEthnicity(document.getElementById("divCertType3"),"ethnicity3");
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		if($("#expDate1").val() != "" && $("#expDate1").val() != "undefined" && $("#expDate1").val() != null)
		{
			if(certificateExpiryValidation($("#expDate1").val()))
				$("#certfilespan1").text("Renewal");
		}
		
		if($("#expDate2").val() != "" && $("#expDate2").val() != "undefined" && $("#expDate2").val() != null)
		{
			if(certificateExpiryValidation($("#expDate2").val()))
				$("#certfilespan2").text("Renewal");
		}
		
		if($("#expDate3").val() != "" && $("#expDate3").val() != "undefined" && $("#expDate3").val() != null)
		{
			if(certificateExpiryValidation($("#expDate3").val()))
				$("#certfilespan3").text("Renewal");
		}
	});
	
	function certificateExpiryValidation(certExpiryDate)
	{
		var todayDate = new Date();
		var expiryDate = new Date(certExpiryDate);
		
		if(expiryDate <= todayDate)
		{
			return true;
		}
		return false;
	}
	
	function fn_diverseSup() {
		if (document.getElementById("chkDiverse").checked == true) {
			document.getElementById("diverseClassificationTab").style.display = "block";
			document.getElementById("diverseCertTab").style.display = "block";
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
			document.getElementById("diverseCertTab").style.display = "none";
		}
	}
	
	var imgPath = "jquery/images/calendar.gif";
	datePickerExp();
	
	function checkDiverseClassificationType(type, value) {
		var divCertType1 = document.getElementById("divCertType1").value;
		var divCertType2 = document.getElementById("divCertType2").value;
		var divCertType3 = document.getElementById("divCertType3").value;
		if (((divCertType3!="" && divCertType2!="" && divCertType3 == divCertType2))
				|| ((divCertType3!="" && divCertType1!="" && divCertType3 == divCertType1))
				||(divCertType2!="" && divCertType1!="" && divCertType2 == divCertType1)) {
			alert("Classification already exists");
			document.getElementById(type).selectedIndex = 0;
			$("#"+type).select2('open');
			document.getElementById(type).focus();
		} else {
			updateCertificateData(type, value);
		}
	}

	var divCert1=false;
	var divCert2=false;
	var divCert3=false;
	function enableEthnicity(ele, id, row) {		
		divCert1=false;
		divCert2=false;
		divCert3=false;
		var status = ele.value;
		var flag=false;
		<logic:present  name="certificateTypes">
		<bean:size id="size" name="certificateTypes" />
		<logic:greaterEqual value="0" name="size">
		<logic:iterate id="certificateType" name="certificateTypes">
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="isEthinicity" />'){
				flag=true;
			}
		}
		if(row=='1'){
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="certificateUpload" />'){
				divCert1=true;
			}
		}}
		if(row=='2'){
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="certificateUpload" />'){
				divCert2=true;
			}
		}}
		if(row=='3'){
		if (status == '<bean:write name="certificateType" property="id" />') {
			if("1"=='<bean:write name="certificateType" property="certificateUpload" />'){
				divCert3=true;
			}
		}}
		</logic:iterate>
		if(flag){
			$("#"+id).prop('disabled', false);
			$("#"+id).select2();
		}else{
			$("#"+id).prop('disabled', true);
			$("#"+id).val(0);
			$("#"+id).select2();
		}
		</logic:greaterEqual>
		</logic:present>
	}
	
	function updateCertificateData(id, value) {
		if(value!=''){
			$.ajax({
				url : "ajaxclassification.do?method=getClassificationData&" + id
						+ "=" + value,
				type : "POST",
				async : false,
				success : function(data) {
					var split = data.split('|');
					var agency = split[0];
					var certificateType = split[1];
					if (id == "divCertType1") {
						$('#divCertAgen1').find('option').remove().end().append('<option value="">--Select--</option>').append(
								agency);
						$('#certType1').find('option').remove().end().append('<option value="">--Select--</option>').append(
								certificateType);
						$("#certType1").select2();
						$("#divCertAgen1").select2();
					} else if (id == "divCertType2") {
						$('#divCertAgen2').find('option').remove().end().append('<option value="">--Select--</option>').append(
								agency);
						$('#certType2').find('option').remove().end().append('<option value="">--Select--</option>').append(
								certificateType);
						$("#certType2").select2();
						$("#divCertAgen2").select2();
					} else if (id == "divCertType3") {
						$('#divCertAgen3').find('option').remove().end().append('<option value="">--Select--</option>').append(
								agency);
						$('#certType3').find('option').remove().end().append('<option value="">--Select--</option>').append(
								certificateType);
						$("#certType3").select2();
						$("#divCertAgen3").select2();
					}
				}
			});
		}
	}
</script>

<style>
	#moreInfo td {
		padding: 5px;
	}
	
	.swMain div.actionBar {
		z-index: 0 !important;
	}
	
	.swMain .stepContainer .StepTitle {
		z-index: 0 !important;
	}
</style>
<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Diverse Classification Information" name="privilege" property="objectId.objectName">
		<html:hidden property="filelastrowcount" value="3" styleId="filelastrowcount" />
		<html:form action="${action}.do?method=saveDiverseClassification"
			method="post" styleId="diverseForm" enctype="multipart/form-data">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>
			<logic:equal value="1" name="privilege" property="view">				
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer" >
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"/>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit1" name="saveandexit1">
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit1" name="submit1">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=serviceAreaNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showBusinessArea();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit" name="submit">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>								
							</div>
							<div id="step-1" class="content">
								<header class="mt-1">
									<h2 class="card-title text-center pb-2">Diverse Classification</h2>
								</header>
								<div class="row-wrapper form-group row">
								<label class="col-sm-2 label-col-wrapper control-label text-sm-right">
									Diverse Supplier</label>
									<div class="col-sm-7 ctrl-col-wrapper">
									<html:checkbox property="deverseSupplier" styleId="chkDiverse" onclick="fn_diverseSup()" tabindex="104" />
									</div>
								</div>
								<div id="diverseClassificationTab" class="panelCenter_1" style="display: none;">
									
									<div class="form-box card-body" style="padding: 1%;">
										<h3>Diversity Information</h3>
										<p style="font-weight: bold;">Diversity/Small Business Classifications - check all that apply</p>
										<logic:notEmpty name="certificateTypes">
											<bean:size id="size1" name="certificateTypes" />
											<div class="coulmn-1-row">
												<logic:greaterEqual value="0" name="size1">
													<logic:iterate id="certificate" name="certificateTypes">
														<bean:define id="id" name="certificate" property="id"></bean:define>
														<div class="coulmn-2">
															<html:checkbox property="certificates" value="<%=id.toString()%>" tabindex="105">
																<bean:write name="certificate" property="certificateName"></bean:write>
															</html:checkbox>
														</div>
													</logic:iterate>
												</logic:greaterEqual>
											</div>
										</logic:notEmpty>
									</div>
								</div>
								<div class="clear"></div>
								<div id="diverseCertTab" style="display: none; padding-top: 2%;" class="panelCenter_1">
									<h3>Upload Diverse Certificates </h3>
								
									<table style="background: #fff;" class="main-table table table-bordered table-striped mb-0">
									<thead>
										<tr>
											<td class="">Classification</td>
											<td class="">Agency</td>
											<td class="">Certification Type</td>
											<td style="width:30px;">Certification #</td>
											<td class="">Effective Date</td>
											<td class="">Expire Date</td>
											<td class="">Certificate</td>
										</tr>
										</thead>
										<tr class="even" id="TemplateRow1">
											<td><html:hidden property="vendorCertID1" styleId="vendorCertID1"/> <html:select
													property="divCertType1" styleClass="chosen-select"
													styleId="divCertType1" tabindex="106" style="max-width:150px;"
													onchange="checkDiverseClassificationType('divCertType1',this.value);enableEthnicity(this,'ethnicity1','1');">
													<html:option value="">--Select--</html:option>
													<logic:notEmpty name="certificateTypes">
														<logic:iterate id="certificate" name="certificateTypes">
															<bean:define id="id" name="certificate" property="id"></bean:define>
															<html:option value="<%=id.toString()%>">
																<bean:write name="certificate" property="certificateName"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:notEmpty>
													</html:select> <span class="error"> <html:errors property="divCertType"></html:errors></span>
											</td>
											<td><html:select property="divCertAgen1" tabindex="107"
													styleClass="chosen-select" styleId="divCertAgen1" style="max-width:200px;">
													<html:option value="">--Select--</html:option>
													<logic:notEmpty property="certAgencies1" name="editVendorMasterForm">
														<logic:iterate id="certAgency" property="certAgencies1" name="editVendorMasterForm">
															<bean:define id="id" name="certAgency" property="id"></bean:define>
															<html:option value="${id}">
																<bean:write name="certAgency" property="agencyName"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:notEmpty>
													</html:select> <span class="error"> <html:errors property="divCertAgen"></html:errors></span>
											</td>
											<td><html:select property="certType1" styleId="certType1"
													styleClass="chosen-select" tabindex="108" style="position:relative;top:-8px; max-width:100px;">
													<html:option value="">---- Select ----</html:option>
														<logic:notEmpty property="certificateTypes1" name="editVendorMasterForm">
															<logic:iterate id="certType" property="certificateTypes1" name="editVendorMasterForm">
																<bean:define id="id" name="certType" property="id"></bean:define>
																<html:option value="${id}">
																	<bean:write name="certType" property="certificateTypeDesc"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:notEmpty>
												</html:select>
											</td>
											<td><html:text property="certificationNo1" tabindex="109" style="position:relative;top:-8px;"
													styleId="certificationNo1" alt="" styleClass="main-text-box" />
											</td>
											<td><html:text property="effDate1" styleId="effDate1" alt="Please click to select date"
													readonly="true"
													styleClass="main-text-box" tabindex="110" style="position:relative;top:-8px;" />
											</td>
											<td><html:text property="expDate1" styleId="expDate1" alt="Please click to select date"
													readonly="true"
													styleClass="main-text-box" tabindex="111" /> <span class="error">
													<html:errors property="expDate"></html:errors></span>
											</td>
											<td>
												<div class="fileUpload btn btn-primary">
													<span id="certfilespan1">Browse</span>
													<html:file property="certFile1" styleId="certfile1" tabindex="112"
														styleClass="upload">
													</html:file>
												</div> <span id="uploadFile1"></span> <span class="error"> <html:errors
														property="certFile"></html:errors></span>
											</td>
										</tr>
										<tr class="even">
											<td colspan="5">
												<div class="row-wrapper" style="width: 50%">
													<div class="label-col-wrapper">Ethnicity</div>
													<div class="ctrl-col-wrapper">
														<html:select property="ethnicity1" styleId="ethnicity1"
															styleClass="chosen-select-width" tabindex="117"
															style="height: 2%;width:80%;" >
															<html:option value="0">--Select--</html:option>
															<html:optionsCollection name="ethnicities" label="ethnicity" value="id" />
														</html:select>
													</div>
												</div>
											</td>
											<td><logic:present property="certFile1Id"
													name="editVendorMasterForm">
													<logic:notEqual value="0" property="certFile1Id"
														name="editVendorMasterForm">
														<bean:define id="id" property="certFile1Id"
															name="editVendorMasterForm"></bean:define>
															<html:hidden property="certFile1Id"/>
														<html:link href="download.jsp" paramId="id" paramName="id"
															styleId="downloadFile1" styleClass="downloadFile" target="_blank">Download</html:link>
													</logic:notEqual>
												</logic:present>
											</td>
											<td>
												<logic:equal value="1" name="privilege" property="modify">
													<logic:equal value="1" name="privilege" property="delete">
														<a onclick="return clearCertificate('1');" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a>
													</logic:equal>
												</logic:equal>
											</td>
										</tr>
										<tr class="even" id="TemplateRow1">
											<td><html:hidden property="vendorCertID2" styleId="vendorCertID2"/> <html:select
													property="divCertType2" tabindex="113" styleClass="chosen-select"
													styleId="divCertType2" style="max-width:150px;"
													onchange="checkDiverseClassificationType('divCertType2',this.value);enableEthnicity(this,'ethnicity2','2');">
													<html:option value="">--Select--</html:option>
													<logic:notEmpty name="certificateTypes">
														<logic:iterate id="certificate" name="certificateTypes">
															<bean:define id="id" name="certificate" property="id"></bean:define>
															<html:option value="<%=id.toString()%>">
																<bean:write name="certificate" property="certificateName"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:notEmpty>
												</html:select>
											</td>
											<td><html:select property="divCertAgen2" tabindex="114"
													styleClass="chosen-select" styleId="divCertAgen2" style="max-width:200px;"
													onchange="checkDiverseAgent()">
													<html:option value="">--Select--</html:option>
														<logic:notEmpty property="certAgencies2" name="editVendorMasterForm">
															<logic:iterate id="certAgency" property="certAgencies2" name="editVendorMasterForm">
																<bean:define id="id" name="certAgency" property="id"></bean:define>
																<html:option value="<%=id.toString()%>">
																	<bean:write name="certAgency" property="agencyName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:notEmpty>
												</html:select>
											</td>
											<td><html:select property="certType2" styleId="certType2"
													styleClass="chosen-select" tabindex="115" style="max-width:100px;">
													<html:option value="">---- Select ----</html:option>
													<logic:notEmpty property="certificateTypes2" name="editVendorMasterForm">
														<logic:iterate id="certType" property="certificateTypes2" name="editVendorMasterForm">
															<bean:define id="id" name="certType" property="id"></bean:define>
															<html:option value="<%=id.toString()%>">
																<bean:write name="certType" property="certificateTypeDesc"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:notEmpty>
												</html:select>
											</td>
											<td><html:text property="certificationNo2" tabindex="116"
													styleId="certificationNo2" alt="" styleClass="main-text-box" />
											</td>
											<td><html:text property="effDate2" styleId="effDate2" alt="Please click to select date"
													readonly="true" styleClass="main-text-box" tabindex="94" />
											</td>
											<td><html:text property="expDate2" styleId="expDate2" alt="Please click to select date"
													readonly="true" styleClass="main-text-box" tabindex="95" />
											</td>
											<td>
												<div class="fileUpload btn btn-primary">
													<span id="certfilespan2">Browse</span>
													<html:file property="certFile2" styleId="certfile2" tabindex="117"
														styleClass="upload">
													</html:file>
												</div> <span id="uploadFile2"></span>
											</td>
										</tr>
										<tr class="even">
											<td colspan="5">
												<div class="row-wrapper" style="width: 50%">
													<div class="label-col-wrapper">Ethnicity</div>
													<div class="ctrl-col-wrapper">
														<html:select property="ethnicity2" styleId="ethnicity2"
															styleClass="chosen-select-width" tabindex="117"
															style="height: 2%;width:80%;" >
															<html:option value="0">--Select--</html:option>
															<html:optionsCollection name="ethnicities" label="ethnicity"
																value="id" />
														</html:select>
													</div>
												</div>
											</td>
											<td><logic:present property="certFile2Id"
													name="editVendorMasterForm">
													<logic:notEqual value="0" property="certFile2Id"
														name="editVendorMasterForm">
														<bean:define id="id" property="certFile2Id"
															name="editVendorMasterForm"></bean:define>
															<html:hidden property="certFile2Id"/>
														<html:link href="download.jsp" paramId="id" paramName="id"
															styleId="downloadFile2" styleClass="downloadFile" target="_blank">Download</html:link>
													</logic:notEqual>
												</logic:present>
											</td>
											<td>
												<logic:equal value="1" name="privilege" property="modify">
													<logic:equal value="1" name="privilege" property="delete">
														<a onclick="return clearCertificate('2');" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a>
													</logic:equal>
												</logic:equal>
											</td>												
										</tr>
										<tr class="even" id="TemplateRow1">
											<td><html:hidden property="vendorCertID3" styleId="vendorCertID3" /> <html:select
													property="divCertType3" tabindex="118" styleClass="chosen-select"
													styleId="divCertType3" style="max-width:150px;"
													onchange="checkDiverseClassificationType('divCertType3',this.value);enableEthnicity(this,'ethnicity3','3');">
													<html:option value="">--Select--</html:option>
													<logic:notEmpty name="certificateTypes">
														<logic:iterate id="certificate" name="certificateTypes">
															<bean:define id="id" name="certificate" property="id"></bean:define>
															<html:option value="<%=id.toString()%>">
																<bean:write name="certificate" property="certificateName"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:notEmpty>
												</html:select>
											</td>
											<td><html:select property="divCertAgen3" tabindex="119"
													styleClass="chosen-select" styleId="divCertAgen3" style="max-width:200px;"
													onchange="checkDiverseAgent()">
													<html:option value="">--Select--</html:option>
														<logic:notEmpty property="certAgencies3" name="editVendorMasterForm">
															<logic:iterate id="certAgency" property="certAgencies3" name="editVendorMasterForm">
																<bean:define id="id" name="certAgency" property="id"></bean:define>
																<html:option value="<%=id.toString()%>">
																	<bean:write name="certAgency" property="agencyName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:notEmpty>
												</html:select>
											</td>
											<td><html:select property="certType3" styleId="certType3" style="max-width:100px;"
													styleClass="chosen-select" tabindex="120">
													<html:option value="">---- Select ----</html:option>
													<logic:notEmpty property="certificateTypes3" name="editVendorMasterForm">
														<logic:iterate id="certType" property="certificateTypes3" name="editVendorMasterForm">
															<bean:define id="id" name="certType" property="id"></bean:define>
															<html:option value="<%=id.toString()%>">
																<bean:write name="certType" property="certificateTypeDesc"></bean:write>
															</html:option>
														</logic:iterate>
													</logic:notEmpty>
												</html:select>
											</td>
											<td><html:text property="certificationNo3" tabindex="121"
													styleId="certificationNo3" alt="" styleClass="main-text-box" />
											</td>
											<td><html:text property="effDate3" styleId="effDate3" alt="Please click to select date"
													readonly="true" styleClass="main-text-box" tabindex="122" />
											</td>
											<td><html:text property="expDate3" styleId="expDate3" alt="Please click to select date"
													readonly="true" styleClass="main-text-box" tabindex="123" />
											</td>
											<td>
												<div class="fileUpload btn btn-primary">
													<span id="certfilespan3">Browse</span>
													<html:file property="certFile3" styleId="certfile3" tabindex="124"
														styleClass="upload">
													</html:file>
												</div> <span id="uploadFile3" style="overflow: hidden;"></span>
											</td>
										</tr>
										<tr class="even">
											<td colspan="5">
												<div class="row-wrapper" style="width: 50%">
													<div class="label-col-wrapper">Ethnicity</div>
													<div class="ctrl-col-wrapper">
														<html:select property="ethnicity3" styleId="ethnicity3"
															styleClass="chosen-select-width" tabindex="117"
															style="height: 2%;width:80%;" >
															<html:option value="0">--Select--</html:option>
															<html:optionsCollection name="ethnicities" label="ethnicity"
																value="id" />
														</html:select>
													</div>
												</div>
											</td>
											<td><logic:present property="certFile3Id"
													name="editVendorMasterForm">
													<logic:notEqual value="0" property="certFile3Id"
														name="editVendorMasterForm">
														<bean:define id="id" property="certFile3Id"
															name="editVendorMasterForm"></bean:define>
															<html:hidden property="certFile3Id"/>
														<html:link href="download.jsp" paramId="id" paramName="id"
															styleId="downloadFile3" styleClass="downloadFile" target="_blank">Download</html:link>
													</logic:notEqual>
												</logic:present>
											</td>
											<td>
												<logic:equal value="1" name="privilege" property="modify">
													<logic:equal value="1" name="privilege" property="delete">
														<a onclick="return clearCertificate('3');" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a>
													</logic:equal>
												</logic:equal>
											</td>
										</tr>
									</table>
								</div>
								<div class="clear"></div>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit2" name="saveandexit2">
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3" name="submit3">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=serviceAreaNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>								
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showBusinessArea();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer" >
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"/>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>			
								<html:link action="/${back}.do?parameter=serviceAreaNavigation" styleClass="btn">Next</html:link>					
								<input type="button" class="btn" value="Back" onclick="showBusinessArea();">								
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">Diverse Classification</h2>
								<br/>
								<h3>You have no rights to view diverse classification information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=serviceAreaNavigation" styleClass="btn">Next</html:link>
								<input type="button" class="btn" value="Back" onclick="showBusinessArea();">								
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
		</html:form>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		$("#menu5").removeClass().addClass("current");
	});
	
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});

	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function() {
		$("#diverseForm").validate({
			rules : {
				
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					if (validateNaics()){
						$('#diverseForm').append("<input type='hidden' name='submitType' value='submit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else {
						$("#ajaxloader").css('display','none');
					}
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					if (validateNaics()){
						$('#diverseForm').append("<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();	
					} else {
						$("#ajaxloader").css('display','none');
					}					
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					if (validateNaics()){
						$('#diverseForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();	
					} else {
						$("#ajaxloader").css('display','none');
					}					
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});	
</script>