<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="jquery/js/additional-methods.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js">
<logic:present name="currentUser" property="id">
	<c:set var="action" value="updateownership" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimeownership" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}			
		}
	});
		
	function companyOwnershipChecked(id, value) {
		$('#' + id).val(value);
	}
	
	function showAddressInfo() {
		window.location = "${back}.do?parameter=companyAddressNavigation";
	}
</script>

<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Company Ownership Information" name="privilege" property="objectId.objectName">
		<html:form action="${action}.do?method=saveOwnerInformation" method="post" styleId="ownerForm">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>
			<logic:equal value="1" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn btn-primary mr-1" value="Save and Exit" id="saveandexit1" name="saveandexit1">
									<input type="submit" class="btn btn btn-primary mr-1" value="Next" id="submit1" name="submit1">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=diverseClassificationNavigation" styleClass="btn btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn btn-primary mr-1" value="Back" onclick="showAddressInfo();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn btn-primary mr-1" value="Save" id="submit" name="submit">
									<input type="reset" class="btn btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
							<div id="step-1" class="content" style="display: block;">
								<header class="mt-1">
									<h2 class="card-title text-center">Ownership Information</h2>
								</header>
								<div class="panelCenter_1" style="padding-top: 1%;">									
									<div class="form-box card-body">
										<header>
											<h3 class="card-title pt-1 pb-2">Company Ownership</h3>
										</header>
										<h4 style="padding: 1%;">
											<html:radio property="companyOwnership" value="ownerPublic"
												idName="editVendorMasterForm" styleId="ownerPublic"
												onclick="companyOwnershipChecked('ownerPublic','1')">Publicly Traded </html:radio>
											<html:radio property="companyOwnership" value="ownerPrivate"
												idName="editVendorMasterForm" styleId="ownerPrivate"
												onclick="companyOwnershipChecked('ownerPrivate','2')">Privately Owned </html:radio>
										</h4>
										<div id="ownerInfo">
											<div class="panelCenter_1">
												<h3>Owner 1</h3>
												<div class="form-box card-body">
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Name</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerName1"
																	alt="" styleId="ownerName1" tabindex="58" />
																<html:hidden property="ownerId1" alt="" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerTitle1"
																	alt="" styleId="ownerTitle1" tabindex="59" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">E-mail</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerEmail1"
																	alt="" styleId="ownerEmail1" tabindex="60" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerPhone1"
																	alt="" styleId="ownerPhone1" tabindex="61" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Extension</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerExt1"
																	alt="Optional" styleId="ownerExt1" tabindex="62" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerMobile1"
																	alt="Optional" styleId="ownerMobile1" tabindex="63" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:select property="ownerGender1" styleId="ownerGender1"
																	styleClass="chosen-select-width form-control" tabindex="64">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="M">Male</html:option>
																	<html:option value="F">Female</html:option>
																	<html:option value="T">Transgender</html:option>
																</html:select>
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Owner Ethnicity</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:select property="ownerEthnicity1"
																	styleId="ownerEthnicity1" styleClass="chosen-select-width form-control"
																	style="height: 2%;" tabindex="65">
																	<html:option value="">-----Select-----</html:option>
																	<bean:size id="size" name="ethnicities" />
																	<logic:greaterEqual value="0" name="size">
																		<logic:iterate id="ethni" name="ethnicities">
																			<bean:define id="name" name="ethni" property="id"></bean:define>
																			<html:option value="<%=name.toString()%>">
																				<bean:write name="ethni" property="ethnicity" />
																			</html:option>
																		</logic:iterate>
																	</logic:greaterEqual>
																</html:select>
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">% Ownership</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerOwnership1"
																	alt="" styleId="ownerOwnership1" tabindex="66" />
															</div>
														</div>
													
												</div>
												<div class="clear"></div>
											</div>
											<div class="panelCenter_1" style="padding-top: 2%;">
												<h3>Owner 2</h3>
												<div class="form-box card-body">
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Name</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerName2"
																	alt="Optional" styleId="ownerName2" tabindex="67" />
																<html:hidden property="ownerId2" alt="" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerTitle2"
																	alt="Optional" styleId="ownerTitle2" tabindex="68" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">E-mail</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerEmail2"
																	alt="Optional" styleId="ownerName2" tabindex="69" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerPhone2"
																	alt="" styleId="ownerPhone2" tabindex="70" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Extension</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerExt2"
																	alt="Optional" styleId="ownerExt2" tabindex="71" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerMobile2"
																	alt="Optional" styleId="ownerMobile2" tabindex="72" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:select property="ownerGender2" styleId="ownerGender2"
																	styleClass="chosen-select-width form-control" tabindex="73">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="M">Male</html:option>
																	<html:option value="F">Female</html:option>
																	<html:option value="T">Transgender</html:option>
																</html:select>
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Owner Ethnicity</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:select property="ownerEthnicity2"
																	styleId="ownerEthnicity2" styleClass="chosen-select-width form-control"
																	style="height: 2%;" tabindex="74">
																	<html:option value="">-----Select-----</html:option>
																	<bean:size id="size" name="ethnicities" />
																	<logic:greaterEqual value="0" name="size">
																		<logic:iterate id="ethni" name="ethnicities">
																			<bean:define id="name" name="ethni" property="id"></bean:define>
																			<html:option value="<%=name.toString()%>">
																				<bean:write name="ethni" property="ethnicity" />
																			</html:option>
																		</logic:iterate>
																	</logic:greaterEqual>
																</html:select>
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">% Ownership</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerOwnership2"
																	alt="Optional" styleId="ownerOwnership2" tabindex="75" />
															</div>
														</div>
													
												</div>
												<div class="clear"></div>
											</div>
											<div class="panelCenter_1" style="padding-top: 2%;">
												<h3>Owner 3</h3>
												<div class="form-box card-body">
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Name</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerName3"
																	alt="Optional" styleId="ownerName3" tabindex="76" />
																<html:hidden property="ownerId3" alt="" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerTitle3"
																	alt="Optional" styleId="ownerTitle3" tabindex="77" />
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">E-mail</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerEmail3"
																	alt="Optional" styleId="ownerEmail3" tabindex="78" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerPhone3"
																	alt="" styleId="ownerPhone3" tabindex="79" />
															</div>
														</div>													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Extension</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerExt3"
																	alt="Optional" styleId="ownerExt3"  tabindex="80" />
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerMobile3"
																	alt="Optional" styleId="ownerMobile3" tabindex="81" />
																</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:select property="ownerGender3" styleId="ownerGender3"
																	styleClass="chosen-select-width form-control" tabindex="82">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="M">Male</html:option>
																	<html:option value="F">Female</html:option>
																	<html:option value="T">Transgender</html:option>
																</html:select>
															</div>
														</div>
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Owner Ethnicity</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:select property="ownerEthnicity3"
																	styleId="ownerEthnicity3" styleClass="chosen-select-width form-control"
																	style="height: 2%;" tabindex="83">
																	<html:option value="">-----Select-----</html:option>
																	<bean:size id="size" name="ethnicities" />
																	<logic:greaterEqual value="0" name="size">
																		<logic:iterate id="ethni" name="ethnicities">
																			<bean:define id="name" name="ethni" property="id"></bean:define>
																			<html:option value="<%=name.toString()%>">
																				<bean:write name="ethni" property="ethnicity" />
																			</html:option>
																		</logic:iterate>
																	</logic:greaterEqual>
																</html:select>
															</div>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">% Ownership</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="ownerOwnership3"
																	alt="Optional" styleId="ownerOwnership3" tabindex="84" />
															</div>
														</div>
													
												</div>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit2" name="saveandexit2"> 
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3" name="submit3">	
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=diverseClassificationNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>								
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showAddressInfo();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>								
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=diverseClassificationNavigation" styleClass="btn">Next</html:link>								
								<input type="button" class="btn" value="Back" onclick="showAddressInfo();">								
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">Ownership Information</h2>
								<br/>
								<h3>You have no rights to view company ownership information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=diverseClassificationNavigation" styleClass="btn">Next</html:link>
								<input type="button" class="btn" value="Back" onclick="showAddressInfo();">
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
		</html:form>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="Company Ownership Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);
					$('#step-1 :radio').attr('disabled', true);		
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		$("#menu4").removeClass().addClass("current");
		
		if($("#ownerExt1").val() === 'Optional'){
			$("#ownerExt1").val('');
		}
		
		if($("#ownerName2").val() === 'Optional'){
			$("#ownerName2").val('');
		}
		
		if($("#ownerTitle2").val() === 'Optional'){
			$("#ownerTitle2").val('');
		}
		
		if($("#ownerEmail2").val() === 'Optional'){
			$("#ownerEmail2").val('');
		}
		
		if($("#ownerExt2").val() === 'Optional'){
			$("#ownerExt2").val('');
		}
		
		if($("#ownerOwnership2").val() === 'Optional'){
			$("#ownerOwnership2").val('');
		}
		
		if($("#ownerName3").val() === 'Optional'){
			$("#ownerName3").val('');
		}
		
		if($("#ownerTitle3").val() === 'Optional'){
			$("#ownerTitle3").val('');
		}
		
		if($("#ownerEmail3").val() === 'Optional'){
			$("#ownerEmail3").val('');
		}
		
		if($("#ownerExt3").val() === 'Optional'){
			$("#ownerExt3").val('');
		}
		
		if($("#ownerOwnership3").val() === 'Optional'){
			$("#ownerOwnership3").val('');
		}
	});

	$(function($) {
		$("#ownerPhone1").mask("(999) 999-9999?");
		$("#ownerPhone2").mask("(999) 999-9999?");
		$("#ownerPhone3").mask("(999) 999-9999?");
		$("#ownerMobile1").mask("(999) 999-9999?");
		$("#ownerMobile2").mask("(999) 999-9999?");
		$("#ownerMobile3").mask("(999) 999-9999?");
	});
	
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z0-9 \.]+$/.test(value);
	}, "No Special Characters Allowed.");
	
	jQuery.validator.addMethod("onlyNumbers", function(value, element) {
		return this.optional(element) || /^[0-9]+$/.test(value);
	}, "Please enter only numbers.");
	
	jQuery.validator.addMethod("allowhyphens", function(value, element) {
		return this.optional(element) || /^[0-9-]+$/.test(value);
	}, "Please enter valid numbers.");

	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function() {

		$("#ownerForm").validate({
			rules : {
				ownerName1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					},
					alpha : true
				},
				ownerTitle1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					}
				},
				ownerEmail1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					},
					email : true
				},
				ownerPhone1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					}
				},
				ownerExt1 : {
					onlyNumbers : true
				},
				ownerExt2 : {
					onlyNumbers : true
				},
				ownerExt3 : {
					onlyNumbers : true
				},
				ownerGender1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					}
				},
				ownerOwnership1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					},
					range : [ 0, 100 ]
				},
				ownerEthnicity1 : {
					required : function(element) {
						if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
							if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
									|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
								return true;
							} else {
								return false;
							}
							return false;
						} else {
							return false;
						}
					}
				},
				ownerEmail2 : {
					email : true,
				},
				ownerOwnership2 : {
					range : [ 0, 100 ]
				},
				ownerEmail3 : {
					email : true,
				},
				ownerOwnership3 : {
					range : [ 0, 100 ]
				},
				ownerName1EmailId : {
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					},
					email : true
				},
				ownerName2 : {
					alpha : true
				},
				ownerName3 : {
					alpha : true
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					$('#ownerForm').append("<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					$('#ownerForm').append("<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#ownerForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
</script>