<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.fg.vms.util.CommonUtils"%>
<%@ page import="com.fg.vms.customer.model.VendorOtherCertificate"%>
<%@ page import="com.fg.vms.customer.model.CustomerVendornetworldCertificate"%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<logic:present name="currentUser" property="id">
	<c:set var="action" value="updateothercertificate" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimeothercertificate" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
		
	function showReferences() {
		window.location = "${back}.do?parameter=referencesNavigation";
	}
	
	$(document).ready(function() {
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="Quality/Other Certifications Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);
					$('#step-1 :input').datepicker().datepicker('disable');							
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		if(document.getElementById("otherCert1") != null){
			document.getElementById("otherCert1").onchange = function() {
				document.getElementById("otherFile1").innerHTML = this.value;
			};
		}
		
		$("#otherCert1").on("change",function() {
			document.getElementById("otherFile1").innerHTML = this.value;
		});		

		if(document.getElementById("otherCert2") != null){
			document.getElementById("otherCert2").onchange = function() {
				document.getElementById("otherFile2").innerHTML = this.value;
			};
		}
		
		$("#otherCert2").on("change",function() {
			document.getElementById("otherFile2").innerHTML = this.value;
		});	
		
		if($("#otherCertType1").val() === "-1")
			$("#othexpDate1").val('');

		$("#yesnet").hide();
		$("#yesnetworld").click(function() {
			
			$('#nonetworld').attr('checked',false);
		    $("#yesnet").show();
		  });
		$("#nonetworld").click(function() {
			
			$('#yesnetworld').attr('checked',false);
		    $("#yesnet").hide();
		  });
		  
	});
	
	var imgPath = "jquery/images/calendar.gif";
	datePickerOtherExp();
	function checkDiverseType(element) 
	{
		var rowCount = $('#otherCertificate tr').length - 1;
		for ( var i = 1; i <= rowCount; i++) 
		{
			var id = "otherCertType" + i;
			if (id == element) {
			} else {
				var v1 = $('#' + id).val();
				var v2 = $('#' + element).val();
				if (v1 == v2) {
					alert("Certificate should be unique");
					document.getElementById(element).selectedIndex = 0;
				}
			}
		}
	}
	
	function addRow(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = 6;//table.rows[1].cells.length;
		for ( var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[1].cells[i].innerHTML;
			if (i == 2) {
				newcell.innerHTML = '<div class="fileUpload btn btn-primary"><span>Browse</span>'
						+ '	<input name="otherCert['+(rowCount-1)+']" type="file" id="otherCert'
						+ rowCount
						+ '" class="upload"/> </div> <span id="otherFile'+rowCount+'"></span>';
				$("#otherCert" + rowCount).on("change",	function() {
									document.getElementById("otherFile"
											+ rowCount).innerHTML = this.value;
								});
			}else if(i==0) {
				newcell.innerHTML = '<select name="otherCertType1" id="otherCertType'+ rowCount+'" class="chosen-select fields" >'
				+ $('#otherCertType1').html() + '</select>';
				newcell.childNodes[0].selectedIndex = 0;
				//newcell.childNodes[0].id = "otherCertType" + rowCount;
				$('#otherCertType'+rowCount).select2();
			}else if(i==5){
				newcell.innerHTML = '<a onclick="return clearOtherCertificate(\''+rowCount+'\',\'0\')" href="#"><img src="images/deleteicon.jpg" alt="Delete" /></a>';
			}else if(i==4){
				newcell.innerHTML = '';
			}else if(i==1){
				newcell.innerHTML = " <input class='othexpDate1 main-text-box ' id='othexpDate"+rowCount+"' alt='Please click to select date' readonly='readonly' name='otherExpiryDate' style='width:45%;'/>";
				newcell.childNodes[0].value = "";
			}else if(i==3){
				newcell.innerHTML = '';
			}
		}
	}
	
	$('body').on('focus', ".othexpDate1", function() {
		$(this).datepicker();
	});
</script>

<style>
	<!--
	.swMain div.actionBar {
		z-index: 0 !important;
	}
	.swMain .stepContainer .StepTitle {
		z-index: 0 !important;
	}
	-->
</style>

<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Quality/Other Certifications Information" name="privilege" property="objectId.objectName">
		<html:form action="${action}.do?method=saveOtherCertificates"
			method="post" styleId="otherCertForm" enctype="multipart/form-data">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="netmsg" property="networld" message="true">
					<div class="alert alert-info nomargin"><bean:write name="netmsg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<span><bean:write name="msg" /></span>
				</html:messages>
					<html:messages id="netmsg" property="transactionFailure" message="true">
					<div class="alert alert-info nomargin"><bean:write name="netmsg" /></div>
				</html:messages>
			</div>
			<logic:equal value="1" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-priary mr-1" value="Save and Exit" id="saveandexit1" name="saveandexit1">
									<input type="submit" class="btn btn-priary mr-1" value="Next" id="submit1" name="submit1">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=documentsNavigation" styleClass="btn">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-priary mr-1" value="Back" onclick="showReferences();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn btn-priary mr-1" value="Save" id="submit" name="submit">
									<input type="reset" class="btn btn-priary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
							<div id="step-1" class="content">
								<header class="mt-1 card-header">
									<h2 class="card-title">Quality/Other Certifications</h2>
								</header>
								<table style="background:#fff;" class="main-table table table-bordered table-striped mb-1" id="otherCertificate">
									<tr>
										<td class="">Certification Type</td>
										<td class="">Expire Date</td>
										<td class="">Certificate</td>
										<td class="">Is this Avetta certification?</td>
										<td class=""></td>
										<td class="">Action</td>
									</tr>
									<logic:present name="otherCertificates">
										<bean:size id="size" name="otherCertificates" />
										<logic:equal value="0" name="size">
											<tr>
												<td><html:select styleId="otherCertType1" tabindex="127"
														property="otherCertType1" onchange="checkDiverseType(this.id);"
														styleClass="chosen-select form-control">
														<html:option value="-1">----- Select -----</html:option>
														<logic:present name="qualityCertificates">
															<logic:iterate id="qualityCertificate" name="qualityCertificates">
																<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
																<html:option value="${id}">
																	<bean:write name="qualityCertificate"
																		property="certificateName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</td>
												<td><html:text property="otherExpiryDate" styleId="othexpDate1" readonly="true"
														styleClass="othexpDate1 main-text-box form-control" alt="Please click to select date" tabindex="128" />
												</td>
												<td>
													<div class="fileUpload btn btn-primary">
														<span>Browse</span>
														<html:file property="otherCert[0]" styleId="otherCert1" tabindex="129" styleClass="upload form-control"></html:file>
													</div> 
													<span id="otherFile1"></span>
												</td>
												<td align="center">
												<label for=""></label>
													<html:checkbox property="isPics" styleId="isPics" disabled="true" value="1"/>
												</td>
												<td></td>
												<td>
													<logic:equal value="1" name="privilege" property="modify">
														<logic:equal value="1" name="privilege" property="delete">
															<a onclick="return clearOtherCertificate('1','0');" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a>
														</logic:equal>
													</logic:equal>
												</td>
											</tr>
										</logic:equal>
									</logic:present>
									<logic:notPresent name="otherCertificates">
										<tr>
											<td><html:select styleId="otherCertType1" tabindex="127"
														property="otherCertType1" onchange="checkDiverseType(this.id);"
														styleClass="chosen-select">
														<html:option value="-1">----- Select -----</html:option>
														<logic:present name="qualityCertificates">
															<logic:iterate id="qualityCertificate" name="qualityCertificates">
																<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
																<html:option value="${id}">
																	<bean:write name="qualityCertificate"
																		property="certificateName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
											</td>
											<td><html:text property="otherExpiryDate" styleId="othexpDate1" readonly="true"
													styleClass="othexpDate1 main-text-box" alt="" tabindex="128" style="width:45%;" />
											</td>
											<td>
												<div class="fileUpload btn btn-primary">
													<span>Browse</span>
													<html:file property="otherCert[0]" styleId="otherCert1" tabindex="129" styleClass="upload"></html:file>
												</div> 
												<span id="otherFile1"></span>
											</td>
											<td></td>
											<td></td>
											<td>
												<logic:equal value="1" name="privilege" property="modify">
													<logic:equal value="1" name="privilege" property="delete">
														<a onclick="return clearOtherCertificate('1','0');" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a>
													</logic:equal>
												</logic:equal>
											</td>
										</tr>
									</logic:notPresent>
									<logic:present name="otherCertificates">
										<logic:iterate id="otherCert" name="otherCertificates" indexId="index">
											<tr>
												<%
													int i = index + 1;
													String certType = "-1";
												%>
												<logic:greaterThan value="0" name="otherCert" property="certificationType">
													<bean:define id="type" name="otherCert" property="certificationType"></bean:define>
													<%
														certType = type.toString();
													%>
												</logic:greaterThan>
												<td><html:select onchange="checkDiverseType(this.id);"
														styleId='<%="otherCertType" + i%>' property="otherCertType1"
														styleClass="chosen-select" value="<%=certType%>">
														<html:option value="-1">----- Select -----</html:option>
														<logic:present name="qualityCertificates">
															<logic:iterate id="qualityCertificate" name="qualityCertificates">
																<bean:define id="id" name="qualityCertificate" property="id"></bean:define>
																<html:option value="<%=id.toString()%>">
																	<bean:write name="qualityCertificate"
																		property="certificateName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</td>
												<%
													String date = CommonUtils.convertDateToString(((VendorOtherCertificate) otherCert).getExpiryDate());
												%>
												<td><html:text property="otherExpiryDate" styleId='<%="othexpDate" + i%>'
														value="<%=date.toString()%>" readonly="true"
														styleClass="othexpDate1 main-text-box" alt="" style="width:45%;" />
												</td>
												<td>
													<div class="fileUpload btn btn-primary">
														<span>Browse</span>
														<html:file property='<%="otherCert[" + index+"]"%>' styleId='<%="otherCert" + i%>' tabindex="130" styleClass="upload"></html:file>
														<script>
															$("#otherCert" + <%=i%>).on("change", function() {
																document.getElementById("otherFile" + <%=i%>).innerHTML = this.value;
															});
														</script>
													</div> 
													<span id='<%="otherFile" + i%>'></span>
												</td>
												<td align="center">
													<logic:present name="otherCert" property="isPics">
														<html:checkbox property="isPics" name="otherCert" styleId='isPics' disabled="true" value="1"/>
													</logic:present>										
												</td>
												<td><logic:present name="otherCert" property="filename">
														<bean:define id="id" property="id" name="otherCert"></bean:define>
														<html:link href="download_othercertificate.jsp" paramId="id"
															 paramName="id" styleClass="downloadFile" styleId='<%="downloadOtherFile" + i%>' target="_blank">Download</html:link>
													</logic:present>
												</td>
												<bean:define id="oID"  property="id" name="otherCert"/>
												<td>
													<logic:equal value="1" name="privilege" property="modify">
														<logic:equal value="1" name="privilege" property="delete">
															<a onclick="return clearOtherCertificate('<%=i%>','<%=oID %>');" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a>
														</logic:equal>
													</logic:equal>
												</td>
											</tr>
										</logic:iterate>
									</logic:present>
								</table>
								<logic:equal value="1" name="privilege" property="modify">
									<logic:equal value="1" name="privilege" property="add">
										<INPUT id="cmd1" type="button" value="Add Quality Certificate" class="btn btn-primary" onclick="addRow('otherCertificate')" />	
									</logic:equal>
								</logic:equal>				
							</div>
							
							<div class="content">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<div id="networld" class="content">
								<header class="mt-1 card-header">
									<h2 class="card-title text-center">Networld certificate</h2>	
								</header>
								<table style="background:#fff;" class="main-table table table-bordered table-striped mb-0" id="isnetwotldcerti">
									<tr>
										<td class="">Discription</td>
										<td class="">Yes/No</td>	
										<td class="">Action</td>									
<!-- 										<td class="header">Uploaded Certificate</td> -->
										<td class="">Uploaded Certificate</td>
										
									</tr>
									
								 <c:set var ="hasnetworld" scope = "session" value ='<%= session.getAttribute("hasnetworldcsrtificate") %>'/>
								<c:choose>
								<c:when test="${hasnetworld == true}">
								 <c:set var ="networldcert" scope = "session" value ='<%= session.getAttribute("networldcertificate") %>'/>
								 <td>
								<h3 style="color: #009900">Do yo want to modify your company ISNetworld certification? </h3>
								</td>
								<td>
								<input name="hasNetWorldCert" type="radio" value="true" id="yesnetworld" onclick="yesNet()"><b>Yes</b>
								<input type="radio" id="nonetworld" onclick="noNet()"><b>No</b>
								</td>
								<td>
								<div id="yesnet">
								<div class="fileUpload btn btn-primary" style="float: left;">
								<span>Modify</span>
								<html:file property="networldCert" styleId="otherCert2"  styleClass="upload"></html:file>
								</div> 
								<span id="otherFile2"></span>	
								</div>	
								</td>
								<td>
								<h4 style="color: #009900"><b>${networldcert}</b></h4>	
								</td>
<!-- 								<td> -->
<!-- 								<a onclick="" href="#"><img src='images/deleteicon.jpg' alt='Delete' /></a> -->
<!-- 								</td> -->
								</c:when>
								<c:otherwise>
								<td>
								<h3 style="color: #009900">Does your company have ISNetworld certification? </h3>															
								</td>
								<td>
								<input name="hasNetWorldCert" type="radio" value="true" id="yesnetworld" onclick="yesNet()"><b>Yes</b>
								<input name="hasNetWorldCert" type="radio" value="false" id="nonetworld" onclick="noNet()"><b>No</b>
								</td>
								<td>
								<div id="yesnet">
								<div class="fileUpload btn btn-primary" style="float: left;">
								<span>Browse</span>
								<html:file property="networldCert" styleId="otherCert2"  styleClass="upload"></html:file>
								</div> 
								<span id="otherFile2"></span>	
								</div>	
								</td>
								<td></td>
								</c:otherwise>
								</c:choose>
								</table>
						</div>	
					</div>
							
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit2" name="saveandexit2"> 
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3" name="submit3">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=documentsNavigation" styleClass="btn">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showReferences();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
						</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=documentsNavigation" styleClass="btn">Next</html:link>								
								<input type="button" class="btn" value="Back" onclick="showReferences();"> 
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">Quality/Other Certifications</h2>
								<br/>
								<h3>You have no rights to view quality/other certifications information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=documentsNavigation" styleClass="btn">Next</html:link>								
								<input type="button" class="btn" value="Back" onclick="showReferences();"> 
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
	
		</html:form>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		$("#menu10").removeClass().addClass("current");
	});
	
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function() {
		$("#otherCertForm").validate({
			rules : {
				
			},
			submitHandler : function(form) {
				if (validateSelfVendorOtherCertificate()) {
					if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
						$('#otherCertForm').append("<input type='hidden' name='submitType' value='submit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
						$('#otherCertForm').append("<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#saveandexit1').data('clicked')
							|| $('#saveandexit2').data('clicked')){
						$('#otherCertForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}
				}
				else{
					$("#ajaxloader").hide();
					$('#submit1').data('clicked', false);
					$('#submit').data('clicked', false);
					$('#submit2').data('clicked', false);
					$('#submit3').data('clicked', false);
					$('#saveandexit1').data('clicked', false);
					$('#saveandexit2').data('clicked', false);
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});	
</script>