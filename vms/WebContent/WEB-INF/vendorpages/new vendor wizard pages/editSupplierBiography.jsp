<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.form.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<logic:present name="currentUser" property="id">
	<c:set var="action" value="updatebiography" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimebiography" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">

var lastDate = new Date();
lastDate.setDate(lastDate.getDate());

$.datepicker.setDefaults({
	changeYear : true,
	defaultDate: lastDate,
	dateFormat : 'mm/dd/yy'
		
});
$(document).ready(function() {

$("#workstartdate").datepicker();
});

	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else if($(e.target).attr('id') == 'picsCertificateSubmit')
				{
					e.preventDefault();
					$('#picsCertificateSubmit').click();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
		
	function showdiverseClassification() {
		window.location = "${back}.do?parameter=businessAreaNavigation";
	}
	
	$(document).ready(function() {
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="Business Biography and Safety Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);
					$('#step-1 :radio').attr('disabled', true);
					/* $(':radio, :checkbox').click(function(){
					    return false;
					}); */					
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		if ($("input:radio[name=expOilGas]:checked").val() == 1) {
			$('#oilGasExp').css('display', 'block');
		} else {
			$('#oilGasExp').css('display', 'none');
		}
		
		if ($("input:radio[name=isBpSupplier]:checked").val() == 1) {
			$('#bpsupplier').css('display', 'block');
		} else {
			$('#bpsupplier').css('display', 'none');
		}
		
	/* 	if ($("input:radio[name=isCurrentBpSupplier]:checked").val() == 1) {
			$('#bpFranchise').css('display', 'block');
		} else {
			$('#bpFranchise').css('display', 'block');
		} */
		
		if ($("input:radio[name=isFortune]:checked").val() == 1) {
			$('#fortune').css('display', 'block');
		} else {
			$('#fortune').css('display', 'none');
		}
		
		$('input[name=expOilGas]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#oilGasExp').css('display', 'block');
			} else {
				$('#oilGasExp').css('display', 'none');
			}
		});
		
		$('input[name=isBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpsupplier').css('display', 'block');
			} else {
				$('#bpsupplier').css('display', 'none');
			}
		});
		
		/* $('input[name=isCurrentBpSupplier]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#bpFranchise').css('display', 'block');
			} else {
				$('#bpFranchise').css('display', 'none');
			}
		}); */
		
		$('input[name=isFortune]:radio').click(function() {
			if ($(this).val() == 1) {
				$('#fortune').css('display', 'block');
			} else {
				$('#fortune').css('display', 'none');
			}
		});		
	});
	
	function setBusinessValue(ele, value) {
		ele.value = value;
		
		//To Display a Message, When Is your Company PICS Certified = Yes
		if(ele.id == 'yesPicsCertified') {			
			$("#PICSCertified").css(
			{
				"display" : "block"
			});		
			
			$("#PICSCertified").dialog(
			{
				width : 700,
				modal : true,
				closeOnEscape: false,
				open: function() {
	                $(this).parent().find(".ui-dialog-titlebar-close").hide();
	            },
				show : 
				{
					effect : "scale",
					duration : 1000
				},
				hide : 
				{
					effect : "scale",
					duration : 1000
				}
			});			
		}		
	}
	
	function closeDialog() {
		$("#noPicsCertified").prop("checked", true);
		$("#PICSCertified").dialog("close");
	}
	
	$('body').on('focus', ".picsExpireDate", function() {
		$(this).datepicker();
	});
		
	function validatePicsCertificate() 
	{
		var picsCertificate = $('#picsCertificate').val();
		var expireDate = $('#picsExpireDate').val();
				
		if(picsCertificate == "") {
			alert('Pics Certificate is Mandatory.');
		} else if(expireDate == "") {
			alert('Expire Date is Mandatory.');
		} else if(picsCertificate != "" && expireDate != "") {
			var options = { 
            	url     : "${action}.do?method=savePicsCertificate",
	            type    : "POST",
	            dataType: 'json',
	            success:function( data ) {
               		if(data.result == "success") {
						alert("PICS Certificate Saved Successfully.");	
					} else {
						alert("PICS Certificate Not Saved, Due to Error.");
					}					
					$('#picsCertificate').val('');
					$('#picsExpireDate').val('');
					$("#PICSCertified").dialog("close");
                },
            }; 
	        $('#picsCertificateForm').ajaxSubmit(options);
	        return false;
		}
		return false;	
	}
</script>

<div class="clear"></div>

<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<div id="PICSCertified" title="PICS Certification" style="display: none;">
	<p>Please be Sure to Upload a Copy of Your  Avetta Certification.</p>
	<html:form styleId="picsCertificateForm" enctype="multipart/form-data">
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">1.Avetta Certificate</label>
				<div class="ctrl-col-wrapper">					
					<html:file property="picsCertificate" styleId="picsCertificate" styleClass="upload"/>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">2.Expire Date</label>
				<div class="ctrl-col-wrapper">
					<html:text property="picsExpireDate" styleId="picsExpireDate" readonly="true"
						styleClass="picsExpireDate main-text-box" alt="Please click to select date" />
				</div>
			</div>
		
		<div class="wrapper-btn" style=" margin: 20px 0 0 6%;">
			<html:reset value="Cancel" styleClass="btn" onclick="closeDialog();"/>
			<html:submit value="Upload" styleClass="btn" styleId="picsCertificateSubmit" onclick="return validatePicsCertificate();"/>	
		</div>
	</html:form>
</div>
<div class="clear"></div>

<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Business Biography and Safety Information" name="privilege" property="objectId.objectName">
		<html:form action="${action}.do?method=saveBusinessBiography" method="post" styleId="biographyForm">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>
			<logic:equal value="1" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit1" name="saveandexit1">
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit1" name="submit1">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=referencesNavigation" styleClass="btn">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" id="previous1" onclick="showdiverseClassification();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit" name="submit">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
							
							<div id="step-1" class="content">
								<header class="mt-1">
									<h2 class="card-title text-center pb-2">Business Biography and Safety</h2>
								</header>
								
								<div class="form-box card-body">
									<h3>Biography</h3>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Do you have current experience in the Oil &amp; Gas Industry?</label>
											<div class="col-sm-7 ctrl-col-wrapper pt-1">
												<span class="radio-custom radio-primary">
												<html:radio property="expOilGas" value="bussines[0]" idName="editVendorMasterForm" 
													tabindex="225" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
													<html:radio property="expOilGas" value="bussines[1]"
													tabindex="226" idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
					
									<div class="" id="oilGasExp" style="display: none;">
									
											<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is your work primarily Onshore or Offshore?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
											<html:select property="workPlace" styleId="workPlace"
																	styleClass="chosen-select-width form-control" tabindex="64">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="Onshore">Onshore</html:option>
																	<html:option value="OffShore">OffShore</html:option>
																</html:select>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Please list your clients below</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:textarea property="listClients" rows="5" cols="30"
													tabindex="227" styleClass="main-text-area" styleId="listClients"></html:textarea>
											</div>
										</div>
									</div>
							
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Are you a BP Supplier?</label>
											<div class="col-sm-7 ctrl-col-wrapper pt-1">
												<span class="radio-custom radio-primary">
												<html:radio property="isBpSupplier" value="bussines[2]" tabindex="228"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isBpSupplier" value="bussines[3]" tabindex="229"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									<div class="" id="bpsupplier" style="display: none;">
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">BP Contact Name:</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="bpContact" styleId="bpContact" styleClass="text-box form-control" tabindex="230"/>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">BP Division:</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="bpDivision" styleId="bpDivision" styleClass="text-box form-control" tabindex="231"/>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">BP Contact Phone:</label>
											<div class="col-sm-4 ctrl-col-wrapper">
												<html:text property="bpContactPhone" alt="" styleClass="text-box form-control"
													styleId="bpContactPhone" tabindex="232" />
											</div>
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Ext</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="bpContactPhoneExt" styleId="bpContactPhoneExt" tabindex="233"
													size="3" styleClass="text-box form-control" alt="Optional" ></html:text>
											</div>
											</div>
										</div>
											<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Are you a 1st Tier or 2nd Tier supplier?</label>
											<div class="col-sm-7 ctrl-col-wrapper"> 
											<html:select property="typeBpSupplier" styleId="typeBpSupplier"
																	styleClass="chosen-select-width form-control" tabindex="64">
																	<html:option value="">-- Select --</html:option>
																	<html:option value="1st Tier">1st Tier</html:option>
																	<html:option value="2nd Tier">2nd Tier</html:option>
																</html:select>	
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Please describe the specific work (products or services) you are providing to BP.</label>
											<div class="col-sm-7 ctrl-col-wrapper"> 
											<html:textarea property="specificwork" rows="5" cols="30"
													tabindex="227" styleClass="main-text-area" styleId="specificwork"></html:textarea>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Start date of the work </label>
											<div class="col-sm-7 ctrl-col-wrapper"> 
											<html:text property="workstartdate"
												alt="Please click to select date"
												styleId="workstartdate" styleClass="text-box form-control"
												readonly="false" />
										</div>
											</div>
											<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Contract value ($)</label>
											<div class="col-sm-7 ctrl-col-wrapper"> 
											<html:text property="contractValue"
												alt="enter-contractValue"
												styleId="contractValue" styleClass="text-box form-control" />
										</div>
											</div>
												<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Cities/States/BP site locations where this work for BP takes place?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
											<html:textarea property="specificwork" rows="5" cols="30"
													tabindex="227" styleClass="main-text-area form-control" styleId="specificwork"></html:textarea>
										</div>
											</div>
										</div>
									
								<%-- 	<div class="wrapper-full">
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Are you a current 1st or 2nd Tier Supplier to BP or a BP Franchise?</div>
											<div class="ctrl-col-wrapper">
												<html:radio property="isCurrentBpSupplier" value="bussines[4]" tabindex="234"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
												<html:radio property="isCurrentBpSupplier" value="bussines[5]" tabindex="235"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
											</div>
										</div>
									</div>
									<div class="wrapper-full" id="bpFranchise" style="display: block;">
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Please describe the scope of
												services. Include contract value, locations serviced, and other pertinent information:</div>
											<div class="ctrl-col-wrapper">
												<html:textarea property="currentBpSupplierDesc" rows="5" cols="30"
													styleClass="main-text-area" tabindex="236" styleId="currentBpSupplierDesc"></html:textarea>
											</div>
										</div>
									</div> --%>
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Are you currently doing business with any Fortune 500 companies?</label>
											<div class="col-sm-7 ctrl-col-wrapper pt-1">
												<span class="radio-custom radio-primary">
												<html:radio property="isFortune" value="bussines[6]" tabindex="237"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)" >Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isFortune" value="bussines[7]" tabindex="238"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									<div class="" id="fortune" style="display: none;">
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Please list your Fortune 500 clients below:</label>
											<div class="col-sm-7 ctrl-col-wrapper ">
												<html:textarea property="fortuneList" rows="5" cols="30"
													styleClass="main-text-area" tabindex="239" styleId="fortuneList"></html:textarea>
											</div>
										</div>
									</div>
							
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Does your company utilize union represented workforce?</label>
											<div class="col-sm-7 ctrl-col-wrapper pt-1">
												<span class="radio-custom radio-primary">
												<html:radio property="isUnionWorkforce" value="bussines[8]" tabindex="240"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isUnionWorkforce" value="bussines[9]" tabindex="241"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
							
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">If yes, does your company also utilize non-union represented workforce?</label>
											<div class="col-sm-7 ctrl-col-wrapper pt-1">
												<span class="radio-custom radio-primary">
												<html:radio property="isNonUnionWorkforce" value="bussines[10]" tabindex="242"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isNonUnionWorkforce" value="bussines[11]" tabindex="243"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-box card-body mt-5">
									<header class="pb-2">
										<h2 class="card-title">Safety</h2>
									</header>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is your company Avetta certified?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isPicsCertified" value="bussines[12]" tabindex="244"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)" styleId="yesPicsCertified">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isPicsCertified" value="bussines[13]" tabindex="245"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)" styleId="noPicsCertified">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isPicsCertified" value="bussines[14]" tabindex="246"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)" styleId="naPicsCertified">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
										
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is your company ISNetworld certified?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isNetworldCertified" value="bussines[36]" tabindex="246"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isNetworldCertified" value="bussines[37]" tabindex="247"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isNetworldCertified" value="bussines[38]" tabindex="248"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is your company ISO 9000 certified?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isIso9000Certified" value="bussines[15]" tabindex="246"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isIso9000Certified" value="bussines[16]" tabindex="247"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isIso9000Certified" value="bussines[17]" tabindex="248"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is your company ISO 14001 certified?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isIso14000Certified" value="bussines[18]" tabindex="249"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isIso14000Certified" value="bussines[19]" tabindex="250"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isIso14000Certified" value="bussines[20]" tabindex="251"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
												<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Have you received any OSHA citations in the last three years?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaRecd" value="bussines[21]" tabindex="252"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaRecd" value="bussines[22]" tabindex="253"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaRecd" value="bussines[23]" tabindex="254"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Does your company have an
												Occupational Safety &amp; Health Administration (OSHA) 3-yr average of 2.0 or less?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaAvg" value="bussines[24]" tabindex="255"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaAvg" value="bussines[25]" tabindex="256"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">												
												<html:text property="isOshaAvgTxt" styleClass="text-box form-control" tabindex="257"></html:text>
												<html:radio property="isOshaAvg" value="bussines[26]" tabindex="258"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Does your company have an
												Experience Modification Rate (EMR) of 1.0 or less?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isEmr" value="bussines[27]" tabindex="259"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isEmr" value="bussines[28]" tabindex="260"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:text property="isEmrTxt" styleClass="text-box form-control" tabindex="261"></html:text>
												<html:radio property="isEmr" value="bussines[29]" tabindex="262"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Will your company agree to BP's
												4 hour onsite safety training and have a 10-hr OSHA card for all
												contractor personnel working at the refinery?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaAgree" value="bussines[30]" tabindex="263"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaAgree" value="bussines[31]" tabindex="264"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isOshaAgree" value="bussines[32]" tabindex="265"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Is your company in compliance
												with the Northwest Indiana Business Roundtable substance abuse policy?</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<span class="radio-custom radio-primary">
												<html:radio property="isNib" value="bussines[33]" tabindex="266"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,1)">Yes</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isNib" value="bussines[34]" tabindex="267"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,0)">No</html:radio>
													<label for=""></label>
												</span>
												<span class="radio-custom radio-primary">
												<html:radio property="isNib" value="bussines[35]" tabindex="268"
													idName="editVendorMasterForm" onclick="setBusinessValue(this,2)">NA</html:radio>
													<label for=""></label>
												</span>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Please provide a snapshot of major jobs / projects completed:</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:textarea property="jobsDesc" rows="5" cols="30"
													styleClass="main-text-area" tabindex="269" styleId="jobsDesc"></html:textarea>
											</div>
										</div>
									
									
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Customer / Location</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="custLocation" styleId="custLocation"
													styleClass="text-box form-control" tabindex="270"/>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Customer contact</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="custContact" styleId="custContact"
													styleClass="text-box form-control" tabindex="271"/>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Telephone</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="telephone" styleId="telephone"
													styleClass="text-box form-control" tabindex="272"/>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Type of work</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="workType" styleId="workType"
													styleClass="text-box form-control" tabindex="273"/>
											</div>
										</div>
										<div class="row-wrapper form-group row">
											<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Size $</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:text property="size" styleId="size" tabindex="274"
												 styleClass="text-box form-control" onblur="moneyFormatToUS('formattedValue','size');" />
											</div>
										</div>
									
								</div>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit2" name="saveandexit2">
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3" name="submit3">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=referencesNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" id="previous1" onclick="showdiverseClassification();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
						</div>
					</div>
					<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=referencesNavigation" styleClass="btn">Next</html:link>								
								<input type="button" class="btn" value="Back" id="previous1" onclick="showdiverseClassification();">
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">Business Biography and Safety</h2>
								<br/>
								<h3>You have no rights to view business biography and safety information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=referencesNavigation" styleClass="btn">Next</html:link>								
								<input type="button" class="btn" value="Back" id="previous1" onclick="showdiverseClassification();">
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
		</html:form>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>

<script type="text/javascript">
	$(document).ready(function() {
		$("#menu8").removeClass().addClass("current");
	});
	
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});

	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function($) {
		  $("#telephone").mask("(999) 999-9999?");
		  $("#bpContactPhone").mask("(999) 999-9999?");
	});
	
	jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
        return this.optional(element) || /^[0-9]+$/.test(value);
    },"Please enter only numbers.");
	
	$(function() {
		$("#biographyForm").validate({
			rules : {
				bpContactPhoneExt : {
					onlyNumbers : true
				},
				contractValue : {
					onlyNumbers : true
					},
				listClients	: {
					rangelength : [ 0, 60000 ]
				},
				currentBpSupplierDesc : {
					rangelength : [ 0, 60000 ]
				},
				fortuneList : {
					rangelength : [ 0, 60000 ]
				},
				jobsDesc : {
					rangelength : [ 0, 60000 ]
				}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					$('#biographyForm').append("<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					$('#biographyForm').append("<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#biographyForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});	
</script>