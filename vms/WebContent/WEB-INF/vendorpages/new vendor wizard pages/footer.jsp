<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<footer>
	<div class="page-wrapper">
		<div class="copy">
			A Solution of First Genesis Professional Services Inc. Powered by
			AVMS<img alt="first genesis logo" src="images/logo-2.png">
		</div>
	</div>
</footer>