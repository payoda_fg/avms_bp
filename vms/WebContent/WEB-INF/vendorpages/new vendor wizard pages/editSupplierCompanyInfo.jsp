<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<logic:present name="currentUser" property="id">
	<c:set var="back" value="vendornavigation" />
	<c:set var="action" value="updatecompanyinfo" />
	<bean:define id="commonPrivileges" name="privileges" />
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="back" value="primevendornavigation" />
	<c:set var="action" value="updateprimecompanyinfo" />
	<bean:define id="commonPrivileges" name="rolePrivileges" />
</logic:present>

<style type="text/css">
/* footer {
	    margin-top: 125px;
	} */
</style>

<!-- <style type="text/css">
	* {
 	margin-top: -4px;
   	padding: 0px;
    text-decoration: none;
}
</style> -->

<script type="text/javascript">
	$(document).keypress(function(e) {
		if (e.which == 13) {
			if (e.target.nodeName != 'TEXTAREA') {
				if ($(e.target).attr('id') == 'isApprovedDesc') {
					e.preventDefault();
				} else if ($(e.target).attr('id') == 'statusButton') {
					e.preventDefault();
					getSelectedIds();
				} else {
					e.preventDefault();
					$('#submit').click();
				}
			}
		}
	});

	$(document)
			.ready(
					function() { // on page DOM load
						<logic:iterate id="privilege1" name="commonPrivileges">
						<logic:equal value="Company Information" name="privilege1" property="objectId.objectName">
						<logic:equal value="0" name="privilege1" property="modify">
						$('#step-1 :input').attr('readonly', true);
						$('#step-1 :radio').attr('disabled', true);
						/* $(':radio, :checkbox').click(function(){
						    return false;
						}); */
						</logic:equal>
						</logic:equal>
						</logic:iterate>

						if ($("#vendorStatus option:selected").text() != "- Select -") {
							$("#vendorStatusValue").val(
									$("#vendorStatus option:selected").text());
						}

						var revenue1 = $("#annualYear1").val();
						var revenue2 = $("#annualYear2").val();
						var revenue3 = $("#annualYear3").val();
						if (revenue1 === '0') {
							$("#annualYear1").val('');
						}
						if (revenue2 === '0') {
							$("#annualYear2").val('');
						}
						if (revenue3 === '0') {
							$("#annualYear3").val('');
						}
						if ($("#annualTurnover").val() === 'Optional') {
							$("#annualTurnover").val('');
						}
						if ($("#sales2").val() === 'Optional') {
							$("#sales2").val('');
						}
						if ($("#sales3").val() === 'Optional') {
							$("#sales3").val('');
						}
						$("#previous").prop('disabled', true);
						$("#previous").css('background',
								'none repeat scroll 0 0 #848484');
						$("#previous1").prop('disabled', true);
						$("#previous1").css('background',
								'none repeat scroll 0 0 #848484');

						if ($("input[name='primeNonPrimeVendor']:checked")) {
							$("#nonprime").attr('disabled', 'true');
						}

						$("input[name='primeNonPrimeVendor']")
								.click(
										function() {
											if ($(this).val() === '1') {
												input_box = confirm("Do you want change from Non-Prime Vendor to Prime Vendor? Prime vendor can not be changed as Non-Prime Vendor");
												if (input_box == true) {
													return true;
												} else {
													return false;
												}
											}
										});
					});

	function copyContact() {
		var firstName = $("#preparerFirstName").val();
		var lastName = $('#preparerLastName').val();
		var title = $('#preparerTitle').val();
		var phone = $('#preparerPhone').val();
		var mobile = $('#preparerMobile').val();
		var fax = $('#preparerFax').val();
		if ($("#sameContact").is(':checked')) {
			$("#firstName").val(firstName);
			$('#lastName').val(lastName);
			$('#designation').val(title);
			$('#contactPhone').val(phone);
			$('#contactMobile').val(mobile);
			$('#contactFax').val(fax);
		} else {
			$("#firstName").val('');
			$('#lastName').val('');
			$('#designation').val('');
			$('#contactPhone').val('');
			$('#contactMobile').val('');
			$('#contactFax').val('');
		}
	}

	function validateSalesYear(ele) {
		// To get the current year
		var d = new Date();
		var curr_year = d.getFullYear();
		var salesYear = ele.value;
		var yearOfEstablishment = $("#yearOfEstablishment").val();
		var isAvailable = true;

		var annualYear1 = $("#annualYear1").val();
		var annualYear2 = $("#annualYear2").val();
		var annualYear3 = $("#annualYear3").val();

		var elementId = ele.id;

		if (salesYear != "" && salesYear.length >= 4) {
			if (elementId == "annualYear1") {
				if (annualYear2 == salesYear || annualYear3 == salesYear) {
					isAvailable = false;
					alert("Sales year already available");
					$("#annualYear1").val('');
				}
			}

			if (elementId == "annualYear2") {
				if (annualYear1 == salesYear || annualYear3 == salesYear) {
					isAvailable = false;
					alert("Sales year already available");
					$("#annualYear2").val('');
				}
			}
			if (elementId == "annualYear3") {
				if (annualYear1 == salesYear || annualYear2 == salesYear) {
					isAvailable = false;
					alert("Sales year already available");
					$("#annualYear3").val('');
				}
			}
		}

		if (salesYear != "" && isAvailable && salesYear.length >= 4) {
			if (salesYear<yearOfEstablishment || salesYear>curr_year) {
				alert("Please enter year between " + yearOfEstablishment
						+ " and current year( " + curr_year + " )");
				$(ele).val('');
				$(ele).focus();
				return false;
			}
		}
	}

	function vendorContactPage() {
		window.location = "${back}.do?parameter=contactNavigation";
	}

	function showVendorWebsite() {

		var link = $("#website").val();
		var url = "";
		if (link != "" && link != " ") {
			if (link.substring(4, 5) == "s" || link.substring(4, 5) == "S") {
				if (link.substring(0, 8) == "https://"
						|| link.substring(0, 8) == "HTTPS://") {
					$("#anchorid").attr("href", link);
					return true;
				} else {
					url = "https://" + link;
					$('#website').val(url);
					$("#anchorid").attr("href", url);
					return true;
				}
			} else if (link.substring(4, 5) == ":") {
				if (link.substring(0, 7) == "http://"
						|| link.substring(0, 7) == "HTTP://") {
					$("#anchorid").attr("href", link);
					return true;
				} else {
					url = "http://" + link;
					$('#website').val(url);
					$("#anchorid").attr("href", url);
					return true;
				}
			} else {
				url = "http://" + link;
				$('#website').val(url);
				$("#anchorid").attr("href", url);
				return true;
			}
		} else {
			alert("Please Enter URL");
			return false;
		}
	}
	function setPrimeValue() {
		$('#prime').val('1');
	}
	function setNonPrimeValue() {
		$('#nonprime').val('0');
	}
</script>

<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<logic:iterate id="privilege" name="commonPrivileges">
				<logic:equal value="Company Information" name="privilege"
					property="objectId.objectName">
					<html:form action="${action}.do?method=saveCompanyInformation"
						method="post" styleId="companyForm" styleClass="FORM">
						<div id="successMsg">
							<html:messages id="msg" property="vendor" message="true">
								<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
							</html:messages>
							<html:messages id="msg" property="transactionFailure"
								message="true">
								<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
							</html:messages>
						</div>
						<logic:equal value="1" name="privilege" property="view">
							<div id="content-area">
								<div id="wizard" class="swMain edit-vendor-profile">
									<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
									<div class="stepContainer">
										<!-- style="height: 717px;" -->
										<html:hidden property="id" styleId="vendorId"></html:hidden>
										<bean:define id="cancelUrl" name="cancelString"></bean:define>
										<div class="actionBar top_actionbar_fix"
											style="margin: 3px 0;">
											<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
											<logic:equal value="1" name="privilege" property="modify">
												<input type="submit" class="btn btn-primary mr-1" value="Save and Exit"
													id="saveandexit1" name="saveandexit1">
												<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit1"
													name="submit1">
											</logic:equal>
											<logic:equal value="0" name="privilege" property="modify">
												<html:link
													action="/${back}.do?parameter=companyAddressNavigation"
													styleClass="btn">Next</html:link>
											</logic:equal>
											<input type="button" class="btn btn-primary mr-1" value="Back"
												onclick="vendorContactPage();">
											<logic:equal value="1" name="privilege" property="modify">
												<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit"
													name="submit">
												<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
											</logic:equal>
										</div>
										<div id="step-1" class="content" style="display: block;">
											<header class="mt-1">
												<h2 class="card-title text-center">Company Information</h2>
											</header>
											<div style="padding-top: 2%;">
												<div class="form-box card-body">
													<header>
														<h3 class="card-title pt-1 pb-2">Company</h3>
													</header>
													
														<div class="row-wrapper form-group row">
															<label class="col-sm-3 label-col-wrapper control-label text-sm-right">
																<bean:message key="comp.name" />
															</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text property="vendorName" alt=""
																	styleId="vendorName" styleClass="text-box form-control"
																	tabindex="23" />
															</div>
															<span class="error"> <html:errors
																	property="vendorName"></html:errors></span>
														</div>
														<logic:present name="currentUser">
															<div class="row-wrapper form-group row">
																<label
																	class="col-sm-3 label-col-wrapper control-label text-sm-right">Status</label>
																<div class="ctrl-col-wrapper" style="display: block;">
																	<input type="text" class="text-box"
																		id="vendorStatusValue" readonly="readonly"
																		tabindex="24">
																</div>
																<div class="ctrl-col-wrapper" style="display: none;">
																	<html:select property="vendorStatus"
																		styleId="vendorStatus"
																		styleClass="chosen-select-width" tabindex="24"
																		style="display:none;">
																		<logic:present name="editVendorMasterForm"
																			property="statusMasters">
																			<logic:iterate id="status"
																				name="editVendorMasterForm" property="statusMasters">
																				<bean:define id="id" name="status" property="id"></bean:define>
																				<html:option value="<%=id.toString()%>">
																					<bean:write name="status" property="statusname" />
																				</html:option>
																			</logic:iterate>
																		</logic:present>
																	</html:select>
																</div>
															</div>
														</logic:present>
													
													<div id="hideDivforTier2Vendor">
														
															<div class="row-wrapper form-group row">
																<label
																	class="col-sm-3 label-col-wrapper control-label text-sm-right">Duns
																	Number</label>
																<div class="col-sm-7 ctrl-col-wrapper">
																	<html:text styleClass="text-box form-control" property="dunsNumber"
																		styleId="dunsNumber" alt="Optional" tabindex="25" />
																	<span class="error"> <html:errors
																			property="dunsNumber"></html:errors></span>
																</div>
															</div>
															<div class="row-wrapper form-group row">
																<label
																	class="col-sm-3 label-col-wrapper control-label text-sm-right">Federal
																	Tax ID
															</label>
															<div class="col-sm-7 ctrl-col-wrapper">
																<html:text styleClass="text-box form-control" property="taxId" alt=""
																	styleId="taxId" tabindex="26" />
															</div>
															<span class="error"> <html:errors property="taxId"></html:errors></span>
														</div>
													
													
														<div class="row-wrapper form-group row">
															<label
																class="col-sm-3 label-col-wrapper control-label text-sm-right">Company
																Type
														</label>
														<div class="col-sm-7 ctrl-col-wrapper">
															<html:select property="companytype" styleId="companytype"
																styleClass="chosen-select-width" tabindex="27">
																<html:option value="">Select One</html:option>
																<logic:present name="editVendorMasterForm"
																	property="legalStructures">
																	<logic:iterate id="company" name="editVendorMasterForm"
																		property="legalStructures">
																		<bean:define id="id" name="company" property="id"></bean:define>
																		<html:option value="<%=id.toString()%>">
																			<bean:write name="company" property="name" />
																		</html:option>
																	</logic:iterate>
																</logic:present>
															</html:select>
														</div>
													</div>
													<div class="row-wrapper form-group row">
														<label
															class="col-sm-3 label-col-wrapper control-label text-sm-right">Number
															of Employees
													</label>
													<div class="col-sm-7 ctrl-col-wrapper">
														<html:text styleClass="text-box form-control"
															property="numberOfEmployees" alt=""
															styleId="numberOfEmployees" tabindex="28" />
													</div>
													<span class="error"> <html:errors
															property="numberOfEmployees"></html:errors>
													</span>
												</div>
											
										</div>
										
											<div class="row-wrapper form-group row">
												<label
													class="col-sm-3 label-col-wrapper control-label text-sm-right">Business
													Type
											</label>
											<div class="col-sm-7 ctrl-col-wrapper">
												<html:select property="businessType" styleId="businessType"
													styleClass="chosen-select-width" tabindex="29">
													<html:option value="">Select One</html:option>
													<logic:present name="editVendorMasterForm"
														property="businessTypes">
														<logic:iterate id="business" name="editVendorMasterForm"
															property="businessTypes">
															<bean:define id="id" name="business" property="id"></bean:define>
															<html:option value="<%=id.toString()%>">
																<bean:write name="business" property="typeName" />
															</html:option>
														</logic:iterate>
													</logic:present>
												</html:select>
											</div>
											<span class="error"> <html:errors
													property="businessType"></html:errors></span>
										</div>
										<div class="row-wrapper form-group row">
											<label
												class="col-sm-3 label-col-wrapper control-label text-sm-right">Year
												Established
										</label>
										<div class="col-sm-7 ctrl-col-wrapper">
											<html:text styleClass="text-box form-control"
												property="yearOfEstablishment" alt=""
												styleId="yearOfEstablishment" tabindex="30"
												onblur="return currentYearValidation('yearOfEstablishment');" />
										</div>
										<span class="error"> <html:errors
												property="yearOfEstablishment"></html:errors></span>
									</div>
								
									<div class="row-wrapper form-group row">
										<label
											class="col-sm-3 label-col-wrapper control-label text-sm-right">Website
											URL(http://)
									</label>
									<div class="col-sm-7 ctrl-col-wrapper">
										<html:text styleClass="text-box form-control" property="website" alt=""
											styleId="website" tabindex="31" />
										<logic:present name="currentUser" property="id">
											<a href="website" id="anchorid" target="_blank"
												onclick="return showVendorWebsite();"> <img
												src="images/go.png" alt="" />
											</a>
										</logic:present>
										<span class="error"> <html:errors property="website"></html:errors></span>
									</div>
								</div>
								<div class="row-wrapper form-group row">
									<label
										class="col-sm-3 label-col-wrapper control-label text-sm-right">Email
								</label>
								<div class="col-sm-7 ctrl-col-wrapper">
									<html:text styleClass="text-box form-control" property="emailId"
										styleId="emailId" alt="" tabindex="32" />
								</div>
								<span class="error"> <html:errors property="emailId"></html:errors></span>
							</div>
		
		
			<div class="row-wrapper form-group row">
				<label
					class="col-sm-3 label-col-wrapper control-label text-sm-right">State
					Incorporation
			</label>
			<div class="col-sm-7 ctrl-col-wrapper">
				<html:text styleClass="text-box form-control" property="stateIncorporation"
					styleId="stateIncorporation" tabindex="33" alt="Optional" />
			</div>
		</div>
		<div class="row-wrapper form-group row">
			<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State
				Sales TaxId
		</label>
		<div class="col-sm-7 ctrl-col-wrapper">
			<html:text styleClass="text-box form-control" property="stateSalesTaxId"
				styleId="stateSalesTaxId" tabindex="34" alt="Optional" />
		</div>
	</div>
	
	</div>
	</div>

	<div class="panelCenter_1">
		<h3>Revenue (for the past three years)</h3>
		<div class="form-box card-body">
			<table class="main-table table table-bordered table-striped mb-0" style="width: 100%;">
				<tr>
					<td class="">Year</td>
					<td class="">Annual Revenue</td>
				</tr>
				<tr class="even">
					<td><html:text styleClass="main-text-box form-control"
							name="editVendorMasterForm" property="annualYear1" alt=""
							styleId="annualYear1" tabindex="35"
							onkeyup="validateSalesYear(this);" /></td>
					<td><html:hidden property="annualTurnoverId1" alt="" /> <html:text
							styleClass="main-text-box form-control" property="annualTurnover" alt=""
							styleId="annualTurnover"
							onchange="moneyFormatToUS('annualTurnover','turnoverFormatValue');"
							tabindex="35" /> <html:hidden styleId="turnoverFormatValue"
							property="annualTurnoverFormat" alt="" /> <span class="error">
							<html:errors property="annualTurnover"></html:errors>
					</span></td>
				</tr>
				<tr class="even">
					<td><html:text styleClass="main-text-box form-control"
							name="editVendorMasterForm" property="annualYear2" alt="Optional"
							styleId="annualYear2" tabindex="36"
							onkeyup="validateSalesYear(this);" /></td>
					<td><html:hidden property="annualTurnoverId2" alt="" /> <html:text
							styleClass="main-text-box form-control" property="sales2" tabindex="36"
							styleId="sales2" alt="Optional"
							onchange="moneyFormatToUS('sales2','salesFormat2');" /> <html:hidden
							styleId="salesFormat2" property="salesFormat2" alt="" /><span
						class="error"> <html:errors property="sales2"></html:errors>
					</span></td>
				</tr>
				<tr class="even">
					<td><html:text styleClass="main-text-box form-control"
							name="editVendorMasterForm" property="annualYear3" alt="Optional"
							styleId="annualYear3" tabindex="37"
							onkeyup="validateSalesYear(this);" /></td>
					<td><html:hidden property="annualTurnoverId3" alt="" /> <html:text
							styleClass="main-text-box form-control" property="sales3" alt="Optional"
							tabindex="37" styleId="sales3"
							onchange="moneyFormatToUS('sales3','salesFormat3');" /> <html:hidden
							styleId="salesFormat3" property="salesFormat3" alt="" /><span
						class="error"> <html:errors property="sales3"></html:errors>
					</span></td>
				</tr>
			</table>
		</div>
	</div>

	<logic:present name="currentUser" property="id">
		<div class="">
			<h2 class="StepTitle">Vendor Classification</h2>			
				<div class="row-wrapper form-group row">
					<label
						class="col-sm-3 label-col-wrapper control-label text-sm-right">
				</label>
				<div class="col-sm-7 ctrl-col-wrapper">
					Prime Vendor
					<html:radio property="primeNonPrimeVendor" value="prime"
						idName="editVendorMasterForm" tabindex="113" styleId="prime"
						onclick="setPrimeValue()"></html:radio>
					Non-Prime Vendor
					<html:radio property="primeNonPrimeVendor" value="nonprime"
						idName="editVendorMasterForm" tabindex="114" styleId="nonprime"
						onclick="setNonPrimeValue()"></html:radio>
				</div>
			</div>
		
		</div>
	</logic:present>
</div>


	<div class="actionBar bottom_actionbar_fix">
		<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
		<logic:equal value="1" name="privilege" property="modify">
			<input type="submit" class="btn btn-primary mr-1" value="Save and Exit"
				id="saveandexit2" name="saveandexit2">
			<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3"
				name="submit3">
		</logic:equal>
		<logic:equal value="0" name="privilege" property="modify">
			<html:link action="/${back}.do?parameter=companyAddressNavigation"
				styleClass="btn">Next</html:link>
		</logic:equal>
		<input type="button" class="btn btn-primary mr-1" value="Back"
			onclick="vendorContactPage();">
		<logic:equal value="1" name="privilege" property="modify">
			<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2"
				name="submit2">
			<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
		</logic:equal>
	</div>
	</div>
</div>
</div>
</logic:equal>
	<logic:equal value="0" name="privilege" property="view">
		<div id="content-area">
			<div id="wizard" class="swMain">
				<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
				<div class="stepContainer" style="height: 717px;">
					<html:hidden property="id" styleId="vendorId"></html:hidden>
					<bean:define id="cancelUrl" name="cancelString"></bean:define>
					<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
						<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
						<html:link action="/${back}.do?parameter=companyAddressNavigation"
							styleClass="btn btn-primary mr-1">Next</html:link>
						<input type="button" class="btn btn-primary mr-1" value="Back"
							onclick="vendorContactPage();">
					</div>
					<div id="step-1" class="content" style="display: block;">
						<h2 class="StepTitle">Company Information</h2>
						<br />
						<h3>You have no rights to view company information.</h3>
					</div>
					<div class="actionBar bottom_actionbar_fix">
						<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-1">Exit</html:link>
						<html:link action="/${back}.do?parameter=companyAddressNavigation"
							styleClass="btn">Next</html:link>
						<input type="button" class="btn btn-primary mr-1" value="Back"
							onclick="vendorContactPage();">
					</div>
				</div>
			</div>
		</div>
	
	</logic:equal>
	</html:form>
	</logic:equal>
	</logic:iterate>
	</div>
</div>
</section>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#menu2").removeClass().addClass("current");
		});
		jQuery.validator.addMethod("alpha", function(value, element) {
			return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
		}, "No Special Characters Allowed.");
		jQuery.validator.addMethod("onlyNumbers", function(value, element) {
			return this.optional(element) || /^[0-9]+$/.test(value);
		}, "Please enter only numbers.");
		jQuery.validator.addMethod("allowhyphens", function(value, element) {
			return this.optional(element) || /^[0-9-]+$/.test(value);
		}, "Please enter valid numbers.");

		jQuery('#submit1').click(function() {
			$(this).data('clicked', true);
		});
		jQuery('#submit').click(function() {
			$(this).data('clicked', true);
		});
		$('#submit2').click(function() {
			$(this).data('clicked', true);
		});
		$('#submit3').click(function() {
			$(this).data('clicked', true);
		});
		$('#saveandexit1').click(function() {
			$(this).data('clicked', true);
		});
		$('#saveandexit2').click(function() {
			$(this).data('clicked', true);
		});
		$(function() {

			$("#companyForm")
					.validate(
							{
								rules : {
									vendorName : {
										maxlength : 255,
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									yearOfEstablishment : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										onlyNumbers : true,
										range : [ 1900, 3000 ]
									},
									website : {
										maxlength : 255,
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									dunsNumber : {
										allowhyphens : true,
										maxlength : 255
									},
									taxId : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										allowhyphens : true,
										maxlength : 255
									},
									businessType : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									companytype : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									numberOfEmployees : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										onlyNumbers : true,
										maxlength : 10
									},
									address1 : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										maxlength : 120
									},
									city : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										alpha : true,
										maxlength : 60
									},
									state : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									province : {
										maxlength : 60
									},
									region : {
										maxlength : 60
									},
									userSecQnAns : {
										required : true
									},
									country : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									zipcode : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										allowhyphens : true
									},
									phone : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										}
									},
									zipcode2 : {
										allowhyphens : true
									},
									emailId : {
										required : function(element) {
											if ($('#submit').data('clicked')
													|| $('#submit1').data(
															'clicked')
													|| $('#submit2').data(
															'clicked')
													|| $('#submit3').data(
															'clicked')) {
												return true;
											} else {
												return false;
											}
										},
										email : true
									},
									annualYear1 : {
										range : [ 1900, 3000 ],
										required : true,
										onlyNumbers : true
									},
									annualTurnover : {
										required : true
									},
									annualYear2 : {
										required : function() {
											if ($('#sales2').val() != "") {
												range: [ 1900, 3000 ]
												return true;
											} else {
												return false;
											}
										},
										onlyNumbers : true,
										annualYear2 : false
									},
									sales2 : {
										required : function() {
											if ($('#annualYear2').val() != "") {
												return true;
											} else {
												return false;
											}
										},
										sales2 : false

									},
									sales3 : {
										required : function() {
											if ($('#annualYear3').val() != "") {
												return true;
											} else {
												return false;
											}
										},
										sales3 : false

									},
									annualYear3 : {

										required : function() {
											if ($('#sales3').val() != "") {
												range: [ 1900, 3000 ]
												return true;
											} else {
												return false;
											}
										},
										onlyNumbers : true,
										annualYear3 : false
									}
								},
								submitHandler : function(form) {

									if ($('#submit1').data('clicked')
											|| $('#submit3').data('clicked')) {
										$('#companyForm')
												.append(
														"<input type='hidden' name='submitType' value='submit' />");
										$("#ajaxloader")
												.css('display', 'block');
										$("#ajaxloader").show();
										form.submit();
									} else if ($('#submit').data('clicked')
											|| $('#submit2').data('clicked')) {
										$('#companyForm')
												.append(
														"<input type='hidden' name='submitType' value='save' />");
										$("#ajaxloader")
												.css('display', 'block');
										$("#ajaxloader").show();
										form.submit();
									} else if ($('#saveandexit1').data(
											'clicked')
											|| $('#saveandexit2').data(
													'clicked')) {
										$('#companyForm')
												.append(
														"<input type='hidden' name='submitType' value='saveandexit' />");
										$("#ajaxloader")
												.css('display', 'block');
										$("#ajaxloader").show();
										form.submit();
									}
								},
								invalidHandler : function(form, validator) {
									$("#ajaxloader").css('display', 'none');
									alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
									$('#submit1').data('clicked', false);
									$('#submit').data('clicked', false);
									$('#submit2').data('clicked', false);
									$('#submit3').data('clicked', false);
									$('#saveandexit1').data('clicked', false);
									$('#saveandexit2').data('clicked', false);
								}
							});
		});
	</script>