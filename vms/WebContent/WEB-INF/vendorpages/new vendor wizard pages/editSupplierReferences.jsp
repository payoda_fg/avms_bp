<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<logic:present name="currentUser" property="id">
	<c:set var="action" value="updatereferences" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimereferences" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
		
	function showBusinessBiography() {
		window.location = "${back}.do?parameter=biographyNavigation";
	}
	
	function changestate(ele, id) {
		var country = ele.value;
		if (country != null && country != '') {
			$.ajax({
				url : 'state.do?method=getState&id=' + country + '&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					$("#" + id).find('option').remove().end().append(data);
					$('#' + id).select2();
				}
			});
		} else {
			$("#" + id).find('option').remove().end().append('<option value="">--Select--</option>');
			$('#' + id).select2();
		}
	}
</script>
<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="References Information" name="privilege" property="objectId.objectName">
		<html:form action="${action}.do?method=saveReferences" method="post" styleId="referenceForm">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>
			<logic:equal value="1" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit1" name="saveandexit1">
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit1" name="submit1">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=otherCertificateNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showBusinessBiography();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit" name="submit">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
							<div id="step-1" class="content">
								<header class="mt-1">
									<h2 class="card-title text-center pb-2">References</h2>
								</header>
								<div class="panelCenter_1">
									<div class="form-box card-body">									
									<h3>Reference 1</h3>
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Company Name</label>
													<div class="col-sm-7 ctrl-col-wrapper">
														<html:text property="referenceCompanyName1" alt="" styleClass="text-box form-control"
															styleId="referenceCompanyName1" tabindex="132" />
												</div>
											</div>
											<div class="row-wrapper form-group row">
													<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Name</label>
													<div class="col-sm-7 ctrl-col-wrapper">
														<html:text property="referenceName1" alt="" styleClass="text-box form-control"
															styleId="referenceName1" tabindex="133" />
													</div>
										    </div>
										 
										 
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Address</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:textarea property="referenceAddress1" styleId="referenceAddress1" 
														alt="" styleClass="main-text-area" tabindex="133" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referencePhone1" alt="" tabindex="134"
														styleClass="text-box form-control" styleId="referencePhone1" style="width:47%;display:inline-block;" />
												</div>
											</div>
												
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Ext</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="extension1" styleId="extension1" size="3"
														styleClass="text-box form-control" alt="Optional" style="width:22%;display:inline-block;" tabindex="135" />
												</div>
											</div>
									        <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceMailId1" alt="" styleClass="text-box form-control"
														styleId="referenceMailId1" tabindex="136" />
												</div>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceMobile1" alt="Optional"
														styleClass="text-box form-control" styleId="referenceMobile1" tabindex="137" />
												</div>
											</div>
									        <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceCity1" alt="" styleClass="text-box form-control"
														styleId="referenceCity1" tabindex="138" />
												</div>
												<span class="error"> <html:errors property="referenceCity1"></html:errors></span>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Zip</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceZip1" alt="" styleClass="text-box form-control"
														styleId="referenceZip1" tabindex="139" />
												</div>
											</div>
										   <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<logic:present name="userDetails" property="workflowConfiguration">
														<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
															<html:select property="referenceCountry1" styleId="referenceCountry1" 
																tabindex="140" onchange="changestate(this,'referenceState1');" styleClass="chosen-select-width form-control">
																<html:option value="">--Select--</html:option>
																<bean:size id="size" name="countryList" />
																<logic:greaterEqual value="0" name="size">
																	<logic:iterate id="country" name="countryList">
																		<bean:define id="name" name="country" property="id"/>
																		<html:option value="<%=name.toString()%>">
																			<bean:write name="country" property="countryname" />
																		</html:option>
																	</logic:iterate>
																</logic:greaterEqual>
															</html:select>
														</logic:equal>
														<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
															<input type="text" class="text-box form-control" id="referenceCountryValue1" readonly="readonly">
															<html:select property="referenceCountry1" styleId="referenceCountry1" style="display:none;"
																tabindex="140" onchange="changestate(this,'referenceState1');">
																<bean:size id="size" name="countryList" />
																<logic:greaterEqual value="0" name="size">
																	<logic:iterate id="country" name="countryList">
																		<bean:define id="name" name="country" property="id"/>
																		<html:option value="<%=name.toString()%>">
																			<bean:write name="country" property="countryname" />
																		</html:option>
																	</logic:iterate>
																</logic:greaterEqual>
															</html:select>
														</logic:equal>
													</logic:present>													
												</div>
											</div>
										 
										  
										     <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:select property="referenceState1" styleId="referenceState1"
														styleClass="chosen-select-width" tabindex="140">
														<html:option value="">--Select--</html:option>
														<logic:present name="editVendorMasterForm" property="refStates1">
															<logic:iterate id="states" name="editVendorMasterForm" property="refStates1">
																<bean:define id="name" name="states" property="id"/>
																<html:option value="<%=name.toString()%>">
																	<bean:write name="states" property="statename" />
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">County/Province</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text styleClass="text-box form-control" property="referenceProvince1"
														alt="" styleId="referenceProvince1" tabindex="140" />
												</div>
											</div>
										
									</div>
								</div>
								<div class="clear"></div>
								
								<div class="panelCenter_1" style="margin-top: 2%;">
									<h3>Reference 2</h3>
									<div class="form-box card-body">
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Company Name</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceCompanyName2" alt="" styleClass="text-box form-control"
														styleId="referenceCompanyName2" tabindex="141" />
												</div>
											 </div>
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Name</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceName2" alt="Optional"
														styleClass="text-box form-control" styleId="referenceName2" tabindex="141" />
												</div>
											</div>
									   
									   	
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Address</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:textarea property="referenceAddress2"
														styleId="referenceAddress2" alt="Optional" styleClass="main-text-area" tabindex="142" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referencePhone2" alt=""
														styleClass="text-box form-control" styleId="referencePhone2" style="width:45%;" tabindex="143" />
												</div>
											</div>
									  
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Ext</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="extension2" styleId="extension2" size="3"
														styleClass="text-box form-control" alt="Optional"  tabindex="144" />
												</div>
											</div>
									        <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceMailId2" alt="Optional"
														styleClass="text-box form-control" styleId="referenceMailId2" tabindex="145" />
												</div>
											</div>
									  	
									  	
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceMobile2" alt="Optional"
														styleClass="text-box form-control" styleId="referenceMobile2" tabindex="146" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceCity2" alt="Optional"
														styleClass="text-box form-control" styleId="referenceCity2" tabindex="147" />
												</div>
											</div>
									   	
									   	
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Zip</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceZip2" alt="Optional"
														styleClass="text-box form-control" styleId="referenceZip2" tabindex="148" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<logic:present name="userDetails" property="workflowConfiguration">
														<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
															<html:select property="referenceCountry2" styleId="referenceCountry2"
																tabindex="149" onchange="changestate(this,'referenceState2');" styleClass="chosen-select-width">
																<html:option value="">--Select--</html:option>
																<bean:size id="size" name="countryList" />
																<logic:greaterEqual value="0" name="size">
																	<logic:iterate id="country" name="countryList">
																		<bean:define id="name" name="country" property="id"/>
																		<html:option value="<%=name.toString()%>">
																			<bean:write name="country" property="countryname" />
																		</html:option>
																	</logic:iterate>
																</logic:greaterEqual>
															</html:select>
														</logic:equal>
														<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
															<input type="text" class="text-box form-control" id="referenceCountryValue2" readonly="readonly">
															<html:select property="referenceCountry2" styleId="referenceCountry2" style="display:none;"
																tabindex="149" onchange="changestate(this,'referenceState2');">
																<bean:size id="size" name="countryList" />
																<logic:greaterEqual value="0" name="size">
																	<logic:iterate id="country" name="countryList">
																		<bean:define id="name" name="country" property="id"/>
																		<html:option value="<%=name.toString()%>">
																			<bean:write name="country" property="countryname" />
																		</html:option>
																	</logic:iterate>
																</logic:greaterEqual>
															</html:select>
														</logic:equal>
													</logic:present>
												</div>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:select property="referenceState2" styleId="referenceState2"
														styleClass="chosen-select-width" tabindex="150">
														<html:option value="">--Select--</html:option>
														<logic:present name="editVendorMasterForm" property="refStates2">
															<logic:iterate id="states" name="editVendorMasterForm" property="refStates2">
																<bean:define id="name" name="states" property="id"/>
																<html:option value="<%=name.toString()%>">
																	<bean:write name="states" property="statename" />
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">County/Province</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text styleClass="text-box form-control" property="referenceProvince2"
														alt="Optional" styleId="referenceProvince2" tabindex="150" />
												</div>
											</div>
					                	
				                	</div>
				                </div>
								<div class="clear"></div>
								
								<div class="panelCenter_1" style="margin-top: 2%;">
									<h3>Reference 3</h3>
									<div class="form-box card-body">
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Company Name</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceCompanyName3" alt="" styleClass="text-box form-control"
														styleId="referenceCompanyName3" tabindex="151" />
												</div>
											</div>
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Name</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceName3" alt="Optional"
														styleClass="text-box form-control" styleId="referenceName3" tabindex="151" />
												</div>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Address</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:textarea property="referenceAddress3"
														styleId="referenceAddress3" alt="Optional" styleClass="main-text-area" tabindex="152" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referencePhone3" alt="" tabindex="153"
														styleClass="text-box form-control" styleId="referencePhone3" style="width:45%;" />
												</div>
											</div>
									 	
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Ext</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="extension3" styleId="extension3" size="3"
														styleClass="text-box form-control" alt="Optional" style="width:22%;" tabindex="154" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceMailId3" alt="Optional"
														styleClass="text-box form-control" styleId="referenceMailId3" tabindex="155" />
												</div>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceMobile3" alt="Optional"
														styleClass="text-box form-control" styleId="referenceMobile3" tabindex="156" />
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">City</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceCity3" alt="Optional"
														styleClass="text-box form-control" styleId="referenceCity3" tabindex="157" />
												</div>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Zip</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text property="referenceZip3" alt="Optional"
														styleClass="text-box form-control" styleId="referenceZip3" tabindex="158" />
												</div>
											</div>
									        <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Country</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<logic:present name="userDetails" property="workflowConfiguration">
														<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
															<html:select property="referenceCountry3" styleId="referenceCountry3"
																tabindex="159" onchange="changestate(this,'referenceState3');" styleClass="chosen-select-width">
																<html:option value="">--Select--</html:option>
																<bean:size id="size" name="countryList" />
																<logic:greaterEqual value="0" name="size">
																	<logic:iterate id="country" name="countryList">
																		<bean:define id="name" name="country" property="id"/>
																		<html:option value="<%=name.toString()%>">
																			<bean:write name="country" property="countryname" />
																		</html:option>
																	</logic:iterate>
																</logic:greaterEqual>
															</html:select>
														</logic:equal>
														<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
															<input type="text" class="text-box form-control" id="referenceCountryValue3" readonly="readonly">
															<html:select property="referenceCountry3" styleId="referenceCountry3" style="display:none;"
																tabindex="159" onchange="changestate(this,'referenceState3');">
																<bean:size id="size" name="countryList" />
																<logic:greaterEqual value="0" name="size">
																	<logic:iterate id="country" name="countryList">
																		<bean:define id="name" name="country" property="id"/>
																		<html:option value="<%=name.toString()%>">
																			<bean:write name="country" property="countryname" />
																		</html:option>
																	</logic:iterate>
																</logic:greaterEqual>
															</html:select>
														</logic:equal>
													</logic:present>
												</div>
											</div>
										
										
											<div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">State</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:select property="referenceState3" styleId="referenceState3"
														styleClass="chosen-select-width" tabindex="160">
														<html:option value="">--Select--</html:option>
														<logic:present name="editVendorMasterForm" property="refStates3">
															<logic:iterate id="states" name="editVendorMasterForm" property="refStates3">
																<bean:define id="name" name="states" property="id"/>
																<html:option value="<%=name.toString()%>">
																	<bean:write name="states" property="statename" />
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</div>
											</div>
										    <div class="row-wrapper form-group row">
												<label class="col-sm-3 label-col-wrapper control-label text-sm-right">County/Province</label>
												<div class="col-sm-7 ctrl-col-wrapper">
													<html:text styleClass="text-box form-control" property="referenceProvince3"
														alt="Optional" styleId="referenceProvince3" tabindex="160" />
												</div>
											</div>
										
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit2" name="saveandexit2"> 
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3" name="submit3">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=otherCertificateNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showBusinessBiography();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=otherCertificateNavigation" styleClass="btn">Next</html:link>
								<input type="button" class="btn" value="Back" onclick="showBusinessBiography();"> 
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">References</h2>
								<br/>
								<h3>You have no rights to view references information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<html:link action="/${back}.do?parameter=otherCertificateNavigation" styleClass="btn">Next</html:link>								
								<input type="button" class="btn" value="Back" onclick="showBusinessBiography();"> 
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
		</html:form>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
	$(document).ready(function() 
	{
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="References Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);
					//$('#step-1 :radio').attr('disabled', true);
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		$("#menu9").removeClass().addClass("current");
		
		if($("#referenceName2").val() === 'Optional'){
			$("#referenceName2").val('');
		}
		
		if($("#extension2").val() === 'Optional'){
			$("#extension2").val('');
		}
		
		if($("#referenceMailId2").val() === 'Optional'){
			$("#referenceMailId2").val('');
		}
		
		if($("#referenceCity2").val() === 'Optional'){
			$("#referenceCity2").val('');
		}
		
		if($("#referenceZip2").val() === 'Optional'){
			$("#referenceZip2").val('');
		}
		
		if($("#referenceProvince2").val() === 'Optional'){
			$("#referenceProvince2").val('');
		}
		
		if($("#referenceName3").val() === 'Optional'){
			$("#referenceName3").val('');
		}
		
		if($("#extension3").val() === 'Optional'){
			$("#extension3").val('');
		}
		
		if($("#referenceMailId3").val() === 'Optional'){
			$("#referenceMailId3").val('');
		}
		
		if($("#referenceCity3").val() === 'Optional'){
			$("#referenceCity3").val('');
		}
		
		if($("#referenceZip3").val() === 'Optional'){
			$("#referenceZip3").val('');
		}
		
		if($("#referenceProvince3").val() === 'Optional'){
			$("#referenceProvince3").val('');
		}
		
		$("#referenceCountryValue1").val($("#referenceCountry1 option:selected").text());
		$("#referenceCountryValue2").val($("#referenceCountry2 option:selected").text());
		$("#referenceCountryValue3").val($("#referenceCountry3 option:selected").text());
	});
	
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function($) {
		 $("#referenceMobile1").mask("(999) 999-9999?");
		 $("#referenceMobile2").mask("(999) 999-9999?");
		 $("#referenceMobile3").mask("(999) 999-9999?");
		 $("#referencePhone1").mask("(999) 999-9999?");
		 $("#referencePhone2").mask("(999) 999-9999?");
		 $("#referencePhone3").mask("(999) 999-9999?");
	});
	
	jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
        return this.optional(element) || /^[0-9]+$/.test(value);
  	},"Please enter only numbers.");
	
	jQuery.validator.addMethod("allowhyphens", function(value, element) { 
        return this.optional(element) || /^[0-9-]+$/.test(value);
  	},"Please enter valid numbers.");
	
	jQuery.validator.addMethod("alpha", function(value, element) {
		return this.optional(element) || /^[A-Za-z ]+$/.test(value);
	}, "Special Characters, Numbers not Allowed.");
	
	$(function() {
		$("#referenceForm").validate({
			rules : {
				referenceCompanyName1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				referenceName1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}, alpha : true},
				referenceName2:{ alpha : true},
				referenceName3:{ alpha : true},
				referenceAddress1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				referencePhone1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				referenceMailId1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}, 
					email : true
				},
				referenceCity1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}, 
					alpha : true
				},
				referenceCity2:{ alpha : true},
				referenceCity3:{ alpha : true},
				referenceZip1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}, 
					allowhyphens : true
				},
				referenceState1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							if($("#referenceCountry1 option:selected").text()=='United States') {
								return true;
							}else{
								return false;
							}
						} else {
							return false;
						}
					}
				},
				referenceCountry1:{
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				referenceProvince1: {
					required : function(element) {
						if ($('#submit').data('clicked') || $('#submit1').data('clicked') 
								|| $('#submit2').data('clicked') || $('#submit3').data('clicked')) {
							if($("#referenceCountry1 option:selected").text()!='United States' && $('#referenceCountry1').val()!='' && $('#referenceState1').val()=='') {
								return true;
							}else{
								return false;
							}
						} else {
							return false;
						}
					}, 
					alpha : true
				},
	            extension1: {onlyNumbers : true},referenceMailId2:{email : true},
				referenceProvince2 : {
					required : function(element) {
						if($("#referenceCountry2 option:selected").text()!='United States' && $('#referenceCountry2').val()!='' && $('#referenceState2').val()=='') {
							return true;
						}else{
							return false;
						}
					},
					maxlength : 60, alpha : true
				},
				referenceProvince3 : {
					required : function(element) {
						if($("#referenceCountry3 option:selected").text()!='United States' && $('#referenceCountry3').val()!='' && $('#referenceState3').val()=='') {
							return true;
						}else{
							return false;
						}
					},
					maxlength : 60,alpha : true
				},
				referenceZip2:{allowhyphens : true},	extension2: {onlyNumbers : true},referenceMailId3:{email : true},
				referenceZip3:{allowhyphens : true},	extension3: {onlyNumbers : true}
			},
			submitHandler : function(form) {
				if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
					$('#referenceForm').append("<input type='hidden' name='submitType' value='submit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
					$('#referenceForm').append("<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				} else if ($('#saveandexit1').data('clicked')
						|| $('#saveandexit2').data('clicked')){
					$('#referenceForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});	
</script>