<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:present name="currentUser" property="id">
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>	
</logic:present>
<logic:present name="vendorUser" property="vendorId">	
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Contact Information" name="privilege" property="objectId.objectName">
		<div id="content-area">
			<div id="wizard" class="swMain edit-vendor-profile">
				<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
				<div class="stepContainer">
					<bean:define id="cancelUrl" name="cancelString"></bean:define>
					<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
						<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
						<html:link action="/${back}.do?parameter=companyInfoNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
					</div>
					<logic:equal value="1" name="privilege" property="view">
						<div id="step-1" class="content">
							<h2 class="step-title text-center">Contact Information</h2>
							<div id="contacts">
								<logic:present name="currentUser" property="id">
									<jsp:include page="/WEB-INF/pages/edit_vendorcontact.jsp"></jsp:include>
								</logic:present>
								<logic:present name="vendorUser" property="vendorId">
									<jsp:include page="/WEB-INF/vendorpages/edit_primevendorcontact.jsp"></jsp:include>
								</logic:present>
							</div>
						</div>
					</logic:equal>
					<logic:equal value="0" name="privilege" property="view">
						<div id="step-1" class="content" style="display: block;">
							<h2 class="card-title pull-left">Contact Information</h2>
							<br/>
							<h3>You have no rights to view contact information.</h3>
						</div>
					</logic:equal>
					<div class="actionBar bottom_actionbar_fix">
						<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
						<html:link action="/${back}.do?parameter=companyInfoNavigation" styleClass="btn btn-primary mr-1">Next</html:link>					
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		$("#menu1").removeClass().addClass("current");
	});
//-->
</script>
