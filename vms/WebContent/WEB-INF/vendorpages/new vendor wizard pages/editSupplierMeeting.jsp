<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>

<logic:present name="currentUser" property="id">
	<c:set var="action" value="updatemeetinginfo" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimemeetinginfo" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if (e.which == 13) 
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else if($(e.target).attr('id') == 'noteSave')
				{
					e.preventDefault();
					saveMeetingNotes();
				}else if($(e.target).attr('id') == 'noterfiSave')
				{
					e.preventDefault();
					saverfiMeetingNotes();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
		
	function showDocumentsPage()
	{
		window.location = "${back}.do?parameter=documentsNavigation";
	}
	
	$(document).ready(function()
	{
		var vendorNotesId = $("#vendorNotesId").val();
		var showOrHideNotesButton = "Hide";
		if(vendorNotesId != "" && vendorNotesId != null && vendorNotesId >0)
		{
			$("#noteSave").show();
			$("#vendorNotesTextAreaId").show();
			showOrHideNotesButton = "Show";
		}
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="Vendor Notes and Meeting Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);					
				</logic:equal>
				if(showOrHideNotesButton == "Hide")
				{
					<logic:equal value="0" name="privilege1" property="add">
						$("#noteSave").hide();
						$("#vendorNotesTextAreaId").hide();
					</logic:equal>
				}
			</logic:equal>
		</logic:iterate>	

		if($("#contactState option:selected").text() != "- Select -")
		{
			$("#stateValue").val($("#contactState option:selected").text());
		}
		
		var contactName = $("#contactFirstName").val();
		if(contactName != "")
		{
			$("#metYes").attr('checked', true);
		}
		
		/* $.datepicker.setDefaults({
			changeYear : true,
			dateFormat : 'mm/dd/yy'
			});
		$("#contactDate").datepicker();  */
		
		$('#metNo').click(function()
		{
		   $("#meetingInfo").hide();
		});
		
		$('#metYes').click(function()
		{
		   $("#meetingInfo").show();
		});
	});
</script>

<style>
	<!--
	.swMain div.actionBar
	{
		z-index: 0 !important;
	}
	
	.swMain .stepContainer .StepTitle
	{
		z-index: 0 !important;
	}
	-->
</style>

<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Vendor Notes and Meeting Information" name="privilege" property="objectId.objectName">
		<html:form action="${action}.do?method=saveMeetingInfo" method="post" styleId="meetingForm">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<span><bean:write name="msg" /></span>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<span><bean:write name="msg" /></span>
				</html:messages>
			</div>
			
			<logic:equal value="1" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn" value="Save and Exit" id="saveandexit1" name="saveandexit1">
								</logic:equal>
								<input type="button" class="btn" value="Back" onclick="showDocumentsPage();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn" value="Save" id="submit" name="submit">
									<input type="reset" class="btn" value="Reset" id="reset">
								</logic:equal>
							</div>
							
							<div id="step-1" class="content" style="display: block;">
								<logic:present name="currentUser" property="id">
									<h2 class="StepTitle">Internal Notes About This Vendor</h2>
									<div class="panelCenter_1">
										<div class="wrapper-full" id="bpsupplier">
											<div class="row-wrapper form-group row">
												<html:hidden property="vendorNotesId" styleId="vendorNotesId"/>
												<div class="ctrl-col-wrapper" style="padding: 1%; width: 100%">
													<html:textarea property="vendorNotes" styleClass="main-text-area"
														cols="100" tabindex="205" style="height:90px;padding:0;" styleId="vendorNotesTextAreaId">
													</html:textarea>
												</div>
												<%-- <logic:equal value="1" name="privilege" property="add"> --%>
													<input type="button" id="noterfiSave" value="Save RFI/RPF Notes" class="btn btn-primary" onclick="saverfiMeetingNotes();"/>
												<%-- </logic:equal> --%>												
											</div>
										</div>
									</div>
								</logic:present>
								
								<div class="form-box">
									<table class='main-table' border='0' style='border: none; border-collapse: collapse; float: none;'>
										<thead>
											<tr>
												<td class='header' width="15%">Created by</td>
												<td class='header'  width="20%">Status date</td>
												<td class='header'  width="50%">Notes</td>
												<td class='header' width="10%">Action</td>
											</tr>
										</thead>
										<tbody>
											<logic:present name="editVendorMasterForm" property="meetingNotesList">
												<bean:size id="size" name="editVendorMasterForm" property="meetingNotesList" />
												<logic:equal value="0" name="size">
													<tr>
														<td>Nothing to display</td>
													</tr>
												</logic:equal>
											</logic:present>
											<logic:present name="editVendorMasterForm" property="meetingNotesList">
												<logic:iterate id="noteStatus" name="editVendorMasterForm" property="meetingNotesList" indexId="index">
													<bean:define id="meetingId" name="noteStatus" property="meetingNotesId"></bean:define>
													<tr id="row<%=index%>" class="even">
														<td width="15%"><bean:write property="createdBy" name="noteStatus"/> </td>
														<td width="20%"><bean:write property="createdOn" name="noteStatus"/></td>
														<td width="50%"><bean:write property="notes" name="noteStatus"/></td>	
														<td width="10%" align="center">
															<logic:equal value="1" name="privilege" property="modify"> 
																<a href="#" onclick="editMeeting('<%=meetingId.toString()%>');">Edit</a> 
																<logic:equal value="1" name="privilege" property="delete">/</logic:equal>
															</logic:equal>
															<logic:equal value="1" name="privilege" property="delete"> 
																<a href="#" onclick="deleteMeeting('<%=meetingId.toString()%>');">Delete</a>
															</logic:equal>
														</td>								
													</tr>
												</logic:iterate>
											</logic:present>
										</tbody>
									</table>	
								</div>	
								
								<logic:present name="currentUser" property="id">
									<h2 class="StepTitle">RFI/RFP Notes</h2>
									<div class="panelCenter_1">
										<div class="wrapper-full" id="bpsupplier">
											<div class="row-wrapper form-group row">
												<html:hidden property="vendorrfiNotesId" styleId="vendorrfiNotesId"/>
												<div class="ctrl-col-wrapper" style="padding: 1%; width: 100%">
													<html:textarea property="vendorrfiNotes" styleClass="main-text-area"
														cols="100" tabindex="205" style="height:90px;padding:0;" styleId="vendorrfiNotesTextAreaId">
													</html:textarea>
												</div>
												<%-- <logic:equal value="1" name="privilege" property="add"> --%>
													<input type="button" id="noteSave" value="Save RFI/RPF Notes" class="btn" onclick="saverfiMeetingNotes();"/>
												<%-- </logic:equal> --%>												
											</div>
										</div>
									</div>
								</logic:present>
								
								
								<div class="form-box">
									<table class='main-table' border='0' style='border: none; border-collapse: collapse; float: none;'>
										<thead>
											<tr>
												<td class='header' width="15%">Created by</td>
												<td class='header'  width="20%">Status date</td>
												<td class='header'  width="50%">Notes</td>
												<td class='header' width="10%">Action</td>
											</tr>
										</thead>
										<tbody>
											<logic:present name="editVendorMasterForm" property="rfiNotesList">
												<bean:size id="size" name="editVendorMasterForm" property="rfiNotesList" />
												<logic:equal value="0" name="size">
													<tr>
														<td>Nothing to display</td>
													</tr>
												</logic:equal>
											</logic:present>
											<logic:present name="editVendorMasterForm" property="rfiNotesList">
												<logic:iterate id="noterfiStatus" name="editVendorMasterForm" property="rfiNotesList" indexId="indexrfi">
													<bean:define id="meetingrfiId" name="noterfiStatus" property="rfiNotesId"></bean:define>
													<tr id="row<%=indexrfi%>" class="even">
														<td width="15%"><bean:write property="createdBy" name="noterfiStatus"/> </td>
														<td width="20%"><bean:write property="createdOn" name="noterfiStatus"/></td>
														<td width="50%"><bean:write property="notes" name="noterfiStatus"/></td>	
														<td width="10%" align="center">
															<logic:equal value="1" name="privilege" property="modify"> 
																<a href="#" onclick="editrfiMeeting('<%=meetingrfiId.toString()%>');">Edit</a> 
																<logic:equal value="1" name="privilege" property="delete">/</logic:equal>
															</logic:equal>
															<logic:equal value="1" name="privilege" property="delete"> 
																<a href="#" onclick="deleterfiMeeting('<%=meetingrfiId.toString()%>');">Delete</a>
															</logic:equal>
														</td>								
													</tr>
												</logic:iterate>
											</logic:present>
										</tbody>
									</table>	
								</div>	
								
<%-- 								<logic:present name="currentUser" property="id">
									<h2 class="StepTitle">RFI/RFP Notes</h2>
									<div class="panelCenter_1">
										<div class="wrapper-full" id="bpsupplier">
											<div class="row-wrapper form-group row">
												<html:hidden property="vendorNotesId" styleId="vendorNotesId"/>
												<div class="ctrl-col-wrapper" style="padding: 1%; width: 100%">
													<html:textarea property="vendorRfiRfpNotes" styleClass="main-text-area"
														cols="100" tabindex="205" style="height:90px;padding:0;" styleId="vendorRfiRfpNotes">
													</html:textarea>
												</div>
												<logic:equal value="1" name="privilege" property="add">
													<input type="button" id="noteSave" value="Save Notes" class="btn" onclick="saveRfiRfpNotes();"/>
												</logic:equal>												
											</div>
										</div>
									</div>
								</logic:present>	 --%>		
								
								<logic:notPresent name="currentUser" property="id">
									<h2 class="StepTitle">Vendor Meeting Information</h2>
								</logic:notPresent>
								<div class="clear"></div>
								
								<div class="panelCenter_1">
									<h3 class="left">Contact Meeting Information</h3>
									<!-- <div>
									<label>Have you met with a BP Supplier Diversity personnel recently?</label>
											 <label for="metYes">Yes</label>&nbsp;<input type="radio" value="1" name="metBpSupplier" id="metYes">
											 <label for="metNo">No</label>&nbsp;<input type="radio" value="0" name="metBpSupplier" id="metNo">
									</div> -->
									<div class="form-box" id="meetingInfo">
										<div class="wrapper-half">
											<div class="row-wrapper form-group row">
												<div class="label-col-wrapper">Contact First Name</div>
												<div class="ctrl-col-wrapper">
													<html:text property="contactFirstName" alt="" styleId="contactFirstName" styleClass="text-box"  readonly="true" />
												</div>
											</div>
											<div class="row-wrapper form-group row">
												<div class="label-col-wrapper">Contact Last Name</div>
												<div class="ctrl-col-wrapper">
													<html:text property="contactLastName" alt="" styleId="contactLastName" styleClass="text-box"  readonly="true" />
												</div>
											</div>
										</div>
										<div class="wrapper-half">
											<div class="row-wrapper form-group row">
												<div class="label-col-wrapper">Contact Date</div>
												<div class="ctrl-col-wrapper">
													<html:text property="contactDate" alt="Please click to select date" styleId="contactDate" styleClass="text-box" readonly="true" />
												</div>
											</div>
											<div class="row-wrapper form-group row">
												<div class="label-col-wrapper">Contact State</div>
												<div class="ctrl-col-wrapper">
													<input type="text" class="text-box" id="stateValue" readonly="readonly">
													<html:select property="contactState" styleId="contactState" style="display:none;">
														<html:option value="">- Select -</html:option>
														<logic:present name="contactStates">
															<logic:iterate id="states" name="contactStates">
																<bean:define id="name" name="states" property="id"></bean:define>
																<html:option value="<%=name.toString()%>">
																	<bean:write name="states" property="statename" />
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</div>
											</div>
										</div>
										<div class="wrapper-half">
											<div class="row-wrapper form-group row">
												<div class="label-col-wrapper">Contact Event</div>
												<div class="ctrl-col-wrapper">
													<html:textarea property="contactEvent" alt="" readonly="true" styleId="contactEvent" styleClass="main-text-area" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								
								<div class="panelCenter_1">
									<h3 class="left">Vendor Status</h3>									
									<div class="form-box">
										<table class='main-table' border='0' style='border: none; border-collapse: collapse; float: none;'>
											<thead>
												<tr>
													<td class='header' width="20%">Status Date</td>
													<td class='header'  width="15%">Status</td>
													<td class='header'  width="65%">Comments</td>
												</tr>
											</thead>
											<tbody>
												<logic:present name="editVendorMasterForm" property="vendorStatusList">
													<bean:size id="size" name="editVendorMasterForm" property="vendorStatusList" />
													<logic:equal value="0" name="size">
														<tr>
															<td>Nothing to display</td>
														</tr>
													</logic:equal>
												</logic:present>
												<logic:present name="editVendorMasterForm" property="vendorStatusList">
													<logic:iterate id="vendorStatus" name="editVendorMasterForm" property="vendorStatusList" indexId="index">
														<tr id="row<%=index%>" class="even">
															<td width="20%"><bean:write property="statusDate" name="vendorStatus"/> </td>
															<td width="15%"><bean:write property="status" name="vendorStatus"/></td>
															<td width="65%"><bean:write property="comments" name="vendorStatus"/> </td>										
														</tr>
													</logic:iterate>
												</logic:present>
											</tbody>
										</table>
									</div>
								</div>
								<div class="clear"></div>
								
								<div class="panelCenter_1">
									<h3 class="left">Email History</h3>
									<div class="form-box">
										<input type="button" id="emailThisVendorButton" value="Email this Vendor" class="btn" onclick="emailThisVendor();" style="float: left;"/>
										<table class='main-table' border='0' style='border: none; border-collapse: collapse; float: none;'>
											<thead>
												<tr>
													<td class='header' width="20%">Email Date</td>
													<td class='header' width="5%">Type</td>
													<td class='header' width="35%">Email ID</td>
													<td class='header' width="40%">Subject</td>
													<!-- <td class='header' width="10%">Action</td> -->
												</tr>
											</thead>
											<tbody>
												<logic:present name="editVendorMasterForm" property="emailHistoryList">
													<bean:size id="size" name="editVendorMasterForm" property="emailHistoryList"/>
													<logic:equal value="0" name="size">
														<tr>
															<td>Nothing to display</td>
														</tr>
													</logic:equal>
													<logic:greaterThan value="0" name="size">
														<logic:iterate id="emailHistory" name="editVendorMasterForm" property="emailHistoryList" indexId="index">
															<bean:define id="emailHistoryId1" name="emailHistory" property="emailHistoryId"/>
															<tr id="row<%=index%>" class="even">
																<td width="20%"><bean:write property="emailDate" name="emailHistory"/> </td>
																<td width="5%"><bean:write property="addressType" name="emailHistory"/> </td>
																<td width="35%"><bean:write property="emailId" name="emailHistory"/></td>
																<td width="40%"><bean:write property="emailSubject" name="emailHistory"/> </td>
																<%-- <td width="10%" align="center">																	
																	<a href="#" onclick="viewEmailMessage('<%=emailHistoryId1.toString()%>');">View</a>
																</td> --%>
															</tr>
														</logic:iterate>
													</logic:greaterThan>
												</logic:present>
												<logic:notPresent name="editVendorMasterForm" property="emailHistoryList">
													<tr>
														<td>Nothing to display</td>
													</tr>
												</logic:notPresent>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn" value="Save and Exit" id="saveandexit2" name="saveandexit2">
								</logic:equal> 
								<input type="button" class="btn" value="Back" onclick="showDocumentsPage();">
								<logic:equal value="1" name="privilege" property="modify"> 
									<input type="submit" class="btn" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn" value="Reset" id="reset">
								</logic:equal>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>								
								<input type="button" class="btn" value="Back" onclick="showDocumentsPage();">								
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">Vendor Notes</h2>
								<br/>
								<h3>You have no rights to view vendor notes and meeting information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn">Exit</html:link>
								<input type="button" class="btn" value="Back" onclick="showDocumentsPage();"> 
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
		</html:form>
	</logic:equal>
</logic:iterate>

<script type="text/javascript">
	$(document).ready(function() {
		$("#menu12").removeClass().addClass("current");
	});
	
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function()
	{
		$("#meetingForm").validate(
		{
			rules :
			{
				contactFirstName : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactLastName : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactState : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactDate : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				contactEvent : {
					required : function(element) {
						if ($("#metYes").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			submitHandler : function(form)
			{
				if ($('#submit').data('clicked') || $('#submit2').data('clicked'))
				{
					$('#meetingForm').append("<input type='hidden' name='submitType' value='save' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
				else if ($('#saveandexit1').data('clicked') || $('#saveandexit2').data('clicked'))
				{
					$('#meetingForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
					$("#ajaxloader").css('display', 'block');
					$("#ajaxloader").show();
					form.submit();
				}
			},
			invalidHandler : function(form,	validator)
			{
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
 
	function editMeeting(meetingId)
	{
		if(meetingId != "" && meetingId != 0)
		{
			window.location = "vendornavigation.do?parameter=editMeetingNotes&vendorNotesId="+meetingId;
		}
			
	}
	
	function deleteMeeting(meetingId)
	{
		input_box = confirm("Are you sure you want to delete this vendor note?");
		if (input_box == true) 
		{
			if(meetingId != "" && meetingId != 0)
			{
				window.location = "vendornavigation.do?parameter=deleteMeetingNotes&vendorNotesId="+meetingId;
			}
		}
	}
	
	function saveMeetingNotes()
	{
		var vendorNotesId = $("#vendorNotesId").val();	
		var vendorNotesTextAreaId = $.trim($('#vendorNotesTextAreaId').val());
		
		if(vendorNotesTextAreaId != null && vendorNotesTextAreaId != "")
		{
			window.location = "vendornavigation.do?parameter=save_Or_Update_MeetingNotes&vendorNotesId="+vendorNotesId+"&vendorNotes="+vendorNotesTextAreaId;
		}
		else
		{
			alert("Enter vendor notes");
			return false;
		}
	}



	function editrfiMeeting(meetingId)
	{
		if(meetingId != "" && meetingId != 0)
		{
			window.location = "vendornavigation.do?parameter=editrfiMeetingNotes&vendorrfiNotesId="+meetingId;
		}
			
	}
	
	function deleterfiMeeting(meetingId)
	{
		input_box = confirm("Are you sure you want to delete this vendor note?");
		if (input_box == true) 
		{
			if(meetingId != "" && meetingId != 0)
			{
				window.location = "vendornavigation.do?parameter=deleterfiMeetingNotes&vendorrfiNotesId="+meetingId;
			}
		}
	}
	
	function saverfiMeetingNotes()
	{
		var rfiNotesId = $("#vendorrfiNotesId").val();	
		var vendorrfiNotesTextArea = $.trim($('#vendorrfiNotesTextAreaId').val());
		
		if(vendorrfiNotesTextArea != null && vendorrfiNotesTextArea != "")
		{
			window.location = "vendornavigation.do?parameter=save_Or_Update_rfiMeetingNotes&vendorrfiNotesId="+rfiNotesId+"&vendorrfiNotes="+vendorrfiNotesTextArea;
		}
		else
		{
			alert("Enter vendor RFI/RFP notes");
			return false;
		}
	}
	

	
	function saveRfiRfpNotes()
	{
		var vendorRfiRfpNotes = $("#vendorRfiRfpNotes").val();	
		
		if(vendorNotesTextAreaId != null && vendorNotesTextAreaId != "")
		{
			window.location = "vendornavigation.do?parameter=saveRfiRfpNotes&vendorRfiRfpNotes="+vendorRfiRfpNotes;
		}
		else
		{
			alert("Enter vendor RFI/RFP notes");
			return false;
		}
	}
	
	function emailThisVendor()
	{		
		var vendorId = $("#vendorId").val();
		window.location = "mailNotification.do?parameter=showEmailThisVendorPage&requestFrom=vendorProfilePage&vendorId=" + vendorId;
	}
</script>