<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<!-- Files used for load jqgrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<logic:present name="currentUser" property="id">
	<c:set var="action" value="updateservicearea" />
	<c:set var="back" value="vendornavigation" />
	<bean:define id="commonPrivileges" name="privileges"/>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<c:set var="action" value="updateprimeservicearea" />
	<c:set var="back" value="primevendornavigation" />
	<bean:define id="commonPrivileges" name="rolePrivileges"/>
</logic:present>

<script type="text/javascript">
	$(document).keypress(function(e) 
	{
		if(e.which == 13)
		{
			if(e.target.nodeName != 'TEXTAREA')
			{
				if($(e.target).attr('id') == 'search')
				{
					searchNaicsCodes();
				}
				else if($(e.target).attr('id') == 'isApprovedDesc')
				{				
					e.preventDefault();
				}
				else if($(e.target).attr('id') == 'statusButton')
				{
					e.preventDefault();
					getSelectedIds();
				}
				else
				{
					e.preventDefault();
					$('#submit').click();
				}
			}		
		}
	});
	
	function searchNaicsCodes()
	{
		var text = $("#search").val();
		var postdata = $("#addtree").jqGrid('getGridParam', 'postData');
		var isNumber = $.isNumeric(text);
		$("#addtree").jqGrid('resetSelection');
		if(isNumber){
			naicsCode = '';
			des = '';
			$("#addtree").jqGrid('resetSelection');
			$.extend(postdata, {
				filters : '',
				searchField : 'code',
				searchOper : 'eq',
				searchString : text
			});
		} else {
			$.extend(postdata, {
				filters : '',
				searchField : 'name',
				searchOper : 'bw',
				searchString : text
			});
		}
		$("#addtree").jqGrid('setGridParam', {
			search : text.length > 0,
			postData : postdata
		});
		$("#addtree").trigger("reloadGrid", [ {
			page : 1
		} ]);
	}
		
	function showOwnerInfo() {
		window.location = "${back}.do?parameter=diverseClassificationNavigation";
	}
	
	function pickNaics(value) {
		press = value;
		var search=$("#search").val();
		if(search!=null && search!=''){
			$('#search').val('');
			$("#addtree").jqGrid('GridUnload');
			naicsCode = '';
			des = '';
			pickNaicsCode();
		}
		else
			pickNaicsCode();
		var wWidth = $(window).width();
		var dWidth = wWidth * 0.73;
		var wHeight = $(window).height();
		var dHeight = wHeight * 0.85;
		$(function() {
			$("#dialog").css({
				'display' : 'block',
				'font-size' : 'inherit'
			});
			$("#dialog").dialog({
				autoOpen : false,
				width : dWidth,
				height : dHeight,
				modal : true,
				close : function(event, ui) {
					//close event goes here
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
		});
		$("#addtree").jqGrid('resetSelection');
		$("#dialog").dialog("open");
	}
	
	function add() {
		var code1 = document.getElementById("naicsCode_0").value;
		var code2 = document.getElementById("naicsCode_1").value;
		var code3 = document.getElementById("naicsCode_2").value;
		if(code2=='Optional'){
			code2="";
		}
		if(code3=='Optional'){
			code3="";
		}
		if(naicsCode!=""){
			if (((code3!="" && code2!="" && code3 == code2))
					|| ((code3!="" && code1!="" && code3 == code1))
					||(code2!="" && code1!="" && code2 == code1)
					||(naicsCode!="" && naicsCode==code1)||
					(naicsCode==code2)||(naicsCode==code3)) {
				alert("Selected NAICS already Exists ");
			} else{
				if (press == 0) {
					$('#naicsCode_0').val(naicsCode);
					$('#naicsDesc_0').val(des);
					$('#naicsDesc0_0').focus();
				} else if (press == 1) {
					$('#naicsCode_1').val(naicsCode);
					$('#naicsDesc_1').val(des);
					$('#naicsDesc1_1').focus();
				} else if (press == 2) {
					$('#naicsCode_2').val(naicsCode);
					$('#naicsDesc_2').val(des);
					$('#naicsDesc2_2').focus();
				}
				naicsCode = '';
				rowLevel = '';
				des = '';
				press = 0;
				$("#addtree").jqGrid('resetSelection');
				$("#dialog").dialog("close");
			}
		} else {
			alert('Please Select NAICS');
		}
	}
	
	function resetNaics() {
		naicscode = '';
		rowLevel = '';
		des = '';
		press = 0;
		$("#addtree").jqGrid('resetSelection');
		$("#dialog").dialog("close");
	}
	
	$(document).ready(function() {
		<logic:iterate id="privilege1" name="commonPrivileges">
			<logic:equal value="Service Area Information" name="privilege1" property="objectId.objectName">
				<logic:equal value="0" name="privilege1" property="modify">
					$('#step-1 :input').attr('readonly', true);
					$("#step-1 img").removeAttr("onclick");					
				</logic:equal>
				<logic:equal value="0" name="privilege1" property="add">
					$("#step-1 img").removeAttr("onclick");
				</logic:equal>
			</logic:equal>
		</logic:iterate>
		
		/* $("#search").keyup(function(e) {
			if(e.which == 13) {
			
			}
		}); */
		
		if($("#bpsegment option:selected").text() != "-- Select --"){
			$("#bpsegmentIdValue").val($("#bpsegment option:selected").text());
		}
	});// document.ready
</script>

<style>
	.main-text-box {
		width: 86%;
		padding: 0%;
	}
</style>

<!-- For Adding Keyword Dynamically -->
<script type="text/javascript">
	var index = 0;
</script>
<section role="main" class="content-body card-margin pt-2">
	<div class="row">
		<div class="col-lg-12 mx-auto">
<logic:iterate id="privilege" name="commonPrivileges">
	<logic:equal value="Service Area Information" name="privilege" property="objectId.objectName">
		<html:form action="${action}.do?method=saveServiceArea" method="post" styleId="naicsForm">
			<div id="successMsg">
				<html:messages id="msg" property="vendor" message="true">
					<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
				</html:messages>
				<html:messages id="msg" property="transactionFailure" message="true">
					<div class="alert alert-danger nomargin"><bean:write name="msg" /></div>
				</html:messages>
			</div>
			<logic:equal value="1" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit1" name="saveandexit1">
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit1" name="submit1">
								</logic:equal>								
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=businessAreaNavigation" styleClass="btn btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showOwnerInfo();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit" name="submit">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>								
							</div>
							<div id="step-1" class="content">
								<header class="mt-1">
									<h2 class="card-title text-center pb-2">Service Area</h2>
								</header>
								<html:hidden property="naicslastrowcount" value="3" styleId="naicslastrowcount"></html:hidden>
								<html:hidden property="lastrowcount" name="editVendorMasterForm" styleId="lastrowcount" />
								<table style="background: #fff !important;" border="0" class="main-table table table-bordered table-striped mb-0">
									<tr>
										<td class="">NAICS Code</td>
										<td class="">NAICS Desc</td>
										<td class="">Capabilities</td>
										<td class="">NAICS Type</td>
										<td class="">Action</td>
									</tr>
									<tr class="even">
										<td><html:hidden property="naicsID1" styleId="naicsID1" /> <html:text
												property="naicsCode_0" styleId="naicsCode_0" tabindex="86"
												size="10" alt=""
												title="Click the search image to select NAICS code"
												readonly="true" styleClass="main-text-box" /><img
											src="images/magnifier.gif" onClick="pickNaics(0);"> <span
											class="error"><html:errors property="naicsCode_0"></html:errors></span>
										</td>
										<td><html:textarea property="naicsDesc1"
												styleId="naicsDesc_0" readonly="true"
												styleClass="main-text-area" tabindex="87" />
										</td>
										<td><html:textarea property="capabilitie1"
												styleId="naicsDesc0_0" styleClass="main-text-area"
												tabindex="88" />
										</td>
										<td>Primary</td>
									</tr>
									<tr class="even">
										<td><html:hidden property="naicsID2" styleId="naicsID2" /> <html:text
												property="naicsCode_1" styleId="naicsCode_1" tabindex="90"
												size="10" alt="Optional" readonly="true"
												styleClass="main-text-box" /><img src="images/magnifier.gif"
											onClick="pickNaics(1);"> <span class="error"><html:errors
													property="naicsCode_1"></html:errors> </span>
										</td>
										<td><html:textarea property="naicsDesc2"
												styleId="naicsDesc_1" readonly="true"
												styleClass="main-text-area" tabindex="91" />
										</td>
										<td><html:textarea property="capabilitie2"
												styleId="naicsDesc1_1" styleClass="main-text-area"
												tabindex="92" />
										</td>
										<td>Secondary</td>
										<td id="naicsDelete2">
											<logic:equal value="1" name="privilege" property="modify">
												<logic:equal value="1" name="privilege" property="delete">
													<a onclick="return clearNaics('2');" href="#">
														<img src='images/deleteicon.jpg' alt='Delete' />
													</a>
												</logic:equal>
											</logic:equal>
										</td>
									</tr>
									<tr class="even">
										<td><html:hidden property="naicsID3" styleId="naicsID3" /> <html:text
												property="naicsCode_2" styleId="naicsCode_2" tabindex="94"
												size="10" alt="Optional" readonly="true"
												styleClass="main-text-box" /><img src="images/magnifier.gif"
											onClick="pickNaics(2);"> <span class="error"><html:errors
													property="naicsCode_2"></html:errors> </span>
										</td>
										<td><html:textarea property="naicsDesc3"
												styleId="naicsDesc_2" readonly="true"
												styleClass="main-text-area" tabindex="95" />
										</td>
										<td><html:textarea property="capabilitie3"
												styleId="naicsDesc2_2" styleClass="main-text-area"
												tabindex="96" />
										</td>
										<td>Secondary</td>
										<td id="naicsDelete3">
											<logic:equal value="1" name="privilege" property="modify">
												<logic:equal value="1" name="privilege" property="delete">
													<a onclick="return clearNaics('3');" href="#">
														<img src='images/deleteicon.jpg' alt='Delete' />
													</a>
												</logic:equal>
											</logic:equal>
										</td>
									</tr>
								</table>
								<div class="clear"></div>
			
								<div id="dialog" title="NAICS Code" style="display: none;">
									<div class="page-title">
										<input type="search" placeholder="Search" alt="Search"
											style="float: left; width: 100%;" class="main-text-box"
											id="search">
									</div>
									<div class="clear"></div>
									<div id="gridContainer" style="height: 350px; overflow: auto;">
										<table id="addtree" width="100%" class="table table-bordered table-striped mb-0"></table>
										<div id="paddtree"></div>
									</div>
									<div class="btn-wrapper text-center" style="margin: 0px;">
										<input type="button" value="Cancel" class="btn btn-default" onclick="resetNaics();">
										<input type="button" value="Ok" class="btn btn-primary" onclick="add();">
									</div>
								</div>
								<div class="clear"></div>
								
								<div class="panelCenter_1">
									<header class="mt-1">
									<h2 class="card-title text-center pb-2">Keywords</h2>
								</header>
									<div class="form-box card-body">
										<table class='main-table table table-bordered table-striped mb-0' width='100%' border='0' id="keywords"
											style='border: none; border-collapse: collapse; float: none;'>
											<thead>
												<tr>
													<td class=''>Keywords</td>								
													<td class=''>Action</td>
												</tr>
											</thead>
											<tbody>
												<logic:present name="vendorKeywords">
													<bean:size id="size" name="vendorKeywords" />
													<logic:equal value="0" name="size">
														<tr>
															<td>Nothing to display</td>
														</tr>
													</logic:equal>
												</logic:present>
												<logic:present name="vendorKeywords">
													<logic:iterate id="vendorKeyword" name="vendorKeywords" indexId="index">
														<tr id="<%=index %>" class="even">
															<td>
																<logic:present property="keyword" name="vendorKeyword">
																	<bean:define id="keyword" property="keyword" name="vendorKeyword"/>
																	<html:text property="vendorKeyword" value="<%=keyword.toString() %>" styleClass="main-text-box form-control" alt=""/>
																</logic:present>
															</td>
															<td>
																<logic:equal value="1" name="privilege" property="modify">
																	<logic:equal value="1" name="privilege" property="delete">
																		<logic:present property="id" name="vendorKeyword">
																			<bean:define id="id" property="id" name="vendorKeyword"/>
																			<html:link action="deleteKeyword.do?method=deleteVendorKeyword" paramId="id" paramName="id" styleId="deleteFile2" styleClass="del">
																				<img src='images/deleteicon.jpg' alt='Delete'/>
																			</html:link>
																		</logic:present>
																	</logic:equal>
																</logic:equal>
															</td>
														</tr>
														<script type="text/javascript">
															index++;
														</script>
													</logic:iterate>
												</logic:present>
											</tbody>
										</table>
										<div class="clear"></div>
										<logic:equal value="1" name="privilege" property="modify">
											<logic:equal value="1" name="privilege" property="add">
												<INPUT id="cmd1" type="button" value="Add Keyword" class="btn btn-primary" onclick="addRowKeyword('keywords')" />
											</logic:equal>
										</logic:equal>
										<div class="clear"></div>
									</div>
								</div>								
								
								<logic:present name="currentUser">
									<logic:present name="userDetails" property="workflowConfiguration">
										<logic:equal value="1" name="userDetails" property="workflowConfiguration.bpSegment">
											<logic:iterate id="privilege1" name="commonPrivileges">
												<logic:equal value="BP Segment" name="privilege1" property="objectId.objectName">
													<logic:equal value="1" name="privilege1" property="view">
														<bean:define id="vendorStatus" name="userDetails" property="workflowConfiguration.status.id"/>														
														<logic:present name="editVendorMasterForm" property="vendorStatus">
															<logic:equal value="${vendorStatus}" property="vendorStatus" name="editVendorMasterForm">
																<div class="clear"></div>
																<div class="panelCenter_1">
																	<header class="mt-1">
																		<h2 class="card-title text-center pb-2">BP Segment</h2>
																		</header>
																	<div class="form-box card-body">
																		<div class="row-wrapper form-group row">
																			<div class="label-col-wrapper">BP Segment</div>
																			<logic:equal value="1" name="privilege1" property="modify">
																				<div class="ctrl-col-wrapper" style="display: block;">															
																					<html:select  styleClass="chosen-select"  styleId="bpsegment" property="bpSegmentId" >																
																						<logic:present name ="bpSegmentList" >
																							<bean:size id="size" name="bpSegmentList" />
																							<logic:greaterEqual value="1" name="size">
																								<logic:iterate id="bpsegmentIterateId" name="bpSegmentList">
																									<bean:define id="id" name="bpsegmentIterateId" property="id"></bean:define>
																									<html:option value="<%=id.toString()%>">
																										<bean:write name="bpsegmentIterateId" property="segmentName"></bean:write>
																									</html:option>
																								</logic:iterate>
																							</logic:greaterEqual>
																						</logic:present>
																					</html:select>
																				</div>
																			</logic:equal>
																			<logic:equal value="0" name="privilege1" property="modify">
																				<div class="ctrl-col-wrapper" style="display: block;">
																					<input type="text" class="text-box" id="bpsegmentIdValue" readonly="readonly">
																				</div>
																				<div class="ctrl-col-wrapper" style="display: none;">
																					<html:select  styleClass="chosen-select"  styleId="bpsegment" property="bpSegmentId" >
																						<html:option value="0">-- Select --</html:option>
																						<logic:present name ="bpSegmentList" >
																							<bean:size id="size" name="bpSegmentList" />
																							<logic:greaterEqual value="1" name="size">
																								<logic:iterate id="bpsegmentIterateId" name="bpSegmentList">
																									<bean:define id="id" name="bpsegmentIterateId" property="id"></bean:define>
																									<html:option value="<%=id.toString()%>">
																										<bean:write name="bpsegmentIterateId" property="segmentName"></bean:write>
																									</html:option>
																								</logic:iterate>
																							</logic:greaterEqual>
																						</logic:present>
																					</html:select>
																				</div>
																			</logic:equal>														
																		</div>
																	</div>
																</div>
															</logic:equal>
														</logic:present>													
													</logic:equal>										
												</logic:equal>
											</logic:iterate>
										</logic:equal>
									</logic:present>
								</logic:present>																								
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save and Exit" id="saveandexit2" name="saveandexit2"> 
									<input type="submit" class="btn btn-primary mr-1" value="Next" id="submit3" name="submit3">
								</logic:equal>
								<logic:equal value="0" name="privilege" property="modify">
									<html:link action="/${back}.do?parameter=businessAreaNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								</logic:equal>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showOwnerInfo();">
								<logic:equal value="1" name="privilege" property="modify">
									<input type="submit" class="btn btn-primary mr-1" value="Save" id="submit2" name="submit2">
									<input type="reset" class="btn btn-primary mr-1" value="Reset" id="reset">
								</logic:equal>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</logic:equal>
			<logic:equal value="0" name="privilege" property="view">
				<div id="content-area">
					<div id="wizard" class="swMain edit-vendor-profile">
						<jsp:include page="vendorWizardMenu.jsp"></jsp:include>
						<div class="stepContainer">
							<html:hidden property="id" styleId="vendorId"></html:hidden>
							<bean:define id="cancelUrl" name="cancelString"></bean:define>
							<div class="actionBar top_actionbar_fix" style="margin: 3px 0;">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<html:link action="/${back}.do?parameter=businessAreaNavigation" styleClass="btn btn-primary mr-1">Next</html:link>
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showOwnerInfo();"> 
							</div>
							<div id="step-1" class="content" style="display: block;">
								<h2 class="StepTitle">Service Area</h2>
								<br/>
								<h3>You have no rights to view service area information.</h3>
							</div>
							<div class="actionBar bottom_actionbar_fix">
								<html:link action="/${cancelUrl}" styleClass="btn btn-default mr-2">Exit</html:link>
								<html:link action="/${back}.do?parameter=businessAreaNavigation" styleClass="btn btn-primary mr-1">Next</html:link>								
								<input type="button" class="btn btn-primary mr-1" value="Back" onclick="showOwnerInfo();">								
							</div>
						</div>
					</div>
				</div>
			</logic:equal>
		</html:form>
	</logic:equal>
</logic:iterate>
</div>
</div>
</section>	
<script type="text/javascript">
	$(document).ready(function() {
		$("#menu6").removeClass().addClass("current");
	});
	
	jQuery('#submit1').click(function() {
		$(this).data('clicked', true);
	});
	
	jQuery('#submit').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#submit2').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#submit3').click(function(){
		  $(this).data('clicked', true);
	});
	
	$('#saveandexit1').click(function() {
		$(this).data('clicked', true);
	});
	
	$('#saveandexit2').click(function() {
		$(this).data('clicked', true);
	});
	
	$(function() {
		$("#naicsForm").validate({
			rules : {
				vendorKeyword : {					
					maxlength : 100
				}
			},
			submitHandler : function(form) {
				var primaryInfo = $.trim($('#naicsCode_0').val());
				if(primaryInfo != '')
				{
					if ($('#submit1').data('clicked') || $('#submit3').data('clicked')) {
						//if (validateNaics()){
							$('#naicsForm').append("<input type='hidden' name='submitType' value='submit' />");
							$("#ajaxloader").css('display', 'block');
							$("#ajaxloader").show();
							form.submit();
						//}
					} else if ($('#submit').data('clicked') || $('#submit2').data('clicked')) {
						$('#naicsForm').append("<input type='hidden' name='submitType' value='save' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					} else if ($('#saveandexit1').data('clicked')
							|| $('#saveandexit2').data('clicked')){
						$('#naicsForm').append("<input type='hidden' name='submitType' value='saveandexit' />");
						$("#ajaxloader").css('display', 'block');
						$("#ajaxloader").show();
						form.submit();
					}
				}
				else
				{
					alert("Primary Naics information(Row - 1) mandatory");
					$("#ajaxloader").hide();
					$('#submit1').data('clicked', false);
					$('#submit').data('clicked', false);
					$('#submit2').data('clicked', false);
					$('#submit3').data('clicked', false);
					$('#saveandexit1').data('clicked', false);
					$('#saveandexit2').data('clicked', false);
					return false;
				}
			},
			invalidHandler : function(form,	validator) {
				$("#ajaxloader").css('display','none');
				alert("Please fill all the required information before Clicking Next button. The required fields are blank and it will not contain a word 'Optional'");
				$('#submit1').data('clicked', false);
				$('#submit').data('clicked', false);
				$('#submit2').data('clicked', false);
				$('#submit3').data('clicked', false);
				$('#saveandexit1').data('clicked', false);
				$('#saveandexit2').data('clicked', false);
			}
		});
	});
	
	function naicsChecked(id, value) {
		$('#' + id).val(value);
	}
	
	function deleteRow(ele) {
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$(ele).closest("tr").remove();
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}
	
	function addRowKeyword(table) 
	{
		var rowCount = $("#" + table + " tr").length;		
		rowCount -= 2;
		var html = "<tr id='"+rowCount+"' class='even'><td><input type='text' name='vendorKeyword' class='main-text-box'></td></tr>";
		$("#" + table).append(html);
	}
	
	$("#keywords tr td .del").click(function(e) 
	{
		e.preventDefault();
		var row = $(this).closest('tr');
		var rowid = row.attr('id');
		var url = $(this).attr('href');
		var ajaxUrl = url.substring(url.lastIndexOf("/") + 1, url.length);
		
		input_box = confirm("Are You Sure You Want to Delete this Record?");
		if (input_box == true)
		{
			// Output when OK is clicked
			$.ajax(
			{
				url : ajaxUrl,
				type : "POST",
				async : false,
				success : function() 
				{
					$('#' + rowid).remove();
					alert('Record Deleted');
				}
			});
			return false;
		} 
		else 
		{			
			return false;
		}
	});
</script>