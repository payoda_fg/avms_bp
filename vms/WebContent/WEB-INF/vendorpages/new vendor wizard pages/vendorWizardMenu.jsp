<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<link href="jquery/css/smart_wizard_vertical.css" rel="stylesheet"
	type="text/css" />
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="js/exportpdf.js"></script>
<script type="text/javascript">

function printPDFXLS(id) {			
	$('#vendorID_hdn').val(id);
	$("#exportPDFXLS").css({
		"display" : "block"
	});
	$("#exportPDFXLS").dialog({
		minWidth : 300,
		modal : true
	});
}
//<!--
	$(document).ready(function() {
		<logic:present name="menuStatus">
		<logic:iterate id="menu" name="menuStatus">
			<logic:equal value="1" name="menu" property="tab">
				<logic:equal value="1" name="menu" property="status">
					$("#menu2").removeClass().addClass("done");
				</logic:equal>
			</logic:equal>

			<logic:equal value="2" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu2").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu2").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu2").removeClass().addClass("selected");
				</logic:equal>
				<logic:equal value="3" name="menu" property="status">
					$("#menu2").removeClass().addClass("error");
				</logic:equal>
			</logic:equal>

			<logic:equal value="3" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu3").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu3").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu3").removeClass().addClass("selected");
				</logic:equal>
				<logic:equal value="3" name="menu" property="status">
					$("#menu3").removeClass().addClass("error");
				</logic:equal>
			</logic:equal>

			<logic:equal value="4" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu4").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu4").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu4").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>

			<logic:equal value="5" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu5").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu5").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu5").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>

			<logic:equal value="6" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu6").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu6").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu6").removeClass().addClass("selected");
				</logic:equal>
				<logic:equal value="3" name="menu" property="status">
					$("#menu6").removeClass().addClass("error");
				</logic:equal>
			</logic:equal>

			<logic:equal value="7" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu7").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu7").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu7").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>

			<logic:equal value="8" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu8").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu8").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu8").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>

			<logic:equal value="9" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu9").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu9").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu9").removeClass().addClass("selected");
				</logic:equal>
				<logic:equal value="3" name="menu" property="status">
					$("#menu9").removeClass().addClass("error");
				</logic:equal>
			</logic:equal>

			<logic:equal value="10" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu10").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu10").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu10").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>

			<logic:equal value="11" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu11").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu11").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu11").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>

			<logic:equal value="12" name="menu" property="tab">
				<logic:equal value="0" name="menu" property="status">
					$("#menu12").removeClass().addClass("disabled");
					//$('.disabled').bind('click', false);
				</logic:equal>				
				<logic:equal value="1" name="menu" property="status">
					$("#menu12").removeClass().addClass("done");
				</logic:equal>
				<logic:equal value="2" name="menu" property="status">
					$("#menu12").removeClass().addClass("selected");
				</logic:equal>
			</logic:equal>
		</logic:iterate>
	</logic:present>
	});
//-->
</script>
<style>
<!--
.form-box {
	border: 1px solid #009900;
}

.btn {
	float: right;
}

.clearfix {
	clear: both;
}
-->
</style>

<!-- Dialog Box for Approve Vendor -->
<div id="dialog1" style="display: none;" title="Approve Vendor">
	<table cellpadding="10" cellspacing="10">
		<tr></tr>
		<tr>
			<td align="right">Status:</td>
			<td>
				<input type="hidden" id="vendorid" /> 
				<select name="changeVendorStatus" class="chosen-select"
					id="changeVendorStatus1" onchange="getVendorStatus(this.value);">
				</select>
			</td>
		</tr>
		<logic:present name ="statusMastersList">
			<bean:size id="size" name="statusMastersList" />
			<logic:greaterEqual value="1" name="size">
				<input type="hidden" id="bpSegmentStatus" value="Yes" />
				<tr style="display: none;" id="bpSegmentTRId">
					<td align="right">BP Segment:</td>
					<td>
						<select name="bpsegmentstatus" class="chosen-select" id="bpsegmentId">
							<option value="0">-- Select --</option>
							<bean:size id="size1" name="bpSegmentList" />
							<logic:greaterEqual value="0" name="size1">
								<logic:iterate id="bpsegmentIterateId" name="bpSegmentList">
									<bean:define id="id" name="bpsegmentIterateId" property="id"></bean:define>
									<option value="<%=id.toString()%>">
										<bean:write name="bpsegmentIterateId" property="segmentName"></bean:write>
									</option>
								</logic:iterate>
							</logic:greaterEqual>
						</select>
					</td>
				</tr>
			</logic:greaterEqual>
			<logic:lessEqual value="0" name ="size">
				<input type="hidden" id="bpSegmentStatus" value="No" />
			</logic:lessEqual>
		</logic:present>
		<logic:notPresent  name ="statusMastersList">
			<input type="hidden" id="bpSegmentStatus" value="No" />
		</logic:notPresent>
		<tr id="primeRow" style="visibility: hidden;">
			<td>Vendor Type:</td>
			<td>
				<input type="radio" name="prime" id="primevendor" class="selectedPrimeVendor" value="1" />&nbsp;Prime&nbsp; 
				<input type="radio" class="selectedPrimeVendor" name="prime" value="0" />&nbsp;Non-Prime&nbsp;
			</td>
		</tr>
		<tr>
			<td align="right">Comments:</td>
			<td>
				<textarea cols="50" rows="5" name="isApprovedDesc" id="isApprovedDesc"></textarea>
			</td>
		</tr>
	</table>
	<div id="UserInstructions">
		<p id="UserInstruction" style="color: red; background-color:yellow; text-align: center; display: none;"><b>
			* Refer to Oyster and apply the matching BP Segment and Market Sector hierarchy to the Business Area of the profile.
		</b></p>
	</div>	
	<div class="btn-wrapper text-center">
		<input type="button" class="exportBtn btn btn-primary" onclick="getSelectedIds();"
			value="Submit" id="statusButton">
	</div>
	<div id="ajaxloader" style="display: none;">
		<img src="images/ajax-loader.gif" alt="Ajax Loading Image" />
	</div>
</div>

<!-- Dialog Box for View Modified Date -->

<div id="viewModifiedInfo" title="View Modified Date"
	style="display: none;">
	<div id="viewModifiedDates"></div>
</div>
<div class="mt-3">
<logic:present name="currentUser" property="id">
	<ul class="anchor">
		<li><html:link
			action="/vendornavigation.do?parameter=contactNavigation"
			styleId="menu1" styleClass="done">
			<label class="stepNumber"><img src="images/contact_info.png"></label>
			<span class="stepDesc"><small>Contact Information</small> </span>
		</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=companyInfoNavigation"
				styleId="menu2">
				<label class="stepNumber"><img src="images/company_info.png"></label>
				<span class="stepDesc"><small> Company Information</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=companyAddressNavigation"
				styleId="menu3">
				<label class="stepNumber"><img src="images/address.png"></label>
				<span class="stepDesc"><small>Company Address Info</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=companyOwnerNavigation"
				styleId="menu4">
				<label class="stepNumber"><img src="images/ownership.png"></label>
				<span class="stepDesc"><small>Company Ownership</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=diverseClassificationNavigation"
				styleId="menu5">
				<label class="stepNumber"><img
					src="images/classification.png"></label>
				<span class="stepDesc"><small> Diverse
						Classifications </small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=serviceAreaNavigation"
				styleId="menu6">
				<label class="stepNumber"><img src="images/service_icon.png"></label>
				<span class="stepDesc"><small> Service Area</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=businessAreaNavigation"
				styleId="menu7">
				<label class="stepNumber"><img src="images/business.png"></label>
				<span class="stepDesc"><small> Business Area</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=biographyNavigation"
				styleId="menu8">
				<label class="stepNumber"><img src="images/biography.png"></label>
				<span class="stepDesc"><small>Business Biography and
						Safety </small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=referencesNavigation"
				styleId="menu9">
				<label class="stepNumber"><img src="images/reference.png"></label>
				<span class="stepDesc"><small>Reference</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=otherCertificateNavigation"
				styleId="menu10">
				<label class="stepNumber"><img src="images/quality.png"></label>
				<span class="stepDesc"><small>Quality and Other
						Certifications</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=documentsNavigation"
				styleId="menu11">
				<label class="stepNumber"><img src="images/upload.png"></label>
				<span class="stepDesc"><small>Documents Upload</small> </span>
			</html:link></li>
		<li><html:link
				action="/vendornavigation.do?parameter=contactMeetingNavigation"
				styleId="menu12">
				<label class="stepNumber"><img src="images/submit.png"></label>
				<span class="stepDesc"><small>Vendor Notes and
						Meeting Information</small> </span>
			</html:link></li>
	</ul>
</logic:present>
<logic:present name="vendorUser" property="vendorId">
	<ul class="anchor">
		<li><html:link
			action="/primevendornavigation.do?parameter=contactNavigation"
			styleId="menu1" styleClass="done">
			<label class="stepNumber"><img src="images/contact_info.png"></label>
			<span class="stepDesc"><small>Contact Information</small> </span>
		</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=companyInfoNavigation"
				styleId="menu2" styleClass="done">
				<label class="stepNumber"><img src="images/company_info.png"></label>
				<span class="stepDesc"><small> Company Information</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=companyAddressNavigation"
				styleId="menu3" styleClass="done">
				<label class="stepNumber"><img src="images/address.png"></label>
				<span class="stepDesc"><small>Company Address Info</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=companyOwnerNavigation"
				styleId="menu4" styleClass="done">
				<label class="stepNumber"><img src="images/ownership.png"></label>
				<span class="stepDesc"><small>Company Ownership</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=diverseClassificationNavigation"
				styleId="menu5" styleClass="done">
				<label class="stepNumber"><img
					src="images/classification.png"></label>
				<span class="stepDesc"><small> Diverse
						Classifications </small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=serviceAreaNavigation"
				styleId="menu6" styleClass="done">
				<label class="stepNumber"><img src="images/service_icon.png"></label>
				<span class="stepDesc"><small> Service Area</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=businessAreaNavigation"
				styleId="menu7" styleClass="done">
				<label class="stepNumber"><img src="images/business.png"></label>
				<span class="stepDesc"><small> Business Area</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=biographyNavigation"
				styleId="menu8" styleClass="done">
				<label class="stepNumber"><img src="images/biography.png"></label>
				<span class="stepDesc"><small>Business Biography and
						Safety </small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=referencesNavigation"
				styleId="menu9" styleClass="done">
				<label class="stepNumber"><img src="images/reference.png"></label>
				<span class="stepDesc"><small>Reference</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=otherCertificateNavigation"
				styleId="menu10" styleClass="done">
				<label class="stepNumber"><img src="images/quality.png"></label>
				<span class="stepDesc"><small>Quality and Other
						Certifications</small> </span>
			</html:link></li>
		<li><html:link
				action="/primevendornavigation.do?parameter=documentsNavigation"
				styleId="menu11" styleClass="done">
				<label class="stepNumber"><img src="images/upload.png"></label>
				<span class="stepDesc"><small>Documents Upload</small> </span>
			</html:link></li>
	</ul>
</logic:present>
</div>
<div id="header_title">
	<header class="card-header">
		<h2 class="card-title text-left col-sm-7">
	<img src="images/edit-cust.gif" />Edit Vendor</h2>
	<bean:define id="vendorName" name="vendorName"></bean:define>
	<bean:define id="vendorId" name="supplierId"></bean:define>
	<bean:define id="vendorStatus" name="vendorStatus"></bean:define>
	<span style="color: #009900; font-size:18px;" class="text-right">${vendorName}</span>
	</header>
	<div style="float: right;">
		<logic:present name="privileges">
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Print Profile" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="view">
						<input type="button" onclick="printPDFXLS('${vendorId}')" class="btn btn-primary" value="Print Profile">												
					</logic:equal>
				</logic:equal>
				<logic:equal value="View Modified Date" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="view">
						<input type="button" onclick="viewModifiedDate('${vendorId}')" class="btn btn-primary" value="View Modified Date">												
					</logic:equal>
				</logic:equal>
				<logic:equal value="Update Status" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="view">
						<input type="button" onclick="return updateStatus('${vendorId}','${vendorStatus}')" class="btn btn-default" value="Update Status">						
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			<html:link action="/viewVendorsStatus.do?method=getPreviousSearchResults&valueFrom=wizard" styleClass="btn">Back to Results</html:link>
		</logic:present>		
	</div>
	<div id="exportPDFXLS" style="display: none;" title="Choose Export Type">
				<input type="hidden" id="vendorID_hdn"/>
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;" class="table table-bordered table-striped mb-0">
					<tr>
						<td style="padding: 1%;">
							<input type="radio" name="export" value="1" id="excel">
						</td>
						<td>
							<label for="excel"><img id="excelExport" src="images/excel_export.png" />Excel</label>
						</td>						
						<td style="padding: 1%;">
							<input type="radio" name="export" value="3" id="pdf">
						</td>
						<td>
							<label for="pdf"><img id="pdfExport" src="images/pdf_export.png" />PDF</label>
						</td>
					</tr>
				</table>
				
				<div class="wrapper-btn text-center">
					<logic:present name="userDetails">
						<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
						<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportVendorDetailsHelper();">
					</logic:present>
				</div>
			</div>
</div>
<!-- <div id="progressbar"></div> -->


<style>
.chosen-select {
	width: 90%;
}

.chosen-select-width {
	width: 90%
}

.chosen-select-deselect: {
	width: 90%
}

#contacts {
	margin-top: 2%;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("select.chosen-select").select2();
		$("#serviceArea").select2({
			placeholder : "Click to select",
			width : "90%"
		});
		$("#businessArea").select2({
			placeholder : "Click to select",
			width : "90%"
		});
		$("select.chosen-select-width").select2();
		$("select.chosen-select-deselect").select2();
	});
	
	function updateStatus(vendorId, vendorStatus)
	{		
		$('#vendorid').val(vendorId);
		$("#dialog1").css(
		{
			"display" : "block"
		});
		
		$("#statusButton").prop("disabled", false);
		$("#statusButton").css("background", " none repeat scroll 0 0 #009900");
		$("#UserInstruction").css('display', 'none');
		$("#bpSegmentTRId").css('display', 'none');		

		$.ajax({
			url : 'status.do?method=getStatus&id=' + vendorStatus + '&random='
					+ Math.random(),
			type : "POST",
			async : true,
			beforeSend : function() {
				$("#ajaxloader").show(); //show image loading
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$('#changeVendorStatus1').find('option').remove().end().append(
						data);
				$('#changeVendorStatus1').select2();
				$('#changeVendorStatus1').trigger("chosen:updated");
			}
		});
		$("#dialog1").dialog({
			minWidth : 600,
			height : 350,
			modal : true,
			close : function(event, ui) {
				$('#isApprovedDesc').val('');
				$('#primeRow').css('visibility', 'hidden');
				$('input:radio[name=prime]').prop('checked', false);
			}
		});
	}
	
	function getVendorStatus(status) 
	{
		if (status != '' && status == 'P') {
			$("#primeRow").css('visibility', 'visible');
		} else {
			$("#primeRow").css('visibility', 'hidden');
		}
		
		if (status != '' && status == 'B') {			
			$("#UserInstruction").slideDown(1000);
		} else {
			$("#UserInstruction").css('display', 'none');
		}
		
		$("#bpsegmentId option[value="+0+"]").prop('selected', true);
		$("#bpsegmentId").select2();
		
		if(status != '' && status != 'I' && status != '0')
		{
			$('#bpSegmentStatus').val('No');
			$('#bpSegmentTRId').hide();
			<logic:present name="privileges">
				<logic:iterate id="privilege" name="privileges">
					<logic:equal value="BP Segment" name="privilege" property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="view">
							<logic:present name ="statusMastersList">
								<bean:size id="size" name="statusMastersList" />
								<logic:equal value ="6" name = "size">
									$('#bpSegmentStatus').val('Yes');
									$('#bpSegmentTRId').show();	
								</logic:equal>
								/* <logic:equal value ="1" name = "size">
									$('#bpSegmentStatus').val('Yes');
									$('#bpSegmentTRId').show();	
								</logic:equal> */
								<logic:equal value="5" name = "size">
									if(status != 'N')
									{
										$('#bpSegmentStatus').val('Yes');
										$('#bpSegmentTRId').show();	
									}
								</logic:equal>
								<logic:equal value="4" name = "size">
									if(status != 'N' && status != 'P')
									{
										$('#bpSegmentStatus').val('Yes');
										$('#bpSegmentTRId').show();	
									}
								</logic:equal>
								<logic:equal value="3" name = "size">
									if(status != 'N' && status != 'P' && status != 'A')
									{
										$('#bpSegmentStatus').val('Yes');
										$('#bpSegmentTRId').show();	
									}
								</logic:equal>
							</logic:present>
						</logic:equal>
					</logic:equal>
				</logic:iterate>
			</logic:present>			
		}
		else
		{
			$('#bpSegmentStatus').val('No');
			$('#bpSegmentTRId').hide();
		}
	}	
</script>