<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.VendorDocuments" %>
<%@page import="java.util.*" %>
 
 <%
 double documentSize=0;
 int documentCount=0;
 if (session.getAttribute("vendorDocuments") != null) {
	 List<VendorDocuments> documents=(List<VendorDocuments>)session.getAttribute("vendorDocuments");
	 for(VendorDocuments vendorDocuments:documents){
		 documentSize=documentSize+vendorDocuments.getDocumentFilesize();
		 documentCount=documents.size();
	 }
	}
 %>
<!-- For JQuery Panel -->

<script type="text/javascript" src="js/rfiinformation.js"></script>
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script src="jquery/1.10.3/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="jquery/1.10.3/jquery-ui.css" /> -->
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>
<link href="select2/select2.css" rel="stylesheet"/>
<script src="select2/select2.js"></script>
<style>
	.chosen-select{width: 150px;} 
	.chosen-select-width{width : 90%}
	.chosen-select-deselect:{width : 90%}
</style>
<script type="text/javascript">
$(document).ready(function() { $(".chosen-select").select2();
$(".chosen-select-width").select2(); $("#businessArea").select2(); });
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});
	});
	function deleteRow(rowId){
		input_box = confirm("Are you sure you want to delete this Record?");
		if (input_box == true) {
			$("#" + table + " tr#" + rowId).remove();
			return true;
		} else {
			// Output when Cancel is clicked
			return false;
		}
	}
	function clearForm() {
		window.location = "viewVendor.do?method=view&tire2vendor=yes";
	}
</script>

<div class="page-title">
	<img src="images/icon-registration.png" />&nbsp;Vendor Registration
</div>
<div class="clear"></div>
<div class="form-box">
	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="add">
		<html:form enctype="multipart/form-data" styleId="vendorForm"
			action="/createtire2Vendor?method=createTire2Vendor" >
			<html:javascript formName="vendorMasterForm" />
				<div id="tabs">
					<ul class="resp-tabs-list">
						<li><a href="#tabs-1" style="color: #fff;">Contact Information</a></li>
						<li><a href="#tabs-2"  style="color: #fff;">Company Information</a></li>
						<li><a href="#tabs-3" style="color: #fff;">Services Description</a></li>
						<li><a href="#tabs-4" style="color: #fff;">Diverse Classification</a></li>
						<li><a href="#tabs-5" style="color: #fff;">Quality Certifications</a></li>
						<li><a href="#tabs-6" style="color: #fff;">Documents</a></li>
						<li><a href="#tabs-7" style="color: #fff;">References</a></li>
					</ul>
					<div class="resp-tabs-container">
					<div id="tabs-1">
						<%@include file="/WEB-INF/pages/vendorcontactinfo.jsp"%>
					</div>
					<div id="tabs-2">
						<%@include file="/WEB-INF/pages/vendorregister.jsp"%>
					</div>
					<div id="tabs-3">
						<%@include file="/WEB-INF/pages/naicscodes.jsp"%>
					</div>
					<div id="tabs-4">
						<%@include file="/WEB-INF/pages/diversecertificates.jsp"%>
					</div>
					<div id="tabs-5">
						<%@include file="/WEB-INF/pages/qualityothercertificates.jsp"%>
					</div>
					<div id="tabs-6">
						<%@include file="/WEB-INF/pages/vendorDocuments.jsp"%>
					</div>
					<div id="tabs-7">
						<%@include file="/WEB-INF/pages/vendorReferences.jsp"%>
					</div>
				</div>
			</div>
			<div id="auth" style="padding-top: 1%;">
						<input type="checkbox" id="authentication" tabindex="112" >
						&nbsp;<label for="authentication"><b>Under 15 U.S C. 645(d), any person who misrepresents its size status shall 
						(1) be punished by a fine, imprisonment, or both; (2) be subject to administrative remedies; and 
						(3) be ineligible for participation in programs conducted under the authority of the Small Business Act.<br/>
						By choosing to submit this form, you certify that the information you have provided above is true and accurate</b></label>
			</div>
			<div class="btn-wrapper">
				<html:submit value="Submit" styleClass="btn" styleId="submit1" 
					disabled="true" style="background: none repeat scroll 0 0 #848484;"></html:submit>
				<input type="button" class="btn" value="Clear" onclick="clearForm();">
			</div>
		</html:form>
		<script type="text/javascript">
		$(function($) {
			  $("#phone").mask("(999) 999-9999?");
			  $("#contactPhone").mask("(999) 999-9999?");
			  $("#phone2").mask("(999) 999-9999?");
			  $("#bpContactPhone").mask("(999) 999-9999?");
			  $("#telephone").mask("(999) 999-9999?");
			  $("#referencePhone1").mask("(999) 999-9999?");
			  $("#referencePhone2").mask("(999) 999-9999?");
			  $("#referencePhone3").mask("(999) 999-9999?");
			  $("#ownerPhone1").mask("(999) 999-9999?");
			  $("#ownerPhone2").mask("(999) 999-9999?");
			  $("#ownerPhone3").mask("(999) 999-9999?");
			  
			  $("#fax").mask("(999) 999-9999?");
			  $("#fax2").mask("(999) 999-9999?");
			  $("#contactFax").mask("(999) 999-9999?");
		});
			$(function() {
				$("#tabs").easyResponsiveTabs();
				
				jQuery.validator.addMethod("alpha", function(value, element) { 
			          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
			    },"No Special Characters Allowed.");
				jQuery.validator.addMethod("phoneNumber", function(value, element) { 
			        return this.optional(element) || /^[0-9-()]+$/.test(value);
			  	},"Please enter valid phone number.");
				jQuery.validator.addMethod("password",function(value, element) {
					return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
				},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
				jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
			          return this.optional(element) || /^[0-9]+$/.test(value);
			    },"Please enter only numbers.");
				jQuery.validator.addMethod("allowhyphens", function(value, element) { 
			          return this.optional(element) || /^[0-9-]+$/.test(value);
			    },"Please enter valid numbers.");
				
				$("#vendorForm").validate({
					
					rules: {
						vendorName : {
							required : true
						},
						dunsNumber : {
							allowhyphens: true
						},
						taxId : {
							allowhyphens: true
						},
						numberOfEmployees : {
							onlyNumbers: true
						},
						businessType : {
							required : true
						},
						/* annualTurnover : {
							required : true
						},
						sales2: {
							required : true
						},
						sales3: {
							required : true
						}, */
						yearOfEstablishment : {
							required : true,
							range: [1900, 3000]
						},
						address1 : {
							required : true
						},
						city : {
							required : true,
							alpha : true,
							maxlength : 60
						},
						state : {
							required : true,
							alpha : true,
							maxlength : 60
						},
						province : {
							maxlength : 60
						},
						region : {
							maxlength : 60
						},
						country : {
							required : true,
							maxlength : 120
						},
						zipcode : {
							required : true,
							maxlength : 60,
							allowhyphens : true
						},
						zipcode2 : {
							allowhyphens : true
						},
						referenceZip1 : {
							allowhyphens : true
						},
						referenceZip2 : {
							allowhyphens : true
						},
						referenceZip3 : {
							allowhyphens : true
						},
						mobile : {
							maxlength : 16,
							phoneNumber : true
						},
						mobile2 : {
							maxlength : 16 ,
							onlyNumbers : true 
						},
						referenceMobile1 : {
							maxlength : 16 ,
							onlyNumbers : true 
						},
						referenceMobile2 : {
							maxlength : 16 ,
							onlyNumbers : true 
						},
						referenceMobile3 : {
							maxlength : 16 ,
							onlyNumbers : true 
						},
						policyNumber : {
							onlyNumbers : true 
						},
						phone : {
							required : true
						},
						website : {
							url: true
						},
						emailId : {
							required : true,
							maxlength : 120,
							email: true
						},
						referenceMailId1 : {
							maxlength : 120,
							email : true
						},	
						referenceMailId2 : {
							maxlength : 120,
							email : true
						},	
						referenceMailId3 : {
							maxlength : 120,
							email : true
						},
						naicsCode_0 : {
							required : true
						},
						firstName : {
							required : true,
							alpha : true
						},
						lastName : {
							required : true,
							alpha : true
						},
						designation : {
							required : true,
							rangelength: [1, 60]
						},
						contactPhone : {
							required : true
						},
						contanctEmail : {
							required : true,
							email : true,
							maxlength : 120
						},
						loginpassword : {
							required : true,
							password:true
						},
						confirmPassword : {
							required : true,
							equalTo: "#loginpassword"
						},
						userSecQn : {
							required : true
						},
						userSecQnAns : {
							required : true,
							rangelength: [3, 15]
						},
						extension1 : {
							onlyNumbers : true
						},
						extension2 : {
							onlyNumbers : true
						},
						extension3 : {
							onlyNumbers : true
						},
						bpContactPhoneExt : {
							onlyNumbers : true
						},
						vendorDescription : {
							rangelength : [ 0, 2000 ]
						},
						companyInformation : {
							rangelength : [ 0, 2000 ]
						},
						ownerName1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							},
						},
						ownerTitle1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							},
						},
						ownerEmail1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							},email : true,
						},
						ownerExt1 : {
							phoneNumber : true
						},
						ownerPhone1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							}
						},
						ownerMobile1 : {
							phoneNumber : true
						},
						ownerGender1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							},
						},
						ownerOwnership1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							},onlyNumbers : true
						},
						ownerEthnicity1 : {
							required : function(element) {
								if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
									return true;
								} else {
									return false;
								}
							},
						},
						ownerMobile2 : {
							phoneNumber : true
						},
						ownerEmail2 : {
							email : true,
						},
						ownerOwnership2 : {
							onlyNumbers : true
						},
						ownerMobile3 : {
							phoneNumber : true
						},
						ownerEmail3 : {
							email : true,
						},
						ownerOwnership3 : {
							onlyNumbers : true
						},
						/*serviceArea : {
							required : true
						},
						businessArea : {
							required : true
						},*/
						referenceName1:{
							required : true
						},
						referenceAddress1:{
							required : true
						},
						referencePhone1:{
							required : true
						},
						referenceMailId1:{required : true},
						referenceCity1:{required : true},
						referenceZip1:{required : true, allowhyphens : true},
						referenceState1:{required : true},
						referenceCountry1:{required : true}
					  },
					  ignore:"ui-tabs-hide",
					  submitHandler: function(form) {
						if (validateNaics()) {
							if(validateVendorOtherCertificate()){
								$("#ajaxloader").css('display','block');
								$("#ajaxloader").show();
								form.submit();
							}
						}
					  },
					  invalidHandler : function(form,
								validator) {
							var errors = validator.numberOfInvalids();
							if (errors) {
								var invalidPanels = $(validator.invalidElements())
										.closest(".resp-tab-content",form);
								
								if (invalidPanels
										.size() > 0) {
									$.each($.unique(invalidPanels.get()), function() {
														
										if ( $(window).width() > 650 ) {
												$("a[href='#"+ this.id+ "']").parent()
												.not(".resp-accordion").addClass("error-tab-validation")
												.show("pulsate",{times : 3});
										} else {
											   $("a[href='#"+ this.id+ "']").parent()
												.addClass("error-tab-validation")
													.show("pulsate",{times : 3});
												}
										});
									/* $('input[type="text"]').each(function() {
										if ($(this).attr('alt') == 'Optional'
												&& $(this).attr('value') == '') {
											this.value = 'Optional';
										}
									}); */
								}
							}
						},
						unhighlight : function(element,
								errorClass, validClass) {
							$(element).removeClass(errorClass);
							$(element.form).find("label[for="+ element.id+ "]")
									.removeClass(errorClass);
							var $panel = $(element).closest(".resp-tab-content",element.form);
	
							if ($panel.size() > 0) {
								
								if ($panel.find(".error:visible").size() > 0) {
	
									if ($(window).width() > 650) {
										$("a[href='#"+ $panel[0].id+ "']")
												.parent()
												.removeClass("error-tab-validation");
									} else {
										$("a[href='#"+ $panel[0].id+ "']")
												.parent()
												.removeClass("error-tab-validation");
									}
								}
							}
						}
					});
					
				});
				
			</script>
			</logic:equal>
		
		<logic:equal value="0" name="privilege" property="add">
		<h3>You have no rights to add new vendor </h3>
		</logic:equal>
		</logic:equal>
		</logic:iterate>
		</logic:present>
	</div>
	