
<%@page import="com.fg.vms.customer.model.VendorMaster"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();
	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("tier2VendorList") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("tier2VendorList");
	}
	Tier2ReportDto master = null;
	if (reportDtos != null) {
		for (int index = 0; index < reportDtos.size(); index++) {
			master = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("id", master.getVendorId());
			cellobj.put("vendorName", master.getVendorName());
			cellobj.put("address", master.getAddress1());
			cellobj.put("city", master.getCity());
			cellobj.put("state", master.getState());
			cellobj.put("zipcode", master.getZip());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
