
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<SCRIPT type="text/javascript">
	function viewPriviliges() {
		var roleId = document.getElementById('roleName').value;
		window.location = "vendorroleprivileges.do?method=viewPrivilegesByRole&roleId="
				+ roleId;
	}

	$(function() {

		var roleName = $('#roleName').val();

		/*  add multiple select / deselect functionality */
		if (roleName != 0) {

			$("#selectall").click(function() {

				var isChecked = $('.main-table input:checkbox').is(':checked');

				if (!isChecked) {//Check whether any of the checkboxes is checked or not
					$('.main-table input:checkbox').attr('checked', 'checked');
					$('#selectall').val('UnSelectAll');
				} else {
					$('.main-table input:checkbox').attr('checked', false);
					$('#selectall').val('SelectAll');
				}
			});
		}
	});

	/* Function to check the checkbox status and update button value on page load */
	$(document).ready(function() {

		var isChecked = $('.main-table input:checkbox').is(':checked');

		if (!isChecked) {//Check whether any of the checkboxes is checked or not
			$('#selectall').val('SelectAll');
		} else {
			$('#selectall').val('UnSelectAll');
		}
	});
</SCRIPT>
<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Available Role Privileges
</div>
<div class="form-box card-body">
	<html:form
		action="/vendorroleprivileges.do?method=persistPrivilegesByRole">
		
			<div class="row-wrapper form-group row">
				<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Role Name</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<bean:define id="role1" name="roleName"></bean:define>
					<bean:size id="size" name="vendorRoles" />
					<html:select styleId="roleName" property="roleName"
						onchange="viewPriviliges()" value="<%=role1.toString()%>"
						styleClass="list-box form-control">
						<logic:greaterEqual value="0" name="size">
							<html:option value="0">-- Select --</html:option>
							<logic:iterate id="role" name="vendorRoles">
								<bean:define id="id" name="role" property="id"></bean:define>
								<bean:define id="roleName" name="role" property="roleName"></bean:define>
								<html:option value="<%=id.toString()%>">
									<%=roleName.toString()%>
								</html:option>
							</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
		
			<div class="row-wrapper form-group row">

				<div class="col-sm-9 ctrl-col-wrapper">
					<input type="button" value="SelectAll" class="btn btn-primary" id="selectall" />
				</div>

			</div>
	

		<div class="row-wrapper" style="width: 100%;" align="center">
			<layout:datagrid property="datagrid" model="datagrid"
				styleClass="headerRole">
				<layout:datagridText property="objectName" title="Object Name" />
				<layout:datagridCheckbox property="add" title="Add" />
				<layout:datagridCheckbox property="delete" title="Delete" />
				<layout:datagridCheckbox property="modify" title="Modify" />
				<layout:datagridCheckbox property="view" title="View" />
				<%-- <layout:datagridCheckbox property="visible" title="Visible" /> --%>

			</layout:datagrid>
		</div>
		<div class="wrapper-btn text-center">
			<layout:submit value="Submit" styleClass="btn btn-primary"></layout:submit>
		</div>
	</html:form>
</div>
</div>
</div>
</section>
<script>
	$('#datagridJsId').addClass('main-table');
</script>