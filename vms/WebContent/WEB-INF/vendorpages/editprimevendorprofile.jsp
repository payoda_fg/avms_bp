<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!-- For JQuery Panel -->
<script type="text/javascript" src="jquery/js/jquery.ui.dialog.js"></script>
<script type="text/javascript">
$(document).ready(function() { $(".chosen-select").select2();
$(".chosen-select-width").select2(); $("#serviceArea").select2({placeholder:"Click to select",width: "90%"});
$("#businessArea").select2({placeholder:"Click to select",width: "90%"}); });
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$("#contactInformation").click(function() {
			editValidateCertificate();
		});
		$('#authentication').click(function(){
			if ($(this).is(":checked"))
			{
				$("#submit1").css('background','none repeat scroll 0 0 #009900');
				$("#submit1").prop('disabled',false);
			}else{
				$("#submit1").css('background','none repeat scroll 0 0 #848484');
				$("#submit1").prop('disabled',true);
			}
		});
		$("#manualFlag").css("display","block");
	});
	function cancle() {
		window.location = "tire2home.do?method=tire2HomePage";
	}
	function beforeSubmit(){
		 $('#vendorForm').append("<input type='hidden' name='moreInfo' value='more' id='moreinfo' />");
		 return true;
	}
	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement
				: null);
		if (evt != null
				&& node != null
				&& ((evt.keyCode == 13) && ((node.type == "text")
						|| (node.type == "radio") || (node.type == "textarea") || (node.type == "checkbox")))) {
			if ($('#authentication').is(":checked")) {
				return true;
			} else if (node.id == 'search') {
			} else {
				alert("Please certify that the information you have provided is true and accurate. ");
				return false;
			}
		}
	}
	document.onkeypress = stopRKey;
</script>

<!-- For JQuery Panel -->

<script type="text/javascript" src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>

<script type="text/javascript" src="jquery/js/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
<link href="css/easy-responsive-tabs.css" rel="stylesheet"
	type="text/css"></link>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<link href="select2/select2.css" rel="stylesheet"/>
<script src="select2/select2.js"></script>
<style>
	.chosen-select{width: 150px;} 
	.chosen-select-width{width : 90%}
	.chosen-select-deselect:{width : 90%}
</style>
<html:form enctype="multipart/form-data" styleClass="FORM"
					action="/retrivePrimeVendor?method=create" styleId="vendorForm">
<div class="page-title">
	<img src="images/edit-cust.gif" />&nbsp;Edit Vendor 
		<span style="color: #009900; margin-left: 2%;font-size: xx-large;"><bean:write name="editVendorMasterForm" property="vendorName"/></span>
		<html:submit value="More" styleClass="btn" style="float:right;"	styleId="submit" onclick="return beforeSubmit();"></html:submit>
</div>
<div class="clear"></div>
<div class="form-box">
<div id="successMsg">
	<html:messages id="msg" property="updateVendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="vendor" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
	<html:messages id="msg" property="transactionFailure" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
	<logic:iterate id="privilege" name="rolePrivileges">
		<logic:match value="Edit Profile" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
					<html:javascript formName="editVendorMasterForm" />
					<html:hidden property="id" styleId="vendorId"></html:hidden>
					<div id="tabs">
						<ul class="resp-tabs-list">
							<li><a href="#tabs-1" style="color: #fff;">Company
									Information</a></li>
							<li><a href="#tabs-2" style="color: #fff;">Services Description</a></li>
							<li><a href="#tabs-3" style="color: #fff;">Diverse
									Classifications</a></li>
							<li><a href="#tabs-4" id="contactInformation"
								style="color: #fff;">Quality/Other Certifications</a></li>
						</ul>
						<div class="resp-tabs-container">
							<div id="tabs-1">
								<%@include file="../pages/editvendorregister.jsp"%>
							</div>
							<div id="tabs-2">
								<%@include file="../pages/editnaicscodes.jsp"%>
							</div>
							<div id="tabs-3">
								<%@include file="../pages/editdiversecertificates.jsp"%>
							</div>
							<div id="tabs-4">
								<%@include file="../pages/editqualityothercertificates.jsp"%>
							</div>
						</div>
					</div>
					<div id="auth" style="padding-top: 1%;">
						<input type="checkbox" id="authentication" tabindex="112" >
						&nbsp;<label for="authentication"><b>Under 15 U.S C. 645(d), any person who misrepresents its size status shall 
						(1) be punished by a fine, imprisonment, or both; (2) be subject to administrative remedies; and 
						(3) be ineligible for participation in programs conducted under the authority of the Small Business Act.<br/>
						By choosing to submit this form, you certify that the information you have provided above is true and accurate</b></label>
					</div>
					<div class="btn-wrapper">
						<html:submit value="Update" styleClass="btn" styleId="submit1" 
							disabled="true" style="background: none repeat scroll 0 0 #848484;"></html:submit>
						<html:reset value="Cancel" styleClass="btn" onclick="cancle();"></html:reset>
					</div>
				
				<script type="text/javascript">
					$(function() {
						$("#tabs").easyResponsiveTabs();
					});
					$(function($) {
						  $("#phone").mask("(999) 999-9999?");
						  $("#contactPhone").mask("(999) 999-9999?");
						  $("#phone2").mask("(999) 999-9999?");
						  $("#bpContactPhone").mask("(999) 999-9999?");
						  $("#telephone").mask("(999) 999-9999?");
						  $("#referencePhone1").mask("(999) 999-9999?");
						  $("#referencePhone2").mask("(999) 999-9999?");
						  $("#referencePhone3").mask("(999) 999-9999?");
						  $("#ownerPhone1").mask("(999) 999-9999?");
						  $("#ownerPhone2").mask("(999) 999-9999?");
						  $("#ownerPhone3").mask("(999) 999-9999?");
						  
						  $("#fax").mask("(999) 999-9999?");
						  $("#fax2").mask("(999) 999-9999?");
						  $("#contactFax").mask("(999) 999-9999?");
						  $("#mobile").mask("(999) 999-9999?");
						  $("#mobile2").mask("(999) 999-9999?");
						  $("#ownerMobile1").mask("(999) 999-9999?");
						  $("#ownerMobile2").mask("(999) 999-9999?");
						  $("#ownerMobile3").mask("(999) 999-9999?");
					});
					fn_diverseSup();
				</script>
				<script type="text/javascript">
					$(function() {
						
						jQuery.validator.addMethod("alpha", function(value, element) { 
					          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value);
					    },"No Special Characters Allowed.");
						
						jQuery.validator.addMethod("allowhyphens", function(value, element) { 
					          return this.optional(element) || /^[0-9-]+$/.test(value);
					    },"Please enter valid numbers.");
						
						jQuery.validator.addMethod("phoneNumber", function(value, element) { 
					        return this.optional(element) || /^[0-9-()]+$/.test(value);
					  	},"Please enter valid phone number.");
						
						jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
					          return this.optional(element) || /^[0-9]+$/.test(value);
					    },"Please enter only numbers.");
						
						jQuery.validator.addMethod("password",function(value, element) {
							return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
						},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
						
					    
						$("#vendorForm").validate({
							
							rules: {
								vendorName : {
									required : true
								},
								dunsNumber : {
									allowhyphens : true
								},
								taxId : {
									required : true,
									allowhyphens : true
								},
								companytype : {
									required : true
								},
								businessType:{required : true},
								yearOfEstablishment : {
									required : true,
									range: [1900, 3000]
								},
								address1 : {
									required : true
								},
								city : {
									required : true,
									maxlength : 60
								},
								state : {
									required : function(element) {if($("#country option:selected").text()=='United States') {return true;}else{return false;}},
									maxlength : 60
								},
								province : {
									required : function(element) {if($("#country option:selected").text()!='United States' && $('#country').val()!='' && $('#state').val()=='') {return true;}else{return false;}},
									maxlength : 60
								},
								state2 : {
									required : function(element) {if($("#country2 option:selected").text()=='United States') {return true;}else{return false;}},
									alpha : true,
									maxlength : 60
								},
								province2 : {
									required : function(element) {if($("#country2 option:selected").text()!='United States' && $('#country2').val()!='' && $('#state2').val()=='') {return true;}else{return false;}},
									maxlength : 60
								},
								region : {
									maxlength : 60
								},
								country : {
									required : true,
									maxlength : 120
								},
								zipcode : {
									required : true,
									maxlength : 60,
									allowhyphens : true
								},
								zipcode2 : {
									allowhyphens : true
								},
								referenceZip1 : {
									allowhyphens : true
								},
								referenceZip2 : {
									allowhyphens : true
								},
								referenceZip3 : {
									allowhyphens : true
								},
								policyNumber : {
									onlyNumbers : true 
								},
								phone : {
									required : true
								},
								website : {
									url: true
								},
								emailId : {
									required : true,
									maxlength : 120,
									email: true
								},
								referenceMailId1 : {
									maxlength : 120,
									email : true
								},	
								referenceMailId2 : {
									maxlength : 120,
									email : true
								},	
								referenceMailId3 : {
									maxlength : 120,
									email : true
								},
								naicsCode_0 : {
									required : true
								},
								firstName : {
									required : true
								},
								lastName : {
									required : true
								},
								designation : {
									required : true
								},
								contactPhone : {
									required : true
								},
								contanctEmail : {
									required : true,
									email : true,
									maxlength : 120
								},
								loginpassword : {
									required : true,
									password:true
								},
								confirmPassword : {
									required : true,
									equalTo: "#loginpassword"
								},
								userSecQn : {
									required : true
								},
								userSecQnAns : {
									required : true,
									rangelength: [3, 15]
								},
								extension1 : {
									onlyNumbers : true
								},
								extension2 : {
									onlyNumbers : true
								},
								extension3 : {
									onlyNumbers : true
								},
								bpContactPhoneExt : {
									onlyNumbers : true
								},
								vendorDescription : {
									rangelength : [ 0, 2000 ]
								},
								companyInformation : {
									rangelength : [ 0, 2000 ]
								},
								ownerName1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									},
								},
								ownerTitle1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									},
								},
								ownerEmail1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									},email : true,
								},
								ownerPhone1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									}
								},
								ownerExt1 : {
									phoneNumber : true
								},
								ownerGender1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									},
								},
								ownerOwnership1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									},range : [ 0, 100 ]
								},
								ownerEthnicity1 : {
									required : function(element) {
										if ($("input:radio[name=companyOwnership]:checked").val() == 2) {
											return true;
										} else {
											return false;
										}
									},
								},
								ownerEmail2 : {
									email : true,
								},
								ownerOwnership2 : {
									range : [ 0, 100 ]
								},
								ownerEmail3 : {
									email : true,
								},
								ownerOwnership3 : {
									range : [ 0, 100 ]
								},
								annualYear1 : {
									range : [ 1900, 3000 ]
								},
								annualYear2 : {
									range : [ 1900, 3000 ]
								},
								annualYear3 : {
									range : [ 1900, 3000 ]
								}
							  },
							  ignore:"ui-tabs-hide",
							  submitHandler: function(form) {
								  if (validateNaics() && validateVendorOtherCertificate()) {
									  $("#ajaxloader").css('display','block');
									  $("#ajaxloader").show();
									  form.submit();
								 }else{
									$("#ajaxloader").css('display','none');
									if ($('#moreinfo').length > 0) {
										$('#moreinfo').val("");
									}
								 }
							  },
							  invalidHandler: function(form, validator) {
								$("#ajaxloader").css('display','none');
								$("#moreinfo").val("");
							    var errors = validator.numberOfInvalids();
							    if (errors) {
							      var invalidPanels = $(validator.invalidElements()).closest(".resp-tab-content", form);
							      if (invalidPanels.size() > 0) {
							        $.each($.unique(invalidPanels.get()), function(){
							        	
							        	if ( $(window).width() > 650 ) {
											$("a[href='#"+ this.id+ "']").parent()
											.not(".resp-accordion").addClass("error-tab-validation")
											.show("pulsate",{times : 3});
										} else {
										   $("a[href='#"+ this.id+ "']").parent()
											.addClass("error-tab-validation")
												.show("pulsate",{times : 3});
											}
							        });
							      }
							    }
							  },
							  unhighlight: function(element, errorClass, validClass) {
								  $(element).removeClass(errorClass);
									$(element.form).find("label[for="+ element.id+ "]")
											.removeClass(errorClass);
									var $panel = $(element).closest(".resp-tab-content",element.form);

									if ($panel.size() > 0) {
										
										if ($panel.find(".error:visible").size() > 0) {

											if ($(window).width() > 650) {
												$("a[href='#"+ $panel[0].id+ "']")
														.parent()
														.removeClass("error-tab-validation");
											} else {
												$("a[href='#"+ $panel[0].id+ "']")
														.parent()
														.removeClass("error-tab-validation");
											}
										}
									}
							    }
							});
					});
				</script>
				<%@include file="../vendorpages/edit_primevendorcontact.jsp"%>
			</logic:match>
		</logic:match>
	</logic:iterate>
</div>
</html:form>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="Edit Profile" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You Have No Rights to View Your Profile</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
