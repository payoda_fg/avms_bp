
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<!--Menupart 
<div class="inner-menu">
	<div class="item-box-one">
		<div class="box-width">
			<div class="box-title-green">Vendors</div>
			<div class="btn-yellow">
				<logic:present name="rolePrivileges">
					<logic:iterate id="privilege" name="rolePrivileges">
						<logic:equal value="Create Vendor" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<html:link action="/viewVendor.do?method=view&tire2vendor=yes"
									styleClass="icon-1" styleId="menuselect0">
									<span class="icon-text">Create Vendor</span>
									<span class="icon-img"><img src="bpimages/vendor.png" />
									</span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate>

					<logic:iterate id="privilege" name="rolePrivileges">
						<logic:equal value="View Vendors" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<html:link
									action="/viewSubVendors.do?method=showSubVendorSearch"
									styleClass="icon-3 " styleId="menuselect1">
									<span class="icon-text">Search Vendor</span>
									<span class="icon-img"><img
										src="bpimages/search_vendor.png" /> </span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
				</logic:present>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="inner-menu-list-yellow">
	<logic:present name="rolePrivileges">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Create Vendor" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewVendor.do?method=view&tire2vendor=yes"
						styleId="menuselect0">
						<img src="images/new-icon/icon-1.png" alt="" />
					</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="View Vendors" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link action="/viewSubVendors.do?method=showSubVendorSearch"
						styleId="menuselect1">
						<img src="images/new-icon/icon-3.png" alt="" />
					</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:equal value="Capability Assessment" name="privilege"
				property="objectId.objectName">
				<logic:equal value="1" name="privilege" property="visible">
					<html:link
						action="/capabilityAssessment.do?method=showCapabilityAssessmentPage"
						styleId="menuselect2">
						<img src="images/new-icon/icon-5.png" alt="" />
					</html:link>
				</logic:equal>
			</logic:equal>
		</logic:iterate>

	</logic:present>

</div>-->
<script type="text/javascript">
	var pathname = window.location.href;
	$(".submenuheader").click(function() {
		var $curr = $(this);
		$("a").removeClass("active");
		$("div").removeClass("active");
		$(this).addClass("active");
		$curr.parent().prev().addClass("active");
	});
	if (pathname.indexOf("viewSubVendors") >= 0) {
		$('.icon-3').addClass("active");
	} else if (pathname.indexOf("viewVendor") >= 0) {
		$('.icon-1').addClass("active");
	} else if (pathname.indexOf("capabilityAssessment") >= 0) {
		$('.icon-4').addClass("active");
	}
</script>