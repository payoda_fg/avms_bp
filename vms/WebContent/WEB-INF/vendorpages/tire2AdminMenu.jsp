
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<html class="fixed no-overflowscrolling sidebar-left-collapsed">
<!--Menupart -->
<section class="body">
	<header class="page-header">
		<h2>Administration</h2>
	</header>
			<div class="btn-orange inner-wrapper">
			<aside id="sidebar-left" class="sidebar-left">				
				    <div class="sidebar-header">
				         <div class="sidebar-title">Navigation</div>
				        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
				            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				        </div>
				    </div>
				<!--  vendor  menu -->
				<logic:present name="rolePrivileges">
					<!--<logic:iterate id="privilege" name="rolePrivileges">
						<logic:equal value="Roles" name="privilege"
							property="objectId.objectName">
							<logic:match value="1" name="privilege" property="visible">
								<html:link action="/viewvendorrole.do?parameter=viewVendorRoles"
									styleClass="icon-12" styleId="menuselect0">
									<span class="icon-text">Manage Roles</span>
									<span class="icon-img"><img src="bpimages/roles.png" />
									</span>
								</html:link>
							</logic:match>
						</logic:equal>
					</logic:iterate> -->
			<div class="nano sidepanelMenu">
		        <div class="nano-content">
		            <nav id="menu" class="nav-main" role="navigation">				            
		                <ul class="nav nav-main">
		                    <li class="nav-active">
					<logic:iterate id="privilege" name="rolePrivileges">
						<logic:equal value="User" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<html:link action="/viewvendoruser.do?parameter=viewVendors"
									styleClass="icon-13" styleId="menuselect1">									
									<i class="icon-img"><img src="bpimages/users.png" />
									</i>
									<span class="icon-text">Manage Users</span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
					</li>
				<!-- 	<logic:iterate id="privilege" name="rolePrivileges">
						<logic:equal value="Privileges" name="privilege"
							property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<html:link
									action="/vendorroleprivileges.do?method=rolePrivileges"
									styleClass="icon-17" styleId="menuselect2">
									<span class="icon-text">Role Privileges</span>
									<span class="icon-img"><img
										src="bpimages/roleprivileges.png" /> </span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate> -->
					
					<li>
					<logic:iterate id="privilege" name="rolePrivileges">
						<logic:equal value="Edit Profile" name="privilege" property="objectId.objectName">
							<logic:equal value="1" name="privilege" property="visible">
								<bean:define id="vendorId" name="vendorUser" property="vendorId.id"></bean:define>
								<html:link action="/retrieveprimesupplier.do?method=showSupplierContactDetails&requestString=prime"
									paramId="id" paramName="vendorId" styleClass="icon-14" styleId="menuselect3">
									
									<i class="icon-img">
										<img src="bpimages/edit-customer-info.png" /> 
									</i>
									<span class="icon-text">Edit Profile</span>
								</html:link>
							</logic:equal>
						</logic:equal>
					</logic:iterate>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</logic:present>
</aside>
</div>
</section>

	
		<div class="clear"></div>
<script type="text/javascript">
	var pathname = window.location.href;

	if (pathname.indexOf("viewvendorrole") >= 0
			|| pathname.indexOf("retrivevendorrole") >= 0
			|| pathname.indexOf("editvendorrole") >= 0) {
		$('.icon-12').addClass("active");
	} else if (pathname.indexOf("userrole") >= 0) {
		$('.icon-12').addClass("active");
	} else if (pathname.indexOf("viewvendoruser") >= 0
			|| pathname.indexOf("retrivevendor") >= 0) {
		$('.icon-13').addClass("active");
	} else if (pathname.indexOf("privileges") >= 0) {
		$('.icon-17').addClass("active");
	} else if (pathname.indexOf("retrivePrimeVendor") >= 0) {
		$('.icon-14').addClass("active");
	}
</script>
</html>