<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.dto.RetriveVendorInfoDto"%>
<%
	RetriveVendorInfoDto vendorInfo = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");
	/*  session.setAttribute("vendorContacts",
		    vendorInfo.getVendorContacts()); */
	session.setAttribute("vendorMaster", vendorInfo.getVendorMaster());
%>
<div class="page-title">
	<img src="images/user-group.gif" />&nbsp;&nbsp;Vendor Contacts
	<!--<html:link action="/subVendorContact.do?method=subVendorContact"
		styleClass="btn" style="float:right;">Add Contact</html:link> -->
</div>
<table id="box2" class="main-table" width="100%" border="0">
	<logic:notEmpty name="vendorInfoDto" property="vendorContacts">
		<thead>
			<tr>
				<td class="header">First Name</td>
				<td class="header">Phone</td>
				<td class="header">Mobile</td>
				<td class="header">Fax</td>
				<td class="header">Email</td>
				<td class="header">Actions</td>
			</tr>
		</thead>
		<logic:iterate id="vendorContact" name="vendorInfoDto"
			property="vendorContacts" indexId="index">
			<tbody>
				<tr>
					<td><bean:write name="vendorContact" property="firstName" />
					</td>
					<td><bean:write name="vendorContact" property="phoneNumber" />
					</td>
					<td><bean:write name="vendorContact" property="mobile" /></td>
					<td><bean:write name="vendorContact" property="fax" /></td>
					<td><bean:write name="vendorContact" property="emailId" /></td>
					<bean:define id="vendorId" name="vendorContact" property="id"></bean:define>
					<td align="center"><html:link
							action="/viewnonprimevendorcontact.do?method=viewContact"
							paramId="vendorId" paramName="vendorId">Edit</html:link>
						<%-- |  <html:link
							action="/viewvendorcontact.do?method=deleteContact" paramId="vendorId"
							paramName="vendorId" onclick="return confirm_delete();">Delete</html:link> --%></td>
				</tr>
			</tbody>
		</logic:iterate>
	</logic:notEmpty>
	<logic:empty name="vendorInfoDto" property="vendorContacts">
	No such Records
	</logic:empty>
</table>