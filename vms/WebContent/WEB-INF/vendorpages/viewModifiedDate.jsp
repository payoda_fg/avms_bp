<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<div>
	<table width="100%" border="0" class="main-table">
		<tr>
			<th>Information</th>
			<th>Created On</th>
			<th>Modified On</th>
		</tr>
		<logic:present name="vendorHistories">
			<logic:iterate id="vendorHistory" name="vendorHistories">
				<tr>
					<td>${vendorHistory.info}</td>
					<td align="center"><bean:write name="vendorHistory" property="createdOn" format="yyyy-MM-dd HH:mm:SS"/> </td>
					<td align="center"><bean:write name="vendorHistory" property="modifiedOn" format="yyyy-MM-dd HH:mm:SS"/></td>
				</tr>
			</logic:iterate>
		</logic:present>
	</table>
</div>