<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<!--Menupart -->

<div id="bluemenu" class="bluetabs">
	<ul>
		<li><html:link
				action="nonprimevendorhome.do?method=nonprimeVendorHomePage">Home</html:link></li>
		<li><a href="#" rel="dropmenu1_b">Manage Vendors</a></li>
		<li><a href="#" rel="dropmenu2_b">Dashboards</a></li>
		<li><a href="#" rel="dropmenu3_b">Query</a></li>
		<li><a href="#" rel="dropmenu6_b">Sourcing</a></li>
		<li><a href="#" rel="dropmenu4_b">Utilities</a></li>
		<li><a href="#" rel="dropmenu5_b">Administration</a></li>
	</ul>
</div>



<!--AdminTab -->
<div id="dropmenu5_b" class="dropmenudiv_b" style="width: 150px;">
	<bean:define id="vendorId" name="vendorUser" property="vendorId.id"></bean:define>
	<html:link action="/viewnonprimevendor?method=retriveVendor&nonprime=0"
		paramId="id" paramName="vendorId">Edit Profile</html:link>
</div>



<script type="text/javascript">
	//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
	tabdropdown.init("bluemenu");
</script>