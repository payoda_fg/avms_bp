
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<!--Menupart -->
<div class="inner-menu">
	<div class="item-box-one">
		<div class="box-width">
			<div class="box-title-green">Vendors</div>
			<div class="btn-yellow">
				<!--  vendor  menu -->
				<logic:present name="vendorUser" property="vendorId">
					<logic:present name="vendorUser" property="vendorId.vendorStatus">
						<logic:equal value="P" name="vendorUser"
							property="vendorId.vendorStatus">
							<logic:present name="vendorUser"
								property="vendorId.parentVendorId">
								<logic:equal value="0" name="vendorUser"
									property="vendorId.parentVendorId">
									<html:link
										action="/capabilityAssessment.do?method=showCapabilityAssessmentPage"
										styleClass="icon-3">
										<span class="icon-text">Capability Assessment</span>
										<span class="icon-img"><img
											src="bpimages/capability_assessment1.png" /> </span>
									</html:link>
								</logic:equal>
							</logic:present>
						</logic:equal>
					</logic:present>
				</logic:present>
				<logic:present name="vendorUser" property="vendorId">
					<logic:present name="vendorUser" property="vendorId.vendorStatus">
						<logic:equal value="A" name="vendorUser"
							property="vendorId.vendorStatus">
							<logic:present name="vendorUser"
								property="vendorId.parentVendorId">
								<logic:equal value="0" name="vendorUser"
									property="vendorId.parentVendorId">
									<html:link
										action="/capabilityAssessment.do?method=showCapabilityAssessmentPage"
										styleClass="icon-3">
										<span class="icon-text">Capability Assessment</span>
										<span class="icon-img"><img
											src="bpimages/capability_assessment1.png" /> </span>
									</html:link>
								</logic:equal>
							</logic:present>
						</logic:equal>
					</logic:present>
				</logic:present>
				<bean:define id="vendorId" name="vendorUser" property="vendorId.id"></bean:define>
				<html:link paramId="id" paramName="vendorId"
					action="/retrievenonprimesupplier.do?method=showSupplierContactDetails&requestString=nonprime"
					styleClass="icon-14">
					<span class="icon-text">Edit Profile</span>
					<span class="icon-img"><img src="bpimages/profile.png" /> </span>
				</html:link>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="inner-menu-list-yellow"></div>
<script type="text/javascript">
	var pathname = window.location.href;
	$(".submenuheader").click(function() {
		var $curr = $(this);
		$("a").removeClass("active");
		$("div").removeClass("active");
		$(this).addClass("active");
		$curr.parent().prev().addClass("active");
	});
	if (pathname.indexOf("viewSubVendors") >= 0) {
		$('.icon-3').addClass("active");
	} else if (pathname.indexOf("viewVendor") >= 0) {
		$('.icon-1').addClass("active");
	} else if (pathname.indexOf("capabilityAssessment") >= 0) {
		$('.icon-4').addClass("active");
	}
</script>