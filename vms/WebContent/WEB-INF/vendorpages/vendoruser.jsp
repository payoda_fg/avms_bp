<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />

<script type="text/javascript">
<!--
	function backToVendorUsers() {
		window.location = "viewvendoruser.do?parameter=viewVendors";
	}
	
	$(document).ready(function() {
	
		$("#roleId").select2({width: "90%"});
		$("#secId").select2({width: "90%"});
	});
//-->
</script>
<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<bean:define id="currentUserId" name="userId"></bean:define>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-9 mx-auto">
<div id="successMsg">
	<html:messages id="msg" property="vendor" message="true">
		<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
	</html:messages>
	<html:messages id="msg" property="loginId" message="true">
		<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
	</html:messages>
	<html:messages id="msg" property="emailId" message="true">
		<div class="alert alert-info nomargin"><bean:write name="msg" /></div>
	</html:messages>
</div>
<header class="card-header">
	<h2 class="card-title pull-left">
	<img src="images/userRegistration.png" alt="" />Vendor User Registration</h2>
</header>

<div class="form-box card-body">
	<html:form action="/addvendoruser?parameter=addVendors"
		styleId="vendorForm">
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:match value="User" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="add">					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="firstName" alt="" styleId="firstName"
									styleClass="text-box form-control" tabindex="1" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="lastName" alt="" styleId="lastName"
									styleClass="text-box form-control" tabindex="2" />
							</div>
						</div>					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="designation" alt="" styleId="designation"
									styleClass="text-box form-control" tabindex="3" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone Number</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="phonenumber" alt="" styleId="phonenumber"
									styleClass="text-box form-control" tabindex="4" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="mobile" alt="Optional" styleId="mobile"
									styleClass="text-box form-control" tabindex="5" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">FAX</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="fax" alt="" styleId="fax"
									styleClass="text-box form-control" tabindex="6" />
							</div>
						</div>					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email ID</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="emailId" alt="" styleId="emailId"
									onchange="ajaxFn(this,'VE');" styleClass="text-box form-control"
									tabindex="7" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">User Role</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:select property="roleId" 
									styleId="roleId" tabindex="8" styleClass="form-control">
									<bean:size id="size" name="vendorRoles" />
									<html:option value="">----Select---</html:option>
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="role" name="vendorRoles">
											<bean:define id="id" name="role" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="role" property="roleName"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Password</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:password property="password" alt="" styleId="password"
									styleClass="text-box form-control" tabindex="11" />
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Confirm Password</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:password property="confirmPassword" styleClass="text-box form-control"
									styleId="confirmPassword" name="vendorUserForm" alt=""
									tabindex="12" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question ID</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:select property="secId" alt="" 
									styleId="secId" styleClass="form-control" tabindex="13">
									<bean:size id="size" name="secretIds" />
									<html:option value="">----Select---</html:option>
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="role" name="secretIds">
											<bean:define id="id" name="role" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="role" property="secQuestionParticular"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question Answer</label>
							<div class="col-sm-7 ctrl-col-wrapper">
								<html:text property="secretQuestionAnswer" alt=""
									styleId="secretQuestionAnswer" styleClass="text-box form-control"
									tabindex="14" />
							</div>
						</div>
					
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
							<div class="col-sm-7 ctrl-col-wrapper">
							<span class="radio-custom radio-primary">
								<html:radio property="gender" value="M">Male</html:radio>
								<label for=""></label>
							</span>	
							<span class="radio-custom radio-primary">
								<html:radio property="gender" value="F">Female</html:radio>
								<label for=""></label>
							</span>
							<span class="radio-custom radio-primary">
								<html:radio property="gender" value="T">Transgender</html:radio>
								<label for=""></label>
							</span>
							</div>
						</div>
					
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
						<html:submit value="Submit" styleClass="btn btn-primary"></html:submit>
						<html:reset value="Clear" styleClass="btn btn-default"
							onclick="backToVendorUsers();"></html:reset>
						</div>
					</div>
				</footer>
				</logic:match>
			</logic:match>
		</logic:iterate>
		<logic:iterate id="privilege" name="rolePrivileges">
			<logic:match value="User" name="privilege"
				property="objectId.objectName">
				<logic:match value="0" name="privilege" property="add">
					<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to add new vendor user</h3>
					</div>
				</logic:match>
			</logic:match>
		</logic:iterate>
	</html:form>
</div>
<div class="clear"></div>

<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<header class="card-header mt-4">
				<h2 class="card-title pull-left">
				<img src="images/user-group.gif" alt="" />Registered Users</h2>
			</header>
			<div class="form-box card-body" style="overflow: auto;">
				<table class="main-table table table-bordered table-striped mb-0" id="vendorUserTable" width="100%" >
					<bean:size id="size" name="vendorusers" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="">Role</td>
								<td class="">FirstName</td>
								<td class="">LastName</td>
								<td class="">Designation</td>
								<td class="">Phone Number</td>
								<td class="">Email Id</td>
								<td class="">Actions</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="vendorusers" id="vendorlist">
								<tr>
									<td><bean:write name="vendorlist"
											property="vendorUserRoleId.roleName" /></td>
									<td><bean:write name="vendorlist" property="firstName" /></td>
									<td><bean:write name="vendorlist" property="lastName" /></td>
									<td><bean:write name="vendorlist" property="designation" /></td>
									<td><bean:write name="vendorlist" property="phoneNumber" /></td>
									<td><bean:write name="vendorlist" property="emailId" /></td>
									<bean:define id="vendorId" name="vendorlist" property="id"></bean:define>
									<td><logic:iterate id="privilege2" name="rolePrivileges">
											<logic:match value="User" name="privilege2"
												property="objectId.objectName">
												<logic:match value="1" name="privilege2" property="modify">
													<html:link
														action="/retrivevendor.do?parameter=retriveVendor"
														paramId="id" paramName="vendorId">
																	Edit</html:link>
												</logic:match>
											</logic:match>
										</logic:iterate> | <logic:iterate id="privilege1" name="rolePrivileges">
											<logic:match value="User" name="privilege1"
												property="objectId.objectName">
												<logic:match value="1" name="privilege1" property="delete">
													<logic:notEqual
														value="<%=currentUserId
														.toString()%>"
														name="vendorlist" property="id">
														<html:link
															action="/deletevendor.do?parameter=deleteVendor"
															paramId="id" paramName="vendorId"
															onclick="return confirm_delete();">
																			Delete</html:link>
													</logic:notEqual>
													<logic:equal
														value="<%=currentUserId
														.toString()%>"
														name="vendorlist" property="id">
														<span style="color: red; font-weight: bold;">Current
															User</span>
													</logic:equal>
												</logic:match>
											</logic:match>
										</logic:iterate></td>
								</tr>
							</logic:iterate>
						</tbody>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view vendor users</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
</div>
</div>
</section>
<script>
	$(document).ready(function() {
		$(function($) {
			  $("#phonenumber").mask("(999) 999-9999?");
			  $("#fax").mask("(999) 999-9999?");
			  $("#mobile").mask("(999) 999-9999?");
		});
		
		jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
	          return this.optional(element) || /^[0-9]+$/.test(value)
	    },"Please enter only numbers.");
		
		jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
		},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");

		/* $('input[type="text"]').each(
				function() {
					if ($(this).attr('alt') == 'Optional'
							&& $(this).attr('value') == '') {
						this.value = 'Optional';
					}
				}); */

		$("#vendorForm").validate({

			rules : {
				firstName : {
					required : true
				},
				lastName : {
					required : true
				},
				designation : {
					required : true
				},
				emailId : {
					required : true,
					email : true
				},
				password : {
					required : true,
					password:true
				},
				confirmPassword : {
					required : true,
					equalTo : "#password"
				},
				roleId : {
					required : true
				},
				secId : {
					required : true
				},
				secretQuestionAnswer : {
					required : true
				}
			}
		});
	});
</script>
<script type="text/javascript">
		var config = {
			'.chosen-select' : {
				width : "90%"
			},
		}
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
</script>