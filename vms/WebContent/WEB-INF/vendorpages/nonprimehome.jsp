<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<html:messages id="msg" property="rfip" message="true">

	<script type="text/javascript">
		alert("Successfully saved  ");
	</script>
</html:messages>
<article>
	<!--Content Area Pane Start Here-->
	<section role="main" class="content-body">
	<div class="page-wrapper">
		<div id="content-area">
			<div class="home-widgets" style="padding-top:100px !important;margin-bottom: 100px !important;">
					<div class="row">
					<div class="col-lg-4"></div>
						<div class="col-lg-4 col-xl-4">
						
						
			
					<div class="box-title-green font-weight-semibold text-dark text-uppercase mb-2">Vendor</div>

					
						<logic:present name="vendorUser" property="vendorId">
							<logic:present name="vendorUser" property="vendorId.vendorStatus">
								<logic:equal value="P" name="vendorUser"
									property="vendorId.vendorStatus">
									<logic:present name="vendorUser"
										property="vendorId.parentVendorId">
										<logic:equal value="0" name="vendorUser"
											property="vendorId.parentVendorId">
											<div class="widget-summary">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon btn-tertiary">
											<html:link
												action="/capabilityAssessment.do?method=showCapabilityAssessmentPage"
												styleClass="icon-3">
												<i class="icon-img"><img
													src="bpimages/capability_assessment1.png" /> </i>
												<span class="icon-text">Capability Assessment</span>
											</html:link>
										</div>
										</div>
										</div>										
										</logic:equal>
									</logic:present>
								</logic:equal>
							</logic:present>
						</logic:present>
						<logic:present name="vendorUser" property="vendorId">
							<logic:present name="vendorUser" property="vendorId.vendorStatus">
								<logic:equal value="A" name="vendorUser"
									property="vendorId.vendorStatus">
									<logic:present name="vendorUser"
										property="vendorId.parentVendorId">
										<logic:equal value="0" name="vendorUser"
											property="vendorId.parentVendorId">
											<html:link
												action="/capabilityAssessment.do?method=showCapabilityAssessmentPage"
												styleClass="icon-3">
												<span class="icon-text">Capability Assessment</span>
												<i class="icon-img"><img
													src="bpimages/capability_assessment1.png" /> </i>
											</html:link>
										</logic:equal>

									</logic:present>
								</logic:equal>
							</logic:present>
						</logic:present>
						<div class="card card-featured-left card-featured-tertiary mb-4">
						<div class="card-body" id="yellow" style="min-height: 200px;">
						<div class="widget-summary">
							<div class="widget-summary-col">	
							
						<bean:define id="vendorId" name="vendorUser"
							property="vendorId.id"></bean:define>
							<div class="summary pt-5">
						<html:link paramId="id" paramName="vendorId"
							action="/retrievenonprimesupplier.do?method=showSupplierContactDetails&requestString=nonprime"
							styleClass="icon-14">
							
							<i class="icon-img"><img src="bpimages/profile.png" />
							</i>
							<span class="card-title">Edit Profile</span>
							
						</html:link>
							</div>				
					</div>
				</div>
				<div class="clear"></div>
				<div class="summary-footer"></div>		
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<!--Content Area Pane End Here-->
	</section>
</article>
