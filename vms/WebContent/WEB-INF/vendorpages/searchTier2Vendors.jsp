
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />

<div class="clear"></div>
<div class="page-title" style="float: left;">
	<p style="float: left;">Tier 2 Vendors</p>
	<input type="text" placeholder="Search" alt="Search"
		style="float: left; margin:0 0 1% 1%; width: 82%;"
		class="main-text-box" id="search" onkeyup="filter(this, 'sf',1)"
		onblur="if (this.value == '') this.value = '';">
</div>
<div class="clear"></div>
<div id="form-box">

	<table id="addtree" class="filterable"></table>
	<div id="paddtree"></div>
</div>
<div class="wrapper-btn">
	<input type="button" value="Ok" class="btn"
		onclick="addTier2Vendors();">
	<input type="button" value="Cancel" class="btn"
		onclick="cancel();">
</div>
<script type="text/javascript">
<!--
	function filter(term, _id, cellNr) {
		var suche = term.value.toLowerCase();
		var table = document.getElementById('addtree');
		var ele;
		for ( var r = 0; r < table.rows.length; r++) {
			ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g, "");
			if (ele.toLowerCase().indexOf(suche) >= 0)
				table.rows[r].style.display = '';
			else
				table.rows[r].style.display = 'none';
		}
	}
	var vendorId;
	function addTier2Vendors() {
		if (vendorId != null) {
			$.ajax({
				url : 'tier2Vendors.do?method=assignTier2VendorToVendor&id='+vendorId+'&random='
						+ Math.random(),
				type : "POST",
				async : false,
				success : function(data) {
					var widget = $("#widgetId").val();
					
					$('#tier2vendorNameCopy').find('option').remove().end().append(data);
					
					$("#"+widget).find('option').remove().end().append(data);
					//var value=getMax(widget);
					$('#'+widget+' option[value=' + vendorId + ']').attr('selected', true);
					$("#"+widget).select2();
					$('#dialog3').dialog('close');
				}
			});
		}
	}

	function cancel(){
		var widget = $("#widgetId").val();
		$('#'+widget+' option[value=""]').attr('selected', true);
		$("#"+widget).select2();
		$('#dialog3').dialog('close');
		
	}
//-->
</script>
<script type="text/javascript">
	function tier2VendorSearchGrid() {
		$.ajax({
			url : 'tier2Vendors.do?method=findAllTier2Vendors' + '&random='
					+ Math.random(),
			type : "POST",
			async : false,
			success : function(data) {
				$("#addtree").jqGrid('GridUnload');
				loadtier2VendorGrid(data);
			}
		});
	}

	function loadtier2VendorGrid(myGridData) {
		var newdata = jQuery.parseJSON(myGridData);
		jQuery("#addtree").jqGrid({
			data : newdata,
			datatype : "local",
			colNames : [ "id", "Vendor Name", "Address","City","State","Zip Code" ],
			colModel : [ {
				name : 'id',
				index : 'id',
				/* width : 1, */
				hidden : true,
				key : true,
				editable : true
			}, {
				name : 'vendorName',
				index : 'vendorName',
				/* width : 600, */
				align : "left",
				editable : false,

			}, {
				name : 'address',
				index : 'address',
				/* width : 120, */
				editable : false,

			},{
				name : 'city',
				index : 'city',
				/* width : 120, */
				editable : false,

			},{
				name : 'state',
				index : 'state',
				/* width : 120, */
				editable : false,

			},{
				name : 'zipcode',
				index : 'zipcode',
				/* width : 120, */
				editable : false,

			} ],
			height : 'auto',
			rowNum : 10,
			rowList : [ 10, 20, 50 ],
			pager : "#paddtree",
			autowidth : true,
			/* expanded : true, */
			loaded : true,
			hidegrid : false,
			cmTemplate : {
				title : false
			},
			onSelectRow : function(id) {
				var rowData = jQuery(this).getRowData(id);
				vendorId=rowData.id;
			},

		});
		jQuery("#addtree").jqGrid('navGrid', "#paddtree", {
			add : false,
			edit : false,
			del : false,
			search : false
		});
	}
</script>
