<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!-- For JQuery Panel -->

<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>


<script type="text/javascript">
<!--
	$(document).ready(function() {

		$('#chkDiverse').attr('checked','checked');
		
		if ($("#chkDiverse").is(':checked')) {
			document.getElementById("diverseClassificationTab").style.display = "block";
		} else {
			document.getElementById("diverseClassificationTab").style.display = "none";
		}
		
	});
function closePopup() {
	$('#vendorName').val('');
	$('#address1').val('');
	$('#city').val('');
	$('#state').val('');
	$('#zipcode').val('');
	$('#firstName').val('');
	$('#lastName').val('');
	$('#contactPhone').val('');
	$('#contanctEmail').val('');
	$('input [name=certificates]').val('');
	var widget = $("#widgetId").val();
	$('#'+widget+' option[value=""]').attr('selected', true);
	$("#"+widget).select2();
	$('#dialog2').dialog('close');
}
	
function fn_diverseSup() {
	if (document.getElementById("chkDiverse").checked == true) {
		document.getElementById("diverseClassificationTab").style.display = "block";
	} else {
		document.getElementById("diverseClassificationTab").style.display = "none";
	}
}

function saveVendor(){
	var_form_data = $('#shortVendorForm').serialize();
	var divCertificates = [];
	$('input:checkbox.cert').each(function () {
		if(this.checked){
			divCertificates.push($(this).val());
		}
	});
	
	$.ajax({
		url : "saveShortVendor.do?method=saveShortTier2Vendor&divCertificates="+divCertificates,
		data : var_form_data,
		type : "POST",
		async : false,
		success : function(data) {
			var widget = $("#widgetId").val();
			$('#tier2vendorNameCopy').find('option').remove().end().append(data);
			$('#tier2indirectvendorNameCopy').find('option').remove().end().append(data);
			$("#"+widget).find('option').remove().end().append(data);
			var value=getMax(widget);
			$('#'+widget+' option[value=' + value + ']').attr('selected', true);
			$("#"+widget).select2();
			$('#vendorName').val('');
			$('#address1').val('');
			$('#city').val('');
			$('#state').val('');
			$('#zipcode').val('');
			$('#firstName').val('');
			$('#lastName').val('');
			$('#contactPhone').val('');
			$('#contanctEmail').val('');
			$('input [name=certificates]').val('');
			$('#dialog2').dialog('close');
			//chosenConfig();
		}
	});
}

function getMax(widget) {
    var maxValue = undefined;
    $('option', $('#'+widget)).each(function() {
        var val = $(this).val();
        val = parseInt(val, 10);
        if (!isNaN(val)) {
            if (maxValue === undefined || maxValue < val) {
                maxValue = val;
            }
        }
    });
    return maxValue;
}
//-->
</script>
<div class="page-title">
	<img src="images/icon-registration.png">Vendor Registration
</div>
<form id="shortVendorForm">
	<div class="form-box" style="height: 450px;overflow: auto;">
		<%@include file="/WEB-INF/pages/shortvendorregister.jsp" %>
	</div>
	<div class="btn-wrapper">
		<input type="submit" value="Submit" class="btn" id="submit1"/>
		<input type="button" value="Close" class="btn" id="close" onclick="closePopup();"/>
	</div>
</form>		
<script type="text/javascript">

	$(function() {
		
		$(function($) {
			  $("#contactPhone").mask("(999) 999-9999?");
		});
		
		jQuery.validator.addMethod("alpha", function(value, element) { 
	          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
	    },"No special characters allowed.");
		
		jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
	          return this.optional(element) || /^[0-9]+$/.test(value)
	    },"Please enter only numbers.");

		$("#shortVendorForm").validate({
			
			rules: {
				vendorName : {
					required : true,
					alpha : true
				},
				address1 : {
					required : true
				},
				city : {
					required : true,
					alpha : true,
					maxlength : 60
				},
				country : {
					required : true
				},
				state : {
					required : true,
					alpha : true,
					maxlength : 60
				},
				zipcode : {
					required : true,
					maxlength : 60,
					onlyNumbers : true
				},
				firstName : {
					required : true,
					alpha : true,
					rangelength: [2, 60]
				},
				middleName : {
					required : true,
					rangelength: [1, 5]
				},
				lastName : {
					required : true,
					alpha : true,
					rangelength: [2, 60]
				},
				contactPhone : {
					required : true,
					maxlength : 16
				},
				contanctEmail : {
					required : true,
					email : true,
					maxlength : 120
				}
			  },
								
		  ignore:"ui-tabs-hide",
		  submitHandler: function(form) {
				$("#ajaxloader").css('display','block');
				$("#ajaxloader").show();
				saveVendor();
		  }
		});
		
	});
	
</script>
