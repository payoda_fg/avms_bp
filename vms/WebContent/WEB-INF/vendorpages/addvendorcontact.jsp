<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html class="fixed sidebar-left-collapsed">
<%@page
	import="com.fg.vms.customer.dto.RetriveVendorInfoDto,com.fg.vms.customer.model.VendorMaster"%>
	
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />
<script>
<!--
	$(document).ready(function() {

		$('.panelCenter_1').panel({
			collapsible : false
		});
		
		$("#userSecQn").select2({width: "90%"});
		
		$("#roleId").select2({width: "90%"});
		
		$(function($) {
			$("#contactPhone").mask("(999) 999-9999?");
			$("#contactFax").mask("(999) 999-9999?");
			$("#contactMobile").mask("(999) 999-9999?");
		});
		
		jQuery.validator.addMethod("phoneNumber", function(value, element) { 
	        return this.optional(element) || /^[0-9-]+$/.test(value);
	  	},"Please enter valid phone number.");

		jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
		},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
		
		$("#vendorContactForm").validate({
			
			rules : {
				firstName : {
					required : true,
					maxlength : 255
				},
				lastName : {
					required : true,
					maxlength : 255
				},
				designation : {
					required : true,
					maxlength : 255
				},
				contactPhone : {
					required : true
				},
				roleId : {
					required : true
				},
				contanctEmail : {
					required : true,
					email : true,
					maxlength : 120
				},
				loginpassword : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					password : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				confirmPassword : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					password : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					equalTo : "#loginpassword"
				},
				userSecQn : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				userSecQnAns : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					rangelength : [ 3, 15 ]
				}
			}
		/*,	submitHandler : function(form) {
				$("#ajaxloader").css('display', 'block');
				$("#ajaxloader").show();
				form.submit();
			}*/
		});
	});
	function allowed_to_login() {
		if (document.getElementById("loginAllowed").checked == true) {
			document.getElementById("loginDisplay").style.visibility = "visible";
			$('#loginAllowed').val("on");
		} else {
			document.getElementById("loginDisplay").style.visibility = "hidden";
			document.getElementById("loginAllowed").value = "";
		}
	}
	
	function backToRetrieve(){
		var vendorId = $("#vendorId").val();
		window.location = "retrieveprimesupplier.do?method=showSupplierContactDetails&id="+vendorId;
	}
//-->
</script>
<%
	RetriveVendorInfoDto vendorInfo = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");
	VendorMaster vendorMaster = vendorInfo.getVendorMaster();
%>
<!-- For JQuery Panel -->
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>
<link href="jquery/css/jquery-ui.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<div class="clear"></div>
<div class="page-title">
	<img src="images/userRegistration.png" alt="" />New Contact
</div>
<div class="clear"></div>
<html:form action="/saveSubVendorContact?method=saveContact"
	styleId="vendorContactForm">
	<%
		session.setAttribute("vendorMaster", vendorMaster);
	%>
	<html:javascript formName="vendorContactForm" />
	<input type="hidden" value="${vendorMaster.id}" id="vendorId">
	<div class="panelCenter_1">
		<h3>Add Contact Information</h3>		
		<div class="form-box">
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">First Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="firstName" alt=""
							styleId="firstName" />
					</div>
					<span class="error"><html:errors property="firstName"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Last Name</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="lastName" alt=""
							styleId="lastName" />
					</div>
					<span class="error"><html:errors property="lastName"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Title</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="designation"
							alt="" styleId="designation" />
					</div>
					<span class="error"><html:errors property="designation"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Phone</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contactPhone"
							alt="" styleId="contactPhone" />
					</div>
					<span class="error"><html:errors property="contactPhone"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Mobile</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contactMobile"
							alt="Optional" styleId="contactMobile" />
					</div>
					<span class="error"><html:errors property="contactMobile"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">FAX</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contactFax"
							alt="" styleId="contactFax" />
					</div>
					<span class="error"><html:errors property="contactFax"></html:errors></span>
				</div>
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">User Role</div>
					<div class="ctrl-col-wrapper">
						<html:select property="roleId" styleId="roleId">
							<html:option value="">--Select--</html:option>
							<logic:present name="vendorRoles">
								<html:optionsCollection name="vendorRoles" value="id" label="roleName" />
							</logic:present>							
						</html:select>
					</div>					
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Email ID</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="contanctEmail"
							alt="" styleId="contanctEmail" onchange="ajaxFn(this,'VE');" />
					</div>
					<span class="error"><html:errors property="contanctEmail"></html:errors></span>
				</div>				
			</div>
            <div class="wrapper-half">
            	<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Password</div>
					<div class="ctrl-col-wrapper">
						<html:password property="loginpassword" styleClass="main-text-box"
							styleId="loginpassword" alt=""/>
					</div>
					<span class="error"><html:errors property="loginpassword"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Confirm Password</div>
					<div class="ctrl-col-wrapper">
						<html:password property="confirmPassword" 
							styleClass="main-text-box" styleId="confirmPassword" alt="" />
					</div>
					<span class="error"><html:errors property="confirmPassword"></html:errors></span>
				</div>				
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Secret Question</div>
					<div class="ctrl-col-wrapper">
						<html:select property="userSecQn" styleId="userSecQn">
							<html:option value="">--Select--</html:option>
							<bean:size id="size" name="secretQnsList" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="secretQn" name="secretQnsList">
									<bean:define id="id" name="secretQn" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</html:select>
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Secret Question Answer</div>
					<div class="ctrl-col-wrapper">
						<html:text styleClass="main-text-box" property="userSecQnAns"
							alt="" styleId="userSecQnAns" />
					</div>
					<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
				</div>				
			</div>
			<div class="wrapper-half">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Gender</div>
					<div class="ctrl-col-wrapper">
						<html:radio property="gender" value="M">Male</html:radio>
						<html:radio property="gender" value="F">Female</html:radio>
						<html:radio property="gender" value="T">Transgender</html:radio>
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper">Allowed to Login</div>
					<div class="ctrl-col-wrapper">
						<html:checkbox property="loginAllowed" styleId="loginAllowed"
							onclick="allowed_to_login()" />
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	
	<div class="clear"></div>
	<div class="btn-wrapper">
		<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
		<html:reset value="Clear" styleClass="btn" ></html:reset>
		<input type="button" value="Cancel" class="btn" onclick="backToRetrieve();">
	</div>
</html:form>
</html>