<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<div id="form-container">
	<div id="form-container-in">

		<table width="56%" height="100" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td><font class="fontbb">You have successfully saved few
						details with AVMS system. </font></td>
				<td><font class="fontbb">Use your login credentials for
						future login.</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<html:link
			href="viewanonymousvendors.do?parameter=manageAnonymousVendors">Back to login page </html:link>

	</div>
</div>