<%@page import="com.fg.vms.customer.dao.impl.CURDTemplateImpl"%>
<%@page import="com.fg.vms.customer.dao.CURDDao"%>
<%@page import="com.fg.vms.customer.dto.Tier2ReportDto"%>
<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%@page import="com.fg.vms.customer.dao.impl.VendorDaoImpl"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="com.fg.vms.customer.dao.VendorDao"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.customer.model.VendorMaster"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%
	UserDetailsDto appDetails = (UserDetailsDto) session
			.getAttribute("userDetails");
	VendorContact contact = (VendorContact) session
			.getAttribute("vendorUser");
	Integer parent = contact.getVendorId().getId();
	String query = "Select new com.fg.vms.customer.dto.Tier2ReportDto(vm.id,vm.vendorName,vm.address1,vm.city,vm.state,vm.zipCode) from  VendorMaster vm "
			+ " where  vm.primeNonPrimeVendor=0 and  vm.isActive=1 and vm.isApproved=1 and vm.isinvited=1 and vm.parentVendorId= "
			+ contact.getVendorId().getId();
	CURDDao<?> curdDao = new CURDTemplateImpl<Tier2ReportDto>();
	List<Tier2ReportDto> tier2Vendors = (List<Tier2ReportDto>) curdDao
			.findAllByQuery(appDetails, query);

	StringBuffer buffer = new StringBuffer(
			"<option value=''>- select -</option>");
	buffer.append("<option value='Add New Tier2 Vendor'>&lt;&lt; Add New Tier2 Vendor &gt;&gt;</option>");
	//buffer.append("<option value='Existing Tier2 Vendor'>&lt;&lt; Add Existing Tier2 Vendor &gt;&gt;</option>");

	for (Tier2ReportDto vendor : tier2Vendors) {

		buffer.append("<option value='" + vendor.getVendorId() + "'>"
				+ vendor.getVendorName() + "</option>");

	}
	response.setHeader("Cache-Control", "no-cache");
	response.getWriter().println(buffer);
%>