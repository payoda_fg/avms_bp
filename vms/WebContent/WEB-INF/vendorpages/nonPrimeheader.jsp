<%@page import="com.fg.vms.admin.dao.impl.LoginDaoImpl"%>
<%@page import="com.fg.vms.admin.dao.LoginDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html class="fixed sidebar-left-collapsed">
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
	rel="stylesheet" type="text/css" />

<!-- Vendor CSS -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" />
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css" />
<link href="css/tabber.css" type="text/css" rel="stylesheet"></link>

<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/validation.js"></script>	
<script src="js/nanoscroller.js"></script>
<!--<link href="jquery/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />-->
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/tabber.js"></script>
<link href="css/tabber.css" type="text/css" rel="stylesheet" />
<script src="jquery/js/jquery-1.7.2.min.js"></script>
<script src="jquery/js/jquery-ui-1.8.11.custom.min.js"></script>
<script src="jquery/ui/jquery.ui.autocomplete.js"></script>
<script src="jquery/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript">
	ddaccordion.init({
		headerclass : "submenuheader", //Shared CSS class name of headers group
		contentclass : "submenu", //Shared CSS class name of contents group
		revealtype : "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
		mouseoverdelay : 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
		collapseprev : true, //Collapse previous content (so only one open at any time)? true/false 
		defaultexpanded : [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
		onemustopen : false, //Specify whether at least one header should be open always (so never all headers closed)
		animatedefault : false, //Should contents open by default be animated into view?
		persiststate : true, //persist state of opened contents within browser session?
		toggleclass : [ "", "active" ], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
		//Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
		animatespeed : "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
		oninit : function(headers, expandedindices) { //custom code to run when headers have initalized

		},
		onopenclose : function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
			//do nothing

		}
	});
</script>
<script>
$(document).ready(function (){
	$(".sidepanelMenu").nanoScroller();
});	
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var current = location.pathname;
	    $('.nav li a').each(function(){
	        var $this = $(this);
	        // if the current path is like this link, make it active
	        if($this.attr('href').indexOf(current) !== -1){
	            $this.parent().addClass('active');
	        }
	    })
	});			
		
</script>
<header>
	<div class="page-wrapper">
		<header class="header header-nav-menu header-nav-stripe">
			<div class="logo-container float-left">
				<logic:present name="userDetails">
					<bean:define id="logoPath" name="userDetails"
						property="settings.logoPath"></bean:define>
				<a href="#"><img
					src="<%=request.getContextPath()%>/images/logo.png"
					height="80px" /> </a>
			</logic:present>
				<div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</div>
			<div class="header-right">
				<div class="notifications">
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#headerNavbar" aria-controls="headerNavbar" aria-expanded="false" aria-label="Toggle navigation">
					    <span class="fa fa-bars"></span>
					</button>
				</div>
				<div id="userbox4" class="userbox">
					<a href="#" data-toggle="dropdown"> 
						<div class="profile-info" data-lock-name="Help">
							<span class="name"> 									
									Help								
							</span>
						</div>
						<i class="fa custom-caret"></i>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdownMenu">
						<ul class="list-unstyled mb-2">
							<li class="divider"></li>
							<li><a href="http://support.avmsystem.com" target="_blank">Support</a></li>
							<li>
								<logic:present name="userDetails" property="settings.nonPrimeTrainingUrl">
								<bean:define id="nonPrimeTrainingUrl" name="userDetails" property="settings.nonPrimeTrainingUrl"/>
								<a href="${nonPrimeTrainingUrl}" target="_blank">Training</a>
								</logic:present>						
							</li>
							<li>
							<logic:present name="userDetails" property="customer.custCode">
								<bean:define id="custCode" name="userDetails" property="customer.custCode"/>
								<a href="<%=request.getContextPath()%>/support/?file=${custCode}/help/Diverse Supplier Registration_v2.0.pdf" target="_blank">User Manual Documentation</a>
							</logic:present>
							</li>
						</ul>
					</div>
				</div>
				
				<div id="userbox3" class="userbox" style="width:110px;">
					<a href="#" data-toggle="dropdown"> 
						<div class="profile-info" data-lock-name="">
							<span class="name"> 									
								
								<logic:present
									name="vendorUser">
									<bean:write name="vendorUser" property="firstName" />
									
								</logic:present>
								
							</span>
						</div>
						<i class="fa custom-caret"></i>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdownMenu">
						<ul class="list-unstyled mb-2">
							<li class="divider"></li>
							<li><logic:present name="vendorUser">
									<bean:define id="currentUserId" name="userId"></bean:define>
									<a role="menuitem" tabindex="-1" href="#"> <html:link
											action="/retrivevendor.do?parameter=retriveVendor"
											paramId="id" paramName="currentUserId">
											<i class="fa fa-user"></i> Signed in as <span><bean:write name="vendorUser" property="firstName" /></span></html:link>
											
									</a>									
								</logic:present></li>
							<li><html:link action="/logout.do">Sign out</html:link></li>
						</ul>
					</div>
				</div>
				
				<div class="header-nav collapse navbar-collapse" id="headerNavbar">
			<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 header-nav-main-square">
				<nav>
					<ul class="nav nav-pills" id="mainNav">
						<li>
							<html:link action="tire2home.do?method=tire2HomePage">Home</html:link>
						</li>
						<li> 
							<a href="http://support.avmsystem.com/support/solutions/folders/1000030759" target="_blank">FAQ</a>
						</li>
						<li>
							 <a href="http://support.avmsystem.com" target="_blank">Contact Us</a> 
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
</div>
</header>

						
		
<script type="text/javascript">
	jQuery(document).ready(function() {
		$('body').on('click', '.toggletrigger', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer').slideToggle();
		});
		$('body').on('click', '.toggletrigger1', function(e) {
			e.stopPropagation();
			$(this).next('.togglecontainer1').slideToggle();
		});
		$('body').bind('click', function(e) {
			// event.preventDefault();   
			$('.togglecontainer').slideUp();
			$('.togglecontainer1').slideUp();
		});
	});
</script>

</html>