<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<html class="fixed sidebar-left-collapsed">
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<html:messages id="msg" property="rfip" message="true">

	<script type="text/javascript">
		alert("Successfully saved  ");
	</script>
</html:messages>
<article>
	<!--Content Area Pane Start Here-->
<section role="main" class="content-body">
	<div class="page-wrapper">			
		<div id="content-area">
			<div class="home-widgets" style="padding-top:110px !important;margin-bottom: 110px !important;">
					<div class="row">
					<div class="col-lg-2"></div>
						<div class="col-lg-4 col-xl-4">
				<!--<div class="box-width">
					<div class="box-title-green">Vendor</div>
					<div class="btn-yellow">
						<logic:present name="rolePrivileges">
							<logic:iterate id="privilege" name="rolePrivileges">
								<logic:equal value="Create Vendor" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">

										<html:link action="/viewVendor.do?method=view&tire2vendor=yes"
											styleClass="icon-1">
											<span class="icon-text">Create Vendor</span>
											<span class="icon-img"><img src="bpimages/vendor.png" />
											</span>
										</html:link>
									</logic:equal>
								</logic:equal>
							</logic:iterate>

							<logic:iterate id="privilege" name="rolePrivileges">
								<logic:equal value="View Vendors" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">

										<html:link
											action="/viewSubVendors.do?method=showSubVendorSearch"
											styleClass="icon-3">
											<span class="icon-text">Search Vendor</span>
											<span class="icon-img"><img
												src="bpimages/search_vendor.png" /> </span>
										</html:link>
									</logic:equal>
								</logic:equal>
							</logic:iterate>

						</logic:present>
					</div>
				</div> -->
				<div class="card card-featured-left card-featured-tertiary mb-4">
					<div class="card-body" id="yellow" style="min-height: 200px;">
						<div class="widget-summary">
							<div class="widget-summary-col">	
			<logic:present name="vendorUser" property="vendorId">
				<logic:present name="vendorUser" property="vendorId.vendorStatus">
					<logic:equal value="B" name="vendorUser"
						property="vendorId.vendorStatus">				
						<div class="font-weight-semibold text-dark text-uppercase mb-2">Reports</div>
								
										<div class="widget-summary">
											<div class="widget-summary-col widget-summary-col-icon">
												<div class="summary-icon btn-tertiary">
													<img src="bpimages/createreport1.png" />
												</div>
											</div>
											<div class="widget-summary-col">
												<div class="summary">
													<a href="#menu6" class="icon-6 submenuheader"
														onclick="linkPage('tier2Report.do?method=showSpendReportHistory')">
														<span class="card-title">Create Tier 2 Spend Report</span> 
													</a>
												</div>
											</div>								
										</div>																	
										<div class="clear"></div>		
										<div class="summary-footer"></div>				
								</logic:equal>
							</logic:present>
						</logic:present>
					</div>
				</div>
			</div>		
		</div>
	</div>
		<div class="col-lg-4 col-xl-4">
			<div class="card card-featured-left card-featured-primary mb-4">
					<div class="card-body" id="orange">
						<div class="widget-summary">
							<div class="widget-summary-col">	
					<div class="font-weight-semibold text-dark text-uppercase mb-2">Administration</div>
					<div class="btn-orange">
						<!--  vendor  menu -->
						<logic:present name="rolePrivileges">							
							<logic:iterate id="privilege" name="rolePrivileges">
								<logic:equal value="User" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary">
												<img src="bpimages/users.png" />
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
										<html:link action="/viewvendoruser.do?parameter=viewVendors"
											styleClass="icon-13">
											<span class="card-title">Manage Users</span>
											
										</html:link>
										</div>
									</div>
								</div>
								
									</logic:equal>
								</logic:equal>
							</logic:iterate>

							<!--<logic:iterate id="privilege" name="rolePrivileges">
								<logic:equal value="Privileges" name="privilege"
									property="objectId.objectName">
									<logic:equal value="1" name="privilege" property="visible">
										<html:link
											action="/vendorroleprivileges.do?method=rolePrivileges"
											styleClass="icon-17">
											<span class="icon-text">Role Privileges</span>
											<span class="icon-img"><img
												src="bpimages/roleprivileges.png" /> </span>
										</html:link>
									</logic:equal>
								</logic:equal>
							</logic:iterate> -->
							<div class="summary-footer"></div>	
							<bean:define id="vendorId" name="vendorUser"
								property="vendorId.id"></bean:define>
								
									<div class="widget-summary">
										<div class="widget-summary-col widget-summary-col-icon">
											<div class="summary-icon bg-primary">
												<img src="bpimages/edit-customer-info.png" />
											</div>
										</div>
										<div class="widget-summary-col">
											<div class="summary">
										<html:link action="/retrieveprimesupplier.do?method=showSupplierContactDetails&requestString=prime"
								paramId="id" paramName="vendorId" styleClass="icon-14">
											<span class="card-title">Edit Profile</span>
											
										</html:link>
										</div>
									</div>
								</div>									
						</logic:present>
						<div class="summary-footer"></div>	
					</div>
				</div>
			</div>
		</div>
		</div>
		
				<div class="clear"></div>
			</div>
			<!--<div class="item-box-border">&nbsp;</div>
			<div class="item-box-four">
				<div class="box-width">
					<div class="box-title">Training</div>
					<div class="btn-bg">
						<a href="#" class="icon-18">System Overview</a> <a href="#"
							class="icon-19">Administration</a> <a href="#" class="icon-20">Vendor<br>Management
						</a> <a href="#" class="icon-21">Reports<br>Management </a>
					</div>
				</div>
				<div class="clear"></div>
			</div>-->
			<div class="clear"></div>
		</div>
		</div>
	</div>	
</div>
</section>
	
	<!--Content Area Pane End Here-->
</article>

<script>
	$(".submenuheader").click(function() {
		var $curr = $(this);
		$("a").removeClass("active");
		$("div").removeClass("active");
		$(this).addClass("active");
		$curr.parent().prev().addClass("active");
	});

	$(".sub").click(function() {
		$(".sub").removeClass("active");
		$(this).addClass("active");
	});
</script>
</html>