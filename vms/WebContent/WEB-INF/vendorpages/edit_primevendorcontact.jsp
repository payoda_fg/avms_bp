<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.fg.vms.customer.dto.RetriveVendorInfoDto"%>

<%
	RetriveVendorInfoDto vendorInfo = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");
	/* session.setAttribute("vendorContacts",
			vendorInfo.getVendorContacts()); */
	session.setAttribute("vendorMaster", vendorInfo.getVendorMaster());
%>
<c:set var="action" value="updatediversecertificate" />
<header class="card-header">
	<h2 class="card-title pull-left">
	<img src="images/user-group.gif" alt="" /> Vendor Contacts</h2>
	<logic:notPresent name="primeType">
		<logic:notEqual value="0" name="primeType">
			<html:link action="/subVendorContact.do?method=subVendorContact"
				styleClass="btn" style="float:right">Add Contact</html:link>
		</logic:notEqual>
	</logic:notPresent>
</header>
<table class="main-table table table-bordered table-striped mb-0" width="100%" border="0">
	<logic:notEmpty name="vendorInfoDto" property="vendorContacts">
		<thead>
			<tr>
				<td class="">First Name</td>
				<td class="">Last Name</td>
				<td class="">Gender</td>
				<td class="">Title</td>
				<td class="">Phone</td>
				<td class="">Fax</td>
				<td class="">Email</td>
				<td class="">Actions</td>
			</tr>
		</thead>
		<tbody>
			<logic:iterate id="vendorContact" name="vendorInfoDto"
				property="vendorContacts" indexId="index">
				<logic:equal value="1" name="vendorContact" property="isActive">
					<tr>
						<td><bean:write name="vendorContact" property="firstName" /></td>
						<td><bean:write name="vendorContact" property="lastName" /></td>
						<td><logic:equal value="M" name="vendorContact" property="gender">Male</logic:equal>
             				<logic:equal value="F" name="vendorContact" property="gender">Female</logic:equal>
             				<logic:equal value="T" name="vendorContact" property="gender">Transgender</logic:equal>
             			</td>
					    <td><bean:write name="vendorContact" property="designation" /></td>
						<td><bean:write name="vendorContact" property="phoneNumber" /></td>
					    <td><bean:write name="vendorContact" property="fax" /></td>
						<td><bean:write name="vendorContact" property="emailId" /></td>
						<bean:define id="vendorId" name="vendorContact" property="id"></bean:define>
						<td><html:link
								action="/viewprimevendorcontact.do?method=viewContact"
								paramId="vendorId" paramName="vendorId">View </html:link> <logic:notPresent
								name="primeType">
								<logic:notEqual value="0" name="primeType">
									<logic:equal value="0" name="vendorContact" property="isPreparer">
											<html:link
												action="/viewprimevendorcontact.do?method=deleteContact"
												paramId="vendorId" paramName="vendorId"
												onclick="return confirm_delete();">| <img src='images/deleteicon.jpg' alt='Delete' /></html:link>
									</logic:equal>	
									<logic:equal value="1" name="vendorContact" property="isPreparer">
										<span style="color: #009900;font-weight: bold;">Primary</span>
									</logic:equal>
								</logic:notEqual>
							</logic:notPresent>
						</td>
					</tr>
				</logic:equal>
			</logic:iterate>
		</tbody>
	</logic:notEmpty>
	<logic:empty name="vendorInfoDto" property="vendorContacts">
	No such Records
	</logic:empty>
</table>