<%@page import="com.fg.vms.customer.model.VendorRoles"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
<!--
	$(document).ready(function() {

		jQuery.validator.addMethod("alpha", function(value, element) { 
	          return this.optional(element) || /^[A-Za-z0-9 ]+$/.test(value)
	    },"No Special Characters Allowed.");
		
		$("#vendorRolesForm").validate({
			rules : {
				roleName : {
					required : true,
					alpha : true,
					minlength : 4
				}
			}
		});

	});

	function backToVendorRole() {
		window.location = "viewvendorrole.do?parameter=viewVendorRoles";
	}
//-->
</script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<!-- <div id="sample1" align="center" style="padding: 2% 0 0 30%;"></div> -->
<div id="successMsg">
	<html:messages id="msg" property="userrole" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Update Vendor Role
</div>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
			<div class="form-box">
				<html:form action="/editvendorrole.do?parameter=update"
					styleId="vendorRolesForm">
					<html:javascript formName="userRoleForm" />
					<html:hidden property="id" name="userRoleForm" />

					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Role Name</div>
							<div class="ctrl-col-wrapper">
								<html:hidden property="hiddenRoleName" name="userRoleForm"
									styleId="hiddenRoleName" />
								<html:text property="roleName" name="userRoleForm"
									styleId="roleName" alt="" styleClass="text-box"
									onchange="ajaxEdFn(this,'hiddenRoleName','VR');" />
							</div>
							<span class="error"><html:errors property="roleName" /></span>
						</div>
					</div>
					<div class="wrapper-btn">
						<html:submit value="Update" styleClass="btn" styleId="submit"></html:submit>
						<html:reset value="Cancel" styleClass="btn"
							onclick="backToVendorRole();"></html:reset>
					</div>
				</html:form>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to modify vendor roles</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Vendor Roles
</div>
<logic:iterate id="privilege" name="rolePrivileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">

			<div class="form-box">
				<table width="100%" border="0" class="main-table">
					<bean:size id="size" name="vendorroles" />
					<logic:greaterEqual value="0" name="size">
						<thead>
							<tr>
								<td class="header">RoleName</td>
								<td class="header">Actions</td>
							</tr>
						</thead>
						<logic:iterate id="vendorlist" name="vendorroles">
							<tbody>
								<tr class="even">
									<td><bean:write name="vendorlist" property="roleName"></bean:write></td>
									<bean:define id="id" name="vendorlist" property="id"></bean:define>
									<td align="center"><logic:iterate id="privilege2"
											name="rolePrivileges">
											<logic:match value="Roles" name="privilege2"
												property="objectId.objectName">
												<logic:match value="1" name="privilege2" property="modify">
													<html:link action="/retrivevendorrole.do?parameter=retrive"
														paramId="id" paramName="id">Edit </html:link>
												</logic:match>
											</logic:match>
										</logic:iterate> <logic:iterate id="privilege1" name="rolePrivileges">
											<logic:match value="Roles" name="privilege1"
												property="objectId.objectName">
												<logic:match value="1" name="privilege1" property="delete">
													<logic:notEqual value="<%=id.toString()%>" property="id"
														name="userRoleForm">
																	| <html:link
															action="/deletevendorrole?parameter=delete" paramId="id"
															paramName="id" onclick="return confirm_delete();"> Delete</html:link>
													</logic:notEqual>
													<logic:equal value="<%=id.toString()%>" property="id"
														name="userRoleForm">
																	| <span style="color: green; font-weight: bold;">Role
															in Edit</span>
													</logic:equal>

												</logic:match>
											</logic:match>
										</logic:iterate></td>
								</tr>
							</tbody>
						</logic:iterate>
					</logic:greaterEqual>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

