<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page
	import="com.fg.vms.customer.dto.RetriveVendorInfoDto,com.fg.vms.customer.model.VendorMaster"%>
<%
	RetriveVendorInfoDto vendorInfo1 = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");

	session.setAttribute("vendorMaster1", vendorInfo1.getVendorMaster());
%>

<!-- For JQuery Panel -->
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet"></link>

<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>

<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />
<script>
	$(document).ready(function() {

		$('.panelCenter_1').panel({
			collapsible : false
		});
		
		$("#userSecQn").select2({width: "90%"});
	});
	function allowed_to_login() {
		if (document.getElementById("loginAllowed").checked == true) {
			document.getElementById("loginDisplay").style.visibility = "visible";
		} else {
			document.getElementById("loginDisplay").style.visibility = "hidden";
		}
	}
	function backToRetrieve(){
// 		var vendorId = $("#vendorId").val();
		window.location = "viewnonprimevendor.do?method=retriveNonPrimeVendor";
	}
</script>

<div onload="allowed_to_login();">
	<html:form
		action="/updatenonprimevendorcontact?method=updateSubVendorContact&nonprime=0"
		styleClass="AVMS" styleId="vendorContactForm">
		<html:javascript formName="editVendorContactForm" />
		<html:hidden property="id" styleId="vendorId"/>
		<div class="panelCenter_1"
			style="font-size: 11px; width: 93%; margin: 1% 0% 0% 3%;">
			<h3>Update Contact Information</h3>

			<div class="form-box">
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">First Name</div>
						<div class="ctrl-col-wrapper">
							<html:text property="firstName" alt="" styleClass="text-box"
								tabindex="" />
						</div>
						<span class="error"><html:errors property="firstName"></html:errors></span>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Last Name</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="lastName"
								styleId="lastName" alt="" tabindex="" />
						</div>
						<span class="error"><html:errors property="lastName"></html:errors></span>
					</div>
				</div>
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Title</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="designation"
								styleId="designation" alt="" tabindex="" />
						</div>
						<span class="error"><html:errors property="designation"></html:errors></span>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Phone</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="contactPhone"
								styleId="contactPhone" alt="" tabindex="" />
						</div>
						<span class="error"><html:errors property="contactPhone"></html:errors></span>
					</div>
				</div>
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Mobile</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="contactMobile"
								styleId="contactMobile" alt="Optional" tabindex="" />
						</div>
						<span class="error"><html:errors property="contactMobile"></html:errors></span>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">FAX</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="contactFax"
								styleId="contactFax" alt="" tabindex="" />
						</div>
						<span class="error"><html:errors property="contactFax"></html:errors></span>
					</div>
				</div>
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Email ID</div>
						<html:hidden property="hiddenEmailId" alt=""
							styleId="hiddenEmailId" />
						<div class="ctrl-col-wrapper">
							<html:text property="contanctEmail" alt="" styleId="emailAddress"
								onchange="ajaxEdFn(this,'hiddenEmailId','VE');"
								styleClass="text-box" tabindex="" />
						</div>
						<span style="color: red;"><html:errors
								property="contanctEmail"></html:errors></span>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Allowed to Login</div>
						<div class="ctrl-col-wrapper">
							<html:checkbox property="loginAllowed" styleId="loginAllowed"
								onclick="allowed_to_login()" />
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="panelCenter_1" id="loginDisplay"
			style="visibility: hidden;width: 93%; margin: 1% 0% 0% 3%;">
			<h3>Update User Information</h3>

			<div class="form-box">
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Password</div>
						<div class="ctrl-col-wrapper">
							<html:password property="loginpassword" styleId="loginpassword"
								tabindex="" styleClass="text-box" />
						</div>
						<span class="error"><html:errors property="loginpassword"></html:errors></span>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Confirm Password</div>
						<div class="ctrl-col-wrapper">
							<html:password property="confirmPassword"
								styleId="confirmPassword" tabindex="" styleClass="text-box" />
						</div>
						<span class="error"><html:errors property="confirmPassword"></html:errors></span>
					</div>
				</div>
				<div class="wrapper-half">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Secret Question</div>
						<div class="ctrl-col-wrapper">
							<html:select property="userSecQn" styleId="userSecQn">
								<html:option value="">----Select---</html:option>
								<bean:size id="size" name="secretQnsList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="secretQn" name="secretQnsList">
										<bean:define id="id" name="secretQn" property="id"></bean:define>
										<html:option value="<%=id.toString()%>">
											<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
										</html:option>
									</logic:iterate>
								</logic:greaterEqual>
							</html:select>
						</div>
					</div>
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper">Secret Question Answer</div>
						<div class="ctrl-col-wrapper">
							<html:text styleClass="text-box" property="userSecQnAns"
								styleId="userSecQnAns" alt="" tabindex="37" />
						</div>
						<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="wrapper-btn">
			<html:submit value="Update" styleClass="btn"
				styleId="submit"></html:submit>
			<html:reset value="Cancel" styleClass="btn"
				onclick="backToRetrieve();"></html:reset>
		</div>

	</html:form>
</div>
<script>
	$(document).ready(function() {
		
		$("#contactPhone").mask("(999) 999-9999?");
		$("#contactFax").mask("(999) 999-9999?");
		$("#contactMobile").mask("(999) 999-9999?");
		
		jQuery.validator.addMethod("phoneNumber", function(value, element) { 
	        return this.optional(element) || /^[0-9-()]+$/.test(value);
	  	},"Please enter valid phone number.");
		
		jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
		},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
		
		allowed_to_login();
		
		$("#vendorContactForm").validate({

			rules : {
				firstName : {
					required : true
				},
				lastName : {
					required : true
				},
				designation : {
					required : true
				},
				contactPhone : {
					required : true
				},
				contanctEmail : {
					required : true,
					email : true
				},
				loginpassword : {
					password : true
				},
				confirmPassword : {
					equalTo : "#loginpassword"
				},
				loginpassword : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					password : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				confirmPassword : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					},
					equalTo : "#loginpassword"
				},
				userSecQn : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				},
				userSecQnAns : {
					required : function(element) {
						if ($("#loginAllowed").is(':checked')) {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		});
	});

	chosenConfig();
	function chosenConfig() {
		var config = {
			'.chosen-select' : {
				width : "90%"
			},
			'.chosen-select-deselect' : {
				allow_single_deselect : true,
				width : "90%"
			},
			'.chosen-select-no-single' : {
				disable_search_threshold : 10
			},
			'.chosen-select-no-results' : {
				no_results_text : 'Oops, nothing found!'
			},
			'.chosen-select-width' : {
				width : "90%"
			}
		};
		for ( var selector in config) {
			$(selector).chosen(config[selector]);
		}
	}
</script>

