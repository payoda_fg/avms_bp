<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page
	import="com.fg.vms.customer.dto.RetriveVendorInfoDto,com.fg.vms.customer.model.VendorMaster"%>
<%
	RetriveVendorInfoDto vendorInfo1 = (RetriveVendorInfoDto) session
			.getAttribute("vendorInfoDto");

	session.setAttribute("vendorMaster1", vendorInfo1.getVendorMaster());
%>
<html class="fixed sidebar-left-collapsed">
<!-- For JQuery Panel -->

<script class="jsbin" type="text/javascript"
	src="jquery/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/ui.panel.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>
<link href="jquery/css/ui.panel.css" type="text/css" rel="stylesheet" />

<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />

<script>
	$(document).ready(function() {
		$('.panelCenter_1').panel({
			collapsible : false
		});
		$("#userSecQn").select2({width: "90%"});
		$("#roleId").select2({width: "90%"});
	});
	function allowed_to_login() {
		if(document.getElementById("loginDisplay") != null){
			if (document.getElementById("loginAllowed").checked == true) {
				document.getElementById("loginDisplay").style.visibility = "visible";
				$('#loginAllowed').val("on");
			} else {
				document.getElementById("loginDisplay").style.visibility = "hidden";
				document.getElementById("loginAllowed").value = "";
			}
		}
	}
	function backToRetrieve(){
		var vendorId = $("#vendorId").val();
		window.location = "retrieveprimesupplier.do?method=showSupplierContactDetails&requestString=prime&id="+vendorId;
	}
</script>
<div class="clear"></div>
<section role="main" class="content-body card-margin pt-5">
	<div class="row">
		<div class="col-lg-9 mx-auto">
		<header class="card-header">
			<h2 class="card-title pull-left" onload="allowed_to_login();">
				<img src="images/edit_user.png" alt="" />Edit Contact</h2>
		</header>
<div class="clear"></div>
<html:form action="/updatePrimeVendorContact?method=updateContact"
	styleClass="AVMS" styleId="vendorContactForm">
	<html:javascript formName="editVendorContactForm" />
	<html:hidden property="id"/>
	<html:hidden property="vendorId" styleId="vendorId"/>
	<div class="">
	
		<div class="form-box card-body">	
		<header>
		<h3 class="card-title pt-1 pb-1 text-center">Update Contact Information</h3>
	</header>		
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">First Name</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="firstName" alt=""
							styleId="firstName" />
					</div>
					<span class="error"><html:errors property="firstName"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Last Name</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="lastName" alt=""
							styleId="lastName" />
					</div>
					<span class="error"><html:errors property="lastName"></html:errors></span>
				</div>			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Title</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="designation"
							alt="" styleId="designation" />
					</div>
					<span class="error"><html:errors property="designation"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Phone</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="contactPhone"
							alt="" styleId="contactPhone" />
					</div>
					<span class="error"><html:errors property="contactPhone"></html:errors></span>
				</div>
			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Mobile</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="contactMobile"
							alt="Optional" styleId="contactMobile" />
					</div>
					<span class="error"><html:errors property="contactMobile"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">FAX</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="contactFax"
							alt="" styleId="contactFax" />
					</div>
					<span class="error"><html:errors property="contactFax"></html:errors></span>
				</div>
			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">User Role</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:select property="roleId" styleId="roleId">
							<html:option value="">--Select--</html:option>
							<logic:present name="vendorRoles">
								<html:optionsCollection name="vendorRoles" value="id" label="roleName" />
							</logic:present>														
						</html:select>
					</div>					
				</div>
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Email ID</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:hidden property="hiddenEmailId" alt=""
							styleId="hiddenEmailId" />
						<html:text property="contanctEmail" alt="" styleId="emailAddress"
							onchange="ajaxEdFn(this,'hiddenEmailId','VE');"
							styleClass="main-text-box form-control" />

					</div>
					<span class="error"><html:errors property="contanctEmail"></html:errors></span>
				</div>				
			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Password</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:password property="loginpassword" styleClass="main-text-box form-control"
							styleId="loginpassword" alt="" />
					</div>
					<span class="error"><html:errors property="loginpassword"></html:errors></span>
				</div>
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Confirm Password</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:password property="confirmPassword" 
							styleClass="main-text-box form-control" styleId="confirmPassword" alt="" />
					</div>
					<span class="error"><html:errors property="confirmPassword"></html:errors></span>
				</div>	
			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:select property="userSecQn" styleId="userSecQn">
							<html:option value="">--Select--</html:option>
							<bean:size id="size" name="secretQnsList" />
							<logic:greaterEqual value="0" name="size">
								<logic:iterate id="secretQn" name="secretQnsList">
									<bean:define id="id" name="secretQn" property="id"></bean:define>
									<html:option value="<%=id.toString()%>">
										<bean:write name="secretQn" property="secQuestionParticular"></bean:write>
									</html:option>
								</logic:iterate>
							</logic:greaterEqual>
						</html:select>
					</div>
				</div>
				
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Secret Question Answer</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:text styleClass="main-text-box form-control" property="userSecQnAns"
							alt="" styleId="userSecQnAns" />
					</div>
					<span class="error"><html:errors property="userSecQnAns"></html:errors></span>
				</div>				
			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Gender</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<span class="radio-custom radio-primary">
							<html:radio property="gender" value="M">Male</html:radio>
							<label for=""></label>
						</span>
						<span class="radio-custom radio-primary">
							<html:radio property="gender" value="F">Female</html:radio>
							<label for=""></label>
						</span>
						<span class="radio-custom radio-primary">
							<html:radio property="gender" value="T">Transgender</html:radio>
							<label for=""></label>
						</span>
					</div>
					<span class="error"><html:errors property="gender"></html:errors></span>
				</div>
				
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Primary Contact</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:checkbox property="primaryContact"
							name="editVendorContactForm" styleId="primaryContact" />
					</div>
				</div>		
			
			
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Allowed to Login</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:checkbox property="loginAllowed" styleId="loginAllowed"
							onclick="allowed_to_login()" />
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Business Contact</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:checkbox property="isBusinessContact" styleId="isBusinessContact" />
					</div>
				</div>				
			
				<div class="row-wrapper form-group row" id="preparer">
					<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Preparer</label>
					<div class="col-sm-7 ctrl-col-wrapper">
						<html:checkbox property="isPreparer" styleId="isPreparer" disabled="true" />
					</div>
				</div>
	<div class="clear"></div>
	<footer class="mt-2 card-footer">
		<div class="row justify-content-end">
			<div class="col-sm-9 btn-wrapper">
				<html:submit value="Update" styleClass="btn btn-primary" styleId="submit"></html:submit>
				<html:reset value="Cancel" styleClass="btn btn-default" onclick="backToRetrieve();"></html:reset>
			</div>
		</div>
	</footer>
	</div>
	</div>
</html:form>
</div>
</div>
</section>
<script>
	allowed_to_login();
		
	jQuery.validator.addMethod("onlyNumbers", function(value, element) { 
        return this.optional(element) || /^[0-9]+$/.test(value);
    },"Please enter only numbers.");
	
	 jQuery.validator.addMethod("password",function(value, element) {
			return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	 
	 jQuery.validator.addMethod("allowhyphens", function(value, element) { 
         return this.optional(element) || /^[0-9-]+$/.test(value);
     },"Please enter valid numbers.");
	 
	 $(document).ready(function() {
		 
		 $("#contactPhone").mask("(999) 999-9999?");
		  $("#contactFax").mask("(999) 999-9999?");
		  $("#contactMobile").mask("(999) 999-9999?");
		  
				$("#vendorContactForm").validate({
					rules : {
						firstName : {
							required : true,
							maxlength : 255
						},
						lastName : {
							required : true,
							maxlength : 255
						},
						designation : {
							required : true,
							maxlength : 255
						},
						contactPhone : {
							required : true
						},
						roleId : {
							required : true
						},
						contanctEmail : {
							required : true,
							email : true
						},
						loginId : {
							required : function(element) {
								if ($("#loginAllowed").is(':checked')) {
									return true;
								} else {
									return false;
								}
							},
							login:true,
							maxlength : 120
						},
						loginpassword : {
							required : function(element) {
								if ($("#loginAllowed").is(':checked')) {
									return true;
								} else {
									return false;
								}
							},
							password : function(element) {
								if ($("#loginAllowed").is(':checked')) {
									return true;
								} else {
									return false;
								}
							}
						},
						confirmPassword : {
							required : function(element) {
								if ($("#loginAllowed").is(':checked')) {
									return true;
								} else {
									return false;
								}
							},
							equalTo : "#loginpassword"
						},
						userSecQn : {
							required : function(element) {
								if ($("#loginAllowed").is(':checked')) {
									return true;
								} else {
									return false;
								}
							}
						},
						userSecQnAns : {
							required : function(element) {
								if ($("#loginAllowed").is(':checked')) {
									return true;
								} else {
									return false;
								}
							},
							rangelength : [ 3, 15 ]
						},
					}
				});
			});
</script>
</html>
