<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("statusDashboard") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("statusDashboard");
	}
	Tier2ReportDto tier2ReportDto = null;

	if (reportDtos != null) {
		for (int index = 0; index < reportDtos.size(); index++) {
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("status", tier2ReportDto.getStatusName());
			cellobj.put("count", tier2ReportDto.getStatusCount()
					.intValue());
			cellobj.put("percent",
					Math.round(tier2ReportDto.getStatusPercent()));
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
