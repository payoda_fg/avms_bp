<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;
	List<Tier2ReportDto> reportDtos = null;
	
	if (session.getAttribute("vendorsByBPMarketSector") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("vendorsByBPMarketSector");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			
			cellobj.put("bpMarketSector", tier2ReportDto.getMarketSector());
			cellobj.put("vendorCount", tier2ReportDto.getCount().intValue());
			cellobj.put("percent", Math.round(tier2ReportDto.getPercentage()));
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>