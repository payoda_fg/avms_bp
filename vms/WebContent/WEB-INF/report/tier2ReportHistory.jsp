<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/grid.locale-en1.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min1.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid1.css" />


<!-- <link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />-->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<!--
#paddtree_center{
	padding: 0 0 0 30%;
}
#editmodaddtree{
	height: 150px !important;
}
-->
</style>

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">Tier2 Report History</h2>
				<input type="button" value="Add New Report" class="btn pull-right btn-primary"
					onclick="linkPage('tier2Report.do?method=tire2Report')">
			</header>
<div class="clear"></div>
<div id="form-box" class="card-body">
	<table id="reportHistory" class="table table-bordered table-striped mb-0">
		<tr>
	        <td />
	    </tr>
	</table>
	<div id="paddtree"></div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
var grid = $("#reportHistory");

$(document).ready(function() {
	 
}); 
	$.ajax({
		url : 'tier2Report.do?method=listReportHistory' + '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) {
			$("#reportHistory").jqGrid('GridUnload');
			reportHisrory(data);
		}
	});
	
		
	function reportHisrory(myGridData) {
		var newdata = jQuery.parseJSON(myGridData);

		jQuery("#reportHistory").jqGrid(
				{
										data : newdata,
					datatype : "local",
					colNames : ["id", "Vendor Name", "Year","Reporting Period","Direct Spend","Indirect Spend","Total Sales","Total Sales To Company","Submitted", "Created On"],
				colModel : [{
						name : 'id',
						index : 'id' ,
						hidden : true,
						search : false
					},
					{
						name : 'vendorName',
						index : 'vendorName',
						align : "center",
						editable : true,
						sorttype:'String',
						searchoptions : {sopt : ['cn', 'eq', 'bw', 'nc', 'ew', 'en']}
					},
				 	{
						name : 'year',
						index : 'year',
						align : "center",
						editable : true ,
						sorttype:'String',
						width :130,
						resizable:true,
						searchoptions : {sopt : ['cn', 'eq', 'bw', 'nc', 'ew', 'en']}
					},
				 	{
						name : 'reportPeriod',
						index : 'reportPeriod',
						align : "center",
						editable : true  ,
						width :280,
						resizable:true,
						sorttype:'String',
						searchoptions : {sopt : ['cn', 'eq', 'bw', 'nc', 'ew', 'en']}
						
					},
				 	{
						name : 'directExp',
						index : 'directExp',
						align : "right",
						editable : true ,
						sorttype:'Integer',
						search : false
					},
				 	{
						name : 'indirectExp',
						index : 'indirectExp',
						align : "right",
						editable : true,
						sorttype:'Integer' ,
						search : false
					},
				 	{
						name : 'totalSales',
						index : 'totalSales',
						align : "right",
						editable : true,
						sorttype:'Integer',
						search : false
				},
				 {
					name : 'totalSalesToCompany',
					index : 'totalSalesToCompany',
					align : "right",
					editable : true ,
					sorttype:'Integer',
					search : false,
					width :240,
				 	resizable:true
				},
				{
					name : 'isSubmitted',
					index : 'isSubmitted',
					align : "center",
					readonly:"readonly",
					 editable:false, 
					 sorttype:'Integer',
					formatter: checkboxFormatter,
					search : false
				},
				{
					name : 'createdOn',
					index : 'createdOn',
					align : "center",					
					sorttype : 'date',
					formatter : 'date',
					formatoptions : 
					{
						srcformat:'m-d-y',
					 	newformat: 'm-d-Y'
					},
					editable:true,					 
					search : false
				}],

					height : '350',
					pager : "#paddtree",
					rowNum : 15,
					rowList : [ 15, 30, 50 ],
					loaded : true,
					loadonce: true,
					cmTemplate: { title: false },
					hidegrid : false,
					ignoreCase: true,
					shrinkToFit : false,
					autowidth : true,
					viewrecords : true,
					emptyrecords : 'No Data Available...',
					userDataOnFooter : true,
					cmTemplate: {title: false},		
					gridview: true,
					width: ($.browser.webkit?466:497),
					onSelectRow : function(id) {
				    	var rowData = jQuery(this).getRowData(id)
						currentId = rowData['id'];
						window.location = "tier2Report.do?method=editTier2ReportMaster&type=history&id="+currentId;
					}
				});
	 
		$(window).bind('resize', function() {
			$("#reportHistory").setGridWidth($('#form-box').width() - 30, true);
		}).trigger('resize');
	}
	
	jQuery("#reportHistory").jqGrid('filterToolbar',{
		stringResult : true,
		searchOperators : true, 
		searchOnEnter:false
	});
	
  function checkboxFormatter(cellvalue, options, rowObject) {
	    cellvalue = cellvalue + "";
	    cellvalue = cellvalue.toLowerCase();
	    var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked=\"checked\"" : "";
	    return "<input type='checkbox' disabled='true' onclick=\"ajaxSave('" + options.rowId + "', this);\" " + bchk + " value='" + cellvalue + "' offval='no' />";
	}
</script>
