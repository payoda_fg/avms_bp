<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<div id="content" class="card-body">
	<header class="card-header">
		<h2 class="card-title pull-left">Spend Data Dashboard Report</h2>
	</header>
	<table id="report" class="main-table table table-bordered table-striped mb-0">
		<tbody>
			<%
				List<Tier2ReportDto> reportDtos = null;
				if (session.getAttribute("spendDashboard") != null) {
					reportDtos = (List<Tier2ReportDto>) session
							.getAttribute("spendDashboard");
				}
				Tier2ReportDto tier2ReportDto = null;
				java.text.NumberFormat format = java.text.NumberFormat
						.getCurrencyInstance(java.util.Locale.US);
				if (reportDtos != null) {
					for (int index = 0; index < reportDtos.size(); index++) {
						tier2ReportDto = reportDtos.get(index);
			%>
			<tr>
				<td><%=tier2ReportDto.getVendorName()%></td>
			</tr>
			<tr>
				<td>Reporting
					Period</td>
				<td><%=tier2ReportDto.getReportPeriod()%></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Tier 2 Vendor Name</b></td>
				<td><%=tier2ReportDto.getVendorUserName()%></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Total Sales US</b></td>
				<c:choose>
					<c:when test="${empty tier2ReportDto.getTotalSales()}">
       					 <td><%=new java.text.DecimalFormat("$#,###").format(tier2ReportDto.getTotalSales())%></td>
    				</c:when>
					<c:otherwise>
					<td><%=format.format(tier2ReportDto.getTotalSales())%></td>
					</c:otherwise>
				</c:choose>
				<%-- <td><%=format.format(tier2ReportDto.getTotalSales())%></td> --%>
			</tr>
			<tr>
				<td></td>
				<td><b>Total Sales to Customer</b></td>
				<c:choose>
					<c:when test="${empty tier2ReportDto.getTotalSalesToCompany()}">
       					 <td><%=new java.text.DecimalFormat("$#,###").format(tier2ReportDto.getTotalSalesToCompany())%></td>
    				</c:when>
					<c:otherwise>
					<td><%=format.format(tier2ReportDto.getTotalSalesToCompany())%></td>
					</c:otherwise>
				</c:choose>
				<%-- <td><%=format.format(tier2ReportDto
							.getTotalSalesToCompany())%></td> --%>
			</tr>
			<tr>
				<td><b>Direct Expenditures Reported</b></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Certificate</b></td>
				<td><%=tier2ReportDto.getCertificateName()%></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Direct Expenditures</b></td>
				<c:choose>
					<c:when test="${empty tier2ReportDto.getDirectExp()}">
       					 <td ><%=new java.text.DecimalFormat("$#,###").format(tier2ReportDto.getDirectExp())%></td>
    				</c:when>
					<c:otherwise>
					<td><%=format.format(tier2ReportDto.getDirectExp())%></td>
					</c:otherwise>
				</c:choose>
				<%-- <td align="right"><%=format.format(tier2ReportDto.getDirectExp())%></td> --%>
			</tr>
			<tr>
				<td><b>Indirect Expenditures Reported</b></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Diversity Category</b></td>
				<td><%=tier2ReportDto.getCertificateName()%></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Ethnicity Classification</b></td>
				<td><%=tier2ReportDto.getEthnicity()%></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Indirect Purchase Amount</b></td>
				<c:choose>
					<c:when test="${empty tier2ReportDto.getIndirectExp()}">
       					 <td><%=new java.text.DecimalFormat("$#,###").format(tier2ReportDto.getIndirectExp())%></td>
    				</c:when>
					<c:otherwise>
					<td><%=format.format(tier2ReportDto.getIndirectExp())%></td>
					</c:otherwise>
				</c:choose>
				<%-- <td align="right"><%=format.format(tier2ReportDto.getIndirectExp())%></td> --%>
			</tr>
			<tr>
				<td></td>
				<td><b>Total</b></td>
				<c:choose>
					<c:when test="${empty tier2ReportDto.getGrandTotal()}">
       					 <td><%=new java.text.DecimalFormat("$#,###").format(tier2ReportDto.getGrandTotal())%></td>
    				</c:when>
					<c:otherwise>
					<td><%=format.format(tier2ReportDto.getGrandTotal())%></td>
					</c:otherwise>
				</c:choose>
				<%-- <td align="right"><%=format.format(tier2ReportDto.getGrandTotal())%></td> --%>
			</tr>
			<%
				}
				}
			%>
		</tbody>
	</table>
</div>
