<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Supplier Custom Search Report" name="privilege"
		property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
			<script>
			function backToSearch() {
				window.location = "searchReport.do?method=searchVendorReport";
			}
				//Gets Country (United States) and Sets Related States in State Button...
				$(document).ready(function() 
				{
					<logic:present name="userDetails" property="workflowConfiguration">
						<logic:equal value="0" name="userDetails" property="workflowConfiguration.internationalMode">
							$.ajax(
							{
								url : 'state.do?method=getStateByDefaultCountry&random=' + Math.random(),
								type : "POST",
								async : true,
								success : function(data) 
								{
									$("#state").find('option').remove().end().append(data);
									$('#state').trigger("chosen:updated");
								}
							});
						</logic:equal>
					</logic:present>
					
					callAutoCompleteForNaicsCode();
					callAutoCompleteForNaicsDesc();
				});
			
				function assignValue(value, id) {
					$('#' + id).val(value.value);
				}
				
				function changestate(ele, id) {
					var country = ele.value;
					if (country != null && country != '') {
						$.ajax({
							url : 'state.do?method=getState&id=' + country + '&random='
									+ Math.random(),
							type : "POST",
							async : false,
							success : function(data) {
								$("#" + id).find('option').remove().end().append(data);
								$('#' + id).trigger("chosen:updated");
							}
						});
					} else {
						$("#" + id).find('option').remove().end().append('<option value="">&nbsp;&nbsp;</option>');
						$('#' + id).trigger("chosen:updated");
					}
				}
				function customizeColumns() {

					$.ajax({
						url : "dynamicReport.do?method=getSearchReportData&type=customize" ,
						data : $("#searchform").serialize(),
						type : "POST",
						async : true,
						success : function(data) {
							window.location = "dynamicReport.do?method=showDynamicColumnSet";
							
							}
					});
				}
			function recallPreviousSearch(){
				
				$("#dialog1").css({
					"display" : "block"
				});
				$("#dialog1").dialog({
					minWidth : 900,
					modal : true
				});
		}
			function dynamicExport() {

				$("#dialog2").css({
					"display" : "block"
				});
				$("#dialog2").dialog({
					minWidth : 300,
					modal : true
				});

			}
			function confirm_delete1() {
				input_box = confirm("Are you sure you want to delete this Record?");
				if (input_box == true) {
					return true;
				} else {
					return false;
				}
			}
			function deleteSearchFilter(searchId) {
				var result=confirm_delete1();
			if(result){
					$.ajax({
					url : "viewVendors.do?method=deleteSearchFilter&searchType=R"
							+"&searchId="+searchId,
					type : 'POST',
					async : false,
					dataType : "json",
					beforeSend : function() {
						$("#ajaxloader").show(); 
					},
					success : function(data) {
						$("#ajaxloader").hide();
						$("#dialog1").dialog("close");
						if(data.result=="success")
						{
							alert("Search Filter Successfully Deleted");
							window.location.reload(true);
						}
						else{
								alert("Sorry Transaction Failed");
							}
					}
				});		
			}
			else
				return false;
			}
			</script>
			<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
			<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
			<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
			
			<script type="text/javascript" src="js/rfiinformation.js"></script>
			<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
			<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
			<!-- Added for populate jqgrid. -->
			<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
			<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
			
			<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
			<!-- For select 2 -->
			

<link rel="stylesheet" type="text/css" media="screen" 	href="jquery/css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" 	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>

			<style>
.main-list-box {
	padding: 0% 1%;
}

.main-table1 td.header {
	background: none repeat scroll 0 0 #009900;
	color: #FFFFFF;
}

.main-table1 td {
	border: 1px solid #E9EAEA;
	padding: 5px;
}

tr:nth-child(2n+1) {
	background: none repeat scroll 0 0;
}

#searchTable td {
	border: 0px solid #E9EAEA;
	text-align: left !important;
}

.chosen-disabled {
	opacity: 1 !important;
}

</style>
			<script type="text/javascript">
			<!--
				$().ready(function() {
					/* $("#searchFields11").select2({
						width : "50%"
					}); */
				})
			//-->
			</script>
<section role="main" class="content-body card-margin reports-module">
	<div class="row">
		<div class="col-lg-12 mx-auto col-sm-12 col-md-12 col-xl-12 col-xs-12">	
				<section class="card">
					<header class="card-header">
						<h2 class="card-title pull-left"> Supplier Custom Search Report </h2>
					<div class="row justify-content-end">
						<div class="col-sm-9 wrapper-btn">
							<input type="button" value="Recall Previous Search" class="btn btn-primary pull-right"
							onclick="recallPreviousSearch();">
						</div>
					</div>
					</header>
				<div class="card-body">
				<div id="select"  style="display: block;" class="form-box">
				<html:form action="/viewVendors?method=vendorSearch"
					styleId="searchform" styleClass="form-horizontal search-report-form">
					<table border="0" class="main-table1 table table-bordered mb-0 table-no-more" id="searchTable">
						<thead>
						<tr align="center">
							<th  width="50%">Fields</th>
							<th  width="50%">Values</th>
							<!--<td style="float: right;"><input type="button" -->
							<!--value="Search" class="btn" onclick="searchReport();"></td> -->
						</tr>
						</thead>
						<tr>
							<td>Vendor Status</td>
						<%-- 	<td><select name="primeVendorStatus" id="primeVendorStatus"
								class="chosen-select fields"
								onchange="showStatusDesc(this.value)">
									<option value="AB">Both</option>
									<logic:present name="searchVendorForm" property="vendorStatus">
										<bean:size id="size" property="vendorStatus"
											name="searchVendorForm" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="primeVendorStatus" property="vendorStatus"
												name="searchVendorForm">
												<bean:define id="name" name="primeVendorStatus"
													property="id"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="primeVendorStatus" property="statusname" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td> --%>
							<td>
								<div class="search-value-wrapper">
									<select name="searchVendorStatus" id="searchVendorStatus"
									class="chosen-select-deselect form-control" multiple="multiple"
									data-placeholder=" " 
									onchange="showStatusDesc(this.value)">
										<option value="0">All Status</option>
										<logic:present name="vendorStatusList">
											<bean:size id="size" name="vendorStatusList" />
											<logic:greaterEqual value="0" name="size">
												<logic:iterate id="primeVendorStatus" name="vendorStatusList">
													<bean:define id="name" name="primeVendorStatus"
														property="id"></bean:define>
													<option value="<%=name.toString()%>">
													<bean:write
															name="primeVendorStatus" property="statusname" /></option>
												</logic:iterate>
											</logic:greaterEqual>
										</logic:present>
									</select>
								</div>
							</td>
						
							<td><div id="statusDescArea"
									style="width: 400px; display: none;">
									<textarea name="statusdesc" id="statusdesc" readonly="readonly"
										style="width: 400px; font-weight: bolder; height: 60px; width: 400px; color: #009900;"></textarea>
								</div></td>
						</tr>
<!-- 						<tr> -->
<!-- 							<td>Supplier Type</td> -->
<!-- 							<td><select name="vendorType" class="chosen-select fields"> -->
<!-- 									<option value="PRIME">Tier 1</option> -->
<!-- 									<option value="NONPRIME">Tier 2</option> -->
<!-- 									<option value="BOTH">Both</option> -->
<!-- 							</select></td> -->
<!-- 						</tr> -->
						<tr>
							<td><select name="searchFields" id="searchFields1"
								onchange="changeInputType(2,this);" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option selected="selected" value="LEGALCOMPANYNAME">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME">First Name</option>
										<option value = "CONTACTLASTNAME">Last Name</option>
										<option value = "CONTACTDATE">Contact Date</option>
										<option value = "CONTACTSTATE">Contact State</option>
										<option value = "CONTACTEVENT">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" style="width: 95%;" /></td>
							<td><input type="hidden"name="searchFields" style="width:95%;" id="hiddenSearchFields1" value="LEGALCOMPANYNAME" /></td>
						</tr>		
						
						<tr>
							<td><select name="searchFields" id="searchFields2"
								onchange="changeInputType(3,this);" class="chosen-select fields" disabled="disabled">
								<option value=""></option>
								<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES"  disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" selected="selected">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" style="width: 95%;"  /></td>
							<td><input type="hidden"name="searchFields" style="width: 95%;"  id="hiddenSearchFields2" value="KEYWORD" /></td>
						</tr>
						
						<tr>
							<td>
								<select name="searchFields" id="searchFields3" onchange="changeInputType(4,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE"  disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<!-- <optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup> -->
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" selected="selected">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
									<optgroup label="Business Biography and Safety">
										<option value="OILANDGASINDUSTRYEXPERIENCE" disabled="disabled">Oil and Gas Industry Experience</option>
									</optgroup>
								</select>
							</td>
							<td>
								<input type="hidden" class="main-text-box" name="searchFieldsValue" value="" id="bpMarketSectorHiddenId" />
								<select name="bpMarketSectorId" class="chosen-select-deselect" style="width: 350px;" multiple="multiple"
									data-placeholder=" " onchange="assignValue(this,'bpMarketSectorHiddenId');">
									<option value=""></option>
									<logic:present name="marketSectorsList">
										<bean:size id="size" name="marketSectorsList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="marketSector" name="marketSectorsList">
												<bean:define id="sectorId" name="marketSector" property="id"/>
 												<option value="<%=sectorId.toString()%>"><bean:write name="marketSector" property="sectorDescription"/></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields3" value="BPMARKETSECTOR" /></td>
						</tr>
						
					<%-- 	<tr>
							<td><select name="searchFields" id="searchFields4"
								onchange="changeInputType(5,this)" class="chosen-select fields">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<option value="COMPANYCODE" disabled="disabled">Company Code</option>
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE" disabled="disabled">Province</option>
										<option value="REGION" disabled="disabled">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS
											Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<option value="DIVERSESUPPLIER">Diverse Supplier</option>
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" selected="selected"
											disabled="disabled">Commodity Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="commodityDescription1" />
								<select name="commodityDescription"
								class="chosen-select-deselect" style="width: 350px;"
								multiple="multiple"
								data-placeholder="Choose a commodity description... "
								onchange="assignValue(this,'commodityDescription1');">
									<option value=""></option>
									<logic:present name="commodityDescription">
										<bean:size id="size" name="commodityDescription" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="commodity" name="commodityDescription">
												<bean:define id="category" name="commodity"
													property="commodityCategory"></bean:define>
												<bean:define id="description" name="commodity"
													property="commodityDescription"></bean:define>
												<optgroup label="<%=category.toString()%>">

													<%
														String[] descriptions = description
																							.toString().split("\\|");
																					if (descriptions != null
																							&& descriptions.length != 0) {
																						for (int i = 0; i < descriptions.length; i++) {
																							String[] parts = descriptions[i]
																									.split(":");
																							String descriptionId = parts[0];
																							String descriptionName = parts[1];
													%>
													<option value="<%=descriptionId%>">
														<%=descriptionName%>
													</option>
													<%
														}
																					}
													%>

												</optgroup>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
						</tr> --%>
						
						<tr>
							<td><select name="searchFields" id="searchFields4"
								onchange="changeInputType(5,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE"  disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" selected="selected">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="vendorCommodity1" />
								<select name="vendorCommodity"
								class="chosen-select-deselect" style="width: 350px;"
								multiple="multiple"
								data-placeholder=" "
								onchange="assignValue(this,'vendorCommodity1');">
									<option value=""></option>
									<logic:present name="vendorCommodity">
										<bean:size id="size" name="vendorCommodity" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="commodity" name="vendorCommodity">										
												<bean:define id="subSectorDesc" name="commodity" property="subSectorDescription"/>
												<bean:define id="commodityDesc" name="commodity" property="commodityDescription"/>
												<optgroup label="<%=subSectorDesc.toString()%>">
													<%
														String[] descriptions = commodityDesc.toString().split("\\|");
														if (descriptions != null && descriptions.length != 0) 
														{
															for (int i = 0; i < descriptions.length; i++) 
															{
																String[] parts = descriptions[i].split(":");
																String commodityDescId = parts[0];
																String commodityDescName = parts[1];
													%>
													<option value="<%=commodityDescId%>"><%=commodityDescName%></option>
													<%
															}
														}
													%>
												</optgroup>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields4" value="VENDORCOMMODITY" /></td>
						</tr>
						
						<tr>
							<td><select name="searchFields" id="searchFields5"
								onchange="changeInputType(6,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE"	disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE" disabled="disabled">Province</option>
										<option value="REGION" disabled="disabled">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" selected="selected">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="businessArea1" /> <select
								name="businessAreaName" class="chosen-select-deselect"
								style="width: 350px;" multiple="multiple"
								data-placeholder=" "
								onchange="assignValue(this,'businessArea1');">
									<option value=""></option>
									<logic:present name="businessAreas">
										<bean:size id="size" name="businessAreas" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="businessArea" name="businessAreas">
												<bean:define id="bGroup" name="businessArea"
													property="businessGroup"></bean:define>
												<bean:define id="barea" name="businessArea"
													property="serviceArea"></bean:define>
												<optgroup label="<%=bGroup.toString()%>">

													<%
														String[] areas = barea.toString()
																							.split("\\|");
																					if (areas != null && areas.length != 0) {
																						for (int i = 0; i < areas.length; i++) {
																							String[] parts = areas[i]
																									.split("-");
																							String areaId = parts[0];
																							String areaName = parts[1];
													%>
													<option value="<%=areaId%>">
														<%=areaName%>
													</option>
													<%
														}
																					}
													%>

												</optgroup>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields5" value="BUSINESSAREA" /></td>
						</tr>
						
						<tr>
							<logic:present name="userDetails" property="workflowConfiguration">
								<logic:equal value="1" name="userDetails" property="workflowConfiguration.internationalMode">
									<td><select name="searchFields" id="searchFields6"
										onchange="changeInputType(7,this)" class="chosen-select fields" disabled="disabled">
											<option value=""></option>
											<optgroup label="Company Information">
												<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
													Company Name</option>
												<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
												<option value="DUNSNUMBER">Duns Number</option>
												<option value="TAXID">Tax ID</option>
												<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
												<option value="COMPANYTYPE">Company Type</option>
												<option value="YEARESTABLISHED">Year Established</option>
												<option value="ADDRESS">Address</option>
												<option value="CITY">City</option>
												<option value="STATE" disabled="disabled">State</option>
												<option value="PROVINCE">Province</option>
												<option value="REGION">Region</option>
												<option value="COUNTRY" selected="selected">Country</option>
												<option value="ZIPCODE">Zip Code</option>
												<option value="MOBILE">Mobile</option>
												<option value="PHONE">Phone</option>
												<option value="FAX">Fax</option>
												<option value="WEBSITEURL">Website URL</option>
												<option value="EMAIL">Email</option>
												<option value="CERTIFICATIONTYPE">Certification Type</option>
												<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
											</optgroup>
											<optgroup label="NAICS">
												<!-- <option value="NAICSCODE" disabled="disabled">NAICS
													Code</option> -->
												<option value="NAICSDESC" disabled="disabled">NAICS
													Desc</option>
											</optgroup>
											<optgroup label="Diverse Classification">
												<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
												<option value="DIVERSECLASSIFICATION">Diverse
													Classification</option>
												<option value="CERTIFYINGAGENCY">Certifying Agency</option>
												<option value="CERTIFICATION">Certification #</option>
												<option value="EFFECTIVEDATE">Effective Date</option>
												<option value="EXPIREDATE">Expire Date</option>
												<option value="DIVERSESUPPLIER">Diverse Supplier</option>
												<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
													Classification Notes</option>
											</optgroup>
											<optgroup label="Contact Information">
												<option value="FIRSTNAME">Contact First Name</option>
												<option value="LASTNAME">Contact Last Name</option>
												<option value="DESIGNATION">Title</option>
												<option value="CONTACTPHONE">Contact Phone</option>
												<option value="CONTACTMOBILE">Contact Mobile</option>
												<option value="CONTACTFAX">Contact Fax</option>
												<option value="CONTACTEMAIL">Contact Email</option>
											</optgroup>
											<optgroup label="Business Area">
												<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
												<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
												<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
											</optgroup>
											<!-- <optgroup label="Commodity Description">
												<option value="COMMODITYDESCRIPTION">Commodity
													Description</option>
											</optgroup> -->
											<optgroup label="Service Area">
												<option value="KEYWORD" disabled="disabled">Keyword</option>
											</optgroup>
											<optgroup label = "Contact Meeting Information">
												<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
												<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
												<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
												<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
												<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
											</optgroup>
									</select></td>
									<td><input type="hidden" class="main-text-box"
										name="searchFieldsValue" value="" id='selectCountries1' /><select
										name="selectCountries" 
										class="chosen-select" style="width: 250px;"								
										data-placeholder=" " 
										onchange="changestate(this,'state');assignValue(this,'selectCountries1');">
											<option value=""></option>
											<logic:present name="searchVendorForm" property="countries">
												<bean:size id="size" property="countries"
													name="searchVendorForm" />
												<logic:greaterEqual value="0" name="size">
													<logic:iterate id="country" property="countries"
														name="searchVendorForm">
														<bean:define id="name" name="country" property="id"></bean:define>
														<option value="<%=name.toString()%>"><bean:write
																name="country" property="countryname" /></option>
													</logic:iterate>
												</logic:greaterEqual>
											</logic:present>
											<!-- <option value="1"> United States </option> -->
									</select></td>
									<td><input type="hidden"name="searchFields" id="hiddenSearchFields6" value="COUNTRY" /></td>
								</logic:equal>							
							</logic:present>
						</tr>						
						
						<tr>
							<td><select name="searchFields" id="searchFields7"
								onchange="changeInputType(8,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE"	disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" selected="selected" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><select name="searchFieldsValue" class="chosen-select"
								id="state">
									<option value="">&nbsp;&nbsp;</option>
							</select></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields7" value="STATE" /></td>
						</tr>
						
						<tr>
							<td><select name="searchFields" id="searchFields8"
								onchange="changeInputType(9,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE"	disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE" selected="selected"
											disabled="disabled">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
										<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="certificationType1" /> <select
								name="certificationType" class="chosen-select-deselect" 
								style="width: 350px;" multiple="multiple" 
								data-placeholder=" "
								onchange="assignValue(this,'certificationType1');">
									<option value=""></option>
									<logic:present name="certificationTypes">
										<bean:size id="size" name="certificationTypes" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="certificationType"
												name="certificationTypes">
												<bean:define id="name" name="certificationType"
													property="certficateTypeId"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="certificationType" property="certficateTypeDescription" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields8" value="CERTIFICATIONTYPE" /></td>
						</tr>
						
						<tr>
							<td><select name="searchFields" id="searchFields9"
								onchange="changeInputType(10,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE"	disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE" disabled="disabled">Province</option>
										<option value="REGION" disabled="disabled">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" selected="selected"
											disabled="disabled">Diverse Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="diverseCertificate1" /> <select
								name="diverseCertificateNames" class="chosen-select-deselect"
								style="width: 350px;" multiple="multiple"
								data-placeholder=" "
								onchange="assignValue(this,'diverseCertificate1');">
									<option value=""></option>
									<logic:present name="certificateTypes">
										<bean:size id="size" name="certificateTypes" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="certificate" name="certificateTypes">
												<bean:define id="id" name="certificate" property="id"></bean:define>
												<option value="<%=id.toString()%>"><bean:write
														name="certificate" property="certificateName"></bean:write></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields9" value="DIVERSECLASSIFICATION" /></td>
						</tr>
						
						<!-- <tr>
							<td><select name="searchFields" id="searchFields10"
								onchange="changeInputType(11,this)" class="chosen-select fields">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<option value="COMPANYCODE"	disabled="disabled">Company Code</option>
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE" disabled="disabled">Province</option>
										<option value="REGION" disabled="disabled">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS
											Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<option value="DIVERSESUPPLIER" selected="selected"
											disabled="disabled">Diverse Supplier</option>
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><select name="searchFieldsValue"
								class="chosen-select-deselect" style="width: 250px;"
								data-placeholder="Choose a diverse supplier...">
									<option value=""></option>
									<option value="1">Diverse</option>
									<option value="0">Non Diverse</option>
							</select></td>
						</tr> -->
						
						<!-- <tr>
							<td><select name="searchFields" id="searchFields10"
								onchange="changeInputType(11,this)" class="chosen-select fields">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<option value="COMPANYCODE" selected="selected" disabled="disabled">Company Code</option>
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" disabled="disabled">NAICS
											Code</option>
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<option value="DIVERSESUPPLIER">Diverse Supplier</option>
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup>
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" /></td>
						</tr> -->
						
						<tr>
							<td><select name="searchFields" id="searchFields10"
								onchange="changeInputType(11,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="VENDORCLASSIFICATION">Vendor Classification</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE">NAICS
											Code</option> -->
										<option value="NAICSDESC">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" selected="selected"
											disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse
											Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" style="width: 95%;" /></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields10" value="CAPABILITIES" /></td>
						</tr>
												
						<%-- <tr>
							<td><select name="searchFields" id="searchFields11"
								onchange="changeInputType(12,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<option value="NAICSCODE" selected="selected"
											disabled="disabled">NAICS Code</option>
										<option value="NAICSDESC">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsCode1" />
							<input  type="text" class="main-text-box"  id="naicsCode"  name="naicsCode"
							placeholder=" "
								onchange="assignValue(this,'naicsCode1');" /></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields11" value="NAICSCODE" /></td>
							<!-- <td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsCode1" /> <select
								name="naicsCode" class="chosen-select-deselect"
								style="width: 350px;" multiple="multiple"
								data-placeholder="Choose a naics code..."
								onchange="assignValue(this,'naicsCode1');">
									<option value=""></option>
									<logic:present name="naicsCodeList">
										<bean:size id="size" name="naicsCodeList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="naicsCodes" name="naicsCodeList">
												<bean:define id="name" name="naicsCodes"
													property="naicsCode"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="naicsCodes" property="naicsCode" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td> -->
						</tr> --%>
						
						<tr>
							<td><select name="searchFields" id="searchFields11"
								onchange="changeInputType(12,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" selected="selected"
											disabled="disabled">NAICS Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsDescription1" style="width: 95%;" /> 
							<input type="text" class="main-text-box" id="naicsDescription" name="naicsDescription"
								placeholder=" " style="width: 95%;" 
								onchange="assignValue(this,'naicsDescription1');" /></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields11" value="NAICSDESC" /></td>
							<%-- <td><input type="hidden" class="main-text-box"
								name="searchFieldsValue" value="" id="naicsDescription1" /> <select
								name="naicsDescription" class="chosen-select-deselect"
								style="width: 350px;" multiple="multiple"
								data-placeholder="Choose a naics description..."
								onchange="assignValue(this,'naicsDescription1');">
									<option value=""></option>
									<logic:present name="naicsCodeList">
										<bean:size id="size" name="naicsCodeList" />
										<logic:greaterEqual value="0" name="size">
											<logic:iterate id="naicsCodes" name="naicsCodeList">
												<bean:define id="name" name="naicsCodes"
													property="naicsDescription"></bean:define>
												<option value="<%=name.toString()%>"><bean:write
														name="naicsCodes" property="naicsDescription" /></option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
							</select></td> --%>
						</tr>						
						
						<tr>
							<td><select name="searchFields" id="searchFields12"
								onchange="changeInputType(13,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal
											Company Name</option>
										<!-- <option value="COMPANYCODE"	disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE" selected="selected" disabled="disabled">Province</option>
										<option value="REGION">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" style="width: 95%;" /></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields12" value="PROVINCE" /></td>
						</tr>
						
						<tr>
							<td><select name="searchFields" id="searchFields13"
								onchange="changeInputType(14,this)" class="chosen-select fields" disabled="disabled">
									<option value=""></option>
									<optgroup label="Company Information">
										<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
										<!-- <option value="COMPANYCODE"	disabled="disabled">Company Code</option> -->
										<option value="DUNSNUMBER">Duns Number</option>
										<option value="TAXID">Tax ID</option>
										<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
										<option value="COMPANYTYPE">Company Type</option>
										<option value="YEARESTABLISHED">Year Established</option>
										<option value="ADDRESS">Address</option>
										<option value="CITY">City</option>
										<option value="STATE" disabled="disabled">State</option>
										<option value="PROVINCE" disabled="disabled">Province</option>
										<option value="REGION" selected="selected" disabled="disabled">Region</option>
										<option value="COUNTRY" disabled="disabled">Country</option>
										<option value="ZIPCODE">Zip Code</option>
										<option value="MOBILE">Mobile</option>
										<option value="PHONE">Phone</option>
										<option value="FAX">Fax</option>
										<option value="WEBSITEURL">Website URL</option>
										<option value="EMAIL">Email</option>
										<option value="CERTIFICATIONTYPE">Certification Type</option>
										<option value = "BPSEGMENT" disabled="disabled">BP Segment</option>
									</optgroup>
									<optgroup label="NAICS">
										<!-- <option value="NAICSCODE" disabled="disabled">NAICS
											Code</option> -->
										<option value="NAICSDESC" disabled="disabled">NAICS
											Desc</option>
									</optgroup>
									<optgroup label="Diverse Classification">
										<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
										<option value="DIVERSECLASSIFICATION">Diverse
											Classification</option>
										<option value="CERTIFYINGAGENCY">Certifying Agency</option>
										<option value="CERTIFICATION">Certification #</option>
										<option value="EFFECTIVEDATE">Effective Date</option>
										<option value="EXPIREDATE">Expire Date</option>
										<!-- <option value="DIVERSESUPPLIER">Diverse Supplier</option> -->
										<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity
											Classification Notes</option>
									</optgroup>
									<optgroup label="Contact Information">
										<option value="FIRSTNAME">Contact First Name</option>
										<option value="LASTNAME">Contact Last Name</option>
										<option value="DESIGNATION">Title</option>
										<option value="CONTACTPHONE">Contact Phone</option>
										<option value="CONTACTMOBILE">Contact Mobile</option>
										<option value="CONTACTFAX">Contact Fax</option>
										<option value="CONTACTEMAIL">Contact Email</option>
									</optgroup>
									<optgroup label="Business Area">
										<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
										<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
										<option value="BPMARKETSECTOR" disabled="disabled">BP Market Sector</option>
									</optgroup>
									<!-- <optgroup label="Commodity Description">
										<option value="COMMODITYDESCRIPTION">Commodity
											Description</option>
									</optgroup> -->
									<optgroup label="Service Area">
										<option value="KEYWORD" disabled="disabled">Keyword</option>
									</optgroup>
									<optgroup label = "Contact Meeting Information">
										<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
										<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
										<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
										<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
										<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
									</optgroup>
							</select></td>
							<td><input type="text" class="main-text-box"
								name="searchFieldsValue" style="width: 95%;"  /></td>
							<td><input type="hidden"name="searchFields" id="hiddenSearchFields13" value="REGION" /></td>
						</tr>
						<tr>
							<logic:present name="userDetails" property="workflowConfiguration">
								<logic:equal value="1" name="userDetails" property="workflowConfiguration.bpSegment">
									<logic:iterate id="privilege1" name="privileges">
										<logic:equal value="BP Segment" name="privilege1" property="objectId.objectName">
											<logic:equal value="1" name="privilege1" property="view">
												<td><select name="searchFields" id="searchFields14"
														onchange="changeInputType(15,this)" class="chosen-select fields" disabled="disabled">
														<option value=""></option>
														<optgroup label="Company Information">
															<option value="LEGALCOMPANYNAME" disabled="disabled">Legal Company Name</option>
															<!-- <option value="COMPANYCODE" disabled="disabled">Company Code</option> -->
															<option value="DUNSNUMBER">Duns Number</option>
															<option value="TAXID">Tax ID</option>
															<option value="NUMBEROFEMPLOYEES">Number of Employees</option>
															<option value="COMPANYTYPE">Company Type</option>
															<option value="YEARESTABLISHED">Year Established</option>
															<option value="ADDRESS">Address</option>
															<option value="CITY">City</option>
															<option value="STATE" disabled="disabled">State</option>
															<option value="PROVINCE">Province</option>
															<option value="REGION">Region</option>
															<option value="COUNTRY" disabled="disabled">Country</option>
															<option value="ZIPCODE">Zip Code</option>
															<option value="MOBILE">Mobile</option>
															<option value="PHONE">Phone</option>
															<option value="FAX">Fax</option>
															<option value="WEBSITEURL">Website URL</option>
															<option value="EMAIL">Email</option>
															<option value="VENDORCLASSIFICATION" disabled="disabled">Vendor Classification</option>
															<option value="CERTIFICATIONTYPE" disabled="disabled">Certification Type</option>
															<option value = "BPSEGMENT" selected="selected">BP Segment</option>
														</optgroup>
														<optgroup label="NAICS">
															<!-- <option value="NAICSCODE"  disabled="disabled">NAICS Code</option> -->
															<option value="NAICSDESC" disabled="disabled">NAICS Desc</option>
														</optgroup>
														<optgroup label="Diverse Classification">
															<option value="CAPABILITIES" disabled="disabled">Capabilities</option>
															<option value="DIVERSECLASSIFICATION" disabled="disabled">Diverse Classification</option>
															<option value="CERTIFYINGAGENCY" disabled="disabled">Certifying Agency</option>
															<option value="CERTIFICATION">Certification #</option>
															<option value="EFFECTIVEDATE">Effective Date</option>
															<option value="EXPIREDATE">Expire Date</option>
															<!-- <option value="DIVERSESUPPLIER" disabled="disabled">Diverse Supplier</option> -->
															<option value="DIVERSITYCLASSIFICATIONNOTES">Diversity Classification Notes</option>
														</optgroup>
														<optgroup label="Contact Information">
															<option value="FIRSTNAME" disabled="disabled">Contact First Name</option>
															<option value="LASTNAME" disabled="disabled">Contact Last Name</option>
															<option value="DESIGNATION">Title</option>
															<option value="CONTACTPHONE">Contact Phone</option>
															<option value="CONTACTMOBILE">Contact Mobile</option>
															<option value="CONTACTFAX">Contact Fax</option>
															<option value="CONTACTEMAIL">Contact Email</option>
														</optgroup>
														<optgroup label="Business Area">
															<option value="BUSINESSAREA" disabled="disabled">Business Area</option>
															<option value="VENDORCOMMODITY" disabled="disabled">BP Commodity</option>
														</optgroup>
														<!-- <optgroup label="Commodity Description">
															<option value="COMMODITYDESCRIPTION" disabled="disabled">Commodity Description</option>
														</optgroup> -->
														<optgroup label="Service Area">
															<option value="KEYWORD" disabled="disabled">Keyword</option>
														</optgroup>
														<optgroup label = "Contact Meeting Information">
															<option value = "CONTACTFIRSTNAME" disabled="disabled">First Name</option>
															<option value = "CONTACTLASTNAME" disabled="disabled">Last Name</option>
															<option value = "CONTACTDATE" disabled="disabled">Contact Date</option>
															<option value = "CONTACTSTATE" disabled="disabled">Contact State</option>
															<option value = "CONTACTEVENT" disabled="disabled">Contact Event</option>
														</optgroup>
												</select></td>
												<td><input type="hidden" class="main-text-box"
													name="searchFieldsValue" value="" id="bpSegmentHiddenId" /> <select
													name="bpSegmentIds" class="chosen-select-deselect"
													style="width: 350px;" multiple="multiple"
													data-placeholder=" "
													onchange="assignValue(this,'bpSegmentHiddenId');">
														<option value=""></option>
														<logic:present name="bpSegmentSearchList">
															<bean:size id="size" name="bpSegmentSearchList" />
															<logic:greaterEqual value="0" name="size">
																<logic:iterate id="bpSegmentSearchListId"
																	name="bpSegmentSearchList">
																	<bean:define id="name" name="bpSegmentSearchListId"
																		property="id"></bean:define>
																	<option value="<%=name.toString()%>"><bean:write
																			name="bpSegmentSearchListId" property="segmentName" /></option>
																</logic:iterate>
															</logic:greaterEqual>
														</logic:present>
												</select></td>
												<td><input type="hidden"name="searchFields" id="hiddenSearchFields14" value="BPSEGMENT" /></td>
											</logic:equal>
										</logic:equal>
									</logic:iterate>									
								</logic:equal>
							</logic:present>
						</tr>
					</table>
					<br />
					<div style="display: none;">
						<select id="diverseCertificateNames">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="certificateTypes">
								<bean:size id="size" name="certificateTypes" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="certificate" name="certificateTypes">
										<bean:define id="id" name="certificate" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="certificate" property="certificateName"></bean:write></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select name="country" id="country">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="searchVendorForm" property="countries">
								<bean:size id="size" property="countries"
									name="searchVendorForm" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="country" property="countries"
										name="searchVendorForm">
										<bean:define id="name" name="country" property="id"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="country" property="countryname" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="certifyingAgencies">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="searchVendorForm"
								property="certifyingAgencies">
								<bean:size id="size" property="certifyingAgencies"
									name="searchVendorForm" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="agency" property="certifyingAgencies"
										name="searchVendorForm">
										<bean:define id="id" name="agency" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="agency" property="agencyName" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> <select id="businessAreaName">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="businessAreas">
								<bean:size id="size" name="businessAreas" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="businessArea" name="businessAreas">
										<bean:define id="bGroup" name="businessArea"
											property="businessGroup"></bean:define>
										<bean:define id="barea" name="businessArea"
											property="serviceArea"></bean:define>
										<optgroup label="<%=bGroup.toString()%>">

											<%
												String[] areas = barea.toString()
																					.split("\\|");
																			if (areas != null && areas.length != 0) {
																				for (int i = 0; i < areas.length; i++) {
																					String[] parts = areas[i]
																							.split("-");
																					String areaId = parts[0];
																					String areaName = parts[1];
											%>
											<option value="<%=areaId%>">
												<%=areaName%>
											</option>
											<%
												}
																			}
											%>
										</optgroup>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select>
						<select id="commodityDescription">
							<option value=""></option>
							<option value="">All</option>
							<logic:present name="commodityDescription">
								<bean:size id="size" name="commodityDescription" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="commodity" name="commodityDescription">
										<bean:define id="category" name="commodity"
											property="commodityCategory"></bean:define>
										<bean:define id="description" name="commodity"
											property="commodityDescription"></bean:define>
										<optgroup label="<%=category.toString()%>">
											<%
												String[] descriptions = description
																					.toString().split("\\|");
																			if (descriptions != null
																					&& descriptions.length != 0) {
																				for (int i = 0; i < descriptions.length; i++) {
																					String[] parts = descriptions[i]
																							.split(":");
																					String descriptionId = parts[0];
																					String descriptionName = parts[1];
											%>
											<option value="<%=descriptionId%>">
												<%=descriptionName%>
											</option>
											<%
												}
																			}
											%>

										</optgroup>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select>
						<input type="text" class="main-text-box" id="naicsCode" name="naicsCode" placeholder=" " onchange="assignValue(this,'naicsCode1');"/>
						<input type="text" class="main-text-box" id="naicsDescription" name="naicsDescription" placeholder=" " onchange="assignValue(this,'naicsDescription1');"/>
						<%-- <select id="naicsCode">
							<option value=""></option>
							<logic:present name="naicsCodeList">
								<bean:size id="size" name="naicsCodeList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="naicsCodes" name="naicsCodeList">
										<bean:define id="name" name="naicsCodes" property="naicsCode"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="naicsCodes" property="naicsCode" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select><select id="naicsDescription">
							<option value=""></option>
							<logic:present name="naicsCodeList">
								<bean:size id="size" name="naicsCodeList" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="naicsCodes" name="naicsCodeList">
										<bean:define id="name" name="naicsCodes"
											property="naicsDescription"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="naicsCodes" property="naicsDescription" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select> --%>
						<select id="vendorCommodity">
							<option value=""></option>
							<logic:present name="vendorCommodity">
								<bean:size id="size" name="vendorCommodity" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="commodity" name="vendorCommodity">										
										<bean:define id="subSectorDesc" name="commodity" property="subSectorDescription"/>
										<bean:define id="commodityDesc" name="commodity" property="commodityDescription"/>
										<optgroup label="<%=subSectorDesc.toString()%>">
											<%
												String[] descriptions = commodityDesc.toString().split("\\|");
												if (descriptions != null && descriptions.length != 0) 
												{
													for (int i = 0; i < descriptions.length; i++) 
													{
														String[] parts = descriptions[i].split(":");
														String commodityDescId = parts[0];
														String commodityDescName = parts[1];
											%>
											<option value="<%=commodityDescId%>"><%=commodityDescName%></option>
											<%
													}
												}
											%>
										</optgroup>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select>
						<select id="certificationType">
							<option value=""></option>
							<logic:present name="certificationTypes">
								<bean:size id="size" name="certificationTypes" />
								<logic:greaterEqual value="0" name="size">
									<logic:iterate id="certificationType" name="certificationTypes">
										<bean:define id="name" name="certificationType"
											property="certficateTypeId"></bean:define>
										<option value="<%=name.toString()%>"><bean:write
												name="certificationType" property="certficateTypeDescription" /></option>
									</logic:iterate>
								</logic:greaterEqual>
							</logic:present>
						</select><select id="diversesupplier1">
									<option value=""></option>
									<option value="1">Diverse</option>
									<option value="0">Non Diverse</option>
							</select>
							<select id="contactStates">
								<option value=""></option>
								<logic:present name="contactStates">
									<bean:size id="size" name="contactStates" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="contactState" name="contactStates">
											<bean:define id="name" name="contactState" property="id"/>
											<option value="<%=name.toString()%>"><bean:write name="contactState" property="statename"/></option>
										</logic:iterate>
									</logic:greaterEqual>
								</logic:present>
							</select>
							<select id="bpSegmentIds">
								<option value=""></option>
								<logic:present name="bpSegmentSearchList">
									<bean:size id="size1" name="bpSegmentSearchList" />
									<logic:greaterEqual value="0" name="size1">
										<logic:iterate id="bpSegmentSearchListId" name="bpSegmentSearchList">
											<bean:define id="name" name="bpSegmentSearchListId" property="id"/>
											<option value="<%=name.toString()%>"><bean:write name="bpSegmentSearchListId" property="segmentName" /></option>
										</logic:iterate>
									</logic:greaterEqual>
								</logic:present>
							</select>
							<select id = "bpMarketSectorId">
								<option value=""></option>
								<logic:present name="marketSectorsList">
									<bean:size id="size" name="marketSectorsList" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="marketSector" name="marketSectorsList">
											<bean:define id="sectorId" name="marketSector" property="id"/>
											<option value="<%=sectorId.toString()%>"><bean:write name="marketSector" property="sectorDescription"/></option>
										</logic:iterate>
									</logic:greaterEqual>
								</logic:present>
							</select>
					</div>
					<div class="clear"></div>
					<INPUT id="cmd1" type="button" value="Other Search Criteria" class="btn btn-primary btn-other-search"
						onclick="addRow('searchTable')" />
					<%-- <div class="clear"></div>
		Choose Fields to display and export: <select name="exportFields"
						id="searchFields11" multiple>
						<logic:present name="searchVendorForm" property="exportMasters">
							<logic:greaterEqual value="0" name="searchVendorForm" property="exportMasters">
								<logic:iterate id="area" name="searchVendorForm" property="exportMasters">
									<bean:define id="category" name="area" property="businessGroup"></bean:define>
									<bean:define id="exportFields" name="area" property="serviceArea"></bean:define>
									<optgroup label="<%=category.toString()%>">
										<%
											String[] field = exportFields.toString().split("\\|");
											if (field != null && field.length != 0) {
												for (int i = 0; i < field.length; i++) {
													String[] parts = field[i]
															.split("-");
													String fieldId = parts[0];
													String fieldName = parts[1];
										%>
										<option value="<%=fieldId%>"><%=fieldName%></option>
										<%
											}
										}
										%>
									</optgroup>
								</logic:iterate>
							</logic:greaterEqual>
						</logic:present>
					</select> --%>
					<div class="clear"></div>
					<footer class="card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
								<input type="button" value="Search" class="btn btn-tertiary" onclick="searchReport();">
								<input type="button" value="Customize Columns" class="btn btn-primary" onclick="customizeColumns();">
							</div>
						</div>
					</footer>
				</html:form>
			</div>
		</div>
	</section>
</div>
</div>

			<script type="text/javascript">
				$(document).keypress(function(e) {
					if (e.which == 13) {
						$(e.target).blur();
						e.preventDefault();
						return searchReport();
					}
				});
				
				function initDatePicker() {
					$(".datePicker").datepicker({
						changeYear : true,
						dateFormat : 'mm/dd/yy'
					});
				}
				
				$(document).ajaxError(function() {
					$("#ajaxloader").hide();
				});
				/**
				 * Add a new row
				 */
				/**
				 * Add a new row
				 */
				function addRow(tableID) {
					var table = document.getElementById(tableID);
					var rowCount = table.rows.length;
					var row = table.insertRow(rowCount);
					var colCount = table.rows[1].cells.length;
					for ( var i = 0; i < colCount; i++) {
						var newcell = row.insertCell(i);
						newcell.innerHTML = table.rows[2].cells[i].innerHTML;

						switch (newcell.childNodes[1].type) {
						case "text":
							newcell.innerHTML = " <input class='main-text-box ' name='searchFieldsValue'/>";
							newcell.childNodes[0].value = "";
							break;
						case "select-one":
							newcell.innerHTML = '<select name="searchFields" id="searchFields'
									+ rowCount
									+ '" class="chosen-select fields" onchange="changeInputType('
									+ rowCount
									+ ',this)" >'
									+ $('#searchFields1').html() + '</select>';
							newcell.childNodes[0].selectedIndex = 0;
							$("#searchFields" + rowCount).chosen({
								width : "96%"
							});
							var values = $(".chosen-select").map(function() {
								return this.value;
							}).get();

							$('select[id=searchFields' + rowCount + '] option')
									.each(
											function() {
												for ( var index = 0; index < values.length; index++)
													if ($(this).val() == values[index])
														$(this).attr(
																'disabled',
																'disabled');
											});
							break;
						}
						$("#searchFields" + rowCount).trigger("chosen:updated");
						//$("#searchFields"+rowCount).trigger("liszt:updated");
					}
				}

				/**
				 * used to change the input type when select the field
				 */
				function changeInputType(rowNo, value) {

					$('select[id=' + value.id + '] option').each(function() {

						$(this).removeAttr('disabled');

					});

					$("#" + value.id).trigger("chosen:updated");

					var values = $(".fields").map(function() {

						return this.value;
					}).get()

					$('select[id=' + value.id + '] option').each(function() {

						for ( var index = 0; index < values.length; index++)
							if ($(this).val() == values[index])
								$(this).attr('disabled', 'disabled');

					});
					$("#" + value.id).trigger("chosen:updated");
					var table = document.getElementById('searchTable');

					if (value.value == 'DIVERSESUPPLIER') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="diversesupplier1" name="searchFieldsValue" value="">'
								+'<select name="searchFieldsValue" style="width:265px;" class="chosen-single chosen-default"'+
								'data-placeholder=" "   >'
								+$('#diversesupplier1').html()+'</select>';
//					 			chosenConfig();
								$("#diversesupplier1").next().chosen();
								$("#diversesupplier1").next().trigger("chosen:updated");
					} else if (value.value == 'COUNTRY') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="selectCountries1" name="searchFieldsValue" value="">'
								+ '<select name="selectCountries" style="width:265px;" data-placeholder=" " '
								//+ 'multiple class="chosen-select-deselect" onchange="assignValue(this,\'selectCountries1\');"> '
								+ 'class="chosen-select" onchange="changestate(this,\'state\');assignValue(this,\'selectCountries1\');"> '
								//+ ' class="chosen-select-deselect" disabled="disabled"> <option value="1"> United States</option> '
								+ $('#country').html() + '</select>';
			 			//chosenConfig();						
						$("#selectCountries1").next().chosen();
						$("#selectCountries1").next().trigger("chosen:updated");
					} else if (value.value == 'DIVERSECLASSIFICATION') {

						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="diverseCertificate1"	name="searchFieldsValue" value=""/>'
								+ '<select name="diverseCertificateNames" style="width:265px;" multiple="multiple" class="chosen-select-deselect"'
								+ ' data-placeholder=" " onchange="assignValue(this,\'diverseCertificate1\');">'
								+ $('#diverseCertificateNames').html() + '</select>';
//			 			chosenConfig();
						$("#diverseCertificate1").next().chosen();
						$("#diverseCertificate1").next().trigger("chosen:updated");

					} else if (value.value == 'CERTIFYINGAGENCY') {

						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="certifyAgencies1"/>'
								+ '<select name="certifyAgencies" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
								+ 'data-placeholder=" " onchange="assignValue(this,\'certifyAgencies1\');">'
								+ $('#certifyingAgencies').html() + '</select>';
//			 			chosenConfig();
						$("#certifyAgencies1").next().chosen();
						$("#certifyAgencies1").next().trigger("chosen:updated");

					} else if (value.value == 'BUSINESSAREA') {

						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="businessArea1"/>'
								+ '<select name="businessAreaName" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
								+ 'data-placeholder=" " onchange="assignValue(this,\'businessArea1\');">'
								+ $('#businessAreaName').html() + '</select>';
//			 			chosenConfig();
						$("#businessArea1").next().chosen();
						$("#businessArea1").next().trigger("chosen:updated");

					} else if (value.value == 'COMMODITYDESCRIPTION') {

						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="commodityDescription1"/>'
								+ '<select name="commodityDescription" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
								+ 'data-placeholder=" " onchange="assignValue(this,\'commodityDescription1\');">'
								+ $('#commodityDescription').html() + '</select>';
//			 			chosenConfig();
						$("#commodityDescription1").next().chosen();
						$("#commodityDescription1").next().trigger("chosen:updated");

					} else if (value.value == 'NAICSCODE') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsCode1"/>'
							+ '<input type="text" id="naicsCode" class="main-text-box" name="naicsCode" placeholder=" " onchange="assignValue(this,\'naicsCode1\');" />'
							+ $('#naicsCode').html();
						callAutoCompleteForNaicsCode();
						
						/* table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsCode1"/>'
								+ '<select name="naicsCode" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
								+ 'data-placeholder=" " onchange="assignValue(this,\'naicsCode1\');">'
								+ $('#naicsCode').html() + '</select>';
			 			//chosenConfig();
						$("#naicsCode1").next().chosen();
						$("#naicsCode1").next().trigger("chosen:updated"); */						
					} else if (value.value == 'NAICSDESC') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsDescription1"/>'
							+ '<input type="text" id="naicsDescription" class="main-text-box" name="naicsDescription" placeholder=" " onchange="assignValue(this,\'naicsDescription1\');" />'
							+ $('#naicsDescription').html();
						callAutoCompleteForNaicsDesc();
						
						/* table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="naicsDescription1"/>'
								+ '<select name="naicsDescription" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
								+ 'data-placeholder=" " onchange="assignValue(this,\'naicsDescription1\');">'
								+ $('#naicsDescription').html() + '</select>';
			 			//chosenConfig();
						$("#naicsDescription1").next().chosen();
						$("#naicsDescription1").next().trigger("chosen:updated"); */
					} else if (value.value == 'CERTIFICATIONTYPE') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="certificatioType1"/>'
								+ '<select name="certificationType" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
								+ 'data-placeholder=" " onchange="assignValue(this,\'certificatioType1\');">'
								+ $('#certificationType').html() + '</select>';
//			 			chosenConfig();
						$("#certificatioType1").next().chosen();
						$("#certificatioType1").next().trigger("chosen:updated");
					} else if (value.value == 'VENDORCOMMODITY') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="vendorCommodity6"/>'
							+ '<select name="vendorCommodity" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'vendorCommodity6\');">'
							+ $('#vendorCommodity').html() + '</select>';
//						chosenConfig();
						$("#vendorCommodity6").next().chosen();
						$("#vendorCommodity6").next().trigger("chosen:updated");
					} else if (value.value == 'CONTACTSTATE') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box" id="contactStates" name="searchFieldsValue" value="">'
							+'<select name="contactState" style="width:265px;" class="chosen-single chosen-default"'+
							'data-placeholder=" "  onchange="assignValue(this,\'contactStates\');"  >'
							+$('#contactStates').html()+'</select>';
							$("#contactStates").next().chosen();
							$("#contactStates").next().trigger("chosen:updated");
					} else if (value.value == 'CONTACTDATE') {
						table.rows[rowNo].cells[1].innerHTML = "<input name='searchFieldsValue' class='main-text-box datePicker' id='contactDate' alt='Please click to select date' readonly='readonly'/>";
						initDatePicker();
					} else if (value.value == 'BPSEGMENT') {				
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpSegmentId"/>'
							+ '<select name="bpSegmentIds" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'bpSegmentId\');">'
							+ $('#bpSegmentIds').html() + '</select>';
	 					//chosenConfig();
						$("#bpSegmentId").next().chosen();
						$("#bpSegmentId").next().trigger("chosen:updated");
					} else if (value.value == 'BPMARKETSECTOR') {
						table.rows[rowNo].cells[1].innerHTML = '<input type="hidden" class="main-text-box"	name="searchFieldsValue" value="" id="bpMarketSectorDynamicId"/>'
							+ '<select name="bpMarketSectorId" multiple="multiple" class="chosen-select-deselect" style="width:265px;"'
							+ 'data-placeholder=" " onchange="assignValue(this,\'bpMarketSectorDynamicId\');">'
							+ $('#bpMarketSectorId').html() + '</select>';
						//chosenConfig();
						$("#bpMarketSectorDynamicId").next().chosen();
						$("#bpMarketSectorDynamicId").next().trigger("chosen:updated");
					} else {
						table.rows[rowNo].cells[1].innerHTML = "<input class='main-text-box ' name='searchFieldsValue'/>";
					}

				}
			</script>
			<div class="clear"></div>
			<section class="card col-lg-12 mx-auto col-sm-12 col-md-12 col-xl-12 col-xs-12">
				<header class="">
					<div id="resultHeading" class="card-title"  style="display: none;">
							<img src="images/icon-registration.png" />&nbsp;&nbsp;Following are
						your Vendors
						<div id="export" style="display: none; float: right;">
							<input type="button" class="btn btn-tertiary" value="Export" id="export"
								onclick="exportGrid();">
						</div>
					</div>
				</header>
			</section>
			<div id="dynamicresultHeading" class="page-title" style="display: none;">
				<img src="images/icon-registration.png" />&nbsp;&nbsp;Following are
				your Vendors
				<div id="dynamicExport" style="display: none; float: right;">
					<input type="button" class="btn" value="Export" id="export"
						onclick="dynamicExport();">
				</div>
			</div>
			<div class="view-reports-table">
		<div class="form-box card-body">
	
				<div id="grid_container">
					<div class="responsiveGrid">
						<table id="gridtable" class="table table-bordered table-striped mb-0" style="width:100%;">
							<tbody>
								<tr>
									<td />
								</tr>
							</tbody>
						</table>
					
						<div id="pager"></div>
					</div>
				</div>
		</div>
	</div>
		
	<div id="dialog" style="display: none;" title="Choose Export Type">
				<p>Please Choose the Export Type</p>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 1%;"><input type="radio" name="export"
							value="1" id="excel" /></td>
						<td><label for="excel"><img id="excelExport"
								src="images/excel_export.png" />Excel</label></td>
						<td style="padding: 1%;"><input type="radio" name="export"
							value="2" id="csv" /> <input type="hidden" name="fileName"
							id="fileName" value="SupplierReport" /></td>
						<td><label for="csv"><img id="csvExport"
								src="images/csv_export.png" />CSV</label></td>
						<td style="padding: 1%;"><input type="radio" name="export"
							value="3" id="pdf" /></td>
						<td><label for="pdf"><img id="pdfExport"
								src="images/pdf_export.png" />PDF</label></td>
					</tr>
				</table>
				<div class="wrapper-btn text-center">
					<logic:present name="userDetails">
						<bean:define id="logoPath" name="userDetails"
							property="settings.logoPath"></bean:define>
						<input type="button" value="Export" class="exportBtn btn btn-primary"
							onclick="exportHelper('gridtable','<%=logoPath%>');">
					</logic:present>
				</div>
			</div>
			<div id="backToSearch" class="wrapper-btn" style="display: none;">
				 <input type="button"  class="btn btn-primary mt-3" value="Back To Search" id="buttonClass"onclick="backToSearch();">
			</div>
		

<div id="dialog1" title="Choose Previous Search Filters" style="display: none;">
	<logic:present name="previousSearchList">
		<table class="main-table">
			<bean:size id="size" name="previousSearchList" />
			<logic:greaterThan value="0" name="size">
				<thead>
					<tr>
						<td class="header">Search Name</td>
						<td class="header">Search Date</td>
						<td class="header">Criteria</td>
						<td class="header">Actions</td>
					</tr>
				</thead>
				<fmt:setLocale value="en_US"/>
				<logic:iterate name="previousSearchList" id="previousSearch">
				<bean:define id="previousSearchId" name="previousSearch" property="id"></bean:define>
					<tbody>
						<tr>
							<td><html:link href="Javascript:void ( 0 ) ;" onclick="searchByPreviousSavedColumns(${previousSearchId});">
										<bean:write name="previousSearch" property="searchName" />
							</html:link>
							</td>
							<td><bean:write name="previousSearch" property="searchDate"  format="yyyy-MM-dd hh:mma"/></td>
							<td><bean:write name="previousSearch" property="criteria" /></td>
							
							<td><html:link href="Javascript:void ( 0 ) ;"  onclick="deleteSearchFilter(${previousSearchId});">Delete</html:link>
							</td>
						</tr>
					</tbody>
				</logic:iterate>
			</logic:greaterThan>
			<logic:equal value="0" name="size">
					No such Records
			</logic:equal>
		</table>
	</logic:present>
	<logic:notPresent name="previousSearchList">
		<p>There is no record(s) available.</p>
	</logic:notPresent>
</div>

<div id="dialog2" style="display: none;" title="Choose Export Type">
		<p>Please Choose the Export Type</p>
		<table style="width: 100%;">
			<tr>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="1" id="excel" /></td>
				<td><label for="excel"><img id="excelExport"
						src="images/excel_export.png" />Excel</label></td>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="2" id="csv" /> <input type="hidden" name="fileName"
					id="fileName" value="SupplierReport" /></td>
				<td><label for="csv"><img id="csvExport"
						src="images/csv_export.png" />CSV</label></td>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="3" id="pdf" /></td>
				<td><label for="pdf"><img id="pdfExport"
						src="images/pdf_export.png" />PDF</label></td>
			</tr>
		</table>
		<div class="wrapper-btn text-center">
			<logic:present name="userDetails">
				<bean:define id="logoPath" name="userDetails"
					property="settings.logoPath"></bean:define>
				<input type="button" value="Export" class="exportBtn btn btn-primary"
					onclick="exportHelperForDynamic('gridtable','<%=logoPath%>');">
			</logic:present>
		</div>
	</div>
	</section>
	
			<script type="text/javascript">
				chosenConfig();
				function chosenConfig() {
					var config = {
						'.chosen-select' : {
							width : "95%"
						},
						'.chosen-select-deselect' : {
							allow_single_deselect : true,
							width : "95%"
						},
						'.chosen-select-no-single' : {
							disable_search_threshold : 10
						},
						'.chosen-select-no-results' : {
							no_results_text : 'Oops, nothing found!'
						},
						'.chosen-select-width' : {
							width : "95%"
						}

					}
					for ( var selector in config) {
						$(selector).chosen(config[selector]);
					}
				}
			</script>
			<script>
				function showStatusDesc(statusId) {
					var status = statusId;
					var statusdesc = "";
					<logic:present  property="vendorStatusList"  name="searchVendorForm">
					<bean:size id="size" property="vendorStatusList" name="searchVendorForm" />
					<logic:greaterEqual value="0" name="size">
					<logic:iterate id="vendorStatus" property="vendorStatusList" name="searchVendorForm">
					if (status == '<bean:write name="vendorStatus" property="id" />') {
						statusdesc = '<bean:write name="vendorStatus" property="statusdescription" />';
					}
					</logic:iterate>
					</logic:greaterEqual>
					</logic:present>
					if (statusdesc != "") {
						document.getElementById('statusDescArea').style.display = "block";
						document.getElementById("statusdesc").value = statusdesc;
					} else {
						document.getElementById('statusDescArea').style.display = "hidden";
					}
				}
			</script>
		</logic:equal>
		<logic:equal value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have no rights to view Supplier Custom Search Report.</h3>
			</div>
		</logic:equal>
	</logic:equal>
</logic:iterate>

<script>
function callAutoCompleteForNaicsCode()
{
$(function() {
	var items=[];	
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}
$( "#naicsCode" )
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
	}
})
.autocomplete({
source: function( request, response ) {
	var searchCode = extractLast( request.term );
	 $.ajax({
	        url: "viewVendorsStatus.do?method=searchNaicsCode&term="+searchCode,
	        dataType: "json",
	        type: "GET",
	        success: function (data) {
	        	while(items.length>0){
	        		items.pop();
	        	}
	        	for(i=0;i < data.length;i++ )
					{					
					var naicsStr='';
				 naicsStr=naicsStr+data[i];
						items.push(naicsStr);
					}
	            response(items);
	        }
	    });	
	},
	search: function() {
	var term = extractLast( this.value );
	if ( term.length < 2 ) {
			return false;
		}
	},
	focus: function() {
		return false;
	},
	select: function( event, ui ) {
	var terms = split( this.value );
			terms.pop();
		terms.push( ui.item.value );
		terms.push( "" );
	this.value = terms.join( "," );
		return false;
	}
	});
});
}
</script>


<script>
function callAutoCompleteForNaicsDesc()
{
$(function() {
	var items=[];
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}
$( "#naicsDescription" )	
	.bind( "keydown", function( event ) {
	if ( event.keyCode === $.ui.keyCode.TAB &&
	$( this ).data( "ui-autocomplete" ).menu.active ) {
	event.preventDefault();
	}
})
.autocomplete({
source: function( request, response ) {
	var searchCode = extractLast( request.term );
	 $.ajax({
	        url: "viewVendorsStatus.do?method=searchNaicsDescription&term="+searchCode,
	        dataType: "text",
	        type: "GET",
	        success: function (data) {
	        	while(items.length>0){
	        		items.pop();
	        	}
	        	var naicsData=data.split(',');
					for(i=0;i <naicsData.length;i++ )
					{
						var naicsDesc='';
						naicsDesc=naicsData[i];
						 if(i==0)
							 naicsDesc= naicsDesc.replace('[',' ');
						 if(i==naicsData.length-1)
							 naicsDesc= naicsDesc.replace(']',' ');
						if(naicsDesc.charAt(0) === ' '){
							naicsDesc = naicsDesc.substr(1);
						}
						items.push(naicsDesc);
					}
	            response(items);
	        }
	    });	
	},
	search: function() {
		var term = extractLast( this.value );
		if ( term.length < 2 ) {
		return false;
		}
	},
	focus: function() {
		return false;
	},
	select: function( event, ui ) {
		var terms = split( this.value );
		terms.pop();
	terms.push( ui.item.value );
		terms.push( "" );
		this.value = terms.join( "," );
	return false;
	}
	});
});
}
</script>

