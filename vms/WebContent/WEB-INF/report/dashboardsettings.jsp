<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="js/custom-form-elements.js"></script>
<link href="css/form.css" type="text/css" rel="stylesheet"></link>

<div id="successMsg">
	<html:messages id="msg" property="successMsg" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div id="failureMsg">
	<html:messages id="msg" property="failureMsg" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Dashboard Configuration
</div>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Dashboard Settings" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">
		<logic:match value="1" name="privilege" property="modify">
			<div class="form-box">
				<html:form
					action="/updatedashboardsettings?method=saveDashboardSettings"
					styleId="dashboardForm">

					<html:javascript formName="tier2ReportForm" />
					<html:hidden property="dashboardId"></html:hidden>
					<b>Choose dashboard to view</b>
					<table style="padding: 2%; width: 95%;" class="main-table">
						<tr></tr>
						<tr>
							<td style="border: none;"><html:checkbox
									property="diversityDashboard" styleId="diversityDashboard"
									styleClass="styled">Diversity Analysis</html:checkbox></td>
							<td style="border: none;"><html:checkbox
									property="directSpend" styleId="directSpend"
									styleClass="styled">Supplier by Direct Report</html:checkbox></td>
						</tr>
						<tr></tr>
						<tr>
							<td style="border: none;"><html:checkbox
									property="vendorStatus" styleId="vendorStatus"
									styleClass="styled">Vendor Status Breakdown</html:checkbox></td>
							<td style="border: none;"><html:checkbox
									property="indirectSpend" styleId="indirectSpend"
									styleClass="styled">Supplier by Indirect Report</html:checkbox></td>
						</tr>
						<tr></tr>
						<tr>
							<td style="border: none;"><html:checkbox
									property="supplierDashboard" styleId="supplierDashboard"
									styleClass="styled">Supplier Count by State</html:checkbox></td>
							<td style="border: none;"><html:checkbox
									property="diversityStatus" styleId="diversityStatus"
									styleClass="styled">Tier 2 Vendor Name Report</html:checkbox></td>
						</tr>
						<tr></tr>
						<tr>
							<td style="border: none;"><html:checkbox
									property="agencyDashboard" styleId="agencyDashboard"
									styleClass="styled">Prime Supplier Report T2 Spend by Agency</html:checkbox></td>
							<td style="border: none;"><html:checkbox
									property="ethnicityAnalysis" styleId="ethnicityAnalysis"
									styleClass="styled">Tier 2 Reporting Summary</html:checkbox></td>
						</tr>
						<tr></tr>
						<tr>
							<td style="border: none;"><html:checkbox
									property="totalSpend" styleId="totalSpend" styleClass="styled">Supplier by Total Spend Report</html:checkbox></td>
							<td style="border: none;"><html:checkbox
									property="bpVendorsCountDashboard"
									styleId="bpVendorsCountDashboard" styleClass="styled">BP Vendor Count by State</html:checkbox></td>
						</tr>
						<tr></tr>
						<tr>
							<!-- 			Removed Bcoz Client Don't want this					 -->
							<%-- 				<td style="border: none;"><html:checkbox --%>
							<%-- 						property="ethnicityDashboard" styleId="ethnicityDashboard" --%>
							<%-- 						styleClass="styled">Tier2 Reporting Ethnicity Breakdown</html:checkbox></td> --%>
							<%-- 				<td style="border: none;"><html:checkbox --%>
							<%-- 						property="spendDashboard" styleId="spendDashboard" --%>
							<%-- 						styleClass="styled">Spend Data Dashboard</html:checkbox></td> --%>
						</tr>
					</table>

					<div class="wrapper-btn text-center">
						<html:submit value="Save Configuration" styleClass="exportBtn btn btn-primary"
							styleId="savesettings"></html:submit>
					</div>
				</html:form>
			</div>
			</logic:match>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Dashboard Settings" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to view Dashboard Configurations</h3>
			</div>
		</logic:match>
		<logic:match value="0" name="privilege" property="modify">
			<div style="padding: 5%; text-align: center;">
				<h3>You have no rights to modify Dashboard Configurations</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:present name="tier2ReportForm" property="diversityDashboard">
	<logic:equal value="1" name="tier2ReportForm"
		property="diversityDashboard">
		<script>
			$("#diversityDashboard").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="ethnicityDashboard">
	<logic:equal value="1" name="tier2ReportForm"
		property="ethnicityDashboard">
		<script>
			$("#ethnicityDashboard").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="supplierDashboard">
	<logic:equal value="1" name="tier2ReportForm"
		property="supplierDashboard">
		<script>
			$("#supplierDashboard").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="agencyDashboard">
	<logic:equal value="1" name="tier2ReportForm"
		property="agencyDashboard">
		<script>
			$("#agencyDashboard").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="totalSpend">
	<logic:equal value="1" name="tier2ReportForm" property="totalSpend">
		<script>
			$("#totalSpend").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="directSpend">
	<logic:equal value="1" name="tier2ReportForm" property="directSpend">
		<script>
			$("#directSpend").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="indirectSpend">
	<logic:equal value="1" name="tier2ReportForm" property="indirectSpend">
		<script>
			$("#indirectSpend").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="diversityStatus">
	<logic:equal value="1" name="tier2ReportForm"
		property="diversityStatus">
		<script>
			$("#diversityStatus").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="ethnicityAnalysis">
	<logic:equal value="1" name="tier2ReportForm"
		property="ethnicityAnalysis">
		<script>
			$("#ethnicityAnalysis").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="spendDashboard">
	<logic:equal value="1" name="tier2ReportForm" property="spendDashboard">
		<script>
			$("#spendDashboard").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="vendorStatus">
	<logic:equal value="1" name="tier2ReportForm" property="vendorStatus">
		<script>
			$("#vendorStatus").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>
<logic:present name="tier2ReportForm" property="bpVendorsCountDashboard">
	<logic:equal value="1" name="tier2ReportForm" property="bpVendorsCountDashboard">
		<script>
			$("#bpVendorsCountDashboard").attr('checked', true);
		</script>
	</logic:equal>
</logic:present>