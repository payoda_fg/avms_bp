<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Included to Generate Report -->
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Added for Populate JqGrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css" />-->

<!-- Added for Export CSV Files -->
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>

<script>
	$(document).ready(function() 
	{
		//Generate Report
		getVendorCommoditiesNotSavedReport();		
	});
</script>
<section role="main" class="content-body card-margin reports-module">
	<div class="row">
		<div class="col-lg-12 mx-auto col-sm-12 col-md-12 col-xl-12 col-xs-12">
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendor Commodities Not Saved" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="view">
						<!-- Heading Part-->
						<header class="card-header">
							<h2 class="card-title">Vendor Commodities Not Saved Report</h2>
						</header>
						
						<div class="clear"></div>
						
						<!-- Report Table Grid -->			
						<div class="grid-wrapper view-reports-table pt-0">
						<div class="form-box">
							<div class="row">
								<div class="col-lg-12">
									<section class="">
										<div class="card-body">
											<div id="grid_container">
												<div class="responsiveGrid table-full-width">
													<table id="gridtable" class="table table-bordered table-striped mb-0">
														<tr>
															<td/>
														</tr>
													</table>
													<div id="pager"></div>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>				
					</logic:equal>
					<logic:equal value="0" name="privilege" property="view">
						<div class="form-box">
							<h3>You have No Rights to View Vendor Commodities Not Saved Report.</h3>
						</div>
					</logic:equal>		
				</logic:equal>
			</logic:iterate>
</div>
</div>
</section>
<!-- Export Dialog Box -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel">
			</td>
			<td>
				<label for="excel">
					<img id="excelExport" src="images/excel_export.png" />Excel
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"> 
				<input type="hidden" name="fileName" id="fileName" value="VendorCommoditiesNotSavedReport">
			</td>
			<td>
				<label for="csv">
					<img id="csvExport" src="images/csv_export.png" />CSV
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf">
					<img id="pdfExport" src="images/pdf_export.png" />PDF
				</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportVendorCommoditiesNotSavedReportData('gridtable','<%=logoPath%>');">
		</logic:present>
	</div>
</div>