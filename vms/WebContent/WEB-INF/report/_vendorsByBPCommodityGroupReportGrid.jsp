<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("vendorsByBPCommodityGroup") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("vendorsByBPCommodityGroup");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		List<String> sectorList= new ArrayList<String>();
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			String marketSector = "";
			if(!sectorList.contains(tier2ReportDto.getMarketSector())){
				marketSector = tier2ReportDto.getMarketSector();
				sectorList.add(tier2ReportDto.getMarketSector());
			}
			cellobj.put("bpMarketSector", marketSector);
			cellobj.put("bpMarketSubSector", tier2ReportDto.getSubSector());
			cellobj.put("bpCommodityGroup", tier2ReportDto.getCommodityDisecription());
			cellobj.put("vendorCount", new java.text.DecimalFormat("#,###").format(tier2ReportDto.getCount().intValue()));
			cellobj.put("percent", Math.round(tier2ReportDto.getPercentage()));
			
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>