
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>
<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>

<script src="js/dhtmlxChart/dhtmlxChart/codebase/dhtmlxchart.js"
	type="text/javascript"></script>
<link rel="STYLESHEET" type="text/css"
	href="js/dhtmlxChart/dhtmlxChart/codebase/dhtmlxchart.css">
<style>
.dhx_axis_item_x {
	font-size: 11px
}

.dhx_axis_item_y {
	font-size: 11px
}
</style>
<script>
				var barChart;
				window.onload = function() {
					barChart = new dhtmlXChart({
						view : "bar",
						color : "#66ccff",
						gradient : "3d",
						container : "chart_container",
						value : "#data3#",
						label : "#data3#",
						radius : 3,
						tooltip : {
							template : "#data3#"
						},
						width : 50,
						origin : 0,
						yAxis : {
							
							title : "Amount"
						},
						group : {
							by : "#data2#",
							map : {
								data3 : [ "#data3#", "sum" ]
							}
						},
						xAxis : {
							title : "Diverse Classification",
							template : "#id#"
						},
						border : false
					});

					function refresh_chart() {
						barChart.clearAll();
						barChart.parse(mygrid, "dhtmlxgrid");
					};

					mygrid = new dhtmlXGridObject('gridbox');
					
					mygrid.setImagePath("js/common/imgs/");
					mygrid.setSkin("light");
					mygrid.customGroupFormat=function(name,count){
					     return name+"    -  Total Spend Value ";
					};
					mygrid.groupBy(2,["#title","","","#stat_total"]);
					mygrid.loadXML("xml/<%=session.getId()%>/spendData.xml", refresh_chart);
					mygrid.attachEvent("onEditCell", function(stage) {
						if (stage == 2)
							refresh_chart();
						return true;
					});
				};
			</script>
		<logic:present property="status" name="searchVendorForm">
			<logic:equal value="success" property="status" name="searchVendorForm">
				<div id="gridbox"></div>
				<div id="chart_container"></div>
			</logic:equal>
			<logic:notEqual value="success" property="status"
				name="searchVendorForm">
				<table class="main-table table table-bordered table-striped mb-0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td>No Spend Report Data</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					</tbody>
				</table>
			</logic:notEqual>
		</logic:present>

	</div>
</div>