<%@page import="java.util.*"%>
<%
	ArrayList<String> periods = null;
	if (session.getAttribute("reportPeriod") != null) {
		periods = (ArrayList<String>) session
				.getAttribute("reportPeriod");
	}
	if (periods != null) {
		int i = 1;
		out.println("<option value='" + 0 + "'>-- Select --</option>");
		for (String period : periods) {
			out.println("<option value='" + period + "'>" + period
					+ "</option>");
			i++;
		}
	}
%>

