<%@page import="com.fg.vms.util.CommonUtils"%>
<%@page import="java.util.Date"%>
<%@page import="com.fg.vms.admin.dto.UserDetailsDto"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<%
	
	UserDetailsDto applicationSettings = (UserDetailsDto) session
			.getAttribute("userDetails");
	String fromDate = "";
	String endDate = "";
	if (applicationSettings.getSettings() != null) {
		fromDate = CommonUtils.convertDateToString(applicationSettings
				.getSettings().getFiscalStartDate());
		endDate = CommonUtils.convertDateToString(applicationSettings
				.getSettings().getFiscalEndDate());

	}
	String path="";
	if(session.getAttribute("path")!=null){
		path=session.getAttribute("path").toString();
	}
%>
<script src="js/report.js" type="text/javascript"></script>
<script type="text/javascript">	

function validate() {

	if (document.getElementById("byOption1").checked
			|| document.getElementById("byOption2").checked
			|| document.getElementById("byOption3").checked
			|| document.getElementById("byOption4").checked) {

		if (document.getElementById("byOption1").checked) {
			if (document.getElementById("supplier").value == 0) {
				alert("Kindly select the supplier.");
				return false;
			}
			return true;
		}
		if (document.getElementById("byOption2").checked) {
			if ((document.getElementById("startDate").value).length != 0
					&& (document.getElementById("endDate").value).length != 0) {
				var startDate = new Date($('#startDate').val());
				var endDate = new Date($('#endDate').val());

				if (startDate >= endDate) {
					alert("Start date must be less then end date.");
					return false;
				}
				return true;
			} else {
				alert("Must be enter the start date and end date.");
				return false;
			}

		}
		if (document.getElementById("byOption3").checked) {
			if ((document.getElementById("year").value).length == 0) {
				alert("Must be enter the year.");
				return false;
			}
			return true;
		}
		if (document.getElementById("byOption4").checked) {
			return true;
		}
	} else {
		alert("Kindly select the option to generate report");
		return false;
	}

}


function checkFiscalFromDate() {
	var startDate = new Date($('#startDate').val());
	var fiscalStartDate = new Date('<%=fromDate%>');
	var fiscalStartDate1 = new Date('<%=fromDate%>');
	var fiscalEndDate = new Date('<%=endDate%>');
	var temp = fiscalStartDate;
	var flag = false;
	var flag1 = false;
	var dates=temp.getMonth()+1+"/"+temp.getDate()+"/"+temp.getFullYear()+"\n";
	for (; fiscalEndDate > temp;) {
		
		if (temp.getTime() == startDate.getTime()) {
			flag = true;
			break;
		}
		if (fiscalStartDate1.getDate() == 31) {
			if ((temp.getMonth() + 3) > 11) {
				temp.setFullYear(temp.getFullYear() + 1,
						(temp.getMonth() + 3) % 2, temp.getDate());
			} else if ((temp.getMonth() + 3) == 0 || (temp.getMonth() + 3) == 2
					|| (temp.getMonth() + 3) == 4 || (temp.getMonth() + 3) == 6
					|| (temp.getMonth() + 3) == 7 || (temp.getMonth() + 3) == 9
					|| (temp.getMonth() + 3) == 11) {

				temp.setMonth(temp.getMonth() + 3);
				if (flag1 == true) {
					temp.setDate(temp.getDate() + 1);
					flag1 = false;
				}
			} else if ((temp.getMonth() + 3) == 3 || (temp.getMonth() + 3) == 5
					|| (temp.getMonth() + 3) == 8
					|| (temp.getMonth() + 3) == 10) {
				temp.setDate(temp.getDate() - 1);
				temp.setMonth(temp.getMonth() + 3);
				flag1 = true;
			}
		} else {
			temp.setMonth(temp.getMonth() + 3);
		}
		if(fiscalEndDate > temp){
			dates+=temp.getMonth()+1+"/"+temp.getDate()+"/"+temp.getFullYear()+"\n";
		}
	}
	if (flag == false) {
		alert("Fiscal Year Start Date : '<%=fromDate%>'\nPlease enter any one of the below dates\n"+dates);
		$('#startDate').val('');
		$('#startDate').focus();
		return false;
	}
}

function checkFiscalEndDate() {
	var endDate = new Date($('#endDate').val());
	var fiscalStartDate = new Date('<%=fromDate%>');
	var fiscalEndDate = new Date('<%=endDate%>');
	var fiscalEndDate1 = new Date('<%=endDate%>');
	var temp = fiscalEndDate;
	var flag = false;
	var flag1 = false;
	var dates=temp.getMonth()+1+"/"+temp.getDate()+"/"+temp.getFullYear()+"\n";
	for (; fiscalStartDate < temp;) {
		if (temp.getTime() == endDate.getTime()) {
			flag = true;
			break;
		}
		if (fiscalEndDate1.getDate() == 31) {
			if ((temp.getMonth() - 3) < 0) {
				temp.setFullYear(temp.getFullYear() - 1,
						(temp.getMonth() - 3) + 12, temp.getDate());
			} else if ((temp.getMonth() - 3) == 0 || (temp.getMonth() - 3) == 2
					|| (temp.getMonth() - 3) == 4 || (temp.getMonth() - 3) == 6
					|| (temp.getMonth() - 3) == 7 || (temp.getMonth() - 3) == 9
					|| (temp.getMonth() - 3) == 11) {

				temp.setMonth(temp.getMonth() - 3);
				if (flag1 == true) {
					temp.setDate(temp.getDate() + 1);
					flag1 = false;
				}
			} else if ((temp.getMonth() - 3) == 3 || (temp.getMonth() - 3) == 5
					|| (temp.getMonth() - 3) == 8
					|| (temp.getMonth() - 3) == 10) {

				if (flag1 == false) {

					temp.setDate(temp.getDate() - 1);
					flag1 = true;
				}

				temp.setMonth(temp.getMonth() - 3);

			}
		} else {
			temp.setMonth(temp.getMonth() - 3);
		}
		if(fiscalStartDate < temp){
			dates+=temp.getMonth()+1+"/"+temp.getDate()+"/"+temp.getFullYear()+"\n";
		}
	}
	if (flag == false) {
		alert("Fiscal Year End Date : '<%=endDate%>'\n\nPlease enter any one of the below dates\n"+dates);
		$('#endDate').val('');
		$('#endDate').focus();
		return false;
	}
}


	var imgPath = "jquery/images/calendar.gif";
	datePicker_Report();
</script>


<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>
<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>

<script src="js/dhtmlxChart/dhtmlxChart/codebase/dhtmlxchart.js"
	type="text/javascript"></script>
<link rel="STYLESHEET" type="text/css"
	href="js/dhtmlxChart/dhtmlxChart/codebase/dhtmlxchart.css">
<style>
.dhx_axis_item_x {
	font-size: 11px
}

.dhx_axis_item_y {
	font-size: 11px
}
</style>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 450px; width: 100%;">
		<html:form action="/indirectSpendReport?method=indirectSpendReport">

			<div style="padding: 20px 10px 0 10px;">
				<fieldset style="padding: 20px 0 20px 50px;">
					<legend>Indirect Spend Report By</legend>
					<html:radio property="option" value="Supplier"
						onclick="displaySupplier();" styleId="byOption1">&nbsp;Prime Supplier &nbsp; </html:radio>
					<html:radio property="option" value="Quarter"
						onclick="displayQuarter();" styleId="byOption2">&nbsp;Quarter &nbsp;</html:radio>
					<html:radio property="option" value="Year" onclick="displayYear();"
						styleId="byOption3">&nbsp; Year &nbsp;</html:radio>
					<html:radio property="option" value="YearToDate"
						styleId="byOption4" onclick="displayDateToYear();"> &nbsp;Year To Date &nbsp;</html:radio>

					<div style="padding: 10px 0 5px 280px;">
						<div id="by_supplier" style="display: none;">
							<fieldset style="width: 250px; padding: 10px 0 5px 10px;">
								<legend>By Supplier</legend>
								<label>Select Supplier</label>

								<html:select property="supplier" styleId="supplier"
									multiple="true" style="width:210px;">
									<logic:present name="reportForm" property="vendors">
										<logic:iterate id="vendor" name="reportForm"
											property="vendors">
											<bean:define id="id" name="vendor" property="id"></bean:define>
											<html:option value="<%=id.toString() %>">
												<bean:write name="vendor" property="vendorName"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:present>

								</html:select>
							</fieldset>
						</div>
						<div id="by_quarter" style="display: none;">
							<fieldset style="width: 250px; padding: 10px 0 5px 10px;">
								<legend>By Quarter</legend>
								<label> Start Date </label>
								<html:text property="startDate" alt="" styleId="startDate"
									onchange="return checkFiscalFromDate();"></html:text>
								<br /> <br /> <label>End Date &nbsp;</label>
								<html:text property="endDate" alt="" styleId="endDate"
									onchange="return checkFiscalEndDate();"></html:text>
								<br />

							</fieldset>

						</div>
						<div id="by_year" style="display: none;">
							<fieldset style="width: 250px; padding: 10px 0 5px 10px;">
								<legend>By Year</legend>

								<html:select property="year" alt="" styleId="year"
									multiple="true" style="width:110px;">
									<logic:present property="years" name="reportForm">
										<logic:iterate id="years" property="years" name="reportForm">
											<bean:define id="year" name="years"></bean:define>
											<html:option value="<%=year.toString() %>">
												<%=year.toString()%>
											</html:option>
										</logic:iterate>
									</logic:present>
								</html:select>
							</fieldset>
						</div>
					</div>
					<div style="padding: 10px 0 5px 350px;">
						<html:submit value="Generate Report" styleClass="customerbtTxt"
							onclick="return validate();"></html:submit>
					</div>
				</fieldset>
			</div>


			<logic:present property="status" name="reportForm">

				<logic:equal value="success" property="status" name="reportForm">
					<h2><img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;
						Indirect Spend Report Based on
						<bean:write name="reportForm" property="option" />
					</h2>
					<script>
					var path='<%=path%>';
				var barChart;
				window.onload = function() {
					barChart = new dhtmlXChart({
						view : "bar",
						color : "#66ccff",
						gradient : "3d",
						container : "chart_container",
						value : "#data2#",
						label : "#data2#",
						radius : 3,
						tooltip : {
							template : "#data2#"
						},
						width : 50,
						origin : 0,
						yAxis : {
							
							title : "Amount"
						},

						group : {
							by : "#data0#,#data1#",
							map : {
								data2 : [ "#data2#", "sum" ]
							}
						},
						xAxis : {
							title : "Diverse Classification",
							template : "#id#"
						},
						border : false
					});

					function refresh_chart() {
						barChart.clearAll();
						barChart.parse(mygrid, "dhtmlxgrid");
					};

					mygrid = new dhtmlXGridObject('gridbox');
					mygrid
							.setImagePath('js/dhtmlxChart/dhtmlxGrid/codebase/imgs/');
					mygrid.setImagePath("js/common/imgs/");
					mygrid.setSkin("light");
					
					mygrid.loadXML(path, refresh_chart);
					
				};
			</script>
					<div style="padding: 20px 10px 50px 10px;">
						<div id="gridbox"
							style="width: 850px; height: 170px; background-color: white; margin: 0 0 0 6%;"></div>

						<div id="chart_container" style="width: 850px; height: 250px;"></div>
					</div>
				</logic:equal>


			</logic:present>

			<logic:present property="status" name="reportForm">

				<logic:equal value="noreport" property="status" name="reportForm">
					<table width="95%" height="400" cellpadding="0" cellspacing="0">
						<tr align="center">
							<td><font size="3px">No Indirect Spend Data</font></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
				</logic:equal>
			</logic:present>

		</html:form>
		<script type="text/javascript">
			initialize();
		</script>
	</div>
</div>
