<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Spend Data Dashboard Report" name="privilege"
		property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
<!-- Added for jquery validation -->
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" media="screen" 	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>


<!--<link rel="stylesheet" type="text/css" 	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />

 -->
 
<style type="text/css">
/*th.ui-th-column div {
	white-space: normal !important;
	height: auto !important;
	padding: 2px;
}

.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
	height: auto;
	vertical-align: text-top;
	padding-top: 2px;
}*/
</style>
<script>

$(function() {
	  var start_year = new Date().getFullYear();

	  for (var i = start_year; i > start_year - 15; i--) {
	    $('#spendYear').append('<option value="' + i + '">' + i + '</option>');
	  }
	  
	  $('#spendYear').val($("#spendYearH").val());
	});
	
	$(document).ready(function() {

		$("#tier2VendorNames").multiselect({
			selectedText : "# of # selected"
		});
		$("#reportingPeriod").multiselect({
			multiple : false,
			header : "Select an option",
			noneSelectedText : "Select an Option",
			selectedList : 1
		});
		$("#spendYear").multiselect({
			multiple : false,
			header : "-- Year --",
			noneSelectedText : "-- Year --",
			selectedList : 1,
			minWidth : '160'
		});

		$("#spendYear").change(function() {
			var id = $(this).val();
			var dataString = 'year=' + id;

			$.ajax({
				type : "POST",
				url : "spendReport.do?method=loadReportPeriod",
				data : dataString,
				cache : false,
				success : function(html) {

					$("#reportingPeriod").html(html);

					$("#reportingPeriod").multiselect({
						multiple : false,
						header : "Select an option",
						noneSelectedText : "Select an Option",
						selectedList : 1
					});
				}
			});
		});
	});
</script>
<section role="main" class="reports-export content-body card-margin reports-module">
	<div class="row">
		<div class="col-lg-9 mx-auto">
				<form id="reportform" class="form-horizontal">
					<section class="card">
						<header class="card-header">
							<h2 class="card-title">Spend Data Dashboard Report</h2>
						</header>
		<div class="form-box card-body">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-sm-right label-col-wrapper">View the
					report by Vendor:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:select property="tier2VendorNames" multiple="true"
						name="tier2ReportForm" styleId="tier2VendorNames" styleClass="form-control">
						<logic:present name="tier2ReportForm" property="vendors">
						<html:optionsCollection name="tier2ReportForm" property="vendors" label="vendorName" value="id"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="form-group row">	
					<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Spend
					Year:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<select name="spendYear" id="spendYear" class="form-control">
						<option value="0">-- Select --</option>
						<logic:present name="tier2ReportForm" property="spendYears">
						<logic:iterate id="year" name="tier2ReportForm" property="spendYears">
						<option value="${year}">${year}</option>
						</logic:iterate>
						</logic:present>
					</select>
				</div>
			</div>
				<div class="form-group row">	
							<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Reporting
							Period:</label>
						<div class="col-sm-9 ctrl-col-wrapper">
							<select id="reportingPeriod" name="reportingPeriod" class="form-control">
								<option value="0">-- Select --</option>
							</select>
						</div>
					</div>
				</div>
		<footer class="card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
			<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getSpendDashboardData();" />
		</div>
					</div>
				</footer>	
			</section>	
		</form>
<div class="clear"></div>
<div class="grid-wrapper view-reports-table">
	<div class="form-box card-body">	
		<div id="grid_container">
			<div class="responsiveGrid">
				<table id="gridtable" class="table table-bordered table-striped mb-0">
					<div id="ajaxcontent"></div>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="pdfOption" style="display: none;">
	<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails"
						property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="btn btn-primary pull-right" 
				onclick="generateSpendDataDashboardReport('<%=logoPath%>');">
	</logic:present>
</div>
</div>
</div>
</section>

<script>
function generateSpendDataDashboardReport(path){
	window.location = "spendReport.do?method=getSpendDataDashboardReportPdf";
	/*var pageHead = "Spend Data Dashboard Report";
	var pathArray = window.location.href.split( '/' );
	var protocol = pathArray[0];
	var host = pathArray[2];
	var parameter = pathArray[3];
	var url = protocol + '//' + host + "/" + parameter;
	var html = "<html><head>"
		+ "<style script=&quot;css/text&quot;>"
		+ "table.tableList_1 th {border:1px solid black; text-align:center; "
		+ "vertical-align: middle; padding:5px;}"
		+ "table.tableList_1 td {border:1px solid black; text-align: left; vertical-align: top; padding:5px;}"
		+ "</style>"
		+ "</head>"
		+ "<body style=&quot;page:land;font-family:Verdana;&quot;>"
		+ "<div class=&quot;logo&quot;>"
		+ "<img src=&quot;" 
		+ url
		+ "/temp/"
		+ path
		+ "&quot; style=&quot;float:left;"
		+ "position:absolute;top:0px;left:0px;&quot; "
		+ "height=&quot;60px&quot; width=&quot;60px&quot; alt=&quot;Logo&quot; />"
		+ "</div><br/>"
		+ "<div class=&quot;pageHead_1&quot; style=&quot;text-align:center;padding: 5%;font-family:Verdana;&quot;>"
		+ pageHead + "</div> ";
	 html+=$('#ajaxcontent').html() +"</body></html>";
	 html = html.replace(/'/g, '&apos;');
	var form = "<form name='pdfexportform' action='generategrid' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='SpendDataDashboard'>";
	form = form + "<input type='hidden' name='imagePath' value='" + path + "'>";
	form = form + "<input type='hidden' name='pdfBuffer' value='" + html + "'>";
	form = form + "</form><script>document.pdfexportform.submit();</sc"
			+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();*/
}
</script>
</logic:equal>
<logic:equal value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have no rights to view Spend Data Dashboard Report.</h3>
			</div>
</logic:equal>

</logic:equal></logic:iterate>