<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="chosen/chosen.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="chosen/chosen.css" />
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<link rel="stylesheet" type="text/css" href="jquery/css/ui.multiselect.css" />
<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<!-- For select 2 -->
<script src="select2/select2.js"></script>
<link href="select2/select2.css" rel="stylesheet" />

<!-- Sorting Table Header -->
<script type="text/javascript" src="jquery/js/sorttable.js"></script>

<script>

$(document).ready(function() 
{
	$(document).keypress(function(e) {
		if (e.which == 13) {
			$(e.target).blur();
			return saveDynamicColumns();
		}
	});
});

	function showSaveFilter(){
		$("#dialog2").css({
			"display" : "block"
		});
		$("#dialog2").dialog({
			minWidth : 400,
			modal : true
		});
		$("#searchName").focus();
		
		$(document).keypress(function(e) {
			if (e.which == 13 && $("#dialog2").is(':visible')) {
				$(e.target).blur();
				return saveFilter();
			}
		}); 
	}
	function backToSearch() {
		window.location = "searchReport.do?method=searchVendorReport";
	}
	$(function() {
		$("#accordion").accordion({
			 heightStyle: "content"
		});
	});
	
	
	function clearFields(){
		window.location.reload(true);
	}
	
	function saveDynamicColumns(){
		var chkArray = [];
		var selected;
		
		$(".chk:checked").each(function() {
			chkArray.push($(this).val());
		});
		selected = chkArray.join(',');
		if(selected.length > 1){
		$.ajax({
			url : "dynamicReport.do?method=saveCustomColumnsForSearch&columnIds="+selected,
			type : "POST",
			async : false,
			success : function(data) {
				$('#dynamicColumnSelect').hide();
				$('#result').css('display', 'block');
				$("#gridtable").jqGrid('GridUnload');
				gridSearchVendorData(data);
			}
		});
			
		}else{
			alert("Please select atleast one checkbox");	
		}  
	}
	
	function gridSearchVendorData(myGridData) {
		
		var dynamicData = myGridData.split("|");
		var dynamicColNames = jQuery.parseJSON(dynamicData[0]);
		var dynamicColModel = jQuery.parseJSON(dynamicData[1]);
		var dynamicColData = dynamicData[2];

		var newdata1 = jQuery.parseJSON(dynamicColData);

		$('#gridtable').jqGrid({
			
					data : newdata1,
					datatype : 'local',
					colNames : dynamicColNames,
					colModel : dynamicColModel,
					pager : '#pager',
					shrinkToFit : false,
					viewrecords : true,
					autowidth : true,
					emptyrecords : 'No data available',
					gridview: true,
		            pgbuttons:false,
		            pgtext:'',
		            width: ($.browser.webkit?466:497),
		            loadonce: true,
		            rowNum: 10000,
					cmTemplate: { title: false },
					height: 250
				}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		},
		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{
			drag : true,
			closeOnEscape : true
		}
		);

		$("#gridtable").jqGrid('navButtonAdd', '#pager', {
			caption : "Change Order",
			title : "Reorder Columns",
			onClickButton : function() {
				$("#gridtable").jqGrid('columnChooser', {
					title : "Select and reorder columns",
					width : 700,
					height : 350,
					done : function(perm) {
						if (perm) {
							this.jqGrid("remapColumns", perm, true);
						}
					}
				});
			}
		});
	}
	
function saveFilter() {
		
		var searchName=$("#searchName").val();
		if (searchName != '' && searchName != 'undefined') 
		{
			$.ajax({
				url : "dynamicReport.do?method=saveSearchedData&searchType=R&searchName="+searchName,
				type : "POST",
				async : false,
				dataType : "json",
				beforeSend : function() {
						$("#ajaxloader").show();
				},
				success : function(data) {
					$("#ajaxloader").hide();
					if((data.result)=="success"){
						$('#searchName').val('');
						$("#dialog2").dialog("close");
						alert("Search criteria successfully saved ");
						window.location.reload(true);	
					}
					else if((data.result)=="unique"){
						alert("Search Name allready available ");
					}
					else
						alert("Search criteria failed to save ");
				}
			});
		}
		else{
			alert("Please Enter a Name for search");
		}
	}
	
</script>

<section role="main" class="content-body reports-module">
	<div class="row">
		<div class="col-lg-12">
<div id="dynamicColumnSelect">
<header class="card-header" style="background: #0088cc;">
	<h2 class="card-title" style="color: #fff;">SELECT COLUMNS FROM HERE..</h2>
</header>

	<form action="" class="mt-2">

		<div id="accordion">
			<logic:present name="searchFeildsList">
				<bean:size id="size" name="searchFeildsList" />
				<logic:greaterEqual value="0" name="size">
					<logic:iterate id="searchFeildsList" name="searchFeildsList">
						<div class="">
							<header class="card-header">
								<h4 class="card-title " style="font-size:16px !important;">
								<bean:write name="searchFeildsList" property="category"></bean:write>
								</h4>
							</header>
						</div>
						<div class="mb-1">
							<bean:define id="columnTitle" name="searchFeildsList"
								property="columnTitle"></bean:define>

							<bean:size id="size" name="columnTitle" />
							<logic:greaterEqual value="0" name="size">
								<table class="table mb-0">
									<tr>
										<%	int j=0; %>
										<logic:iterate id="columnDetails" name="columnTitle">
											<%
								String[] columnValue = columnDetails.toString().split("\\,");
															if (columnValue != null
																		&& columnValue.length != 0) {
																	for (int i = 0; i < columnValue.length; i++) {
																		String[] parts = columnValue[i]
																				.split(":");
																		String coulumnId = parts[0];
																		String columnName = parts[1];
																		if(j==5){
																			j=0;
																			%>
										
									</tr>
									<tr>
									</tr>
									<tr>
									</tr>
									<tr>
									</tr>
									<tr>
										<%
														}
														j++;
						%>

										
										<td style="width:10px;"><input type="checkbox" value="<%=coulumnId%>"
											class="chk"></td>
										<td align="left"><label > <%=columnName%></label></td>

										<%
							}
								}
						%>
										</logic:iterate>
									</tr>

								</table>
							</logic:greaterEqual>

							
						</div>
					</logic:iterate>
				</logic:greaterEqual>
			</logic:present>
		</div>
		<footer class="mt-2 ">
			<div class="row justify-content-end text-sm-right">
				<div class="wrapper-btn">
					<input type="button" class="btn btn-primary" value="Show Report" id="buttonClass" onclick="saveDynamicColumns();"> 
					<input type="button" class="btn btn-default" value="Clear" id="buttonClass" onclick="clearFields();">
					<input type="button" class="btn btn-default" value="Back To Search" id="buttonClass" onclick="backToSearch();"> 
				</div>
			</div>
		</footer>
			
	</form>
</div>

<div id="result" class="card-body form-box" style="display: none;">
	<header class="card-header">
		<h2 class="card-title pull-left">
		<img src="images/icon-registration.png" />Following are
		your Vendors</h2>	
		<div class="">
			<input type="button" class="btn btn-primary pull-right" value="Export" id="export"
				onclick="exportGrid();">
		
			<input type="button" value="Save Search" class="btn btn-default pull-right mr-2"
			onclick="showSaveFilter();">
		</div>
	</header>

	<div id="grid_container" class="card-body">
		<table id="gridtable" class="main-table table table-bordered table-striped mb-0">
			<tr>
				<td />
			</tr>
		</table>
		<div id="pager"></div>
	</div>
	<div id="dialog" style="display: none;" title="Choose Export Type">
		<p>Please Choose the Export Type</p>
		<table class="main-table table table-bordered table-striped mb-0">
			<tr>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="1" id="excel" /></td>
				<td><label for="excel"><img id="excelExport"
						src="images/excel_export.png" />Excel</label></td>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="2" id="csv" /> <input type="hidden" name="fileName"
					id="fileName" value="SupplierReport" /></td>
				<td><label for="csv"><img id="csvExport"
						src="images/csv_export.png" />CSV</label></td>
				<td style="padding: 1%;"><input type="radio" name="export"
					value="3" id="pdf" /></td>
				<td><label for="pdf"><img id="pdfExport"
						src="images/pdf_export.png" />PDF</label></td>
			</tr>
		</table>
		
				<div class="wrapper-btn">
			<logic:present name="userDetails">
				<bean:define id="logoPath" name="userDetails"
					property="settings.logoPath"></bean:define>
				<input type="button" value="Export" class="exportBtn btn-primary"
					onclick="exportHelperForDynamic('gridtable','<%=logoPath%>');">
			</logic:present>
		</div>
	</div>
	<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="wrapper-btn">
		<input type="button" class="btn btn-primary mr-1" value="Back To Search" id="buttonClass" onclick="backToSearch();"> 
		<input type="button" class="btn btn-default" value="Back" id="buttonClass" onclick="clearFields();">
	</div>
	</div>
	</footer>
</div>
<div id="dialog2" class="form-box" style="display: none;"
	title="Save Filter">

	<div class="wrapper-full">
		<div class="row-wrapper">
			<div class="label-col-wrapper" style="width: 35%;">Search Name
				:</div>
			<div class="ctrl-col-wrapper">
				<input type="text" id="searchName" name="searchName"
					class="text-box" />
			</div>
		</div>
	</div>
	<div class="btn-wrapper">
		<input type="button" class="exportBtn" onclick="saveFilter();"
			value="Submit" id="statusButton" />
	</div>
</div>
<div id="ajaxloader" style="display: none;">
	<img src="images/ajax-loader.gif" alt="Ajax Loading Image" />
</div>
</div>
</div>
</section>