<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("spendByAgencyReport") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("spendByAgencyReport");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("agencyName", tier2ReportDto.getCertAgencyName());
			cellobj.put("certificateShortName", tier2ReportDto.getCertificateShortName());
			cellobj.put("spend", Math.round(tier2ReportDto.getCount().doubleValue()));			
			cellobj.put("percent", Math.round(Double.parseDouble(tier2ReportDto.getDiversityPercent())) + "%");

			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
