<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Included to Generate Report -->
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Added for Populate JqGrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css"/>

<!-- Added for Export CSV Files -->
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<section class="card">	
<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Registered Vendors" name="privilege" property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
			<!-- Heading Part-->
			<header class="card-header">
				<h2 class="card-title pull-left">Registered Vendors Report</h2>
			</header>		
			
			<!-- Form Part -->
			<div class="form-box card-body">
				<html:form styleId="reportform">				
					<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Start Date:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<html:text property="startDate" styleId="startDate" alt="Please Select Start Date" readonly="true"
								styleClass="main-text-box form-control" />	
								
								</div>							
							</div>
						</div>
						<div class="row-wrapper form-group row mt-5">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">End Date:</label>
							<div class="col-sm-9 ctrl-col-wrapper">								
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
									<html:text property="endDate" styleId="endDate" alt="Please Select End Date" readonly="true"
											styleClass="main-text-box form-control" />
								</div>
							</div>
						</div>
					
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
								<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getRegisteredVendorsReport();" />						
							</div>
						</div>
					</footer>
				</html:form>
			</div>
			
			<div class="clear"></div>
			
			<!-- Result Part -->			
			<div id="grid_container" class="card-body">
				<div class="responsiveGrid">
				<table id="gridtable" class="table table-bordered table-striped mb-0">
					<tr>
						<td />
					</tr>
				</table>
				<div id="pager"></div>
				</div>
			</div>
		</logic:equal>
		<logic:equal value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have No Rights to View Registered Vendors Report.</h3>
			</div>
		</logic:equal>
	</logic:equal>
</logic:iterate>

<!-- Export Dialog Box -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel">
			</td>
			<td>
				<label for="excel">
					<img id="excelExport" src="images/excel_export.png" />Excel
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"> 
				<input type="hidden" name="fileName" id="fileName" value="RegisteredVendorsReport">
			</td>
			<td>
				<label for="csv">
					<img id="csvExport" src="images/csv_export.png" />CSV
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf">
					<img id="pdfExport" src="images/pdf_export.png" />PDF
				</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportRegisteredVendorsReportData('gridtable');">		
	</div>
</div>
</section>
</div>
</div>
</section>
<script type="text/javascript">
	$( document ).ajaxError(function() 
	{
		$("#ajaxloader").hide();
	});

	$(document).ready(function() 
	{
		$.datepicker.setDefaults(
		{
			changeYear : true,
			dateFormat : 'mm/dd/yy'
		});
		$("#startDate").datepicker();
		$("#endDate").datepicker();
		
		//To Reset Date Field
		$.datepicker._clearDate("#startDate");
		$.datepicker._clearDate("#endDate");
	});
	
	function getRegisteredVendorsReport() 
	{
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		
		if(startDate == '') 
		{
			alert('Please Select Start Date.');
			return false;
		} 
		else if(endDate == '') {
			alert('Please Select End Date.');
			return false;
		} 
		else if(new Date(startDate) >= new Date(endDate)) 
		{
			alert("Invalid Date Range.");
			return false;
		} 
		else 
		{
			$.ajax(
			{
				url : "registeredVendorsReport.do?method=getRegisteredVendorsReport&random=" + Math.random(),
				type : "POST",
				data : $("#reportform").serialize(),
				async : true,
				beforeSend : function()
				{
					$("#ajaxloader").show();
				},
				success : function(data) 
				{
					$("#ajaxloader").hide();
					$("#gridtable").jqGrid('GridUnload');
					gridRegisteredVendorsReportData(data);
				}
			});			
		}
	}
	
	function gridRegisteredVendorsReportData(myGridData)
	{
		var newdata1 = jQuery.parseJSON(myGridData);

		$('#gridtable').jqGrid(
		{
			data : newdata1,
			datatype : 'local',
			colNames : [ 'Vendor Name', 'First Name', 'Last Name', 'Email Id', 
			             'Phone Number', 'Created On', 'City', 'State', 'Vendor Status'],
			colModel : [
		            {
						name : 'vendorName',
						index : 'vendorName',
						sorttype: 'string'
					}, {
						name : 'firstName',
						index : 'firstName',
						sorttype: 'string'
					}, {
						name : 'lastName',
						index : 'lastName',
						sorttype: 'string'
					}, {
						name : 'emailId',
						index : 'emailId',
						sorttype: 'string'
					}, {
						name : 'phone',
						index : 'phone',
						sorttype: 'string'
					}, {
						name : 'createdDate',
						index : 'createdDate',
						sorttype : 'date',
						formatter : 'date',
						formatoptions : {
						 srcformat:'m-d-y',
						 newformat: 'm-d-Y'
						}
					}, {
						name : 'city',
						index : 'city',
						sorttype: 'string'
					}, {
						name : 'state',
						index : 'state',
						sorttype: 'string'
					}, {
						name : 'vendorStatus',
						index : 'vendorStatus',
						sorttype: 'string'
					}],
			rowNum : 5000,
			rowList : [ 5000, 10000, 15000 ],
			pager : '#pager',
			shrinkToFit : false,
			autowidth : true,
			viewrecords : true,
			emptyrecords : 'No Data Available...',
			footerrow : true,
			userDataOnFooter : true,
			cmTemplate: {title: false},
			height : 250,		
			gridview: true,
	        pgbuttons:true,
	        width: ($.browser.webkit?466:497),
	        loadonce: true        
		}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		},

		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
			caption : "Export",
			buttonicon : "ui-icon ui-icon-newwin",
			onClickButton : function() 
			{
				exportGrid();
			},
			position : "last"
		});

		$(window).bind('resize', function() 
		{
			$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
		}).trigger('resize');
	}
	
	/**
	 * Function to Export the Registered Vendors Report Data.
	 */
	function exportRegisteredVendorsReportData(table) 
	{	
		var exportType = $("input[name=export]:checked").val();
		var cols = [];
		$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
		{
			if (!this.hidden) 
			{
				cols.push(this.index);
			}
		});

		var index = [];
		var valueForCsv = [];
		for ( var i = 0; i <= cols.length; i++) {
			if (cols[i] == "vendorName") {
				index.push(0);
				valueForCsv.push("Vendor Name");
			} else if (cols[i] == "firstName") {
				index.push(1);
				valueForCsv.push("First Name");
			} else if (cols[i] == "lastName") {
				index.push(2);
				valueForCsv.push("Last Name");
			} else if (cols[i] == "emailId") {
				index.push(3);
				valueForCsv.push("Email Id");
			} else if (cols[i] == "phone") {
				index.push(4);
				valueForCsv.push("Phone Number");
			} else if (cols[i] == "createdDate") {
				index.push(5);
				valueForCsv.push("Created On");
			} else if (cols[i] == "city") {
				index.push(6);
				valueForCsv.push("City");
			} else if (cols[i] == "state") {
				index.push(7);
				valueForCsv.push("State");
			} else if (cols[i] == "vendorStatus") {
				index.push(8);
				valueForCsv.push("Vendor Status");
			}
		}

		if (exportType == 1) 
		{
			exportExcel(table, index, 'RegisteredVendorsReport');
		} 
		else if (exportType == 2) 
		{
			$('#' + table).table2CSV({
				header : valueForCsv,
				fileName : fileName
			});
		} 
		else if (exportType == 3) 
		{		
			window.location = "registeredVendorsReport.do?method=getRegisteredVendorsReportPdf";
		} 
		else 
		{
			alert("Please Choose a Type to Export");
			return false;
		}

		$('#dialog').dialog('close');
	}
</script>