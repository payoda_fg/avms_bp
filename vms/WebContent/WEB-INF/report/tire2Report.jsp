<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.io.*"%>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.smartWizard-2.0.js"></script>
<link href="jquery/css/smart_wizard.css" rel="stylesheet"
	type="text/css">
<link href="select2/select2.css" rel="stylesheet" />
<script src="select2/select2.js"></script>
<script src="jquery/js/jquery.placeholder.js"></script>
<script src="js/accounting.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jquery.maskedinput-1.3.js"></script>

<script type="text/javascript" src="jquery/js/ui.multiselect.js"></script>

<!-- Files used for load jqgrid -->
<!-- <script type="text/javascript" src="jquery/js/grid.locale-en.js"></script> -->
<!-- <script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script> -->
<!-- This is for Search Option of JqGrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en1.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min1.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid1.css" />

<!-- This is for Other Things of JqGrid -->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />

<!-- This is for Auto Sizing the Text Area -->
<script type="text/javascript" src="jquery/ui/jquery.autosize.min.js"></script>

<script type="text/javascript" src="jquery/js/naicstreegrid.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<bean:define name="userDetails" id="compName"
	property="currentCustomer.customerId.custName" />
<bean:define name="tier2ReportForm" id="groupName" property="groupName" />
<bean:define name="vendorUser" property="vendorId.vendorName"
	id="supplierName" />

<style>
.chosen-select {
	width: 150px;
}
</style>
${requestScope.errorAlert}
<script type="text/javascript">
function cancelTier2Vendors() {
	$("#dialog3").dialog("close");
	$("#okButtonDiv").hide();
}

function tier2VendorsListBySearch()
{
	var searchVendorsText = $("#searchVendorsText").val();
	selectedIDs = pickTier2Vendors(searchVendorsText);
	$("#okButtonDiv").show();
}

function addTier2Vendor() {
	var grid = jQuery("#list2");
	var html = "";
	var messages = [];
// 	messages.push("Following Tier2Vendor already selected. You cannot add it again.\n");
	if (selectedIDs.length > 0) {
		for ( var i = 0, il = selectedIDs.length; i < il; i++) {
			var row = grid.getLocalRow(selectedIDs[i]);
			var vendorId = row.id;
			var vendorName = row.vendorName;
			
			var tier2VendorExists = false;
			if(venIdName != null && venIdName == 'tier2indirectvendorName'){
			$("input[name='tier2indirectvendorName']").each(function(index) { //For each of inputs
			    if (vendorId == $(this).val()) { //if match against array
			    	 messages.push(vendorName + "\n");
			    	tier2VendorExists = true;
			    } 
			});
			}

			if(venIdName != null && venIdName == 'tier2vendorName'){
			$("input[name='tier2vendorName']").each(function(index) { //For each of inputs
			    if (vendorId == $(this).val()) { //if match against array
			    	 messages.push(vendorName + "\n");
			    	tier2VendorExists = true;
			    } 
			});
			}

			if(/* !tier2VendorExists &&  */vendorId !=''){
					$('#'+tier2VendorId).val(vendorId);
					$('#'+tier2VendorName).val(vendorName);
			}
		}
		if(messages.length > 1){
			alert(messages.join(""));
		}
// 		$("#vendorCommodity").append(html);
		$("#dialog3").dialog("close");
	} else
		{
			alert("Please select atleast one record");	
			return false;
		}
}

function getTier2Vendors(venId,venName,vendIdName) 
{
	tier2VendorId = venId;
	tier2VendorName = venName;
	venIdName = vendIdName;
	
	var wWidth = $(window).width();
	var dWidth = wWidth * 0.73;
	$("#okButtonDiv").hide();
	$("#searchVendorsText").val('');
	//var wHeight = $(window).height();
	//var dHeight = wHeight * 0.8;
	
	//To Reset the Select Boxes....
	$("#marketSectors option[value = " + 0 + "]").prop('selected', true);
	$("#marketSectors").select2();
	
	$('#marketSubSectors').empty();
	$('#marketSubSectors').find('option').remove().end().append("<option value='0'>-- Select --</option>");
	$('#marketSubSectors').select2();
	
	//To Reset Grid Data
	$("#list2").jqGrid('GridUnload');
	  
	$("#dialog3").css({
		'display' : 'block',
		'font-size' : 'inherit'
	});
	$("#dialog3").dialog({
		width : dWidth,
		//height : dHeight,
		minheight : 400,
		modal : true,
		open: function(){
			document.getElementById("searchVendorsText").focus();
	    },
		close : function(event, ui) {
			//close event goes here
		},
		show : {
			effect : "scale",
			duration : 1000
		},
		hide : {
			effect : "scale",
			duration : 1000
		}
	});
}

$(function() {
	  var start_year = new Date().getFullYear();

	  for (var i = start_year; i > start_year - 15; i--) {
	    $('#spendYear').append('<option value="' + i + '">' + i + '</option>');
	  }
	  
	  $('#spendYear').val($("#spendYearH").val());
	});
	
    $(document).ready(function() {	
    	$("#searchVendorsText").keyup(function(e) {
			if($("#searchVendorsText").val() != '')
				tier2VendorsListBySearch();
			else{
				$("#list2").hide();
				$("#okButtonDiv").hide();
			}
		});
		
    	$(document).ready(function() { $(".chosen-select").select2(); });
    	var tier2VendorId = null;
    	var tier2VendorName = null;
    	var venIdName = null;
        // Smart Wizard     	
        $('#wizard').smartWizard({
            transitionEffect: 'slide',
            onLeaveStep: leaveAStepCallback,
            onFinish: onFinishCallback
            /* enableFinishButton : true */
        });
        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel');
            return validateSteps(step_num);
        }
        function onFinishCallback() {
            if (validateAllSteps() && submitHandler()) {
                $('form').submit();
            }
        }
        //For Manual Validation Reporting Year In Tab 1 We Commented this following code.
        $("#spendYear").change(function() {
			var id = $(this).val();
			var dataString = 'year=' + id;

			$.ajax({
				type : "POST",
				url : "spendReport.do?method=loadReportPeriodByYear",
				data : dataString,
				cache : false,
				success : function(html) {
					$("#reportingPeriod").html(html);
				}
			});
		});
        var per=$('#indirectPer').val();
        if(per != "") {
        	setAllocatedAmount(per);        		
        }			
    });
    
    function submitHandler()
    {
    	saveVal = $('#isSubmit').val();
    	$('#tier2ReportForm').find('select').removeAttr('disabled');
        $('#tier2ReportForm').append("<input type='x' name='submitType' value='"+
        		saveVal+"' />");
        return true;
    }

    function validateAllSteps() {
        var isStepValid = true;

        if (validateStep1() == false) {
            isStepValid = false;
            $('#wizard').smartWizard('setError', {
                stepnum: 1,
                iserror: true
            });
        } else {
            $('#wizard').smartWizard('setError', {
                stepnum: 1,
                iserror: false
            });
        }

        if (validateStep2() == false) {
            isStepValid = false;
            $('#wizard').smartWizard('setError', {
                stepnum: 2,
                iserror: true
            });
        } else {
            $('#wizard').smartWizard('setError', {
                stepnum: 2,
                iserror: false
            });
        }

        if (!isStepValid) {
            $('#wizard').smartWizard('showMessage',
                    'Please correct the errors in the steps and continue');
        }
        return isStepValid;
    }

    function validateSteps(step) {
        var isStepValid = true;
        // validate step 1
        if (step == 1) {
            if (validateStep1() == false) {
                isStepValid = false;
                $('#wizard').smartWizard(
                        'showMessage',
                        'Please correct the errors in step' + step
                        + ' and click next.');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: true
                });
            } else {
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: false
                });
            }
        }
        
        // validate step 2
        if (step == 2) {
            if (validateStep2() == false) {
                isStepValid = false;
                $('#wizard').smartWizard(
                        'showMessage',
                        'Please correct the errors in step' + step
                        + ' and click next.');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: true
                });
            } else {
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: false
                });
            }
        }
        return isStepValid;
    }

	function validateStep1() 
    {

		debugger;
	    
    	var isValid = true;

        //dynamic validation for stepp1 starts from here   
        var tier2IndirectVendorNames = $("input[name='tier2indirectvendorName']").map(function() {
            return this.value;
        }).get();
        var tier1CertNames = $("select[name='certificate']").map(function() {
            return this.value;
        }).get();
        var tier1Ethnicity = $("select[name='ethnicity']").map(function() {
            return this.value;
        }).get();
        var tier1IndirectAmt = $("input[name='indirectAmt']").map(function() {
            return this.value;
        }).get();      

        var tier1MarketSector = $("select[name='marketSector']").map(function() {
            return this.value;
        }).get();  

        for (var i = 1; i < tier1IndirectAmt.length; i++) 
        {        	
			var idamt = tier1IndirectAmt[i];
            var tier1Certnames=tier1CertNames[i];
            var tvenname=tier2IndirectVendorNames[i];
            var eths=tier1Ethnicity[i];
            var value = idamt.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var intRegex = /^\d+$/;

			var tMarketSector = tier1MarketSector[i];
			var certificates=$('#certificates').val();
			var inDirectT2Vendor = $('#tier2indirectvendorName').val();
			var enthincity=$('#ethnicities').val();
			var marketSectorList=$('#marketSectorList').val();
			
			if(tvenname != 0)
			{
				if(tier1Certnames!=0)			
	           	{
	           		if(eths!=0)
	        		{
	        			if(idamt!=0)
	        			{
	        				if(intRegex.test(value))
	        		    	{
	       		    			isVlaid=true;
	       		    	    	$('#msg_indirectAmt'+(i+1)).html('').hide();
	       		                $('#msg_ethnicity'+(i+1)).html('').hide();
	       		                $('#msg_certificate'+(i+1)).html('').hide();
	       		             	$('#msg_tire2indirectvendorName1'+(i+1)).html('').hide();
	        		    	}
	        		    	else 
	        		    	{
	        		    		isValid=false;
	       		    	    	$('#msg_indirectAmt'+(i+1)).html('Please enter numeric value').show();
	                          	$('#msg_ethnicity'+(i+1)).html('').hide();
	                          	$('#msg_certificate'+(i+1)).html('').hide();
	                          	$('#msg_tire2indirectvendorName1'+(i+1)).html('').hide();
	        		    	}

	        			/*	 var table = document.getElementById("certificateTable");
	        			     var rowCount = table.rows.length;
	        			     alert(rowCount);

	        			     var vendorNameIn1=$("#vendorNameIn").value();
	        			     var certificate1=$("#certificate" + rowCount).value();
	        			     var ethnicity1=$("#ethnicity" + rowCount).value();
	        			     var marketSector1=$("#marketSector" + rowCount).value();
	        			     
	        				 if(((inDirectT2Vendor || vendorNameIn1) == tvenname) && ((tier1Certnames || certificate1) == certificates) && ((enthincity || ethnicity1) == eths) && ((tMarketSector || marketSector1) == marketSectorList) ){
	        			 			
	        			       		isValid=false;
	        			    		$('#msg_tire2indirectvendorName1'+(i+1)).html('Selected vendor details already exists').show();
	        			    		$('#msg_indirectAmt'+(i+1)).html('').hide();
	        			         	$('#msg_ethnicity'+(i+1)).html('').hide();
	        			         	$('#msg_certificate'+(i+1)).html('').hide();

	        				  }    */

	        			     if((inDirectT2Vendor  == tvenname) && (tier1Certnames  == certificates) && (enthincity == eths) && (tMarketSector == marketSectorList) ){
	        			 			
	        			       		isValid=false;
	        			    		$('#msg_tire2indirectvendorName1'+(i+1)).html('Selected vendor details already exists').show();
	        			    		$('#msg_indirectAmt'+(i+1)).html('').hide();
	        			         	$('#msg_ethnicity'+(i+1)).html('').hide();
	        			         	$('#msg_certificate'+(i+1)).html('').hide();

	        				  }  
	        		    	
	        			}
	        			else
			    		{
			    			isValid=false;
			    	    	$('#msg_indirectAmt'+(i+1)).html('Please fill indirect spend amount').show();
			                $('#msg_ethnicity'+(i+1)).html('').hide();
			                $('#msg_certificate'+(i+1)).html('').hide();
			                $('#msg_tire2indirectvendorName1'+(i+1)).html('').hide();
			    		}        		    	    
	        		}
	        		else
	        		{
		        		isValid=false;
		        		$('#msg_ethnicity'+(i+1)).html('Please select ethnicity certificate').show();
		        		$('#msg_indirectAmt'+(i+1)).html('').hide();
		        		$('#msg_certificate'+(i+1)).html('').hide();
		        		$('#msg_tire2indirectvendorName1'+(i+1)).html('').hide();
	                }
	           	}
	           	else
	           	{
	        		isValid=false;
	        		$('#msg_certificate'+(i+1)).html('Please select diversity classification').show();
	        		$('#msg_indirectAmt'+(i+1)).html('').hide();
	             	$('#msg_ethnicity'+(i+1)).html('').hide();
	             	$('#msg_tire2indirectvendorName1'+(i+1)).html('').hide();
	           	}
			}
			else
			{
				isValid=false;
				$('#msg_tire2indirectvendorName1'+(i+1)).html('Please select tier 2 vendor').show();
        		$('#msg_indirectAmt'+(i+1)).html('').hide();
             	$('#msg_ethnicity'+(i+1)).html('').hide();
             	$('#msg_certificate'+(i+1)).html('').hide();             	
			}
   		}
    
       
        var sy= $('#spendYear').val();
        
        if (!sy && sy.length <= 0) 
        {
            isValid = false;
            $('#msg_spendYear').html('Please select spend year')
                    .show();
        } 
        else
        {
            $('#msg_spendYear').html('').hide();
        }
       
        var rp = $('#reportingPeriod').val();
        if (rp == 0) {
            isValid = false;
            $('#msg_reportingPeriod').html('Please select reporting period')
                    .show();            
        } else {
            $('#msg_reportingPeriod').html('').hide();
        }
        
        if(sy.length == 1 && rp == 0)
        {        	
            isValid = false;
            $('#msg_reportingPeriod').html('Please select reporting period')
                    .show();
        }
       

        var tus = $('#totalUsSalesSelectedQtr').val();        
        var value = $('#totalUsSalesSelectedQtr').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        
        var intRegex = /^\d+$/;
        if (!tus && tus.length <= 0) {
            isValid = false;
            $('#msg_totalUsSales').html('Please fill total US sales').show();
        } else if (!intRegex.test(value)) {
            isValid = false;
            $('#msg_totalUsSales').html('Please enter numeric value').show();
        } else {
            $('#msg_totalUsSales').html('').hide();
        }

        var tust = $('#totalUsSalestoSelectedQtr').val();
        var value2 = $('#totalUsSalestoSelectedQtr').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        if (!tust && tust.length <= 0) {
            isValid = false;
            $('#msg_totalUsSalesto').html(
                    'Please fill total US sales to customer').show();
        } else if (!intRegex.test(value2)) {
            isValid = false;
            $('#msg_totalUsSalesto').html('Please enter numeric value').show();
        } else {
            $('#msg_totalUsSalesto').html('').hide();
        }
        var inp = $('#indirectPer').val();
        if (!inp && inp.length <= 0) {
            isValid = false;
            $('#msg_indirectPer').html('Please fill indirect percentage')
                    .show();
        } else {
            $('#msg_indirectPer').html('').hide();
        }
        
        if (parseFloat(tus) < parseFloat(tust)) {
        	isValid = false;
            $('#msg_totalUsSales').html('Total US sales cannot be less than total sales of vendor').show();        	
        } else {
            $('#msg_totalUsSales').html('').hide();
        }
        
        //Manually Validating Indirect Spend Amount and Diversity Certificate Ethnicity Certificate
        var tus = $('#indirectAmt').val();
        var value3 = $('#indirectAmt').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        var certificates=$('#certificates').val();
        var enthincity=$('#ethnicities').val();
        var inDirectT2Vendor = $('#tier2indirectvendorName').val();
//         var intRegex = /^\d+$/;
        var intRegex = /(\d+(\.\d+)?)/
        if(certificates!=0 && enthincity==0 )
        {        	
        	$('#msg_ethnicity').html('Please select ethnicity certificate ').show();
        	isValid=false;
        }
       	else if (!tus && tus.length <= 0 && certificates !=0 ) 
       	{
            isValid = false;
            $('#msg_indirectAmt').html('Please fill indirect spend amount').show();
        } 
       	else if (!intRegex.test(value3) && certificates !=0) 
       	{
            isValid = false;
            $('#msg_indirectAmt').html('Please enter numeric value').show(); 
        } 
       	else if(inDirectT2Vendor!=0 && certificates==0)
        {
       	    isValid = false;
            $('#msg_certificate').html('Please select diversity certificate').show(); 
       	}
       	else if(inDirectT2Vendor==0 && certificates !=0 && enthincity !=0 && msg_indirectAmt !=0 )
       	{
       		isValid=false;
       		$('#msg_tire2indirectvendorName1').html('Please select tier 2 vendor').show();
       	}
       	else 
       	{
            $('#msg_indirectAmt').html('').hide();
            $('#msg_ethnicity').html('').hide();
            $('#msg_tire2indirectvendorName1').html('').hide();
            $('#msg_certificate').html('').hide();
           
        }
      	return isValid;      
    }

    function validateStep2() 
    {  

        debugger;  	
        
    	//dynamic validation for step2 starts from here
        var isValid = true;
        var tier2vendorNames = $("input[name='tier2vendorName']").map(function() {
            return this.value;
        }).get();
        var certificateDrts = $("select[name='certificateDrt']").map(function() {
            return this.value;
        }).get();
        var directAmts = $("input[name='directAmt']").map(function() {
            return this.value;
        }).get();
        var deth = $("select[name='ethnicityDrt']").map(function() {
            return this.value;
        }).get();
        var dms = $("select[name='marketSectorDrt']").map(function() {
            return this.value;
        }).get();
        
        for (var i = 1; i < directAmts.length; i++) 
        {
            
        	var damt = directAmts[i];            
            var tier2name=tier2vendorNames[i];
            var certs=certificateDrts[i];
            var de = deth[i];
            var value = damt.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
//             var intRegex = /^\d+$/;
            var intRegex = /(\d+(\.\d+)?)/

            var marketSectorDrtData = dms[i];
            
            var directT2VendorDrtData = $('#tier2vendorName').val();
			var certificatesDrtData = $('#certificateDrt').val();
			var enthincityDrtData = $('#ethnicityDrt').val();
			var marketSectorListDrtData = $('#marketSectorDrt').val();

                
            if(tier2name!=0) 
            {
            	if(certs!=0)
            	{
                  if(de!=0)
                  {
            		if(damt!=0)
            		{
  		    	    	if(intRegex.test(value))
            		    {
      		    	    	isVlaid=true;
      		    	    	$('#msg_directAmt'+(i+1)).html('').hide();
      		                $('#msg_certificateDrt'+(i+1)).html('').hide();
      		                $('#msg_ethnicityDrt'+(i+1)).html('').hide();
      		                $('#msg_marketSectorDrt'+(i+1)).html('').hide();
      		                $('#msg_tire2vendorName'+(i+1)).html('').hide();
   		    	    	}
  		    	    	else 
            		    {
            				isValid=false;
            		    	$('#msg_directAmt'+(i+1)).html('Please enter numeric value').show();
                            $('#msg_certificateDrt'+(i+1)).html('').hide();
                            $('#msg_ethnicityDrt'+(i+1)).html('').hide();
                            $('#msg_marketSectorDrt'+(i+1)).html('').hide();
                            $('#msg_tire2vendorName'+(i+1)).html('').hide();
            		    }
  		    	  		
						
   		    	  		 if(directT2VendorDrtData == tier2name && certificatesDrtData == certs && enthincityDrtData == de &&  marketSectorListDrtData == marketSectorDrtData){
   	   		    	  		 
   		    	  			isValid=false;
   		    	  		 	$('#msg_tire2vendorName'+(i+1)).html('Selected vendor details already exists').show();
   	            	 		$('#msg_directAmt'+(i+1)).html('').hide();
   	            	 		$('#msg_marketSectorDrt'+(i+1)).html('').hide();
   	            	 		$('#msg_ethnicityDrt'+(i+1)).html('').hide();
   	                 		$('#msg_certificateDrt'+(i+1)).html('').hide();
     					}  
             		    
          		    }
       		       	else
	    	    	{
   	    				isValid=false;
   	    	 			$('#msg_directAmt'+(i+1)).html('Please fill direct spend amount').show();
                        $('#msg_certificateDrt'+(i+1)).html('').hide();
                        $('#msg_ethnicityDrt'+(i+1)).html('').hide();
                        $('#msg_marketSectorDrt'+(i+1)).html('').hide();
                        $('#msg_tire2vendorName'+(i+1)).html('').hide();
 		    	    }


                  }
     		       	else
	    	    	{
 	    				isValid=false;
 	    			  $('#msg_ethnicityDrt'+(i+1)).html('Please select ethnisity').show();
 	    	 		  $('#msg_directAmt'+(i+1)).html('').hide();
                      $('#msg_certificateDrt'+(i+1)).html('').hide();
                      $('#msg_marketSectorDrt'+(i+1)).html('').hide();
                      $('#msg_tire2vendorName'+(i+1)).html('').hide();
		    	    }
           		}
            	else
            	{
            		isValid=false;
       		     	$('#msg_certificateDrt'+(i+1)).html('Please select diversity certificate').show();
       		    	$('#msg_directAmt'+(i+1)).html('').hide();
       		    	$('#msg_ethnicityDrt'+(i+1)).html('').hide();
       		    	$('#msg_marketSectorDrt'+(i+1)).html('').hide();
       		    	$('#msg_tire2vendorName'+(i+1)).html('').hide();
                 
       		   	}
           	}
            else
           	{
            	if(i!=0)
            	{
           			isValid=false;
            	 	$('#msg_tire2vendorName'+(i+1)).html('Please select tier 2 vendor').show();
            	 	$('#msg_directAmt'+(i+1)).html('').hide();
            	 	$('#msg_marketSectorDrt'+(i+1)).html('').hide();
            	 	$('#msg_ethnicityDrt'+(i+1)).html('').hide();
                 	$('#msg_certificateDrt'+(i+1)).html('').hide();
           		}
           	}            	    	    	 
       }
        
        //Manually Validating direct Spend Amount and Diversity Certificat
      	//var isValid = true;
      	var tus1 = $('#directAmt').val();
      	var value4 = $('#directAmt').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		var certificates1=$('#certificateDrt').val();
      	var directT2Vendor = $('#tier2vendorName').val();    	
//       	var intRegex = /^\d+$/;
      	var intRegex = /(\d+(\.\d+)?)/
	  	if(certificates1 == 0 && directT2Vendor != 0)
      	{        
			isValid=false;
			$('#msg_certificateDrt').html('Please select diversity certificate').show();						
        }
	  	else if(directT2Vendor == 0 && certificates1 != 0)
   		{
       		isValid=false;
       		$('#msg_tire2vendorName1').html('Please select tier 2 vendor').show();
   		}
       	else if (certificates1 != 0 && !tus1 && tus1.length <= 0) 
       	{
            isValid = false;
            $('#msg_directAmt').html('Please fill direct spend amount').show();
        } 
       	else if (certificates1 != 0 && !intRegex.test(value4)) 
     	{
            isValid = false;
            $('#msg_directAmt').html('Please enter numeric value').show();
        }       	
       	else if(directT2Vendor == 0 && certificates1 == 0 && intRegex.test(value4))
   		{
       		isValid=false;
       		$('#msg_tire2vendorName1').html('Please select tier 2 vendor').show();
   		}
     	else 
       	{
     		$('#msg_directAmt').html('').hide();            
            $('#msg_tire2vendorName1').html('').hide();
            $('#msg_certificate').html('').hide();
       	} 
             		 
       if (isValid) 
       {
    	   var tuss = $('#totalUsSalesSelectedQtr').val();
           var tust = $('#totalUsSalestoSelectedQtr').val();
           var per =( parseInt(tust) / parseInt(tuss))*100;
           if(isNaN(per)) {
        	   per = 0;
           }
           $('#indirectPer').val(per.toFixed(2));
           
           if(per != "") {
           	setAllocatedAmount(per);        		
           }			           	
                   
            $("#report").find("tr:gt(0)").remove();
            $("#reportLast").find("tr:gt(0)").remove();
            var tier2Name = $(
                    "#tier2vendorName option[value='" + $('#tier2vendorName').val()
                    + "']").text();
            $('#report tr:last').after('<tr><td><font style="font-size: 15px;font-weight: bold;"><%=supplierName%></font></td><td></td><td></td></tr>');
            $('#report tr:last');
            var reportPer = $(
                    "#reportingPeriod option[value='" + $('#reportingPeriod').val()
                    + "']").text();
            $('#report tr:last').after(
                    '<tr><td>Reporting Period :</td><td>' + reportPer + '</td><td></td></tr>');

            $('#report tr:last').after(
                    '<tr><td>Total Supplier US Sales :</td><td>'
                    + accounting.formatMoney($('#totalUsSalesSelectedQtr').val(),"$ ",0) + '</td><td></td></tr>');
            $('#report tr:last').after('<tr><td>Total Sales to <b><%=compName%></b> :</td><td>'
                    +  accounting.formatMoney($('#totalUsSalestoSelectedQtr').val(),"$ ",0) + '</td><td></td></tr>');
            $('#report tr:last').after('<tr><td>Indirect Allocation Percentage :</td><td>'+ $('#indirectPer').val()+'%' + '</td><td></td></tr>');
            $('#report tr:last').after('<tr><td>Allocated Amount :</td><td>$'+ $('#allocatedamount').val()+' ' + '</td><td></td></tr>');
            
            $('#report tr:last')
                    .after(
                    '<tr><td colspan="4"><b>Direct Expenditures Reported</b></td></tr>');
            $('#report tr:last')
                    .after(
                    '<tr><td><b>Tier 2 Vendor Name</b></td><td><b>Diversity Classification</b></td><td><b>Ethinicity</b></td><td><b>Category</b></td><td><b>Direct Expenditures</b></td></tr>');
            var drtAmts = $("input[name='directAmt']").map(function() {
                return this.value;
            }).get();
            var drtCerts = $("select[name='certificateDrt']").map(function() {
                return this.value;
            }).get();
            var drtTier2Vens = $("input[name='vendorNamesn']").map(function() {
                return this.value;
            }).get();
            var deth = $("select[name='ethnicityDrt']").map(function() {
                return this.value;
            }).get();
            var dms = $("select[name='marketSectorDrt']").map(function() {
                return this.value;
            }).get();
            var totalDrt=parseFloat("0");
			if(drtAmts.length!=0 &&drtCerts.length!=0 &&drtTier2Vens.length!=0){
            for (var i = 0; i < drtAmts.length; i++) {
                if (drtTier2Vens[i] != '') {
                    var dtvn = drtTier2Vens[i];
                    var dcn = $(
                            "#certificateDrt option[value='" + drtCerts[i] + "']")
                            .text();
                    var det = $(
                            "#ethnicityDrt option[value='" + deth[i] + "']")
                            .text();
                    var dm = $(
                            "#marketSectorDrt option[value='" + dms[i] + "']")
                            .text();
                    $('#report tr:last').after(
                            '<tr><td>' + dtvn + '</td><td>' + dcn + '</td><td>'+det+'</td><td>'+dm+'</td><td>'  +  accounting.formatMoney(drtAmts[i],"$ ",0)
                            + '</td></tr>');                    
                    totalDrt=parseFloat(+totalDrt + +drtAmts[i]);	
                }
            }            
            $('#report tr:last').after(
            '<tr><td colspan="2" align="right"><b>Total: </b></td><td>'+accounting.formatMoney(totalDrt,"$ ",0)+'</td></tr>');
			}
            $('#report tr:last').after(
                    '<tr><td colspan="3"><b>Indirect Expenditures Reported</b></td></tr>');
            $('#report tr:last')
                    .after(
                    '<tr><td><b>Tier 2 Vendor Name</b></td><td><b>Diversity Classification</b></td><td><b>Ethnicity</b></td><td><b>Category</b></td><td><b>Indirect Purchase Amount</b></td></tr>');
            var inDrtTier2Vens = $("input[name='vendorName']").map(function() {
                return this.value;
            }).get();
            var idc = $("select[name='certificate']").map(function() {
                return this.value;
            }).get();
            var iddc = $("select[name='ethnicity']").map(function() {
                return this.value;
            }).get();
            var idms = $("select[name='marketSector']").map(function() {
                return this.value;
            }).get();
            var index = 0;
            var totalInDrt=parseFloat("0");
            var arr2 = $("input[name='indirectAmt']").map(
                    function() {

                        if (idc[index] != '' && this.value != '') {
                            var dn = $(
                                    "#certificates option[value='" + idc[index] + "']")
                                    .text();
                            var iddcv = '';
                            if (iddc[index] != '') {
                                iddcv = $(
                                        "#ethnicities option[value='" + iddc[index] + "']")
                                        .text();
                            }
                            var idm = '';
                            if (idms[index] != '') {
                                idm = $(
                                        "#marketSectorList option[value='" + idms[index] + "']")
                                        .text();
                            }
                            var idt2v = '';
                            if (inDrtTier2Vens[index] != '') {
                            	idt2v = inDrtTier2Vens[index];
                            }
                            $('#report tr:last').after(
                                    '<tr><td>' + idt2v + '</td><td>' + dn + '</td><td>' + iddcv + '</td><td>' + idm + '</td><td>'  +  accounting.formatMoney(this.value,"$ ",0)
                                    + '</td></tr>');
                            totalInDrt=parseFloat(+totalInDrt + +this.value);
                        }
                        index++;
                    }).get();

            //alert(arr1);
            //console.log(arr);
            $('#report tr:last').after(
            	'<tr><td colspan="3" align="right"><b>Total: </b></td><td>'+accounting.formatMoney(totalInDrt,"$ ",0)+'</td></tr>');
            $('#reportLast tr:last')
                    .after('<tr><td>If this information is correct, click <b>Submit</b> to submit the report to <b> <%=groupName%>'
                    + '</b></td></tr>');
            $('#reportLast tr:last')
                    .after(
                    '<tr><td>Please note that this information cannot be changed for this reporting period once submitted.  </td></tr>');
        } 
      	return isValid;        
    }
    
    function validate(tableId)
    {
    	var rowCount = $('#otherCertificate tr').length - 1;
    }

    // Email Validation
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(
                /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }
    
    function calculate() {
        var tuss = $('#totalUsSalesSelectedQtr').val();
        var tust = $('#totalUsSalestoSelectedQtr').val();
        var value = $('#totalUsSalesSelectedQtr').val().replace(/^\s\s*/, '')
                .replace(/\s\s*$/, '');
        var value2 = $('#totalUsSalestoSelectedQtr').val()
                .replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        var intRegex = /^\d+$/;

        if (intRegex.test(value) && intRegex.test(value2)) 
        {
            if ((!tuss && tuss.length <= 0) && (!tust && tust.length <= 0)) 
            {
                alert("Enter valid value to calculate the indirect allocation percentage");
            } 
            else 
            {
                var per =( parseInt(tust) / parseInt(tuss))*100;
                if(isNaN(per)) {
                	per = 0;
                }
                $('#indirectPer').val(per.toFixed(2));
                //if(per != "") {
                	setAllocatedAmount(per);        		
                //}             
            }
        } else {
            alert("Please enter numeric value in previous text fields.");
        }

    }
    
    function validatesales() {
        var tuss = $.trim($('#totalUsSalesSelectedQtr').val());
        var tust = $.trim($('#totalUsSalestoSelectedQtr').val());

        if (parseFloat(tuss) < parseFloat(tust)) {
            alert("Total US sales cannot be less than total sales of vendor");
            $('#totalUsSalesSelectedQtr').focus();
        }
    }
    
    function setAllocatedAmount(indirectPercentage) {
    	var totalIndirectAmt= parseInt(0);
        var indirectAmtLength = document.getElementsByName("indirectAmt").length;                
        if(indirectAmtLength > 0)
    	{                	
        	for(var i = 0; i < indirectAmtLength; i++) 
    		{
        		if(document.getElementsByName("indirectAmt")[i].value != '')
        			totalIndirectAmt += parseInt(document.getElementsByName("indirectAmt")[i].value);                		
    		}                	
    	}     
//     	$('#allocatedamount').val(((indirectPercentage/100)*totalIndirectAmt).toFixed(2));      
        $('#allocatedamount').val(Math.round(((indirectPercentage/100)*totalIndirectAmt).toFixed(2)));
    }
</script>
<style>
.custom-combobox {
	position: relative;
	display: inline-block;
}

.main-text-area {
	width: 75%;
	height: 75px;
}

.custom-combobox-toggle {
	position: absolute;
	top: 0;
	bottom: 0;
	margin-left: -1px;
	padding: 0;
	/* support: IE7 */
	*height: 1.7em;
	*top: 0.1em;
}

.custom-combobox-input {
	margin: 0;
	padding: 0.3em;
}

.main-text-box {
	width: 75%;
}

.label-col-wrapper1 {
	float: left;
	line-height: 25px;
	padding: 0 0 0 2%;
	width: 5%;
}

.btn-wrapper { /* width: 100%;
        margin: 0%; */
	
}

.main-list-box {
	width: 85%;
}

.ui-button-icon-only .ui-button-text, .ui-button-icons-only .ui-button-text
	{
	margin-bottom: -3px !important;
	margin-top: 2px !important;
	padding: 0.9em !important;
}

.ui-button {
	position: absolute;
}

.ui-autocomplete {
	max-height: 200px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
}
/* IE 6 doesn't support max-height
* we use height instead, but this forces the menu to always be this tall
*/

</style>
<section role="main" class="content-body card-margin">
<div class="row">
	<div class="col-lg-12 mx-auto">
		<header>
			<div id="successMsg">
				<html:messages id="msg" property="successMsg" message="true">
					<div class="alert alert-info nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
				<html:messages id="msg" property="saveSuccessMsg" message="true">
					<div class="alert alert-info nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
			</div>
			<div id="failureMsg">
				<html:messages id="msg" property="errorMsg" message="true">
					<div class="alert alert-danger nomargin">
						<bean:write name="msg" />
					</div>
				</html:messages>
			</div>
		</header>
			<div class="card-body" style="display:inline-block;">
			<form action="tier2Report.do?method=tire2ReportCreate" method="POST"
				id="tier2ReportForm" >
				<%--     <html:javascript formName="tier2ReportForm"/> --%>
				<input type="hidden" id="widgetId" /> 
				<input type="hidden" value=""
					id="isSubmit" /> <input type='hidden' name="issubmit" value="1" />
				<input type="hidden" name="tier2ReportMasterId"
					id="tier2ReportMasterId" value="" /> 
					<input type="button"
					id="retrieve" value="Recall Saved List" class="btn pull-right btn-default"
					onclick="retrieveReportMaster();"> 
					<input
					type="button" id="retrieve" value="Recall Previous Quarter"
					class="btn pull-right btn-tertiary mr-1" onclick="recallPreviousQuarterReport();"
					> 
					<input type="button" id="uploadFile"
					class="btn pull-right btn-tertiary mr-1" value="Choose Excel File"
					onclick="uploadDailog();"> 
					<input type="button"
					id="downloadFile" class="btn pull-right btn-primary mr-1"
					value="Download Excel File" onclick="excelHelpDialog();" />
					
				<!-- <input type="button" id="deleteExcelFile" class="btn" style="float : right;" value="Delete Unsubmitted Records" onclick="deleteUnsubmittedRecords();"/> -->
				<!-- Tabs -->
				<div id="wizard" class="swMain pt-2 tier-report">
					<ul>
						<li><a href="#step-1" style="border-radius: 5px 5px 5px 5px;">
								<label class="stepNumber">1</label> <span class="stepDesc">
									Indirect Expenditures <br /> <small>Step 1 of 3</small>
							</span>
						</a></li>
						<li><a href="#step-2" style="border-radius: 5px 5px 5px 5px;">
								<label class="stepNumber">2</label> <span class="stepDesc">
									Direct Expenditures<br /> <small>Step 2 of 3</small>
							</span>
						</a></li>
						<li><a href="#step-3" style="border-radius: 5px 5px 5px 5px;">
								<label class="stepNumber">3</label> <span class="stepDesc">
									Review Tier 2 Spend Report<br /> <small>Step 3 of 3</small>
							</span>
						</a></li>
					</ul>
					<div id="step-1" class="nano" style="min-height:500px; overflow:auto !important;">
						<header class="card-header">
							<h2 class="card-title pull-left">Tier 2 Spend Report</h2>
						</header>
						<table cellspacing="3" cellpadding="3" align="center" width="100%"
							class="main-table table table-bordered table-striped mb-0" id="reportDetail">
							<tr>
								<td align="center" colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<td class="col-sm-3">Spend Year :</td>
								<td class="col-sm-7"><input type="hidden" id="spendYearH" /> <select
									name="spendYear" id="spendYear" class="chosen-select form-control"
									style="width: 100%;">
										<option value="">- Select -</option>
										<!-- <option value="2015">2015</option>
						<option value="2014">2014</option>
						<option value="2013">2013</option>
						<option value="2012">2012</option>
						<option value="2011">2011</option>
						<option value="2010">2010</option>
						<option value="2009">2009</option>
						<option value="2008">2008</option>
						<option value="2007">2007</option>
						<option value="2006">2006</option>
						<option value="2005">2005</option>
						<option value="2004">2004</option>
						<option value="2003">2003</option>
						<option value="2002">2002</option>
						<option value="2001">2001</option>
						<option value="2000">2000</option> -->
								</select></td>
								<td align="left"><span id="msg_spendYear" class="error"></span>&nbsp;</td>
							</tr>
							<tr>
								<td class="col-sm-3">Reporting Period :</td>
								<td class="col-sm-7"><select id="reportingPeriod"
									name="reportingPeriod" class="chosen-select form-control"
									style="width: 100%;">
										<option value="">-Select-</option>
										<logic:present name="tier2ReportForm" property="reportPeriods">
											<logic:iterate id="period" name="tier2ReportForm"
												property="reportPeriods">
												<option value='<bean:write name="period" />'><bean:write
														name="period" /></option>
											</logic:iterate>
										</logic:present>
								</select></td>
								<td align="left"><span id="msg_reportingPeriod"
									class="error"></span>&nbsp;</td>
							</tr>
							<tr>
								<td class="col-sm-3">Total US Sales :$</td>
								<td class="col-sm-9"><input type="text"
									id="totalUsSalesSelectedQtr" name="totalUsSalesSelectedQtr"
									value="" class="main-text-box form-control" alt="" /></td>
								<td align="left"><span id="msg_totalUsSales" class="error"></span>&nbsp;</td>
							</tr>
							<tr>
								<td class="col-sm-3">Total Sales with <b><logic:present
											name="userDetails">
											<bean:write name="userDetails"
												property="currentCustomer.customerId.custName" />
										</logic:present> </b> :$
								</td>
								<td class="col-sm-9"><input type="text"
									id="totalUsSalestoSelectedQtr" name="totalUsSalestoSelectedQtr"
									value="" class="main-text-box form-control" alt="" onblur="validatesales();" />
								</td>
								<td align="left"><span id="msg_totalUsSalesto"
									class="error"></span>&nbsp;</td>
							</tr>
							<tr>
								<td class="col-sm-3">Indirect Allocation Percentage :</td>
								<td class="col-sm-9">&nbsp;&nbsp; <input type="text"
									id="indirectPer" name="indirectPer" value=""
									class="main-text-box form-control" alt="" readonly="readonly"
									onfocus="calculate()" />%
								</td>
								<td align="left"><span id="msg_indirectPer" class="error"></span>&nbsp;</td>
							</tr>
							<tr>
								<td class="col-sm-3">Allocated Amount :$</td>
								<td class="col-sm-9"><input type="text"
									id="allocatedamount" name="allocatedamount" value=""
									class="main-text-box form-control" alt="" readonly="readonly"
									onfocus="calculate()" /></td>
							</tr>
							<tr>
								<td class="col-sm-3">Comments :</td>
								<td class="col-sm-9"><textarea name="comments"
										class="main-text-area form-control" rows="" cols="" id="comments"></textarea></td>
								<td align="left"><span id="msg_comments" class="error"></span>&nbsp;</td>
							</tr>
						</table>
						<header class="card-header">
							<h2 class="card-title pull-left">Indirect Expenditures</h2>
						</header>
						<table cellspacing="3" cellpadding="3" align="center"
							id="certificateTable" width="100%" class="main-table table table-bordered table-striped mb-0">
							<tr>
								<td align="center" colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<th align="center">Tier2 Vendor</th>
								<th align="center">Diversity Classification</th>
								<th align="center">Ethnicity</th>
								<th align="center">Category</th>
								<th align="left">Indirect Spend($)</th>
							</tr>
							<tr>
								<td align="left"><input type="hidden"
									name="tier2indirectvendorNameCopy" alt="" value=""
									class="main-text-box" id="tier2indirectvendorNameCopy">
									<!-- <input type="text" name="vendorName" alt="" style="display: none;" title="Click the search image to select Vendors" value="" class="main-text-box" id="vendorName">
						<img src="images/magnifier.gif" onClick="getTier2Vendors();" style="display: none;"> -->
									<input type="hidden" name="tier2indirectvendorName" alt=""
									value="" class="main-text-box" id="tier2indirectvendorName">
									<input type="text" name="vendorName" alt=""
									title="Click the search image to select Vendors" value=""
									class="main-text-box" id="vendorNameIn"> <img
									src="images/magnifier.gif"
									onClick="getTier2Vendors('tier2indirectvendorName','vendorNameIn','tier2indirectvendorName');">

									<%-- <select id="tier2indirectvendorNameCopy"
							style="display: none;"><option value="">-Select-</option>
								<option value="Add New Tier2 Vendor">&lt;&lt;Add New
									Tier2 Vendor&gt;&gt;</option>
								<logic:present name="tier2ReportForm"
									property="indirectReportDtos">
									<logic:iterate id="vendor" name="tier2ReportForm"
										property="indirectReportDtos"> -
										<bean:define name="vendor" property="vendorId" id="id" />
										<option value='<%=id.toString()%>'><bean:write
												name="vendor" property="vendorName" /></option>
									</logic:iterate>
								</logic:present>
						</select> <select id="tier2indirectvendorName"
							name="tier2indirectvendorName"
							class="chosen-select tier2indirectvendorName"
							onchange="addNewTier2Vendor(this)">
								<option value="">-Select-</option>
								<option value="Add New Tier2 Vendor">&lt;&lt;Add New
									Tier2 Vendor&gt;&gt;</option>
								<logic:present name="tier2ReportForm"
									property="indirectReportDtos">
									<logic:iterate id="vendor" name="tier2ReportForm"
										property="reportDtos">
										<bean:define name="vendor" property="vendorId" id="id" />
										<option value='<%=id.toString()%>'><bean:write
												name="vendor" property="vendorName" /></option>
									</logic:iterate>
								</logic:present>
						</select> --%> <span id="msg_tire2indirectvendorName1"
									class="error"></span></td>
								<td align="left"><select name="certificate"
									id="certificates" class="chosen-select" onchange="change(this)"><option
											value="">-Select-</option>
										<logic:present name="tier2ReportForm" property="certificates">
											<logic:iterate id="certificate" name="tier2ReportForm"
												property="certificates">
												<bean:define id="id" name="certificate" property="id"></bean:define>
												<option value="<%=id.toString()%>"><bean:write
														name="certificate" property="certificateName"></bean:write></option>
											</logic:iterate>
										</logic:present>
								</select><span id="msg_certificate" class="error"></span></td>
								<td align="left"><select name="ethnicity" id="ethnicities"
									class="chosen-select"><option value="">-Select-</option>
										<logic:present name="tier2ReportForm" property="ethnicities">
											<logic:iterate id="ethnicity" name="tier2ReportForm"
												property="ethnicities">
												<bean:define id="id" name="ethnicity" property="id"></bean:define>
												<option value="<%=id.toString()%>"><bean:write
														name="ethnicity" property="ethnicity"></bean:write></option>
											</logic:iterate>
										</logic:present>
								</select><span id="msg_ethnicity" class="error"></span></td>

								<c:choose>
									<c:when test="${createdOnDate eq 'true'}">
										<td align="left"><select name="marketSector"
											id="marketSectorList" class="chosen-select"><option
													value="">-Select-</option>
												<logic:present name="tier2ReportForm"
													property="marketSubSectorList">
													<logic:iterate id="marketSubSector" name="tier2ReportForm"
														property="marketSubSectorList">
														<bean:define id="id" name="marketSubSector" property="id"></bean:define>
														<option value="<%=id.toString()%>"><bean:write
																name="marketSubSector" property="categoryDescription"></bean:write></option>
													</logic:iterate>
												</logic:present>
										</select><span id="msg_ethnicity" class="error"></span></td>
									</c:when>
									<c:otherwise>
										<td align="left"><select name="marketSector"
											id="marketSectorList" class="chosen-select"><option
													value="">-Select-</option>
												<logic:present name="tier2ReportForm"
													property="marketSectorList">
													<logic:iterate id="marketSector" name="tier2ReportForm"
														property="marketSectorList">
														<bean:define id="id" name="marketSector" property="id"></bean:define>
														<option value="<%=id.toString()%>"><bean:write
																name="marketSector" property="sectorDescription"></bean:write></option>
													</logic:iterate>
												</logic:present>
										</select><span id="msg_ethnicity" class="error"></span></td>
									</c:otherwise>
								</c:choose>
								<%-- <td align="left"><select name="marketSector"
							id="marketSectorList" class="chosen-select"><option
									value="">-Select-</option>
								<logic:present name="tier2ReportForm"
									property="marketSectorList">
									<logic:iterate id="marketSector" name="tier2ReportForm"
										property="marketSectorList">
										<bean:define id="id" name="marketSector" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="marketSector" property="sectorDescription"></bean:write></option>
									</logic:iterate>
								</logic:present>
						</select><span id="msg_ethnicity" class="error"></span></td> --%>
								<%-- <td align="left"><select name="marketSector"
							id="marketSectorList" class="chosen-select"><option
									value="">-Select-</option>
								<logic:present name="tier2ReportForm"
									property="marketSubSectorList">
									<logic:iterate id="marketSubSector" name="tier2ReportForm"
										property="marketSubSectorList">
										<bean:define id="id" name="marketSubSector" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="marketSubSector" property="categoryDescription"></bean:write></option>
									</logic:iterate>
								</logic:present>
						</select><span id="msg_ethnicity" class="error"></span></td> --%>
								<td align="left"><input type="text" name="indirectAmt"
									alt="" value="" class="main-text-box" id="indirectAmt">
									<span id="msg_indirectAmt" class="error"></span></td>
							</tr>
						</table>
						<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
							<input id="cmd1" type="button" value="Add Expenditure Line Item"
								class="btn btn-primary" onclick="validateStep1();" style="margin-top: 5%;" />
							<INPUT id="cmd23" type="button"
								value="Remove Expenditure Line Item" class="btn btn-default"
								style="margin-top: 5%;" />
							</div>
						</div>
					</footer>
					</div>

					<div id="step-2" style="height: 500px; overflow: auto;">
						<h2 class="StepTitle">Direct Expenditures</h2>
						<table cellspacing="3" cellpadding="3" align="center"
							id="directspend" width="100%" class="main-table main-table table table-bordered table-striped mb-0">
							<tr>
								<td align="center" colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<th>Tier 2 Vendor</th>
								<th>Diversity Classification</th>
								<th>Ethnicity</th>
								<th>Category</th>
								<th>Direct Spend($)</th>
							</tr>
							<tr>
								<td align="left"><input type="hidden"
									name="tier2vendorNameCopy" alt="" value=""
									class="main-text-box form-control" id="tier2vendorNameCopy"> <!-- <input type="text" name="vendorName" alt="" style="display: none;" title="Click the search image to select Vendors" value="" class="main-text-box" id="vendorName">
						<img src="images/magnifier.gif" onClick="getTier2Vendors();" style="display: none;"> -->
									<input type="hidden" name="tier2vendorName" alt="" value=""
									class="main-text-box form-control" id="tier2vendorName"> <input
									type="text" name="vendorNamesn" alt=""
									title="Click the search image to select Vendors" value=""
									class="main-text-box" id="vendorNamesn"> <img
									src="images/magnifier.gif"
									onClick="getTier2Vendors('tier2vendorName','vendorNamesn','tier2vendorName');">
									<%-- <select id="tier2vendorNameCopy"
							style="display: none;"><option value="">-Select-</option>
								<option value="Add New Tier2 Vendor">&lt;&lt;Add New
									Tier2 Vendor&gt;&gt;</option>
								<logic:present name="tier2ReportForm" property="reportDtos">
									<logic:iterate id="vendor" name="tier2ReportForm"
										property="reportDtos">
										<bean:define name="vendor" property="vendorId" id="id" />
										<option value='<%=id.toString()%>'><bean:write
												name="vendor" property="vendorName" /></option>
									</logic:iterate>
								</logic:present>
						</select> <select id="tier2vendorName" name="tier2vendorName"
							class="chosen-select tier2vendorName"
							onchange="addNewTier2Vendor(this)">
								<option value="">-Select-</option>
								<option value="Add New Tier2 Vendor">&lt;&lt;Add New
									Tier2 Vendor&gt;&gt;</option>
								<logic:present name="tier2ReportForm" property="reportDtos">
									<logic:iterate id="vendor" name="tier2ReportForm"
										property="reportDtos">
										<bean:define name="vendor" property="vendorId" id="id" />
										<option value='<%=id.toString()%>'><bean:write
												name="vendor" property="vendorName" /></option>
									</logic:iterate>
								</logic:present>
						</select> --%> <span id="msg_tire2vendorName1" class="error"></span></td>
								<td align="left"><select name="certificateDrt"
									id="certificateDrt" class="chosen-select"><option
											value="">-Select-</option>
										<logic:present name="tier2ReportForm" property="certificates">
											<logic:iterate id="certificate" name="tier2ReportForm"
												property="certificates">
												<bean:define id="id" name="certificate" property="id"></bean:define>
												<option value="<%=id.toString()%>"><bean:write
														name="certificate" property="certificateName"></bean:write></option>
											</logic:iterate>
										</logic:present>
								</select> <span id="msg_certificateDrt1" class="error"></span></td>
								<td align="left"><select name="ethnicityDrt"
									id="ethnicityDrt" class="chosen-select"><option
											value="">-Select-</option>
										<logic:present name="tier2ReportForm" property="ethnicities">
											<logic:iterate id="ethnicity" name="tier2ReportForm"
												property="ethnicities">
												<bean:define id="id" name="ethnicity" property="id"></bean:define>
												<option value="<%=id.toString()%>"><bean:write
														name="ethnicity" property="ethnicity"></bean:write></option>
											</logic:iterate>
										</logic:present>
								</select><span id="msg_ethnicityDrt1" class="error"></span></td>

								<c:choose>
									<c:when test="${createdOnDate eq 'true'}">
										<td align="left"><select name="marketSectorDrt"
											id="marketSectorDrt" class="chosen-select"><option
													value="">-Select-</option>
												<logic:present name="tier2ReportForm"
													property="marketSubSectorList">
													<logic:iterate id="marketSubSector" name="tier2ReportForm"
														property="marketSubSectorList">
														<bean:define id="id" name="marketSubSector" property="id"></bean:define>
														<option value="<%=id.toString()%>"><bean:write
																name="marketSubSector" property="categoryDescription"></bean:write></option>
													</logic:iterate>
												</logic:present>
										</select><span id="msg_marketSectorDrt1" class="error"></span></td>
									</c:when>
									<c:otherwise>
										<td align="left"><select name="marketSectorDrt"
											id="marketSectorDrt" class="chosen-select"><option
													value="">-Select-</option>
												<logic:present name="tier2ReportForm"
													property="marketSectorList">
													<logic:iterate id="marketSector" name="tier2ReportForm"
														property="marketSectorList">
														<bean:define id="id" name="marketSector" property="id"></bean:define>
														<option value="<%=id.toString()%>"><bean:write
																name="marketSector" property="sectorDescription"></bean:write></option>
													</logic:iterate>
												</logic:present>
										</select><span id="msg_marketSectorDrt1" class="error"></span></td>
									</c:otherwise>
								</c:choose>
								<%-- <td align="left"><select name="marketSectorDrt"
							id="marketSectorDrt" class="chosen-select"><option
									value="">-Select-</option>
								<logic:present name="tier2ReportForm"
									property="marketSectorList">
									<logic:iterate id="marketSector" name="tier2ReportForm"
										property="marketSectorList">
										<bean:define id="id" name="marketSector" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="marketSector" property="sectorDescription"></bean:write></option>
									</logic:iterate>
								</logic:present>
						</select><span id="msg_marketSectorDrt1" class="error"></span></td> --%>
								<%-- <td align="left"><select name="marketSectorDrt"
							id="marketSectorDrt" class="chosen-select"><option
									value="">-Select-</option>
								<logic:present name="tier2ReportForm"
									property="marketSubSectorList">
									<logic:iterate id="marketSubSector" name="tier2ReportForm"
										property="marketSubSectorList">
										<bean:define id="id" name="marketSubSector" property="id"></bean:define>
										<option value="<%=id.toString()%>"><bean:write
												name="marketSubSector" property="categoryDescription"></bean:write></option>
									</logic:iterate>
								</logic:present>
						</select><span id="msg_marketSectorDrt1" class="error"></span></td> --%>
								<td align="left"><input type='text' id='directAmt'
									name='directAmt' class='main-text-box'> <span
									id="msg_directAmt1" class="error"></span><span
									id="msg_certificateDrt1" class="error"></span></td>
							</tr>
						</table>

						<div class="btn-wrapper text-center" style="width: 100%; margin: 0%;">
							<input type='button' value='Add Expenditure Line Item'
								id='addButton1' class="btn btn-primary" style="margin-top: 2%;"
								onclick="validateStep2();" /> <input type='button'
								value='Remove Expenditure Line Item' id='removeButton'
								class="btn btn-default" style="margin-top: 2%;">
						</div>
					</div>
					<div id="step-3" style="height: 300px; overflow: auto;">
						<h2 class="StepTitle">Review Tier 2 Spend Report and Submit</h2>
						<logic:present name="userDetails">
							<bean:define id="logoPath" name="userDetails"
								property="settings.logoPath"></bean:define>
							<input type="button" value="Export" class="btn btn-primary"
								style="float: right"
								onclick="generateTier2SpendReport('<%=logoPath%>');">
						</logic:present>
						<table cellspacing="4" cellpadding="4" align="center" id="report"
							width="100%" class="main-table main-table table table-bordered table-striped mb-0">
							<TR>
								<td></td>
								<td></td>
								<td></td>
							</TR>
						</table>
						<table cellspacing="3" cellpadding="3" align="center"
							id="reportLast" width="100%" class="main-table">
							<TR>
								<td></td>
							</TR>
						</table>
					</div>
				</div>
				<!-- End SmartWizard Content -->
			</form>
			</div>
			<div class="clear"></div>
		
		<div id="dialog2" title="Create New Tier2 Vendor"
			style="display: none;">
			<jsp:include page="../vendorpages/createTier2VendorPopup.jsp"></jsp:include>
		</div>
		<div id="dialog1" title="Choose vendor to submit"
			style="display: none;">
			<logic:present name="reportMasters">
				<table class="main-table main-table table table-bordered table-striped mb-0">
					<bean:size id="size" name="reportMasters" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="">Reporting Period</td>
								<td class="">Start Date</td>
								<td class="">End Date</td>
								<td class="">Total Sales</td>
								<td class="">Total Sales to Company</td>
								<td class="">Created on</td>
								<td class="">Modified on</td>
								<td class="">Actions</td>
							</tr>
						</thead>
						<fmt:setLocale value="en_US" />
						<logic:iterate name="reportMasters" id="reportMasterList">
							<tbody>
								<tr>
									<td><bean:write name="reportMasterList"
											property="reportingPeriod" /></td>
									<td><bean:write name="reportMasterList"
											property="reportingStartDate" /></td>
									<td><bean:write name="reportMasterList"
											property="reportingEndDate" /></td>
									<td align="right"><bean:define name="reportMasterList"
											property="totalSales" id="totalSales" /> <fmt:formatNumber
											value="${totalSales}" type="currency" /></td>
									<td align="right"><bean:define name="reportMasterList"
											property="totalSalesToCompany" id="totalSalesToCompany" /> <fmt:formatNumber
											value="${totalSalesToCompany}" type="currency" /></td>
									<bean:define id="tier2ReportMasterid" name="reportMasterList"
										property="tier2ReportMasterid"></bean:define>
									<td><bean:write name="reportMasterList"
											property="createdOn" format="yyyy-MM-dd hh:mma" /></td>
									<td><bean:write name="reportMasterList"
											property="modifiedOn" format="yyyy-MM-dd hh:mma" /></td>
									<td><html:link
											action="/tier2Report.do?method=editTier2ReportMaster"
											paramId="id" paramName="tier2ReportMasterid">Select</html:link>
									</td>
								</tr>
							</tbody>
						</logic:iterate>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
					No such records
			</logic:equal>
				</table>
			</logic:present>
			<logic:notPresent name="reportMasters">
				<p>There is no record(s) available.</p>
			</logic:notPresent>
		</div>
	</div>
</div>
</section>

		<script type="text/javascript">

	//var count=0;
	function addRow(tableID) {

		// alert(count++);

		//debugger;
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        var createdOnDate = true;
        var colCount = 5;//table.rows[1].cells.length;
        for (var i = 0; i < colCount; i++) {
            var newcell = row.insertCell(i);
            newcell.innerHTML = table.rows[2].cells[i].innerHTML;
            //alert(newcell.childNodes);
             if (i == 0) {
            
                /* newcell.innerHTML = '<select  name="tier2indirectvendorName" class="chosen-select tier2indirectvendorName" onchange="addNewTier2Vendor(this)">'
                        + $('#tier2indirectvendorNameCopy').html() + '</select><span id="msg_tire2indirectvendorName1'+(rowCount-1)+'" class="error"></span>';
                newcell.childNodes[0].id = "tier2indirectvendorName" + rowCount;
                newcell.childNodes[0].name = "tier2indirectvendorName";
                $("#tier2indirectvendorName" + rowCount).select2(); 
 */
 			var vId="tier2indirectvendorName"+ rowCount;
			var vName="vendorNameIn"+rowCount;
            newcell.innerHTML = '<input type="hidden" name="tier2indirectvendorName" alt="" value="" class="main-text-box" id="tier2indirectvendorName">'
                    + $('#tier2indirectvendorNameCopy').html() + '<input type="text" name="vendorName" alt="" title="Click the search image to select Vendors" value="" class="main-text-box" id="vendorNameIn">'
                    + '<img src="images/magnifier.gif" onClick="getTier2Vendors(\''+vId+'\',\''+vName+'\',\'tier2indirectvendorName\');"><span id="msg_tire2indirectvendorName1'+(rowCount-1)+'" class="error"></span>';
            newcell.childNodes[0].id = "tier2indirectvendorName" + rowCount;
            newcell.childNodes[0].name = "tier2indirectvendorName";
            newcell.childNodes[1].id = "vendorNameIn" + rowCount;
            newcell.childNodes[1].name = "vendorName";
//             newcell.childNodes[1].value="${vendorNames1}";
            }
              if (i == 1) {
                	 newcell.innerHTML = '<select id="certificate' + rowCount+'" name="certificate" class="chosen-select fields" onchange="change(this)" >'
                         + $('#certificates').html() + '</select><span id="msg_certificate'+(rowCount-1)+'" class="error"></span>';
                         newcell.childNodes[0].selectedIndex = 0;
                	 $("#certificate" + rowCount).select2();
             } else if (i == 2) {
                	 newcell.innerHTML = '<select id="ethnicity' + rowCount+'" name="ethnicity" class="chosen-select" >'
                     + $('#ethnicities').html() + '</select><span id="msg_ethnicity'+(rowCount-1)+'" class="error"></span>';
                     document.getElementById('ethnicity' + rowCount).selectedIndex = 0;                     
                	 $("#ethnicity" + rowCount).select2();
             } else if (i == 3) {
                	 newcell.innerHTML = '<select id="marketSector' + rowCount+'" name="marketSector" class="chosen-select" >'
                     + $('#marketSectorList').html() + '</select><span id="msg_marketSector'+(rowCount-1)+'" class="error"></span>';
                     document.getElementById('marketSector' + rowCount).selectedIndex = 0;                     
                	 $("#marketSector" + rowCount).select2();
             }
            switch (newcell.childNodes[0].type) {
                case "text":
                    newcell.innerHTML = " <input  class='main-text-box' name='indirectAmt'/><span id='msg_indirectAmt"+(rowCount-1)+"' class='error'></span>";
                    newcell.childNodes[0].value = "";
                    break;
            }
        }
    }

    function deleteRow(tableID) {
        var rowCount = $('#' + tableID + ' tr').length;
        if (rowCount > 3) {
            $('#' + tableID + ' tr:last').remove();
        }
    }


   
    function addRowDirect(tableID) {

        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        var createdOnDate = true;
        var colCount = table.rows[1].cells.length;
        for (var i = 0; i < colCount; i++) {
            var newcell = row.insertCell(i);
            if (i == 0) {

            	var venId="tier2vendorName"+ rowCount;
	    		var venName="vendorNamesn"+rowCount;
            	newcell.innerHTML = '<input type="hidden" name="tier2vendorName" alt="" value="" class="main-text-box" id="tier2vendorName">'
                    + $('#tier2vendorNameCopy').html()+'<input type="text" name="vendorNamesn" alt="" title="Click the search image to select Vendors" value="" class="main-text-box" id="vendorNamesn">'
                    + '<img src="images/magnifier.gif" onClick="getTier2Vendors(\''+venId+'\',\''+venName+'\',\'tier2vendorName\');"><span id="msg_tire2vendorName'+(rowCount-1)+'" class="error"></span>';
            newcell.childNodes[0].id = "tier2vendorName" + rowCount;
            newcell.childNodes[0].name = "tier2vendorName";
<%--             newcell.childNodes[0].value="<%=vendorName%>"; --%>
            newcell.childNodes[1].id = "vendorNamesn" + rowCount;
            newcell.childNodes[1].name = "vendorNamesn";
                
                /* newcell.innerHTML = '<select  name="tier2vendorName" class="chosen-select tier2vendorName" onchange="addNewTier2Vendor(this)">'
                        + $('#tier2vendorNameCopy').html() + '</select><span id="msg_tire2vendorName'+(rowCount-1)+'" class="error"></span>';
                newcell.childNodes[0].id = "tier2vendorName" + rowCount;
                newcell.childNodes[0].name = "tier2vendorName";
                $("#tier2vendorName" + rowCount).select2(); */
            } else if (i==1){
            	newcell.innerHTML = '<select id="certificateDrt'+rowCount+'" name="certificateDrt" class="chosen-select ">'
               						 + $('#certificateDrt').html() + '</select><span id="msg_certificateDrt'+(rowCount-1)+'" class="error"></span>';
               	 document.getElementById('certificateDrt' + rowCount).selectedIndex = 0;
        		 $("#certificateDrt"+ rowCount).select2();
            }else if (i == 2) {
	           	 newcell.innerHTML = '<select id="ethnicityDrt' + rowCount+'" name="ethnicityDrt" class="chosen-select" >'
	             + $('#ethnicityDrt').html() + '</select><span id="msg_ethnicityDrt'+(rowCount-1)+'" class="error"></span>';
	             document.getElementById('ethnicityDrt' + rowCount).selectedIndex = 0;                     
	        	 $("#ethnicityDrt" + rowCount).select2();
		     }else if (i == 3) {
            	 newcell.innerHTML = '<select id="marketSectorDrt' + rowCount+'" name="marketSectorDrt" class="chosen-select" >'
                 + $('#marketSectorDrt').html() + '</select><span id="msg_marketSectorDrt'+(rowCount-1)+'" class="error"></span>';
                 document.getElementById('marketSectorDrt' + rowCount).selectedIndex = 0;                     
            	 $("#marketSectorDrt" + rowCount).select2();
         	}else{
                newcell.innerHTML = table.rows[2].cells[i].innerHTML;
                switch (newcell.childNodes[0].type) {
                    case "text":
                        newcell.innerHTML = " <input  class='main-text-box' name='directAmt'/><span	id='msg_directAmt"+(rowCount-1)+"' class='error'></span>";
                        newcell.childNodes[0].value = "";
                        break;
                }
            }
        }
    }
    function deleteRowDirect(tableID) {
        var rowCount = $('#' + tableID + ' tr').length;
        if (rowCount > 3) {
            $('#' + tableID + ' tr:last').remove();
        }
    }
    
    $(document).ready(function() {
        var increment = 5;
       $("#addButton1").click(function() {
    	   var flag=validateStep2();    
    	   if(flag!=false){    		  
            addRowDirect('directspend');
            increment++;
    	   }
        });
        $("#removeButton").click(function() {
            if (increment == 2) {
                return false;
            }
            increment--;
            deleteRowDirect('directspend');
            //	$("#TextBoxDiv" + increment).remove();
        });
        
        var increment = 5;
        $("#cmd1").click(function() {
     	   var flag=validateStep1();     	 
     	   if(flag!=false){     		
     	   	addRow('certificateTable');
             	increment++;
     	   }
         });
        $("#cmd23").click(function() {
            if (increment == 2) {
                return false;
            }
            increment--;
            deleteRow('certificateTable');
            //	$("#TextBoxDiv" + increment).remove();
        });
        
        var value1 = $("#totalUsSalesSelectedQtr").val();
        var value2 = $("#totalUsSalestoSelectedQtr").val();
        if(value1 == "NaN" && value2 == "NaN"){
        	$("#totalUsSalesSelectedQtr").val('');
            $("#totalUsSalestoSelectedQtr").val('');
        }
    });
    
   
	function addNewTier2Vendor(ele) {
		//$('input[type=text]').val('');
		if (ele.value == 'Add New Tier2 Vendor') {
			$("#widgetId").val(ele.id);
			$("#dialog2").css({
				"display" : "block"
			});
			$("#dialog2").dialog({	
				width : 1000,
				modal : true,
				closeOnEscape: false,
				open: function() {
	                $(this).parent().find(".ui-dialog-titlebar-close").hide();
	            },
				/*open: function(event, ui) { 
					$(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); 
				},*/				
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});			
		} else if (ele.value == 'Existing Tier2 Vendor') {
			
			$("#widgetId").val(ele.id);
			$("#dialog3").css({
				"display" : "block"
			});
			$("#dialog3").dialog({
				width : 800,
				modal : true,
				closeOnEscape: false,
				open: function(event, ui) { 
					$(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); 
				},
				show : {
					effect : "scale",
					duration : 1000
				},
				hide : {
					effect : "scale",
					duration : 1000
				}
			});
			tier2VendorSearchGrid();
		}
	}

	function retrieveReportMaster() {
		var masterId = $("#tier2ReportMasterId").val();
		if (masterId == null || masterId == "") {
			$("#dialog1").css({
				"display" : "block"
			});
			$("#dialog1").dialog({
				minWidth : 900,
				//stack: true,
				modal : true
			});
		}
	}

	function change(value) {

		//disable ethnicity when WBE is selected
		var certificate = $.trim($(
				"#certificates option[value='" + value.value + "']").text());
		var lastChar = value.id.substr(value.id.length - 1);
		var ethnicityId='';
		if(lastChar=='s'){
			ethnicityId='ethnicities';
		}else{
			ethnicityId='ethnicity'+lastChar;
		}
		if (certificate == "Women Owned Business"
				|| certificate == "Women Business Enterprise") {
			var textToFind = 'Caucasian';
			var dd = document.getElementById(ethnicityId);
			for ( var i = 0; i < dd.options.length; i++) {
				if (dd.options[i].text === textToFind) {
					dd.selectedIndex = i;
					$('#'+ethnicityId+' option[value=' + dd.options[i].value + ']').attr('selected', true);
					break;
				}
			}
			$("#" + ethnicityId).prop('disabled', true);
		} else {
			$("#" + ethnicityId).prop('disabled', false);
		}
		$('#'+ethnicityId).select2();
	}
</script>
		<style>
.main-table td {
	border: 0px solid #E9EAEA;
}
</style>

		<div class="clear"></div>

		<!-- To set value while editing -->
		<logic:present name="tier2ReportForm" property="tier2ReportMasterId">
			<bean:define id="tier2ReportMasterId" name="tier2ReportForm"
				property="tier2ReportMasterId"></bean:define>
			<script>$("#tier2ReportMasterId").val("<%=tier2ReportMasterId%>"); </script>
		</logic:present>
		<logic:present name="tier2ReportForm" property="spendYear">
			<bean:define id="spendYear" name="tier2ReportForm"
				property="spendYear"></bean:define>
			<script>$("#spendYearH").val(<%=spendYear%>);</script>
		</logic:present>
		<logic:present name="tier2ReportForm" property="reportingPeriod">
			<bean:define id="reportingPeriod" name="tier2ReportForm"
				property="reportingPeriod"></bean:define>
			<script>$("#reportingPeriod").val("<%=reportingPeriod%>");</script>
		</logic:present>
		<logic:present name="tier2ReportForm"
			property="totalUsSalesSelectedQtr">
			<bean:define id="total" name="tier2ReportForm"
				property="totalUsSalesSelectedQtr"></bean:define>
			<script>
		var totalAmt = Math.round(<%=total%>);
		$("#totalUsSalesSelectedQtr").val(totalAmt);
	</script>
		</logic:present>
		<logic:present name="tier2ReportForm"
			property="totalUsSalestoSelectedQtr">
			<bean:define id="totalTo" name="tier2ReportForm"
				property="totalUsSalestoSelectedQtr"></bean:define>
			<script>
		var totalAmt = Math.round(<%=totalTo%>);
		$("#totalUsSalestoSelectedQtr").val(totalAmt);
	</script>
		</logic:present>
		<logic:present name="tier2ReportForm" property="indirectPer">
			<bean:define id="indirectPercentage" name="tier2ReportForm"
				property="indirectPer"></bean:define>
			<script>$("#indirectPer").val("<%=indirectPercentage%>");</script>
		</logic:present>
		<logic:present name="tier2ReportForm" property="comments">
			<bean:define id="comments" name="tier2ReportForm" property="comments"></bean:define>
			<script>$("#comments").val("<%=comments%>");</script>
		</logic:present>
		<logic:present name="directExpensesSession">
			<script>
		var count1 = 1;
		var table = document.getElementById("directspend");
		var rowCount = table.rows.length ;
	</script>
			<logic:iterate id="direct" name="directExpensesSession" indexId="i">
				<bean:define id="vendorName" name="direct"
					property="tier2Vendorid.id"></bean:define>
				<c:set var="vendorNames" value="${direct.tier2Vendorid.vendorName}" />
				<%-- <bean:define id="vendorNames" name="direct" property="tier2Vendorid.vendorName"></bean:define> --%>
				<script>
		if(count1 >= 2){
			var table = document.getElementById("directspend");
	        var rowCount = table.rows.length;
	        var row = table.insertRow(rowCount);
<%-- 	        alert(<%=vendorNames%>); --%>
	        var colCount = table.rows[1].cells.length;
	        for (var i = 0; i < colCount; i++) {
	            var newcell = row.insertCell(i);
	            if (i == 0) {

	            var venId="tier2vendorName"+ rowCount;
	    		var venName="vendorNamesn"+rowCount;
				
	            newcell.innerHTML = '<input type="hidden" name="tier2vendorName" alt="" value="" class="main-text-box" id="tier2vendorName">'
                        + $('#tier2vendorNameCopy').html()+'<input type="text" name="vendorNamesn" alt="" title="Click the search image to select Vendors" value="" class="main-text-box" id="vendorNamesn">'
                        + '<img src="images/magnifier.gif" onClick="getTier2Vendors(\''+venId+'\',\''+venName+'\',\'tier2vendorName\');"><span id="msg_tire2vendorName'+(rowCount-1)+'" class="error"></span>';
                newcell.childNodes[0].id = "tier2vendorName" + rowCount;
                newcell.childNodes[0].name = "tier2vendorName";
                newcell.childNodes[0].value="<%=vendorName%>";
                newcell.childNodes[1].id = "vendorNamesn" + rowCount;
                newcell.childNodes[1].name = "vendorNamesn";
                newcell.childNodes[1].value="${vendorNames}";
		            
	                <%-- newcell.innerHTML = '<select  name="tier2vendorName" class="chosen-select tier2vendorName" onchange="addNewTier2Vendor(this)">'
	                        + $('#tier2vendorNameCopy').html() + '</select><span id="msg_tire2vendorName'+(rowCount-1)+'" class="error"></span>';
	                newcell.childNodes[0].id = "tier2vendorName" + rowCount;
	                newcell.childNodes[0].name = "tier2vendorName";
	                newcell.childNodes[0].value="<%=vendorName%>"; --%>
	               // $("#tier2vendorName" + rowCount).select2();

	            } else if(i==1){
	            	newcell.innerHTML = '<select id="certificateDrt'+rowCount+'" name="certificateDrt" class="chosen-select">'
                    				+ $('#certificateDrt').html() + '</select><span id="msg_certificateDrt'+(rowCount-1)+'" class="error"></span>';
	            	// $("#certificateDrt" + rowCount).select2();
	            }else if (i == 2) {
		           	 newcell.innerHTML = '<select id="ethnicityDrt' + rowCount+'" name="ethnicityDrt" class="chosen-select" >'
		             + $('#ethnicityDrt').html() + '</select><span id="msg_ethnicityDrt'+(rowCount-1)+'" class="error"></span>';
		        	 //$("#ethnicityDrt" + rowCount).select2();
			     }else if (i == 3) {
	            	 newcell.innerHTML = '<select id="marketSectorDrt' + rowCount+'" name="marketSectorDrt" class="chosen-select" >'
	                 + $('#marketSectorDrt').html() + '</select><span id="msg_marketSectorDrt'+(rowCount-1)+'" class="error"></span>';
	         	}else{
	                newcell.innerHTML = table.rows[2].cells[i].innerHTML;
	                //alert(newcell.childNodes);
	                switch (newcell.childNodes[0].type) {
	                    case "text":
	                        newcell.innerHTML = " <input  class='main-text-box' name='directAmt' id='directAmt"+rowCount+"'/><span id='msg_directAmt"+(rowCount-1)+"' class='error'></span>";
	                        break;
	                }
	            }
	        }
		}
	</script>
				<logic:present name="direct" property="tier2Vendorid.id">

					<script>
			if(count1==1){				
				$("#tier2vendorName").val("<%=vendorName%>"); 
				$("#vendorNamesn").val("${vendorNames}");
				//$("#tier2vendorName").select2();
			}
			</script>
				</logic:present>
				<logic:present name="direct" property="certificateId.id">
					<bean:define id="certName" name="direct"
						property="certificateId.id"></bean:define>
					<script>
			
			if(count1==1){
				$("#certificateDrt").val("<%=certName%>");
				//$("#certificateDrt").select2();
			}else{
				$("#certificateDrt"+rowCount).val("<%=certName%>");
				//$("#certificateDrt"+rowCount).select2();
			}
			
			</script>
				</logic:present>
				<logic:present name="direct" property="ethnicityId.id">
					<bean:define id="ethnicityName" name="direct"
						property="ethnicityId.id"></bean:define>
					<script>
			
			if(count1==1){
				$("#ethnicityDrt").val("<%=ethnicityName%>");
				//$("#certificateDrt").select2();
			}else{
				$("#ethnicityDrt"+rowCount).val("<%=ethnicityName%>");
				//$("#certificateDrt"+rowCount).select2();
			}
			
			</script>
				</logic:present>
				<logic:present name="direct" property="marketSector.id">
					<bean:define id="marketSectorName" name="direct"
						property="marketSector.id"></bean:define>
					<script>
			
			if(count1==1){
				$("#marketSectorDrt").val("<%=marketSectorName%>");
				//$("#certificateDrt").select2();
			}else{
				$("#marketSectorDrt"+rowCount).val("<%=marketSectorName%>");
				//$("#certificateDrt"+rowCount).select2();
			}
			
			</script>
				</logic:present>
				<logic:present name="direct" property="marketSubSector.id">
					<bean:define id="marketSectorName" name="direct"
						property="marketSubSector.id"></bean:define>
					<script>
			
			if(count1==1){
				$("#marketSectorDrt").val("<%=marketSectorName%>");
				//$("#certificateDrt").select2();
			}else{
				$("#marketSectorDrt"+rowCount).val("<%=marketSectorName%>");
				//$("#certificateDrt"+rowCount).select2();
			}
			
			</script>
				</logic:present>
				<logic:present name="direct" property="dirExpenseAmt">
					<bean:define id="directAmt" name="direct" property="dirExpenseAmt"></bean:define>
					<script>
			if(count1==1){
				$("#directAmt").val(<%=directAmt%>);
			}else{
				$("#directAmt"+rowCount).val(<%=directAmt%>);
			}
				count1 = count1 + 1;
			</script>
				</logic:present>

			</logic:iterate>
		</logic:present>
		<logic:present name="indirectExpensesSession">
			<script>
		var count = 1;
		var table = document.getElementById("certificateTable");
		var rowCount = table.rows.length ;
	</script>
			<logic:iterate id="indirect" name="indirectExpensesSession"
				indexId="i">
				<c:set var="vendorName1" value="" />
				<logic:present name="indirect" property="tier2IndirectVendorId">
					<c:set var="vendorName1"
						value="${indirect.tier2IndirectVendorId.id}" />
					<c:set var="vendorNames1"
						value="${indirect.tier2IndirectVendorId.vendorName}" />
				</logic:present>
				<bean:define id="certName" name="indirect"
					property="certificateId.id"></bean:define>
				<%-- <bean:define id="ethnicity" name="indirect" property="ethnicityId.id"></bean:define> --%>
				<logic:present name="indirect" property="ethnicityId">
					<c:set var="ethnicity" value="${indirect.ethnicityId.id}" />
				</logic:present>
				<%-- <logic:present name="indirect" property="marketSector.id">
			<bean:define id="marketSectorName" name="indirect"
				property="marketSector.id"></bean:define>
			<script>
			
			if(count1==1){
				$("#marketSector").val("<%=marketSectorName%>");
				//$("#certificateDrt").select2();
			}else{
				$("#marketSector"+rowCount).val("<%=marketSectorName%>");
				//$("#certificateDrt"+rowCount).select2();
			}
			
			</script>
		</logic:present> --%>
				<logic:present name="indirect" property="marketSubSector.id">
					<bean:define id="marketSubSectorName" name="indirect"
						property="marketSubSector.id"></bean:define>
					<script>
			
			if(count1==1){
				$("#marketSector").val("<%=marketSubSectorName%>");
				//$("#certificateDrt").select2();
			}else{
				$("#marketSector"+rowCount).val("<%=marketSubSectorName%>");
				//$("#certificateDrt"+rowCount).select2();
			}
			
			</script>
				</logic:present>

				<script>
		 rowCount = table.rows.length ;
		if(count >= 2){
			var row = table.insertRow(rowCount);
	        var colCount = 5;//table.rows[1].cells.length;
	        for (var i = 0; i < colCount; i++) {
	            var newcell = row.insertCell(i);
	            newcell.innerHTML = table.rows[2].cells[i].innerHTML;
	            if (i == 0) {

	            	var vId="tier2indirectvendorName"+ rowCount;
	    			var vName="vendorNameIn"+rowCount;
	            	
	                newcell.innerHTML = '<input type="hidden" name="tier2indirectvendorName" alt="" value="" class="main-text-box" id="tier2indirectvendorName">'
	                        + $('#tier2indirectvendorNameCopy').html()+'<input type="text" name="vendorName" alt="" title="Click the search image to select Vendors"  class="main-text-box" id="vendorNameIn">'
	                        + '<img src="images/magnifier.gif" onClick="getTier2Vendors(\''+vId+'\',\''+vName+'\',\'tier2indirectvendorName\');"><span id="msg_tire2indirectvendorName1'+(rowCount-1)+'" class="error"></span>';
	                newcell.childNodes[0].id = "tier2indirectvendorName" + rowCount;
	                newcell.childNodes[0].name = "tier2indirectvendorName";
	                newcell.childNodes[0].value="${vendorName1}"; 
	                newcell.childNodes[1].id = "vendorNameIn" + rowCount;
	                newcell.childNodes[1].name = "vendorName";
	                newcell.childNodes[1].value="${vendorNames1}";
	            /* newcell.innerHTML = '<input type="text" name="vendorName" alt="" title="Click the search image to select Vendors" value="" class="main-text-box" id="vendorName">'
                        + '<img src="images/magnifier.gif" onClick="getTier2Vendors();"><span id="msg_tire2indirectvendorName1'+(rowCount-1)+'" class="error"></span>';
                newcell.childNodes[0].id = "vendorName";
                newcell.childNodes[0].name = "vendorName";
                newcell.childNodes[0].value="${vendorNames1}"; */
	               // $("#tier2vendorName" + rowCount).select2();

	            }
	           
	            else  if (i == 1) {
	            	
	            	 newcell.innerHTML = '<select  name="certificate" class="chosen-select tier2vendorName onchange="addNewTier2Vendor(this)" >'
	                        + $('#certificates').html() + '</select>';
                     newcell.childNodes[0].id = "certificate" + rowCount;
                     newcell.childNodes[0].value="<%=certName%>";
                     newcell.childNodes[0].setAttribute('onChange',
                             'change(this);');
                    // $("#certificate"+rowCount).select2();
                 } else if (i == 2) {
                	
                	 newcell.innerHTML = '<select  name="ethnicity" class="chosen-select tier2vendorName" onchange="addNewTier2Vendor(this)">'
	                        + $('#ethnicities').html() + '</select>';
                     newcell.childNodes[0].id = "ethnicity" + rowCount;
                     <%-- newcell.childNodes[0].value="<%=ethnicity%>"; --%>
                     newcell.childNodes[0].value="${ethnicity}";
                     //$("#ethnicity"+rowCount).select2();
                 } else if (i == 3) {
                	
                	 newcell.innerHTML = '<select  name="marketSector" class="chosen-select tier2vendorName"> '
	                        + $('#marketSectorList').html() + '</select>';
                     newcell.childNodes[0].id = "marketSector" + rowCount;
                 }
                   switch (newcell.childNodes[0].type) {
	                 case "text":
	                    newcell.innerHTML = " <input  class='main-text-box' name='indirectAmt' id='indirectAmt"+rowCount+"'/><span	id='msg_indirectAmt"+(rowCount-1)+"' class='error'></span>";
	                    newcell.childNodes[0].value = "";
	                    break;
	            }
	        }
		}
	</script>
				<logic:present name="indirect" property="tier2IndirectVendorId.id">
					<script>
		if(count==1){	 	
		$("#tier2indirectvendorName").val("${vendorName1}");
		$("#vendorNameIn").val("${vendorNames1}");
	  }
	</script>
				</logic:present>
				<logic:present name="indirect" property="certificateId.id">
					<script>
				if(count==1){
					$("#certificates").val("<%=certName%>");
					//$("#certificates").select2();
				}
			</script>
				</logic:present>
				<logic:present name="indirect" property="ethnicityId.id">
					<script>
			if(count==1){
				<%-- $("#ethnicities").val("<%=ethnicity%>"); --%>
				$("#ethnicities").val("${ethnicity}");
				//$("#ethnicities").select2();
			}else{
				<%-- $("#ethnicity"+rowCount).val("<%=ethnicity%>"); --%>
				$("#ethnicity"+rowCount).val("${ethnicity}");
				//$("#ethnicity"+rowCount).select2();
			}
			</script>
				</logic:present>
				<logic:present name="indirect" property="marketSector.id">
					<bean:define id="marketSectorName" name="indirect"
						property="marketSector.id"></bean:define>
					<script>
			if(count==1){
				$("#marketSectorList").val(<%=marketSectorName%>);
			}else{
				$("#marketSector"+rowCount).val(<%=marketSectorName%>);
			}
			</script>
				</logic:present>
				<logic:present name="indirect" property="marketSubSector.id">
					<bean:define id="marketSectorName" name="indirect"
						property="marketSubSector.id"></bean:define>
					<script>
			if(count==1){
				$("#marketSectorList").val(<%=marketSectorName%>);
			}else{
				$("#marketSector"+rowCount).val(<%=marketSectorName%>);
			}
			</script>
				</logic:present>
				<logic:present name="indirect" property="indirectExpenseAmt">
					<bean:define id="indirectAmt" name="indirect"
						property="indirectExpenseAmt"></bean:define>
					<script>
				if (count == 1) {
					$("#indirectAmt").val(<%=indirectAmt%>);
				} else {
					$("#indirectAmt" + rowCount).val(<%=indirectAmt%>);
				}
				count = count + 1;
			</script>
				</logic:present>
			</logic:iterate>
		</logic:present>
		<script>
function generateTier2SpendReport(path){
	var pageHead = "TIER 2 SPEND REPORT";
	var pathArray = window.location.href.split( '/' );
	var protocol = pathArray[0];
	var host = pathArray[2];
	var parameter = pathArray[3];
	var url = protocol + '//' + host + "/" + parameter;
	var html = "<html><head>"
		+ "<style script=&quot;css/text&quot;>"
		+ "table.tableList_1 th {border:1px solid black; text-align:center; "
		+ "vertical-align: middle; padding:5px;}"
		+ "table.tableList_1 td {border:1px solid black; text-align: left; vertical-align: top; padding:5px;}"
		+ "</style>"
		+ "</head>"
		+ "<body style=&quot;page:land; font-family:Verdana;&quot;>"
		+ "<div class=&quot;logo&quot;>"
		+ "<img src=&quot;" 
		+ url
		+ "/temp/"
		+ path
		+ "&quot; style=&quot;float:left;"
		+ "position:absolute;top:0px;left:0px;&quot; "
		+ "height=&quot;60px&quot; width=&quot;60px&quot; alt=&quot;Logo&quot; />"
		+ "</div>"
		+ "<div class=&quot;pageHead_1&quot; style=&quot;text-align:center;padding: 5%;font-family:Verdana;&quot;>"
		+ pageHead + "</div> <table>";
// 	 html+=$('#report').html() +"</table><table>";
// 	 html+=$('#reportLast').html()+"</table></body></html>";
	 html+=$('#report').html() +"</table></body></html>";
	 html = html.replace(/'/g, '&apos;');
	var form = "<form name='pdfexportform' action='generategrid' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='Tier2SpendReport'>";
	form = form + "<input type='hidden' name='imagePath' value='" + path + "'>";
	form = form + "<input type='hidden' name='pdfBuffer' value='" + html + "'>";
	form = form + "</form><script>document.pdfexportform.submit();</sc"
			+ "ript>";
		OpenWindow = window.open('', '');
		OpenWindow.document.write(form);
		OpenWindow.document.close();
	}
	
	function recallPreviousQuarterReport() {
		<logic:present name="tier2ReportForm" property="tier2ReportPreviousQuarterId">
		window.location = "tier2Report.do?method=recallPrviousQuarter";
		</logic:present>
		<logic:notPresent name="tier2ReportForm" property="tier2ReportPreviousQuarterId">
		alert("Please submit atleast one report.");
		</logic:notPresent>
	}
	
	var help=0;
	
	function uploadDailog()
	{
		help=1;
		 $("#upload").css(
		{
			"display" : "block"
		});		
					
		$("#upload").dialog(
		{
			width : 500,
			modal : true,
			closeOnEscape: false,
			open: function() {
                $(this).parent().find(".ui-dialog-titlebar-close").hide();
            },
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
	}
	
	function closeUploadDialog()
	{
		$("#upload").dialog("close");
		help=0;
 		window.location.reload(true);
	}
	
	function uploadExcelFile()
	{
		var fileAvaliable=$("#uploadTextId").val();
		
		if(fileAvaliable=='' && help==1){
			alert("Please select downloaded excel file");
			return false;
		}
		else if(fileAvaliable!='')
		{
			  return true;
		}
		else
			return false;
	}
	
	$(document).ready(function() { 
		//Don't Remove this Bcoz its Needed for IE < 9 Versions to Take indexOf().
		if(!Array.prototype.indexOf) {
		    Array.prototype.indexOf = function(obj, start) {
		         for (var i = (start || 0), j = this.length; i < j; i++) {
		             if (this[i] === obj) { return i; }
		         }
		         return -1;
		    }
		}
	});	
	
	function uploadExcelFileValidation(selectedFile)
	{		   
		//Validation for File type
	    var validExts = new Array(".xlsx", ".xls");
	    var fileExt = selectedFile.value;
	    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
	    
	    if (validExts.indexOf(fileExt) < 0) 
	    {
	    	alert("You can import excel files only [.xls & .xlsx].");
	      	$("#uploadDataId").val('');
	      	$("#uploadTextId").val('');
	      	return false;
	    }
	    else
	    {
	    	$("#uploadTextId").val(selectedFile.value);
	    }
	}
	
	/* function fileValue(selectedFile)
	{
		return selectedFile;
	} */
	
	function excelHelpDialog()
	{
		$("#helpForExcelUpload").css(
		{
			"display" : "block"
		});		
		
		$("#helpForExcelUpload").dialog(
		{
			width : 600,
			modal : true,
			show : 
			{
				effect : "scale",
				duration : 1000
			},
			hide : 
			{
				effect : "scale",
				duration : 1000
			}
		});
	}
	
	function downloadExcel()
	{
     // debugger;
		
		$("#helpForExcelUpload").dialog("close");
		document.location='tier2Report.do?method=downloadExcelFormate&random=' + Math.random();
	}
	
	/* function deleteUnsubmittedRecords()
	{
		input_box = confirm("Are you sure you want to delete this Record?");
		if(input_box==true){
		$.ajax({
					url : 'tier2Report.do?method=deleteUnsubmittedRecords&random=' + Math.random(),
					type : "POST",
					async : false,
					success : function(data) 
					{
						alert(data);
					}
				});
		}else 
		{
			// Output when Cancel is clicked
			// alert("Delete cancelled");
			return false;
		}
	} */
</script>

		<!-- Dialog Box for Upload Excel File -->
		<div id="upload" title="Upload Excel File" style="display: none;">
			<html:form action="tier2Report.do?method=tier2ReportExcelFileUpload"
				method="post" styleId="importFileForm" enctype="multipart/form-data"
				onsubmit="return uploadExcelFile();">
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper" style="width: 35%;">Select
							Excel File to Import:</div>
						<div class="ctrl-col-wrapper">
							<div class="fileUpload btn btn-primary">
								<span>Browse</span>
								<html:file property="importExcelFile" styleId="uploadDataId"
									styleClass="upload" onchange="uploadExcelFileValidation(this);" />
							</div>
						</div>
					</div>
				</div>
				<div class="wrapper-full">
					<div class="row-wrapper form-group row">
						<div class="label-col-wrapper" style="width: 35%;">Selected
							Excel File Name:</div>
						<div class="ctrl-col-wrapper">
							<input type="text" class="text-box" id="uploadTextId"
								readonly="readonly" />
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="wrapper-btn">
					<html:submit value="Upload" styleClass="btn" />
					<input type="button" id="clear" value="Cancel" class="btn"
						onclick="closeUploadDialog();" />
				</div>
			</html:form>
		</div>

		<div id="dialog3" title="Choose Vendors" style="display: none;">
			<div class="wrapper-full">
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper" style="width: 25%;">Search
						Tier2 Vendor:</div>
					<div class="ctrl-col-wrapper">
						<input type="search" class="text-box" id="searchVendorsText"
							placeholder="Search" autofocus="autofocus">
					</div>
				</div>
				<div class="row-wrapper form-group row">
					<div class="label-col-wrapper" style="width: 25%;"></div>
					<div class="ctrl-col-wrapper">
						<input type="button" align="middle" value="Add New Tier2 Vendor"
							class="btn" onclick="addNewTier2Vendor(this);">
					</div>
				</div>
				<%-- <div class="label-col-wrapper" style="width: 25%;">Choose BP Market Sector:</div>
											<div class="ctrl-col-wrapper">
												<select name="marketSectors" class="chosen-select" id="marketSectors" 
													onchange="changeMarketSubSectorBasedOnSector()" style="width: 60%;">
													<option value="0">-- Select --</option>	
													<logic:present name="marketsectors">
														<logic:iterate id="sectorIterator" name="marketsectors">
															<option value='<bean:write name="sectorIterator" property="id"  />'>
																			<bean:write name="sectorIterator" property="sectorDescription" />
															</option>
														</logic:iterate>
													</logic:present>
												</select>
											</div>
										</div> --%>
				<%-- <div class="row-wrapper form-group row">
											<div class="label-col-wrapper" style="width: 25%;">Choose BP Market Subsector:</div>
											<div class="ctrl-col-wrapper">
												<select name="marketSubSectors" class="chosen-select" id="marketSubSectors" 
													onchange="changeGridBasedOnSubSector()"  style="width: 60%;">
													<option value="0">-- Select --</option>
													<logic:present name="marketsubsectors">
														<logic:iterate id="subsectorIterator" name="marketsubsectors">
															<option value='<bean:write name="subsectorIterator" property="id"  />'>
																			<bean:write name="subsectorIterator" property="categoryDescription" />
															</option>
														</logic:iterate>
													</logic:present>
												</select>
											</div>
										</div> --%>
			</div>
			<div class="clear"></div>
			<table id="list2" width="100%"></table>
			<div id="pager2"></div>
			<div class="clear"></div>
			<div class="btn-wrapper" style="margin: 5px; width: 55%;"
				id="okButtonDiv" hidden="true">
				<input type="button" value="Ok" class="btn"
					onclick="addTier2Vendor();"> <input type="button"
					value="Cancel" class="btn" onclick="cancelTier2Vendors();">
			</div>
		</div>

		<!-- Dialog Box for Tier 2 Spend Report Excel Upload Help Instructions -->
		<div id="helpForExcelUpload"
			title="Tier 2 Spend Report Excel Upload Help"
			style="display: none; max-height: 300px;">
			<p>
				<b style="color: black;">Note:</b> Please see <span
					style="color: red;">sheet named 'Help'</span> for Instructions on
				Completing Spreadsheet.
			</p>
			<div class="wrapper-btn" style="margin: 35px 0 0 40%;">
				<input type="button" value="Download Excel File" class="btn"
					onclick="downloadExcel();" />
			</div>
		</div>