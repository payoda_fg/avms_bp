<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("indirectProcurementCommodities") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("indirectProcurementCommodities");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			
			cellobj.put("vendorId", tier2ReportDto.getVendorId());
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("businessTypeName", tier2ReportDto.getBusinessTypeName());
			if(tier2ReportDto.getRecentAnnaulRevenue() != null){
				cellobj.put("recentAnnaulRevenue",Math.round(tier2ReportDto.getRecentAnnaulRevenue()));
			}else{
				cellobj.put("recentAnnaulRevenue", tier2ReportDto.getRecentAnnaulRevenue());
			}
			cellobj.put("vendorStatus", tier2ReportDto.getStatusName());
			cellobj.put("bpMarketSector", tier2ReportDto.getMarketSector());
			cellobj.put("bpMarketSubSector", tier2ReportDto.getMarketSubSector());
			cellobj.put("diverseClassification", tier2ReportDto.getDiverseClassification());
			cellobj.put("state", tier2ReportDto.getState());
			cellobj.put("contactFirstName", tier2ReportDto.getFirstName());
			cellobj.put("contactLastName", tier2ReportDto.getLastName());
			cellobj.put("contactPhone", tier2ReportDto.getPhone());
			cellobj.put("contactEmail", tier2ReportDto.getEmailId());
			cellobj.put("website", tier2ReportDto.getWebsite());
			
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>