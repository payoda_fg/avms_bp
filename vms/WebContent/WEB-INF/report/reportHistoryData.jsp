<%@page import="com.fg.vms.customer.model.Ethnicity"%>
<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<Tier2ReportDto> tier2ReportHistory = null;
	if (session.getAttribute("tier2ReportHistory") != null) {
		tier2ReportHistory = (List<Tier2ReportDto>) session
				.getAttribute("tier2ReportHistory");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	int i = 0;
	if (tier2ReportHistory != null) {
		for (int index = i; index < tier2ReportHistory.size(); index++) {
			tier2ReportDto = tier2ReportHistory.get(index);
			cellobj = new JSONObject();
			cellobj.put("id", tier2ReportDto.getTier2reportId());
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("year", tier2ReportDto.getYear());
			cellobj.put("reportPeriod", tier2ReportDto.getReportPeriod());
			cellobj.put("directExp", tier2ReportDto.getDirectExp());
			cellobj.put("indirectExp", tier2ReportDto.getIndirectExp());
			cellobj.put("totalSales", tier2ReportDto.getTotalSales());
			cellobj.put("totalSalesToCompany", tier2ReportDto.getTotalSalesToCompany());
			cellobj.put("isSubmitted", tier2ReportDto.getIsSubmitted());
			cellobj.put("createdOn", tier2ReportDto.getCreatedOn());
			cellarray.add(cellobj);
			i++;
		}
		out.println(cellarray);
	}
%>
