<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Added for jquery validation -->
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<!--<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />-->
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<style type="text/css">
th.ui-th-column div {
	white-space: normal !important;
	height: auto !important;
	padding: 2px;
}

.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
	height: auto;
	vertical-align: text-top;
	padding-top: 2px;
}
</style>
<script>
	$(document).ready(function() {

		$("#tier2VendorNames").multiselect({
			selectedText : "# of # selected"
		});
		$("#reportingPeriod").multiselect({
			multiple : false,
			header : "Select an option",
			noneSelectedText : "Select an Option",
			selectedList : 1
		});
		$("#spendYear").multiselect({
			multiple : false,
			header : "-- Year --",
			noneSelectedText : "-- Year --",
			selectedList : 1,
			minWidth : '160'
		});

		$("#spendYear").change(function() {
			var id = $(this).val();
			var dataString = 'year=' + id;

			$.ajax({
				type : "POST",
				url : "spendReport.do?method=loadReportPeriod",
				data : dataString,
				cache : false,
				success : function(html) {

					$("#reportingPeriod").html(html);

					$("#reportingPeriod").multiselect({
						multiple : false,
						header : "Select an option",
						noneSelectedText : "Select an Option",
						selectedList : 1
					});
				}
			});
		});
	});
</script>
<div class="page-title">
	<img src="images/arrow-down.png" alt="Rejected Report" />Total Spend
	by Ethnicity
</div>
<div class="form-box" style="width: 94%;">
	<form id="reportform">

		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper" style="width: 35%;">View the
					report by Vendor :</div>
				<div class="ctrl-col-wrapper">
					<html:select property="tier2VendorNames" multiple="true"
						name="tier2ReportForm" styleId="tier2VendorNames" style="width:31%;">
						<logic:present name="tier2ReportForm" property="vendors">
						<html:optionsCollection name="tier2ReportForm" property="vendors" label="vendorName" value="id"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper" style="width: 33%; padding-left: 5%;">Spend
					Year :</div>
				<div class="ctrl-col-wrapper">
					<select name="spendYear" id="spendYear">
						<option value="0">-- Select --</option>
						<logic:present name="tier2ReportForm" property="spendYears">
						<logic:iterate id="year" name="tier2ReportForm" property="spendYears">
						<option value="${year}">${year}</option>
						</logic:iterate>
						</logic:present>
					</select>
				</div>
			</div>

		</div>
		<div class="wrapper-half">
			<div class="row-wrapper form-group row">
				<div class="label-col-wrapper" style="width: 35%;">Reporting
					Period :</div>
				<div class="ctrl-col-wrapper">
					<select id="reportingPeriod" name="reportingPeriod">
						<option value="0">-- Select --</option>
					</select>
				</div>
			</div>
			<div class="row-wrapper form-group row">
				<div class="wrapper-btn">
					<input type="button" value="Get Report" class="btn"
						id="spendreport" onclick="getEthnicitySpendReport();" />
				</div>
			</div>
		</div>
	</form>
</div>
<div class="clear"></div>
<div id="grid_container" style="width: 100%;">
	<table id="gridtable">
		<tr>
			<td />
		</tr>
	</table>
	<div id="pager"></div>
</div>
