
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<!--Menupart -->
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<section class="body">
		<header class="page-header">
			<h2 class="page-title">Report</h2>
		</header>	
			<div class="inner-wrapper">
				<aside id="sidebar-left" class="sidebar-left">				
				    <div class="sidebar-header">
				         <div class="sidebar-title">Navigation</div>
				        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
				            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				        </div>
				    </div>
				    <div class="nano sidepanelMenu">
				        <div class="nano-content">
				            <nav id="menu" class="nav-main" role="navigation">				            
				                <ul class="nav nav-main">				                    	
									<logic:present name="vendorUser" property="vendorId">
										<logic:present name="vendorUser" property="vendorId.vendorStatus">
											<logic:equal value="B" name="vendorUser"
												property="vendorId.vendorStatus">
												<li class="nav-active">					   	
											<a href="#menu6" class="icon-6 submenuheader"
											onclick="linkPage('tier2Report.do?method=showSpendReportHistory')"> <i
											class="icon-img"><img src="bpimages/createreport1.png" /> </i>
											<span
											class="icon-text">Create Tier 2 Spend Report</span>
											</a>	
											</li>								
											</logic:equal>
										</logic:present>												
									</logic:present>						
								</ul>
					</nav>
				</div>
			</div>
		</aside>
	</div>
</section>
<script type="text/javascript">
	var pathname = window.location.href;

	if (pathname.indexOf("tier2Report") >= 0) {
		$('.icon-6').addClass("active");
	}
</script>