<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Included to Generate Report -->

<script src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Added for Populate JqGrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css"/>-->

<!-- Added for Export CSV Files -->
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<!-- Added for Multiselect (Select) Dropdown -->
<!-- <script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" /> -->

<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Certificate Expiration Notification Email" name="privilege" property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
					<!-- Heading Part-->
			<section role="main" class="content-body card-margin reports-module">
				<div class="row">
					<div class="col-lg-8 mx-auto col-sm-12 col-md-12 col-xl-9 col-xs-12">
						<section class="card">
							<header class="card-header">
								<h2 class="card-title">Certificate Expiration Notification Email Report</h2>
							</header>
			
			<!-- Form Part -->
			<html:form styleId="reportform">
			<div class="form-box card-body">								
					<div class="form-group row">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Start Date:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="date" data-plugin-datepicker id="startDate" class="form-control" />	
								</div>							
							</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">End Date:</label>
							<div class="col-sm-9 ctrl-col-wrapper">		
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>						
									<input type="date" data-plugin-datepicker id="endDate" class="form-control" />	
								</div>
							</div>
						</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Is Renewed:</label>
							<div class="col-sm-9 ctrl-col-wrapper">								
								<select id="isRenewed" name="isRenewed" class="form-control">
									<option value="0">-- Select --</option>
									<option value="1">All</option>
									<option value="2">Yes</option>
									<option value="3">No</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
						<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Vendor Status:</label>
							<div class="col-sm-9 ctrl-col-wrapper">								
								<html:select property="vendorStatusList" styleClass="form-control" multiple="true" name="tier2ReportForm"	styleId="vendorStatusList" >
		 							<bean:size id="size" name="VendorStatusMasterList" />
		 							<logic:greaterEqual value="0" name="size">
		 								<logic:iterate id="statusList" name="VendorStatusMasterList">
		 									<bean:define id="id" name="statusList" property="id"/>
		 									<html:option value="${id}">
		 										<bean:write name="statusList" property="statusname"></bean:write>
		 									</html:option>
		 								</logic:iterate>
		 							</logic:greaterEqual>
	 							</html:select>		
							</div>
						</div>
					</div>
					<footer class="card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
								<%-- <html:submit value="Get Report" styleClass="btn" styleId="spendreport" onclick="return getCertificateExpirationNotificationEmailReport()"/> --%>
								<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getCertificateExpirationNotificationEmailReport();" />						
							</div>	
						</div>
					</footer>			
				</html:form>
		</section>
	</div>
</div>
			
	<div class="clear"></div>
		
		<!-- Result Part -->			
		<div class="grid-wrapper view-reports-table">
			<div class="form-box">
				<div class="row">
					<div class="col-lg-12">
						<section class="card">
							<div class="card-body">
								<div id="grid_container">
									<div class="responsiveGrid table-full-width">
										<table id="gridtable" class="table table-bordered table-striped mb-0">
											<tr>
												<td />
											</tr>
										</table>
										<div id="pager"></div>
									</div>
								</div>	
							</div>						
						</section>
					</div>
				</div>
			</div>
	</div>
	<!-- Export Dialog Box -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel">
			</td>
			<td>
				<label for="excel">
					<img id="excelExport" src="images/excel_export.png" />Excel
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"> 
				<input type="hidden" name="fileName" id="fileName" value="CertificateExpirationNotificationEmailReport">
			</td>
			<td>
				<label for="csv">
					<img id="csvExport" src="images/csv_export.png" />CSV
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf">
					<img id="pdfExport" src="images/pdf_export.png" />PDF
				</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails" property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportCertificateExpirationNotificationEmailReportData('gridtable','<%=logoPath%>');">
		</logic:present>
	</div>
</div>


</section>	
</logic:equal>
	<logic:equal value="0" name="privilege" property="view">
		<div class="form-box">
			<h3>You have No Rights to View Certificate Expiration Notification Email Report.</h3>
		</div>		
	</logic:equal>			
</logic:equal>
</logic:iterate>




<script type="text/javascript">
	$( document ).ajaxError(function() 
	{
		$("#ajaxloader").hide();
	});
	
	$(document).ready(function() 
	{
		$.datepicker.setDefaults(
		{
			changeYear : true,
			dateFormat : 'mm/dd/yy'
		});
		$("#startDate").datepicker();
		$("#endDate").datepicker();
		
		//To Reset Date Field
		$.datepicker._clearDate("#startDate");
		$.datepicker._clearDate("#endDate");
		
		$("#isRenewed").multiselect(
		{
			multiple : false,
			header : "Select an option",
			noneSelectedText : "Select an Option",
			selectedList : 1
		});
		
		$("#vendorStatusList").multiselect({
			selectedText : "# of # selected"
		});
	});
	
	function getCertificateExpirationNotificationEmailReport() 
	{
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		
		if(startDate == '') 
		{
			alert('Please Select Start Date.');
			return false;
		} 
		else if(endDate == '') {
			alert('Please Select End Date.');
			return false;
		}
		else if(new Date(startDate) >= new Date(endDate)) 
		{
			alert("Invalid Date Range.");
			return false;
		} 
		else 
		{
			$.ajax(
			{
				url : "certificateExpirationNotificationEmailReport.do?method=getCertificateExpirationNotificationEmailReport&random=" + Math.random(),
				type : "POST",
				data : $("#reportform").serialize(),
				async : true,
				beforeSend : function()
				{
					$("#ajaxloader").show();
				},
				success : function(data) 
				{
					$("#ajaxloader").hide();
					$("#gridtable").jqGrid('GridUnload');
					gridCertificateExpirationNotificationEmailReportData(data);
				}
			});			
		}
	}
	
	function gridCertificateExpirationNotificationEmailReportData(myGridData)
	{
		var newdata1 = jQuery.parseJSON(myGridData);

		$('#gridtable').jqGrid(
		{
			data : newdata1,
			datatype : 'local',
			colNames : [ 'Vendor Name', 'Email Id', 'Email Date', 'Certificate Number', 
			             'Certificate Name', 'Agency Name', 'Expiration Date', 'Is Renewed'],
			colModel : [
		            {
						name : 'vendorName',
						index : 'vendorName'
					}, {
						name : 'emailId',
						index : 'emailId'
					}, {
						name : 'emailDate',
						index : 'emailDate'
					}, {
						name : 'certificateNumber',
						index : 'certificateNumber'
					}, {
						name : 'certificateName',
						index : 'certificateName'
					}, {
						name : 'agencyName',
						index : 'agencyName'
					}, {
						name : 'expDate',
						index : 'expDate'
					}, {
						name : 'isRenewed',
						index : 'isRenewed'
					}],
			/*rowNum : 10,
			rowList : [ 10, 20, 50 ],*/
			pager : '#pager',
			shrinkToFit : false,
			autowidth : true,
			viewrecords : true,
			emptyrecords : 'No Data Available...',
			footerrow : true,
			userDataOnFooter : true,
			cmTemplate: {title: false},
			height : 250,		
			gridview: true,
	        pgbuttons:false,
	        pgtext:'',
	        width: ($.browser.webkit?466:497),
	        loadonce: true,
	        rowNum: 2000	        
		}).jqGrid('navGrid', '#pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		},

		{}, /* edit options */
		{}, /* add options */
		{}, /* del options */
		{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
			caption : "Export",
			buttonicon : "ui-icon ui-icon-newwin",
			onClickButton : function() 
			{
				exportGrid();
			},
			position : "last"
		});

		$(window).bind('resize', function() 
		{
			$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
		}).trigger('resize');
	}
	
	/**
	 * Function to Export the Certificate Expiration Notification Email Report Data.
	 */
	function exportCertificateExpirationNotificationEmailReportData(table, path) 
	{	
		var exportType = $("input[name=export]:checked").val();
		var cols = [];
		$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
		{
			if (!this.hidden) 
			{
				cols.push(this.index);
			}
		});

		var index = [];
		var valueForCsv = [];
		for ( var i = 0; i <= cols.length; i++) {
			if (cols[i] == "vendorName") {
				index.push(0);
				valueForCsv.push("Vendor Name");
			} else if (cols[i] == "emailId") {
				index.push(1);
				valueForCsv.push("Email Id");
			} else if (cols[i] == "emailDate") {
				index.push(2);
				valueForCsv.push("Email Date");
			} else if (cols[i] == "certificateNumber") {
				index.push(3);
				valueForCsv.push("Certificate Number");
			} else if (cols[i] == "certificateName") {
				index.push(4);
				valueForCsv.push("Certificate Name");
			} else if (cols[i] == "agencyName") {
				index.push(5);
				valueForCsv.push("Agency Name");
			} else if (cols[i] == "expDate") {
				index.push(6);
				valueForCsv.push("Expiration Date");
			} else if (cols[i] == "isRenewed") {
				index.push(7);
				valueForCsv.push("Is Renewed");
			}
		}

		if (exportType == 1) 
		{
			exportExcel(table, index, 'CertificateExpirationNotificationEmailReport');
		} 
		else if (exportType == 2) 
		{
			$('#' + table).table2CSV({
				header : valueForCsv,
				fileName : fileName
			});
		} 
		else if (exportType == 3) 
		{		
			window.location = "certificateExpirationNotificationEmailReport.do?method=getCertificateExpirationNotificationEmailReportPdf";
		} 
		else 
		{
			alert("Please Choose a Type to Export");
			return false;
		}

		$('#dialog').dialog('close');
	}
</script>
