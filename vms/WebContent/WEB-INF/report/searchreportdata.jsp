<%@page import="com.fg.vms.util.ExportGridUtil"%>
<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList, com.fg.vms.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();
	JSONArray colNamesArray = new JSONArray();
	JSONArray colModelArray = new JSONArray();
	
	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("searchList") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("searchList");
	}
	Tier2ReportDto tier2ReportDto = null;
	colNamesArray = ExportGridUtil.getColumnNamesArray(colNamesArray);
	colModelArray = ExportGridUtil.getColumnModelArray(colModelArray);
   
	if (reportDtos != null) {
		for (int index = 0; index < reportDtos.size(); index++) {
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();			
			
			if(tier2ReportDto.getVendorName() != null)
				cellobj.put("vendorName", tier2ReportDto.getVendorName().replace("|", " "));
			else
				cellobj.put("vendorName", tier2ReportDto.getVendorName());
			
			if(tier2ReportDto.getOwnerName() != null)
				cellobj.put("ownerName", tier2ReportDto.getOwnerName().replace("|", " "));
			else
				cellobj.put("ownerName", tier2ReportDto.getOwnerName()); 
			
		/* 	if(tier2ReportDto.getFirstName() != null)
				cellobj.put("firstName", tier2ReportDto.getFirstName().replace("|", " "));
			else
				cellobj.put("firstName", tier2ReportDto.getFirstName()); */
			
			if(tier2ReportDto.getAddress1() != null)
				cellobj.put("address", tier2ReportDto.getAddress1().replace("|", " "));
			else
				cellobj.put("address", tier2ReportDto.getAddress1());
			
			cellobj.put("city", tier2ReportDto.getCity());
			cellobj.put("state", tier2ReportDto.getState());
			cellobj.put("zipcode", tier2ReportDto.getZip());
			cellobj.put("phone", tier2ReportDto.getPhone());
			cellobj.put("fax", tier2ReportDto.getFax());
			cellobj.put("emailId", tier2ReportDto.getEmailId());
			
			/* if(tier2ReportDto.getCertAgencyName() != null)
			cellobj.put("agencyName", tier2ReportDto.getCertAgencyName().replace("|", " "));
		else
			cellobj.put("agencyName", tier2ReportDto.getCertAgencyName());
		
		if(tier2ReportDto.getCertificateName() != null)
			cellobj.put("certName", tier2ReportDto.getCertificateName().replace("|", " "));
		else
			cellobj.put("certName", tier2ReportDto.getCertificateName());
		
		cellobj.put("effDate", tier2ReportDto.getEffDate());
		cellobj.put("expDate", tier2ReportDto.getExpDate());
		
		if(tier2ReportDto.getCapablities() != null)
			cellobj.put("capablities", tier2ReportDto.getCapablities().replace("|", " "));
		else
			cellobj.put("capablities", tier2ReportDto.getCapablities());
		
		if(tier2ReportDto.getNaicsDesc() != null)
			cellobj.put("naicsDesc", tier2ReportDto.getNaicsDesc().replace("|", " "));
		else
			cellobj.put("naicsDesc", tier2ReportDto.getNaicsDesc()); */
			
			cellobj.put("naicsCapabilities", tier2ReportDto.getNaicsCapabilities());
			cellobj.put("diverseClassification", tier2ReportDto.getDiverseClassification());
			cellobj.put("certificateAgencyDetails", tier2ReportDto.getCertificateAgencyDetails());
		
			
			cellarray.add(cellobj);
		}
		out.println(colNamesArray);
		out.println("|");
		out.println(colModelArray);
		out.println("|");
		out.println(cellarray);
	}
%>
