<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Included to Generate Report -->
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Added for Populate JqGrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css"/>

<!-- Added for Export CSV Files -->
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>

<!-- Added for MultiSelect Drop-down Box 
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" />-->

<section role="main" class="content-body card-margin admin-module">
	<div class="row">
		<div class="col-lg-9 mx-auto">
			<section class="card">	
<logic:iterate id="privilege" name="privileges">
	<logic:equal value="Vendors By BP Market Subsector Report" name="privilege" property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
			<!-- Heading Part-->
			<header class="card-header">
				<h2 class="card-title pull-left">Vendors By BP Market Subsector</h2>
			</header>		
			
			<!-- Form Part -->
			<div class="form-box card-body">
				<html:form styleId="reportform">				
					<div class="wrapper-full">
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor Status:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="vendorStatusList" multiple="true" name="tier2ReportForm" styleClass="form-control" styleId="vendorStatusList">
									<logic:present name="tier2ReportForm" property="vendorStatusMaster">
										<html:optionsCollection name="tier2ReportForm" property="vendorStatusMaster" label="statusname" value="id"/>
									</logic:present>
								</html:select>		
							</div>
						</div>
					</div>
					<footer class="mt-2 card-footer">
						<div class="row justify-content-end">
							<div class="col-sm-9 wrapper-btn">
								<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getVendorsByBPMarketSubsectorReport();" />						
							</div>
						</div>
					</footer>
				</html:form>
			</div>
			
			<div class="clear"></div>
			
			<!-- Result Part -->			
			<div id="grid_container" class="card-body">
				<div class="responsiveGrid">
				<table id="gridtable" class="table table-bordered table-striped mb-0">
					<tr>
						<td />
					</tr>
				</table>
				<div id="pager"></div>
				</div>
			</div>
		</logic:equal>
		<logic:equal value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have no rights to view vendors by bp market subsector.</h3>
			</div>
		</logic:equal>
	</logic:equal>
</logic:iterate>

<!-- Export Dialog Box -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel">
			</td>
			<td>
				<label for="excel">
					<img id="excelExport" src="images/excel_export.png" />Excel
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"> 
				<input type="hidden" name="fileName" id="fileName" value="VendorsByBPMarketSubsectorReport">
			</td>
			<td>
				<label for="csv">
					<img id="csvExport" src="images/csv_export.png" />CSV
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf">
					<img id="pdfExport" src="images/pdf_export.png" />PDF
				</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportVendorsByBPMarketSubsectorReportData('gridtable');">		
	</div>
</div>
</section>
</div>
</div>
</section>

<script type="text/javascript">
$( document ).ajaxError(function() {
	$("#ajaxloader").hide();
});

$(document).ready(function() {
	$("#vendorStatusList").multiselect({
		selectedText : "# of # selected"
	});
});

function getVendorsByBPMarketSubsectorReport()
{
	$.ajax(
	{
		url : "vendorsByBPMarketSubsectorReport.do?method=getVendorsByBPMarketSubsectorReport&random=" + Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function()
		{
			$("#ajaxloader").show();
		},
		success : function(data) 
		{
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			gridVendorsByBPMarketSubsectorReportData(data);
		}
	});	
}

function gridVendorsByBPMarketSubsectorReportData(myGridData)
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{
				data : newdata1,
				datatype : 'local',
				colNames : [ 'BP Market Sector','BP Market SubSector', 'Vendor Count', '%' ],
				colModel : [ 
				            {
				            	name : 'bpMarketSector',
				            	index : 'bpMarketSector',
				            	sorttype: 'string',
				            	align : 'center'
				            },{
				            	name : 'bpMarketSubSector',
				            	index : 'bpMarketSubSector',
				            	sorttype: 'string',
				            	align : 'center'
				            }, {
				            	name : 'vendorCount',
				            	index : 'vendorCount',
				            	sorttype: 'number',
				            	formatoptions : {						
									thousandsSeparator : ',',
									decimalPlaces: 0
								},
				            	align : 'center'
				            }, {
				            	name : 'percent',
				            	index : 'percent',
				            	align : 'center',
				            	formatter: 'number', 
				            	formatoptions: {suffix: '%'},
				            	sorttype: 'number'
				            } ],
		rowNum : 5000,
		rowList : [ 5000, 10000, 15000 ],
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,		
		gridview: true,
        pgbuttons:true,
        width: ($.browser.webkit?466:497),
        loadonce: true,
        gridComplete : function() 
        {
			var totalvendorcount = $("#gridtable").jqGrid('getCol', 'vendorCount', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', {
				bpMarketSector : 'Total Records',
				formatoptions : {						
					thousandsSeparator : ',',
					decimalPlaces: 0
				},
				vendorCount : totalvendorcount
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/**
 * Function to Export the Vendors By NAICS Description Data.
 */
function exportVendorsByBPMarketSubsectorReportData(table) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "bpMarketSector") {
			index.push(0);
			valueForCsv.push("BP Market Sector");
		}else if (cols[i] == "bpMarketSubSector") {
			index.push(1);
			valueForCsv.push("BP Market SubSector");
		} else if (cols[i] == "vendorCount") {
			index.push(2);
			valueForCsv.push("Vendor Count");
		} else if (cols[i] == "percent") {
			index.push(3);
			valueForCsv.push("Percentage");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'VendorsByBPMarketSubsectorReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "vendorsByBPMarketSubsectorReport.do?method=getVendorsByBPMarketSubsectorReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}
</script>