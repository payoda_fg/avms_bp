<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("diversityAnalysisReport") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("diversityAnalysisReport");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("certificateName", tier2ReportDto.getCertificateName());
			cellobj.put("certificateShortName", tier2ReportDto.getCertificateShortName());
			cellobj.put("count", tier2ReportDto.getCount());
			cellobj.put("diversityAnalysisPercent", Math.round(Double.parseDouble(tier2ReportDto.getDiversityPercent())) + "%");			

			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
