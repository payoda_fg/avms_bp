<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>
<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();
	List<Tier2ReportDto> tier2Vendors = null;
	if (session.getAttribute("tier2Vendors") != null) {
		tier2Vendors = (List<Tier2ReportDto>) session.getAttribute("tier2Vendors");
	}
	Tier2ReportDto tier2Vendor = null;
	if (tier2Vendors != null) {
		for (int index = 0; index < tier2Vendors.size(); index++) {
			tier2Vendor = tier2Vendors.get(index);
			cellobj = new JSONObject();
			if(tier2Vendor.getVendorId() !=null)
			  cellobj.put("id", tier2Vendor.getVendorId());
			else
			  cellobj.put("id", 0);
			cellobj.put("vendorName", tier2Vendor.getVendorName());
			cellobj.put("vendorAddress", tier2Vendor.getAddress1());
			cellobj.put("vendorCity", tier2Vendor.getCity());
			cellobj.put("vendorState", tier2Vendor.getState());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>