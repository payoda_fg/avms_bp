
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>
<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>

<script src="js/dhtmlxChart/dhtmlxChart/codebase/dhtmlxchart.js"
	type="text/javascript"></script>
<link rel="STYLESHEET" type="text/css"
	href="js/dhtmlxChart/dhtmlxChart/codebase/dhtmlxchart.css">
<style>
.dhx_axis_item_x {
	font-size: 11px
}

.dhx_axis_item_y {
	font-size: 11px
}
</style>

<script>
    var barChart;
    window.onload = function() {
	barChart = new dhtmlXChart({
	    view : "bar",
	    color : "#66ccff",
	    gradient : "3d",
	    container : "chart_container",
	    value : "#data3#",
	    label : "#data3#",
	    radius : 3,
	    tooltip : {
		template : "#data3#"
	    },
	    width : 50,
	    origin : 0,
	    yAxis : {
		
		title : "Amount"
	    },

	    group : {
		by : "#data1#",
		map : {
		    data3 : [ "#data3#", "sum" ]
		}
	    },
	    xAxis : {
		title : "Diverse Classification",
		template : "#id#",
		lines:true
	    },
	    border : false
	});

	function refresh_chart() {
	    barChart.clearAll();
	    barChart.parse(mygrid, "dhtmlxgrid");
	}
	;

	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath('js/dhtmlxChart/dhtmlxGrid/codebase/imgs/');
	mygrid.setSkin("dhx_skyblue");
	
	mygrid.loadXML("xml/<%=session.getId()%>/diverseReport.xml",
				refresh_chart);
		mygrid.attachEvent("onEditCell", function(stage) {
			if (stage == 2)
				refresh_chart();
			return true;
		});
	};
</script>


<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">


		<html:form
			action="/t2supplierSpendReport.do?method=showDiversityReport">

			<table style="padding-left: 30%;" cellspacing="10" cellpadding="5">

				<tr>
					<td>Enter Year</td>
					<td><html:text property="year" alt="" size="22"
							styleId="reportYear"
							onblur="return validateReportYear(this.value);"></html:text></td>

				</tr>
				<tr>
					<td>Select Vendor</td>
					<td><html:select property="vendorId"
							style="width:130px;font-size:11px;">
							<logic:present name="searchVendorForm" property="vendors">
								<logic:iterate id="vendor" name="searchVendorForm"
									property="vendors">
									<bean:define id="id" name="vendor" property="id"></bean:define>
									<html:option value="<%=id.toString() %>">
										<bean:write name="vendor" property="vendorName"></bean:write>
									</html:option>
								</logic:iterate>


							</logic:present>

						</html:select></td>
				</tr>

			</table>
			<div style="padding-left: 40%;">
				<html:submit value="Generate Report" styleClass="customerbtTxt"></html:submit>
			</div>
		</html:form>


		<logic:present property="status" name="searchVendorForm">
			<logic:equal value="success" property="status"
				name="searchVendorForm">
				<div id="gridbox"
					style="width: 850px; height: 170px; background-color: white; margin: 1% 0 0 5.9%;"></div>
				<div id="chart_container" style="width: 850px; height: 250px;"></div>

			</logic:equal>
			<logic:notEqual value="success" property="status"
				name="searchVendorForm">
				<div style="padding-left: 320px; padding-top: 80px;">
					<font size="3px">No Diversity Report Data</font>
				</div>
			</logic:notEqual>
		</logic:present>

	</div>
</div>