<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<c:choose>
    <c:when test="${type == 'ethnicity'}">
       <c:set var="privilegeValue" value="Prime Supplier by Tier 2 Diversity By Ethnicity"/>
    </c:when>    
    <c:otherwise>
        <c:set var="privilegeValue" value="Prime Supplier by Tier 2 Diversity Category"/>
    </c:otherwise>
</c:choose>
<logic:iterate id="privilege" name="privileges">
	<logic:equal value="${privilegeValue}"
				name="privilege" property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">
		
<!-- Added for jquery validation -->
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>

<!-- 
<link rel="stylesheet" type="text/css" media="screen" 	href="jquery/css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" 	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen"	href="jquery/css/jquery.multiselect.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

 -->
 
<style type="text/css">
/*th.ui-th-column div {
	white-space: normal !important;
	height: auto !important;
	padding: 2px;
}
.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
	height: auto;
	vertical-align: text-top;
	padding-top: 2px;
}*/
</style>
<script>
$( document ).ajaxError(function() {
	$("#ajaxloader").hide();
});
	$(document).ready(function() {
		$("#tier2VendorNames").multiselect({
			selectedText : "# of # selected"
		});
		$("#reportingPeriod").multiselect({
			multiple : true,
			header : "Select an option",
			noneSelectedText : "Select an Option",
			selectedList : 1
		});
		$("#spendYear").multiselect({
			multiple : true,
			header : "-- Year --",
			noneSelectedText : "-- Year --",
			selectedList : 1,
			minWidth : '160'
		});
		
		$("#reportType").multiselect({
			multiple : false,
			header : "Select an option",
			noneSelectedText : "Select an Option",
			selectedList : 1,
			minWidth : '160'
		});		
		
		$("#certificate").multiselect({
			selectedText : "# of # selected"
		});

		$("#spendYear").change(function() {
			var id = $(this).val();
			var dataString = 'year=' + id;

			$.ajax({
				type : "POST",
				url : "spendReport.do?method=loadReportPeriodForMultipleYears",
				data : dataString,
				cache : false,
				success : function(html) {

					$("#reportingPeriod").html(html);

					$("#reportingPeriod").multiselect({
						multiple : true,
						header : "Select an option",
						noneSelectedText : "Select an Option",
						selectedList : 1
					});
				}
			});
		});
	});
</script>
<style>
<!-- /* .ui-jqgrid .ui-jqgrid-btable {
	table-layout: fixed;
}

.ui-jqgrid .ui-jqgrid-bdiv {
	position: relative;
	margin: 0em;
	padding: 0;
	overflow-x: hidden;
	overflow-y: auto;
	text-align: left;
} */
-->
</style>
<section role="main" class="content-body card-margin reports-module">
	<div class="row">
		<div class="col-lg-12 mx-auto col-sm-12 col-md-12 col-xl-9 col-xs-12">
				<form id="reportform" class="form-horizontal">
					<section class="card">
						<header class="card-header">
							<c:choose>
	    						<c:when test="${type == 'ethnicity'}">
									<h2 class="card-title">Prime Supplier Report by Tier 2 Indirect Spend</h2>
								</c:when>    
	   						 	<c:otherwise>
	   						 		<h2 class="card-title">Prime Supplier by Tier 2 Diversity Category</h2>
	 						 	</c:otherwise>
							</c:choose>
						</header>

		<div class="form-box card-body">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-sm-right label-col-wrapper">View the report by Vendor:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:select property="tier2VendorNames" multiple="true"
						name="tier2ReportForm" styleClass="form-control" styleId="tier2VendorNames">
						<logic:present name="tier2ReportForm" property="vendors">
						<html:optionsCollection name="tier2ReportForm" property="vendors" label="vendorName" value="id"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="form-group row">	
					<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Spend Year:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<select name="spendYear" id="spendYear" class="form-control">
						<option value="0">-- Select --</option>
						<logic:present name="tier2ReportForm" property="spendYears">
						<logic:iterate id="year" name="tier2ReportForm" property="spendYears">
						<option value="${year}">${year}</option>
						</logic:iterate>
						</logic:present>
					</select>
				</div>
			</div>
			<div class="form-group row">	
						<label class="col-sm-3 control-label text-sm-right  label-col-wrapper">Reporting Period:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<select id="reportingPeriod" name="reportingPeriod" class="form-control">
						<option value="0">-- Select --</option>
					</select>
				</div>
			</div>
			<div class="form-group row">	
					<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Spend Type:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<select id="reportType" name="reportType" class="form-control">
						<option value="-1">-- Select --</option>
						<option value="0">Direct</option>
						<option value="1">Indirect</option>
						<option value="2">Both</option>
					</select>
				</div>
			</div>
				<%-- <div class="label-col-wrapper" style="width: 35%; padding-left: 5%;">Direct or Indirect:</div>
				<div class="ctrl-col-wrapper">
					<html:radio property="reportType" name="tier2ReportForm" value="0">Direct</html:radio>
					<html:radio property="reportType" name="tier2ReportForm" value="1">Indirect</html:radio>
				</div> --%>
			<div class="form-group row">	
				<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Certificate Name:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:select property="certificate" name="tier2ReportForm" styleClass="form-control" styleId="certificate" multiple="true"> 
						<bean:size id="size" name="certificatesList"/>
						<logic:greaterEqual value="0" name="size">
							<logic:iterate id="certificates" name="certificatesList">
								<bean:define id="id" name="certificates" property="id"/>
									<html:option value="${id}">
										<bean:write name="certificates" property="certificateName"/>
									</html:option>
								</logic:iterate>
						</logic:greaterEqual>
					</html:select>
				</div>
			</div>
		</div>
		<footer class="card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
					<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getSupplierDiversityData('${type}');" />
			<%-- <html:submit value="Get Report" styleClass="btn" styleId="spendreport"></html:submit> --%>
		</div>
					</div>
				</footer>	
			</section>	
		</form>
	</div>
</div>
<div class="clear"></div>
<div class="grid-wrapper view-reports-table">
		<div class="form-box">
			<div class="row">
				<div class="col-lg-12">
					<section class="card">
						<div class="card-body">
							<div id="grid_container">
								<div class="responsiveGrid">
									<table id="gridtable" class="table table-bordered table-striped mb-0">
										<tbody>
											<tr>
												<td />
											</tr>
										</tbody>
									</table>
									<div id="pager"></div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="1" id="excel"></td>
			<td><label for="excel"><img id="excelExport"
					src="images/excel_export.png" />Excel</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="2" id="csv"> <input type="hidden" name="fileName"
				id="fileName" value="SupplierDiversityReport"></td>
			<td><label for="csv"><img id="csvExport"
					src="images/csv_export.png" />CSV</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="3" id="pdf"></td>
			<td><label for="pdf"><img id="pdfExport"
					src="images/pdf_export.png" />PDF</label></td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails"
				property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="exportBtn btn btn-primary"
				onclick="exportDiversityData('gridtable','<%=logoPath%>','${type}');">
		</logic:present>
	</div>
</div>
</section>
</logic:equal>
<logic:equal value="0" name="privilege" property="view">
	<c:choose>
    <c:when test="${type == 'ethnicity'}">
       <div class="form-box">
				<h3>You have no rights to view Prime Supplier by Tier 2 Diversity By Ethnicity.</h3>
			</div>
    </c:when>    
    <c:otherwise>
        <div class="form-box">
				<h3>You have no rights to view Prime Supplier by Tier 2 Diversity Category.</h3>
			</div>
    </c:otherwise>
</c:choose>
</logic:equal>

</logic:equal>
</logic:iterate>