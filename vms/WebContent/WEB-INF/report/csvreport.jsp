
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
	String fileName = request.getParameter("fileName");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss");
	fileName += "_" + sdf.format(new Date()).toString();
	response.setHeader("Content-disposition", "attachment;filename="
			+ fileName + ".csv");
	// response.setHeader("Content-disposition", "attachment;filename=SupplierReport.csv");
	response.setContentType("application/csv");
	String buf = request.getParameter("csvBuffer");
	String verifiedData = buf.replace("�", "");
	try {
		response.getWriter().println(verifiedData);
	} catch (Exception e) {
		System.out.println("Caught an exception : " + e);
		e.printStackTrace();
	}
%>