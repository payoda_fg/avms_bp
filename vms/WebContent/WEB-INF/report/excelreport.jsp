
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
	String fileName = request.getParameter("fileName");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss");
	fileName += "_" + sdf.format(new Date()).toString();
	response.setHeader("Content-disposition", "attachment;filename="
			+ fileName + ".xls");
	response.setContentType("application/vnd.ms-excel");
	String buf = request.getParameter("csvBuffer");
	try {
		response.getWriter().println(buf);
	} catch (Exception e) {
		System.out.println("Caught an exception : " + e);
		e.printStackTrace();
	}
%>