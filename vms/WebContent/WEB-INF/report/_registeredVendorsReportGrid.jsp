<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("registeredVendorsReport") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("registeredVendorsReport");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			
			cellobj.put("vendorId", tier2ReportDto.getVendorId());
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("firstName", tier2ReportDto.getFirstName());
			cellobj.put("lastName", tier2ReportDto.getLastName());
			cellobj.put("emailId", tier2ReportDto.getEmailId());
			cellobj.put("phone", tier2ReportDto.getPhone());
			cellobj.put("createdDate", tier2ReportDto.getCreatedOn());
			cellobj.put("city", tier2ReportDto.getCity());
			cellobj.put("state", tier2ReportDto.getState());			
			cellobj.put("vendorStatus", tier2ReportDto.getStatusName());
			
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>