<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("certificateExpirationNotificationEmailReport") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("certificateExpirationNotificationEmailReport");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			
			cellobj.put("vendorName", tier2ReportDto.getVendorName());			
			cellobj.put("emailId", tier2ReportDto.getEmailId());
			cellobj.put("emailDate", tier2ReportDto.getEmailDate());
			cellobj.put("certificateNumber", tier2ReportDto.getCertificateNumber());
			cellobj.put("certificateName", tier2ReportDto.getCertificateName());
			cellobj.put("agencyName", tier2ReportDto.getCertAgencyName());
			cellobj.put("expDate", tier2ReportDto.getExpDate());
			cellobj.put("isRenewed", tier2ReportDto.getCertificateExpiryStatus());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>