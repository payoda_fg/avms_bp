<%@page import="ChartDirector.*"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%
	List<Tier2ReportDto> reportDtos = null;
	Tier2ReportDto tier2ReportDto = null;

	ArrayList<Double> data1 = new ArrayList<Double>();
	ArrayList<String> labels1 = new ArrayList<String>();

	if (session.getAttribute("diversityDashboard") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("diversityDashboard");
		int i = 0;
		if (reportDtos != null) {
			for (int index = i; index < reportDtos.size(); index++) {
				tier2ReportDto = reportDtos.get(index);
				data1.add(tier2ReportDto.getCount());
				labels1.add(tier2ReportDto.getCertificateShortName());
				i++;
			}
		}
	}

	// The data for the pie chart
	double[] data = {};
	if (data1 != null) {
		data = new double[data1.size()];
		for (int i = 0; i < data.length; i++) {
			data[i] = Math.round(data1.get(i));
		}
	}

	// The labels for the pie chart
	String[] labels = {};
	if (labels1 != null) {
		labels = new String[labels1.size()];
		labels1.toArray(labels);
	}

	// Create a PieChart object of size 360 x 300 pixels
	PieChart c = new PieChart(600, 320);
//c.setBackground(c.linearGradientColor(0, 0, 0, 100, 0x99ccff, 0xffffff), 0x888888);
//c.setRoundedFrame();
//c.setDropShadow();

// Add a title using 18 pts Times New Roman Bold Italic font. Add 16 pixels top
// margin to the title.

// Set the center of the pie at (160, 165) and the radius to 110 pixels
c.setPieSize(160, 165, 110);

// Draw the pie in 3D with a pie thickness of 25 pixels
c.set3D(25);

// Set the pie data and the pie labels
c.setData(data, labels);


// Use local gradient shading for the sectors
c.setSectorStyle(Chart.LocalGradientShading);

// Use the side label layout method, with the labels positioned 16 pixels from the
// pie bounding box
c.setLabelLayout(Chart.SideLayout, 16);

// Show only the sector number as the sector label
c.setLabelFormat("{={sector}+1}");

// Set the sector label style to Arial Bold 10pt, with a dark grey (444444) border
c.setLabelStyle("Arial Bold", 10).setBackground(Chart.Transparent, 0x444444);

// Add a legend box, with the center of the left side anchored at (330, 175), and
// using 10 pts Arial Bold Italic font
LegendBox b = c.addLegend(330, 175, true, "Arial Bold ", 10);
b.setAlignment(Chart.Left);

// Set the legend box border to dark grey (444444), and with rounded conerns
b.setBackground(Chart.Transparent, 0x444444);
b.setRoundedCorners();

// Set the legend box margin to 16 pixels, and the extra line spacing between the
// legend entries as 5 pixels
b.setMargin(16);
b.setKeySpacing(0, 5);

// Set the legend box icon to have no border (border color same as fill color)
b.setKeyBorder(Chart.SameAsMainColor);

// Set the legend text to show the sector number, followed by a 120 pixels wide block
// showing the sector label, and a 40 pixels wide block showing the percentage
b.setText(
    "<*block,valign=top*>{={sector}+1}.<*advanceTo=22*><*block,width=120*>{label}" +
    "<*/*><*block,width=40,halign=right*>{percent}<*/*>%");
//c.setExplode(-1);
	// Output the chart
	String chart2URL = c.makeSession(request, "chart2");

	// Include tool tip for the chart
	String imageMap2 = c.getHTMLImageMap("", "",
			"title='{label}: {value} ({percent}%)'");
%>

<img src='<%=response.encodeURL("getchart.jsp?" + chart2URL)%>'
	usemap="#map2" border="0">
<map name="map2"><%
if(imageMap2 != null){
	out.println(imageMap2);
}else{
	out.println("Chart is not available");
}
%></map>


