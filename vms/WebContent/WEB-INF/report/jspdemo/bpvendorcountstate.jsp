<%@page import="ChartDirector.*"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%
	List<Tier2ReportDto> reportDtos = null;
	Tier2ReportDto tier2ReportDto = null;

	ArrayList<Double> data1 = new ArrayList<Double>();
	ArrayList<String> labels1 = new ArrayList<String>();

	if (session.getAttribute("bpVendorsCount") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("bpVendorsCount");
		int i = 0;
		if (reportDtos != null) {
			for (int index = i; index < reportDtos.size(); index++) {
				tier2ReportDto = reportDtos.get(index);
				data1.add(tier2ReportDto.getCount());
				labels1.add(tier2ReportDto.getState());
				i++;
			}
		}
	}

	// The data for the pie chart
	double[] data = {};
	if (data1 != null) {
		data = new double[data1.size()];
		for (int i = 0; i < data.length; i++) {
			data[i] = Math.round(data1.get(i));
		}
	}
	

	// The labels for the pie chart
	String[] labels = {};
	if (labels1 != null) {
		labels = new String[labels1.size()];
		labels1.toArray(labels);
	}
	
	Integer chartWidth=labels.length * 22;
	if(chartWidth < 250){
	chartWidth=250;
	}
	// Create a XYChart object of size 600 x 380 pixels. Set background color to brushed
	// silver, with a 2 pixel 3D border. Use rounded corners of 20 pixels radius.
	XYChart c = new XYChart(1300, chartWidth, Chart.brushedSilverColor(), Chart.Transparent, 2);

	// Add a title to the chart using 18pts Times Bold Italic font. Set top/bottom
	// margins to 8 pixels.
// 	c.addTitle("Annual Revenue for Star Tech", "Times New Roman Bold Italic", 18
// 	    ).setMargin2(0, 0, 8, 8);

	// Set the plotarea at (70, 55) and of size 460 x 280 pixels. Use transparent border
	// and black grid lines. Use rounded frame with radius of 20 pixels.
	c.setPlotArea(150, 50, 1000, chartWidth-100, -1, -1, Chart.Transparent, 0x000000);
	c.setRoundedFrame(0xffffff, 20);

	// Add a multi-color bar chart layer using the supplied data. Set cylinder bar shape.
	//c.addBarLayer3(data).setBarShape(Chart.CircleShape);
    BarLayer layer = c.addBarLayer(data, c.linearGradientColor(1000, 0, 0, 0, 0xffffff,0x008000,false));
// 	Set the bar gap to 10%
	layer.setBarGap(0.1);

// 	Use the format "US$ xxx millions" as the bar label
	layer.setAggregateLabelFormat("({value})");

// 	Set the bar label font to 10 pts Times Bold Italic/dark red (0x663300)
	layer.setAggregateLabelStyle("Arial Bold", 8, 0x663300);

	// Set the labels on the x axis.
	c.xAxis().setLabels(labels);

	// Show the same scale on the left and right y-axes
	c.syncYAxis();

	// Set the left y-axis and right y-axis title using 10pt Arial Bold font
	c.yAxis().setTitle("BP Vendor Count ", "Arial Bold", 10);
	c.xAxis().setTitle("State", "Arial Bold", 10);

	// Set y-axes to transparent
	//c.yAxis().setColors(Chart.Transparent);
	c.yAxis2().setColors(Chart.Transparent);

	// Disable ticks on the x-axis by setting the tick color to transparent
	c.xAxis().setTickColor(Chart.Transparent);

	// Set the label styles of all axes to 8pt Arial Bold font
	c.xAxis().setLabelStyle("Arial Bold", 8);
	//c.yAxis().setLabelStyle("Arial Bold", 8);
	c.yAxis2().setLabelStyle("Arial Bold", 8);
	c.swapXY();

	// Output the chart
	String chart11URL = c.makeSession(request, "chart11", Chart.JPG);

	// Include tool tip for the chart
	String imageMap11 = c.getHTMLImageMap("", "", "title=' {xLabel}: Count {value}'");
%>

<img src='<%=response.encodeURL("getchart.jsp?" + chart11URL)%>'
	usemap="#map11" border="0">
<map name="map11">
<%
		if(imageMap11 != null){
			out.println(imageMap11);
		}else{
			out.println("Chart is not available");
		}
%>
</map>


