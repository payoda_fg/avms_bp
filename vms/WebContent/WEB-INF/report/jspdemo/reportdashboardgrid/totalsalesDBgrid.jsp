<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();
	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("totalSalesDBG") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("totalSalesDBG");
	}
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) {
		for (int index = 0; index < reportDtos.size(); index++) {
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("spendYear", tier2ReportDto.getSpendYear());
			cellobj.put("reportPeriod",
					tier2ReportDto.getReportPeriod());
			cellobj.put("indirectExp", tier2ReportDto.getIndirectExp());
			cellobj.put("directExp", tier2ReportDto.getDirectExp());
			cellobj.put("totalSales", tier2ReportDto.getIndirectExp()
					+ tier2ReportDto.getDirectExp());
			cellobj.put("createdOn", tier2ReportDto.getCreatedOn());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
