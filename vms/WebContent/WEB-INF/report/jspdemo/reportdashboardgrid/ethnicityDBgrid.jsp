<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("ethnicityDBG") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("ethnicityDBG");
	}
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) {
		for (int index = 0; index < reportDtos.size(); index++) {
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("reportPeriod",
					tier2ReportDto.getReportPeriod());
			cellobj.put("indirectExp", tier2ReportDto.getIndirectExp());
			cellobj.put("directExp", tier2ReportDto.getDirectExp());
			cellobj.put("totalSales", tier2ReportDto.getTotalSales());
			cellobj.put("diversityPercent",
					tier2ReportDto.getDiversityPercent());
			cellobj.put("createdOn", tier2ReportDto.getCreatedOn());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
