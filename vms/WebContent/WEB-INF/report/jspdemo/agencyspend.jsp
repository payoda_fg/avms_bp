<%@page import="ChartDirector.*"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList,java.text.*"%>
<%
	List<Tier2ReportDto> reportDtos = null;
	Tier2ReportDto tier2ReportDto = null;

	ArrayList<Double> data1 = new ArrayList<Double>();
	ArrayList<String> labels1 = new ArrayList<String>();

	if (session.getAttribute("agencyDashboard") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("agencyDashboard");
		int i = 0;
		if (reportDtos != null) {
			for (int index = i; index < reportDtos.size(); index++) {
				tier2ReportDto = reportDtos.get(index);
				data1.add(tier2ReportDto.getCount());
				labels1.add(tier2ReportDto.getCertificateShortName());
				i++;
			}
		}
	}

	// The data for the pie chart
	double[] data = {};
	if (data1 != null) {
		data = new double[data1.size()];
		for (int i = 0; i < data.length; i++) {
			data[i] = Math.round(data1.get(i)/1000000);
		}
	}
	
	//System.out.println(datafinal[1]);

	// The labels for the pie chart
	String[] labels = {};
	if (labels1 != null) {
		labels = new String[labels1.size()];
		labels1.toArray(labels);
	}

	// Create a PieChart object of size 360 x 300 pixels
	//PieChart c = new PieChart(700, 300);

	// Set the center of the pie at (180, 140) and the radius to 100 pixels
	//c.setPieSize(320, 160, 100);

	// Add a title to the pie chart
	//c.addTitle("Ethnicity Breakdown Tier 2 Reporting");

	// Draw the pie in 3D
	//c.set3D();

	// Set the pie data and the pie labels
	//c.setData(data, labels);

	// Explode the 1st sector (index = 0)
	//c.setExplode(-1);

	// Output the chart
	//String chart4URL = c.makeSession(request, "chart4");

	// Include tool tip for the chart
	//String imageMap4 = c.getHTMLImageMap("", "",
	//		"title='{label}: US${value} ({percent}%)'");
	
	
	
	//--------------------------------------------------------Old--------------------------------------------------------
	/*Integer chartHeight=labels.length * 12;
	if(chartHeight < 300)
	{
		chartHeight=300;
	}
	
	// Create a XYChart object of size 560 x 280 pixels.
	XYChart c = new XYChart(1360, chartHeight);

	// Add a title to the chart using 14 pts Arial Bold Italic font
	//c.addTitle("Total Spend", "Arial Bold Italic", 14);

	// Set the plotarea at (50, 50) and of 500 x 200 pixels in size. Use alternating
	// light grey (f8f8f8) / white (ffffff) background. Set border to transparent and use
	// grey (CCCCCC) dotted lines as horizontal and vertical grid lines
	c.setPlotArea(100, 50, 1000, chartHeight-100, 0xffffff, 0xf8f8f8,
			Chart.Transparent,
			c.dashLineColor(0xcccccc, Chart.DotLine),
			c.dashLineColor(0xcccccc, Chart.DotLine));

	// Add a legend box at (50, 22) using horizontal layout. Use 10 pt Arial Bold Italic
	// font, with transparent background
	c.addLegend(50, 22, false, "Arial Bold Italic", 10).setBackground(
			Chart.Transparent);

	// Set the x axis labels
	c.xAxis().setLabels(labels);

	// Draw the ticks between label positions (instead of at label positions)
	c.xAxis().setTickOffset(0.5);

	// Add axis title
	c.yAxis().setTitle("Spend ($)");

	// Set axis line width to 2 pixels
	c.xAxis().setWidth(2);
	c.yAxis().setWidth(2);

	// Swapping X and Y Axis
	c.swapXY(true);
	
	// Add a multi-bar layer with 3 data sets
	BarLayer layer = c.addBarLayer(data, c.linearGradientColor(1000, 0, 0, 0, 0x00ff00,0x00ff00,false));
	//layer.addDataSet(data, 0x00ff00, "Spend");
	
	// Set bar shape to circular (cylinder)
	layer.setBarShape(Chart.CircleShape);

	// Configure the bars within a group to touch each others (no gap)
	layer.setBarGap(0.2, Chart.TouchBar);*/
	
	
	//--------------------------------------------------------New--------------------------------------------------------
	Integer chartWidth=labels.length * 22;
	if(chartWidth < 250)
	{
		chartWidth=250;
	}

	XYChart c = new XYChart(1360, chartWidth);
		
	c.setPlotArea(250, 50, 1000, chartWidth-100, 0xffffff, 0xf8f8f8,
			Chart.Transparent,
			c.dashLineColor(0xcccccc, Chart.DotLine),
			c.dashLineColor(0xcccccc, Chart.DotLine));	
	
	c.addLegend(50, 22, false, "Arial Bold Italic", 10).setBackground(
			Chart.Transparent);	
	
	c.xAxis().setLabels(labels);
	
	c.xAxis().setLabelStyle("Arial Bold", 8);
	
	c.xAxis().setTickOffset(0.5);
	
	c.xAxis().setTickColor(Chart.Transparent);	
		
	c.yAxis().setTitle("Spend ($)", "Arial Bold", 10);
	
	c.yAxis2().setColors(Chart.Transparent);	
	
	c.yAxis2().setLabelStyle("Arial Bold", 8);
	
	c.yAxis().setLabelFormat("{value|0,} m");
	
	c.swapXY(true);
	
	BarLayer layer = c.addBarLayer(data, c.linearGradientColor(1000, 0, 0, 0, 0x00ff00,0x00ff00,false));	
	layer.setBarGap(0.2, Chart.TouchBar);
	layer.setBarShape(Chart.CircleShape); 	
	
	// Output the chart
	String chart4URL = c.makeSession(request, "chart4");

	// Include tool tip for the chart
	String imageMap4 = c.getHTMLImageMap("", "",
			"title='{xLabel} : ${value}'");
%>

<img src='<%=response.encodeURL("getchart.jsp?" + chart4URL)%>'
	usemap="#map4" border="0">
<map name="map4">
	<%
		if (imageMap4 != null) {
			out.println(imageMap4);
		} else {
			out.println("Chart is not available");
		}
	%>
</map>


