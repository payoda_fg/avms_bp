<%@page import="ChartDirector.*"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%
	List<Tier2ReportDto> reportDtos = null;
	Tier2ReportDto tier2ReportDto = null;

	ArrayList<Double> data1 = new ArrayList<Double>();
	ArrayList<String> labels1 = new ArrayList<String>();

	if (session.getAttribute("dashboard") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("dashboard");
		int i = 0;
		if (reportDtos != null) {
			for (int index = i; index < reportDtos.size(); index++) {
				tier2ReportDto = reportDtos.get(index);
				data1.add(tier2ReportDto.getCount());
				labels1.add(tier2ReportDto.getEthnicity());
				i++;
			}
		}
	}

	// The data for the pie chart
	double[] data = {};
	if (data1 != null) {
		data = new double[data1.size()];
		for (int i = 0; i < data.length; i++) {
			data[i] = Math.round(data1.get(i));
		}
	}
	

	// The labels for the pie chart
	String[] labels = {};
	if (labels1 != null) {
		labels = new String[labels1.size()];
		labels1.toArray(labels);
	}
	
	// Create a PieChart object of size 360 x 300 pixels
	PieChart c = new PieChart(500, 300);

	// Set the center of the pie at (180, 140) and the radius to 100 pixels
	c.setPieSize(220, 140, 100);

	// Add a title to the pie chart
	//c.addTitle("Ethnicity Breakdown Tier 2 Reporting");

	// Draw the pie in 3D
	c.set3D();

	// Set the pie data and the pie labels
	c.setData(data, labels);

	// Explode the 1st sector (index = 0)
	c.setExplode(-1);

	// Output the chart
	String chart1URL = c.makeSession(request, "chart1");

	// Include tool tip for the chart
	String imageMap1 = c.getHTMLImageMap("", "",
			"title='{label}: {value} ({percent}%)'");
%>

<img src='<%=response.encodeURL("getchart.jsp?" + chart1URL)%>'
	usemap="#map1" border="0">
<map name="map1">
<%
	if(imageMap1 != null){
		out.println(imageMap1);
	}else{
		out.println("Chart is not available");
	}
%>
</map>


