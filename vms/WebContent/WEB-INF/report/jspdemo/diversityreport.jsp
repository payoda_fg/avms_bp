<%@page import="ChartDirector.*"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%
	List<Tier2ReportDto> reportDtos = null;
	Tier2ReportDto tier2ReportDto = null;

	ArrayList<Double> data1 = new ArrayList<Double>();
	ArrayList<Double> data2 = new ArrayList<Double>();
	ArrayList<String> labels1 = new ArrayList<String>();

	if (session.getAttribute("diversityDBG") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("diversityDBG");
		int i = 0;
		if (reportDtos != null) {
			for (int index = i; index < reportDtos.size(); index++) {
				tier2ReportDto = reportDtos.get(index);
				data1.add(tier2ReportDto.getDirectExp());
				data2.add(tier2ReportDto.getTotalSales());
				labels1.add(tier2ReportDto.getVendorUserName());
				i++;
			}
		}
	}

	// The data for the pie chart
	double[] data4 = {};
	if (data1 != null) {
		data4 = new double[data1.size()];
		for (int i = 0; i < data4.length; i++) {
			data4[i] = Math.round(data1.get(i)/1000000);
		}
	}
	double[] data5 = {};
	if (data2 != null) {
		data5 = new double[data2.size()];
		for (int i = 0; i < data5.length; i++) {
			data5[i] = Math.round(data2.get(i)/1000000);
		}
	}

	// The labels for the pie chart
	String[] labels = {};
	if (labels1 != null) {
		labels = new String[labels1.size()];
		labels1.toArray(labels);
	}

	Integer chartHeight=labels.length * 22;
	if(chartHeight < 400)
	{
		chartHeight=400;
	}
	
	// Create a XYChart object of size 560 x 280 pixels.
	XYChart c = new XYChart(1360, chartHeight);

	// Add a title to the chart using 14 pts Arial Bold Italic font
	//c.addTitle("Total Spend", "Arial Bold Italic", 14);

	// Set the plotarea at (50, 50) and of 500 x 200 pixels in size. Use alternating
	// light grey (f8f8f8) / white (ffffff) background. Set border to transparent and use
	// grey (CCCCCC) dotted lines as horizontal and vertical grid lines
	c.setPlotArea(250, 50, 1000, chartHeight-100, 0xffffff, 0xf8f8f8,
			Chart.Transparent,
			c.dashLineColor(0xcccccc, Chart.DotLine),
			c.dashLineColor(0xcccccc, Chart.DotLine));

	// Add a legend box at (50, 22) using horizontal layout. Use 10 pt Arial Bold Italic
	// font, with transparent background
	c.addLegend(50, 22, false, "Arial Bold Italic", 10).setBackground(
			Chart.Transparent);

	// Set the x axis labels
	c.xAxis().setLabels(labels);

	// Draw the ticks between label positions (instead of at label positions)
	c.xAxis().setTickOffset(0.5);

	// Add axis title
	c.yAxis().setTitle("Spend ($)");

	// Set axis line width to 2 pixels
	c.xAxis().setWidth(2);
	c.yAxis().setWidth(2);
	
	c.yAxis().setLabelFormat("{value|0,} m");
	
	// Swapping X and Y Axis
	c.swapXY(true);

	// Add a multi-bar layer with 3 data sets
	BarLayer layer = c.addBarLayer2(Chart.Side);
	layer.addDataSet(data4, 0xff0000, "Direct Spend");
	layer.addDataSet(data5, 0x00ff00, "Total Spend");
	//layer.addDataSet(data6, 0x0000ff, "Total Spend");

	// Set bar shape to circular (cylinder)
	layer.setBarShape(Chart.CircleShape);

	// Configure the bars within a group to touch each others (no gap)
	layer.setBarGap(0.2, Chart.TouchBar);

	// Output the chart
	String chart8URL = c.makeSession(request, "chart8");

	// Include tool tip for the chart
	String imageMap8 = c.getHTMLImageMap("", "",
			"title='{dataSetName} for {xLabel} : ${value}'");
%>
<html>
<body>
	<img src='<%=response.encodeURL("getchart.jsp?" + chart8URL)%>'
		usemap="#map8" border="0">
	<map name="map8"><%=imageMap8%></map>
</body>
</html>

