<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("vendorCommoditiesNotSavedReport") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("vendorCommoditiesNotSavedReport");
	}
	
	/********************************** For Building Grouping Name Starts Here **********************************/
	Tier2ReportDto tier2ReportDto1 = null;
	StringBuilder bpVendorLabel= new StringBuilder("BP Vendor");
	StringBuilder activeVendorLabel=new StringBuilder("Reviewed by Supplier Diversity");
	StringBuilder pendingVendorLabel=new StringBuilder("Pending Review");
	StringBuilder newRegVendorLabel=new StringBuilder("New Registration");
	
	if (reportDtos != null) 
	{
		int bpvendorCount=0;
		int activeVendorCount=0;
		int pendingVendorCount=0;
		int newRegVendorCount=0;
		int totalCount=0;
		
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto1 = reportDtos.get(index);
			if(tier2ReportDto1.getStatusName().equalsIgnoreCase("BP Vendor"))
			{
				bpvendorCount++;
			}
			else if(tier2ReportDto1.getStatusName().equalsIgnoreCase("Reviewed by Supplier Diversity"))
			{
				activeVendorCount++;
			}
			else if(tier2ReportDto1.getStatusName().equalsIgnoreCase("Pending Review"))
			{
				pendingVendorCount++;
			}
			else if(tier2ReportDto1.getStatusName().equalsIgnoreCase("New Registration"))
			{
				newRegVendorCount++;
			}
			totalCount=tier2ReportDto1.getCount().intValue();
		}
		
		float bpVendorPercent=Math.round(bpvendorCount*100f/totalCount);
		float activeVendorPercent=Math.round(activeVendorCount*100f/totalCount);
		float pendingVendorPercent=Math.round(pendingVendorCount*100f/totalCount);
		float newRegVendorPercent=Math.round(newRegVendorCount*100f/totalCount);
		
		bpVendorLabel.append(" - " + bpvendorCount + " Item(s) Out of " + totalCount+ " Item(s), Percentage: " + bpVendorPercent+"% .");
		activeVendorLabel.append(" - " + activeVendorCount + " Item(s) Out of " + totalCount+ " Item(s), Percentage: " + activeVendorPercent+"% .");
		pendingVendorLabel.append(" - " + pendingVendorCount + " Item(s) Out of " + totalCount+ " Item(s), Percentage: " + pendingVendorPercent+"% .");
		newRegVendorLabel.append(" - " + newRegVendorCount + " Item(s) Out of " + totalCount+ " Item(s), Percentage: " + newRegVendorPercent+"% .");

	}
	/********************************** For Building Grouping Name Ends Here **********************************/
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("vendorId", tier2ReportDto.getVendorId());
			cellobj.put("vendorStatus", tier2ReportDto.getStatusName());
			
			if(tier2ReportDto.getStatusName().equalsIgnoreCase("BP Vendor"))
			{
				cellobj.put("vendorStatusLabel", bpVendorLabel.toString());
			}
			else if(tier2ReportDto.getStatusName().equalsIgnoreCase("Reviewed by Supplier Diversity"))
			{
				cellobj.put("vendorStatusLabel", activeVendorLabel.toString());
			}
			else if(tier2ReportDto.getStatusName().equalsIgnoreCase("Pending Review"))
			{
				cellobj.put("vendorStatusLabel", pendingVendorLabel.toString());
			}
			else if(tier2ReportDto.getStatusName().equalsIgnoreCase("New Registration"))
			{
				cellobj.put("vendorStatusLabel", newRegVendorLabel.toString());
			}
			
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("firstName", tier2ReportDto.getFirstName());
			cellobj.put("lastName", tier2ReportDto.getLastName());
			cellobj.put("emailId", tier2ReportDto.getEmailId());
			cellobj.put("phone", tier2ReportDto.getPhone());
			cellobj.put("city", tier2ReportDto.getCity());
			cellobj.put("state", tier2ReportDto.getState());			
			cellobj.put("vendorCount", tier2ReportDto.getStatusCount());
			cellobj.put("totalCount", tier2ReportDto.getCount());
			cellobj.put("statusPercent", Math.round(tier2ReportDto.getStatusPercent()) + "%");
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>