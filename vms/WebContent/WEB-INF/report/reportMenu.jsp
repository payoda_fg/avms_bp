<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!--Menupart -->
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>

<section class="content-body">
		<header class="page-header">
			<h2>Reports</h2>
		</header>
		
	<div class="inner-wrapper">
		<aside id="sidebar-left" class="sidebar-left">
				
				    <div class="sidebar-header">
				         <div class="sidebar-title">Navigation</div>
				        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
				            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				        </div>
				    </div>
				
				    <div class="nano sidepanelMenu">
				        <div class="nano-content">
				            <nav id="menu" class="nav-main" role="navigation">				            
				                <ul class="nav nav-main">
				                    <li class="nav-active">				                           
				                            <logic:iterate id="privilege" name="privileges">
												<logic:equal value="Prime Supplier Report by Tier 2 Total Spend"
													name="privilege" property="objectId.objectName">
													<logic:equal value="1" name="privilege" property="visible">
														<a class="nav-link" href="#"
															onclick="linkPage('totalSalesReport.do?method=viewTotalSalesReport')">
															<i class="icon-img">
				                            <img src="bpimages/supplierbytotalspend.png" /> </i> 
															<span
															class="icon-text">Prime Supplier Report by Tier 2 Total
																Spend</span> </a>
													</logic:equal>
												</logic:equal>
											</logic:iterate>  
										
				                    </li>
				                    
				                    <li>
				                            <logic:iterate id="privilege" name="privileges">
				<logic:equal value="Prime Supplier Report by Tier 2 Direct Spend"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-9"
							onclick="linkPage('tier2Report.do?method=viewDirectSpendReport')">
							<i class="icon-img"><img
								src="bpimages/supplierbydirectspend.png" /> </i>
							<span class="icon-text">Prime Supplier Report by Tier 2 Direct
								Spend </span>  </a>
									</logic:equal>
								</logic:equal>
							</logic:iterate>				                        
				                    </li>
				                   			                    
				                 		
				                    <li>
				                       <logic:iterate id="privilege" name="privileges">
				<logic:equal value="Prime Supplier Report by Tier 2 Indirect Spend"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-10"
							onclick="linkPage('tier2Report.do?method=viewIndirectSpendReport')">
							<i class="icon-img"><img
								src="bpimages/supplierbyindirectspend.png" /> </i>
								<span class="icon-text">Prime Supplier Report by Tier 2
								Indirect Spend</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>                        
				                    </li>
				                    <li>
				                    	<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Prime Supplier by Tier 2 Diversity Category"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-7"
							onclick="linkPage('diversityReport.do?method=viewDiversityReport')">
							<i class="icon-img"><img
								src="bpimages/supplierbydiversitystatus.png" /> </i> 
							<span
							class="icon-text">Prime Supplier by Tier 2 Diversity
								Category</span> </a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
				                    </li>
				                    <li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Tier 2 Reporting Summary"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-8"
							onclick="linkPage('ethnicityReport.do?method=viewEthnicityBreakdownReport')">
							<i class="icon-img"><img src="bpimages/ethnicity.png" /> </i>
							<span class="icon-text">Tier 2 Reporting Summary</span>  </a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Spend Data Dashboard Report" name="privilege"
					property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-11"
							onclick="linkPage('spendReport.do?method=viewSpendDashboard')">
							<i class="icon-img"><img src="bpimages/spenddatadashbord.png" /></i>
							<span class="icon-text">Spend Data Dashboard Report</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Supplier Custom Search Report" name="privilege"
					property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3"
							onclick="linkPage('searchReport.do?method=searchVendorReport')">
							<i class="icon-img"><img src="bpimages/tier2spend.png" /> </i>
							<span class="icon-text">Supplier Custom Search Report</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Diversity Analysis"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3"
							onclick="linkPage('diversityAnalysisReport.do?method=viewDiversityAnalysisReport')">
							<i
							class="icon-img"><img src="bpimages/diversity-analysis.png" />
						</i>
							<span class="icon-text">Diversity Analysis</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendor Status Breakdown"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3"
							onclick="linkPage('vendorStatusBreakdownReport.do?method=viewVendorStatusBreakdownReport')">
							<i
							class="icon-img"><img src="bpimages/vendor-status-breakdown.png" />
						</i>
							<span class="icon-text">Vendor Status Breakdown</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Supplier Count By State"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3"
							onclick="linkPage('supplierCountByStateReport.do?method=viewSupplierCountByStateReport')">
							<i 	class="icon-img"><img src="bpimages/supplier-count-by-state.png" />	</i>
							<span class="icon-text">Supplier Count By State</span>
							 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="BP Vendor Count By State"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3"
							onclick="linkPage('bpVendorCountByStateReport.do?method=viewBPVendorCountByStateReport')">
							<i
							class="icon-img"><img src="bpimages/bp-vendor-count-by-state.png" />
						</i>
							<span class="icon-text">BP Vendor Count By State</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Spend By Agency"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3"
							onclick="linkPage('spendByAgencyReport.do?method=viewSpendByAgencyReport')">
							<i
							class="icon-img"><img src="bpimages/spend-by-agency.png" />
						</i>
							<span class="icon-text">Spend By Agency</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendor Commodities Not Saved" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('vendorCommoditiesNotSavedReport.do?method=viewVendorCommoditiesNotSavedReport')">
							<i class="icon-img"><img src="bpimages/vendor-commodities-not-saved.png" /></i>
							<span class="icon-text">Vendor Commodities Not Saved</span> 
							
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Certificate Expiration Notification Email" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link"  href="#" class="icon-3" onclick="linkPage('certificateExpirationNotificationEmailReport.do?method=viewCertificateExpirationNotificationEmailReport')">
							<i class="icon-img"><img src="bpimages/certificate-expiration-notification-email.png" /></i>
							<span class="icon-text">Certificate Expiration Notification Email</span> 
							
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Registered Vendors" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('registeredVendorsReport.do?method=viewRegisteredVendorsReport')">
							<i class="icon-img"><img src="bpimages/registered-vendors.png" /></i>
							<span class="icon-text">Registered Vendors Report</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendors By Business Type Report" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('vendorsByIndustryReport.do?method=viewVendorsByIndustryReport')">
							<i class="icon-img"><img src="bpimages/vendors-by-industry.png" /></i>
							<span class="icon-text">Vendors By Business Type</span> 
						
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendors By NAICS Description Report" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('vendorsByNaicsReport.do?method=viewVendorsByNaicsReport')">
							
							<i class="icon-img"><img src="bpimages/vendors-by-naics-description.png" /></i>
							<span class="icon-text">Vendors By NAICS Description</span> 
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendors By BP Market Sector Report" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('vendorsByBPMarketSectorReport.do?method=viewVendorsByBPMarketSectorReport')">
							<i class="icon-img"><img src="bpimages/vendors-by-bp-market-sector.png" /></i>
							<span class="icon-text">Vendors By BP Market Sector</span> 
				
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="BP Market Sector Search Report" name="privilege" property="objectId.objectName">
						<logic:equal value="1" name="privilege" property="visible">
							<a href="#" class="icon-3 nav-link" onclick="linkPage('indirectProcurementCommoditiesReport.do?method=viewIndirectProcurementCommoditiesReport')">								
								<i class="icon-img"><img src="bpimages/indirect-procurement-commodities.png" /></i>
								<span class="icon-text">Indirect Procurement Suppliers</span> 
							</a>
						</logic:equal>
					</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendors By BP Market Subsector Report" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('vendorsByBPMarketSubsectorReport.do?method=viewVendorsByBPMarketSubsectorReport')">
							<i class="icon-img"><img src="bpimages/vendors-by-bp-market-sector.png" /></i>
							<span class="icon-text">Vendors By BP Market Subsector</span> 
							
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Vendors By BP Commodity Group Report" name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-3" onclick="linkPage('vendorsByBPCommodityGroupReport.do?method=viewVendorsByBPCommodityGroupReport')">
							<i class="icon-img"><img src="bpimages/vendors-by-bp-market-sector.png" /></i>
							<span class="icon-text">Vendors By BP Commodity Group</span> 
							
						</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
			
			<li>
			<logic:iterate id="privilege" name="privileges">
				<logic:equal value="Prime Supplier by Tier 2 Diversity By Ethnicity"
					name="privilege" property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="visible">
						<a class="nav-link" href="#" class="icon-7"
							onclick="linkPage('diversityReport.do?method=viewDiversityReport&type=ethnicity')">
							<i class="icon-img"><img
								src="bpimages/supplierbydiversitystatus.png" /> </i> 
							<span class="icon-text">Prime Supplier by Tier 2 Diversity By Ethnicity</span> 
							</a>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			</li>
				                </ul>
				            </nav>
				
				            <hr class="separator" />
				
				            
				        </div>
				
				        <script>
				            // Maintain Scroll Position
				            if (typeof localStorage !== 'undefined') {
				                if (localStorage.getItem('sidebar-left-position') !== null) {
				                    var initialPosition = localStorage.getItem('sidebar-left-position'),
				                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');
				                    
				                    sidebarLeft.scrollTop = initialPosition;
				                }
				            }
				        </script>
				    </div>				
				</aside>
		</div>
	<div class="clear"></div>
</section>
<div class="inner-menu-list-green">
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Prime Supplier Report by Tier 2 Total Spend"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('totalSalesReport.do?method=viewTotalSalesReport')">
					<img src="images/new-icon/report-6.png" alt=""
					title="Prime Supplier Report by Tier 2 Total Spend" />
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Prime Supplier Report by Tier 2 Direct Spend"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('tier2Report.do?method=viewDirectSpendReport')"><img
					src="images/new-icon/icon-9.png" alt=""
					title="Prime Supplier Report by Tier 2 Direct Spend" /> </a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Prime Supplier Report by Tier 2 Indirect Spend"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('tier2Report.do?method=viewIndirectSpendReport')">
					<img src="images/new-icon/icon-10.png" alt=""
					title="Prime Supplier Report by Tier 2 Indirect Spend" />
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Prime Supplier by Tier 2 Diversity Category"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('diversityReport.do?method=viewDiversityReport')"><img
					src="images/new-icon/icon-7.png" alt=""
					title="Supplier by Tier 2 Diversity Status" /> </a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Tier 2 Reporting Summary"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('ethnicityReport.do?method=viewEthnicityBreakdownReport')"><img
					src="images/new-icon/icon-8.png" alt=""
					title="Tier 2 Reporting Summary" /> </a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Spend Data Dashboard Report" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('spendReport.do?method=viewSpendDashboard')"><img
					src="bpimages/spenddatadashbord.png" alt=""
					title="Spend Data Dashboard Report" /> </a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Supplier Custom Search Report" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('searchReport.do?method=searchVendorReport')"><img
					src="bpimages/tier2spend.png" alt=""
					title="Supplier Custom Search Report" /> </a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Diversity Analysis"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" class="icon-3"
					onclick="linkPage('diversityAnalysisReport.do?method=viewDiversityAnalysisReport')">
					<img src="bpimages/diversity-analysis.png" alt="" title="Diversity Analysis"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendor Status Breakdown"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorStatusBreakdownReport.do?method=viewVendorStatusBreakdownReport')">
					<img src="bpimages/vendor-status-breakdown.png" alt="" title="Vendor Status Breakdown"/>				
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Supplier Count By State"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('supplierCountByStateReport.do?method=viewSupplierCountByStateReport')">
					<img src="bpimages/supplier-count-by-state.png" alt="" title="Supplier Count By State"/>				
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="BP Vendor Count By State"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('bpVendorCountByStateReport.do?method=viewBPVendorCountByStateReport')">
					<img src="bpimages/bp-vendor-count-by-state.png"  alt="" title="BP Vendor Count By State"/>				
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Spend By Agency"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('spendByAgencyReport.do?method=viewSpendByAgencyReport')">
					<img src="bpimages/spend-by-agency.png"   alt="" title="Spend By Agency"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendor Commodities Not Saved" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorCommoditiesNotSavedReport.do?method=viewVendorCommoditiesNotSavedReport')">
					<img src="bpimages/vendor-commodities-not-saved.png"   alt="" title="Vendor Commodities Not Saved Report"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Certificate Expiration Notification Email" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('certificateExpirationNotificationEmailReport.do?method=viewCertificateExpirationNotificationEmailReport')">					
					<img src="bpimages/certificate-expiration-notification-email.png" alt="" title="Certificate Expiration Notification Email"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Registered Vendors" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('registeredVendorsReport.do?method=viewRegisteredVendorsReport')">
					<img src="bpimages/registered-vendors.png" alt="" title="Registered Vendors Report"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendors By Business Type Report" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorsByIndustryReport.do?method=viewVendorsByIndustryReport')">
					<img src="bpimages/vendors-by-industry.png" alt="" title="Vendors By Business Type"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendors By NAICS Description Report" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorsByNaicsReport.do?method=viewVendorsByNaicsReport')">
					<img src="bpimages/vendors-by-naics-description.png" alt="" title="Vendors By NAICS Description"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendors By BP Market Sector Report" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorsByBPMarketSectorReport.do?method=viewVendorsByBPMarketSectorReport')">
					<img src="bpimages/vendors-by-bp-market-sector.png" alt="" title="Vendors By BP Market Sector"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="BP Market Sector Search Report" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('indirectProcurementCommoditiesReport.do?method=viewIndirectProcurementCommoditiesReport')">
					<img src="bpimages/indirect-procurement-commodities.png" alt="" title="BP Market Sector Search"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendors By BP Market Subsector Report" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorsByBPMarketSubsectorReport.do?method=viewVendorsByBPMarketSubsectorReport')">
					<img src="bpimages/vendors-by-bp-market-sector.png" alt="" title="Vendors By BP Market Sector"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Vendors By BP Commodity Group Report" name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#" onclick="linkPage('vendorsByBPCommodityGroupReport.do?method=viewVendorsByBPCommodityGroupReport')">
					<img src="bpimages/vendors-by-bp-market-sector.png" alt="" title="Vendors By BP Market Sector"/>
				</a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Prime Supplier by Tier 2 Diversity By Ethnicity"
			name="privilege" property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<a href="#"
					onclick="linkPage('diversityReport.do?method=viewDiversityReport&type=ethnicity')"><img
					src="images/new-icon/icon-7.png" alt=""
					title="Supplier by Tier 2 Diversity Status" /> </a>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
</div>


<script type="text/javascript">
	var pathname = window.location.href;

	if (pathname.indexOf("viewDiversityReport") >= 0) {
		$('.icon-7').addClass("active");
	} else if (pathname.indexOf("viewIndirectSpendReport") >= 0) {
		$('.icon-10').addClass("active");
	} else if (pathname.indexOf("viewSpendDashboard") >= 0) {
		$('.icon-11').addClass("active");
	} else if (pathname.indexOf("privileges") >= 0) {
		$('.icon-17').addClass("active");
	} else if (pathname.indexOf("viewDirectSpendReport") >= 0) {
		$('.icon-9').addClass("active");
	} else if (pathname.indexOf("viewTotalSalesReport") >= 0) {
		$('.icon-28').addClass("active");
	} else if (pathname.indexOf("viewEthnicityBreakdownReport") >= 0) {
		$('.icon-8').addClass("active");
	} else if (pathname.indexOf("searchVendorReport") >= 0) {
		$('.icon-3').addClass("active");
	}
</script>