<%@page import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null;
	List<Tier2ReportDto> reportDtos = null;
	
	if (session.getAttribute("newRegistrationsByMonth") != null) 
	{
		reportDtos = (List<Tier2ReportDto>) session.getAttribute("newRegistrationsByMonth");
	}
	
	Tier2ReportDto tier2ReportDto = null;
	if (reportDtos != null) 
	{
		for (int index = 0; index < reportDtos.size(); index++) 
		{
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			
			cellobj.put("month", tier2ReportDto.getMonth());
			cellobj.put("count", tier2ReportDto.getCount().intValue());
			cellobj.put("percentage", Math.round(tier2ReportDto.getPercentage()));
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>