<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<logic:iterate id="privilege" name="privileges">
<logic:equal value="Prime Supplier Report by Tier 2 Direct Spend"
			name="privilege" property="objectId.objectName">
<logic:equal value="1" name="privilege" property="view">

<!--  theme css starts -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="css/theme.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/custom.css" type="text/css" />
<!--  theme css ends-->

<!-- Added for jquery validation -->
<script src="js/jquery.js"></script>
<script src="js/jquery.browser.mobile.js"></script>
<script type="text/javascript"  src="js/bootstrap.js"></script>

<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<!-- Added for populate jqgrid. -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/ui.jqgrid.css" />

<!-- <link rel="stylesheet" type="text/css" media="screen"
	href="jquery/css/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css"
	href="jquery/css/redmond/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>-->
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>
<style type="text/css">
/*th.ui-th-column div {
	white-space: normal !important;
	height: auto !important;
	padding: 2px;
}

.ui-jqgrid tr.jqgrow td {
	white-space: normal !important;
	height: auto;
	vertical-align: text-top;
	padding-top: 2px;
}*/
</style>


<script>
$(function() {
	  var start_year = new Date().getFullYear();

	  for (var i = start_year; i > start_year - 15; i--) {
	    $('#spendYear').append('<option value="' + i + '">' + i + '</option>');
	  }
	  
	  $('#spendYear').val($("#spendYearH").val());
	});

$( document ).ajaxError(function() {
	$("#ajaxloader").hide();
});

	$(document).ready(function() {

		$("#tier2VendorNames").multiselect({
			selectedText : "# of # selected"
		});
		$("#reportingPeriod").multiselect({
			multiple : false,
			header : "Select an option",
			noneSelectedText : "Select an Option",
			selectedList : 1
		});
		$("#spendYear").multiselect({
			multiple : false,
			header : "-- Year --",
			noneSelectedText : "-- Year --",
			selectedList : 1,
			minWidth : '160'
		});

		$("#spendYear").change(function() {
			var id = $(this).val();
			var dataString = 'year=' + id;

			$.ajax({
				type : "POST",
				url : "spendReport.do?method=loadReportPeriod",
				data : dataString,
				cache : false,
				success : function(html) {

					$("#reportingPeriod").html(html);

					$("#reportingPeriod").multiselect({
						multiple : false,
						header : "Select an option",
						noneSelectedText : "Select an Option",
						selectedList : 1
					});
				}
			});
		});
	});
</script>
<style>
<!-- /* .ui-jqgrid .ui-jqgrid-btable {
	table-layout: fixed;
}

.ui-jqgrid .ui-jqgrid-bdiv {
	position: relative;
	margin: 0em;
	padding: 0;
	overflow-x: hidden;
	overflow-y: auto;
	text-align: left;
} */
-->
</style>


<section role="main" class="content-body card-margin reports-module">
	<div class="row">
	<div class="col-lg-8 mx-auto col-sm-12 col-md-12 col-xl-6 col-xs-12">
<div class="form-box">
	<section class="card">
		<header class="card-header">
			<h2 class="card-title">Prime Supplier Report by Tier 2 Direct Spend</h2>
		</header>
		<div class="card-body">
			<div class="form-group row row-wrapper">
				<label class="col-sm-3 control-label text-sm-right label-col-wrapper">View the
					report by Vendor:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:select property="tier2VendorNames" multiple="true" styleClass="form-control" 
						name="tier2ReportForm" styleId="tier2VendorNames">
						<logic:present name="tier2ReportForm" property="vendors">
						<html:optionsCollection name="tier2ReportForm" property="vendors" label="vendorName" value="id"/>
						</logic:present>
					</html:select>
				</div>
			</div>
			<div class="form-group row">	
					<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Spend
					Year:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<select name="spendYear" id="spendYear"  class="form-control" required="">
						<option value="0">-- Select --</option>
						<logic:present name="tier2ReportForm" property="spendYears">
						<logic:iterate id="year" name="tier2ReportForm" property="spendYears">
						<option value="${year}">${year}</option>
						</logic:iterate>
						</logic:present>
					</select>
				</div>
			</div>
			<div class="form-group row">	
				<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Reporting
					Period:</label>
				<div class="col-sm-9 ctrl-col-wrapper">
					<select id="reportingPeriod" name="reportingPeriod" class="form-control">
						<option value="0">-- Select --</option>
					</select>
				</div>
			</div>
	</div><!--  card body -->
		<footer class="card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9 wrapper-btn">
					<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getDirectSpendData();" />
				</div>			
			</div>
		</footer>
		</section>
</div>
</div>
</div>
<div class="clear"></div>
<div class="grid-wrapper view-reports-table">
	<div class="form-box">
		<div class="row">
			<div class="col-lg-12">
				<section class="card">
					<div class="card-body">
						<div id="grid_container">
							<div class="responsiveGrid">
								<table id="gridtable" class="table table-bordered table-striped mb-0">
									<tbody>
										<tr>
											<td />
										</tr>
									</tbody>
								</table>
								<div id="pager"></div>
							</div>
						</div>
						</div>
				</section>
			</div>
		</div>
	</div>
</div>
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table style="width: 100%;">
		<tr>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="1" id="excel"></td>
			<td><label for="excel"><img id="excelExport"
					src="images/excel_export.png" />Excel</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="2" id="csv"> <input type="hidden" name="fileName"
				id="fileName" value="DirectSpendReport"></td>
			<td><label for="csv"><img id="csvExport"
					src="images/csv_export.png" />CSV</label></td>
			<td style="padding: 1%;"><input type="radio" name="export"
				value="3" id="pdf"></td>
			<td><label for="pdf"><img id="pdfExport"
					src="images/pdf_export.png" />PDF</label></td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<logic:present name="userDetails">
			<bean:define id="logoPath" name="userDetails"
				property="settings.logoPath"></bean:define>
			<input type="button" value="Export" class="exportBtn btn btn-primary"
				onclick="exportDirectSpend('gridtable','<%=logoPath%>');">
		</logic:present>
	</div>
</div>
</section>
</logic:equal>
<logic:equal value="0" name="privilege" property="view">
			<div class="form-box">
				<h3>You have no rights to view Prime Supplier Report by Tier 2 Direct Spend.</h3>
			</div>
</logic:equal>
</logic:equal>
</logic:iterate>
</html>