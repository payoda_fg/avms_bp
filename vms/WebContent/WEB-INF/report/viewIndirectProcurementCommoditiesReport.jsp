<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!-- Included to Generate Report -->
<script type="text/javascript" src="jquery/js/jquery.ajax.avms.js"></script>

<!-- Added for Populate JqGrid -->
<script type="text/javascript" src="jquery/js/grid.locale-en.js"></script>
<script type="text/javascript" src="jquery/js/jquery.jqGrid.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/ui.jqgrid.css"/>

<!-- Added for Export CSV Files -->
<script type="text/javascript" src="jquery/js/table2CSV.js"></script>

<!-- Added for MultiSelect Drop-down Box -->
<!--  <script type="text/javascript" src="jquery/js/jquery.multiselect.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="jquery/css/jquery.multiselect.css" /> -->

<!-- Heading Part-->

<section role="main" class="content-body card-margin">
	<div class="row">
		<div class="col-lg-12 mx-auto">
			<header class="card-header">
				<h2 class="card-title pull-left">BP Market Sector Search</h2>
			</header>
			
<logic:iterate id="privilege" name="privileges">
	<logic:equal value="BP Market Sector Search Report" name="privilege" property="objectId.objectName">
		<logic:equal value="1" name="privilege" property="view">			
			<!-- Form Part -->
			<div class="form-box card-body">
				<html:form styleId="reportform">				
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Vendor Status:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="vendorStatusList" name="tier2ReportForm"	styleClass="form-control" styleId="vendorStatusList" multiple="true">
		 							<bean:size id="size" name="VendorStatusMasterList" />
		 							<logic:greaterEqual value="0" name="size">
		 								<logic:iterate id="statusList" name="VendorStatusMasterList">
		 									<bean:define id="id" name="statusList" property="id"/>
		 									<html:option value="${id}">
		 										<bean:write name="statusList" property="statusname"/>
		 									</html:option>
		 								</logic:iterate>
		 							</logic:greaterEqual>
	 							</html:select>
							</div>
						</div>
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Market Sectors:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="indirectCommoditiesList" styleClass="form-control" name="tier2ReportForm" styleId="indirectCommoditiesList" multiple="true"> 
		 							<bean:size id="size" name="marketSectorsList"/>
		 							<logic:greaterEqual value="0" name="size">
		 								<logic:iterate id="marketSectors" name="marketSectorsList">
		 									<bean:define id="id" name="marketSectors" property="id"/>
		 									<html:option value="${id}">
		 										<bean:write name="marketSectors" property="sectorDescription"/>
		 									</html:option>
		 								</logic:iterate>
		 							</logic:greaterEqual>
	 							</html:select>
							</div>
						</div>
					
					<logic:equal value="1" name="privilege" property="modify">
						<div style="float: right;">
							<input type="button" value="Save Selected Commodity List" class="btn btn-primary" onclick="saveIndirectProcurementCommodityList();"/>						
						</div>
					</logic:equal>
					
						<div class="row-wrapper form-group row">
							<label class="col-sm-3 label-col-wrapper control-label text-sm-right">Certificate Type:</label>
							<div class="col-sm-9 ctrl-col-wrapper">
								<html:select property="certificateTypeList" name="tier2ReportForm" styleId="certificateTypeList" multiple="true" styleClass="form-control"> 
		 							<bean:size id="size" name="certificateTypesList"/>
		 							<logic:greaterEqual value="0" name="size">
		 								<logic:iterate id="certificateTypes" name="certificateTypesList">
		 									<bean:define id="id" name="certificateTypes" property="id"/>
		 									<html:option value="${id}">
		 										<bean:write name="certificateTypes" property="certificateTypeDesc"/>
		 									</html:option>
		 								</logic:iterate>
		 							</logic:greaterEqual>
	 							</html:select>
							</div>
						</div>
					
					<div class="wrapper-btn text-sm-right">
						<input type="button" value="Get Report" class="btn btn-primary" id="spendreport" onclick="getIndirectProcurementCommoditiesReport();" />						
					</div>					
				</html:form>
			</div>
			
			<div class="clear"></div>
			
			<!-- Result Part -->	
			<div class="form-box card-body">		
			<div id="grid_container" style="width: 100%;">
				<table id="gridtable" class="main-table table table-bordered table-striped mb-0">
					<tr>
						<td />
					</tr>
				</table>
				<div id="pager"></div>
			</div>
			</div>
		</logic:equal>
		<logic:equal value="0" name="privilege" property="view">
			<div class="form-box card-body">
				<h3>You have no rights to view bp market sector search.</h3>
			</div>
		</logic:equal>
		
	</logic:equal>
</logic:iterate>

<!-- Export Dialog Box -->
<div id="dialog" style="display: none;" title="Choose Export Type">
	<p>Please Choose the Export Type</p>
	<table class="table table-bordered table-striped mb-0">
		<tr>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="1" id="excel">
			</td>
			<td>
				<label for="excel">
					<img id="excelExport" src="images/excel_export.png" />Excel
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="2" id="csv"> 
				<input type="hidden" name="fileName" id="fileName" value="BPMarketSectorSearchReport">
			</td>
			<td>
				<label for="csv">
					<img id="csvExport" src="images/csv_export.png" />CSV
				</label>
			</td>
			<td style="padding: 1%;">
				<input type="radio" name="export" value="3" id="pdf">
			</td>
			<td>
				<label for="pdf">
					<img id="pdfExport" src="images/pdf_export.png" />PDF
				</label>
			</td>
		</tr>
	</table>
	<div class="wrapper-btn text-center">
		<input type="button" value="Export" class="exportBtn btn btn-primary" onclick="exportIndirectProcurementCommoditiesReportData('gridtable');">		
	</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
$( document ).ajaxError(function() {
	$("#ajaxloader").hide();
});

$(document).ready(function() {
	$("#vendorStatusList").multiselect({
		selectedText : "# of # selected"
	});
	
	$("#indirectCommoditiesList").multiselect({
		selectedText : "# of # selected"
	});
	
	$("#certificateTypeList").multiselect({
		selectedText : "# of # selected"
	});
});

function saveIndirectProcurementCommodityList()
{
	var indirectCommoditiesList = [];
    $.each($("#indirectCommoditiesList option:selected"), function()
	{            
    	indirectCommoditiesList.push($(this).val());
    });
        
    if(indirectCommoditiesList.length >= 0)
   	{
    	$.ajax(
		{
			url : "indirectProcurementCommoditiesReport.do?method=saveIndirectProcurementCommodityList&random=" + Math.random(),
			type : "POST",
			data : $("#reportform").serialize(),
			async : true,
			beforeSend : function()
			{
				$("#ajaxloader").show();
			},
			success : function(data) 
			{
				$("#ajaxloader").hide();
				if(data.result == "Success")
					alert("BP Market Sector Search List Saved Successfully.");
				else if(data.result == "No Data Found")
					alert("Please Select Market Sector to Save \"BP Market Sector Search List\".");
				else
					alert("BP Market Sector Search List Not Saved. Please try again.");
			}
		});	
   	}    
    else
   	{
    	alert("Please Select Market Sector to Save \"BP Market Sector Search List\".");
   	}
}

function getIndirectProcurementCommoditiesReport()
{
	$.ajax(
	{
		url : "indirectProcurementCommoditiesReport.do?method=getIndirectProcurementCommoditiesReport&random=" + Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function()
		{
			$("#ajaxloader").show();
		},
		success : function(data) 
		{
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			gridIndirectProcurementCommoditiesReportData(data);
		}
	});	
}

function gridIndirectProcurementCommoditiesReportData(myGridData)
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{ 
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Vendor Name', 'Vendor Status', 'Business Type', 'Annual Revenue', 'BP Market Sector', 'BP Market SubSector',
		             'Diverse Classification', 'State', 'Contact First Name', 'Contact Last Name', 'Contact Phone', 'Contact Email', 'Website' ],
		colModel : [ 
					{
						name : 'vendorName',
						index : 'vendorName',
						sorttype: 'string'
					}, {
		            	name : 'vendorStatus',
		            	index : 'vendorStatus',
		            	sorttype: 'string'
					}, {
		            	name : 'businessTypeName',
		            	index : 'businessTypeName',
		            	sorttype: 'string'
					}, {
		            	name : 'recentAnnaulRevenue',
		            	index : 'recentAnnaulRevenue',
		            	sorttype: 'number',
		            	formatter : 'currency',
						formatoptions : {
							prefix : '$',
							thousandsSeparator : ',',
							decimalPlaces: 0
						}
		            }, {
		            	name : 'bpMarketSector',
		            	index : 'bpMarketSector',
		            	sorttype: 'string'
		            }, {
		            	name : 'bpMarketSubSector',
		            	index : 'bpMarketSubSector',
		            	sorttype: 'string'
		            }, {
		            	name : 'diverseClassification',
		            	index : 'diverseClassification',
		            	sorttype: 'string'
		            }, {
		            	name : 'state',
		            	index : 'state',
		            	sorttype: 'string'
		            }, {
		            	name : 'contactFirstName',
		            	index : 'contactFirstName',
		            	sorttype: 'string'
		            }, {
		            	name : 'contactLastName',
		            	index : 'contactLastName',
		            	sorttype: 'string'
		            }, {
		            	name : 'contactPhone',
		            	index : 'contactPhone',
		            	sorttype: 'number'
		            }, {
		            	name : 'contactEmail',
		            	index : 'contactEmail',
		            	sorttype: 'string'
		            }, {
		            	name : 'website',
		            	index : 'website',
		            	sorttype: 'string'
		            }],
		rowNum : 5000,
		rowList : [ 5000, 10000, 15000 ],
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,		
		gridview: true,
        pgbuttons:true,
        width: ($.browser.webkit?466:497),
        loadonce: true
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/**
 * Function to Export the BP Market Sector Search Data.
 */
function exportIndirectProcurementCommoditiesReportData(table) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++)
	{
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "vendorStatus") {
			index.push(1);
			valueForCsv.push("Vendor Status");
		} else if (cols[i] == "businessTypeName") {
			index.push(2);
			valueForCsv.push("Business Type");
		} else if (cols[i] == "recentAnnaulRevenue") {
			index.push(3);
			valueForCsv.push("Annual Revenue");
		} else if (cols[i] == "bpMarketSector") {
			index.push(4);
			valueForCsv.push("BP Market Sector");
		} else if (cols[i] == "bpMarketSubSector") {
			index.push(5);
			valueForCsv.push("BP Market SubSector ");
		} else if (cols[i] == "diverseClassification") {
			index.push(6);
			valueForCsv.push("Diverse Classification");
		} else if (cols[i] == "state") {
			index.push(7);
			valueForCsv.push("State");
		} else if (cols[i] == "contactFirstName") {
			index.push(8);
			valueForCsv.push("Contact First Name");
		} else if (cols[i] == "contactLastName") {
			index.push(9);
			valueForCsv.push("Contact Last Name");
		} else if (cols[i] == "contactPhone") {
			index.push(10);
			valueForCsv.push("Contact Phone");
		} else if (cols[i] == "contactEmail") {
			index.push(11);
			valueForCsv.push("Contact Email");
		} else if (cols[i] == "website") {
			index.push(12);
			valueForCsv.push("Website");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'BPMarketSectorSearchReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "indirectProcurementCommoditiesReport.do?method=getIndirectProcurementCommoditiesReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}
</script>