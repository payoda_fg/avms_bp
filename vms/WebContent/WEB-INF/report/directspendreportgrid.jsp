<%@page import="java.sql.ResultSet"%>
<%@page
	import="java.util.*,com.fg.vms.customer.dto.*,java.util.ArrayList"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="net.sf.json.JSONArray"%>

<%
	JSONArray cellarray = new JSONArray();
	JSONObject cellobj = null; //new JSONObject();

	List<Tier2ReportDto> reportDtos = null;
	if (session.getAttribute("directSpend") != null) {
		reportDtos = (List<Tier2ReportDto>) session
				.getAttribute("directSpend");
	}

	Tier2ReportDto tier2ReportDto = null;

	if (reportDtos != null) {
		for (int index = 0; index < reportDtos.size(); index++) {
			tier2ReportDto = reportDtos.get(index);
			cellobj = new JSONObject();
			cellobj.put("vendorName", tier2ReportDto.getVendorName());
			cellobj.put("vendorUserName",
					tier2ReportDto.getVendorUserName());
			cellobj.put("spendYear", tier2ReportDto.getSpendYear());
			cellobj.put("reportPeriod",
					tier2ReportDto.getReportPeriod());
			cellobj.put("certificateName",
					tier2ReportDto.getCertificateName());
			if(tier2ReportDto.getIndirectExp() != null){
			cellobj.put("indirectExp", Math.round(tier2ReportDto.getIndirectExp()));
			}else{
				cellobj.put("indirectExp", tier2ReportDto.getIndirectExp());
			}
			if(tier2ReportDto.getDirectExp() != null){
			cellobj.put("directExp", Math.round(tier2ReportDto.getDirectExp()));
			}else{
				cellobj.put("directExp", tier2ReportDto.getDirectExp());
			}
			if(tier2ReportDto.getTotalSales() != null){
			cellobj.put("totalSales", Math.round(tier2ReportDto.getTotalSales()));
			}else{
				cellobj.put("totalSales", tier2ReportDto.getTotalSales());
			}
			cellobj.put("createdOn", tier2ReportDto.getCreatedOn());
			cellarray.add(cellobj);
		}
		out.println(cellarray);
	}
%>
