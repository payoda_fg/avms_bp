
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#formId").validate({
			rules : {
				roleName : {
					required : true
				}
			}
		});

	});

	function backToUserrole() {
		window.location = "viewfgrole.do?parameter=viewUserRoles";
	}
</script>


<div id="successMsg">
	<span style="color: red;"><html:errors property="referencerole" />
	</span>
	<html:messages id="msg" property="userrole" message="true">
		<span><bean:write name="msg" /></span>
	</html:messages>
</div>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Add User Roles
</div>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="add">

			<div class="form-box">

				<html:form action="/fguserrole.do?parameter=addUserRole"
					styleId="formId">
					<html:javascript formName="userRoleForm" />

					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Role Name</div>
							<div class="ctrl-col-wrapper">
								<html:text property="roleName" alt="" styleClass="text-box"
									styleId="roleName" onchange="ajaxFn(this,'R');" />
							</div>
							<span class="error"><html:errors property="roleName" /></span>
						</div>
					</div>
					<div class="wrapper-btn">
						<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
						<html:reset value="Cancel" styleClass="btn"
							onclick="backToUserrole();"></html:reset>
					</div>

				</html:form>

			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="add">
			<div style="text-align: center;">
				<h3>You have no right to add new role</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>

<div class="page-title">
	<img src="images/arrow-down.png" alt="" />User Roles
</div>

<logic:iterate id="privilege" name="privileges">

	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">

			<div class="form-box">
				<table class="main-table" id="userroleTable">

					<bean:size id="size" name="userRoles" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="header">Role Name</td>
								<td class="header">Active</td>
								<td class="header">Action</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="userRoles" id="userRole">
								<tr>
									<td><bean:write name="userRole" property="roleName" /></td>


									<td align="center"><html:checkbox property="isActive"
											name="userRole" disabled="true" value="isActive">
										</html:checkbox></td>
									<bean:define id="id" name="userRole" property="id"></bean:define>

									<td align="center"><logic:iterate id="privilege"
											name="privileges">

											<logic:match value="Roles" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="modify">
													<html:link property="text" paramName="id" paramId="id"
														action="/viewfgrole.do?parameter=retrive">Edit </html:link>
												</logic:match>
											</logic:match>

										</logic:iterate> <logic:iterate id="privilege" name="privileges">

											<logic:match value="Roles" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="delete">
															| <html:link action="/deletefgrole?parameter=delete"
														paramId="id" paramName="id"
														onclick="return confirm_delete();">Delete</html:link>

												</logic:match>
											</logic:match>

										</logic:iterate></td>
								</tr>
							</logic:iterate>
						</tbody>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>

			</div>
		</logic:match>
	</logic:match>

</logic:iterate>

<logic:iterate id="privilege" name="privileges">

	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view roles</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>

<!-- 		<div id="sample1" align="center" style="padding: 2% 0 0 30%;"></div> -->