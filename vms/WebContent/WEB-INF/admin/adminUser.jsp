<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<%
	if (application.getAttribute("applicationscope") != null) {
		out.println(application.getAttribute("applicationscope"));

	}
%>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>

<script>
	jQuery.validator.addMethod("password",function(value, element) {
		return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
	},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	$(document).ready(function() {
		$("#formID").validate({
			rules : {
				userName : {
					required : true,
					minlength : 4
				},
				
				userPassword : {
					required : true,
					password:true
				},
				confirmPassword : {
					required : true,
					equalTo : "#userPassword"
				},
				userEmailId : {
					required : true,
					email : true
				},
				roleId : {
					required : true
				}
			}
		});

	});

	// Function to reset the form
	function clearfields() {
		window.location = "viewfguser.do?parameter=viewUsers";
	}
</script>

<bean:define id="currentUserId" name="userId"></bean:define>
<section role="main" class="content-body card-margin">

<div class="row">
		<div class="col-lg-9 mx-auto">
		<div id="successMsg" >
			<html:messages id="msg" property="user" message="true">			
				<div class="alert alert-info nomargin"><bean:write name="msg" />
				</div>
			</html:messages>
			<html:messages id="msg" property="deleteUser" message="true">
			<div class="alert alert-info nomargin">
				<bean:write name="msg" />
				</div>
			</html:messages>
			<html:messages id="msg" property="updateUser" message="true">
			<div class="alert alert-info nomargin">
				<bean:write name="msg" />
				</div>
			</html:messages>
		</div>
<div class="page-title">
	<img src="images/userRegistration.png" alt="" />User Registration
</div>

<div class="form-box">

	<html:form action="/createFGUser?parameter=create" styleId="formID">
		<html:javascript formName="userForm" />
		<logic:iterate id="privilege" name="privileges">
			<logic:match value="User" name="privilege"
				property="objectId.objectName">
				<logic:match value="1" name="privilege" property="add">

					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Name</div>
							<div class="ctrl-col-wrapper">
								<html:text property="userName" name="userForm" alt=""
									styleId="userName" styleClass="text-box" />
							</div>
							<span class="error"><html:errors property="userName"></html:errors></span>
						</div>
						
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Password</div>
							<div class="ctrl-col-wrapper">
								<html:password property="userPassword" name="userForm"
									styleId="userPassword" alt="" styleClass="text-box" />
							</div>
							<span class="error"><html:errors property="userPassword"></html:errors></span>
						</div>

						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Confirm Password</div>
							<div class="ctrl-col-wrapper">
								<html:password property="confirmPassword" name="userForm"
									styleId="confirmPassword" styleClass="text-box" />
							</div>
							<span class="error"><html:errors
									property="confirmPassword"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User EmailId</div>
							<div class="ctrl-col-wrapper">
								<html:text property="userEmailId" name="userForm" alt=""
									styleClass="text-box" styleId="userEmailId"
									onchange="ajaxFn(this,'E');" />
							</div>
							<span class="error"><html:errors property="userEmailId"></html:errors></span>
						</div>

						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Role</div>
							<div class="ctrl-col-wrapper">
								<html:select property="roleId" name="userForm"
									styleId="userForm" styleClass="list-box">
									<html:option value="">-- Select --</html:option>
									<bean:size id="size" name="userRoles" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="role" name="userRoles">
											<bean:define id="id" name="role" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="role" property="roleName"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
							<span class="error"><html:errors property="roleId"></html:errors></span>
						</div>
					</div>

					<div class="clear"></div>
					<div class="wrapper-btn">
						<logic:iterate id="privilege" name="privileges">
							<logic:match value="User" name="privilege"
								property="objectId.objectName">
								<logic:match value="1" name="privilege" property="add">
									<html:submit value="Submit" styleClass="btn" styleId="submit"></html:submit>
									<html:reset value="Clear" styleClass="btn"
										onclick="clearfields();"></html:reset>
								</logic:match>
							</logic:match>
						</logic:iterate>
					</div>

				</logic:match>
			</logic:match>
		</logic:iterate>
	</html:form>
</div>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="add">
			<div style="text-align: center;">
				<h3>You have no rights to add new user</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
<div class="page-title">
	<img src="images/user-group.gif" alt="" />Registered Users
</div>
<logic:iterate id="privilege" name="privileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">

			<div class="form-box">
				<table class="main-table" id="userTable">
					<bean:size id="size" name="users" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="header">User Name</td>
								<td class="header">User EmailId</td>
								<td class="header">User Role</td>
								<td class="header">Actions</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="users" id="userlist">
								<tr>
									<td><bean:write name="userlist" property="userName" /></td>
									<td><bean:write name="userlist" property="userEmailId" /></td>
									<td><bean:write name="userlist"
											property="userRoleId.roleName" /></td>
									<bean:define id="userId" name="userlist" property="id"></bean:define>
									<td><logic:iterate id="privilege" name="privileges">

											<logic:match value="User" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="modify">
													<html:link action="/viewfguser.do?parameter=retrive"
														paramId="id" paramName="userId">Edit</html:link>
												</logic:match>
											</logic:match>

										</logic:iterate>|<logic:iterate id="privilege" name="privileges">

											<logic:match value="User" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="delete">
													<logic:notEqual
														value="<%=currentUserId
														.toString()%>"
														name="userlist" property="id">
														<html:link action="/viewfguser.do?parameter=delete"
															paramId="id" paramName="userId"
															onclick="return confirm_delete();">Delete</html:link>
													</logic:notEqual>
													<logic:equal
														value="<%=currentUserId
														.toString()%>"
														name="userlist" property="id">
														<span style="color: red; font-weight: bold;">Current
															User</span>
													</logic:equal>
												</logic:match>

											</logic:match>

										</logic:iterate></td>
								</tr>
							</logic:iterate>
						</tbody>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view users</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
</div>
</div>
</section>

