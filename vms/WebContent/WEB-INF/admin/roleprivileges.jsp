
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-layout.tld" prefix="layout"%>

<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>

<script type="text/javascript">
	function viewPriviliges() {
		var roleId = document.getElementById('roleName').value;

		window.location = "fgroleprivileges.do?method=viewPrivilegesByRole&roleId="
				+ roleId;
	}

	$(function() {

		var roleName = $('#roleName').val();

		/*  add multiple select / deselect functionality */
		if (roleName != 0) {

			$("#selectall").click(function() {

				var isChecked = $('.main-table input:checkbox').is(':checked');

				if (!isChecked) {//Check whether any of the checkboxes is checked or not
					$('.main-table input:checkbox').attr('checked', 'checked');
					$('#selectall').val('UnSelectAll');
				} else {
					$('.main-table input:checkbox').attr('checked', false);
					$('#selectall').val('SelectAll');
				}
			});
		}

	});

	/* Function to check the checkbox status and update button value on page load */
	$(document).ready(function() {

		var isChecked = $('.main-table input:checkbox').is(':checked');

		if (!isChecked) {//Check whether any of the checkboxes is checked or not
			$('#selectall').val('SelectAll');
		} else {
			$('#selectall').val('UnSelectAll');
		}
	});
</script>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />Available Role Privileges
</div>
<div class="form-box">
	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Role Privileges" name="privilege"
			property="objectId.objectName">
			<logic:match value="1" name="privilege" property="view">
				<html:form
					action="/fgroleprivileges.do?method=persistPrivilegesByRole"
					styleClass="AVMS">

					<bean:define id="role1" name="roleName"></bean:define>

					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Role Name</div>
							<div class="ctrl-col-wrapper">
								<html:select styleId="roleName" property="roleName"
									onchange="viewPriviliges()" value="<%=role1.toString()%>"
									styleClass="list-box">
									<bean:size id="size" name="userRoles" />
									<logic:greaterEqual value="0" name="size">
										<html:option value="0">-- Select --</html:option>
										<logic:iterate id="role" name="userRoles">
											<bean:define id="id" name="role" property="id"></bean:define>
											<bean:define id="roleName" name="role" property="roleName"></bean:define>
											<html:option value="<%=id.toString()%>">
												<%=roleName.toString()%>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
						</div>
						
						<div class="row-wrapper form-group row">
							<div class="ctrl-col-wrapper">
								<input type="button" value="SelectAll" class="btn"
									id="selectall" />
							</div>
						</div>
					</div>

					<div class="row-wrapper" style="width: 100%;" align="center">

						<layout:datagrid property="datagrid" model="datagrid"
							styleClass="headerRole">
							<layout:datagridText property="objectName" title="Object Name" />
							<layout:datagridCheckbox property="add" title="Add" />
							<layout:datagridCheckbox property="delete" title="Delete" />
							<layout:datagridCheckbox property="modify" title="Modify" />
							<layout:datagridCheckbox property="view" title="View" />
							<layout:datagridCheckbox property="visible" title="Visible" />

						</layout:datagrid>
					</div>

					<div class="wrapper-btn text-center">
						<logic:iterate id="privilege" name="privileges">
							<logic:match value="Role Privileges" name="privilege"
								property="objectId.objectName">
								<logic:match value="1" name="privilege" property="add">

									<layout:submit value="Submit" styleClass="btTxt submit"
										styleId="submit"></layout:submit>
								</logic:match>
							</logic:match>
						</logic:iterate>
					</div>


				</html:form>
			</logic:match>
		</logic:match>
	</logic:iterate>

	<logic:iterate id="privilege" name="privileges">
		<logic:match value="Role Privileges" name="privilege"
			property="objectId.objectName">
			<logic:match value="0" name="privilege" property="view">
				<div style="text-align: center;">
					<h3>You have no rights to view role privileges</h3>
				</div>
			</logic:match>
		</logic:match>

	</logic:iterate>
</div>
<script>
	$('#datagridJsId').addClass('main-table');
</script>
