
<%@page import="java.util.List"%>
<%@page import="com.fg.vms.admin.model.RolePrivileges"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page import="com.fg.vms.admin.model.Users"%>
<!--Menupart -->
<script type="text/javascript">
<!--
	function linkPage(link) {
		$(".submenu").css("display", "none");
		window.location = link;
	}
//-->
</script>
<div class="inner-menu">
	<div class="item-box-one">
		<div class="item-box-three">
			<div class="box-width">
				<div class="box-title">Administration</div>
				<div class="btn-bg">
					<logic:present name="privileges">
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Roles" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu1" class="icon-12 "
										onclick="linkPage('viewfgrole.do?parameter=viewUserRoles')">Manage
										Roles</a>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="User" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu2" class="icon-13"
										onclick="linkPage('viewfguser.do?parameter=viewUsers')">Manage
										Users</a>

								</logic:equal>
							</logic:equal>
						</logic:iterate>

						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Role Privileges" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu3" class="icon-17"
										onclick="linkPage('fgroleprivileges.do?method=rolePrivileges')">Role
										Privileges</a>
								</logic:equal>
							</logic:equal>
						</logic:iterate>
						<logic:iterate id="privilege" name="privileges">
							<logic:equal value="Customer" name="privilege"
								property="objectId.objectName">
								<logic:equal value="1" name="privilege" property="visible">
									<a href="#menu4" class="icon-14"
										onclick="linkPage('displayCustomer.do?method=display')">Manage
										Customer<br>Data
									</a>
								</logic:equal>
							</logic:equal>
						</logic:iterate>

					</logic:present>

				</div>
			</div>
			<div class="clear"></div>
		</div>

		<script type="text/javascript">
			var pathname = window.location.href;

			if (pathname.indexOf("viewfgrole") >= 0
					|| pathname.indexOf("editfgrole") >= 0) {
				$('.icon-12').addClass("active");
			}
			if (pathname.indexOf("fguserrole") >= 0) {
				$('.icon-12').addClass("active");
			}
			if (pathname.indexOf("fguser") >= 0) {
				$('.icon-13').addClass("active");
			}
			if (pathname.indexOf("privileges") >= 0) {
				$('.icon-17').addClass("active");
			}
			if (pathname.indexOf("displayCustomer") >= 0) {
				$('.icon-14').addClass("active");
			}
			if (pathname.indexOf("custConfiguration") >= 0) {
				$('.icon-14').addClass("active");
			}
			if (pathname.indexOf("retriveCustConfiguration") >= 0) {
				$('.icon-14').addClass("active");
			}
		</script>

	</div>
</div>
<div class="inner-menu-list">

	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Roles" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/viewfgrole.do?parameter=viewUserRoles">
					<img src="images/new-icon/icon-12.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="User" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/viewfguser.do?parameter=viewUsers">
					<img src="images/new-icon/icon-13.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>

	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Role Privileges" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/fgroleprivileges.do?method=rolePrivileges">
					<img src="images/new-icon/icon-13.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
	<logic:iterate id="privilege" name="privileges">
		<logic:equal value="Customer" name="privilege"
			property="objectId.objectName">
			<logic:equal value="1" name="privilege" property="visible">
				<html:link action="/displayCustomer.do?method=display">
					<img src="images/new-icon/icon-14.png" alt="" />
				</html:link>
			</logic:equal>
		</logic:equal>
	</logic:iterate>
</div>
