<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>

<script>
jQuery.validator.addMethod("password",function(value, element) {
	return this.optional(element)|| (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}$/).test(value);
},	"Your password must contain at least 8 characters and at least 1 number, 1 uppercase letter and 1 lowercase letter.");
	$(document).ready(function() {
		$("#formID").validate({
			rules : {
				userName : {
					required : true,
					minlength : 4
				},
				userLogin : {
					required : true,
					rangelength : [ 4, 15 ]
				},
				userPassword : {
					required : true,
					password:true
				},
				confirmPassword : {
					required : true,
					equalTo : "#userPassword"
				},
				userEmailId : {
					required : true,
					email : true
				},
				roleId : {
					required : true
				}
			}
		});

		$('#userTable').tableScroll({
			height : 150
		});
	});

	// Function to reset the form
	function clearfields() {
		window.location = "viewfguser.do?parameter=viewUsers";
	}
</script>
<bean:define id="currentUserId" name="userId"></bean:define>

<div id="successMsg">
	<html:messages id="msg" property="user" message="true">
		<bean:write name="msg" />
	</html:messages>
</div>
<div class="page-title">
	<img src="images/edit_user.png" alt="" />Edit User
</div>

<logic:iterate id="privilege" name="privileges">
	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="modify">

			<div class="form-box">

				<html:form action="/updatefguser?parameter=update" styleId="formID">
					<html:javascript formName="userForm" />
					<html:hidden property="id" name="userForm" styleId="selectedUserId" />


					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Name</div>
							<div class="ctrl-col-wrapper">
								<html:text property="userName" name="userForm" alt=""
									styleId="userName" styleClass="text-box" />
							</div>
							<span class="usererror"><html:errors property="userName"></html:errors></span>
						</div>

						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Login</div>
							<div class="ctrl-col-wrapper">
								<html:text property="userLogin" name="userForm" alt=""
									styleId="userLogin" styleClass="text-box"
									onchange="ajaxEdFn(this,'hiddenUserLogin','U');" />
							</div>
							<span class="usererror"><html:errors property="userLogin"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Password</div>
							<div class="ctrl-col-wrapper">
								<html:password property="userPassword" name="userForm"
									styleId="userPassword" styleClass="text-box" />
							</div>
							<span class="usererror"><html:errors
									property="userPassword"></html:errors></span>
						</div>

						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Confirm Password</div>
							<div class="ctrl-col-wrapper">
								<html:password property="confirmPassword" name="userForm"
									styleId="confirmPassword" styleClass="text-box" />
							</div>
							<span class="usererror"><html:errors
									property="confirmPassword"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User EmailId</div>
							<div class="ctrl-col-wrapper">
								<html:hidden property="hiddenEmailId" name="userForm" alt=""
									styleId="hiddenEmailId" />
								<html:text property="userEmailId" name="userForm" alt=""
									styleId="userEmailId" styleClass="text-box"
									onchange="ajaxEdFn(this,'hiddenEmailId','E');" />
							</div>
							<span class="usererror"><html:errors
									property="userEmailId"></html:errors></span>
						</div>

						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">User Role</div>
							<div class="ctrl-col-wrapper">
								<html:select property="roleId" name="userForm"
									styleId="userForm" styleClass="list-box">
									<html:option value="">----Select---</html:option>
									<bean:size id="size" name="userRoles" />
									<logic:greaterEqual value="0" name="size">
										<logic:iterate id="role" name="userRoles">
											<bean:define id="id" name="role" property="id"></bean:define>
											<html:option value="<%=id.toString()%>">
												<bean:write name="role" property="roleName"></bean:write>
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select>
							</div>
							<span class="usererror"><html:errors property="roleId"></html:errors></span>
						</div>
					</div>
					<div class="wrapper-half">
						<div class="row-wrapper form-group row">
							<div class="label-col-wrapper">Status</div>
							<div class="ctrl-col-wrapper">
								Active
								<html:radio property="active" value="1" name="userForm"></html:radio>
								InActive
								<html:radio property="active" value="0" name="userForm"></html:radio>
							</div>
						</div>

					</div>
					<div class="clear"></div>
					<div class="wrapper-btn">
						<logic:iterate id="privilege" name="privileges">
							<logic:match value="User" name="privilege"
								property="objectId.objectName">
								<logic:match value="1" name="privilege" property="add">
									<html:submit value="Update" styleClass="btn" styleId="submit"></html:submit>
									<html:reset value="Cancel" styleClass="btn"
										onclick="clearfields();"></html:reset>
								</logic:match>
							</logic:match>
						</logic:iterate>
					</div>
				</html:form>
			</div>

			<div class="page-title">
				<img src="images/user-group.gif" alt="" />Registered Users
			</div>

			<logic:iterate id="privilege" name="privileges">

				<logic:match value="User" name="privilege"
					property="objectId.objectName">
					<logic:match value="1" name="privilege" property="view">

						<div class="form-box">
							<table class="main-table" id="userTable">
								<bean:size id="size" name="users" />
								<logic:greaterThan value="0" name="size">
									<thead>
										<tr>
											<td class="header">User Name</td>
											<td class="header">User Login</td>
											<td class="header">User EmailId</td>
											<td class="header">User Role</td>
											<td class="header">Actions</td>
										</tr>
									</thead>
									<tbody>
										<logic:iterate name="users" id="userlist">
											<tr>
												<td><bean:write name="userlist" property="userName" /></td>
												<td><bean:write name="userlist" property="userLogin" /></td>
												<td><bean:write name="userlist" property="userEmailId" /></td>
												<td><bean:write name="userlist"
														property="userRoleId.roleName" /></td>
												<bean:define id="userId" name="userlist" property="id"></bean:define>
												<td><logic:iterate id="privilege" name="privileges">

														<logic:match value="User" name="privilege"
															property="objectId.objectName">
															<logic:match value="1" name="privilege" property="modify">

																<html:link action="/viewfguser.do?parameter=retrive"
																	paramId="id" paramName="userId">Edit</html:link>

															</logic:match>
														</logic:match>

													</logic:iterate>|<logic:iterate id="privilege" name="privileges">

														<logic:match value="User" name="privilege"
															property="objectId.objectName">
															<logic:match value="1" name="privilege" property="delete">
																<logic:notEqual
																	value="<%=currentUserId
																	.toString()%>"
																	name="userlist" property="id">
																	<logic:notEqual
																		value="<%=userId
																		.toString()%>"
																		property="id" name="userForm">
																		<html:link action="/viewfguser.do?parameter=delete"
																			paramId="id" paramName="userId"
																			onclick="return confirm_delete();">Delete</html:link>
																	</logic:notEqual>
																	<logic:equal
																		value="<%=userId
																		.toString()%>"
																		property="id" name="userForm">
																		<span style="color: green; font-weight: bold;">User
																			in Edit</span>
																	</logic:equal>
																</logic:notEqual>
																<logic:equal
																	value="<%=currentUserId
																	.toString()%>"
																	name="userlist" property="id">
																	<span style="color: red; font-weight: bold;">Current
																		User</span>
																</logic:equal>
															</logic:match>
														</logic:match>

													</logic:iterate></td>
											</tr>
										</logic:iterate>
									</tbody>
								</logic:greaterThan>
								<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
							</table>
						</div>
					</logic:match>
				</logic:match>

			</logic:iterate>

			<logic:iterate id="privilege" name="privileges">

				<logic:match value="User" name="privilege"
					property="objectId.objectName">
					<logic:match value="0" name="privilege" property="view">
						<div style="text-align: center;">
							<h3>You have no rights to view users</h3>
						</div>
					</logic:match>
				</logic:match>

			</logic:iterate>
		</logic:match>
	</logic:match>
</logic:iterate>
<logic:iterate id="privilege" name="privileges">

	<logic:match value="User" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="modify">
			<div style="text-align: center;">
				<h3>You have no rights to edit users</h3>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>
