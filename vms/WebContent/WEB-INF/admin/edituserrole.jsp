<%@page import="com.fg.vms.admin.model.UserRoles"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript" src="ajax/ajaxvalidation.js"></script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script>
	$(document).ready(function() {
		$("#formId").validate({
			rules : {
				roleName : {
					required : true
				}
			}
		});

	});

	function backToUserrole() {
		window.location = "viewfgrole.do?parameter=viewUserRoles";
	}
</script>


<section role="main" class="content-body card-margin">
	<div class="row">
	<div class="col-lg-6 mx-auto">
	<div id="successMsg">
	<span style="color: red;"><html:errors property="referencerole" />
	</span>
	<html:messages id="msg" property="userrole" message="true">
	<div class="alert alert-info nomargin">
		<span><bean:write name="msg" /></span>
		</div>
	</html:messages>
</div>
<div class="form-box">
<html:form action="/editfgrole.do?parameter=update" styleId="formId">
	<html:hidden property="id" name="userRoleForm" />
	<html:javascript formName="userRoleForm" />
	<div class="form-box">
		<header class="card-header">
			<img src="images/arrow-down.png" alt="" />Update User Role
		</header>
		<div class="card-body">
			<div class="form-group row row-wrapper">
				<label class="col-sm-3 control-label text-sm-right label-col-wrapper">Role Name</label>
				<html:hidden property="hiddenRoleName" alt=""
					styleId="hiddenRoleName" name="userRoleForm" />
				<div class="col-sm-9 ctrl-col-wrapper">
					<html:text property="roleName" name="userRoleForm" alt=""
						onchange="ajaxEdFn(this,'hiddenRoleName','R');"
						styleClass="text-box form-control" />
				</div>
				<span class="error"><html:errors property="roleName" /></span>
			</div>
		<footer class="card-footer">
			<div class="row justify-content-end">
				<div class="col-sm-9">
			<logic:iterate id="privilege" name="privileges">

				<logic:match value="Roles" name="privilege"
					property="objectId.objectName">
					<logic:match value="1" name="privilege" property="add">
						<html:submit value="Update" styleClass="btn btn-primary" styleId="submit"></html:submit>
						<html:reset value="Cancel" styleClass="btn btn-default"
							onclick="backToUserrole();"></html:reset>
					</logic:match>
				</logic:match>

			</logic:iterate>
			</div>
			</div>
			</footer>
		</div>
	</div>
	</html:form>
</div>
</div>
</div>
</section>
<div class="page-title">
	<img src="images/arrow-down.png" alt="" />User Roles
</div>
<logic:iterate id="privilege" name="privileges">

	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="1" name="privilege" property="view">

			<div class="form-box">
				<table class="main-table" id="userroleTable">

					<bean:size id="size" name="userRoles" />
					<logic:greaterThan value="0" name="size">
						<thead>
							<tr>
								<td class="header">Role Name</td>
								<td class="header">Active</td>
								<td class="header">Action</td>
							</tr>
						</thead>
						<tbody>
							<logic:iterate name="userRoles" id="userRole">
								<tr>
									<td><bean:write name="userRole" property="roleName" /></td>


									<td align="center"><html:checkbox property="isActive"
											name="userRole" disabled="true" value="isActive">
										</html:checkbox></td>
									<bean:define id="id" name="userRole" property="id"></bean:define>

									<td align="center"><logic:iterate id="privilege"
											name="privileges">

											<logic:match value="Roles" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="modify">
													<html:link property="text" paramName="id" paramId="id"
														action="/viewfgrole.do?parameter=retrive">Edit </html:link>
												</logic:match>
											</logic:match>

										</logic:iterate> <logic:iterate id="privilege" name="privileges">

											<logic:match value="Roles" name="privilege"
												property="objectId.objectName">
												<logic:match value="1" name="privilege" property="delete">
													<logic:notEqual value="<%=id.toString()%>" property="id"
														name="userRoleForm">
																	| <html:link action="/deletefgrole?parameter=delete"
															paramId="id" paramName="id"
															onclick="return confirm_delete();">Delete</html:link>
													</logic:notEqual>
													<logic:equal value="<%=id.toString()%>" property="id"
														name="userRoleForm">
																	| <span style="color: green; font-weight: bold;">User
															Role in Edit</span>
													</logic:equal>
												</logic:match>
											</logic:match>

										</logic:iterate></td>
								</tr>
							</logic:iterate>
						</tbody>
					</logic:greaterThan>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>
			</div>
		</logic:match>
	</logic:match>

</logic:iterate>

<logic:iterate id="privilege" name="privileges">

	<logic:match value="Roles" name="privilege"
		property="objectId.objectName">
		<logic:match value="0" name="privilege" property="view">
			<div style="text-align: center;">
				<h3>You have no rights to view user role</h3>
			</div>
		</logic:match>
	</logic:match>
</logic:iterate>
