<%@page import="java.text.NumberFormat"%>
<%@page import="com.fg.vms.util.Decrypt"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%
	
	Decrypt decrypt = new Decrypt();
	String totalSales = "";
	String totalPrimeSales = "";
	NumberFormat formatter = NumberFormat.getCurrencyInstance();
%>
<script src="js/common/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
<script src="js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxcommon.js"></script>
<script src="js/common/connector.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript"
	src="jquery/js/jqueryUtils.js"></script>
<link rel='STYLESHEET' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/dhtmlxgrid.css'>
<link rel='stylesheet' type='text/css'
	href='js/dhtmlxChart/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css'>
	
<!-- Fixed Table Header -->
<script type="text/javascript">
<!--
    $(document).ready(function() {

	$('#spendTable').tableScroll({
	    height : 150
	});
    });
//-->
</script>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 20px;">
		<h2 id="show">
			<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Spend Data
			Master

		</h2>
		<div id="boxxyz" style="margin-left: 1.5%">
			<table style="width: 96%" id="spendTable">
				<logic:present name="spenduploadform" property="spendDatas">
					<thead>
						<tr>
							<th>Non-Prime Vendor Name</th>
							<th>Date Of Upload</th>
							<th>Data For The Year</th>
							<th>Total Customer Sales</th>
							<th>Total Prime Supplier Sales</th>

						</tr>
					</thead>
					<logic:iterate id="spendData" name="spenduploadform"
						property="spendDatas">
						<tbody>
						<tr>
								<td><logic:iterate id="privilege" name="rolePrivileges">
										<logic:match value="Spend Upload" name="privilege"
											property="objectId.objectName">
											<logic:match value="1" name="privilege" property="modify">
												<bean:define id="spendDataId" name="spendData" property="id"></bean:define>
												<html:link
													action="/viewspenddata?method=retriveSpendDetails"
													paramId="id" paramName="spendDataId">
													<bean:write name="spendData" property="vendor.vendorName"></bean:write>
												</html:link>
											</logic:match>
											<logic:match value="0" name="privilege" property="modify">
											<bean:write name="spendData" property="vendor.vendorName"></bean:write>
											</logic:match>
										</logic:match>
									</logic:iterate></td>

								<td><bean:write name="spendData" property="dateOfUpload"></bean:write></td>
							<td><bean:write name="spendData" property="yearOfData"></bean:write></td>

							<td><logic:present name="spendData"
									property="totalCustomerSales">
									<bean:define id="totalCustomerSales" name="spendData"
										property="totalCustomerSales"></bean:define>
									<logic:present name="spendData" property="keyValue">
										<bean:define id="key" name="spendData" property="keyValue"></bean:define>

										<%
											totalSales = decrypt.decryptText(
																	String.valueOf(key.toString()),
																	totalCustomerSales.toString());
															out.print(formatter.format(Double
																	.parseDouble(totalSales.toString())));
										%>
									</logic:present>
								</logic:present></td>
							<td><logic:present name="spendData"
									property="totalPrimeSupplierSales">
									<bean:define id="totalPrimeSupplierSales" name="spendData"
										property="totalPrimeSupplierSales"></bean:define>
									<logic:present name="spendData" property="keyValue">
										<bean:define id="key" name="spendData" property="keyValue"></bean:define>
										<%
											totalPrimeSales = decrypt.decryptText(
																	String.valueOf(key.toString()),
																	totalPrimeSupplierSales.toString());
															out.print(formatter.format(Double
																	.parseDouble(totalPrimeSales.toString())));
										%>
									</logic:present>
								</logic:present></td>
						</tr>
</tbody>
					</logic:iterate>
				</logic:present>
			</table>
		</div>

		<logic:present name="selectedSpendMasterId">
			<h2 id="show">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Spend
				Details

			</h2>
			<div id="box"
				style="background-color: white; overflow: hidden; height: 250px; width: 800px; margin-left: 5%;"></div>

			<script>
		mygrid = new dhtmlXGridObject('box');
		mygrid.setImagePath("js/common/imgs/");
		//mygrid.setColAlign("center,center,center");
		mygrid
			.setHeader("Spend Type,Diverse Classification,Naics Code,Spend Value,Valid");
		mygrid.setInitWidths("300,200,100,100,*");
		mygrid.setColTypes("ro,edtxt,ro,edn,ro");
		mygrid.setColSorting("str,str,str,str,str,str");

		mygrid.enableMultiselect(true);

		mygrid.init();
		mygrid.setNumberFormat("$0,000.00", 3, ".", ",");
		mygrid.setSkin("light");

		mygrid.customGroupFormat = function(name, count) {
		    return name + "    -  Spend Value ";
		};
		mygrid
			.groupBy(1,
				[ "#title", "", "", "#stat_total", "#cspan" ]);
		//mygrid.groupBy(1);
		mygrid.loadXML("grid_SpendDataDetails.spend");
		var dp = new dataProcessor("grid_SpendDataDetails.spend");
		dp.init(mygrid);
	    </script>
			<logic:iterate id="privilege" name="rolePrivileges">
				<logic:match value="Spend Upload" name="privilege"
					property="objectId.objectName">
					<logic:match value="1" name="privilege" property="delete">
						<input type="button" name="delete" value="Delete"
							style="margin: 1% 0% 0% 80%;" class="customerbtTxt"
							onclick="mygrid.deleteSelectedRows()">
					</logic:match>
				</logic:match>
			</logic:iterate>
			<!--  	<h2 id="show">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Indirect
				Spend Data
			</h2>
			<div id="indirectspend"
				style="background-color: white; overflow: hidden; height: 250px; width: 800px; margin-left: 5%;"></div>
			<script>
				grid = new dhtmlXGridObject('indirectspend');
				grid.setImagePath("js/common/imgs/");
				//grid.setColAlign("center,center,center");
				grid
						.setHeader("Indirect Amount Based On Direct Sales,Indirect Spend Value,Pro Rate Amount Based On Supply");
				grid.setInitWidths("300,200,*");
				grid.setColTypes("ro,ro,ro");
				grid.setColSorting("str,str,str");
				grid.setColAlign("right,right,right");
				grid.enableSmartRendering(true, 10);
				grid.enableMultiselect(true);

				grid.init();
				grid.setSkin("dhx_skyblue");
				grid.loadXML("grid_SpendIndirectDetails.indirectSpend");
				var dp1 = new dataProcessor("grid_SpendIndirectDetails.indirectSpend");
				dp1.init(grid);
			</script>
			<input type="button" name="delete" value="Delete"
				style="margin: 1% 0% 0% 80%;" class="customerbtTxt"
				onclick="grid.deleteSelectedRows()"> -->
		</logic:present>


	</div>
</div>