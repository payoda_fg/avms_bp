<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<%@page import="com.fg.vms.customer.model.VendorContact"%>

<script type="text/javascript">
	function datePicker() {
		$(document).ready(function() {
			$.datepicker.setDefaults({
// 				showOn : 'both',
// 				buttonImageOnly : true,
// 				buttonImage : imgPath,
				changeYear : true,
				dateFormat : 'dd/mm/yy',
				onClose: function(dateText, inst) {
				    $('#spendStartDate').removeClass('text-label');
				    $('#spendEndDate').removeClass('text-label');
				}
			});
			$("#spendStartDate").datepicker();
			$("#spendEndDate").datepicker();
		});
	}
	var imgPath ="jquery/images/calendar.gif";
	datePicker();
</script>
<script type="text/javascript"
	src="jquery/js/jqueryUtils.js"></script>


<script type="text/javascript">
	function calculate() {
		var totalDirectSales = parseInt(document
				.getElementById('totalDirectSales').value);
		var totalPrimeSupplierSales = parseInt(document
				.getElementById('totalPrimeSupplierSales').value);
		var percentageOfDirectSales = (totalDirectSales / totalPrimeSupplierSales) * 100;
		$('#percentageOfDirectSales').val(percentageOfDirectSales.toFixed(2));
	}
</script>


<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 20px;">

		<html:form action="/spendIndirect?method=calculateDirectSales"
			method="post" enctype="multipart/form-data">
			<html:javascript formName="spendIndirectForm" />
			<div class="toggleFilter">
				<h2 id="show" title="Click here to hide/show the form"
					style="cursor: pointer;">
					<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Indirect
					Spend
				</h2>
			</div>
			<div id="table-holder"
				style="margin: 0 1.5%; padding: 10px 0px 10px 40px;">

				<fieldset style="width: 95%">
					<legend>Reporting Period</legend>

					<table cellpadding="10px" cellspacing="10px">

						<tr>
							<td>Reporting Year</td>
							<td><html:text property="reportingYear" alt=""
									styleId="yearData"></html:text> <span class="error"><html:errors
										property="reportingYear" /></span></td>
						</tr>
					</table>


					<table cellpadding="10px" cellspacing="20px">
						<tr>

							<td>From Date</td>
							<td width="200px"><html:text
									property="reportingQuaterFromDate" alt="Please click to select date"
									styleId="spendStartDate"></html:text> <span class="error"><html:errors
										property="reportingQuaterFromDate" /></span></td>
							<td>To Date</td>
							<td width="200px"><html:text
									property="reportingQuaterToDate" alt="Please click to select date"
									styleId="spendEndDate"
									onchange="checkDateSpend('spendStartDate','spendEndDate')"></html:text>
								<span class="error"><html:errors
										property="reportingQuaterToDate" /></span></td>

						</tr>
					</table>
				</fieldset>
				<br>
				<fieldset style="width: 95%">
					<legend>Sales</legend>

					<table cellpadding="10px" cellspacing="12px">

						<tr>
							<td>Total prime Suppliers Sales</td>
							<td><html:text property="totalPrimeSupplierSales"
									styleId="totalPrimeSupplierSales" alt=""></html:text></td>


							<td>Total Direct Sales</td>
							<td><html:text property="totalDirectSales"
									styleId="totalDirectSales" alt="" onblur="calculate()"></html:text></td>


							<td>% of Direct Sales</td>
							<td><html:text property="percentageOfDirectSales"
									styleId="percentageOfDirectSales" alt=""></html:text></td>


							<!--  	<html:link property="text"
									action="/searchvendor2.do?method=calculateDirectSales">Calculate Spend Indirect</html:link> -->
						</tr>


					</table>
				</fieldset>
				<br>
				<html:submit value="Calculate Indirect Spend"
					styleClass="customerbtTxt" styleId="submit"
					style="float:right;margin-right: 42px;"></html:submit>
				<br style="clear: both;" />
			</div>
		</html:form>
		<br>


		<html:form action="/spendIndirectPro?method=saveProRateSupply">
			<div id="box2"
				style="max-height: 150px; min-height: 100px; overflow: auto;">
				<table width="100%">
					<thead>
						<tr>
							<th>Purchase</th>
							<th>Actual Amount</th>
							<th>Pro Rated Amt on the basis of Supply %</th>
							<th>% On Direct Spend</th>
						</tr>
					</thead>

					<logic:present name="resultList">
						<logic:iterate id="result" name="resultList">
							<tbody>
								<tr>

									<td><bean:write name="result"
											property="diverseCertificate" /></td>
									<td><bean:write name="result" property="directSpendValue" /></td>
									<td><bean:write name="result" property="prorated_amount" /></td>
									<td><bean:write name="result"
											property="direct_Spendpercent" /></td>
								</tr>

							</tbody>

						</logic:iterate>
					</logic:present>
					<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
				</table>
			</div>


			<div style="padding: 0 0 2% 40%">
				<html:submit value="Submit" styleClass="customerbtTxt"
					styleId="submit" />
				<html:reset value="Cancel" styleClass="customerbtTxt" />
			</div>
		</html:form>
	</div>

</div>


