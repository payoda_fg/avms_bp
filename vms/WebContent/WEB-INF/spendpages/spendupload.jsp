<%@page import="java.text.NumberFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@page import="com.fg.vms.customer.model.CustomerApplicationSettings"%>
<%@page import="com.fg.vms.customer.model.VendorContact"%>
<%
    NumberFormat formatter = NumberFormat.getCurrencyInstance();
%>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<script type="text/javascript" src="jquery/js/jquery.validate.js"></script>
<script type="text/javascript">
	function calculate() {
		var str_totalDirectSales = document.getElementById('totalDirectSales').value;

		var doubleTotalDirectSales = str_totalDirectSales.replace(/[^0-9.]+/g,
				"");

		var str_totalPrimeSupplierSales = document
				.getElementById('totalPrimeSupplierSales').value;
		var doubleTotalPrimeSupplierSales = str_totalPrimeSupplierSales
				.replace(/[,$]+/g, "");
		if (isNaN(doubleTotalPrimeSupplierSales)) {
			alert("Enter correct value in total direct sales");
			return false;
		}
		var doubleTotalPrimeSupplierSal = str_totalPrimeSupplierSales.replace(
				/[^0-9.]+/g, "");
		var percentageOfDirectSales = (doubleTotalDirectSales / doubleTotalPrimeSupplierSal) * 100;
		$('#percentageOfDirectSales').val(percentageOfDirectSales.toFixed(2));
		return true;
	}

	function validateAmt() {
		var str_doubleTotalPrimeSupplierSales = document
				.getElementById('totalPrimeSupplierSales').value;
		var doubleTotalPrimeSupplierSales = str_doubleTotalPrimeSupplierSales
				.replace(/[,$]+/g, "");
		if (isNaN(doubleTotalPrimeSupplierSales)) {
			alert("Enter correct value in total prime suppliers sales");
			return false;
		}
		return true;
	}

	var imgPath = "jquery/images/calendar.gif";
	spendDatePicker();
</script>
<script>
	$(document).ready(function() {
		/* Validation for Form. */

		$("#spendForm").validate({
			rules : {
				vendor : {
					required : true
				},
				yearOfData : {
					required : true
				},
				spendStartDate : {
					required : true
				},
				spendEndDate : {
					required : true
				},
				file : {
					required : true,
					accept : "csv|xls|xlsx"
				}
			}
		});
		$('#spendTable').tableScroll({
			height : 200
		});

		$('#spendIndirectTable').tableScroll({
			height : 200
		});
	});
</script>
<script type="text/javascript" src="jquery/js/jqueryUtils.js"></script>
<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in"
		style="height: 95%; width: 100%; padding-bottom: 20px;">
		<logic:present name="rolePrivileges">
			<logic:iterate id="privilege" name="rolePrivileges">
				<logic:equal value="Spend Upload" name="privilege"
					property="objectId.objectName">
					<logic:equal value="1" name="privilege" property="add">
						<html:form action="/spendupload?method=uploadFile" method="post"
							enctype="multipart/form-data" styleId="spendForm"
							onsubmit="return checkDateSpend('spendStartDate','spendEndDate');">
							<div class="toggleFilter">
								<h2 id="show" title="Click here to hide/show the form"
									style="cursor: pointer;">
									<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Direct
									Spend Upload
								</h2>
								<div id="table-holder"
									style="margin: 0 1.5%; padding: 10px 0px 10px 100px;">
									<table cellpadding="10px" cellspacing="10px">
										<tr>
											<td><p>
													<label for="vendor">Select Vendor</label>
													<html:select property="vendor"
														style="width:120px;font-size:11px;" styleId="vendor">
														<html:option value="">-----Select-----</html:option>
														<logic:present name="spenduploadform" property="vendors">
															<logic:iterate id="vendor" name="spenduploadform"
																property="vendors">
																<bean:define id="id" name="vendor" property="id"></bean:define>
																<html:option value="<%=id.toString() %>">
																	<bean:write name="vendor" property="vendorName"></bean:write>
																</html:option>
															</logic:iterate>
														</logic:present>
													</html:select>
												</p></td>
											<td><p>
													<label for="yearOfData">Year Of Data</label>
													<html:text property="yearOfData" alt=""
														styleId="yearOfData"
														onblur="return currentYearOfDataValidation();"></html:text>
												</p></td>
										</tr>
										<tr id="showDates">
											<td><p>
													<label for="spendStartDate">From Date</label>
													<html:text property="spendStartDate" alt="Please click to select date"
														styleId="spendStartDate"></html:text>
												</p></td>
											<td><p>
													<label for="spendEndDate">To Date</label>
													<html:text property="spendEndDate" alt="Please click to select date"
														styleId="spendEndDate"
														onchange="checkDateSpend('spendStartDate','spendEndDate')"></html:text>
												</p></td>
										</tr>

										<tr>
											<td><p>
													<label for="file">Import Data From Excel/CSV</label>
													<html:file property="file" styleId="file"></html:file>
												</p></td>
										</tr>
									</table>


									<div style="padding: 0 0 2% 40%">
										<html:submit value="Upload" styleClass="customerbtTxt"
											styleId="submit" />
										<html:reset value="Clear" styleClass="customerbtTxt" />
									</div>

								</div>
							</div>
						</html:form>


						<div id="box" style="margin-left: 1.5%;">
							<table width="96%" id="spendTable">
								<logic:present name="spenduploadform"
									property="spendDataDTOList">
									<thead>
										<tr>
											<th>Category</th>
											<th>Spend Date</th>
											<th>NAICS Code</th>
											<th>Spend Value</th>
											<th>Currency</th>
											<th>Diverse Classification Code</th>
											<th>Valid</th>
										</tr>
									</thead>
									<tbody>
										<logic:iterate id="spendDataDTOList1" name="spenduploadform"
											property="spendDataDTOList">

											<tr>
												<td><bean:write name="spendDataDTOList1"
														property="category" /></td>
												<td><bean:write name="spendDataDTOList1"
														property="spenDate" /></td>
												<td><bean:write name="spendDataDTOList1"
														property="naicsCode" /></td>
												<td><logic:present name="spendDataDTOList1"
														property="spentValue">
														<bean:define id="spendAmt" name="spendDataDTOList1"
															property="spentValue" />

														<%
														    out.print(formatter.format(Double
																										.parseDouble(spendAmt
																												.toString())));
														%>
													</logic:present></td>
												<td><bean:write name="spendDataDTOList1"
														property="currency" /></td>
												<td><bean:write name="spendDataDTOList1"
														property="diverseCerificateCode" /></td>
												<td><logic:equal value="1" name="spendDataDTOList1"
														property="isValid">
										Yes
									</logic:equal> <logic:equal value="0" name="spendDataDTOList1"
														property="isValid">
										No
									</logic:equal></td>
											</tr>
										</logic:iterate>
									</tbody>
								</logic:present>
							</table>
						</div>


						<div class="toggleFilter">
							<h2 id="show2" title="Click here to hide/show the form"
								style="cursor: pointer;">
								<img id="showx" src="images/arrow-down.png" />&nbsp;&nbsp;Indirect
								Spend
							</h2>
						</div>
						<div id="table-holder2"
							style="margin: 0 1.5%; padding: 10px 0px 10px 100px;">

							<html:form action="/spendIndirect?method=calculateDirectSales"
								method="post" enctype="multipart/form-data">
								<html:javascript formName="spendIndirectForm" />

								<html:hidden property="yearOfData" alt="" styleId="yearData"></html:hidden>

								<html:hidden property="spendStartDate" alt=""
									styleId="spendStartDate"></html:hidden>

								<html:hidden property="spendEndDate" alt=""
									styleId="spendEndDate"
									onchange="checkDateSpend('spendStartDate','spendEndDate')"></html:hidden>


								<fieldset style="width: 95%">
									<legend>Sales</legend>

									<table cellpadding="10px" cellspacing="12px">

										<tr>
											<td>Total prime Suppliers Sales</td>
											<td><html:text property="totalPrimeSupplierSales"
													styleId="totalPrimeSupplierSales" alt=""
													onblur="return validateAmt();"></html:text></td>


											<td>Total Direct Sales</td>
											<td><html:text property="totalDirectSales"
													styleId="totalDirectSales" alt=""
													onblur="return calculate();"></html:text></td>


											<td>% of Direct Sales</td>
											<td><html:text property="percentageOfDirectSales"
													styleId="percentageOfDirectSales" alt=""></html:text></td>


											<!--  	<html:link property="text"
									action="/searchvendor2.do?method=calculateDirectSales">Calculate Spend Indirect</html:link> -->
										</tr>


									</table>
								</fieldset>
								<br>
								<html:submit value="Calculate Indirect Spend"
									styleClass="customerbtTxt" styleId="submit"
									style="float:right;margin-right: 42px;"></html:submit>
								<br style="clear: both;" />

							</html:form>
							<br>

							<html:form action="/spendIndirectPro?method=saveProRateSupply">
								<div id="box" style="margin-left: 1.5%;">
									<table width="96%" id="spendIndirectTable">
										<logic:present name="resultList">
											<thead>
												<tr>
													<th>Purchase</th>
													<th>Actual Amount</th>
													<th>Pro Rated Amt on the basis of Supply %</th>
													<th>% On Direct Spend</th>
												</tr>
											</thead>


											<logic:iterate id="result" name="resultList">
												<tbody>
													<tr>

														<td><bean:write name="result"
																property="diverseCertificate" /></td>
														<td><logic:present name="result"
																property="directSpendValue">
																<bean:define id="directSpendAmt" name="result"
																	property="directSpendValue" />
																<%
																    out.print(formatter.format(Double
																													.parseDouble(directSpendAmt
																															.toString())));
																%>
															</logic:present></td>
														<td><logic:present name="result"
																property="prorated_amount">
																<bean:define id="proratedAmt" name="result"
																	property="prorated_amount" />
																<%
																    out.print(formatter.format(Double
																													.parseDouble(proratedAmt
																															.toString())));
																%>
															</logic:present></td>
														<td><logic:present name="result"
																property="direct_Spendpercent">
																<bean:define id="spendPercent" name="result"
																	property="direct_Spendpercent" />
																<%
																    out.print(formatter.format(Double
																													.parseDouble(spendPercent
																															.toString())));
																%>
															</logic:present></td>
													</tr>

												</tbody>

											</logic:iterate>
										</logic:present>
										<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
									</table>
								</div>


								<div style="padding: 0 0 2% 40%">
									<html:submit value="Submit" styleClass="customerbtTxt"
										styleId="submit" />
									<html:reset value="Cancel" styleClass="customerbtTxt" />
								</div>
							</html:form>
						</div>
					</logic:equal>
				</logic:equal>
			</logic:iterate>
			<logic:iterate id="privilege" name="rolePrivileges">
				<logic:match value="Spend Upload" name="privilege"
					property="objectId.objectName">
					<logic:match value="0" name="privilege" property="add">
						<div style="padding: 5%; text-align: center;">
						<h3>You have no rights to add spend data</h3>
					</div>
					</logic:match>
				</logic:match>
			</logic:iterate>
		</logic:present>
	</div>
</div>
