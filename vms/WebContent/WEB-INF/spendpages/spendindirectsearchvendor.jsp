<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.fg.vms.customer.model.VendorMaster"%>



<SCRIPT>
	$(function() {

		// add multiple select / deselect functionality
		$("#selectall").click(function() {
			$('.case').attr('checked', this.checked);
		});

		// if all checkbox are selected, check the selectall checkbox
		// and viceversa
		$(".case").click(function() {

			if ($(".case").length == $(".case:checked").length) {
				$("#selectall").attr("checked", "checked");
			} else {
				$("#selectall").removeAttr("checked");
			}
		});
	});
</SCRIPT>

<script type="text/javascript">
	function reportDownload() {
		window.location = "naicreportdownload.do?method=downloadReport";
	}
</script>
<script type="text/javascript" src="js/rfiinformation.js"></script>
<script type="text/javascript"
	src="jquery/js/jqueryUtils.js"></script>

<div id="form-container" style="height: 100%; width: 95%;">
	<div id="form-container-in" style="min-height: 420px; width: 100%;">
		<div class="toggleFilter">
			<h2 id="show" title="Click here to hide/show the form"
				style="cursor: pointer;">
				<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Search
				Vendor
			</h2>
		</div>
		<div id="table-holder"
			style="margin: 0 1.5%; float: left; width: 96%; height: 190px;">

			<html:form action="/searchvendor1.do?method=vendorNameSearch">
				<html:javascript formName="searchVendorForm" />
				<div style="float: left;">

					<table style="padding-left: 10%;" cellspacing="10" cellpadding="5">
						<tr>
							<td>NAICS Category</td>
							<td><html:select property="category"
									styleId="naicsCategoryId" style="width:140px;"
									onchange="ajaxFnSubCategory(this.value,'get_subCategory','subCategory','');">
									<bean:size id="size" name="searchVendorForm"
										property="categories" />
									<logic:greaterEqual value="0" name="size">
										<html:option value="0" key="select">-----Select-----</html:option>
										<logic:iterate id="categories" name="searchVendorForm"
											property="categories">
											<bean:define id="id" name="categories" property="id"></bean:define>
											<bean:define id="naicsCategoryDesc" name="categories"
												property="naicsCategoryDesc"></bean:define>
											<html:option value="<%=id.toString() %>">
												<bean:write name="categories" property="naicsCategoryDesc" />
											</html:option>
										</logic:iterate>
									</logic:greaterEqual>
								</html:select></td>
						</tr>
						<tr>
							<td>NAICS Sub-Category</td>

							<td><div id="get_subCategory">
									<html:select property="subCategory" styleId="subCategory"
										style="width:140px;">
										<html:option value="0" key="select">-----Select-----</html:option>
										<logic:present name="searchVendorForm"
											property="subCategories">


											<bean:size id="size" name="searchVendorForm"
												property="subCategories" />
											<logic:greaterEqual value="0" name="size">

												<logic:iterate id="categories" name="searchVendorForm"
													property="subCategories">
													<bean:define id="id" name="categories" property="id"></bean:define>
													<bean:define id="naicSubCategoryDesc" name="categories"
														property="naicSubCategoryDesc"></bean:define>
													<html:option value="<%=id.toString() %>">
														<bean:write name="categories"
															property="naicSubCategoryDesc" />
													</html:option>
												</logic:iterate>
											</logic:greaterEqual>
										</logic:present>
									</html:select>
								</div></td>
						</tr>
						<tr>
							<td>Country</td>
							<td><html:select property="country"
									styleId="naicsCategoryId" style="width:140px;">
									<logic:present name="searchVendorForm" property="countries">
										<bean:size id="size" property="countries"
											name="searchVendorForm" />
										<logic:greaterEqual value="0" name="size">
											<html:option value="0" key="select">-----Select-----</html:option>
											<logic:iterate id="country" property="countries"
												name="searchVendorForm">

												<html:option value="<%=country.toString() %>">
													<bean:write name="country" />
												</html:option>
											</logic:iterate>
										</logic:greaterEqual>
									</logic:present>
								</html:select></td>

						</tr>

						<tr>
							<td>Status</td>

							<td><html:select property="status" styleId="statusId"
									style="width:140px;">
									<html:option value="-1" key="select">-----Select-----</html:option>
									<html:option value="1" key="select">Active</html:option>
									<html:option value="0" key="select">Pending</html:option>
									<!--<html:option value="3" key="select"></html:option>
									<html:option value="4" key="select">D</html:option> -->
								</html:select></td>
						</tr>


					</table>
				</div>
				<div style="float: right; position: absolute; left: 350px;">

					<table style="padding-right: 10%;" cellspacing="10" cellpadding="5">

						<tr>
							<td>Vendor Name</td>
							<td><html:text property="vendorName" alt="Optional" /></td>
						</tr>
						<tr>
							<td>NAICS Code</td>
							<td><html:text property="naicCode" alt="Optional" /></td>
						</tr>
						<tr>
							<td>City</td>
							<td><html:text property="city" alt="Optional" /></td>
						</tr>
						<tr>
							<td>Region</td>
							<td><html:text property="region" alt="Optional" /></td>
						</tr>


					</table>

				</div>

				<div style="float: right; position: absolute; left: 600px;">

					<table style="padding-right: 10%;" cellspacing="10" cellpadding="5">

						<tr>
							<td>Province</td>
							<td><html:text property="province" alt="Optional" /></td>
						</tr>

						<tr>
							<td>State</td>
							<td><html:text property="state" alt="Optional" /></td>
						</tr>


						<tr>

							<td><html:radio property="diverse" value="1">&nbsp;Diverse&nbsp;</html:radio></td>
							<td><div style="padding-left: 0%;">

									<html:radio property="diverse" value="0">&nbsp;Non Diverse&nbsp;</html:radio>
								</div></td>

						</tr>
						<tr>

							<td><html:radio property="prime" value="1">&nbsp;Prime&nbsp;</html:radio></td>
							<td><div style="padding-left: 0%;">

									<html:radio property="prime" value="0">&nbsp;Non-Prime&nbsp;</html:radio>
								</div></td>
						</tr>

					</table>
					<div style="float: right; position: absolute; left: 100px;">
						<html:submit value="Search" styleClass="customerbtTxt"
							styleId="submit"></html:submit>
						<html:reset value="Clear" styleClass="customerbtTxt"
							styleId="submit"></html:reset>
					</div>
				</div>

			</html:form>
		</div>


		<h2 style="float: left; margin: 1% 1% 1% 1.2%;">
			<img id="show1" src="images/arrow-down.png" />&nbsp;&nbsp;Vendor
			List
		</h2>
		<div id="box2" style="max-height: 180px; overflow: auto;"
			onload="clearForms()">
			<html:form action="/searchvendor1.do?method=viewVendorName">
				<table width="100%">
					<logic:present property="vendorsList" name="searchVendorForm">
						<bean:size id="size" property="vendorsList"
							name="searchVendorForm" />
						<logic:greaterThan value="0" name="size">
							<thead>
								<tr>
									<th>Select All <input type="checkbox" id="selectall" />
									</th>
									<th>Vendor Name</th>
									<th>Country</th>
									<th>NAICS Code</th>
									<th>Duns Number</th>
									<th>Registered Date</th>
									<th>Diverse</th>
									<th>Prime</th>
									<th>Approved</th>
								</tr>
							</thead>
							<logic:iterate property="vendorsList" name="searchVendorForm"
								id="list">
								<bean:define id="vendor" name="list" property="vendor"></bean:define>
								<bean:define id="naics" name="list" property="naics"></bean:define>
								<bean:define id="emailId" name="vendor" property="emailId"></bean:define>


								<tbody>
									<tr>
										<td>
											<div id="checkboxes">
												<input type="checkbox" name="selectedEmailAddress"
													value="<%=emailId.toString()%>" class="case" />
											</div>
										</td>
										<td><bean:write name="vendor" property="vendorName" /></td>
										<td><bean:write name="vendor" property="country" /></td>
										<td><bean:write name="naics" property="naicsCode" /></td>
										<td><bean:write name="vendor" property="dunsNumber" /></td>
										<td><bean:write name="vendor" property="createdOn" /></td>
										<td><logic:equal value="1" name="vendor"
												property="deverseSupplier">
												Diverse
											</logic:equal> <logic:equal value="0" name="vendor"
												property="deverseSupplier">
												Quality
											</logic:equal></td>
										<td><logic:equal value="1" name="vendor"
												property="primeNonPrimeVendor">
												Prime
											</logic:equal> <logic:equal value="0" name="vendor"
												property="primeNonPrimeVendor">
												Non-Prime
											</logic:equal>
										<td><logic:equal value="1" name="vendor"
												property="isApproved">
												Yes
											</logic:equal> <logic:equal value="0" name="vendor" property="isApproved">
												No
											</logic:equal></td>

									</tr>

								</tbody>

							</logic:iterate>
						</logic:greaterThan>
						<logic:equal value="0" name="size">
	No such Records
	</logic:equal>
					</logic:present>

				</table>
				<div style="float: right; position: absolute; left: 400px;">
					<html:submit value="Ok" styleClass="customerbtTxt" styleId="submit"></html:submit>
					<html:reset value="Reset" styleClass="customerbtTxt"></html:reset>
				</div>
			</html:form>

		</div>
	</div>
</div>

