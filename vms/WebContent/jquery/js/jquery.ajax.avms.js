/**
 * Jquery ajax scripts for building jqgrids.
 * 
 * @author vinoth
 */

/*
 * This is functionality for populate grid using total Spend(Report-1).
 */

function getTotalSalesReport() {
	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

	$.ajax({
		url : "totalSalesReport.do?method=searchSpendReport&random="
				+ Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			gridTotalSalesData(data);
		}
	});

}

function gridTotalSalesData(myGridData) {

	var newdata = jQuery.parseJSON(myGridData);
	$('#gridtable').jqGrid(
			{

				data : newdata,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Year', 'Reporting Period',
						'Direct Spend', 'Indirect Spend',
						'Total Spend Reported', 'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency', 
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				sortname : 'totalSales',
				sortorder : 'desc',
				emptyrecords : 'No data available for search criteria..',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalDirectExp = $("#gridtable").jqGrid('getCol',
							'directExp', false, 'sum'), totalIndirectExp = $(
							"#gridtable").jqGrid('getCol', 'indirectExp',
							false, 'sum'), totalSales = $("#gridtable").jqGrid(
							'getCol', 'totalSales', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						reportPeriod : 'Total Spend',
						directExp : totalDirectExp,
						indirectExp : totalIndirectExp,
						totalSales : totalSales
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{
		drag : true,
		closeOnEscape : true
	}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in Indirect spend data report.
 */
function getIndirectSpendData() {
	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

	$.ajax({
		url : "tier2Report.do?method=getIndirectSpendReport&random="
				+ Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			gridIndirectSpendData(data);
		}
	});
}

function gridIndirectSpendData(myGridData) {

	var newdata = jQuery.parseJSON(myGridData);
	$('#gridtable').jqGrid(
			{
				data : newdata,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name',
				'Year', 'Reporting Period', 'Certificate Name',
						'Indirect Spend', 'Total Spend Reported',
						'% Diversity Spend', 'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear',
					align : 'center'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'vendorName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalIndirectExp = $("#gridtable").jqGrid('getCol',
							'indirectExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						indirectExp : totalIndirectExp
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in direct spend data report.
 */
function getDirectSpendData() {

	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

	$.ajax({
		url : "tier2Report.do?method=getDirectSpendReport&random="
				+ Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			gridDirectSpendData(data);
		}
	});
}

function gridDirectSpendData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name', 'Year',
						'Reporting Period', 'Certificate Name', 'Direct Spend',
						'Total Direct Spend Reported', '% Diversity Spend',
						'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName'
				}, {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'vendorName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalDirectExp = $("#gridtable").jqGrid('getCol',
							'directExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						directExp : totalDirectExp
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in supplier diversity report.
 */
function getSupplierDiversityData(param)
{
	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function()
	{
		tier2VendorNames.push($(this).val());
	});
	
	var spendYear = $('#spendYear').val();
	if (spendYear == 0)
	{
		alert('Please Enter Spend Year.');
		return false;
	}
	
	var reportingPeriod = $('#reportingPeriod').val();

	var reportType = $('#reportType').val();
	if (reportType == -1)
	{
		alert('Please Select Spend Type (Direct/Indirect/Both).');
		return false;
	}
	
	/*var selected = $(".row-wrapper input[type='radio']:checked");
	if (selected.length > 0) {
		reportType = selected.val();
	} else {
		alert('Please Enter Report Type (Direct/Indirect).');
		return false;
	}*/
	if(param == 'category'){
	$.ajax(
	{
		url : "diversityReport.do?method=getDiversityReport&type="+ param +"&random=" + Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function()
		{
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data)
		{
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			if (reportType == 0)
			{
				gridDiversityDirectSpendData(data);
			}
			else if (reportType == 1)
			{
				gridDiversityIndirectSpendData(data);
			}
			else
			{
				gridDiversityBothSpendData(data);
			}
		}
	});
	}else{
		$.ajax(
				{
					url : "diversityReport.do?method=getDiversityReport&type="+ param +"&random=" + Math.random(),
					type : "POST",
					data : $("#reportform").serialize(),
					async : true,
					beforeSend : function()
					{
						$("#ajaxloader").show(); //show image loading
					},
					success : function(data)
					{
						$("#ajaxloader").hide();
						$("#gridtable").jqGrid('GridUnload');
						if (reportType == 0)
						{
							gridDiversityDirectSpendDataEthnicity(data);
						}
						else if (reportType == 1)
						{
							gridDiversityIndirectSpendDataEthnicity(data);
						}
						else
						{
							gridDiversityBothSpendDataEthnicity(data);
						}
					}
				});	
	}
}

function gridDiversityDirectSpendData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name', 'Year',
						'Reporting Period', 'Category', 'Direct Spend', 'Sector Description', 'Ethnicity',
						'Total Spend Reported', '% Diversity Spend',
						'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName'
				}, {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				},{
					name : 'sectorDescription',
					index : 'sectorDescription'
				},{
					name : 'ethnicity',
					index : 'ethnicity'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'certificateName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalDirectExp = $("#gridtable").jqGrid('getCol',
							'directExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						directExp : totalDirectExp,
						decimalPlaces: 0
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');

}

function gridDiversityIndirectSpendData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name', 'Year',
						'Reporting Period', 'Category', 'Indirect Spend', 'Sector Description', 'Ethnicity',
						'Total Spend Reported', '% Diversity Spend',
						'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'sectorDescription',
					index : 'sectorDescription'
				},{
					name : 'ethnicity',
					index : 'ethnicity'
				},{
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'certificateName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalIndirectExp = $("#gridtable").jqGrid('getCol',
							'indirectExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						decimalPlaces: 0,
						indirectExp : totalIndirectExp
						
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');

}

function gridDiversityBothSpendData(myGridData)
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : ['Vendor Name', 'Tier2 Vendor Name', 'Year', 'Reporting Period', 'Category', 'Direct Spend', 
		            'Indirect Spend', 'Sector Description', 'Ethnicity', 'Total Spend Reported', '% Diversity Spend', 'Date Submitted' ],
		colModel : [ 
        {
        	name : 'vendorName',
			index : 'vendorName'
		}, {
			name : 'vendorUserName',
			index : 'vendorUserName'
		}, {
			name : 'spendYear',
			index : 'spendYear'
		}, {
			name : 'reportPeriod',
			index : 'reportPeriod'
		}, {
			name : 'certificateName',
			index : 'certificateName'
		}, {
			name : 'directExp',
			index : 'directExp',
			align : 'right',
			formatter : 'currency',
			formatoptions : {
				prefix : '$',
				thousandsSeparator : ',',
				decimalPlaces: 0
			},
			summaryType : 'sum'
		}, {
			name : 'indirectExp',
			index : 'indirectExp',
			align : 'right',
			formatter : 'currency',
			formatoptions : {
				prefix : '$',
				thousandsSeparator : ',',
				decimalPlaces: 0
			},
			summaryType : 'sum'
		},{
			name : 'sectorDescription',
			index : 'sectorDescription'
		},{
			name : 'ethnicity',
			index : 'ethnicity'
		}, {
			name : 'totalSales',
			index : 'totalSales',
			align : 'right',
			sorttype : 'number',
			formatter : 'currency',
			formatoptions : {
				prefix : '$',
				thousandsSeparator : ',',
				decimalPlaces: 0
			}
		}, {
			name : 'diversityPercent',
			index : 'diversityPercent',
			formatter : 'number',
			formatoptions : {
				suffix : '%',
				decimalSeparator : ".",
				decimalPlaces : 1
			},
			align : 'center'
		}, {
			name : 'createdOn',
			index : 'createdOn'
		} ],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available for search criteria..',
		grouping : true,
		groupingView : {
			groupField : [ 'certificateName' ],
			groupColumnShow : [ true ],
			groupText : [ '<b>{0} - {1} Item(s)</b>' ],
			groupCollapse : false,
			groupOrder : [ 'asc' ],
			groupSummary : [ true ],
			showSummaryOnHide : true,
			groupDataSorted : true
		},
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		height : 250,
		gridComplete : function()
		{
			var totalDirectExp = $("#gridtable").jqGrid('getCol', 'directExp', false, 'sum');
			var totalIndirectExp = $("#gridtable").jqGrid('getCol', 'indirectExp', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', 
			{
				certificateName : 'Total Spend',
				directExp : totalDirectExp,
				decimalPlaces: 0,
				indirectExp : totalIndirectExp
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { 
		// add custom button to export the data to excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function()
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function()
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

//Ethnicity
function gridDiversityDirectSpendDataEthnicity(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Ethnicity','Vendor Name', 'Tier2 Vendor Name', 'Year',
						'Reporting Period', 'Category', 'Direct Spend',
						'Total Spend Reported', '% Diversity Spend',
						'Date Submitted' ],
				colModel : [ {
					name : 'ethnicity',
					index : 'ethnicity'
				},{
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName',
					summaryType:'count',
					summaryTpl : 'Total ({0})'
				}, {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'ethnicity', 'certificateName' ],
					groupColumnShow : [ true,true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>','<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc','asc' ],
					groupSummary : [ true,true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalDirectExp = $("#gridtable").jqGrid('getCol',
							'directExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						decimalPlaces: 0,
						directExp : totalDirectExp
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');

}

function gridDiversityIndirectSpendDataEthnicity(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Ethnicity','Vendor Name', 'Tier2 Vendor Name', 'Year',
						'Reporting Period', 'Category', 'Indirect Spend',
						'Total Spend Reported', '% Diversity Spend',
						'Date Submitted' ],
				colModel : [ {
					name : 'ethnicity',
					index : 'ethnicity'
				},{
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName',
					summaryType:'count',
					summaryTpl : 'Total ({0})'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum',
					decimalPlaces: 0
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'ethnicity', 'certificateName' ],
					groupColumnShow : [ true,true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>','<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc','asc' ],
					groupSummary : [ true,true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalIndirectExp = $("#gridtable").jqGrid('getCol',
							'indirectExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						indirectExp : totalIndirectExp,
						decimalPlaces: 0
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');

}

function gridDiversityBothSpendDataEthnicity(myGridData)
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : ['Ethnicity','Vendor Name', 'Tier2 Vendor Name', 'Year', 'Reporting Period', 'Category', 'Direct Spend', 
		            'Indirect Spend', 'Total Spend Reported', '% Diversity Spend', 'Date Submitted' ],
		colModel : [ {
			name : 'ethnicity',
			index : 'ethnicity'
		},{
        	name : 'vendorName',
			index : 'vendorName'
		}, {
			name : 'vendorUserName',
			index : 'vendorUserName'
		}, {
			name : 'spendYear',
			index : 'spendYear'
		}, {
			name : 'reportPeriod',
			index : 'reportPeriod'
		}, {
			name : 'certificateName',
			index : 'certificateName',
			summaryType:'count',
			summaryTpl : 'Total ({0})'
		}, {
			name : 'directExp',
			index : 'directExp',
			align : 'right',
			formatter : 'currency',
			formatoptions : {
				prefix : '$',
				thousandsSeparator : ',',
				decimalPlaces: 0
			},
			summaryType : 'sum'
		}, {
			name : 'indirectExp',
			index : 'indirectExp',
			align : 'right',
			formatter : 'currency',
			formatoptions : {
				prefix : '$',
				thousandsSeparator : ',',
				decimalPlaces: 0
			},
			summaryType : 'sum'
		}, {
			name : 'totalSales',
			index : 'totalSales',
			align : 'right',
			sorttype : 'number',
			formatter : 'currency',
			formatoptions : {
				prefix : '$',
				thousandsSeparator : ',',
				decimalPlaces: 0
			}
		}, {
			name : 'diversityPercent',
			index : 'diversityPercent',
			formatter : 'number',
			formatoptions : {
				suffix : '%',
				decimalSeparator : ".",
				decimalPlaces : 1
			},
			align : 'center'
		}, {
			name : 'createdOn',
			index : 'createdOn'
		} ],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available for search criteria..',
		grouping : true,
		groupingView : {
			groupField : [ 'ethnicity','certificateName' ],
			groupColumnShow : [ true,true ],
			groupText : [ '<b>{0} - {1} Item(s)</b>','<b>{0} - {1} Item(s)</b>' ],
			groupCollapse : false,
			groupOrder : [ 'asc','asc' ],
			groupSummary : [ true,true ],
			showSummaryOnHide : true,
			groupDataSorted : true
		},
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		height : 250,
		gridComplete : function()
		{
			var totalDirectExp = $("#gridtable").jqGrid('getCol', 'directExp', false, 'sum');
			var totalIndirectExp = $("#gridtable").jqGrid('getCol', 'indirectExp', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', 
			{
				certificateName : 'Total Spend',
				directExp : totalDirectExp,
				decimalPlaces: 0,
				indirectExp : totalIndirectExp
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},
	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { 
		// add custom button to export the data to excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function()
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function()
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}
//Ethnicity

/*
 * This is functionality for populate grid in supplier diversity report.
 */
function getAgencyReportData() {

	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

	$.ajax({
		url : "agencyReport.do?method=getAgencyReport&random="
				+ Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide(); 
			$("#gridtable").jqGrid('GridUnload');
			gridAgencyData(data);
		}
	});
}

function gridAgencyData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name', 'Year',
						'Reporting Period', 'Agency Name', 'Direct Spend',
						'Indirect Spend', 'Total Spend Reported',
						'% Diversity Spend', 'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName'
				}, {
					name : 'spendYear',
					index : 'spendYear'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName'
				}, {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ','
					},
					summaryType : 'sum'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ','
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ','
					},
					summaryType : 'sum'
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'certificateName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalDirectExp = $("#gridtable").jqGrid('getCol',
							'directExp', false, 'sum'), totalIndirectExp = $(
							"#gridtable").jqGrid('getCol', 'indirectExp',
							false, 'sum'), totalSales = $("#gridtable").jqGrid(
							'getCol', 'totalSales', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						certificateName : 'Total Spend',
						directExp : totalDirectExp,
						indirectExp : totalIndirectExp,
						totalSales : totalSales
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in ethnicity spend report.
 */
function getEthnicitySpendReport() {

	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

	$.ajax({
			url : "ethnicityReport.do?method=getEthnicitySpendReport&random="
			+ Math.random(),
			type : "POST",
			data : $("#reportform").serialize(),
			async : true,
			beforeSend : function(){
				$("#ajaxloader").show(); //show image loading
			},
			success : function(data) {
				$("#ajaxloader").hide();
				$("#gridtable").jqGrid('GridUnload');
				gridEthnicitySpendData(data);
			}
		});
}

function gridEthnicitySpendData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable')
			.jqGrid(
					{

						data : newdata1,
						datatype : 'local',
						colNames : [ 'Vendor Name', 'Black',
								'Hispanic American', 'Asian Pacific',
								'Asian Indian', 'Native American',
								'Total Spend', '% Diversity Spend',
								'Date Submitted' ],
						colModel : [ {
							name : 'vendorName',
							index : 'vendorName'
						}, {
							name : 'black',
							index : 'black',
							align : 'right',
							sorttype : 'number',
							formatter : 'currency',
							formatoptions : {
								prefix : '$',
								thousandsSeparator : ','
							},
							summaryType : 'sum'
						}, {
							name : 'hispanicAmerican',
							index : 'hispanicAmerican',
							align : 'right',
							sorttype : 'number',
							formatter : 'currency',
							formatoptions : {
								prefix : '$',
								thousandsSeparator : ','
							},
							summaryType : 'sum'
						}, {
							name : 'asianPacific',
							index : 'asianPacific',
							align : 'right',
							sorttype : 'number',
							formatter : 'currency',
							formatoptions : {
								prefix : '$',
								thousandsSeparator : ','
							},
							summaryType : 'sum'
						}, {
							name : 'asianIndian',
							index : 'asianIndian',
							align : 'right',
							sorttype : 'number',
							formatter : 'currency',
							formatoptions : {
								prefix : '$',
								thousandsSeparator : ','
							},
							summaryType : 'sum'
						}, {
							name : 'nativeAmerican',
							index : 'nativeAmerican',
							align : 'right',
							sorttype : 'number',
							formatter : 'currency',
							formatoptions : {
								prefix : '$',
								thousandsSeparator : ','
							},
							summaryType : 'sum'
						}, {
							name : 'totalSales',
							index : 'totalSales',
							align : 'right',
							sorttype : 'number',
							formatter : 'currency',
							formatoptions : {
								prefix : '$',
								thousandsSeparator : ','
							},
							summaryType : 'sum'
						}, {
							name : 'diversityPercent',
							index : 'diversityPercent',
							formatter : 'number',
							formatoptions : {
								suffix : '%',
								decimalSeparator : ".",
								decimalPlaces : 1
							},
							align : 'center'
						}, {
							name : 'createdOn',
							index : 'createdOn'
						} ],
						/*rowNum : 10,
						rowList : [ 10, 20, 50 ],*/
						pager : '#pager',
						shrinkToFit : false,
						autowidth : true,
						viewrecords : true,
						emptyrecords : 'No data available for search criteria..',
						grouping : true,
						groupingView : {
							groupField : [ 'vendorName' ],
							groupColumnShow : [ true ],
							groupText : [ '<b>{0} - {1} Item(s)</b>' ],
							groupCollapse : false,
							groupOrder : [ 'asc' ],
							groupSummary : [ true ],
							showSummaryOnHide : true,
							groupDataSorted : true
						},
						footerrow : true,
						userDataOnFooter : true,
						cmTemplate: { title: false },
						gridview: true,
			            pgbuttons:false,
			            pgtext:'',
			            width: ($.browser.webkit?466:497),
			            loadonce: true,
			            rowNum: 2000,
						height : 250,
						gridComplete : function() {
							var totalBlack = $("#gridtable").jqGrid('getCol',
									'black', false, 'sum'), totalHispanicAmerican = $(
									"#gridtable").jqGrid('getCol',
									'hispanicAmerican', false, 'sum'), totalAsianPacific = $(
									"#gridtable").jqGrid('getCol',
									'asianPacific', false, 'sum'), totalAsianIndian = $(
									"#gridtable").jqGrid('getCol',
									'asianIndian', false, 'sum'), totalNativeAmerican = $(
									"#gridtable").jqGrid('getCol',
									'nativeAmerican', false, 'sum'), totalSales = $(
									"#gridtable").jqGrid('getCol',
									'totalSales', false, 'sum');
							$("#gridtable").jqGrid('footerData', 'set', {
								vendorName : 'Total Spend',
								black : totalBlack,
								hispanicAmerican : totalHispanicAmerican,
								asianPacific : totalAsianPacific,
								asianIndian : totalAsianIndian,
								nativeAmerican : totalNativeAmerican,
								totalSales : totalSales
							});
						}
					}).jqGrid('navGrid', '#pager', {
				add : false,
				edit : false,
				del : false,
				search : false
			},

			{}, /* edit options */
			{}, /* add options */
			{}, /* del options */
			{});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in ethnicity breakdown report.
 */
function getEthnicityBreakdownReport() {

	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

//	var reportType = "";
//	var selected = $(".row-wrapper input[type='radio']:checked");
//	if (selected.length > 0) {
//		reportType = selected.val();
//	} else {
//		alert('Please Enter Report Type (Direct/Indirect).');
//		return false;
//	}

	$.ajax({
		url : "ethnicityReport.do?method=getEthnicityBreakdownReport&random="
		+ Math.random(),
		type : "POST",
		data : $("#reportform").serialize(),
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$("#gridtable").jqGrid('GridUnload');
			/*
			 * Find type of report type
			 */
//			if (reportType == 0) {
//				gridEthnicityBreakdownDirectData(data);
//			} else {
				gridEthnicityBreakdownIndirectData(data);
//			}
		}
	});

}

function gridEthnicityBreakdownDirectData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name',
						'Reporting Period', 'Certificate Name', 'Ethnicity',
						'Direct Spend' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName',
					align : 'center'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName',
					align : 'center'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'certificateName',
					index : 'certificateName'
				}, {
					name : 'ethnicity',
					index : 'ethnicity'
				}, {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ','
					},
					summaryType : 'sum'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'vendorName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalDirectExp = $("#gridtable").jqGrid('getCol',
							'directExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						ethnicity : 'Total Spend',
						directExp : totalDirectExp
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

function gridEthnicityBreakdownIndirectData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Reporting Period', 'Indirect Spend' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName',
					align : 'center'
				}, {
					name : 'reportPeriod',
					index : 'reportPeriod'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				} ],
				/*rowNum : 10,
				rowList : [ 10, 20, 50 ],*/
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'vendorName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 2000,
				height : 250,
				gridComplete : function() {
					var totalIndirectExp = $("#gridtable").jqGrid('getCol',
							'indirectExp', false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						ethnicity : 'Total Spend',
						decimalPlaces: 0,
						indirectExp : totalIndirectExp
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // add custom button to export the data to
		// excel
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() {
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');
}

/**
 * Function to show/hide necessary columns in grid for ethnicity breakdown
 * report.
 */
function showEthnicityBreakdownReport(reportType) {
	if (reportType == 0) {
		jQuery("#gridtable").jqGrid('hideCol', [ "indirectExp" ]);
		jQuery("#gridtable").jqGrid(
				'showCol',
				[ "vendorName", "vendorUserName", "reportPeriod",
						"certificateName", "ethnicity", "directExp" ]);
	} else {
		jQuery("#gridtable").jqGrid('hideCol',
				[ "vendorUserName", "directExp" ]);
		jQuery("#gridtable").jqGrid(
				'showCol',
				[ "vendorName", "reportPeriod", "certificateName", "ethnicity",
						"indirectExp" ]);
	}
}

/*
 * This is functionality get spend dashboard report.
 */
function getSpendDashboardData() {

	var tier2VendorNames = [];
	$('#tier2VendorNames > :selected').each(function() {
		tier2VendorNames.push($(this).val());
	});
	var spendYear = $('#spendYear').val();
	if (spendYear == 0) {
		alert('Please Enter Spend Year.');
		return false;
	}
	var reportingPeriod = $('#reportingPeriod').val();

	$.ajax({
		url : "spendReport.do?method=getSpendReportDashboard&tier2VendorNames="
				+ tier2VendorNames + "&spendYear=" + spendYear
				+ "&reportingPeriod=" + reportingPeriod + "&random="
				+ Math.random(),
		type : "POST",
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$('#ajaxcontent').html(data);
			$('#pdfOption').css( "display", "block");
			// emptyDittoCells($('#report'));
		}
	});
}

function emptyDittoCells(table) {

	if (!(table instanceof jQuery && table.is("table")))
		throw "bad parameter";

	$("#report").find('tr').each(
			function(rowElement) {
				var row = $(rowElement);
				var previousRow = $(rowElement).prev('tr');
				if (!previousRow.length)
					return;

				row.each(function(cellElement, index) {
					var cell = $(cellElement);
					var previousCell = previousRow.children("td:nth-child("
							+ index + ")");

					while (previousCell.data('ditto')) {
						previousCell = previousCell.data('ditto');
					}

					if (hasSameContents(cell, previousCell)) {
						cell.empty().data('ditto', previousCell);
					}
				});
			});
}

function hasSameContents(a, b) {
	return (a.text() == b.text()); // naive but usable in the simple case
}

/**
 * Function to load the search vendor data.
 */
function getSelectedIds() {
	var approvalStatus = $('#changeVendorStatus1 :selected').val();
	
	var selectedId = $('#vendorid').val();
	var prime = $('input:radio[name=prime]:checked').val();	
	var isApprovedDesc = $('#isApprovedDesc').val();
	var bpSegmentId = $('#bpsegmentId').val();
	var bpSegmentStatus = $('#bpSegmentStatus').val();
	if(prime===undefined){
		prime='2';
	}
	if(bpSegmentStatus == "Yes")
	{
		if(bpSegmentId != 0 && bpSegmentId != null && bpSegmentId != undefined)
		{
			
			if (selectedId != 0 && selectedId != null
					&& approvalStatus != null && approvalStatus != "" ) {
				$.ajax({
					url : "searchvendor.do?method=approveVendorCustomer&selectedId="
						+ selectedId +  "&prime=" + prime +  "&approvalStatus="
						+ approvalStatus + "&isApprovedDesc=" + isApprovedDesc+ "&bpSegmentId="+ bpSegmentId + '&random='
						+ Math.random(),
					type : "POST",
					async : true,
					beforeSend : function(){
						$("#ajaxloader").show(); //show image loading
						$("#statusButton").prop("disabled", true);
						$("#statusButton").css("background"," none repeat scroll 0 0 #848484");
					},
					success : function(data) {
						$("#ajaxloader").hide();
						$('#dialog1').dialog('close');
						//$("#gridsearchvendor").jqGrid('GridUnload');
						//gridSearchData(data);				
						/*$("#changeVendorStatus1 option[value='" + approvalStatus + "']").remove();
						$('#changeVendorStatus1').trigger("chosen:updated");*/
					    $('#isApprovedDesc').val('');
					    $('input:radio[name=prime]:checked').val('');
					    if($.trim(approvalStatus) == 'B') {
					    	window.location = "vendornavigation.do?parameter=businessAreaNavigation";
					    }
					}
				});
				
			} else {
				alert('Kindly fill the information.');
			}
		}
		else {
			alert('Kindly fill the information.');
		}
	}
	else
	{
		if (selectedId != 0 && selectedId != null
				&& approvalStatus != null && approvalStatus != "") {
			$.ajax({
				url : "searchvendor.do?method=approveVendorCustomer&selectedId="
					+ selectedId +  "&prime=" + prime +  "&approvalStatus="
					+ approvalStatus + "&isApprovedDesc=" + isApprovedDesc + '&random='
					+ Math.random(),
				type : "POST",
				async : true,
				beforeSend : function(){
					$("#ajaxloader").show(); //show image loading
					$("#statusButton").prop("disabled", true);
					$("#statusButton").css("background"," none repeat scroll 0 0 #848484");
				},
				success : function(data) {
					$("#ajaxloader").hide();
					$('#dialog1').dialog('close');
					//$("#gridsearchvendor").jqGrid('GridUnload');
					//gridSearchData(data);				
					/*$("#changeVendorStatus1 option[value='" + approvalStatus + "']").remove();
					$('#changeVendorStatus1').trigger("chosen:updated");*/
				    $('#isApprovedDesc').val('');
				    $('input:radio[name=prime]:checked').val('');
				    if($.trim(approvalStatus) == 'B') {
				    	window.location = "vendornavigation.do?parameter=businessAreaNavigation";
				    }
				}
			});
			
		} else {
			alert('Kindly fill the information.');
		}
	}
}

function gridSearchData(myGridData) {
	var newdata1 = jQuery.parseJSON(myGridData);
	$('#gridsearchvendor').jqGrid(
			{
				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Id', 'Vendor Name', 'Country',
						'NAICS Code', 'Duns Number', 'Registered Date',
						'Status', 'Diverse', 'Action' ],
				colModel : [ {
					name : 'vendorId',
					index : 'vendorId',
					hidden : true
				}, {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'country',
					index : 'country',
					align : 'center'
				}, {
					name : 'naicsCode',
					index : 'naicsCode',
					align : 'center'
				}, {
					name : 'dunsNumber',
					index : 'dunsNumber',
					align : 'center'
				}, {
					name : 'createdOn',
					index : 'createdOn',
					align : 'center',
					sorttype : 'date',
					formatter : 'date',
					formatoptions : {
						srcformat : 'ISO8601Long',
						newformat : 'd-m-Y'
					}
				}, {
					name : 'status',
					index : 'status',
					align : 'center'
				}, {
					name : 'deverseSupplier',
					index : 'deverseSupplier',
					align : 'center'
				}, {
					name : 'action',
					index : 'action',
					align : 'center'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				cmTemplate: { title: false },
				height : 'auto',
				onSelectRow : function(id) {
					var rowData = jQuery(this).getRowData(id);
					var id = rowData['vendorId'];
					$('#vendorid').val(id);
					$("#dialog").css({
						"display" : "block"
					});
					$("#dialog").dialog({
						minWidth : 521,
						modal : true,
						close : function(event, ui) {
							resetFields();
						}
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});
}

function resetFields() {
	$('.selectedPrimeVendor').prop('checked', false);
	$('.selectedApprovalStatus').prop('checked', false);
	$('#isApprovedDesc').val('');
}

/*
 * This is functionality for populate grid in ethnicity dashboard report.
 */
function getDashboardReports() {

	$.ajax({
		url : "reportdashboard.do?method=getDashboardData",
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable").jqGrid('GridUnload');
			gridEthnicityDashboardData(data);
		}
	});

}

function gridEthnicityDashboardData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Records', 'count', '%' ],
				colModel : [ {
					name : 'ethnicity',
					index : 'ethnicity',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager',
				/* autowidth: true, */
				shrinkToFit : false,
				viewrecords : true,
				emptyrecords : 'No data available',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalcount = $("#gridtable").jqGrid('getCol', 'count',
							false, 'sum');
					$("#gridtable").jqGrid('footerData', 'set', {
						ethnicity : 'Total Records',
						count : totalcount
					});
				}
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() {
		$("#gridtable").setGridWidth($('#grid_container1').width() - 30, true);
	}).trigger('resize');

}

/*
 * This is functionality for populate grid in ethnicity dashboard report.
 */
function getDiversityCharts() {

	$.ajax({
		url : "reportdashboard.do?method=getDiversityChartData",
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable1").jqGrid('GridUnload');
			gridDiversityDashboardData(data);
		}
	});

}

function gridDiversityDashboardData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable1').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : ['Certificate Name', 'Short Name', 'Count', '%' ],
				colModel : [{
					name : 'certificateName',
					index : 'certificateName',
					align : 'center',
					sorttype: 'string',
					cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="white-space: normal;"' } 
				},{
					name : 'certificateShortName',
					index : 'certificateShortName',
					sorttype: 'string',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					sorttype: 'number',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center',
					formatter: 'number', 
					formatoptions: {suffix: '%'},
					sorttype: 'number'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager1',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalcount = $("#gridtable1").jqGrid('getCol', 'count',
							false, 'sum');
					$("#gridtable1").jqGrid('footerData', 'set', {
						certificateName : 'Total Records',
						count : totalcount
					});
				}
			}).jqGrid('navGrid', '#pager1', {
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind(
			'resize',
			function() {
				$("#gridtable1").setGridWidth(
						$('#grid_container2').width() - 30, true);
			}).trigger('resize');
}

/*
 * This is functionality for populate grid in search report.
 */

function searchByPreviousSavedColumns(searchId) {
	$.ajax({
		url : "dynamicReport.do?method=searchByPreviousSavedColumns&searchId="+searchId ,
		data : $("#searchform").serialize(),
		type : "POST",
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$("#dialog1").dialog("close");
			$('#select').css('display', 'none');
			$('#title').css('display', 'none');
			$('#backToSearch').css('display', 'block');
			$("#gridtable").jqGrid('GridUnload');
			gridSearchVendorData(data);
			if (data != "" && data != "undefined") {
				$('#resultHeading').css('display', 'none');
				$('#dynamicresultHeading').css('display', 'block');
				$('#dynamicExport').css('display', 'block');
			}
		}
	});
	
	return false;
}

function searchReport() {

	$.ajax({
		url : "searchReport.do?method=getSearchReportData" ,
		data : $("#searchform").serialize(),
		type : "POST",
		async : true,
		beforeSend : function(){
			$("#ajaxloader").show(); //show image loading
		},
		success : function(data) {
			$("#ajaxloader").hide();
			$('#select').css('display', 'none');
			$('#title').css('display', 'none');
			$('#backToSearch').css('display', 'block');
			$("#gridtable").jqGrid('GridUnload');
			gridSearchVendorData(data);
			if (data != "" && data != "undefined") {
				$('#export').css('display', 'block');
				$('#resultHeading').css('display', 'block');
				$('#dynamicresultHeading').css('display', 'none');
			}
		}
	});
	
	return false;
}

function gridSearchVendorData(myGridData) {
	
	var dynamicData = myGridData.split("|");
	var dynamicColNames = jQuery.parseJSON(dynamicData[0]);
	var dynamicColModel = jQuery.parseJSON(dynamicData[1]);
	var dynamicColData = dynamicData[2];

	var newdata1 = jQuery.parseJSON(dynamicColData);

	$('#gridtable').jqGrid({
		
				data : newdata1,
				datatype : 'local',
				colNames : dynamicColNames,
				colModel : dynamicColModel,
				pager : '#pager',
				shrinkToFit : false,
				viewrecords : true,
				autowidth : true,
				emptyrecords : 'No data available',
				gridview: true,
	            pgbuttons:false,
	            pgtext:'',
	            width: ($.browser.webkit?466:497),
	            loadonce: true,
	            rowNum: 10000,
				cmTemplate: { title: false },
				height: 250
			}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},
	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{
		drag : true,
		closeOnEscape : true
	}
	);

	$("#gridtable").jqGrid('navButtonAdd', '#pager', {
		caption : "Choose Columns",
		title : "Reorder Columns",
		onClickButton : function() {
			$("#gridtable").jqGrid('columnChooser', {
				title : "Select and reorder columns",
				width : 700,
				height : 350,
				done : function(perm) {
					if (perm) {
						this.jqGrid("remapColumns", perm, true);
					}
				}
			});
		}
	});
}

function exportGrid() {

	$("#dialog").css({
		"display" : "block"
	});
	$("#dialog").dialog({
		minWidth : 300,
		modal : true
	});

}
function exportHelper(table, path) {
	var exportType = $("input[name=export]:checked").val();

	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Company Name");
		} else if (cols[i] == "ownerName") {
			index.push(1);
			valueForCsv.push("Owner Name");
		} else if (cols[i] == "address") {
			index.push(2);
			valueForCsv.push("Mailing Address");
		} else if (cols[i] == "city") {
			index.push(3);
			valueForCsv.push("City");
		} else if (cols[i] == "state") {
			index.push(4);
			valueForCsv.push("State");
		} else if (cols[i] == "zipcode") {
			index.push(5);
			valueForCsv.push("Zip");
		} else if (cols[i] == "phone") {
			index.push(6);
			valueForCsv.push("Phone");
		} else if (cols[i] == "fax") {
			index.push(7);
			valueForCsv.push("Fax");
		} else if (cols[i] == "emailId") {
			index.push(8);
			valueForCsv.push("Email");
		} else if (cols[i] == "naicsCapabilities") {
			index.push(9);
			valueForCsv.push("NAICS Capabilities");
		} 	
		else if (cols[i] == "diverseClassification") {
			index.push(10);
			valueForCsv.push("Diverse Classification");
		} 	
		else if (cols[i] == "certificateAgencyDetails") {
			index.push(11);
			valueForCsv.push("Certificate Agency Details");
		} 	
		
		
		/*else if (cols[i] == "agencyName") {
			index.push(9);
			valueForCsv.push("Agency");
		} else if (cols[i] == "certName") {
			index.push(10);
			valueForCsv.push("Certification Type");
		} else if (cols[i] == "effDate") {
			index.push(11);
			valueForCsv.push("Certified");
		} else if (cols[i] == "expDate") {
			index.push(12);
			valueForCsv.push("Expiration");
		} else if (cols[i] == "capablities") {
			index.push(13);
			valueForCsv.push("Capability");
		} else if (cols[i] == "naicsDesc") {
			index.push(14);
			valueForCsv.push("Category");
		}else if (cols[i] == "lastName") {
			index.push(15);
			valueForCsv.push("Owner Last");
		} */
	}

	if (exportType == 1) {
		exportExcel(table, index, 'SupplierReport');
	} else if (exportType == 2) {
		$('#' + table).table2CSV({
			header : valueForCsv
		});
	} else if (exportType == 3) {
		exportPDF(table, index, 'SupplierReport', path);
	} else {
		alert("Please Choose a type to export");
		return false;
	}

	$('#dialog').dialog('close');
}


function exportHelperForDynamic(table, path) {
	var exportType = $("input[name=export]:checked").val();

	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});
	var index = [];
	var valueForCsv = [];
	//cols[] has correct order of header label from jQGrid so use directly for CSV & Excel
	for ( var i = 0; i < cols.length; i++) {
		index.push(i);
		valueForCsv.push(cols[i]);
	}
	if (exportType == 1) {
		window.location = "searchReport.do?method=getCustomSearchExcelReport&colHeader="+valueForCsv;
//		dynamicExportExcel(table, index, 'SupplierReport');
	} else if (exportType == 2) {
		window.location = "searchReport.do?method=getCustomSearchCSVReport&colHeader="+valueForCsv;
//		$('#' + table).table2CSV({
//			header : valueForCsv
//		});
	} else if (exportType == 3) {
		exportPDF(table, index, 'SupplierReportWithCustomColumns', path);
	} else {
		alert("Please Choose a type to export");
		return false;
	}
	$('#dialog').dialog('close');
	$('#dialog2').dialog('close');
}

/**
 * Function to export the total supplier spend data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportSupplierSales(table, path) {
	var exportType = $("input[name=export]:checked").val();

	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});

	/*
	 * $('#' + table).find('tr').each(function() {
	 * $(this).filter(':visible').find('td').each(function() {
	 * cols.push($(this).html()); }); });
	 */

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "spendYear") {
			index.push(1);
			valueForCsv.push("Spend Year");
		} else if (cols[i] == "reportPeriod") {
			index.push(2);
			valueForCsv.push("Report Period");
		} else if (cols[i] == "directExp") {
			index.push(3);
			valueForCsv.push("Direct Spend");
		} else if (cols[i] == "indirectExp") {
			index.push(4);
			valueForCsv.push("Indirect Spend");
		} else if (cols[i] == "totalSales") {
			index.push(5);
			valueForCsv.push("Total Spend Reported");
		} else if (cols[i] == "createdOn") {
			index.push(6);
			valueForCsv.push("Date Submitted");
		}
	}

	if (exportType == 1) {
		exportExcel(table, index, 'TotalSalesReport');
	} else if (exportType == 2) {
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} else if (exportType == 3) {		
		window.location = "totalSalesReport.do?method=getSearchSpendReportPdf";
	} else {
		alert("Please Choose a type to export");
		return false;
	}

	$('#dialog').dialog('close');

}

/**
 * Function to export the direct supplier spend.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportDirectSpend(table, path) {
	var exportType = $("input[name=export]:checked").val();

	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "vendorUserName") {
			index.push(1);
			valueForCsv.push("Tier2 Vendor Name");
		} else if (cols[i] == "spendYear") {
			index.push(2);
			valueForCsv.push("Spend Year");
		} else if (cols[i] == "reportPeriod") {
			index.push(3);
			valueForCsv.push("Report Period");
		} else if (cols[i] == "certificateName") {
			index.push(4);
			valueForCsv.push("Certificate Name");
		} else if (cols[i] == "directExp") {
			index.push(5);
			valueForCsv.push("Direct Spend");
		} else if (cols[i] == "totalSales") {
			index.push(6);
			valueForCsv.push("Total Spend Reported");
		} else if (cols[i] == "diversityPercent") {
			index.push(7);
			valueForCsv.push("% Diversity Spend");
		} else if (cols[i] == "createdOn") {
			index.push(8);
			valueForCsv.push("Date Submitted");
		}
	}

	if (exportType == 1) {
		exportExcel(table, index, 'DirectSpendReport');
	} else if (exportType == 2) {
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} else if (exportType == 3) {		
		window.location = "tier2Report.do?method=getDirectSpendReportPdf";
	} else {
		alert("Please Choose a type to export");
		return false;
	}

	$('#dialog').dialog('close');

}

/**
 * Function to export the indirect supplier spend.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportIndirectSpend(table, path) {
	var exportType = $("input[name=export]:checked").val();

	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "vendorUserName") {
			index.push(1);
			valueForCsv.push("Tier2 Vendor Name");
		} else if (cols[i] == "spendYear") {
			index.push(2);
			valueForCsv.push("Spend Year");
		} else if (cols[i] == "reportPeriod") {
			index.push(3);
			valueForCsv.push("Report Period");
		} else if (cols[i] == "certificateName") {
			index.push(4);
			valueForCsv.push("Certificate Name");
		} else if (cols[i] == "indirectExp") {
			index.push(5);
			valueForCsv.push("Indirect Spend");
		} else if (cols[i] == "totalSales") {
			index.push(6);
			valueForCsv.push("Total Spend Reported");
		} else if (cols[i] == "diversityPercent") {
			index.push(7);
			valueForCsv.push("% Diversity Spend");
		} else if (cols[i] == "createdOn") {
			index.push(8);
			valueForCsv.push("Date Submitted");
		}
	}

	if (exportType == 1) {
		exportExcel(table, index, 'IndirectSpendReport');
	} else if (exportType == 2) {
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} else if (exportType == 3) {		
		window.location = "tier2Report.do?method=getIndirectSpendReportPdf";
	} else {
		alert("Please Choose a type to export");
		return false;
	}

	$('#dialog').dialog('close');

}

/**
 * Function to export the diversity data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportDiversityData(table, path, type) {
	var exportType = $("input[name=export]:checked").val();

	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});

	var reportType=1;//For Getting Type of report Direct or Indirect or Both, if Both = 2 or Direct = 1 or Indirect = 0. Used for PDF Generation
	var reportTypeCount = 0;//For Getting Type of report Direct or Indirect or Both, if Both = 2 or Direct or Indirect = 0. Used for PDF Generation
	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "vendorUserName") {
			index.push(1);
			valueForCsv.push("Tier2 Vendor Name");
		} else if (cols[i] == "spendYear") {
			index.push(2);
			valueForCsv.push("Spend Year");
		} else if (cols[i] == "reportPeriod") {
			index.push(3);
			valueForCsv.push("Report Period");
		} else if (cols[i] == "certificateName") {
			index.push(4);
			valueForCsv.push("Certificate Name");
		} else if (cols[i] == "directExp") {
			index.push(5);
			valueForCsv.push("Direct Spend");
			reportType=1;
			reportTypeCount++;
		} else if (cols[i] == "indirectExp") {
			if(reportTypeCount == "0")
			{
				index.push(5);
				valueForCsv.push("Indirect Spend");
				reportType=0;
			}
			else//For Both
			{
				index.push(6);
				valueForCsv.push("Indirect Spend");
				reportType=2;
			}
			reportTypeCount++;
		} else if (cols[i] == "totalSales") {
			if(reportTypeCount == "2")//For Both
			{
				index.push(7);
				valueForCsv.push("Total Spend Reported");
			}
			else
			{
				index.push(6);
				valueForCsv.push("Total Spend Reported");
			}
			
		} else if (cols[i] == "diversityPercent") {
			if(reportTypeCount == "2")//For Both
			{
				index.push(8);
				valueForCsv.push("% Diversity Spend");
			}
			else
			{
				index.push(7);
				valueForCsv.push("% Diversity Spend");
			}
			
		} else if (cols[i] == "createdOn") {
			if(reportTypeCount == "2")//For Both
			{
				index.push(9);
				valueForCsv.push("Date Submitted");
			}
			else
			{
				index.push(8);
				valueForCsv.push("Date Submitted");
			}			
		}else if (cols[i] == "ethnicity") {
			index.push(10);
			valueForCsv.push("Ethnicity");
		}else if (cols[i] == "sectorDescription") {
			index.push(11);
			valueForCsv.push("Sector Description");
		}
	}

	if (exportType == 1) {
		exportExcel(table, index, 'PrimeSupplierbyTier2DiversityCategory');
	} else if (exportType == 2) {
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} else if (exportType == 3) {		
		window.location = "diversityReport.do?method=getDiversityReportPdf&type="+type+"&reportType="+reportType;
	} else {
		alert("Please Choose a type to export");
		return false;
	}

	$('#dialog').dialog('close');

}

/**
 * Function to export the ethnicity data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportEthnicityData(table, path) {
	
//	var reportType = $("input[name=reportType]:checked").val();	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "vendorUserName") {
			index.push(1);
			valueForCsv.push("Tier2 Vendor Name");
		} else if (cols[i] == "reportPeriod") {
			index.push(1);
			valueForCsv.push("Report Period");
		} else if (cols[i] == "certificateName") {
			index.push(3);
			valueForCsv.push("Certificate Name");
		} else if (cols[i] == "ethnicity") {
			index.push(4);
			valueForCsv.push("Ethnicity");
		} else if (cols[i] == "directExp") {
			index.push(5);
			valueForCsv.push("Direct Spend");
		} else if (cols[i] == "indirectExp") {
			index.push(2);
			valueForCsv.push("Indirect Spend");
		}
	}

	if (exportType == 1) {
		exportExcel(table, index, 'Tier2ReportingSummary');
	} else if (exportType == 2) {
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} else if (exportType == 3) {
		window.location = "ethnicityReport.do?method=getEthnicityBreakdownReportPdf";
	} else {
		alert("Please Choose a type to export");
		return false;
	}

	$('#dialog').dialog('close');

}

/**
 * Export data in excel format
 */
function exportExcel(table, filtered, fileName) {

	var mya = new Array();
	mya = $("#" + table).getDataIDs(); // Get All IDs
	var data = $("#" + table).getRowData(mya[0]); // Get First row to get the
	// labels
	var colNames = new Array();
	var ii = 0;
	for ( var i in data) {
		colNames[ii++] = i;
	} // capture col names
	var html = "";
	for ( var k = 0; k < colNames.length; k++) {		
		if ($.inArray(k, filtered) >= 0) {			
			if (colNames[k] == "vendorName") {
				html = html + "Company Name" + "\t";
			} else if (colNames[k] == "spendYear") {
				html = html + "Spend Year" + "\t";
			} else if (colNames[k] == "reportPeriod") {
				html = html + "Report Period" + "\t";
			} else if (colNames[k] == "directExp") {
				html = html + "Direct Spend" + "\t";
			} else if (colNames[k] == "indirectExp") {
				html = html + "Indirect Spend" + "\t";
			} else if (colNames[k] == "totalSales") {
				html = html + "Total Spend Reported" + "\t";
			} else if (colNames[k] == "createdOn") {
				html = html + "Date Submitted" + "\t";
			} else if (colNames[k] == "vendorUserName") {
				html = html + "Tier2 Vendor Name" + "\t";
			} else if (colNames[k] == "certificateName") {
				html = html + "Certificate Name" + "\t";
			} else if (colNames[k] == "diversityPercent") {
				html = html + "% Diversity Spend" + "\t";
			} else if (colNames[k] == "ethnicity") {
				html = html + "Ethnicity" + "\t";
			} else if (colNames[k] == "firstName") {
				html = html + "First Name" + "\t";
			} else if (colNames[k] == "lastName") {
				html = html + "Last Name" + "\t";
			} else if (colNames[k] == "address") {
				html = html + "Mailing Address" + "\t";
			} else if (colNames[k] == "city") {
				html = html + "City" + "\t";
			} else if (colNames[k] == "state") {
				html = html + "State" + "\t";
			} else if (colNames[k] == "zipcode") {
				html = html + "Zip" + "\t";
			} else if (colNames[k] == "phone") {
				html = html + "Phone" + "\t";
			} else if (colNames[k] == "fax") {
				html = html + "Fax" + "\t";
			} else if (colNames[k] == "emailId") {
				html = html + "Email Id" + "\t";
			} else if (colNames[k] == "agencyName") {
				html = html + "Agency Name" + "\t";
			} else if (colNames[k] == "certName") {
				html = html + "Certification Type" + "\t";
			} else if (colNames[k] == "effDate") {
				html = html + "Certified" + "\t";
			} else if (colNames[k] == "expDate") {
				html = html + "Expiration Date" + "\t";
			} else if (colNames[k] == "capablities") {
				html = html + "Capability" + "\t";
			} else if (colNames[k] == "naicsDesc") {
				html = html + "NAICS Description" + "\t";
			} else if (colNames[k] == "certificateShortName") {
				html = html + "Certificate Short Name" + "\t";
			} else if (colNames[k] == "count") {
				html = html + "Count" + "\t";
			} else if (colNames[k] == "diversityAnalysisPercent") {
				html = html + "Percent" + "\t";
			} else if (colNames[k] == "vendorStatus") {
				html = html + "Vendor Status" + "\t";
			} else if (colNames[k] == "statusCount") {
				html = html + "Count" + "\t";
			} else if (colNames[k] == "statusPercent") {
				html = html + "Percent" + "\t";
			} else if (colNames[k] == "percent") {
				html = html + "Percentage" + "\t";
			} else if (colNames[k] == "naicsCapabilities") {
				html = html + "NAICS Capabilities" + "\t";
			} else if (colNames[k] == "diverseClassification") {
				html = html + "Diverse Classification" + "\t";
			} else if (colNames[k] == "certificateAgencyDetails") {
				html = html + "Certificate Agency Details" + "\t";
			} else if (colNames[k] == "vendorCount") {
				html = html + "Vendor Count" + "\t";
			} else if (colNames[k] == "totalCount") {
				html = html + "Total Count" + "\t";
			} else if (colNames[k] == "emailDate") {
				html = html + "Email Date" + "\t";
			} else if (colNames[k] == "certificateNumber") {
				html = html + "Certificate Number" + "\t";
			} else if (colNames[k] == "createdDate") {
				html = html + "Created On" + "\t";
			} else if (colNames[k] == "businessTypeName") {
				html = html + "Business Type" + "\t";
			} else if (colNames[k] == "ownerName") {
				html = html + "Owner Name" + "\t";
			} else if (colNames[k] == "bpMarketSector") {
				html = html + "BP Market Sector" + "\t";
			} else if (colNames[k] == "isRenewed") {
				html = html + "Is Renewed" + "\t";
			} else if (colNames[k] == "website") {
				html = html + "Website"+"\t";
			} else if (colNames[k] == "bpMarketSubSector") {
				html = html + "BP Market SubSector"+"\t";
			} else if (colNames[k] == "contactFirstName") {
				html = html + "Contact First Name"+"\t";
			} else if (colNames[k] == "contactLastName") {
				html = html + "Contact Last Name"+"\t";
			} else if (colNames[k] == "contactPhone") {
				html = html + "Contact Phone"+"\t";
			} else if (colNames[k] == "contactEmail") {
				html = html + "Contact Email"+"\t";
			} else if (colNames[k] == "recentAnnaulRevenue") {
				html = html + "Annual Revenue"+"\t";
			} else if (colNames[k] == "spend") {
				html = html + "Spend"+"\t";
			}else if (colNames[k] == "bpCommodityGroup") {
				html = html + "BP Commodity Group"+"\t";
			}else if (colNames[k] == "sectorDescription") {
				html = html + "Sector Description"+"\t";
			}
		}
	}
	html = html + "\n"; // Output header with end of line
	var RE = new RegExp(/^\d*\.\d\d$/);
	for (i = 0; i < mya.length; i++) {
		data = $("#" + table).getRowData(mya[i]); // get each row
		for ( var j = 0; j < colNames.length; j++) {
			if ($.inArray(j, filtered) >= 0) {
		       if (colNames[j]=="directExp" ||colNames[j]=="indirectExp" || colNames[j]== "totalSales") {
		    	   html = html + "$" + data[colNames[j]].replace(/,/g,'') + "\t"; // output for money each Row as
					// tab delimited
		       } else {
		    	   html = html + data[colNames[j]].replace(/,/g,'') + "\t"; // output each Row as
					// tab delimited
		       }
			}
		}
		html = html + "\n"; // output each row with end of line
	}
	html = html + "\n"; // end of line at the end
	//alert(html);
	html = html.replace(/'/g, '&apos;');
	var form = "<form name='csvexportform' action='exportData.do?method=excelExport' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='" + fileName
			+ "'>";
	form = form + "<input type='hidden' name='csvBuffer' value='" + html + "'>";
	form = form + "</form><script>document.csvexportform.submit();</sc"
			+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();

}
/**
 * Export dynamic column based data in Excel format
 */
function dynamicExportExcel(table, filtered, fileName) {
	
	var mya = new Array();
	mya = $("#" + table).getDataIDs(); // Get All IDs
	var data = $("#" + table).getRowData(mya[0]); // Get First row to get the labels
	var html = "";
	var colNames = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			colNames.push(this.index);
		}
	});
	for ( var k = 0; k < colNames.length; k++) {		
		if ($.inArray(k, filtered) >= 0) {		
				html = html + colNames[k] + "\t";
		}	
	}
	html = html + "\n"; // Output header with end of line
	var RE = new RegExp(/^\d*\.\d\d$/);
	for (var i = 0; i < mya.length; i++) {
		data = $("#" + table).getRowData(mya[i]); // get each row
		for ( var j = 0; j < colNames.length; j++) {
			if ($.inArray(j, filtered) >= 0) {
		       if (colNames[j]=="directExp" ||colNames[j]=="indirectExp" || colNames[j]== "totalSales") {
		    	   html = html + "$" + data[colNames[j]] + "\t"; // output for money each Row as
					// tab delimited
		       } else {
		    	   html = html + data[colNames[j]] + "\t"; // output each Row as
					// tab delimited
		       }
			}
		}
		html = html + "\n"; // output each row with end of line
	}
	html = html + "\n"; // end of line at the end
	//alert(html);
	html = html.replace(/'/g, '&apos;');
	var form = "<form name='csvexportform' action='exportData.do?method=excelExport' method='post'>";
	form = form + "<input type='hidden' name='fileName' value='" + fileName
			+ "'>";
	form = form + "<input type='hidden' name='csvBuffer' value='" + html + "'>";
	form = form + "</form><script>document.csvexportform.submit();</sc"
			+ "ript>";
	OpenWindow = window.open('', '');
	OpenWindow.document.write(form);
	OpenWindow.document.close();
}

/**
 * Export data in PDF format
 */
function exportPDF(table, filtered, fileName, path) {

	var pathArray = window.location.href.split( '/' );
	var protocol = pathArray[0];
	var host = pathArray[2];
	var parameter = pathArray[3];
	var url = protocol + '//' + host + "/" + parameter;
	
	var colHeaderForDynamic = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() {
		if (!this.hidden) {
			colHeaderForDynamic.push(this.index);
		}
	});
	
	//alert(url);

	var mya = new Array();
	mya = $("#" + table).getDataIDs(); // Get All IDs
	var data = $("#" + table).getRowData(mya[0]); // Get First row to get the
	// labels
	var colNames = new Array();
	var ii = 0;
	
	for ( var i in data) {
		colNames[ii++] = i;		
	} // capture col names
	
	var pageHead = "";
	if (fileName == "SupplierReport") {
		pageHead = "Supplier Custom Search Report ";
	} else if (fileName == "TotalSalesReport") {
		pageHead = "Prime Supplier Report by Tier 2 Total Spend ";
	} else if (fileName == "DirectSpendReport") {
		pageHead = "Prime Supplier Report by Tier 2 Direct Spend ";
	} else if (fileName == "IndirectSpendReport") {
		pageHead = "Prime Supplier Report by Tier 2 Indirect Spend ";
	} else if (fileName == "PrimeSupplierbyTier2DiversityCategory") {
		pageHead = "Prime Supplier by Tier 2 Diversity Category";
	} else if (fileName == "Tier2ReportingSummary") {
		pageHead = "Tier 2 Reporting Summary";
	}else if (fileName == "SupplierReportWithCustomColumns") {
		pageHead = "Supplier Custom Search Report";
	}	
	var html = "<html><head>"
			+ "<style script=&quot;css/text&quot;>"
			+ "table.tableList_1 th {text-align:center; "
			+ "vertical-align: middle; padding:5px;}"
			+ "table.tableList_1 td {vertical-align: top; padding:5px;}"		
			+ "</style>"
			+ "</head>"
			+ "<body style=&quot;page:land;&quot;>"
			+ "<div class=&quot;logo&quot;>"
			+ "<img src=&quot;" 
			+ url
			+ "/temp/"
			+ path
			+ "&quot; style=&quot;float:left;"
			+ "position:absolute;top:0px;left:0px;&quot; "
			+ "height=&quot;60px&quot; width=&quot;60px&quot; alt=&quot;Logo&quot; />"
			+ "</div><br/>"
			+ "<div class=&quot;pageHead_1&quot; style=&quot;text-align:center;padding: 5%;font-family:Verdana;&quot;>"
			+ pageHead + "</div>";
	if (fileName == "SupplierReport") {
		html = html
				+ "<table align=&quot;center&quot; class=&quot;tableList_1 t_space&quot; style = &quot;border-collapse: collapse;font-family:Verdana;"
				+ "font-size:6px;table-layout:auto !important;width: auto !important;&quot;><tr>";
	} else {
		html = html
				+ "<table align=&quot;center&quot; class=&quot;tableList_1 t_space&quot; style = &quot;border-collapse: collapse; font-family:Verdana;"
				+ "font-size:10px;table-layout:auto !important;width: auto !important;&quot;><tr>";
	}
	//For Jasper Export Starts Here
	var colHeader="";	
	for ( var k = 0; k < colNames.length; k++) {
		
		if ($.inArray(k, filtered) >= 0) {		
			//For Jasper Export
			colHeader = colHeader + colNames[k] + ",";
			if (colNames[k] == "vendorName") {
				html = html + "<th>" + "Vendor Name" + "</th>";				
			} else if (colNames[k] == "spendYear") {
				html = html + "<th>" + "Spend Year" + "</th>";
			} else if (colNames[k] == "reportPeriod") {
				html = html + "<th>" + "Report Period" + "</th>";
			} else if (colNames[k] == "directExp") {
				html = html + "<th>" + "Direct Spend" + "</th>";
			} else if (colNames[k] == "indirectExp") {
				html = html + "<th>" + "Indirect Spend" + "</th>";
			} else if (colNames[k] == "totalSales") {
				html = html + "<th>" + "Total Spend Reported" + "</th>";
			} else if (colNames[k] == "createdOn") {
				html = html + "<th>" + "Date Submitted" + "</th>";
			} else if (colNames[k] == "vendorUserName") {
				html = html + "<th>" + "Tier2 Vendor Name" + "</th>";
			} else if (colNames[k] == "certificateName") {
				html = html + "<th>" + "Certificate Name" + "</th>";
			} else if (colNames[k] == "diversityPercent") {
				html = html + "<th>" + "% Diversity Spend" + "</th>";
			} else if (colNames[k] == "ethnicity") {
				html = html + "<th>" + "Ethnicity" + "</th>";
			} else if (colNames[k] == "firstName") {
				html = html + "<th>" + "Owner First" + "</th>";
			} else if (colNames[k] == "lastName") {
				html = html + "<th>" + "Owner Last" + "</th>";
			} else if (colNames[k] == "address") {
				html = html + "<th>" + "Mailing Address" + "</th>";
			} else if (colNames[k] == "city") {
				html = html + "<th>" + "City" + "</th>";
			} else if (colNames[k] == "state") {
				html = html + "<th>" + "State" + "</th>";
			} else if (colNames[k] == "zipcode") {
				html = html + "<th>" + "Zip" + "</th>";
			} else if (colNames[k] == "phone") {
				html = html + "<th>" + "Phone" + "</th>";
			} else if (colNames[k] == "fax") {
				html = html + "<th>" + "Fax" + "</th>";
			} else if (colNames[k] == "emailId") {
				html = html + "<th>" + "Email" + "</th>";
			} else if (colNames[k] == "agencyName") {
				html = html + "<th>" + "Agency" + "</th>";
			} else if (colNames[k] == "certName") {
				html = html + "<th>" + "Certification Type" + "</th>";
			} else if (colNames[k] == "effDate") {
				html = html + "<th>" + "Certified" + "</th>";
			} else if (colNames[k] == "expDate") {
				html = html + "<th>" + "Expiration" + "</th>";
			} else if (colNames[k] == "capablities") {
				html = html + "<th>" + "Capability" + "</th>";
			} else if (colNames[k] == "naicsDesc") {
				html = html + "<th>" + "Category" + "</th>";
			} else if (colNames[k] == "naicsCapabilities") {
				html = html + "<th>" + "NAICS Capabilities" + "</th>";
			} else if (colNames[k] == "diverseClassification") {
				html = html + "<th>" + "Diverse Classification" + "</th>";
			} else if (colNames[k] == "certificateAgencyDetails") {
				html = html + "<th>" + "Certificate Agency Details" + "</th>";
			} else if (colNames[k] == "ownerName") {
				html = html + "<th>" + "Owner Name" + "</th>";
			}
		}
	}
	//For Jasper Export Ends Here
	
	html = html + "</tr>"; // Output header with end of line
	var RE = new RegExp(/^\d*\.\d\d$/);
	var sumIndirectSpend=parseFloat("0");
	for (i = 0; i < mya.length; i++) {
		html = html + "<tr>";
		data = $("#" + table).getRowData(mya[i]); // get each row
		for ( var j = 0; j < colNames.length; j++) {
			
			if ($.inArray(j, filtered) >= 0) {
				if (RE.test(data[colNames[j]])) {
				   html = html + "<td align=&quot;right&quot;>" + "$" + data[colNames[j]] + "</td>"; // output for money each Row as
					// tab delimited
				   //Total Indirect Spend only for Tier 2 Reporting Summary
				   if (fileName == "Tier2ReportingSummary" && colNames.length == 3) {			   
					   sumIndirectSpend=parseFloat(+sumIndirectSpend + +data[colNames[j]]);					   
				   }
				} else {
				   html = html + "<td>" + data[colNames[j]] + "</td>"; // output each Row as
					// tab delimited				   
				}
			}
		}		
		html = html + "</tr>"; // output each row with end of line
	}
	
	if (fileName == "SupplierReport") 
	{		
		window.location = "searchReport.do?method=getSearchVendorReport&colHeader="+colHeader;
	}
	else if(fileName == "SupplierReportWithCustomColumns") 
	{
		window.location = "searchReport.do?method=getCustomSearchVendorReport&colHeader="+colHeaderForDynamic;
	}
	else
	{
		//Total Indirect Spend only for Tier 2 Reporting Summary
		if (fileName == "Tier2ReportingSummary")
		{
			html = html + "<tr><td colspan=&quot;2&quot; align=&quot;right&quot;>Total Amount </td>";
			html = html + "<td align=&quot;right&quot;>" + "$" + sumIndirectSpend.toFixed(2) + "</td></tr>";
		}	
		html = html + "</table></body></html>"; // end of line at the end
		//alert(html);
		html = html.replace(/'/g, '&apos;');
		var form = "<form name='pdfexportform' action='generategrid' method='post'>";
		form = form + "<input type='hidden' name='fileName' value='" + fileName
				+ "'>";
		form = form + "<input type='hidden' name='imagePath' value='" + path + "'>";
		form = form + "<input type='hidden' name='pdfBuffer' value='" + html + "'>";
		form = form + "</form><script>document.pdfexportform.submit();</sc"
				+ "ript>";
		OpenWindow = window.open('', '');
		OpenWindow.document.write(form);
		OpenWindow.document.close();
	}	
}

/*
 * This is functionality for populate grid in supplier state dashboard report.
 */
function getSupplierStateReport() {

	$.ajax({
		url : "reportdashboard.do?method=getSupplierStateChartData",
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable2").jqGrid('GridUnload');
			gridSupplierStateData(data);
		}
	});

}

function gridSupplierStateData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable2').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Records', 'Count', '%' ],
				colModel : [ {
					name : 'state',
					index : 'state',
					sorttype: 'string',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					sorttype: 'number',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center',
					formatter: 'number', 
					formatoptions: {suffix: '%'},
					sorttype: 'number'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager2',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalcount = $("#gridtable2").jqGrid('getCol', 'count',
							false, 'sum');
					$("#gridtable2").jqGrid('footerData', 'set', {
						state : 'Total Records',
						count : totalcount
					});
				}
			}).jqGrid('navGrid', '#pager2', {
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind(
			'resize',
			function() {
				$("#gridtable2").setGridWidth(
						$('#grid_container3').width() - 30, true);
			}).trigger('resize');
}

/*
 * This is functionality for populate grid in agency dashboard report.
 */
function getAgencyReport() {

	$.ajax({
		url : "reportdashboard.do?method=getAgencyChartData",
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable3").jqGrid('GridUnload');
			gridAgencyData(data);
		}
	});

}

function gridAgencyData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable3').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Records', 'Spend', '%' ],
				colModel : [ {
					name : 'Agency',
					index : 'Agency',
					sorttype: 'string',
					align : 'center'
				}, {
					name : 'Spend',
					index : 'Spend',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center',
					formatter: 'number', 
					formatoptions: {suffix: '%'},
					sorttype: 'number'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager3',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalcount = $("#gridtable3").jqGrid('getCol', 'Spend',
							false, 'sum');
					$("#gridtable3").jqGrid('footerData', 'set', {
						Agency : 'Total Spend',
						Spend : totalcount
					});
				}
			}).jqGrid('navGrid', '#pager3', {
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize',function() {
		$("#gridtable3").setGridWidth($('#grid_container4').width() - 30, true);
	}).trigger('resize');

}

/*
 * This is functionality for populate grid in vendor status dashboard report.
 */
function getVendorStatusReport() {

	$.ajax({
		url : "reportdashboard.do?method=getVendorStatusChartData",
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable9").jqGrid('GridUnload');
			gridStatusDashboardData(data);
		}
	});

}

function gridStatusDashboardData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable9').jqGrid({

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Status', 'Count', '%' ],
				colModel : [ {
					name : 'status',
					index : 'status',
					sorttype: 'string',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					sorttype: 'number',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center',
					formatter: 'number', 
					formatoptions: {suffix: '%'},
					sorttype: 'number'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager9',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalcount = $("#gridtable9").jqGrid('getCol', 'count',
							false, 'sum');
					$("#gridtable9").jqGrid('footerData', 'set', {
						status : 'Total Records',
						count : totalcount
					});
				}
			}).jqGrid('navGrid', '#pager9', {
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize',function() {$("#gridtable9").setGridWidth(
			$('#grid_container10').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in BP Vendor Count state dashboard report.
 */
function getBPVendorStateReport() {

	$.ajax({
		url : "reportdashboard.do?method=getBPVendorStateChartData",
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable11").jqGrid('GridUnload');
			gridBPVendorStateData(data);
		}
	});

}

function gridBPVendorStateData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable11').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Records', 'Certificate Name',
						'Certificate Shortname', 'Count', '%' ],
				colModel : [ {
					name : 'state',
					index : 'state',
					sorttype: 'string',
					align : 'center'
				}, {
					name : 'certificateName',
					index : 'certificateName',
					align : 'center',
					sorttype: 'string',
					cellattr : function(rowId, tv, rawObject, cm, rdata) {
						return 'style="white-space: normal;"'
					}
				}, {
					name : 'certShortname',
					index : 'certShortname',
					sorttype: 'string',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					sorttype: 'number',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center',
					formatter: 'number', 
					formatoptions: {suffix: '%'},
					sorttype: 'number'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager11',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalcount = $("#gridtable11").jqGrid('getCol', 'count',
							false, 'sum');
					$("#gridtable11").jqGrid('footerData', 'set', {
						state : 'Total Records',
						count : totalcount
					});
				}
			}).jqGrid('navGrid', '#pager11', {
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind(
			'resize',
			function() {
				$("#gridtable11").setGridWidth(
						$('#grid_container11').width() - 30, true);
			}).trigger('resize');
}

/*
 * This is Functionality for Populate Grid in Diversity Analysis Report.
 */
function getDiversityAnalysisReport() {
	$.ajax(
	{
		url : "diversityAnalysisReport.do?method=getDiversityAnalysisReport&random=" + Math.random(),
		type : "POST",
		async : true,
		success : function(data) 
		{			
			$("#gridtable").jqGrid('GridUnload');			
			gridDiversityAnalysisReportData(data);
		}
	});
}

function gridDiversityAnalysisReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Certificate Name', 'Certificate Short Name', 'Count', '%'],
		colModel : [
	            {
					name : 'certificateName',
					index : 'certificateName',
					align : 'center'
				}, {
					name : 'certificateShortName',
					index : 'certificateShortName',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					align : 'center',
					formatoptions : {
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityAnalysisPercent',
					index : 'diversityAnalysisPercent',
					align : 'center'
				}],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,
		
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		
        gridComplete : function() 
        {
			var totalcount = $("#gridtable").jqGrid('getCol', 'count', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', {
				certificateName : 'Total Records',
				formatoptions : {
					thousandsSeparator : ',',
					decimalPlaces: 0
				},
				count : totalcount
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');	
}

/**
 * Function to Export the Diversity Analysis Report Data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportDiversityAnalysisReportData(table, path) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "certificateName") {
			index.push(0);
			valueForCsv.push("Certificate Name");
		} else if (cols[i] == "certificateShortName") {
			index.push(1);
			valueForCsv.push("Certificate Short Name");
		} else if (cols[i] == "count") {
			index.push(2);
			valueForCsv.push("Count");
		} else if (cols[i] == "diversityAnalysisPercent") {
			index.push(3);
			valueForCsv.push("Percent");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'DiversityAnalysisReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "diversityAnalysisReport.do?method=getDiversityAnalysisReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}

/*
 * This is Functionality for Populate Grid in Vendor Status Breakdown Report.
 */
function getVendorStatusBreakdownReport() {	
	$.ajax(
	{
		url : "vendorStatusBreakdownReport.do?method=getVendorStatusBreakdownReport&random=" + Math.random(),
		type : "POST",
		async : true,
		success : function(data) 
		{			
			$("#gridtable").jqGrid('GridUnload');			
			gridVendorStatusBreakdownReportData(data);
		}
	});
}

function gridVendorStatusBreakdownReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Vendor Status', 'Count', '%'],
		colModel : [
	            {
					name : 'vendorStatus',
					index : 'vendorStatus',
					align : 'center'
				}, {
					name : 'statusCount',
					index : 'statusCount',
					align : 'center',
					formatoptions : {						
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
				}, {
					name : 'statusPercent',
					index : 'statusPercent',
					align : 'center'
				}],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,
		
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		
        gridComplete : function() 
        {
			var totalcount = $("#gridtable").jqGrid('getCol', 'statusCount', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', {
				vendorStatus : 'Total Records',
				formatoptions : {						
					thousandsSeparator : ',',
					decimalPlaces: 0
				},
				statusCount : totalcount
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');	
}

/**
 * Function to Export the Vendor Status Breakdown Report Data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportVendorStatusBreakdownReportData(table, path) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorStatus") {
			index.push(0);
			valueForCsv.push("Vendor Status");
		} else if (cols[i] == "statusCount") {
			index.push(1);
			valueForCsv.push("Count");
		} else if (cols[i] == "statusPercent") {
			index.push(2);
			valueForCsv.push("Percent");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'VendorStatusBreakdownReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "vendorStatusBreakdownReport.do?method=getVendorStatusBreakdownReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}

/*
 * This is Functionality for Populate Grid in Supplier Count By State Report.
 */
function getSupplierCountByStateReport() {	
	$.ajax(
	{
		url : "supplierCountByStateReport.do?method=getSupplierCountByStateReport&random=" + Math.random(),
		type : "POST",
		async : true,
		success : function(data) 
		{			
			$("#gridtable").jqGrid('GridUnload');			
			gridSupplierCountByStateReportData(data);
		}
	});
}

function gridSupplierCountByStateReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'State', 'Count', '%'],
		colModel : [
	            {
					name : 'state',
					index : 'state',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center'
				}],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,
		
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		
        gridComplete : function() 
        {
			var totalcount = $("#gridtable").jqGrid('getCol', 'count', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', {
				state : 'Total Records',
				count : totalcount
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');	
}

/**
 * Function to Export the Supplier Count By State Report Data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportSupplierCountByStateReportData(table, path) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "state") {
			index.push(0);
			valueForCsv.push("State");
		} else if (cols[i] == "count") {
			index.push(1);
			valueForCsv.push("Count");
		} else if (cols[i] == "percent") {
			index.push(2);
			valueForCsv.push("Percent");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'SupplierCountByStateReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "supplierCountByStateReport.do?method=getSupplierCountByStateReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}

/*
 * This is Functionality for Populate Grid in BP Vendor Count By State Report.
 */
function getBPVendorCountByStateReport() {	
	$.ajax(
	{
		url : "bpVendorCountByStateReport.do?method=getBPVendorCountByStateReport&random=" + Math.random(),
		type : "POST",
		async : true,
		success : function(data) 
		{			
			$("#gridtable").jqGrid('GridUnload');			
			gridBPVendorCountByStateReportData(data);
		}
	});
}

function gridBPVendorCountByStateReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'State', 'Certificate Name', 'Certificate Short Name', 'Count', '%'],
		colModel : [
	            {
					name : 'state',
					index : 'state',
					align : 'center'
				}, {
					name : 'certificateName',
					index : 'certificateName',
					align : 'center'
				}, {
					name : 'certificateShortName',
					index : 'certificateShortName',
					align : 'center'
				}, {
					name : 'count',
					index : 'count',
					align : 'center'
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center'
				}],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,
		
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		
        gridComplete : function() 
        {
			var totalcount = $("#gridtable").jqGrid('getCol', 'count', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', {
				state : 'Total Records',
				count : totalcount
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');	
}

/**
 * Function to Export the BP Vendor Count By State Report Data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportBPVendorCountByStateReportData(table, path) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "state") {
			index.push(0);
			valueForCsv.push("State");
		} else if (cols[i] == "certificateName") {
			index.push(1);
			valueForCsv.push("Certificate Name");
		} else if (cols[i] == "certificateShortName") {
			index.push(2);
			valueForCsv.push("Certificate Short Name");
		} else if (cols[i] == "count") {
			index.push(3);
			valueForCsv.push("Count");
		} else if (cols[i] == "percent") {
			index.push(4);
			valueForCsv.push("Percent");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'BPVendorCountByStateReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "bpVendorCountByStateReport.do?method=getBPVendorCountByStateReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}

/*
 * This is Functionality for Populate Grid in Spend By Agency Report.
 */
function getSpendByAgencyReport() {	
	$.ajax(
	{
		url : "spendByAgencyReport.do?method=getSpendByAgencyReport&random=" + Math.random(),
		type : "POST",
		async : true,
		success : function(data) 
		{			
			$("#gridtable").jqGrid('GridUnload');			
			gridSpendByAgencyReportData(data);
		}
	});
}

function gridSpendByAgencyReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Agency Name', 'Short Name', 'Spend', '%'],
		colModel : [
	            {
					name : 'agencyName',
					index : 'agencyName',
					align : 'center'
				}, {
					name : 'certificateShortName',
					index : 'certificateShortName',
					align : 'center'
				}, {
					name : 'spend',
					index : 'spend',
					align : 'right',
					sorttype : 'number',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'percent',
					index : 'percent',
					align : 'center'
				}],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 250,
		
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000,
		
        gridComplete : function() 
        {
			var totalcount = $("#gridtable").jqGrid('getCol', 'spend', false, 'sum');
			$("#gridtable").jqGrid('footerData', 'set', {
				agencyName : 'Total Spend',
				count : totalcount
			});
		}
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');	
}

/**
 * Function to Export the Spend By Agency Report Data.
 * 
 * @param table
 * @returns {Boolean}
 */
function exportSpendByAgencyReportData(table, path) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "agencyName") {
			index.push(0);
			valueForCsv.push("Agency Name");
		} else if (cols[i] == "certificateShortName") {
			index.push(1);
			valueForCsv.push("Certificate Short Name");
		} else if (cols[i] == "spend") {
			index.push(2);
			valueForCsv.push("Spend");
		} else if (cols[i] == "percent") {
			index.push(3);
			valueForCsv.push("Percent");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'SpendByAgencyReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "spendByAgencyReport.do?method=getSpendByAgencyReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}

/*
 * This is Functionality for Populate Grid in Vendor Commodities Not Saved Report.
 */
function getVendorCommoditiesNotSavedReport() {	
	$.ajax(
	{
		url : "vendorCommoditiesNotSavedReport.do?method=getVendorCommoditiesNotSavedReport&random=" + Math.random(),
		type : "POST",
		async : true,
		success : function(data) 
		{			
			$("#gridtable").jqGrid('GridUnload');			
			gridVendorCommoditiesNotSavedReportData(data);
		}
	});
}

function gridVendorCommoditiesNotSavedReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);	
	
	$('#gridtable').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Vendor Name', 'First Name', 'Last Name', 'Email Id', 'Phone No',
		             'City', 'State', 'Vendor Status', 'Vendor Status Label'],
		colModel : [
	            {
					name : 'vendorName',
					index : 'vendorName'
				}, {
					name : 'firstName',
					index : 'firstName'
				}, {
					name : 'lastName',
					index : 'lastName'
				}, {
					name : 'emailId',
					index : 'emailId'
				}, {
					name : 'phone',
					index : 'phone'
				}, {
					name : 'city',
					index : 'city'
				}, {
					name : 'state',
					index : 'state'
				}, {
					name : 'vendorStatus',
					index : 'vendorStatus'
				}, {
					name : 'vendorStatusLabel',
					index : 'vendorStatusLabel',
					hidden : true
				}],
		/*rowNum : 10,
		rowList : [ 10, 20, 50 ],*/
		pager : '#pager',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No Data Available...',
		grouping : true,
		groupingView : {
			groupField : [ 'vendorStatusLabel' ],
			groupColumnShow : [ false ],
			groupText : [ '<b>{0}</b>' ],
			groupCollapse : true,
			groupOrder : [ 'asc' ],
			groupSummary : [ true ],
			showSummaryOnHide : true,
			groupDataSorted : true
		},
		footerrow : false,
		userDataOnFooter : true,
		cmTemplate: {title: false},
		height : 400,		
		gridview: true,
        pgbuttons:false,
        pgtext:'',
        width: ($.browser.webkit?466:497),
        loadonce: true,
        rowNum: 2000
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{}).navButtonAdd('#pager', { // Add Custom Button to Export the Data to Excel, CSV & Pdf
		caption : "Export",
		buttonicon : "ui-icon ui-icon-newwin",
		onClickButton : function() 
		{
			exportGrid();
		},
		position : "last"
	});

	$(window).bind('resize', function() 
	{
		$("#gridtable").setGridWidth($('#grid_container').width() - 30, true);
	}).trigger('resize');	
}

/**
 * Function to Export the Vendor Commodities Not Saved Report Data.
 */
function exportVendorCommoditiesNotSavedReportData(table, path) 
{	
	var exportType = $("input[name=export]:checked").val();
	var cols = [];
	$.each($('#' + table).jqGrid('getGridParam', 'colModel'), function() 
	{
		if (!this.hidden) 
		{
			cols.push(this.index);
		}
	});

	var index = [];
	var valueForCsv = [];
	for ( var i = 0; i <= cols.length; i++) {
		if (cols[i] == "vendorName") {
			index.push(0);
			valueForCsv.push("Vendor Name");
		} else if (cols[i] == "firstName") {
			index.push(1);
			valueForCsv.push("First Name");
		} else if (cols[i] == "lastName") {
			index.push(2);
			valueForCsv.push("Last Name");
		} else if (cols[i] == "emailId") {
			index.push(3);
			valueForCsv.push("Email Id");
		} else if (cols[i] == "phone") {
			index.push(4);
			valueForCsv.push("Phone No");
		} else if (cols[i] == "city") {
			index.push(5);
			valueForCsv.push("City");
		} else if (cols[i] == "state") {
			index.push(6);
			valueForCsv.push("State");
		} else if (cols[i] == "vendorStatus") {
			index.push(7);
			valueForCsv.push("Vendor Status");
		}
	}

	if (exportType == 1) 
	{
		exportExcel(table, index, 'VendorCommoditiesNotSavedReport');
	} 
	else if (exportType == 2) 
	{
		$('#' + table).table2CSV({
			header : valueForCsv,
			fileName : fileName
		});
	} 
	else if (exportType == 3) 
	{		
		window.location = "vendorCommoditiesNotSavedReport.do?method=getVendorCommoditiesNotSavedReportPdf";
	} 
	else 
	{
		alert("Please Choose a Type to Export");
		return false;
	}

	$('#dialog').dialog('close');
}