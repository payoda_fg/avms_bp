var naicsCode = '';
var rowLevel = '';
var des;
var press = 0;
function pickNaicsCode() {
	var wWidth = $(window).width();
	var dWidth = wWidth * 0.7;
	var wHeight = $(window).height();
	var dHeight = wHeight * 0.5;
	jQuery("#addtree").jqGrid({
		url : 'naicscode.do?method=listNaicsCode' + '&random=' + Math.random(),
		treedatatype : "xml",
		treeGridModel : 'adjacency',
		mtype : "POST",
		colNames : [ "id", "NAICS Description", "NAICS Code", "Parent" ],
		colModel : [ {
			name : 'id',
			index : 'id',
			/* width : 1, */
			hidden : true,
			key : true,
			editable : false
		}, {
			name : 'NAICSDESCRIPTION',
			index : 'NAICSDESCRIPTION',
			/* width : 600, */
			align : "left",
			editable : false,

		}, {
			name : 'NAICSCODE',
			index : 'NAICSCODE',
			/* width : 120, */
			editable : false,

		}, {
			name : 'parentID',
			index : 'parentID',
			/* width : 1, */
			hidden : true,
			editable : true,
		} ],
		height : "auto",
		pager : "#paddtree",
		
		/* expanded : true, */
		loaded : true,
		treeGrid : true,
		ExpandColumn : 'NAICSDESCRIPTION',
		hidegrid : false,
		/* autowidth: true, */
		width : dWidth,
		cmTemplate : {
			title : false
		},
		onSelectRow : function(id) {
			var rowData = jQuery(this).getRowData(id);
			naicsCode = rowData['NAICSCODE'];
			rowLevel = rowData.level;
			des = rowData['NAICSDESCRIPTION'];
		}

	});
	jQuery("#addtree").jqGrid('navGrid', "#paddtree", {
		add : false,
		edit : false,
		del : false,
		search : false
	});
}

function pickGeographicalRegion() {
	
	$.ajax({
		url : 'geographicalsettings.do?method=listGeographicalRegion' + '&random='
				+ Math.random() ,	
		type : "POST",
		async : false,
		success : function(data) {
			$("#list").jqGrid('GridUnload');
			fillGeographyGrid(data);
		}
	});
}

function fillGeographyGrid(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);
	var wWidth = $(window).width();
	var dWidth = wWidth * 0.7;
	var wHeight = $(window).height();
	var dHeight = wHeight * 0.5;

	$('#list').jqGrid({

		data : newdata1,
		datatype : 'local',
		colNames : [ 'Region Name', 'State Name' ],
		colModel : [ {
			name : 'region',
			index : 'region',
			align : 'center'
		}, {
			name : 'state',
			index : 'state',
			align : 'center'
		} ],
		rowNum : 10,
		rowList : [ 10, 20, 50 ],
		pager : '#pager',
		height : "230",
		width : dWidth,
		multiselect : true,
		viewrecords : true,
		emptyrecords : 'No data available..',
		cmTemplate : {
			title : false
		},
		height : 'auto'
	}).jqGrid('navGrid', '#pager', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

}

function pickCommodityCode() {
	var selectedIds = [];
	$.ajax({
		url : 'commoditycategory.do?method=listCommodity' + '&random='
				+ Math.random(),
		type : "POST",
		async : false,
		success : function(data) {
			$("#list2").jqGrid('GridUnload');
			selectedIds = fillCommodityGrid(data);
		}
	});
	
	return selectedIds;
}

function pickCommodityCode(searchCommodityText) 
{	
	var selectedIds = [];
	$.ajax(
	{
		url : 'commoditycategory.do?method=listOfCommoditiesBasedOnSearchCommodityText&searchCommodityText=' + searchCommodityText + '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) 
		{			
			$("#list2").jqGrid('GridUnload');
			selectedIds = fillCommodityGrid(data);
		}
	});	
	return selectedIds;
}

function fillCommodityGrid(myGridData) {

	var newdata = jQuery.parseJSON(myGridData);
	var wWidth = $(window).width();
	var dWidth = wWidth * 0.7;
	var wHeight = $(window).height();
	var dHeight = wHeight * 0.5, idsOfSelectedRows = [];
	
	jQuery("#list2").jqGrid({
		data : newdata,
		datatype : "local",
		colNames : [ "id", "BP Commodity Code","BP Market Sector", "BP Market Subsector", "BP Commodity Description" ],
		colModel : [
				{
					name : 'id',
					index : 'id',
					hidden : true,
					editable : true
				},
				{
					name : 'commodityCode1',
					index : 'commodityCode1',
					align : "center",
					sorttype:'integer',
					hidden : true,
					//search : false,
					//searchoptions:{sopt:['cn','eq','ne','le','lt','gt','ge']}
					/*editable : true,
					editrules : {
						required : true
					}*/
				},
				{
					name : "sectorDesc",
					index : "sectorDesc",
					align : "center",
					sorttype : "string",
					//search : false,
					//searchoptions : {sopt : ['cn', 'eq', 'bw', 'nc', 'ew', 'en']},
				},
				{
					name : 'commodityCategoryId',
					index : 'commodityCategoryId',
					align : "center",
					sorttype:'string',
					//search : false,
					//searchoptions:{sopt:['cn','eq','bw','bn','nc','ew','en']}
					// hidden : true,
					/*editable : true,
					edittype : "select",
					editoptions : {
						dataUrl : 'commoditycategory.do?method=parentCategory'
								+ '&random=' + Math.random()
					},
					editrules : {
						edithidden : true,
						required : true
					// custom : true
					}*/
				},
				{
					name : 'commodityDesc',
					index : 'commodityDesc',
					align : "center",
					sorttype:'string',
					//search : false,
					//searchoptions:{sopt:['cn','eq','bw','bn','nc','ew','en']}
					/*editable : true,
					editrules : {
						required : true
					}*/
				}],
		pager : "#pager2",
		rowNum : 10,
		rowList : [ 10, 20, 50 ],
		height : "230",
		ignoreCase:true,
		width : dWidth,
		beforeSelectRow: function(id, e) {
			if(id != 0)
			{
				return true;
			}
			else
			{
				alert("This record does not have Commodity Description.");
				return false;
			}
		},
		onSelectRow: function (id, isSelected) {
            var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
            item.cb = isSelected;
            if (!isSelected && i >= 0) {
                idsOfSelectedRows.splice(i,1); // remove id from the list
            } else if (i < 0) {
                idsOfSelectedRows.push(id);
            }
        },
        loadComplete: function () {
            var p = this.p, /*idsOfSelectedRows = p.idsOfSelectedRows,*/ data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
            for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
                rowid = idsOfSelectedRows[i];
                item = data[index[rowid]];
                if ('cb' in item && item.cb) {
                    $this.jqGrid('setSelection', rowid, false);
                }
            }
            $("#dialog2").dialog('option','position','center');
        },
		multiselect : true,
		cmTemplate : {
			title : false
		}
	});
	
	/*jQuery("#list2").jqGrid('filterToolbar',{
		stringResult : true,
		searchOperators : true, 
		searchOnEnter:false
	});*/
	
	$grid = $("#list2"),
	$("#cb_" + $grid[0].id).hide();
    $("#jqgh_" + $grid[0].id + "_cb").addClass("ui-jqgrid-sortable");
    cbColModel = $grid.jqGrid('getColProp', 'cb');
    cbColModel.sortable = true;
    cbColModel.sorttype = function (value, item) {
        return typeof (item.cb) === "boolean" && item.cb ? 1 : 0;
    };
    
    return idsOfSelectedRows;
	/*jQuery("#list2").jqGrid('navGrid', "#pager2", {
		add : false,
		edit : false,
		del : false,
		refreah : false,
		search : false
	}, {}, {}, {}, {});*/
}



function pickTier2Vendors(searchVendorsText) 
{	
	var selectedIds = [];
	$.ajax(
	{
		url : 'tier2Report.do?method=listOfTier2Vendors&searchVendorsText=' + searchVendorsText + '&random=' + Math.random(),
		type : "POST",
		async : false,
		success : function(data) 
		{			
			$("#list2").jqGrid('GridUnload');
			selectedIds = fillVendorsGrid(data);
		}
	});	
	return selectedIds;
}


function fillVendorsGrid(myGridData) {

	var newdata = jQuery.parseJSON(myGridData);
	var wWidth = $(window).width();
	var dWidth = wWidth * 0.7;
	var wHeight = $(window).height();
	var dHeight = wHeight * 0.5, idsOfSelectedRows = [];
	
	jQuery("#list2").jqGrid({
		data : newdata,
		datatype : "local",
		colNames : [ "id", "Vendord Name","Vendor Address", "Vendor City", "Vendor State" ],
		colModel : [
				{
					name : 'id',
					index : 'id',
					hidden : true,
					editable : true
				},
				{
					name : 'vendorName',
					index : 'vendorName',
					align : "center",
					sorttype:'string',
//					hidden : true,
					//search : false,
					//searchoptions:{sopt:['cn','eq','ne','le','lt','gt','ge']}
					/*editable : true,
					editrules : {
						required : true
					}*/
				},
				{
					name : "vendorAddress",
					index : "vendorAddress",
					align : "center",
					sorttype : "string",
					//search : false,
					//searchoptions : {sopt : ['cn', 'eq', 'bw', 'nc', 'ew', 'en']},
				},
				{
					name : 'vendorCity',
					index : 'vendorCity',
					align : "center",
					sorttype:'string',
					//search : false,
					//searchoptions:{sopt:['cn','eq','bw','bn','nc','ew','en']}
					// hidden : true,
					/*editable : true,
					edittype : "select",
					editoptions : {
						dataUrl : 'commoditycategory.do?method=parentCategory'
								+ '&random=' + Math.random()
					},
					editrules : {
						edithidden : true,
						required : true
					// custom : true
					}*/
				},
				{
					name : 'vendorState',
					index : 'vendorState',
					align : "center",
					sorttype:'string',
					//search : false,
					//searchoptions:{sopt:['cn','eq','bw','bn','nc','ew','en']}
					/*editable : true,
					editrules : {
						required : true
					}*/
				}],
		pager : "#pager2",
		rowNum : 10,
		rowList : [ 10, 20, 50 ],
		height : "230",
		ignoreCase:true,
		width : dWidth,
		beforeSelectRow: function(id, e) {
			if(id != 0)
			{
				return true;
			}
			else
			{
				alert("This record does not have Commodity Description.");
				return false;
			}
		},
		onSelectRow: function (id, isSelected) {
            var p = this.p, item = p.data[p._index[id]], i = $.inArray(id, idsOfSelectedRows);
            item.cb = isSelected;
            if (!isSelected && i >= 0) {
                idsOfSelectedRows.splice(i,1); // remove id from the list
            } else if (i < 0) {
                idsOfSelectedRows.push(id);
            }
        },
        loadComplete: function () {
            var p = this.p, /*idsOfSelectedRows = p.idsOfSelectedRows,*/ data = p.data, item, $this = $(this), index = p._index, rowid, i, selCount;
            for (i = 0, selCount = idsOfSelectedRows.length; i < selCount; i++) {
                rowid = idsOfSelectedRows[i];
                item = data[index[rowid]];
                if ('cb' in item && item.cb) {
                    $this.jqGrid('setSelection', rowid, false);
                }
            }
            $("#dialog3").dialog('option','position','center');
        },
		multiselect : false,
		cmTemplate : {
			title : false
		}
	});
	
	/*jQuery("#list2").jqGrid('filterToolbar',{
		stringResult : true,
		searchOperators : true, 
		searchOnEnter:false
	});*/
	
	$grid = $("#list2"),
	$("#cb_" + $grid[0].id).hide();
    $("#jqgh_" + $grid[0].id + "_cb").addClass("ui-jqgrid-sortable");
    cbColModel = $grid.jqGrid('getColProp', 'cb');
    cbColModel.sortable = true;
    cbColModel.sorttype = function (value, item) {
        return typeof (item.cb) === "boolean" && item.cb ? 1 : 0;
    };
    
    return idsOfSelectedRows;
	/*jQuery("#list2").jqGrid('navGrid', "#pager2", {
		add : false,
		edit : false,
		del : false,
		refreah : false,
		search : false
	}, {}, {}, {}, {});*/
}