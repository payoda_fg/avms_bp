/**
 *  This page used for jquery functions
 */

/**
 * Function toggling divs
 */
$(function() {
	$('div#table-holder').show();
	$('#show').toggle(function() {
		$('div#table-holder').slideUp("slow");
		$('#show1').attr("src", "images/downarrow1.png");
	}, function() {
		$('div#table-holder').slideDown("slow");
		$('#show1').attr("src", "images/arrow-down.png");
	});
});

$(function() {
	$('div#table-holder2').show();
	$('#show2').toggle(function() {
		$('div#table-holder2').slideUp("slow");
		$('#showx').attr("src", "images/downarrow1.png");
	}, function() {
		$('div#table-holder2').slideDown("slow");
		$('#showx').attr("src", "images/arrow-down.png");
	});
});

$(function() {
	$('div#table-holder3').show();
	$('#show3').toggle(function() {
		$('div#table-holder3').slideUp("slow");
		$('#showy').attr("src", "images/downarrow1.png");
	}, function() {
		$('div#table-holder3').slideDown("slow");
		$('#showy').attr("src", "images/arrow-down.png");
	});
});

$(function() {
	$('div#table-holder4').show();
	$('#show4').toggle(function() {
		$('div#table-holder4').slideUp("slow");
		$('#showz').attr("src", "images/downarrow1.png");
	}, function() {
		$('div#table-holder4').slideDown("slow");
		$('#showz').attr("src", "images/arrow-down.png");
	});
});

$(function() {
	$('div#table-holder5').show();
	$('#show5').toggle(function() {
		$('div#table-holder5').slideUp("slow");
		$('#showi').attr("src", "images/downarrow1.png");
	}, function() {
		$('div#table-holder5').slideDown("slow");
		$('#showi').attr("src", "images/arrow-down.png");
	});
});

/**
 * Function for clear the text boxes and added textbox label.
 */
$(function() {
	$('#submit').click(function() {

		$("#ajaxloader").css('display', 'block');
		$("#ajaxloader").show();

		$('input[type="text"]').each(function() {
			if ($(this).attr('value') == 'Optional') {
				this.value = '';
			}
		});

	});

	$('#save').click(function() {

		// var username = $("#userName").val();
		// var password = $("#passwordTemp").val();

		$("#ajaxloader").css('display', 'block');
		$("#ajaxloader").show();

		$('input[type="text"]').each(function() {
			if ($(this).attr('value') == 'Optional') {
				this.value = '';
			}
		});
		// if(username != '' && password != ''){
		// if(!validateEmail(username)){
		// alert('Please Enter Valid EmailId.');
		// return false;
		// }else{
		// $("#UnknownVendorForm").submit();
		// return true;
		// }
		// // else if(!validatePassword(password)){
		// // alert('Password atleast should contain 4 characters.');
		// // return false;
		// // }
		// // else if(validateEmail(username)){
		// // }
		// }else if(username == '' || password == ''){
		// alert('EmailId and password is mandatory for login again.');
		// return false;
		// }

	});
});

// function validateEmail(email) {
//	  
// var re =
// /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// return re.test(email);
// }

// function validatePassword(password) {
//	  
// var re = /^[d]{4,10}$/;
// return re.test(password);
// }

$(function() {
	$('#submit1').click(function() {
		$('input[type="text"]').each(function() {
			if ($(this).attr('value') == 'Optional') {
				this.value = '';
			}
		});

	});
});
$(function() {
	$('#submit2').click(function() {
		$('input[type="text"]').each(function() {
			if ($(this).attr('value') == 'Optional') {
				this.value = '';
			}
		});

	});
});

/**
 * Function for create the jquery datepicker in required pages.
 */
function datePicker() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
			/*
			 * showOn : 'both', buttonImageOnly : true, buttonImage : imgPath,
			 */
			changeYear : true,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#slaStartDate').removeClass('text-label');
				$('#slaEndDate').removeClass('text-label');
			}
		});
		// $("#slaStartDate").datepicker();
		// $("#slaEndDate").datepicker();
		$("#slaStartDate").datepicker({
			onSelect : function(selected) {
				$("#slaEndDate").datepicker("option", "minDate", selected);
			}
		});
		$("#slaEndDate").datepicker({
			onSelect : function(selected) {
				$("#slaStartDate").datepicker("option", "maxDate", selected);
			}
		});
	});
}

function datePickerAssessment() {
	$(document).ready(function() {

		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			dateFormat : 'dd/mm/yy',
			onClose : function(dateText, inst) {
				$('.datePicker').removeClass('text-label');
			}
		});
		$('.datePicker').each(function() {
			if ($(this).val() == null || $(this).val() == "") {
				$(this).datepicker();
			}
		});

	});
}

function datePickerFiscal() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#fiscalStartDate').removeClass('text-label');
				$('#fiscalEndDate').removeClass('text-label');
			}
		});
		$("#fiscalStartDate").datepicker();
		$("#fiscalEndDate").datepicker();

	});
}

function datePickerExp() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			// minDate : 0,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#expDate1').removeClass('text-label');
				$('#expDate2').removeClass('text-label');
				$('#expDate3').removeClass('text-label');
				$('#effDate1').removeClass('text-label');
				$('#effDate2').removeClass('text-label');
				$('#effDate3').removeClass('text-label');
			}
		});
		$("#expDate1").datepicker();
		$("#expDate2").datepicker();
		$("#expDate3").datepicker();
		$("#effDate1").datepicker();
		$("#effDate2").datepicker();
		$("#effDate3").datepicker();

		$("#effDate1").datepicker({
			onSelect : function(selected) {
				$("#expDate1").datepicker("option", "minDate", selected);
			}
		});
		$("#expDate1").datepicker({
			onSelect : function(selected) {
				$("#effDate1").datepicker("option", "maxDate", selected);
			}
		});
		$("#effDate2").datepicker({
			onSelect : function(selected) {
				$("#expDate2").datepicker("option", "minDate", selected);
			}
		});
		$("#expDate2").datepicker({
			onSelect : function(selected) {
				$("#effDate2").datepicker("option", "maxDate", selected);
			}
		});
		$("#effDate3").datepicker({
			onSelect : function(selected) {
				$("#expDate3").datepicker("option", "minDate", selected);
			}
		});
		$("#expDate3").datepicker({
			onSelect : function(selected) {
				$("#effDate3").datepicker("option", "maxDate", selected);
			}
		});

	});
}

function datePickerOtherExp() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			// minDate : 0,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('.othexpDate1').removeClass('text-label');
			}
		});

		$('.othexpDate1').each(function() {
			if ($(this).val() == null || $(this).val() == "") {
				$(this).datepicker();
			}
		});
	});
}

function datePickerRFI() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			minDate : 0,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#rfiStartDate').removeClass('text-label');
				$('#rfiEndDate').removeClass('text-label');
			}
		});
		$("#rfiStartDate").datepicker();
		$("#rfiEndDate").datepicker();

	});
}

function spendDatePicker() {
	$(document).ready(function() {

		/* For Datepicker */
		$.datepicker.setDefaults({
			/*
			 * showOn : 'both', buttonImageOnly : true, buttonImage : imgPath,
			 */
			changeYear : true,
			dateFormat : 'mm/dd/yy',
			onClose : function(dateText, inst) {
				$('#spendStartDate').removeClass('text-label');
				$('#spendEndDate').removeClass('text-label');
			}
		});
		$("#spendStartDate").datepicker();
		$("#spendEndDate").datepicker();

	});
}

/*
 * Validation for diverse classification entry in edit vendors page.
 * 
 */
function editValidateCertificate() {

	var certificateType1 = $('#divCertType1').val();
	var certifyingAgency1 = $('#divCertAgen1').val();
	var expairyDate1 = $('#expDate1').val();

	var downloadlink1 = null;

	if ($('#downloadFile1').attr('href')) {
		downloadlink1 = $('#downloadFile1').attr('href');
	} else {
		downloadlink1 = $('#certfile1').val();
	}

	if (certificateType1 != 0 && certifyingAgency1 == "" && expairyDate1 == ""
			&& downloadlink1 == "") {
		alert('Corresponding certifying agency and expairy date and upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	} else if (certificateType1 != 0 && certifyingAgency1 != 0
			&& expairyDate1 == "" && downloadlink1 == "") {
		alert('Corresponding expairy date and upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	} else if (certificateType1 != 0 && certifyingAgency1 != 0
			&& expairyDate1 != "" && downloadlink1 == "") {
		alert('Corresponding upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	}

	var certificateType2 = $('#divCertType2').val();
	var certifyingAgency2 = $('#divCertAgen2').val();
	var expairyDate2 = $('#expDate2').val();

	var downloadlink2 = null;

	if ($('#downloadFile2').attr('href')) {
		downloadlink2 = $('#downloadFile2').attr('href');
	} else {
		downloadlink2 = $('#certfile2').val();
	}

	if (certificateType2 != 0 && certifyingAgency2 == "" && expairyDate2 == ""
			&& downloadlink2 == "") {
		alert('Corresponding certifying agency and expairy date and upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	} else if (certificateType2 != 0 && certifyingAgency2 != 0
			&& expairyDate2 == "" && downloadlink2 == "") {
		alert('Corresponding expairy date and upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	} else if (certificateType2 != 0 && certifyingAgency2 != 0
			&& expairyDate2 != "" && downloadlink2 == "") {
		alert('Corresponding upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	}

	var certificateType3 = $('#divCertType3').val();
	var certifyingAgency3 = $('#divCertAgen3').val();
	var expairyDate3 = $('#expDate3').val();

	var downloadlink3 = null;

	if ($('#downloadFile3').attr('href')) {
		downloadlink3 = $('#downloadFile3').attr('href');
	} else {
		downloadlink3 = $('#certfile3').val();
	}

	if (certificateType3 != 0 && certifyingAgency3 == "" && expairyDate3 == ""
			&& downloadlink3 == "") {
		alert('Corresponding certifying agency and expairy date and upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	} else if (certificateType3 != 0 && certifyingAgency3 != 0
			&& expairyDate3 == "" && downloadlink3 == "") {
		alert('Corresponding expairy date and upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	} else if (certificateType3 != 0 && certifyingAgency3 != 0
			&& expairyDate3 != "" && downloadlink3 == "") {
		alert('Corresponding upload certificate in diverse classification tab is mandatory ');
		e.preventDefault();
	}
}

function enableContactDate() {
	$(document).ready(function() {
		$.datepicker.setDefaults({
//			showOn : 'both',
//			buttonImageOnly : true,
//			buttonImage : imgPath,
			changeYear : true,
			minDate : 0,
			dateFormat : 'mm/dd/yy'
		});
		$("#contactDate").datepicker();

	});
}

function clearNaics(value) {
	//var vendorId = $("#vendorId").val();
	input_box = confirm("Are you sure you want to delete this Record?");
	if (input_box == true) {
		if (value == '1') {
			var naicsID1 = $("#naicsID1").val();
			if (naicsID1 != "") {
				$.ajax({
					url : "deletenaics.do?method=deleteNaics&id=" + naicsID1,
					type : "POST",
					async : false,
					success : function(data) {
						$("#naicsID1").val('0');
						$("#naicsCode_0").val(' ');
						$("#naicsDesc_0").val(' ');
						$("#naicsDesc0_0").val(' ');
						//$("#primary_0").prop('checked', false);
					}
				});
			}
			$("#naicsCode_0").val(' ');
			$("#naicsDesc_0").val(' ');
			$("#naicsDesc0_0").val(' ');
		//	$("#primary_0").prop('checked', false);
		} else if (value == '2') {
			var naicsID2 = $("#naicsID2").val();
			if (naicsID2 != "") {
				$.ajax({
					url : "deletenaics.do?method=deleteNaics&id=" + naicsID2,
					type : "POST",
					async : false,
					success : function(data) {
						$("#naicsID2").val('0');
						$("#naicsCode_1").val(' ');
						$("#naicsDesc_1").val(' ');
						$("#naicsDesc1_1").val(' ');
					//	$("#primary_1").prop('checked', false);
					}
				});
			}
			$("#naicsCode_1").val(' ');
			$("#naicsDesc_1").val(' ');
			$("#naicsDesc1_1").val(' ');
			//$("#primary_1").prop('checked', false);
		} else {
			var naicsID3 = $("#naicsID3").val();
			if (naicsID3 != "") {
				$.ajax({
					url : "deletenaics.do?method=deleteNaics&id=" + naicsID3,
					type : "POST",
					async : false,
					success : function(data) {
						$("#naicsID3").val('0');
						$("#naicsCode_2").val(' ');
						$("#naicsDesc_2").val(' ');
						$("#naicsDesc2_2").val(' ');
					//	$("#primary_2").prop('checked', false);
					}
				});
			}
			$("#naicsCode_2").val(' ');
			$("#naicsDesc_2").val(' ');
			$("#naicsDesc2_2").val(' ');
			//$("#primary_2").prop('checked', false);
		}
		return true;
	} else {
		// Output when Cancel is clicked
		// alert("Delete cancelled");
		return false;
	}
}

function clearCertificate(value) {
	var vendorId = $("#vendorId").val();
	input_box = confirm("Are you sure you want to delete this Record?");
	if (input_box == true) {
		if (value == '1') {
			var vendorCertID1 = $("#vendorCertID1").val();
			if (vendorCertID1 != "" && vendorId != "") {
				$.ajax({
					url : "deletevendorcert.do?method=deleteVendorCert&id="
							+ vendorCertID1,
					type : "POST",
					async : false,
					success : function(data) {
						$("#vendorCertID1").val('0');
						$("#divCertType1").val('').select2();
						$("#divCertAgen1").val('').select2();
						$("#certType1").val('').select2();
						$("#certificationNo1").val('');
						$("#effDate1").val('');
						$("#expDate1").val('');
						$("#uploadFile1").html('');
						$("#ethnicity1").val('').select2();
						$("#downloadFile1").remove();
					}
				});
			}
			$("#divCertType1").val('').select2();
			$("#divCertAgen1").val('').select2();
			$("#certType1").val('').select2();
			$("#certificationNo1").val('');
			$("#effDate1").val('');
			$("#expDate1").val('');
			$("#uploadFile1").html('');
			$("#ethnicity1").val('').select2();
		} else if (value == '2') {
			var vendorCertID2 = $("#vendorCertID2").val();
			if (vendorCertID2 != "" && vendorId != "") {
				$.ajax({
					url : "deletevendorcert.do?method=deleteVendorCert&id="
							+ vendorCertID2,
					type : "POST",
					async : false,
					success : function(data) {
						$("#vendorCertID2").val('0');
						$("#divCertType2").val('').select2();
						$("#divCertAgen2").val('').select2();
						$("#certType2").val('').select2();
						$("#certificationNo2").val('');
						$("#effDate2").val('');
						$("#expDate2").val('');
						$("#uploadFile2").html('');
						$("#ethnicity2").val('').select2();
						$("#downloadFile2").remove();
					}
				});
			}
			$("#divCertType2").val('').select2();
			$("#divCertAgen2").val('').select2();
			$("#certType2").val('').select2();
			$("#certificationNo2").val('');
			$("#effDate2").val('');
			$("#expDate2").val('');
			$("#uploadFile2").html('');
			$("#ethnicity2").val('').select2();
		} else {
			var vendorCertID3 = $("#vendorCertID3").val();
			if (vendorCertID3 != "" && vendorId != "") {
				$.ajax({
					url : "deletevendorcert.do?method=deleteVendorCert&id="
							+ vendorCertID3,
					type : "POST",
					async : false,
					success : function(data) {
						$("#vendorCertID3").val('0');
						$("#divCertType3").val('').select2();
						$("#divCertAgen3").val('').select2();
						$("#certType3").val('').select2();
						$("#certificationNo3").val('');
						$("#effDate3").val('');
						$("#expDate3").val('');
						$("#uploadFile3").html('');
						$("#ethnicity3").val('').select2();
						$("#downloadFile3").remove();
					}
				});
			}
			$("#divCertType3").val('').select2();
			$("#divCertAgen3").val('').select2();
			$("#certType3").val('').select2();
			$("#certificationNo3").val('');
			$("#effDate3").val('');
			$("#expDate3").val('');
			$("#uploadFile3").html('');
			$("#ethnicity3").val('').select2();
		}
		return true;
	} else {
		return false;
	}
}

function clearOtherCertificate(value, vendorOtherCertID) {
	//alert(vendorOtherCertID);
	var vendorId = $("#vendorId").val();
	input_box = confirm("Are you sure you want to delete this Record?");
	if (input_box == true) {
		if (value != '0') {
			if (vendorOtherCertID != "" && vendorId != ""
					&& "0" != vendorOtherCertID) {
				$.ajax({
					url : "deleteothercert.do?method=deleteVendorOtherCert&id="
							+ vendorOtherCertID,
					type : "POST",
					async : false,
					success : function(data) {
						$("#otherCertType" + value).val('-1').select2();
						$("#othexpDate" + value).val('');
						$("#otherFile" + value).html('');
						$("#downloadOtherFile" + value).remove();
					}
				});
			}
			$("#otherCertType" + value).val('').select2();
			$("#othexpDate" + value).val('');
			$("#otherFile" + value).html('');
			if($("#downloadOtherFile" + value).length>0){
				$("#downloadOtherFile" + value).remove();
			}
		}
		return true;
	} else {
		return false;
	}
}

function viewModifiedDate(id) {
	
	if(id != "" && id != 0){
		$.ajax({
	        url: "viewModifiedDate.do?method=viewModifiedInfo&id=" + id,
	        type: "POST",
	        async: false,
	        success: function (data) {
	            var wWidth = $(window).width();
	            var dWidth = wWidth * 0.73;
	            var wHeight = $(window).height();
	            var dHeight = wHeight * 0.8;
	            $(function () {
	                $("#viewModifiedInfo").css({
	                    'display': 'block',
	                    'font-size': 'inherit'
	                });
	                $("#viewModifiedInfo").dialog({
	                    autoOpen: false,
	                    width: dWidth,
	                    height: dHeight,
	                    modal: true,
	                    close: function (event, ui) {
	                        //close event goes here
	                    },
	                    show: {
	                        effect: "scale",
	                        duration: 1000
	                    },
	                    hide: {
	                        effect: "scale",
	                        duration: 1000
	                    }
	                });
	            });
	            $("#viewModifiedDates").html(data);
	            $("#viewModifiedInfo").dialog("open");
	        }
	    });
	}
}