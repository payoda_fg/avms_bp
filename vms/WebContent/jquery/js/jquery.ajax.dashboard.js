/**
 * This script used to load the dashboard reports.
 */

/*
 * Get the quarter of current month.
 */
function getQuarter(year) {
	d = new Date(); // If no date supplied, use today
	var q = [ 4, 1, 2, 3 ];
	var currentQuarterNo = q[Math.floor(d.getMonth() / 3)];
	var currentQuarter = '';
	if (currentQuarterNo == 0) {
		currentQuarter = 'Jan 01, ' + year + ' - Mar 31, ' + year + '';
	} else if (currentQuarterNo == 1) {
		currentQuarter = 'Apr 01, ' + year + ' - Jun 30, ' + year + '';
	} else if (currentQuarterNo == 2) {
		currentQuarter = 'Jul 01, ' + year + ' - Sep 30, ' + year + '';
	} else if (currentQuarterNo == 3) {
		currentQuarter = 'Oct 01, ' + year + ' - Dec 31, ' + year + '';
	}
	return currentQuarter;
}

/*
 * Get Total sales dashboard.
 */
function getTotalSalesDBReport(year) {

	var currentQuarter = getQuarter(year);

	$.ajax({
		url : "totalSalesReportDB.do?method=totalSpendDashboardReport&spendYear="
				+ year + "&reportingPeriod=" + currentQuarter,
		type : "POST",
		async : false,
		success : function(data) {
			//alert(data);
			$("#gridtable4").jqGrid('GridUnload');
			gridTotalSalesData(data);
		}
	});
}

function gridTotalSalesData(myGridData) {

	var newdata = jQuery.parseJSON(myGridData);
	$('#gridtable4').jqGrid(
			{

				data : newdata,
				datatype : 'local',
				colNames : [ 'Vendor Name', /*'Reporting Period',*/ 'Direct Spend', 'Indirect Spend',
						'Total Spend Reported', 'Date Submitted' ],
				colModel : [ {
					name : 'vendorName',					
					index : 'vendorName',
					sorttype: 'string'
				},/* {
					name : 'reportPeriod',
					index : 'reportPeriod'
				},*/ {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'createdOn',
					index : 'createdOn',
					sorttype : 'date',
					formatter : 'date',
					formatoptions : {
					 srcformat:'m-d-y',
					 newformat: 'm-d-Y'
					}
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager4',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				sortname : 'totalSales',
				sortorder : 'desc',
				emptyrecords : 'No data available for search criteria..',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate : {
					title : false
				},
				height : 'auto',
				gridComplete : function() {
					var totalDirectExp = $("#gridtable4").jqGrid('getCol',
							'directExp', false, 'sum'), totalIndirectExp = $(
							"#gridtable4").jqGrid('getCol', 'indirectExp',
							false, 'sum'), totalSales = $("#gridtable4")
							.jqGrid('getCol', 'totalSales', false, 'sum');
					$("#gridtable4").jqGrid('footerData', 'set', {
						vendorName : 'Total Spend',
						directExp : totalDirectExp,
						indirectExp : totalIndirectExp,
						totalSales : totalSales
					});
				}
			}).jqGrid('navGrid', '#pager4', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{
		drag : true,
		closeOnEscape : true
	});

	$(window).bind('resize',function() {
		$("#gridtable4").setGridWidth(
				$('#grid_container5').width() - 30, true);
	}).trigger('resize');
}

/*
 * Get direct spend dashboard.
 */
function getDirectSpendDBReport(year) {

	var currentQuarter = getQuarter(year);

	$.ajax({
		url : "directSpendReportDB.do?method=directSpendDashboardReport&spendYear="
				+ year + "&reportingPeriod=" + currentQuarter,
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable5").jqGrid('GridUnload');
			gridDirectSpendDBData(data);
		}
	});
}

function gridDirectSpendDBData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable5').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name', /*'Reporting Period',*/ 'Direct Spend',
						'Total Direct Spend Reported', '% Diversity Spend'],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName',
					sorttype: 'string'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName',
					sorttype: 'string'
				},/* {
					name : 'reportPeriod',
					index : 'reportPeriod'
				},*/ {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					sorttype : 'number',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager5',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'vendorName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalDirectExp = $("#gridtable5").jqGrid('getCol',
							'directExp', false, 'sum');
					$("#gridtable5").jqGrid('footerData', 'set', {
						vendorName : 'Total Spend',
						directExp : totalDirectExp
					});
				}
			}).jqGrid('navGrid', '#pager5', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() {
		$("#gridtable5").setGridWidth($('#grid_container6').width() - 30, true);
	}).trigger('resize');
}

/*
 * Get the indirect spend report for dashboard.
 */
function getIndirectSpendDBReport(year){
	
	var currentQuarter = getQuarter(year);

	$.ajax({
		url : "indirectSpendReportDB.do?method=indirectSpendDashboardReport&spendYear="
				+ year + "&reportingPeriod=" + currentQuarter,
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable6").jqGrid('GridUnload');
			gridIndirectSpendData(data);
		}
	});
}

function gridIndirectSpendData(myGridData) {

	var newdata = jQuery.parseJSON(myGridData);
	$('#gridtable6').jqGrid(
			{
				data : newdata,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier 2 Vendor Name', /*'Reporting Period',*/ 'Indirect Spend', 
				             'Total Spend Reported',
				             	'% Diversity Spend'],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName',
					sorttype: 'string'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName',
					sorttype: 'string'
				},/* {
					name : 'reportPeriod',
					index : 'reportPeriod'
				},*/ {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					sorttype: 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					sorttype: 'number',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager6',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				grouping : true,
				groupingView : {
					groupField : [ 'vendorName' ],
					groupColumnShow : [ true ],
					groupText : [ '<b>{0} - {1} Item(s)</b>' ],
					groupCollapse : false,
					groupOrder : [ 'asc' ],
					groupSummary : [ true ],
					showSummaryOnHide : true,
					groupDataSorted : true
				},
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalIndirectExp = $("#gridtable6").jqGrid('getCol',
							'indirectExp', false, 'sum');
					$("#gridtable6").jqGrid('footerData', 'set', {
						vendorName : 'Total Spend',
						indirectExp : totalIndirectExp
					});
				}
			}).jqGrid('navGrid', '#pager6', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() {
		$("#gridtable6").setGridWidth($('#grid_container7').width() - 30, true);
	}).trigger('resize');
}

/*
 * Get the diversity report for dashboard.
 */
function getDiversityDBReport(year){
	
	var currentQuarter = getQuarter(year);

	$.ajax({
		url : "diversityReportDB.do?method=diversityDashboardReport&spendYear="
				+ year + "&reportingPeriod=" + currentQuarter,
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable7").jqGrid('GridUnload');
			gridDiversityData(data);
		}
	});
}

function gridDiversityData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable7').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', 'Tier2 Vendor Name', /*'Reporting Period', */
				             'Direct Spend', 'Total Spend Reported', 
				             '% Diversity Spend' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName',
					sorttype: 'string'
				}, {
					name : 'vendorUserName',
					index : 'vendorUserName',
					sorttype: 'string'
				},/* {
					name : 'reportPeriod',
					index : 'reportPeriod'
				},*/ {
					name : 'directExp',
					index : 'directExp',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				}, {
					name : 'totalSales',
					index : 'totalSales',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					}
				}, {
					name : 'diversityPercent',
					index : 'diversityPercent',
					sorttype : 'number',
					formatter : 'number',
					formatoptions : {
						suffix : '%',
						decimalSeparator : ".",
						decimalPlaces : 1
					},
					align : 'center'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager7',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalDirectExp = $("#gridtable7").jqGrid('getCol',
							'directExp', false, 'sum');
					$("#gridtable7").jqGrid('footerData', 'set', {
						vendorName : 'Total Spend',
						directExp : totalDirectExp
					});
				}
			}).jqGrid('navGrid', '#pager7', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() {
		$("#gridtable7").setGridWidth($('#grid_container8').width() - 30, true);
	}).trigger('resize');

}

/*
 * Get Ethnicity analysis report.
 */
function getEthnicityAnalysisDBReport(year){
	var currentQuarter = getQuarter(year);

	$.ajax({
		url : "ethnicityReportDB.do?method=ethnicityDashboardReport&spendYear="
				+ year + "&reportingPeriod=" + currentQuarter,
		type : "POST",
		async : false,
		success : function(data) {
			$("#gridtable8").jqGrid('GridUnload');
			gridEthnicityData(data);
		}
	});
}

function gridEthnicityData(myGridData) {

	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable8').jqGrid(
			{

				data : newdata1,
				datatype : 'local',
				colNames : [ 'Vendor Name', /*'Reporting Period',*/ 'InDirect Spend' ],
				colModel : [ {
					name : 'vendorName',
					index : 'vendorName',
					sorttype: 'string',
					align : 'center'
				}, /*{
					name : 'reportPeriod',
					index : 'reportPeriod'
				},*/ {
					name : 'indirectExp',
					index : 'indirectExp',
					align : 'right',
					sorttype : 'currency',
					formatter : 'currency',
					formatoptions : {
						prefix : '$',
						thousandsSeparator : ',',
						decimalPlaces: 0
					},
					summaryType : 'sum'
				} ],
				rowNum : 10,
				rowList : [ 10, 20, 50 ],
				pager : '#pager8',
				shrinkToFit : false,
				autowidth : true,
				viewrecords : true,
				emptyrecords : 'No data available for search criteria..',
				footerrow : true,
				userDataOnFooter : true,
				cmTemplate: { title: false },
				height : 'auto',
				gridComplete : function() {
					var totalIndirectExp = $("#gridtable8").jqGrid('getCol',
							'indirectExp', false, 'sum');
					$("#gridtable8").jqGrid('footerData', 'set', {
						vendorName : 'Total Spend',
						indirectExp : totalIndirectExp
					});
				}
			}).jqGrid('navGrid', '#pager8', {
		add : false,
		edit : false,
		del : false,
		search : false,
		refresh : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() {
		$("#gridtable8").setGridWidth($('#grid_container9').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in New Registrations By Month Dashboard Report.
 */
function getNewRegistrationsByMonthReport() 
{
	$.ajax(
	{
		url : "newRegistrationsByMonthReportDB.do?method=getNewRegistrationsByMonthReportChartData",
		type : "POST",
		async : false,
		success : function(data) 
		{
			$("#gridtable12").jqGrid('GridUnload');
			gridNewRegistrationsByMonthReportData(data);
		}
	});
}

function gridNewRegistrationsByMonthReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable12').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Month & Year', 'Count', '%' ],
		colModel : [ 
		            {
		            	name : 'month',
		            	index : 'month',
		            	sorttype: 'string',
		            	align : 'center'
		            }, {
		            	name : 'count',
		            	index : 'count',
		            	sorttype: 'number',
		            	align : 'center'
		            }, {
		            	name : 'percentage',
		            	index : 'percentage',
		            	align : 'center',
		            	formatter: 'number', 
		            	formatoptions: {suffix: '%'},
		            	sorttype: 'number'
		            } ],
		rowNum : 10,
		rowList : [ 10, 20, 50 ],
		pager : '#pager12',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available',
		footerrow : true,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		height : 'auto',
		gridComplete : function() 
		{
			var totalcount = $("#gridtable12").jqGrid('getCol', 'count', false, 'sum');
			$("#gridtable12").jqGrid('footerData', 'set', 
			{
				month : 'Total Records',
				count : totalcount
			});
		}
	}).jqGrid('navGrid', '#pager12', 
	{
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() 
	{
		$("#gridtable12").setGridWidth($('#grid_container12').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in Vendors By Business Type Dashboard Report.
 */
function getVendorsByIndustryDashboardReport() 
{
	$.ajax(
	{
		url : "vendorsByIndustryReportDB.do?method=getVendorsByIndustryDashboardReportChartData",
		type : "POST",
		async : false,
		success : function(data) 
		{
			$("#gridtable13").jqGrid('GridUnload');
			gridVendorsByIndustryDashboardReportData(data);
		}
	});
}

function gridVendorsByIndustryDashboardReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable13').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Business Type', 'Vendor Count', '%' ],
		colModel : [ 
		            {
		            	name : 'businessTypeName',
		            	index : 'businessTypeName',
		            	sorttype: 'string',
		            	align : 'center'
		            }, {
		            	name : 'vendorCount',
		            	index : 'vendorCount',
		            	sorttype: 'number',
		            	align : 'center'
		            }, {
		            	name : 'percent',
		            	index : 'percent',
		            	align : 'center',
		            	formatter: 'number', 
		            	formatoptions: {suffix: '%'},
		            	sorttype: 'number'
		            } ],
		rowNum : 10,
		pgbuttons:false,
        pgtext:'',
		pager : '#pager13',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available',
		footerrow : false,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		height : 'auto'		
	}).jqGrid('navGrid', '#pager13', 
	{
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() 
	{
		$("#gridtable13").setGridWidth($('#grid_container13').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in BP Vendors By Business Type Dashboard Report.
 */
function getBPVendorsByIndustryDashboardReport() 
{
	$.ajax(
	{
		url : "bpVendorsByIndustryReportDB.do?method=getBPVendorsByIndustryDashboardReportChartData",
		type : "POST",
		async : false,
		success : function(data) 
		{
			$("#gridtable14").jqGrid('GridUnload');
			gridBPVendorsByIndustryDashboardReportData(data);
		}
	});
}

function gridBPVendorsByIndustryDashboardReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable14').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'Business Type', 'Vendor Count', '%' ],
		colModel : [ 
		            {
		            	name : 'businessTypeName',
		            	index : 'businessTypeName',
		            	sorttype: 'string',
		            	align : 'center'
		            }, {
		            	name : 'vendorCount',
		            	index : 'vendorCount',
		            	sorttype: 'number',
		            	align : 'center'
		            }, {
		            	name : 'percent',
		            	index : 'percent',
		            	align : 'center',
		            	formatter: 'number', 
		            	formatoptions: {suffix: '%'},
		            	sorttype: 'number'
		            } ],
		rowNum : 10,
		pgbuttons:false,
        pgtext:'',
		pager : '#pager14',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available',
		footerrow : false,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		height : 'auto'		
	}).jqGrid('navGrid', '#pager14', 
	{
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() 
	{
		$("#gridtable14").setGridWidth($('#grid_container14').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in Vendors By NAICS Description Dashboard Report.
 */
function getVendorsByNaicsDescriptionDashboardReport() 
{
	$.ajax(
	{
		url : "vendorsByNaicsDescriptionReportDB.do?method=getVendorsByNaicsDescriptionDashboardReportChartData",
		type : "POST",
		async : false,
		success : function(data) 
		{
			$("#gridtable15").jqGrid('GridUnload');
			gridVendorsByNaicsDescriptionDashboardReportData(data);
		}
	});
}

function gridVendorsByNaicsDescriptionDashboardReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable15').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'NAICS Description', 'Vendor Count', '%' ],
		colModel : [ 
		            {
		            	name : 'naicsDesc',
		            	index : 'naicsDesc',
		            	sorttype: 'string',
		            	align : 'center'
		            }, {
		            	name : 'vendorCount',
		            	index : 'vendorCount',
		            	sorttype: 'number',
		            	align : 'center'
		            }, {
		            	name : 'percent',
		            	index : 'percent',
		            	align : 'center',
		            	formatter: 'number', 
		            	formatoptions: {suffix: '%'},
		            	sorttype: 'number'
		            } ],
		rowNum : 10,
		pgbuttons:false,
        pgtext:'',
		pager : '#pager15',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available',
		footerrow : false,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		height : 'auto'		
	}).jqGrid('navGrid', '#pager15', 
	{
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() 
	{
		$("#gridtable15").setGridWidth($('#grid_container15').width() - 30, true);
	}).trigger('resize');
}

/*
 * This is functionality for populate grid in BP Vendors By BP Market Sector Dashboard Report.
 */
function getVendorsByBPMarketSectorDashboardReport() 
{
	$.ajax(
	{
		url : "vendorsByBPMarketSectorReportDB.do?method=getVendorsByBPMarketSectorDashboardReportChartData",
		type : "POST",
		async : false,
		success : function(data) 
		{
			$("#gridtable16").jqGrid('GridUnload');
			gridVendorsByBPMarketSectorDashboardReportData(data);
		}
	});
}

function gridVendorsByBPMarketSectorDashboardReportData(myGridData) 
{
	var newdata1 = jQuery.parseJSON(myGridData);

	$('#gridtable16').jqGrid(
	{
		data : newdata1,
		datatype : 'local',
		colNames : [ 'BP Market Sector', 'Vendor Count', '%' ],
		colModel : [ 
		            {
		            	name : 'bpMarketSector',
		            	index : 'bpMarketSector',
		            	sorttype: 'string',
		            	align : 'center'
		            }, {
		            	name : 'vendorCount',
		            	index : 'vendorCount',
		            	sorttype: 'number',
		            	align : 'center'
		            }, {
		            	name : 'percent',
		            	index : 'percent',
		            	align : 'center',
		            	formatter: 'number', 
		            	formatoptions: {suffix: '%'},
		            	sorttype: 'number'
		            } ],
		rowNum : 10,
		pgbuttons:false,
        pgtext:'',
		pager : '#pager16',
		shrinkToFit : false,
		autowidth : true,
		viewrecords : true,
		emptyrecords : 'No data available',
		footerrow : false,
		userDataOnFooter : true,
		cmTemplate: { title: false },
		height : 'auto'		
	}).jqGrid('navGrid', '#pager16', 
	{
		add : false,
		edit : false,
		del : false,
		refresh : false,
		search : false
	},

	{}, /* edit options */
	{}, /* add options */
	{}, /* del options */
	{});

	$(window).bind('resize', function() 
	{
		$("#gridtable16").setGridWidth($('#grid_container16').width() - 30, true);
	}).trigger('resize');
}